package android.support.coreutils;

public final class BuildConfig
{
  public static final String APPLICATION_ID = "android.support.coreutils";
  public static final String BUILD_TYPE = "release";
  public static final boolean DEBUG = false;
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = -1;
  public static final String VERSION_NAME = "";
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/android/support/coreutils/BuildConfig.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */