package android.support.v4.os;

import android.os.Parcelable.Creator;

class ParcelableCompatCreatorHoneycombMR2Stub
{
  static <T> Parcelable.Creator<T> instantiate(ParcelableCompatCreatorCallbacks<T> paramParcelableCompatCreatorCallbacks)
  {
    return new ParcelableCompatCreatorHoneycombMR2(paramParcelableCompatCreatorCallbacks);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/android/support/v4/os/ParcelableCompatCreatorHoneycombMR2Stub.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */