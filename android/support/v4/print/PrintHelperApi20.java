package android.support.v4.print;

import android.content.Context;

class PrintHelperApi20
  extends PrintHelperKitkat
{
  PrintHelperApi20(Context paramContext)
  {
    super(paramContext);
    this.mPrintActivityRespectsOrientation = false;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/android/support/v4/print/PrintHelperApi20.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */