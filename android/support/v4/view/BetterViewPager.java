package android.support.v4.view;

import android.content.Context;
import android.util.AttributeSet;

public class BetterViewPager
  extends ViewPager
{
  public BetterViewPager(Context paramContext)
  {
    super(paramContext);
  }
  
  public BetterViewPager(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  public void setChildrenDrawingOrderEnabledCompat(boolean paramBoolean)
  {
    setChildrenDrawingOrderEnabled(paramBoolean);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/android/support/v4/view/BetterViewPager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */