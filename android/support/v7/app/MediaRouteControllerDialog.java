package android.support.v7.app;

import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.v4.media.MediaDescriptionCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.MediaControllerCompat.Callback;
import android.support.v4.media.session.MediaControllerCompat.TransportControls;
import android.support.v4.media.session.MediaSessionCompat.Token;
import android.support.v4.media.session.PlaybackStateCompat;
import android.support.v7.media.MediaRouteSelector;
import android.support.v7.media.MediaRouter;
import android.support.v7.media.MediaRouter.Callback;
import android.support.v7.media.MediaRouter.RouteGroup;
import android.support.v7.media.MediaRouter.RouteInfo;
import android.support.v7.mediarouter.R.attr;
import android.support.v7.mediarouter.R.dimen;
import android.support.v7.mediarouter.R.id;
import android.support.v7.mediarouter.R.integer;
import android.support.v7.mediarouter.R.interpolator;
import android.support.v7.mediarouter.R.layout;
import android.support.v7.mediarouter.R.string;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.view.animation.Transformation;
import android.view.animation.TranslateAnimation;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class MediaRouteControllerDialog
  extends AlertDialog
{
  private static final int BUTTON_DISCONNECT_RES_ID = 16908314;
  private static final int BUTTON_NEUTRAL_RES_ID = 16908315;
  private static final int BUTTON_STOP_RES_ID = 16908313;
  private static final int CONNECTION_TIMEOUT_MILLIS = (int)TimeUnit.SECONDS.toMillis(30L);
  private static final boolean DEBUG = Log.isLoggable("MediaRouteCtrlDialog", 3);
  private static final String TAG = "MediaRouteCtrlDialog";
  private static final int VOLUME_UPDATE_DELAY_MILLIS = 500;
  private Interpolator mAccelerateDecelerateInterpolator;
  private final AccessibilityManager mAccessibilityManager;
  private Bitmap mArtIconBitmap;
  private Uri mArtIconUri;
  private ImageView mArtView;
  private boolean mAttachedToWindow;
  private final MediaRouterCallback mCallback = new MediaRouterCallback(null);
  private ImageButton mCloseButton;
  private Context mContext = getContext();
  private MediaControllerCallback mControllerCallback = new MediaControllerCallback(null);
  private boolean mCreated;
  private FrameLayout mCustomControlLayout;
  private View mCustomControlView;
  private FrameLayout mDefaultControlLayout;
  private MediaDescriptionCompat mDescription;
  private LinearLayout mDialogAreaLayout;
  private int mDialogContentWidth;
  private Button mDisconnectButton;
  private View mDividerView;
  private FrameLayout mExpandableAreaLayout;
  private Interpolator mFastOutSlowInInterpolator;
  private FetchArtTask mFetchArtTask;
  private MediaRouteExpandCollapseButton mGroupExpandCollapseButton;
  private int mGroupListAnimationDurationMs;
  private Runnable mGroupListFadeInAnimation = new Runnable()
  {
    public void run()
    {
      MediaRouteControllerDialog.this.startGroupListFadeInAnimation();
    }
  };
  private int mGroupListFadeInDurationMs;
  private int mGroupListFadeOutDurationMs;
  private List<MediaRouter.RouteInfo> mGroupMemberRoutes;
  private Set<MediaRouter.RouteInfo> mGroupMemberRoutesAdded;
  private Set<MediaRouter.RouteInfo> mGroupMemberRoutesAnimatingWithBitmap;
  private Set<MediaRouter.RouteInfo> mGroupMemberRoutesRemoved;
  private Interpolator mInterpolator;
  private boolean mIsGroupExpanded;
  private boolean mIsGroupListAnimating;
  private boolean mIsGroupListAnimationPending;
  private Interpolator mLinearOutSlowInInterpolator;
  private MediaControllerCompat mMediaController;
  private LinearLayout mMediaMainControlLayout;
  private ImageButton mPlayPauseButton;
  private RelativeLayout mPlaybackControlLayout;
  private final MediaRouter.RouteInfo mRoute = this.mRouter.getSelectedRoute();
  private MediaRouter.RouteInfo mRouteInVolumeSliderTouched;
  private TextView mRouteNameTextView;
  private final MediaRouter mRouter = MediaRouter.getInstance(this.mContext);
  private PlaybackStateCompat mState;
  private Button mStopCastingButton;
  private TextView mSubtitleView;
  private TextView mTitleView;
  private VolumeChangeListener mVolumeChangeListener;
  private boolean mVolumeControlEnabled = true;
  private LinearLayout mVolumeControlLayout;
  private VolumeGroupAdapter mVolumeGroupAdapter;
  private OverlayListView mVolumeGroupList;
  private int mVolumeGroupListItemHeight;
  private int mVolumeGroupListItemIconSize;
  private int mVolumeGroupListMaxHeight;
  private final int mVolumeGroupListPaddingTop;
  private SeekBar mVolumeSlider;
  private Map<MediaRouter.RouteInfo, SeekBar> mVolumeSliderMap;
  
  public MediaRouteControllerDialog(Context paramContext)
  {
    this(paramContext, 0);
  }
  
  public MediaRouteControllerDialog(Context paramContext, int paramInt)
  {
    super(MediaRouterThemeHelper.createThemedContext(paramContext, paramInt), paramInt);
    setMediaSession(this.mRouter.getMediaSessionToken());
    this.mVolumeGroupListPaddingTop = this.mContext.getResources().getDimensionPixelSize(R.dimen.mr_controller_volume_group_list_padding_top);
    this.mAccessibilityManager = ((AccessibilityManager)this.mContext.getSystemService("accessibility"));
    if (Build.VERSION.SDK_INT >= 21)
    {
      this.mLinearOutSlowInInterpolator = AnimationUtils.loadInterpolator(paramContext, R.interpolator.mr_linear_out_slow_in);
      this.mFastOutSlowInInterpolator = AnimationUtils.loadInterpolator(paramContext, R.interpolator.mr_fast_out_slow_in);
    }
    this.mAccelerateDecelerateInterpolator = new AccelerateDecelerateInterpolator();
  }
  
  private void animateGroupListItems(final Map<MediaRouter.RouteInfo, Rect> paramMap, final Map<MediaRouter.RouteInfo, BitmapDrawable> paramMap1)
  {
    this.mVolumeGroupList.setEnabled(false);
    this.mVolumeGroupList.requestLayout();
    this.mIsGroupListAnimating = true;
    this.mVolumeGroupList.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener()
    {
      public void onGlobalLayout()
      {
        MediaRouteControllerDialog.this.mVolumeGroupList.getViewTreeObserver().removeGlobalOnLayoutListener(this);
        MediaRouteControllerDialog.this.animateGroupListItemsInternal(paramMap, paramMap1);
      }
    });
  }
  
  private void animateGroupListItemsInternal(Map<MediaRouter.RouteInfo, Rect> paramMap, Map<MediaRouter.RouteInfo, BitmapDrawable> paramMap1)
  {
    if ((this.mGroupMemberRoutesAdded == null) || (this.mGroupMemberRoutesRemoved == null)) {
      return;
    }
    int i1 = this.mGroupMemberRoutesAdded.size() - this.mGroupMemberRoutesRemoved.size();
    int k = 0;
    Object localObject1 = new Animation.AnimationListener()
    {
      public void onAnimationEnd(Animation paramAnonymousAnimation) {}
      
      public void onAnimationRepeat(Animation paramAnonymousAnimation) {}
      
      public void onAnimationStart(Animation paramAnonymousAnimation)
      {
        MediaRouteControllerDialog.this.mVolumeGroupList.startAnimationAll();
        MediaRouteControllerDialog.this.mVolumeGroupList.postDelayed(MediaRouteControllerDialog.this.mGroupListFadeInAnimation, MediaRouteControllerDialog.this.mGroupListAnimationDurationMs);
      }
    };
    int i2 = this.mVolumeGroupList.getFirstVisiblePosition();
    int j = 0;
    final Object localObject2;
    Object localObject3;
    int i;
    if (j < this.mVolumeGroupList.getChildCount())
    {
      localObject2 = this.mVolumeGroupList.getChildAt(j);
      localObject3 = (MediaRouter.RouteInfo)this.mVolumeGroupAdapter.getItem(i2 + j);
      Object localObject4 = (Rect)paramMap.get(localObject3);
      int n = ((View)localObject2).getTop();
      if (localObject4 != null) {}
      for (i = ((Rect)localObject4).top;; i = n + this.mVolumeGroupListItemHeight * i1)
      {
        localObject4 = new AnimationSet(true);
        int m = i;
        if (this.mGroupMemberRoutesAdded != null)
        {
          m = i;
          if (this.mGroupMemberRoutesAdded.contains(localObject3))
          {
            m = n;
            localObject5 = new AlphaAnimation(0.0F, 0.0F);
            ((Animation)localObject5).setDuration(this.mGroupListFadeInDurationMs);
            ((AnimationSet)localObject4).addAnimation((Animation)localObject5);
          }
        }
        Object localObject5 = new TranslateAnimation(0.0F, 0.0F, m - n, 0.0F);
        ((Animation)localObject5).setDuration(this.mGroupListAnimationDurationMs);
        ((AnimationSet)localObject4).addAnimation((Animation)localObject5);
        ((AnimationSet)localObject4).setFillAfter(true);
        ((AnimationSet)localObject4).setFillEnabled(true);
        ((AnimationSet)localObject4).setInterpolator(this.mInterpolator);
        i = k;
        if (k == 0)
        {
          i = 1;
          ((AnimationSet)localObject4).setAnimationListener((Animation.AnimationListener)localObject1);
        }
        ((View)localObject2).clearAnimation();
        ((View)localObject2).startAnimation((Animation)localObject4);
        paramMap.remove(localObject3);
        paramMap1.remove(localObject3);
        j += 1;
        k = i;
        break;
      }
    }
    localObject1 = paramMap1.entrySet().iterator();
    label343:
    if (((Iterator)localObject1).hasNext())
    {
      paramMap1 = (Map.Entry)((Iterator)localObject1).next();
      localObject2 = (MediaRouter.RouteInfo)paramMap1.getKey();
      paramMap1 = (BitmapDrawable)paramMap1.getValue();
      localObject3 = (Rect)paramMap.get(localObject2);
      if (!this.mGroupMemberRoutesRemoved.contains(localObject2)) {
        break label454;
      }
      paramMap1 = new OverlayListView.OverlayObject(paramMap1, (Rect)localObject3).setAlphaAnimation(1.0F, 0.0F).setDuration(this.mGroupListFadeOutDurationMs).setInterpolator(this.mInterpolator);
    }
    for (;;)
    {
      this.mVolumeGroupList.addOverlayObject(paramMap1);
      break label343;
      break;
      label454:
      i = this.mVolumeGroupListItemHeight;
      paramMap1 = new OverlayListView.OverlayObject(paramMap1, (Rect)localObject3).setTranslateYAnimation(i1 * i).setDuration(this.mGroupListAnimationDurationMs).setInterpolator(this.mInterpolator).setAnimationEndListener(new OverlayListView.OverlayObject.OnAnimationEndListener()
      {
        public void onAnimationEnd()
        {
          MediaRouteControllerDialog.this.mGroupMemberRoutesAnimatingWithBitmap.remove(localObject2);
          MediaRouteControllerDialog.this.mVolumeGroupAdapter.notifyDataSetChanged();
        }
      });
      this.mGroupMemberRoutesAnimatingWithBitmap.add(localObject2);
    }
  }
  
  private void animateLayoutHeight(final View paramView, final int paramInt)
  {
    Animation local7 = new Animation()
    {
      protected void applyTransformation(float paramAnonymousFloat, Transformation paramAnonymousTransformation)
      {
        int i = this.val$startValue;
        int j = (int)((this.val$startValue - paramInt) * paramAnonymousFloat);
        MediaRouteControllerDialog.setLayoutHeight(paramView, i - j);
      }
    };
    local7.setDuration(this.mGroupListAnimationDurationMs);
    if (Build.VERSION.SDK_INT >= 21) {
      local7.setInterpolator(this.mInterpolator);
    }
    paramView.startAnimation(local7);
  }
  
  private boolean canShowPlaybackControlLayout()
  {
    return (this.mCustomControlView == null) && ((this.mDescription != null) || (this.mState != null));
  }
  
  private void fadeInAddedRoutes()
  {
    Animation.AnimationListener local12 = new Animation.AnimationListener()
    {
      public void onAnimationEnd(Animation paramAnonymousAnimation)
      {
        MediaRouteControllerDialog.this.finishAnimation(true);
      }
      
      public void onAnimationRepeat(Animation paramAnonymousAnimation) {}
      
      public void onAnimationStart(Animation paramAnonymousAnimation) {}
    };
    int i = 0;
    int m = this.mVolumeGroupList.getFirstVisiblePosition();
    int k = 0;
    while (k < this.mVolumeGroupList.getChildCount())
    {
      View localView = this.mVolumeGroupList.getChildAt(k);
      Object localObject = (MediaRouter.RouteInfo)this.mVolumeGroupAdapter.getItem(m + k);
      int j = i;
      if (this.mGroupMemberRoutesAdded.contains(localObject))
      {
        localObject = new AlphaAnimation(0.0F, 1.0F);
        ((Animation)localObject).setDuration(this.mGroupListFadeInDurationMs);
        ((Animation)localObject).setFillEnabled(true);
        ((Animation)localObject).setFillAfter(true);
        j = i;
        if (i == 0)
        {
          j = 1;
          ((Animation)localObject).setAnimationListener(local12);
        }
        localView.clearAnimation();
        localView.startAnimation((Animation)localObject);
      }
      k += 1;
      i = j;
    }
  }
  
  private void finishAnimation(boolean paramBoolean)
  {
    this.mGroupMemberRoutesAdded = null;
    this.mGroupMemberRoutesRemoved = null;
    this.mIsGroupListAnimating = false;
    if (this.mIsGroupListAnimationPending)
    {
      this.mIsGroupListAnimationPending = false;
      updateLayoutHeight(paramBoolean);
    }
    this.mVolumeGroupList.setEnabled(true);
  }
  
  private int getDesiredArtHeight(int paramInt1, int paramInt2)
  {
    if (paramInt1 >= paramInt2) {
      return (int)(this.mDialogContentWidth * paramInt2 / paramInt1 + 0.5F);
    }
    return (int)(this.mDialogContentWidth * 9.0F / 16.0F + 0.5F);
  }
  
  private MediaRouter.RouteGroup getGroup()
  {
    if ((this.mRoute instanceof MediaRouter.RouteGroup)) {
      return (MediaRouter.RouteGroup)this.mRoute;
    }
    return null;
  }
  
  private static int getLayoutHeight(View paramView)
  {
    return paramView.getLayoutParams().height;
  }
  
  private int getMainControllerHeight(boolean paramBoolean)
  {
    int i = 0;
    if ((paramBoolean) || (this.mVolumeControlLayout.getVisibility() == 0))
    {
      int j = 0 + (this.mMediaMainControlLayout.getPaddingTop() + this.mMediaMainControlLayout.getPaddingBottom());
      i = j;
      if (paramBoolean) {
        i = j + this.mPlaybackControlLayout.getMeasuredHeight();
      }
      j = i;
      if (this.mVolumeControlLayout.getVisibility() == 0) {
        j = i + this.mVolumeControlLayout.getMeasuredHeight();
      }
      i = j;
      if (paramBoolean)
      {
        i = j;
        if (this.mVolumeControlLayout.getVisibility() == 0) {
          i = j + this.mDividerView.getMeasuredHeight();
        }
      }
    }
    return i;
  }
  
  private boolean isVolumeControlAvailable(MediaRouter.RouteInfo paramRouteInfo)
  {
    return (this.mVolumeControlEnabled) && (paramRouteInfo.getVolumeHandling() == 1);
  }
  
  private void loadInterpolator()
  {
    if (Build.VERSION.SDK_INT >= 21)
    {
      if (this.mIsGroupExpanded) {}
      for (Interpolator localInterpolator = this.mLinearOutSlowInInterpolator;; localInterpolator = this.mFastOutSlowInInterpolator)
      {
        this.mInterpolator = localInterpolator;
        return;
      }
    }
    this.mInterpolator = this.mAccelerateDecelerateInterpolator;
  }
  
  private void rebuildVolumeGroupList(boolean paramBoolean)
  {
    if (getGroup() == null) {}
    for (List localList = null; localList == null; localList = getGroup().getRoutes())
    {
      this.mGroupMemberRoutes.clear();
      this.mVolumeGroupAdapter.notifyDataSetChanged();
      return;
    }
    if (MediaRouteDialogHelper.listUnorderedEquals(this.mGroupMemberRoutes, localList))
    {
      this.mVolumeGroupAdapter.notifyDataSetChanged();
      return;
    }
    HashMap localHashMap1;
    if (paramBoolean)
    {
      localHashMap1 = MediaRouteDialogHelper.getItemBoundMap(this.mVolumeGroupList, this.mVolumeGroupAdapter);
      if (!paramBoolean) {
        break label203;
      }
    }
    label203:
    for (HashMap localHashMap2 = MediaRouteDialogHelper.getItemBitmapMap(this.mContext, this.mVolumeGroupList, this.mVolumeGroupAdapter);; localHashMap2 = null)
    {
      this.mGroupMemberRoutesAdded = MediaRouteDialogHelper.getItemsAdded(this.mGroupMemberRoutes, localList);
      this.mGroupMemberRoutesRemoved = MediaRouteDialogHelper.getItemsRemoved(this.mGroupMemberRoutes, localList);
      this.mGroupMemberRoutes.addAll(0, this.mGroupMemberRoutesAdded);
      this.mGroupMemberRoutes.removeAll(this.mGroupMemberRoutesRemoved);
      this.mVolumeGroupAdapter.notifyDataSetChanged();
      if ((!paramBoolean) || (!this.mIsGroupExpanded) || (this.mGroupMemberRoutesAdded.size() + this.mGroupMemberRoutesRemoved.size() <= 0)) {
        break label209;
      }
      animateGroupListItems(localHashMap1, localHashMap2);
      return;
      localHashMap1 = null;
      break;
    }
    label209:
    this.mGroupMemberRoutesAdded = null;
    this.mGroupMemberRoutesRemoved = null;
  }
  
  private static void setLayoutHeight(View paramView, int paramInt)
  {
    ViewGroup.LayoutParams localLayoutParams = paramView.getLayoutParams();
    localLayoutParams.height = paramInt;
    paramView.setLayoutParams(localLayoutParams);
  }
  
  private void setMediaSession(MediaSessionCompat.Token paramToken)
  {
    Object localObject = null;
    if (this.mMediaController != null)
    {
      this.mMediaController.unregisterCallback(this.mControllerCallback);
      this.mMediaController = null;
    }
    if (paramToken == null) {}
    while (!this.mAttachedToWindow) {
      return;
    }
    try
    {
      this.mMediaController = new MediaControllerCompat(this.mContext, paramToken);
      if (this.mMediaController != null) {
        this.mMediaController.registerCallback(this.mControllerCallback);
      }
      if (this.mMediaController == null)
      {
        paramToken = null;
        if (paramToken != null) {
          break label136;
        }
        paramToken = null;
        this.mDescription = paramToken;
        if (this.mMediaController != null) {
          break label144;
        }
        paramToken = (MediaSessionCompat.Token)localObject;
        this.mState = paramToken;
        update(false);
      }
    }
    catch (RemoteException paramToken)
    {
      for (;;)
      {
        Log.e("MediaRouteCtrlDialog", "Error creating media controller in setMediaSession.", paramToken);
        continue;
        paramToken = this.mMediaController.getMetadata();
        continue;
        label136:
        paramToken = paramToken.getDescription();
        continue;
        label144:
        paramToken = this.mMediaController.getPlaybackState();
      }
    }
  }
  
  private void startGroupListFadeInAnimation()
  {
    clearGroupListAnimation(true);
    this.mVolumeGroupList.requestLayout();
    this.mVolumeGroupList.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener()
    {
      public void onGlobalLayout()
      {
        MediaRouteControllerDialog.this.mVolumeGroupList.getViewTreeObserver().removeGlobalOnLayoutListener(this);
        MediaRouteControllerDialog.this.startGroupListFadeInAnimationInternal();
      }
    });
  }
  
  private void startGroupListFadeInAnimationInternal()
  {
    if ((this.mGroupMemberRoutesAdded != null) && (this.mGroupMemberRoutesAdded.size() != 0))
    {
      fadeInAddedRoutes();
      return;
    }
    finishAnimation(true);
  }
  
  private void update(boolean paramBoolean)
  {
    if ((!this.mRoute.isSelected()) || (this.mRoute.isDefaultOrBluetooth())) {
      dismiss();
    }
    while (!this.mCreated) {
      return;
    }
    this.mRouteNameTextView.setText(this.mRoute.getName());
    Button localButton = this.mDisconnectButton;
    if (this.mRoute.canDisconnect()) {}
    for (int i = 0;; i = 8)
    {
      localButton.setVisibility(i);
      if (this.mCustomControlView == null)
      {
        if (this.mFetchArtTask != null) {
          this.mFetchArtTask.cancel(true);
        }
        this.mFetchArtTask = new FetchArtTask();
        this.mFetchArtTask.execute(new Void[0]);
      }
      updateVolumeControlLayout();
      updatePlaybackControlLayout();
      updateLayoutHeight(paramBoolean);
      return;
    }
  }
  
  private void updateLayoutHeight(final boolean paramBoolean)
  {
    this.mDefaultControlLayout.requestLayout();
    this.mDefaultControlLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener()
    {
      public void onGlobalLayout()
      {
        MediaRouteControllerDialog.this.mDefaultControlLayout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
        if (MediaRouteControllerDialog.this.mIsGroupListAnimating)
        {
          MediaRouteControllerDialog.access$1202(MediaRouteControllerDialog.this, true);
          return;
        }
        MediaRouteControllerDialog.this.updateLayoutHeightInternal(paramBoolean);
      }
    });
  }
  
  private void updateLayoutHeightInternal(boolean paramBoolean)
  {
    int i = getLayoutHeight(this.mMediaMainControlLayout);
    setLayoutHeight(this.mMediaMainControlLayout, -1);
    updateMediaControlVisibility(canShowPlaybackControlLayout());
    View localView = getWindow().getDecorView();
    localView.measure(View.MeasureSpec.makeMeasureSpec(getWindow().getAttributes().width, 1073741824), 0);
    setLayoutHeight(this.mMediaMainControlLayout, i);
    int j = 0;
    i = j;
    Object localObject;
    if (this.mCustomControlView == null)
    {
      i = j;
      if ((this.mArtView.getDrawable() instanceof BitmapDrawable))
      {
        localObject = ((BitmapDrawable)this.mArtView.getDrawable()).getBitmap();
        i = j;
        if (localObject != null)
        {
          i = getDesiredArtHeight(((Bitmap)localObject).getWidth(), ((Bitmap)localObject).getHeight());
          ImageView localImageView = this.mArtView;
          if (((Bitmap)localObject).getWidth() < ((Bitmap)localObject).getHeight()) {
            break label480;
          }
          localObject = ImageView.ScaleType.FIT_XY;
          localImageView.setScaleType((ImageView.ScaleType)localObject);
        }
      }
    }
    int n = getMainControllerHeight(canShowPlaybackControlLayout());
    int m = this.mGroupMemberRoutes.size();
    label187:
    int k;
    label220:
    int i1;
    label311:
    label333:
    boolean bool;
    if (getGroup() == null)
    {
      j = 0;
      k = j;
      if (m > 0) {
        k = j + this.mVolumeGroupListPaddingTop;
      }
      j = Math.min(k, this.mVolumeGroupListMaxHeight);
      if (!this.mIsGroupExpanded) {
        break label509;
      }
      k = Math.max(i, j) + n;
      localObject = new Rect();
      localView.getWindowVisibleDisplayFrame((Rect)localObject);
      m = this.mDialogAreaLayout.getMeasuredHeight();
      i1 = this.mDefaultControlLayout.getMeasuredHeight();
      m = ((Rect)localObject).height() - (m - i1);
      if ((this.mCustomControlView != null) || (i <= 0) || (k > m)) {
        break label514;
      }
      this.mArtView.setVisibility(0);
      setLayoutHeight(this.mArtView, i);
      if ((!canShowPlaybackControlLayout()) || (k > m)) {
        break label559;
      }
      this.mPlaybackControlLayout.setVisibility(0);
      if (this.mPlaybackControlLayout.getVisibility() != 0) {
        break label571;
      }
      bool = true;
      label346:
      updateMediaControlVisibility(bool);
      if (this.mPlaybackControlLayout.getVisibility() != 0) {
        break label577;
      }
      bool = true;
      label365:
      i1 = getMainControllerHeight(bool);
      n = Math.max(i, j) + i1;
      k = n;
      i = j;
      if (n > m)
      {
        i = j - (n - m);
        k = m;
      }
      this.mMediaMainControlLayout.clearAnimation();
      this.mVolumeGroupList.clearAnimation();
      this.mDefaultControlLayout.clearAnimation();
      if (!paramBoolean) {
        break label583;
      }
      animateLayoutHeight(this.mMediaMainControlLayout, i1);
      animateLayoutHeight(this.mVolumeGroupList, i);
      animateLayoutHeight(this.mDefaultControlLayout, k);
    }
    for (;;)
    {
      setLayoutHeight(this.mExpandableAreaLayout, ((Rect)localObject).height());
      rebuildVolumeGroupList(paramBoolean);
      return;
      label480:
      localObject = ImageView.ScaleType.FIT_CENTER;
      break;
      j = this.mVolumeGroupListItemHeight * getGroup().getRoutes().size();
      break label187;
      label509:
      j = 0;
      break label220;
      label514:
      if (getLayoutHeight(this.mVolumeGroupList) + this.mMediaMainControlLayout.getMeasuredHeight() >= this.mDefaultControlLayout.getMeasuredHeight()) {
        this.mArtView.setVisibility(8);
      }
      i = 0;
      k = j + n;
      break label311;
      label559:
      this.mPlaybackControlLayout.setVisibility(8);
      break label333;
      label571:
      bool = false;
      break label346;
      label577:
      bool = false;
      break label365;
      label583:
      setLayoutHeight(this.mMediaMainControlLayout, i1);
      setLayoutHeight(this.mVolumeGroupList, i);
      setLayoutHeight(this.mDefaultControlLayout, k);
    }
  }
  
  private void updateMediaControlVisibility(boolean paramBoolean)
  {
    int j = 8;
    Object localObject = this.mDividerView;
    if ((this.mVolumeControlLayout.getVisibility() == 0) && (paramBoolean))
    {
      i = 0;
      ((View)localObject).setVisibility(i);
      localObject = this.mMediaMainControlLayout;
      if ((this.mVolumeControlLayout.getVisibility() != 8) || (paramBoolean)) {
        break label68;
      }
    }
    label68:
    for (int i = j;; i = 0)
    {
      ((LinearLayout)localObject).setVisibility(i);
      return;
      i = 8;
      break;
    }
  }
  
  private void updatePlaybackControlLayout()
  {
    Object localObject;
    int i;
    label27:
    CharSequence localCharSequence;
    label37:
    int m;
    label48:
    int j;
    if (canShowPlaybackControlLayout())
    {
      if (this.mDescription != null) {
        break label233;
      }
      localObject = null;
      if (TextUtils.isEmpty((CharSequence)localObject)) {
        break label245;
      }
      i = 1;
      if (this.mDescription != null) {
        break label250;
      }
      localCharSequence = null;
      if (TextUtils.isEmpty(localCharSequence)) {
        break label262;
      }
      m = 1;
      j = 0;
      k = 0;
      if (this.mRoute.getPresentationDisplayId() == -1) {
        break label268;
      }
      this.mTitleView.setText(R.string.mr_controller_casting_screen);
      i = 1;
      label75:
      localObject = this.mTitleView;
      if (i == 0) {
        break label362;
      }
      i = 0;
      label87:
      ((TextView)localObject).setVisibility(i);
      localObject = this.mSubtitleView;
      if (k == 0) {
        break label368;
      }
      i = 0;
      label105:
      ((TextView)localObject).setVisibility(i);
      if (this.mState != null)
      {
        if ((this.mState.getState() != 6) && (this.mState.getState() != 3)) {
          break label374;
        }
        i = 1;
        label143:
        if ((this.mState.getActions() & 0x204) == 0L) {
          break label379;
        }
        j = 1;
        label161:
        if ((this.mState.getActions() & 0x202) == 0L) {
          break label384;
        }
      }
    }
    label233:
    label245:
    label250:
    label262:
    label268:
    label362:
    label368:
    label374:
    label379:
    label384:
    for (int k = 1;; k = 0)
    {
      if ((i == 0) || (k == 0)) {
        break label389;
      }
      this.mPlayPauseButton.setVisibility(0);
      this.mPlayPauseButton.setImageResource(MediaRouterThemeHelper.getThemeResource(this.mContext, R.attr.mediaRoutePauseDrawable));
      this.mPlayPauseButton.setContentDescription(this.mContext.getResources().getText(R.string.mr_controller_pause));
      return;
      localObject = this.mDescription.getTitle();
      break;
      i = 0;
      break label27;
      localCharSequence = this.mDescription.getSubtitle();
      break label37;
      m = 0;
      break label48;
      if ((this.mState == null) || (this.mState.getState() == 0))
      {
        this.mTitleView.setText(R.string.mr_controller_no_media_selected);
        i = 1;
        break label75;
      }
      if ((i == 0) && (m == 0))
      {
        this.mTitleView.setText(R.string.mr_controller_no_info_available);
        i = 1;
        break label75;
      }
      if (i != 0)
      {
        this.mTitleView.setText((CharSequence)localObject);
        j = 1;
      }
      i = j;
      if (m == 0) {
        break label75;
      }
      this.mSubtitleView.setText(localCharSequence);
      k = 1;
      i = j;
      break label75;
      i = 8;
      break label87;
      i = 8;
      break label105;
      i = 0;
      break label143;
      j = 0;
      break label161;
    }
    label389:
    if ((i == 0) && (j != 0))
    {
      this.mPlayPauseButton.setVisibility(0);
      this.mPlayPauseButton.setImageResource(MediaRouterThemeHelper.getThemeResource(this.mContext, R.attr.mediaRoutePlayDrawable));
      this.mPlayPauseButton.setContentDescription(this.mContext.getResources().getText(R.string.mr_controller_play));
      return;
    }
    this.mPlayPauseButton.setVisibility(8);
  }
  
  private void updateVolumeControlLayout()
  {
    int i = 8;
    if (isVolumeControlAvailable(this.mRoute))
    {
      MediaRouteExpandCollapseButton localMediaRouteExpandCollapseButton;
      if (this.mVolumeControlLayout.getVisibility() == 8)
      {
        this.mVolumeControlLayout.setVisibility(0);
        this.mVolumeSlider.setMax(this.mRoute.getVolumeMax());
        this.mVolumeSlider.setProgress(this.mRoute.getVolume());
        localMediaRouteExpandCollapseButton = this.mGroupExpandCollapseButton;
        if (getGroup() != null) {
          break label80;
        }
      }
      for (;;)
      {
        localMediaRouteExpandCollapseButton.setVisibility(i);
        return;
        label80:
        i = 0;
      }
    }
    this.mVolumeControlLayout.setVisibility(8);
  }
  
  private void updateVolumeGroupItemHeight(View paramView)
  {
    setLayoutHeight((LinearLayout)paramView.findViewById(R.id.volume_item_container), this.mVolumeGroupListItemHeight);
    paramView = paramView.findViewById(R.id.mr_volume_item_icon);
    ViewGroup.LayoutParams localLayoutParams = paramView.getLayoutParams();
    localLayoutParams.width = this.mVolumeGroupListItemIconSize;
    localLayoutParams.height = this.mVolumeGroupListItemIconSize;
    paramView.setLayoutParams(localLayoutParams);
  }
  
  private static boolean uriEquals(Uri paramUri1, Uri paramUri2)
  {
    if ((paramUri1 != null) && (paramUri1.equals(paramUri2))) {}
    while ((paramUri1 == null) && (paramUri2 == null)) {
      return true;
    }
    return false;
  }
  
  void clearGroupListAnimation(boolean paramBoolean)
  {
    int j = this.mVolumeGroupList.getFirstVisiblePosition();
    int i = 0;
    if (i < this.mVolumeGroupList.getChildCount())
    {
      View localView = this.mVolumeGroupList.getChildAt(i);
      Object localObject = (MediaRouter.RouteInfo)this.mVolumeGroupAdapter.getItem(j + i);
      if ((paramBoolean) && (this.mGroupMemberRoutesAdded != null) && (this.mGroupMemberRoutesAdded.contains(localObject))) {}
      for (;;)
      {
        i += 1;
        break;
        ((LinearLayout)localView.findViewById(R.id.volume_item_container)).setVisibility(0);
        localObject = new AnimationSet(true);
        AlphaAnimation localAlphaAnimation = new AlphaAnimation(1.0F, 1.0F);
        localAlphaAnimation.setDuration(0L);
        ((AnimationSet)localObject).addAnimation(localAlphaAnimation);
        new TranslateAnimation(0.0F, 0.0F, 0.0F, 0.0F).setDuration(0L);
        ((AnimationSet)localObject).setFillAfter(true);
        ((AnimationSet)localObject).setFillEnabled(true);
        localView.clearAnimation();
        localView.startAnimation((Animation)localObject);
      }
    }
    this.mVolumeGroupList.stopAnimationAll();
    if (!paramBoolean) {
      finishAnimation(false);
    }
  }
  
  public View getMediaControlView()
  {
    return this.mCustomControlView;
  }
  
  public MediaSessionCompat.Token getMediaSession()
  {
    if (this.mMediaController == null) {
      return null;
    }
    return this.mMediaController.getSessionToken();
  }
  
  public MediaRouter.RouteInfo getRoute()
  {
    return this.mRoute;
  }
  
  public boolean isVolumeControlEnabled()
  {
    return this.mVolumeControlEnabled;
  }
  
  public void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    this.mAttachedToWindow = true;
    this.mRouter.addCallback(MediaRouteSelector.EMPTY, this.mCallback, 2);
    setMediaSession(this.mRouter.getMediaSessionToken());
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    getWindow().setBackgroundDrawableResource(17170445);
    setContentView(R.layout.mr_controller_material_dialog_b);
    findViewById(16908315).setVisibility(8);
    Object localObject1 = new ClickListener(null);
    this.mExpandableAreaLayout = ((FrameLayout)findViewById(R.id.mr_expandable_area));
    this.mExpandableAreaLayout.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        MediaRouteControllerDialog.this.dismiss();
      }
    });
    this.mDialogAreaLayout = ((LinearLayout)findViewById(R.id.mr_dialog_area));
    this.mDialogAreaLayout.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView) {}
    });
    int i = MediaRouterThemeHelper.getButtonTextColor(this.mContext);
    this.mDisconnectButton = ((Button)findViewById(16908314));
    this.mDisconnectButton.setText(R.string.mr_controller_disconnect);
    this.mDisconnectButton.setTextColor(i);
    this.mDisconnectButton.setOnClickListener((View.OnClickListener)localObject1);
    this.mStopCastingButton = ((Button)findViewById(16908313));
    this.mStopCastingButton.setText(R.string.mr_controller_stop);
    this.mStopCastingButton.setTextColor(i);
    this.mStopCastingButton.setOnClickListener((View.OnClickListener)localObject1);
    this.mRouteNameTextView = ((TextView)findViewById(R.id.mr_name));
    this.mCloseButton = ((ImageButton)findViewById(R.id.mr_close));
    this.mCloseButton.setOnClickListener((View.OnClickListener)localObject1);
    this.mCustomControlLayout = ((FrameLayout)findViewById(R.id.mr_custom_control));
    this.mDefaultControlLayout = ((FrameLayout)findViewById(R.id.mr_default_control));
    Object localObject2 = new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        if (MediaRouteControllerDialog.this.mMediaController != null)
        {
          paramAnonymousView = MediaRouteControllerDialog.this.mMediaController.getSessionActivity();
          if (paramAnonymousView == null) {}
        }
        try
        {
          paramAnonymousView.send();
          MediaRouteControllerDialog.this.dismiss();
          return;
        }
        catch (PendingIntent.CanceledException localCanceledException)
        {
          Log.e("MediaRouteCtrlDialog", paramAnonymousView + " was not sent, it had been canceled.");
        }
      }
    };
    this.mArtView = ((ImageView)findViewById(R.id.mr_art));
    this.mArtView.setOnClickListener((View.OnClickListener)localObject2);
    findViewById(R.id.mr_control_title_container).setOnClickListener((View.OnClickListener)localObject2);
    this.mMediaMainControlLayout = ((LinearLayout)findViewById(R.id.mr_media_main_control));
    this.mDividerView = findViewById(R.id.mr_control_divider);
    this.mPlaybackControlLayout = ((RelativeLayout)findViewById(R.id.mr_playback_control));
    this.mTitleView = ((TextView)findViewById(R.id.mr_control_title));
    this.mSubtitleView = ((TextView)findViewById(R.id.mr_control_subtitle));
    this.mPlayPauseButton = ((ImageButton)findViewById(R.id.mr_control_play_pause));
    this.mPlayPauseButton.setOnClickListener((View.OnClickListener)localObject1);
    this.mVolumeControlLayout = ((LinearLayout)findViewById(R.id.mr_volume_control));
    this.mVolumeControlLayout.setVisibility(8);
    this.mVolumeSlider = ((SeekBar)findViewById(R.id.mr_volume_slider));
    this.mVolumeSlider.setTag(this.mRoute);
    this.mVolumeChangeListener = new VolumeChangeListener(null);
    this.mVolumeSlider.setOnSeekBarChangeListener(this.mVolumeChangeListener);
    this.mVolumeGroupList = ((OverlayListView)findViewById(R.id.mr_volume_group_list));
    this.mGroupMemberRoutes = new ArrayList();
    this.mVolumeGroupAdapter = new VolumeGroupAdapter(this.mContext, this.mGroupMemberRoutes);
    this.mVolumeGroupList.setAdapter(this.mVolumeGroupAdapter);
    this.mGroupMemberRoutesAnimatingWithBitmap = new HashSet();
    localObject1 = this.mContext;
    localObject2 = this.mMediaMainControlLayout;
    OverlayListView localOverlayListView = this.mVolumeGroupList;
    if (getGroup() != null) {}
    for (boolean bool = true;; bool = false)
    {
      MediaRouterThemeHelper.setMediaControlsBackgroundColor((Context)localObject1, (View)localObject2, localOverlayListView, bool);
      MediaRouterThemeHelper.setVolumeSliderColor(this.mContext, (MediaRouteVolumeSlider)this.mVolumeSlider, this.mMediaMainControlLayout);
      this.mVolumeSliderMap = new HashMap();
      this.mVolumeSliderMap.put(this.mRoute, this.mVolumeSlider);
      this.mGroupExpandCollapseButton = ((MediaRouteExpandCollapseButton)findViewById(R.id.mr_group_expand_collapse));
      this.mGroupExpandCollapseButton.setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          paramAnonymousView = MediaRouteControllerDialog.this;
          if (!MediaRouteControllerDialog.this.mIsGroupExpanded) {}
          for (boolean bool = true;; bool = false)
          {
            MediaRouteControllerDialog.access$602(paramAnonymousView, bool);
            if (MediaRouteControllerDialog.this.mIsGroupExpanded) {
              MediaRouteControllerDialog.this.mVolumeGroupList.setVisibility(0);
            }
            MediaRouteControllerDialog.this.loadInterpolator();
            MediaRouteControllerDialog.this.updateLayoutHeight(true);
            return;
          }
        }
      });
      loadInterpolator();
      this.mGroupListAnimationDurationMs = this.mContext.getResources().getInteger(R.integer.mr_controller_volume_group_list_animation_duration_ms);
      this.mGroupListFadeInDurationMs = this.mContext.getResources().getInteger(R.integer.mr_controller_volume_group_list_fade_in_duration_ms);
      this.mGroupListFadeOutDurationMs = this.mContext.getResources().getInteger(R.integer.mr_controller_volume_group_list_fade_out_duration_ms);
      this.mCustomControlView = onCreateMediaControlView(paramBundle);
      if (this.mCustomControlView != null)
      {
        this.mCustomControlLayout.addView(this.mCustomControlView);
        this.mCustomControlLayout.setVisibility(0);
      }
      this.mCreated = true;
      updateLayout();
      return;
    }
  }
  
  public View onCreateMediaControlView(Bundle paramBundle)
  {
    return null;
  }
  
  public void onDetachedFromWindow()
  {
    this.mRouter.removeCallback(this.mCallback);
    setMediaSession(null);
    this.mAttachedToWindow = false;
    super.onDetachedFromWindow();
  }
  
  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramInt == 25) || (paramInt == 24))
    {
      paramKeyEvent = this.mRoute;
      if (paramInt == 25) {}
      for (paramInt = -1;; paramInt = 1)
      {
        paramKeyEvent.requestUpdateVolume(paramInt);
        return true;
      }
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }
  
  public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramInt == 25) || (paramInt == 24)) {
      return true;
    }
    return super.onKeyUp(paramInt, paramKeyEvent);
  }
  
  public void setVolumeControlEnabled(boolean paramBoolean)
  {
    if (this.mVolumeControlEnabled != paramBoolean)
    {
      this.mVolumeControlEnabled = paramBoolean;
      if (this.mCreated)
      {
        updateVolumeControlLayout();
        updateLayoutHeight(false);
      }
    }
  }
  
  void updateLayout()
  {
    int i = MediaRouteDialogHelper.getDialogWidth(this.mContext);
    getWindow().setLayout(i, -2);
    Object localObject = getWindow().getDecorView();
    this.mDialogContentWidth = (i - ((View)localObject).getPaddingLeft() - ((View)localObject).getPaddingRight());
    localObject = this.mContext.getResources();
    this.mVolumeGroupListItemIconSize = ((Resources)localObject).getDimensionPixelSize(R.dimen.mr_controller_volume_group_list_item_icon_size);
    this.mVolumeGroupListItemHeight = ((Resources)localObject).getDimensionPixelSize(R.dimen.mr_controller_volume_group_list_item_height);
    this.mVolumeGroupListMaxHeight = ((Resources)localObject).getDimensionPixelSize(R.dimen.mr_controller_volume_group_list_max_height);
    this.mArtIconBitmap = null;
    this.mArtIconUri = null;
    update(false);
  }
  
  private final class ClickListener
    implements View.OnClickListener
  {
    private ClickListener() {}
    
    public void onClick(View paramView)
    {
      int i = 1;
      int j = paramView.getId();
      if ((j == 16908313) || (j == 16908314))
      {
        if (MediaRouteControllerDialog.this.mRoute.isSelected())
        {
          paramView = MediaRouteControllerDialog.this.mRouter;
          if (j == 16908313) {
            i = 2;
          }
          paramView.unselect(i);
        }
        MediaRouteControllerDialog.this.dismiss();
      }
      label104:
      label121:
      label222:
      label236:
      label238:
      do
      {
        do
        {
          return;
          if (j != R.id.mr_control_play_pause) {
            break;
          }
        } while ((MediaRouteControllerDialog.this.mMediaController == null) || (MediaRouteControllerDialog.this.mState == null));
        if (MediaRouteControllerDialog.this.mState.getState() == 3)
        {
          i = 1;
          if (i == 0) {
            break label222;
          }
          MediaRouteControllerDialog.this.mMediaController.getTransportControls().pause();
          if ((MediaRouteControllerDialog.this.mAccessibilityManager == null) || (!MediaRouteControllerDialog.this.mAccessibilityManager.isEnabled())) {
            break label236;
          }
          paramView = AccessibilityEvent.obtain(16384);
          paramView.setPackageName(MediaRouteControllerDialog.this.mContext.getPackageName());
          paramView.setClassName(getClass().getName());
          if (i == 0) {
            break label238;
          }
        }
        for (i = R.string.mr_controller_pause;; i = R.string.mr_controller_play)
        {
          paramView.getText().add(MediaRouteControllerDialog.this.mContext.getString(i));
          MediaRouteControllerDialog.this.mAccessibilityManager.sendAccessibilityEvent(paramView);
          return;
          i = 0;
          break label104;
          MediaRouteControllerDialog.this.mMediaController.getTransportControls().play();
          break label121;
          break;
        }
      } while (j != R.id.mr_close);
      MediaRouteControllerDialog.this.dismiss();
    }
  }
  
  private class FetchArtTask
    extends AsyncTask<Void, Void, Bitmap>
  {
    int mBackgroundColor;
    final Bitmap mIconBitmap;
    final Uri mIconUri;
    
    FetchArtTask()
    {
      Bitmap localBitmap;
      if (MediaRouteControllerDialog.this.mDescription == null)
      {
        localBitmap = null;
        this.mIconBitmap = localBitmap;
        if (MediaRouteControllerDialog.this.mDescription != null) {
          break label51;
        }
      }
      label51:
      for (this$1 = (MediaRouteControllerDialog)localObject;; this$1 = MediaRouteControllerDialog.this.mDescription.getIconUri())
      {
        this.mIconUri = MediaRouteControllerDialog.this;
        return;
        localBitmap = MediaRouteControllerDialog.this.mDescription.getIconBitmap();
        break;
      }
    }
    
    private boolean isIconChanged()
    {
      if (this.mIconBitmap != MediaRouteControllerDialog.this.mArtIconBitmap) {}
      while ((this.mIconBitmap == null) && (!MediaRouteControllerDialog.uriEquals(this.mIconUri, MediaRouteControllerDialog.this.mArtIconUri))) {
        return true;
      }
      return false;
    }
    
    private InputStream openInputStreamByScheme(Uri paramUri)
      throws IOException
    {
      String str = paramUri.getScheme().toLowerCase();
      if (("android.resource".equals(str)) || ("content".equals(str)) || ("file".equals(str))) {}
      for (paramUri = MediaRouteControllerDialog.this.mContext.getContentResolver().openInputStream(paramUri); paramUri == null; paramUri = paramUri.getInputStream())
      {
        return null;
        paramUri = new URL(paramUri.toString()).openConnection();
        paramUri.setConnectTimeout(MediaRouteControllerDialog.CONNECTION_TIMEOUT_MILLIS);
        paramUri.setReadTimeout(MediaRouteControllerDialog.CONNECTION_TIMEOUT_MILLIS);
      }
      return new BufferedInputStream(paramUri);
    }
    
    /* Error */
    protected Bitmap doInBackground(Void... paramVarArgs)
    {
      // Byte code:
      //   0: iconst_0
      //   1: istore_2
      //   2: aconst_null
      //   3: astore 8
      //   5: aload_0
      //   6: getfield 29	android/support/v7/app/MediaRouteControllerDialog$FetchArtTask:mIconBitmap	Landroid/graphics/Bitmap;
      //   9: ifnull +64 -> 73
      //   12: aload_0
      //   13: getfield 29	android/support/v7/app/MediaRouteControllerDialog$FetchArtTask:mIconBitmap	Landroid/graphics/Bitmap;
      //   16: astore 5
      //   18: aload 5
      //   20: ifnull +50 -> 70
      //   23: aload 5
      //   25: invokevirtual 138	android/graphics/Bitmap:getWidth	()I
      //   28: aload 5
      //   30: invokevirtual 141	android/graphics/Bitmap:getHeight	()I
      //   33: if_icmpge +37 -> 70
      //   36: new 143	android/support/v7/graphics/Palette$Builder
      //   39: dup
      //   40: aload 5
      //   42: invokespecial 146	android/support/v7/graphics/Palette$Builder:<init>	(Landroid/graphics/Bitmap;)V
      //   45: iconst_1
      //   46: invokevirtual 150	android/support/v7/graphics/Palette$Builder:maximumColorCount	(I)Landroid/support/v7/graphics/Palette$Builder;
      //   49: invokevirtual 154	android/support/v7/graphics/Palette$Builder:generate	()Landroid/support/v7/graphics/Palette;
      //   52: astore_1
      //   53: aload_1
      //   54: invokevirtual 160	android/support/v7/graphics/Palette:getSwatches	()Ljava/util/List;
      //   57: invokeinterface 165 1 0
      //   62: ifeq +489 -> 551
      //   65: aload_0
      //   66: iload_2
      //   67: putfield 167	android/support/v7/app/MediaRouteControllerDialog$FetchArtTask:mBackgroundColor	I
      //   70: aload 5
      //   72: areturn
      //   73: aload 8
      //   75: astore 5
      //   77: aload_0
      //   78: getfield 31	android/support/v7/app/MediaRouteControllerDialog$FetchArtTask:mIconUri	Landroid/net/Uri;
      //   81: ifnull -63 -> 18
      //   84: aconst_null
      //   85: astore 5
      //   87: aconst_null
      //   88: astore_1
      //   89: aload_0
      //   90: aload_0
      //   91: getfield 31	android/support/v7/app/MediaRouteControllerDialog$FetchArtTask:mIconUri	Landroid/net/Uri;
      //   94: invokespecial 169	android/support/v7/app/MediaRouteControllerDialog$FetchArtTask:openInputStreamByScheme	(Landroid/net/Uri;)Ljava/io/InputStream;
      //   97: astore 6
      //   99: aload 6
      //   101: ifnonnull +50 -> 151
      //   104: aload 6
      //   106: astore_1
      //   107: aload 6
      //   109: astore 5
      //   111: ldc -85
      //   113: new 173	java/lang/StringBuilder
      //   116: dup
      //   117: invokespecial 174	java/lang/StringBuilder:<init>	()V
      //   120: ldc -80
      //   122: invokevirtual 180	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   125: aload_0
      //   126: getfield 31	android/support/v7/app/MediaRouteControllerDialog$FetchArtTask:mIconUri	Landroid/net/Uri;
      //   129: invokevirtual 183	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   132: invokevirtual 184	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   135: invokestatic 190	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
      //   138: pop
      //   139: aload 6
      //   141: ifnull +8 -> 149
      //   144: aload 6
      //   146: invokevirtual 195	java/io/InputStream:close	()V
      //   149: aconst_null
      //   150: areturn
      //   151: aload 6
      //   153: astore_1
      //   154: aload 6
      //   156: astore 5
      //   158: new 197	android/graphics/BitmapFactory$Options
      //   161: dup
      //   162: invokespecial 198	android/graphics/BitmapFactory$Options:<init>	()V
      //   165: astore 9
      //   167: aload 6
      //   169: astore_1
      //   170: aload 6
      //   172: astore 5
      //   174: aload 9
      //   176: iconst_1
      //   177: putfield 202	android/graphics/BitmapFactory$Options:inJustDecodeBounds	Z
      //   180: aload 6
      //   182: astore_1
      //   183: aload 6
      //   185: astore 5
      //   187: aload 6
      //   189: aconst_null
      //   190: aload 9
      //   192: invokestatic 208	android/graphics/BitmapFactory:decodeStream	(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
      //   195: pop
      //   196: aload 6
      //   198: astore_1
      //   199: aload 6
      //   201: astore 5
      //   203: aload 9
      //   205: getfield 211	android/graphics/BitmapFactory$Options:outWidth	I
      //   208: ifeq +20 -> 228
      //   211: aload 6
      //   213: astore_1
      //   214: aload 6
      //   216: astore 5
      //   218: aload 9
      //   220: getfield 214	android/graphics/BitmapFactory$Options:outHeight	I
      //   223: istore_3
      //   224: iload_3
      //   225: ifne +15 -> 240
      //   228: aload 6
      //   230: ifnull +8 -> 238
      //   233: aload 6
      //   235: invokevirtual 195	java/io/InputStream:close	()V
      //   238: aconst_null
      //   239: areturn
      //   240: aload 6
      //   242: astore 5
      //   244: aload 6
      //   246: invokevirtual 217	java/io/InputStream:reset	()V
      //   249: aload 6
      //   251: astore_1
      //   252: aload 6
      //   254: astore 5
      //   256: aload 9
      //   258: iconst_0
      //   259: putfield 202	android/graphics/BitmapFactory$Options:inJustDecodeBounds	Z
      //   262: aload 6
      //   264: astore_1
      //   265: aload 6
      //   267: astore 5
      //   269: aload_0
      //   270: getfield 20	android/support/v7/app/MediaRouteControllerDialog$FetchArtTask:this$0	Landroid/support/v7/app/MediaRouteControllerDialog;
      //   273: aload 9
      //   275: getfield 211	android/graphics/BitmapFactory$Options:outWidth	I
      //   278: aload 9
      //   280: getfield 214	android/graphics/BitmapFactory$Options:outHeight	I
      //   283: invokestatic 221	android/support/v7/app/MediaRouteControllerDialog:access$3800	(Landroid/support/v7/app/MediaRouteControllerDialog;II)I
      //   286: istore_3
      //   287: aload 6
      //   289: astore_1
      //   290: aload 6
      //   292: astore 5
      //   294: aload 9
      //   296: iconst_1
      //   297: aload 9
      //   299: getfield 214	android/graphics/BitmapFactory$Options:outHeight	I
      //   302: iload_3
      //   303: idiv
      //   304: invokestatic 227	java/lang/Integer:highestOneBit	(I)I
      //   307: invokestatic 233	java/lang/Math:max	(II)I
      //   310: putfield 236	android/graphics/BitmapFactory$Options:inSampleSize	I
      //   313: aload 6
      //   315: astore_1
      //   316: aload 6
      //   318: astore 5
      //   320: aload_0
      //   321: invokevirtual 239	android/support/v7/app/MediaRouteControllerDialog$FetchArtTask:isCancelled	()Z
      //   324: istore 4
      //   326: iload 4
      //   328: ifeq +101 -> 429
      //   331: aload 6
      //   333: ifnull +8 -> 341
      //   336: aload 6
      //   338: invokevirtual 195	java/io/InputStream:close	()V
      //   341: aconst_null
      //   342: areturn
      //   343: astore_1
      //   344: aload 6
      //   346: astore_1
      //   347: aload 6
      //   349: astore 5
      //   351: aload 6
      //   353: invokevirtual 195	java/io/InputStream:close	()V
      //   356: aload 6
      //   358: astore_1
      //   359: aload 6
      //   361: astore 5
      //   363: aload_0
      //   364: aload_0
      //   365: getfield 31	android/support/v7/app/MediaRouteControllerDialog$FetchArtTask:mIconUri	Landroid/net/Uri;
      //   368: invokespecial 169	android/support/v7/app/MediaRouteControllerDialog$FetchArtTask:openInputStreamByScheme	(Landroid/net/Uri;)Ljava/io/InputStream;
      //   371: astore 7
      //   373: aload 7
      //   375: astore 6
      //   377: aload 7
      //   379: ifnonnull -130 -> 249
      //   382: aload 7
      //   384: astore_1
      //   385: aload 7
      //   387: astore 5
      //   389: ldc -85
      //   391: new 173	java/lang/StringBuilder
      //   394: dup
      //   395: invokespecial 174	java/lang/StringBuilder:<init>	()V
      //   398: ldc -80
      //   400: invokevirtual 180	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   403: aload_0
      //   404: getfield 31	android/support/v7/app/MediaRouteControllerDialog$FetchArtTask:mIconUri	Landroid/net/Uri;
      //   407: invokevirtual 183	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   410: invokevirtual 184	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   413: invokestatic 190	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
      //   416: pop
      //   417: aload 7
      //   419: ifnull +8 -> 427
      //   422: aload 7
      //   424: invokevirtual 195	java/io/InputStream:close	()V
      //   427: aconst_null
      //   428: areturn
      //   429: aload 6
      //   431: astore_1
      //   432: aload 6
      //   434: astore 5
      //   436: aload 6
      //   438: aconst_null
      //   439: aload 9
      //   441: invokestatic 208	android/graphics/BitmapFactory:decodeStream	(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
      //   444: astore 7
      //   446: aload 7
      //   448: astore_1
      //   449: aload_1
      //   450: astore 5
      //   452: aload 6
      //   454: ifnull -436 -> 18
      //   457: aload 6
      //   459: invokevirtual 195	java/io/InputStream:close	()V
      //   462: aload_1
      //   463: astore 5
      //   465: goto -447 -> 18
      //   468: astore 5
      //   470: aload_1
      //   471: astore 5
      //   473: goto -455 -> 18
      //   476: astore 6
      //   478: aload_1
      //   479: astore 5
      //   481: ldc -85
      //   483: new 173	java/lang/StringBuilder
      //   486: dup
      //   487: invokespecial 174	java/lang/StringBuilder:<init>	()V
      //   490: ldc -80
      //   492: invokevirtual 180	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   495: aload_0
      //   496: getfield 31	android/support/v7/app/MediaRouteControllerDialog$FetchArtTask:mIconUri	Landroid/net/Uri;
      //   499: invokevirtual 183	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   502: invokevirtual 184	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   505: aload 6
      //   507: invokestatic 242	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
      //   510: pop
      //   511: aload 8
      //   513: astore 5
      //   515: aload_1
      //   516: ifnull -498 -> 18
      //   519: aload_1
      //   520: invokevirtual 195	java/io/InputStream:close	()V
      //   523: aload 8
      //   525: astore 5
      //   527: goto -509 -> 18
      //   530: astore_1
      //   531: aload 8
      //   533: astore 5
      //   535: goto -517 -> 18
      //   538: astore_1
      //   539: aload 5
      //   541: ifnull +8 -> 549
      //   544: aload 5
      //   546: invokevirtual 195	java/io/InputStream:close	()V
      //   549: aload_1
      //   550: athrow
      //   551: aload_1
      //   552: invokevirtual 160	android/support/v7/graphics/Palette:getSwatches	()Ljava/util/List;
      //   555: iconst_0
      //   556: invokeinterface 246 2 0
      //   561: checkcast 248	android/support/v7/graphics/Palette$Swatch
      //   564: invokevirtual 251	android/support/v7/graphics/Palette$Swatch:getRgb	()I
      //   567: istore_2
      //   568: goto -503 -> 65
      //   571: astore_1
      //   572: goto -423 -> 149
      //   575: astore_1
      //   576: goto -338 -> 238
      //   579: astore_1
      //   580: goto -153 -> 427
      //   583: astore_1
      //   584: goto -243 -> 341
      //   587: astore 5
      //   589: goto -40 -> 549
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	592	0	this	FetchArtTask
      //   0	592	1	paramVarArgs	Void[]
      //   1	567	2	i	int
      //   223	81	3	j	int
      //   324	3	4	bool	boolean
      //   16	448	5	localObject1	Object
      //   468	1	5	localIOException1	IOException
      //   471	74	5	localObject2	Object
      //   587	1	5	localIOException2	IOException
      //   97	361	6	localObject3	Object
      //   476	30	6	localIOException3	IOException
      //   371	76	7	localObject4	Object
      //   3	529	8	localObject5	Object
      //   165	275	9	localOptions	android.graphics.BitmapFactory.Options
      // Exception table:
      //   from	to	target	type
      //   244	249	343	java/io/IOException
      //   457	462	468	java/io/IOException
      //   89	99	476	java/io/IOException
      //   111	139	476	java/io/IOException
      //   158	167	476	java/io/IOException
      //   174	180	476	java/io/IOException
      //   187	196	476	java/io/IOException
      //   203	211	476	java/io/IOException
      //   218	224	476	java/io/IOException
      //   256	262	476	java/io/IOException
      //   269	287	476	java/io/IOException
      //   294	313	476	java/io/IOException
      //   320	326	476	java/io/IOException
      //   351	356	476	java/io/IOException
      //   363	373	476	java/io/IOException
      //   389	417	476	java/io/IOException
      //   436	446	476	java/io/IOException
      //   519	523	530	java/io/IOException
      //   89	99	538	finally
      //   111	139	538	finally
      //   158	167	538	finally
      //   174	180	538	finally
      //   187	196	538	finally
      //   203	211	538	finally
      //   218	224	538	finally
      //   244	249	538	finally
      //   256	262	538	finally
      //   269	287	538	finally
      //   294	313	538	finally
      //   320	326	538	finally
      //   351	356	538	finally
      //   363	373	538	finally
      //   389	417	538	finally
      //   436	446	538	finally
      //   481	511	538	finally
      //   144	149	571	java/io/IOException
      //   233	238	575	java/io/IOException
      //   422	427	579	java/io/IOException
      //   336	341	583	java/io/IOException
      //   544	549	587	java/io/IOException
    }
    
    protected void onCancelled()
    {
      MediaRouteControllerDialog.access$3902(MediaRouteControllerDialog.this, null);
    }
    
    protected void onPostExecute(Bitmap paramBitmap)
    {
      MediaRouteControllerDialog.access$3902(MediaRouteControllerDialog.this, null);
      if ((MediaRouteControllerDialog.this.mArtIconBitmap != this.mIconBitmap) || (MediaRouteControllerDialog.this.mArtIconUri != this.mIconUri))
      {
        MediaRouteControllerDialog.access$4002(MediaRouteControllerDialog.this, this.mIconBitmap);
        MediaRouteControllerDialog.access$4102(MediaRouteControllerDialog.this, this.mIconUri);
        MediaRouteControllerDialog.this.mArtView.setImageBitmap(paramBitmap);
        MediaRouteControllerDialog.this.mArtView.setBackgroundColor(this.mBackgroundColor);
        MediaRouteControllerDialog.this.updateLayoutHeight(true);
      }
    }
    
    protected void onPreExecute()
    {
      if (!isIconChanged()) {
        cancel(true);
      }
    }
  }
  
  private final class MediaControllerCallback
    extends MediaControllerCompat.Callback
  {
    private MediaControllerCallback() {}
    
    public void onMetadataChanged(MediaMetadataCompat paramMediaMetadataCompat)
    {
      MediaRouteControllerDialog localMediaRouteControllerDialog = MediaRouteControllerDialog.this;
      if (paramMediaMetadataCompat == null) {}
      for (paramMediaMetadataCompat = null;; paramMediaMetadataCompat = paramMediaMetadataCompat.getDescription())
      {
        MediaRouteControllerDialog.access$2802(localMediaRouteControllerDialog, paramMediaMetadataCompat);
        MediaRouteControllerDialog.this.update(false);
        return;
      }
    }
    
    public void onPlaybackStateChanged(PlaybackStateCompat paramPlaybackStateCompat)
    {
      MediaRouteControllerDialog.access$2702(MediaRouteControllerDialog.this, paramPlaybackStateCompat);
      MediaRouteControllerDialog.this.update(false);
    }
    
    public void onSessionDestroyed()
    {
      if (MediaRouteControllerDialog.this.mMediaController != null)
      {
        MediaRouteControllerDialog.this.mMediaController.unregisterCallback(MediaRouteControllerDialog.this.mControllerCallback);
        MediaRouteControllerDialog.access$402(MediaRouteControllerDialog.this, null);
      }
    }
  }
  
  private final class MediaRouterCallback
    extends MediaRouter.Callback
  {
    private MediaRouterCallback() {}
    
    public void onRouteChanged(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo)
    {
      MediaRouteControllerDialog.this.update(true);
    }
    
    public void onRouteUnselected(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo)
    {
      MediaRouteControllerDialog.this.update(false);
    }
    
    public void onRouteVolumeChanged(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo)
    {
      paramMediaRouter = (SeekBar)MediaRouteControllerDialog.this.mVolumeSliderMap.get(paramRouteInfo);
      int i = paramRouteInfo.getVolume();
      if (MediaRouteControllerDialog.DEBUG) {
        Log.d("MediaRouteCtrlDialog", "onRouteVolumeChanged(), route.getVolume:" + i);
      }
      if ((paramMediaRouter != null) && (MediaRouteControllerDialog.this.mRouteInVolumeSliderTouched != paramRouteInfo)) {
        paramMediaRouter.setProgress(i);
      }
    }
  }
  
  private class VolumeChangeListener
    implements SeekBar.OnSeekBarChangeListener
  {
    private final Runnable mStopTrackingTouch = new Runnable()
    {
      public void run()
      {
        if (MediaRouteControllerDialog.this.mRouteInVolumeSliderTouched != null) {
          MediaRouteControllerDialog.access$2502(MediaRouteControllerDialog.this, null);
        }
      }
    };
    
    private VolumeChangeListener() {}
    
    public void onProgressChanged(SeekBar paramSeekBar, int paramInt, boolean paramBoolean)
    {
      if (paramBoolean)
      {
        paramSeekBar = (MediaRouter.RouteInfo)paramSeekBar.getTag();
        if (MediaRouteControllerDialog.DEBUG) {
          Log.d("MediaRouteCtrlDialog", "onProgressChanged(): calling MediaRouter.RouteInfo.requestSetVolume(" + paramInt + ")");
        }
        paramSeekBar.requestSetVolume(paramInt);
      }
    }
    
    public void onStartTrackingTouch(SeekBar paramSeekBar)
    {
      if (MediaRouteControllerDialog.this.mRouteInVolumeSliderTouched != null) {
        MediaRouteControllerDialog.this.mVolumeSlider.removeCallbacks(this.mStopTrackingTouch);
      }
      MediaRouteControllerDialog.access$2502(MediaRouteControllerDialog.this, (MediaRouter.RouteInfo)paramSeekBar.getTag());
    }
    
    public void onStopTrackingTouch(SeekBar paramSeekBar)
    {
      MediaRouteControllerDialog.this.mVolumeSlider.postDelayed(this.mStopTrackingTouch, 500L);
    }
  }
  
  private class VolumeGroupAdapter
    extends ArrayAdapter<MediaRouter.RouteInfo>
  {
    final float mDisabledAlpha;
    
    public VolumeGroupAdapter(List<MediaRouter.RouteInfo> paramList)
    {
      super(0, localList);
      this.mDisabledAlpha = MediaRouterThemeHelper.getDisabledAlpha(paramList);
    }
    
    public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      Object localObject;
      boolean bool1;
      if (paramView == null)
      {
        paramView = LayoutInflater.from(MediaRouteControllerDialog.this.mContext).inflate(R.layout.mr_controller_volume_item, paramViewGroup, false);
        paramViewGroup = (MediaRouter.RouteInfo)getItem(paramInt);
        if (paramViewGroup != null)
        {
          boolean bool2 = paramViewGroup.isEnabled();
          localObject = (TextView)paramView.findViewById(R.id.mr_name);
          ((TextView)localObject).setEnabled(bool2);
          ((TextView)localObject).setText(paramViewGroup.getName());
          localObject = (MediaRouteVolumeSlider)paramView.findViewById(R.id.mr_volume_slider);
          MediaRouterThemeHelper.setVolumeSliderColor(MediaRouteControllerDialog.this.mContext, (MediaRouteVolumeSlider)localObject, MediaRouteControllerDialog.this.mVolumeGroupList);
          ((MediaRouteVolumeSlider)localObject).setTag(paramViewGroup);
          MediaRouteControllerDialog.this.mVolumeSliderMap.put(paramViewGroup, localObject);
          if (bool2) {
            break label327;
          }
          bool1 = true;
          label131:
          ((MediaRouteVolumeSlider)localObject).setHideThumb(bool1);
          ((MediaRouteVolumeSlider)localObject).setEnabled(bool2);
          if (bool2)
          {
            if (!MediaRouteControllerDialog.this.isVolumeControlAvailable(paramViewGroup)) {
              break label333;
            }
            ((MediaRouteVolumeSlider)localObject).setMax(paramViewGroup.getVolumeMax());
            ((MediaRouteVolumeSlider)localObject).setProgress(paramViewGroup.getVolume());
            ((MediaRouteVolumeSlider)localObject).setOnSeekBarChangeListener(MediaRouteControllerDialog.this.mVolumeChangeListener);
          }
          label191:
          localObject = (ImageView)paramView.findViewById(R.id.mr_volume_item_icon);
          if (!bool2) {
            break label356;
          }
          paramInt = 255;
          label212:
          ((ImageView)localObject).setAlpha(paramInt);
          localObject = (LinearLayout)paramView.findViewById(R.id.volume_item_container);
          if (!MediaRouteControllerDialog.this.mGroupMemberRoutesAnimatingWithBitmap.contains(paramViewGroup)) {
            break label368;
          }
        }
      }
      label327:
      label333:
      label356:
      label368:
      for (paramInt = 4;; paramInt = 0)
      {
        ((LinearLayout)localObject).setVisibility(paramInt);
        if ((MediaRouteControllerDialog.this.mGroupMemberRoutesAdded != null) && (MediaRouteControllerDialog.this.mGroupMemberRoutesAdded.contains(paramViewGroup)))
        {
          paramViewGroup = new AlphaAnimation(0.0F, 0.0F);
          paramViewGroup.setDuration(0L);
          paramViewGroup.setFillEnabled(true);
          paramViewGroup.setFillAfter(true);
          paramView.clearAnimation();
          paramView.startAnimation(paramViewGroup);
        }
        return paramView;
        MediaRouteControllerDialog.this.updateVolumeGroupItemHeight(paramView);
        break;
        bool1 = false;
        break label131;
        ((MediaRouteVolumeSlider)localObject).setMax(100);
        ((MediaRouteVolumeSlider)localObject).setProgress(100);
        ((MediaRouteVolumeSlider)localObject).setEnabled(false);
        break label191;
        paramInt = (int)(255.0F * this.mDisabledAlpha);
        break label212;
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/android/support/v7/app/MediaRouteControllerDialog.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */