package android.support.v7.cardview;

public final class R
{
  public static final class attr
  {
    public static final int cardBackgroundColor = 2130772159;
    public static final int cardCornerRadius = 2130772160;
    public static final int cardElevation = 2130772161;
    public static final int cardMaxElevation = 2130772162;
    public static final int cardPreventCornerOverlap = 2130772164;
    public static final int cardUseCompatPadding = 2130772163;
    public static final int contentPadding = 2130772165;
    public static final int contentPaddingBottom = 2130772169;
    public static final int contentPaddingLeft = 2130772166;
    public static final int contentPaddingRight = 2130772167;
    public static final int contentPaddingTop = 2130772168;
  }
  
  public static final class color
  {
    public static final int cardview_dark_background = 2131558423;
    public static final int cardview_light_background = 2131558424;
    public static final int cardview_shadow_end_color = 2131558425;
    public static final int cardview_shadow_start_color = 2131558426;
  }
  
  public static final class dimen
  {
    public static final int cardview_compat_inset_shadow = 2131296351;
    public static final int cardview_default_elevation = 2131296352;
    public static final int cardview_default_radius = 2131296353;
  }
  
  public static final class style
  {
    public static final int Base_CardView = 2131361961;
    public static final int CardView = 2131361946;
    public static final int CardView_Dark = 2131362008;
    public static final int CardView_Light = 2131362009;
  }
  
  public static final class styleable
  {
    public static final int[] CardView = { 16843071, 16843072, 2130772159, 2130772160, 2130772161, 2130772162, 2130772163, 2130772164, 2130772165, 2130772166, 2130772167, 2130772168, 2130772169 };
    public static final int CardView_android_minHeight = 1;
    public static final int CardView_android_minWidth = 0;
    public static final int CardView_cardBackgroundColor = 2;
    public static final int CardView_cardCornerRadius = 3;
    public static final int CardView_cardElevation = 4;
    public static final int CardView_cardMaxElevation = 5;
    public static final int CardView_cardPreventCornerOverlap = 7;
    public static final int CardView_cardUseCompatPadding = 6;
    public static final int CardView_contentPadding = 8;
    public static final int CardView_contentPaddingBottom = 12;
    public static final int CardView_contentPaddingLeft = 9;
    public static final int CardView_contentPaddingRight = 10;
    public static final int CardView_contentPaddingTop = 11;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/android/support/v7/cardview/R.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */