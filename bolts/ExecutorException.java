package bolts;

public class ExecutorException
  extends RuntimeException
{
  public ExecutorException(Exception paramException)
  {
    super("An exception was thrown by an Executor", paramException);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/bolts/ExecutorException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */