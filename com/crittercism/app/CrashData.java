package com.crittercism.app;

import java.util.Date;

public class CrashData
{
  private String a;
  private String b;
  private Date c;
  
  public CrashData(String paramString1, String paramString2, Date paramDate)
  {
    this.a = paramString1;
    this.b = paramString2;
    this.c = paramDate;
  }
  
  public CrashData copy()
  {
    return new CrashData(this.a, this.b, this.c);
  }
  
  public String getName()
  {
    return this.a;
  }
  
  public String getReason()
  {
    return this.b;
  }
  
  public Date getTimeOccurred()
  {
    return this.c;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/app/CrashData.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */