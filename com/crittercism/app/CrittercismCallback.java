package com.crittercism.app;

public abstract interface CrittercismCallback<T>
{
  public abstract void onDataReceived(T paramT);
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/app/CrittercismCallback.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */