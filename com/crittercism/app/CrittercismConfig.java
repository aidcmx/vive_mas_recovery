package com.crittercism.app;

import android.os.Build.VERSION;
import com.crittercism.internal.cd;
import java.util.LinkedList;
import java.util.List;

public class CrittercismConfig
{
  private String a = null;
  private boolean b = false;
  private boolean c = false;
  private boolean d = true;
  private boolean e = true;
  private boolean f = false;
  private boolean g = a();
  private List<String> h = new LinkedList();
  private List<String> i = new LinkedList();
  
  public CrittercismConfig() {}
  
  public CrittercismConfig(CrittercismConfig paramCrittercismConfig)
  {
    this.a = paramCrittercismConfig.a;
    this.b = paramCrittercismConfig.b;
    this.c = paramCrittercismConfig.c;
    this.d = paramCrittercismConfig.d;
    this.e = paramCrittercismConfig.e;
    this.f = paramCrittercismConfig.f;
    this.g = paramCrittercismConfig.g;
    setURLBlacklistPatterns(paramCrittercismConfig.h);
    setPreserveQueryStringPatterns(paramCrittercismConfig.i);
  }
  
  private static boolean a()
  {
    return (Build.VERSION.SDK_INT >= 10) && (Build.VERSION.SDK_INT <= 23);
  }
  
  public final boolean allowsCellularAccess()
  {
    return this.d;
  }
  
  public final boolean delaySendingAppLoad()
  {
    return this.b;
  }
  
  public boolean equals(Object paramObject)
  {
    if (!(paramObject instanceof CrittercismConfig)) {}
    for (;;)
    {
      return false;
      paramObject = (CrittercismConfig)paramObject;
      if ((this.b == ((CrittercismConfig)paramObject).b) && (this.f == ((CrittercismConfig)paramObject).f) && (this.d == ((CrittercismConfig)paramObject).d) && (this.e == ((CrittercismConfig)paramObject).e) && (isServiceMonitoringEnabled() == ((CrittercismConfig)paramObject).isServiceMonitoringEnabled()) && (isVersionCodeToBeIncludedInVersionString() == ((CrittercismConfig)paramObject).isVersionCodeToBeIncludedInVersionString()))
      {
        String str1 = this.a;
        String str2 = ((CrittercismConfig)paramObject).a;
        boolean bool;
        if (str1 == null) {
          if (str2 == null) {
            bool = true;
          }
        }
        while ((bool) && (this.h.equals(((CrittercismConfig)paramObject).h)) && (this.i.equals(((CrittercismConfig)paramObject).i)))
        {
          return true;
          bool = false;
          continue;
          bool = str1.equals(str2);
        }
      }
    }
  }
  
  public final String getCustomVersionName()
  {
    return this.a;
  }
  
  public List<String> getPreserveQueryStringPatterns()
  {
    return new LinkedList(this.i);
  }
  
  public List<String> getURLBlacklistPatterns()
  {
    return new LinkedList(this.h);
  }
  
  public int hashCode()
  {
    int i3 = 1;
    String str = this.a;
    if (str != null) {}
    for (int j = str.hashCode();; j = 0)
    {
      int i4 = this.h.hashCode();
      int i5 = this.i.hashCode();
      int k;
      int m;
      label60:
      int n;
      label70:
      int i1;
      label80:
      int i2;
      if (this.b)
      {
        k = 1;
        if (!this.f) {
          break label155;
        }
        m = 1;
        if (!this.d) {
          break label160;
        }
        n = 1;
        if (!this.e) {
          break label166;
        }
        i1 = 1;
        if (!isServiceMonitoringEnabled()) {
          break label172;
        }
        i2 = 1;
        label90:
        if (!isVersionCodeToBeIncludedInVersionString()) {
          break label178;
        }
      }
      for (;;)
      {
        return Integer.valueOf((i2 + (i1 + (n + (m + (k + 0 << 1) << 1) << 1) << 1) << 1) + i3).hashCode() + (i5 + ((j + 0) * 31 + i4) * 31) * 31;
        k = 0;
        break;
        label155:
        m = 0;
        break label60;
        label160:
        n = 0;
        break label70;
        label166:
        i1 = 0;
        break label80;
        label172:
        i2 = 0;
        break label90;
        label178:
        i3 = 0;
      }
    }
  }
  
  public final boolean isLogcatReportingEnabled()
  {
    return this.f;
  }
  
  public final boolean isServiceMonitoringEnabled()
  {
    return this.g;
  }
  
  public final boolean isVersionCodeToBeIncludedInVersionString()
  {
    return this.c;
  }
  
  public boolean reportLocationData()
  {
    return this.e;
  }
  
  public final void setAllowsCellularAccess(boolean paramBoolean)
  {
    this.d = paramBoolean;
  }
  
  public final void setCustomVersionName(String paramString)
  {
    this.a = paramString;
  }
  
  public final void setDelaySendingAppLoad(boolean paramBoolean)
  {
    this.b = paramBoolean;
  }
  
  public final void setLogcatReportingEnabled(boolean paramBoolean)
  {
    this.f = paramBoolean;
  }
  
  public void setPreserveQueryStringPatterns(List<String> paramList)
  {
    this.i.clear();
    if (paramList != null) {
      this.i.addAll(paramList);
    }
  }
  
  public final void setReportLocationData(boolean paramBoolean)
  {
    this.e = paramBoolean;
  }
  
  public final void setServiceMonitoringEnabled(boolean paramBoolean)
  {
    if ((!a()) && (paramBoolean))
    {
      cd.c("OPTMZ is currently only allowed for api levels 10 to 23.  APM will not be installed");
      return;
    }
    this.g = paramBoolean;
  }
  
  public void setURLBlacklistPatterns(List<String> paramList)
  {
    this.h.clear();
    if (paramList != null) {
      this.h.addAll(paramList);
    }
  }
  
  public final void setVersionCodeToBeIncludedInVersionString(boolean paramBoolean)
  {
    this.c = paramBoolean;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/app/CrittercismConfig.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */