package com.crittercism.app;

import android.content.Context;
import android.content.res.AssetManager;
import com.crittercism.internal.cd;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class CrittercismNDK
{
  private static final String ASSET_SO_FILE_NAME = "lib64libcrittercism-v3.crt";
  private static final String DST_SO_FILE_NAME = "lib64libcrittercism-v3.so";
  private static final String[] LEGACY_SO_FILE_NAMES = { "libcrittercism-ndk.so", "libcrittercism-v3.so" };
  private static final String LIBRARY_NAME = "64libcrittercism-v3";
  private static boolean isNdkInstalled = false;
  
  public static boolean copyLibFromAssets(Context paramContext, File paramFile)
  {
    try
    {
      Object localObject = paramFile.getParentFile();
      cd.d("copyLibFromAssets: creating dir: " + ((File)localObject).getAbsolutePath());
      ((File)localObject).mkdirs();
      cd.d("copyLibFromAssets: installing library into: " + paramFile.getAbsolutePath());
      paramFile = new FileOutputStream(paramFile);
      paramContext = getJarredLibFileStream(paramContext);
      localObject = new byte[' '];
      for (;;)
      {
        int i = paramContext.read((byte[])localObject);
        if (i < 0) {
          break;
        }
        paramFile.write((byte[])localObject, 0, i);
      }
      paramContext.close();
    }
    catch (Exception paramContext)
    {
      cd.a("Could not install breakpad library: " + paramContext.toString());
      return false;
    }
    paramFile.close();
    cd.d("copyLibFromAssets: successful");
    return true;
  }
  
  public static File crashDumpDirectory(Context paramContext)
  {
    return new File(paramContext.getFilesDir().getAbsolutePath(), "/com.crittercism/dumps");
  }
  
  public static boolean doNdkSharedLibrariesExist(Context paramContext)
  {
    try
    {
      getJarredLibFileStream(paramContext);
      return true;
    }
    catch (IOException paramContext) {}
    return false;
  }
  
  public static File getInstalledLibraryFile(Context paramContext)
  {
    paramContext = paramContext.getFilesDir().getAbsolutePath() + "/com.crittercism/lib/";
    return new File(paramContext + "lib64libcrittercism-v3.so");
  }
  
  public static InputStream getJarredLibFileStream(Context paramContext)
  {
    String str1 = "armeabi";
    String str2 = System.getProperty("os.arch");
    cd.d("getJarredLibFileStream: os.arch: " + str2);
    if (str2.contains("v7")) {
      str1 = "armeabi" + "-v7a";
    }
    for (;;)
    {
      str1 = str1 + "/lib64libcrittercism-v3.crt";
      cd.d("getJarredLibFileStream: openning input stream from: " + str1);
      return paramContext.getAssets().open(str1);
      if (str2.equals("aarch64")) {
        str1 = "arm64-v8a";
      }
    }
  }
  
  public static native boolean installNdk(String paramString);
  
  public static void installNdkLib(Context paramContext)
  {
    boolean bool;
    if (doNdkSharedLibrariesExist(paramContext)) {
      bool = loadLibraryFromAssets(paramContext);
    }
    while (!bool)
    {
      cd.d("did not load NDK library.");
      return;
      try
      {
        System.loadLibrary("64libcrittercism-v3");
        bool = true;
      }
      catch (Throwable localThrowable)
      {
        bool = false;
      }
    }
    cd.d("loaded NDK library.");
    try
    {
      paramContext = crashDumpDirectory(paramContext);
      if (installNdk(paramContext.getAbsolutePath()))
      {
        paramContext.mkdirs();
        isNdkInstalled = true;
        cd.c("initialized NDK crash reporting.");
        return;
      }
    }
    catch (Throwable paramContext)
    {
      cd.a(paramContext);
      return;
    }
    cd.b("Unable to initialize NDK crash reporting.");
  }
  
  public static boolean isNdkCrashReportingInstalled()
  {
    return isNdkInstalled;
  }
  
  public static boolean loadLibraryFromAssets(Context paramContext)
  {
    File localFile2 = new File(paramContext.getFilesDir(), "/com.crittercism/lib/");
    File localFile1 = new File(localFile2, "lib64libcrittercism-v3.so");
    if (!localFile1.exists())
    {
      if (!copyLibFromAssets(paramContext, localFile1))
      {
        localFile1.delete();
        return false;
      }
      int i = 0;
      if (i < LEGACY_SO_FILE_NAMES.length)
      {
        File localFile3 = new File(localFile2, LEGACY_SO_FILE_NAMES[i]);
        if (localFile3.exists()) {}
        for (paramContext = "deleting";; paramContext = "not found")
        {
          cd.d("legacy lib: " + localFile3.getAbsolutePath() + ": " + paramContext);
          localFile3.delete();
          i += 1;
          break;
        }
      }
    }
    try
    {
      System.load(localFile1.getAbsolutePath());
      return true;
    }
    catch (Throwable paramContext)
    {
      cd.a("Unable to install NDK library: " + paramContext.getMessage());
      cd.a(paramContext);
      localFile1.delete();
    }
    return false;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/app/CrittercismNDK.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */