package com.crittercism.internal;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.SparseArray;
import java.text.ParseException;

public enum a
{
  private static SparseArray<a> f;
  int e;
  
  static
  {
    SparseArray localSparseArray = new SparseArray();
    f = localSparseArray;
    localSparseArray.put(0, a);
    f.put(1, b);
  }
  
  private a(int paramInt)
  {
    this.e = paramInt;
  }
  
  public static a a(int paramInt)
  {
    a[] arrayOfa = values();
    int j = arrayOfa.length;
    int i = 0;
    while (i < j)
    {
      a locala = arrayOfa[i];
      if (locala.e == paramInt) {
        return locala;
      }
      i += 1;
    }
    throw new ParseException("Unknown status code: " + Integer.toString(paramInt), 0);
  }
  
  public static a a(ConnectivityManager paramConnectivityManager)
  {
    if (paramConnectivityManager == null) {
      paramConnectivityManager = c;
    }
    a locala;
    do
    {
      return paramConnectivityManager;
      paramConnectivityManager = paramConnectivityManager.getActiveNetworkInfo();
      if ((paramConnectivityManager == null) || (!paramConnectivityManager.isConnected())) {
        return d;
      }
      int i = paramConnectivityManager.getType();
      locala = (a)f.get(i);
      paramConnectivityManager = locala;
    } while (locala != null);
    return c;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */