package com.crittercism.internal;

public abstract class ab
  extends v
{
  boolean d = false;
  int e;
  boolean f = false;
  private boolean g = false;
  private boolean h = false;
  
  public ab(v paramv)
  {
    super(paramv);
  }
  
  public final boolean a(w paramw)
  {
    int i = this.b.b;
    if ((i == 0) || ((i == 1) && (this.b.a[0] == '\r')))
    {
      i = 1;
      if (i == 0) {
        break label49;
      }
      this.h = true;
    }
    for (;;)
    {
      return true;
      i = 0;
      break;
      try
      {
        label49:
        Object localObject = paramw.toString().split(":", 2);
        if (localObject.length != 2) {
          return false;
        }
        paramw = localObject[0].trim();
        localObject = localObject[1].trim();
        if ((!this.d) && (paramw.equalsIgnoreCase("content-length")))
        {
          i = Integer.parseInt((String)localObject);
          if (i < 0) {
            return false;
          }
          this.d = true;
          this.e = i;
          return true;
        }
        if (paramw.equalsIgnoreCase("transfer-encoding"))
        {
          this.f = ((String)localObject).equalsIgnoreCase("chunked");
          return true;
        }
        if ((!this.g) && (paramw.equalsIgnoreCase("host")) && (localObject != null))
        {
          this.g = true;
          this.a.a((String)localObject);
          return true;
        }
      }
      catch (NumberFormatException paramw) {}
    }
    return false;
  }
  
  public final v b()
  {
    if (this.h) {
      return g();
    }
    this.b.b = 0;
    return this;
  }
  
  public final v c()
  {
    this.b.b = 0;
    return new ai(this);
  }
  
  protected final int d()
  {
    return 32;
  }
  
  protected final int e()
  {
    return 128;
  }
  
  protected abstract v g();
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/ab.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */