package com.crittercism.internal;

public final class ag
  extends v
{
  private int d = -1;
  
  public ag(ac paramac)
  {
    super(paramac);
  }
  
  public final boolean a(w paramw)
  {
    paramw = paramw.toString().split(" ");
    if (paramw.length >= 3) {
      try
      {
        this.d = Integer.parseInt(paramw[1]);
        this.a.a(this.d);
        return true;
      }
      catch (NumberFormatException paramw) {}
    }
    return false;
  }
  
  public final v b()
  {
    return new af(this, this.d);
  }
  
  public final v c()
  {
    return aj.d;
  }
  
  protected final int d()
  {
    return 20;
  }
  
  protected final int e()
  {
    return 64;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/ag.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */