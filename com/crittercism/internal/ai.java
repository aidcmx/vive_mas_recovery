package com.crittercism.internal;

public final class ai
  extends v
{
  private v d;
  
  public ai(v paramv)
  {
    super(paramv);
    this.d = paramv;
  }
  
  public final boolean a(int paramInt)
  {
    if (paramInt == -1)
    {
      this.a.a(aj.d);
      return true;
    }
    this.c += 1;
    if ((char)paramInt == '\n')
    {
      this.d.b(a());
      this.a.a(this.d);
      return true;
    }
    return false;
  }
  
  public final boolean a(w paramw)
  {
    return true;
  }
  
  public final v b()
  {
    return this;
  }
  
  public final v c()
  {
    return this;
  }
  
  protected final int d()
  {
    return 0;
  }
  
  protected final int e()
  {
    return 0;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/ai.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */