package com.crittercism.internal;

public final class aj
  extends v
{
  public static final aj d = new aj();
  
  private aj()
  {
    super(null);
  }
  
  public final boolean a(int paramInt)
  {
    this.c += 1;
    return false;
  }
  
  public final boolean a(w paramw)
  {
    return true;
  }
  
  public final int b(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    this.c += paramInt2;
    return -1;
  }
  
  public final v b()
  {
    return this;
  }
  
  public final v c()
  {
    return this;
  }
  
  protected final int d()
  {
    return 0;
  }
  
  protected final int e()
  {
    return 0;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/aj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */