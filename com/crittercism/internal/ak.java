package com.crittercism.internal;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import com.crittercism.app.CrittercismConfig;

public final class ak
{
  public String a = "1.0";
  public int b = 0;
  
  public ak(Context paramContext, CrittercismConfig paramCrittercismConfig)
  {
    try
    {
      paramContext = paramContext.getPackageManager().getPackageInfo(paramContext.getPackageName(), 0);
      this.a = paramContext.versionName;
      this.b = paramContext.versionCode;
      paramContext = paramCrittercismConfig.getCustomVersionName();
      if ((paramContext != null) && (paramContext.length() > 0)) {
        this.a = paramContext;
      }
      if (paramCrittercismConfig.isVersionCodeToBeIncludedInVersionString()) {
        this.a = (this.a + "-" + Integer.toString(this.b));
      }
      return;
    }
    catch (PackageManager.NameNotFoundException paramContext)
    {
      for (;;) {}
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/ak.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */