package com.crittercism.internal;

public abstract class al
  implements Thread.UncaughtExceptionHandler
{
  private Thread.UncaughtExceptionHandler a;
  
  public al(Thread.UncaughtExceptionHandler paramUncaughtExceptionHandler)
  {
    this.a = paramUncaughtExceptionHandler;
  }
  
  public abstract void a(Throwable paramThrowable);
  
  /* Error */
  public final void uncaughtException(Thread paramThread, Throwable paramThrowable)
  {
    // Byte code:
    //   0: aload_0
    //   1: aload_2
    //   2: invokevirtual 25	com/crittercism/internal/al:a	(Ljava/lang/Throwable;)V
    //   5: aload_0
    //   6: getfield 15	com/crittercism/internal/al:a	Ljava/lang/Thread$UncaughtExceptionHandler;
    //   9: ifnull +14 -> 23
    //   12: aload_0
    //   13: getfield 15	com/crittercism/internal/al:a	Ljava/lang/Thread$UncaughtExceptionHandler;
    //   16: aload_1
    //   17: aload_2
    //   18: invokeinterface 27 3 0
    //   23: return
    //   24: astore_3
    //   25: aload_3
    //   26: athrow
    //   27: astore_3
    //   28: aload_0
    //   29: getfield 15	com/crittercism/internal/al:a	Ljava/lang/Thread$UncaughtExceptionHandler;
    //   32: ifnull +14 -> 46
    //   35: aload_0
    //   36: getfield 15	com/crittercism/internal/al:a	Ljava/lang/Thread$UncaughtExceptionHandler;
    //   39: aload_1
    //   40: aload_2
    //   41: invokeinterface 27 3 0
    //   46: aload_3
    //   47: athrow
    //   48: astore_3
    //   49: ldc 29
    //   51: aload_3
    //   52: invokestatic 34	com/crittercism/internal/cd:a	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   55: aload_0
    //   56: getfield 15	com/crittercism/internal/al:a	Ljava/lang/Thread$UncaughtExceptionHandler;
    //   59: ifnull -36 -> 23
    //   62: aload_0
    //   63: getfield 15	com/crittercism/internal/al:a	Ljava/lang/Thread$UncaughtExceptionHandler;
    //   66: aload_1
    //   67: aload_2
    //   68: invokeinterface 27 3 0
    //   73: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	74	0	this	al
    //   0	74	1	paramThread	Thread
    //   0	74	2	paramThrowable	Throwable
    //   24	2	3	localThreadDeath	ThreadDeath
    //   27	20	3	localObject	Object
    //   48	4	3	localThrowable	Throwable
    // Exception table:
    //   from	to	target	type
    //   0	5	24	java/lang/ThreadDeath
    //   0	5	27	finally
    //   25	27	27	finally
    //   49	55	27	finally
    //   0	5	48	java/lang/Throwable
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/al.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */