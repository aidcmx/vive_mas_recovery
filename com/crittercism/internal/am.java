package com.crittercism.internal;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.net.ConnectivityManager.NetworkCallback;
import android.net.NetworkRequest;
import android.net.NetworkRequest.Builder;
import android.os.Build.VERSION;
import android.os.Process;
import android.webkit.WebView;
import com.crittercism.app.CrashData;
import com.crittercism.app.CrittercismCallback;
import com.crittercism.app.CrittercismConfig;
import com.crittercism.app.CrittercismNDK;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import org.json.JSONArray;
import org.json.JSONObject;

public final class am
{
  private Set<WebView> A = new HashSet();
  private Date B;
  private Date C = new Date();
  Application a;
  av<aq> b;
  av<bc> c;
  av<aq> d;
  av<at> e;
  av<ba> f = null;
  List<bv> g = new LinkedList();
  bv h;
  ScheduledExecutorService i = cf.a("crittercism networking");
  public ScheduledExecutorService j = cf.b("crittercism data");
  ap k;
  protected d l;
  cc m;
  public bo n;
  au o;
  private String p = null;
  private av<ar> q;
  private av<b> r;
  private av<cb> s;
  private ak t;
  private bp u;
  private CrittercismConfig v;
  private as w;
  private bx x;
  private bq y;
  private br z;
  
  public am(Application paramApplication, String paramString, CrittercismConfig paramCrittercismConfig)
  {
    this.p = paramString;
    this.a = paramApplication;
    this.w = new as(paramString);
    this.v = new CrittercismConfig(paramCrittercismConfig);
    this.t = new ak(this.a, this.v);
    this.x = new bx(this.a);
    boolean bool = a(this.a);
    this.k = new ap(this.a, this.p);
    paramApplication = this.a;
    if (bool) {
      paramApplication = new bd();
    }
    for (;;)
    {
      this.q = paramApplication;
      paramApplication = this.a;
      if (bool)
      {
        paramApplication = new az(10);
        label204:
        this.e = paramApplication;
        paramApplication = this.a;
        if (!bool) {
          break label867;
        }
        paramApplication = new az(1);
        label228:
        this.b = paramApplication;
        paramApplication = this.a;
        if (!bool) {
          break label891;
        }
        paramApplication = new az(1);
        label252:
        this.d = paramApplication;
        paramApplication = this.a;
        if (!bool) {
          break label915;
        }
        paramApplication = new az(5);
        label276:
        this.r = paramApplication;
        paramApplication = this.a;
        if (!bool) {
          break label940;
        }
        paramApplication = new bd();
        label299:
        this.c = paramApplication;
        paramApplication = this.a;
        if (!bool) {
          break label964;
        }
        paramApplication = new az(5);
        label323:
        this.s = paramApplication;
        paramApplication = this.a;
        paramString = this.p;
        if (!bool) {
          break label989;
        }
        paramApplication = new bd();
        label351:
        this.f = paramApplication;
        paramApplication = paramCrittercismConfig.getURLBlacklistPatterns();
        paramApplication.add(this.w.a.getHost());
        paramApplication.add(this.w.b.getHost());
        paramApplication.add(this.w.d.getHost());
        paramApplication.add(this.w.c.getHost());
        paramString = new d.a();
        paramString.a = this.j;
        paramString.b = paramApplication;
        paramString.c = paramCrittercismConfig.getPreserveQueryStringPatterns();
        paramString.d = this.r;
        paramString.e = this.e;
        paramString.f = this.k;
        this.l = new d(paramString.a, paramString.b, paramString.c, paramString.d, paramString.e, paramString.f, (byte)0);
        this.m = new cc(this.a, this.j, this.s, this.k);
        this.o = new au(this.t, this.a, new ao(this.a, this.v), this.p);
        this.j.submit(new b(bool));
      }
      try
      {
        this.B = new Date(bn.f());
        if (this.B != null)
        {
          a(at.a(this.B));
          if (this.v.isServiceMonitoringEnabled())
          {
            paramApplication = new Thread(new a((byte)0));
            paramApplication.start();
          }
        }
      }
      catch (IOException paramApplication)
      {
        try
        {
          for (;;)
          {
            paramApplication.join();
            Thread.setDefaultUncaughtExceptionHandler(new al(Thread.getDefaultUncaughtExceptionHandler())
            {
              public final void a(Throwable paramAnonymousThrowable)
              {
                Object localObject = am.this;
                Date localDate = new Date();
                paramAnonymousThrowable = new am.3((am)localObject, paramAnonymousThrowable, Thread.currentThread().getId(), localDate, Thread.getAllStackTraces());
                paramAnonymousThrowable = ((am)localObject).j.submit(paramAnonymousThrowable);
                try
                {
                  paramAnonymousThrowable.get();
                  paramAnonymousThrowable = ((am)localObject).h;
                  localObject = paramAnonymousThrowable.e;
                  if (localObject != null) {
                    ((ScheduledFuture)localObject).get();
                  }
                  localObject = paramAnonymousThrowable.f;
                  if (localObject != null) {
                    ((Future)localObject).get();
                  }
                  paramAnonymousThrowable = paramAnonymousThrowable.g;
                  if (paramAnonymousThrowable != null) {
                    paramAnonymousThrowable.get();
                  }
                  return;
                }
                catch (InterruptedException paramAnonymousThrowable)
                {
                  cd.b(paramAnonymousThrowable);
                  return;
                }
                catch (ExecutionException paramAnonymousThrowable)
                {
                  cd.b(paramAnonymousThrowable);
                }
              }
            });
            this.n = new bo(this.a, this.j, this.q, new ar(this.o), this.k, paramCrittercismConfig.delaySendingAppLoad(), this.m, this.B);
            this.u = new bp(this.a, this.j, this.e, this.k);
            this.y = new bq(this.a, this.j, this.e, this.k);
            if (Build.VERSION.SDK_INT >= 14) {
              this.z = new br(this.a, this.o);
            }
            return;
            paramApplication = new aw(paramApplication, "app_loads_2", new ar.b((byte)0), 10);
            break;
            paramApplication = new aw(paramApplication, "breadcrumbs", new at.a((byte)0), 250);
            break label204;
            label867:
            paramApplication = new aw(paramApplication, "exceptions", new aq.a((byte)0), 5);
            break label228;
            label891:
            paramApplication = new aw(paramApplication, "sdk_crashes", new aq.a((byte)0), 5);
            break label252;
            label915:
            paramApplication = new aw(paramApplication, "network_statistics", new b.b((byte)0), 50);
            break label276;
            label940:
            paramApplication = new aw(paramApplication, "ndk_crashes", new bc.b((byte)0), 5);
            break label299;
            label964:
            paramApplication = new aw(paramApplication, "finished_txns", new cb.g((byte)0), 50);
            break label323;
            label989:
            paramApplication = new bb(paramApplication, paramString);
            break label351;
            paramApplication = paramApplication;
            cd.a(paramApplication);
          }
          a(at.a(this.C));
        }
        catch (InterruptedException paramApplication)
        {
          for (;;)
          {
            cd.b(paramApplication);
          }
        }
      }
    }
  }
  
  private static boolean a(Context paramContext)
  {
    int i2 = Process.myUid();
    int i3 = Process.myPid();
    paramContext = (ActivityManager)paramContext.getSystemService("activity");
    Iterator localIterator = paramContext.getRunningAppProcesses().iterator();
    int i1 = 0;
    if (localIterator.hasNext())
    {
      if (((ActivityManager.RunningAppProcessInfo)localIterator.next()).uid != i2) {
        break label113;
      }
      i1 += 1;
    }
    label113:
    for (;;)
    {
      break;
      if (i1 <= 1) {}
      do
      {
        while (!paramContext.hasNext())
        {
          return false;
          paramContext = paramContext.getRunningServices(Integer.MAX_VALUE).iterator();
        }
      } while (((ActivityManager.RunningServiceInfo)paramContext.next()).pid != i3);
      return true;
    }
  }
  
  /* Error */
  public final void a(WebView paramWebView)
  {
    // Byte code:
    //   0: invokestatic 464	android/os/Looper:myLooper	()Landroid/os/Looper;
    //   3: invokestatic 467	android/os/Looper:getMainLooper	()Landroid/os/Looper;
    //   6: if_acmpeq +10 -> 16
    //   9: ldc_w 469
    //   12: invokestatic 471	com/crittercism/internal/cd:a	(Ljava/lang/String;)V
    //   15: return
    //   16: aload_0
    //   17: getfield 124	com/crittercism/internal/am:A	Ljava/util/Set;
    //   20: astore_2
    //   21: aload_2
    //   22: monitorenter
    //   23: aload_0
    //   24: getfield 124	com/crittercism/internal/am:A	Ljava/util/Set;
    //   27: aload_1
    //   28: invokeinterface 476 2 0
    //   33: ifeq +11 -> 44
    //   36: aload_2
    //   37: monitorexit
    //   38: return
    //   39: astore_1
    //   40: aload_2
    //   41: monitorexit
    //   42: aload_1
    //   43: athrow
    //   44: aload_0
    //   45: getfield 124	com/crittercism/internal/am:A	Ljava/util/Set;
    //   48: aload_1
    //   49: invokeinterface 477 2 0
    //   54: pop
    //   55: aload_2
    //   56: monitorexit
    //   57: new 479	com/crittercism/internal/ck
    //   60: dup
    //   61: aload_0
    //   62: aload_0
    //   63: getfield 240	com/crittercism/internal/am:l	Lcom/crittercism/internal/d;
    //   66: aload_0
    //   67: getfield 131	com/crittercism/internal/am:a	Landroid/app/Application;
    //   70: invokespecial 482	com/crittercism/internal/ck:<init>	(Lcom/crittercism/internal/am;Lcom/crittercism/internal/d;Landroid/content/Context;)V
    //   73: astore_3
    //   74: new 484	com/crittercism/internal/cj
    //   77: dup
    //   78: invokespecial 485	com/crittercism/internal/cj:<init>	()V
    //   81: pop
    //   82: getstatic 348	android/os/Build$VERSION:SDK_INT	I
    //   85: bipush 15
    //   87: if_icmpgt +70 -> 157
    //   90: aload_1
    //   91: invokestatic 488	com/crittercism/internal/cj:a	(Landroid/webkit/WebView;)Landroid/webkit/WebViewClient;
    //   94: astore_2
    //   95: aload_1
    //   96: new 490	com/crittercism/internal/ci
    //   99: dup
    //   100: aload_2
    //   101: aload_3
    //   102: getfield 492	com/crittercism/internal/ck:b	Lcom/crittercism/internal/d;
    //   105: aload_3
    //   106: getfield 495	com/crittercism/internal/ck:c	Lcom/crittercism/internal/c;
    //   109: aload_3
    //   110: getfield 497	com/crittercism/internal/ck:d	Ljava/lang/String;
    //   113: invokespecial 500	com/crittercism/internal/ci:<init>	(Landroid/webkit/WebViewClient;Lcom/crittercism/internal/d;Lcom/crittercism/internal/c;Ljava/lang/String;)V
    //   116: invokevirtual 506	android/webkit/WebView:setWebViewClient	(Landroid/webkit/WebViewClient;)V
    //   119: aload_1
    //   120: invokevirtual 510	android/webkit/WebView:getSettings	()Landroid/webkit/WebSettings;
    //   123: invokevirtual 515	android/webkit/WebSettings:getJavaScriptEnabled	()Z
    //   126: ifeq -111 -> 15
    //   129: aload_1
    //   130: new 517	com/crittercism/webview/CritterJSInterface
    //   133: dup
    //   134: aload_3
    //   135: getfield 520	com/crittercism/internal/ck:a	Lcom/crittercism/internal/am;
    //   138: invokespecial 523	com/crittercism/webview/CritterJSInterface:<init>	(Lcom/crittercism/internal/am;)V
    //   141: ldc_w 525
    //   144: invokevirtual 529	android/webkit/WebView:addJavascriptInterface	(Ljava/lang/Object;Ljava/lang/String;)V
    //   147: return
    //   148: astore_1
    //   149: aload_1
    //   150: invokevirtual 532	com/crittercism/internal/bh:getMessage	()Ljava/lang/String;
    //   153: invokestatic 534	com/crittercism/internal/cd:b	(Ljava/lang/String;)V
    //   156: return
    //   157: getstatic 348	android/os/Build$VERSION:SDK_INT	I
    //   160: bipush 18
    //   162: if_icmpgt +11 -> 173
    //   165: aload_1
    //   166: invokestatic 536	com/crittercism/internal/cj:b	(Landroid/webkit/WebView;)Landroid/webkit/WebViewClient;
    //   169: astore_2
    //   170: goto -75 -> 95
    //   173: aload_1
    //   174: invokestatic 538	com/crittercism/internal/cj:c	(Landroid/webkit/WebView;)Landroid/webkit/WebViewClient;
    //   177: astore_2
    //   178: goto -83 -> 95
    //   181: astore_1
    //   182: aload_1
    //   183: invokestatic 404	com/crittercism/internal/cd:b	(Ljava/lang/Throwable;)V
    //   186: ldc_w 540
    //   189: invokestatic 471	com/crittercism/internal/cd:a	(Ljava/lang/String;)V
    //   192: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	193	0	this	am
    //   0	193	1	paramWebView	WebView
    //   20	158	2	localObject	Object
    //   73	62	3	localck	ck
    // Exception table:
    //   from	to	target	type
    //   23	38	39	finally
    //   40	42	39	finally
    //   44	57	39	finally
    //   74	82	148	com/crittercism/internal/bh
    //   82	95	181	com/crittercism/internal/bh
    //   157	170	181	com/crittercism/internal/bh
    //   173	178	181	com/crittercism/internal/bh
  }
  
  public final void a(final CrittercismCallback<CrashData> paramCrittercismCallback)
  {
    this.j.execute(new Runnable()
    {
      public final void run()
      {
        CrashData localCrashData = bz.a;
        Object localObject = localCrashData;
        if (localCrashData != null) {
          localObject = localCrashData.copy();
        }
        localObject = new am.c(paramCrittercismCallback, (CrashData)localObject);
        am.this.i.execute((Runnable)localObject);
      }
    });
  }
  
  public final void a(ap paramap)
  {
    Object localObject1 = this.x.a();
    boolean bool = new by(this.a).a();
    this.g.add(new bv(this.w, this.j, this.i, this.b, new aq.b(this.p, (String)localObject1, "exceptions", "/android_v2/handle_exceptions"), "EXCEPTIONS", paramap, ap.j, ap.k));
    this.h = new bv(this.w, this.j, this.i, this.d, new aq.b(this.p, (String)localObject1, "crashes", "/android_v2/handle_crashes"), "CRASHES", paramap, ap.f, ap.g);
    this.g.add(this.h);
    this.g.add(new bv(this.w, this.j, this.i, this.c, new bc.a(this.p, (String)localObject1), "NDK", paramap, ap.n, ap.o));
    this.g.add(new bv(this.w, this.j, this.i, this.f, new ba.a(this.p, (String)localObject1), "METADATA", paramap, ap.r, ap.s));
    this.g.add(new bv(this.w, this.j, this.i, this.r, new b.a(this.o), "APM", paramap, ap.b, ap.c));
    localObject1 = new bv(this.w, this.j, this.i, this.q, new ar.a(this.p, (String)localObject1), "APPLOADS", paramap, ap.v, ap.w);
    ((bv)localObject1).c = new bs(this.w.b, this.a, this.o, paramap);
    this.g.add(localObject1);
    this.g.add(new bv(this.w, this.j, this.i, this.s, new cb.f(this.e, this.o), "USERFLOWS", paramap, ap.z, ap.A));
    if (ao.a(this.a, "android.permission.ACCESS_NETWORK_STATE")) {}
    for (paramap = (ConnectivityManager)this.a.getSystemService("connectivity");; paramap = null)
    {
      localObject1 = this.g.iterator();
      while (((Iterator)localObject1).hasNext())
      {
        bv localbv = (bv)((Iterator)localObject1).next();
        localbv.i = paramap;
        if ((localbv.i != null) && (Build.VERSION.SDK_INT >= 21))
        {
          Object localObject2 = new NetworkRequest.Builder();
          ((NetworkRequest.Builder)localObject2).addCapability(12);
          if (!localbv.h) {
            ((NetworkRequest.Builder)localObject2).addTransportType(1);
          }
          localObject2 = ((NetworkRequest.Builder)localObject2).build();
          localbv.j = new bv.4(localbv);
          paramap.registerNetworkCallback((NetworkRequest)localObject2, (ConnectivityManager.NetworkCallback)localbv.j);
        }
        localbv.h = this.v.allowsCellularAccess();
        localbv.a(bool);
      }
      return;
    }
  }
  
  public final void a(final at paramat)
  {
    this.j.execute(new Runnable()
    {
      public final void run()
      {
        am.this.e.a(paramat);
      }
    });
  }
  
  public final void a(String paramString)
  {
    cc localcc = this.m;
    long l1 = System.currentTimeMillis();
    synchronized (localcc.a)
    {
      localcc.a.remove(paramString);
      if (localcc.a.size() >= 50)
      {
        cd.b("Aborting beginUserflow(" + paramString + "). Maximum number of userflows exceeded.");
        return;
      }
      long l2 = ((Long)localcc.d.a(ap.H)).longValue();
      l2 = ((Long)localcc.d.a(ap.a(paramString, l2))).longValue();
      Object localObject = new cb.a();
      ((cb.a)localObject).a = paramString;
      ((cb.a)localObject).b = l1;
      ((cb.a)localObject).c = -1;
      ((cb.a)localObject).d = l2;
      localObject = ((cb.a)localObject).a();
      localcc.a.put(paramString, localObject);
      cd.d("Added userflow: " + paramString);
      return;
    }
  }
  
  public final void a(String paramString, int paramInt)
  {
    Object localObject = this.m;
    synchronized (((cc)localObject).a)
    {
      localObject = (cb)((cc)localObject).a.get(paramString);
      if (localObject == null)
      {
        cd.b("setUserflowValue(" + paramString + "): no such userflow");
        return;
      }
      ((cb)localObject).c = paramInt;
      return;
    }
  }
  
  public final void a(String paramString1, String paramString2, long paramLong1, long paramLong2, long paramLong3, int paramInt, bj parambj)
  {
    long l1 = System.currentTimeMillis() - paramLong1;
    if (paramString1 == null)
    {
      cd.a("Null HTTP request method provided. Endpoint will not be logged.");
      return;
    }
    if (paramString2 == null)
    {
      cd.a("Null url provided. Endpoint will not be logged");
      return;
    }
    if ((paramLong2 < 0L) || (paramLong3 < 0L))
    {
      cd.a("Invalid byte values. Bytes need to be non-negative. Endpoint will not be logged.");
      return;
    }
    if ((paramLong1 < 0L) || (l1 < 0L))
    {
      cd.a("Invalid latency '" + paramLong1 + "'. Endpoint will not be logged.");
      return;
    }
    c localc = new c(this.a);
    b localb = new b();
    localb.j = paramString1.toUpperCase();
    localb.a(paramString2);
    localb.a(paramLong2);
    localb.b(paramLong3);
    localb.i = paramInt;
    localb.o = a.a(localc.a);
    localb.c(l1);
    localb.d(l1 + paramLong1);
    localb.k = parambj;
    if (an.b()) {
      localb.a(an.a());
    }
    this.l.a(localb, b.c.l);
  }
  
  public final void a(final Throwable paramThrowable)
  {
    if (paramThrowable == null) {}
    for (;;)
    {
      try
      {
        cd.b("Calling logHandledException with a null java.lang.Throwable. Nothing will be reported to Crittercism");
        return;
      }
      finally {}
      paramThrowable = new Runnable()
      {
        public final void run()
        {
          aq localaq;
          String str;
          HashMap localHashMap;
          if (((Boolean)am.this.k.a(ap.i)).booleanValue())
          {
            localaq = new aq(paramThrowable, am.this.o, this.b);
            if (((Boolean)am.this.k.a(ap.G)).booleanValue())
            {
              localObject = localaq.d;
              str = localaq.e;
              localHashMap = new HashMap();
              if (localObject == null) {
                break label201;
              }
              localHashMap.put("name", localObject);
              if (str == null) {
                break label207;
              }
            }
          }
          label201:
          label207:
          for (Object localObject = str;; localObject = "")
          {
            localHashMap.put("reason", localObject);
            localObject = new at(at.b.g, new JSONObject(localHashMap));
            am.this.e.a((bf)localObject);
            localaq.a(am.this.e);
            localaq.m = ((Float)am.this.k.a(ap.l)).floatValue();
            am.this.b.a(localaq);
            return;
            localObject = "";
            break;
          }
        }
      };
      this.j.execute(paramThrowable);
    }
  }
  
  public final void a(final JSONObject paramJSONObject)
  {
    this.j.execute(new Runnable()
    {
      public final void run()
      {
        if (((Boolean)am.this.k.a(ap.q)).booleanValue()) {
          ((bb)am.this.f).a(paramJSONObject);
        }
      }
    });
  }
  
  public final boolean a()
  {
    Future localFuture = this.j.submit(new Callable() {});
    try
    {
      boolean bool = ((Boolean)localFuture.get()).booleanValue();
      return bool;
    }
    catch (InterruptedException localInterruptedException)
    {
      cd.b(localInterruptedException);
      return false;
    }
    catch (ExecutionException localExecutionException)
    {
      cd.b(localExecutionException);
    }
    return false;
  }
  
  public final void b(String paramString)
  {
    this.m.a(paramString);
  }
  
  public final boolean b()
  {
    Future localFuture = this.j.submit(new Callable() {});
    try
    {
      boolean bool = ((Boolean)localFuture.get()).booleanValue();
      return bool;
    }
    catch (InterruptedException localInterruptedException)
    {
      cd.b(localInterruptedException);
      return false;
    }
    catch (ExecutionException localExecutionException)
    {
      cd.b(localExecutionException);
    }
    return false;
  }
  
  public final void c(String paramString)
  {
    cc localcc = this.m;
    long l1 = System.currentTimeMillis();
    synchronized (localcc.a)
    {
      cb localcb = (cb)localcc.a.remove(paramString);
      if (localcb == null)
      {
        cd.b("failUserflow(" + paramString + "): no such userflow");
        return;
      }
      localcb.a(cb.d.e, l1);
      localcc.b.submit(new cc.2(localcc, localcb));
      return;
    }
  }
  
  public final void d(String paramString)
  {
    cc localcc = this.m;
    synchronized (localcc.a)
    {
      localcc.a.remove(paramString);
      return;
    }
  }
  
  public final int e(String paramString)
  {
    return this.m.b(paramString);
  }
  
  final class a
    implements Runnable
  {
    private a() {}
    
    public final void run()
    {
      boolean bool2 = false;
      boolean bool3;
      boolean bool1;
      do
      {
        for (;;)
        {
          try
          {
            c localc = new c(am.this.a);
            d locald = am.this.l;
            bool3 = t.a(locald, localc);
            cd.d("Http network insights installation: " + bool3);
            if (Build.VERSION.SDK_INT >= 19)
            {
              bool1 = m.a(locald, localc);
              cd.d("Https network insights installation: " + bool1);
              if (!bool1) {
                break;
              }
              bool2 = i.a(locald, localc);
              cd.d("Network insights provider service instrumented: " + bool2);
              break;
              cd.c("installed service monitoring");
              return;
            }
            if (Build.VERSION.SDK_INT >= 14) {
              bool1 = l.a(locald, localc);
            } else {
              bool1 = false;
            }
          }
          catch (Exception localException)
          {
            cd.d("Exception in installApm: " + localException.getClass().getName());
            cd.a(localException);
            return;
          }
        }
      } while ((bool3) || (bool1) || (bool2));
    }
  }
  
  final class b
    implements Runnable
  {
    private boolean b;
    
    public b(boolean paramBoolean)
    {
      this.b = paramBoolean;
    }
    
    private void a(av<at> paramav)
    {
      int j = 0;
      Object localObject = paramav.b();
      if ((localObject instanceof LinkedList)) {}
      int i;
      at localat;
      for (localObject = (LinkedList)localObject;; localObject = new LinkedList((Collection)localObject))
      {
        localObject = ((LinkedList)localObject).descendingIterator();
        i = 0;
        for (;;)
        {
          if (!((Iterator)localObject).hasNext()) {
            break label104;
          }
          localat = (at)((Iterator)localObject).next();
          if (i == 0) {
            break;
          }
          paramav.a(localat.a);
        }
      }
      if (localat.c == at.b.a) {
        i = 1;
      }
      for (;;)
      {
        break;
        label104:
        i = j;
        while (i < 4)
        {
          ce.a(aw.a(am.this.a, new String[] { "network_bcs", "previous_bcs", "current_bcs", "system_bcs" }[i]));
          i += 1;
        }
        return;
      }
    }
    
    public final void run()
    {
      am.this.k.a(ap.v);
      ce.a(new File(am.this.a.getFilesDir(), "com.crittercism/pending"));
      Object localObject1 = am.this.a.getSharedPreferences("com.crittercism.usersettings", 0);
      if (!((SharedPreferences)localObject1).getBoolean("crashedOnLastLoad", false)) {
        localObject1 = null;
      }
      for (;;)
      {
        bz.a = (CrashData)localObject1;
        bz.a(am.this.a, null);
        localObject1 = new ca(am.this.a);
        int i = ((ca)localObject1).a.getInt("sessionIDSetting", 0);
        ((ca)localObject1).a.edit().putInt("sessionIDSetting", i + 1).commit();
        if (!this.b)
        {
          localObject1 = bc.a(CrittercismNDK.crashDumpDirectory(am.this.a), am.this.e, am.this.o);
          if (localObject1 != null)
          {
            if (((Boolean)am.this.k.a(ap.m)).booleanValue())
            {
              ((bc)localObject1).f = ((Float)am.this.k.a(ap.p)).floatValue();
              am.this.c.a((bf)localObject1);
            }
            bz.a = new CrashData("NDK crash", "", new Date());
          }
        }
        a(am.this.e);
        if (!this.b) {}
        try
        {
          CrittercismNDK.installNdkLib(am.this.a);
          localObject1 = am.this.a;
          String str1 = am.this.o.e;
          ((Context)localObject1).getSharedPreferences("com.crittercism." + str1 + ".usermetadata", 0).edit().clear().commit();
          am.this.a(am.this.k);
          return;
          str1 = ((SharedPreferences)localObject1).getString("crashName", "");
          String str2 = ((SharedPreferences)localObject1).getString("crashReason", "");
          long l = ((SharedPreferences)localObject1).getLong("crashDate", 0L);
          if (l != 0L)
          {
            localObject1 = new Date(l);
            localObject1 = new CrashData(str1, str2, (Date)localObject1);
          }
        }
        catch (Throwable localThrowable)
        {
          for (;;)
          {
            cd.d("Exception installing ndk library: " + localThrowable.getClass().getName());
            continue;
            Object localObject2 = null;
          }
        }
      }
    }
  }
  
  static final class c
    implements Runnable
  {
    private CrittercismCallback<CrashData> a;
    private CrashData b;
    
    public c(CrittercismCallback<CrashData> paramCrittercismCallback, CrashData paramCrashData)
    {
      this.a = paramCrittercismCallback;
      this.b = paramCrashData;
    }
    
    public final void run()
    {
      this.a.onDataReceived(this.b);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/am.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */