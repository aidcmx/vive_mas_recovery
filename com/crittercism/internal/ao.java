package com.crittercism.internal;

import android.content.Context;
import android.content.pm.PackageManager;
import com.crittercism.app.CrittercismConfig;

public final class ao
{
  public boolean a;
  public boolean b;
  private boolean c;
  
  public ao(Context paramContext, CrittercismConfig paramCrittercismConfig)
  {
    this.a = paramCrittercismConfig.isLogcatReportingEnabled();
    this.b = a("android.permission.ACCESS_NETWORK_STATE", paramContext);
    this.c = a("android.permission.GET_TASKS", paramContext);
  }
  
  public static boolean a(Context paramContext, String paramString)
  {
    return paramContext.getPackageManager().checkPermission(paramString, paramContext.getPackageName()) != -1;
  }
  
  private static boolean a(String paramString, Context paramContext)
  {
    return paramContext.getPackageManager().checkPermission(paramString, paramContext.getPackageName()) == 0;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/ao.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */