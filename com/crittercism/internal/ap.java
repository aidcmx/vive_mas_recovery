package com.crittercism.internal;

import android.content.Context;
import android.content.SharedPreferences;
import java.util.Iterator;
import java.util.LinkedList;

public final class ap
{
  public static final d A = new d("reporter.userflow.frequency", Long.valueOf(20000L));
  public static final b B = new b("data.userflow.rate", Float.valueOf(1.0F));
  public static final a C = new a("data.breadcrumb.foreground.enabled", Boolean.valueOf(true));
  public static final a D = new a("data.breadcrumb.activity.enabled", Boolean.valueOf(true));
  public static final a E = new a("data.breadcrumb.networkstate.enabled", Boolean.valueOf(true));
  public static final a F = new a("data.breadcrumb.networkstats.enabled", Boolean.valueOf(true));
  public static final a G = new a("data.breadcrumb.exception.enabled", Boolean.valueOf(true));
  public static final d H = new d("userflow.defaultTimeoutMs", Long.valueOf(3600000L));
  public static final a a = new a("data.apm.enabled", Boolean.valueOf(true));
  public static final a b = new a("reporter.apm.enabled", Boolean.valueOf(false));
  public static final d c = new d("reporter.apm.frequency", Long.valueOf(20000L));
  public static final b d = new b("data.apm.rate", Float.valueOf(1.0F));
  public static final a e = new a("data.crash.enabled", Boolean.valueOf(true));
  public static final a f = new a("reporter.crash.enabled", Boolean.valueOf(true));
  public static final d g = new d("reporter.crash.frequency", Long.valueOf(0L));
  public static final b h = new b("data.crash.rate", Float.valueOf(1.0F));
  public static final a i = new a("data.he.enabled", Boolean.valueOf(true));
  public static final a j = new a("reporter.he.enabled", Boolean.valueOf(true));
  public static final d k = new d("reporter.he.frequency", Long.valueOf(60000L));
  public static final b l = new b("data.he.rate", Float.valueOf(1.0F));
  public static final a m = new a("data.ndk.enabled", Boolean.valueOf(true));
  public static final a n = new a("reporter.ndk.enabled", Boolean.valueOf(true));
  public static final d o = new d("reporter.ndk.frequency", Long.valueOf(10000L));
  public static final b p = new b("data.ndk.rate", Float.valueOf(1.0F));
  public static final a q = new a("data.metadata.enabled", Boolean.valueOf(true));
  public static final a r = new a("reporter.metadata.enabled", Boolean.valueOf(true));
  public static final d s = new d("reporter.metadata.frequency", Long.valueOf(10000L));
  public static final b t = new b("data.metadata.rate", Float.valueOf(1.0F));
  public static final a u = new a("data.appload.enabled", Boolean.valueOf(true));
  public static final a v = new a("reporter.appload.enabled", Boolean.valueOf(true));
  public static final d w = new d("reporter.appload.frequency", Long.valueOf(10000L));
  public static final b x = new b("data.appload.rate", Float.valueOf(1.0F));
  public static final a y = new a("data.userflow.enabled", Boolean.valueOf(true));
  public static final a z = new a("reporter.userflow.enabled", Boolean.valueOf(false));
  public final LinkedList<c> I = new LinkedList();
  private String J;
  private Context K;
  private SharedPreferences L;
  
  public ap(Context paramContext, String paramString)
  {
    this.J = paramString;
    this.K = paramContext;
  }
  
  private SharedPreferences a()
  {
    if (this.L == null) {
      this.L = this.K.getSharedPreferences("com.crittercism.settings." + this.J, 0);
    }
    return this.L;
  }
  
  public static d a(String paramString, long paramLong)
  {
    return new d("userflow.timeouts." + paramString, Long.valueOf(paramLong));
  }
  
  public final <T> T a(e<T> parame)
  {
    return (T)parame.a(a());
  }
  
  public final <T> void a(e<T> parame, T paramT)
  {
    parame.a(a(), paramT);
    paramT = this.I.iterator();
    while (paramT.hasNext()) {
      ((c)paramT.next()).a(this, parame.a);
    }
  }
  
  public final void b(String paramString, long paramLong)
  {
    a(a(paramString, paramLong), Long.valueOf(paramLong));
  }
  
  public static final class a
    extends ap.e<Boolean>
  {
    public a(String paramString, Boolean paramBoolean)
    {
      super(paramBoolean);
    }
  }
  
  public static final class b
    extends ap.e<Float>
  {
    public b(String paramString, Float paramFloat)
    {
      super(paramFloat);
    }
  }
  
  public static abstract interface c
  {
    public abstract void a(ap paramap, String paramString);
  }
  
  public static final class d
    extends ap.e<Long>
  {
    public d(String paramString, Long paramLong)
    {
      super(paramLong);
    }
  }
  
  public static abstract class e<T>
  {
    protected String a;
    protected T b;
    
    public e(String paramString, T paramT)
    {
      this.a = paramString;
      this.b = paramT;
    }
    
    public abstract T a(SharedPreferences paramSharedPreferences);
    
    public final String a()
    {
      return this.a;
    }
    
    public abstract void a(SharedPreferences paramSharedPreferences, T paramT);
    
    public final T b()
    {
      return (T)this.b;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/ap.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */