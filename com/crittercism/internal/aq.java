package com.crittercism.internal;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Debug;
import android.os.Debug.MemoryInfo;
import android.view.Display;
import android.view.WindowManager;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class aq
  implements bf
{
  public long a;
  JSONArray b = new JSONArray();
  JSONArray c = new JSONArray();
  public String d;
  public String e = "";
  JSONArray f = new JSONArray();
  public JSONArray g;
  public String h = cg.a.a();
  JSONObject i;
  int j = -1;
  boolean k = false;
  String l = be.a.a();
  public float m = 1.0F;
  
  private aq() {}
  
  public aq(Throwable paramThrowable, au paramau, long paramLong)
  {
    this.k = (paramThrowable instanceof bm);
    this.l = be.a.a();
    this.i = new JSONObject();
    for (;;)
    {
      try
      {
        JSONObject localJSONObject = this.i.putOpt("activity", paramau.g).putOpt("app_version", paramau.a.a).putOpt("app_version_code", paramau.a()).putOpt("arch", System.getProperty("os.arch"));
        localObject1 = paramau.b.getApplicationContext().registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        if (localObject1 == null) {
          continue;
        }
        localObject2 = Double.valueOf(1.0D);
        n = ((Intent)localObject1).getIntExtra("level", -1);
        double d1 = ((Intent)localObject1).getIntExtra("scale", -1);
        localObject1 = localObject2;
        if (n >= 0)
        {
          localObject1 = localObject2;
          if (d1 > 0.0D) {
            localObject1 = Double.valueOf(n / d1);
          }
        }
        localObject2 = localJSONObject.putOpt("battery_level", localObject1).putOpt("carrier", paramau.b()).putOpt("mobile_country_code", paramau.c()).putOpt("mobile_network_code", paramau.d()).putOpt("disk_space_free", paramau.j()).putOpt("disk_space_total", paramau.k()).putOpt("dpi", paramau.e()).putOpt("xdpi", Float.valueOf(paramau.f())).putOpt("ydpi", Float.valueOf(paramau.g())).putOpt("locale", paramau.i());
        if (!paramau.c.a) {
          continue;
        }
        localObject1 = paramau.d.a();
        localObject1 = ((JSONObject)localObject2).putOpt("logcat", localObject1);
        localObject2 = new Debug.MemoryInfo();
        Debug.getMemoryInfo((Debug.MemoryInfo)localObject2);
        n = ((Debug.MemoryInfo)localObject2).dalvikPss;
        i1 = ((Debug.MemoryInfo)localObject2).nativePss;
        localObject1 = ((JSONObject)localObject1).putOpt("memory_usage", Integer.valueOf((((Debug.MemoryInfo)localObject2).otherPss + (n + i1)) * 1024)).putOpt("memory_total", au.l()).putOpt("mobile_network", paramau.a(0)).putOpt("model", Build.MODEL).putOpt("name", new String());
        i1 = paramau.b.getResources().getConfiguration().orientation;
        n = i1;
        if (i1 == 0)
        {
          localObject2 = ((WindowManager)paramau.b.getSystemService("window")).getDefaultDisplay();
          if (((Display)localObject2).getWidth() != ((Display)localObject2).getHeight()) {
            continue;
          }
          n = 3;
        }
        ((JSONObject)localObject1).putOpt("orientation", Integer.valueOf(n)).putOpt("sd_space_free", paramau.m()).putOpt("sd_space_total", paramau.n()).putOpt("system", "android").putOpt("system_version", Build.VERSION.RELEASE).putOpt("wifi", paramau.a(1));
      }
      catch (JSONException paramau)
      {
        Object localObject1;
        Object localObject2;
        int n;
        int i1;
        continue;
      }
      this.b = new JSONArray();
      this.a = paramLong;
      this.d = a(paramThrowable);
      if (paramThrowable.getMessage() != null) {
        this.e = paramThrowable.getMessage();
      }
      if (!this.k) {
        this.j = c(paramThrowable);
      }
      paramThrowable = b(paramThrowable);
      i1 = paramThrowable.length;
      n = 0;
      if (n >= i1) {
        continue;
      }
      paramau = paramThrowable[n];
      this.f.put(paramau);
      n += 1;
      continue;
      localObject1 = null;
      continue;
      localObject1 = null;
      continue;
      n = ((Display)localObject2).getWidth();
      i1 = ((Display)localObject2).getHeight();
      if (n > i1) {
        n = 2;
      } else {
        n = 1;
      }
    }
  }
  
  private String a(Throwable paramThrowable)
  {
    Throwable localThrowable = paramThrowable;
    if (this.k) {
      return ((bm)paramThrowable).a;
    }
    String str;
    do
    {
      localThrowable = paramThrowable;
      str = localThrowable.getClass().getName();
      paramThrowable = localThrowable.getCause();
    } while ((paramThrowable != null) && (paramThrowable != localThrowable));
    return str;
  }
  
  private static String[] b(Throwable paramThrowable)
  {
    StringWriter localStringWriter = new StringWriter();
    PrintWriter localPrintWriter = new PrintWriter(localStringWriter);
    for (;;)
    {
      paramThrowable.printStackTrace(localPrintWriter);
      Throwable localThrowable = paramThrowable.getCause();
      if ((localThrowable == null) || (localThrowable == paramThrowable)) {
        return localStringWriter.toString().split("\n");
      }
      paramThrowable = localThrowable;
    }
  }
  
  private static int c(Throwable paramThrowable)
  {
    StackTraceElement[] arrayOfStackTraceElement = paramThrowable.getStackTrace();
    int n = 0;
    if (n < arrayOfStackTraceElement.length) {
      paramThrowable = arrayOfStackTraceElement[n];
    }
    for (;;)
    {
      try
      {
        Class localClass = Class.forName(paramThrowable.getClassName());
        paramThrowable = ClassLoader.getSystemClassLoader();
        if (paramThrowable == null) {
          break label71;
        }
        if (localClass.getClassLoader() == paramThrowable)
        {
          i1 = 1;
          if (i1 == 0) {
            return n + 1;
          }
        }
        else
        {
          paramThrowable = paramThrowable.getParent();
          continue;
        }
      }
      catch (ClassNotFoundException paramThrowable)
      {
        n += 1;
      }
      return -1;
      label71:
      int i1 = 0;
    }
  }
  
  public final void a(av<at> paramav)
  {
    this.b = paramav.a();
  }
  
  public final void a(Collection<cb> paramCollection)
  {
    this.c = new JSONArray();
    paramCollection = paramCollection.iterator();
    while (paramCollection.hasNext())
    {
      cb localcb = (cb)paramCollection.next();
      this.c.put(localcb.b());
    }
  }
  
  public final String f()
  {
    return this.l;
  }
  
  public static final class a
    implements aw.b<aq>
  {
    private static aq b(File paramFile)
    {
      paramFile = ce.b(paramFile);
      try
      {
        paramFile = new JSONObject(paramFile);
        aq localaq = new aq((byte)0);
        localaq.a = paramFile.getLong("currentThreadID");
        localaq.b = paramFile.getJSONArray("breadcrumbs");
        localaq.c = paramFile.getJSONArray("txns");
        localaq.d = paramFile.getString("exceptionName");
        localaq.e = paramFile.getString("exceptionReason");
        localaq.f = paramFile.getJSONArray("stacktrace");
        localaq.g = paramFile.optJSONArray("threads");
        localaq.h = paramFile.getString("ts");
        localaq.m = ((float)paramFile.getDouble("rate"));
        localaq.i = paramFile.getJSONObject("appState");
        localaq.j = paramFile.getInt("suspectLineIndex");
        localaq.k = paramFile.getBoolean("isPluginException");
        localaq.l = paramFile.getString("fileName");
        return localaq;
      }
      catch (JSONException paramFile)
      {
        throw new IOException(paramFile.getMessage());
      }
    }
  }
  
  public static final class b
    extends bw
  {
    private String c;
    private String d;
    
    public b(String paramString1, String paramString2, String paramString3, String paramString4)
    {
      super(paramString2);
      this.c = paramString3;
      this.d = paramString4;
    }
    
    public final bt a(as paramas, List<? extends bf> paramList)
    {
      paramas = new URL(paramas.b, this.d);
      JSONObject localJSONObject;
      try
      {
        localJSONObject = new JSONObject();
        localJSONObject.put("app_id", this.a);
        localJSONObject.put("hashed_device_id", this.b);
        localJSONObject.put("library_version", "5.8.1");
        JSONArray localJSONArray = new JSONArray();
        paramList = paramList.iterator();
        while (paramList.hasNext()) {
          localJSONArray.put(((bf)paramList.next()).g());
        }
        localJSONObject.put(this.c, localJSONArray);
      }
      catch (JSONException paramas)
      {
        throw ((IOException)new IOException(paramas.getMessage()).initCause(paramas));
      }
      paramas = bt.a(paramas, this.a, localJSONObject);
      return paramas;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/aq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */