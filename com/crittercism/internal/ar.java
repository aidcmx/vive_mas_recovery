package com.crittercism.internal;

import android.os.Build;
import android.os.Build.VERSION;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class ar
  implements bf
{
  String a;
  String b;
  String c;
  String d;
  String e;
  String f;
  String g;
  int h;
  int i;
  String j;
  String k;
  long l;
  public float m = 1.0F;
  
  private ar() {}
  
  public ar(au paramau)
  {
    this.a = be.a.a();
    this.b = paramau.e;
    this.c = paramau.h();
    this.d = "5.8.1";
    this.e = Build.MODEL;
    this.f = Build.VERSION.RELEASE;
    this.g = paramau.b();
    this.h = paramau.c().intValue();
    this.i = paramau.d().intValue();
    this.j = paramau.a.a;
    this.k = paramau.i();
    this.l = System.nanoTime();
  }
  
  public final JSONObject a()
  {
    try
    {
      JSONObject localJSONObject = new JSONObject().putOpt("appID", this.b).putOpt("deviceID", this.c).putOpt("crPlatform", "android").putOpt("crVersion", this.d).putOpt("rate", Float.valueOf(this.m)).putOpt("deviceModel", this.e).putOpt("osName", "android").putOpt("osVersion", this.f).putOpt("carrier", this.g).putOpt("mobileCountryCode", Integer.valueOf(this.h)).putOpt("mobileNetworkCode", Integer.valueOf(this.i)).putOpt("appVersion", this.j).putOpt("locale", this.k);
      return localJSONObject;
    }
    catch (JSONException localJSONException) {}
    return null;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool2 = false;
    boolean bool1;
    if (this == paramObject) {
      bool1 = true;
    }
    do
    {
      do
      {
        do
        {
          do
          {
            do
            {
              do
              {
                do
                {
                  do
                  {
                    do
                    {
                      do
                      {
                        do
                        {
                          do
                          {
                            return bool1;
                            bool1 = bool2;
                          } while (paramObject == null);
                          bool1 = bool2;
                        } while (getClass() != paramObject.getClass());
                        paramObject = (ar)paramObject;
                        bool1 = bool2;
                      } while (this.h != ((ar)paramObject).h);
                      bool1 = bool2;
                    } while (this.i != ((ar)paramObject).i);
                    bool1 = bool2;
                  } while (!this.b.equals(((ar)paramObject).b));
                  bool1 = bool2;
                } while (!this.c.equals(((ar)paramObject).c));
                bool1 = bool2;
              } while (!this.d.equals(((ar)paramObject).d));
              bool1 = bool2;
            } while (Float.compare(this.m, ((ar)paramObject).m) != 0);
            bool1 = bool2;
          } while (!this.e.equals(((ar)paramObject).e));
          bool1 = bool2;
        } while (!this.f.equals(((ar)paramObject).f));
        bool1 = bool2;
      } while (!this.g.equals(((ar)paramObject).g));
      bool1 = bool2;
    } while (!this.j.equals(((ar)paramObject).j));
    return this.k.equals(((ar)paramObject).k);
  }
  
  public final String f()
  {
    return this.a;
  }
  
  public final int hashCode()
  {
    return (((((((((this.b.hashCode() * 31 + this.c.hashCode()) * 31 + this.d.hashCode()) * 31 + Float.floatToIntBits(this.m)) * 31 + this.e.hashCode()) * 31 + this.f.hashCode()) * 31 + this.g.hashCode()) * 31 + this.h) * 31 + this.i) * 31 + this.j.hashCode()) * 31 + this.k.hashCode();
  }
  
  public static final class a
    extends bw
  {
    public a(String paramString1, String paramString2)
    {
      super(paramString2);
    }
    
    public final bt a(as paramas, List<? extends bf> paramList)
    {
      HashMap localHashMap = new HashMap();
      Object localObject = null;
      Iterator localIterator = paramList.iterator();
      paramList = (List<? extends bf>)localObject;
      if (localIterator.hasNext())
      {
        localObject = (ar)localIterator.next();
        if (!localHashMap.containsKey(localObject)) {
          break label289;
        }
      }
      label289:
      for (int i = ((Integer)localHashMap.get(localObject)).intValue();; i = 0)
      {
        localHashMap.put(localObject, Integer.valueOf(i + 1));
        if (((ar)localObject).l > 0L) {
          paramList = (List<? extends bf>)localObject;
        }
        for (;;)
        {
          break;
          localObject = new JSONArray();
          for (;;)
          {
            try
            {
              localIterator = localHashMap.keySet().iterator();
              if (!localIterator.hasNext()) {
                break;
              }
              ar localar = (ar)localIterator.next();
              i = ((Integer)localHashMap.get(localar)).intValue();
              boolean bool;
              if ((paramList != null) && (paramList.equals(localar)))
              {
                bool = true;
                ((JSONArray)localObject).put(new JSONObject().put("appLoads", localar.a()).put("count", i).put("current", bool));
              }
              else
              {
                bool = false;
              }
            }
            catch (JSONException paramas)
            {
              throw new IOException(paramas.getMessage());
            }
          }
          paramas = new URL(paramas.d, "/v0/appload");
          paramList = this.a;
          return new bt("POST", paramas, ((JSONArray)localObject).toString().getBytes("UTF8"), "application/json", paramList);
        }
      }
    }
  }
  
  public static final class b
    implements aw.b<ar>
  {
    private static ar b(File paramFile)
    {
      try
      {
        paramFile = new JSONObject(ce.b(paramFile));
        ar localar = new ar((byte)0);
        localar.a = paramFile.getString("fileName");
        localar.b = paramFile.getString("appId");
        localar.c = paramFile.getString("deviceId");
        localar.d = paramFile.getString("sdkVersion");
        localar.m = ((float)paramFile.getDouble("rate"));
        localar.e = paramFile.getString("model");
        localar.f = paramFile.getString("osVersion");
        localar.g = paramFile.getString("carrier");
        localar.h = paramFile.getInt("mobileCountryCode");
        localar.i = paramFile.getInt("mobileNetworkCode");
        localar.j = paramFile.getString("appVersion");
        localar.k = paramFile.getString("locale");
        localar.l = paramFile.getLong("timestamp");
        return localar;
      }
      catch (JSONException paramFile)
      {
        throw new IOException(paramFile.getMessage());
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/ar.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */