package com.crittercism.internal;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public final class as
{
  private static final Map<String, String> e;
  public URL a;
  public URL b;
  public URL c;
  public URL d;
  
  static
  {
    HashMap localHashMap = new HashMap();
    e = localHashMap;
    localHashMap.put("00555300", "crittercism.com");
    e.put("00555304", "crit-ci.com");
    e.put("00555305", "crit-staging.com");
    e.put("00444503", "eu.crittercism.com");
  }
  
  public as(String paramString)
  {
    if (paramString == null) {
      throw new IllegalArgumentException("null App ID");
    }
    if (!paramString.matches("[0-9a-fA-F]+")) {
      throw new IllegalArgumentException("App ID must be hexadecimal characters");
    }
    if ((paramString.length() != 24) && (paramString.length() != 40)) {
      throw new IllegalArgumentException("App ID must be either 24 or 40 characters");
    }
    String str = null;
    if (paramString.length() == 24) {
      str = "00555300";
    }
    for (;;)
    {
      paramString = (String)e.get(str);
      if (paramString != null) {
        break;
      }
      throw new IllegalArgumentException("Invalid character sequence");
      if (paramString.length() == 40) {
        str = paramString.substring(paramString.length() - 8);
      }
    }
    try
    {
      this.a = new URL(System.getProperty("com.crittercism.apmUrl", "https://apm." + paramString));
      this.b = new URL(System.getProperty("com.crittercism.apiUrl", "https://api." + paramString));
      this.c = new URL(System.getProperty("com.crittercism.txnUrl", "https://txn.ingest." + paramString));
      this.d = new URL(System.getProperty("com.crittercism.appLoadUrl", "https://appload.ingest." + paramString));
      return;
    }
    catch (MalformedURLException paramString)
    {
      throw new IllegalArgumentException("Crittercism failed to initialize", paramString);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/as.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */