package com.crittercism.internal;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class at
  implements bf
{
  public String a;
  String b;
  public int c;
  Object d;
  
  public at(int paramInt, Object paramObject)
  {
    this(be.a.a(), cg.a.a(), paramInt, paramObject);
  }
  
  private at(String paramString1, String paramString2, int paramInt, Object paramObject)
  {
    this.a = paramString1;
    this.b = paramString2;
    this.c = paramInt;
    this.d = paramObject;
  }
  
  private at(Date paramDate, int paramInt)
  {
    this(be.a.a(), cg.a.a(paramDate), paramInt, null);
  }
  
  public static at a(int paramInt, String paramString)
  {
    if (paramInt != c.c) {
      int i = c.d;
    }
    HashMap localHashMap = new HashMap();
    localHashMap.put("change", Integer.valueOf(paramInt - 1));
    localHashMap.put("type", paramString);
    return new at(b.e, new JSONObject(localHashMap));
  }
  
  public static at a(String paramString)
  {
    String str = paramString;
    if (paramString.length() > 140) {
      str = paramString.substring(0, 140);
    }
    paramString = new HashMap();
    paramString.put("text", str);
    paramString.put("level", Integer.valueOf(0));
    return new at(b.b, new JSONObject(paramString));
  }
  
  public static at a(Date paramDate)
  {
    return new at(paramDate, b.a);
  }
  
  private JSONArray a()
  {
    JSONArray localJSONArray = new JSONArray().put(this.b).put(this.c - 1);
    if (this.d != null) {
      localJSONArray.put(this.d);
    }
    return localJSONArray;
  }
  
  public final String f()
  {
    return this.a;
  }
  
  public final String toString()
  {
    try
    {
      String str = a().toString(4);
      return str;
    }
    catch (JSONException localJSONException)
    {
      return localJSONException.toString();
    }
  }
  
  public static final class a
    implements aw.b<at>
  {
    /* Error */
    private static at b(java.io.File paramFile)
    {
      // Byte code:
      //   0: aload_0
      //   1: invokestatic 28	com/crittercism/internal/ce:b	(Ljava/io/File;)Ljava/lang/String;
      //   4: astore_0
      //   5: new 30	org/json/JSONObject
      //   8: dup
      //   9: aload_0
      //   10: invokespecial 33	org/json/JSONObject:<init>	(Ljava/lang/String;)V
      //   13: astore_0
      //   14: aload_0
      //   15: ldc 35
      //   17: invokevirtual 39	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
      //   20: astore_2
      //   21: aload_0
      //   22: ldc 41
      //   24: invokevirtual 39	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
      //   27: astore_3
      //   28: invokestatic 46	com/crittercism/internal/at$b:a	()[I
      //   31: aload_0
      //   32: ldc 48
      //   34: invokevirtual 52	org/json/JSONObject:getInt	(Ljava/lang/String;)I
      //   37: iaload
      //   38: istore_1
      //   39: aload_0
      //   40: ldc 54
      //   42: invokevirtual 58	org/json/JSONObject:has	(Ljava/lang/String;)Z
      //   45: ifeq +25 -> 70
      //   48: aload_0
      //   49: ldc 54
      //   51: invokevirtual 62	org/json/JSONObject:get	(Ljava/lang/String;)Ljava/lang/Object;
      //   54: astore_0
      //   55: new 9	com/crittercism/internal/at
      //   58: dup
      //   59: aload_2
      //   60: aload_3
      //   61: iload_1
      //   62: aload_0
      //   63: iconst_0
      //   64: invokespecial 65	com/crittercism/internal/at:<init>	(Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;B)V
      //   67: astore_0
      //   68: aload_0
      //   69: areturn
      //   70: aconst_null
      //   71: astore_0
      //   72: goto -17 -> 55
      //   75: astore_0
      //   76: new 67	java/io/IOException
      //   79: dup
      //   80: aload_0
      //   81: invokevirtual 71	org/json/JSONException:getMessage	()Ljava/lang/String;
      //   84: invokespecial 72	java/io/IOException:<init>	(Ljava/lang/String;)V
      //   87: athrow
      //   88: astore_0
      //   89: new 67	java/io/IOException
      //   92: dup
      //   93: aload_0
      //   94: invokevirtual 73	java/lang/IndexOutOfBoundsException:getMessage	()Ljava/lang/String;
      //   97: invokespecial 72	java/io/IOException:<init>	(Ljava/lang/String;)V
      //   100: athrow
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	101	0	paramFile	java.io.File
      //   38	24	1	i	int
      //   20	40	2	str1	String
      //   27	34	3	str2	String
      // Exception table:
      //   from	to	target	type
      //   5	55	75	org/json/JSONException
      //   55	68	75	org/json/JSONException
      //   5	55	88	java/lang/IndexOutOfBoundsException
      //   55	68	88	java/lang/IndexOutOfBoundsException
    }
  }
  
  public static enum b
  {
    public static int[] a()
    {
      return (int[])i.clone();
    }
  }
  
  public static enum c {}
  
  public static enum d {}
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/at.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */