package com.crittercism.internal;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build.VERSION;
import android.os.Environment;
import android.os.StatFs;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import java.io.File;
import java.math.BigInteger;
import java.util.Locale;
import org.json.JSONException;
import org.json.JSONObject;

public final class au
{
  public ak a;
  Context b;
  ao c;
  ax d;
  public String e;
  public ca f;
  public String g;
  private bx h;
  private a i;
  
  public au(ak paramak, Context paramContext, ao paramao, String paramString)
  {
    this.a = paramak;
    this.b = paramContext;
    this.c = paramao;
    this.d = new ay();
    this.e = paramString;
    this.h = new bx(paramContext);
    this.f = new ca(paramContext);
    if (Build.VERSION.SDK_INT >= 18)
    {
      this.i = new b((byte)0);
      return;
    }
    this.i = new c((byte)0);
  }
  
  public static Long l()
  {
    return Long.valueOf(Runtime.getRuntime().maxMemory());
  }
  
  public final Integer a()
  {
    return Integer.valueOf(this.a.b);
  }
  
  final JSONObject a(int paramInt)
  {
    Object localObject;
    if (!this.c.b) {
      localObject = null;
    }
    do
    {
      for (;;)
      {
        return (JSONObject)localObject;
        if (!ConnectivityManager.isNetworkTypeValid(paramInt)) {
          return null;
        }
        NetworkInfo localNetworkInfo = ((ConnectivityManager)this.b.getSystemService("connectivity")).getNetworkInfo(paramInt);
        JSONObject localJSONObject = new JSONObject();
        if (localNetworkInfo != null) {}
        try
        {
          localJSONObject.put("available", localNetworkInfo.isAvailable());
          localJSONObject.put("connected", localNetworkInfo.isConnected());
          if (!localNetworkInfo.isConnected()) {
            localJSONObject.put("connecting", localNetworkInfo.isConnectedOrConnecting());
          }
          localJSONObject.put("failover", localNetworkInfo.isFailover());
          localObject = localJSONObject;
          if (paramInt == 0)
          {
            localJSONObject.put("roaming", localNetworkInfo.isRoaming());
            return localJSONObject;
          }
        }
        catch (JSONException localJSONException) {}
      }
      localJSONObject.put("available", false);
      localJSONObject.put("connected", false);
      localJSONObject.put("connecting", false);
      localJSONObject.put("failover", false);
      localObject = localJSONObject;
    } while (paramInt != 0);
    localJSONObject.put("roaming", false);
    return localJSONObject;
    return null;
  }
  
  public final String b()
  {
    try
    {
      String str = ((TelephonyManager)this.b.getSystemService("phone")).getNetworkOperatorName();
      return str;
    }
    catch (Exception localException) {}
    return "";
  }
  
  public final Integer c()
  {
    try
    {
      String str = ((TelephonyManager)this.b.getSystemService("phone")).getNetworkOperator();
      if (str == null) {
        break label36;
      }
      j = Integer.parseInt(str.substring(0, 3));
    }
    catch (Exception localException)
    {
      for (;;)
      {
        label36:
        int j = 0;
      }
    }
    return Integer.valueOf(j);
  }
  
  public final Integer d()
  {
    try
    {
      String str = ((TelephonyManager)this.b.getSystemService("phone")).getNetworkOperator();
      if (str != null)
      {
        int j = Integer.parseInt(str.substring(3));
        return Integer.valueOf(j);
      }
    }
    catch (Exception localException) {}
    return Integer.valueOf(0);
  }
  
  public final Float e()
  {
    return Float.valueOf(this.b.getResources().getDisplayMetrics().density);
  }
  
  public final float f()
  {
    return this.b.getResources().getDisplayMetrics().xdpi;
  }
  
  public final float g()
  {
    return this.b.getResources().getDisplayMetrics().ydpi;
  }
  
  public final String h()
  {
    String str = "";
    if (this.h != null) {
      str = this.h.a();
    }
    return str;
  }
  
  public final String i()
  {
    String str2 = this.b.getResources().getConfiguration().locale.getLanguage();
    String str1;
    if (str2 != null)
    {
      str1 = str2;
      if (str2.length() != 0) {}
    }
    else
    {
      str1 = "en";
    }
    return str1;
  }
  
  public final String j()
  {
    try
    {
      String str = this.i.b();
      return str;
    }
    catch (ThreadDeath localThreadDeath)
    {
      throw localThreadDeath;
    }
    catch (Throwable localThrowable) {}
    return null;
  }
  
  public final String k()
  {
    try
    {
      String str = this.i.a();
      return str;
    }
    catch (ThreadDeath localThreadDeath)
    {
      throw localThreadDeath;
    }
    catch (Throwable localThrowable) {}
    return null;
  }
  
  public final String m()
  {
    try
    {
      String str = this.i.d();
      return str;
    }
    catch (ThreadDeath localThreadDeath)
    {
      throw localThreadDeath;
    }
    catch (Throwable localThrowable) {}
    return null;
  }
  
  public final String n()
  {
    try
    {
      String str = this.i.c();
      return str;
    }
    catch (ThreadDeath localThreadDeath)
    {
      throw localThreadDeath;
    }
    catch (Throwable localThrowable) {}
    return null;
  }
  
  static abstract interface a
  {
    public abstract String a();
    
    public abstract String b();
    
    public abstract String c();
    
    public abstract String d();
  }
  
  static final class b
    implements au.a
  {
    public final String a()
    {
      int i = Build.VERSION.SDK_INT;
      if (Build.VERSION.SDK_INT < 18) {
        return null;
      }
      StatFs localStatFs = new StatFs(Environment.getDataDirectory().getPath());
      return BigInteger.valueOf(localStatFs.getBlockCountLong()).multiply(BigInteger.valueOf(localStatFs.getBlockSizeLong())).toString();
    }
    
    public final String b()
    {
      int i = Build.VERSION.SDK_INT;
      if (Build.VERSION.SDK_INT < 18) {
        return null;
      }
      StatFs localStatFs = new StatFs(Environment.getDataDirectory().getPath());
      return BigInteger.valueOf(localStatFs.getAvailableBlocksLong()).multiply(BigInteger.valueOf(localStatFs.getBlockSizeLong())).toString();
    }
    
    public final String c()
    {
      int i = Build.VERSION.SDK_INT;
      if (Build.VERSION.SDK_INT < 18) {
        return null;
      }
      StatFs localStatFs = new StatFs(Environment.getExternalStorageDirectory().getPath());
      return BigInteger.valueOf(localStatFs.getBlockCountLong()).multiply(BigInteger.valueOf(localStatFs.getBlockSizeLong())).toString();
    }
    
    public final String d()
    {
      int i = Build.VERSION.SDK_INT;
      if (Build.VERSION.SDK_INT < 18) {
        return null;
      }
      StatFs localStatFs = new StatFs(Environment.getExternalStorageDirectory().getPath());
      return BigInteger.valueOf(localStatFs.getAvailableBlocksLong()).multiply(BigInteger.valueOf(localStatFs.getBlockSizeLong())).toString();
    }
  }
  
  static final class c
    implements au.a
  {
    public final String a()
    {
      int i = Build.VERSION.SDK_INT;
      if (Build.VERSION.SDK_INT >= 18) {
        return null;
      }
      StatFs localStatFs = new StatFs(Environment.getDataDirectory().getPath());
      return BigInteger.valueOf(localStatFs.getBlockCount()).multiply(BigInteger.valueOf(localStatFs.getBlockSize())).toString();
    }
    
    public final String b()
    {
      int i = Build.VERSION.SDK_INT;
      if (Build.VERSION.SDK_INT >= 18) {
        return null;
      }
      StatFs localStatFs = new StatFs(Environment.getDataDirectory().getPath());
      return BigInteger.valueOf(localStatFs.getAvailableBlocks()).multiply(BigInteger.valueOf(localStatFs.getBlockSize())).toString();
    }
    
    public final String c()
    {
      int i = Build.VERSION.SDK_INT;
      if (Build.VERSION.SDK_INT >= 18) {
        return null;
      }
      StatFs localStatFs = new StatFs(Environment.getExternalStorageDirectory().getPath());
      return BigInteger.valueOf(localStatFs.getBlockCount()).multiply(BigInteger.valueOf(localStatFs.getBlockSize())).toString();
    }
    
    public final String d()
    {
      int i = Build.VERSION.SDK_INT;
      if (Build.VERSION.SDK_INT >= 18) {
        return null;
      }
      StatFs localStatFs = new StatFs(Environment.getExternalStorageDirectory().getPath());
      return BigInteger.valueOf(localStatFs.getAvailableBlocks()).multiply(BigInteger.valueOf(localStatFs.getBlockSize())).toString();
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/au.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */