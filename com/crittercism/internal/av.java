package com.crittercism.internal;

import java.util.List;
import org.json.JSONArray;

public abstract interface av<T extends bf>
{
  public abstract JSONArray a();
  
  public abstract void a(a parama);
  
  public abstract void a(String paramString);
  
  public abstract void a(List<T> paramList);
  
  public abstract boolean a(T paramT);
  
  public abstract List<T> b();
  
  public abstract List<T> c();
  
  public static abstract interface a
  {
    public abstract void a();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/av.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */