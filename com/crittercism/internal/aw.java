package com.crittercism.internal;

import android.content.Context;
import android.support.annotation.NonNull;
import java.io.File;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.json.JSONArray;

public final class aw<T extends bf>
  implements av<T>
{
  private final a a;
  private b<T> b;
  private int c;
  private final List<av.a> d = new LinkedList();
  
  public aw(Context paramContext, String paramString, b<T> paramb, int paramInt)
  {
    this.b = paramb;
    this.c = paramInt;
    this.a = new a(paramContext, paramString);
  }
  
  public static File a(Context paramContext, String paramString)
  {
    return new File(paramContext.getFilesDir() + "/com.crittercism/" + paramString);
  }
  
  /* Error */
  private boolean b(T paramT)
  {
    // Byte code:
    //   0: new 46	java/io/File
    //   3: dup
    //   4: aload_0
    //   5: getfield 40	com/crittercism/internal/aw:a	Lcom/crittercism/internal/aw$a;
    //   8: invokevirtual 80	com/crittercism/internal/aw$a:a	()Ljava/io/File;
    //   11: aload_1
    //   12: invokeinterface 85 1 0
    //   17: invokespecial 88	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   20: astore_2
    //   21: new 90	java/io/BufferedOutputStream
    //   24: dup
    //   25: new 92	java/io/FileOutputStream
    //   28: dup
    //   29: aload_2
    //   30: invokespecial 95	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   33: invokespecial 98	java/io/BufferedOutputStream:<init>	(Ljava/io/OutputStream;)V
    //   36: astore_3
    //   37: aload_0
    //   38: getfield 33	com/crittercism/internal/aw:b	Lcom/crittercism/internal/aw$b;
    //   41: aload_1
    //   42: aload_3
    //   43: invokeinterface 101 3 0
    //   48: aload_3
    //   49: invokevirtual 106	java/io/OutputStream:close	()V
    //   52: iconst_1
    //   53: ireturn
    //   54: astore_1
    //   55: new 48	java/lang/StringBuilder
    //   58: dup
    //   59: ldc 108
    //   61: invokespecial 109	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   64: aload_2
    //   65: invokevirtual 59	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   68: invokevirtual 68	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   71: aload_1
    //   72: invokestatic 114	com/crittercism/internal/cd:c	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   75: iconst_0
    //   76: ireturn
    //   77: astore_1
    //   78: aload_2
    //   79: invokevirtual 118	java/io/File:delete	()Z
    //   82: pop
    //   83: new 48	java/lang/StringBuilder
    //   86: dup
    //   87: ldc 120
    //   89: invokespecial 109	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   92: aload_2
    //   93: invokevirtual 123	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   96: invokevirtual 64	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   99: invokevirtual 68	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   102: aload_1
    //   103: invokestatic 125	com/crittercism/internal/cd:a	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   106: iconst_0
    //   107: ireturn
    //   108: astore_1
    //   109: aload_2
    //   110: invokevirtual 118	java/io/File:delete	()Z
    //   113: pop
    //   114: new 48	java/lang/StringBuilder
    //   117: dup
    //   118: ldc 127
    //   120: invokespecial 109	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   123: aload_2
    //   124: invokevirtual 123	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   127: invokevirtual 64	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   130: invokevirtual 68	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   133: aload_1
    //   134: invokestatic 125	com/crittercism/internal/cd:a	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   137: aload_3
    //   138: invokevirtual 106	java/io/OutputStream:close	()V
    //   141: iconst_0
    //   142: ireturn
    //   143: astore_1
    //   144: aload_2
    //   145: invokevirtual 118	java/io/File:delete	()Z
    //   148: pop
    //   149: new 48	java/lang/StringBuilder
    //   152: dup
    //   153: ldc 120
    //   155: invokespecial 109	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   158: aload_2
    //   159: invokevirtual 123	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   162: invokevirtual 64	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   165: invokevirtual 68	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   168: aload_1
    //   169: invokestatic 125	com/crittercism/internal/cd:a	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   172: iconst_0
    //   173: ireturn
    //   174: astore_1
    //   175: aload_3
    //   176: invokevirtual 106	java/io/OutputStream:close	()V
    //   179: aload_1
    //   180: athrow
    //   181: astore_1
    //   182: aload_2
    //   183: invokevirtual 118	java/io/File:delete	()Z
    //   186: pop
    //   187: new 48	java/lang/StringBuilder
    //   190: dup
    //   191: ldc 120
    //   193: invokespecial 109	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   196: aload_2
    //   197: invokevirtual 123	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   200: invokevirtual 64	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   203: invokevirtual 68	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   206: aload_1
    //   207: invokestatic 125	com/crittercism/internal/cd:a	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   210: iconst_0
    //   211: ireturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	212	0	this	aw
    //   0	212	1	paramT	T
    //   20	177	2	localFile	File
    //   36	140	3	localBufferedOutputStream	java.io.BufferedOutputStream
    // Exception table:
    //   from	to	target	type
    //   21	37	54	java/io/FileNotFoundException
    //   48	52	77	java/io/IOException
    //   37	48	108	java/lang/Exception
    //   137	141	143	java/io/IOException
    //   37	48	174	finally
    //   109	137	174	finally
    //   175	179	181	java/io/IOException
  }
  
  private File[] d()
  {
    File[] arrayOfFile = e();
    Arrays.sort(arrayOfFile);
    return arrayOfFile;
  }
  
  private File[] e()
  {
    File[] arrayOfFile2 = this.a.a().listFiles();
    File[] arrayOfFile1 = arrayOfFile2;
    if (arrayOfFile2 == null) {
      arrayOfFile1 = new File[0];
    }
    return arrayOfFile1;
  }
  
  public final JSONArray a()
  {
    Object localObject = b();
    JSONArray localJSONArray = new JSONArray();
    localObject = ((List)localObject).iterator();
    while (((Iterator)localObject).hasNext()) {
      localJSONArray.put(((bf)((Iterator)localObject).next()).g());
    }
    return localJSONArray;
  }
  
  public final void a(av.a parama)
  {
    if (parama != null) {
      synchronized (this.d)
      {
        this.d.add(parama);
        return;
      }
    }
  }
  
  public final void a(@NonNull String paramString)
  {
    paramString = new File(this.a.a(), paramString);
    if (paramString.exists()) {
      paramString.delete();
    }
  }
  
  public final void a(List<T> paramList)
  {
    paramList = paramList.iterator();
    while (paramList.hasNext()) {
      a(((bf)paramList.next()).f());
    }
  }
  
  public final boolean a(T arg1)
  {
    Object localObject1;
    if (e().length >= this.c)
    {
      localObject1 = d();
      int m = this.c - localObject1.length + 1;
      int i = 0;
      int k;
      for (int j = 0; (i < m) && (i < localObject1.length); j = k)
      {
        k = j;
        if (localObject1[i].delete()) {
          k = j + 1;
        }
        i += 1;
      }
      if (j == m) {}
      for (i = 1; i == 0; i = 0) {
        return false;
      }
    }
    boolean bool = b(???);
    synchronized (this.d)
    {
      localObject1 = this.d.iterator();
      if (((Iterator)localObject1).hasNext()) {
        ((av.a)((Iterator)localObject1).next()).a();
      }
    }
    return bool;
  }
  
  public final List<T> b()
  {
    ArrayList localArrayList = new ArrayList();
    File[] arrayOfFile = d();
    int j = arrayOfFile.length;
    int i = 0;
    for (;;)
    {
      if (i < j)
      {
        File localFile = arrayOfFile[i];
        try
        {
          localArrayList.add(this.b.a(localFile));
          i += 1;
        }
        catch (ThreadDeath localThreadDeath)
        {
          throw localThreadDeath;
        }
        catch (Throwable localThrowable)
        {
          for (;;)
          {
            a(localFile.getName());
          }
        }
      }
    }
    return localThreadDeath;
  }
  
  public final List<T> c()
  {
    return b();
  }
  
  static final class a
  {
    private Context a;
    private String b;
    private File c;
    
    public a(Context paramContext, String paramString)
    {
      this.a = paramContext;
      this.b = paramString;
    }
    
    public final File a()
    {
      if (this.c != null) {
        return this.c;
      }
      this.c = aw.a(this.a, this.b);
      if (!this.c.isDirectory()) {
        this.c.mkdirs();
      }
      return this.c;
    }
  }
  
  public static abstract interface b<T extends bf>
  {
    public abstract T a(File paramFile);
    
    public abstract void a(T paramT, OutputStream paramOutputStream);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/aw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */