package com.crittercism.internal;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;
import org.json.JSONArray;

public final class az<T extends bf>
  implements av<T>
{
  private final SortedMap<String, T> a = new TreeMap();
  private final List<av.a> b = new LinkedList();
  private int c;
  
  public az(int paramInt)
  {
    this.c = paramInt;
  }
  
  public final JSONArray a()
  {
    JSONArray localJSONArray = new JSONArray();
    Iterator localIterator = b().iterator();
    while (localIterator.hasNext()) {
      localJSONArray.put(((bf)localIterator.next()).g());
    }
    return localJSONArray;
  }
  
  public final void a(av.a parama)
  {
    synchronized (this.b)
    {
      this.b.add(parama);
      return;
    }
  }
  
  public final void a(String paramString)
  {
    this.a.remove(paramString);
  }
  
  public final void a(List<T> paramList)
  {
    paramList = paramList.iterator();
    while (paramList.hasNext())
    {
      bf localbf = (bf)paramList.next();
      this.a.remove(localbf.f());
    }
  }
  
  public final boolean a(T arg1)
  {
    try
    {
      if (this.a.size() >= this.c) {
        this.a.remove(this.a.firstKey());
      }
      this.a.put(???.f(), ???);
      synchronized (this.b)
      {
        Iterator localIterator = this.b.iterator();
        if (localIterator.hasNext()) {
          ((av.a)localIterator.next()).a();
        }
      }
    }
    finally {}
    return true;
  }
  
  public final List<T> b()
  {
    return new LinkedList(this.a.values());
  }
  
  public final List<T> c()
  {
    return b();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/az.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */