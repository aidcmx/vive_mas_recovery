package com.crittercism.internal;

import android.content.SharedPreferences;
import android.location.Location;
import android.os.Build;
import android.os.Build.VERSION;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.URL;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class b
  implements bf
{
  public long a = Long.MAX_VALUE;
  long b = Long.MAX_VALUE;
  boolean c = false;
  String d = be.a.a();
  float e = 1.0F;
  c f = c.a;
  long g = 0L;
  public long h = 0L;
  public int i = 0;
  public String j = "";
  public bj k = new bj(null);
  double[] l;
  public f m = new f();
  public String n;
  public a o = a.a;
  private boolean p = false;
  private boolean q = false;
  private boolean r = false;
  private boolean s = false;
  
  public b() {}
  
  public b(String paramString)
  {
    if (paramString != null) {
      this.n = paramString;
    }
  }
  
  public final String a()
  {
    int i2 = 1;
    String str1 = this.n;
    Object localObject1 = str1;
    Object localObject2;
    if (str1 == null)
    {
      localObject2 = this.m;
      if (((f)localObject2).b == null) {
        break label117;
      }
      str1 = ((f)localObject2).b;
    }
    for (;;)
    {
      int i1;
      String str2;
      if (((f)localObject2).f)
      {
        i1 = ((f)localObject2).e;
        localObject1 = str1;
        if (i1 > 0)
        {
          str2 = ":" + i1;
          localObject1 = str1;
          if (!str1.endsWith(str2)) {
            localObject1 = str1 + str2;
          }
        }
        this.n = ((String)localObject1);
        return (String)localObject1;
        label117:
        if (((f)localObject2).a != null) {
          str1 = ((f)localObject2).a.getHostName();
        }
      }
      else
      {
        str2 = ((f)localObject2).c;
        if (str2 != null)
        {
          i1 = i2;
          if (!str2.regionMatches(true, 0, "http:", 0, 5)) {
            if (!str2.regionMatches(true, 0, "https:", 0, 6)) {
              break label193;
            }
          }
        }
        label193:
        for (i1 = i2;; i1 = 0)
        {
          if (i1 == 0) {
            break label198;
          }
          localObject1 = str2;
          break;
        }
        label198:
        if (((f)localObject2).d != null) {}
        for (localObject1 = "" + f.a.a(((f)localObject2).d) + ":";; localObject1 = "")
        {
          if (str2.startsWith("//"))
          {
            localObject1 = (String)localObject1 + str2;
            break;
          }
          String str4 = (String)localObject1 + "//";
          if (str2.startsWith(str1))
          {
            localObject1 = str4 + str2;
            break;
          }
          String str3 = "";
          localObject1 = str3;
          if (((f)localObject2).e > 0) {
            if (((f)localObject2).d != null)
            {
              localObject1 = str3;
              if (f.a.b(((f)localObject2).d) == ((f)localObject2).e) {}
            }
            else
            {
              localObject2 = ":" + ((f)localObject2).e;
              localObject1 = str3;
              if (!str1.endsWith((String)localObject2)) {
                localObject1 = localObject2;
              }
            }
          }
          localObject1 = str4 + str1 + (String)localObject1 + str2;
          break;
        }
      }
      str1 = "unknown-host";
    }
  }
  
  public final void a(long paramLong)
  {
    this.r = true;
    this.g = paramLong;
  }
  
  public final void a(Location paramLocation)
  {
    this.l = new double[] { paramLocation.getLatitude(), paramLocation.getLongitude() };
  }
  
  public final void a(String paramString)
  {
    if (paramString == null) {
      throw new NullPointerException();
    }
    this.n = paramString;
  }
  
  public final void a(Throwable paramThrowable)
  {
    this.k = new bj(paramThrowable);
  }
  
  public final long b()
  {
    long l2 = Long.MAX_VALUE;
    long l1 = l2;
    if (this.a != Long.MAX_VALUE)
    {
      l1 = l2;
      if (this.b != Long.MAX_VALUE) {
        l1 = this.b - this.a;
      }
    }
    return l1;
  }
  
  public final void b(long paramLong)
  {
    this.s = true;
    this.h = paramLong;
  }
  
  public final void b(String paramString)
  {
    this.n = null;
    this.m.b = paramString;
  }
  
  public final void c()
  {
    if ((!this.p) && (this.a == Long.MAX_VALUE)) {
      this.a = System.currentTimeMillis();
    }
  }
  
  public final void c(long paramLong)
  {
    this.a = paramLong;
    this.p = true;
  }
  
  public final void d()
  {
    if ((!this.q) && (this.b == Long.MAX_VALUE)) {
      this.b = System.currentTimeMillis();
    }
  }
  
  public final void d(long paramLong)
  {
    this.b = paramLong;
    this.q = true;
  }
  
  public final JSONArray e()
  {
    JSONArray localJSONArray1 = new JSONArray();
    try
    {
      localJSONArray1.put(this.j);
      localJSONArray1.put(a());
      localJSONArray1.put(cg.a.a(new Date(this.a)));
      localJSONArray1.put(b());
      localJSONArray1.put(this.o.e);
      localJSONArray1.put(this.g);
      localJSONArray1.put(this.h);
      localJSONArray1.put(this.i);
      localJSONArray1.put(this.k.a);
      localJSONArray1.put(this.k.b);
      if (this.l != null)
      {
        JSONArray localJSONArray2 = new JSONArray();
        localJSONArray2.put(this.l[0]);
        localJSONArray2.put(this.l[1]);
        localJSONArray1.put(localJSONArray2);
      }
      return localJSONArray1;
    }
    catch (Exception localException)
    {
      System.out.println("Failed to create statsArray");
      localException.printStackTrace();
    }
    return null;
  }
  
  public final String f()
  {
    return this.d;
  }
  
  public final String toString()
  {
    Object localObject = "" + "URI            : " + this.n + "\n";
    localObject = (String)localObject + "URI Builder    : " + this.m.toString() + "\n";
    localObject = (String)localObject + "\n";
    localObject = (String)localObject + "Logged by      : " + this.f.toString() + "\n";
    localObject = (String)localObject + "Error type:         : " + this.k.a + "\n";
    localObject = (String)localObject + "Error code:         : " + this.k.b + "\n";
    localObject = (String)localObject + "\n";
    localObject = (String)localObject + "Response time  : " + b() + "\n";
    localObject = (String)localObject + "Start time     : " + this.a + "\n";
    localObject = (String)localObject + "End time       : " + this.b + "\n";
    localObject = (String)localObject + "\n";
    localObject = (String)localObject + "Bytes out    : " + this.h + "\n";
    localObject = (String)localObject + "Bytes in     : " + this.g + "\n";
    localObject = (String)localObject + "\n";
    localObject = (String)localObject + "Response code  : " + this.i + "\n";
    String str = (String)localObject + "Request method : " + this.j + "\n";
    localObject = str;
    if (this.l != null) {
      localObject = str + "Location       : " + Arrays.toString(this.l) + "\n";
    }
    return (String)localObject;
  }
  
  public static final class a
    extends bw
  {
    private au c;
    
    public a(au paramau)
    {
      super(paramau.h());
      this.c = paramau;
    }
    
    public final bt a(as paramas, List<? extends bf> paramList)
    {
      if (paramList.size() == 0) {
        throw new IOException("No events provided");
      }
      JSONObject localJSONObject = new JSONObject();
      JSONArray localJSONArray1 = new JSONArray();
      JSONArray localJSONArray2 = new JSONArray();
      localJSONArray2.put(this.c.e);
      localJSONArray2.put(this.c.a.a);
      localJSONArray2.put(this.c.h());
      localJSONArray2.put("5.8.1");
      localJSONArray2.put(this.c.f.a.getInt("sessionIDSetting", 0));
      localJSONArray1.put(localJSONArray2);
      localJSONArray2 = new JSONArray();
      localJSONArray2.put(cg.a.a());
      localJSONArray2.put(this.c.b());
      localJSONArray2.put(Build.MODEL);
      localJSONArray2.put("Android");
      localJSONArray2.put(Build.VERSION.RELEASE);
      localJSONArray2.put(this.c.c());
      localJSONArray2.put(this.c.d());
      localJSONArray1.put(localJSONArray2);
      localJSONArray2 = new JSONArray();
      paramList = paramList.iterator();
      while (paramList.hasNext()) {
        localJSONArray2.put(((b)paramList.next()).e());
      }
      localJSONArray1.put(localJSONArray2);
      try
      {
        localJSONObject.put("d", localJSONArray1);
        return bt.a(new URL(paramas.a, "/api/apm/network"), this.a, localJSONObject);
      }
      catch (JSONException paramas)
      {
        throw new IOException(paramas.getMessage());
      }
    }
  }
  
  public static final class b
    implements aw.b<b>
  {
    private static b b(File paramFile)
    {
      Object localObject = ce.b(paramFile);
      paramFile = new b();
      try
      {
        localObject = new JSONObject((String)localObject);
        paramFile.d = ((JSONObject)localObject).getString("Sequence Number");
        paramFile.j = ((JSONObject)localObject).getString("Request Method");
        paramFile.n = ((JSONObject)localObject).getString("Uri");
        paramFile.a = cg.a.a(((JSONObject)localObject).getString("Time Stamp"));
        paramFile.b = (paramFile.a + ((JSONObject)localObject).getLong("Response Time"));
        paramFile.o = a.a(((JSONObject)localObject).getInt("Network Status"));
        paramFile.g = ((JSONObject)localObject).getLong("Bytes In");
        paramFile.h = ((JSONObject)localObject).getLong("Bytes Out");
        paramFile.i = ((JSONObject)localObject).getInt("Return Code");
        paramFile.k = new bj(((JSONObject)localObject).getInt("Error Type"), ((JSONObject)localObject).getInt("Error Code"));
        if (((JSONObject)localObject).has("Location"))
        {
          JSONArray localJSONArray = ((JSONObject)localObject).getJSONArray("Location");
          paramFile.l = new double[] { localJSONArray.getDouble(0), localJSONArray.getDouble(1) };
        }
        paramFile.e = ((float)((JSONObject)localObject).getDouble("rate"));
        return paramFile;
      }
      catch (JSONException paramFile)
      {
        throw new IOException(paramFile.getMessage());
      }
      catch (ParseException paramFile)
      {
        throw new IOException(paramFile.getMessage());
      }
    }
  }
  
  public static enum c
  {
    private String o;
    
    private c(String paramString)
    {
      this.o = paramString;
    }
    
    public final String toString()
    {
      return this.o;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */