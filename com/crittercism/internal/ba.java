package com.crittercism.internal;

import java.io.IOException;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public final class ba
  implements bf
{
  String a;
  String b;
  
  public ba(String paramString1, String paramString2)
  {
    this.a = paramString1;
    this.b = paramString2;
  }
  
  public final String f()
  {
    return this.a;
  }
  
  public final Object g()
  {
    throw new UnsupportedOperationException();
  }
  
  public static final class a
    extends bw
  {
    public a(String paramString1, String paramString2)
    {
      super(paramString2);
    }
    
    public final bt a(as paramas, List<? extends bf> paramList)
    {
      paramas = new URL(paramas.b, "/android_v2/update_user_metadata");
      JSONObject localJSONObject1;
      try
      {
        localJSONObject1 = new JSONObject();
        localJSONObject1.put("app_id", this.a);
        localJSONObject1.put("hashed_device_id", this.b);
        localJSONObject1.put("library_version", "5.8.1");
        JSONObject localJSONObject2 = new JSONObject();
        paramList = paramList.iterator();
        while (paramList.hasNext())
        {
          ba localba = (ba)paramList.next();
          localJSONObject2.put(localba.a, localba.b);
        }
        localJSONObject1.put("metadata", localJSONObject2);
      }
      catch (JSONException paramas)
      {
        throw new IOException(paramas.getMessage());
      }
      paramas = bt.a(paramas, this.a, localJSONObject1);
      return paramas;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/ba.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */