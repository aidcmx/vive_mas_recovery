package com.crittercism.internal;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class bb
  implements av<ba>
{
  private SharedPreferences a;
  private final List<av.a> b = new LinkedList();
  
  public bb(Context paramContext, String paramString)
  {
    this.a = paramContext.getSharedPreferences("com.crittercism." + paramString + ".usermetadata.v2", 0);
  }
  
  private boolean a(ba paramba)
  {
    String str1 = this.a.getString(paramba.a, null);
    String str2 = paramba.a;
    String str3 = paramba.a;
    str3 = "__timestamp_" + str3;
    if (paramba.b.equals(str1))
    {
      this.a.edit().putLong(str3, System.nanoTime()).commit();
      return true;
    }
    if (((this.a.getAll().size() - 1) / 2 >= 25) && (!this.a.contains(paramba.a)) && (!e())) {
      return false;
    }
    boolean bool = this.a.edit().putString(str2, paramba.b).putLong(str3, System.nanoTime()).putBoolean("dirty", true).commit();
    if (bool)
    {
      paramba = this.b.iterator();
      while (paramba.hasNext()) {
        ((av.a)paramba.next()).a();
      }
    }
    return bool;
  }
  
  private boolean d()
  {
    return this.a.getBoolean("dirty", false);
  }
  
  private boolean e()
  {
    Object localObject2 = this.a.getAll();
    Object localObject1 = null;
    long l = Long.MAX_VALUE;
    Iterator localIterator = ((Map)localObject2).keySet().iterator();
    while (localIterator.hasNext())
    {
      localObject2 = (String)localIterator.next();
      if (((String)localObject2).startsWith("__timestamp_"))
      {
        if ((localObject1 != null) && (l <= this.a.getLong((String)localObject2, l))) {
          break label153;
        }
        l = this.a.getLong((String)localObject2, l);
        localObject1 = localObject2;
      }
    }
    label153:
    for (;;)
    {
      break;
      boolean bool = false;
      if (localObject1 != null)
      {
        localObject2 = ((String)localObject1).substring(12);
        bool = this.a.edit().remove((String)localObject1).remove((String)localObject2).commit();
      }
      return bool;
    }
  }
  
  public final JSONArray a()
  {
    JSONArray localJSONArray = new JSONArray();
    Iterator localIterator = b().iterator();
    if (localIterator.hasNext())
    {
      localIterator.next();
      throw new UnsupportedOperationException();
    }
    return localJSONArray;
  }
  
  public final void a(av.a parama)
  {
    synchronized (this.b)
    {
      this.b.add(parama);
      if (d()) {
        parama.a();
      }
      return;
    }
  }
  
  public final void a(String paramString)
  {
    throw new UnsupportedOperationException();
  }
  
  public final void a(List<ba> paramList)
  {
    this.a.edit().putBoolean("dirty", false).commit();
  }
  
  public final boolean a(JSONObject paramJSONObject)
  {
    Iterator localIterator = paramJSONObject.keys();
    boolean bool1 = true;
    for (;;)
    {
      if (localIterator.hasNext()) {}
      try
      {
        String str = (String)localIterator.next();
        boolean bool2 = a(new ba(str, paramJSONObject.get(str).toString()));
        bool1 = bool2 & bool1;
      }
      catch (JSONException localJSONException) {}
      return bool1;
    }
  }
  
  public final List<ba> b()
  {
    Map localMap = this.a.getAll();
    LinkedList localLinkedList = new LinkedList();
    Iterator localIterator = localMap.keySet().iterator();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      if ((!str.startsWith("__timestamp_")) && (!str.equals("dirty"))) {
        localLinkedList.add(new ba(str, (String)localMap.get(str)));
      }
    }
    return localLinkedList;
  }
  
  public final List<ba> c()
  {
    if (d()) {
      return b();
    }
    return new LinkedList();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/bb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */