package com.crittercism.internal;

import android.os.Build;
import android.os.Build.VERSION;
import android.util.Base64;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class bc
  implements bf
{
  JSONObject a;
  JSONArray b;
  String c;
  String d;
  String e;
  public float f = 1.0F;
  
  private bc() {}
  
  private bc(File paramFile, av<at> paramav, au paramau)
  {
    this.e = be.a.a();
    RandomAccessFile localRandomAccessFile = new RandomAccessFile(paramFile, "r");
    byte[] arrayOfByte = new byte[(int)localRandomAccessFile.length()];
    localRandomAccessFile.read(arrayOfByte);
    this.d = new String(Base64.encode(arrayOfByte, 0));
    this.c = paramFile.getName();
    this.a = new JSONObject();
    try
    {
      this.a.putOpt("app_version", paramau.a.a).putOpt("app_version_code", paramau.a()).putOpt("arch", System.getProperty("os.arch")).putOpt("carrier", paramau.b()).putOpt("mobile_country_code", paramau.c()).putOpt("mobile_network_code", paramau.d()).putOpt("disk_space_total", paramau.k()).putOpt("dpi", paramau.e()).putOpt("xdpi", Float.valueOf(paramau.f())).putOpt("ydpi", Float.valueOf(paramau.g())).putOpt("locale", paramau.i()).putOpt("model", Build.MODEL).putOpt("memory_total", au.l()).putOpt("name", new String()).putOpt("system", "android").putOpt("system_version", Build.VERSION.RELEASE);
      this.b = paramav.a();
      return;
    }
    catch (JSONException paramFile)
    {
      for (;;) {}
    }
  }
  
  public static bc a(File paramFile, av<at> paramav, au paramau)
  {
    if ((!paramFile.exists()) || (!paramFile.isDirectory())) {}
    File[] arrayOfFile;
    do
    {
      return null;
      arrayOfFile = paramFile.listFiles();
    } while ((arrayOfFile == null) || (arrayOfFile.length == 0));
    if (arrayOfFile.length == 1)
    {
      paramFile = arrayOfFile[0];
      if (!paramFile.isFile()) {}
    }
    for (;;)
    {
      try
      {
        paramFile = new bc(paramFile, paramav, paramau);
        int j = arrayOfFile.length;
        int i = 0;
        if (i >= j) {
          break;
        }
        ce.a(arrayOfFile[i]);
        i += 1;
        continue;
        paramFile = null;
      }
      catch (ThreadDeath paramFile)
      {
        throw paramFile;
      }
      catch (Throwable paramFile) {}
    }
    return paramFile;
  }
  
  public final String f()
  {
    return this.e;
  }
  
  public static final class a
    extends bw
  {
    public a(String paramString1, String paramString2)
    {
      super(paramString2);
    }
    
    public final bt a(as paramas, List<? extends bf> paramList)
    {
      paramas = new URL(paramas.b, "/android_v2/handle_ndk_crashes");
      JSONObject localJSONObject;
      try
      {
        localJSONObject = new JSONObject();
        localJSONObject.put("app_id", this.a);
        localJSONObject.put("hashed_device_id", this.b);
        localJSONObject.put("library_version", "5.8.1");
        JSONArray localJSONArray = new JSONArray();
        paramList = paramList.iterator();
        while (paramList.hasNext()) {
          localJSONArray.put(((bf)paramList.next()).g());
        }
        localJSONObject.put("crashes", localJSONArray);
      }
      catch (JSONException paramas)
      {
        throw new IOException(paramas.getMessage());
      }
      paramas = bt.a(paramas, this.a, localJSONObject);
      return paramas;
    }
  }
  
  public static final class b
    implements aw.b<bc>
  {
    private static bc b(File paramFile)
    {
      paramFile = ce.b(paramFile);
      try
      {
        paramFile = new JSONObject(paramFile);
        bc localbc = new bc((byte)0);
        localbc.a = paramFile.getJSONObject("appState");
        localbc.b = paramFile.getJSONArray("breadcrumbs");
        localbc.c = paramFile.getString("crashDumpFileName");
        localbc.d = paramFile.getString("base64EncodedCrash");
        localbc.e = paramFile.getString("fileName");
        localbc.f = ((float)paramFile.getDouble("rate"));
        return localbc;
      }
      catch (JSONException paramFile)
      {
        throw new IOException(paramFile.getMessage());
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/bc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */