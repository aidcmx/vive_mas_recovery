package com.crittercism.internal;

import java.util.LinkedList;
import java.util.List;
import org.json.JSONArray;

public final class bd<T extends bf>
  implements av<T>
{
  public final JSONArray a()
  {
    return new JSONArray();
  }
  
  public final void a(av.a parama) {}
  
  public final void a(String paramString) {}
  
  public final void a(List<T> paramList) {}
  
  public final boolean a(T paramT)
  {
    return true;
  }
  
  public final List<T> b()
  {
    return new LinkedList();
  }
  
  public final List<T> c()
  {
    return new LinkedList();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/bd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */