package com.crittercism.internal;

import java.util.Locale;

public final class be
{
  public static final be a = new be();
  private volatile int b = 1;
  private final long c = System.currentTimeMillis();
  
  private int b()
  {
    try
    {
      int i = this.b;
      this.b = (i + 1);
      return i;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public final String a()
  {
    return String.format(Locale.US, "%d.%d.%09d", new Object[] { Integer.valueOf(1), Long.valueOf(this.c), Integer.valueOf(b()) });
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/be.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */