package com.crittercism.internal;

public final class bh
  extends Exception
{
  private static final long serialVersionUID = 4511293437269420307L;
  
  public bh(String paramString)
  {
    this(paramString, null);
  }
  
  public bh(String paramString, Throwable paramThrowable)
  {
    super(paramString, paramThrowable);
  }
  
  public bh(Throwable paramThrowable)
  {
    super(paramThrowable);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/bh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */