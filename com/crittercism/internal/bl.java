package com.crittercism.internal;

import java.io.PrintStream;
import java.io.PrintWriter;

public final class bl
  extends bm
{
  private static final long serialVersionUID = -5983887553268578751L;
  private String[] b = null;
  private boolean c;
  
  public bl(String paramString1, String paramString2, String paramString3, boolean paramBoolean)
  {
    super(paramString1, paramString2, paramString3);
    this.b = paramString3.split("\\r\\n");
    this.c = paramBoolean;
  }
  
  public final void printStackTrace(PrintStream paramPrintStream)
  {
    String[] arrayOfString = this.b;
    int j = arrayOfString.length;
    int i = 0;
    while (i < j)
    {
      paramPrintStream.append(arrayOfString[i]);
      paramPrintStream.append("\n");
      i += 1;
    }
  }
  
  public final void printStackTrace(PrintWriter paramPrintWriter)
  {
    String[] arrayOfString = this.b;
    int j = arrayOfString.length;
    int i = 0;
    while (i < j)
    {
      paramPrintWriter.append(arrayOfString[i]);
      paramPrintWriter.append("\n");
      i += 1;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/bl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */