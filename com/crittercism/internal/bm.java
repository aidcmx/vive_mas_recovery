package com.crittercism.internal;

public class bm
  extends Throwable
{
  private static final long serialVersionUID = -1947260712494608235L;
  public String a = null;
  
  public bm(String paramString1, String paramString2, String paramString3)
  {
    super(paramString2);
    paramString2 = paramString1;
    if (paramString1 == null) {
      paramString2 = "";
    }
    if (paramString2.length() > 0)
    {
      this.a = paramString2;
      if (paramString3 != null) {
        break label58;
      }
    }
    label58:
    for (paramString1 = new String[0];; paramString1 = paramString3.split("\\r?\\n"))
    {
      setStackTrace(a(paramString1));
      return;
      this.a = "JavaScript Exception";
      break;
    }
  }
  
  private static StackTraceElement[] a(String[] paramArrayOfString)
  {
    int k = 0;
    StackTraceElement[] arrayOfStackTraceElement;
    int j;
    if ((paramArrayOfString.length >= 2) && (paramArrayOfString[0] != null) && (paramArrayOfString[1] != null) && (paramArrayOfString[0].equals(paramArrayOfString[1])))
    {
      arrayOfStackTraceElement = new StackTraceElement[paramArrayOfString.length - 1];
      j = 1;
    }
    for (;;)
    {
      int i = k;
      if (j == 0)
      {
        arrayOfStackTraceElement = new StackTraceElement[paramArrayOfString.length];
        i = k;
      }
      if (i < paramArrayOfString.length)
      {
        if ((i != 0) || (j == 0)) {
          if (j == 0) {
            break label106;
          }
        }
        label106:
        for (k = i - 1;; k = i)
        {
          arrayOfStackTraceElement[k] = new StackTraceElement("", paramArrayOfString[i], "", -1);
          i += 1;
          break;
        }
      }
      return arrayOfStackTraceElement;
      j = 0;
      arrayOfStackTraceElement = null;
    }
  }
  
  public String toString()
  {
    String str1 = getLocalizedMessage();
    String str2 = this.a;
    if (str1 == null) {
      return str2;
    }
    return str2 + ": " + str1;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/bm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */