package com.crittercism.internal;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import android.content.Context;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Process;
import android.os.SystemClock;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

@SuppressLint({"NewApi"})
public abstract class bn
{
  private static final int[] g = { 32, 544, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 8224 };
  boolean a = false;
  private int b = 0;
  private boolean c = false;
  private boolean d = false;
  private Application e = null;
  private a f = null;
  
  public bn(Application paramApplication)
  {
    if (Build.VERSION.SDK_INT < 14) {
      throw new IllegalStateException("App lifecycle monitoring is only supported on API 14 and later");
    }
    this.e = paramApplication;
  }
  
  private static boolean a(Context paramContext)
  {
    if ((paramContext instanceof Activity)) {
      return true;
    }
    Object localObject;
    try
    {
      paramContext = Class.forName("android.app.ActivityThread");
      localObject = paramContext.getMethod("currentActivityThread", new Class[0]);
      ((Method)localObject).setAccessible(true);
      if (!paramContext.isAssignableFrom(((Method)localObject).getReturnType())) {
        throw new NoSuchMethodException("currentActivityThread");
      }
    }
    catch (ThreadDeath paramContext)
    {
      throw paramContext;
      localObject = ((Method)localObject).invoke(null, new Object[0]);
      Class localClass = Integer.TYPE;
      paramContext = paramContext.getDeclaredField("mNumVisibleActivities");
      paramContext.setAccessible(true);
      if (!localClass.isAssignableFrom(paramContext.getType())) {
        throw new NoSuchFieldException("mNumVisibleActivities");
      }
    }
    catch (Throwable paramContext)
    {
      cd.a("Unable to detect if app has finished launching", paramContext);
      return false;
    }
    int i = ((Integer)paramContext.get(localObject)).intValue();
    return i > 0;
  }
  
  public static long f()
  {
    arrayOfLong = new long[1];
    int i = Process.myPid();
    localObject2 = "/proc/" + i + "/stat";
    localObject1 = Boolean.valueOf(false);
    try
    {
      localObject2 = (Boolean)Process.class.getDeclaredMethod("readProcFile", new Class[] { String.class, int[].class, String[].class, long[].class, float[].class }).invoke(null, new Object[] { localObject2, g, null, arrayOfLong, null });
      localObject1 = null;
    }
    catch (NoSuchMethodException localNoSuchMethodException)
    {
      for (;;)
      {
        localObject2 = localObject1;
        localObject1 = localNoSuchMethodException;
      }
    }
    catch (IllegalAccessException localIllegalAccessException)
    {
      for (;;)
      {
        localObject2 = localObject1;
        localObject1 = localIllegalAccessException;
      }
    }
    catch (InvocationTargetException localInvocationTargetException)
    {
      for (;;)
      {
        localObject2 = localObject1;
        localObject1 = localInvocationTargetException;
      }
      long l1 = arrayOfLong[0];
      long l2 = SystemClock.elapsedRealtime();
      return System.currentTimeMillis() - (l2 - l1 * 10L);
    }
    if (!((Boolean)localObject2).booleanValue()) {
      throw new IOException("Unable to determine process start time", (Throwable)localObject1);
    }
  }
  
  private void g()
  {
    if (this.a) {
      return;
    }
    this.a = true;
    d();
  }
  
  public final void a()
  {
    if (this.f != null) {}
    do
    {
      return;
      this.f = new a((byte)0);
      this.e.registerActivityLifecycleCallbacks(this.f);
    } while (!a(this.e));
    g();
  }
  
  public void a(Activity paramActivity) {}
  
  public void b() {}
  
  public void b(Activity paramActivity) {}
  
  public void c() {}
  
  public void c(Activity paramActivity) {}
  
  public void d() {}
  
  public void e() {}
  
  final class a
    implements Application.ActivityLifecycleCallbacks
  {
    private a() {}
    
    public final void onActivityCreated(Activity paramActivity, Bundle paramBundle) {}
    
    public final void onActivityDestroyed(Activity paramActivity) {}
    
    public final void onActivityPaused(Activity paramActivity)
    {
      if (paramActivity != null) {}
      try
      {
        bn.this.c(paramActivity);
        if (!bn.b(bn.this))
        {
          bn.e(bn.this);
          bn.g(bn.this);
          bn.c(bn.this);
        }
        bn.a(bn.this);
        return;
      }
      catch (ThreadDeath paramActivity)
      {
        throw paramActivity;
      }
      catch (Throwable paramActivity)
      {
        cd.b(paramActivity);
      }
    }
    
    public final void onActivityResumed(Activity paramActivity)
    {
      if (paramActivity != null) {
        try
        {
          bn.this.b(paramActivity);
          bn.a(bn.this);
          if (!bn.b(bn.this)) {
            bn.c(bn.this);
          }
          if (bn.d(bn.this))
          {
            cd.d("not a foreground. rotation event.");
            bn.a(bn.this, false);
          }
          for (;;)
          {
            bn.f(bn.this);
            return;
            if (bn.e(bn.this) == 0) {
              bn.this.b();
            }
            bn.this.a(paramActivity);
          }
          return;
        }
        catch (ThreadDeath paramActivity)
        {
          throw paramActivity;
        }
        catch (Throwable paramActivity)
        {
          cd.b(paramActivity);
        }
      }
    }
    
    public final void onActivitySaveInstanceState(Activity paramActivity, Bundle paramBundle) {}
    
    public final void onActivityStarted(Activity paramActivity) {}
    
    public final void onActivityStopped(Activity paramActivity)
    {
      if (paramActivity != null) {
        try
        {
          bn.this.e();
          if (!bn.b(bn.this))
          {
            bn.e(bn.this);
            bn.g(bn.this);
            bn.c(bn.this);
          }
          bn.h(bn.this);
          if (paramActivity.isChangingConfigurations())
          {
            cd.d("not a background. rotation event.");
            bn.a(bn.this, true);
            return;
          }
          if (bn.e(bn.this) == 0)
          {
            bn.this.c();
            return;
          }
        }
        catch (ThreadDeath paramActivity)
        {
          throw paramActivity;
        }
        catch (Throwable paramActivity)
        {
          cd.b(paramActivity);
        }
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/bn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */