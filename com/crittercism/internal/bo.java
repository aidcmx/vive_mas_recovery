package com.crittercism.internal;

import android.app.Application;
import android.support.annotation.NonNull;
import java.util.Date;
import java.util.concurrent.ExecutorService;

public final class bo
  extends bn
{
  private final ExecutorService b;
  private final av<ar> c;
  private ar d;
  private ap e;
  private boolean f;
  private cc g;
  private Date h;
  
  public bo(@NonNull Application paramApplication, @NonNull ExecutorService paramExecutorService, @NonNull av<ar> paramav, @NonNull ar paramar, @NonNull ap paramap, boolean paramBoolean, @NonNull cc paramcc, Date paramDate)
  {
    super(paramApplication);
    this.b = paramExecutorService;
    this.c = paramav;
    this.d = paramar;
    this.f = paramBoolean;
    this.e = paramap;
    this.g = paramcc;
    this.h = paramDate;
    a();
  }
  
  private void h()
  {
    final ar localar = this.d;
    if (localar == null) {
      return;
    }
    this.b.submit(new Runnable()
    {
      public final void run()
      {
        if (((Boolean)bo.a(bo.this).a(ap.u)).booleanValue())
        {
          float f = ((Float)bo.a(bo.this).a(ap.x)).floatValue();
          localar.m = f;
          bo.b(bo.this).a(localar);
        }
      }
    });
    this.d = null;
  }
  
  /* Error */
  public final void d()
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 35	com/crittercism/internal/bo:f	Z
    //   6: istore_1
    //   7: iload_1
    //   8: ifeq +6 -> 14
    //   11: aload_0
    //   12: monitorexit
    //   13: return
    //   14: aload_0
    //   15: invokespecial 62	com/crittercism/internal/bo:h	()V
    //   18: aload_0
    //   19: getfield 39	com/crittercism/internal/bo:g	Lcom/crittercism/internal/cc;
    //   22: ifnull -11 -> 11
    //   25: aload_0
    //   26: getfield 41	com/crittercism/internal/bo:h	Ljava/util/Date;
    //   29: ifnull +62 -> 91
    //   32: aload_0
    //   33: getfield 39	com/crittercism/internal/bo:g	Lcom/crittercism/internal/cc;
    //   36: astore_2
    //   37: aload_0
    //   38: getfield 41	com/crittercism/internal/bo:h	Ljava/util/Date;
    //   41: astore_3
    //   42: aload_2
    //   43: getfield 66	com/crittercism/internal/cc:e	Z
    //   46: ifne +45 -> 91
    //   49: invokestatic 72	android/os/Looper:myLooper	()Landroid/os/Looper;
    //   52: invokestatic 75	android/os/Looper:getMainLooper	()Landroid/os/Looper;
    //   55: if_acmpne +36 -> 91
    //   58: aload_2
    //   59: getfield 78	com/crittercism/internal/cc:b	Ljava/util/concurrent/ScheduledExecutorService;
    //   62: new 80	com/crittercism/internal/cc$3
    //   65: dup
    //   66: aload_2
    //   67: aload_3
    //   68: invokespecial 83	com/crittercism/internal/cc$3:<init>	(Lcom/crittercism/internal/cc;Ljava/util/Date;)V
    //   71: invokeinterface 86 2 0
    //   76: pop
    //   77: invokestatic 90	android/os/Looper:myQueue	()Landroid/os/MessageQueue;
    //   80: new 92	com/crittercism/internal/cc$4
    //   83: dup
    //   84: aload_2
    //   85: invokespecial 95	com/crittercism/internal/cc$4:<init>	(Lcom/crittercism/internal/cc;)V
    //   88: invokevirtual 101	android/os/MessageQueue:addIdleHandler	(Landroid/os/MessageQueue$IdleHandler;)V
    //   91: aload_0
    //   92: aconst_null
    //   93: putfield 39	com/crittercism/internal/bo:g	Lcom/crittercism/internal/cc;
    //   96: aload_0
    //   97: aconst_null
    //   98: putfield 41	com/crittercism/internal/bo:h	Ljava/util/Date;
    //   101: goto -90 -> 11
    //   104: astore_2
    //   105: aload_0
    //   106: monitorexit
    //   107: aload_2
    //   108: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	109	0	this	bo
    //   6	2	1	bool	boolean
    //   36	49	2	localcc	cc
    //   104	4	2	localObject	Object
    //   41	27	3	localDate	Date
    // Exception table:
    //   from	to	target	type
    //   2	7	104	finally
    //   14	91	104	finally
    //   91	101	104	finally
  }
  
  public final void g()
  {
    try
    {
      this.f = false;
      if (this.a) {
        h();
      }
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/bo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */