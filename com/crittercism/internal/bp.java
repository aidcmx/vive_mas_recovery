package com.crittercism.internal;

import android.app.Activity;
import android.app.Application;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import org.json.JSONObject;

public final class bp
  extends bn
{
  private ExecutorService b;
  private av<at> c;
  private ap d;
  
  public bp(Application paramApplication, ExecutorService paramExecutorService, av<at> paramav, ap paramap)
  {
    super(paramApplication);
    this.b = paramExecutorService;
    this.c = paramav;
    this.d = paramap;
    a();
  }
  
  public final void a(final Activity paramActivity)
  {
    this.b.submit(new Runnable()
    {
      public final void run()
      {
        if (((Boolean)bp.a(bp.this).a(ap.D)).booleanValue())
        {
          Object localObject = paramActivity.getClass().getName();
          int i = at.d.a;
          HashMap localHashMap = new HashMap();
          localHashMap.put("event", Integer.valueOf(i - 1));
          localHashMap.put("viewName", localObject);
          localObject = new at(at.b.f, new JSONObject(localHashMap));
          bp.b(bp.this).a((bf)localObject);
        }
      }
    });
  }
  
  public final void b()
  {
    this.b.submit(new Runnable()
    {
      public final void run()
      {
        if (((Boolean)bp.a(bp.this).a(ap.C)).booleanValue())
        {
          av localav = bp.b(bp.this);
          HashMap localHashMap = new HashMap();
          localHashMap.put("event", "foregrounded");
          localav.a(new at(at.b.d, new JSONObject(localHashMap)));
        }
      }
    });
  }
  
  public final void c()
  {
    this.b.submit(new Runnable()
    {
      public final void run()
      {
        if (((Boolean)bp.a(bp.this).a(ap.C)).booleanValue())
        {
          av localav = bp.b(bp.this);
          HashMap localHashMap = new HashMap();
          localHashMap.put("event", "backgrounded");
          localav.a(new at(at.b.d, new JSONObject(localHashMap)));
        }
      }
    });
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/bp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */