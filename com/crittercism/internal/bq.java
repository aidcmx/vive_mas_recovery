package com.crittercism.internal;

import android.annotation.TargetApi;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.ConnectivityManager.NetworkCallback;
import android.net.LinkProperties;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.NetworkRequest.Builder;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import java.util.concurrent.ExecutorService;

public final class bq
{
  av<at> a;
  ap b;
  private b c = b.f;
  private ConnectivityManager d;
  private ExecutorService e;
  private a f;
  
  @TargetApi(21)
  public bq(@NonNull Context paramContext, @NonNull ExecutorService paramExecutorService, @NonNull av<at> paramav, @NonNull ap paramap)
  {
    this.e = paramExecutorService;
    this.a = paramav;
    this.b = paramap;
    if (!ao.a(paramContext, "android.permission.ACCESS_NETWORK_STATE")) {}
    do
    {
      do
      {
        return;
      } while (Build.VERSION.SDK_INT < 21);
      this.d = ((ConnectivityManager)paramContext.getSystemService("connectivity"));
    } while (this.d == null);
    paramContext = new NetworkRequest.Builder().addCapability(12).build();
    this.f = new a((byte)0);
    this.d.registerNetworkCallback(paramContext, this.f);
  }
  
  @TargetApi(21)
  final class a
    extends ConnectivityManager.NetworkCallback
  {
    private a() {}
    
    public final void onAvailable(Network paramNetwork)
    {
      bq.a(bq.this);
    }
    
    public final void onCapabilitiesChanged(Network paramNetwork, NetworkCapabilities paramNetworkCapabilities) {}
    
    public final void onLinkPropertiesChanged(Network paramNetwork, LinkProperties paramLinkProperties) {}
    
    public final void onLosing(Network paramNetwork, int paramInt) {}
    
    public final void onLost(Network paramNetwork)
    {
      bq.a(bq.this);
    }
  }
  
  static enum b
  {
    private String g;
    
    private b(String paramString)
    {
      this.g = paramString;
    }
    
    public static b a(NetworkInfo paramNetworkInfo)
    {
      if ((paramNetworkInfo == null) || (!paramNetworkInfo.isConnected())) {
        return a;
      }
      int i = paramNetworkInfo.getType();
      if (i == 0) {
        switch (paramNetworkInfo.getSubtype())
        {
        }
      }
      while (i != 1)
      {
        return f;
        return b;
        return c;
        return d;
      }
      return e;
    }
    
    public final String toString()
    {
      return this.g;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/bq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */