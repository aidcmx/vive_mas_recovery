package com.crittercism.internal;

import android.app.Activity;
import android.app.Application;
import android.content.ComponentName;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import java.util.ArrayDeque;
import java.util.Deque;

public final class br
  extends bn
{
  public au b;
  public Deque<String> c = new ArrayDeque();
  
  public br(@NonNull Application paramApplication, @NonNull au paramau)
  {
    super(paramApplication);
    if (Build.VERSION.SDK_INT < 9) {
      throw new IllegalStateException("Activity monitoring is only supported on API 9 and later");
    }
    this.b = paramau;
    a();
  }
  
  private static String d(Activity paramActivity)
  {
    if (paramActivity == null) {
      return null;
    }
    return paramActivity.getComponentName().flattenToShortString().replace("/", "");
  }
  
  private void g()
  {
    this.b.g = ((String)this.c.peekFirst());
  }
  
  public final void b(Activity paramActivity)
  {
    if (this.c.size() >= 10000) {
      this.c.removeLast();
    }
    paramActivity = d(paramActivity);
    if (paramActivity == null) {
      return;
    }
    this.c.addFirst(paramActivity);
    g();
  }
  
  public final void c(Activity paramActivity)
  {
    paramActivity = d(paramActivity);
    if (paramActivity == null) {
      return;
    }
    this.c.removeFirstOccurrence(paramActivity);
    g();
  }
  
  public final void e() {}
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/br.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */