package com.crittercism.internal;

import android.content.Context;
import android.support.annotation.NonNull;
import java.io.IOException;
import java.net.URL;
import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;

public final class bs
  implements bv.a
{
  private au a;
  private Context b;
  private URL c;
  private boolean d;
  private ap e;
  
  public bs(@NonNull URL paramURL, @NonNull Context paramContext, @NonNull au paramau, @NonNull ap paramap)
  {
    this.c = paramURL;
    this.b = paramContext;
    this.a = paramau;
    this.e = paramap;
  }
  
  private void a(JSONObject paramJSONObject)
  {
    try
    {
      paramJSONObject = paramJSONObject.getJSONObject("txnConfig");
      a(paramJSONObject, ap.A, ap.z, ap.y, ap.B);
      long l1 = paramJSONObject.optLong("defaultTimeout", ((Long)ap.H.b()).longValue());
      this.e.a(ap.H, Long.valueOf(l1));
      paramJSONObject = paramJSONObject.optJSONObject("transactions");
      if (paramJSONObject != null)
      {
        Iterator localIterator = paramJSONObject.keys();
        while (localIterator.hasNext())
        {
          String str = (String)localIterator.next();
          long l2 = paramJSONObject.getJSONObject(str).optLong("timeout", l1);
          this.e.b(str, l2);
        }
      }
      return;
    }
    catch (JSONException paramJSONObject) {}
  }
  
  private void a(JSONObject paramJSONObject, ap.d paramd, ap.a parama1, ap.a parama2, ap.b paramb)
  {
    try
    {
      long l = paramJSONObject.getLong("interval");
      this.e.a(paramd, Long.valueOf(l * 1000L));
      try
      {
        boolean bool = paramJSONObject.getBoolean("enabled");
        this.e.a(parama2, Boolean.valueOf(bool));
        this.e.a(parama1, Boolean.valueOf(bool));
        try
        {
          float f = (float)paramJSONObject.getDouble("rate");
          this.e.a(paramb, Float.valueOf(f));
          return;
        }
        catch (JSONException paramJSONObject) {}
      }
      catch (JSONException paramd)
      {
        for (;;) {}
      }
    }
    catch (JSONException paramd)
    {
      for (;;) {}
    }
  }
  
  public final void a(bu parambu)
  {
    if ((parambu == null) || (parambu.b == null)) {
      return;
    }
    try
    {
      parambu = new JSONObject(new String(parambu.b));
      if (parambu.optBoolean("internalExceptionReporting", false)) {
        cd.a = cd.a.b;
      }
      for (;;)
      {
        int i = parambu.optInt("needPkg", 0);
        if (i == 1) {}
        try
        {
          JSONObject localJSONObject = new JSONObject();
          localJSONObject.putOpt("device_name", "Android");
          localJSONObject.putOpt("pkg", this.b.getPackageName());
          localJSONObject.putOpt("app_id", this.a.e);
          localJSONObject.putOpt("hashed_device_id", this.a.h());
          localJSONObject.putOpt("library_version", "5.8.1");
          bt.a(new URL(this.c + "/android_v2/update_package_name"), this.a.e, localJSONObject).a();
          this.d = true;
          try
          {
            a(parambu.getJSONObject("crashConfig"), ap.g, ap.f, ap.e, ap.h);
            try
            {
              a(parambu.getJSONObject("ndkConfig"), ap.o, ap.n, ap.m, ap.p);
              try
              {
                a(parambu.getJSONObject("heConfig"), ap.k, ap.j, ap.i, ap.l);
                try
                {
                  a(parambu.getJSONObject("metadataConfig"), ap.s, ap.r, ap.q, ap.t);
                  try
                  {
                    a(parambu.getJSONObject("apm").getJSONObject("net"), ap.c, ap.b, ap.a, ap.d);
                    a(parambu);
                    return;
                    cd.a = cd.a.c;
                  }
                  catch (JSONException localJSONException1)
                  {
                    for (;;) {}
                  }
                }
                catch (JSONException localJSONException2)
                {
                  for (;;) {}
                }
              }
              catch (JSONException localJSONException3)
              {
                for (;;) {}
              }
            }
            catch (JSONException localJSONException4)
            {
              for (;;) {}
            }
          }
          catch (JSONException localJSONException5)
          {
            for (;;) {}
          }
        }
        catch (IOException localIOException)
        {
          for (;;) {}
        }
        catch (JSONException localJSONException6)
        {
          for (;;) {}
        }
      }
      return;
    }
    catch (JSONException parambu) {}
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/bs.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */