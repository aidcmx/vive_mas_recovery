package com.crittercism.internal;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

public final class bt
{
  private final URL a;
  private final String b;
  private final byte[] c;
  private final Map<String, String> d;
  
  public bt(String paramString1, URL paramURL, byte[] paramArrayOfByte, String paramString2, String paramString3)
  {
    this.b = paramString1;
    this.a = paramURL;
    this.c = paramArrayOfByte;
    this.d = new HashMap();
    this.d.put("Content-type", paramString2);
    this.d.put("CRPlatform", "android");
    this.d.put("CRVersion", "5.8.1");
    this.d.put("CRAppId", paramString3);
  }
  
  public static bt a(URL paramURL, String paramString, JSONObject paramJSONObject)
  {
    return new bt("POST", paramURL, paramJSONObject.toString().getBytes("UTF8"), "application/json", paramString);
  }
  
  /* Error */
  public final bu a()
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 4
    //   3: invokestatic 76	com/crittercism/internal/cd:a	()V
    //   6: aload_0
    //   7: getfield 22	com/crittercism/internal/bt:a	Ljava/net/URL;
    //   10: invokevirtual 82	java/net/URL:openConnection	()Ljava/net/URLConnection;
    //   13: checkcast 84	java/net/HttpURLConnection
    //   16: astore_2
    //   17: aload_0
    //   18: getfield 29	com/crittercism/internal/bt:d	Ljava/util/Map;
    //   21: invokeinterface 88 1 0
    //   26: invokeinterface 94 1 0
    //   31: astore_3
    //   32: aload_3
    //   33: invokeinterface 100 1 0
    //   38: ifeq +73 -> 111
    //   41: aload_3
    //   42: invokeinterface 104 1 0
    //   47: checkcast 106	java/util/Map$Entry
    //   50: astore 5
    //   52: aload_2
    //   53: aload 5
    //   55: invokeinterface 109 1 0
    //   60: checkcast 61	java/lang/String
    //   63: aload 5
    //   65: invokeinterface 112 1 0
    //   70: checkcast 61	java/lang/String
    //   73: invokevirtual 116	java/net/HttpURLConnection:addRequestProperty	(Ljava/lang/String;Ljava/lang/String;)V
    //   76: goto -44 -> 32
    //   79: astore_3
    //   80: aconst_null
    //   81: astore_2
    //   82: new 118	com/crittercism/internal/bu
    //   85: dup
    //   86: aload_3
    //   87: invokespecial 121	com/crittercism/internal/bu:<init>	(Ljava/lang/Exception;)V
    //   90: astore 4
    //   92: aload 4
    //   94: astore_3
    //   95: aload_2
    //   96: ifnull +10 -> 106
    //   99: aload_2
    //   100: invokevirtual 124	java/net/HttpURLConnection:disconnect	()V
    //   103: aload 4
    //   105: astore_3
    //   106: invokestatic 126	com/crittercism/internal/cd:b	()V
    //   109: aload_3
    //   110: areturn
    //   111: aload_2
    //   112: sipush 2500
    //   115: invokevirtual 130	java/net/HttpURLConnection:setConnectTimeout	(I)V
    //   118: aload_2
    //   119: sipush 2500
    //   122: invokevirtual 133	java/net/HttpURLConnection:setReadTimeout	(I)V
    //   125: aload_2
    //   126: aload_0
    //   127: getfield 20	com/crittercism/internal/bt:b	Ljava/lang/String;
    //   130: ldc 51
    //   132: invokevirtual 137	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   135: invokevirtual 141	java/net/HttpURLConnection:setDoOutput	(Z)V
    //   138: aload_2
    //   139: aload_0
    //   140: getfield 20	com/crittercism/internal/bt:b	Ljava/lang/String;
    //   143: invokevirtual 145	java/net/HttpURLConnection:setRequestMethod	(Ljava/lang/String;)V
    //   146: aload_2
    //   147: instanceof 147
    //   150: ifeq +54 -> 204
    //   153: aload_2
    //   154: checkcast 147	javax/net/ssl/HttpsURLConnection
    //   157: astore 6
    //   159: ldc -107
    //   161: invokestatic 155	javax/net/ssl/SSLContext:getInstance	(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;
    //   164: astore_3
    //   165: aload_3
    //   166: aconst_null
    //   167: aconst_null
    //   168: aconst_null
    //   169: invokevirtual 159	javax/net/ssl/SSLContext:init	([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V
    //   172: aload_3
    //   173: invokevirtual 163	javax/net/ssl/SSLContext:getSocketFactory	()Ljavax/net/ssl/SSLSocketFactory;
    //   176: astore 5
    //   178: aload 5
    //   180: astore_3
    //   181: aload 5
    //   183: instanceof 165
    //   186: ifeq +12 -> 198
    //   189: aload 5
    //   191: checkcast 165	com/crittercism/internal/k
    //   194: invokevirtual 167	com/crittercism/internal/k:a	()Ljavax/net/ssl/SSLSocketFactory;
    //   197: astore_3
    //   198: aload 6
    //   200: aload_3
    //   201: invokevirtual 171	javax/net/ssl/HttpsURLConnection:setSSLSocketFactory	(Ljavax/net/ssl/SSLSocketFactory;)V
    //   204: aload_2
    //   205: invokevirtual 175	java/net/HttpURLConnection:getOutputStream	()Ljava/io/OutputStream;
    //   208: aload_0
    //   209: getfield 24	com/crittercism/internal/bt:c	[B
    //   212: invokevirtual 181	java/io/OutputStream:write	([B)V
    //   215: aload_2
    //   216: invokevirtual 185	java/net/HttpURLConnection:getResponseCode	()I
    //   219: istore_1
    //   220: iload_1
    //   221: sipush 200
    //   224: if_icmplt +39 -> 263
    //   227: iload_1
    //   228: sipush 300
    //   231: if_icmpge +32 -> 263
    //   234: aload_2
    //   235: invokevirtual 189	java/net/HttpURLConnection:getInputStream	()Ljava/io/InputStream;
    //   238: invokestatic 194	com/crittercism/internal/ce:a	(Ljava/io/InputStream;)[B
    //   241: astore_3
    //   242: new 118	com/crittercism/internal/bu
    //   245: dup
    //   246: iload_1
    //   247: aload_3
    //   248: invokespecial 197	com/crittercism/internal/bu:<init>	(I[B)V
    //   251: astore_3
    //   252: aload_2
    //   253: ifnull +59 -> 312
    //   256: aload_2
    //   257: invokevirtual 124	java/net/HttpURLConnection:disconnect	()V
    //   260: goto -154 -> 106
    //   263: aload_2
    //   264: invokevirtual 200	java/net/HttpURLConnection:getErrorStream	()Ljava/io/InputStream;
    //   267: invokestatic 194	com/crittercism/internal/ce:a	(Ljava/io/InputStream;)[B
    //   270: astore_3
    //   271: goto -29 -> 242
    //   274: astore_2
    //   275: aload 4
    //   277: astore_3
    //   278: aload_3
    //   279: ifnull +7 -> 286
    //   282: aload_3
    //   283: invokevirtual 124	java/net/HttpURLConnection:disconnect	()V
    //   286: aload_2
    //   287: athrow
    //   288: astore 4
    //   290: aload_2
    //   291: astore_3
    //   292: aload 4
    //   294: astore_2
    //   295: goto -17 -> 278
    //   298: astore 4
    //   300: aload_2
    //   301: astore_3
    //   302: aload 4
    //   304: astore_2
    //   305: goto -27 -> 278
    //   308: astore_3
    //   309: goto -227 -> 82
    //   312: goto -206 -> 106
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	315	0	this	bt
    //   219	28	1	i	int
    //   16	248	2	localHttpURLConnection	java.net.HttpURLConnection
    //   274	17	2	localObject1	Object
    //   294	11	2	localObject2	Object
    //   31	11	3	localIterator	java.util.Iterator
    //   79	8	3	localException1	Exception
    //   94	208	3	localObject3	Object
    //   308	1	3	localException2	Exception
    //   1	275	4	localbu	bu
    //   288	5	4	localObject4	Object
    //   298	5	4	localObject5	Object
    //   50	140	5	localObject6	Object
    //   157	42	6	localHttpsURLConnection	javax.net.ssl.HttpsURLConnection
    // Exception table:
    //   from	to	target	type
    //   6	32	79	java/lang/Exception
    //   32	76	79	java/lang/Exception
    //   111	178	79	java/lang/Exception
    //   181	198	79	java/lang/Exception
    //   198	204	79	java/lang/Exception
    //   6	32	274	finally
    //   32	76	274	finally
    //   111	178	274	finally
    //   181	198	274	finally
    //   198	204	274	finally
    //   204	220	288	finally
    //   234	242	288	finally
    //   242	252	288	finally
    //   263	271	288	finally
    //   82	92	298	finally
    //   204	220	308	java/lang/Exception
    //   234	242	308	java/lang/Exception
    //   242	252	308	java/lang/Exception
    //   263	271	308	java/lang/Exception
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/bt.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */