package com.crittercism.internal;

import android.net.ConnectivityManager;
import android.net.ConnectivityManager.NetworkCallback;
import android.net.LinkProperties;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.support.annotation.Nullable;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public final class bv
  implements av.a
{
  final ScheduledExecutorService a;
  av b;
  public a c;
  volatile boolean d = false;
  public volatile ScheduledFuture e;
  public volatile Future f;
  public volatile Future g;
  public boolean h = true;
  public ConnectivityManager i;
  public Object j;
  private as k;
  private final ExecutorService l;
  private bw m;
  private long n;
  private boolean o = false;
  private boolean p = true;
  private long q = 0L;
  private String r;
  
  public bv(as paramas, ScheduledExecutorService paramScheduledExecutorService, ExecutorService paramExecutorService, av paramav, bw parambw, String paramString, ap paramap, ap.a parama, ap.d paramd)
  {
    this.k = paramas;
    this.a = paramScheduledExecutorService;
    this.l = paramExecutorService;
    this.b = paramav;
    this.m = parambw;
    this.b.a(this);
    this.n = ((Long)paramap.a(paramd)).longValue();
    this.o = ((Boolean)paramap.a(parama)).booleanValue();
    this.r = paramString;
    paramas = new b(parama, paramd);
    paramap.I.add(paramas);
  }
  
  private static boolean a(@Nullable Future paramFuture)
  {
    return (paramFuture == null) || (paramFuture.isDone());
  }
  
  private long d()
  {
    long l2 = 0L;
    for (;;)
    {
      try
      {
        long l3 = this.n;
        long l1 = System.currentTimeMillis();
        long l4 = this.q;
        l4 = l1 - l4;
        l1 = l3;
        if (l4 > 0L)
        {
          l3 -= l4;
          l1 = l3;
          if (l3 < 0L)
          {
            l1 = l2;
            return l1;
          }
        }
      }
      finally
      {
        localObject = finally;
        throw ((Throwable)localObject);
      }
    }
  }
  
  public final void a()
  {
    this.d = true;
    b();
  }
  
  public final void a(long paramLong, TimeUnit paramTimeUnit)
  {
    try
    {
      this.n = paramTimeUnit.toMillis(paramLong);
      return;
    }
    finally
    {
      paramTimeUnit = finally;
      throw paramTimeUnit;
    }
  }
  
  /* Error */
  public final void a(boolean paramBoolean)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 58	com/crittercism/internal/bv:p	Z
    //   6: istore_2
    //   7: iload_2
    //   8: iload_1
    //   9: if_icmpne +6 -> 15
    //   12: aload_0
    //   13: monitorexit
    //   14: return
    //   15: aload_0
    //   16: iload_1
    //   17: putfield 58	com/crittercism/internal/bv:p	Z
    //   20: aload_0
    //   21: getfield 58	com/crittercism/internal/bv:p	Z
    //   24: ifne -12 -> 12
    //   27: aload_0
    //   28: invokestatic 127	java/lang/System:currentTimeMillis	()J
    //   31: putfield 60	com/crittercism/internal/bv:q	J
    //   34: aload_0
    //   35: invokevirtual 130	com/crittercism/internal/bv:b	()Ljava/util/concurrent/Future;
    //   38: pop
    //   39: goto -27 -> 12
    //   42: astore_3
    //   43: aload_0
    //   44: monitorexit
    //   45: aload_3
    //   46: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	47	0	this	bv
    //   0	47	1	paramBoolean	boolean
    //   6	4	2	bool	boolean
    //   42	4	3	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   2	7	42	finally
    //   15	39	42	finally
  }
  
  /* Error */
  public final Future b()
  {
    // Byte code:
    //   0: iconst_1
    //   1: istore_2
    //   2: aload_0
    //   3: monitorenter
    //   4: aload_0
    //   5: getfield 142	com/crittercism/internal/bv:e	Ljava/util/concurrent/ScheduledFuture;
    //   8: invokestatic 144	com/crittercism/internal/bv:a	(Ljava/util/concurrent/Future;)Z
    //   11: ifne +47 -> 58
    //   14: iconst_1
    //   15: istore_1
    //   16: aload_0
    //   17: getfield 146	com/crittercism/internal/bv:f	Ljava/util/concurrent/Future;
    //   20: invokestatic 144	com/crittercism/internal/bv:a	(Ljava/util/concurrent/Future;)Z
    //   23: ifne +40 -> 63
    //   26: aload_0
    //   27: getfield 56	com/crittercism/internal/bv:o	Z
    //   30: ifeq +20 -> 50
    //   33: aload_0
    //   34: getfield 58	com/crittercism/internal/bv:p	Z
    //   37: istore_3
    //   38: iload_3
    //   39: ifne +11 -> 50
    //   42: iload_1
    //   43: ifne +7 -> 50
    //   46: iload_2
    //   47: ifeq +21 -> 68
    //   50: aconst_null
    //   51: astore 4
    //   53: aload_0
    //   54: monitorexit
    //   55: aload 4
    //   57: areturn
    //   58: iconst_0
    //   59: istore_1
    //   60: goto -44 -> 16
    //   63: iconst_0
    //   64: istore_2
    //   65: goto -39 -> 26
    //   68: new 8	com/crittercism/internal/bv$1
    //   71: dup
    //   72: aload_0
    //   73: invokespecial 149	com/crittercism/internal/bv$1:<init>	(Lcom/crittercism/internal/bv;)V
    //   76: astore 4
    //   78: aload_0
    //   79: aload_0
    //   80: getfield 68	com/crittercism/internal/bv:a	Ljava/util/concurrent/ScheduledExecutorService;
    //   83: aload 4
    //   85: aload_0
    //   86: invokespecial 151	com/crittercism/internal/bv:d	()J
    //   89: getstatic 155	java/util/concurrent/TimeUnit:MILLISECONDS	Ljava/util/concurrent/TimeUnit;
    //   92: invokeinterface 161 5 0
    //   97: putfield 142	com/crittercism/internal/bv:e	Ljava/util/concurrent/ScheduledFuture;
    //   100: aload_0
    //   101: getfield 142	com/crittercism/internal/bv:e	Ljava/util/concurrent/ScheduledFuture;
    //   104: astore 4
    //   106: goto -53 -> 53
    //   109: astore 4
    //   111: ldc -93
    //   113: aload 4
    //   115: invokestatic 168	com/crittercism/internal/cd:a	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   118: goto -18 -> 100
    //   121: astore 4
    //   123: aload_0
    //   124: monitorexit
    //   125: aload 4
    //   127: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	128	0	this	bv
    //   15	45	1	i1	int
    //   1	64	2	i2	int
    //   37	2	3	bool	boolean
    //   51	54	4	localObject1	Object
    //   109	5	4	localRejectedExecutionException	java.util.concurrent.RejectedExecutionException
    //   121	5	4	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   78	100	109	java/util/concurrent/RejectedExecutionException
    //   4	14	121	finally
    //   16	26	121	finally
    //   26	38	121	finally
    //   68	78	121	finally
    //   78	100	121	finally
    //   100	106	121	finally
    //   111	118	121	finally
  }
  
  /* Error */
  final void b(boolean paramBoolean)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 56	com/crittercism/internal/bv:o	Z
    //   6: istore_2
    //   7: iload_2
    //   8: iload_1
    //   9: if_icmpne +6 -> 15
    //   12: aload_0
    //   13: monitorexit
    //   14: return
    //   15: aload_0
    //   16: iload_1
    //   17: putfield 56	com/crittercism/internal/bv:o	Z
    //   20: aload_0
    //   21: getfield 56	com/crittercism/internal/bv:o	Z
    //   24: ifeq -12 -> 12
    //   27: aload_0
    //   28: invokevirtual 130	com/crittercism/internal/bv:b	()Ljava/util/concurrent/Future;
    //   31: pop
    //   32: goto -20 -> 12
    //   35: astore_3
    //   36: aload_0
    //   37: monitorexit
    //   38: aload_3
    //   39: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	40	0	this	bv
    //   0	40	1	paramBoolean	boolean
    //   6	4	2	bool	boolean
    //   35	4	3	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   2	7	35	finally
    //   15	32	35	finally
  }
  
  final void c()
  {
    int i2 = 1;
    for (;;)
    {
      try
      {
        this.q = System.currentTimeMillis();
        if (!this.p)
        {
          localObject1 = this.i;
          if (localObject1 != null) {
            continue;
          }
          i1 = i2;
          if (i1 != 0) {
            continue;
          }
        }
      }
      finally
      {
        try
        {
          final Object localObject1;
          int i3;
          final bt localbt = this.m.a(this.k, (List)localObject1);
          this.f = this.l.submit(new Runnable()
          {
            public final void run()
            {
              int j = 0;
              bu localbu = localbt.a();
              if ((localbu.a >= 200) && (localbu.a < 300)) {}
              for (int i = 1;; i = 0)
              {
                if (i == 0)
                {
                  if (localbu.a < 500)
                  {
                    i = j;
                    if (!(localbu.c instanceof SocketTimeoutException)) {}
                  }
                  else
                  {
                    i = 1;
                  }
                  if (i != 0) {}
                }
                else
                {
                  bv localbv = bv.this;
                  List localList = localObject1;
                  localbv.g = localbv.a.submit(new bv.3(localbv, localList));
                }
                if (bv.this.c != null) {
                  bv.this.c.a(localbu);
                }
                bv.this.f = null;
                if (bv.this.d) {
                  bv.this.b();
                }
                return;
              }
            }
          });
        }
        catch (IOException localIOException)
        {
          localIterator = ((List)localObject2).iterator();
        }
        localObject2 = finally;
      }
      return;
      localObject1 = this.i.getActiveNetworkInfo();
      if (localObject1 != null)
      {
        i3 = ((NetworkInfo)localObject1).getType();
        if (((NetworkInfo)localObject1).isConnected())
        {
          i1 = i2;
          if (this.h) {
            continue;
          }
          i1 = i2;
          if (i3 == 1) {
            continue;
          }
          break label215;
          localObject1 = this.b.c();
          this.d = false;
          i1 = ((List)localObject1).size();
          if (i1 == 0) {
            continue;
          }
          Iterator localIterator;
          while (localIterator.hasNext())
          {
            bf localbf = (bf)localIterator.next();
            this.b.a(localbf.f());
          }
          continue;
        }
      }
      label215:
      int i1 = 0;
    }
  }
  
  public final String toString()
  {
    return this.r;
  }
  
  public static abstract interface a
  {
    public abstract void a(bu parambu);
  }
  
  final class b
    implements ap.c
  {
    private ap.a b;
    private ap.d c;
    
    public b(ap.a parama, ap.d paramd)
    {
      this.b = parama;
      this.c = paramd;
    }
    
    public final void a(ap paramap, String paramString)
    {
      if (this.b.a().equals(paramString)) {
        bv.this.b(((Boolean)paramap.a(this.b)).booleanValue());
      }
      while (!this.c.a().equals(paramString)) {
        return;
      }
      long l = ((Long)paramap.a(this.c)).longValue();
      bv.this.a(l, TimeUnit.MILLISECONDS);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/bv.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */