package com.crittercism.internal;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import java.util.UUID;

public final class bx
{
  private SharedPreferences a;
  private SharedPreferences b;
  private Context c;
  
  public bx(Context paramContext)
  {
    if (paramContext == null) {
      throw new NullPointerException("context was null");
    }
    this.c = paramContext;
    this.a = paramContext.getSharedPreferences("com.crittercism.usersettings", 0);
    this.b = paramContext.getSharedPreferences("com.crittercism.prefs", 0);
    if (this.a == null) {
      throw new NullPointerException("prefs were null");
    }
    if (this.b == null) {
      throw new NullPointerException("legacy prefs were null");
    }
  }
  
  private boolean a(String paramString)
  {
    SharedPreferences.Editor localEditor = this.a.edit();
    localEditor.putString("hashedDeviceID", paramString);
    return localEditor.commit();
  }
  
  private static String b()
  {
    try
    {
      String str = UUID.randomUUID().toString();
      return str;
    }
    catch (ThreadDeath localThreadDeath)
    {
      throw localThreadDeath;
    }
    catch (Throwable localThrowable)
    {
      cd.b(localThrowable);
    }
    return null;
  }
  
  public final String a()
  {
    Object localObject2 = this.a.getString("hashedDeviceID", null);
    Object localObject1 = localObject2;
    if (localObject2 == null)
    {
      localObject2 = this.b.getString("com.crittercism.prefs.did", null);
      localObject1 = localObject2;
      if (localObject2 != null)
      {
        localObject1 = localObject2;
        if (a((String)localObject2))
        {
          localObject1 = this.b.edit();
          ((SharedPreferences.Editor)localObject1).remove("com.crittercism.prefs.did");
          ((SharedPreferences.Editor)localObject1).commit();
          localObject1 = localObject2;
        }
      }
    }
    localObject2 = localObject1;
    if (localObject1 == null)
    {
      localObject2 = b();
      a((String)localObject2);
    }
    return (String)localObject2;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/bx.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */