package com.crittercism.internal;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import org.json.JSONException;
import org.json.JSONObject;

public final class by
{
  public SharedPreferences a;
  
  public by(Context paramContext)
  {
    this.a = paramContext.getSharedPreferences("com.crittercism.usersettings", 0);
    if (this.a.contains("optOutStatusSettings")) {}
    try
    {
      boolean bool = new JSONObject(this.a.getString("optOutStatusSettings", null)).getBoolean("optOutStatus");
      this.a.edit().putBoolean("isOptedOut", bool).commit();
      this.a.edit().remove("optOutStatusSettings").commit();
      return;
    }
    catch (JSONException paramContext)
    {
      for (;;) {}
    }
  }
  
  public final boolean a()
  {
    return this.a.getBoolean("isOptedOut", false);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/by.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */