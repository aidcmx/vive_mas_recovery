package com.crittercism.internal;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.crittercism.app.CrashData;
import java.util.Date;

public final class bz
{
  public static CrashData a;
  
  public static void a(Context paramContext, CrashData paramCrashData)
  {
    boolean bool = false;
    paramContext = paramContext.getSharedPreferences("com.crittercism.usersettings", 0).edit();
    if (paramCrashData != null) {
      bool = true;
    }
    paramContext.putBoolean("crashedOnLastLoad", bool);
    if (bool)
    {
      paramContext.putString("crashName", paramCrashData.getName());
      paramContext.putString("crashReason", paramCrashData.getReason());
      paramContext.putLong("crashDate", paramCrashData.getTimeOccurred().getTime());
    }
    paramContext.commit();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/bz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */