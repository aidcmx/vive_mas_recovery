package com.crittercism.internal;

import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;

public final class c
{
  public ConnectivityManager a;
  
  public c(Context paramContext)
  {
    if (paramContext == null)
    {
      cd.a("Given a null Context.");
      return;
    }
    if (paramContext.getPackageManager().checkPermission("android.permission.ACCESS_NETWORK_STATE", paramContext.getPackageName()) == 0)
    {
      this.a = ((ConnectivityManager)paramContext.getSystemService("connectivity"));
      return;
    }
    cd.a("Add android.permission.ACCESS_NETWORK_STATE to AndroidManifest.xml to get more detailed OPTMZ data");
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */