package com.crittercism.internal;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public final class ca
{
  public SharedPreferences a;
  
  public ca(Context paramContext)
  {
    this.a = paramContext.getSharedPreferences("com.crittercism.usersettings", 0);
    if (!this.a.contains("sessionIDSetting")) {
      this.a.edit().putInt("sessionIDSetting", 0).commit();
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/ca.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */