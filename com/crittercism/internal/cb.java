package com.crittercism.internal;

import android.os.Build;
import android.os.Build.VERSION;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class cb
  implements bf
{
  String a;
  long b = -1L;
  public int c = -1;
  long d;
  long e = -1L;
  int f = d.b;
  int g = e.a;
  String h = be.a.a();
  List<b> i = new LinkedList();
  float j = 1.0F;
  
  private cb() {}
  
  public cb(String paramString, long paramLong1, int paramInt1, long paramLong2, int paramInt2)
  {
    String str = paramString;
    if (paramString.length() > 255) {
      str = paramString.substring(0, 255);
    }
    this.a = str;
    this.c = paramInt1;
    this.d = paramLong1;
    this.b = paramLong2;
    this.g = paramInt2;
  }
  
  public final long a()
  {
    long l1 = 0L;
    long l2 = this.e;
    if (this.e < 0L) {
      l2 = System.currentTimeMillis();
    }
    for (;;)
    {
      Collections.sort(this.i);
      Iterator localIterator = this.i.iterator();
      b localb;
      while (localIterator.hasNext())
      {
        localb = (b)localIterator.next();
        if (localb.b < this.d) {
          localIterator.remove();
        } else if (localb.b > l2) {
          localIterator.remove();
        }
      }
      long l4;
      if (this.f == d.f)
      {
        l4 = this.b;
        return l4;
      }
      if ((this.g == e.c) || (this.i.size() == 0)) {
        return l2 - this.d;
      }
      int k = c.b;
      if (((b)this.i.get(0)).a == c.b) {
        k = c.a;
      }
      for (;;)
      {
        long l3 = this.d;
        localIterator = this.i.iterator();
        while (localIterator.hasNext())
        {
          localb = (b)localIterator.next();
          if (localb.b >= this.d)
          {
            if (localb.b > l2) {
              break;
            }
            l4 = l1;
            if (k == c.b) {
              l4 = l1 + (localb.b - l3);
            }
            l3 = localb.b;
            k = localb.a;
            l1 = l4;
          }
        }
        l4 = l1;
        if (k != c.b) {
          break;
        }
        return l1 + (l2 - l3);
      }
    }
  }
  
  public final void a(int paramInt, long paramLong)
  {
    if (this.f != d.b) {
      return;
    }
    this.e = paramLong;
    if (a() > this.b)
    {
      this.f = d.f;
      return;
    }
    this.f = paramInt;
  }
  
  public final JSONArray b()
  {
    Object localObject3 = null;
    Object localObject1 = localObject3;
    try
    {
      JSONArray localJSONArray = new JSONArray().put(this.a).put(this.f - 1).put(this.b / 1000.0D);
      localObject1 = localObject3;
      if (this.c == -1) {
        localObject1 = localObject3;
      }
      for (Object localObject2 = JSONObject.NULL;; localObject2 = Integer.valueOf(this.c))
      {
        localObject1 = localObject3;
        localObject2 = localJSONArray.put(localObject2).put(new JSONObject()).put(cg.a.a(new Date(this.d))).put(cg.a.a(new Date(this.e)));
        localObject1 = localObject2;
        if (Build.VERSION.SDK_INT < 14) {
          break;
        }
        localObject1 = localObject2;
        ((JSONArray)localObject2).put(a() / 1000.0D);
        return (JSONArray)localObject2;
        localObject1 = localObject3;
      }
      localObject1 = localObject2;
      ((JSONArray)localObject2).put(JSONObject.NULL);
      return (JSONArray)localObject2;
    }
    catch (JSONException localJSONException) {}
    return (JSONArray)localObject1;
  }
  
  public final String f()
  {
    return this.h;
  }
  
  public static final class a
  {
    public String a;
    public long b;
    public int c;
    public long d;
    int e = cb.e.a;
    
    public final cb a()
    {
      return new cb(this.a, this.b, this.c, this.d, this.e);
    }
  }
  
  static final class b
    implements Comparable<b>
  {
    public int a;
    public long b;
    
    public b(int paramInt, long paramLong)
    {
      this.a = paramInt;
      this.b = paramLong;
    }
    
    public b(JSONObject paramJSONObject)
    {
      this.a = cb.c.a()[paramJSONObject.getInt("type")];
      this.b = paramJSONObject.getLong("time");
    }
  }
  
  static enum c
  {
    public static int[] a()
    {
      return (int[])c.clone();
    }
  }
  
  public static enum d
  {
    public static int[] a()
    {
      return (int[])j.clone();
    }
  }
  
  public static enum e
  {
    public static int[] a()
    {
      return (int[])d.clone();
    }
  }
  
  public static final class f
    extends bw
  {
    private au c;
    private av<at> d;
    
    public f(av<at> paramav, au paramau)
    {
      super(paramau.h());
      this.c = paramau;
      this.d = paramav;
    }
    
    private static boolean a(List<? extends bf> paramList)
    {
      paramList = paramList.iterator();
      while (paramList.hasNext())
      {
        int i = ((cb)paramList.next()).f;
        if ((i != cb.d.c) && (i != cb.d.i) && (i != cb.d.h)) {
          return true;
        }
      }
      return false;
    }
    
    public final bt a(as paramas, List<? extends bf> paramList)
    {
      paramas = new URL(paramas.c + "/api/v1/transactions");
      JSONObject localJSONObject;
      try
      {
        localJSONObject = new JSONObject();
        Object localObject = new JSONObject();
        ((JSONObject)localObject).put("appID", this.c.e);
        ((JSONObject)localObject).put("deviceID", this.c.h());
        ((JSONObject)localObject).put("crPlatform", "android");
        ((JSONObject)localObject).put("crVersion", "5.8.1");
        ((JSONObject)localObject).put("deviceModel", Build.MODEL);
        ((JSONObject)localObject).put("osName", "android");
        ((JSONObject)localObject).put("osVersion", Build.VERSION.RELEASE);
        ((JSONObject)localObject).put("carrier", this.c.b());
        ((JSONObject)localObject).put("mobileCountryCode", this.c.c());
        ((JSONObject)localObject).put("mobileNetworkCode", this.c.d());
        ((JSONObject)localObject).put("appVersion", this.c.a.a);
        ((JSONObject)localObject).put("locale", this.c.i());
        localJSONObject.put("appState", localObject);
        localObject = new JSONArray();
        Iterator localIterator = paramList.iterator();
        while (localIterator.hasNext()) {
          ((JSONArray)localObject).put(((bf)localIterator.next()).g());
        }
        localJSONObject.put("transactions", localObject);
      }
      catch (JSONException paramas)
      {
        throw ((IOException)new IOException(paramas.getMessage()).initCause(paramas));
      }
      if (a(paramList))
      {
        localJSONObject.put("systemBreadcrumbs", this.d.a());
        localJSONObject.put("breadcrumbs", new JSONObject());
        localJSONObject.put("endpoints", new JSONArray());
      }
      paramas = bt.a(paramas, this.a, localJSONObject);
      return paramas;
    }
  }
  
  public static final class g
    implements aw.b<cb>
  {
    private static cb b(File paramFile)
    {
      int i = 0;
      try
      {
        Object localObject = new JSONObject(ce.b(paramFile));
        paramFile = new cb((byte)0);
        paramFile.a = ((JSONObject)localObject).getString("name");
        paramFile.f = cb.d.a()[localObject.getInt("state")];
        paramFile.b = ((JSONObject)localObject).getLong("timeout");
        paramFile.c = ((JSONObject)localObject).getInt("value");
        paramFile.d = ((JSONObject)localObject).getLong("startTime");
        paramFile.e = ((JSONObject)localObject).getLong("endTime");
        paramFile.h = ((JSONObject)localObject).getString("sequenceNumber");
        paramFile.j = ((float)((JSONObject)localObject).getDouble("rate"));
        paramFile.g = cb.e.a()[localObject.getInt("type")];
        localObject = ((JSONObject)localObject).getJSONArray("lifeCycleTransitions");
        while (i < ((JSONArray)localObject).length())
        {
          JSONObject localJSONObject = ((JSONArray)localObject).getJSONObject(i);
          paramFile.i.add(new cb.b(localJSONObject));
          i += 1;
        }
        return paramFile;
      }
      catch (JSONException paramFile)
      {
        throw new IOException(paramFile.getMessage());
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/cb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */