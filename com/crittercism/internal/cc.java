package com.crittercism.internal;

import android.app.Application;
import android.os.MessageQueue.IdleHandler;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public final class cc
{
  public final HashMap<String, cb> a = new HashMap();
  public final ScheduledExecutorService b;
  final av<cb> c;
  public ap d;
  public boolean e = "true".equals(System.getProperty("com.crittercism.appLoadUserflowIsDisabled", "false"));
  private ScheduledFuture f;
  private a g;
  
  public cc(Application paramApplication, ScheduledExecutorService paramScheduledExecutorService, av<cb> paramav, ap paramap)
  {
    this.b = paramScheduledExecutorService;
    this.c = paramav;
    this.d = paramap;
    paramScheduledExecutorService = TimeUnit.SECONDS;
    if (this.f != null) {
      this.f.cancel(false);
    }
    if ((this.f == null) || (this.f.isDone())) {
      this.f = this.b.scheduleAtFixedRate(new b((byte)0), 10L, 10L, paramScheduledExecutorService);
    }
    this.g = new a(paramApplication);
  }
  
  public final Collection<cb> a()
  {
    long l = System.currentTimeMillis();
    synchronized (this.a)
    {
      LinkedList localLinkedList = new LinkedList(this.a.values());
      this.a.clear();
      Iterator localIterator = localLinkedList.iterator();
      if (localIterator.hasNext()) {
        ((cb)localIterator.next()).a(cb.d.g, l);
      }
    }
    return localCollection;
  }
  
  public final void a(String paramString)
  {
    long l = System.currentTimeMillis();
    synchronized (this.a)
    {
      final cb localcb = (cb)this.a.remove(paramString);
      if (localcb == null)
      {
        cd.b("endUserflow(" + paramString + "): no such userflow");
        return;
      }
      localcb.a(cb.d.c, l);
      this.b.submit(new Runnable()
      {
        public final void run()
        {
          if (((Boolean)cc.this.d.a(ap.y)).booleanValue())
          {
            localcb.j = ((Float)cc.this.d.a(ap.B)).floatValue();
            cc.this.c.a(localcb);
          }
        }
      });
      return;
    }
  }
  
  public final int b(String paramString)
  {
    synchronized (this.a)
    {
      cb localcb = (cb)this.a.get(paramString);
      if (localcb == null)
      {
        cd.b("getUserflowValue(" + paramString + "): no such userflow");
        return -1;
      }
      int i = localcb.c;
      return i;
    }
  }
  
  final class a
    extends bn
  {
    public a(Application paramApplication)
    {
      super();
      a();
    }
    
    public final void b()
    {
      long l = System.currentTimeMillis();
      synchronized (cc.this.a)
      {
        Iterator localIterator = cc.this.a.values().iterator();
        while (localIterator.hasNext())
        {
          cb localcb = (cb)localIterator.next();
          if ((localcb.f == cb.d.b) && (l >= localcb.d)) {
            localcb.i.add(new cb.b(cb.c.b, l));
          }
        }
      }
    }
    
    public final void c()
    {
      long l = System.currentTimeMillis();
      synchronized (cc.this.a)
      {
        Iterator localIterator = cc.this.a.values().iterator();
        while (localIterator.hasNext())
        {
          cb localcb = (cb)localIterator.next();
          if ((localcb.f == cb.d.b) && (l >= localcb.d)) {
            localcb.i.add(new cb.b(cb.c.a, l));
          }
        }
      }
    }
  }
  
  final class b
    implements Runnable
  {
    private b() {}
    
    public final void run()
    {
      HashMap localHashMap2 = new HashMap();
      Object localObject2;
      Object localObject3;
      synchronized (cc.this.a)
      {
        localObject2 = cc.this.a.entrySet().iterator();
        while (((Iterator)localObject2).hasNext())
        {
          Object localObject4 = (Map.Entry)((Iterator)localObject2).next();
          localObject3 = (String)((Map.Entry)localObject4).getKey();
          localObject4 = (cb)((Map.Entry)localObject4).getValue();
          if (((cb)localObject4).a() >= ((cb)localObject4).b) {
            localHashMap2.put(localObject3, localObject4);
          }
        }
      }
      Iterator localIterator = ((Map)localObject1).entrySet().iterator();
      while (localIterator.hasNext())
      {
        localObject3 = (Map.Entry)localIterator.next();
        localObject2 = (String)((Map.Entry)localObject3).getKey();
        localObject3 = (cb)((Map.Entry)localObject3).getValue();
        cc.this.a.remove(localObject2);
        ((cb)localObject3).a(cb.d.f, System.currentTimeMillis());
        if (((Boolean)cc.this.d.a(ap.y)).booleanValue())
        {
          ((cb)localObject3).j = ((Float)cc.this.d.a(ap.B)).floatValue();
          cc.this.c.a((bf)localObject3);
        }
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/cc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */