package com.crittercism.internal;

import android.util.Log;
import com.crittercism.app.Crittercism.LoggingLevel;

public final class cd
{
  public static int a = a.a;
  private static b b = b.d;
  private static cd c = new cd();
  
  public static void a() {}
  
  public static void a(Crittercism.LoggingLevel paramLoggingLevel)
  {
    b = b.a(paramLoggingLevel);
  }
  
  public static void a(String paramString)
  {
    if (b.a(b.b)) {
      Log.e("Crittercism", paramString);
    }
  }
  
  public static void a(String paramString, Throwable paramThrowable)
  {
    if (b.a(b.b)) {
      Log.e("Crittercism", paramString, paramThrowable);
    }
  }
  
  public static void a(Throwable paramThrowable)
  {
    if (b.a(b.e)) {
      Log.d("Crittercism", paramThrowable.getMessage(), paramThrowable);
    }
  }
  
  public static void b() {}
  
  public static void b(String paramString)
  {
    if (b.a(b.c)) {
      Log.w("Crittercism", paramString);
    }
  }
  
  public static void b(String paramString, Throwable paramThrowable)
  {
    if (b.a(b.c)) {
      Log.w("Crittercism", paramString, paramThrowable);
    }
  }
  
  public static void b(Throwable paramThrowable)
  {
    try
    {
      a(paramThrowable);
      return;
    }
    catch (ThreadDeath paramThrowable)
    {
      throw paramThrowable;
    }
    catch (Throwable paramThrowable) {}
  }
  
  public static void c(String paramString)
  {
    if (b.a(b.d)) {
      Log.i("Crittercism", paramString);
    }
  }
  
  public static void c(String paramString, Throwable paramThrowable)
  {
    if (b.a(b.e)) {
      Log.d("Crittercism", paramString, paramThrowable);
    }
  }
  
  public static void d(String paramString)
  {
    if (b.a(b.e)) {
      Log.d("Crittercism", paramString);
    }
  }
  
  public static enum a {}
  
  public static enum b
  {
    private int g;
    
    private b(int paramInt)
    {
      this.g = paramInt;
    }
    
    public static b a(Crittercism.LoggingLevel paramLoggingLevel)
    {
      switch (cd.1.b[paramLoggingLevel.ordinal()])
      {
      default: 
        return c;
      case 1: 
        return a;
      case 2: 
        return b;
      case 3: 
        return c;
      }
      return d;
    }
    
    public final boolean a(b paramb)
    {
      return this.g >= paramb.g;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/cd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */