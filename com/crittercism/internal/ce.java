package com.crittercism.internal;

import android.content.Context;
import android.content.res.AssetManager;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.util.Scanner;

public final class ce
{
  public static String a(Context paramContext, String paramString)
  {
    Object localObject = paramContext.getAssets();
    paramContext = null;
    try
    {
      paramString = ((AssetManager)localObject).open(paramString);
      paramContext = paramString;
      localObject = new String(a(paramString));
      return (String)localObject;
    }
    finally
    {
      if (paramContext != null) {
        paramContext.close();
      }
    }
  }
  
  public static void a(File paramFile)
  {
    if (!paramFile.getAbsolutePath().contains("com.crittercism")) {
      return;
    }
    if (paramFile.isDirectory())
    {
      File[] arrayOfFile = paramFile.listFiles();
      int j = arrayOfFile.length;
      int i = 0;
      while (i < j)
      {
        a(arrayOfFile[i]);
        i += 1;
      }
    }
    paramFile.delete();
  }
  
  public static byte[] a(InputStream paramInputStream)
  {
    ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
    byte[] arrayOfByte = new byte['က'];
    for (;;)
    {
      int i = paramInputStream.read(arrayOfByte);
      if (-1 == i) {
        break;
      }
      localByteArrayOutputStream.write(arrayOfByte, 0, i);
    }
    return localByteArrayOutputStream.toByteArray();
  }
  
  public static String b(File paramFile)
  {
    paramFile = new Scanner(paramFile);
    try
    {
      String str = paramFile.useDelimiter("\\A").next();
      return str;
    }
    finally
    {
      paramFile.close();
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/ce.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */