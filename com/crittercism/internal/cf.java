package com.crittercism.internal;

import android.support.annotation.NonNull;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

public final class cf
  implements ScheduledExecutorService
{
  private ScheduledThreadPoolExecutor a;
  private String b;
  
  private cf(ScheduledThreadPoolExecutor paramScheduledThreadPoolExecutor, String paramString)
  {
    this.a = paramScheduledThreadPoolExecutor;
    this.b = paramString;
  }
  
  public static ScheduledExecutorService a(String paramString)
  {
    return new cf(new ScheduledThreadPoolExecutor(3, new a(paramString)), paramString);
  }
  
  public static ScheduledExecutorService b(String paramString)
  {
    return new cf(new ScheduledThreadPoolExecutor(1, new a(paramString)), paramString);
  }
  
  public final boolean awaitTermination(long paramLong, TimeUnit paramTimeUnit)
  {
    return this.a.awaitTermination(paramLong, paramTimeUnit);
  }
  
  public final void execute(Runnable paramRunnable)
  {
    this.a.execute(new c(paramRunnable));
  }
  
  @NonNull
  public final <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> paramCollection)
  {
    throw new UnsupportedOperationException();
  }
  
  @NonNull
  public final <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> paramCollection, long paramLong, TimeUnit paramTimeUnit)
  {
    throw new UnsupportedOperationException();
  }
  
  @NonNull
  public final <T> T invokeAny(Collection<? extends Callable<T>> paramCollection)
  {
    throw new UnsupportedOperationException();
  }
  
  public final <T> T invokeAny(Collection<? extends Callable<T>> paramCollection, long paramLong, TimeUnit paramTimeUnit)
  {
    throw new UnsupportedOperationException();
  }
  
  public final boolean isShutdown()
  {
    return this.a.isShutdown();
  }
  
  public final boolean isTerminated()
  {
    return this.a.isTerminated();
  }
  
  @NonNull
  public final ScheduledFuture<?> schedule(Runnable paramRunnable, long paramLong, TimeUnit paramTimeUnit)
  {
    return this.a.schedule(new c(paramRunnable), paramLong, paramTimeUnit);
  }
  
  @NonNull
  public final <V> ScheduledFuture<V> schedule(Callable<V> paramCallable, long paramLong, TimeUnit paramTimeUnit)
  {
    return this.a.schedule(new b(paramCallable), paramLong, paramTimeUnit);
  }
  
  @NonNull
  public final ScheduledFuture<?> scheduleAtFixedRate(Runnable paramRunnable, long paramLong1, long paramLong2, TimeUnit paramTimeUnit)
  {
    return this.a.scheduleAtFixedRate(new c(paramRunnable), paramLong1, paramLong2, paramTimeUnit);
  }
  
  @NonNull
  public final ScheduledFuture<?> scheduleWithFixedDelay(Runnable paramRunnable, long paramLong1, long paramLong2, TimeUnit paramTimeUnit)
  {
    return this.a.scheduleWithFixedDelay(new c(paramRunnable), paramLong1, paramLong2, paramTimeUnit);
  }
  
  public final void shutdown()
  {
    cd.a("Shutting down queue. " + toString());
    this.a.shutdown();
  }
  
  @NonNull
  public final List<Runnable> shutdownNow()
  {
    return this.a.shutdownNow();
  }
  
  @NonNull
  public final Future<?> submit(Runnable paramRunnable)
  {
    return this.a.submit(new c(paramRunnable));
  }
  
  @NonNull
  public final <T> Future<T> submit(Runnable paramRunnable, T paramT)
  {
    return this.a.submit(new c(paramRunnable), paramT);
  }
  
  @NonNull
  public final <T> Future<T> submit(Callable<T> paramCallable)
  {
    return this.a.submit(new b(paramCallable));
  }
  
  public final String toString()
  {
    Object localObject = this.a.getQueue();
    Iterator localIterator = ((BlockingQueue)localObject).iterator();
    for (localObject = "ProtectedExecutorService(" + this.b + ") size = " + ((BlockingQueue)localObject).size() + "\n"; localIterator.hasNext(); localObject = (String)localObject + ((Runnable)localIterator.next()).toString() + "\n") {}
    return (String)localObject;
  }
  
  static final class a
    implements ThreadFactory
  {
    private String a;
    
    public a(String paramString)
    {
      this.a = paramString;
    }
    
    public final Thread newThread(Runnable paramRunnable)
    {
      return new Thread(paramRunnable, this.a);
    }
  }
  
  static final class b<T>
    implements Callable<T>
  {
    private Callable<T> a;
    
    public b(Callable<T> paramCallable)
    {
      this.a = paramCallable;
    }
    
    public final T call()
    {
      try
      {
        Object localObject = this.a.call();
        return (T)localObject;
      }
      catch (ThreadDeath localThreadDeath)
      {
        throw localThreadDeath;
      }
      catch (Throwable localThrowable)
      {
        cd.b(localThrowable);
      }
      return null;
    }
  }
  
  static final class c
    implements Runnable
  {
    private Runnable a;
    private String b;
    
    public c(Runnable paramRunnable)
    {
      this.a = paramRunnable;
    }
    
    public final void run()
    {
      try
      {
        this.a.run();
        return;
      }
      catch (ThreadDeath localThreadDeath)
      {
        throw localThreadDeath;
      }
      catch (Throwable localThrowable)
      {
        cd.b(localThrowable);
      }
    }
    
    public final String toString()
    {
      if (this.b != null) {
        return this.b;
      }
      return this.a.toString();
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/cf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */