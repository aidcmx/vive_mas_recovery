package com.crittercism.internal;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public final class cg
{
  public static final cg a = new cg();
  private ch b = new a((byte)0);
  private ThreadLocal<SimpleDateFormat> c = new ThreadLocal();
  private ThreadLocal<SimpleDateFormat> d = new ThreadLocal();
  
  private SimpleDateFormat b()
  {
    SimpleDateFormat localSimpleDateFormat2 = (SimpleDateFormat)this.c.get();
    SimpleDateFormat localSimpleDateFormat1 = localSimpleDateFormat2;
    if (localSimpleDateFormat2 == null)
    {
      localSimpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
      localSimpleDateFormat1.setTimeZone(TimeZone.getTimeZone("GMT"));
      localSimpleDateFormat1.setLenient(false);
      this.c.set(localSimpleDateFormat1);
    }
    return localSimpleDateFormat1;
  }
  
  public final long a(String paramString)
  {
    try
    {
      Date localDate = b().parse(paramString);
      paramString = localDate;
    }
    catch (ParseException localParseException)
    {
      for (;;)
      {
        SimpleDateFormat localSimpleDateFormat = (SimpleDateFormat)this.d.get();
        Object localObject = localSimpleDateFormat;
        if (localSimpleDateFormat == null)
        {
          localObject = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.US);
          ((SimpleDateFormat)localObject).setTimeZone(TimeZone.getTimeZone("GMT"));
          ((SimpleDateFormat)localObject).setLenient(false);
          this.d.set(localObject);
        }
        paramString = ((SimpleDateFormat)localObject).parse(paramString);
      }
    }
    return paramString.getTime();
  }
  
  public final String a()
  {
    return a(this.b.a());
  }
  
  public final String a(Date paramDate)
  {
    return b().format(paramDate);
  }
  
  final class a
    implements ch
  {
    private a() {}
    
    public final Date a()
    {
      return new Date();
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/cg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */