package com.crittercism.internal;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Build.VERSION;
import android.os.Message;
import android.util.Base64;
import android.view.InputEvent;
import android.view.KeyEvent;
import android.webkit.ClientCertRequest;
import android.webkit.HttpAuthHandler;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public final class ci
  extends WebViewClient
{
  private WebViewClient a;
  private c b;
  private final String c;
  private d d;
  private b e;
  
  public ci(WebViewClient paramWebViewClient, d paramd, c paramc, String paramString)
  {
    this.a = paramWebViewClient;
    this.d = paramd;
    this.b = paramc;
    paramWebViewClient = new StringBuilder();
    paramWebViewClient.append("javascript:(function() {");
    paramWebViewClient.append("  if (typeof(Crittercism) !== \"undefined\") {");
    paramWebViewClient.append("    Crittercism.init({");
    paramWebViewClient.append("      \"platform\": \"android\"});");
    paramWebViewClient.append("  } else {");
    paramWebViewClient.append("    (");
    paramWebViewClient.append("      function() {");
    paramWebViewClient.append("        var parent = document.getElementsByTagName('head').item(0);");
    paramWebViewClient.append("        var script = document.createElement('script');");
    paramWebViewClient.append("        script.type = 'text/javascript';");
    paramWebViewClient.append("        script.innerHTML = window.atob('");
    paramWebViewClient.append(Base64.encodeToString(paramString.getBytes(), 2));
    paramWebViewClient.append("                                     ');");
    paramWebViewClient.append("        parent.appendChild(script)");
    paramWebViewClient.append("      }");
    paramWebViewClient.append("    )();");
    paramWebViewClient.append("    if (typeof(BasicCrittercism) !== \"undefined\") {");
    paramWebViewClient.append("      BasicCrittercism.instrumentOnError({");
    paramWebViewClient.append("        errorCallback: function(errorMsg, stackStr) {");
    paramWebViewClient.append("          _crttr.logError(errorMsg, stackStr);");
    paramWebViewClient.append("          }, ");
    paramWebViewClient.append("        platform: \"android\"");
    paramWebViewClient.append("      });");
    paramWebViewClient.append("      BasicCrittercism.initApm();");
    paramWebViewClient.append("    } ");
    paramWebViewClient.append("  }");
    paramWebViewClient.append("})()");
    this.c = paramWebViewClient.toString();
  }
  
  public final void doUpdateVisitedHistory(WebView paramWebView, String paramString, boolean paramBoolean)
  {
    if (this.a != null) {
      this.a.doUpdateVisitedHistory(paramWebView, paramString, paramBoolean);
    }
  }
  
  public final void onFormResubmission(WebView paramWebView, Message paramMessage1, Message paramMessage2)
  {
    if (this.a != null) {
      this.a.onFormResubmission(paramWebView, paramMessage1, paramMessage2);
    }
  }
  
  public final void onLoadResource(WebView paramWebView, String paramString)
  {
    if (this.a != null) {
      this.a.onLoadResource(paramWebView, paramString);
    }
  }
  
  @TargetApi(23)
  public final void onPageCommitVisible(WebView paramWebView, String paramString)
  {
    if (this.a != null) {
      this.a.onPageCommitVisible(paramWebView, paramString);
    }
  }
  
  public final void onPageFinished(WebView paramWebView, String paramString)
  {
    for (;;)
    {
      try
      {
        cd.d("******** onPageFinished: " + paramString);
        if (Build.VERSION.SDK_INT < 23) {}
      }
      catch (ThreadDeath paramWebView)
      {
        throw paramWebView;
      }
      catch (Throwable localThrowable)
      {
        cd.b(localThrowable);
        continue;
      }
      try
      {
        if (this.e != null)
        {
          this.e.d(System.currentTimeMillis());
          this.d.a(this.e);
          this.e = null;
        }
        if (paramWebView.getSettings().getJavaScriptEnabled()) {
          paramWebView.loadUrl(this.c);
        }
        if (this.a != null) {
          this.a.onPageFinished(paramWebView, paramString);
        }
        return;
      }
      finally {}
    }
  }
  
  public final void onPageStarted(WebView paramWebView, String paramString, Bitmap paramBitmap)
  {
    cd.d("******** onPageStarted: " + paramString);
    if (this.a != null) {
      this.a.onPageStarted(paramWebView, paramString, paramBitmap);
    }
  }
  
  @TargetApi(21)
  public final void onReceivedClientCertRequest(WebView paramWebView, ClientCertRequest paramClientCertRequest)
  {
    if (this.a != null) {
      this.a.onReceivedClientCertRequest(paramWebView, paramClientCertRequest);
    }
  }
  
  public final void onReceivedError(WebView paramWebView, int paramInt, String paramString1, String paramString2)
  {
    if (this.a != null) {
      this.a.onReceivedError(paramWebView, paramInt, paramString1, paramString2);
    }
  }
  
  @TargetApi(23)
  public final void onReceivedError(WebView paramWebView, WebResourceRequest paramWebResourceRequest, WebResourceError paramWebResourceError)
  {
    for (;;)
    {
      try
      {
        if (Build.VERSION.SDK_INT >= 23)
        {
          cd.d("******** onReceivedError (Marshmallow, no http)");
          if (paramWebResourceRequest != null) {
            continue;
          }
          cd.d("null request");
        }
      }
      catch (ThreadDeath paramWebView)
      {
        boolean bool;
        StringBuilder localStringBuilder;
        throw paramWebView;
        Object localObject1 = "not ";
        continue;
        try
        {
          if (this.e == null) {
            continue;
          }
          localObject1 = new bj(bk.e - 1, paramWebResourceError.getErrorCode());
          this.e.k = ((bj)localObject1);
          continue;
        }
        finally {}
      }
      catch (Throwable localThrowable)
      {
        cd.b(localThrowable);
        continue;
      }
      if (this.a != null) {
        this.a.onReceivedError(paramWebView, paramWebResourceRequest, paramWebResourceError);
      }
      return;
      bool = paramWebResourceRequest.isForMainFrame();
      localStringBuilder = new StringBuilder();
      if (!bool) {
        continue;
      }
      localObject1 = "";
      cd.d((String)localObject1 + "main frame");
      if (bool)
      {
        if (paramWebResourceError != null) {
          continue;
        }
        cd.d("null error (no error code)");
      }
    }
  }
  
  public final void onReceivedHttpAuthRequest(WebView paramWebView, HttpAuthHandler paramHttpAuthHandler, String paramString1, String paramString2)
  {
    if (this.a != null) {
      this.a.onReceivedHttpAuthRequest(paramWebView, paramHttpAuthHandler, paramString1, paramString2);
    }
  }
  
  @TargetApi(23)
  public final void onReceivedHttpError(WebView paramWebView, WebResourceRequest paramWebResourceRequest, WebResourceResponse paramWebResourceResponse)
  {
    for (;;)
    {
      try
      {
        if (Build.VERSION.SDK_INT >= 23)
        {
          cd.d("******** onReceivedHttpError (Marshmallow)");
          boolean bool = paramWebResourceRequest.isForMainFrame();
          StringBuilder localStringBuilder = new StringBuilder();
          if (!bool) {
            continue;
          }
          str = "";
          cd.d(str + "main frame");
          if (bool)
          {
            if (paramWebResourceResponse != null) {
              continue;
            }
            cd.d("null response (no status code)");
          }
        }
      }
      catch (ThreadDeath paramWebView)
      {
        String str;
        throw paramWebView;
      }
      catch (Throwable localThrowable)
      {
        cd.b(localThrowable);
        continue;
      }
      if (this.a != null) {
        this.a.onReceivedHttpError(paramWebView, paramWebResourceRequest, paramWebResourceResponse);
      }
      return;
      str = "not ";
      continue;
      try
      {
        if (this.e != null) {
          this.e.i = paramWebResourceResponse.getStatusCode();
        }
      }
      finally {}
    }
  }
  
  @TargetApi(12)
  public final void onReceivedLoginRequest(WebView paramWebView, String paramString1, String paramString2, String paramString3)
  {
    if (this.a != null) {
      this.a.onReceivedLoginRequest(paramWebView, paramString1, paramString2, paramString3);
    }
  }
  
  @TargetApi(8)
  public final void onReceivedSslError(WebView paramWebView, SslErrorHandler paramSslErrorHandler, SslError paramSslError)
  {
    if (this.a != null) {
      this.a.onReceivedSslError(paramWebView, paramSslErrorHandler, paramSslError);
    }
  }
  
  public final void onScaleChanged(WebView paramWebView, float paramFloat1, float paramFloat2)
  {
    if (this.a != null) {
      this.a.onScaleChanged(paramWebView, paramFloat1, paramFloat2);
    }
  }
  
  public final void onTooManyRedirects(WebView paramWebView, Message paramMessage1, Message paramMessage2)
  {
    if (this.a != null) {
      this.a.onTooManyRedirects(paramWebView, paramMessage1, paramMessage2);
    }
  }
  
  @TargetApi(21)
  public final void onUnhandledInputEvent(WebView paramWebView, InputEvent paramInputEvent)
  {
    if (this.a != null) {
      this.a.onUnhandledInputEvent(paramWebView, paramInputEvent);
    }
  }
  
  public final void onUnhandledKeyEvent(WebView paramWebView, KeyEvent paramKeyEvent)
  {
    if (this.a != null) {
      this.a.onUnhandledKeyEvent(paramWebView, paramKeyEvent);
    }
  }
  
  /* Error */
  @TargetApi(21)
  public final WebResourceResponse shouldInterceptRequest(WebView paramWebView, WebResourceRequest paramWebResourceRequest)
  {
    // Byte code:
    //   0: invokestatic 150	java/lang/System:currentTimeMillis	()J
    //   3: lstore_3
    //   4: getstatic 142	android/os/Build$VERSION:SDK_INT	I
    //   7: bipush 23
    //   9: if_icmplt +20 -> 29
    //   12: aload_2
    //   13: ifnull +16 -> 29
    //   16: aload_2
    //   17: invokeinterface 207 1 0
    //   22: istore 5
    //   24: iload 5
    //   26: ifne +47 -> 73
    //   29: aload_0
    //   30: getfield 21	com/crittercism/internal/ci:a	Landroid/webkit/WebViewClient;
    //   33: ifnull +203 -> 236
    //   36: aload_0
    //   37: getfield 21	com/crittercism/internal/ci:a	Landroid/webkit/WebViewClient;
    //   40: aload_1
    //   41: aload_2
    //   42: invokevirtual 283	android/webkit/WebViewClient:shouldInterceptRequest	(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;)Landroid/webkit/WebResourceResponse;
    //   45: astore_1
    //   46: getstatic 142	android/os/Build$VERSION:SDK_INT	I
    //   49: bipush 23
    //   51: if_icmplt +20 -> 71
    //   54: aload_2
    //   55: ifnull +16 -> 71
    //   58: aload_2
    //   59: invokeinterface 207 1 0
    //   64: istore 5
    //   66: iload 5
    //   68: ifne +119 -> 187
    //   71: aload_1
    //   72: areturn
    //   73: ldc_w 285
    //   76: invokestatic 136	com/crittercism/internal/cd:d	(Ljava/lang/String;)V
    //   79: aload_0
    //   80: monitorenter
    //   81: aload_0
    //   82: new 152	com/crittercism/internal/b
    //   85: dup
    //   86: invokespecial 286	com/crittercism/internal/b:<init>	()V
    //   89: putfield 144	com/crittercism/internal/ci:e	Lcom/crittercism/internal/b;
    //   92: aload_0
    //   93: getfield 144	com/crittercism/internal/ci:e	Lcom/crittercism/internal/b;
    //   96: aload_2
    //   97: invokeinterface 290 1 0
    //   102: invokevirtual 293	android/net/Uri:toString	()Ljava/lang/String;
    //   105: invokevirtual 295	com/crittercism/internal/b:a	(Ljava/lang/String;)V
    //   108: aload_0
    //   109: getfield 144	com/crittercism/internal/ci:e	Lcom/crittercism/internal/b;
    //   112: aload_2
    //   113: invokeinterface 298 1 0
    //   118: putfield 301	com/crittercism/internal/b:j	Ljava/lang/String;
    //   121: aload_0
    //   122: getfield 144	com/crittercism/internal/ci:e	Lcom/crittercism/internal/b;
    //   125: lload_3
    //   126: invokevirtual 303	com/crittercism/internal/b:c	(J)V
    //   129: aload_0
    //   130: getfield 144	com/crittercism/internal/ci:e	Lcom/crittercism/internal/b;
    //   133: aload_0
    //   134: getfield 25	com/crittercism/internal/ci:b	Lcom/crittercism/internal/c;
    //   137: getfield 308	com/crittercism/internal/c:a	Landroid/net/ConnectivityManager;
    //   140: invokestatic 313	com/crittercism/internal/a:a	(Landroid/net/ConnectivityManager;)Lcom/crittercism/internal/a;
    //   143: putfield 317	com/crittercism/internal/b:o	Lcom/crittercism/internal/a;
    //   146: invokestatic 321	com/crittercism/internal/an:b	()Z
    //   149: ifeq +13 -> 162
    //   152: aload_0
    //   153: getfield 144	com/crittercism/internal/ci:e	Lcom/crittercism/internal/b;
    //   156: invokestatic 324	com/crittercism/internal/an:a	()Landroid/location/Location;
    //   159: invokevirtual 327	com/crittercism/internal/b:a	(Landroid/location/Location;)V
    //   162: aload_0
    //   163: monitorexit
    //   164: goto -135 -> 29
    //   167: astore 6
    //   169: aload_0
    //   170: monitorexit
    //   171: aload 6
    //   173: athrow
    //   174: astore_1
    //   175: aload_1
    //   176: athrow
    //   177: astore 6
    //   179: aload 6
    //   181: invokestatic 180	com/crittercism/internal/cd:b	(Ljava/lang/Throwable;)V
    //   184: goto -155 -> 29
    //   187: ldc_w 329
    //   190: invokestatic 136	com/crittercism/internal/cd:d	(Ljava/lang/String;)V
    //   193: aload_1
    //   194: ifnull -123 -> 71
    //   197: aload_0
    //   198: monitorenter
    //   199: aload_0
    //   200: getfield 144	com/crittercism/internal/ci:e	Lcom/crittercism/internal/b;
    //   203: ifnull +14 -> 217
    //   206: aload_0
    //   207: getfield 144	com/crittercism/internal/ci:e	Lcom/crittercism/internal/b;
    //   210: aload_1
    //   211: invokevirtual 251	android/webkit/WebResourceResponse:getStatusCode	()I
    //   214: putfield 254	com/crittercism/internal/b:i	I
    //   217: aload_0
    //   218: monitorexit
    //   219: aload_1
    //   220: areturn
    //   221: astore_2
    //   222: aload_0
    //   223: monitorexit
    //   224: aload_2
    //   225: athrow
    //   226: astore_1
    //   227: aload_1
    //   228: athrow
    //   229: astore_2
    //   230: aload_2
    //   231: invokestatic 180	com/crittercism/internal/cd:b	(Ljava/lang/Throwable;)V
    //   234: aload_1
    //   235: areturn
    //   236: aconst_null
    //   237: astore_1
    //   238: goto -192 -> 46
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	241	0	this	ci
    //   0	241	1	paramWebView	WebView
    //   0	241	2	paramWebResourceRequest	WebResourceRequest
    //   3	123	3	l	long
    //   22	45	5	bool	boolean
    //   167	5	6	localObject	Object
    //   177	3	6	localThrowable	Throwable
    // Exception table:
    //   from	to	target	type
    //   81	162	167	finally
    //   162	164	167	finally
    //   169	171	167	finally
    //   4	12	174	java/lang/ThreadDeath
    //   16	24	174	java/lang/ThreadDeath
    //   73	81	174	java/lang/ThreadDeath
    //   171	174	174	java/lang/ThreadDeath
    //   4	12	177	java/lang/Throwable
    //   16	24	177	java/lang/Throwable
    //   73	81	177	java/lang/Throwable
    //   171	174	177	java/lang/Throwable
    //   199	217	221	finally
    //   217	219	221	finally
    //   222	224	221	finally
    //   46	54	226	java/lang/ThreadDeath
    //   58	66	226	java/lang/ThreadDeath
    //   187	193	226	java/lang/ThreadDeath
    //   197	199	226	java/lang/ThreadDeath
    //   224	226	226	java/lang/ThreadDeath
    //   46	54	229	java/lang/Throwable
    //   58	66	229	java/lang/Throwable
    //   187	193	229	java/lang/Throwable
    //   197	199	229	java/lang/Throwable
    //   224	226	229	java/lang/Throwable
  }
  
  @TargetApi(11)
  public final WebResourceResponse shouldInterceptRequest(WebView paramWebView, String paramString)
  {
    if (this.a != null) {
      return this.a.shouldInterceptRequest(paramWebView, paramString);
    }
    return null;
  }
  
  public final boolean shouldOverrideKeyEvent(WebView paramWebView, KeyEvent paramKeyEvent)
  {
    if (this.a != null) {
      return this.a.shouldOverrideKeyEvent(paramWebView, paramKeyEvent);
    }
    return false;
  }
  
  public final boolean shouldOverrideUrlLoading(WebView paramWebView, String paramString)
  {
    cd.d("******** shouldOverrideUrlLoading: " + paramString);
    if (this.a != null) {
      return this.a.shouldOverrideUrlLoading(paramWebView, paramString);
    }
    return false;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/ci.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */