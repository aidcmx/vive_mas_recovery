package com.crittercism.internal;

import android.os.Build.VERSION;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public final class cj
{
  public cj()
  {
    if ((Build.VERSION.SDK_INT < 14) || (Build.VERSION.SDK_INT > 23)) {
      throw new bh("API Level " + Build.VERSION.SDK_INT + " does not supportWebView monitoring. Skipping instrumentation.");
    }
  }
  
  public static WebViewClient a(WebView paramWebView)
  {
    try
    {
      paramWebView = e.a(WebView.class, Class.forName("android.webkit.CallbackProxy"), true).get(paramWebView);
      paramWebView = (WebViewClient)paramWebView.getClass().getMethod("getWebViewClient", new Class[0]).invoke(paramWebView, new Object[0]);
      return paramWebView;
    }
    catch (ClassNotFoundException paramWebView)
    {
      throw new bh(paramWebView);
    }
    catch (InvocationTargetException paramWebView)
    {
      throw new bh(paramWebView);
    }
    catch (NoSuchMethodException paramWebView)
    {
      throw new bh(paramWebView);
    }
    catch (IllegalAccessException paramWebView)
    {
      throw new bh(paramWebView);
    }
    catch (SecurityException paramWebView)
    {
      throw new bh(paramWebView);
    }
  }
  
  public static WebViewClient b(WebView paramWebView)
  {
    try
    {
      paramWebView = WebView.class.getMethod("getWebViewProvider", new Class[0]).invoke(paramWebView, new Object[0]);
      paramWebView = (WebViewClient)paramWebView.getClass().getMethod("getWebViewClient", new Class[0]).invoke(paramWebView, new Object[0]);
      return paramWebView;
    }
    catch (InvocationTargetException paramWebView)
    {
      throw new bh(paramWebView);
    }
    catch (NoSuchMethodException paramWebView)
    {
      throw new bh(paramWebView);
    }
    catch (IllegalAccessException paramWebView)
    {
      throw new bh(paramWebView);
    }
    catch (SecurityException paramWebView)
    {
      throw new bh(paramWebView);
    }
  }
  
  public static WebViewClient c(WebView paramWebView)
  {
    try
    {
      paramWebView = WebView.class.getMethod("getWebViewProvider", new Class[0]).invoke(paramWebView, new Object[0]);
      Field localField = paramWebView.getClass().getDeclaredField("mContentsClientAdapter");
      localField.setAccessible(true);
      paramWebView = localField.get(paramWebView);
      localField = paramWebView.getClass().getDeclaredField("mWebViewClient");
      localField.setAccessible(true);
      paramWebView = (WebViewClient)localField.get(paramWebView);
      return paramWebView;
    }
    catch (InvocationTargetException paramWebView)
    {
      throw new bh(paramWebView);
    }
    catch (NoSuchMethodException paramWebView)
    {
      throw new bh(paramWebView);
    }
    catch (NoSuchFieldException paramWebView)
    {
      throw new bh(paramWebView);
    }
    catch (IllegalAccessException paramWebView)
    {
      throw new bh(paramWebView);
    }
    catch (SecurityException paramWebView)
    {
      throw new bh(paramWebView);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/cj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */