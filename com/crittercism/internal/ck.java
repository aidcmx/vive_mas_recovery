package com.crittercism.internal;

import android.content.Context;
import java.io.IOException;

public final class ck
{
  public am a;
  public d b;
  public c c;
  public String d;
  
  public ck(am paramam, d paramd, Context paramContext)
  {
    this.a = paramam;
    this.b = paramd;
    this.c = new c(paramContext);
    this.d = a(paramContext);
  }
  
  private static String a(Context paramContext)
  {
    try
    {
      paramContext = ce.a(paramContext, "www/error.js");
      return paramContext;
    }
    catch (IOException paramContext)
    {
      cd.b(paramContext);
    }
    return "";
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/ck.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */