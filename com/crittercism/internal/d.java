package com.crittercism.internal;

import android.support.annotation.NonNull;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.RejectedExecutionException;

public final class d
{
  final av<at> a;
  final av<b> b;
  final ap c;
  private final List<String> d;
  private final List<String> e;
  private final Executor f;
  
  private d(@NonNull Executor paramExecutor, @NonNull List<String> paramList1, @NonNull List<String> paramList2, @NonNull av<b> paramav, @NonNull av<at> paramav1, @NonNull ap paramap)
  {
    this.f = paramExecutor;
    this.b = paramav;
    this.a = paramav1;
    this.d = new LinkedList(paramList1);
    this.d.remove(null);
    this.e = new LinkedList(paramList2);
    this.e.remove(null);
    this.c = paramap;
  }
  
  private boolean a(String paramString)
  {
    synchronized (this.e)
    {
      Iterator localIterator = this.e.iterator();
      while (localIterator.hasNext()) {
        if (paramString.contains((String)localIterator.next())) {
          return false;
        }
      }
      return true;
    }
  }
  
  private boolean b(b arg1)
  {
    String str = ???.a();
    synchronized (this.d)
    {
      Iterator localIterator = this.d.iterator();
      while (localIterator.hasNext()) {
        if (str.contains((String)localIterator.next())) {
          return true;
        }
      }
      return false;
    }
  }
  
  @Deprecated
  public final void a(b paramb)
  {
    a(paramb, b.c.e);
  }
  
  public final void a(final b paramb, b.c paramc)
  {
    if (paramb.c) {}
    do
    {
      return;
      paramb.c = true;
      paramb.f = paramc;
    } while (b(paramb));
    paramc = paramb.a();
    if (a(paramc))
    {
      int i = paramc.indexOf("?");
      if (i != -1) {
        paramb.a(paramc.substring(0, i));
      }
    }
    try
    {
      this.f.execute(new Runnable()
      {
        public final void run()
        {
          if (((Boolean)d.this.c.a(ap.a)).booleanValue())
          {
            paramb.e = ((Float)d.this.c.a(ap.d)).floatValue();
            if (((Boolean)d.this.c.a(ap.F)).booleanValue())
            {
              Object localObject = paramb;
              localObject = new at(at.b.c, ((b)localObject).e());
              d.this.a.a((bf)localObject);
            }
            d.this.b.a(paramb);
          }
        }
      });
      return;
    }
    catch (RejectedExecutionException paramb) {}
  }
  
  public static final class a
  {
    public Executor a;
    public List<String> b = new LinkedList();
    public List<String> c = new LinkedList();
    public av<b> d = new bd();
    public av<at> e = new bd();
    public ap f;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/d.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */