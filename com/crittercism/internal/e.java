package com.crittercism.internal;

import java.lang.reflect.Field;

public final class e
{
  public static <C, F> F a(Field paramField, C paramC)
  {
    if (paramField == null) {}
    while (paramField == null) {
      return null;
    }
    paramField.setAccessible(true);
    try
    {
      paramField = paramField.get(paramC);
      return paramField;
    }
    catch (ThreadDeath paramField)
    {
      throw paramField;
    }
    catch (Throwable paramField)
    {
      throw new bh("Unable to get value of field", paramField);
    }
  }
  
  public static Field a(Class<?> paramClass1, Class<?> paramClass2, boolean paramBoolean)
  {
    Field[] arrayOfField = paramClass1.getDeclaredFields();
    paramClass1 = null;
    int i = 0;
    while (i < arrayOfField.length)
    {
      Object localObject = paramClass1;
      if (paramClass2.isAssignableFrom(arrayOfField[i].getType()))
      {
        if (paramClass1 != null) {
          throw new bh("Field is ambiguous: " + paramClass1.getName() + ", " + arrayOfField[i].getName());
        }
        localObject = arrayOfField[i];
      }
      i += 1;
      paramClass1 = (Class<?>)localObject;
    }
    if (paramClass1 == null)
    {
      if (paramBoolean) {
        throw new bh("Could not find field matching type: " + paramClass2.getName());
      }
    }
    else {
      paramClass1.setAccessible(true);
    }
    return paramClass1;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/e.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */