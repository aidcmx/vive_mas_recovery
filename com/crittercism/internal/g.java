package com.crittercism.internal;

import java.io.OutputStream;

public final class g
  extends OutputStream
  implements ac
{
  OutputStream a;
  private u b;
  private b c;
  private v d;
  
  public g(u paramu, OutputStream paramOutputStream)
  {
    if (paramu == null) {
      throw new NullPointerException("socket was null");
    }
    if (paramOutputStream == null) {
      throw new NullPointerException("output stream was null");
    }
    this.b = paramu;
    this.a = paramOutputStream;
    this.d = b();
    if (this.d == null) {
      throw new NullPointerException("parser was null");
    }
  }
  
  private void a(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    try
    {
      this.d.a(paramArrayOfByte, paramInt1, paramInt2);
      return;
    }
    catch (ThreadDeath paramArrayOfByte)
    {
      throw paramArrayOfByte;
    }
    catch (Throwable paramArrayOfByte)
    {
      cd.b(paramArrayOfByte);
      this.d = aj.d;
    }
  }
  
  private b d()
  {
    if (this.c == null) {
      this.c = this.b.a();
    }
    return this.c;
  }
  
  public final v a()
  {
    return this.d;
  }
  
  public final void a(int paramInt) {}
  
  public final void a(v paramv)
  {
    this.d = paramv;
  }
  
  public final void a(String paramString)
  {
    b localb = d();
    if (localb != null) {
      localb.b(paramString);
    }
  }
  
  public final void a(String paramString1, String paramString2)
  {
    b localb = d();
    localb.c();
    localb.j = paramString1;
    localb.n = null;
    paramString1 = localb.m;
    if (paramString2 != null) {
      paramString1.c = paramString2;
    }
    this.b.a(localb);
  }
  
  public final v b()
  {
    return new ae(this);
  }
  
  public final void b(int paramInt)
  {
    b localb = this.c;
    this.c = null;
    if (localb != null) {
      localb.b(paramInt);
    }
  }
  
  public final String c()
  {
    b localb = d();
    String str = null;
    if (localb != null) {
      str = localb.j;
    }
    return str;
  }
  
  public final void close()
  {
    this.a.close();
  }
  
  public final void flush()
  {
    this.a.flush();
  }
  
  public final void write(int paramInt)
  {
    this.a.write(paramInt);
    try
    {
      this.d.a(paramInt);
      return;
    }
    catch (ThreadDeath localThreadDeath)
    {
      throw localThreadDeath;
    }
    catch (Throwable localThrowable)
    {
      cd.b(localThrowable);
      this.d = aj.d;
    }
  }
  
  public final void write(byte[] paramArrayOfByte)
  {
    this.a.write(paramArrayOfByte);
    if (paramArrayOfByte != null) {
      a(paramArrayOfByte, 0, paramArrayOfByte.length);
    }
  }
  
  public final void write(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    this.a.write(paramArrayOfByte, paramInt1, paramInt2);
    if (paramArrayOfByte != null) {
      a(paramArrayOfByte, paramInt1, paramInt2);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/g.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */