package com.crittercism.internal;

import java.io.IOException;
import java.io.InputStream;

public final class h
  extends InputStream
  implements ac
{
  b a;
  InputStream b;
  v c;
  private u d;
  private d e;
  
  public h(u paramu, InputStream paramInputStream, d paramd)
  {
    if (paramu == null) {
      throw new NullPointerException("socket was null");
    }
    if (paramInputStream == null) {
      throw new NullPointerException("delegate was null");
    }
    if (paramd == null) {
      throw new NullPointerException("dispatch was null");
    }
    this.d = paramu;
    this.b = paramInputStream;
    this.e = paramd;
    this.c = b();
    if (this.c == null) {
      throw new NullPointerException("parser was null");
    }
  }
  
  private void a(Exception paramException)
  {
    try
    {
      b localb = d();
      localb.a(paramException);
      this.e.a(localb, b.c.h);
      return;
    }
    catch (ThreadDeath paramException)
    {
      throw paramException;
    }
    catch (Throwable paramException)
    {
      cd.b(paramException);
      return;
    }
    catch (IllegalStateException paramException) {}
  }
  
  private void a(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    try
    {
      this.c.a(paramArrayOfByte, paramInt1, paramInt2);
      return;
    }
    catch (ThreadDeath paramArrayOfByte)
    {
      throw paramArrayOfByte;
    }
    catch (IllegalStateException paramArrayOfByte)
    {
      this.c = aj.d;
      return;
    }
    catch (Throwable paramArrayOfByte)
    {
      this.c = aj.d;
      cd.b(paramArrayOfByte);
    }
  }
  
  private b d()
  {
    if (this.a == null) {
      this.a = this.d.b();
    }
    if (this.a == null) {
      throw new IllegalStateException("No statistics were queued up.");
    }
    return this.a;
  }
  
  public final v a()
  {
    return this.c;
  }
  
  public final void a(int paramInt)
  {
    b localb = d();
    localb.d();
    localb.i = paramInt;
  }
  
  public final void a(v paramv)
  {
    this.c = paramv;
  }
  
  public final void a(String paramString) {}
  
  public final void a(String paramString1, String paramString2) {}
  
  public final int available()
  {
    return this.b.available();
  }
  
  public final v b()
  {
    return new ag(this);
  }
  
  public final void b(int paramInt)
  {
    Object localObject1 = null;
    Object localObject2 = null;
    if (this.a != null)
    {
      int i = this.a.i;
      localObject1 = localObject2;
      if (i >= 100)
      {
        localObject1 = localObject2;
        if (i < 200)
        {
          localObject1 = new b(this.a.a());
          ((b)localObject1).c(this.a.a);
          ((b)localObject1).b(this.a.h);
          ((b)localObject1).j = this.a.j;
        }
      }
      this.a.a(paramInt);
      this.e.a(this.a, b.c.g);
    }
    this.a = ((b)localObject1);
  }
  
  public final String c()
  {
    return d().j;
  }
  
  public final void close()
  {
    try
    {
      this.c.f();
      this.b.close();
      return;
    }
    catch (ThreadDeath localThreadDeath)
    {
      throw localThreadDeath;
    }
    catch (Throwable localThrowable)
    {
      for (;;)
      {
        cd.b(localThrowable);
      }
    }
  }
  
  public final void mark(int paramInt)
  {
    this.b.mark(paramInt);
  }
  
  public final boolean markSupported()
  {
    return this.b.markSupported();
  }
  
  public final int read()
  {
    try
    {
      int i = this.b.read();
      return i;
    }
    catch (IOException localIOException)
    {
      try
      {
        this.c.a(i);
        return i;
      }
      catch (ThreadDeath localThreadDeath)
      {
        throw localThreadDeath;
      }
      catch (IllegalStateException localIllegalStateException)
      {
        this.c = aj.d;
        return i;
      }
      catch (Throwable localThrowable)
      {
        this.c = aj.d;
        cd.b(localThrowable);
      }
      localIOException = localIOException;
      a(localIOException);
      throw localIOException;
    }
  }
  
  public final int read(byte[] paramArrayOfByte)
  {
    try
    {
      int i = this.b.read(paramArrayOfByte);
      a(paramArrayOfByte, 0, i);
      return i;
    }
    catch (IOException paramArrayOfByte)
    {
      a(paramArrayOfByte);
      throw paramArrayOfByte;
    }
  }
  
  public final int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    try
    {
      paramInt2 = this.b.read(paramArrayOfByte, paramInt1, paramInt2);
      a(paramArrayOfByte, paramInt1, paramInt2);
      return paramInt2;
    }
    catch (IOException paramArrayOfByte)
    {
      a(paramArrayOfByte);
      throw paramArrayOfByte;
    }
  }
  
  public final void reset()
  {
    try
    {
      this.b.reset();
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public final long skip(long paramLong)
  {
    return this.b.skip(paramLong);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/h.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */