package com.crittercism.internal;

import android.os.Build.VERSION;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import java.security.Provider.Service;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLContextSpi;

public final class i
  extends Provider.Service
{
  public static final String[] a = { "Default", "SSL", "TLSv1.1", "TLSv1.2", "SSLv3", "TLSv1", "TLS" };
  private d b;
  private c c;
  private Provider.Service d;
  
  private i(Provider.Service paramService, d paramd, c paramc)
  {
    super(paramService.getProvider(), paramService.getType(), paramService.getAlgorithm(), paramService.getClassName(), null, null);
    this.b = paramd;
    this.c = paramc;
    this.d = paramService;
  }
  
  private static i a(Provider.Service paramService, d paramd, c paramc)
  {
    paramc = new i(paramService, paramd, paramc);
    try
    {
      Field[] arrayOfField = Provider.Service.class.getFields();
      int i = 0;
      for (;;)
      {
        paramd = paramc;
        if (i >= arrayOfField.length) {
          break;
        }
        arrayOfField[i].setAccessible(true);
        arrayOfField[i].set(paramc, arrayOfField[i].get(paramService));
        i += 1;
      }
      return paramd;
    }
    catch (Exception paramService)
    {
      paramd = null;
    }
  }
  
  private static Provider a()
  {
    Provider localProvider = null;
    try
    {
      SSLContext localSSLContext = SSLContext.getInstance("TLS");
      if (localSSLContext != null) {
        localProvider = localSSLContext.getProvider();
      }
      return localProvider;
    }
    catch (NoSuchAlgorithmException localNoSuchAlgorithmException) {}
    return null;
  }
  
  public static boolean a(d paramd, c paramc)
  {
    int i = 0;
    if (Build.VERSION.SDK_INT < 17) {}
    Provider localProvider;
    do
    {
      do
      {
        return false;
      } while (!j.a());
      localProvider = a();
    } while (localProvider == null);
    boolean bool2;
    for (boolean bool1 = false; i < a.length; bool1 = bool2)
    {
      Object localObject = localProvider.getService("SSLContext", a[i]);
      bool2 = bool1;
      if (localObject != null)
      {
        bool2 = bool1;
        if (!(localObject instanceof i))
        {
          localObject = a((Provider.Service)localObject, paramd, paramc);
          bool2 = bool1;
          if (localObject != null) {
            bool2 = bool1 | ((i)localObject).b();
          }
        }
      }
      i += 1;
    }
    return bool1;
  }
  
  private boolean b()
  {
    Provider localProvider = getProvider();
    if (localProvider == null) {
      return false;
    }
    try
    {
      Method localMethod = Provider.class.getDeclaredMethod("putService", new Class[] { Provider.Service.class });
      localMethod.setAccessible(true);
      localMethod.invoke(localProvider, new Object[] { this });
      localProvider.put("SSLContext.DummySSLAlgorithm", getClassName());
      localProvider.remove(getType() + "." + getAlgorithm());
      localProvider.remove("SSLContext.DummySSLAlgorithm");
      return true;
    }
    catch (Exception localException) {}
    return false;
  }
  
  public final Object newInstance(Object paramObject)
  {
    Object localObject = super.newInstance(paramObject);
    paramObject = localObject;
    try
    {
      if ((localObject instanceof SSLContextSpi))
      {
        j localj = j.a((SSLContextSpi)localObject, this.b, this.c);
        paramObject = localObject;
        if (localj != null) {
          paramObject = localj;
        }
      }
      return paramObject;
    }
    catch (ThreadDeath paramObject)
    {
      throw ((Throwable)paramObject);
    }
    catch (Throwable paramObject)
    {
      cd.b((Throwable)paramObject);
    }
    return localObject;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/i.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */