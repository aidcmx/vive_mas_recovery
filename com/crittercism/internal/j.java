package com.crittercism.internal;

import android.os.Build.VERSION;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.KeyManagementException;
import java.security.SecureRandom;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContextSpi;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSessionContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;

public final class j
  extends SSLContextSpi
{
  private static Method[] a = new Method[7];
  private static boolean b = false;
  private SSLContextSpi c;
  private d d;
  private c e;
  
  static
  {
    for (;;)
    {
      int i;
      try
      {
        a[0] = SSLContextSpi.class.getDeclaredMethod("engineCreateSSLEngine", new Class[0]);
        a[1] = SSLContextSpi.class.getDeclaredMethod("engineCreateSSLEngine", new Class[] { String.class, Integer.TYPE });
        a[2] = SSLContextSpi.class.getDeclaredMethod("engineGetClientSessionContext", new Class[0]);
        a[3] = SSLContextSpi.class.getDeclaredMethod("engineGetServerSessionContext", new Class[0]);
        a[4] = SSLContextSpi.class.getDeclaredMethod("engineGetServerSocketFactory", new Class[0]);
        a[5] = SSLContextSpi.class.getDeclaredMethod("engineGetSocketFactory", new Class[0]);
        a[6] = SSLContextSpi.class.getDeclaredMethod("engineInit", new Class[] { KeyManager[].class, TrustManager[].class, SecureRandom.class });
        Object localObject1 = a;
        i = 0;
        if (i < localObject1.length)
        {
          Object localObject2 = localObject1[i];
          if (localObject2 != null) {
            ((AccessibleObject)localObject2).setAccessible(true);
          }
        }
        else
        {
          localObject1 = new j(new j(), null, null);
          ((j)localObject1).engineCreateSSLEngine();
          ((j)localObject1).engineCreateSSLEngine(null, 0);
          ((j)localObject1).engineGetClientSessionContext();
          ((j)localObject1).engineGetServerSessionContext();
          ((j)localObject1).engineGetServerSocketFactory();
          ((j)localObject1).engineGetSocketFactory();
          ((j)localObject1).engineInit(null, null, null);
          b = true;
          return;
        }
      }
      catch (Throwable localThrowable)
      {
        cd.a(localThrowable);
        b = false;
        return;
      }
      i += 1;
    }
  }
  
  private j() {}
  
  private j(SSLContextSpi paramSSLContextSpi, d paramd, c paramc)
  {
    this.c = paramSSLContextSpi;
    this.d = paramd;
    this.e = paramc;
  }
  
  public static j a(SSLContextSpi paramSSLContextSpi, d paramd, c paramc)
  {
    if (!b) {
      return null;
    }
    return new j(paramSSLContextSpi, paramd, paramc);
  }
  
  private <T> T a(int paramInt, Object... paramVarArgs)
  {
    if (this.c == null) {
      return null;
    }
    try
    {
      paramVarArgs = a[paramInt].invoke(this.c, paramVarArgs);
      return paramVarArgs;
    }
    catch (IllegalArgumentException paramVarArgs)
    {
      throw new bg(paramVarArgs);
    }
    catch (IllegalAccessException paramVarArgs)
    {
      throw new bg(paramVarArgs);
    }
    catch (InvocationTargetException paramVarArgs)
    {
      Throwable localThrowable = paramVarArgs.getTargetException();
      if (localThrowable == null) {
        throw new bg(paramVarArgs);
      }
      if ((localThrowable instanceof Exception)) {
        throw ((Exception)localThrowable);
      }
      if ((localThrowable instanceof Error)) {
        throw ((Error)localThrowable);
      }
      throw new bg(paramVarArgs);
    }
    catch (ClassCastException paramVarArgs)
    {
      throw new bg(paramVarArgs);
    }
  }
  
  private <T> T a(Object... paramVarArgs)
  {
    try
    {
      paramVarArgs = a(6, paramVarArgs);
      return paramVarArgs;
    }
    catch (RuntimeException paramVarArgs)
    {
      throw paramVarArgs;
    }
    catch (KeyManagementException paramVarArgs)
    {
      throw paramVarArgs;
    }
    catch (Exception paramVarArgs)
    {
      throw new bg(paramVarArgs);
    }
  }
  
  public static boolean a()
  {
    return b;
  }
  
  private <T> T b(int paramInt, Object... paramVarArgs)
  {
    try
    {
      paramVarArgs = a(paramInt, paramVarArgs);
      return paramVarArgs;
    }
    catch (RuntimeException paramVarArgs)
    {
      throw paramVarArgs;
    }
    catch (Exception paramVarArgs)
    {
      throw new bg(paramVarArgs);
    }
  }
  
  protected final SSLEngine engineCreateSSLEngine()
  {
    return (SSLEngine)b(0, new Object[0]);
  }
  
  protected final SSLEngine engineCreateSSLEngine(String paramString, int paramInt)
  {
    return (SSLEngine)b(1, new Object[] { paramString, Integer.valueOf(paramInt) });
  }
  
  protected final SSLSessionContext engineGetClientSessionContext()
  {
    return (SSLSessionContext)b(2, new Object[0]);
  }
  
  protected final SSLSessionContext engineGetServerSessionContext()
  {
    return (SSLSessionContext)b(3, new Object[0]);
  }
  
  protected final SSLServerSocketFactory engineGetServerSocketFactory()
  {
    return (SSLServerSocketFactory)b(4, new Object[0]);
  }
  
  protected final SSLSocketFactory engineGetSocketFactory()
  {
    SSLSocketFactory localSSLSocketFactory = (SSLSocketFactory)b(5, new Object[0]);
    if (localSSLSocketFactory != null) {
      try
      {
        if (Build.VERSION.SDK_INT >= 19) {
          return new m(localSSLSocketFactory, this.d, this.e);
        }
        if (Build.VERSION.SDK_INT >= 14)
        {
          l locall = new l(localSSLSocketFactory, this.d, this.e);
          return locall;
        }
      }
      catch (ThreadDeath localThreadDeath)
      {
        throw localThreadDeath;
      }
      catch (Throwable localThrowable)
      {
        cd.b(localThrowable);
      }
    }
    return localThreadDeath;
  }
  
  protected final void engineInit(KeyManager[] paramArrayOfKeyManager, TrustManager[] paramArrayOfTrustManager, SecureRandom paramSecureRandom)
  {
    a(new Object[] { paramArrayOfKeyManager, paramArrayOfTrustManager, paramSecureRandom });
  }
  
  public final boolean equals(Object paramObject)
  {
    return this.c.equals(paramObject);
  }
  
  public final int hashCode()
  {
    return this.c.hashCode();
  }
  
  public final String toString()
  {
    return this.c.toString();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/j.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */