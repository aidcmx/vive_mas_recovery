package com.crittercism.internal;

import com.android.org.conscrypt.SSLParametersImpl;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.Socket;
import javax.net.ssl.SSLSocketFactory;

public final class m
  extends k
{
  private static boolean a = false;
  private static SSLSocketFactory b;
  private SSLParametersImpl c;
  private d d;
  private SSLSocketFactory delegate;
  private c e;
  
  public m(SSLSocketFactory paramSSLSocketFactory, d paramd, c paramc)
  {
    this.delegate = paramSSLSocketFactory;
    this.d = paramd;
    this.e = paramc;
    this.c = a(paramSSLSocketFactory);
  }
  
  private static SSLParametersImpl a(SSLParametersImpl paramSSLParametersImpl)
  {
    try
    {
      paramSSLParametersImpl = b(paramSSLParametersImpl);
      return paramSSLParametersImpl;
    }
    catch (bh paramSSLParametersImpl) {}
    return null;
  }
  
  private static SSLParametersImpl a(SSLSocketFactory paramSSLSocketFactory)
  {
    try
    {
      paramSSLSocketFactory = (SSLParametersImpl)e.a(e.a(paramSSLSocketFactory.getClass(), SSLParametersImpl.class, false), paramSSLSocketFactory);
      return a(paramSSLSocketFactory);
    }
    catch (bh paramSSLSocketFactory)
    {
      for (;;)
      {
        cd.b(paramSSLSocketFactory);
        paramSSLSocketFactory = null;
      }
    }
  }
  
  /* Error */
  public static boolean a(d paramd, c paramc)
  {
    // Byte code:
    //   0: getstatic 19	com/crittercism/internal/m:a	Z
    //   3: ifeq +7 -> 10
    //   6: getstatic 19	com/crittercism/internal/m:a	Z
    //   9: ireturn
    //   10: invokestatic 76	javax/net/ssl/HttpsURLConnection:getDefaultSSLSocketFactory	()Ljavax/net/ssl/SSLSocketFactory;
    //   13: astore_2
    //   14: new 2	com/crittercism/internal/m
    //   17: dup
    //   18: aload_2
    //   19: aload_0
    //   20: aload_1
    //   21: invokespecial 78	com/crittercism/internal/m:<init>	(Ljavax/net/ssl/SSLSocketFactory;Lcom/crittercism/internal/d;Lcom/crittercism/internal/c;)V
    //   24: astore_0
    //   25: aload_0
    //   26: invokevirtual 84	javax/net/ssl/SSLSocketFactory:createSocket	()Ljava/net/Socket;
    //   29: astore_1
    //   30: aload_0
    //   31: aload_1
    //   32: ldc 86
    //   34: sipush 6895
    //   37: iconst_1
    //   38: invokevirtual 89	javax/net/ssl/SSLSocketFactory:createSocket	(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;
    //   41: pop
    //   42: aload_0
    //   43: invokestatic 93	javax/net/ssl/HttpsURLConnection:setDefaultSSLSocketFactory	(Ljavax/net/ssl/SSLSocketFactory;)V
    //   46: aload_2
    //   47: putstatic 95	com/crittercism/internal/m:b	Ljavax/net/ssl/SSLSocketFactory;
    //   50: iconst_1
    //   51: putstatic 19	com/crittercism/internal/m:a	Z
    //   54: iconst_1
    //   55: ireturn
    //   56: astore_0
    //   57: aload_0
    //   58: athrow
    //   59: astore_0
    //   60: ldc 97
    //   62: aload_0
    //   63: invokestatic 100	com/crittercism/internal/cd:a	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   66: iconst_0
    //   67: ireturn
    //   68: astore_1
    //   69: goto -27 -> 42
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	72	0	paramd	d
    //   0	72	1	paramc	c
    //   13	34	2	localSSLSocketFactory	SSLSocketFactory
    // Exception table:
    //   from	to	target	type
    //   14	30	56	java/lang/ThreadDeath
    //   30	42	56	java/lang/ThreadDeath
    //   14	30	59	java/lang/Throwable
    //   30	42	59	java/lang/Throwable
    //   30	42	68	java/net/SocketException
  }
  
  private static SSLParametersImpl b(SSLParametersImpl paramSSLParametersImpl)
  {
    try
    {
      Method localMethod = SSLParametersImpl.class.getDeclaredMethod("clone", new Class[0]);
      localMethod.setAccessible(true);
      paramSSLParametersImpl = (SSLParametersImpl)localMethod.invoke(paramSSLParametersImpl, new Object[0]);
      return paramSSLParametersImpl;
    }
    catch (NoSuchMethodException paramSSLParametersImpl)
    {
      throw new bh(paramSSLParametersImpl);
    }
    catch (IllegalArgumentException paramSSLParametersImpl)
    {
      throw new bh(paramSSLParametersImpl);
    }
    catch (IllegalAccessException paramSSLParametersImpl)
    {
      throw new bh(paramSSLParametersImpl);
    }
    catch (InvocationTargetException paramSSLParametersImpl)
    {
      throw new bh(paramSSLParametersImpl);
    }
  }
  
  public final SSLSocketFactory a()
  {
    return this.delegate;
  }
  
  public final Socket createSocket()
  {
    return new o(this.d, this.e, a(this.c));
  }
  
  public final Socket createSocket(String paramString, int paramInt)
  {
    return new o(this.d, this.e, paramString, paramInt, a(this.c));
  }
  
  public final Socket createSocket(String paramString, int paramInt1, InetAddress paramInetAddress, int paramInt2)
  {
    return new o(this.d, this.e, paramString, paramInt1, paramInetAddress, paramInt2, a(this.c));
  }
  
  public final Socket createSocket(InetAddress paramInetAddress, int paramInt)
  {
    return new o(this.d, this.e, paramInetAddress, paramInt, a(this.c));
  }
  
  public final Socket createSocket(InetAddress paramInetAddress1, int paramInt1, InetAddress paramInetAddress2, int paramInt2)
  {
    return new o(this.d, this.e, paramInetAddress1, paramInt1, paramInetAddress2, paramInt2, a(this.c));
  }
  
  public final Socket createSocket(Socket paramSocket, String paramString, int paramInt, boolean paramBoolean)
  {
    SSLParametersImpl localSSLParametersImpl = a(this.c);
    return new q(this.d, this.e, paramSocket, paramString, paramInt, paramBoolean, localSSLParametersImpl);
  }
  
  public final String[] getDefaultCipherSuites()
  {
    return this.delegate.getDefaultCipherSuites();
  }
  
  public final String[] getSupportedCipherSuites()
  {
    return this.delegate.getSupportedCipherSuites();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/m.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */