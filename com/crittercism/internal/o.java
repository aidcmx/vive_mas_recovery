package com.crittercism.internal;

import com.android.org.conscrypt.OpenSSLSocketImpl;
import com.android.org.conscrypt.SSLParametersImpl;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;

public final class o
  extends OpenSSLSocketImpl
  implements u
{
  private r a;
  
  protected o(d paramd, c paramc, SSLParametersImpl paramSSLParametersImpl)
  {
    super(paramSSLParametersImpl);
    this.a = new r(f.a.b, paramd, paramc);
  }
  
  protected o(d paramd, c paramc, String paramString, int paramInt, SSLParametersImpl paramSSLParametersImpl)
  {
    super(paramString, paramInt, paramSSLParametersImpl);
    this.a = new r(f.a.b, paramd, paramc);
  }
  
  protected o(d paramd, c paramc, String paramString, int paramInt1, InetAddress paramInetAddress, int paramInt2, SSLParametersImpl paramSSLParametersImpl)
  {
    super(paramString, paramInt1, paramInetAddress, paramInt2, paramSSLParametersImpl);
    this.a = new r(f.a.b, paramd, paramc);
  }
  
  protected o(d paramd, c paramc, InetAddress paramInetAddress, int paramInt, SSLParametersImpl paramSSLParametersImpl)
  {
    super(paramInetAddress, paramInt, paramSSLParametersImpl);
    this.a = new r(f.a.b, paramd, paramc);
  }
  
  protected o(d paramd, c paramc, InetAddress paramInetAddress1, int paramInt1, InetAddress paramInetAddress2, int paramInt2, SSLParametersImpl paramSSLParametersImpl)
  {
    super(paramInetAddress1, paramInt1, paramInetAddress2, paramInt2, paramSSLParametersImpl);
    this.a = new r(f.a.b, paramd, paramc);
  }
  
  public final b a()
  {
    InetAddress localInetAddress = getInetAddress();
    return this.a.a(localInetAddress);
  }
  
  public final void a(b paramb)
  {
    this.a.a(paramb);
  }
  
  public final b b()
  {
    return this.a.b();
  }
  
  public final void close()
  {
    super.close();
    this.a.a();
  }
  
  public final InputStream getInputStream()
  {
    InputStream localInputStream = super.getInputStream();
    return this.a.a(this, localInputStream);
  }
  
  public final OutputStream getOutputStream()
  {
    OutputStream localOutputStream = super.getOutputStream();
    return this.a.a(this, localOutputStream);
  }
  
  public final int getSoTimeout()
  {
    try
    {
      int i = super.getSoTimeout();
      return i;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public final void setSoTimeout(int paramInt)
  {
    try
    {
      super.setSoTimeout(paramInt);
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public final void startHandshake()
  {
    try
    {
      super.startHandshake();
      return;
    }
    catch (IOException localIOException)
    {
      this.a.a(localIOException, this);
      throw localIOException;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/o.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */