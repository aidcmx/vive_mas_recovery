package com.crittercism.internal;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import org.apache.harmony.xnet.provider.jsse.OpenSSLSocketImplWrapper;
import org.apache.harmony.xnet.provider.jsse.SSLParametersImpl;

public final class p
  extends OpenSSLSocketImplWrapper
  implements u
{
  private r a;
  
  protected p(d paramd, c paramc, Socket paramSocket, String paramString, int paramInt, boolean paramBoolean, SSLParametersImpl paramSSLParametersImpl)
  {
    super(paramSocket, paramString, paramInt, paramBoolean, paramSSLParametersImpl);
    this.a = new r(f.a.b, paramd, paramc);
  }
  
  public final b a()
  {
    InetAddress localInetAddress = getInetAddress();
    return this.a.a(localInetAddress);
  }
  
  public final void a(b paramb)
  {
    this.a.a(paramb);
  }
  
  public final b b()
  {
    return this.a.b();
  }
  
  public final void close()
  {
    super.close();
    this.a.a();
  }
  
  public final InputStream getInputStream()
  {
    InputStream localInputStream = super.getInputStream();
    return this.a.a(this, localInputStream);
  }
  
  public final OutputStream getOutputStream()
  {
    OutputStream localOutputStream = super.getOutputStream();
    return this.a.a(this, localOutputStream);
  }
  
  public final void startHandshake()
  {
    try
    {
      super.startHandshake();
      return;
    }
    catch (IOException localIOException)
    {
      this.a.a(localIOException, this);
      throw localIOException;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/p.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */