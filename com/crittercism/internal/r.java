package com.crittercism.internal;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.util.LinkedList;
import java.util.Queue;
import javax.net.ssl.SSLSocket;

public final class r
{
  f.a a;
  private d b;
  private c c;
  private final Queue<b> d;
  private g e;
  private h f;
  
  public r(f.a parama, d paramd, c paramc)
  {
    this.a = parama;
    this.b = paramd;
    this.c = paramc;
    this.d = new LinkedList();
  }
  
  public final b a(InetAddress paramInetAddress)
  {
    return a(paramInetAddress, null, this.a);
  }
  
  final b a(InetAddress paramInetAddress, Integer paramInteger, f.a parama)
  {
    b localb = new b();
    if (paramInetAddress != null)
    {
      localb.n = null;
      localb.m.a = paramInetAddress;
    }
    if ((paramInteger != null) && (paramInteger.intValue() > 0))
    {
      int i = paramInteger.intValue();
      paramInetAddress = localb.m;
      if (i > 0) {
        paramInetAddress.e = i;
      }
    }
    if (parama != null) {
      localb.m.d = parama;
    }
    if (this.c != null) {
      localb.o = a.a(this.c.a);
    }
    if (an.b()) {
      localb.a(an.a());
    }
    return localb;
  }
  
  public final InputStream a(u paramu, InputStream paramInputStream)
  {
    if (paramInputStream != null) {}
    for (;;)
    {
      try
      {
        if (this.f != null)
        {
          if (this.f.b != paramInputStream) {
            break label67;
          }
          i = 1;
          if (i != 0) {
            return this.f;
          }
        }
        this.f = new h(paramu, paramInputStream, this.b);
        paramu = this.f;
        return paramu;
      }
      catch (ThreadDeath paramu)
      {
        throw paramu;
      }
      catch (Throwable paramu)
      {
        cd.b(paramu);
      }
      return paramInputStream;
      label67:
      int i = 0;
    }
  }
  
  public final OutputStream a(u paramu, OutputStream paramOutputStream)
  {
    if (paramOutputStream != null) {}
    for (;;)
    {
      try
      {
        if (this.e != null)
        {
          if (this.e.a != paramOutputStream) {
            break label63;
          }
          i = 1;
          if (i != 0) {
            return this.e;
          }
        }
        this.e = new g(paramu, paramOutputStream);
        paramu = this.e;
        return paramu;
      }
      catch (ThreadDeath paramu)
      {
        throw paramu;
      }
      catch (Throwable paramu)
      {
        cd.b(paramu);
      }
      return paramOutputStream;
      label63:
      int i = 0;
    }
  }
  
  public final void a()
  {
    try
    {
      h localh;
      if (this.f != null)
      {
        localh = this.f;
        if (localh.a != null)
        {
          bj localbj = localh.a.k;
          bi localbi = bi.a;
          if ((localbj.a != bk.d - 1) || (localbj.b != localbi.C)) {
            break label77;
          }
        }
      }
      label77:
      for (int i = 1;; i = 0)
      {
        if ((i != 0) && (localh.c != null)) {
          localh.c.f();
        }
        return;
      }
      return;
    }
    catch (ThreadDeath localThreadDeath)
    {
      throw localThreadDeath;
    }
    catch (Throwable localThrowable)
    {
      cd.b(localThrowable);
    }
  }
  
  public final void a(b paramb)
  {
    synchronized (this.d)
    {
      this.d.add(paramb);
      return;
    }
  }
  
  final void a(IOException paramIOException, InetAddress paramInetAddress, int paramInt, String paramString, f.a parama)
  {
    paramInetAddress = a(paramInetAddress, Integer.valueOf(paramInt), parama);
    if (paramString != null) {
      paramInetAddress.b(paramString);
    }
    paramInetAddress.c();
    paramInetAddress.d();
    paramInetAddress.m.f = true;
    paramInetAddress.a(paramIOException);
    this.b.a(paramInetAddress);
  }
  
  public final void a(IOException paramIOException, SSLSocket paramSSLSocket)
  {
    try
    {
      a(paramIOException, paramSSLSocket.getInetAddress(), paramSSLSocket.getPort(), null, this.a);
      return;
    }
    catch (ThreadDeath paramIOException)
    {
      throw paramIOException;
    }
    catch (Throwable paramIOException)
    {
      cd.b(paramIOException);
    }
  }
  
  public final b b()
  {
    synchronized (this.d)
    {
      b localb = (b)this.d.poll();
      return localb;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/r.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */