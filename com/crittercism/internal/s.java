package com.crittercism.internal;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.PlainSocketImpl;
import java.net.SocketAddress;

public final class s
  extends PlainSocketImpl
  implements u
{
  private r a;
  
  public s(d paramd, c paramc)
  {
    this.a = new r(f.a.a, paramd, paramc);
  }
  
  public final b a()
  {
    InetAddress localInetAddress = getInetAddress();
    int i = getPort();
    r localr = this.a;
    return localr.a(localInetAddress, Integer.valueOf(i), localr.a);
  }
  
  public final void a(b paramb)
  {
    this.a.a(paramb);
  }
  
  public final b b()
  {
    return this.a.b();
  }
  
  public final void close()
  {
    super.close();
    this.a.a();
  }
  
  public final void connect(String paramString, int paramInt)
  {
    InetAddress localInetAddress;
    r localr;
    try
    {
      super.connect(paramString, paramInt);
      return;
    }
    catch (IOException localIOException)
    {
      localInetAddress = getInetAddress();
      localr = this.a;
      if (paramString == null) {}
    }
    try
    {
      localr.a(localIOException, localInetAddress, paramInt, paramString, null);
      throw localIOException;
    }
    catch (ThreadDeath paramString)
    {
      throw paramString;
    }
    catch (Throwable paramString)
    {
      for (;;)
      {
        cd.b(paramString);
      }
    }
  }
  
  public final void connect(InetAddress paramInetAddress, int paramInt)
  {
    r localr;
    try
    {
      super.connect(paramInetAddress, paramInt);
      return;
    }
    catch (IOException localIOException)
    {
      localr = this.a;
      if (paramInetAddress == null) {}
    }
    try
    {
      localr.a(localIOException, paramInetAddress, paramInt, null, null);
      throw localIOException;
    }
    catch (ThreadDeath paramInetAddress)
    {
      throw paramInetAddress;
    }
    catch (Throwable paramInetAddress)
    {
      for (;;)
      {
        cd.b(paramInetAddress);
      }
    }
  }
  
  public final void connect(SocketAddress paramSocketAddress, int paramInt)
  {
    r localr;
    try
    {
      super.connect(paramSocketAddress, paramInt);
      return;
    }
    catch (IOException localIOException)
    {
      localr = this.a;
      if (paramSocketAddress == null) {}
    }
    try
    {
      if ((paramSocketAddress instanceof InetSocketAddress))
      {
        paramSocketAddress = (InetSocketAddress)paramSocketAddress;
        InetAddress localInetAddress = paramSocketAddress.getAddress();
        paramInt = paramSocketAddress.getPort();
        if (localInetAddress != null) {
          localr.a(localIOException, localInetAddress, paramInt, null, null);
        }
      }
    }
    catch (ThreadDeath paramSocketAddress)
    {
      throw paramSocketAddress;
    }
    catch (Throwable paramSocketAddress)
    {
      for (;;)
      {
        cd.b(paramSocketAddress);
      }
    }
    throw localIOException;
  }
  
  public final InputStream getInputStream()
  {
    InputStream localInputStream = super.getInputStream();
    return this.a.a(this, localInputStream);
  }
  
  public final Object getOption(int paramInt)
  {
    return super.getOption(paramInt);
  }
  
  public final OutputStream getOutputStream()
  {
    OutputStream localOutputStream = super.getOutputStream();
    return this.a.a(this, localOutputStream);
  }
  
  public final void setOption(int paramInt, Object paramObject)
  {
    super.setOption(paramInt, paramObject);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/s.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */