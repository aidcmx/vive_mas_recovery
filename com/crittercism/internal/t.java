package com.crittercism.internal;

import java.net.Socket;
import java.net.SocketImpl;
import java.net.SocketImplFactory;

public final class t
  implements SocketImplFactory
{
  private static boolean a = false;
  private d b;
  private c c;
  
  private t(d paramd, c paramc)
  {
    this.b = paramd;
    this.c = paramc;
  }
  
  public static boolean a(d paramd, c paramc)
  {
    if (a) {
      return a;
    }
    paramd = new t(paramd, paramc);
    try
    {
      paramd.createSocketImpl();
      Socket.setSocketImplFactory(paramd);
      a = true;
      return true;
    }
    catch (Throwable paramd) {}
    return a;
  }
  
  public final SocketImpl createSocketImpl()
  {
    return new s(this.b, this.c);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/t.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */