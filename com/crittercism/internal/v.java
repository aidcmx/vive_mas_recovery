package com.crittercism.internal;

public abstract class v
{
  ac a;
  w b;
  protected int c;
  private int d;
  
  public v(ac paramac)
  {
    a(paramac, 0);
  }
  
  public v(v paramv)
  {
    a(paramv.a, paramv.c);
  }
  
  private void a(ac paramac, int paramInt)
  {
    this.a = paramac;
    this.d = e();
    this.b = new w(d());
    this.c = paramInt;
  }
  
  private void g()
  {
    this.a.a(aj.d);
  }
  
  public final int a()
  {
    return this.c;
  }
  
  public final void a(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    int i = b(paramArrayOfByte, paramInt1, paramInt2);
    while ((i > 0) && (i < paramInt2))
    {
      int j = this.a.a().b(paramArrayOfByte, paramInt1 + i, paramInt2 - i);
      if (j <= 0) {
        break;
      }
      i += j;
    }
  }
  
  public boolean a(int paramInt)
  {
    if (paramInt == -1)
    {
      g();
      return true;
    }
    this.c += 1;
    int i = (char)paramInt;
    Object localObject;
    if (i == 10) {
      if (a(this.b)) {
        localObject = b();
      }
    }
    for (;;)
    {
      if (localObject != this) {
        this.a.a((v)localObject);
      }
      if (localObject != this) {
        break;
      }
      return false;
      localObject = aj.d;
      continue;
      if (this.b.b < this.d)
      {
        localObject = this.b;
        paramInt = ((w)localObject).b + 1;
        if (paramInt > ((w)localObject).a.length)
        {
          char[] arrayOfChar = new char[Math.max(((w)localObject).a.length << 1, paramInt)];
          System.arraycopy(((w)localObject).a, 0, arrayOfChar, 0, ((w)localObject).b);
          ((w)localObject).a = arrayOfChar;
        }
        ((w)localObject).a[localObject.b] = i;
        ((w)localObject).b = paramInt;
        localObject = this;
      }
      else
      {
        localObject = c();
      }
    }
  }
  
  public abstract boolean a(w paramw);
  
  protected int b(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    boolean bool = false;
    int i = -1;
    int j;
    if (paramInt2 == -1)
    {
      g();
      j = i;
    }
    do
    {
      do
      {
        return j;
        j = i;
      } while (paramArrayOfByte == null);
      j = i;
    } while (paramInt2 == 0);
    i = 0;
    for (;;)
    {
      j = i;
      if (bool) {
        break;
      }
      j = i;
      if (i >= paramInt2) {
        break;
      }
      bool = a((char)paramArrayOfByte[(paramInt1 + i)]);
      i += 1;
    }
  }
  
  public abstract v b();
  
  public final void b(int paramInt)
  {
    this.c = paramInt;
  }
  
  public abstract v c();
  
  protected abstract int d();
  
  protected abstract int e();
  
  public void f()
  {
    if (this.a != null) {
      this.a.a(aj.d);
    }
  }
  
  public String toString()
  {
    return this.b.toString();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/v.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */