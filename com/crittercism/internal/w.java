package com.crittercism.internal;

public final class w
{
  char[] a;
  int b;
  
  public w(int paramInt)
  {
    this.a = new char[paramInt];
  }
  
  private static boolean a(char paramChar)
  {
    return (paramChar == ' ') || (paramChar == '\t') || (paramChar == '\r') || (paramChar == '\n');
  }
  
  public final String a(int paramInt)
  {
    if (paramInt > this.b) {
      throw new IndexOutOfBoundsException("endIndex: " + paramInt + " > length: " + this.b);
    }
    if (paramInt < 0) {
      throw new IndexOutOfBoundsException("beginIndex: 0 > endIndex: " + paramInt);
    }
    int i = 0;
    int j;
    for (;;)
    {
      j = paramInt;
      if (i >= paramInt) {
        break;
      }
      j = paramInt;
      if (!a(this.a[i])) {
        break;
      }
      i += 1;
    }
    while ((j > i) && (a(this.a[(j - 1)]))) {
      j -= 1;
    }
    return new String(this.a, i, j - i);
  }
  
  public final String toString()
  {
    return new String(this.a, 0, this.b);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/w.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */