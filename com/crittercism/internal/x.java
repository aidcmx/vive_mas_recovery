package com.crittercism.internal;

public final class x
  extends v
{
  private int d;
  private int e = 0;
  
  public x(v paramv, int paramInt)
  {
    super(paramv);
    this.d = paramInt;
  }
  
  public final boolean a(int paramInt)
  {
    if (paramInt == -1)
    {
      this.a.a(aj.d);
      return true;
    }
    this.e += 1;
    this.c += 1;
    if (this.e == this.d)
    {
      this.a.b(a());
      v localv = this.a.b();
      this.a.a(localv);
      return true;
    }
    return false;
  }
  
  public final boolean a(w paramw)
  {
    return true;
  }
  
  public final int b(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    if (paramInt2 == -1)
    {
      this.a.a(aj.d);
      return -1;
    }
    if (this.e + paramInt2 < this.d)
    {
      this.e += paramInt2;
      this.c += paramInt2;
      return paramInt2;
    }
    paramInt1 = this.d - this.e;
    this.c += paramInt1;
    this.a.b(a());
    this.a.a(this.a.b());
    return paramInt1;
  }
  
  public final v b()
  {
    return aj.d;
  }
  
  public final v c()
  {
    return aj.d;
  }
  
  protected final int d()
  {
    return 0;
  }
  
  protected final int e()
  {
    return 0;
  }
  
  public final void f()
  {
    this.a.b(a());
    this.a.a(aj.d);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/x.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */