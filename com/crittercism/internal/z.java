package com.crittercism.internal;

public final class z
  extends v
{
  private int d = -1;
  
  public z(v paramv)
  {
    super(paramv);
  }
  
  public final boolean a(w paramw)
  {
    int j = paramw.b;
    if (j > paramw.b) {
      j = paramw.b;
    }
    for (;;)
    {
      int i;
      if (j >= 0)
      {
        i = 0;
        if (i < j) {
          if (paramw.a[i] == ';')
          {
            label40:
            j = paramw.b;
            if (i <= 0) {
              break label80;
            }
          }
        }
      }
      for (;;)
      {
        try
        {
          this.d = Integer.parseInt(paramw.a(i), 16);
          return true;
        }
        catch (NumberFormatException paramw)
        {
          return false;
        }
        i += 1;
        break;
        i = -1;
        break label40;
        label80:
        i = j;
      }
    }
  }
  
  public final v b()
  {
    if (this.d == 0) {
      return new ah(this);
    }
    this.b.b = 0;
    return new y(this, this.d);
  }
  
  public final v c()
  {
    return aj.d;
  }
  
  protected final int d()
  {
    return 16;
  }
  
  protected final int e()
  {
    return 256;
  }
  
  public final void f()
  {
    this.a.b(a());
    this.a.a(aj.d);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/internal/z.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */