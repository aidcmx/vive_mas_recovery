package com.crittercism.webview;

import android.webkit.JavascriptInterface;
import com.crittercism.internal.am;
import com.crittercism.internal.at;
import com.crittercism.internal.bj;
import com.crittercism.internal.bk;
import com.crittercism.internal.bl;
import com.crittercism.internal.cd;
import org.json.JSONException;
import org.json.JSONObject;

public class CritterJSInterface
{
  private am a;
  
  public CritterJSInterface(am paramam)
  {
    if (paramam == null) {
      a("CritterJSInterface");
    }
    this.a = paramam;
  }
  
  private static void a(String paramString)
  {
    cd.b(CritterJSInterface.class.getName() + "." + paramString + "() badly initialized: null instance.", new NullPointerException());
  }
  
  private static void a(String paramString1, String paramString2, long paramLong)
  {
    b(paramString1, paramString2, "negative long integer: " + paramLong);
  }
  
  private static boolean a(String paramString1, String paramString2, String paramString3)
  {
    if (paramString1 == null)
    {
      if (paramString3.length() > 0) {}
      for (paramString1 = paramString3 + " ";; paramString1 = "")
      {
        cd.b(CritterJSInterface.class.getName() + "." + paramString2 + "() given invalid " + paramString1 + "parameter: null string or invalid javascript type. Request is being ignored...", new NullPointerException());
        return false;
      }
    }
    if (paramString1.length() == 0)
    {
      b(paramString2, paramString3, "empty string");
      return false;
    }
    return true;
  }
  
  private static void b(String paramString1, String paramString2, String paramString3)
  {
    if ((paramString2 != null) && (paramString2.length() > 0)) {}
    for (paramString2 = paramString2 + " ";; paramString2 = "")
    {
      cd.b(CritterJSInterface.class.getName() + "." + paramString1 + "() given invalid " + paramString2 + "parameter: " + paramString3 + ". Request is being ignored.", new IllegalArgumentException());
      return;
    }
  }
  
  @JavascriptInterface
  public void beginTransaction(String paramString)
  {
    try
    {
      if (this.a == null)
      {
        a("beginTransaction");
        return;
      }
      if (a(paramString, "beginTransaction", "name"))
      {
        this.a.a(paramString);
        return;
      }
    }
    catch (ThreadDeath paramString)
    {
      throw paramString;
    }
    catch (Throwable paramString)
    {
      cd.b(paramString);
    }
  }
  
  @JavascriptInterface
  public void cancelTransaction(String paramString)
  {
    try
    {
      if (this.a == null)
      {
        a("cancelTransaction");
        return;
      }
      if (a(paramString, "cancelTransaction", "name"))
      {
        this.a.d(paramString);
        return;
      }
    }
    catch (ThreadDeath paramString)
    {
      throw paramString;
    }
    catch (Throwable paramString)
    {
      cd.b(paramString);
    }
  }
  
  @JavascriptInterface
  public void endTransaction(String paramString)
  {
    try
    {
      if (this.a == null)
      {
        a("endTransaction");
        return;
      }
      if (a(paramString, "endTransaction", "name"))
      {
        this.a.b(paramString);
        return;
      }
    }
    catch (ThreadDeath paramString)
    {
      throw paramString;
    }
    catch (Throwable paramString)
    {
      cd.b(paramString);
    }
  }
  
  @JavascriptInterface
  public void failTransaction(String paramString)
  {
    try
    {
      if (this.a == null)
      {
        a("failTransaction");
        return;
      }
      if (a(paramString, "failTransaction", "name"))
      {
        this.a.c(paramString);
        return;
      }
    }
    catch (ThreadDeath paramString)
    {
      throw paramString;
    }
    catch (Throwable paramString)
    {
      cd.b(paramString);
    }
  }
  
  @JavascriptInterface
  public int getTransactionValue(String paramString)
  {
    try
    {
      if (this.a == null)
      {
        a("getTransactionValue");
        return -1;
      }
      if (a(paramString, "getTransactionValue", "transactionName"))
      {
        int i = this.a.e(paramString);
        return i;
      }
    }
    catch (ThreadDeath paramString)
    {
      throw paramString;
    }
    catch (Throwable paramString)
    {
      cd.b(paramString);
    }
    return -1;
  }
  
  @JavascriptInterface
  public void leaveBreadcrumb(String paramString)
  {
    try
    {
      if (this.a == null)
      {
        a("leaveBreadcrumb");
        return;
      }
      if (a(paramString, "leaveBreadcrumb", "breadcrumb"))
      {
        this.a.a(at.a(paramString));
        return;
      }
    }
    catch (ThreadDeath paramString)
    {
      throw paramString;
    }
    catch (Throwable paramString)
    {
      cd.b(paramString);
    }
  }
  
  @JavascriptInterface
  public void logError(String paramString1, String paramString2)
  {
    try
    {
      if (this.a == null)
      {
        a("logError");
        return;
      }
      if ((paramString1 == null) || (paramString1.length() == 0) || (paramString2 == null) || (paramString2.length() == 0)) {
        return;
      }
      String str2 = "";
      str1 = "";
      arrayOfString = paramString1.split(":", 2);
      paramString1 = str2;
      if (arrayOfString.length <= 0) {
        break label82;
      }
      if (arrayOfString[0].indexOf("Uncaught ") >= 0) {
        break label121;
      }
      paramString1 = arrayOfString[0];
    }
    catch (ThreadDeath paramString1)
    {
      for (;;)
      {
        String str1;
        String[] arrayOfString;
        throw paramString1;
        paramString1 = arrayOfString[0].substring(9);
      }
    }
    catch (Throwable paramString1)
    {
      label82:
      label121:
      cd.b(paramString1);
    }
    paramString1 = paramString1.trim();
    if (arrayOfString.length > 1) {
      str1 = arrayOfString[1].trim();
    }
    paramString1 = new bl(paramString1, str1, paramString2, false);
    this.a.a(paramString1);
    return;
  }
  
  @JavascriptInterface
  public void logHandledException(String paramString1, String paramString2, String paramString3)
  {
    try
    {
      if (this.a == null)
      {
        a("logHandledException");
        return;
      }
      if ((a(paramString1, "logHandledException", "name")) && (a(paramString2, "logHandledException", "reason")) && (a(paramString3, "logHandledException", "stack")))
      {
        paramString1 = new bl(paramString1, paramString2, paramString3, true);
        this.a.a(paramString1);
        return;
      }
    }
    catch (ThreadDeath paramString1)
    {
      throw paramString1;
    }
    catch (Throwable paramString1)
    {
      cd.b(paramString1);
    }
  }
  
  @JavascriptInterface
  public void logNetworkRequest(String paramString1, String paramString2, long paramLong1, long paramLong2, long paramLong3, int paramInt1, int paramInt2)
  {
    int i;
    try
    {
      if (this.a == null)
      {
        a("logNetworkRequest");
        return;
      }
      if (a(paramString1, "logNetworkRequest", "httpMethod")) {
        break label63;
      }
      i = 0;
    }
    catch (ThreadDeath paramString1)
    {
      for (;;)
      {
        throw paramString1;
        i = 0;
        if (i < 9)
        {
          if (!new String[] { "GET", "HEAD", "POST", "PUT", "DELETE", "TRACE", "CONNECT", "OPTIONS", "PATCH" }[i].equals(paramString1)) {
            break;
          }
          i = 1;
        }
        else
        {
          b("logNetworkRequest", "httpMethod", paramString1);
          i = 0;
        }
      }
      if (paramLong3 >= 0L) {
        break label179;
      }
      a("logNetworkRequest", "bytesSent", paramLong3);
      return;
    }
    catch (Throwable paramString1)
    {
      label63:
      cd.b(paramString1);
      return;
    }
    if ((i != 0) && (a(paramString2, "logNetworkRequest", "url")))
    {
      if (paramLong2 < 0L)
      {
        a("logNetworkRequest", "bytesRead", paramLong2);
        return;
      }
      label179:
      if (paramInt1 < 0)
      {
        b("logNetworkRequest", "responseCode", "negative integer: " + paramInt1);
        i = 0;
        label211:
        if (i != 0)
        {
          bj localbj = new bj(bk.e - 1, paramInt2);
          this.a.a(paramString1, paramString2, paramLong1, paramLong2, paramLong3, paramInt1, localbj);
        }
      }
      else
      {
        i = 0;
        break label297;
      }
    }
    label297:
    label613:
    for (;;)
    {
      b("logNetworkRequest", "responseCode", "the given HTTP response is not allowed: " + paramInt1);
      i = 0;
      break label211;
      return;
      i += 1;
      break;
      for (;;)
      {
        if (i >= 42) {
          break label613;
        }
        if (new int[] { 0, 100, 101, 200, 201, 202, 203, 204, 205, 206, 300, 301, 302, 303, 304, 305, 306, 307, 400, 401, 402, 403, 404, 405, 406, 407, 408, 409, 410, 411, 412, 413, 414, 415, 416, 417, 500, 501, 502, 503, 504, 505 }[i] == paramInt1)
        {
          i = 1;
          break;
        }
        i += 1;
      }
    }
  }
  
  @JavascriptInterface
  public void logUnhandledException(String paramString1, String paramString2, String paramString3)
  {
    try
    {
      if (this.a == null)
      {
        a("logUnhandledException");
        return;
      }
      if ((a(paramString1, "logUnhandledException", "name")) && (a(paramString2, "logUnhandledException", "reason")) && (a(paramString3, "logUnhandledException", "stack")))
      {
        paramString1 = new bl(paramString1, paramString2, paramString3, false);
        this.a.a(paramString1);
        return;
      }
    }
    catch (ThreadDeath paramString1)
    {
      throw paramString1;
    }
    catch (Throwable paramString1)
    {
      cd.b(paramString1);
    }
  }
  
  @JavascriptInterface
  public void setMetadata(String paramString)
  {
    try
    {
      if (this.a == null)
      {
        a("setMetadata");
        return;
      }
      boolean bool = a(paramString, "setMetadata", "metadataJson");
      if (bool) {}
      JSONObject localJSONObject;
      return;
    }
    catch (ThreadDeath paramString)
    {
      try
      {
        localJSONObject = new JSONObject(paramString);
        this.a.a(localJSONObject);
        return;
      }
      catch (JSONException localJSONException)
      {
        b("setMetadata", "", "badly formatted json string. " + paramString);
        return;
      }
      paramString = paramString;
      throw paramString;
    }
    catch (Throwable paramString)
    {
      cd.b(paramString);
    }
  }
  
  @JavascriptInterface
  public void setTransactionValue(String paramString, int paramInt)
  {
    try
    {
      if (this.a == null)
      {
        a("setTransactionValue");
        return;
      }
      if (a(paramString, "setTransactionValue", "transactionName"))
      {
        this.a.a(paramString, paramInt);
        return;
      }
    }
    catch (ThreadDeath paramString)
    {
      throw paramString;
    }
    catch (Throwable paramString)
    {
      cd.b(paramString);
    }
  }
  
  @JavascriptInterface
  public void setUsername(String paramString)
  {
    try
    {
      if (this.a == null)
      {
        a("setUsername");
        return;
      }
      JSONObject localJSONObject;
      if (a(paramString, "setUsername", "username")) {
        localJSONObject = new JSONObject();
      }
      return;
    }
    catch (ThreadDeath paramString)
    {
      try
      {
        localJSONObject.putOpt("username", paramString);
        this.a.a(localJSONObject);
        return;
      }
      catch (JSONException paramString)
      {
        cd.b("Crittercism.setUsername()", paramString);
        return;
      }
      paramString = paramString;
      throw paramString;
    }
    catch (Throwable paramString)
    {
      cd.b(paramString);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/crittercism/webview/CritterJSInterface.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */