package com.facebook;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;

public class CustomTabActivity
  extends Activity
{
  public static final String CUSTOM_TAB_REDIRECT_ACTION = CustomTabActivity.class.getSimpleName() + ".action_customTabRedirect";
  private static final int CUSTOM_TAB_REDIRECT_REQUEST_CODE = 2;
  public static final String DESTROY_ACTION = CustomTabActivity.class.getSimpleName() + ".action_destroy";
  private BroadcastReceiver closeReceiver;
  
  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    if (paramInt2 == 0)
    {
      paramIntent = new Intent(CUSTOM_TAB_REDIRECT_ACTION);
      paramIntent.putExtra(CustomTabMainActivity.EXTRA_URL, getIntent().getDataString());
      LocalBroadcastManager.getInstance(this).sendBroadcast(paramIntent);
      this.closeReceiver = new BroadcastReceiver()
      {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
          CustomTabActivity.this.finish();
        }
      };
      LocalBroadcastManager.getInstance(this).registerReceiver(this.closeReceiver, new IntentFilter(DESTROY_ACTION));
    }
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = new Intent(this, CustomTabMainActivity.class);
    paramBundle.setAction(CUSTOM_TAB_REDIRECT_ACTION);
    paramBundle.putExtra(CustomTabMainActivity.EXTRA_URL, getIntent().getDataString());
    paramBundle.addFlags(603979776);
    startActivityForResult(paramBundle, 2);
  }
  
  protected void onDestroy()
  {
    LocalBroadcastManager.getInstance(this).unregisterReceiver(this.closeReceiver);
    super.onDestroy();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/facebook/CustomTabActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */