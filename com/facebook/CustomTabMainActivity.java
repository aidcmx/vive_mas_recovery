package com.facebook;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import com.facebook.internal.CustomTab;

public class CustomTabMainActivity
  extends Activity
{
  public static final String EXTRA_CHROME_PACKAGE = CustomTabMainActivity.class.getSimpleName() + ".extra_chromePackage";
  public static final String EXTRA_PARAMS = CustomTabMainActivity.class.getSimpleName() + ".extra_params";
  public static final String EXTRA_URL = CustomTabMainActivity.class.getSimpleName() + ".extra_url";
  private static final String OAUTH_DIALOG = "oauth";
  public static final String REFRESH_ACTION = CustomTabMainActivity.class.getSimpleName() + ".action_refresh";
  private BroadcastReceiver redirectReceiver;
  private boolean shouldCloseCustomTab = true;
  
  public static final String getRedirectUrl()
  {
    return "fb" + FacebookSdk.getApplicationId() + "://authorize";
  }
  
  private void sendResult(int paramInt, Intent paramIntent)
  {
    LocalBroadcastManager.getInstance(this).unregisterReceiver(this.redirectReceiver);
    if (paramIntent != null) {
      setResult(paramInt, paramIntent);
    }
    for (;;)
    {
      finish();
      return;
      setResult(paramInt);
    }
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    if (CustomTabActivity.CUSTOM_TAB_REDIRECT_ACTION.equals(getIntent().getAction()))
    {
      setResult(0);
      finish();
    }
    while (paramBundle != null) {
      return;
    }
    paramBundle = getIntent().getBundleExtra(EXTRA_PARAMS);
    String str = getIntent().getStringExtra(EXTRA_CHROME_PACKAGE);
    new CustomTab("oauth", paramBundle).openCustomTab(this, str);
    this.shouldCloseCustomTab = false;
    this.redirectReceiver = new BroadcastReceiver()
    {
      public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
      {
        paramAnonymousContext = new Intent(CustomTabMainActivity.this, CustomTabMainActivity.class);
        paramAnonymousContext.setAction(CustomTabMainActivity.REFRESH_ACTION);
        paramAnonymousContext.putExtra(CustomTabMainActivity.EXTRA_URL, paramAnonymousIntent.getStringExtra(CustomTabMainActivity.EXTRA_URL));
        paramAnonymousContext.addFlags(603979776);
        CustomTabMainActivity.this.startActivity(paramAnonymousContext);
      }
    };
    LocalBroadcastManager.getInstance(this).registerReceiver(this.redirectReceiver, new IntentFilter(CustomTabActivity.CUSTOM_TAB_REDIRECT_ACTION));
  }
  
  protected void onNewIntent(Intent paramIntent)
  {
    super.onNewIntent(paramIntent);
    if (REFRESH_ACTION.equals(paramIntent.getAction()))
    {
      localIntent = new Intent(CustomTabActivity.DESTROY_ACTION);
      LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);
      sendResult(-1, paramIntent);
    }
    while (!CustomTabActivity.CUSTOM_TAB_REDIRECT_ACTION.equals(paramIntent.getAction()))
    {
      Intent localIntent;
      return;
    }
    sendResult(-1, paramIntent);
  }
  
  protected void onResume()
  {
    super.onResume();
    if (this.shouldCloseCustomTab) {
      sendResult(0, null);
    }
    this.shouldCloseCustomTab = true;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/facebook/CustomTabMainActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */