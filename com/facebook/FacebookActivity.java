package com.facebook;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import com.facebook.internal.FacebookDialogFragment;
import com.facebook.internal.NativeProtocol;
import com.facebook.login.LoginFragment;
import com.facebook.share.internal.DeviceShareDialogFragment;
import com.facebook.share.model.ShareContent;

public class FacebookActivity
  extends FragmentActivity
{
  private static String FRAGMENT_TAG = "SingleFragment";
  public static String PASS_THROUGH_CANCEL_ACTION = "PassThrough";
  private static final String TAG = FacebookActivity.class.getName();
  private Fragment singleFragment;
  
  private void handlePassThroughError()
  {
    FacebookException localFacebookException = NativeProtocol.getExceptionFromErrorData(NativeProtocol.getMethodArgumentsFromIntent(getIntent()));
    setResult(0, NativeProtocol.createProtocolResultIntent(getIntent(), null, localFacebookException));
    finish();
  }
  
  public Fragment getCurrentFragment()
  {
    return this.singleFragment;
  }
  
  protected Fragment getFragment()
  {
    Intent localIntent = getIntent();
    FragmentManager localFragmentManager = getSupportFragmentManager();
    Fragment localFragment = localFragmentManager.findFragmentByTag(FRAGMENT_TAG);
    Object localObject = localFragment;
    if (localFragment == null)
    {
      if ("FacebookDialogFragment".equals(localIntent.getAction()))
      {
        localObject = new FacebookDialogFragment();
        ((FacebookDialogFragment)localObject).setRetainInstance(true);
        ((FacebookDialogFragment)localObject).show(localFragmentManager, FRAGMENT_TAG);
      }
    }
    else {
      return (Fragment)localObject;
    }
    if ("DeviceShareDialogFragment".equals(localIntent.getAction()))
    {
      localObject = new DeviceShareDialogFragment();
      ((DeviceShareDialogFragment)localObject).setRetainInstance(true);
      ((DeviceShareDialogFragment)localObject).setShareContent((ShareContent)localIntent.getParcelableExtra("content"));
      ((DeviceShareDialogFragment)localObject).show(localFragmentManager, FRAGMENT_TAG);
      return (Fragment)localObject;
    }
    localObject = new LoginFragment();
    ((Fragment)localObject).setRetainInstance(true);
    localFragmentManager.beginTransaction().add(R.id.com_facebook_fragment_container, (Fragment)localObject, FRAGMENT_TAG).commit();
    return (Fragment)localObject;
  }
  
  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    super.onConfigurationChanged(paramConfiguration);
    if (this.singleFragment != null) {
      this.singleFragment.onConfigurationChanged(paramConfiguration);
    }
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = getIntent();
    if (!FacebookSdk.isInitialized())
    {
      Log.d(TAG, "Facebook SDK not initialized. Make sure you call sdkInitialize inside your Application's onCreate method.");
      FacebookSdk.sdkInitialize(getApplicationContext());
    }
    setContentView(R.layout.com_facebook_activity_layout);
    if (PASS_THROUGH_CANCEL_ACTION.equals(paramBundle.getAction()))
    {
      handlePassThroughError();
      return;
    }
    this.singleFragment = getFragment();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/facebook/FacebookActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */