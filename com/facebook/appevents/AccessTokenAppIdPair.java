package com.facebook.appevents;

import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.internal.Utility;
import java.io.Serializable;

class AccessTokenAppIdPair
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  private final String accessTokenString;
  private final String applicationId;
  
  public AccessTokenAppIdPair(AccessToken paramAccessToken)
  {
    this(paramAccessToken.getToken(), FacebookSdk.getApplicationId());
  }
  
  public AccessTokenAppIdPair(String paramString1, String paramString2)
  {
    String str = paramString1;
    if (Utility.isNullOrEmpty(paramString1)) {
      str = null;
    }
    this.accessTokenString = str;
    this.applicationId = paramString2;
  }
  
  private Object writeReplace()
  {
    return new SerializationProxyV1(this.accessTokenString, this.applicationId, null);
  }
  
  public boolean equals(Object paramObject)
  {
    if (!(paramObject instanceof AccessTokenAppIdPair)) {}
    do
    {
      return false;
      paramObject = (AccessTokenAppIdPair)paramObject;
    } while ((!Utility.areObjectsEqual(((AccessTokenAppIdPair)paramObject).accessTokenString, this.accessTokenString)) || (!Utility.areObjectsEqual(((AccessTokenAppIdPair)paramObject).applicationId, this.applicationId)));
    return true;
  }
  
  public String getAccessTokenString()
  {
    return this.accessTokenString;
  }
  
  public String getApplicationId()
  {
    return this.applicationId;
  }
  
  public int hashCode()
  {
    int j = 0;
    int i;
    if (this.accessTokenString == null)
    {
      i = 0;
      if (this.applicationId != null) {
        break label33;
      }
    }
    for (;;)
    {
      return i ^ j;
      i = this.accessTokenString.hashCode();
      break;
      label33:
      j = this.applicationId.hashCode();
    }
  }
  
  static class SerializationProxyV1
    implements Serializable
  {
    private static final long serialVersionUID = -2488473066578201069L;
    private final String accessTokenString;
    private final String appId;
    
    private SerializationProxyV1(String paramString1, String paramString2)
    {
      this.accessTokenString = paramString1;
      this.appId = paramString2;
    }
    
    private Object readResolve()
    {
      return new AccessTokenAppIdPair(this.accessTokenString, this.appId);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/facebook/appevents/AccessTokenAppIdPair.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */