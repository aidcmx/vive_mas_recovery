package com.facebook.appevents;

import android.os.Bundle;
import android.support.annotation.Nullable;
import com.facebook.FacebookException;
import com.facebook.LoggingBehavior;
import com.facebook.internal.Logger;
import com.facebook.internal.Utility;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import java.util.UUID;
import org.json.JSONException;
import org.json.JSONObject;

class AppEvent
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  private static final HashSet<String> validatedIdentifiers = new HashSet();
  private final String checksum;
  private final boolean isImplicit;
  private final JSONObject jsonObject;
  private final String name;
  
  public AppEvent(String paramString1, String paramString2, Double paramDouble, Bundle paramBundle, boolean paramBoolean, @Nullable UUID paramUUID)
    throws JSONException, FacebookException
  {
    this.jsonObject = getJSONObjectForAppEvent(paramString1, paramString2, paramDouble, paramBundle, paramBoolean, paramUUID);
    this.isImplicit = paramBoolean;
    this.name = paramString2;
    this.checksum = calculateChecksum();
  }
  
  private AppEvent(String paramString1, boolean paramBoolean, String paramString2)
    throws JSONException
  {
    this.jsonObject = new JSONObject(paramString1);
    this.isImplicit = paramBoolean;
    this.name = this.jsonObject.optString("_eventName");
    this.checksum = paramString2;
  }
  
  private static String bytesToHex(byte[] paramArrayOfByte)
  {
    StringBuffer localStringBuffer = new StringBuffer();
    int j = paramArrayOfByte.length;
    int i = 0;
    while (i < j)
    {
      localStringBuffer.append(String.format("%02x", new Object[] { Byte.valueOf(paramArrayOfByte[i]) }));
      i += 1;
    }
    return localStringBuffer.toString();
  }
  
  private String calculateChecksum()
  {
    return md5Checksum(this.jsonObject.toString());
  }
  
  private static JSONObject getJSONObjectForAppEvent(String paramString1, String paramString2, Double paramDouble, Bundle paramBundle, boolean paramBoolean, @Nullable UUID paramUUID)
    throws FacebookException, JSONException
  {
    validateIdentifier(paramString2);
    JSONObject localJSONObject = new JSONObject();
    localJSONObject.put("_eventName", paramString2);
    localJSONObject.put("_logTime", System.currentTimeMillis() / 1000L);
    localJSONObject.put("_ui", paramString1);
    if (paramUUID != null) {
      localJSONObject.put("_session_id", paramUUID);
    }
    if (paramDouble != null) {
      localJSONObject.put("_valueToSum", paramDouble.doubleValue());
    }
    if (paramBoolean) {
      localJSONObject.put("_implicitlyLogged", "1");
    }
    paramString1 = AppEventsLogger.getUserID();
    if (paramString1 != null) {
      localJSONObject.put("_app_user_id", paramString1);
    }
    if (paramBundle != null)
    {
      paramString1 = paramBundle.keySet().iterator();
      while (paramString1.hasNext())
      {
        paramString2 = (String)paramString1.next();
        validateIdentifier(paramString2);
        paramDouble = paramBundle.get(paramString2);
        if ((!(paramDouble instanceof String)) && (!(paramDouble instanceof Number))) {
          throw new FacebookException(String.format("Parameter value '%s' for key '%s' should be a string or a numeric type.", new Object[] { paramDouble, paramString2 }));
        }
        localJSONObject.put(paramString2, paramDouble.toString());
      }
    }
    if (!paramBoolean) {
      Logger.log(LoggingBehavior.APP_EVENTS, "AppEvents", "Created app event '%s'", new Object[] { localJSONObject.toString() });
    }
    return localJSONObject;
  }
  
  private static String md5Checksum(String paramString)
  {
    try
    {
      MessageDigest localMessageDigest = MessageDigest.getInstance("MD5");
      paramString = paramString.getBytes("UTF-8");
      localMessageDigest.update(paramString, 0, paramString.length);
      paramString = bytesToHex(localMessageDigest.digest());
      return paramString;
    }
    catch (NoSuchAlgorithmException paramString)
    {
      Utility.logd("Failed to generate checksum: ", paramString);
      return "0";
    }
    catch (UnsupportedEncodingException paramString)
    {
      Utility.logd("Failed to generate checksum: ", paramString);
    }
    return "1";
  }
  
  private static void validateIdentifier(String paramString)
    throws FacebookException
  {
    if ((paramString == null) || (paramString.length() == 0) || (paramString.length() > 40))
    {
      ??? = paramString;
      if (paramString == null) {
        ??? = "<None Provided>";
      }
      throw new FacebookException(String.format(Locale.ROOT, "Identifier '%s' must be less than %d characters", new Object[] { ???, Integer.valueOf(40) }));
    }
    synchronized (validatedIdentifiers)
    {
      boolean bool = validatedIdentifiers.contains(paramString);
      if (!bool) {
        if (!paramString.matches("^[0-9a-zA-Z_]+[0-9a-zA-Z _-]*$")) {
          break label118;
        }
      }
    }
    synchronized (validatedIdentifiers)
    {
      validatedIdentifiers.add(paramString);
      return;
      paramString = finally;
      throw paramString;
    }
    label118:
    throw new FacebookException(String.format("Skipping event named '%s' due to illegal name - must be under 40 chars and alphanumeric, _, - or space, and not start with a space or hyphen.", new Object[] { paramString }));
  }
  
  private Object writeReplace()
  {
    return new SerializationProxyV2(this.jsonObject.toString(), this.isImplicit, this.checksum, null);
  }
  
  public boolean getIsImplicit()
  {
    return this.isImplicit;
  }
  
  public JSONObject getJSONObject()
  {
    return this.jsonObject;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public boolean isChecksumValid()
  {
    if (this.checksum == null) {
      return true;
    }
    return calculateChecksum().equals(this.checksum);
  }
  
  public String toString()
  {
    return String.format("\"%s\", implicit: %b, json: %s", new Object[] { this.jsonObject.optString("_eventName"), Boolean.valueOf(this.isImplicit), this.jsonObject.toString() });
  }
  
  static class SerializationProxyV1
    implements Serializable
  {
    private static final long serialVersionUID = -2488473066578201069L;
    private final boolean isImplicit;
    private final String jsonString;
    
    private SerializationProxyV1(String paramString, boolean paramBoolean)
    {
      this.jsonString = paramString;
      this.isImplicit = paramBoolean;
    }
    
    private Object readResolve()
      throws JSONException
    {
      return new AppEvent(this.jsonString, this.isImplicit, null, null);
    }
  }
  
  static class SerializationProxyV2
    implements Serializable
  {
    private static final long serialVersionUID = 20160803001L;
    private final String checksum;
    private final boolean isImplicit;
    private final String jsonString;
    
    private SerializationProxyV2(String paramString1, boolean paramBoolean, String paramString2)
    {
      this.jsonString = paramString1;
      this.isImplicit = paramBoolean;
      this.checksum = paramString2;
    }
    
    private Object readResolve()
      throws JSONException
    {
      return new AppEvent(this.jsonString, this.isImplicit, this.checksum, null);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/facebook/appevents/AppEvent.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */