package com.facebook.appevents;

import com.facebook.appevents.internal.AppEventUtility;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectStreamClass;
import java.util.Iterator;
import java.util.Set;

class AppEventStore
{
  private static final String PERSISTED_EVENTS_FILENAME = "AppEventsLogger.persistedevents";
  private static final String TAG = AppEventStore.class.getName();
  
  /* Error */
  public static void persistEvents(AccessTokenAppIdPair paramAccessTokenAppIdPair, SessionEventsState paramSessionEventsState)
  {
    // Byte code:
    //   0: ldc 2
    //   2: monitorenter
    //   3: invokestatic 33	com/facebook/appevents/internal/AppEventUtility:assertIsNotMainThread	()V
    //   6: invokestatic 37	com/facebook/appevents/AppEventStore:readAndClearStore	()Lcom/facebook/appevents/PersistedEvents;
    //   9: astore_2
    //   10: aload_2
    //   11: aload_0
    //   12: invokevirtual 43	com/facebook/appevents/PersistedEvents:containsKey	(Lcom/facebook/appevents/AccessTokenAppIdPair;)Z
    //   15: ifeq +26 -> 41
    //   18: aload_2
    //   19: aload_0
    //   20: invokevirtual 47	com/facebook/appevents/PersistedEvents:get	(Lcom/facebook/appevents/AccessTokenAppIdPair;)Ljava/util/List;
    //   23: aload_1
    //   24: invokevirtual 53	com/facebook/appevents/SessionEventsState:getEventsToPersist	()Ljava/util/List;
    //   27: invokeinterface 59 2 0
    //   32: pop
    //   33: aload_2
    //   34: invokestatic 63	com/facebook/appevents/AppEventStore:saveEventsToDisk	(Lcom/facebook/appevents/PersistedEvents;)V
    //   37: ldc 2
    //   39: monitorexit
    //   40: return
    //   41: aload_2
    //   42: aload_0
    //   43: aload_1
    //   44: invokevirtual 53	com/facebook/appevents/SessionEventsState:getEventsToPersist	()Ljava/util/List;
    //   47: invokevirtual 67	com/facebook/appevents/PersistedEvents:addEvents	(Lcom/facebook/appevents/AccessTokenAppIdPair;Ljava/util/List;)V
    //   50: goto -17 -> 33
    //   53: astore_0
    //   54: ldc 2
    //   56: monitorexit
    //   57: aload_0
    //   58: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	59	0	paramAccessTokenAppIdPair	AccessTokenAppIdPair
    //   0	59	1	paramSessionEventsState	SessionEventsState
    //   9	33	2	localPersistedEvents	PersistedEvents
    // Exception table:
    //   from	to	target	type
    //   3	33	53	finally
    //   33	37	53	finally
    //   41	50	53	finally
  }
  
  public static void persistEvents(AppEventCollection paramAppEventCollection)
  {
    try
    {
      AppEventUtility.assertIsNotMainThread();
      PersistedEvents localPersistedEvents = readAndClearStore();
      Iterator localIterator = paramAppEventCollection.keySet().iterator();
      while (localIterator.hasNext())
      {
        AccessTokenAppIdPair localAccessTokenAppIdPair = (AccessTokenAppIdPair)localIterator.next();
        localPersistedEvents.addEvents(localAccessTokenAppIdPair, paramAppEventCollection.get(localAccessTokenAppIdPair).getEventsToPersist());
      }
      saveEventsToDisk(localPersistedEvents);
    }
    finally {}
  }
  
  /* Error */
  public static PersistedEvents readAndClearStore()
  {
    // Byte code:
    //   0: ldc 2
    //   2: monitorenter
    //   3: invokestatic 33	com/facebook/appevents/internal/AppEventUtility:assertIsNotMainThread	()V
    //   6: aconst_null
    //   7: astore 4
    //   9: aconst_null
    //   10: astore_1
    //   11: aconst_null
    //   12: astore_2
    //   13: aconst_null
    //   14: astore_3
    //   15: invokestatic 105	com/facebook/FacebookSdk:getApplicationContext	()Landroid/content/Context;
    //   18: astore 5
    //   20: new 6	com/facebook/appevents/AppEventStore$MovedClassObjectInputStream
    //   23: dup
    //   24: new 107	java/io/BufferedInputStream
    //   27: dup
    //   28: aload 5
    //   30: ldc 11
    //   32: invokevirtual 113	android/content/Context:openFileInput	(Ljava/lang/String;)Ljava/io/FileInputStream;
    //   35: invokespecial 116	java/io/BufferedInputStream:<init>	(Ljava/io/InputStream;)V
    //   38: invokespecial 117	com/facebook/appevents/AppEventStore$MovedClassObjectInputStream:<init>	(Ljava/io/InputStream;)V
    //   41: astore_0
    //   42: aload_0
    //   43: invokevirtual 120	com/facebook/appevents/AppEventStore$MovedClassObjectInputStream:readObject	()Ljava/lang/Object;
    //   46: checkcast 39	com/facebook/appevents/PersistedEvents
    //   49: astore_1
    //   50: aload_0
    //   51: invokestatic 126	com/facebook/internal/Utility:closeQuietly	(Ljava/io/Closeable;)V
    //   54: aload 5
    //   56: ldc 11
    //   58: invokevirtual 130	android/content/Context:getFileStreamPath	(Ljava/lang/String;)Ljava/io/File;
    //   61: invokevirtual 135	java/io/File:delete	()Z
    //   64: pop
    //   65: aload_1
    //   66: astore_0
    //   67: aload_0
    //   68: astore_1
    //   69: aload_0
    //   70: ifnonnull +11 -> 81
    //   73: new 39	com/facebook/appevents/PersistedEvents
    //   76: dup
    //   77: invokespecial 136	com/facebook/appevents/PersistedEvents:<init>	()V
    //   80: astore_1
    //   81: ldc 2
    //   83: monitorexit
    //   84: aload_1
    //   85: areturn
    //   86: astore_0
    //   87: getstatic 22	com/facebook/appevents/AppEventStore:TAG	Ljava/lang/String;
    //   90: ldc -118
    //   92: aload_0
    //   93: invokestatic 144	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   96: pop
    //   97: aload_1
    //   98: astore_0
    //   99: goto -32 -> 67
    //   102: aload_0
    //   103: invokestatic 126	com/facebook/internal/Utility:closeQuietly	(Ljava/io/Closeable;)V
    //   106: aload 5
    //   108: ldc 11
    //   110: invokevirtual 130	android/content/Context:getFileStreamPath	(Ljava/lang/String;)Ljava/io/File;
    //   113: invokevirtual 135	java/io/File:delete	()Z
    //   116: pop
    //   117: aload_3
    //   118: astore_0
    //   119: goto -52 -> 67
    //   122: astore_0
    //   123: getstatic 22	com/facebook/appevents/AppEventStore:TAG	Ljava/lang/String;
    //   126: ldc -118
    //   128: aload_0
    //   129: invokestatic 144	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   132: pop
    //   133: aload_3
    //   134: astore_0
    //   135: goto -68 -> 67
    //   138: astore_0
    //   139: ldc 2
    //   141: monitorexit
    //   142: aload_0
    //   143: athrow
    //   144: astore_2
    //   145: aload 4
    //   147: astore_0
    //   148: aload_0
    //   149: astore_1
    //   150: getstatic 22	com/facebook/appevents/AppEventStore:TAG	Ljava/lang/String;
    //   153: ldc -110
    //   155: aload_2
    //   156: invokestatic 144	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   159: pop
    //   160: aload_0
    //   161: invokestatic 126	com/facebook/internal/Utility:closeQuietly	(Ljava/io/Closeable;)V
    //   164: aload 5
    //   166: ldc 11
    //   168: invokevirtual 130	android/content/Context:getFileStreamPath	(Ljava/lang/String;)Ljava/io/File;
    //   171: invokevirtual 135	java/io/File:delete	()Z
    //   174: pop
    //   175: aload_3
    //   176: astore_0
    //   177: goto -110 -> 67
    //   180: astore_0
    //   181: getstatic 22	com/facebook/appevents/AppEventStore:TAG	Ljava/lang/String;
    //   184: ldc -118
    //   186: aload_0
    //   187: invokestatic 144	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   190: pop
    //   191: aload_3
    //   192: astore_0
    //   193: goto -126 -> 67
    //   196: aload_1
    //   197: invokestatic 126	com/facebook/internal/Utility:closeQuietly	(Ljava/io/Closeable;)V
    //   200: aload 5
    //   202: ldc 11
    //   204: invokevirtual 130	android/content/Context:getFileStreamPath	(Ljava/lang/String;)Ljava/io/File;
    //   207: invokevirtual 135	java/io/File:delete	()Z
    //   210: pop
    //   211: aload_0
    //   212: athrow
    //   213: astore_1
    //   214: getstatic 22	com/facebook/appevents/AppEventStore:TAG	Ljava/lang/String;
    //   217: ldc -118
    //   219: aload_1
    //   220: invokestatic 144	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   223: pop
    //   224: goto -13 -> 211
    //   227: astore_2
    //   228: aload_0
    //   229: astore_1
    //   230: aload_2
    //   231: astore_0
    //   232: goto -36 -> 196
    //   235: astore_2
    //   236: goto -88 -> 148
    //   239: astore_1
    //   240: goto -138 -> 102
    //   243: astore_0
    //   244: aload_2
    //   245: astore_0
    //   246: goto -144 -> 102
    //   249: astore_0
    //   250: goto -54 -> 196
    // Local variable table:
    //   start	length	slot	name	signature
    //   41	29	0	localObject1	Object
    //   86	7	0	localException1	Exception
    //   98	21	0	localObject2	Object
    //   122	7	0	localException2	Exception
    //   134	1	0	localObject3	Object
    //   138	5	0	localObject4	Object
    //   147	30	0	localObject5	Object
    //   180	7	0	localException3	Exception
    //   192	40	0	localObject6	Object
    //   243	1	0	localFileNotFoundException1	java.io.FileNotFoundException
    //   245	1	0	localException4	Exception
    //   249	1	0	localObject7	Object
    //   10	187	1	localObject8	Object
    //   213	7	1	localException5	Exception
    //   229	1	1	localObject9	Object
    //   239	1	1	localFileNotFoundException2	java.io.FileNotFoundException
    //   12	1	2	localObject10	Object
    //   144	12	2	localException6	Exception
    //   227	4	2	localObject11	Object
    //   235	10	2	localException7	Exception
    //   14	178	3	localObject12	Object
    //   7	139	4	localObject13	Object
    //   18	183	5	localContext	android.content.Context
    // Exception table:
    //   from	to	target	type
    //   54	65	86	java/lang/Exception
    //   106	117	122	java/lang/Exception
    //   3	6	138	finally
    //   15	20	138	finally
    //   50	54	138	finally
    //   54	65	138	finally
    //   73	81	138	finally
    //   87	97	138	finally
    //   102	106	138	finally
    //   106	117	138	finally
    //   123	133	138	finally
    //   160	164	138	finally
    //   164	175	138	finally
    //   181	191	138	finally
    //   196	200	138	finally
    //   200	211	138	finally
    //   211	213	138	finally
    //   214	224	138	finally
    //   20	42	144	java/lang/Exception
    //   164	175	180	java/lang/Exception
    //   200	211	213	java/lang/Exception
    //   42	50	227	finally
    //   42	50	235	java/lang/Exception
    //   42	50	239	java/io/FileNotFoundException
    //   20	42	243	java/io/FileNotFoundException
    //   20	42	249	finally
    //   150	160	249	finally
  }
  
  /* Error */
  private static void saveEventsToDisk(PersistedEvents paramPersistedEvents)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_1
    //   2: aconst_null
    //   3: astore_3
    //   4: invokestatic 105	com/facebook/FacebookSdk:getApplicationContext	()Landroid/content/Context;
    //   7: astore 4
    //   9: new 148	java/io/ObjectOutputStream
    //   12: dup
    //   13: new 150	java/io/BufferedOutputStream
    //   16: dup
    //   17: aload 4
    //   19: ldc 11
    //   21: iconst_0
    //   22: invokevirtual 154	android/content/Context:openFileOutput	(Ljava/lang/String;I)Ljava/io/FileOutputStream;
    //   25: invokespecial 157	java/io/BufferedOutputStream:<init>	(Ljava/io/OutputStream;)V
    //   28: invokespecial 158	java/io/ObjectOutputStream:<init>	(Ljava/io/OutputStream;)V
    //   31: astore_2
    //   32: aload_2
    //   33: aload_0
    //   34: invokevirtual 162	java/io/ObjectOutputStream:writeObject	(Ljava/lang/Object;)V
    //   37: aload_2
    //   38: invokestatic 126	com/facebook/internal/Utility:closeQuietly	(Ljava/io/Closeable;)V
    //   41: return
    //   42: astore_2
    //   43: aload_3
    //   44: astore_0
    //   45: aload_0
    //   46: astore_1
    //   47: getstatic 22	com/facebook/appevents/AppEventStore:TAG	Ljava/lang/String;
    //   50: ldc -92
    //   52: aload_2
    //   53: invokestatic 144	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   56: pop
    //   57: aload_0
    //   58: astore_1
    //   59: aload 4
    //   61: ldc 11
    //   63: invokevirtual 130	android/content/Context:getFileStreamPath	(Ljava/lang/String;)Ljava/io/File;
    //   66: invokevirtual 135	java/io/File:delete	()Z
    //   69: pop
    //   70: aload_0
    //   71: invokestatic 126	com/facebook/internal/Utility:closeQuietly	(Ljava/io/Closeable;)V
    //   74: return
    //   75: astore_0
    //   76: aload_1
    //   77: invokestatic 126	com/facebook/internal/Utility:closeQuietly	(Ljava/io/Closeable;)V
    //   80: aload_0
    //   81: athrow
    //   82: astore_0
    //   83: aload_2
    //   84: astore_1
    //   85: goto -9 -> 76
    //   88: astore_1
    //   89: goto -19 -> 70
    //   92: astore_1
    //   93: aload_2
    //   94: astore_0
    //   95: aload_1
    //   96: astore_2
    //   97: goto -52 -> 45
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	100	0	paramPersistedEvents	PersistedEvents
    //   1	84	1	localObject1	Object
    //   88	1	1	localException1	Exception
    //   92	4	1	localException2	Exception
    //   31	7	2	localObjectOutputStream	java.io.ObjectOutputStream
    //   42	52	2	localException3	Exception
    //   96	1	2	localException4	Exception
    //   3	41	3	localObject2	Object
    //   7	53	4	localContext	android.content.Context
    // Exception table:
    //   from	to	target	type
    //   9	32	42	java/lang/Exception
    //   9	32	75	finally
    //   47	57	75	finally
    //   59	70	75	finally
    //   32	37	82	finally
    //   59	70	88	java/lang/Exception
    //   32	37	92	java/lang/Exception
  }
  
  private static class MovedClassObjectInputStream
    extends ObjectInputStream
  {
    private static final String ACCESS_TOKEN_APP_ID_PAIR_SERIALIZATION_PROXY_V1_CLASS_NAME = "com.facebook.appevents.AppEventsLogger$AccessTokenAppIdPair$SerializationProxyV1";
    private static final String APP_EVENT_SERIALIZATION_PROXY_V1_CLASS_NAME = "com.facebook.appevents.AppEventsLogger$AppEvent$SerializationProxyV1";
    
    public MovedClassObjectInputStream(InputStream paramInputStream)
      throws IOException
    {
      super();
    }
    
    protected ObjectStreamClass readClassDescriptor()
      throws IOException, ClassNotFoundException
    {
      ObjectStreamClass localObjectStreamClass2 = super.readClassDescriptor();
      ObjectStreamClass localObjectStreamClass1;
      if (localObjectStreamClass2.getName().equals("com.facebook.appevents.AppEventsLogger$AccessTokenAppIdPair$SerializationProxyV1")) {
        localObjectStreamClass1 = ObjectStreamClass.lookup(AccessTokenAppIdPair.SerializationProxyV1.class);
      }
      do
      {
        return localObjectStreamClass1;
        localObjectStreamClass1 = localObjectStreamClass2;
      } while (!localObjectStreamClass2.getName().equals("com.facebook.appevents.AppEventsLogger$AppEvent$SerializationProxyV1"));
      return ObjectStreamClass.lookup(AppEvent.SerializationProxyV1.class);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/facebook/appevents/AppEventStore.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */