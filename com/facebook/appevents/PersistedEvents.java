package com.facebook.appevents;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

class PersistedEvents
  implements Serializable
{
  private static final long serialVersionUID = 20160629001L;
  private HashMap<AccessTokenAppIdPair, List<AppEvent>> events = new HashMap();
  
  public PersistedEvents() {}
  
  public PersistedEvents(HashMap<AccessTokenAppIdPair, List<AppEvent>> paramHashMap)
  {
    this.events.putAll(paramHashMap);
  }
  
  private Object writeReplace()
  {
    return new SerializationProxyV1(this.events, null);
  }
  
  public void addEvents(AccessTokenAppIdPair paramAccessTokenAppIdPair, List<AppEvent> paramList)
  {
    if (!this.events.containsKey(paramAccessTokenAppIdPair))
    {
      this.events.put(paramAccessTokenAppIdPair, paramList);
      return;
    }
    ((List)this.events.get(paramAccessTokenAppIdPair)).addAll(paramList);
  }
  
  public boolean containsKey(AccessTokenAppIdPair paramAccessTokenAppIdPair)
  {
    return this.events.containsKey(paramAccessTokenAppIdPair);
  }
  
  public List<AppEvent> get(AccessTokenAppIdPair paramAccessTokenAppIdPair)
  {
    return (List)this.events.get(paramAccessTokenAppIdPair);
  }
  
  public Set<AccessTokenAppIdPair> keySet()
  {
    return this.events.keySet();
  }
  
  static class SerializationProxyV1
    implements Serializable
  {
    private static final long serialVersionUID = 20160629001L;
    private final HashMap<AccessTokenAppIdPair, List<AppEvent>> proxyEvents;
    
    private SerializationProxyV1(HashMap<AccessTokenAppIdPair, List<AppEvent>> paramHashMap)
    {
      this.proxyEvents = paramHashMap;
    }
    
    private Object readResolve()
    {
      return new PersistedEvents(this.proxyEvents);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/facebook/appevents/PersistedEvents.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */