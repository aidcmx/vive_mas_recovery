package com.facebook.appevents.internal;

import android.os.Looper;

public class AppEventUtility
{
  public static void assertIsMainThread() {}
  
  public static void assertIsNotMainThread() {}
  
  private static boolean isMainThread()
  {
    return Looper.myLooper() == Looper.getMainLooper();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/facebook/appevents/internal/AppEventUtility.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */