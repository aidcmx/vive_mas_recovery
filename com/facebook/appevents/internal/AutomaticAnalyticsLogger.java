package com.facebook.appevents.internal;

import android.content.Context;
import android.os.Bundle;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.internal.FetchedAppSettings;
import com.facebook.internal.FetchedAppSettingsManager;

class AutomaticAnalyticsLogger
{
  public static void logActivityTimeSpentEvent(Context paramContext, String paramString1, String paramString2, long paramLong)
  {
    paramContext = AppEventsLogger.newLogger(paramContext);
    if ((FetchedAppSettingsManager.queryAppSettings(paramString1, false).getAutomaticLoggingEnabled()) && (paramLong > 0L))
    {
      paramString1 = new Bundle(1);
      paramString1.putCharSequence("fb_aa_time_spent_view_name", paramString2);
      paramContext.logEvent("fb_aa_time_spent_on_view", paramLong, paramString1);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/facebook/appevents/internal/AutomaticAnalyticsLogger.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */