package com.facebook.appevents.internal;

import com.facebook.AccessToken;
import com.facebook.appevents.AppEventsLogger;

class InternalAppEventsLogger
  extends AppEventsLogger
{
  InternalAppEventsLogger(String paramString1, String paramString2, AccessToken paramAccessToken)
  {
    super(paramString1, paramString2, paramAccessToken);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/facebook/appevents/internal/InternalAppEventsLogger.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */