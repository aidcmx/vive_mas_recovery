package com.facebook.devicerequests.internal;

import android.annotation.TargetApi;
import android.content.Context;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdManager.RegistrationListener;
import android.net.nsd.NsdServiceInfo;
import android.os.Build;
import android.os.Build.VERSION;
import com.facebook.FacebookSdk;
import com.facebook.internal.FetchedAppSettings;
import com.facebook.internal.FetchedAppSettingsManager;
import com.facebook.internal.SmartLoginOption;
import java.util.EnumSet;
import java.util.HashMap;
import org.json.JSONException;
import org.json.JSONObject;

public class DeviceRequestsHelper
{
  static final String DEVICE_INFO_DEVICE = "device";
  static final String DEVICE_INFO_MODEL = "model";
  public static final String DEVICE_INFO_PARAM = "device_info";
  static final String SDK_FLAVOR = "android";
  static final String SDK_HEADER = "fbsdk";
  static final String SERVICE_TYPE = "_fb._tcp.";
  private static HashMap<String, NsdManager.RegistrationListener> deviceRequestsListeners = new HashMap();
  
  public static void cleanUpAdvertisementService(String paramString)
  {
    cleanUpAdvertisementServiceImpl(paramString);
  }
  
  @TargetApi(16)
  private static void cleanUpAdvertisementServiceImpl(String paramString)
  {
    NsdManager.RegistrationListener localRegistrationListener = (NsdManager.RegistrationListener)deviceRequestsListeners.get(paramString);
    if (localRegistrationListener != null)
    {
      ((NsdManager)FacebookSdk.getApplicationContext().getSystemService("servicediscovery")).unregisterService(localRegistrationListener);
      deviceRequestsListeners.remove(paramString);
    }
  }
  
  public static String getDeviceInfo()
  {
    JSONObject localJSONObject = new JSONObject();
    try
    {
      localJSONObject.put("device", Build.DEVICE);
      localJSONObject.put("model", Build.MODEL);
      return localJSONObject.toString();
    }
    catch (JSONException localJSONException)
    {
      for (;;) {}
    }
  }
  
  public static boolean isAvailable()
  {
    return (Build.VERSION.SDK_INT >= 16) && (FetchedAppSettingsManager.getAppSettingsWithoutQuery(FacebookSdk.getApplicationId()).getSmartLoginOptions().contains(SmartLoginOption.Enabled));
  }
  
  public static boolean startAdvertisementService(String paramString)
  {
    if (isAvailable()) {
      return startAdvertisementServiceImpl(paramString);
    }
    return false;
  }
  
  @TargetApi(16)
  private static boolean startAdvertisementServiceImpl(final String paramString)
  {
    if (deviceRequestsListeners.containsKey(paramString)) {
      return true;
    }
    Object localObject = String.format("%s_%s_%s", new Object[] { "fbsdk", String.format("%s-%s", new Object[] { "android", FacebookSdk.getSdkVersion().replace('.', '|') }), paramString });
    NsdServiceInfo localNsdServiceInfo = new NsdServiceInfo();
    localNsdServiceInfo.setServiceType("_fb._tcp.");
    localNsdServiceInfo.setServiceName((String)localObject);
    localNsdServiceInfo.setPort(80);
    NsdManager localNsdManager = (NsdManager)FacebookSdk.getApplicationContext().getSystemService("servicediscovery");
    localObject = new NsdManager.RegistrationListener()
    {
      public void onRegistrationFailed(NsdServiceInfo paramAnonymousNsdServiceInfo, int paramAnonymousInt)
      {
        DeviceRequestsHelper.cleanUpAdvertisementService(paramString);
      }
      
      public void onServiceRegistered(NsdServiceInfo paramAnonymousNsdServiceInfo)
      {
        if (!this.val$nsdServiceName.equals(paramAnonymousNsdServiceInfo.getServiceName())) {
          DeviceRequestsHelper.cleanUpAdvertisementService(paramString);
        }
      }
      
      public void onServiceUnregistered(NsdServiceInfo paramAnonymousNsdServiceInfo) {}
      
      public void onUnregistrationFailed(NsdServiceInfo paramAnonymousNsdServiceInfo, int paramAnonymousInt) {}
    };
    deviceRequestsListeners.put(paramString, localObject);
    localNsdManager.registerService(localNsdServiceInfo, 1, (NsdManager.RegistrationListener)localObject);
    return true;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/facebook/devicerequests/internal/DeviceRequestsHelper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */