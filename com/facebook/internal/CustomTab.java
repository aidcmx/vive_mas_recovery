package com.facebook.internal;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.customtabs.CustomTabsIntent;
import android.support.customtabs.CustomTabsIntent.Builder;
import com.facebook.FacebookSdk;

public class CustomTab
{
  private Uri uri;
  
  public CustomTab(String paramString, Bundle paramBundle)
  {
    Bundle localBundle = paramBundle;
    if (paramBundle == null) {
      localBundle = new Bundle();
    }
    this.uri = Utility.buildUri(ServerProtocol.getDialogAuthority(), FacebookSdk.getGraphApiVersion() + "/" + "dialog/" + paramString, localBundle);
  }
  
  public void openCustomTab(Activity paramActivity, String paramString)
  {
    CustomTabsIntent localCustomTabsIntent = new CustomTabsIntent.Builder().build();
    localCustomTabsIntent.intent.setPackage(paramString);
    localCustomTabsIntent.intent.addFlags(1073741824);
    localCustomTabsIntent.launchUrl(paramActivity, this.uri);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/facebook/internal/CustomTab.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */