package com.facebook.internal;

public abstract interface DialogFeature
{
  public abstract String getAction();
  
  public abstract int getMinVersion();
  
  public abstract String name();
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/facebook/internal/DialogFeature.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */