package com.facebook.internal;

import android.net.Uri;
import java.util.EnumSet;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public final class FetchedAppSettings
{
  private boolean automaticLoggingEnabled;
  private boolean customTabsEnabled;
  private Map<String, Map<String, DialogFeatureConfig>> dialogConfigMap;
  private FacebookRequestErrorClassification errorClassification;
  private String nuxContent;
  private boolean nuxEnabled;
  private int sessionTimeoutInSeconds;
  private String smartLoginBookmarkIconURL;
  private String smartLoginMenuIconURL;
  private EnumSet<SmartLoginOption> smartLoginOptions;
  private boolean supportsImplicitLogging;
  
  public FetchedAppSettings(boolean paramBoolean1, String paramString1, boolean paramBoolean2, boolean paramBoolean3, int paramInt, EnumSet<SmartLoginOption> paramEnumSet, Map<String, Map<String, DialogFeatureConfig>> paramMap, boolean paramBoolean4, FacebookRequestErrorClassification paramFacebookRequestErrorClassification, String paramString2, String paramString3)
  {
    this.supportsImplicitLogging = paramBoolean1;
    this.nuxContent = paramString1;
    this.nuxEnabled = paramBoolean2;
    this.customTabsEnabled = paramBoolean3;
    this.dialogConfigMap = paramMap;
    this.errorClassification = paramFacebookRequestErrorClassification;
    this.sessionTimeoutInSeconds = paramInt;
    this.automaticLoggingEnabled = paramBoolean4;
    this.smartLoginOptions = paramEnumSet;
    this.smartLoginBookmarkIconURL = paramString2;
    this.smartLoginMenuIconURL = paramString3;
  }
  
  public static DialogFeatureConfig getDialogFeatureConfig(String paramString1, String paramString2, String paramString3)
  {
    if ((Utility.isNullOrEmpty(paramString2)) || (Utility.isNullOrEmpty(paramString3))) {}
    do
    {
      do
      {
        return null;
        paramString1 = FetchedAppSettingsManager.getAppSettingsWithoutQuery(paramString1);
      } while (paramString1 == null);
      paramString1 = (Map)paramString1.getDialogConfigurations().get(paramString2);
    } while (paramString1 == null);
    return (DialogFeatureConfig)paramString1.get(paramString3);
  }
  
  public boolean getAutomaticLoggingEnabled()
  {
    return this.automaticLoggingEnabled;
  }
  
  public boolean getCustomTabsEnabled()
  {
    return this.customTabsEnabled;
  }
  
  public Map<String, Map<String, DialogFeatureConfig>> getDialogConfigurations()
  {
    return this.dialogConfigMap;
  }
  
  public FacebookRequestErrorClassification getErrorClassification()
  {
    return this.errorClassification;
  }
  
  public String getNuxContent()
  {
    return this.nuxContent;
  }
  
  public boolean getNuxEnabled()
  {
    return this.nuxEnabled;
  }
  
  public int getSessionTimeoutInSeconds()
  {
    return this.sessionTimeoutInSeconds;
  }
  
  public String getSmartLoginBookmarkIconURL()
  {
    return this.smartLoginBookmarkIconURL;
  }
  
  public String getSmartLoginMenuIconURL()
  {
    return this.smartLoginMenuIconURL;
  }
  
  public EnumSet<SmartLoginOption> getSmartLoginOptions()
  {
    return this.smartLoginOptions;
  }
  
  public boolean supportsImplicitLogging()
  {
    return this.supportsImplicitLogging;
  }
  
  public static class DialogFeatureConfig
  {
    private static final String DIALOG_CONFIG_DIALOG_NAME_FEATURE_NAME_SEPARATOR = "\\|";
    private static final String DIALOG_CONFIG_NAME_KEY = "name";
    private static final String DIALOG_CONFIG_URL_KEY = "url";
    private static final String DIALOG_CONFIG_VERSIONS_KEY = "versions";
    private String dialogName;
    private Uri fallbackUrl;
    private String featureName;
    private int[] featureVersionSpec;
    
    private DialogFeatureConfig(String paramString1, String paramString2, Uri paramUri, int[] paramArrayOfInt)
    {
      this.dialogName = paramString1;
      this.featureName = paramString2;
      this.fallbackUrl = paramUri;
      this.featureVersionSpec = paramArrayOfInt;
    }
    
    public static DialogFeatureConfig parseDialogConfig(JSONObject paramJSONObject)
    {
      Object localObject = paramJSONObject.optString("name");
      if (Utility.isNullOrEmpty((String)localObject)) {}
      String str1;
      String str2;
      do
      {
        do
        {
          return null;
          localObject = ((String)localObject).split("\\|");
        } while (localObject.length != 2);
        str1 = localObject[0];
        str2 = localObject[1];
      } while ((Utility.isNullOrEmpty(str1)) || (Utility.isNullOrEmpty(str2)));
      String str3 = paramJSONObject.optString("url");
      localObject = null;
      if (!Utility.isNullOrEmpty(str3)) {
        localObject = Uri.parse(str3);
      }
      return new DialogFeatureConfig(str1, str2, (Uri)localObject, parseVersionSpec(paramJSONObject.optJSONArray("versions")));
    }
    
    private static int[] parseVersionSpec(JSONArray paramJSONArray)
    {
      Object localObject = null;
      if (paramJSONArray != null)
      {
        int m = paramJSONArray.length();
        int[] arrayOfInt = new int[m];
        int j = 0;
        for (;;)
        {
          localObject = arrayOfInt;
          if (j < m)
          {
            int k = paramJSONArray.optInt(j, -1);
            int i = k;
            if (k == -1)
            {
              localObject = paramJSONArray.optString(j);
              i = k;
              if (Utility.isNullOrEmpty((String)localObject)) {}
            }
            try
            {
              i = Integer.parseInt((String)localObject);
              arrayOfInt[j] = i;
              j += 1;
            }
            catch (NumberFormatException localNumberFormatException)
            {
              for (;;)
              {
                Utility.logd("FacebookSDK", localNumberFormatException);
                i = -1;
              }
            }
          }
        }
      }
      return localNumberFormatException;
    }
    
    public String getDialogName()
    {
      return this.dialogName;
    }
    
    public Uri getFallbackUrl()
    {
      return this.fallbackUrl;
    }
    
    public String getFeatureName()
    {
      return this.featureName;
    }
    
    public int[] getVersionSpec()
    {
      return this.featureVersionSpec;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/facebook/internal/FetchedAppSettings.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */