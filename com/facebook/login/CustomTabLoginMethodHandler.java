package com.facebook.login;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.v4.app.Fragment;
import com.facebook.AccessTokenSource;
import com.facebook.CustomTabMainActivity;
import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.FacebookRequestError;
import com.facebook.FacebookSdk;
import com.facebook.FacebookServiceException;
import com.facebook.internal.FetchedAppSettings;
import com.facebook.internal.FetchedAppSettingsManager;
import com.facebook.internal.Utility;
import com.facebook.internal.Validate;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

public class CustomTabLoginMethodHandler
  extends WebLoginMethodHandler
{
  private static final int API_EC_DIALOG_CANCEL = 4201;
  private static final int CHALLENGE_LENGTH = 20;
  private static final String[] CHROME_PACKAGES = { "com.android.chrome", "com.chrome.beta", "com.chrome.dev" };
  public static final Parcelable.Creator<CustomTabLoginMethodHandler> CREATOR = new Parcelable.Creator()
  {
    public CustomTabLoginMethodHandler createFromParcel(Parcel paramAnonymousParcel)
    {
      return new CustomTabLoginMethodHandler(paramAnonymousParcel);
    }
    
    public CustomTabLoginMethodHandler[] newArray(int paramAnonymousInt)
    {
      return new CustomTabLoginMethodHandler[paramAnonymousInt];
    }
  };
  private static final String CUSTOM_TABS_SERVICE_ACTION = "android.support.customtabs.action.CustomTabsService";
  private static final int CUSTOM_TAB_REQUEST_CODE = 1;
  private String currentPackage;
  private String expectedChallenge;
  
  CustomTabLoginMethodHandler(Parcel paramParcel)
  {
    super(paramParcel);
    this.expectedChallenge = paramParcel.readString();
  }
  
  CustomTabLoginMethodHandler(LoginClient paramLoginClient)
  {
    super(paramLoginClient);
    this.expectedChallenge = Utility.generateRandomString(20);
  }
  
  private String getChromePackage()
  {
    if (this.currentPackage != null) {
      return this.currentPackage;
    }
    Object localObject1 = this.loginClient.getActivity();
    Object localObject2 = new Intent("android.support.customtabs.action.CustomTabsService");
    localObject2 = ((Context)localObject1).getPackageManager().queryIntentServices((Intent)localObject2, 0);
    if (localObject2 != null)
    {
      localObject1 = new HashSet(Arrays.asList(CHROME_PACKAGES));
      localObject2 = ((List)localObject2).iterator();
      while (((Iterator)localObject2).hasNext())
      {
        ServiceInfo localServiceInfo = ((ResolveInfo)((Iterator)localObject2).next()).serviceInfo;
        if ((localServiceInfo != null) && (((Set)localObject1).contains(localServiceInfo.packageName)))
        {
          this.currentPackage = localServiceInfo.packageName;
          return this.currentPackage;
        }
      }
    }
    return null;
  }
  
  private boolean isCustomTabsAllowed()
  {
    return (isCustomTabsEnabled()) && (getChromePackage() != null) && (Validate.hasCustomTabRedirectActivity(FacebookSdk.getApplicationContext()));
  }
  
  private boolean isCustomTabsEnabled()
  {
    FetchedAppSettings localFetchedAppSettings = FetchedAppSettingsManager.getAppSettingsWithoutQuery(Utility.getMetadataApplicationId(this.loginClient.getActivity()));
    return (localFetchedAppSettings != null) && (localFetchedAppSettings.getCustomTabsEnabled());
  }
  
  private void onCustomTabComplete(String paramString, LoginClient.Request paramRequest)
  {
    Bundle localBundle;
    if ((paramString != null) && (paramString.startsWith(CustomTabMainActivity.getRedirectUrl())))
    {
      paramString = Uri.parse(paramString);
      localBundle = Utility.parseUrlQueryString(paramString.getQuery());
      localBundle.putAll(Utility.parseUrlQueryString(paramString.getFragment()));
      if (!validateChallengeParam(localBundle)) {
        super.onComplete(paramRequest, null, new FacebookException("Invalid state parameter"));
      }
    }
    else
    {
      return;
    }
    paramString = localBundle.getString("error");
    String str1 = paramString;
    if (paramString == null) {
      str1 = localBundle.getString("error_type");
    }
    String str2 = localBundle.getString("error_msg");
    paramString = str2;
    if (str2 == null) {
      paramString = localBundle.getString("error_message");
    }
    str2 = paramString;
    if (paramString == null) {
      str2 = localBundle.getString("error_description");
    }
    paramString = localBundle.getString("error_code");
    int i = -1;
    if (!Utility.isNullOrEmpty(paramString)) {}
    try
    {
      i = Integer.parseInt(paramString);
      if ((Utility.isNullOrEmpty(str1)) && (Utility.isNullOrEmpty(str2)) && (i == -1))
      {
        super.onComplete(paramRequest, localBundle, null);
        return;
      }
    }
    catch (NumberFormatException paramString)
    {
      for (;;)
      {
        i = -1;
      }
      if ((str1 != null) && ((str1.equals("access_denied")) || (str1.equals("OAuthAccessDeniedException"))))
      {
        super.onComplete(paramRequest, null, new FacebookOperationCanceledException());
        return;
      }
      if (i == 4201)
      {
        super.onComplete(paramRequest, null, new FacebookOperationCanceledException());
        return;
      }
      super.onComplete(paramRequest, null, new FacebookServiceException(new FacebookRequestError(i, str1, str2), str2));
    }
  }
  
  private boolean validateChallengeParam(Bundle paramBundle)
  {
    try
    {
      paramBundle = paramBundle.getString("state");
      if (paramBundle == null) {
        return false;
      }
      boolean bool = new JSONObject(paramBundle).getString("7_challenge").equals(this.expectedChallenge);
      return bool;
    }
    catch (JSONException paramBundle) {}
    return false;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  String getNameForLogging()
  {
    return "custom_tab";
  }
  
  protected String getSSODevice()
  {
    return "chrome_custom_tab";
  }
  
  AccessTokenSource getTokenSource()
  {
    return AccessTokenSource.CHROME_CUSTOM_TAB;
  }
  
  boolean onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if (paramInt1 != 1) {
      return super.onActivityResult(paramInt1, paramInt2, paramIntent);
    }
    LoginClient.Request localRequest = this.loginClient.getPendingRequest();
    if (paramInt2 == -1)
    {
      onCustomTabComplete(paramIntent.getStringExtra(CustomTabMainActivity.EXTRA_URL), localRequest);
      return true;
    }
    super.onComplete(localRequest, null, new FacebookOperationCanceledException());
    return false;
  }
  
  protected void putChallengeParam(JSONObject paramJSONObject)
    throws JSONException
  {
    paramJSONObject.put("7_challenge", this.expectedChallenge);
  }
  
  boolean tryAuthorize(LoginClient.Request paramRequest)
  {
    if (!isCustomTabsAllowed()) {
      return false;
    }
    paramRequest = addExtraParameters(getParameters(paramRequest), paramRequest);
    Intent localIntent = new Intent(this.loginClient.getActivity(), CustomTabMainActivity.class);
    localIntent.putExtra(CustomTabMainActivity.EXTRA_PARAMS, paramRequest);
    localIntent.putExtra(CustomTabMainActivity.EXTRA_CHROME_PACKAGE, getChromePackage());
    this.loginClient.getFragment().startActivityForResult(localIntent, 1);
    return true;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    super.writeToParcel(paramParcel, paramInt);
    paramParcel.writeString(this.expectedChallenge);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/facebook/login/CustomTabLoginMethodHandler.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */