package com.facebook.login;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.facebook.internal.NativeProtocol;

class FacebookLiteLoginMethodHandler
  extends NativeAppLoginMethodHandler
{
  public static final Parcelable.Creator<FacebookLiteLoginMethodHandler> CREATOR = new Parcelable.Creator()
  {
    public FacebookLiteLoginMethodHandler createFromParcel(Parcel paramAnonymousParcel)
    {
      return new FacebookLiteLoginMethodHandler(paramAnonymousParcel);
    }
    
    public FacebookLiteLoginMethodHandler[] newArray(int paramAnonymousInt)
    {
      return new FacebookLiteLoginMethodHandler[paramAnonymousInt];
    }
  };
  
  FacebookLiteLoginMethodHandler(Parcel paramParcel)
  {
    super(paramParcel);
  }
  
  FacebookLiteLoginMethodHandler(LoginClient paramLoginClient)
  {
    super(paramLoginClient);
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  String getNameForLogging()
  {
    return "fb_lite_login";
  }
  
  boolean tryAuthorize(LoginClient.Request paramRequest)
  {
    String str = LoginClient.getE2E();
    paramRequest = NativeProtocol.createFacebookLiteIntent(this.loginClient.getActivity(), paramRequest.getApplicationId(), paramRequest.getPermissions(), str, paramRequest.isRerequest(), paramRequest.hasPublishPermission(), paramRequest.getDefaultAudience(), getClientState(paramRequest.getAuthId()));
    addLoggingExtra("e2e", str);
    return tryIntent(paramRequest, LoginClient.getLoginRequestCode());
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    super.writeToParcel(paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/facebook/login/FacebookLiteLoginMethodHandler.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */