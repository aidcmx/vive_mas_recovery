package com.facebook.share;

public abstract interface ShareBuilder<P, E extends ShareBuilder>
{
  public abstract P build();
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/facebook/share/ShareBuilder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */