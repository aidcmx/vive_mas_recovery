package com.google.ads.mediation;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdLoader.Builder;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdRequest.Builder;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.NativeAdView;
import com.google.android.gms.ads.formats.NativeAppInstallAd;
import com.google.android.gms.ads.formats.NativeAppInstallAd.OnAppInstallAdLoadedListener;
import com.google.android.gms.ads.formats.NativeContentAd;
import com.google.android.gms.ads.formats.NativeContentAd.OnContentAdLoadedListener;
import com.google.android.gms.ads.internal.client.zzm;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.ads.mediation.MediationAdRequest;
import com.google.android.gms.ads.mediation.MediationAdapter.zza;
import com.google.android.gms.ads.mediation.MediationBannerAdapter;
import com.google.android.gms.ads.mediation.MediationBannerListener;
import com.google.android.gms.ads.mediation.MediationInterstitialListener;
import com.google.android.gms.ads.mediation.MediationNativeAdapter;
import com.google.android.gms.ads.mediation.MediationNativeListener;
import com.google.android.gms.ads.mediation.NativeAppInstallAdMapper;
import com.google.android.gms.ads.mediation.NativeContentAdMapper;
import com.google.android.gms.ads.mediation.NativeMediationAdRequest;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.google.android.gms.ads.reward.mediation.MediationRewardedVideoAdAdapter;
import com.google.android.gms.ads.reward.mediation.MediationRewardedVideoAdListener;
import com.google.android.gms.internal.zzji;
import com.google.android.gms.internal.zzmr;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

@zzji
public abstract class AbstractAdViewAdapter
  implements MediationBannerAdapter, MediationNativeAdapter, MediationRewardedVideoAdAdapter, zzmr
{
  public static final String AD_UNIT_ID_PARAMETER = "pubid";
  protected AdView zzgd;
  protected InterstitialAd zzge;
  private AdLoader zzgf;
  private Context zzgg;
  private InterstitialAd zzgh;
  private MediationRewardedVideoAdListener zzgi;
  final RewardedVideoAdListener zzgj = new RewardedVideoAdListener()
  {
    public void onRewarded(RewardItem paramAnonymousRewardItem)
    {
      AbstractAdViewAdapter.zza(AbstractAdViewAdapter.this).onRewarded(AbstractAdViewAdapter.this, paramAnonymousRewardItem);
    }
    
    public void onRewardedVideoAdClosed()
    {
      AbstractAdViewAdapter.zza(AbstractAdViewAdapter.this).onAdClosed(AbstractAdViewAdapter.this);
      AbstractAdViewAdapter.zza(AbstractAdViewAdapter.this, null);
    }
    
    public void onRewardedVideoAdFailedToLoad(int paramAnonymousInt)
    {
      AbstractAdViewAdapter.zza(AbstractAdViewAdapter.this).onAdFailedToLoad(AbstractAdViewAdapter.this, paramAnonymousInt);
    }
    
    public void onRewardedVideoAdLeftApplication()
    {
      AbstractAdViewAdapter.zza(AbstractAdViewAdapter.this).onAdLeftApplication(AbstractAdViewAdapter.this);
    }
    
    public void onRewardedVideoAdLoaded()
    {
      AbstractAdViewAdapter.zza(AbstractAdViewAdapter.this).onAdLoaded(AbstractAdViewAdapter.this);
    }
    
    public void onRewardedVideoAdOpened()
    {
      AbstractAdViewAdapter.zza(AbstractAdViewAdapter.this).onAdOpened(AbstractAdViewAdapter.this);
    }
    
    public void onRewardedVideoStarted()
    {
      AbstractAdViewAdapter.zza(AbstractAdViewAdapter.this).onVideoStarted(AbstractAdViewAdapter.this);
    }
  };
  
  public String getAdUnitId(Bundle paramBundle)
  {
    return paramBundle.getString("pubid");
  }
  
  public View getBannerView()
  {
    return this.zzgd;
  }
  
  public Bundle getInterstitialAdapterInfo()
  {
    return new MediationAdapter.zza().zzbk(1).zzys();
  }
  
  public void initialize(Context paramContext, MediationAdRequest paramMediationAdRequest, String paramString, MediationRewardedVideoAdListener paramMediationRewardedVideoAdListener, Bundle paramBundle1, Bundle paramBundle2)
  {
    this.zzgg = paramContext.getApplicationContext();
    this.zzgi = paramMediationRewardedVideoAdListener;
    this.zzgi.onInitializationSucceeded(this);
  }
  
  public boolean isInitialized()
  {
    return this.zzgi != null;
  }
  
  public void loadAd(MediationAdRequest paramMediationAdRequest, Bundle paramBundle1, Bundle paramBundle2)
  {
    if ((this.zzgg == null) || (this.zzgi == null))
    {
      zzb.e("AdMobAdapter.loadAd called before initialize.");
      return;
    }
    this.zzgh = new InterstitialAd(this.zzgg);
    this.zzgh.zzd(true);
    this.zzgh.setAdUnitId(getAdUnitId(paramBundle1));
    this.zzgh.setRewardedVideoAdListener(this.zzgj);
    this.zzgh.loadAd(zza(this.zzgg, paramMediationAdRequest, paramBundle2, paramBundle1));
  }
  
  public void onDestroy()
  {
    if (this.zzgd != null)
    {
      this.zzgd.destroy();
      this.zzgd = null;
    }
    if (this.zzge != null) {
      this.zzge = null;
    }
    if (this.zzgf != null) {
      this.zzgf = null;
    }
    if (this.zzgh != null) {
      this.zzgh = null;
    }
  }
  
  public void onPause()
  {
    if (this.zzgd != null) {
      this.zzgd.pause();
    }
  }
  
  public void onResume()
  {
    if (this.zzgd != null) {
      this.zzgd.resume();
    }
  }
  
  public void requestBannerAd(Context paramContext, MediationBannerListener paramMediationBannerListener, Bundle paramBundle1, AdSize paramAdSize, MediationAdRequest paramMediationAdRequest, Bundle paramBundle2)
  {
    this.zzgd = new AdView(paramContext);
    this.zzgd.setAdSize(new AdSize(paramAdSize.getWidth(), paramAdSize.getHeight()));
    this.zzgd.setAdUnitId(getAdUnitId(paramBundle1));
    this.zzgd.setAdListener(new zzc(this, paramMediationBannerListener));
    this.zzgd.loadAd(zza(paramContext, paramMediationAdRequest, paramBundle2, paramBundle1));
  }
  
  public void requestInterstitialAd(Context paramContext, MediationInterstitialListener paramMediationInterstitialListener, Bundle paramBundle1, MediationAdRequest paramMediationAdRequest, Bundle paramBundle2)
  {
    this.zzge = new InterstitialAd(paramContext);
    this.zzge.setAdUnitId(getAdUnitId(paramBundle1));
    this.zzge.setAdListener(new zzd(this, paramMediationInterstitialListener));
    this.zzge.loadAd(zza(paramContext, paramMediationAdRequest, paramBundle2, paramBundle1));
  }
  
  public void requestNativeAd(Context paramContext, MediationNativeListener paramMediationNativeListener, Bundle paramBundle1, NativeMediationAdRequest paramNativeMediationAdRequest, Bundle paramBundle2)
  {
    paramMediationNativeListener = new zze(this, paramMediationNativeListener);
    AdLoader.Builder localBuilder = zza(paramContext, paramBundle1.getString("pubid")).withAdListener(paramMediationNativeListener);
    NativeAdOptions localNativeAdOptions = paramNativeMediationAdRequest.getNativeAdOptions();
    if (localNativeAdOptions != null) {
      localBuilder.withNativeAdOptions(localNativeAdOptions);
    }
    if (paramNativeMediationAdRequest.isAppInstallAdRequested()) {
      localBuilder.forAppInstallAd(paramMediationNativeListener);
    }
    if (paramNativeMediationAdRequest.isContentAdRequested()) {
      localBuilder.forContentAd(paramMediationNativeListener);
    }
    this.zzgf = localBuilder.build();
    this.zzgf.loadAd(zza(paramContext, paramNativeMediationAdRequest, paramBundle2, paramBundle1));
  }
  
  public void showInterstitial()
  {
    this.zzge.show();
  }
  
  public void showVideo()
  {
    this.zzgh.show();
  }
  
  protected abstract Bundle zza(Bundle paramBundle1, Bundle paramBundle2);
  
  AdLoader.Builder zza(Context paramContext, String paramString)
  {
    return new AdLoader.Builder(paramContext, paramString);
  }
  
  AdRequest zza(Context paramContext, MediationAdRequest paramMediationAdRequest, Bundle paramBundle1, Bundle paramBundle2)
  {
    AdRequest.Builder localBuilder = new AdRequest.Builder();
    Object localObject = paramMediationAdRequest.getBirthday();
    if (localObject != null) {
      localBuilder.setBirthday((Date)localObject);
    }
    int i = paramMediationAdRequest.getGender();
    if (i != 0) {
      localBuilder.setGender(i);
    }
    localObject = paramMediationAdRequest.getKeywords();
    if (localObject != null)
    {
      localObject = ((Set)localObject).iterator();
      while (((Iterator)localObject).hasNext()) {
        localBuilder.addKeyword((String)((Iterator)localObject).next());
      }
    }
    localObject = paramMediationAdRequest.getLocation();
    if (localObject != null) {
      localBuilder.setLocation((Location)localObject);
    }
    if (paramMediationAdRequest.isTesting()) {
      localBuilder.addTestDevice(zzm.zzkr().zzao(paramContext));
    }
    if (paramMediationAdRequest.taggedForChildDirectedTreatment() != -1) {
      if (paramMediationAdRequest.taggedForChildDirectedTreatment() != 1) {
        break label210;
      }
    }
    label210:
    for (boolean bool = true;; bool = false)
    {
      localBuilder.tagForChildDirectedTreatment(bool);
      localBuilder.setIsDesignedForFamilies(paramMediationAdRequest.isDesignedForFamilies());
      localBuilder.addNetworkExtrasBundle(AdMobAdapter.class, zza(paramBundle1, paramBundle2));
      return localBuilder.build();
    }
  }
  
  static class zza
    extends NativeAppInstallAdMapper
  {
    private final NativeAppInstallAd zzgl;
    
    public zza(NativeAppInstallAd paramNativeAppInstallAd)
    {
      this.zzgl = paramNativeAppInstallAd;
      setHeadline(paramNativeAppInstallAd.getHeadline().toString());
      setImages(paramNativeAppInstallAd.getImages());
      setBody(paramNativeAppInstallAd.getBody().toString());
      setIcon(paramNativeAppInstallAd.getIcon());
      setCallToAction(paramNativeAppInstallAd.getCallToAction().toString());
      if (paramNativeAppInstallAd.getStarRating() != null) {
        setStarRating(paramNativeAppInstallAd.getStarRating().doubleValue());
      }
      if (paramNativeAppInstallAd.getStore() != null) {
        setStore(paramNativeAppInstallAd.getStore().toString());
      }
      if (paramNativeAppInstallAd.getPrice() != null) {
        setPrice(paramNativeAppInstallAd.getPrice().toString());
      }
      setOverrideImpressionRecording(true);
      setOverrideClickHandling(true);
      zza(paramNativeAppInstallAd.getVideoController());
    }
    
    public void trackView(View paramView)
    {
      if ((paramView instanceof NativeAdView)) {
        ((NativeAdView)paramView).setNativeAd(this.zzgl);
      }
    }
  }
  
  static class zzb
    extends NativeContentAdMapper
  {
    private final NativeContentAd zzgm;
    
    public zzb(NativeContentAd paramNativeContentAd)
    {
      this.zzgm = paramNativeContentAd;
      setHeadline(paramNativeContentAd.getHeadline().toString());
      setImages(paramNativeContentAd.getImages());
      setBody(paramNativeContentAd.getBody().toString());
      if (paramNativeContentAd.getLogo() != null) {
        setLogo(paramNativeContentAd.getLogo());
      }
      setCallToAction(paramNativeContentAd.getCallToAction().toString());
      setAdvertiser(paramNativeContentAd.getAdvertiser().toString());
      setOverrideImpressionRecording(true);
      setOverrideClickHandling(true);
    }
    
    public void trackView(View paramView)
    {
      if ((paramView instanceof NativeAdView)) {
        ((NativeAdView)paramView).setNativeAd(this.zzgm);
      }
    }
  }
  
  static final class zzc
    extends AdListener
    implements com.google.android.gms.ads.internal.client.zza
  {
    final AbstractAdViewAdapter zzgn;
    final MediationBannerListener zzgo;
    
    public zzc(AbstractAdViewAdapter paramAbstractAdViewAdapter, MediationBannerListener paramMediationBannerListener)
    {
      this.zzgn = paramAbstractAdViewAdapter;
      this.zzgo = paramMediationBannerListener;
    }
    
    public void onAdClicked()
    {
      this.zzgo.onAdClicked(this.zzgn);
    }
    
    public void onAdClosed()
    {
      this.zzgo.onAdClosed(this.zzgn);
    }
    
    public void onAdFailedToLoad(int paramInt)
    {
      this.zzgo.onAdFailedToLoad(this.zzgn, paramInt);
    }
    
    public void onAdLeftApplication()
    {
      this.zzgo.onAdLeftApplication(this.zzgn);
    }
    
    public void onAdLoaded()
    {
      this.zzgo.onAdLoaded(this.zzgn);
    }
    
    public void onAdOpened()
    {
      this.zzgo.onAdOpened(this.zzgn);
    }
  }
  
  static final class zzd
    extends AdListener
    implements com.google.android.gms.ads.internal.client.zza
  {
    final AbstractAdViewAdapter zzgn;
    final MediationInterstitialListener zzgp;
    
    public zzd(AbstractAdViewAdapter paramAbstractAdViewAdapter, MediationInterstitialListener paramMediationInterstitialListener)
    {
      this.zzgn = paramAbstractAdViewAdapter;
      this.zzgp = paramMediationInterstitialListener;
    }
    
    public void onAdClicked()
    {
      this.zzgp.onAdClicked(this.zzgn);
    }
    
    public void onAdClosed()
    {
      this.zzgp.onAdClosed(this.zzgn);
    }
    
    public void onAdFailedToLoad(int paramInt)
    {
      this.zzgp.onAdFailedToLoad(this.zzgn, paramInt);
    }
    
    public void onAdLeftApplication()
    {
      this.zzgp.onAdLeftApplication(this.zzgn);
    }
    
    public void onAdLoaded()
    {
      this.zzgp.onAdLoaded(this.zzgn);
    }
    
    public void onAdOpened()
    {
      this.zzgp.onAdOpened(this.zzgn);
    }
  }
  
  static final class zze
    extends AdListener
    implements NativeAppInstallAd.OnAppInstallAdLoadedListener, NativeContentAd.OnContentAdLoadedListener, com.google.android.gms.ads.internal.client.zza
  {
    final AbstractAdViewAdapter zzgn;
    final MediationNativeListener zzgq;
    
    public zze(AbstractAdViewAdapter paramAbstractAdViewAdapter, MediationNativeListener paramMediationNativeListener)
    {
      this.zzgn = paramAbstractAdViewAdapter;
      this.zzgq = paramMediationNativeListener;
    }
    
    public void onAdClicked()
    {
      this.zzgq.onAdClicked(this.zzgn);
    }
    
    public void onAdClosed()
    {
      this.zzgq.onAdClosed(this.zzgn);
    }
    
    public void onAdFailedToLoad(int paramInt)
    {
      this.zzgq.onAdFailedToLoad(this.zzgn, paramInt);
    }
    
    public void onAdLeftApplication()
    {
      this.zzgq.onAdLeftApplication(this.zzgn);
    }
    
    public void onAdLoaded() {}
    
    public void onAdOpened()
    {
      this.zzgq.onAdOpened(this.zzgn);
    }
    
    public void onAppInstallAdLoaded(NativeAppInstallAd paramNativeAppInstallAd)
    {
      this.zzgq.onAdLoaded(this.zzgn, new AbstractAdViewAdapter.zza(paramNativeAppInstallAd));
    }
    
    public void onContentAdLoaded(NativeContentAd paramNativeContentAd)
    {
      this.zzgq.onAdLoaded(this.zzgn, new AbstractAdViewAdapter.zzb(paramNativeContentAd));
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/ads/mediation/AbstractAdViewAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */