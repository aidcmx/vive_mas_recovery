package com.google.ads.mediation;

import android.location.Location;
import com.google.ads.AdRequest.Gender;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;

@Deprecated
public class MediationAdRequest
{
  private final Date zzgr;
  private final AdRequest.Gender zzgs;
  private final Set<String> zzgt;
  private final boolean zzgu;
  private final Location zzgv;
  
  public MediationAdRequest(Date paramDate, AdRequest.Gender paramGender, Set<String> paramSet, boolean paramBoolean, Location paramLocation)
  {
    this.zzgr = paramDate;
    this.zzgs = paramGender;
    this.zzgt = paramSet;
    this.zzgu = paramBoolean;
    this.zzgv = paramLocation;
  }
  
  public Integer getAgeInYears()
  {
    if (this.zzgr != null)
    {
      Calendar localCalendar1 = Calendar.getInstance();
      Calendar localCalendar2 = Calendar.getInstance();
      localCalendar1.setTime(this.zzgr);
      Integer localInteger2 = Integer.valueOf(localCalendar2.get(1) - localCalendar1.get(1));
      Integer localInteger1;
      if (localCalendar2.get(2) >= localCalendar1.get(2))
      {
        localInteger1 = localInteger2;
        if (localCalendar2.get(2) == localCalendar1.get(2))
        {
          localInteger1 = localInteger2;
          if (localCalendar2.get(5) >= localCalendar1.get(5)) {}
        }
      }
      else
      {
        localInteger1 = Integer.valueOf(localInteger2.intValue() - 1);
      }
      return localInteger1;
    }
    return null;
  }
  
  public Date getBirthday()
  {
    return this.zzgr;
  }
  
  public AdRequest.Gender getGender()
  {
    return this.zzgs;
  }
  
  public Set<String> getKeywords()
  {
    return this.zzgt;
  }
  
  public Location getLocation()
  {
    return this.zzgv;
  }
  
  public boolean isTesting()
  {
    return this.zzgu;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/ads/mediation/MediationAdRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */