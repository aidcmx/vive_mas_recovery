package com.google.ads.mediation.customevent;

import android.app.Activity;
import android.view.View;
import com.google.ads.AdRequest.ErrorCode;
import com.google.ads.AdSize;
import com.google.ads.mediation.MediationAdRequest;
import com.google.ads.mediation.MediationBannerAdapter;
import com.google.ads.mediation.MediationBannerListener;
import com.google.ads.mediation.MediationInterstitialAdapter;
import com.google.ads.mediation.MediationInterstitialListener;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.ads.mediation.customevent.CustomEventExtras;
import com.google.android.gms.common.annotation.KeepName;

@KeepName
public final class CustomEventAdapter
  implements MediationBannerAdapter<CustomEventExtras, CustomEventServerParameters>, MediationInterstitialAdapter<CustomEventExtras, CustomEventServerParameters>
{
  private View zzgw;
  CustomEventBanner zzgx;
  CustomEventInterstitial zzgy;
  
  private void zza(View paramView)
  {
    this.zzgw = paramView;
  }
  
  private static <T> T zzj(String paramString)
  {
    try
    {
      Object localObject = Class.forName(paramString).newInstance();
      return (T)localObject;
    }
    catch (Throwable localThrowable)
    {
      String str = String.valueOf(localThrowable.getMessage());
      zzb.zzdi(String.valueOf(paramString).length() + 46 + String.valueOf(str).length() + "Could not instantiate custom event adapter: " + paramString + ". " + str);
    }
    return null;
  }
  
  public void destroy()
  {
    if (this.zzgx != null) {
      this.zzgx.destroy();
    }
    if (this.zzgy != null) {
      this.zzgy.destroy();
    }
  }
  
  public Class<CustomEventExtras> getAdditionalParametersType()
  {
    return CustomEventExtras.class;
  }
  
  public View getBannerView()
  {
    return this.zzgw;
  }
  
  public Class<CustomEventServerParameters> getServerParametersType()
  {
    return CustomEventServerParameters.class;
  }
  
  public void requestBannerAd(MediationBannerListener paramMediationBannerListener, Activity paramActivity, CustomEventServerParameters paramCustomEventServerParameters, AdSize paramAdSize, MediationAdRequest paramMediationAdRequest, CustomEventExtras paramCustomEventExtras)
  {
    this.zzgx = ((CustomEventBanner)zzj(paramCustomEventServerParameters.className));
    if (this.zzgx == null)
    {
      paramMediationBannerListener.onFailedToReceiveAd(this, AdRequest.ErrorCode.INTERNAL_ERROR);
      return;
    }
    if (paramCustomEventExtras == null) {}
    for (paramCustomEventExtras = null;; paramCustomEventExtras = paramCustomEventExtras.getExtra(paramCustomEventServerParameters.label))
    {
      this.zzgx.requestBannerAd(new zza(this, paramMediationBannerListener), paramActivity, paramCustomEventServerParameters.label, paramCustomEventServerParameters.parameter, paramAdSize, paramMediationAdRequest, paramCustomEventExtras);
      return;
    }
  }
  
  public void requestInterstitialAd(MediationInterstitialListener paramMediationInterstitialListener, Activity paramActivity, CustomEventServerParameters paramCustomEventServerParameters, MediationAdRequest paramMediationAdRequest, CustomEventExtras paramCustomEventExtras)
  {
    this.zzgy = ((CustomEventInterstitial)zzj(paramCustomEventServerParameters.className));
    if (this.zzgy == null)
    {
      paramMediationInterstitialListener.onFailedToReceiveAd(this, AdRequest.ErrorCode.INTERNAL_ERROR);
      return;
    }
    if (paramCustomEventExtras == null) {}
    for (paramCustomEventExtras = null;; paramCustomEventExtras = paramCustomEventExtras.getExtra(paramCustomEventServerParameters.label))
    {
      this.zzgy.requestInterstitialAd(zza(paramMediationInterstitialListener), paramActivity, paramCustomEventServerParameters.label, paramCustomEventServerParameters.parameter, paramMediationAdRequest, paramCustomEventExtras);
      return;
    }
  }
  
  public void showInterstitial()
  {
    this.zzgy.showInterstitial();
  }
  
  zzb zza(MediationInterstitialListener paramMediationInterstitialListener)
  {
    return new zzb(this, paramMediationInterstitialListener);
  }
  
  static final class zza
    implements CustomEventBannerListener
  {
    private final CustomEventAdapter zzgz;
    private final MediationBannerListener zzha;
    
    public zza(CustomEventAdapter paramCustomEventAdapter, MediationBannerListener paramMediationBannerListener)
    {
      this.zzgz = paramCustomEventAdapter;
      this.zzha = paramMediationBannerListener;
    }
    
    public void onClick()
    {
      zzb.zzdg("Custom event adapter called onFailedToReceiveAd.");
      this.zzha.onClick(this.zzgz);
    }
    
    public void onDismissScreen()
    {
      zzb.zzdg("Custom event adapter called onFailedToReceiveAd.");
      this.zzha.onDismissScreen(this.zzgz);
    }
    
    public void onFailedToReceiveAd()
    {
      zzb.zzdg("Custom event adapter called onFailedToReceiveAd.");
      this.zzha.onFailedToReceiveAd(this.zzgz, AdRequest.ErrorCode.NO_FILL);
    }
    
    public void onLeaveApplication()
    {
      zzb.zzdg("Custom event adapter called onFailedToReceiveAd.");
      this.zzha.onLeaveApplication(this.zzgz);
    }
    
    public void onPresentScreen()
    {
      zzb.zzdg("Custom event adapter called onFailedToReceiveAd.");
      this.zzha.onPresentScreen(this.zzgz);
    }
    
    public void onReceivedAd(View paramView)
    {
      zzb.zzdg("Custom event adapter called onReceivedAd.");
      CustomEventAdapter.zza(this.zzgz, paramView);
      this.zzha.onReceivedAd(this.zzgz);
    }
  }
  
  class zzb
    implements CustomEventInterstitialListener
  {
    private final CustomEventAdapter zzgz;
    private final MediationInterstitialListener zzhb;
    
    public zzb(CustomEventAdapter paramCustomEventAdapter, MediationInterstitialListener paramMediationInterstitialListener)
    {
      this.zzgz = paramCustomEventAdapter;
      this.zzhb = paramMediationInterstitialListener;
    }
    
    public void onDismissScreen()
    {
      zzb.zzdg("Custom event adapter called onDismissScreen.");
      this.zzhb.onDismissScreen(this.zzgz);
    }
    
    public void onFailedToReceiveAd()
    {
      zzb.zzdg("Custom event adapter called onFailedToReceiveAd.");
      this.zzhb.onFailedToReceiveAd(this.zzgz, AdRequest.ErrorCode.NO_FILL);
    }
    
    public void onLeaveApplication()
    {
      zzb.zzdg("Custom event adapter called onLeaveApplication.");
      this.zzhb.onLeaveApplication(this.zzgz);
    }
    
    public void onPresentScreen()
    {
      zzb.zzdg("Custom event adapter called onPresentScreen.");
      this.zzhb.onPresentScreen(this.zzgz);
    }
    
    public void onReceivedAd()
    {
      zzb.zzdg("Custom event adapter called onReceivedAd.");
      this.zzhb.onReceivedAd(CustomEventAdapter.this);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/ads/mediation/customevent/CustomEventAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */