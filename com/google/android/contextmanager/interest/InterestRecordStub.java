package com.google.android.contextmanager.interest;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.util.Log;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.internal.zzare.zzb;
import com.google.android.gms.internal.zzarz;
import com.google.android.gms.internal.zzasa;

public class InterestRecordStub
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<InterestRecordStub> CREATOR = new zza();
  private final int mVersionCode;
  private final zzare.zzb zzajx;
  
  InterestRecordStub(int paramInt, byte[] paramArrayOfByte)
  {
    this.mVersionCode = paramInt;
    Object localObject = null;
    try
    {
      paramArrayOfByte = zzare.zzb.zzbc(paramArrayOfByte);
      this.zzajx = paramArrayOfByte;
      return;
    }
    catch (zzarz paramArrayOfByte)
    {
      for (;;)
      {
        Log.e("InterestRecordStub", "Could not deserialize interest bytes.", paramArrayOfByte);
        paramArrayOfByte = (byte[])localObject;
      }
    }
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zza.zza(this, paramParcel, paramInt);
  }
  
  byte[] zzdr()
  {
    return zzasa.zzf(this.zzajx);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/contextmanager/interest/InterestRecordStub.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */