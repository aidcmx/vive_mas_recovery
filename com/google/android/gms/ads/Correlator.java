package com.google.android.gms.ads;

import com.google.android.gms.ads.internal.client.zzn;
import com.google.android.gms.internal.zzji;

@zzji
public final class Correlator
{
  private zzn zzakl = new zzn();
  
  public void reset()
  {
    this.zzakl.zzkt();
  }
  
  public zzn zzdu()
  {
    return this.zzakl;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/Correlator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */