package com.google.android.gms.ads;

import android.content.Context;
import android.support.annotation.RequiresPermission;
import com.google.android.gms.ads.internal.client.zza;
import com.google.android.gms.ads.internal.client.zzaf;
import com.google.android.gms.ads.purchase.InAppPurchaseListener;
import com.google.android.gms.ads.purchase.PlayStorePurchaseListener;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

public final class InterstitialAd
{
  private final zzaf zzakm;
  
  public InterstitialAd(Context paramContext)
  {
    this.zzakm = new zzaf(paramContext);
  }
  
  public AdListener getAdListener()
  {
    return this.zzakm.getAdListener();
  }
  
  public String getAdUnitId()
  {
    return this.zzakm.getAdUnitId();
  }
  
  public InAppPurchaseListener getInAppPurchaseListener()
  {
    return this.zzakm.getInAppPurchaseListener();
  }
  
  public String getMediationAdapterClassName()
  {
    return this.zzakm.getMediationAdapterClassName();
  }
  
  public boolean isLoaded()
  {
    return this.zzakm.isLoaded();
  }
  
  public boolean isLoading()
  {
    return this.zzakm.isLoading();
  }
  
  @RequiresPermission("android.permission.INTERNET")
  public void loadAd(AdRequest paramAdRequest)
  {
    this.zzakm.zza(paramAdRequest.zzdt());
  }
  
  public void setAdListener(AdListener paramAdListener)
  {
    this.zzakm.setAdListener(paramAdListener);
    if ((paramAdListener != null) && ((paramAdListener instanceof zza))) {
      this.zzakm.zza((zza)paramAdListener);
    }
    while (paramAdListener != null) {
      return;
    }
    this.zzakm.zza(null);
  }
  
  public void setAdUnitId(String paramString)
  {
    this.zzakm.setAdUnitId(paramString);
  }
  
  public void setInAppPurchaseListener(InAppPurchaseListener paramInAppPurchaseListener)
  {
    this.zzakm.setInAppPurchaseListener(paramInAppPurchaseListener);
  }
  
  public void setPlayStorePurchaseParams(PlayStorePurchaseListener paramPlayStorePurchaseListener, String paramString)
  {
    this.zzakm.setPlayStorePurchaseParams(paramPlayStorePurchaseListener, paramString);
  }
  
  public void setRewardedVideoAdListener(RewardedVideoAdListener paramRewardedVideoAdListener)
  {
    this.zzakm.setRewardedVideoAdListener(paramRewardedVideoAdListener);
  }
  
  public void show()
  {
    this.zzakm.show();
  }
  
  public void zzd(boolean paramBoolean)
  {
    this.zzakm.zzd(paramBoolean);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/InterstitialAd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */