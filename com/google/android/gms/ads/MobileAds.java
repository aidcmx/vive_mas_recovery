package com.google.android.gms.ads;

import android.content.Context;
import android.support.annotation.RequiresPermission;
import com.google.android.gms.ads.internal.client.zzag;
import com.google.android.gms.ads.internal.client.zzah;
import com.google.android.gms.ads.reward.RewardedVideoAd;

public class MobileAds
{
  public static RewardedVideoAd getRewardedVideoAdInstance(Context paramContext)
  {
    return zzag.zzli().getRewardedVideoAdInstance(paramContext);
  }
  
  public static void initialize(Context paramContext)
  {
    initialize(paramContext, null, null);
  }
  
  @RequiresPermission("android.permission.INTERNET")
  public static void initialize(Context paramContext, String paramString)
  {
    initialize(paramContext, paramString, null);
  }
  
  @Deprecated
  @RequiresPermission("android.permission.INTERNET")
  public static void initialize(Context paramContext, String paramString, Settings paramSettings)
  {
    zzag localzzag = zzag.zzli();
    if (paramSettings == null) {}
    for (paramSettings = null;; paramSettings = paramSettings.zzdv())
    {
      localzzag.zza(paramContext, paramString, paramSettings);
      return;
    }
  }
  
  public static void openDebugMenu(Context paramContext, String paramString)
  {
    zzag.zzli().openDebugMenu(paramContext, paramString);
  }
  
  public static void setAppMuted(boolean paramBoolean)
  {
    zzag.zzli().setAppMuted(paramBoolean);
  }
  
  public static void setAppVolume(float paramFloat)
  {
    zzag.zzli().setAppVolume(paramFloat);
  }
  
  public static final class Settings
  {
    private final zzah zzakn = new zzah();
    
    @Deprecated
    public String getTrackingId()
    {
      zzah localzzah = this.zzakn;
      return null;
    }
    
    @Deprecated
    public boolean isGoogleAnalyticsEnabled()
    {
      zzah localzzah = this.zzakn;
      return false;
    }
    
    @Deprecated
    public Settings setGoogleAnalyticsEnabled(boolean paramBoolean)
    {
      this.zzakn.zzq(paramBoolean);
      return this;
    }
    
    @Deprecated
    public Settings setTrackingId(String paramString)
    {
      this.zzakn.zzav(paramString);
      return this;
    }
    
    zzah zzdv()
    {
      return this.zzakn;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/MobileAds.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */