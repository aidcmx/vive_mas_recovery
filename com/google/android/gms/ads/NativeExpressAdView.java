package com.google.android.gms.ads;

import android.content.Context;
import android.util.AttributeSet;
import com.google.android.gms.ads.internal.client.zzae;
import com.google.android.gms.internal.zzji;

@zzji
public final class NativeExpressAdView
  extends BaseAdView
{
  public NativeExpressAdView(Context paramContext)
  {
    super(paramContext, 1);
  }
  
  public NativeExpressAdView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet, 1);
  }
  
  public NativeExpressAdView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt, 1);
  }
  
  public VideoController getVideoController()
  {
    return this.zzakk.getVideoController();
  }
  
  public VideoOptions getVideoOptions()
  {
    return this.zzakk.getVideoOptions();
  }
  
  public void setVideoOptions(VideoOptions paramVideoOptions)
  {
    this.zzakk.setVideoOptions(paramVideoOptions);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/NativeExpressAdView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */