package com.google.android.gms.ads;

import android.os.RemoteException;
import android.support.annotation.Nullable;
import com.google.android.gms.ads.internal.client.zzab;
import com.google.android.gms.ads.internal.client.zzap;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.internal.zzji;

@zzji
public final class VideoController
{
  private final Object zzako = new Object();
  @Nullable
  private zzab zzakp;
  @Nullable
  private VideoLifecycleCallbacks zzakq;
  
  public float getAspectRatio()
  {
    float f;
    synchronized (this.zzako)
    {
      if (this.zzakp == null) {
        return 0.0F;
      }
    }
    return 0.0F;
  }
  
  @Nullable
  public VideoLifecycleCallbacks getVideoLifecycleCallbacks()
  {
    synchronized (this.zzako)
    {
      VideoLifecycleCallbacks localVideoLifecycleCallbacks = this.zzakq;
      return localVideoLifecycleCallbacks;
    }
  }
  
  public boolean hasVideoContent()
  {
    for (;;)
    {
      synchronized (this.zzako)
      {
        if (this.zzakp != null)
        {
          bool = true;
          return bool;
        }
      }
      boolean bool = false;
    }
  }
  
  public void setVideoLifecycleCallbacks(VideoLifecycleCallbacks paramVideoLifecycleCallbacks)
  {
    zzaa.zzb(paramVideoLifecycleCallbacks, "VideoLifecycleCallbacks may not be null.");
    synchronized (this.zzako)
    {
      this.zzakq = paramVideoLifecycleCallbacks;
      if (this.zzakp == null) {
        return;
      }
    }
    try
    {
      this.zzakp.zza(new zzap(paramVideoLifecycleCallbacks));
      return;
      paramVideoLifecycleCallbacks = finally;
      throw paramVideoLifecycleCallbacks;
    }
    catch (RemoteException paramVideoLifecycleCallbacks)
    {
      for (;;)
      {
        zzb.zzb("Unable to call setVideoLifecycleCallbacks on video controller.", paramVideoLifecycleCallbacks);
      }
    }
  }
  
  public void zza(zzab paramzzab)
  {
    synchronized (this.zzako)
    {
      this.zzakp = paramzzab;
      if (this.zzakq != null) {
        setVideoLifecycleCallbacks(this.zzakq);
      }
      return;
    }
  }
  
  public zzab zzdw()
  {
    synchronized (this.zzako)
    {
      zzab localzzab = this.zzakp;
      return localzzab;
    }
  }
  
  public static abstract class VideoLifecycleCallbacks
  {
    public void onVideoEnd() {}
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/VideoController.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */