package com.google.android.gms.ads;

import com.google.android.gms.internal.zzji;

@zzji
public final class VideoOptions
{
  private final boolean zzakr;
  
  private VideoOptions(Builder paramBuilder)
  {
    this.zzakr = Builder.zza(paramBuilder);
  }
  
  public boolean getStartMuted()
  {
    return this.zzakr;
  }
  
  public static final class Builder
  {
    private boolean zzakr = false;
    
    public VideoOptions build()
    {
      return new VideoOptions(this, null);
    }
    
    public Builder setStartMuted(boolean paramBoolean)
    {
      this.zzakr = paramBoolean;
      return this;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/VideoOptions.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */