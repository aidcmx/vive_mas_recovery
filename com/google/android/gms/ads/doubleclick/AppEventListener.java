package com.google.android.gms.ads.doubleclick;

public abstract interface AppEventListener
{
  public abstract void onAppEvent(String paramString1, String paramString2);
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/doubleclick/AppEventListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */