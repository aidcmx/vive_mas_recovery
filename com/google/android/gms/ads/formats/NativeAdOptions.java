package com.google.android.gms.ads.formats;

import android.support.annotation.Nullable;
import com.google.android.gms.ads.VideoOptions;
import com.google.android.gms.internal.zzji;
import java.lang.annotation.Annotation;

@zzji
public final class NativeAdOptions
{
  public static final int ADCHOICES_BOTTOM_LEFT = 3;
  public static final int ADCHOICES_BOTTOM_RIGHT = 2;
  public static final int ADCHOICES_TOP_LEFT = 0;
  public static final int ADCHOICES_TOP_RIGHT = 1;
  public static final int ORIENTATION_ANY = 0;
  public static final int ORIENTATION_LANDSCAPE = 2;
  public static final int ORIENTATION_PORTRAIT = 1;
  private final boolean zzaky;
  private final int zzakz;
  private final boolean zzala;
  private final int zzalb;
  private final VideoOptions zzalc;
  
  private NativeAdOptions(Builder paramBuilder)
  {
    this.zzaky = Builder.zza(paramBuilder);
    this.zzakz = Builder.zzb(paramBuilder);
    this.zzala = Builder.zzc(paramBuilder);
    this.zzalb = Builder.zzd(paramBuilder);
    this.zzalc = Builder.zze(paramBuilder);
  }
  
  public int getAdChoicesPlacement()
  {
    return this.zzalb;
  }
  
  public int getImageOrientation()
  {
    return this.zzakz;
  }
  
  @Nullable
  public VideoOptions getVideoOptions()
  {
    return this.zzalc;
  }
  
  public boolean shouldRequestMultipleImages()
  {
    return this.zzala;
  }
  
  public boolean shouldReturnUrlsForImageAssets()
  {
    return this.zzaky;
  }
  
  public static @interface AdChoicesPlacement {}
  
  public static final class Builder
  {
    private boolean zzaky = false;
    private int zzakz = 0;
    private boolean zzala = false;
    private int zzalb = 1;
    private VideoOptions zzalc;
    
    public NativeAdOptions build()
    {
      return new NativeAdOptions(this, null);
    }
    
    public Builder setAdChoicesPlacement(@NativeAdOptions.AdChoicesPlacement int paramInt)
    {
      this.zzalb = paramInt;
      return this;
    }
    
    public Builder setImageOrientation(int paramInt)
    {
      this.zzakz = paramInt;
      return this;
    }
    
    public Builder setRequestMultipleImages(boolean paramBoolean)
    {
      this.zzala = paramBoolean;
      return this;
    }
    
    public Builder setReturnUrlsForImageAssets(boolean paramBoolean)
    {
      this.zzaky = paramBoolean;
      return this;
    }
    
    public Builder setVideoOptions(VideoOptions paramVideoOptions)
    {
      this.zzalc = paramVideoOptions;
      return this;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/formats/NativeAdOptions.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */