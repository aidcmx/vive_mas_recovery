package com.google.android.gms.ads.internal;

import android.app.Activity;
import android.content.Context;
import android.os.RemoteException;
import android.support.annotation.Keep;
import android.support.annotation.Nullable;
import android.widget.FrameLayout;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.client.zzs;
import com.google.android.gms.ads.internal.client.zzu;
import com.google.android.gms.ads.internal.client.zzx.zza;
import com.google.android.gms.ads.internal.client.zzz;
import com.google.android.gms.ads.internal.reward.client.zzb;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.common.util.DynamiteApi;
import com.google.android.gms.internal.zzdn;
import com.google.android.gms.internal.zzdr;
import com.google.android.gms.internal.zzei;
import com.google.android.gms.internal.zzgc;
import com.google.android.gms.internal.zzgz;
import com.google.android.gms.internal.zzhy;
import com.google.android.gms.internal.zzih;
import com.google.android.gms.internal.zzji;
import com.google.android.gms.internal.zzka;

@zzji
@Keep
@DynamiteApi
public class ClientApi
  extends zzx.zza
{
  public zzs createAdLoaderBuilder(com.google.android.gms.dynamic.zzd paramzzd, String paramString, zzgz paramzzgz, int paramInt)
  {
    return new zzk((Context)com.google.android.gms.dynamic.zze.zzae(paramzzd), paramString, paramzzgz, new VersionInfoParcel(9877000, paramInt, true), zzd.zzfd());
  }
  
  public zzhy createAdOverlay(com.google.android.gms.dynamic.zzd paramzzd)
  {
    return new com.google.android.gms.ads.internal.overlay.zzd((Activity)com.google.android.gms.dynamic.zze.zzae(paramzzd));
  }
  
  public zzu createBannerAdManager(com.google.android.gms.dynamic.zzd paramzzd, AdSizeParcel paramAdSizeParcel, String paramString, zzgz paramzzgz, int paramInt)
    throws RemoteException
  {
    return new zzf((Context)com.google.android.gms.dynamic.zze.zzae(paramzzd), paramAdSizeParcel, paramString, paramzzgz, new VersionInfoParcel(9877000, paramInt, true), zzd.zzfd());
  }
  
  public zzih createInAppPurchaseManager(com.google.android.gms.dynamic.zzd paramzzd)
  {
    return new com.google.android.gms.ads.internal.purchase.zze((Activity)com.google.android.gms.dynamic.zze.zzae(paramzzd));
  }
  
  public zzu createInterstitialAdManager(com.google.android.gms.dynamic.zzd paramzzd, AdSizeParcel paramAdSizeParcel, String paramString, zzgz paramzzgz, int paramInt)
    throws RemoteException
  {
    paramzzd = (Context)com.google.android.gms.dynamic.zze.zzae(paramzzd);
    zzdr.initialize(paramzzd);
    VersionInfoParcel localVersionInfoParcel = new VersionInfoParcel(9877000, paramInt, true);
    boolean bool = "reward_mb".equals(paramAdSizeParcel.zzazq);
    if (((!bool) && (((Boolean)zzdr.zzbgg.get()).booleanValue())) || ((bool) && (((Boolean)zzdr.zzbgh.get()).booleanValue()))) {}
    for (paramInt = 1; paramInt != 0; paramInt = 0) {
      return new zzgc(paramzzd, paramString, paramzzgz, localVersionInfoParcel, zzd.zzfd());
    }
    return new zzl(paramzzd, paramAdSizeParcel, paramString, paramzzgz, localVersionInfoParcel, zzd.zzfd());
  }
  
  public zzei createNativeAdViewDelegate(com.google.android.gms.dynamic.zzd paramzzd1, com.google.android.gms.dynamic.zzd paramzzd2)
  {
    return new com.google.android.gms.ads.internal.formats.zzl((FrameLayout)com.google.android.gms.dynamic.zze.zzae(paramzzd1), (FrameLayout)com.google.android.gms.dynamic.zze.zzae(paramzzd2));
  }
  
  public zzb createRewardedVideoAd(com.google.android.gms.dynamic.zzd paramzzd, zzgz paramzzgz, int paramInt)
  {
    paramzzd = (Context)com.google.android.gms.dynamic.zze.zzae(paramzzd);
    VersionInfoParcel localVersionInfoParcel = new VersionInfoParcel(9877000, paramInt, true);
    return new zzka(paramzzd, zzd.zzfd(), paramzzgz, localVersionInfoParcel);
  }
  
  public zzu createSearchAdManager(com.google.android.gms.dynamic.zzd paramzzd, AdSizeParcel paramAdSizeParcel, String paramString, int paramInt)
    throws RemoteException
  {
    return new zzt((Context)com.google.android.gms.dynamic.zze.zzae(paramzzd), paramAdSizeParcel, paramString, new VersionInfoParcel(9877000, paramInt, true));
  }
  
  @Nullable
  public zzz getMobileAdsSettingsManager(com.google.android.gms.dynamic.zzd paramzzd)
  {
    return null;
  }
  
  public zzz getMobileAdsSettingsManagerWithClientJarVersion(com.google.android.gms.dynamic.zzd paramzzd, int paramInt)
  {
    return zzo.zza((Context)com.google.android.gms.dynamic.zze.zzae(paramzzd), new VersionInfoParcel(9877000, paramInt, true));
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/ClientApi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */