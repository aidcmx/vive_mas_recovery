package com.google.android.gms.ads.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.internal.zzji;

@zzji
public final class InterstitialAdParameterParcel
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<InterstitialAdParameterParcel> CREATOR = new zzm();
  public final int versionCode;
  public final boolean zzaok;
  public final boolean zzaol;
  public final String zzaom;
  public final boolean zzaon;
  public final float zzaoo;
  public final int zzaop;
  
  InterstitialAdParameterParcel(int paramInt1, boolean paramBoolean1, boolean paramBoolean2, String paramString, boolean paramBoolean3, float paramFloat, int paramInt2)
  {
    this.versionCode = paramInt1;
    this.zzaok = paramBoolean1;
    this.zzaol = paramBoolean2;
    this.zzaom = paramString;
    this.zzaon = paramBoolean3;
    this.zzaoo = paramFloat;
    this.zzaop = paramInt2;
  }
  
  public InterstitialAdParameterParcel(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, float paramFloat, int paramInt)
  {
    this(3, paramBoolean1, paramBoolean2, null, paramBoolean3, paramFloat, paramInt);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzm.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/InterstitialAdParameterParcel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */