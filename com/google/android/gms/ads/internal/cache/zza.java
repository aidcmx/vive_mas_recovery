package com.google.android.gms.ads.internal.cache;

import android.content.Context;
import android.os.Binder;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.zze.zzb;
import com.google.android.gms.common.internal.zze.zzc;
import com.google.android.gms.internal.zzcz;
import com.google.android.gms.internal.zzcz.zzb;
import com.google.android.gms.internal.zzdn;
import com.google.android.gms.internal.zzdr;
import com.google.android.gms.internal.zzji;
import com.google.android.gms.internal.zzkx;
import com.google.android.gms.internal.zzlb;
import com.google.android.gms.internal.zzlj;

@zzji
public class zza
{
  @Nullable
  private Context mContext;
  private final Object zzako = new Object();
  private final Runnable zzaxy = new Runnable()
  {
    public void run()
    {
      zza.zza(zza.this);
    }
  };
  @Nullable
  private zzc zzaxz;
  @Nullable
  private zzf zzaya;
  
  private void connect()
  {
    synchronized (this.zzako)
    {
      if ((this.mContext == null) || (this.zzaxz != null)) {
        return;
      }
      this.zzaxz = zza(new zze.zzb()new zze.zzc
      {
        public void onConnected(@Nullable Bundle arg1)
        {
          synchronized (zza.zzc(zza.this))
          {
            try
            {
              zza.zza(zza.this, zza.zzd(zza.this).zzjz());
              zza.zzc(zza.this).notifyAll();
              return;
            }
            catch (DeadObjectException localDeadObjectException)
            {
              for (;;)
              {
                zzkx.zzb("Unable to obtain a cache service instance.", localDeadObjectException);
                zza.zza(zza.this);
              }
            }
          }
        }
        
        public void onConnectionSuspended(int paramAnonymousInt)
        {
          synchronized (zza.zzc(zza.this))
          {
            zza.zza(zza.this, null);
            zza.zza(zza.this, null);
            zza.zzc(zza.this).notifyAll();
            zzu.zzhc().zzwk();
            return;
          }
        }
      }, new zze.zzc()
      {
        public void onConnectionFailed(@NonNull ConnectionResult arg1)
        {
          synchronized (zza.zzc(zza.this))
          {
            zza.zza(zza.this, null);
            zza.zza(zza.this, null);
            zza.zzc(zza.this).notifyAll();
            zzu.zzhc().zzwk();
            return;
          }
        }
      });
      this.zzaxz.zzavd();
      return;
    }
  }
  
  private void disconnect()
  {
    synchronized (this.zzako)
    {
      if (this.zzaxz == null) {
        return;
      }
      if ((this.zzaxz.isConnected()) || (this.zzaxz.isConnecting())) {
        this.zzaxz.disconnect();
      }
      this.zzaxz = null;
      this.zzaya = null;
      Binder.flushPendingCommands();
      zzu.zzhc().zzwk();
      return;
    }
  }
  
  public void initialize(Context paramContext)
  {
    if (paramContext == null) {
      return;
    }
    synchronized (this.zzako)
    {
      if (this.mContext != null) {
        return;
      }
    }
    this.mContext = paramContext.getApplicationContext();
    if (((Boolean)zzdr.zzbkr.get()).booleanValue()) {
      connect();
    }
    for (;;)
    {
      return;
      if (((Boolean)zzdr.zzbkq.get()).booleanValue()) {
        zza(new zzcz.zzb()
        {
          public void zzk(boolean paramAnonymousBoolean)
          {
            if (paramAnonymousBoolean)
            {
              zza.zzb(zza.this);
              return;
            }
            zza.zza(zza.this);
          }
        });
      }
    }
  }
  
  public CacheEntryParcel zza(CacheOffering paramCacheOffering)
  {
    synchronized (this.zzako)
    {
      if (this.zzaya == null)
      {
        paramCacheOffering = new CacheEntryParcel();
        return paramCacheOffering;
      }
    }
    return paramCacheOffering;
  }
  
  protected zzc zza(zze.zzb paramzzb, zze.zzc paramzzc)
  {
    return new zzc(this.mContext, zzu.zzhc().zzwj(), paramzzb, paramzzc);
  }
  
  protected void zza(zzcz.zzb paramzzb)
  {
    zzu.zzgp().zza(paramzzb);
  }
  
  public void zzjt()
  {
    if (((Boolean)zzdr.zzbks.get()).booleanValue()) {
      synchronized (this.zzako)
      {
        connect();
        zzu.zzgm();
        zzlb.zzcvl.removeCallbacks(this.zzaxy);
        zzu.zzgm();
        zzlb.zzcvl.postDelayed(this.zzaxy, ((Long)zzdr.zzbkt.get()).longValue());
        return;
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/cache/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */