package com.google.android.gms.ads.internal.cache;

import android.content.Context;
import android.os.DeadObjectException;
import android.os.IBinder;
import android.os.Looper;
import com.google.android.gms.common.internal.zze;
import com.google.android.gms.common.internal.zze.zzb;
import com.google.android.gms.common.internal.zze.zzc;
import com.google.android.gms.internal.zzji;

@zzji
public class zzc
  extends zze<zzf>
{
  zzc(Context paramContext, Looper paramLooper, zze.zzb paramzzb, zze.zzc paramzzc)
  {
    super(paramContext, paramLooper, 123, paramzzb, paramzzc, null);
  }
  
  protected zzf zzg(IBinder paramIBinder)
  {
    return zzf.zza.zzi(paramIBinder);
  }
  
  protected String zzjx()
  {
    return "com.google.android.gms.ads.service.CACHE";
  }
  
  protected String zzjy()
  {
    return "com.google.android.gms.ads.internal.cache.ICacheService";
  }
  
  public zzf zzjz()
    throws DeadObjectException
  {
    return (zzf)super.zzavg();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/cache/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */