package com.google.android.gms.ads.internal.cache;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzd
  implements Parcelable.Creator<CacheOffering>
{
  static void zza(CacheOffering paramCacheOffering, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramCacheOffering.version);
    zzb.zza(paramParcel, 2, paramCacheOffering.url, false);
    zzb.zza(paramParcel, 3, paramCacheOffering.zzayd);
    zzb.zza(paramParcel, 4, paramCacheOffering.zzaye, false);
    zzb.zza(paramParcel, 5, paramCacheOffering.zzayf, false);
    zzb.zza(paramParcel, 6, paramCacheOffering.zzayg, false);
    zzb.zza(paramParcel, 7, paramCacheOffering.zzayh, false);
    zzb.zza(paramParcel, 8, paramCacheOffering.zzayi);
    zzb.zzaj(paramParcel, paramInt);
  }
  
  public CacheOffering zzd(Parcel paramParcel)
  {
    boolean bool = false;
    Bundle localBundle = null;
    int j = zza.zzcr(paramParcel);
    long l = 0L;
    String str1 = null;
    String str2 = null;
    String str3 = null;
    String str4 = null;
    int i = 0;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.zzcq(paramParcel);
      switch (zza.zzgu(k))
      {
      default: 
        zza.zzb(paramParcel, k);
        break;
      case 1: 
        i = zza.zzg(paramParcel, k);
        break;
      case 2: 
        str4 = zza.zzq(paramParcel, k);
        break;
      case 3: 
        l = zza.zzi(paramParcel, k);
        break;
      case 4: 
        str3 = zza.zzq(paramParcel, k);
        break;
      case 5: 
        str2 = zza.zzq(paramParcel, k);
        break;
      case 6: 
        str1 = zza.zzq(paramParcel, k);
        break;
      case 7: 
        localBundle = zza.zzs(paramParcel, k);
        break;
      case 8: 
        bool = zza.zzc(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new CacheOffering(i, str4, l, str3, str2, str1, localBundle, bool);
  }
  
  public CacheOffering[] zzu(int paramInt)
  {
    return new CacheOffering[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/cache/zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */