package com.google.android.gms.ads.internal.client;

import android.location.Location;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.internal.zzji;
import java.util.List;

@zzji
public final class AdRequestParcel
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<AdRequestParcel> CREATOR = new zzg();
  public final Bundle extras;
  public final int versionCode;
  public final long zzayl;
  public final int zzaym;
  public final List<String> zzayn;
  public final boolean zzayo;
  public final int zzayp;
  public final boolean zzayq;
  public final String zzayr;
  public final SearchAdRequestParcel zzays;
  public final Location zzayt;
  public final String zzayu;
  public final Bundle zzayv;
  public final Bundle zzayw;
  public final List<String> zzayx;
  public final String zzayy;
  public final String zzayz;
  public final boolean zzaza;
  
  public AdRequestParcel(int paramInt1, long paramLong, Bundle paramBundle1, int paramInt2, List<String> paramList1, boolean paramBoolean1, int paramInt3, boolean paramBoolean2, String paramString1, SearchAdRequestParcel paramSearchAdRequestParcel, Location paramLocation, String paramString2, Bundle paramBundle2, Bundle paramBundle3, List<String> paramList2, String paramString3, String paramString4, boolean paramBoolean3)
  {
    this.versionCode = paramInt1;
    this.zzayl = paramLong;
    Bundle localBundle = paramBundle1;
    if (paramBundle1 == null) {
      localBundle = new Bundle();
    }
    this.extras = localBundle;
    this.zzaym = paramInt2;
    this.zzayn = paramList1;
    this.zzayo = paramBoolean1;
    this.zzayp = paramInt3;
    this.zzayq = paramBoolean2;
    this.zzayr = paramString1;
    this.zzays = paramSearchAdRequestParcel;
    this.zzayt = paramLocation;
    this.zzayu = paramString2;
    paramBundle1 = paramBundle2;
    if (paramBundle2 == null) {
      paramBundle1 = new Bundle();
    }
    this.zzayv = paramBundle1;
    this.zzayw = paramBundle3;
    this.zzayx = paramList2;
    this.zzayy = paramString3;
    this.zzayz = paramString4;
    this.zzaza = paramBoolean3;
  }
  
  public static void zzj(AdRequestParcel paramAdRequestParcel)
  {
    paramAdRequestParcel.zzayv.putBundle("com.google.ads.mediation.admob.AdMobAdapter", paramAdRequestParcel.extras);
  }
  
  public boolean equals(Object paramObject)
  {
    if (!(paramObject instanceof AdRequestParcel)) {}
    do
    {
      return false;
      paramObject = (AdRequestParcel)paramObject;
    } while ((this.versionCode != ((AdRequestParcel)paramObject).versionCode) || (this.zzayl != ((AdRequestParcel)paramObject).zzayl) || (!zzz.equal(this.extras, ((AdRequestParcel)paramObject).extras)) || (this.zzaym != ((AdRequestParcel)paramObject).zzaym) || (!zzz.equal(this.zzayn, ((AdRequestParcel)paramObject).zzayn)) || (this.zzayo != ((AdRequestParcel)paramObject).zzayo) || (this.zzayp != ((AdRequestParcel)paramObject).zzayp) || (this.zzayq != ((AdRequestParcel)paramObject).zzayq) || (!zzz.equal(this.zzayr, ((AdRequestParcel)paramObject).zzayr)) || (!zzz.equal(this.zzays, ((AdRequestParcel)paramObject).zzays)) || (!zzz.equal(this.zzayt, ((AdRequestParcel)paramObject).zzayt)) || (!zzz.equal(this.zzayu, ((AdRequestParcel)paramObject).zzayu)) || (!zzz.equal(this.zzayv, ((AdRequestParcel)paramObject).zzayv)) || (!zzz.equal(this.zzayw, ((AdRequestParcel)paramObject).zzayw)) || (!zzz.equal(this.zzayx, ((AdRequestParcel)paramObject).zzayx)) || (!zzz.equal(this.zzayy, ((AdRequestParcel)paramObject).zzayy)) || (!zzz.equal(this.zzayz, ((AdRequestParcel)paramObject).zzayz)) || (this.zzaza != ((AdRequestParcel)paramObject).zzaza));
    return true;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { Integer.valueOf(this.versionCode), Long.valueOf(this.zzayl), this.extras, Integer.valueOf(this.zzaym), this.zzayn, Boolean.valueOf(this.zzayo), Integer.valueOf(this.zzayp), Boolean.valueOf(this.zzayq), this.zzayr, this.zzays, this.zzayt, this.zzayu, this.zzayv, this.zzayw, this.zzayx, this.zzayy, this.zzayz, Boolean.valueOf(this.zzaza) });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzg.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/client/AdRequestParcel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */