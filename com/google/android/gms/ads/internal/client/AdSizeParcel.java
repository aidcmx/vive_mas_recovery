package com.google.android.gms.ads.internal.client;

import android.content.Context;
import android.content.res.Resources;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.util.DisplayMetrics;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.internal.zzji;

@zzji
public class AdSizeParcel
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<AdSizeParcel> CREATOR = new zzi();
  public final int height;
  public final int heightPixels;
  public final int versionCode;
  public final int width;
  public final int widthPixels;
  public final String zzazq;
  public final boolean zzazr;
  public final AdSizeParcel[] zzazs;
  public final boolean zzazt;
  public final boolean zzazu;
  public boolean zzazv;
  
  public AdSizeParcel()
  {
    this(5, "interstitial_mb", 0, 0, true, 0, 0, null, false, false, false);
  }
  
  AdSizeParcel(int paramInt1, String paramString, int paramInt2, int paramInt3, boolean paramBoolean1, int paramInt4, int paramInt5, AdSizeParcel[] paramArrayOfAdSizeParcel, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4)
  {
    this.versionCode = paramInt1;
    this.zzazq = paramString;
    this.height = paramInt2;
    this.heightPixels = paramInt3;
    this.zzazr = paramBoolean1;
    this.width = paramInt4;
    this.widthPixels = paramInt5;
    this.zzazs = paramArrayOfAdSizeParcel;
    this.zzazt = paramBoolean2;
    this.zzazu = paramBoolean3;
    this.zzazv = paramBoolean4;
  }
  
  public AdSizeParcel(Context paramContext, AdSize paramAdSize)
  {
    this(paramContext, new AdSize[] { paramAdSize });
  }
  
  public AdSizeParcel(Context paramContext, AdSize[] paramArrayOfAdSize)
  {
    AdSize localAdSize = paramArrayOfAdSize[0];
    this.versionCode = 5;
    this.zzazr = false;
    this.zzazu = localAdSize.isFluid();
    int j;
    label66:
    int k;
    label78:
    DisplayMetrics localDisplayMetrics;
    label129:
    int m;
    int i;
    if (this.zzazu)
    {
      this.width = AdSize.BANNER.getWidth();
      this.height = AdSize.BANNER.getHeight();
      if (this.width != -1) {
        break label314;
      }
      j = 1;
      if (this.height != -2) {
        break label320;
      }
      k = 1;
      localDisplayMetrics = paramContext.getResources().getDisplayMetrics();
      if (j == 0) {
        break label338;
      }
      if ((!zzm.zzkr().zzaq(paramContext)) || (!zzm.zzkr().zzar(paramContext))) {
        break label326;
      }
      this.widthPixels = (zza(localDisplayMetrics) - zzm.zzkr().zzas(paramContext));
      double d = this.widthPixels / localDisplayMetrics.density;
      m = (int)d;
      i = m;
      if (d - (int)d >= 0.01D) {
        i = m + 1;
      }
      label168:
      if (k == 0) {
        break label363;
      }
      m = zzc(localDisplayMetrics);
      label180:
      this.heightPixels = zzm.zzkr().zza(localDisplayMetrics, m);
      if ((j == 0) && (k == 0)) {
        break label372;
      }
      this.zzazq = (26 + i + "x" + m + "_as");
    }
    for (;;)
    {
      if (paramArrayOfAdSize.length <= 1) {
        break label400;
      }
      this.zzazs = new AdSizeParcel[paramArrayOfAdSize.length];
      i = 0;
      while (i < paramArrayOfAdSize.length)
      {
        this.zzazs[i] = new AdSizeParcel(paramContext, paramArrayOfAdSize[i]);
        i += 1;
      }
      this.width = localAdSize.getWidth();
      this.height = localAdSize.getHeight();
      break;
      label314:
      j = 0;
      break label66;
      label320:
      k = 0;
      break label78;
      label326:
      this.widthPixels = zza(localDisplayMetrics);
      break label129;
      label338:
      i = this.width;
      this.widthPixels = zzm.zzkr().zza(localDisplayMetrics, this.width);
      break label168;
      label363:
      m = this.height;
      break label180;
      label372:
      if (this.zzazu) {
        this.zzazq = "320x50_mb";
      } else {
        this.zzazq = localAdSize.toString();
      }
    }
    label400:
    this.zzazs = null;
    this.zzazt = false;
    this.zzazv = false;
  }
  
  public AdSizeParcel(AdSizeParcel paramAdSizeParcel, AdSizeParcel[] paramArrayOfAdSizeParcel)
  {
    this(5, paramAdSizeParcel.zzazq, paramAdSizeParcel.height, paramAdSizeParcel.heightPixels, paramAdSizeParcel.zzazr, paramAdSizeParcel.width, paramAdSizeParcel.widthPixels, paramArrayOfAdSizeParcel, paramAdSizeParcel.zzazt, paramAdSizeParcel.zzazu, paramAdSizeParcel.zzazv);
  }
  
  public static int zza(DisplayMetrics paramDisplayMetrics)
  {
    return paramDisplayMetrics.widthPixels;
  }
  
  public static int zzb(DisplayMetrics paramDisplayMetrics)
  {
    return (int)(zzc(paramDisplayMetrics) * paramDisplayMetrics.density);
  }
  
  private static int zzc(DisplayMetrics paramDisplayMetrics)
  {
    int i = (int)(paramDisplayMetrics.heightPixels / paramDisplayMetrics.density);
    if (i <= 400) {
      return 32;
    }
    if (i <= 720) {
      return 50;
    }
    return 90;
  }
  
  public static AdSizeParcel zzj(Context paramContext)
  {
    return new AdSizeParcel(5, "320x50_mb", 0, 0, false, 0, 0, null, true, false, false);
  }
  
  public static AdSizeParcel zzkc()
  {
    return new AdSizeParcel(5, "reward_mb", 0, 0, true, 0, 0, null, false, false, false);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzi.zza(this, paramParcel, paramInt);
  }
  
  public AdSize zzkd()
  {
    return com.google.android.gms.ads.zza.zza(this.width, this.height, this.zzazq);
  }
  
  public void zzl(boolean paramBoolean)
  {
    this.zzazv = paramBoolean;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/client/AdSizeParcel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */