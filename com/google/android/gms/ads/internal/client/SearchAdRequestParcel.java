package com.google.android.gms.ads.internal.client;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.ads.search.SearchAdRequest;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.internal.zzji;

@zzji
public final class SearchAdRequestParcel
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<SearchAdRequestParcel> CREATOR = new zzao();
  public final int backgroundColor;
  public final int versionCode;
  public final int zzbbx;
  public final int zzbby;
  public final int zzbbz;
  public final int zzbca;
  public final int zzbcb;
  public final int zzbcc;
  public final int zzbcd;
  public final String zzbce;
  public final int zzbcf;
  public final String zzbcg;
  public final int zzbch;
  public final int zzbci;
  public final String zzbcj;
  
  SearchAdRequestParcel(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9, String paramString1, int paramInt10, String paramString2, int paramInt11, int paramInt12, String paramString3)
  {
    this.versionCode = paramInt1;
    this.zzbbx = paramInt2;
    this.backgroundColor = paramInt3;
    this.zzbby = paramInt4;
    this.zzbbz = paramInt5;
    this.zzbca = paramInt6;
    this.zzbcb = paramInt7;
    this.zzbcc = paramInt8;
    this.zzbcd = paramInt9;
    this.zzbce = paramString1;
    this.zzbcf = paramInt10;
    this.zzbcg = paramString2;
    this.zzbch = paramInt11;
    this.zzbci = paramInt12;
    this.zzbcj = paramString3;
  }
  
  public SearchAdRequestParcel(SearchAdRequest paramSearchAdRequest)
  {
    this.versionCode = 1;
    this.zzbbx = paramSearchAdRequest.getAnchorTextColor();
    this.backgroundColor = paramSearchAdRequest.getBackgroundColor();
    this.zzbby = paramSearchAdRequest.getBackgroundGradientBottom();
    this.zzbbz = paramSearchAdRequest.getBackgroundGradientTop();
    this.zzbca = paramSearchAdRequest.getBorderColor();
    this.zzbcb = paramSearchAdRequest.getBorderThickness();
    this.zzbcc = paramSearchAdRequest.getBorderType();
    this.zzbcd = paramSearchAdRequest.getCallButtonColor();
    this.zzbce = paramSearchAdRequest.getCustomChannels();
    this.zzbcf = paramSearchAdRequest.getDescriptionTextColor();
    this.zzbcg = paramSearchAdRequest.getFontFace();
    this.zzbch = paramSearchAdRequest.getHeaderTextColor();
    this.zzbci = paramSearchAdRequest.getHeaderTextSize();
    this.zzbcj = paramSearchAdRequest.getQuery();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzao.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/client/SearchAdRequestParcel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */