package com.google.android.gms.ads.internal.client;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.ads.VideoOptions;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.internal.zzji;

@zzji
public class VideoOptionsParcel
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<VideoOptionsParcel> CREATOR = new zzaq();
  public final int versionCode;
  public final boolean zzbck;
  
  public VideoOptionsParcel(int paramInt, boolean paramBoolean)
  {
    this.versionCode = paramInt;
    this.zzbck = paramBoolean;
  }
  
  public VideoOptionsParcel(VideoOptions paramVideoOptions)
  {
    this(1, paramVideoOptions.getStartMuted());
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzaq.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/client/VideoOptionsParcel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */