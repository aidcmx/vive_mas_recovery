package com.google.android.gms.ads.internal.client;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.ads.mediation.MediationAdapter;
import com.google.android.gms.ads.mediation.NetworkExtras;
import com.google.android.gms.ads.mediation.admob.AdMobExtras;
import com.google.android.gms.ads.mediation.customevent.CustomEvent;
import com.google.android.gms.ads.search.SearchAdRequest;
import com.google.android.gms.internal.zzji;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@zzji
public final class zzad
{
  public static final String DEVICE_ID_EMULATOR = zzm.zzkr().zzdf("emulator");
  private final boolean zzamv;
  private final int zzazc;
  private final int zzazf;
  private final String zzazg;
  private final String zzazi;
  private final Bundle zzazk;
  private final String zzazm;
  private final boolean zzazo;
  private final Bundle zzbar;
  private final Map<Class<? extends NetworkExtras>, NetworkExtras> zzbas;
  private final SearchAdRequest zzbat;
  private final Set<String> zzbau;
  private final Set<String> zzbav;
  private final Date zzgr;
  private final Set<String> zzgt;
  private final Location zzgv;
  
  public zzad(zza paramzza)
  {
    this(paramzza, null);
  }
  
  public zzad(zza paramzza, SearchAdRequest paramSearchAdRequest)
  {
    this.zzgr = zza.zza(paramzza);
    this.zzazi = zza.zzb(paramzza);
    this.zzazc = zza.zzc(paramzza);
    this.zzgt = Collections.unmodifiableSet(zza.zzd(paramzza));
    this.zzgv = zza.zze(paramzza);
    this.zzamv = zza.zzf(paramzza);
    this.zzbar = zza.zzg(paramzza);
    this.zzbas = Collections.unmodifiableMap(zza.zzh(paramzza));
    this.zzazg = zza.zzi(paramzza);
    this.zzazm = zza.zzj(paramzza);
    this.zzbat = paramSearchAdRequest;
    this.zzazf = zza.zzk(paramzza);
    this.zzbau = Collections.unmodifiableSet(zza.zzl(paramzza));
    this.zzazk = zza.zzm(paramzza);
    this.zzbav = Collections.unmodifiableSet(zza.zzn(paramzza));
    this.zzazo = zza.zzo(paramzza);
  }
  
  public Date getBirthday()
  {
    return this.zzgr;
  }
  
  public String getContentUrl()
  {
    return this.zzazi;
  }
  
  public Bundle getCustomEventExtrasBundle(Class<? extends CustomEvent> paramClass)
  {
    Bundle localBundle = this.zzbar.getBundle("com.google.android.gms.ads.mediation.customevent.CustomEventAdapter");
    if (localBundle != null) {
      return localBundle.getBundle(paramClass.getName());
    }
    return null;
  }
  
  public Bundle getCustomTargeting()
  {
    return this.zzazk;
  }
  
  public int getGender()
  {
    return this.zzazc;
  }
  
  public Set<String> getKeywords()
  {
    return this.zzgt;
  }
  
  public Location getLocation()
  {
    return this.zzgv;
  }
  
  public boolean getManualImpressionsEnabled()
  {
    return this.zzamv;
  }
  
  @Deprecated
  public <T extends NetworkExtras> T getNetworkExtras(Class<T> paramClass)
  {
    return (NetworkExtras)this.zzbas.get(paramClass);
  }
  
  public Bundle getNetworkExtrasBundle(Class<? extends MediationAdapter> paramClass)
  {
    return this.zzbar.getBundle(paramClass.getName());
  }
  
  public String getPublisherProvidedId()
  {
    return this.zzazg;
  }
  
  public boolean isDesignedForFamilies()
  {
    return this.zzazo;
  }
  
  public boolean isTestDevice(Context paramContext)
  {
    return this.zzbau.contains(zzm.zzkr().zzao(paramContext));
  }
  
  public String zzkz()
  {
    return this.zzazm;
  }
  
  public SearchAdRequest zzla()
  {
    return this.zzbat;
  }
  
  public Map<Class<? extends NetworkExtras>, NetworkExtras> zzlb()
  {
    return this.zzbas;
  }
  
  public Bundle zzlc()
  {
    return this.zzbar;
  }
  
  public int zzld()
  {
    return this.zzazf;
  }
  
  public Set<String> zzle()
  {
    return this.zzbav;
  }
  
  public static final class zza
  {
    private boolean zzamv = false;
    private int zzazc = -1;
    private int zzazf = -1;
    private String zzazg;
    private String zzazi;
    private final Bundle zzazk = new Bundle();
    private String zzazm;
    private boolean zzazo;
    private final Bundle zzbar = new Bundle();
    private final HashSet<String> zzbaw = new HashSet();
    private final HashMap<Class<? extends NetworkExtras>, NetworkExtras> zzbax = new HashMap();
    private final HashSet<String> zzbay = new HashSet();
    private final HashSet<String> zzbaz = new HashSet();
    private Date zzgr;
    private Location zzgv;
    
    public void setManualImpressionsEnabled(boolean paramBoolean)
    {
      this.zzamv = paramBoolean;
    }
    
    @Deprecated
    public void zza(NetworkExtras paramNetworkExtras)
    {
      if ((paramNetworkExtras instanceof AdMobExtras))
      {
        zza(AdMobAdapter.class, ((AdMobExtras)paramNetworkExtras).getExtras());
        return;
      }
      this.zzbax.put(paramNetworkExtras.getClass(), paramNetworkExtras);
    }
    
    public void zza(Class<? extends MediationAdapter> paramClass, Bundle paramBundle)
    {
      this.zzbar.putBundle(paramClass.getName(), paramBundle);
    }
    
    public void zza(Date paramDate)
    {
      this.zzgr = paramDate;
    }
    
    public void zzam(String paramString)
    {
      this.zzbaw.add(paramString);
    }
    
    public void zzan(String paramString)
    {
      this.zzbay.add(paramString);
    }
    
    public void zzao(String paramString)
    {
      this.zzbay.remove(paramString);
    }
    
    public void zzap(String paramString)
    {
      this.zzazi = paramString;
    }
    
    public void zzaq(String paramString)
    {
      this.zzazg = paramString;
    }
    
    public void zzar(String paramString)
    {
      this.zzazm = paramString;
    }
    
    public void zzas(String paramString)
    {
      this.zzbaz.add(paramString);
    }
    
    public void zzb(Location paramLocation)
    {
      this.zzgv = paramLocation;
    }
    
    public void zzb(Class<? extends CustomEvent> paramClass, Bundle paramBundle)
    {
      if (this.zzbar.getBundle("com.google.android.gms.ads.mediation.customevent.CustomEventAdapter") == null) {
        this.zzbar.putBundle("com.google.android.gms.ads.mediation.customevent.CustomEventAdapter", new Bundle());
      }
      this.zzbar.getBundle("com.google.android.gms.ads.mediation.customevent.CustomEventAdapter").putBundle(paramClass.getName(), paramBundle);
    }
    
    public void zze(String paramString1, String paramString2)
    {
      this.zzazk.putString(paramString1, paramString2);
    }
    
    public void zzo(boolean paramBoolean)
    {
      if (paramBoolean) {}
      for (int i = 1;; i = 0)
      {
        this.zzazf = i;
        return;
      }
    }
    
    public void zzp(boolean paramBoolean)
    {
      this.zzazo = paramBoolean;
    }
    
    public void zzx(int paramInt)
    {
      this.zzazc = paramInt;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/client/zzad.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */