package com.google.android.gms.ads.internal.client;

import android.content.Context;
import android.os.RemoteException;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.Correlator;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.VideoOptions;
import com.google.android.gms.ads.doubleclick.AppEventListener;
import com.google.android.gms.ads.doubleclick.OnCustomRenderedAdLoadedListener;
import com.google.android.gms.ads.purchase.InAppPurchaseListener;
import com.google.android.gms.ads.purchase.PlayStorePurchaseListener;
import com.google.android.gms.dynamic.zzd;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.internal.zzee;
import com.google.android.gms.internal.zzgy;
import com.google.android.gms.internal.zzil;
import com.google.android.gms.internal.zzip;
import com.google.android.gms.internal.zzji;
import java.util.concurrent.atomic.AtomicBoolean;

@zzji
public class zzae
{
  private final zzh zzakc;
  private VideoOptions zzalc;
  private boolean zzamv;
  private String zzant;
  private zza zzayj;
  private AdListener zzayk;
  private AppEventListener zzazw;
  private AdSize[] zzazx;
  private final zzgy zzbba = new zzgy();
  private final AtomicBoolean zzbbb;
  private final VideoController zzbbc = new VideoController();
  final zzo zzbbd = new zzo()
  {
    public void onAdFailedToLoad(int paramAnonymousInt)
    {
      zzae.zza(zzae.this).zza(zzae.this.zzdw());
      super.onAdFailedToLoad(paramAnonymousInt);
    }
    
    public void onAdLoaded()
    {
      zzae.zza(zzae.this).zza(zzae.this.zzdw());
      super.onAdLoaded();
    }
  };
  private Correlator zzbbe;
  private zzu zzbbf;
  private InAppPurchaseListener zzbbg;
  private OnCustomRenderedAdLoadedListener zzbbh;
  private PlayStorePurchaseListener zzbbi;
  private String zzbbj;
  private ViewGroup zzbbk;
  private int zzbbl;
  
  public zzae(ViewGroup paramViewGroup)
  {
    this(paramViewGroup, null, false, zzh.zzkb(), 0);
  }
  
  public zzae(ViewGroup paramViewGroup, int paramInt)
  {
    this(paramViewGroup, null, false, zzh.zzkb(), paramInt);
  }
  
  public zzae(ViewGroup paramViewGroup, AttributeSet paramAttributeSet, boolean paramBoolean)
  {
    this(paramViewGroup, paramAttributeSet, paramBoolean, zzh.zzkb(), 0);
  }
  
  public zzae(ViewGroup paramViewGroup, AttributeSet paramAttributeSet, boolean paramBoolean, int paramInt)
  {
    this(paramViewGroup, paramAttributeSet, paramBoolean, zzh.zzkb(), paramInt);
  }
  
  zzae(ViewGroup paramViewGroup, AttributeSet paramAttributeSet, boolean paramBoolean, zzh paramzzh, int paramInt)
  {
    this(paramViewGroup, paramAttributeSet, paramBoolean, paramzzh, null, paramInt);
  }
  
  zzae(ViewGroup paramViewGroup, AttributeSet paramAttributeSet, boolean paramBoolean, zzh paramzzh, zzu paramzzu, int paramInt)
  {
    this.zzbbk = paramViewGroup;
    this.zzakc = paramzzh;
    this.zzbbf = paramzzu;
    this.zzbbb = new AtomicBoolean(false);
    this.zzbbl = paramInt;
    if (paramAttributeSet != null) {
      paramzzh = paramViewGroup.getContext();
    }
    try
    {
      paramAttributeSet = new zzk(paramzzh, paramAttributeSet);
      this.zzazx = paramAttributeSet.zzm(paramBoolean);
      this.zzant = paramAttributeSet.getAdUnitId();
      if (paramViewGroup.isInEditMode()) {
        zzm.zzkr().zza(paramViewGroup, zza(paramzzh, this.zzazx[0], this.zzbbl), "Ads by Google");
      }
      return;
    }
    catch (IllegalArgumentException paramAttributeSet)
    {
      zzm.zzkr().zza(paramViewGroup, new AdSizeParcel(paramzzh, AdSize.BANNER), paramAttributeSet.getMessage(), paramAttributeSet.getMessage());
    }
  }
  
  private static AdSizeParcel zza(Context paramContext, AdSize paramAdSize, int paramInt)
  {
    paramContext = new AdSizeParcel(paramContext, paramAdSize);
    paramContext.zzl(zzy(paramInt));
    return paramContext;
  }
  
  private static AdSizeParcel zza(Context paramContext, AdSize[] paramArrayOfAdSize, int paramInt)
  {
    paramContext = new AdSizeParcel(paramContext, paramArrayOfAdSize);
    paramContext.zzl(zzy(paramInt));
    return paramContext;
  }
  
  private void zzlf()
  {
    try
    {
      zzd localzzd = this.zzbbf.zzef();
      if (localzzd == null) {
        return;
      }
      this.zzbbk.addView((View)zze.zzae(localzzd));
      return;
    }
    catch (RemoteException localRemoteException)
    {
      com.google.android.gms.ads.internal.util.client.zzb.zzc("Failed to get an ad frame.", localRemoteException);
    }
  }
  
  private static boolean zzy(int paramInt)
  {
    return paramInt == 1;
  }
  
  public void destroy()
  {
    try
    {
      if (this.zzbbf != null) {
        this.zzbbf.destroy();
      }
      return;
    }
    catch (RemoteException localRemoteException)
    {
      com.google.android.gms.ads.internal.util.client.zzb.zzc("Failed to destroy AdView.", localRemoteException);
    }
  }
  
  public AdListener getAdListener()
  {
    return this.zzayk;
  }
  
  public AdSize getAdSize()
  {
    try
    {
      if (this.zzbbf != null)
      {
        Object localObject = this.zzbbf.zzeg();
        if (localObject != null)
        {
          localObject = ((AdSizeParcel)localObject).zzkd();
          return (AdSize)localObject;
        }
      }
    }
    catch (RemoteException localRemoteException)
    {
      com.google.android.gms.ads.internal.util.client.zzb.zzc("Failed to get the current AdSize.", localRemoteException);
      if (this.zzazx != null) {
        return this.zzazx[0];
      }
    }
    return null;
  }
  
  public AdSize[] getAdSizes()
  {
    return this.zzazx;
  }
  
  public String getAdUnitId()
  {
    return this.zzant;
  }
  
  public AppEventListener getAppEventListener()
  {
    return this.zzazw;
  }
  
  public InAppPurchaseListener getInAppPurchaseListener()
  {
    return this.zzbbg;
  }
  
  public String getMediationAdapterClassName()
  {
    try
    {
      if (this.zzbbf != null)
      {
        String str = this.zzbbf.getMediationAdapterClassName();
        return str;
      }
    }
    catch (RemoteException localRemoteException)
    {
      com.google.android.gms.ads.internal.util.client.zzb.zzc("Failed to get the mediation adapter class name.", localRemoteException);
    }
    return null;
  }
  
  public OnCustomRenderedAdLoadedListener getOnCustomRenderedAdLoadedListener()
  {
    return this.zzbbh;
  }
  
  public VideoController getVideoController()
  {
    return this.zzbbc;
  }
  
  public VideoOptions getVideoOptions()
  {
    return this.zzalc;
  }
  
  public boolean isLoading()
  {
    try
    {
      if (this.zzbbf != null)
      {
        boolean bool = this.zzbbf.isLoading();
        return bool;
      }
    }
    catch (RemoteException localRemoteException)
    {
      com.google.android.gms.ads.internal.util.client.zzb.zzc("Failed to check if ad is loading.", localRemoteException);
    }
    return false;
  }
  
  public void pause()
  {
    try
    {
      if (this.zzbbf != null) {
        this.zzbbf.pause();
      }
      return;
    }
    catch (RemoteException localRemoteException)
    {
      com.google.android.gms.ads.internal.util.client.zzb.zzc("Failed to call pause.", localRemoteException);
    }
  }
  
  public void recordManualImpression()
  {
    if (this.zzbbb.getAndSet(true)) {}
    for (;;)
    {
      return;
      try
      {
        if (this.zzbbf != null)
        {
          this.zzbbf.zzei();
          return;
        }
      }
      catch (RemoteException localRemoteException)
      {
        com.google.android.gms.ads.internal.util.client.zzb.zzc("Failed to record impression.", localRemoteException);
      }
    }
  }
  
  public void resume()
  {
    try
    {
      if (this.zzbbf != null) {
        this.zzbbf.resume();
      }
      return;
    }
    catch (RemoteException localRemoteException)
    {
      com.google.android.gms.ads.internal.util.client.zzb.zzc("Failed to call resume.", localRemoteException);
    }
  }
  
  public void setAdListener(AdListener paramAdListener)
  {
    this.zzayk = paramAdListener;
    this.zzbbd.zza(paramAdListener);
  }
  
  public void setAdSizes(AdSize... paramVarArgs)
  {
    if (this.zzazx != null) {
      throw new IllegalStateException("The ad size can only be set once on AdView.");
    }
    zza(paramVarArgs);
  }
  
  public void setAdUnitId(String paramString)
  {
    if (this.zzant != null) {
      throw new IllegalStateException("The ad unit ID can only be set once on AdView.");
    }
    this.zzant = paramString;
  }
  
  public void setAppEventListener(AppEventListener paramAppEventListener)
  {
    try
    {
      this.zzazw = paramAppEventListener;
      zzu localzzu;
      if (this.zzbbf != null)
      {
        localzzu = this.zzbbf;
        if (paramAppEventListener == null) {
          break label38;
        }
      }
      label38:
      for (paramAppEventListener = new zzj(paramAppEventListener);; paramAppEventListener = null)
      {
        localzzu.zza(paramAppEventListener);
        return;
      }
      return;
    }
    catch (RemoteException paramAppEventListener)
    {
      com.google.android.gms.ads.internal.util.client.zzb.zzc("Failed to set the AppEventListener.", paramAppEventListener);
    }
  }
  
  public void setCorrelator(Correlator paramCorrelator)
  {
    this.zzbbe = paramCorrelator;
    try
    {
      if (this.zzbbf != null)
      {
        zzu localzzu = this.zzbbf;
        if (this.zzbbe == null) {}
        for (paramCorrelator = null;; paramCorrelator = this.zzbbe.zzdu())
        {
          localzzu.zza(paramCorrelator);
          return;
        }
      }
      return;
    }
    catch (RemoteException paramCorrelator)
    {
      com.google.android.gms.ads.internal.util.client.zzb.zzc("Failed to set correlator.", paramCorrelator);
    }
  }
  
  public void setInAppPurchaseListener(InAppPurchaseListener paramInAppPurchaseListener)
  {
    if (this.zzbbi != null) {
      throw new IllegalStateException("Play store purchase parameter has already been set.");
    }
    try
    {
      this.zzbbg = paramInAppPurchaseListener;
      zzu localzzu;
      if (this.zzbbf != null)
      {
        localzzu = this.zzbbf;
        if (paramInAppPurchaseListener == null) {
          break label56;
        }
      }
      label56:
      for (paramInAppPurchaseListener = new zzil(paramInAppPurchaseListener);; paramInAppPurchaseListener = null)
      {
        localzzu.zza(paramInAppPurchaseListener);
        return;
      }
      return;
    }
    catch (RemoteException paramInAppPurchaseListener)
    {
      com.google.android.gms.ads.internal.util.client.zzb.zzc("Failed to set the InAppPurchaseListener.", paramInAppPurchaseListener);
    }
  }
  
  public void setManualImpressionsEnabled(boolean paramBoolean)
  {
    this.zzamv = paramBoolean;
    try
    {
      if (this.zzbbf != null) {
        this.zzbbf.setManualImpressionsEnabled(this.zzamv);
      }
      return;
    }
    catch (RemoteException localRemoteException)
    {
      com.google.android.gms.ads.internal.util.client.zzb.zzc("Failed to set manual impressions.", localRemoteException);
    }
  }
  
  public void setOnCustomRenderedAdLoadedListener(OnCustomRenderedAdLoadedListener paramOnCustomRenderedAdLoadedListener)
  {
    this.zzbbh = paramOnCustomRenderedAdLoadedListener;
    try
    {
      zzu localzzu;
      if (this.zzbbf != null)
      {
        localzzu = this.zzbbf;
        if (paramOnCustomRenderedAdLoadedListener == null) {
          break label38;
        }
      }
      label38:
      for (paramOnCustomRenderedAdLoadedListener = new zzee(paramOnCustomRenderedAdLoadedListener);; paramOnCustomRenderedAdLoadedListener = null)
      {
        localzzu.zza(paramOnCustomRenderedAdLoadedListener);
        return;
      }
      return;
    }
    catch (RemoteException paramOnCustomRenderedAdLoadedListener)
    {
      com.google.android.gms.ads.internal.util.client.zzb.zzc("Failed to set the onCustomRenderedAdLoadedListener.", paramOnCustomRenderedAdLoadedListener);
    }
  }
  
  public void setPlayStorePurchaseParams(PlayStorePurchaseListener paramPlayStorePurchaseListener, String paramString)
  {
    if (this.zzbbg != null) {
      throw new IllegalStateException("InAppPurchaseListener has already been set.");
    }
    try
    {
      this.zzbbi = paramPlayStorePurchaseListener;
      this.zzbbj = paramString;
      zzu localzzu;
      if (this.zzbbf != null)
      {
        localzzu = this.zzbbf;
        if (paramPlayStorePurchaseListener == null) {
          break label62;
        }
      }
      label62:
      for (paramPlayStorePurchaseListener = new zzip(paramPlayStorePurchaseListener);; paramPlayStorePurchaseListener = null)
      {
        localzzu.zza(paramPlayStorePurchaseListener, paramString);
        return;
      }
      return;
    }
    catch (RemoteException paramPlayStorePurchaseListener)
    {
      com.google.android.gms.ads.internal.util.client.zzb.zzc("Failed to set the play store purchase parameter.", paramPlayStorePurchaseListener);
    }
  }
  
  public void setVideoOptions(VideoOptions paramVideoOptions)
  {
    this.zzalc = paramVideoOptions;
    try
    {
      if (this.zzbbf != null)
      {
        zzu localzzu = this.zzbbf;
        if (paramVideoOptions == null) {}
        for (paramVideoOptions = null;; paramVideoOptions = new VideoOptionsParcel(paramVideoOptions))
        {
          localzzu.zza(paramVideoOptions);
          return;
        }
      }
      return;
    }
    catch (RemoteException paramVideoOptions)
    {
      com.google.android.gms.ads.internal.util.client.zzb.zzc("Failed to set video options.", paramVideoOptions);
    }
  }
  
  public void zza(zza paramzza)
  {
    try
    {
      this.zzayj = paramzza;
      zzu localzzu;
      if (this.zzbbf != null)
      {
        localzzu = this.zzbbf;
        if (paramzza == null) {
          break label38;
        }
      }
      label38:
      for (paramzza = new zzb(paramzza);; paramzza = null)
      {
        localzzu.zza(paramzza);
        return;
      }
      return;
    }
    catch (RemoteException paramzza)
    {
      com.google.android.gms.ads.internal.util.client.zzb.zzc("Failed to set the AdClickListener.", paramzza);
    }
  }
  
  public void zza(zzad paramzzad)
  {
    try
    {
      if (this.zzbbf == null) {
        zzlg();
      }
      if (this.zzbbf.zzb(this.zzakc.zza(this.zzbbk.getContext(), paramzzad))) {
        this.zzbba.zzi(paramzzad.zzlb());
      }
      return;
    }
    catch (RemoteException paramzzad)
    {
      com.google.android.gms.ads.internal.util.client.zzb.zzc("Failed to load ad.", paramzzad);
    }
  }
  
  public void zza(AdSize... paramVarArgs)
  {
    this.zzazx = paramVarArgs;
    try
    {
      if (this.zzbbf != null) {
        this.zzbbf.zza(zza(this.zzbbk.getContext(), this.zzazx, this.zzbbl));
      }
      this.zzbbk.requestLayout();
      return;
    }
    catch (RemoteException paramVarArgs)
    {
      for (;;)
      {
        com.google.android.gms.ads.internal.util.client.zzb.zzc("Failed to set the ad size.", paramVarArgs);
      }
    }
  }
  
  public boolean zzb(AdSizeParcel paramAdSizeParcel)
  {
    return "search_v2".equals(paramAdSizeParcel.zzazq);
  }
  
  public zzab zzdw()
  {
    if (this.zzbbf == null) {
      return null;
    }
    try
    {
      zzab localzzab = this.zzbbf.zzej();
      return localzzab;
    }
    catch (RemoteException localRemoteException)
    {
      com.google.android.gms.ads.internal.util.client.zzb.zzc("Failed to retrieve VideoController.", localRemoteException);
    }
    return null;
  }
  
  void zzlg()
    throws RemoteException
  {
    if (((this.zzazx == null) || (this.zzant == null)) && (this.zzbbf == null)) {
      throw new IllegalStateException("The ad size and ad unit ID must be set before loadAd is called.");
    }
    this.zzbbf = zzlh();
    this.zzbbf.zza(new zzc(this.zzbbd));
    if (this.zzayj != null) {
      this.zzbbf.zza(new zzb(this.zzayj));
    }
    if (this.zzazw != null) {
      this.zzbbf.zza(new zzj(this.zzazw));
    }
    if (this.zzbbg != null) {
      this.zzbbf.zza(new zzil(this.zzbbg));
    }
    if (this.zzbbi != null) {
      this.zzbbf.zza(new zzip(this.zzbbi), this.zzbbj);
    }
    if (this.zzbbh != null) {
      this.zzbbf.zza(new zzee(this.zzbbh));
    }
    if (this.zzbbe != null) {
      this.zzbbf.zza(this.zzbbe.zzdu());
    }
    if (this.zzalc != null) {
      this.zzbbf.zza(new VideoOptionsParcel(this.zzalc));
    }
    this.zzbbf.setManualImpressionsEnabled(this.zzamv);
    zzlf();
  }
  
  protected zzu zzlh()
    throws RemoteException
  {
    Context localContext = this.zzbbk.getContext();
    AdSizeParcel localAdSizeParcel = zza(localContext, this.zzazx, this.zzbbl);
    if (zzb(localAdSizeParcel)) {
      return zzm.zzks().zza(localContext, localAdSizeParcel, this.zzant);
    }
    return zzm.zzks().zza(localContext, localAdSizeParcel, this.zzant, this.zzbba);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/client/zzae.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */