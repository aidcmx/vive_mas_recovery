package com.google.android.gms.ads.internal.client;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.Correlator;
import com.google.android.gms.ads.doubleclick.AppEventListener;
import com.google.android.gms.ads.doubleclick.OnCustomRenderedAdLoadedListener;
import com.google.android.gms.ads.doubleclick.PublisherInterstitialAd;
import com.google.android.gms.ads.internal.reward.client.zzg;
import com.google.android.gms.ads.purchase.InAppPurchaseListener;
import com.google.android.gms.ads.purchase.PlayStorePurchaseListener;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.google.android.gms.internal.zzee;
import com.google.android.gms.internal.zzgy;
import com.google.android.gms.internal.zzil;
import com.google.android.gms.internal.zzip;
import com.google.android.gms.internal.zzji;

@zzji
public class zzaf
{
  private final Context mContext;
  private final zzh zzakc;
  private String zzant;
  private zza zzayj;
  private AdListener zzayk;
  private AppEventListener zzazw;
  private final zzgy zzbba = new zzgy();
  private Correlator zzbbe;
  private zzu zzbbf;
  private InAppPurchaseListener zzbbg;
  private OnCustomRenderedAdLoadedListener zzbbh;
  private PlayStorePurchaseListener zzbbi;
  private String zzbbj;
  private PublisherInterstitialAd zzbbn;
  private boolean zzbbo;
  private RewardedVideoAdListener zzgj;
  
  public zzaf(Context paramContext)
  {
    this(paramContext, zzh.zzkb(), null);
  }
  
  public zzaf(Context paramContext, PublisherInterstitialAd paramPublisherInterstitialAd)
  {
    this(paramContext, zzh.zzkb(), paramPublisherInterstitialAd);
  }
  
  public zzaf(Context paramContext, zzh paramzzh, PublisherInterstitialAd paramPublisherInterstitialAd)
  {
    this.mContext = paramContext;
    this.zzakc = paramzzh;
    this.zzbbn = paramPublisherInterstitialAd;
  }
  
  private void zzat(String paramString)
    throws RemoteException
  {
    if (this.zzant == null) {
      zzau(paramString);
    }
    if (this.zzbbo) {}
    for (paramString = AdSizeParcel.zzkc();; paramString = new AdSizeParcel())
    {
      this.zzbbf = zzm.zzks().zzb(this.mContext, paramString, this.zzant, this.zzbba);
      if (this.zzayk != null) {
        this.zzbbf.zza(new zzc(this.zzayk));
      }
      if (this.zzayj != null) {
        this.zzbbf.zza(new zzb(this.zzayj));
      }
      if (this.zzazw != null) {
        this.zzbbf.zza(new zzj(this.zzazw));
      }
      if (this.zzbbg != null) {
        this.zzbbf.zza(new zzil(this.zzbbg));
      }
      if (this.zzbbi != null) {
        this.zzbbf.zza(new zzip(this.zzbbi), this.zzbbj);
      }
      if (this.zzbbh != null) {
        this.zzbbf.zza(new zzee(this.zzbbh));
      }
      if (this.zzbbe != null) {
        this.zzbbf.zza(this.zzbbe.zzdu());
      }
      if (this.zzgj != null) {
        this.zzbbf.zza(new zzg(this.zzgj));
      }
      return;
    }
  }
  
  private void zzau(String paramString)
  {
    if (this.zzbbf == null) {
      throw new IllegalStateException(String.valueOf(paramString).length() + 63 + "The ad unit ID must be set on InterstitialAd before " + paramString + " is called.");
    }
  }
  
  public AdListener getAdListener()
  {
    return this.zzayk;
  }
  
  public String getAdUnitId()
  {
    return this.zzant;
  }
  
  public AppEventListener getAppEventListener()
  {
    return this.zzazw;
  }
  
  public InAppPurchaseListener getInAppPurchaseListener()
  {
    return this.zzbbg;
  }
  
  public String getMediationAdapterClassName()
  {
    try
    {
      if (this.zzbbf != null)
      {
        String str = this.zzbbf.getMediationAdapterClassName();
        return str;
      }
    }
    catch (RemoteException localRemoteException)
    {
      com.google.android.gms.ads.internal.util.client.zzb.zzc("Failed to get the mediation adapter class name.", localRemoteException);
    }
    return null;
  }
  
  public OnCustomRenderedAdLoadedListener getOnCustomRenderedAdLoadedListener()
  {
    return this.zzbbh;
  }
  
  public boolean isLoaded()
  {
    try
    {
      if (this.zzbbf == null) {
        return false;
      }
      boolean bool = this.zzbbf.isReady();
      return bool;
    }
    catch (RemoteException localRemoteException)
    {
      com.google.android.gms.ads.internal.util.client.zzb.zzc("Failed to check if ad is ready.", localRemoteException);
    }
    return false;
  }
  
  public boolean isLoading()
  {
    try
    {
      if (this.zzbbf == null) {
        return false;
      }
      boolean bool = this.zzbbf.isLoading();
      return bool;
    }
    catch (RemoteException localRemoteException)
    {
      com.google.android.gms.ads.internal.util.client.zzb.zzc("Failed to check if ad is loading.", localRemoteException);
    }
    return false;
  }
  
  public void setAdListener(AdListener paramAdListener)
  {
    try
    {
      this.zzayk = paramAdListener;
      zzu localzzu;
      if (this.zzbbf != null)
      {
        localzzu = this.zzbbf;
        if (paramAdListener == null) {
          break label38;
        }
      }
      label38:
      for (paramAdListener = new zzc(paramAdListener);; paramAdListener = null)
      {
        localzzu.zza(paramAdListener);
        return;
      }
      return;
    }
    catch (RemoteException paramAdListener)
    {
      com.google.android.gms.ads.internal.util.client.zzb.zzc("Failed to set the AdListener.", paramAdListener);
    }
  }
  
  public void setAdUnitId(String paramString)
  {
    if (this.zzant != null) {
      throw new IllegalStateException("The ad unit ID can only be set once on InterstitialAd.");
    }
    this.zzant = paramString;
  }
  
  public void setAppEventListener(AppEventListener paramAppEventListener)
  {
    try
    {
      this.zzazw = paramAppEventListener;
      zzu localzzu;
      if (this.zzbbf != null)
      {
        localzzu = this.zzbbf;
        if (paramAppEventListener == null) {
          break label38;
        }
      }
      label38:
      for (paramAppEventListener = new zzj(paramAppEventListener);; paramAppEventListener = null)
      {
        localzzu.zza(paramAppEventListener);
        return;
      }
      return;
    }
    catch (RemoteException paramAppEventListener)
    {
      com.google.android.gms.ads.internal.util.client.zzb.zzc("Failed to set the AppEventListener.", paramAppEventListener);
    }
  }
  
  public void setCorrelator(Correlator paramCorrelator)
  {
    this.zzbbe = paramCorrelator;
    try
    {
      if (this.zzbbf != null)
      {
        zzu localzzu = this.zzbbf;
        if (this.zzbbe == null) {}
        for (paramCorrelator = null;; paramCorrelator = this.zzbbe.zzdu())
        {
          localzzu.zza(paramCorrelator);
          return;
        }
      }
      return;
    }
    catch (RemoteException paramCorrelator)
    {
      com.google.android.gms.ads.internal.util.client.zzb.zzc("Failed to set correlator.", paramCorrelator);
    }
  }
  
  public void setInAppPurchaseListener(InAppPurchaseListener paramInAppPurchaseListener)
  {
    if (this.zzbbi != null) {
      throw new IllegalStateException("Play store purchase parameter has already been set.");
    }
    try
    {
      this.zzbbg = paramInAppPurchaseListener;
      zzu localzzu;
      if (this.zzbbf != null)
      {
        localzzu = this.zzbbf;
        if (paramInAppPurchaseListener == null) {
          break label56;
        }
      }
      label56:
      for (paramInAppPurchaseListener = new zzil(paramInAppPurchaseListener);; paramInAppPurchaseListener = null)
      {
        localzzu.zza(paramInAppPurchaseListener);
        return;
      }
      return;
    }
    catch (RemoteException paramInAppPurchaseListener)
    {
      com.google.android.gms.ads.internal.util.client.zzb.zzc("Failed to set the InAppPurchaseListener.", paramInAppPurchaseListener);
    }
  }
  
  public void setOnCustomRenderedAdLoadedListener(OnCustomRenderedAdLoadedListener paramOnCustomRenderedAdLoadedListener)
  {
    try
    {
      this.zzbbh = paramOnCustomRenderedAdLoadedListener;
      zzu localzzu;
      if (this.zzbbf != null)
      {
        localzzu = this.zzbbf;
        if (paramOnCustomRenderedAdLoadedListener == null) {
          break label38;
        }
      }
      label38:
      for (paramOnCustomRenderedAdLoadedListener = new zzee(paramOnCustomRenderedAdLoadedListener);; paramOnCustomRenderedAdLoadedListener = null)
      {
        localzzu.zza(paramOnCustomRenderedAdLoadedListener);
        return;
      }
      return;
    }
    catch (RemoteException paramOnCustomRenderedAdLoadedListener)
    {
      com.google.android.gms.ads.internal.util.client.zzb.zzc("Failed to set the OnCustomRenderedAdLoadedListener.", paramOnCustomRenderedAdLoadedListener);
    }
  }
  
  public void setPlayStorePurchaseParams(PlayStorePurchaseListener paramPlayStorePurchaseListener, String paramString)
  {
    if (this.zzbbg != null) {
      throw new IllegalStateException("In app purchase parameter has already been set.");
    }
    try
    {
      this.zzbbi = paramPlayStorePurchaseListener;
      this.zzbbj = paramString;
      zzu localzzu;
      if (this.zzbbf != null)
      {
        localzzu = this.zzbbf;
        if (paramPlayStorePurchaseListener == null) {
          break label62;
        }
      }
      label62:
      for (paramPlayStorePurchaseListener = new zzip(paramPlayStorePurchaseListener);; paramPlayStorePurchaseListener = null)
      {
        localzzu.zza(paramPlayStorePurchaseListener, paramString);
        return;
      }
      return;
    }
    catch (RemoteException paramPlayStorePurchaseListener)
    {
      com.google.android.gms.ads.internal.util.client.zzb.zzc("Failed to set the play store purchase parameter.", paramPlayStorePurchaseListener);
    }
  }
  
  public void setRewardedVideoAdListener(RewardedVideoAdListener paramRewardedVideoAdListener)
  {
    try
    {
      this.zzgj = paramRewardedVideoAdListener;
      zzu localzzu;
      if (this.zzbbf != null)
      {
        localzzu = this.zzbbf;
        if (paramRewardedVideoAdListener == null) {
          break label38;
        }
      }
      label38:
      for (paramRewardedVideoAdListener = new zzg(paramRewardedVideoAdListener);; paramRewardedVideoAdListener = null)
      {
        localzzu.zza(paramRewardedVideoAdListener);
        return;
      }
      return;
    }
    catch (RemoteException paramRewardedVideoAdListener)
    {
      com.google.android.gms.ads.internal.util.client.zzb.zzc("Failed to set the AdListener.", paramRewardedVideoAdListener);
    }
  }
  
  public void show()
  {
    try
    {
      zzau("show");
      this.zzbbf.showInterstitial();
      return;
    }
    catch (RemoteException localRemoteException)
    {
      com.google.android.gms.ads.internal.util.client.zzb.zzc("Failed to show interstitial.", localRemoteException);
    }
  }
  
  public void zza(zza paramzza)
  {
    try
    {
      this.zzayj = paramzza;
      zzu localzzu;
      if (this.zzbbf != null)
      {
        localzzu = this.zzbbf;
        if (paramzza == null) {
          break label38;
        }
      }
      label38:
      for (paramzza = new zzb(paramzza);; paramzza = null)
      {
        localzzu.zza(paramzza);
        return;
      }
      return;
    }
    catch (RemoteException paramzza)
    {
      com.google.android.gms.ads.internal.util.client.zzb.zzc("Failed to set the AdClickListener.", paramzza);
    }
  }
  
  public void zza(zzad paramzzad)
  {
    try
    {
      if (this.zzbbf == null) {
        zzat("loadAd");
      }
      if (this.zzbbf.zzb(this.zzakc.zza(this.mContext, paramzzad))) {
        this.zzbba.zzi(paramzzad.zzlb());
      }
      return;
    }
    catch (RemoteException paramzzad)
    {
      com.google.android.gms.ads.internal.util.client.zzb.zzc("Failed to load ad.", paramzzad);
    }
  }
  
  public void zzd(boolean paramBoolean)
  {
    this.zzbbo = paramBoolean;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/client/zzaf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */