package com.google.android.gms.ads.internal.client;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.ads.internal.reward.client.zzi;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.internal.zzgy;
import com.google.android.gms.internal.zzji;

@zzji
public class zzag
{
  private static final Object zzaox = new Object();
  private static zzag zzbbp;
  private zzz zzbbq;
  private RewardedVideoAd zzbbr;
  
  public static zzag zzli()
  {
    synchronized (zzaox)
    {
      if (zzbbp == null) {
        zzbbp = new zzag();
      }
      zzag localzzag = zzbbp;
      return localzzag;
    }
  }
  
  public RewardedVideoAd getRewardedVideoAdInstance(Context paramContext)
  {
    synchronized (zzaox)
    {
      if (this.zzbbr != null)
      {
        paramContext = this.zzbbr;
        return paramContext;
      }
      zzgy localzzgy = new zzgy();
      this.zzbbr = new zzi(paramContext, zzm.zzks().zza(paramContext, localzzgy));
      paramContext = this.zzbbr;
      return paramContext;
    }
  }
  
  public void openDebugMenu(Context paramContext, String paramString)
  {
    if (this.zzbbq != null) {}
    for (boolean bool = true;; bool = false)
    {
      zzaa.zza(bool, "MobileAds.initialize() must be called prior to opening debug menu.");
      try
      {
        this.zzbbq.zzb(zze.zzac(paramContext), paramString);
        return;
      }
      catch (RemoteException paramContext)
      {
        zzb.zzb("Unable to open debug menu.", paramContext);
      }
    }
  }
  
  public void setAppMuted(boolean paramBoolean)
  {
    if (this.zzbbq != null) {}
    for (boolean bool = true;; bool = false)
    {
      zzaa.zza(bool, "MobileAds.initialize() must be called prior to setting the app volume.");
      try
      {
        this.zzbbq.setAppMuted(paramBoolean);
        return;
      }
      catch (RemoteException localRemoteException)
      {
        zzb.zzb("Unable to set app mute state.", localRemoteException);
      }
    }
  }
  
  public void setAppVolume(float paramFloat)
  {
    boolean bool2 = true;
    if ((0.0F <= paramFloat) && (paramFloat <= 1.0F))
    {
      bool1 = true;
      zzaa.zzb(bool1, "The app volume must be a value between 0 and 1 inclusive.");
      if (this.zzbbq == null) {
        break label53;
      }
    }
    label53:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      zzaa.zza(bool1, "MobileAds.initialize() must be called prior to setting the app volume.");
      try
      {
        this.zzbbq.setAppVolume(paramFloat);
        return;
      }
      catch (RemoteException localRemoteException)
      {
        zzb.zzb("Unable to set app volume.", localRemoteException);
      }
      bool1 = false;
      break;
    }
  }
  
  public void zza(Context paramContext, String paramString, zzah arg3)
  {
    synchronized (zzaox)
    {
      if (this.zzbbq != null) {
        return;
      }
      if (paramContext == null) {
        throw new IllegalArgumentException("Context cannot be null.");
      }
    }
    try
    {
      this.zzbbq = zzm.zzks().zzk(paramContext);
      this.zzbbq.initialize();
      if (paramString != null) {
        this.zzbbq.zzz(paramString);
      }
    }
    catch (RemoteException paramContext)
    {
      for (;;)
      {
        zzb.zzc("Fail to initialize or set applicationCode on mobile ads setting manager", paramContext);
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/client/zzag.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */