package com.google.android.gms.ads.internal.client;

import android.content.Context;
import android.os.IBinder;
import android.os.RemoteException;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.dynamic.zzd;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.dynamic.zzg;
import com.google.android.gms.dynamic.zzg.zza;
import com.google.android.gms.internal.zzji;

@zzji
public class zzai
  extends zzg<zzaa>
{
  public zzai()
  {
    super("com.google.android.gms.ads.MobileAdsSettingManagerCreatorImpl");
  }
  
  public zzz zzl(Context paramContext)
  {
    try
    {
      zzd localzzd = zze.zzac(paramContext);
      paramContext = zzz.zza.zzu(((zzaa)zzcr(paramContext)).zza(localzzd, 9877000));
      return paramContext;
    }
    catch (RemoteException paramContext)
    {
      zzb.zzc("Could not get remote MobileAdsSettingManager.", paramContext);
      return null;
    }
    catch (zzg.zza paramContext)
    {
      zzb.zzc("Could not get remote MobileAdsSettingManager.", paramContext);
    }
    return null;
  }
  
  protected zzaa zzy(IBinder paramIBinder)
  {
    return zzaa.zza.zzv(paramIBinder);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/client/zzai.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */