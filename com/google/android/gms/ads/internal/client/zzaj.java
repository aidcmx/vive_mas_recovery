package com.google.android.gms.ads.internal.client;

import android.os.Handler;
import android.os.RemoteException;
import com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.internal.zzeq;
import com.google.android.gms.internal.zzer;
import com.google.android.gms.internal.zzes;
import com.google.android.gms.internal.zzet;

public class zzaj
  extends zzs.zza
{
  private zzq zzanl;
  
  public void zza(NativeAdOptionsParcel paramNativeAdOptionsParcel)
    throws RemoteException
  {}
  
  public void zza(zzeq paramzzeq)
    throws RemoteException
  {}
  
  public void zza(zzer paramzzer)
    throws RemoteException
  {}
  
  public void zza(String paramString, zzet paramzzet, zzes paramzzes)
    throws RemoteException
  {}
  
  public void zzb(zzq paramzzq)
    throws RemoteException
  {
    this.zzanl = paramzzq;
  }
  
  public void zzb(zzy paramzzy)
    throws RemoteException
  {}
  
  public zzr zzfl()
    throws RemoteException
  {
    return new zza(null);
  }
  
  private class zza
    extends zzr.zza
  {
    private zza() {}
    
    public String getMediationAdapterClassName()
      throws RemoteException
    {
      return null;
    }
    
    public boolean isLoading()
      throws RemoteException
    {
      return false;
    }
    
    public void zzf(AdRequestParcel paramAdRequestParcel)
      throws RemoteException
    {
      zzb.e("This app is using a lightweight version of the Google Mobile Ads SDK that requires the latest Google Play services to be installed, but Google Play services is either missing or out of date.");
      zza.zzcxr.post(new Runnable()
      {
        public void run()
        {
          if (zzaj.zza(zzaj.this) != null) {}
          try
          {
            zzaj.zza(zzaj.this).onAdFailedToLoad(1);
            return;
          }
          catch (RemoteException localRemoteException)
          {
            zzb.zzc("Could not notify onAdFailedToLoad event.", localRemoteException);
          }
        }
      });
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/client/zzaj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */