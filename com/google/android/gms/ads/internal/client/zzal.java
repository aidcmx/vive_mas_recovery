package com.google.android.gms.ads.internal.client;

import android.os.RemoteException;
import com.google.android.gms.dynamic.zzd;

public class zzal
  extends zzz.zza
{
  public void initialize()
    throws RemoteException
  {}
  
  public void setAppMuted(boolean paramBoolean)
    throws RemoteException
  {}
  
  public void setAppVolume(float paramFloat)
    throws RemoteException
  {}
  
  public void zzb(zzd paramzzd, String paramString)
    throws RemoteException
  {}
  
  public void zzz(String paramString)
    throws RemoteException
  {}
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/client/zzal.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */