package com.google.android.gms.ads.internal.client;

import com.google.android.gms.ads.VideoController.VideoLifecycleCallbacks;

public final class zzap
  extends zzac.zza
{
  private final VideoController.VideoLifecycleCallbacks zzakq;
  
  public zzap(VideoController.VideoLifecycleCallbacks paramVideoLifecycleCallbacks)
  {
    this.zzakq = paramVideoLifecycleCallbacks;
  }
  
  public void onVideoEnd()
  {
    this.zzakq.onVideoEnd();
  }
  
  public void zzkw() {}
  
  public void zzkx() {}
  
  public void zzky() {}
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/client/zzap.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */