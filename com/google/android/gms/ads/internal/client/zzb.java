package com.google.android.gms.ads.internal.client;

import com.google.android.gms.internal.zzji;

@zzji
public final class zzb
  extends zzp.zza
{
  private final zza zzayj;
  
  public zzb(zza paramzza)
  {
    this.zzayj = paramzza;
  }
  
  public void onAdClicked()
  {
    this.zzayj.onAdClicked();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/client/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */