package com.google.android.gms.ads.internal.client;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.internal.zzji;

@zzji
public final class zzc
  extends zzq.zza
{
  private final AdListener zzayk;
  
  public zzc(AdListener paramAdListener)
  {
    this.zzayk = paramAdListener;
  }
  
  public void onAdClosed()
  {
    this.zzayk.onAdClosed();
  }
  
  public void onAdFailedToLoad(int paramInt)
  {
    this.zzayk.onAdFailedToLoad(paramInt);
  }
  
  public void onAdLeftApplication()
  {
    this.zzayk.onAdLeftApplication();
  }
  
  public void onAdLoaded()
  {
    this.zzayk.onAdLoaded();
  }
  
  public void onAdOpened()
  {
    this.zzayk.onAdOpened();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/client/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */