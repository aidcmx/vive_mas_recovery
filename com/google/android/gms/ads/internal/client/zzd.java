package com.google.android.gms.ads.internal.client;

import android.content.Context;
import android.os.IBinder;
import android.os.RemoteException;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.dynamic.zzg;
import com.google.android.gms.dynamic.zzg.zza;
import com.google.android.gms.internal.zzgz;
import com.google.android.gms.internal.zzji;

@zzji
public final class zzd
  extends zzg<zzt>
{
  public zzd()
  {
    super("com.google.android.gms.ads.AdLoaderBuilderCreatorImpl");
  }
  
  public zzs zza(Context paramContext, String paramString, zzgz paramzzgz)
  {
    try
    {
      com.google.android.gms.dynamic.zzd localzzd = zze.zzac(paramContext);
      paramContext = zzs.zza.zzo(((zzt)zzcr(paramContext)).zza(localzzd, paramString, paramzzgz, 9877000));
      return paramContext;
    }
    catch (RemoteException paramContext)
    {
      zzb.zzc("Could not create remote builder for AdLoader.", paramContext);
      return null;
    }
    catch (zzg.zza paramContext)
    {
      for (;;)
      {
        zzb.zzc("Could not create remote builder for AdLoader.", paramContext);
      }
    }
  }
  
  protected zzt zzj(IBinder paramIBinder)
  {
    return zzt.zza.zzp(paramIBinder);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/client/zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */