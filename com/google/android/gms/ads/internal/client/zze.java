package com.google.android.gms.ads.internal.client;

import android.content.Context;
import android.os.IBinder;
import android.os.RemoteException;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.dynamic.zzd;
import com.google.android.gms.dynamic.zzg;
import com.google.android.gms.dynamic.zzg.zza;
import com.google.android.gms.internal.zzgz;
import com.google.android.gms.internal.zzji;

@zzji
public class zze
  extends zzg<zzv>
{
  public zze()
  {
    super("com.google.android.gms.ads.AdManagerCreatorImpl");
  }
  
  public zzu zza(Context paramContext, AdSizeParcel paramAdSizeParcel, String paramString, zzgz paramzzgz, int paramInt)
  {
    try
    {
      zzd localzzd = com.google.android.gms.dynamic.zze.zzac(paramContext);
      paramContext = zzu.zza.zzq(((zzv)zzcr(paramContext)).zza(localzzd, paramAdSizeParcel, paramString, paramzzgz, 9877000, paramInt));
      return paramContext;
    }
    catch (RemoteException paramContext)
    {
      zzb.zza("Could not create remote AdManager.", paramContext);
      return null;
    }
    catch (zzg.zza paramContext)
    {
      for (;;) {}
    }
  }
  
  protected zzv zzk(IBinder paramIBinder)
  {
    return zzv.zza.zzr(paramIBinder);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/client/zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */