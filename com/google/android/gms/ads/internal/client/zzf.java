package com.google.android.gms.ads.internal.client;

import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import com.google.android.gms.internal.zzji;
import java.util.ArrayList;
import java.util.List;

@zzji
public final class zzf
{
  private Bundle mExtras;
  private boolean zzamv;
  private long zzazb;
  private int zzazc;
  private List<String> zzazd;
  private boolean zzaze;
  private int zzazf;
  private String zzazg;
  private SearchAdRequestParcel zzazh;
  private String zzazi;
  private Bundle zzazj;
  private Bundle zzazk;
  private List<String> zzazl;
  private String zzazm;
  private String zzazn;
  private boolean zzazo;
  private Location zzgv;
  
  public zzf()
  {
    this.zzazb = -1L;
    this.mExtras = new Bundle();
    this.zzazc = -1;
    this.zzazd = new ArrayList();
    this.zzaze = false;
    this.zzazf = -1;
    this.zzamv = false;
    this.zzazg = null;
    this.zzazh = null;
    this.zzgv = null;
    this.zzazi = null;
    this.zzazj = new Bundle();
    this.zzazk = new Bundle();
    this.zzazl = new ArrayList();
    this.zzazm = null;
    this.zzazn = null;
    this.zzazo = false;
  }
  
  public zzf(AdRequestParcel paramAdRequestParcel)
  {
    this.zzazb = paramAdRequestParcel.zzayl;
    this.mExtras = paramAdRequestParcel.extras;
    this.zzazc = paramAdRequestParcel.zzaym;
    this.zzazd = paramAdRequestParcel.zzayn;
    this.zzaze = paramAdRequestParcel.zzayo;
    this.zzazf = paramAdRequestParcel.zzayp;
    this.zzamv = paramAdRequestParcel.zzayq;
    this.zzazg = paramAdRequestParcel.zzayr;
    this.zzazh = paramAdRequestParcel.zzays;
    this.zzgv = paramAdRequestParcel.zzayt;
    this.zzazi = paramAdRequestParcel.zzayu;
    this.zzazj = paramAdRequestParcel.zzayv;
    this.zzazk = paramAdRequestParcel.zzayw;
    this.zzazl = paramAdRequestParcel.zzayx;
    this.zzazm = paramAdRequestParcel.zzayy;
    this.zzazn = paramAdRequestParcel.zzayz;
  }
  
  public zzf zza(@Nullable Location paramLocation)
  {
    this.zzgv = paramLocation;
    return this;
  }
  
  public AdRequestParcel zzka()
  {
    return new AdRequestParcel(7, this.zzazb, this.mExtras, this.zzazc, this.zzazd, this.zzaze, this.zzazf, this.zzamv, this.zzazg, this.zzazh, this.zzgv, this.zzazi, this.zzazj, this.zzazk, this.zzazl, this.zzazm, this.zzazn, false);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/client/zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */