package com.google.android.gms.ads.internal.client;

import com.google.android.gms.ads.doubleclick.AppEventListener;
import com.google.android.gms.internal.zzji;

@zzji
public final class zzj
  extends zzw.zza
{
  private final AppEventListener zzazw;
  
  public zzj(AppEventListener paramAppEventListener)
  {
    this.zzazw = paramAppEventListener;
  }
  
  public void onAppEvent(String paramString1, String paramString2)
  {
    this.zzazw.onAppEvent(paramString1, paramString2);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/client/zzj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */