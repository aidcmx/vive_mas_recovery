package com.google.android.gms.ads.internal.client;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.widget.FrameLayout;
import com.google.android.gms.ads.internal.reward.client.zzf;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.internal.zzei;
import com.google.android.gms.internal.zzeu;
import com.google.android.gms.internal.zzgz;
import com.google.android.gms.internal.zzhx;
import com.google.android.gms.internal.zzhy;
import com.google.android.gms.internal.zzih;
import com.google.android.gms.internal.zzim;
import com.google.android.gms.internal.zzji;

@zzji
public class zzl
{
  private final Object zzako = new Object();
  private zzx zzazy;
  private final zze zzazz;
  private final zzd zzbaa;
  private final zzai zzbab;
  private final zzeu zzbac;
  private final zzf zzbad;
  private final zzim zzbae;
  private final zzhx zzbaf;
  
  public zzl(zze paramzze, zzd paramzzd, zzai paramzzai, zzeu paramzzeu, zzf paramzzf, zzim paramzzim, zzhx paramzzhx)
  {
    this.zzazz = paramzze;
    this.zzbaa = paramzzd;
    this.zzbab = paramzzai;
    this.zzbac = paramzzeu;
    this.zzbad = paramzzf;
    this.zzbae = paramzzim;
    this.zzbaf = paramzzhx;
  }
  
  private static boolean zza(Activity paramActivity, String paramString)
  {
    paramActivity = paramActivity.getIntent();
    if (!paramActivity.hasExtra(paramString))
    {
      com.google.android.gms.ads.internal.util.client.zzb.e("useClientJar flag not found in activity intent extras.");
      return false;
    }
    return paramActivity.getBooleanExtra(paramString, false);
  }
  
  private void zzc(Context paramContext, String paramString)
  {
    Bundle localBundle = new Bundle();
    localBundle.putString("action", "no_ads_fallback");
    localBundle.putString("flow", paramString);
    zzm.zzkr().zza(paramContext, null, "gmob-apps", localBundle, true);
  }
  
  @Nullable
  private static zzx zzke()
  {
    try
    {
      Object localObject = zzl.class.getClassLoader().loadClass("com.google.android.gms.ads.internal.ClientApi").newInstance();
      if (!(localObject instanceof IBinder))
      {
        com.google.android.gms.ads.internal.util.client.zzb.zzdi("ClientApi class is not an instance of IBinder");
        return null;
      }
      localObject = zzx.zza.asInterface((IBinder)localObject);
      return (zzx)localObject;
    }
    catch (Exception localException)
    {
      com.google.android.gms.ads.internal.util.client.zzb.zzc("Failed to instantiate ClientApi class.", localException);
    }
    return null;
  }
  
  @Nullable
  private zzx zzkf()
  {
    synchronized (this.zzako)
    {
      if (this.zzazy == null) {
        this.zzazy = zzke();
      }
      zzx localzzx = this.zzazy;
      return localzzx;
    }
  }
  
  public zzu zza(final Context paramContext, final AdSizeParcel paramAdSizeParcel, final String paramString)
  {
    (zzu)zza(paramContext, false, new zza(paramContext)
    {
      public zzu zza(zzx paramAnonymouszzx)
        throws RemoteException
      {
        return paramAnonymouszzx.createSearchAdManager(com.google.android.gms.dynamic.zze.zzac(paramContext), paramAdSizeParcel, paramString, 9877000);
      }
      
      public zzu zzkg()
      {
        zzu localzzu = zzl.zzb(zzl.this).zza(paramContext, paramAdSizeParcel, paramString, null, 3);
        if (localzzu != null) {
          return localzzu;
        }
        zzl.zza(zzl.this, paramContext, "search");
        return new zzak();
      }
    });
  }
  
  public zzu zza(final Context paramContext, final AdSizeParcel paramAdSizeParcel, final String paramString, final zzgz paramzzgz)
  {
    (zzu)zza(paramContext, false, new zza(paramContext)
    {
      public zzu zza(zzx paramAnonymouszzx)
        throws RemoteException
      {
        return paramAnonymouszzx.createBannerAdManager(com.google.android.gms.dynamic.zze.zzac(paramContext), paramAdSizeParcel, paramString, paramzzgz, 9877000);
      }
      
      public zzu zzkg()
      {
        zzu localzzu = zzl.zzb(zzl.this).zza(paramContext, paramAdSizeParcel, paramString, paramzzgz, 1);
        if (localzzu != null) {
          return localzzu;
        }
        zzl.zza(zzl.this, paramContext, "banner");
        return new zzak();
      }
    });
  }
  
  public com.google.android.gms.ads.internal.reward.client.zzb zza(final Context paramContext, final zzgz paramzzgz)
  {
    (com.google.android.gms.ads.internal.reward.client.zzb)zza(paramContext, false, new zza(paramContext)
    {
      public com.google.android.gms.ads.internal.reward.client.zzb zzf(zzx paramAnonymouszzx)
        throws RemoteException
      {
        return paramAnonymouszzx.createRewardedVideoAd(com.google.android.gms.dynamic.zze.zzac(paramContext), paramzzgz, 9877000);
      }
      
      public com.google.android.gms.ads.internal.reward.client.zzb zzkl()
      {
        com.google.android.gms.ads.internal.reward.client.zzb localzzb = zzl.zzf(zzl.this).zzb(paramContext, paramzzgz);
        if (localzzb != null) {
          return localzzb;
        }
        zzl.zza(zzl.this, paramContext, "rewarded_video");
        return new zzan();
      }
    });
  }
  
  public zzei zza(final Context paramContext, final FrameLayout paramFrameLayout1, final FrameLayout paramFrameLayout2)
  {
    (zzei)zza(paramContext, false, new zza(paramFrameLayout1)
    {
      public zzei zze(zzx paramAnonymouszzx)
        throws RemoteException
      {
        return paramAnonymouszzx.createNativeAdViewDelegate(com.google.android.gms.dynamic.zze.zzac(paramFrameLayout1), com.google.android.gms.dynamic.zze.zzac(paramFrameLayout2));
      }
      
      public zzei zzkk()
      {
        zzei localzzei = zzl.zze(zzl.this).zzb(paramContext, paramFrameLayout1, paramFrameLayout2);
        if (localzzei != null) {
          return localzzei;
        }
        zzl.zza(zzl.this, paramContext, "native_ad_view_delegate");
        return new zzam();
      }
    });
  }
  
  @VisibleForTesting
  <T> T zza(Context paramContext, boolean paramBoolean, zza<T> paramzza)
  {
    boolean bool = paramBoolean;
    if (!paramBoolean)
    {
      bool = paramBoolean;
      if (!zzm.zzkr().zzap(paramContext))
      {
        com.google.android.gms.ads.internal.util.client.zzb.zzdg("Google Play Services is not available");
        bool = true;
      }
    }
    Object localObject;
    if (bool)
    {
      localObject = paramzza.zzko();
      paramContext = (Context)localObject;
      if (localObject == null) {
        paramContext = paramzza.zzkp();
      }
    }
    do
    {
      return paramContext;
      localObject = paramzza.zzkp();
      paramContext = (Context)localObject;
    } while (localObject != null);
    return (T)paramzza.zzko();
  }
  
  public zzs zzb(final Context paramContext, final String paramString, final zzgz paramzzgz)
  {
    (zzs)zza(paramContext, false, new zza(paramContext)
    {
      public zzs zzc(zzx paramAnonymouszzx)
        throws RemoteException
      {
        return paramAnonymouszzx.createAdLoaderBuilder(com.google.android.gms.dynamic.zze.zzac(paramContext), paramString, paramzzgz, 9877000);
      }
      
      public zzs zzki()
      {
        zzs localzzs = zzl.zzc(zzl.this).zza(paramContext, paramString, paramzzgz);
        if (localzzs != null) {
          return localzzs;
        }
        zzl.zza(zzl.this, paramContext, "native_ad");
        return new zzaj();
      }
    });
  }
  
  public zzu zzb(final Context paramContext, final AdSizeParcel paramAdSizeParcel, final String paramString, final zzgz paramzzgz)
  {
    (zzu)zza(paramContext, false, new zza(paramContext)
    {
      public zzu zza(zzx paramAnonymouszzx)
        throws RemoteException
      {
        return paramAnonymouszzx.createInterstitialAdManager(com.google.android.gms.dynamic.zze.zzac(paramContext), paramAdSizeParcel, paramString, paramzzgz, 9877000);
      }
      
      public zzu zzkg()
      {
        zzu localzzu = zzl.zzb(zzl.this).zza(paramContext, paramAdSizeParcel, paramString, paramzzgz, 2);
        if (localzzu != null) {
          return localzzu;
        }
        zzl.zza(zzl.this, paramContext, "interstitial");
        return new zzak();
      }
    });
  }
  
  @Nullable
  public zzih zzb(final Activity paramActivity)
  {
    (zzih)zza(paramActivity, zza(paramActivity, "com.google.android.gms.ads.internal.purchase.useClientJar"), new zza(paramActivity)
    {
      public zzih zzg(zzx paramAnonymouszzx)
        throws RemoteException
      {
        return paramAnonymouszzx.createInAppPurchaseManager(com.google.android.gms.dynamic.zze.zzac(paramActivity));
      }
      
      public zzih zzkm()
      {
        zzih localzzih = zzl.zzg(zzl.this).zzg(paramActivity);
        if (localzzih != null) {
          return localzzih;
        }
        zzl.zza(zzl.this, paramActivity, "iap");
        return null;
      }
    });
  }
  
  @Nullable
  public zzhy zzc(final Activity paramActivity)
  {
    (zzhy)zza(paramActivity, zza(paramActivity, "com.google.android.gms.ads.internal.overlay.useClientJar"), new zza(paramActivity)
    {
      public zzhy zzh(zzx paramAnonymouszzx)
        throws RemoteException
      {
        return paramAnonymouszzx.createAdOverlay(com.google.android.gms.dynamic.zze.zzac(paramActivity));
      }
      
      public zzhy zzkn()
      {
        zzhy localzzhy = zzl.zzh(zzl.this).zzf(paramActivity);
        if (localzzhy != null) {
          return localzzhy;
        }
        zzl.zza(zzl.this, paramActivity, "ad_overlay");
        return null;
      }
    });
  }
  
  public zzz zzk(final Context paramContext)
  {
    (zzz)zza(paramContext, false, new zza(paramContext)
    {
      public zzz zzd(zzx paramAnonymouszzx)
        throws RemoteException
      {
        return paramAnonymouszzx.getMobileAdsSettingsManagerWithClientJarVersion(com.google.android.gms.dynamic.zze.zzac(paramContext), 9877000);
      }
      
      public zzz zzkj()
      {
        zzz localzzz = zzl.zzd(zzl.this).zzl(paramContext);
        if (localzzz != null) {
          return localzzz;
        }
        zzl.zza(zzl.this, paramContext, "mobile_ads_settings");
        return new zzal();
      }
    });
  }
  
  @VisibleForTesting
  abstract class zza<T>
  {
    zza() {}
    
    @Nullable
    protected abstract T zzb(zzx paramzzx)
      throws RemoteException;
    
    @Nullable
    protected abstract T zzkh()
      throws RemoteException;
    
    @Nullable
    protected final T zzko()
    {
      Object localObject = zzl.zza(zzl.this);
      if (localObject == null)
      {
        com.google.android.gms.ads.internal.util.client.zzb.zzdi("ClientApi class cannot be loaded.");
        return null;
      }
      try
      {
        localObject = zzb((zzx)localObject);
        return (T)localObject;
      }
      catch (RemoteException localRemoteException)
      {
        com.google.android.gms.ads.internal.util.client.zzb.zzc("Cannot invoke local loader using ClientApi class", localRemoteException);
      }
      return null;
    }
    
    @Nullable
    protected final T zzkp()
    {
      try
      {
        Object localObject = zzkh();
        return (T)localObject;
      }
      catch (RemoteException localRemoteException)
      {
        com.google.android.gms.ads.internal.util.client.zzb.zzc("Cannot invoke remote loader", localRemoteException);
      }
      return null;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/client/zzl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */