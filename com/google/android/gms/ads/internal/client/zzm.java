package com.google.android.gms.ads.internal.client;

import com.google.android.gms.ads.internal.reward.client.zzf;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.internal.zzeu;
import com.google.android.gms.internal.zzhx;
import com.google.android.gms.internal.zzim;
import com.google.android.gms.internal.zzji;

@zzji
public class zzm
{
  private static final Object zzaox = new Object();
  private static zzm zzbal;
  private final zza zzbam = new zza();
  private final zzl zzban = new zzl(new zze(), new zzd(), new zzai(), new zzeu(), new zzf(), new zzim(), new zzhx());
  
  static
  {
    zza(new zzm());
  }
  
  protected static void zza(zzm paramzzm)
  {
    synchronized (zzaox)
    {
      zzbal = paramzzm;
      return;
    }
  }
  
  private static zzm zzkq()
  {
    synchronized (zzaox)
    {
      zzm localzzm = zzbal;
      return localzzm;
    }
  }
  
  public static zza zzkr()
  {
    return zzkq().zzbam;
  }
  
  public static zzl zzks()
  {
    return zzkq().zzban;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/client/zzm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */