package com.google.android.gms.ads.internal.client;

import com.google.android.gms.internal.zzji;
import java.util.Random;

@zzji
public class zzn
  extends zzy.zza
{
  private Object zzako = new Object();
  private final Random zzbao = new Random();
  private long zzbap;
  
  public zzn()
  {
    zzkt();
  }
  
  public long getValue()
  {
    return this.zzbap;
  }
  
  public void zzkt()
  {
    Object localObject1 = this.zzako;
    int i = 3;
    long l1 = 0L;
    for (;;)
    {
      int j = i - 1;
      if (j > 0) {}
      try
      {
        long l2 = this.zzbao.nextInt() + 2147483648L;
        l1 = l2;
        i = j;
        if (l2 == this.zzbap) {
          continue;
        }
        l1 = l2;
        i = j;
        if (l2 == 0L) {
          continue;
        }
        l1 = l2;
        this.zzbap = l1;
        return;
      }
      finally {}
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/client/zzn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */