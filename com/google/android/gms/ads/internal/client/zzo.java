package com.google.android.gms.ads.internal.client;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.internal.zzji;

@zzji
public class zzo
  extends AdListener
{
  private final Object lock = new Object();
  private AdListener zzbaq;
  
  public void onAdClosed()
  {
    synchronized (this.lock)
    {
      if (this.zzbaq != null) {
        this.zzbaq.onAdClosed();
      }
      return;
    }
  }
  
  public void onAdFailedToLoad(int paramInt)
  {
    synchronized (this.lock)
    {
      if (this.zzbaq != null) {
        this.zzbaq.onAdFailedToLoad(paramInt);
      }
      return;
    }
  }
  
  public void onAdLeftApplication()
  {
    synchronized (this.lock)
    {
      if (this.zzbaq != null) {
        this.zzbaq.onAdLeftApplication();
      }
      return;
    }
  }
  
  public void onAdLoaded()
  {
    synchronized (this.lock)
    {
      if (this.zzbaq != null) {
        this.zzbaq.onAdLoaded();
      }
      return;
    }
  }
  
  public void onAdOpened()
  {
    synchronized (this.lock)
    {
      if (this.zzbaq != null) {
        this.zzbaq.onAdOpened();
      }
      return;
    }
  }
  
  public void zza(AdListener paramAdListener)
  {
    synchronized (this.lock)
    {
      this.zzbaq = paramAdListener;
      return;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/client/zzo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */