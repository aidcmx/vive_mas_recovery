package com.google.android.gms.ads.internal.formats;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.Nullable;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.internal.client.VideoOptionsParcel;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.internal.zzji;

@zzji
public class NativeAdOptionsParcel
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<NativeAdOptionsParcel> CREATOR = new zzk();
  public final int versionCode;
  public final boolean zzboj;
  public final int zzbok;
  public final boolean zzbol;
  public final int zzbom;
  @Nullable
  public final VideoOptionsParcel zzbon;
  
  public NativeAdOptionsParcel(int paramInt1, boolean paramBoolean1, int paramInt2, boolean paramBoolean2, int paramInt3, VideoOptionsParcel paramVideoOptionsParcel)
  {
    this.versionCode = paramInt1;
    this.zzboj = paramBoolean1;
    this.zzbok = paramInt2;
    this.zzbol = paramBoolean2;
    this.zzbom = paramInt3;
    this.zzbon = paramVideoOptionsParcel;
  }
  
  public NativeAdOptionsParcel(NativeAdOptions paramNativeAdOptions) {}
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzk.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/formats/NativeAdOptionsParcel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */