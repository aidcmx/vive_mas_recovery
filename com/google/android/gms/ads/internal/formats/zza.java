package com.google.android.gms.ads.internal.formats;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import com.google.android.gms.internal.zzji;
import java.util.List;

@zzji
public class zza
{
  private static final int zzbmk = Color.rgb(12, 174, 206);
  private static final int zzbml = Color.rgb(204, 204, 204);
  static final int zzbmm = zzbml;
  static final int zzbmn = zzbmk;
  private final int mBackgroundColor;
  private final int mTextColor;
  private final String zzbmo;
  private final List<Drawable> zzbmp;
  private final int zzbmq;
  private final int zzbmr;
  private final int zzbms;
  
  public zza(String paramString, List<Drawable> paramList, Integer paramInteger1, Integer paramInteger2, Integer paramInteger3, int paramInt1, int paramInt2)
  {
    this.zzbmo = paramString;
    this.zzbmp = paramList;
    if (paramInteger1 != null)
    {
      i = paramInteger1.intValue();
      this.mBackgroundColor = i;
      if (paramInteger2 == null) {
        break label87;
      }
      i = paramInteger2.intValue();
      label42:
      this.mTextColor = i;
      if (paramInteger3 == null) {
        break label95;
      }
    }
    label87:
    label95:
    for (int i = paramInteger3.intValue();; i = 12)
    {
      this.zzbmq = i;
      this.zzbmr = paramInt1;
      this.zzbms = paramInt2;
      return;
      i = zzbmm;
      break;
      i = zzbmn;
      break label42;
    }
  }
  
  public int getBackgroundColor()
  {
    return this.mBackgroundColor;
  }
  
  public String getText()
  {
    return this.zzbmo;
  }
  
  public int getTextColor()
  {
    return this.mTextColor;
  }
  
  public int getTextSize()
  {
    return this.zzbmq;
  }
  
  public List<Drawable> zzmj()
  {
    return this.zzbmp;
  }
  
  public int zzmk()
  {
    return this.zzbmr;
  }
  
  public int zzml()
  {
    return this.zzbms;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/formats/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */