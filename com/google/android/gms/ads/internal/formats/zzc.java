package com.google.android.gms.ads.internal.formats;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.RemoteException;
import com.google.android.gms.dynamic.zzd;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.internal.zzeg.zza;
import com.google.android.gms.internal.zzji;

@zzji
public class zzc
  extends zzeg.zza
{
  private final Uri mUri;
  private final Drawable zzbmw;
  private final double zzbmx;
  
  public zzc(Drawable paramDrawable, Uri paramUri, double paramDouble)
  {
    this.zzbmw = paramDrawable;
    this.mUri = paramUri;
    this.zzbmx = paramDouble;
  }
  
  public double getScale()
  {
    return this.zzbmx;
  }
  
  public Uri getUri()
    throws RemoteException
  {
    return this.mUri;
  }
  
  public zzd zzmn()
    throws RemoteException
  {
    return zze.zzac(this.zzbmw);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/formats/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */