package com.google.android.gms.ads.internal.formats;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import com.google.android.gms.ads.internal.client.zzab;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.internal.zzeg;
import com.google.android.gms.internal.zzek.zza;
import com.google.android.gms.internal.zzji;
import java.util.List;

@zzji
public class zzd
  extends zzek.zza
  implements zzi.zza
{
  private Bundle mExtras;
  private Object zzako = new Object();
  private String zzbmy;
  private List<zzc> zzbmz;
  private String zzbna;
  private zzeg zzbnb;
  private String zzbnc;
  private double zzbnd;
  private String zzbne;
  private String zzbnf;
  @Nullable
  private zza zzbng;
  @Nullable
  private zzab zzbnh;
  @Nullable
  private View zzbni;
  private zzi zzbnj;
  
  public zzd(String paramString1, List paramList, String paramString2, zzeg paramzzeg, String paramString3, double paramDouble, String paramString4, String paramString5, @Nullable zza paramzza, Bundle paramBundle, zzab paramzzab, View paramView)
  {
    this.zzbmy = paramString1;
    this.zzbmz = paramList;
    this.zzbna = paramString2;
    this.zzbnb = paramzzeg;
    this.zzbnc = paramString3;
    this.zzbnd = paramDouble;
    this.zzbne = paramString4;
    this.zzbnf = paramString5;
    this.zzbng = paramzza;
    this.mExtras = paramBundle;
    this.zzbnh = paramzzab;
    this.zzbni = paramView;
  }
  
  public void destroy()
  {
    this.zzbmy = null;
    this.zzbmz = null;
    this.zzbna = null;
    this.zzbnb = null;
    this.zzbnc = null;
    this.zzbnd = 0.0D;
    this.zzbne = null;
    this.zzbnf = null;
    this.zzbng = null;
    this.mExtras = null;
    this.zzako = null;
    this.zzbnj = null;
    this.zzbnh = null;
    this.zzbni = null;
  }
  
  public String getBody()
  {
    return this.zzbna;
  }
  
  public String getCallToAction()
  {
    return this.zzbnc;
  }
  
  public String getCustomTemplateId()
  {
    return "";
  }
  
  public Bundle getExtras()
  {
    return this.mExtras;
  }
  
  public String getHeadline()
  {
    return this.zzbmy;
  }
  
  public List getImages()
  {
    return this.zzbmz;
  }
  
  public String getPrice()
  {
    return this.zzbnf;
  }
  
  public double getStarRating()
  {
    return this.zzbnd;
  }
  
  public String getStore()
  {
    return this.zzbne;
  }
  
  public void zzb(zzi paramzzi)
  {
    synchronized (this.zzako)
    {
      this.zzbnj = paramzzi;
      return;
    }
  }
  
  public zzab zzej()
  {
    return this.zzbnh;
  }
  
  public zzeg zzmo()
  {
    return this.zzbnb;
  }
  
  public com.google.android.gms.dynamic.zzd zzmp()
  {
    return zze.zzac(this.zzbnj);
  }
  
  public String zzmq()
  {
    return "2";
  }
  
  public zza zzmr()
  {
    return this.zzbng;
  }
  
  public View zzms()
  {
    return this.zzbni;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/formats/zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */