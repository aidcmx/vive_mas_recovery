package com.google.android.gms.ads.internal.formats;

import android.os.Bundle;
import android.support.annotation.Nullable;
import com.google.android.gms.dynamic.zzd;
import com.google.android.gms.internal.zzeg;
import com.google.android.gms.internal.zzem.zza;
import com.google.android.gms.internal.zzji;
import java.util.List;

@zzji
public class zze
  extends zzem.zza
  implements zzi.zza
{
  private Bundle mExtras;
  private Object zzako = new Object();
  private String zzbmy;
  private List<zzc> zzbmz;
  private String zzbna;
  private String zzbnc;
  @Nullable
  private zza zzbng;
  private zzi zzbnj;
  private zzeg zzbnk;
  private String zzbnl;
  
  public zze(String paramString1, List paramList, String paramString2, zzeg paramzzeg, String paramString3, String paramString4, @Nullable zza paramzza, Bundle paramBundle)
  {
    this.zzbmy = paramString1;
    this.zzbmz = paramList;
    this.zzbna = paramString2;
    this.zzbnk = paramzzeg;
    this.zzbnc = paramString3;
    this.zzbnl = paramString4;
    this.zzbng = paramzza;
    this.mExtras = paramBundle;
  }
  
  public void destroy()
  {
    this.zzbmy = null;
    this.zzbmz = null;
    this.zzbna = null;
    this.zzbnk = null;
    this.zzbnc = null;
    this.zzbnl = null;
    this.zzbng = null;
    this.mExtras = null;
    this.zzako = null;
    this.zzbnj = null;
  }
  
  public String getAdvertiser()
  {
    return this.zzbnl;
  }
  
  public String getBody()
  {
    return this.zzbna;
  }
  
  public String getCallToAction()
  {
    return this.zzbnc;
  }
  
  public String getCustomTemplateId()
  {
    return "";
  }
  
  public Bundle getExtras()
  {
    return this.mExtras;
  }
  
  public String getHeadline()
  {
    return this.zzbmy;
  }
  
  public List getImages()
  {
    return this.zzbmz;
  }
  
  public void zzb(zzi paramzzi)
  {
    synchronized (this.zzako)
    {
      this.zzbnj = paramzzi;
      return;
    }
  }
  
  public zzd zzmp()
  {
    return com.google.android.gms.dynamic.zze.zzac(this.zzbnj);
  }
  
  public String zzmq()
  {
    return "1";
  }
  
  public zza zzmr()
  {
    return this.zzbng;
  }
  
  public zzeg zzmt()
  {
    return this.zzbnk;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/formats/zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */