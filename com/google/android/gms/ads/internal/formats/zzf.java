package com.google.android.gms.ads.internal.formats;

import android.support.v4.util.SimpleArrayMap;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.internal.zzeg;
import com.google.android.gms.internal.zzeo.zza;
import com.google.android.gms.internal.zzji;
import java.util.Arrays;
import java.util.List;

@zzji
public class zzf
  extends zzeo.zza
  implements zzi.zza
{
  private final Object zzako = new Object();
  private final zza zzbng;
  private zzi zzbnj;
  private final String zzbnm;
  private final SimpleArrayMap<String, zzc> zzbnn;
  private final SimpleArrayMap<String, String> zzbno;
  
  public zzf(String paramString, SimpleArrayMap<String, zzc> paramSimpleArrayMap, SimpleArrayMap<String, String> paramSimpleArrayMap1, zza paramzza)
  {
    this.zzbnm = paramString;
    this.zzbnn = paramSimpleArrayMap;
    this.zzbno = paramSimpleArrayMap1;
    this.zzbng = paramzza;
  }
  
  public List<String> getAvailableAssetNames()
  {
    int n = 0;
    String[] arrayOfString = new String[this.zzbnn.size() + this.zzbno.size()];
    int j = 0;
    int i = 0;
    int k;
    int m;
    for (;;)
    {
      k = n;
      m = i;
      if (j >= this.zzbnn.size()) {
        break;
      }
      arrayOfString[i] = ((String)this.zzbnn.keyAt(j));
      i += 1;
      j += 1;
    }
    while (k < this.zzbno.size())
    {
      arrayOfString[m] = ((String)this.zzbno.keyAt(k));
      k += 1;
      m += 1;
    }
    return Arrays.asList(arrayOfString);
  }
  
  public String getCustomTemplateId()
  {
    return this.zzbnm;
  }
  
  public void performClick(String paramString)
  {
    synchronized (this.zzako)
    {
      if (this.zzbnj == null)
      {
        zzb.e("Attempt to call performClick before ad initialized.");
        return;
      }
      this.zzbnj.zza(null, paramString, null, null, null);
      return;
    }
  }
  
  public void recordImpression()
  {
    synchronized (this.zzako)
    {
      if (this.zzbnj == null)
      {
        zzb.e("Attempt to perform recordImpression before ad initialized.");
        return;
      }
      this.zzbnj.zzb(null, null);
      return;
    }
  }
  
  public void zzb(zzi paramzzi)
  {
    synchronized (this.zzako)
    {
      this.zzbnj = paramzzi;
      return;
    }
  }
  
  public String zzba(String paramString)
  {
    return (String)this.zzbno.get(paramString);
  }
  
  public zzeg zzbb(String paramString)
  {
    return (zzeg)this.zzbnn.get(paramString);
  }
  
  public String zzmq()
  {
    return "3";
  }
  
  public zza zzmr()
  {
    return this.zzbng;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/formats/zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */