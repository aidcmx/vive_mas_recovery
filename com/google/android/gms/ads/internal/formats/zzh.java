package com.google.android.gms.ads.internal.formats;

import android.content.Context;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.internal.zzav;
import com.google.android.gms.internal.zzhd;
import com.google.android.gms.internal.zzhe;
import com.google.android.gms.internal.zzji;
import com.google.android.gms.internal.zzkx;
import com.google.android.gms.internal.zzmd;
import java.lang.ref.WeakReference;
import java.util.Map;
import org.json.JSONObject;

@zzji
public class zzh
  extends zzj
{
  private Object zzako = new Object();
  @Nullable
  private zzhd zzbnp;
  @Nullable
  private zzhe zzbnq;
  private final zzq zzbnr;
  @Nullable
  private zzi zzbns;
  private boolean zzbnt = false;
  
  private zzh(Context paramContext, zzq paramzzq, zzav paramzzav, zzi.zza paramzza)
  {
    super(paramContext, paramzzq, null, paramzzav, null, paramzza, null, null);
    this.zzbnr = paramzzq;
  }
  
  public zzh(Context paramContext, zzq paramzzq, zzav paramzzav, zzhd paramzzhd, zzi.zza paramzza)
  {
    this(paramContext, paramzzq, paramzzav, paramzza);
    this.zzbnp = paramzzhd;
  }
  
  public zzh(Context paramContext, zzq paramzzq, zzav paramzzav, zzhe paramzzhe, zzi.zza paramzza)
  {
    this(paramContext, paramzzq, paramzzav, paramzza);
    this.zzbnq = paramzzhe;
  }
  
  @Nullable
  public zzb zza(View.OnClickListener paramOnClickListener)
  {
    return null;
  }
  
  public void zza(View paramView, Map<String, WeakReference<View>> arg2, View.OnTouchListener paramOnTouchListener, View.OnClickListener paramOnClickListener)
  {
    synchronized (this.zzako)
    {
      this.zzbnt = true;
      try
      {
        if (this.zzbnp != null) {
          this.zzbnp.zzl(zze.zzac(paramView));
        }
        for (;;)
        {
          this.zzbnt = false;
          return;
          if (this.zzbnq != null) {
            this.zzbnq.zzl(zze.zzac(paramView));
          }
        }
      }
      catch (RemoteException paramView)
      {
        for (;;)
        {
          zzkx.zzc("Failed to call prepareAd", paramView);
        }
      }
    }
  }
  
  public void zza(View paramView1, Map<String, WeakReference<View>> paramMap, JSONObject paramJSONObject, View paramView2)
  {
    zzaa.zzhs("performClick must be called on the main UI thread.");
    synchronized (this.zzako)
    {
      if (this.zzbns != null)
      {
        this.zzbns.zza(paramView1, paramMap, paramJSONObject, paramView2);
        this.zzbnr.onAdClicked();
      }
      for (;;)
      {
        return;
        try
        {
          if ((this.zzbnp != null) && (!this.zzbnp.getOverrideClickHandling()))
          {
            this.zzbnp.zzk(zze.zzac(paramView1));
            this.zzbnr.onAdClicked();
          }
          if ((this.zzbnq == null) || (this.zzbnq.getOverrideClickHandling())) {
            continue;
          }
          this.zzbnq.zzk(zze.zzac(paramView1));
          this.zzbnr.onAdClicked();
        }
        catch (RemoteException paramView1)
        {
          zzkx.zzc("Failed to call performClick", paramView1);
        }
      }
    }
  }
  
  public void zzb(View paramView, Map<String, WeakReference<View>> paramMap)
  {
    zzaa.zzhs("recordImpression must be called on the main UI thread.");
    for (;;)
    {
      synchronized (this.zzako)
      {
        zzr(true);
        if (this.zzbns != null)
        {
          this.zzbns.zzb(paramView, paramMap);
          this.zzbnr.recordImpression();
          return;
        }
        try
        {
          if ((this.zzbnp != null) && (!this.zzbnp.getOverrideImpressionRecording()))
          {
            this.zzbnp.recordImpression();
            this.zzbnr.recordImpression();
          }
        }
        catch (RemoteException paramView)
        {
          zzkx.zzc("Failed to call recordImpression", paramView);
        }
      }
      if ((this.zzbnq != null) && (!this.zzbnq.getOverrideImpressionRecording()))
      {
        this.zzbnq.recordImpression();
        this.zzbnr.recordImpression();
      }
    }
  }
  
  public void zzc(View paramView, Map<String, WeakReference<View>> arg2)
  {
    synchronized (this.zzako)
    {
      try
      {
        if (this.zzbnp != null) {
          this.zzbnp.zzm(zze.zzac(paramView));
        }
        for (;;)
        {
          return;
          if (this.zzbnq != null) {
            this.zzbnq.zzm(zze.zzac(paramView));
          }
        }
      }
      catch (RemoteException paramView)
      {
        for (;;)
        {
          zzkx.zzc("Failed to call untrackView", paramView);
        }
      }
    }
  }
  
  public void zzc(@Nullable zzi paramzzi)
  {
    synchronized (this.zzako)
    {
      this.zzbns = paramzzi;
      return;
    }
  }
  
  public boolean zzmv()
  {
    synchronized (this.zzako)
    {
      boolean bool = this.zzbnt;
      return bool;
    }
  }
  
  public zzi zzmw()
  {
    synchronized (this.zzako)
    {
      zzi localzzi = this.zzbns;
      return localzzi;
    }
  }
  
  @Nullable
  public zzmd zzmx()
  {
    return null;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/formats/zzh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */