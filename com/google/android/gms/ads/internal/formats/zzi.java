package com.google.android.gms.ads.internal.formats;

import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import java.lang.ref.WeakReference;
import java.util.Map;
import org.json.JSONObject;

public abstract interface zzi
{
  public abstract Context getContext();
  
  public abstract void zza(View paramView1, String paramString, JSONObject paramJSONObject, Map<String, WeakReference<View>> paramMap, View paramView2);
  
  public abstract void zza(View paramView1, Map<String, WeakReference<View>> paramMap, JSONObject paramJSONObject, View paramView2);
  
  public abstract void zzb(View paramView, Map<String, WeakReference<View>> paramMap);
  
  public abstract void zzc(View paramView, Map<String, WeakReference<View>> paramMap);
  
  public abstract void zzd(MotionEvent paramMotionEvent);
  
  public abstract void zzd(View paramView, Map<String, WeakReference<View>> paramMap);
  
  public abstract void zzj(View paramView);
  
  public abstract View zzmy();
  
  public static abstract interface zza
  {
    public abstract String getCustomTemplateId();
    
    public abstract void zzb(zzi paramzzi);
    
    public abstract String zzmq();
    
    public abstract zza zzmr();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/formats/zzi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */