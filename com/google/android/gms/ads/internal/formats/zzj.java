package com.google.android.gms.ads.internal.formats;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.client.zzm;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.internal.zzaq;
import com.google.android.gms.internal.zzav;
import com.google.android.gms.internal.zzdn;
import com.google.android.gms.internal.zzdr;
import com.google.android.gms.internal.zzeg;
import com.google.android.gms.internal.zzeg.zza;
import com.google.android.gms.internal.zzfe;
import com.google.android.gms.internal.zzgi;
import com.google.android.gms.internal.zzja;
import com.google.android.gms.internal.zzja.zza;
import com.google.android.gms.internal.zzji;
import com.google.android.gms.internal.zzkx;
import com.google.android.gms.internal.zzmd;
import com.google.android.gms.internal.zzme;
import com.google.android.gms.internal.zzme.zza;
import com.google.android.gms.internal.zzmf;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

@zzji
public class zzj
  implements zzi
{
  private final Context mContext;
  private final Object zzako = new Object();
  @Nullable
  private final VersionInfoParcel zzanu;
  private final zzq zzbnr;
  @Nullable
  private final JSONObject zzbnu;
  @Nullable
  private final zzja zzbnv;
  @Nullable
  private final zzi.zza zzbnw;
  private final zzav zzbnx;
  boolean zzbny;
  private zzmd zzbnz;
  private String zzboa;
  @Nullable
  private String zzbob;
  private WeakReference<View> zzboc = null;
  
  public zzj(Context paramContext, zzq paramzzq, @Nullable zzja paramzzja, zzav paramzzav, @Nullable JSONObject paramJSONObject, @Nullable zzi.zza paramzza, @Nullable VersionInfoParcel paramVersionInfoParcel, @Nullable String paramString)
  {
    this.mContext = paramContext;
    this.zzbnr = paramzzq;
    this.zzbnv = paramzzja;
    this.zzbnx = paramzzav;
    this.zzbnu = paramJSONObject;
    this.zzbnw = paramzza;
    this.zzanu = paramVersionInfoParcel;
    this.zzbob = paramString;
  }
  
  private JSONObject zza(Map<String, WeakReference<View>> paramMap, View paramView)
  {
    localJSONObject1 = new JSONObject();
    if ((paramMap == null) || (paramView == null)) {
      return localJSONObject1;
    }
    try
    {
      paramView = zzk(paramView);
      paramMap = paramMap.entrySet().iterator();
      while (paramMap.hasNext())
      {
        Map.Entry localEntry = (Map.Entry)paramMap.next();
        View localView = (View)((WeakReference)localEntry.getValue()).get();
        if (localView != null)
        {
          int[] arrayOfInt = zzk(localView);
          JSONObject localJSONObject2 = new JSONObject();
          localJSONObject2.put("width", zzab(zzl(localView)));
          localJSONObject2.put("height", zzab(zzm(localView)));
          localJSONObject2.put("x", zzab(arrayOfInt[0] - paramView[0]));
          localJSONObject2.put("y", zzab(arrayOfInt[1] - paramView[1]));
          localJSONObject1.put((String)localEntry.getKey(), localJSONObject2);
        }
      }
      return localJSONObject1;
    }
    catch (JSONException paramMap)
    {
      zzkx.zzdi("Unable to get all view rectangles");
    }
  }
  
  private JSONObject zzb(Rect paramRect)
    throws JSONException
  {
    JSONObject localJSONObject = new JSONObject();
    localJSONObject.put("x", zzab(paramRect.left));
    localJSONObject.put("y", zzab(paramRect.top));
    localJSONObject.put("width", zzab(paramRect.right - paramRect.left));
    localJSONObject.put("height", zzab(paramRect.bottom - paramRect.top));
    localJSONObject.put("relative_to", "self");
    return localJSONObject;
  }
  
  private JSONObject zzb(Map<String, WeakReference<View>> paramMap, View paramView)
  {
    JSONObject localJSONObject1 = new JSONObject();
    if ((paramMap == null) || (paramView == null)) {
      return localJSONObject1;
    }
    paramView = zzk(paramView);
    Iterator localIterator = paramMap.entrySet().iterator();
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      View localView = (View)((WeakReference)localEntry.getValue()).get();
      if (localView != null)
      {
        int[] arrayOfInt = zzk(localView);
        JSONObject localJSONObject2 = new JSONObject();
        paramMap = new JSONObject();
        for (;;)
        {
          try
          {
            paramMap.put("width", zzab(zzl(localView)));
            paramMap.put("height", zzab(zzm(localView)));
            paramMap.put("x", zzab(arrayOfInt[0] - paramView[0]));
            paramMap.put("y", zzab(arrayOfInt[1] - paramView[1]));
            paramMap.put("relative_to", "ad_view");
            localJSONObject2.put("frame", paramMap);
            paramMap = new Rect();
            if (!localView.getLocalVisibleRect(paramMap)) {
              break label309;
            }
            paramMap = zzb(paramMap);
            localJSONObject2.put("visible_bounds", paramMap);
            if ((localView instanceof TextView))
            {
              paramMap = (TextView)localView;
              localJSONObject2.put("text_color", paramMap.getCurrentTextColor());
              localJSONObject2.put("font_size", paramMap.getTextSize());
              localJSONObject2.put("text", paramMap.getText());
            }
            localJSONObject1.put((String)localEntry.getKey(), localJSONObject2);
          }
          catch (JSONException paramMap)
          {
            zzkx.zzdi("Unable to get asset views information");
          }
          break;
          label309:
          paramMap = new JSONObject();
          paramMap.put("x", zzab(arrayOfInt[0] - paramView[0]));
          paramMap.put("y", zzab(arrayOfInt[1] - paramView[1]));
          paramMap.put("width", 0);
          paramMap.put("height", 0);
          paramMap.put("relative_to", "ad_view");
        }
      }
    }
    return localJSONObject1;
  }
  
  private JSONObject zzn(View paramView)
  {
    JSONObject localJSONObject = new JSONObject();
    if (paramView == null) {
      return localJSONObject;
    }
    try
    {
      localJSONObject.put("width", zzab(zzl(paramView)));
      localJSONObject.put("height", zzab(zzm(paramView)));
      return localJSONObject;
    }
    catch (Exception paramView)
    {
      zzkx.zzdi("Unable to get native ad view bounding box");
    }
    return localJSONObject;
  }
  
  private JSONObject zzo(View paramView)
  {
    JSONObject localJSONObject = new JSONObject();
    if (paramView == null) {
      return localJSONObject;
    }
    for (;;)
    {
      int[] arrayOfInt;
      try
      {
        arrayOfInt = zzk(paramView);
        Object localObject = new JSONObject();
        ((JSONObject)localObject).put("width", zzab(zzl(paramView)));
        ((JSONObject)localObject).put("height", zzab(zzm(paramView)));
        ((JSONObject)localObject).put("x", zzab(arrayOfInt[0]));
        ((JSONObject)localObject).put("y", zzab(arrayOfInt[1]));
        ((JSONObject)localObject).put("relative_to", "window");
        localJSONObject.put("frame", localObject);
        localObject = new Rect();
        if (paramView.getGlobalVisibleRect((Rect)localObject))
        {
          paramView = zzb((Rect)localObject);
          localJSONObject.put("visible_bounds", paramView);
          return localJSONObject;
        }
      }
      catch (Exception paramView)
      {
        zzkx.zzdi("Unable to get native ad view bounding box");
        return localJSONObject;
      }
      paramView = new JSONObject();
      paramView.put("x", zzab(arrayOfInt[0]));
      paramView.put("y", zzab(arrayOfInt[1]));
      paramView.put("width", 0);
      paramView.put("height", 0);
      paramView.put("relative_to", "window");
    }
  }
  
  public Context getContext()
  {
    return this.mContext;
  }
  
  public zzb zza(View.OnClickListener paramOnClickListener)
  {
    Object localObject = this.zzbnw.zzmr();
    if (localObject == null) {
      return null;
    }
    localObject = new zzb(this.mContext, (zza)localObject);
    ((zzb)localObject).setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
    ((zzb)localObject).zzmm().setOnClickListener(paramOnClickListener);
    ((zzb)localObject).zzmm().setContentDescription((CharSequence)zzdr.zzbjg.get());
    return (zzb)localObject;
  }
  
  public void zza(View paramView, zzg paramzzg)
  {
    if (!(this.zzbnw instanceof zzd)) {}
    for (;;)
    {
      return;
      Object localObject = (zzd)this.zzbnw;
      FrameLayout.LayoutParams localLayoutParams = new FrameLayout.LayoutParams(-1, -1);
      if (((zzd)localObject).zzms() != null)
      {
        ((FrameLayout)paramView).addView(((zzd)localObject).zzms(), localLayoutParams);
        this.zzbnr.zza(paramzzg);
        return;
      }
      if ((((zzd)localObject).getImages() != null) && (((zzd)localObject).getImages().size() > 0))
      {
        paramzzg = zze(((zzd)localObject).getImages().get(0));
        if (paramzzg != null) {
          try
          {
            paramzzg = paramzzg.zzmn();
            if (paramzzg != null)
            {
              paramzzg = (Drawable)zze.zzae(paramzzg);
              localObject = zznb();
              ((ImageView)localObject).setImageDrawable(paramzzg);
              ((ImageView)localObject).setScaleType(ImageView.ScaleType.CENTER_INSIDE);
              ((FrameLayout)paramView).addView((View)localObject, localLayoutParams);
              return;
            }
          }
          catch (RemoteException paramView)
          {
            zzkx.zzdi("Could not get drawable from image");
          }
        }
      }
    }
  }
  
  public void zza(View paramView1, String paramString, @Nullable JSONObject paramJSONObject, Map<String, WeakReference<View>> paramMap, View paramView2)
  {
    zzaa.zzhs("performClick must be called on the main UI thread.");
    for (;;)
    {
      try
      {
        localJSONObject1 = new JSONObject();
        localJSONObject1.put("asset", paramString);
        localJSONObject1.put("template", this.zzbnw.zzmq());
        localJSONObject2 = new JSONObject();
        localJSONObject2.put("ad", this.zzbnu);
        localJSONObject2.put("click", localJSONObject1);
        if (this.zzbnr.zzaa(this.zzbnw.getCustomTemplateId()) == null) {
          break label331;
        }
        bool = true;
        localJSONObject2.put("has_custom_click_handler", bool);
        if (((Boolean)zzdr.zzbji.get()).booleanValue())
        {
          if (((Boolean)zzdr.zzbjj.get()).booleanValue())
          {
            localJSONObject2.put("asset_view_signal", zzb(paramMap, paramView2));
            localJSONObject2.put("ad_view_signal", zzo(paramView2));
          }
        }
        else if (paramJSONObject != null) {
          localJSONObject2.put("click_point", paramJSONObject);
        }
      }
      catch (JSONException paramView1)
      {
        JSONObject localJSONObject1;
        final JSONObject localJSONObject2;
        zzkx.zzb("Unable to create click JSON.", paramView1);
        return;
      }
      try
      {
        paramJSONObject = this.zzbnu.optJSONObject("tracking_urls_and_actions");
        paramString = paramJSONObject;
        if (paramJSONObject == null) {
          paramString = new JSONObject();
        }
        paramString = paramString.optString("click_string");
        localJSONObject1.put("click_signals", this.zzbnx.zzaz().zza(this.mContext, paramString, paramView1));
      }
      catch (Exception paramView1)
      {
        zzkx.zzb("Exception obtaining click signals", paramView1);
        continue;
      }
      localJSONObject2.put("ads_id", this.zzbob);
      this.zzbnv.zza(new zzja.zza()
      {
        public void zze(zzgi paramAnonymouszzgi)
        {
          paramAnonymouszzgi.zza("google.afma.nativeAds.handleClickGmsg", localJSONObject2);
        }
      });
      return;
      localJSONObject2.put("view_rectangles", zza(paramMap, paramView2));
      localJSONObject2.put("native_view_rectangle", zzn(paramView2));
      continue;
      label331:
      boolean bool = false;
    }
  }
  
  public void zza(View paramView, Map<String, WeakReference<View>> paramMap, View.OnTouchListener paramOnTouchListener, View.OnClickListener paramOnClickListener)
  {
    if (!((Boolean)zzdr.zzbjd.get()).booleanValue()) {}
    for (;;)
    {
      return;
      paramView.setOnTouchListener(paramOnTouchListener);
      paramView.setClickable(true);
      paramView.setOnClickListener(paramOnClickListener);
      paramView = paramMap.entrySet().iterator();
      while (paramView.hasNext())
      {
        paramMap = (View)((WeakReference)((Map.Entry)paramView.next()).getValue()).get();
        if (paramMap != null)
        {
          paramMap.setOnTouchListener(paramOnTouchListener);
          paramMap.setClickable(true);
          paramMap.setOnClickListener(paramOnClickListener);
        }
      }
    }
  }
  
  public void zza(View paramView1, Map<String, WeakReference<View>> paramMap, JSONObject paramJSONObject, View paramView2)
  {
    zzaa.zzhs("performClick must be called on the main UI thread.");
    Iterator localIterator = paramMap.entrySet().iterator();
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      if (paramView1.equals((View)((WeakReference)localEntry.getValue()).get())) {
        zza(paramView1, (String)localEntry.getKey(), paramJSONObject, paramMap, paramView2);
      }
    }
    do
    {
      return;
      if ("2".equals(this.zzbnw.zzmq()))
      {
        zza(paramView1, "2099", paramJSONObject, paramMap, paramView2);
        return;
      }
    } while (!"1".equals(this.zzbnw.zzmq()));
    zza(paramView1, "1099", paramJSONObject, paramMap, paramView2);
  }
  
  int zzab(int paramInt)
  {
    return zzm.zzkr().zzc(this.mContext, paramInt);
  }
  
  public void zzb(View paramView, Map<String, WeakReference<View>> paramMap)
  {
    zzaa.zzhs("recordImpression must be called on the main UI thread.");
    zzr(true);
    for (;;)
    {
      try
      {
        localJSONObject = new JSONObject();
        localJSONObject.put("ad", this.zzbnu);
        localJSONObject.put("ads_id", this.zzbob);
        if (((Boolean)zzdr.zzbji.get()).booleanValue())
        {
          if (!((Boolean)zzdr.zzbjj.get()).booleanValue()) {
            continue;
          }
          localJSONObject.put("asset_view_signal", zzb(paramMap, paramView));
          localJSONObject.put("ad_view_signal", zzo(paramView));
        }
        this.zzbnv.zza(new zzja.zza()
        {
          public void zze(zzgi paramAnonymouszzgi)
          {
            paramAnonymouszzgi.zza("google.afma.nativeAds.handleImpressionPing", localJSONObject);
          }
        });
      }
      catch (JSONException paramView)
      {
        final JSONObject localJSONObject;
        zzkx.zzb("Unable to create impression JSON.", paramView);
        continue;
      }
      this.zzbnr.zza(this);
      return;
      localJSONObject.put("view_rectangles", zza(paramMap, paramView));
      localJSONObject.put("native_view_rectangle", zzn(paramView));
    }
  }
  
  public void zzc(View paramView, Map<String, WeakReference<View>> paramMap)
  {
    if (((Boolean)zzdr.zzbjc.get()).booleanValue()) {}
    for (;;)
    {
      return;
      paramView.setOnTouchListener(null);
      paramView.setClickable(false);
      paramView.setOnClickListener(null);
      paramView = paramMap.entrySet().iterator();
      while (paramView.hasNext())
      {
        paramMap = (View)((WeakReference)((Map.Entry)paramView.next()).getValue()).get();
        if (paramMap != null)
        {
          paramMap.setOnTouchListener(null);
          paramMap.setClickable(false);
          paramMap.setOnClickListener(null);
        }
      }
    }
  }
  
  public void zzd(MotionEvent paramMotionEvent)
  {
    this.zzbnx.zza(paramMotionEvent);
  }
  
  public void zzd(View paramView, Map<String, WeakReference<View>> paramMap)
  {
    synchronized (this.zzako)
    {
      if (this.zzbny) {
        return;
      }
      if (!paramView.isShown()) {
        return;
      }
    }
    if (!paramView.getGlobalVisibleRect(new Rect(), null)) {
      return;
    }
    zzb(paramView, paramMap);
  }
  
  @Nullable
  zzeg zze(Object paramObject)
  {
    if ((paramObject instanceof IBinder)) {
      return zzeg.zza.zzab((IBinder)paramObject);
    }
    return null;
  }
  
  public void zzj(View paramView)
  {
    this.zzboc = new WeakReference(paramView);
  }
  
  int[] zzk(View paramView)
  {
    int[] arrayOfInt = new int[2];
    paramView.getLocationOnScreen(arrayOfInt);
    return arrayOfInt;
  }
  
  int zzl(View paramView)
  {
    return paramView.getMeasuredWidth();
  }
  
  int zzm(View paramView)
  {
    return paramView.getMeasuredHeight();
  }
  
  public zzmd zzmx()
  {
    this.zzbnz = zzna();
    this.zzbnz.getView().setVisibility(8);
    zzja.zza local3 = new zzja.zza()
    {
      public void zze(final zzgi paramAnonymouszzgi)
      {
        paramAnonymouszzgi.zza("/loadHtml", new zzfe()
        {
          public void zza(zzmd paramAnonymous2zzmd, final Map<String, String> paramAnonymous2Map)
          {
            zzj.zzb(zzj.this).zzxc().zza(new zzme.zza()
            {
              public void zza(zzmd paramAnonymous3zzmd, boolean paramAnonymous3Boolean)
              {
                zzj.zza(zzj.this, (String)paramAnonymous2Map.get("id"));
                paramAnonymous3zzmd = new JSONObject();
                try
                {
                  paramAnonymous3zzmd.put("messageType", "htmlLoaded");
                  paramAnonymous3zzmd.put("id", zzj.zza(zzj.this));
                  zzj.3.1.this.zzbof.zzb("sendMessageToNativeJs", paramAnonymous3zzmd);
                  return;
                }
                catch (JSONException paramAnonymous3zzmd)
                {
                  zzkx.zzb("Unable to dispatch sendMessageToNativeJs event", paramAnonymous3zzmd);
                }
              }
            });
            paramAnonymous2zzmd = (String)paramAnonymous2Map.get("overlayHtml");
            paramAnonymous2Map = (String)paramAnonymous2Map.get("baseUrl");
            if (TextUtils.isEmpty(paramAnonymous2Map))
            {
              zzj.zzb(zzj.this).loadData(paramAnonymous2zzmd, "text/html", "UTF-8");
              return;
            }
            zzj.zzb(zzj.this).loadDataWithBaseURL(paramAnonymous2Map, paramAnonymous2zzmd, "text/html", "UTF-8", null);
          }
        });
        paramAnonymouszzgi.zza("/showOverlay", new zzfe()
        {
          public void zza(zzmd paramAnonymous2zzmd, Map<String, String> paramAnonymous2Map)
          {
            zzj.zzb(zzj.this).getView().setVisibility(0);
          }
        });
        paramAnonymouszzgi.zza("/hideOverlay", new zzfe()
        {
          public void zza(zzmd paramAnonymous2zzmd, Map<String, String> paramAnonymous2Map)
          {
            zzj.zzb(zzj.this).getView().setVisibility(8);
          }
        });
        zzj.zzb(zzj.this).zzxc().zza("/hideOverlay", new zzfe()
        {
          public void zza(zzmd paramAnonymous2zzmd, Map<String, String> paramAnonymous2Map)
          {
            zzj.zzb(zzj.this).getView().setVisibility(8);
          }
        });
        zzj.zzb(zzj.this).zzxc().zza("/sendMessageToSdk", new zzfe()
        {
          public void zza(zzmd paramAnonymous2zzmd, Map<String, String> paramAnonymous2Map)
          {
            paramAnonymous2zzmd = new JSONObject();
            try
            {
              Iterator localIterator = paramAnonymous2Map.keySet().iterator();
              while (localIterator.hasNext())
              {
                String str = (String)localIterator.next();
                paramAnonymous2zzmd.put(str, paramAnonymous2Map.get(str));
              }
              paramAnonymous2zzmd.put("id", zzj.zza(zzj.this));
            }
            catch (JSONException paramAnonymous2zzmd)
            {
              zzkx.zzb("Unable to dispatch sendMessageToNativeJs event", paramAnonymous2zzmd);
              return;
            }
            paramAnonymouszzgi.zzb("sendMessageToNativeJs", paramAnonymous2zzmd);
          }
        });
      }
    };
    this.zzbnv.zza(local3);
    return this.zzbnz;
  }
  
  public View zzmy()
  {
    if (this.zzboc != null) {
      return (View)this.zzboc.get();
    }
    return null;
  }
  
  public void zzmz()
  {
    if (!(this.zzbnw instanceof zzd)) {
      return;
    }
    this.zzbnr.zzfw();
  }
  
  zzmd zzna()
  {
    return zzu.zzgn().zza(this.mContext, AdSizeParcel.zzj(this.mContext), false, false, this.zzbnx, this.zzanu);
  }
  
  ImageView zznb()
  {
    return new ImageView(this.mContext);
  }
  
  protected void zzr(boolean paramBoolean)
  {
    this.zzbny = paramBoolean;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/formats/zzj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */