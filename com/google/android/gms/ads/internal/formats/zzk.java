package com.google.android.gms.ads.internal.formats;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.ads.internal.client.VideoOptionsParcel;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzk
  implements Parcelable.Creator<NativeAdOptionsParcel>
{
  static void zza(NativeAdOptionsParcel paramNativeAdOptionsParcel, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramNativeAdOptionsParcel.versionCode);
    zzb.zza(paramParcel, 2, paramNativeAdOptionsParcel.zzboj);
    zzb.zzc(paramParcel, 3, paramNativeAdOptionsParcel.zzbok);
    zzb.zza(paramParcel, 4, paramNativeAdOptionsParcel.zzbol);
    zzb.zzc(paramParcel, 5, paramNativeAdOptionsParcel.zzbom);
    zzb.zza(paramParcel, 6, paramNativeAdOptionsParcel.zzbon, paramInt, false);
    zzb.zzaj(paramParcel, i);
  }
  
  public NativeAdOptionsParcel[] zzac(int paramInt)
  {
    return new NativeAdOptionsParcel[paramInt];
  }
  
  public NativeAdOptionsParcel zzi(Parcel paramParcel)
  {
    int i = 0;
    int m = zza.zzcr(paramParcel);
    VideoOptionsParcel localVideoOptionsParcel = null;
    boolean bool1 = false;
    int j = 0;
    boolean bool2 = false;
    int k = 0;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.zzcq(paramParcel);
      switch (zza.zzgu(n))
      {
      default: 
        zza.zzb(paramParcel, n);
        break;
      case 1: 
        k = zza.zzg(paramParcel, n);
        break;
      case 2: 
        bool2 = zza.zzc(paramParcel, n);
        break;
      case 3: 
        j = zza.zzg(paramParcel, n);
        break;
      case 4: 
        bool1 = zza.zzc(paramParcel, n);
        break;
      case 5: 
        i = zza.zzg(paramParcel, n);
        break;
      case 6: 
        localVideoOptionsParcel = (VideoOptionsParcel)zza.zza(paramParcel, n, VideoOptionsParcel.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza(37 + "Overread allowed size end=" + m, paramParcel);
    }
    return new NativeAdOptionsParcel(k, bool2, j, bool1, i, localVideoOptionsParcel);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/formats/zzk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */