package com.google.android.gms.ads.internal.formats;

import android.graphics.Point;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.ViewTreeObserver.OnScrollChangedListener;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import com.google.android.gms.ads.internal.client.zzm;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.dynamic.zzd;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.internal.zzdn;
import com.google.android.gms.internal.zzdr;
import com.google.android.gms.internal.zzei.zza;
import com.google.android.gms.internal.zzji;
import com.google.android.gms.internal.zzkx;
import com.google.android.gms.internal.zzlb;
import com.google.android.gms.internal.zzly;
import com.google.android.gms.internal.zzmd;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

@zzji
public class zzl
  extends zzei.zza
  implements View.OnClickListener, View.OnTouchListener, ViewTreeObserver.OnGlobalLayoutListener, ViewTreeObserver.OnScrollChangedListener
{
  private final Object zzako = new Object();
  @Nullable
  private FrameLayout zzald;
  @Nullable
  private zzi zzbnj;
  private final FrameLayout zzboo;
  private Map<String, WeakReference<View>> zzbop = new HashMap();
  @Nullable
  private zzb zzboq;
  boolean zzbor = false;
  int zzbos;
  int zzbot;
  
  public zzl(FrameLayout paramFrameLayout1, FrameLayout paramFrameLayout2)
  {
    this.zzboo = paramFrameLayout1;
    this.zzald = paramFrameLayout2;
    zzu.zzhk().zza(this.zzboo, this);
    zzu.zzhk().zza(this.zzboo, this);
    this.zzboo.setOnTouchListener(this);
    this.zzboo.setOnClickListener(this);
  }
  
  private void zzd(zzj paramzzj)
  {
    if (!this.zzbop.containsKey("2011"))
    {
      paramzzj.zzmz();
      return;
    }
    final View localView = (View)((WeakReference)this.zzbop.get("2011")).get();
    if (!(localView instanceof FrameLayout))
    {
      paramzzj.zzmz();
      return;
    }
    paramzzj.zza(localView, new zzg()
    {
      public void zzc(MotionEvent paramAnonymousMotionEvent)
      {
        zzl.this.onTouch(null, paramAnonymousMotionEvent);
      }
      
      public void zzmu()
      {
        zzl.this.onClick(localView);
      }
    });
  }
  
  public void destroy()
  {
    synchronized (this.zzako)
    {
      if (this.zzald != null) {
        this.zzald.removeAllViews();
      }
      this.zzald = null;
      this.zzbop = null;
      this.zzboq = null;
      this.zzbnj = null;
      return;
    }
  }
  
  int getMeasuredHeight()
  {
    return this.zzboo.getMeasuredHeight();
  }
  
  int getMeasuredWidth()
  {
    return this.zzboo.getMeasuredWidth();
  }
  
  public void onClick(View paramView)
  {
    JSONObject localJSONObject;
    synchronized (this.zzako)
    {
      if (this.zzbnj == null) {
        return;
      }
      localJSONObject = new JSONObject();
    }
    try
    {
      localJSONObject.put("x", zzab(this.zzbos));
      localJSONObject.put("y", zzab(this.zzbot));
      if ((this.zzboq != null) && (this.zzboq.zzmm().equals(paramView))) {
        if (((this.zzbnj instanceof zzh)) && (((zzh)this.zzbnj).zzmw() != null))
        {
          ((zzh)this.zzbnj).zzmw().zza(paramView, "1007", localJSONObject, this.zzbop, this.zzboo);
          return;
          paramView = finally;
          throw paramView;
        }
      }
    }
    catch (JSONException localJSONException)
    {
      for (;;)
      {
        zzkx.zzdi("Unable to get click location");
        continue;
        this.zzbnj.zza(paramView, "1007", localJSONObject, this.zzbop, this.zzboo);
        continue;
        this.zzbnj.zza(paramView, this.zzbop, localJSONObject, this.zzboo);
      }
    }
  }
  
  public void onGlobalLayout()
  {
    synchronized (this.zzako)
    {
      if (this.zzbor)
      {
        int i = getMeasuredWidth();
        int j = getMeasuredHeight();
        if ((i != 0) && (j != 0) && (this.zzald != null))
        {
          this.zzald.setLayoutParams(new FrameLayout.LayoutParams(i, j));
          this.zzbor = false;
        }
      }
      if (this.zzbnj != null) {
        this.zzbnj.zzd(this.zzboo, this.zzbop);
      }
      return;
    }
  }
  
  public void onScrollChanged()
  {
    synchronized (this.zzako)
    {
      if (this.zzbnj != null) {
        this.zzbnj.zzd(this.zzboo, this.zzbop);
      }
      return;
    }
  }
  
  public boolean onTouch(View arg1, MotionEvent paramMotionEvent)
  {
    synchronized (this.zzako)
    {
      if (this.zzbnj == null) {
        return false;
      }
      Point localPoint = zze(paramMotionEvent);
      this.zzbos = localPoint.x;
      this.zzbot = localPoint.y;
      paramMotionEvent = MotionEvent.obtain(paramMotionEvent);
      paramMotionEvent.setLocation(localPoint.x, localPoint.y);
      this.zzbnj.zzd(paramMotionEvent);
      paramMotionEvent.recycle();
      return false;
    }
  }
  
  int zzab(int paramInt)
  {
    return zzm.zzkr().zzc(this.zzbnj.getContext(), paramInt);
  }
  
  public zzd zzaw(String paramString)
  {
    synchronized (this.zzako)
    {
      paramString = (WeakReference)this.zzbop.get(paramString);
      if (paramString == null)
      {
        paramString = null;
        paramString = zze.zzac(paramString);
        return paramString;
      }
      paramString = (View)paramString.get();
    }
  }
  
  @Nullable
  zzb zzc(zzj paramzzj)
  {
    return paramzzj.zza(this);
  }
  
  public void zzc(String paramString, zzd paramzzd)
  {
    View localView = (View)zze.zzae(paramzzd);
    paramzzd = this.zzako;
    if (localView == null) {}
    for (;;)
    {
      try
      {
        this.zzbop.remove(paramString);
        return;
      }
      finally {}
      this.zzbop.put(paramString, new WeakReference(localView));
      localView.setOnTouchListener(this);
      localView.setClickable(true);
      localView.setOnClickListener(this);
    }
  }
  
  Point zze(MotionEvent paramMotionEvent)
  {
    int[] arrayOfInt = new int[2];
    this.zzboo.getLocationOnScreen(arrayOfInt);
    float f1 = paramMotionEvent.getRawX();
    float f2 = arrayOfInt[0];
    float f3 = paramMotionEvent.getRawY();
    float f4 = arrayOfInt[1];
    return new Point((int)(f1 - f2), (int)(f3 - f4));
  }
  
  public void zze(final zzd paramzzd)
  {
    for (;;)
    {
      synchronized (this.zzako)
      {
        zzj(null);
        paramzzd = zze.zzae(paramzzd);
        if (!(paramzzd instanceof zzj))
        {
          zzkx.zzdi("Not an instance of native engine. This is most likely a transient error");
          return;
        }
        if (this.zzald != null)
        {
          this.zzald.setLayoutParams(new FrameLayout.LayoutParams(0, 0));
          this.zzboo.requestLayout();
        }
        this.zzbor = true;
        paramzzd = (zzj)paramzzd;
        if ((this.zzbnj != null) && (((Boolean)zzdr.zzbjb.get()).booleanValue())) {
          this.zzbnj.zzc(this.zzboo, this.zzbop);
        }
        if (((this.zzbnj instanceof zzh)) && (((zzh)this.zzbnj).zzmv()))
        {
          ((zzh)this.zzbnj).zzc(paramzzd);
          if (((Boolean)zzdr.zzbjb.get()).booleanValue()) {
            this.zzald.setClickable(false);
          }
          this.zzald.removeAllViews();
          this.zzboq = zzc(paramzzd);
          if (this.zzboq != null)
          {
            this.zzbop.put("1007", new WeakReference(this.zzboq.zzmm()));
            this.zzald.addView(this.zzboq);
          }
          paramzzd.zza(this.zzboo, this.zzbop, this, this);
          zzlb.zzcvl.post(new Runnable()
          {
            public void run()
            {
              zzmd localzzmd = paramzzd.zzmx();
              if ((localzzmd != null) && (zzl.zza(zzl.this) != null)) {
                zzl.zza(zzl.this).addView(localzzmd.getView());
              }
              if (!(paramzzd instanceof zzh)) {
                zzl.zza(zzl.this, paramzzd);
              }
            }
          });
          zzj(this.zzboo);
          return;
        }
      }
      this.zzbnj = paramzzd;
      if ((paramzzd instanceof zzh)) {
        ((zzh)paramzzd).zzc(null);
      }
    }
  }
  
  void zzj(@Nullable View paramView)
  {
    if (this.zzbnj != null) {
      if (!(this.zzbnj instanceof zzh)) {
        break label40;
      }
    }
    label40:
    for (zzi localzzi = ((zzh)this.zzbnj).zzmw();; localzzi = this.zzbnj)
    {
      if (localzzi != null) {
        localzzi.zzj(paramView);
      }
      return;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/formats/zzl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */