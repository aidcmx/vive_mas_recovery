package com.google.android.gms.ads.internal.overlay;

import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.ads.internal.InterstitialAdParameterParcel;
import com.google.android.gms.ads.internal.client.zza;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.dynamic.zzd;
import com.google.android.gms.dynamic.zzd.zza;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.internal.zzfa;
import com.google.android.gms.internal.zzfg;
import com.google.android.gms.internal.zzji;
import com.google.android.gms.internal.zzmd;

@zzji
public final class AdOverlayInfoParcel
  extends AbstractSafeParcelable
  implements ReflectedParcelable
{
  public static final Parcelable.Creator<AdOverlayInfoParcel> CREATOR = new zzf();
  public final int orientation;
  public final String url;
  public final int versionCode;
  public final VersionInfoParcel zzari;
  public final AdLauncherIntentInfoParcel zzcbj;
  public final zza zzcbk;
  public final zzg zzcbl;
  public final zzmd zzcbm;
  public final zzfa zzcbn;
  public final String zzcbo;
  public final boolean zzcbp;
  public final String zzcbq;
  public final zzp zzcbr;
  public final int zzcbs;
  public final zzfg zzcbt;
  public final String zzcbu;
  public final InterstitialAdParameterParcel zzcbv;
  
  AdOverlayInfoParcel(int paramInt1, AdLauncherIntentInfoParcel paramAdLauncherIntentInfoParcel, IBinder paramIBinder1, IBinder paramIBinder2, IBinder paramIBinder3, IBinder paramIBinder4, String paramString1, boolean paramBoolean, String paramString2, IBinder paramIBinder5, int paramInt2, int paramInt3, String paramString3, VersionInfoParcel paramVersionInfoParcel, IBinder paramIBinder6, String paramString4, InterstitialAdParameterParcel paramInterstitialAdParameterParcel)
  {
    this.versionCode = paramInt1;
    this.zzcbj = paramAdLauncherIntentInfoParcel;
    this.zzcbk = ((zza)zze.zzae(zzd.zza.zzfd(paramIBinder1)));
    this.zzcbl = ((zzg)zze.zzae(zzd.zza.zzfd(paramIBinder2)));
    this.zzcbm = ((zzmd)zze.zzae(zzd.zza.zzfd(paramIBinder3)));
    this.zzcbn = ((zzfa)zze.zzae(zzd.zza.zzfd(paramIBinder4)));
    this.zzcbo = paramString1;
    this.zzcbp = paramBoolean;
    this.zzcbq = paramString2;
    this.zzcbr = ((zzp)zze.zzae(zzd.zza.zzfd(paramIBinder5)));
    this.orientation = paramInt2;
    this.zzcbs = paramInt3;
    this.url = paramString3;
    this.zzari = paramVersionInfoParcel;
    this.zzcbt = ((zzfg)zze.zzae(zzd.zza.zzfd(paramIBinder6)));
    this.zzcbu = paramString4;
    this.zzcbv = paramInterstitialAdParameterParcel;
  }
  
  public AdOverlayInfoParcel(zza paramzza, zzg paramzzg, zzp paramzzp, zzmd paramzzmd, int paramInt, VersionInfoParcel paramVersionInfoParcel, String paramString, InterstitialAdParameterParcel paramInterstitialAdParameterParcel)
  {
    this.versionCode = 4;
    this.zzcbj = null;
    this.zzcbk = paramzza;
    this.zzcbl = paramzzg;
    this.zzcbm = paramzzmd;
    this.zzcbn = null;
    this.zzcbo = null;
    this.zzcbp = false;
    this.zzcbq = null;
    this.zzcbr = paramzzp;
    this.orientation = paramInt;
    this.zzcbs = 1;
    this.url = null;
    this.zzari = paramVersionInfoParcel;
    this.zzcbt = null;
    this.zzcbu = paramString;
    this.zzcbv = paramInterstitialAdParameterParcel;
  }
  
  public AdOverlayInfoParcel(zza paramzza, zzg paramzzg, zzp paramzzp, zzmd paramzzmd, boolean paramBoolean, int paramInt, VersionInfoParcel paramVersionInfoParcel)
  {
    this.versionCode = 4;
    this.zzcbj = null;
    this.zzcbk = paramzza;
    this.zzcbl = paramzzg;
    this.zzcbm = paramzzmd;
    this.zzcbn = null;
    this.zzcbo = null;
    this.zzcbp = paramBoolean;
    this.zzcbq = null;
    this.zzcbr = paramzzp;
    this.orientation = paramInt;
    this.zzcbs = 2;
    this.url = null;
    this.zzari = paramVersionInfoParcel;
    this.zzcbt = null;
    this.zzcbu = null;
    this.zzcbv = null;
  }
  
  public AdOverlayInfoParcel(zza paramzza, zzg paramzzg, zzfa paramzzfa, zzp paramzzp, zzmd paramzzmd, boolean paramBoolean, int paramInt, String paramString, VersionInfoParcel paramVersionInfoParcel, zzfg paramzzfg)
  {
    this.versionCode = 4;
    this.zzcbj = null;
    this.zzcbk = paramzza;
    this.zzcbl = paramzzg;
    this.zzcbm = paramzzmd;
    this.zzcbn = paramzzfa;
    this.zzcbo = null;
    this.zzcbp = paramBoolean;
    this.zzcbq = null;
    this.zzcbr = paramzzp;
    this.orientation = paramInt;
    this.zzcbs = 3;
    this.url = paramString;
    this.zzari = paramVersionInfoParcel;
    this.zzcbt = paramzzfg;
    this.zzcbu = null;
    this.zzcbv = null;
  }
  
  public AdOverlayInfoParcel(zza paramzza, zzg paramzzg, zzfa paramzzfa, zzp paramzzp, zzmd paramzzmd, boolean paramBoolean, int paramInt, String paramString1, String paramString2, VersionInfoParcel paramVersionInfoParcel, zzfg paramzzfg)
  {
    this.versionCode = 4;
    this.zzcbj = null;
    this.zzcbk = paramzza;
    this.zzcbl = paramzzg;
    this.zzcbm = paramzzmd;
    this.zzcbn = paramzzfa;
    this.zzcbo = paramString2;
    this.zzcbp = paramBoolean;
    this.zzcbq = paramString1;
    this.zzcbr = paramzzp;
    this.orientation = paramInt;
    this.zzcbs = 3;
    this.url = null;
    this.zzari = paramVersionInfoParcel;
    this.zzcbt = paramzzfg;
    this.zzcbu = null;
    this.zzcbv = null;
  }
  
  public AdOverlayInfoParcel(AdLauncherIntentInfoParcel paramAdLauncherIntentInfoParcel, zza paramzza, zzg paramzzg, zzp paramzzp, VersionInfoParcel paramVersionInfoParcel)
  {
    this.versionCode = 4;
    this.zzcbj = paramAdLauncherIntentInfoParcel;
    this.zzcbk = paramzza;
    this.zzcbl = paramzzg;
    this.zzcbm = null;
    this.zzcbn = null;
    this.zzcbo = null;
    this.zzcbp = false;
    this.zzcbq = null;
    this.zzcbr = paramzzp;
    this.orientation = -1;
    this.zzcbs = 4;
    this.url = null;
    this.zzari = paramVersionInfoParcel;
    this.zzcbt = null;
    this.zzcbu = null;
    this.zzcbv = null;
  }
  
  public static void zza(Intent paramIntent, AdOverlayInfoParcel paramAdOverlayInfoParcel)
  {
    Bundle localBundle = new Bundle(1);
    localBundle.putParcelable("com.google.android.gms.ads.inernal.overlay.AdOverlayInfo", paramAdOverlayInfoParcel);
    paramIntent.putExtra("com.google.android.gms.ads.inernal.overlay.AdOverlayInfo", localBundle);
  }
  
  public static AdOverlayInfoParcel zzb(Intent paramIntent)
  {
    try
    {
      paramIntent = paramIntent.getBundleExtra("com.google.android.gms.ads.inernal.overlay.AdOverlayInfo");
      paramIntent.setClassLoader(AdOverlayInfoParcel.class.getClassLoader());
      paramIntent = (AdOverlayInfoParcel)paramIntent.getParcelable("com.google.android.gms.ads.inernal.overlay.AdOverlayInfo");
      return paramIntent;
    }
    catch (Exception paramIntent) {}
    return null;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzf.zza(this, paramParcel, paramInt);
  }
  
  IBinder zzpv()
  {
    return zze.zzac(this.zzcbk).asBinder();
  }
  
  IBinder zzpw()
  {
    return zze.zzac(this.zzcbl).asBinder();
  }
  
  IBinder zzpx()
  {
    return zze.zzac(this.zzcbm).asBinder();
  }
  
  IBinder zzpy()
  {
    return zze.zzac(this.zzcbn).asBinder();
  }
  
  IBinder zzpz()
  {
    return zze.zzac(this.zzcbt).asBinder();
  }
  
  IBinder zzqa()
  {
    return zze.zzac(this.zzcbr).asBinder();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/overlay/AdOverlayInfoParcel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */