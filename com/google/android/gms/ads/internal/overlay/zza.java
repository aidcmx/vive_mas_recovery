package com.google.android.gms.ads.internal.overlay;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.internal.zzji;
import com.google.android.gms.internal.zzkx;
import com.google.android.gms.internal.zzlb;

@zzji
public class zza
{
  public boolean zza(Context paramContext, Intent paramIntent, zzp paramzzp)
  {
    try
    {
      String str = String.valueOf(paramIntent.toURI());
      if (str.length() != 0) {}
      for (str = "Launching an intent: ".concat(str);; str = new String("Launching an intent: "))
      {
        zzkx.v(str);
        zzu.zzgm().zzb(paramContext, paramIntent);
        if (paramzzp == null) {
          break;
        }
        paramzzp.zzeh();
        break;
      }
      return true;
    }
    catch (ActivityNotFoundException paramContext)
    {
      zzkx.zzdi(paramContext.getMessage());
      return false;
    }
  }
  
  public boolean zza(Context paramContext, AdLauncherIntentInfoParcel paramAdLauncherIntentInfoParcel, zzp paramzzp)
  {
    if (paramAdLauncherIntentInfoParcel == null)
    {
      zzkx.zzdi("No intent data for launcher overlay.");
      return false;
    }
    if (paramAdLauncherIntentInfoParcel.intent != null) {
      return zza(paramContext, paramAdLauncherIntentInfoParcel.intent, paramzzp);
    }
    Intent localIntent = new Intent();
    if (TextUtils.isEmpty(paramAdLauncherIntentInfoParcel.url))
    {
      zzkx.zzdi("Open GMSG did not contain a URL.");
      return false;
    }
    String[] arrayOfString;
    if (!TextUtils.isEmpty(paramAdLauncherIntentInfoParcel.mimeType))
    {
      localIntent.setDataAndType(Uri.parse(paramAdLauncherIntentInfoParcel.url), paramAdLauncherIntentInfoParcel.mimeType);
      localIntent.setAction("android.intent.action.VIEW");
      if (!TextUtils.isEmpty(paramAdLauncherIntentInfoParcel.packageName)) {
        localIntent.setPackage(paramAdLauncherIntentInfoParcel.packageName);
      }
      if (TextUtils.isEmpty(paramAdLauncherIntentInfoParcel.zzbzm)) {
        break label210;
      }
      arrayOfString = paramAdLauncherIntentInfoParcel.zzbzm.split("/", 2);
      if (arrayOfString.length >= 2) {
        break label196;
      }
      paramContext = String.valueOf(paramAdLauncherIntentInfoParcel.zzbzm);
      if (paramContext.length() == 0) {
        break label183;
      }
    }
    label183:
    for (paramContext = "Could not parse component name from open GMSG: ".concat(paramContext);; paramContext = new String("Could not parse component name from open GMSG: "))
    {
      zzkx.zzdi(paramContext);
      return false;
      localIntent.setData(Uri.parse(paramAdLauncherIntentInfoParcel.url));
      break;
    }
    label196:
    localIntent.setClassName(arrayOfString[0], arrayOfString[1]);
    label210:
    paramAdLauncherIntentInfoParcel = paramAdLauncherIntentInfoParcel.zzbzn;
    if (!TextUtils.isEmpty(paramAdLauncherIntentInfoParcel)) {}
    try
    {
      i = Integer.parseInt(paramAdLauncherIntentInfoParcel);
      localIntent.addFlags(i);
      return zza(paramContext, localIntent, paramzzp);
    }
    catch (NumberFormatException paramAdLauncherIntentInfoParcel)
    {
      for (;;)
      {
        zzkx.zzdi("Could not parse intent flags.");
        int i = 0;
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/overlay/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */