package com.google.android.gms.ads.internal.overlay;

import android.annotation.TargetApi;
import android.content.Context;
import android.media.AudioManager;
import android.media.AudioManager.OnAudioFocusChangeListener;
import com.google.android.gms.internal.zzji;

@zzji
@TargetApi(14)
public class zzaa
  implements AudioManager.OnAudioFocusChangeListener
{
  private final AudioManager mAudioManager;
  private boolean zzccd;
  private final zza zzceq;
  private boolean zzcer;
  private boolean zzces;
  private float zzcet = 1.0F;
  
  public zzaa(Context paramContext, zza paramzza)
  {
    this.mAudioManager = ((AudioManager)paramContext.getSystemService("audio"));
    this.zzceq = paramzza;
  }
  
  private void zzri()
  {
    int i;
    if ((this.zzccd) && (!this.zzces) && (this.zzcet > 0.0F))
    {
      i = 1;
      if ((i == 0) || (this.zzcer)) {
        break label55;
      }
      zzrj();
      this.zzceq.zzpk();
    }
    label55:
    while ((i != 0) || (!this.zzcer))
    {
      return;
      i = 0;
      break;
    }
    zzrk();
    this.zzceq.zzpk();
  }
  
  private void zzrj()
  {
    boolean bool = true;
    if ((this.mAudioManager == null) || (this.zzcer)) {
      return;
    }
    if (this.mAudioManager.requestAudioFocus(this, 3, 2) == 1) {}
    for (;;)
    {
      this.zzcer = bool;
      return;
      bool = false;
    }
  }
  
  private void zzrk()
  {
    if ((this.mAudioManager == null) || (!this.zzcer)) {
      return;
    }
    if (this.mAudioManager.abandonAudioFocus(this) == 0) {}
    for (boolean bool = true;; bool = false)
    {
      this.zzcer = bool;
      return;
    }
  }
  
  public void onAudioFocusChange(int paramInt)
  {
    if (paramInt > 0) {}
    for (boolean bool = true;; bool = false)
    {
      this.zzcer = bool;
      this.zzceq.zzpk();
      return;
    }
  }
  
  public void setMuted(boolean paramBoolean)
  {
    this.zzces = paramBoolean;
    zzri();
  }
  
  public void zzb(float paramFloat)
  {
    this.zzcet = paramFloat;
    zzri();
  }
  
  public void zzre()
  {
    this.zzccd = true;
    zzri();
  }
  
  public void zzrf()
  {
    this.zzccd = false;
    zzri();
  }
  
  public float zzrh()
  {
    if (this.zzces) {}
    for (float f = 0.0F; this.zzcer; f = this.zzcet) {
      return f;
    }
    return 0.0F;
  }
  
  static abstract interface zza
  {
    public abstract void zzpk();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/overlay/zzaa.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */