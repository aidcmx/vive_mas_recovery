package com.google.android.gms.ads.internal.overlay;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnInfoListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnVideoSizeChangedListener;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Handler;
import android.view.Surface;
import android.view.TextureView.SurfaceTextureListener;
import android.view.View.MeasureSpec;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.common.util.zze;
import com.google.android.gms.internal.zzji;
import com.google.android.gms.internal.zzkx;
import com.google.android.gms.internal.zzlb;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@zzji
@TargetApi(14)
public class zzc
  extends zzi
  implements MediaPlayer.OnBufferingUpdateListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnInfoListener, MediaPlayer.OnPreparedListener, MediaPlayer.OnVideoSizeChangedListener, TextureView.SurfaceTextureListener
{
  private static final Map<Integer, String> zzbzp = new HashMap();
  private final zzy zzbzq;
  private final boolean zzbzr;
  private int zzbzs = 0;
  private int zzbzt = 0;
  private MediaPlayer zzbzu;
  private Uri zzbzv;
  private int zzbzw;
  private int zzbzx;
  private int zzbzy;
  private int zzbzz;
  private int zzcaa;
  private zzx zzcab;
  private boolean zzcac;
  private int zzcad;
  private zzh zzcae;
  
  static
  {
    if (Build.VERSION.SDK_INT >= 17)
    {
      zzbzp.put(Integer.valueOf(64532), "MEDIA_ERROR_IO");
      zzbzp.put(Integer.valueOf(64529), "MEDIA_ERROR_MALFORMED");
      zzbzp.put(Integer.valueOf(64526), "MEDIA_ERROR_UNSUPPORTED");
      zzbzp.put(Integer.valueOf(-110), "MEDIA_ERROR_TIMED_OUT");
      zzbzp.put(Integer.valueOf(3), "MEDIA_INFO_VIDEO_RENDERING_START");
    }
    zzbzp.put(Integer.valueOf(100), "MEDIA_ERROR_SERVER_DIED");
    zzbzp.put(Integer.valueOf(1), "MEDIA_ERROR_UNKNOWN");
    zzbzp.put(Integer.valueOf(1), "MEDIA_INFO_UNKNOWN");
    zzbzp.put(Integer.valueOf(700), "MEDIA_INFO_VIDEO_TRACK_LAGGING");
    zzbzp.put(Integer.valueOf(701), "MEDIA_INFO_BUFFERING_START");
    zzbzp.put(Integer.valueOf(702), "MEDIA_INFO_BUFFERING_END");
    zzbzp.put(Integer.valueOf(800), "MEDIA_INFO_BAD_INTERLEAVING");
    zzbzp.put(Integer.valueOf(801), "MEDIA_INFO_NOT_SEEKABLE");
    zzbzp.put(Integer.valueOf(802), "MEDIA_INFO_METADATA_UPDATE");
    if (Build.VERSION.SDK_INT >= 19)
    {
      zzbzp.put(Integer.valueOf(901), "MEDIA_INFO_UNSUPPORTED_SUBTITLE");
      zzbzp.put(Integer.valueOf(902), "MEDIA_INFO_SUBTITLE_TIMED_OUT");
    }
  }
  
  public zzc(Context paramContext, boolean paramBoolean1, boolean paramBoolean2, zzy paramzzy)
  {
    super(paramContext);
    setSurfaceTextureListener(this);
    this.zzbzq = paramzzy;
    this.zzcac = paramBoolean1;
    this.zzbzr = paramBoolean2;
    this.zzbzq.zza(this);
  }
  
  private void zza(float paramFloat)
  {
    if (this.zzbzu != null) {}
    try
    {
      this.zzbzu.setVolume(paramFloat, paramFloat);
      return;
    }
    catch (IllegalStateException localIllegalStateException) {}
    zzkx.zzdi("AdMediaPlayerView setMediaPlayerVolume() called before onPrepared().");
    return;
  }
  
  private void zzai(int paramInt)
  {
    if (paramInt == 3)
    {
      this.zzbzq.zzre();
      this.zzcbx.zzre();
    }
    for (;;)
    {
      this.zzbzs = paramInt;
      return;
      if (this.zzbzs == 3)
      {
        this.zzbzq.zzrf();
        this.zzcbx.zzrf();
      }
    }
  }
  
  private void zzaj(int paramInt)
  {
    this.zzbzt = paramInt;
  }
  
  private void zzph()
  {
    zzkx.v("AdMediaPlayerView init MediaPlayer");
    Object localObject1 = getSurfaceTexture();
    if ((this.zzbzv == null) || (localObject1 == null)) {
      return;
    }
    zzz(false);
    try
    {
      this.zzbzu = zzu.zzhd().zzqr();
      this.zzbzu.setOnBufferingUpdateListener(this);
      this.zzbzu.setOnCompletionListener(this);
      this.zzbzu.setOnErrorListener(this);
      this.zzbzu.setOnInfoListener(this);
      this.zzbzu.setOnPreparedListener(this);
      this.zzbzu.setOnVideoSizeChangedListener(this);
      this.zzbzy = 0;
      if (!this.zzcac) {
        break label276;
      }
      this.zzcab = new zzx(getContext());
      this.zzcab.zza((SurfaceTexture)localObject1, getWidth(), getHeight());
      this.zzcab.start();
      localObject2 = this.zzcab.zzqt();
      if (localObject2 == null) {
        break label264;
      }
      localObject1 = localObject2;
    }
    catch (IOException localIOException)
    {
      for (;;)
      {
        Object localObject2 = String.valueOf(this.zzbzv);
        zzkx.zzc(String.valueOf(localObject2).length() + 36 + "Failed to initialize MediaPlayer at " + (String)localObject2, localIOException);
        onError(this.zzbzu, 1, 0);
        return;
        this.zzcab.zzqs();
        this.zzcab = null;
      }
    }
    catch (IllegalStateException localIllegalStateException)
    {
      for (;;) {}
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      for (;;) {}
    }
    this.zzbzu.setDataSource(getContext(), this.zzbzv);
    localObject1 = zzu.zzhe().zza((SurfaceTexture)localObject1);
    this.zzbzu.setSurface((Surface)localObject1);
    this.zzbzu.setAudioStreamType(3);
    this.zzbzu.setScreenOnWhilePlaying(true);
    this.zzbzu.prepareAsync();
    zzai(1);
  }
  
  private void zzpi()
  {
    if (!this.zzbzr) {}
    while ((!zzpj()) || (this.zzbzu.getCurrentPosition() <= 0) || (this.zzbzt == 3)) {
      return;
    }
    zzkx.v("AdMediaPlayerView nudging MediaPlayer");
    zza(0.0F);
    this.zzbzu.start();
    int i = this.zzbzu.getCurrentPosition();
    long l = zzu.zzgs().currentTimeMillis();
    while ((zzpj()) && (this.zzbzu.getCurrentPosition() == i) && (zzu.zzgs().currentTimeMillis() - l <= 250L)) {}
    this.zzbzu.pause();
    zzpk();
  }
  
  private boolean zzpj()
  {
    return (this.zzbzu != null) && (this.zzbzs != -1) && (this.zzbzs != 0) && (this.zzbzs != 1);
  }
  
  private void zzz(boolean paramBoolean)
  {
    zzkx.v("AdMediaPlayerView release");
    if (this.zzcab != null)
    {
      this.zzcab.zzqs();
      this.zzcab = null;
    }
    if (this.zzbzu != null)
    {
      this.zzbzu.reset();
      this.zzbzu.release();
      this.zzbzu = null;
      zzai(0);
      if (paramBoolean)
      {
        this.zzbzt = 0;
        zzaj(0);
      }
    }
  }
  
  public int getCurrentPosition()
  {
    if (zzpj()) {
      return this.zzbzu.getCurrentPosition();
    }
    return 0;
  }
  
  public int getDuration()
  {
    if (zzpj()) {
      return this.zzbzu.getDuration();
    }
    return -1;
  }
  
  public int getVideoHeight()
  {
    if (this.zzbzu != null) {
      return this.zzbzu.getVideoHeight();
    }
    return 0;
  }
  
  public int getVideoWidth()
  {
    if (this.zzbzu != null) {
      return this.zzbzu.getVideoWidth();
    }
    return 0;
  }
  
  public void onBufferingUpdate(MediaPlayer paramMediaPlayer, int paramInt)
  {
    this.zzbzy = paramInt;
  }
  
  public void onCompletion(MediaPlayer paramMediaPlayer)
  {
    zzkx.v("AdMediaPlayerView completion");
    zzai(5);
    zzaj(5);
    zzlb.zzcvl.post(new Runnable()
    {
      public void run()
      {
        if (zzc.zza(zzc.this) != null) {
          zzc.zza(zzc.this).zzqe();
        }
      }
    });
  }
  
  public boolean onError(final MediaPlayer paramMediaPlayer, int paramInt1, int paramInt2)
  {
    paramMediaPlayer = (String)zzbzp.get(Integer.valueOf(paramInt1));
    final String str = (String)zzbzp.get(Integer.valueOf(paramInt2));
    zzkx.zzdi(String.valueOf(paramMediaPlayer).length() + 38 + String.valueOf(str).length() + "AdMediaPlayerView MediaPlayer error: " + paramMediaPlayer + ":" + str);
    zzai(-1);
    zzaj(-1);
    zzlb.zzcvl.post(new Runnable()
    {
      public void run()
      {
        if (zzc.zza(zzc.this) != null) {
          zzc.zza(zzc.this).zzk(paramMediaPlayer, str);
        }
      }
    });
    return true;
  }
  
  public boolean onInfo(MediaPlayer paramMediaPlayer, int paramInt1, int paramInt2)
  {
    paramMediaPlayer = (String)zzbzp.get(Integer.valueOf(paramInt1));
    String str = (String)zzbzp.get(Integer.valueOf(paramInt2));
    zzkx.v(String.valueOf(paramMediaPlayer).length() + 37 + String.valueOf(str).length() + "AdMediaPlayerView MediaPlayer info: " + paramMediaPlayer + ":" + str);
    return true;
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    int k = getDefaultSize(this.zzbzw, paramInt1);
    int m = getDefaultSize(this.zzbzx, paramInt2);
    int j = m;
    int i = k;
    int n;
    if (this.zzbzw > 0)
    {
      j = m;
      i = k;
      if (this.zzbzx > 0)
      {
        j = m;
        i = k;
        if (this.zzcab == null)
        {
          m = View.MeasureSpec.getMode(paramInt1);
          paramInt1 = View.MeasureSpec.getSize(paramInt1);
          n = View.MeasureSpec.getMode(paramInt2);
          paramInt2 = View.MeasureSpec.getSize(paramInt2);
          if ((m != 1073741824) || (n != 1073741824)) {
            break label242;
          }
          if (this.zzbzw * paramInt2 >= this.zzbzx * paramInt1) {
            break label209;
          }
          i = this.zzbzw * paramInt2 / this.zzbzx;
          j = paramInt2;
        }
      }
    }
    for (;;)
    {
      setMeasuredDimension(i, j);
      if (this.zzcab != null) {
        this.zzcab.zzi(i, j);
      }
      if (Build.VERSION.SDK_INT == 16)
      {
        if (((this.zzbzz > 0) && (this.zzbzz != i)) || ((this.zzcaa > 0) && (this.zzcaa != j))) {
          zzpi();
        }
        this.zzbzz = i;
        this.zzcaa = j;
      }
      return;
      label209:
      if (this.zzbzw * paramInt2 > this.zzbzx * paramInt1)
      {
        j = this.zzbzx * paramInt1 / this.zzbzw;
        i = paramInt1;
        continue;
        label242:
        if (m == 1073741824)
        {
          j = this.zzbzx * paramInt1 / this.zzbzw;
          if ((n == Integer.MIN_VALUE) && (j > paramInt2))
          {
            j = paramInt2;
            i = paramInt1;
          }
        }
        else
        {
          if (n == 1073741824)
          {
            k = this.zzbzw * paramInt2 / this.zzbzx;
            j = paramInt2;
            i = k;
            if (m != Integer.MIN_VALUE) {
              continue;
            }
            j = paramInt2;
            i = k;
            if (k <= paramInt1) {
              continue;
            }
            j = paramInt2;
            i = paramInt1;
            continue;
          }
          k = this.zzbzw;
          i = this.zzbzx;
          if ((n == Integer.MIN_VALUE) && (i > paramInt2)) {
            k = this.zzbzw * paramInt2 / this.zzbzx;
          }
          for (;;)
          {
            j = paramInt2;
            i = k;
            if (m != Integer.MIN_VALUE) {
              break;
            }
            j = paramInt2;
            i = k;
            if (k <= paramInt1) {
              break;
            }
            j = this.zzbzx * paramInt1 / this.zzbzw;
            i = paramInt1;
            break;
            paramInt2 = i;
          }
        }
        i = paramInt1;
      }
      else
      {
        j = paramInt2;
        i = paramInt1;
      }
    }
  }
  
  public void onPrepared(MediaPlayer paramMediaPlayer)
  {
    zzkx.v("AdMediaPlayerView prepared");
    zzai(2);
    this.zzbzq.zzqc();
    zzlb.zzcvl.post(new Runnable()
    {
      public void run()
      {
        if (zzc.zza(zzc.this) != null) {
          zzc.zza(zzc.this).zzqc();
        }
      }
    });
    this.zzbzw = paramMediaPlayer.getVideoWidth();
    this.zzbzx = paramMediaPlayer.getVideoHeight();
    if (this.zzcad != 0) {
      seekTo(this.zzcad);
    }
    zzpi();
    int i = this.zzbzw;
    int j = this.zzbzx;
    zzkx.zzdh(62 + "AdMediaPlayerView stream dimensions: " + i + " x " + j);
    if (this.zzbzt == 3) {
      play();
    }
    zzpk();
  }
  
  public void onSurfaceTextureAvailable(SurfaceTexture paramSurfaceTexture, int paramInt1, int paramInt2)
  {
    zzkx.v("AdMediaPlayerView surface created");
    zzph();
    zzlb.zzcvl.post(new Runnable()
    {
      public void run()
      {
        if (zzc.zza(zzc.this) != null) {
          zzc.zza(zzc.this).zzqb();
        }
      }
    });
  }
  
  public boolean onSurfaceTextureDestroyed(SurfaceTexture paramSurfaceTexture)
  {
    zzkx.v("AdMediaPlayerView surface destroyed");
    if ((this.zzbzu != null) && (this.zzcad == 0)) {
      this.zzcad = this.zzbzu.getCurrentPosition();
    }
    if (this.zzcab != null) {
      this.zzcab.zzqs();
    }
    zzlb.zzcvl.post(new Runnable()
    {
      public void run()
      {
        if (zzc.zza(zzc.this) != null)
        {
          zzc.zza(zzc.this).onPaused();
          zzc.zza(zzc.this).zzqf();
        }
      }
    });
    zzz(true);
    return true;
  }
  
  public void onSurfaceTextureSizeChanged(SurfaceTexture paramSurfaceTexture, final int paramInt1, final int paramInt2)
  {
    int j = 1;
    zzkx.v("AdMediaPlayerView surface changed");
    int i;
    if (this.zzbzt == 3)
    {
      i = 1;
      if ((this.zzbzw != paramInt1) || (this.zzbzx != paramInt2)) {
        break label112;
      }
    }
    for (;;)
    {
      if ((this.zzbzu != null) && (i != 0) && (j != 0))
      {
        if (this.zzcad != 0) {
          seekTo(this.zzcad);
        }
        play();
      }
      if (this.zzcab != null) {
        this.zzcab.zzi(paramInt1, paramInt2);
      }
      zzlb.zzcvl.post(new Runnable()
      {
        public void run()
        {
          if (zzc.zza(zzc.this) != null) {
            zzc.zza(zzc.this).zzf(paramInt1, paramInt2);
          }
        }
      });
      return;
      i = 0;
      break;
      label112:
      j = 0;
    }
  }
  
  public void onSurfaceTextureUpdated(SurfaceTexture paramSurfaceTexture)
  {
    this.zzbzq.zzb(this);
    this.zzcbw.zza(paramSurfaceTexture, this.zzcae);
  }
  
  public void onVideoSizeChanged(MediaPlayer paramMediaPlayer, int paramInt1, int paramInt2)
  {
    zzkx.v(57 + "AdMediaPlayerView size changed: " + paramInt1 + " x " + paramInt2);
    this.zzbzw = paramMediaPlayer.getVideoWidth();
    this.zzbzx = paramMediaPlayer.getVideoHeight();
    if ((this.zzbzw != 0) && (this.zzbzx != 0)) {
      requestLayout();
    }
  }
  
  public void pause()
  {
    zzkx.v("AdMediaPlayerView pause");
    if ((zzpj()) && (this.zzbzu.isPlaying()))
    {
      this.zzbzu.pause();
      zzai(4);
      zzlb.zzcvl.post(new Runnable()
      {
        public void run()
        {
          if (zzc.zza(zzc.this) != null) {
            zzc.zza(zzc.this).onPaused();
          }
        }
      });
    }
    zzaj(4);
  }
  
  public void play()
  {
    zzkx.v("AdMediaPlayerView play");
    if (zzpj())
    {
      this.zzbzu.start();
      zzai(3);
      this.zzcbw.zzqd();
      zzlb.zzcvl.post(new Runnable()
      {
        public void run()
        {
          if (zzc.zza(zzc.this) != null) {
            zzc.zza(zzc.this).zzqd();
          }
        }
      });
    }
    zzaj(3);
  }
  
  public void seekTo(int paramInt)
  {
    zzkx.v(34 + "AdMediaPlayerView seek " + paramInt);
    if (zzpj())
    {
      this.zzbzu.seekTo(paramInt);
      this.zzcad = 0;
      return;
    }
    this.zzcad = paramInt;
  }
  
  public void setVideoPath(String paramString)
  {
    setVideoURI(Uri.parse(paramString));
  }
  
  public void setVideoURI(Uri paramUri)
  {
    this.zzbzv = paramUri;
    this.zzcad = 0;
    zzph();
    requestLayout();
    invalidate();
  }
  
  public void stop()
  {
    zzkx.v("AdMediaPlayerView stop");
    if (this.zzbzu != null)
    {
      this.zzbzu.stop();
      this.zzbzu.release();
      this.zzbzu = null;
      zzai(0);
      zzaj(0);
    }
    this.zzbzq.onStop();
  }
  
  public String toString()
  {
    String str1 = String.valueOf(getClass().getName());
    String str2 = String.valueOf(Integer.toHexString(hashCode()));
    return String.valueOf(str1).length() + 1 + String.valueOf(str2).length() + str1 + "@" + str2;
  }
  
  public void zza(float paramFloat1, float paramFloat2)
  {
    if (this.zzcab != null) {
      this.zzcab.zzb(paramFloat1, paramFloat2);
    }
  }
  
  public void zza(zzh paramzzh)
  {
    this.zzcae = paramzzh;
  }
  
  public String zzpg()
  {
    if (this.zzcac) {}
    for (String str = " spherical";; str = "")
    {
      str = String.valueOf(str);
      if (str.length() == 0) {
        break;
      }
      return "MediaPlayer".concat(str);
    }
    return new String("MediaPlayer");
  }
  
  public void zzpk()
  {
    zza(this.zzcbx.zzrh());
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/overlay/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */