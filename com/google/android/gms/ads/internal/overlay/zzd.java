package com.google.android.gms.ads.internal.overlay;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewParent;
import android.view.Window;
import android.webkit.WebChromeClient.CustomViewCallback;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.google.android.gms.ads.internal.InterstitialAdParameterParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.internal.zzdn;
import com.google.android.gms.internal.zzdr;
import com.google.android.gms.internal.zzhy.zza;
import com.google.android.gms.internal.zzji;
import com.google.android.gms.internal.zzkw;
import com.google.android.gms.internal.zzkx;
import com.google.android.gms.internal.zzlb;
import com.google.android.gms.internal.zzlc;
import com.google.android.gms.internal.zzle;
import com.google.android.gms.internal.zzlk;
import com.google.android.gms.internal.zzmd;
import com.google.android.gms.internal.zzme;
import com.google.android.gms.internal.zzme.zza;
import com.google.android.gms.internal.zzmf;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.Future;

@zzji
public class zzd
  extends zzhy.zza
  implements zzu
{
  static final int zzcak = Color.argb(0, 0, 0, 0);
  private final Activity mActivity;
  zzmd zzbnz;
  AdOverlayInfoParcel zzcal;
  zzc zzcam;
  zzo zzcan;
  boolean zzcao = false;
  FrameLayout zzcap;
  WebChromeClient.CustomViewCallback zzcaq;
  boolean zzcar = false;
  boolean zzcas = false;
  zzb zzcat;
  boolean zzcau = false;
  int zzcav = 0;
  zzl zzcaw;
  private final Object zzcax = new Object();
  private Runnable zzcay;
  private boolean zzcaz;
  private boolean zzcba;
  private boolean zzcbb = false;
  private boolean zzcbc = false;
  private boolean zzcbd = true;
  
  public zzd(Activity paramActivity)
  {
    this.mActivity = paramActivity;
    this.zzcaw = new zzs();
  }
  
  public void close()
  {
    this.zzcav = 2;
    this.mActivity.finish();
  }
  
  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent) {}
  
  public void onBackPressed()
  {
    this.zzcav = 0;
  }
  
  public void onCreate(Bundle paramBundle)
  {
    boolean bool = false;
    this.mActivity.requestWindowFeature(1);
    if (paramBundle != null) {
      bool = paramBundle.getBoolean("com.google.android.gms.ads.internal.overlay.hasResumed", false);
    }
    this.zzcar = bool;
    try
    {
      this.zzcal = AdOverlayInfoParcel.zzb(this.mActivity.getIntent());
      if (this.zzcal != null) {
        break label80;
      }
      throw new zza("Could not get info for ad overlay.");
    }
    catch (zza paramBundle)
    {
      zzkx.zzdi(paramBundle.getMessage());
      this.zzcav = 3;
      this.mActivity.finish();
    }
    return;
    label80:
    if (this.zzcal.zzari.zzcyb > 7500000) {
      this.zzcav = 3;
    }
    if (this.mActivity.getIntent() != null) {
      this.zzcbd = this.mActivity.getIntent().getBooleanExtra("shouldCallOnOverlayOpened", true);
    }
    if (this.zzcal.zzcbv != null)
    {
      this.zzcas = this.zzcal.zzcbv.zzaok;
      label151:
      if ((((Boolean)zzdr.zzbip.get()).booleanValue()) && (this.zzcas) && (this.zzcal.zzcbv.zzaop != -1)) {
        Future localFuture = (Future)new zzd(null).zzrz();
      }
      if (paramBundle == null)
      {
        if ((this.zzcal.zzcbl != null) && (this.zzcbd)) {
          this.zzcal.zzcbl.zzer();
        }
        if ((this.zzcal.zzcbs != 1) && (this.zzcal.zzcbk != null)) {
          this.zzcal.zzcbk.onAdClicked();
        }
      }
      this.zzcat = new zzb(this.mActivity, this.zzcal.zzcbu);
      this.zzcat.setId(1000);
      switch (this.zzcal.zzcbs)
      {
      }
    }
    for (;;)
    {
      throw new zza("Could not determine ad overlay type.");
      this.zzcas = false;
      break label151;
      zzab(false);
      return;
      this.zzcam = new zzc(this.zzcal.zzcbm);
      zzab(false);
      return;
      zzab(true);
      return;
      if (this.zzcar)
      {
        this.zzcav = 3;
        this.mActivity.finish();
        return;
      }
      if (com.google.android.gms.ads.internal.zzu.zzgj().zza(this.mActivity, this.zzcal.zzcbj, this.zzcal.zzcbr)) {
        break;
      }
      this.zzcav = 3;
      this.mActivity.finish();
      return;
    }
  }
  
  public void onDestroy()
  {
    if (this.zzbnz != null) {
      this.zzcat.removeView(this.zzbnz.getView());
    }
    zzpp();
  }
  
  public void onPause()
  {
    this.zzcaw.pause();
    zzpl();
    if (this.zzcal.zzcbl != null) {
      this.zzcal.zzcbl.onPause();
    }
    if ((!((Boolean)zzdr.zzblf.get()).booleanValue()) && (this.zzbnz != null) && ((!this.mActivity.isFinishing()) || (this.zzcam == null))) {
      com.google.android.gms.ads.internal.zzu.zzgo().zzl(this.zzbnz);
    }
    zzpp();
  }
  
  public void onRestart() {}
  
  public void onResume()
  {
    if ((this.zzcal != null) && (this.zzcal.zzcbs == 4))
    {
      if (this.zzcar)
      {
        this.zzcav = 3;
        this.mActivity.finish();
      }
    }
    else
    {
      if (this.zzcal.zzcbl != null) {
        this.zzcal.zzcbl.onResume();
      }
      if (!((Boolean)zzdr.zzblf.get()).booleanValue())
      {
        if ((this.zzbnz == null) || (this.zzbnz.isDestroyed())) {
          break label122;
        }
        com.google.android.gms.ads.internal.zzu.zzgo().zzm(this.zzbnz);
      }
    }
    for (;;)
    {
      this.zzcaw.resume();
      return;
      this.zzcar = true;
      break;
      label122:
      zzkx.zzdi("The webview does not exist. Ignoring action.");
    }
  }
  
  public void onSaveInstanceState(Bundle paramBundle)
  {
    paramBundle.putBoolean("com.google.android.gms.ads.internal.overlay.hasResumed", this.zzcar);
  }
  
  public void onStart()
  {
    if (((Boolean)zzdr.zzblf.get()).booleanValue())
    {
      if ((this.zzbnz != null) && (!this.zzbnz.isDestroyed())) {
        com.google.android.gms.ads.internal.zzu.zzgo().zzm(this.zzbnz);
      }
    }
    else {
      return;
    }
    zzkx.zzdi("The webview does not exist. Ignoring action.");
  }
  
  public void onStop()
  {
    if ((((Boolean)zzdr.zzblf.get()).booleanValue()) && (this.zzbnz != null) && ((!this.mActivity.isFinishing()) || (this.zzcam == null))) {
      com.google.android.gms.ads.internal.zzu.zzgo().zzl(this.zzbnz);
    }
    zzpp();
  }
  
  public void setRequestedOrientation(int paramInt)
  {
    this.mActivity.setRequestedOrientation(paramInt);
  }
  
  public void zza(View paramView, WebChromeClient.CustomViewCallback paramCustomViewCallback)
  {
    this.zzcap = new FrameLayout(this.mActivity);
    this.zzcap.setBackgroundColor(-16777216);
    this.zzcap.addView(paramView, -1, -1);
    this.mActivity.setContentView(this.zzcap);
    zzds();
    this.zzcaq = paramCustomViewCallback;
    this.zzcao = true;
  }
  
  public void zza(boolean paramBoolean1, boolean paramBoolean2)
  {
    if (this.zzcan != null) {
      this.zzcan.zza(paramBoolean1, paramBoolean2);
    }
  }
  
  public void zzaa(boolean paramBoolean)
  {
    RelativeLayout.LayoutParams localLayoutParams;
    if (paramBoolean)
    {
      i = 50;
      this.zzcan = new zzo(this.mActivity, i, this);
      localLayoutParams = new RelativeLayout.LayoutParams(-2, -2);
      localLayoutParams.addRule(10);
      if (!paramBoolean) {
        break label88;
      }
    }
    label88:
    for (int i = 11;; i = 9)
    {
      localLayoutParams.addRule(i);
      this.zzcan.zza(paramBoolean, this.zzcal.zzcbp);
      this.zzcat.addView(this.zzcan, localLayoutParams);
      return;
      i = 32;
      break;
    }
  }
  
  protected void zzab(boolean paramBoolean)
    throws zzd.zza
  {
    if (!this.zzcba) {
      this.mActivity.requestWindowFeature(1);
    }
    Object localObject = this.mActivity.getWindow();
    if (localObject == null) {
      throw new zza("Invalid activity, no window available.");
    }
    if ((com.google.android.gms.common.util.zzs.isAtLeastN()) && (((Boolean)zzdr.zzble.get()).booleanValue())) {}
    for (boolean bool1 = com.google.android.gms.ads.internal.zzu.zzgm().zza(this.mActivity, this.mActivity.getResources().getConfiguration());; bool1 = true)
    {
      int i;
      label159:
      boolean bool2;
      if ((this.zzcal.zzcbv != null) && (this.zzcal.zzcbv.zzaol))
      {
        i = 1;
        if (((!this.zzcas) || (i != 0)) && (bool1)) {
          ((Window)localObject).setFlags(1024, 1024);
        }
        zzme localzzme = this.zzcal.zzcbm.zzxc();
        if (localzzme == null) {
          break label659;
        }
        bool1 = localzzme.zzic();
        this.zzcau = false;
        if (bool1)
        {
          if (this.zzcal.orientation != com.google.android.gms.ads.internal.zzu.zzgo().zzvw()) {
            break label670;
          }
          if (this.mActivity.getResources().getConfiguration().orientation != 1) {
            break label664;
          }
          bool2 = true;
          label204:
          this.zzcau = bool2;
        }
        label210:
        bool2 = this.zzcau;
        zzkx.zzdg(46 + "Delay onShow to next orientation change: " + bool2);
        setRequestedOrientation(this.zzcal.orientation);
        if (com.google.android.gms.ads.internal.zzu.zzgo().zza((Window)localObject)) {
          zzkx.zzdg("Hardware acceleration on the AdActivity window enabled.");
        }
        if (this.zzcas) {
          break label721;
        }
        this.zzcat.setBackgroundColor(-16777216);
        label287:
        this.mActivity.setContentView(this.zzcat);
        zzds();
        if (!paramBoolean) {
          break label788;
        }
        this.zzbnz = com.google.android.gms.ads.internal.zzu.zzgn().zza(this.mActivity, this.zzcal.zzcbm.zzeg(), true, bool1, null, this.zzcal.zzari, null, null, this.zzcal.zzcbm.zzec());
        this.zzbnz.zzxc().zza(null, null, this.zzcal.zzcbn, this.zzcal.zzcbr, true, this.zzcal.zzcbt, null, this.zzcal.zzcbm.zzxc().zzxu(), null, null);
        this.zzbnz.zzxc().zza(new zzme.zza()
        {
          public void zza(zzmd paramAnonymouszzmd, boolean paramAnonymousBoolean)
          {
            paramAnonymouszzmd.zzps();
          }
        });
        if (this.zzcal.url == null) {
          break label734;
        }
        this.zzbnz.loadUrl(this.zzcal.url);
        label456:
        if (this.zzcal.zzcbm != null) {
          this.zzcal.zzcbm.zzc(this);
        }
        label479:
        this.zzbnz.zzb(this);
        localObject = this.zzbnz.getParent();
        if ((localObject != null) && ((localObject instanceof ViewGroup))) {
          ((ViewGroup)localObject).removeView(this.zzbnz.getView());
        }
        if (this.zzcas) {
          this.zzbnz.zzxt();
        }
        this.zzcat.addView(this.zzbnz.getView(), -1, -1);
        if ((!paramBoolean) && (!this.zzcau)) {
          zzps();
        }
        zzaa(bool1);
        if (this.zzbnz.zzxd()) {
          zza(bool1, true);
        }
        localObject = this.zzbnz.zzec();
        if (localObject == null) {
          break label815;
        }
      }
      label659:
      label664:
      label670:
      label721:
      label734:
      label788:
      label815:
      for (localObject = ((com.google.android.gms.ads.internal.zzd)localObject).zzamr;; localObject = null)
      {
        if (localObject == null) {
          break label821;
        }
        this.zzcaw = ((zzm)localObject).zza(this.mActivity, this.zzbnz, this.zzcat);
        return;
        i = 0;
        break;
        bool1 = false;
        break label159;
        bool2 = false;
        break label204;
        if (this.zzcal.orientation != com.google.android.gms.ads.internal.zzu.zzgo().zzvx()) {
          break label210;
        }
        if (this.mActivity.getResources().getConfiguration().orientation == 2) {}
        for (bool2 = true;; bool2 = false)
        {
          this.zzcau = bool2;
          break;
        }
        this.zzcat.setBackgroundColor(zzcak);
        break label287;
        if (this.zzcal.zzcbq != null)
        {
          this.zzbnz.loadDataWithBaseURL(this.zzcal.zzcbo, this.zzcal.zzcbq, "text/html", "UTF-8", null);
          break label456;
        }
        throw new zza("No URL or HTML to display in ad overlay.");
        this.zzbnz = this.zzcal.zzcbm;
        this.zzbnz.setContext(this.mActivity);
        break label479;
      }
      label821:
      zzkx.zzdi("Appstreaming controller is null.");
      return;
    }
  }
  
  protected void zzak(int paramInt)
  {
    this.zzbnz.zzak(paramInt);
  }
  
  public void zzds()
  {
    this.zzcba = true;
  }
  
  public void zzg(zzmd paramzzmd, Map<String, String> paramMap)
  {
    this.zzcaw.zzg(paramzzmd, paramMap);
  }
  
  public void zzn(com.google.android.gms.dynamic.zzd paramzzd)
  {
    if ((((Boolean)zzdr.zzble.get()).booleanValue()) && (com.google.android.gms.common.util.zzs.isAtLeastN()))
    {
      paramzzd = (Configuration)zze.zzae(paramzzd);
      if (com.google.android.gms.ads.internal.zzu.zzgm().zza(this.mActivity, paramzzd))
      {
        this.mActivity.getWindow().addFlags(1024);
        this.mActivity.getWindow().clearFlags(2048);
      }
    }
    else
    {
      return;
    }
    this.mActivity.getWindow().addFlags(2048);
    this.mActivity.getWindow().clearFlags(1024);
  }
  
  public void zzpl()
  {
    if ((this.zzcal != null) && (this.zzcao)) {
      setRequestedOrientation(this.zzcal.orientation);
    }
    if (this.zzcap != null)
    {
      this.mActivity.setContentView(this.zzcat);
      zzds();
      this.zzcap.removeAllViews();
      this.zzcap = null;
    }
    if (this.zzcaq != null)
    {
      this.zzcaq.onCustomViewHidden();
      this.zzcaq = null;
    }
    this.zzcao = false;
  }
  
  public void zzpm()
  {
    this.zzcav = 1;
    this.mActivity.finish();
  }
  
  public boolean zzpn()
  {
    boolean bool1 = true;
    boolean bool2 = true;
    this.zzcav = 0;
    if (this.zzbnz == null) {
      return bool2;
    }
    if (this.zzbnz.zzxi()) {
      zzl localzzl = this.zzcaw;
    }
    for (;;)
    {
      bool2 = bool1;
      if (bool1) {
        break;
      }
      this.zzbnz.zza("onbackblocked", Collections.emptyMap());
      return bool1;
      bool1 = false;
    }
  }
  
  public void zzpo()
  {
    this.zzcat.removeView(this.zzcan);
    zzaa(true);
  }
  
  protected void zzpp()
  {
    if ((!this.mActivity.isFinishing()) || (this.zzcbb)) {
      return;
    }
    this.zzcbb = true;
    if (this.zzbnz != null)
    {
      zzak(this.zzcav);
      synchronized (this.zzcax)
      {
        if ((!this.zzcaz) && (this.zzbnz.zzxo()))
        {
          this.zzcay = new Runnable()
          {
            public void run()
            {
              zzd.this.zzpq();
            }
          };
          zzlb.zzcvl.postDelayed(this.zzcay, ((Long)zzdr.zzbgf.get()).longValue());
          return;
        }
      }
    }
    zzpq();
  }
  
  void zzpq()
  {
    if (this.zzcbc) {
      return;
    }
    this.zzcbc = true;
    if (this.zzbnz != null)
    {
      this.zzcat.removeView(this.zzbnz.getView());
      if (this.zzcam == null) {
        break label151;
      }
      this.zzbnz.setContext(this.zzcam.zzahs);
      this.zzbnz.zzak(false);
      this.zzcam.parent.addView(this.zzbnz.getView(), this.zzcam.index, this.zzcam.zzcbg);
      this.zzcam = null;
    }
    for (;;)
    {
      this.zzbnz = null;
      if ((this.zzcal != null) && (this.zzcal.zzcbl != null)) {
        this.zzcal.zzcbl.zzeq();
      }
      this.zzcaw.destroy();
      return;
      label151:
      if (this.mActivity.getApplicationContext() != null) {
        this.zzbnz.setContext(this.mActivity.getApplicationContext());
      }
    }
  }
  
  public void zzpr()
  {
    if (this.zzcau)
    {
      this.zzcau = false;
      zzps();
    }
  }
  
  protected void zzps()
  {
    this.zzbnz.zzps();
  }
  
  public void zzpt()
  {
    this.zzcat.disable();
  }
  
  public void zzpu()
  {
    synchronized (this.zzcax)
    {
      this.zzcaz = true;
      if (this.zzcay != null)
      {
        zzlb.zzcvl.removeCallbacks(this.zzcay);
        zzlb.zzcvl.post(this.zzcay);
      }
      return;
    }
  }
  
  @zzji
  private static final class zza
    extends Exception
  {
    public zza(String paramString)
    {
      super();
    }
  }
  
  @zzji
  static class zzb
    extends RelativeLayout
  {
    zzle zzasr;
    boolean zzcbf;
    
    public zzb(Context paramContext, String paramString)
    {
      super();
      this.zzasr = new zzle(paramContext, paramString);
    }
    
    void disable()
    {
      this.zzcbf = true;
    }
    
    public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
    {
      if (!this.zzcbf) {
        this.zzasr.zzg(paramMotionEvent);
      }
      return false;
    }
  }
  
  @zzji
  public static class zzc
  {
    public final int index;
    public final ViewGroup parent;
    public final Context zzahs;
    public final ViewGroup.LayoutParams zzcbg;
    
    public zzc(zzmd paramzzmd)
      throws zzd.zza
    {
      this.zzcbg = paramzzmd.getLayoutParams();
      ViewParent localViewParent = paramzzmd.getParent();
      this.zzahs = paramzzmd.zzwz();
      if ((localViewParent != null) && ((localViewParent instanceof ViewGroup)))
      {
        this.parent = ((ViewGroup)localViewParent);
        this.index = this.parent.indexOfChild(paramzzmd.getView());
        this.parent.removeView(paramzzmd.getView());
        paramzzmd.zzak(true);
        return;
      }
      throw new zzd.zza("Could not get the parent of the WebView for an overlay.");
    }
  }
  
  @zzji
  private class zzd
    extends zzkw
  {
    private zzd() {}
    
    public void onStop() {}
    
    public void zzfp()
    {
      final Object localObject = com.google.android.gms.ads.internal.zzu.zzhh().zza(Integer.valueOf(zzd.this.zzcal.zzcbv.zzaop));
      if (localObject != null)
      {
        localObject = com.google.android.gms.ads.internal.zzu.zzgo().zza(zzd.zza(zzd.this), (Bitmap)localObject, zzd.this.zzcal.zzcbv.zzaon, zzd.this.zzcal.zzcbv.zzaoo);
        zzlb.zzcvl.post(new Runnable()
        {
          public void run()
          {
            zzd.zza(zzd.this).getWindow().setBackgroundDrawable(localObject);
          }
        });
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/overlay/zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */