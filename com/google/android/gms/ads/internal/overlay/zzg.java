package com.google.android.gms.ads.internal.overlay;

public abstract interface zzg
{
  public abstract void onPause();
  
  public abstract void onResume();
  
  public abstract void zzeq();
  
  public abstract void zzer();
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/overlay/zzg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */