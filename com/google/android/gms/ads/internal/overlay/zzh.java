package com.google.android.gms.ads.internal.overlay;

import android.support.annotation.Nullable;

public abstract interface zzh
{
  public abstract void onPaused();
  
  public abstract void zzf(int paramInt1, int paramInt2);
  
  public abstract void zzk(String paramString1, @Nullable String paramString2);
  
  public abstract void zzqb();
  
  public abstract void zzqc();
  
  public abstract void zzqd();
  
  public abstract void zzqe();
  
  public abstract void zzqf();
  
  public abstract void zzqg();
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/overlay/zzh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */