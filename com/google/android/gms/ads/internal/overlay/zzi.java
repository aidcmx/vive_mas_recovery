package com.google.android.gms.ads.internal.overlay;

import android.annotation.TargetApi;
import android.content.Context;
import android.view.TextureView;
import com.google.android.gms.internal.zzji;

@zzji
@TargetApi(14)
public abstract class zzi
  extends TextureView
  implements zzaa.zza
{
  protected final zzv zzcbw = new zzv();
  protected final zzaa zzcbx = new zzaa(paramContext, this);
  
  public zzi(Context paramContext)
  {
    super(paramContext);
  }
  
  public abstract int getCurrentPosition();
  
  public abstract int getDuration();
  
  public abstract int getVideoHeight();
  
  public abstract int getVideoWidth();
  
  public abstract void pause();
  
  public abstract void play();
  
  public abstract void seekTo(int paramInt);
  
  public abstract void setVideoPath(String paramString);
  
  public abstract void stop();
  
  public abstract void zza(float paramFloat1, float paramFloat2);
  
  public abstract void zza(zzh paramzzh);
  
  public void zzb(float paramFloat)
  {
    this.zzcbx.zzb(paramFloat);
    zzpk();
  }
  
  public abstract String zzpg();
  
  public abstract void zzpk();
  
  public void zzqh()
  {
    this.zzcbx.setMuted(true);
    zzpk();
  }
  
  public void zzqi()
  {
    this.zzcbx.setMuted(false);
    zzpk();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/overlay/zzi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */