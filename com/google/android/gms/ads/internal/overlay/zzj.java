package com.google.android.gms.ads.internal.overlay;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.support.annotation.Nullable;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.common.util.zzs;
import com.google.android.gms.internal.zzdz;
import com.google.android.gms.internal.zzji;
import com.google.android.gms.internal.zzmd;

@zzji
public abstract class zzj
{
  @Nullable
  public abstract zzi zza(Context paramContext, zzmd paramzzmd, int paramInt, boolean paramBoolean, zzdz paramzzdz);
  
  protected boolean zzh(zzmd paramzzmd)
  {
    return paramzzmd.zzeg().zzazr;
  }
  
  protected boolean zzp(Context paramContext)
  {
    paramContext = paramContext.getApplicationInfo();
    return (zzs.zzayq()) && ((paramContext == null) || (paramContext.targetSdkVersion >= 11));
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/overlay/zzj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */