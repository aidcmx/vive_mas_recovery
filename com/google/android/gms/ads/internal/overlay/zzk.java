package com.google.android.gms.ads.internal.overlay;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.android.gms.ads.internal.zzd;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.common.internal.zzc;
import com.google.android.gms.common.util.zze;
import com.google.android.gms.internal.zzdn;
import com.google.android.gms.internal.zzdr;
import com.google.android.gms.internal.zzdz;
import com.google.android.gms.internal.zzji;
import com.google.android.gms.internal.zzkx;
import com.google.android.gms.internal.zzlb;
import com.google.android.gms.internal.zzmd;
import java.util.HashMap;

@zzji
public class zzk
  extends FrameLayout
  implements zzh
{
  private final zzmd zzbnz;
  private String zzbrb;
  private final FrameLayout zzcby;
  private final zzdz zzcbz;
  private final zzz zzcca;
  private final long zzccb;
  @Nullable
  private zzi zzccc;
  private boolean zzccd;
  private boolean zzcce;
  private boolean zzccf;
  private boolean zzccg;
  private long zzcch;
  private long zzcci;
  private Bitmap zzccj;
  private ImageView zzcck;
  private boolean zzccl;
  
  public zzk(Context paramContext, zzmd paramzzmd, int paramInt, boolean paramBoolean, zzdz paramzzdz)
  {
    super(paramContext);
    this.zzbnz = paramzzmd;
    this.zzcbz = paramzzdz;
    this.zzcby = new FrameLayout(paramContext);
    addView(this.zzcby, new FrameLayout.LayoutParams(-1, -1));
    zzc.zzu(paramzzmd.zzec());
    this.zzccc = paramzzmd.zzec().zzamq.zza(paramContext, paramzzmd, paramInt, paramBoolean, paramzzdz);
    if (this.zzccc != null)
    {
      this.zzcby.addView(this.zzccc, new FrameLayout.LayoutParams(-1, -1, 17));
      if (((Boolean)zzdr.zzbdx.get()).booleanValue()) {
        zzqj();
      }
    }
    this.zzcck = new ImageView(paramContext);
    this.zzccb = ((Long)zzdr.zzbeb.get()).longValue();
    this.zzccg = ((Boolean)zzdr.zzbdz.get()).booleanValue();
    if (this.zzcbz != null)
    {
      paramzzmd = this.zzcbz;
      if (!this.zzccg) {
        break label248;
      }
    }
    label248:
    for (paramContext = "1";; paramContext = "0")
    {
      paramzzmd.zzg("spinner_used", paramContext);
      this.zzcca = new zzz(this);
      this.zzcca.zzrg();
      if (this.zzccc != null) {
        this.zzccc.zza(this);
      }
      if (this.zzccc == null) {
        zzk("AdVideoUnderlay Error", "Allocating player failed.");
      }
      return;
    }
  }
  
  private void zza(String paramString, String... paramVarArgs)
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put("event", paramString);
    int j = paramVarArgs.length;
    int i = 0;
    paramString = null;
    if (i < j)
    {
      String str = paramVarArgs[i];
      if (paramString == null) {}
      for (paramString = str;; paramString = null)
      {
        i += 1;
        break;
        localHashMap.put(paramString, str);
      }
    }
    this.zzbnz.zza("onVideoEvent", localHashMap);
  }
  
  private void zzg(int paramInt1, int paramInt2)
  {
    if (!this.zzccg) {}
    do
    {
      return;
      paramInt1 = Math.max(paramInt1 / ((Integer)zzdr.zzbea.get()).intValue(), 1);
      paramInt2 = Math.max(paramInt2 / ((Integer)zzdr.zzbea.get()).intValue(), 1);
    } while ((this.zzccj != null) && (this.zzccj.getWidth() == paramInt1) && (this.zzccj.getHeight() == paramInt2));
    this.zzccj = Bitmap.createBitmap(paramInt1, paramInt2, Bitmap.Config.ARGB_8888);
    this.zzccl = false;
  }
  
  public static void zzi(zzmd paramzzmd)
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put("event", "no_video_view");
    paramzzmd.zza("onVideoEvent", localHashMap);
  }
  
  @TargetApi(14)
  private void zzql()
  {
    if (this.zzccj == null) {}
    long l;
    do
    {
      do
      {
        return;
        l = zzu.zzgs().elapsedRealtime();
        if (this.zzccc.getBitmap(this.zzccj) != null) {
          this.zzccl = true;
        }
        l = zzu.zzgs().elapsedRealtime() - l;
        if (zzkx.zzvo()) {
          zzkx.v(46 + "Spinner frame grab took " + l + "ms");
        }
      } while (l <= this.zzccb);
      zzkx.zzdi("Spinner frame grab crossed jank threshold! Suspending spinner.");
      this.zzccg = false;
      this.zzccj = null;
    } while (this.zzcbz == null);
    this.zzcbz.zzg("spinner_jank", Long.toString(l));
  }
  
  private void zzqm()
  {
    if ((this.zzccl) && (this.zzccj != null) && (!zzqo()))
    {
      this.zzcck.setImageBitmap(this.zzccj);
      this.zzcck.invalidate();
      this.zzcby.addView(this.zzcck, new FrameLayout.LayoutParams(-1, -1));
      this.zzcby.bringChildToFront(this.zzcck);
    }
  }
  
  private void zzqn()
  {
    if (zzqo()) {
      this.zzcby.removeView(this.zzcck);
    }
  }
  
  private boolean zzqo()
  {
    return this.zzcck.getParent() != null;
  }
  
  private void zzqp()
  {
    if (this.zzbnz.zzwy() == null) {
      break label12;
    }
    label12:
    while (this.zzcce) {
      return;
    }
    if ((this.zzbnz.zzwy().getWindow().getAttributes().flags & 0x80) != 0) {}
    for (boolean bool = true;; bool = false)
    {
      this.zzccf = bool;
      if (this.zzccf) {
        break;
      }
      this.zzbnz.zzwy().getWindow().addFlags(128);
      this.zzcce = true;
      return;
    }
  }
  
  private void zzqq()
  {
    if (this.zzbnz.zzwy() == null) {}
    while ((!this.zzcce) || (this.zzccf)) {
      return;
    }
    this.zzbnz.zzwy().getWindow().clearFlags(128);
    this.zzcce = false;
  }
  
  public void destroy()
  {
    this.zzcca.cancel();
    if (this.zzccc != null) {
      this.zzccc.stop();
    }
    zzqq();
  }
  
  public void onPaused()
  {
    zza("pause", new String[0]);
    zzqq();
    this.zzccd = false;
  }
  
  public void pause()
  {
    if (this.zzccc == null) {
      return;
    }
    this.zzccc.pause();
  }
  
  public void play()
  {
    if (this.zzccc == null) {
      return;
    }
    this.zzccc.play();
  }
  
  public void seekTo(int paramInt)
  {
    if (this.zzccc == null) {
      return;
    }
    this.zzccc.seekTo(paramInt);
  }
  
  public void zza(float paramFloat1, float paramFloat2)
  {
    if (this.zzccc != null) {
      this.zzccc.zza(paramFloat1, paramFloat2);
    }
  }
  
  public void zzb(float paramFloat)
  {
    if (this.zzccc == null) {
      return;
    }
    this.zzccc.zzb(paramFloat);
  }
  
  public void zzce(String paramString)
  {
    this.zzbrb = paramString;
  }
  
  public void zzd(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    if ((paramInt3 == 0) || (paramInt4 == 0)) {
      return;
    }
    FrameLayout.LayoutParams localLayoutParams = new FrameLayout.LayoutParams(paramInt3, paramInt4);
    localLayoutParams.setMargins(paramInt1, paramInt2, 0, 0);
    this.zzcby.setLayoutParams(localLayoutParams);
    requestLayout();
  }
  
  public void zzf(int paramInt1, int paramInt2)
  {
    zzg(paramInt1, paramInt2);
  }
  
  @TargetApi(14)
  public void zzf(MotionEvent paramMotionEvent)
  {
    if (this.zzccc == null) {
      return;
    }
    this.zzccc.dispatchTouchEvent(paramMotionEvent);
  }
  
  public void zzk(String paramString1, @Nullable String paramString2)
  {
    zza("error", new String[] { "what", paramString1, "extra", paramString2 });
  }
  
  public void zznt()
  {
    if (this.zzccc == null) {
      return;
    }
    if (!TextUtils.isEmpty(this.zzbrb))
    {
      this.zzccc.setVideoPath(this.zzbrb);
      return;
    }
    zza("no_src", new String[0]);
  }
  
  public void zzqb()
  {
    zzlb.zzcvl.post(new Runnable()
    {
      public void run()
      {
        zzk.zza(zzk.this, "surfaceCreated", new String[0]);
      }
    });
  }
  
  public void zzqc()
  {
    if (this.zzccc == null) {}
    while (this.zzcci != 0L) {
      return;
    }
    zza("canplaythrough", new String[] { "duration", String.valueOf(this.zzccc.getDuration() / 1000.0F), "videoWidth", String.valueOf(this.zzccc.getVideoWidth()), "videoHeight", String.valueOf(this.zzccc.getVideoHeight()) });
  }
  
  public void zzqd()
  {
    zzqp();
    this.zzccd = true;
  }
  
  public void zzqe()
  {
    zza("ended", new String[0]);
    zzqq();
  }
  
  public void zzqf()
  {
    zzqm();
    this.zzcci = this.zzcch;
    zzlb.zzcvl.post(new Runnable()
    {
      public void run()
      {
        zzk.zza(zzk.this, "surfaceDestroyed", new String[0]);
      }
    });
  }
  
  public void zzqg()
  {
    if (this.zzccd) {
      zzqn();
    }
    zzql();
  }
  
  public void zzqh()
  {
    if (this.zzccc == null) {
      return;
    }
    this.zzccc.zzqh();
  }
  
  public void zzqi()
  {
    if (this.zzccc == null) {
      return;
    }
    this.zzccc.zzqi();
  }
  
  @TargetApi(14)
  public void zzqj()
  {
    if (this.zzccc == null) {
      return;
    }
    TextView localTextView = new TextView(this.zzccc.getContext());
    String str = String.valueOf(this.zzccc.zzpg());
    if (str.length() != 0) {}
    for (str = "AdMob - ".concat(str);; str = new String("AdMob - "))
    {
      localTextView.setText(str);
      localTextView.setTextColor(-65536);
      localTextView.setBackgroundColor(65280);
      this.zzcby.addView(localTextView, new FrameLayout.LayoutParams(-2, -2, 17));
      this.zzcby.bringChildToFront(localTextView);
      return;
    }
  }
  
  void zzqk()
  {
    if (this.zzccc == null) {}
    long l;
    do
    {
      return;
      l = this.zzccc.getCurrentPosition();
    } while ((this.zzcch == l) || (l <= 0L));
    zza("timeupdate", new String[] { "time", String.valueOf((float)l / 1000.0F) });
    this.zzcch = l;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/overlay/zzk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */