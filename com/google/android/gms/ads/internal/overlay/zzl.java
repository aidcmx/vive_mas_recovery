package com.google.android.gms.ads.internal.overlay;

import com.google.android.gms.internal.zzmd;
import java.util.Map;

public abstract interface zzl
{
  public abstract void destroy();
  
  public abstract void pause();
  
  public abstract void resume();
  
  public abstract void zzg(zzmd paramzzmd, Map<String, String> paramMap);
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/overlay/zzl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */