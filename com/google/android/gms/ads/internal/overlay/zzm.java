package com.google.android.gms.ads.internal.overlay;

import android.app.Activity;
import android.widget.RelativeLayout;
import com.google.android.gms.internal.zzmd;

public abstract interface zzm
{
  public abstract zzl zza(Activity paramActivity, zzmd paramzzmd, RelativeLayout paramRelativeLayout);
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/overlay/zzm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */