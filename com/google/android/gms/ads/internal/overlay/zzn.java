package com.google.android.gms.ads.internal.overlay;

import android.content.Context;
import android.support.annotation.Nullable;
import com.google.android.gms.internal.zzdz;
import com.google.android.gms.internal.zzji;
import com.google.android.gms.internal.zzmd;

@zzji
public class zzn
  extends zzj
{
  @Nullable
  public zzi zza(Context paramContext, zzmd paramzzmd, int paramInt, boolean paramBoolean, zzdz paramzzdz)
  {
    if (!zzp(paramContext)) {
      return null;
    }
    return new zzc(paramContext, paramBoolean, zzh(paramzzmd), new zzy(paramContext, paramzzmd.zzxf(), paramzzmd.getRequestId(), paramzzdz, paramzzmd.zzxl()));
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/overlay/zzn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */