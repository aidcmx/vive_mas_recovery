package com.google.android.gms.ads.internal.overlay;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageButton;
import com.google.android.gms.ads.internal.client.zzm;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.internal.zzji;

@zzji
public class zzo
  extends FrameLayout
  implements View.OnClickListener
{
  private final ImageButton zzccn;
  private final zzu zzcco;
  
  public zzo(Context paramContext, int paramInt, zzu paramzzu)
  {
    super(paramContext);
    this.zzcco = paramzzu;
    setOnClickListener(this);
    this.zzccn = new ImageButton(paramContext);
    this.zzccn.setImageResource(17301527);
    this.zzccn.setBackgroundColor(0);
    this.zzccn.setOnClickListener(this);
    this.zzccn.setPadding(0, 0, 0, 0);
    this.zzccn.setContentDescription("Interstitial close button");
    paramInt = zzm.zzkr().zzb(paramContext, paramInt);
    addView(this.zzccn, new FrameLayout.LayoutParams(paramInt, paramInt, 17));
  }
  
  public void onClick(View paramView)
  {
    if (this.zzcco != null) {
      this.zzcco.zzpm();
    }
  }
  
  public void zza(boolean paramBoolean1, boolean paramBoolean2)
  {
    if (paramBoolean2)
    {
      if (paramBoolean1)
      {
        this.zzccn.setVisibility(4);
        return;
      }
      this.zzccn.setVisibility(8);
      return;
    }
    this.zzccn.setVisibility(0);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/overlay/zzo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */