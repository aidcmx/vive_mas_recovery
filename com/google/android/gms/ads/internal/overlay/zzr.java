package com.google.android.gms.ads.internal.overlay;

import android.annotation.TargetApi;
import android.graphics.SurfaceTexture;
import android.view.Surface;

public class zzr
{
  @TargetApi(14)
  public Surface zza(SurfaceTexture paramSurfaceTexture)
  {
    return new Surface(paramSurfaceTexture);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/overlay/zzr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */