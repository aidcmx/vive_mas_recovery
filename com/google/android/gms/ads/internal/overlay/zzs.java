package com.google.android.gms.ads.internal.overlay;

import com.google.android.gms.internal.zzmd;
import java.util.Map;

public class zzs
  implements zzl
{
  public void destroy() {}
  
  public void pause() {}
  
  public void resume() {}
  
  public void zzg(zzmd paramzzmd, Map<String, String> paramMap) {}
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/overlay/zzs.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */