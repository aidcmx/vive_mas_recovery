package com.google.android.gms.ads.internal.overlay;

import android.annotation.TargetApi;
import android.graphics.SurfaceTexture;
import android.os.Handler;
import com.google.android.gms.internal.zzdn;
import com.google.android.gms.internal.zzdr;
import com.google.android.gms.internal.zzji;
import com.google.android.gms.internal.zzlb;
import java.util.concurrent.TimeUnit;

@zzji
@TargetApi(14)
public class zzv
{
  private final long zzccp = TimeUnit.MILLISECONDS.toNanos(((Long)zzdr.zzbdy.get()).longValue());
  private long zzccq;
  private boolean zzccr = true;
  
  public void zza(SurfaceTexture paramSurfaceTexture, final zzh paramzzh)
  {
    if (paramzzh == null) {}
    long l;
    do
    {
      return;
      l = paramSurfaceTexture.getTimestamp();
    } while ((!this.zzccr) && (Math.abs(l - this.zzccq) < this.zzccp));
    this.zzccr = false;
    this.zzccq = l;
    zzlb.zzcvl.post(new Runnable()
    {
      public void run()
      {
        paramzzh.zzqg();
      }
    });
  }
  
  public void zzqd()
  {
    this.zzccr = true;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/overlay/zzv.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */