package com.google.android.gms.ads.internal.overlay;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.view.Display;
import android.view.WindowManager;
import com.google.android.gms.internal.zzji;
import com.google.android.gms.internal.zzkx;

@zzji
class zzw
  implements SensorEventListener
{
  private final SensorManager zzccu;
  private final Object zzccv;
  private final Display zzccw;
  private final float[] zzccx;
  private final float[] zzccy;
  private float[] zzccz;
  private Handler zzcda;
  private zza zzcdb;
  
  zzw(Context paramContext)
  {
    this.zzccu = ((SensorManager)paramContext.getSystemService("sensor"));
    this.zzccw = ((WindowManager)paramContext.getSystemService("window")).getDefaultDisplay();
    this.zzccx = new float[9];
    this.zzccy = new float[9];
    this.zzccv = new Object();
  }
  
  private void zzh(int paramInt1, int paramInt2)
  {
    float f = this.zzccy[paramInt1];
    this.zzccy[paramInt1] = this.zzccy[paramInt2];
    this.zzccy[paramInt2] = f;
  }
  
  int getRotation()
  {
    return this.zzccw.getRotation();
  }
  
  public void onAccuracyChanged(Sensor paramSensor, int paramInt) {}
  
  public void onSensorChanged(SensorEvent paramSensorEvent)
  {
    zza(paramSensorEvent.values);
  }
  
  void start()
  {
    if (this.zzcda != null) {}
    Sensor localSensor;
    do
    {
      return;
      localSensor = this.zzccu.getDefaultSensor(11);
      if (localSensor == null)
      {
        zzkx.e("No Sensor of TYPE_ROTATION_VECTOR");
        return;
      }
      HandlerThread localHandlerThread = new HandlerThread("OrientationMonitor");
      localHandlerThread.start();
      this.zzcda = new Handler(localHandlerThread.getLooper());
    } while (this.zzccu.registerListener(this, localSensor, 0, this.zzcda));
    zzkx.e("SensorManager.registerListener failed.");
    stop();
  }
  
  void stop()
  {
    if (this.zzcda == null) {
      return;
    }
    this.zzccu.unregisterListener(this);
    this.zzcda.post(new Runnable()
    {
      public void run()
      {
        Looper.myLooper().quit();
      }
    });
    this.zzcda = null;
  }
  
  void zza(zza paramzza)
  {
    this.zzcdb = paramzza;
  }
  
  void zza(float[] arg1)
  {
    if ((???[0] == 0.0F) && (???[1] == 0.0F) && (???[2] == 0.0F)) {}
    for (;;)
    {
      return;
      synchronized (this.zzccv)
      {
        if (this.zzccz == null) {
          this.zzccz = new float[9];
        }
        SensorManager.getRotationMatrixFromVector(this.zzccx, ???);
        switch (getRotation())
        {
        default: 
          System.arraycopy(this.zzccx, 0, this.zzccy, 0, 9);
          label103:
          zzh(1, 3);
          zzh(2, 6);
          zzh(5, 7);
        }
      }
      synchronized (this.zzccv)
      {
        System.arraycopy(this.zzccy, 0, this.zzccz, 0, 9);
        if (this.zzcdb == null) {
          continue;
        }
        this.zzcdb.zzpr();
        return;
        ??? = finally;
        throw ???;
        SensorManager.remapCoordinateSystem(this.zzccx, 2, 129, this.zzccy);
        break label103;
        SensorManager.remapCoordinateSystem(this.zzccx, 129, 130, this.zzccy);
        break label103;
        SensorManager.remapCoordinateSystem(this.zzccx, 130, 1, this.zzccy);
      }
    }
  }
  
  boolean zzb(float[] paramArrayOfFloat)
  {
    synchronized (this.zzccv)
    {
      if (this.zzccz == null) {
        return false;
      }
      System.arraycopy(this.zzccz, 0, paramArrayOfFloat, 0, this.zzccz.length);
      return true;
    }
  }
  
  static abstract interface zza
  {
    public abstract void zzpr();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/overlay/zzw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */