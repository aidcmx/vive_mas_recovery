package com.google.android.gms.ads.internal.overlay;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.SurfaceTexture;
import android.graphics.SurfaceTexture.OnFrameAvailableListener;
import android.opengl.GLES20;
import android.opengl.GLUtils;
import android.support.annotation.Nullable;
import android.util.Log;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.internal.zzdn;
import com.google.android.gms.internal.zzdr;
import com.google.android.gms.internal.zzji;
import com.google.android.gms.internal.zzkr;
import com.google.android.gms.internal.zzkx;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.concurrent.CountDownLatch;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;

@zzji
@TargetApi(14)
public class zzx
  extends Thread
  implements SurfaceTexture.OnFrameAvailableListener, zzw.zza
{
  private static final float[] zzcdd = { -1.0F, -1.0F, -1.0F, 1.0F, -1.0F, -1.0F, -1.0F, 1.0F, -1.0F, 1.0F, 1.0F, -1.0F };
  private int zzakh;
  private int zzaki;
  private final float[] zzccz;
  private final zzw zzcde;
  private final float[] zzcdf;
  private final float[] zzcdg;
  private final float[] zzcdh;
  private final float[] zzcdi;
  private final float[] zzcdj;
  private final float[] zzcdk;
  private float zzcdl;
  private float zzcdm;
  private float zzcdn;
  private SurfaceTexture zzcdo;
  private SurfaceTexture zzcdp;
  private int zzcdq;
  private int zzcdr;
  private int zzcds;
  private FloatBuffer zzcdt = ByteBuffer.allocateDirect(zzcdd.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
  private final CountDownLatch zzcdu;
  private final Object zzcdv;
  private EGL10 zzcdw;
  private EGLDisplay zzcdx;
  private EGLContext zzcdy;
  private EGLSurface zzcdz;
  private volatile boolean zzcea;
  private volatile boolean zzceb;
  
  public zzx(Context paramContext)
  {
    super("SphericalVideoProcessor");
    this.zzcdt.put(zzcdd).position(0);
    this.zzccz = new float[9];
    this.zzcdf = new float[9];
    this.zzcdg = new float[9];
    this.zzcdh = new float[9];
    this.zzcdi = new float[9];
    this.zzcdj = new float[9];
    this.zzcdk = new float[9];
    this.zzcdl = NaN.0F;
    this.zzcde = new zzw(paramContext);
    this.zzcde.zza(this);
    this.zzcdu = new CountDownLatch(1);
    this.zzcdv = new Object();
  }
  
  private void zza(float[] paramArrayOfFloat, float paramFloat)
  {
    paramArrayOfFloat[0] = 1.0F;
    paramArrayOfFloat[1] = 0.0F;
    paramArrayOfFloat[2] = 0.0F;
    paramArrayOfFloat[3] = 0.0F;
    paramArrayOfFloat[4] = ((float)Math.cos(paramFloat));
    paramArrayOfFloat[5] = ((float)-Math.sin(paramFloat));
    paramArrayOfFloat[6] = 0.0F;
    paramArrayOfFloat[7] = ((float)Math.sin(paramFloat));
    paramArrayOfFloat[8] = ((float)Math.cos(paramFloat));
  }
  
  private void zza(float[] paramArrayOfFloat1, float[] paramArrayOfFloat2, float[] paramArrayOfFloat3)
  {
    paramArrayOfFloat1[0] = (paramArrayOfFloat2[0] * paramArrayOfFloat3[0] + paramArrayOfFloat2[1] * paramArrayOfFloat3[3] + paramArrayOfFloat2[2] * paramArrayOfFloat3[6]);
    paramArrayOfFloat1[1] = (paramArrayOfFloat2[0] * paramArrayOfFloat3[1] + paramArrayOfFloat2[1] * paramArrayOfFloat3[4] + paramArrayOfFloat2[2] * paramArrayOfFloat3[7]);
    paramArrayOfFloat1[2] = (paramArrayOfFloat2[0] * paramArrayOfFloat3[2] + paramArrayOfFloat2[1] * paramArrayOfFloat3[5] + paramArrayOfFloat2[2] * paramArrayOfFloat3[8]);
    paramArrayOfFloat1[3] = (paramArrayOfFloat2[3] * paramArrayOfFloat3[0] + paramArrayOfFloat2[4] * paramArrayOfFloat3[3] + paramArrayOfFloat2[5] * paramArrayOfFloat3[6]);
    paramArrayOfFloat1[4] = (paramArrayOfFloat2[3] * paramArrayOfFloat3[1] + paramArrayOfFloat2[4] * paramArrayOfFloat3[4] + paramArrayOfFloat2[5] * paramArrayOfFloat3[7]);
    paramArrayOfFloat1[5] = (paramArrayOfFloat2[3] * paramArrayOfFloat3[2] + paramArrayOfFloat2[4] * paramArrayOfFloat3[5] + paramArrayOfFloat2[5] * paramArrayOfFloat3[8]);
    paramArrayOfFloat1[6] = (paramArrayOfFloat2[6] * paramArrayOfFloat3[0] + paramArrayOfFloat2[7] * paramArrayOfFloat3[3] + paramArrayOfFloat2[8] * paramArrayOfFloat3[6]);
    paramArrayOfFloat1[7] = (paramArrayOfFloat2[6] * paramArrayOfFloat3[1] + paramArrayOfFloat2[7] * paramArrayOfFloat3[4] + paramArrayOfFloat2[8] * paramArrayOfFloat3[7]);
    paramArrayOfFloat1[8] = (paramArrayOfFloat2[6] * paramArrayOfFloat3[2] + paramArrayOfFloat2[7] * paramArrayOfFloat3[5] + paramArrayOfFloat2[8] * paramArrayOfFloat3[8]);
  }
  
  private float[] zza(float[] paramArrayOfFloat1, float[] paramArrayOfFloat2)
  {
    return new float[] { paramArrayOfFloat1[0] * paramArrayOfFloat2[0] + paramArrayOfFloat1[1] * paramArrayOfFloat2[1] + paramArrayOfFloat1[2] * paramArrayOfFloat2[2], paramArrayOfFloat1[3] * paramArrayOfFloat2[0] + paramArrayOfFloat1[4] * paramArrayOfFloat2[1] + paramArrayOfFloat1[5] * paramArrayOfFloat2[2], paramArrayOfFloat1[6] * paramArrayOfFloat2[0] + paramArrayOfFloat1[7] * paramArrayOfFloat2[1] + paramArrayOfFloat1[8] * paramArrayOfFloat2[2] };
  }
  
  private void zzb(float[] paramArrayOfFloat, float paramFloat)
  {
    paramArrayOfFloat[0] = ((float)Math.cos(paramFloat));
    paramArrayOfFloat[1] = ((float)-Math.sin(paramFloat));
    paramArrayOfFloat[2] = 0.0F;
    paramArrayOfFloat[3] = ((float)Math.sin(paramFloat));
    paramArrayOfFloat[4] = ((float)Math.cos(paramFloat));
    paramArrayOfFloat[5] = 0.0F;
    paramArrayOfFloat[6] = 0.0F;
    paramArrayOfFloat[7] = 0.0F;
    paramArrayOfFloat[8] = 1.0F;
  }
  
  private float zzc(float[] paramArrayOfFloat)
  {
    paramArrayOfFloat = zza(paramArrayOfFloat, new float[] { 0.0F, 1.0F, 0.0F });
    return (float)Math.atan2(paramArrayOfFloat[1], paramArrayOfFloat[0]) - 1.5707964F;
  }
  
  private int zzc(int paramInt, String paramString)
  {
    int i = GLES20.glCreateShader(paramInt);
    zzcf("createShader");
    if (i != 0)
    {
      GLES20.glShaderSource(i, paramString);
      zzcf("shaderSource");
      GLES20.glCompileShader(i);
      zzcf("compileShader");
      paramString = new int[1];
      GLES20.glGetShaderiv(i, 35713, paramString, 0);
      zzcf("getShaderiv");
      if (paramString[0] == 0)
      {
        Log.e("SphericalVideoRenderer", 37 + "Could not compile shader " + paramInt + ":");
        Log.e("SphericalVideoRenderer", GLES20.glGetShaderInfoLog(i));
        GLES20.glDeleteShader(i);
        zzcf("deleteShader");
        return 0;
      }
    }
    return i;
  }
  
  private void zzcf(String paramString)
  {
    int i = GLES20.glGetError();
    if (i != 0) {
      Log.e("SphericalVideoRenderer", String.valueOf(paramString).length() + 21 + paramString + ": glError " + i);
    }
  }
  
  private void zzqv()
  {
    GLES20.glViewport(0, 0, this.zzakh, this.zzaki);
    zzcf("viewport");
    int i = GLES20.glGetUniformLocation(this.zzcdq, "uFOVx");
    int j = GLES20.glGetUniformLocation(this.zzcdq, "uFOVy");
    if (this.zzakh > this.zzaki)
    {
      GLES20.glUniform1f(i, 0.87266463F);
      GLES20.glUniform1f(j, this.zzaki * 0.87266463F / this.zzakh);
      return;
    }
    GLES20.glUniform1f(i, this.zzakh * 0.87266463F / this.zzaki);
    GLES20.glUniform1f(j, 0.87266463F);
  }
  
  private int zzqx()
  {
    int i = zzc(35633, zzra());
    if (i == 0) {}
    int j;
    do
    {
      return 0;
      j = zzc(35632, zzrb());
    } while (j == 0);
    int k = GLES20.glCreateProgram();
    zzcf("createProgram");
    if (k != 0)
    {
      GLES20.glAttachShader(k, i);
      zzcf("attachShader");
      GLES20.glAttachShader(k, j);
      zzcf("attachShader");
      GLES20.glLinkProgram(k);
      zzcf("linkProgram");
      int[] arrayOfInt = new int[1];
      GLES20.glGetProgramiv(k, 35714, arrayOfInt, 0);
      zzcf("getProgramiv");
      if (arrayOfInt[0] != 1)
      {
        Log.e("SphericalVideoRenderer", "Could not link program: ");
        Log.e("SphericalVideoRenderer", GLES20.glGetProgramInfoLog(k));
        GLES20.glDeleteProgram(k);
        zzcf("deleteProgram");
        return 0;
      }
      GLES20.glValidateProgram(k);
      zzcf("validateProgram");
    }
    return k;
  }
  
  @Nullable
  private EGLConfig zzqz()
  {
    int[] arrayOfInt = new int[1];
    EGLConfig[] arrayOfEGLConfig = new EGLConfig[1];
    if (!this.zzcdw.eglChooseConfig(this.zzcdx, new int[] { 12352, 4, 12324, 8, 12323, 8, 12322, 8, 12325, 16, 12344 }, arrayOfEGLConfig, 1, arrayOfInt)) {
      return null;
    }
    if (arrayOfInt[0] > 0) {
      return arrayOfEGLConfig[0];
    }
    return null;
  }
  
  private String zzra()
  {
    zzdn localzzdn = zzdr.zzbgo;
    if (!((String)localzzdn.get()).equals(localzzdn.zzlp())) {
      return (String)localzzdn.get();
    }
    return "attribute highp vec3 aPosition;varying vec3 pos;void main() {  gl_Position = vec4(aPosition, 1.0);  pos = aPosition;}";
  }
  
  private String zzrb()
  {
    zzdn localzzdn = zzdr.zzbgp;
    if (!((String)localzzdn.get()).equals(localzzdn.zzlp())) {
      return (String)localzzdn.get();
    }
    return "#extension GL_OES_EGL_image_external : require\n#define INV_PI 0.3183\nprecision highp float;varying vec3 pos;uniform samplerExternalOES uSplr;uniform mat3 uVMat;uniform float uFOVx;uniform float uFOVy;void main() {  vec3 ray = vec3(pos.x * tan(uFOVx), pos.y * tan(uFOVy), -1);  ray = (uVMat * ray).xyz;  ray = normalize(ray);  vec2 texCrd = vec2(    0.5 + atan(ray.x, - ray.z) * INV_PI * 0.5, acos(ray.y) * INV_PI);  gl_FragColor = vec4(texture2D(uSplr, texCrd).xyz, 1.0);}";
  }
  
  public void onFrameAvailable(SurfaceTexture arg1)
  {
    this.zzcds += 1;
    synchronized (this.zzcdv)
    {
      this.zzcdv.notifyAll();
      return;
    }
  }
  
  public void run()
  {
    int i = 1;
    if (this.zzcdp == null)
    {
      zzkx.e("SphericalVideoProcessor started with no output texture.");
      this.zzcdu.countDown();
      return;
    }
    boolean bool = zzqy();
    int j = zzqw();
    if (this.zzcdq != 0)
    {
      if ((bool) && (i != 0)) {
        break label139;
      }
      ??? = String.valueOf(GLUtils.getEGLErrorString(this.zzcdw.eglGetError()));
      if (((String)???).length() == 0) {
        break label124;
      }
    }
    label124:
    for (??? = "EGL initialization failed: ".concat((String)???);; ??? = new String("EGL initialization failed: "))
    {
      zzkx.e((String)???);
      zzu.zzgq().zza(new Throwable((String)???), "SphericalVideoProcessor.run.1");
      zzrc();
      this.zzcdu.countDown();
      return;
      i = 0;
      break;
    }
    label139:
    this.zzcdo = new SurfaceTexture(j);
    this.zzcdo.setOnFrameAvailableListener(this);
    this.zzcdu.countDown();
    this.zzcde.start();
    try
    {
      this.zzcea = true;
      for (;;)
      {
        if (!this.zzceb)
        {
          zzqu();
          if (this.zzcea)
          {
            zzqv();
            this.zzcea = false;
          }
          try
          {
            synchronized (this.zzcdv)
            {
              if ((!this.zzceb) && (!this.zzcea) && (this.zzcds == 0)) {
                this.zzcdv.wait();
              }
            }
          }
          catch (InterruptedException localInterruptedException) {}
        }
      }
      return;
    }
    catch (IllegalStateException localIllegalStateException)
    {
      zzkx.zzdi("SphericalVideoProcessor halted unexpectedly.");
      return;
    }
    catch (Throwable localThrowable)
    {
      zzkx.zzb("SphericalVideoProcessor died.", localThrowable);
      zzu.zzgq().zza(localThrowable, "SphericalVideoProcessor.run.2");
      return;
    }
    finally
    {
      this.zzcde.stop();
      this.zzcdo.setOnFrameAvailableListener(null);
      this.zzcdo = null;
      zzrc();
    }
  }
  
  public void zza(SurfaceTexture paramSurfaceTexture, int paramInt1, int paramInt2)
  {
    this.zzakh = paramInt1;
    this.zzaki = paramInt2;
    this.zzcdp = paramSurfaceTexture;
  }
  
  public void zzb(float paramFloat1, float paramFloat2)
  {
    float f;
    if (this.zzakh > this.zzaki)
    {
      paramFloat1 = 1.7453293F * paramFloat1 / this.zzakh;
      f = 1.7453293F * paramFloat2 / this.zzakh;
      paramFloat2 = paramFloat1;
      paramFloat1 = f;
    }
    for (;;)
    {
      this.zzcdm -= paramFloat2;
      this.zzcdn -= paramFloat1;
      if (this.zzcdn < -1.5707964F) {
        this.zzcdn = -1.5707964F;
      }
      if (this.zzcdn > 1.5707964F) {
        this.zzcdn = 1.5707964F;
      }
      return;
      f = 1.7453293F * paramFloat1 / this.zzaki;
      paramFloat1 = 1.7453293F * paramFloat2 / this.zzaki;
      paramFloat2 = f;
    }
  }
  
  public void zzi(int paramInt1, int paramInt2)
  {
    synchronized (this.zzcdv)
    {
      this.zzakh = paramInt1;
      this.zzaki = paramInt2;
      this.zzcea = true;
      this.zzcdv.notifyAll();
      return;
    }
  }
  
  public void zzpr()
  {
    synchronized (this.zzcdv)
    {
      this.zzcdv.notifyAll();
      return;
    }
  }
  
  public void zzqs()
  {
    synchronized (this.zzcdv)
    {
      this.zzceb = true;
      this.zzcdp = null;
      this.zzcdv.notifyAll();
      return;
    }
  }
  
  public SurfaceTexture zzqt()
  {
    if (this.zzcdp == null) {
      return null;
    }
    try
    {
      this.zzcdu.await();
      return this.zzcdo;
    }
    catch (InterruptedException localInterruptedException)
    {
      for (;;) {}
    }
  }
  
  void zzqu()
  {
    while (this.zzcds > 0)
    {
      this.zzcdo.updateTexImage();
      this.zzcds -= 1;
    }
    if (this.zzcde.zzb(this.zzccz))
    {
      if (Float.isNaN(this.zzcdl)) {
        this.zzcdl = (-zzc(this.zzccz));
      }
      zzb(this.zzcdj, this.zzcdl + this.zzcdm);
    }
    for (;;)
    {
      zza(this.zzcdf, 1.5707964F);
      zza(this.zzcdg, this.zzcdj, this.zzcdf);
      zza(this.zzcdh, this.zzccz, this.zzcdg);
      zza(this.zzcdi, this.zzcdn);
      zza(this.zzcdk, this.zzcdi, this.zzcdh);
      GLES20.glUniformMatrix3fv(this.zzcdr, 1, false, this.zzcdk, 0);
      GLES20.glDrawArrays(5, 0, 4);
      zzcf("drawArrays");
      GLES20.glFinish();
      this.zzcdw.eglSwapBuffers(this.zzcdx, this.zzcdz);
      return;
      zza(this.zzccz, -1.5707964F);
      zzb(this.zzcdj, this.zzcdm);
    }
  }
  
  int zzqw()
  {
    this.zzcdq = zzqx();
    GLES20.glUseProgram(this.zzcdq);
    zzcf("useProgram");
    int i = GLES20.glGetAttribLocation(this.zzcdq, "aPosition");
    GLES20.glVertexAttribPointer(i, 3, 5126, false, 12, this.zzcdt);
    zzcf("vertexAttribPointer");
    GLES20.glEnableVertexAttribArray(i);
    zzcf("enableVertexAttribArray");
    int[] arrayOfInt = new int[1];
    GLES20.glGenTextures(1, arrayOfInt, 0);
    zzcf("genTextures");
    i = arrayOfInt[0];
    GLES20.glBindTexture(36197, i);
    zzcf("bindTextures");
    GLES20.glTexParameteri(36197, 10240, 9729);
    zzcf("texParameteri");
    GLES20.glTexParameteri(36197, 10241, 9729);
    zzcf("texParameteri");
    GLES20.glTexParameteri(36197, 10242, 33071);
    zzcf("texParameteri");
    GLES20.glTexParameteri(36197, 10243, 33071);
    zzcf("texParameteri");
    this.zzcdr = GLES20.glGetUniformLocation(this.zzcdq, "uVMat");
    GLES20.glUniformMatrix3fv(this.zzcdr, 1, false, new float[] { 1.0F, 0.0F, 0.0F, 0.0F, 1.0F, 0.0F, 0.0F, 0.0F, 1.0F }, 0);
    return i;
  }
  
  boolean zzqy()
  {
    this.zzcdw = ((EGL10)EGLContext.getEGL());
    this.zzcdx = this.zzcdw.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
    if (this.zzcdx == EGL10.EGL_NO_DISPLAY) {
      return false;
    }
    Object localObject = new int[2];
    if (!this.zzcdw.eglInitialize(this.zzcdx, (int[])localObject)) {
      return false;
    }
    localObject = zzqz();
    if (localObject == null) {
      return false;
    }
    this.zzcdy = this.zzcdw.eglCreateContext(this.zzcdx, (EGLConfig)localObject, EGL10.EGL_NO_CONTEXT, new int[] { 12440, 2, 12344 });
    if ((this.zzcdy == null) || (this.zzcdy == EGL10.EGL_NO_CONTEXT)) {
      return false;
    }
    this.zzcdz = this.zzcdw.eglCreateWindowSurface(this.zzcdx, (EGLConfig)localObject, this.zzcdp, null);
    if ((this.zzcdz == null) || (this.zzcdz == EGL10.EGL_NO_SURFACE)) {
      return false;
    }
    return this.zzcdw.eglMakeCurrent(this.zzcdx, this.zzcdz, this.zzcdz, this.zzcdy);
  }
  
  boolean zzrc()
  {
    boolean bool2 = false;
    boolean bool1 = bool2;
    if (this.zzcdz != null)
    {
      bool1 = bool2;
      if (this.zzcdz != EGL10.EGL_NO_SURFACE)
      {
        bool1 = this.zzcdw.eglMakeCurrent(this.zzcdx, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_CONTEXT) | false | this.zzcdw.eglDestroySurface(this.zzcdx, this.zzcdz);
        this.zzcdz = null;
      }
    }
    bool2 = bool1;
    if (this.zzcdy != null)
    {
      bool2 = bool1 | this.zzcdw.eglDestroyContext(this.zzcdx, this.zzcdy);
      this.zzcdy = null;
    }
    bool1 = bool2;
    if (this.zzcdx != null)
    {
      bool1 = bool2 | this.zzcdw.eglTerminate(this.zzcdx);
      this.zzcdx = null;
    }
    return bool1;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/overlay/zzx.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */