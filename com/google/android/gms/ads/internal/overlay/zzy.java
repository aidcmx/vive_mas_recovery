package com.google.android.gms.ads.internal.overlay;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.TextureView;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.common.util.zze;
import com.google.android.gms.internal.zzdn;
import com.google.android.gms.internal.zzdr;
import com.google.android.gms.internal.zzdv;
import com.google.android.gms.internal.zzdx;
import com.google.android.gms.internal.zzdz;
import com.google.android.gms.internal.zzji;
import com.google.android.gms.internal.zzkx;
import com.google.android.gms.internal.zzlb;
import com.google.android.gms.internal.zzlh;
import com.google.android.gms.internal.zzlh.zza;
import com.google.android.gms.internal.zzlh.zzb;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

@zzji
public class zzy
{
  private final Context mContext;
  private final VersionInfoParcel zzapc;
  @Nullable
  private final zzdz zzcbz;
  private boolean zzccd;
  private final String zzcec;
  @Nullable
  private final zzdx zzced;
  private final zzlh zzcee = new zzlh.zzb().zza("min_1", Double.MIN_VALUE, 1.0D).zza("1_5", 1.0D, 5.0D).zza("5_10", 5.0D, 10.0D).zza("10_20", 10.0D, 20.0D).zza("20_30", 20.0D, 30.0D).zza("30_max", 30.0D, Double.MAX_VALUE).zzwi();
  private final long[] zzcef;
  private final String[] zzceg;
  private boolean zzceh = false;
  private boolean zzcei = false;
  private boolean zzcej = false;
  private boolean zzcek = false;
  private zzi zzcel;
  private boolean zzcem;
  private boolean zzcen;
  private long zzceo = -1L;
  
  public zzy(Context paramContext, VersionInfoParcel paramVersionInfoParcel, String paramString, @Nullable zzdz paramzzdz, @Nullable zzdx paramzzdx)
  {
    this.mContext = paramContext;
    this.zzapc = paramVersionInfoParcel;
    this.zzcec = paramString;
    this.zzcbz = paramzzdz;
    this.zzced = paramzzdx;
    paramContext = (String)zzdr.zzbdv.get();
    if (paramContext == null)
    {
      this.zzceg = new String[0];
      this.zzcef = new long[0];
      return;
    }
    paramContext = TextUtils.split(paramContext, ",");
    this.zzceg = new String[paramContext.length];
    this.zzcef = new long[paramContext.length];
    int i = 0;
    while (i < paramContext.length) {
      try
      {
        this.zzcef[i] = Long.parseLong(paramContext[i]);
        i += 1;
      }
      catch (NumberFormatException paramVersionInfoParcel)
      {
        for (;;)
        {
          zzkx.zzc("Unable to parse frame hash target time number.", paramVersionInfoParcel);
          this.zzcef[i] = -1L;
        }
      }
    }
  }
  
  private void zzc(zzi paramzzi)
  {
    long l1 = ((Long)zzdr.zzbdw.get()).longValue();
    long l2 = paramzzi.getCurrentPosition();
    int i = 0;
    if (i < this.zzceg.length)
    {
      if (this.zzceg[i] != null) {}
      while (l1 <= Math.abs(l2 - this.zzcef[i]))
      {
        i += 1;
        break;
      }
      this.zzceg[i] = zza(paramzzi);
    }
  }
  
  private void zzrd()
  {
    if ((this.zzcej) && (!this.zzcek))
    {
      zzdv.zza(this.zzcbz, this.zzced, new String[] { "vff2" });
      this.zzcek = true;
    }
    long l = zzu.zzgs().nanoTime();
    if ((this.zzccd) && (this.zzcen) && (this.zzceo != -1L))
    {
      double d = TimeUnit.SECONDS.toNanos(1L) / (l - this.zzceo);
      this.zzcee.zza(d);
    }
    this.zzcen = this.zzccd;
    this.zzceo = l;
  }
  
  public void onStop()
  {
    if ((((Boolean)zzdr.zzbdu.get()).booleanValue()) && (!this.zzcem))
    {
      Bundle localBundle = new Bundle();
      localBundle.putString("type", "native-player-metrics");
      localBundle.putString("request", this.zzcec);
      localBundle.putString("player", this.zzcel.zzpg());
      Object localObject1 = this.zzcee.getBuckets().iterator();
      Object localObject2;
      String str1;
      if (((Iterator)localObject1).hasNext())
      {
        localObject2 = (zzlh.zza)((Iterator)localObject1).next();
        str1 = String.valueOf("fps_c_");
        String str2 = String.valueOf(((zzlh.zza)localObject2).name);
        if (str2.length() != 0)
        {
          str1 = str1.concat(str2);
          label129:
          localBundle.putString(str1, Integer.toString(((zzlh.zza)localObject2).count));
          str1 = String.valueOf("fps_p_");
          str2 = String.valueOf(((zzlh.zza)localObject2).name);
          if (str2.length() == 0) {
            break label202;
          }
        }
        label202:
        for (str1 = str1.concat(str2);; str1 = new String(str1))
        {
          localBundle.putString(str1, Double.toString(((zzlh.zza)localObject2).zzcwo));
          break;
          str1 = new String(str1);
          break label129;
        }
      }
      int i = 0;
      if (i < this.zzcef.length)
      {
        str1 = this.zzceg[i];
        if (str1 == null) {}
        for (;;)
        {
          i += 1;
          break;
          localObject1 = String.valueOf("fh_");
          localObject2 = String.valueOf(Long.valueOf(this.zzcef[i]));
          localBundle.putString(String.valueOf(localObject1).length() + 0 + String.valueOf(localObject2).length() + (String)localObject1 + (String)localObject2, str1);
        }
      }
      zzu.zzgm().zza(this.mContext, this.zzapc.zzda, "gmob-apps", localBundle, true);
      this.zzcem = true;
    }
  }
  
  @TargetApi(14)
  String zza(TextureView paramTextureView)
  {
    paramTextureView = paramTextureView.getBitmap(8, 8);
    long l2 = 0L;
    long l1 = 63L;
    int i = 0;
    while (i < 8)
    {
      long l3 = l1;
      int j = 0;
      l1 = l2;
      l2 = l3;
      if (j < 8)
      {
        int k = paramTextureView.getPixel(j, i);
        int m = Color.blue(k);
        int n = Color.red(k);
        if (Color.green(k) + (m + n) > 128) {}
        for (l3 = 1L;; l3 = 0L)
        {
          l1 |= l3 << (int)l2;
          l2 -= 1L;
          j += 1;
          break;
        }
      }
      i += 1;
      l3 = l1;
      l1 = l2;
      l2 = l3;
    }
    return String.format("%016X", new Object[] { Long.valueOf(l2) });
  }
  
  public void zza(zzi paramzzi)
  {
    zzdv.zza(this.zzcbz, this.zzced, new String[] { "vpc2" });
    this.zzceh = true;
    if (this.zzcbz != null) {
      this.zzcbz.zzg("vpn", paramzzi.zzpg());
    }
    this.zzcel = paramzzi;
  }
  
  public void zzb(zzi paramzzi)
  {
    zzrd();
    zzc(paramzzi);
  }
  
  public void zzqc()
  {
    if ((!this.zzceh) || (this.zzcei)) {
      return;
    }
    zzdv.zza(this.zzcbz, this.zzced, new String[] { "vfr2" });
    this.zzcei = true;
  }
  
  public void zzre()
  {
    this.zzccd = true;
    if ((this.zzcei) && (!this.zzcej))
    {
      zzdv.zza(this.zzcbz, this.zzced, new String[] { "vfp2" });
      this.zzcej = true;
    }
  }
  
  public void zzrf()
  {
    this.zzccd = false;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/overlay/zzy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */