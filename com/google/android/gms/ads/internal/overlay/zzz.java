package com.google.android.gms.ads.internal.overlay;

import android.os.Handler;
import com.google.android.gms.internal.zzji;
import com.google.android.gms.internal.zzlb;

@zzji
class zzz
  implements Runnable
{
  private boolean mCancelled = false;
  private zzk zzcep;
  
  zzz(zzk paramzzk)
  {
    this.zzcep = paramzzk;
  }
  
  public void cancel()
  {
    this.mCancelled = true;
    zzlb.zzcvl.removeCallbacks(this);
  }
  
  public void run()
  {
    if (!this.mCancelled)
    {
      this.zzcep.zzqk();
      zzrg();
    }
  }
  
  public void zzrg()
  {
    zzlb.zzcvl.postDelayed(this, 250L);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/overlay/zzz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */