package com.google.android.gms.ads.internal.purchase;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.dynamic.zzd;
import com.google.android.gms.dynamic.zzd.zza;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.internal.zzif;
import com.google.android.gms.internal.zzji;

@zzji
public final class GInAppPurchaseManagerInfoParcel
  extends AbstractSafeParcelable
  implements ReflectedParcelable
{
  public static final Parcelable.Creator<GInAppPurchaseManagerInfoParcel> CREATOR = new zza();
  public final int versionCode;
  public final zzk zzasf;
  public final zzif zzcfa;
  public final Context zzcfb;
  public final zzj zzcfc;
  
  GInAppPurchaseManagerInfoParcel(int paramInt, IBinder paramIBinder1, IBinder paramIBinder2, IBinder paramIBinder3, IBinder paramIBinder4)
  {
    this.versionCode = paramInt;
    this.zzasf = ((zzk)zze.zzae(zzd.zza.zzfd(paramIBinder1)));
    this.zzcfa = ((zzif)zze.zzae(zzd.zza.zzfd(paramIBinder2)));
    this.zzcfb = ((Context)zze.zzae(zzd.zza.zzfd(paramIBinder3)));
    this.zzcfc = ((zzj)zze.zzae(zzd.zza.zzfd(paramIBinder4)));
  }
  
  public GInAppPurchaseManagerInfoParcel(Context paramContext, zzk paramzzk, zzif paramzzif, zzj paramzzj)
  {
    this.versionCode = 2;
    this.zzcfb = paramContext;
    this.zzasf = paramzzk;
    this.zzcfa = paramzzif;
    this.zzcfc = paramzzj;
  }
  
  public static void zza(Intent paramIntent, GInAppPurchaseManagerInfoParcel paramGInAppPurchaseManagerInfoParcel)
  {
    Bundle localBundle = new Bundle(1);
    localBundle.putParcelable("com.google.android.gms.ads.internal.purchase.InAppPurchaseManagerInfo", paramGInAppPurchaseManagerInfoParcel);
    paramIntent.putExtra("com.google.android.gms.ads.internal.purchase.InAppPurchaseManagerInfo", localBundle);
  }
  
  public static GInAppPurchaseManagerInfoParcel zzc(Intent paramIntent)
  {
    try
    {
      paramIntent = paramIntent.getBundleExtra("com.google.android.gms.ads.internal.purchase.InAppPurchaseManagerInfo");
      paramIntent.setClassLoader(GInAppPurchaseManagerInfoParcel.class.getClassLoader());
      paramIntent = (GInAppPurchaseManagerInfoParcel)paramIntent.getParcelable("com.google.android.gms.ads.internal.purchase.InAppPurchaseManagerInfo");
      return paramIntent;
    }
    catch (Exception paramIntent) {}
    return null;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zza.zza(this, paramParcel, paramInt);
  }
  
  IBinder zzrn()
  {
    return zze.zzac(this.zzcfc).asBinder();
  }
  
  IBinder zzro()
  {
    return zze.zzac(this.zzasf).asBinder();
  }
  
  IBinder zzrp()
  {
    return zze.zzac(this.zzcfa).asBinder();
  }
  
  IBinder zzrq()
  {
    return zze.zzac(this.zzcfb).asBinder();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/purchase/GInAppPurchaseManagerInfoParcel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */