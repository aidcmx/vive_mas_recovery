package com.google.android.gms.ads.internal.purchase;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.SystemClock;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.common.stats.zza;
import com.google.android.gms.internal.zzik;
import com.google.android.gms.internal.zzji;
import com.google.android.gms.internal.zzkw;
import com.google.android.gms.internal.zzkx;
import com.google.android.gms.internal.zzlb;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

@zzji
public class zzc
  extends zzkw
  implements ServiceConnection
{
  private Context mContext;
  private final Object zzako = new Object();
  private zzik zzbta;
  private boolean zzcff = false;
  private zzb zzcfg;
  private zzh zzcfh;
  private List<zzf> zzcfi = null;
  private zzk zzcfj;
  
  public zzc(Context paramContext, zzik paramzzik, zzk paramzzk)
  {
    this(paramContext, paramzzik, paramzzk, new zzb(paramContext), zzh.zzq(paramContext.getApplicationContext()));
  }
  
  zzc(Context paramContext, zzik paramzzik, zzk paramzzk, zzb paramzzb, zzh paramzzh)
  {
    this.mContext = paramContext;
    this.zzbta = paramzzik;
    this.zzcfj = paramzzk;
    this.zzcfg = paramzzb;
    this.zzcfh = paramzzh;
    this.zzcfi = this.zzcfh.zzg(10L);
  }
  
  private void zze(long paramLong)
  {
    do
    {
      if (!zzf(paramLong)) {
        zzkx.v("Timeout waiting for pending transaction to be processed.");
      }
    } while (!this.zzcff);
  }
  
  private boolean zzf(long paramLong)
  {
    paramLong = 60000L - (SystemClock.elapsedRealtime() - paramLong);
    if (paramLong <= 0L) {
      return false;
    }
    try
    {
      this.zzako.wait(paramLong);
      return true;
    }
    catch (InterruptedException localInterruptedException)
    {
      for (;;)
      {
        zzkx.zzdi("waitWithTimeout_lock interrupted");
      }
    }
  }
  
  public void onServiceConnected(ComponentName arg1, IBinder paramIBinder)
  {
    synchronized (this.zzako)
    {
      this.zzcfg.zzav(paramIBinder);
      zzrr();
      this.zzcff = true;
      this.zzako.notify();
      return;
    }
  }
  
  public void onServiceDisconnected(ComponentName paramComponentName)
  {
    zzkx.zzdh("In-app billing service disconnected.");
    this.zzcfg.destroy();
  }
  
  public void onStop()
  {
    synchronized (this.zzako)
    {
      zza.zzaxr().zza(this.mContext, this);
      this.zzcfg.destroy();
      return;
    }
  }
  
  protected void zza(final zzf paramzzf, String paramString1, String paramString2)
  {
    final Intent localIntent = new Intent();
    zzu.zzha();
    localIntent.putExtra("RESPONSE_CODE", 0);
    zzu.zzha();
    localIntent.putExtra("INAPP_PURCHASE_DATA", paramString1);
    zzu.zzha();
    localIntent.putExtra("INAPP_DATA_SIGNATURE", paramString2);
    zzlb.zzcvl.post(new Runnable()
    {
      public void run()
      {
        try
        {
          if (zzc.zza(zzc.this).zza(paramzzf.zzcfu, -1, localIntent))
          {
            zzc.zzc(zzc.this).zza(new zzg(zzc.zzb(zzc.this), paramzzf.zzcfv, true, -1, localIntent, paramzzf));
            return;
          }
          zzc.zzc(zzc.this).zza(new zzg(zzc.zzb(zzc.this), paramzzf.zzcfv, false, -1, localIntent, paramzzf));
          return;
        }
        catch (RemoteException localRemoteException)
        {
          zzkx.zzdi("Fail to verify and dispatch pending transaction");
        }
      }
    });
  }
  
  public void zzfp()
  {
    synchronized (this.zzako)
    {
      Intent localIntent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
      localIntent.setPackage("com.android.vending");
      zza.zzaxr().zza(this.mContext, localIntent, this, 1);
      zze(SystemClock.elapsedRealtime());
      zza.zzaxr().zza(this.mContext, this);
      this.zzcfg.destroy();
      return;
    }
  }
  
  protected void zzrr()
  {
    if (this.zzcfi.isEmpty()) {
      return;
    }
    HashMap localHashMap = new HashMap();
    Object localObject1 = this.zzcfi.iterator();
    Object localObject2;
    while (((Iterator)localObject1).hasNext())
    {
      localObject2 = (zzf)((Iterator)localObject1).next();
      localHashMap.put(((zzf)localObject2).zzcfv, localObject2);
    }
    localObject1 = null;
    for (;;)
    {
      localObject1 = this.zzcfg.zzm(this.mContext.getPackageName(), (String)localObject1);
      if (localObject1 == null) {}
      do
      {
        do
        {
          localObject1 = localHashMap.keySet().iterator();
          while (((Iterator)localObject1).hasNext())
          {
            localObject2 = (String)((Iterator)localObject1).next();
            this.zzcfh.zza((zzf)localHashMap.get(localObject2));
          }
          break;
        } while (zzu.zzha().zzd((Bundle)localObject1) != 0);
        localObject2 = ((Bundle)localObject1).getStringArrayList("INAPP_PURCHASE_ITEM_LIST");
        ArrayList localArrayList1 = ((Bundle)localObject1).getStringArrayList("INAPP_PURCHASE_DATA_LIST");
        ArrayList localArrayList2 = ((Bundle)localObject1).getStringArrayList("INAPP_DATA_SIGNATURE_LIST");
        localObject1 = ((Bundle)localObject1).getString("INAPP_CONTINUATION_TOKEN");
        int i = 0;
        while (i < ((ArrayList)localObject2).size())
        {
          if (localHashMap.containsKey(((ArrayList)localObject2).get(i)))
          {
            String str1 = (String)((ArrayList)localObject2).get(i);
            String str2 = (String)localArrayList1.get(i);
            String str3 = (String)localArrayList2.get(i);
            zzf localzzf = (zzf)localHashMap.get(str1);
            String str4 = zzu.zzha().zzcg(str2);
            if (localzzf.zzcfu.equals(str4))
            {
              zza(localzzf, str2, str3);
              localHashMap.remove(str1);
            }
          }
          i += 1;
        }
      } while ((localObject1 == null) || (localHashMap.isEmpty()));
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/purchase/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */