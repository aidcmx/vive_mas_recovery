package com.google.android.gms.ads.internal.purchase;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build.VERSION;
import android.os.SystemClock;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.internal.zzif.zza;
import com.google.android.gms.internal.zzji;
import com.google.android.gms.internal.zzkr;
import com.google.android.gms.internal.zzks;
import com.google.android.gms.internal.zzkx;
import com.google.android.gms.internal.zzlb;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

@zzji
public class zzd
  extends zzif.zza
{
  private Context mContext;
  private String zzasx;
  private String zzcfm;
  private ArrayList<String> zzcfn;
  
  public zzd(String paramString1, ArrayList<String> paramArrayList, Context paramContext, String paramString2)
  {
    this.zzcfm = paramString1;
    this.zzcfn = paramArrayList;
    this.zzasx = paramString2;
    this.mContext = paramContext;
  }
  
  public String getProductId()
  {
    return this.zzcfm;
  }
  
  public void recordPlayBillingResolution(int paramInt)
  {
    if (paramInt == 0) {
      zzrt();
    }
    Map localMap = zzrs();
    localMap.put("google_play_status", String.valueOf(paramInt));
    localMap.put("sku", this.zzcfm);
    localMap.put("status", String.valueOf(zzan(paramInt)));
    LinkedList localLinkedList = new LinkedList();
    Iterator localIterator = this.zzcfn.iterator();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      localLinkedList.add(zzu.zzgm().zzc(str, localMap));
    }
    zzu.zzgm().zza(this.mContext, this.zzasx, localLinkedList);
  }
  
  public void recordResolution(int paramInt)
  {
    if (paramInt == 1) {
      zzrt();
    }
    Map localMap = zzrs();
    localMap.put("status", String.valueOf(paramInt));
    localMap.put("sku", this.zzcfm);
    LinkedList localLinkedList = new LinkedList();
    Iterator localIterator = this.zzcfn.iterator();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      localLinkedList.add(zzu.zzgm().zzc(str, localMap));
    }
    zzu.zzgm().zza(this.mContext, this.zzasx, localLinkedList);
  }
  
  protected int zzan(int paramInt)
  {
    if (paramInt == 0) {
      return 1;
    }
    if (paramInt == 1) {
      return 2;
    }
    if (paramInt == 4) {
      return 3;
    }
    return 0;
  }
  
  Map<String, String> zzrs()
  {
    String str = this.mContext.getPackageName();
    Object localObject1 = "";
    try
    {
      localObject2 = this.mContext.getPackageManager().getPackageInfo(str, 0).versionName;
      localObject1 = localObject2;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      for (;;)
      {
        Object localObject2;
        long l1;
        long l2;
        zzkx.zzc("Error to retrieve app version", localNameNotFoundException);
      }
    }
    l1 = zzu.zzgq().zzut().zzvk();
    l2 = SystemClock.elapsedRealtime();
    localObject2 = new HashMap();
    ((Map)localObject2).put("sessionid", zzu.zzgq().getSessionId());
    ((Map)localObject2).put("appid", str);
    ((Map)localObject2).put("osversion", String.valueOf(Build.VERSION.SDK_INT));
    ((Map)localObject2).put("sdkversion", this.zzasx);
    ((Map)localObject2).put("appversion", localObject1);
    ((Map)localObject2).put("timestamp", String.valueOf(l2 - l1));
    return (Map<String, String>)localObject2;
  }
  
  void zzrt()
  {
    try
    {
      this.mContext.getClassLoader().loadClass("com.google.ads.conversiontracking.IAPConversionReporter").getDeclaredMethod("reportWithProductId", new Class[] { Context.class, String.class, String.class, Boolean.TYPE }).invoke(null, new Object[] { this.mContext, this.zzcfm, "", Boolean.valueOf(true) });
      return;
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      zzkx.zzdi("Google Conversion Tracking SDK 1.2.0 or above is required to report a conversion.");
      return;
    }
    catch (NoSuchMethodException localNoSuchMethodException)
    {
      zzkx.zzdi("Google Conversion Tracking SDK 1.2.0 or above is required to report a conversion.");
      return;
    }
    catch (Exception localException)
    {
      zzkx.zzc("Fail to report a conversion.", localException);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/purchase/zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */