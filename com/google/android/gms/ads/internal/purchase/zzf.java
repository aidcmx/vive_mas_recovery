package com.google.android.gms.ads.internal.purchase;

import com.google.android.gms.internal.zzji;

@zzji
public final class zzf
{
  public long zzcft;
  public final String zzcfu;
  public final String zzcfv;
  
  public zzf(long paramLong, String paramString1, String paramString2)
  {
    this.zzcft = paramLong;
    this.zzcfv = paramString1;
    this.zzcfu = paramString2;
  }
  
  public zzf(String paramString1, String paramString2)
  {
    this(-1L, paramString1, paramString2);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/purchase/zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */