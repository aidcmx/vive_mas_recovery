package com.google.android.gms.ads.internal.purchase;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.common.stats.zza;
import com.google.android.gms.internal.zzij.zza;
import com.google.android.gms.internal.zzji;
import com.google.android.gms.internal.zzkx;

@zzji
public final class zzg
  extends zzij.zza
  implements ServiceConnection
{
  private Context mContext;
  private int mResultCode;
  zzb zzcfg;
  private String zzcfm;
  private zzf zzcfq;
  private boolean zzcfw = false;
  private Intent zzcfx;
  
  public zzg(Context paramContext, String paramString, boolean paramBoolean, int paramInt, Intent paramIntent, zzf paramzzf)
  {
    this.zzcfm = paramString;
    this.mResultCode = paramInt;
    this.zzcfx = paramIntent;
    this.zzcfw = paramBoolean;
    this.mContext = paramContext;
    this.zzcfq = paramzzf;
  }
  
  public void finishPurchase()
  {
    int i = zzu.zzha().zzd(this.zzcfx);
    if ((this.mResultCode != -1) || (i != 0)) {
      return;
    }
    this.zzcfg = new zzb(this.mContext);
    Intent localIntent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
    localIntent.setPackage("com.android.vending");
    zza.zzaxr().zza(this.mContext, localIntent, this, 1);
  }
  
  public String getProductId()
  {
    return this.zzcfm;
  }
  
  public Intent getPurchaseData()
  {
    return this.zzcfx;
  }
  
  public int getResultCode()
  {
    return this.mResultCode;
  }
  
  public boolean isVerified()
  {
    return this.zzcfw;
  }
  
  public void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder)
  {
    zzkx.zzdh("In-app billing service connected.");
    this.zzcfg.zzav(paramIBinder);
    paramComponentName = zzu.zzha().zze(this.zzcfx);
    paramComponentName = zzu.zzha().zzch(paramComponentName);
    if (paramComponentName == null) {
      return;
    }
    if (this.zzcfg.zzl(this.mContext.getPackageName(), paramComponentName) == 0) {
      zzh.zzq(this.mContext).zza(this.zzcfq);
    }
    zza.zzaxr().zza(this.mContext, this);
    this.zzcfg.destroy();
  }
  
  public void onServiceDisconnected(ComponentName paramComponentName)
  {
    zzkx.zzdh("In-app billing service disconnected.");
    this.zzcfg.destroy();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/purchase/zzg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */