package com.google.android.gms.ads.internal.purchase;

import android.content.Intent;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.internal.zzji;
import com.google.android.gms.internal.zzkx;
import com.google.android.gms.internal.zzlb;

@zzji
public class zzk
{
  private final String zzbbj;
  
  public zzk(String paramString)
  {
    this.zzbbj = paramString;
  }
  
  public boolean zza(String paramString, int paramInt, Intent paramIntent)
  {
    if ((paramString == null) || (paramIntent == null)) {}
    String str;
    do
    {
      return false;
      str = zzu.zzha().zze(paramIntent);
      paramIntent = zzu.zzha().zzf(paramIntent);
    } while ((str == null) || (paramIntent == null));
    if (!paramString.equals(zzu.zzha().zzcg(str)))
    {
      zzkx.zzdi("Developer payload not match.");
      return false;
    }
    if ((this.zzbbj != null) && (!zzl.zzc(this.zzbbj, str, paramIntent)))
    {
      zzkx.zzdi("Fail to verify signature.");
      return false;
    }
    return true;
  }
  
  public String zzrv()
  {
    return zzu.zzgm().zzvs();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/purchase/zzk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */