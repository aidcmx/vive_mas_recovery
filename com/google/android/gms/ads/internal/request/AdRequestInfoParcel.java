package com.google.android.gms.ads.internal.request;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.os.Messenger;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.Nullable;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.internal.zzji;
import java.util.Collections;
import java.util.List;

@zzji
public final class AdRequestInfoParcel
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<AdRequestInfoParcel> CREATOR = new zzf();
  public final ApplicationInfo applicationInfo;
  public final int versionCode;
  public final String zzarf;
  public final String zzarg;
  public final VersionInfoParcel zzari;
  public final AdSizeParcel zzarm;
  public final NativeAdOptionsParcel zzasa;
  public final List<String> zzase;
  public final float zzavd;
  public final boolean zzbvo;
  @Nullable
  public final Bundle zzcjt;
  public final AdRequestParcel zzcju;
  @Nullable
  public final PackageInfo zzcjv;
  public final String zzcjw;
  public final String zzcjx;
  public final String zzcjy;
  public final Bundle zzcjz;
  public final int zzcka;
  public final Bundle zzckb;
  public final boolean zzckc;
  public final Messenger zzckd;
  public final int zzcke;
  public final int zzckf;
  public final String zzckg;
  public final long zzckh;
  public final String zzcki;
  @Nullable
  public final List<String> zzckj;
  public final List<String> zzckk;
  public final long zzckl;
  public final CapabilityParcel zzckm;
  public final String zzckn;
  public final float zzcko;
  public final int zzckp;
  public final int zzckq;
  public final boolean zzckr;
  public final boolean zzcks;
  public final String zzckt;
  public final boolean zzcku;
  public final String zzckv;
  public final int zzckw;
  public final Bundle zzckx;
  public final String zzcky;
  
  AdRequestInfoParcel(int paramInt1, Bundle paramBundle1, AdRequestParcel paramAdRequestParcel, AdSizeParcel paramAdSizeParcel, String paramString1, ApplicationInfo paramApplicationInfo, PackageInfo paramPackageInfo, String paramString2, String paramString3, String paramString4, VersionInfoParcel paramVersionInfoParcel, Bundle paramBundle2, int paramInt2, List<String> paramList1, Bundle paramBundle3, boolean paramBoolean1, Messenger paramMessenger, int paramInt3, int paramInt4, float paramFloat1, String paramString5, long paramLong1, String paramString6, List<String> paramList2, String paramString7, NativeAdOptionsParcel paramNativeAdOptionsParcel, List<String> paramList3, long paramLong2, CapabilityParcel paramCapabilityParcel, String paramString8, float paramFloat2, boolean paramBoolean2, int paramInt5, int paramInt6, boolean paramBoolean3, boolean paramBoolean4, String paramString9, String paramString10, boolean paramBoolean5, int paramInt7, Bundle paramBundle4, String paramString11)
  {
    this.versionCode = paramInt1;
    this.zzcjt = paramBundle1;
    this.zzcju = paramAdRequestParcel;
    this.zzarm = paramAdSizeParcel;
    this.zzarg = paramString1;
    this.applicationInfo = paramApplicationInfo;
    this.zzcjv = paramPackageInfo;
    this.zzcjw = paramString2;
    this.zzcjx = paramString3;
    this.zzcjy = paramString4;
    this.zzari = paramVersionInfoParcel;
    this.zzcjz = paramBundle2;
    this.zzcka = paramInt2;
    this.zzase = paramList1;
    if (paramList3 == null)
    {
      paramBundle1 = Collections.emptyList();
      this.zzckk = paramBundle1;
      this.zzckb = paramBundle3;
      this.zzckc = paramBoolean1;
      this.zzckd = paramMessenger;
      this.zzcke = paramInt3;
      this.zzckf = paramInt4;
      this.zzavd = paramFloat1;
      this.zzckg = paramString5;
      this.zzckh = paramLong1;
      this.zzcki = paramString6;
      if (paramList2 != null) {
        break label279;
      }
    }
    label279:
    for (paramBundle1 = Collections.emptyList();; paramBundle1 = Collections.unmodifiableList(paramList2))
    {
      this.zzckj = paramBundle1;
      this.zzarf = paramString7;
      this.zzasa = paramNativeAdOptionsParcel;
      this.zzckl = paramLong2;
      this.zzckm = paramCapabilityParcel;
      this.zzckn = paramString8;
      this.zzcko = paramFloat2;
      this.zzcku = paramBoolean2;
      this.zzckp = paramInt5;
      this.zzckq = paramInt6;
      this.zzckr = paramBoolean3;
      this.zzcks = paramBoolean4;
      this.zzckt = paramString9;
      this.zzckv = paramString10;
      this.zzbvo = paramBoolean5;
      this.zzckw = paramInt7;
      this.zzckx = paramBundle4;
      this.zzcky = paramString11;
      return;
      paramBundle1 = Collections.unmodifiableList(paramList3);
      break;
    }
  }
  
  public AdRequestInfoParcel(@Nullable Bundle paramBundle1, AdRequestParcel paramAdRequestParcel, AdSizeParcel paramAdSizeParcel, String paramString1, ApplicationInfo paramApplicationInfo, @Nullable PackageInfo paramPackageInfo, String paramString2, String paramString3, String paramString4, VersionInfoParcel paramVersionInfoParcel, Bundle paramBundle2, int paramInt1, List<String> paramList1, List<String> paramList2, Bundle paramBundle3, boolean paramBoolean1, Messenger paramMessenger, int paramInt2, int paramInt3, float paramFloat1, String paramString5, long paramLong1, String paramString6, @Nullable List<String> paramList3, String paramString7, NativeAdOptionsParcel paramNativeAdOptionsParcel, long paramLong2, CapabilityParcel paramCapabilityParcel, String paramString8, float paramFloat2, boolean paramBoolean2, int paramInt4, int paramInt5, boolean paramBoolean3, boolean paramBoolean4, String paramString9, String paramString10, boolean paramBoolean5, int paramInt6, Bundle paramBundle4, String paramString11)
  {
    this(19, paramBundle1, paramAdRequestParcel, paramAdSizeParcel, paramString1, paramApplicationInfo, paramPackageInfo, paramString2, paramString3, paramString4, paramVersionInfoParcel, paramBundle2, paramInt1, paramList1, paramBundle3, paramBoolean1, paramMessenger, paramInt2, paramInt3, paramFloat1, paramString5, paramLong1, paramString6, paramList3, paramString7, paramNativeAdOptionsParcel, paramList2, paramLong2, paramCapabilityParcel, paramString8, paramFloat2, paramBoolean2, paramInt4, paramInt5, paramBoolean3, paramBoolean4, paramString9, paramString10, paramBoolean5, paramInt6, paramBundle4, paramString11);
  }
  
  public AdRequestInfoParcel(zza paramzza, String paramString, long paramLong)
  {
    this(paramzza.zzcjt, paramzza.zzcju, paramzza.zzarm, paramzza.zzarg, paramzza.applicationInfo, paramzza.zzcjv, paramString, paramzza.zzcjx, paramzza.zzcjy, paramzza.zzari, paramzza.zzcjz, paramzza.zzcka, paramzza.zzase, paramzza.zzckk, paramzza.zzckb, paramzza.zzckc, paramzza.zzckd, paramzza.zzcke, paramzza.zzckf, paramzza.zzavd, paramzza.zzckg, paramzza.zzckh, paramzza.zzcki, paramzza.zzckj, paramzza.zzarf, paramzza.zzasa, paramLong, paramzza.zzckm, paramzza.zzckn, paramzza.zzcko, paramzza.zzcku, paramzza.zzckp, paramzza.zzckq, paramzza.zzckr, paramzza.zzcks, paramzza.zzckt, paramzza.zzckv, paramzza.zzbvo, paramzza.zzckw, paramzza.zzckx, paramzza.zzcky);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzf.zza(this, paramParcel, paramInt);
  }
  
  @zzji
  public static final class zza
  {
    public final ApplicationInfo applicationInfo;
    public final String zzarf;
    public final String zzarg;
    public final VersionInfoParcel zzari;
    public final AdSizeParcel zzarm;
    public final NativeAdOptionsParcel zzasa;
    public final List<String> zzase;
    public final float zzavd;
    public final boolean zzbvo;
    @Nullable
    public final Bundle zzcjt;
    public final AdRequestParcel zzcju;
    @Nullable
    public final PackageInfo zzcjv;
    public final String zzcjx;
    public final String zzcjy;
    public final Bundle zzcjz;
    public final int zzcka;
    public final Bundle zzckb;
    public final boolean zzckc;
    public final Messenger zzckd;
    public final int zzcke;
    public final int zzckf;
    public final String zzckg;
    public final long zzckh;
    public final String zzcki;
    @Nullable
    public final List<String> zzckj;
    public final List<String> zzckk;
    public final CapabilityParcel zzckm;
    public final String zzckn;
    public final float zzcko;
    public final int zzckp;
    public final int zzckq;
    public final boolean zzckr;
    public final boolean zzcks;
    public final String zzckt;
    public final boolean zzcku;
    public final String zzckv;
    public final int zzckw;
    public final Bundle zzckx;
    public final String zzcky;
    
    public zza(@Nullable Bundle paramBundle1, AdRequestParcel paramAdRequestParcel, AdSizeParcel paramAdSizeParcel, String paramString1, ApplicationInfo paramApplicationInfo, @Nullable PackageInfo paramPackageInfo, String paramString2, String paramString3, VersionInfoParcel paramVersionInfoParcel, Bundle paramBundle2, List<String> paramList1, List<String> paramList2, Bundle paramBundle3, boolean paramBoolean1, Messenger paramMessenger, int paramInt1, int paramInt2, float paramFloat1, String paramString4, long paramLong, String paramString5, @Nullable List<String> paramList3, String paramString6, NativeAdOptionsParcel paramNativeAdOptionsParcel, CapabilityParcel paramCapabilityParcel, String paramString7, float paramFloat2, boolean paramBoolean2, int paramInt3, int paramInt4, boolean paramBoolean3, boolean paramBoolean4, String paramString8, String paramString9, boolean paramBoolean5, int paramInt5, Bundle paramBundle4, String paramString10)
    {
      this.zzcjt = paramBundle1;
      this.zzcju = paramAdRequestParcel;
      this.zzarm = paramAdSizeParcel;
      this.zzarg = paramString1;
      this.applicationInfo = paramApplicationInfo;
      this.zzcjv = paramPackageInfo;
      this.zzcjx = paramString2;
      this.zzcjy = paramString3;
      this.zzari = paramVersionInfoParcel;
      this.zzcjz = paramBundle2;
      this.zzckc = paramBoolean1;
      this.zzckd = paramMessenger;
      this.zzcke = paramInt1;
      this.zzckf = paramInt2;
      this.zzavd = paramFloat1;
      if ((paramList1 != null) && (paramList1.size() > 0))
      {
        this.zzcka = 3;
        this.zzase = paramList1;
      }
      for (this.zzckk = paramList2;; this.zzckk = null)
      {
        this.zzckb = paramBundle3;
        this.zzckg = paramString4;
        this.zzckh = paramLong;
        this.zzcki = paramString5;
        this.zzckj = paramList3;
        this.zzarf = paramString6;
        this.zzasa = paramNativeAdOptionsParcel;
        this.zzckm = paramCapabilityParcel;
        this.zzckn = paramString7;
        this.zzcko = paramFloat2;
        this.zzcku = paramBoolean2;
        this.zzckp = paramInt3;
        this.zzckq = paramInt4;
        this.zzckr = paramBoolean3;
        this.zzcks = paramBoolean4;
        this.zzckt = paramString8;
        this.zzckv = paramString9;
        this.zzbvo = paramBoolean5;
        this.zzckw = paramInt5;
        this.zzckx = paramBundle4;
        this.zzcky = paramString10;
        return;
        this.zzcka = 0;
        this.zzase = null;
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/request/AdRequestInfoParcel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */