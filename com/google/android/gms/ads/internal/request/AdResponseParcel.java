package com.google.android.gms.ads.internal.request;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.reward.mediation.client.RewardItemParcel;
import com.google.android.gms.ads.internal.safebrowsing.SafeBrowsingConfigParcel;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.internal.zzji;
import java.util.Collections;
import java.util.List;

@zzji
public final class AdResponseParcel
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<AdResponseParcel> CREATOR = new zzh();
  public String body;
  public final int errorCode;
  public final int orientation;
  public final int versionCode;
  public final boolean zzazt;
  public final boolean zzazu;
  public final boolean zzazv;
  public final List<String> zzbvk;
  public final List<String> zzbvl;
  public final List<String> zzbvn;
  public final boolean zzbvo;
  public final long zzbvq;
  private AdRequestInfoParcel zzbws;
  public final String zzcbo;
  public final boolean zzckc;
  public final boolean zzcks;
  @Nullable
  public String zzckt;
  public final long zzcla;
  public final boolean zzclb;
  public final long zzclc;
  public final List<String> zzcld;
  public final String zzcle;
  public final long zzclf;
  public final String zzclg;
  public final boolean zzclh;
  public final String zzcli;
  public final String zzclj;
  public final boolean zzclk;
  public final boolean zzcll;
  public final boolean zzclm;
  public LargeParcelTeleporter zzcln;
  public String zzclo;
  public final String zzclp;
  @Nullable
  public final RewardItemParcel zzclq;
  @Nullable
  public final List<String> zzclr;
  @Nullable
  public final List<String> zzcls;
  public final boolean zzclt;
  @Nullable
  public final AutoClickProtectionConfigurationParcel zzclu;
  @Nullable
  public final String zzclv;
  @Nullable
  public final SafeBrowsingConfigParcel zzclw;
  @Nullable
  public final String zzclx;
  public final boolean zzcly;
  
  public AdResponseParcel(int paramInt)
  {
    this(18, null, null, null, paramInt, null, -1L, false, -1L, null, -1L, -1, null, -1L, null, false, null, null, false, false, false, true, false, null, null, null, false, false, null, null, null, false, null, false, null, null, false, null, null, null, true);
  }
  
  public AdResponseParcel(int paramInt, long paramLong)
  {
    this(18, null, null, null, paramInt, null, -1L, false, -1L, null, paramLong, -1, null, -1L, null, false, null, null, false, false, false, true, false, null, null, null, false, false, null, null, null, false, null, false, null, null, false, null, null, null, true);
  }
  
  AdResponseParcel(int paramInt1, String paramString1, String paramString2, List<String> paramList1, int paramInt2, List<String> paramList2, long paramLong1, boolean paramBoolean1, long paramLong2, List<String> paramList3, long paramLong3, int paramInt3, String paramString3, long paramLong4, String paramString4, boolean paramBoolean2, String paramString5, String paramString6, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5, boolean paramBoolean6, boolean paramBoolean7, LargeParcelTeleporter paramLargeParcelTeleporter, String paramString7, String paramString8, boolean paramBoolean8, boolean paramBoolean9, RewardItemParcel paramRewardItemParcel, List<String> paramList4, List<String> paramList5, boolean paramBoolean10, AutoClickProtectionConfigurationParcel paramAutoClickProtectionConfigurationParcel, boolean paramBoolean11, String paramString9, List<String> paramList6, boolean paramBoolean12, String paramString10, SafeBrowsingConfigParcel paramSafeBrowsingConfigParcel, String paramString11, boolean paramBoolean13)
  {
    this.versionCode = paramInt1;
    this.zzcbo = paramString1;
    this.body = paramString2;
    if (paramList1 != null)
    {
      paramString1 = Collections.unmodifiableList(paramList1);
      this.zzbvk = paramString1;
      this.errorCode = paramInt2;
      if (paramList2 == null) {
        break label333;
      }
      paramString1 = Collections.unmodifiableList(paramList2);
      label52:
      this.zzbvl = paramString1;
      this.zzcla = paramLong1;
      this.zzclb = paramBoolean1;
      this.zzclc = paramLong2;
      if (paramList3 == null) {
        break label338;
      }
    }
    label333:
    label338:
    for (paramString1 = Collections.unmodifiableList(paramList3);; paramString1 = null)
    {
      this.zzcld = paramString1;
      this.zzbvq = paramLong3;
      this.orientation = paramInt3;
      this.zzcle = paramString3;
      this.zzclf = paramLong4;
      this.zzclg = paramString4;
      this.zzclh = paramBoolean2;
      this.zzcli = paramString5;
      this.zzclj = paramString6;
      this.zzclk = paramBoolean3;
      this.zzazt = paramBoolean4;
      this.zzckc = paramBoolean5;
      this.zzcll = paramBoolean6;
      this.zzcly = paramBoolean13;
      this.zzclm = paramBoolean7;
      this.zzcln = paramLargeParcelTeleporter;
      this.zzclo = paramString7;
      this.zzclp = paramString8;
      if ((this.body == null) && (this.zzcln != null))
      {
        paramString1 = (StringParcel)this.zzcln.zza(StringParcel.CREATOR);
        if ((paramString1 != null) && (!TextUtils.isEmpty(paramString1.zzth()))) {
          this.body = paramString1.zzth();
        }
      }
      this.zzazu = paramBoolean8;
      this.zzazv = paramBoolean9;
      this.zzclq = paramRewardItemParcel;
      this.zzclr = paramList4;
      this.zzcls = paramList5;
      this.zzclt = paramBoolean10;
      this.zzclu = paramAutoClickProtectionConfigurationParcel;
      this.zzcks = paramBoolean11;
      this.zzckt = paramString9;
      this.zzbvn = paramList6;
      this.zzbvo = paramBoolean12;
      this.zzclv = paramString10;
      this.zzclw = paramSafeBrowsingConfigParcel;
      this.zzclx = paramString11;
      return;
      paramString1 = null;
      break;
      paramString1 = null;
      break label52;
    }
  }
  
  public AdResponseParcel(AdRequestInfoParcel paramAdRequestInfoParcel, String paramString1, String paramString2, List<String> paramList1, List<String> paramList2, long paramLong1, boolean paramBoolean1, long paramLong2, List<String> paramList3, long paramLong3, int paramInt, String paramString3, long paramLong4, String paramString4, String paramString5, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5, boolean paramBoolean6, String paramString6, boolean paramBoolean7, boolean paramBoolean8, RewardItemParcel paramRewardItemParcel, List<String> paramList4, List<String> paramList5, boolean paramBoolean9, AutoClickProtectionConfigurationParcel paramAutoClickProtectionConfigurationParcel, boolean paramBoolean10, String paramString7, List<String> paramList6, boolean paramBoolean11, String paramString8, SafeBrowsingConfigParcel paramSafeBrowsingConfigParcel, String paramString9, boolean paramBoolean12)
  {
    this(18, paramString1, paramString2, paramList1, -2, paramList2, paramLong1, paramBoolean1, paramLong2, paramList3, paramLong3, paramInt, paramString3, paramLong4, paramString4, false, null, paramString5, paramBoolean2, paramBoolean3, paramBoolean4, paramBoolean5, paramBoolean6, null, null, paramString6, paramBoolean7, paramBoolean8, paramRewardItemParcel, paramList4, paramList5, paramBoolean9, paramAutoClickProtectionConfigurationParcel, paramBoolean10, paramString7, paramList6, paramBoolean11, paramString8, paramSafeBrowsingConfigParcel, paramString9, paramBoolean12);
    this.zzbws = paramAdRequestInfoParcel;
  }
  
  public AdResponseParcel(AdRequestInfoParcel paramAdRequestInfoParcel, String paramString1, String paramString2, List<String> paramList1, List<String> paramList2, long paramLong1, boolean paramBoolean1, long paramLong2, List<String> paramList3, long paramLong3, int paramInt, String paramString3, long paramLong4, String paramString4, boolean paramBoolean2, String paramString5, String paramString6, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5, boolean paramBoolean6, boolean paramBoolean7, String paramString7, boolean paramBoolean8, boolean paramBoolean9, RewardItemParcel paramRewardItemParcel, List<String> paramList4, List<String> paramList5, boolean paramBoolean10, AutoClickProtectionConfigurationParcel paramAutoClickProtectionConfigurationParcel, boolean paramBoolean11, String paramString8, List<String> paramList6, boolean paramBoolean12, String paramString9, SafeBrowsingConfigParcel paramSafeBrowsingConfigParcel, String paramString10, boolean paramBoolean13)
  {
    this(18, paramString1, paramString2, paramList1, -2, paramList2, paramLong1, paramBoolean1, paramLong2, paramList3, paramLong3, paramInt, paramString3, paramLong4, paramString4, paramBoolean2, paramString5, paramString6, paramBoolean3, paramBoolean4, paramBoolean5, paramBoolean6, paramBoolean7, null, null, paramString7, paramBoolean8, paramBoolean9, paramRewardItemParcel, paramList4, paramList5, paramBoolean10, paramAutoClickProtectionConfigurationParcel, paramBoolean11, paramString8, paramList6, paramBoolean12, paramString9, paramSafeBrowsingConfigParcel, paramString10, paramBoolean13);
    this.zzbws = paramAdRequestInfoParcel;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    if ((this.zzbws != null) && (this.zzbws.versionCode >= 9) && (!TextUtils.isEmpty(this.body)))
    {
      this.zzcln = new LargeParcelTeleporter(new StringParcel(this.body));
      this.body = null;
    }
    zzh.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/request/AdResponseParcel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */