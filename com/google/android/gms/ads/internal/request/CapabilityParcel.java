package com.google.android.gms.ads.internal.request;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.internal.zzji;

@zzji
public class CapabilityParcel
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<CapabilityParcel> CREATOR = new zzj();
  public final int versionCode;
  public final boolean zzcmb;
  public final boolean zzcmc;
  public final boolean zzcmd;
  
  CapabilityParcel(int paramInt, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
  {
    this.versionCode = paramInt;
    this.zzcmb = paramBoolean1;
    this.zzcmc = paramBoolean2;
    this.zzcmd = paramBoolean3;
  }
  
  public CapabilityParcel(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
  {
    this(2, paramBoolean1, paramBoolean2, paramBoolean3);
  }
  
  public Bundle toBundle()
  {
    Bundle localBundle = new Bundle();
    localBundle.putBoolean("iap_supported", this.zzcmb);
    localBundle.putBoolean("default_iap_supported", this.zzcmc);
    localBundle.putBoolean("app_streaming_supported", this.zzcmd);
    return localBundle;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzj.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/request/CapabilityParcel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */