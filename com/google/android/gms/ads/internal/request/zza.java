package com.google.android.gms.ads.internal.request;

import android.content.Context;
import android.os.Bundle;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.internal.zzav;
import com.google.android.gms.internal.zzji;
import com.google.android.gms.internal.zzko.zza;
import com.google.android.gms.internal.zzkw;
import java.util.concurrent.Future;

@zzji
public class zza
{
  public zzkw zza(Context paramContext, AdRequestInfoParcel.zza paramzza, zzav paramzzav, zza paramzza1)
  {
    if (paramzza.zzcju.extras.getBundle("sdk_less_server_data") != null) {}
    for (paramContext = new zzn(paramContext, paramzza, paramzza1);; paramContext = new zzb(paramContext, paramzza, paramzzav, paramzza1))
    {
      paramzza = (Future)paramContext.zzrz();
      return paramContext;
    }
  }
  
  public static abstract interface zza
  {
    public abstract void zza(zzko.zza paramzza);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/request/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */