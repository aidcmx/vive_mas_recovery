package com.google.android.gms.ads.internal.request;

import android.content.Context;
import android.content.res.Resources;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.webkit.CookieManager;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.common.util.zze;
import com.google.android.gms.internal.zzaq;
import com.google.android.gms.internal.zzav;
import com.google.android.gms.internal.zzdn;
import com.google.android.gms.internal.zzdr;
import com.google.android.gms.internal.zzgq;
import com.google.android.gms.internal.zzji;
import com.google.android.gms.internal.zzko.zza;
import com.google.android.gms.internal.zzkr;
import com.google.android.gms.internal.zzkw;
import com.google.android.gms.internal.zzkx;
import com.google.android.gms.internal.zzla;
import com.google.android.gms.internal.zzlb;
import com.google.android.gms.internal.zzlc;
import com.google.android.gms.internal.zzld;
import com.google.android.gms.internal.zzlw;
import com.google.android.gms.internal.zzlx;
import org.json.JSONException;
import org.json.JSONObject;

@zzji
public class zzb
  extends zzkw
  implements zzc.zza
{
  private final Context mContext;
  private final zzav zzbnx;
  zzgq zzbwc;
  private AdRequestInfoParcel zzbws;
  AdResponseParcel zzcgg;
  private Runnable zzcgh;
  private final Object zzcgi = new Object();
  private final zza.zza zzcjh;
  private final AdRequestInfoParcel.zza zzcji;
  zzld zzcjj;
  
  public zzb(Context paramContext, AdRequestInfoParcel.zza paramzza, zzav paramzzav, zza.zza paramzza1)
  {
    this.zzcjh = paramzza1;
    this.mContext = paramContext;
    this.zzcji = paramzza;
    this.zzbnx = paramzzav;
  }
  
  private void zzd(int paramInt, String paramString)
  {
    if ((paramInt == 3) || (paramInt == -1))
    {
      zzkx.zzdh(paramString);
      if (this.zzcgg != null) {
        break label93;
      }
      this.zzcgg = new AdResponseParcel(paramInt);
      label33:
      if (this.zzbws == null) {
        break label115;
      }
    }
    label93:
    label115:
    for (paramString = this.zzbws;; paramString = new AdRequestInfoParcel(this.zzcji, null, -1L))
    {
      paramString = new zzko.zza(paramString, this.zzcgg, this.zzbwc, null, paramInt, -1L, this.zzcgg.zzclf, null);
      this.zzcjh.zza(paramString);
      return;
      zzkx.zzdi(paramString);
      break;
      this.zzcgg = new AdResponseParcel(paramInt, this.zzcgg.zzbvq);
      break label33;
    }
  }
  
  public void onStop()
  {
    synchronized (this.zzcgi)
    {
      if (this.zzcjj != null) {
        this.zzcjj.cancel();
      }
      return;
    }
  }
  
  zzld zza(VersionInfoParcel paramVersionInfoParcel, zzlw<AdRequestInfoParcel> paramzzlw)
  {
    return zzc.zza(this.mContext, paramVersionInfoParcel, paramzzlw, this);
  }
  
  protected AdSizeParcel zzb(AdRequestInfoParcel paramAdRequestInfoParcel)
    throws zzb.zza
  {
    int j;
    int i;
    AdSizeParcel localAdSizeParcel;
    if (this.zzcgg.zzazu)
    {
      localObject = paramAdRequestInfoParcel.zzarm.zzazs;
      j = localObject.length;
      i = 0;
      while (i < j)
      {
        localAdSizeParcel = localObject[i];
        if (localAdSizeParcel.zzazu) {
          return new AdSizeParcel(localAdSizeParcel, paramAdRequestInfoParcel.zzarm.zzazs);
        }
        i += 1;
      }
    }
    if (this.zzcgg.zzcle == null) {
      throw new zza("The ad response must specify one of the supported ad sizes.", 0);
    }
    Object localObject = this.zzcgg.zzcle.split("x");
    if (localObject.length != 2)
    {
      paramAdRequestInfoParcel = String.valueOf(this.zzcgg.zzcle);
      if (paramAdRequestInfoParcel.length() != 0) {}
      for (paramAdRequestInfoParcel = "Invalid ad size format from the ad response: ".concat(paramAdRequestInfoParcel);; paramAdRequestInfoParcel = new String("Invalid ad size format from the ad response: ")) {
        throw new zza(paramAdRequestInfoParcel, 0);
      }
    }
    try
    {
      m = Integer.parseInt(localObject[0]);
      n = Integer.parseInt(localObject[1]);
      localObject = paramAdRequestInfoParcel.zzarm.zzazs;
      i1 = localObject.length;
      i = 0;
    }
    catch (NumberFormatException paramAdRequestInfoParcel)
    {
      for (;;)
      {
        int m;
        int n;
        int i1;
        float f;
        paramAdRequestInfoParcel = String.valueOf(this.zzcgg.zzcle);
        if (paramAdRequestInfoParcel.length() != 0) {}
        for (paramAdRequestInfoParcel = "Invalid ad size number from the ad response: ".concat(paramAdRequestInfoParcel);; paramAdRequestInfoParcel = new String("Invalid ad size number from the ad response: ")) {
          throw new zza(paramAdRequestInfoParcel, 0);
        }
        j = localAdSizeParcel.width;
        continue;
        label359:
        int k = localAdSizeParcel.height;
        continue;
        label369:
        i += 1;
      }
      paramAdRequestInfoParcel = String.valueOf(this.zzcgg.zzcle);
      if (paramAdRequestInfoParcel.length() == 0) {
        break label411;
      }
    }
    if (i < i1)
    {
      localAdSizeParcel = localObject[i];
      f = this.mContext.getResources().getDisplayMetrics().density;
      if (localAdSizeParcel.width == -1)
      {
        j = (int)(localAdSizeParcel.widthPixels / f);
        if (localAdSizeParcel.height != -2) {
          break label359;
        }
        k = (int)(localAdSizeParcel.heightPixels / f);
        if ((m != j) || (n != k) || (localAdSizeParcel.zzazu)) {
          break label369;
        }
        return new AdSizeParcel(localAdSizeParcel, paramAdRequestInfoParcel.zzarm.zzazs);
      }
    }
    label411:
    for (paramAdRequestInfoParcel = "The ad size from the ad response was not one of the requested sizes: ".concat(paramAdRequestInfoParcel);; paramAdRequestInfoParcel = new String("The ad size from the ad response was not one of the requested sizes: ")) {
      throw new zza(paramAdRequestInfoParcel, 0);
    }
  }
  
  public void zzb(@NonNull AdResponseParcel arg1)
  {
    zzkx.zzdg("Received ad response.");
    this.zzcgg = ???;
    long l = zzu.zzgs().elapsedRealtime();
    synchronized (this.zzcgi)
    {
      this.zzcjj = null;
      zzu.zzgq().zzd(this.mContext, this.zzcgg.zzcks);
      try
      {
        if ((this.zzcgg.errorCode != -2) && (this.zzcgg.errorCode != -3))
        {
          int i = this.zzcgg.errorCode;
          throw new zza(66 + "There was a problem getting an ad response. ErrorCode: " + i, this.zzcgg.errorCode);
        }
      }
      catch (zza ???)
      {
        zzd(???.getErrorCode(), ???.getMessage());
        zzlb.zzcvl.removeCallbacks(this.zzcgh);
        return;
      }
    }
    zzta();
    if (this.zzbws.zzarm.zzazs != null) {}
    for (??? = zzb(this.zzbws);; ??? = null)
    {
      zzu.zzgq().zzaf(this.zzcgg.zzcll);
      zzu.zzgq().zzag(this.zzcgg.zzcly);
      if (!TextUtils.isEmpty(this.zzcgg.zzclj)) {}
      for (;;)
      {
        try
        {
          JSONObject localJSONObject = new JSONObject(this.zzcgg.zzclj);
          ??? = new zzko.zza(this.zzbws, this.zzcgg, this.zzbwc, ???, -2, l, this.zzcgg.zzclf, localJSONObject);
          this.zzcjh.zza(???);
          zzlb.zzcvl.removeCallbacks(this.zzcgh);
          return;
        }
        catch (Exception localException)
        {
          zzkx.zzb("Error parsing the JSON for Active View.", localException);
        }
        Object localObject2 = null;
      }
    }
  }
  
  public void zzfp()
  {
    zzkx.zzdg("AdLoaderBackgroundTask started.");
    this.zzcgh = new Runnable()
    {
      public void run()
      {
        synchronized (zzb.zza(zzb.this))
        {
          if (zzb.this.zzcjj == null) {
            return;
          }
          zzb.this.onStop();
          zzb.zza(zzb.this, 2, "Timed out waiting for ad response.");
          return;
        }
      }
    };
    zzlb.zzcvl.postDelayed(this.zzcgh, ((Long)zzdr.zzbhj.get()).longValue());
    final zzlx localzzlx = new zzlx();
    long l = zzu.zzgs().elapsedRealtime();
    zzla.zza(new Runnable()
    {
      public void run()
      {
        synchronized (zzb.zza(zzb.this))
        {
          zzb.this.zzcjj = zzb.this.zza(zzb.zzb(zzb.this).zzari, localzzlx);
          if (zzb.this.zzcjj == null)
          {
            zzb.zza(zzb.this, 0, "Could not start the ad request service.");
            zzlb.zzcvl.removeCallbacks(zzb.zzc(zzb.this));
          }
          return;
        }
      }
    });
    String str = this.zzbnx.zzaz().zzb(this.mContext);
    this.zzbws = new AdRequestInfoParcel(this.zzcji, str, l);
    localzzlx.zzg(this.zzbws);
  }
  
  protected void zzta()
    throws zzb.zza
  {
    if (this.zzcgg.errorCode == -3) {
      return;
    }
    if (TextUtils.isEmpty(this.zzcgg.body)) {
      throw new zza("No fill from ad server.", 3);
    }
    zzu.zzgq().zzc(this.mContext, this.zzcgg.zzckc);
    if (this.zzcgg.zzclb) {}
    for (;;)
    {
      try
      {
        this.zzbwc = new zzgq(this.zzcgg.body);
        zzu.zzgq().zzah(this.zzbwc.zzbvo);
        if ((TextUtils.isEmpty(this.zzcgg.zzckt)) || (!((Boolean)zzdr.zzbkn.get()).booleanValue())) {
          break;
        }
        zzkx.zzdg("Received cookie from server. Setting webview cookie in CookieManager.");
        CookieManager localCookieManager = zzu.zzgo().zzal(this.mContext);
        if (localCookieManager == null) {
          break;
        }
        localCookieManager.setCookie("googleads.g.doubleclick.net", this.zzcgg.zzckt);
        return;
      }
      catch (JSONException localJSONException)
      {
        zzkx.zzb("Could not parse mediation config.", localJSONException);
        str = String.valueOf(this.zzcgg.body);
        if (str.length() == 0) {}
      }
      for (String str = "Could not parse mediation config: ".concat(str);; str = new String("Could not parse mediation config: ")) {
        throw new zza(str, 0);
      }
      zzu.zzgq().zzah(this.zzcgg.zzbvo);
    }
  }
  
  @zzji
  static final class zza
    extends Exception
  {
    private final int zzcgw;
    
    public zza(String paramString, int paramInt)
    {
      super();
      this.zzcgw = paramInt;
    }
    
    public int getErrorCode()
    {
      return this.zzcgw;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/request/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */