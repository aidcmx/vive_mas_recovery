package com.google.android.gms.ads.internal.request;

import android.content.Context;
import com.google.android.gms.ads.internal.client.zzm;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.common.util.zzi;
import com.google.android.gms.internal.zzdn;
import com.google.android.gms.internal.zzdr;
import com.google.android.gms.internal.zzji;
import com.google.android.gms.internal.zzkx;
import com.google.android.gms.internal.zzld;
import com.google.android.gms.internal.zzlw;

@zzji
public final class zzc
{
  public static zzld zza(Context paramContext, VersionInfoParcel paramVersionInfoParcel, zzlw<AdRequestInfoParcel> paramzzlw, zza paramzza)
  {
    zza(paramContext, paramVersionInfoParcel, paramzzlw, paramzza, new zzb()
    {
      public boolean zza(VersionInfoParcel paramAnonymousVersionInfoParcel)
      {
        return (paramAnonymousVersionInfoParcel.zzcyc) || ((zzi.zzcj(zzc.this)) && (!((Boolean)zzdr.zzbel.get()).booleanValue()));
      }
    });
  }
  
  static zzld zza(Context paramContext, VersionInfoParcel paramVersionInfoParcel, zzlw<AdRequestInfoParcel> paramzzlw, zza paramzza, zzb paramzzb)
  {
    if (paramzzb.zza(paramVersionInfoParcel)) {
      return zza(paramContext, paramzzlw, paramzza);
    }
    return zzb(paramContext, paramVersionInfoParcel, paramzzlw, paramzza);
  }
  
  private static zzld zza(Context paramContext, zzlw<AdRequestInfoParcel> paramzzlw, zza paramzza)
  {
    zzkx.zzdg("Fetching ad response from local ad request service.");
    paramContext = new zzd.zza(paramContext, paramzzlw, paramzza);
    paramzzlw = (Void)paramContext.zzrz();
    return paramContext;
  }
  
  private static zzld zzb(Context paramContext, VersionInfoParcel paramVersionInfoParcel, zzlw<AdRequestInfoParcel> paramzzlw, zza paramzza)
  {
    zzkx.zzdg("Fetching ad response from remote ad request service.");
    if (!zzm.zzkr().zzap(paramContext))
    {
      zzkx.zzdi("Failed to connect to remote ad request service.");
      return null;
    }
    return new zzd.zzb(paramContext, paramVersionInfoParcel, paramzzlw, paramzza);
  }
  
  public static abstract interface zza
  {
    public abstract void zzb(AdResponseParcel paramAdResponseParcel);
  }
  
  static abstract interface zzb
  {
    public abstract boolean zza(VersionInfoParcel paramVersionInfoParcel);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/request/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */