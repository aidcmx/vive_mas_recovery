package com.google.android.gms.ads.internal.request;

import android.content.Context;
import android.os.Binder;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.zze.zzb;
import com.google.android.gms.common.internal.zze.zzc;
import com.google.android.gms.internal.zzdk;
import com.google.android.gms.internal.zzdn;
import com.google.android.gms.internal.zzdr;
import com.google.android.gms.internal.zzji;
import com.google.android.gms.internal.zzjk;
import com.google.android.gms.internal.zzjl;
import com.google.android.gms.internal.zzkr;
import com.google.android.gms.internal.zzkx;
import com.google.android.gms.internal.zzlb;
import com.google.android.gms.internal.zzld;
import com.google.android.gms.internal.zzlj;
import com.google.android.gms.internal.zzlw;
import com.google.android.gms.internal.zzlw.zza;
import com.google.android.gms.internal.zzlw.zzc;

@zzji
public abstract class zzd
  implements zzc.zza, zzld<Void>
{
  private final Object zzako = new Object();
  private final zzlw<AdRequestInfoParcel> zzcjm;
  private final zzc.zza zzcjn;
  
  public zzd(zzlw<AdRequestInfoParcel> paramzzlw, zzc.zza paramzza)
  {
    this.zzcjm = paramzzlw;
    this.zzcjn = paramzza;
  }
  
  public void cancel()
  {
    zztb();
  }
  
  boolean zza(zzk paramzzk, AdRequestInfoParcel paramAdRequestInfoParcel)
  {
    try
    {
      paramzzk.zza(paramAdRequestInfoParcel, new zzg(this));
      return true;
    }
    catch (RemoteException paramzzk)
    {
      zzkx.zzc("Could not fetch ad response from ad request service.", paramzzk);
      zzu.zzgq().zza(paramzzk, "AdRequestClientTask.getAdResponseFromService");
      this.zzcjn.zzb(new AdResponseParcel(0));
      return false;
    }
    catch (NullPointerException paramzzk)
    {
      for (;;)
      {
        zzkx.zzc("Could not fetch ad response from ad request service due to an Exception.", paramzzk);
        zzu.zzgq().zza(paramzzk, "AdRequestClientTask.getAdResponseFromService");
      }
    }
    catch (SecurityException paramzzk)
    {
      for (;;)
      {
        zzkx.zzc("Could not fetch ad response from ad request service due to an Exception.", paramzzk);
        zzu.zzgq().zza(paramzzk, "AdRequestClientTask.getAdResponseFromService");
      }
    }
    catch (Throwable paramzzk)
    {
      for (;;)
      {
        zzkx.zzc("Could not fetch ad response from ad request service due to an Exception.", paramzzk);
        zzu.zzgq().zza(paramzzk, "AdRequestClientTask.getAdResponseFromService");
      }
    }
  }
  
  public void zzb(AdResponseParcel paramAdResponseParcel)
  {
    synchronized (this.zzako)
    {
      this.zzcjn.zzb(paramAdResponseParcel);
      zztb();
      return;
    }
  }
  
  public Void zzrw()
  {
    final zzk localzzk = zztc();
    if (localzzk == null)
    {
      this.zzcjn.zzb(new AdResponseParcel(0));
      zztb();
      return null;
    }
    this.zzcjm.zza(new zzlw.zzc()new zzlw.zza
    {
      public void zzc(AdRequestInfoParcel paramAnonymousAdRequestInfoParcel)
      {
        if (!zzd.this.zza(localzzk, paramAnonymousAdRequestInfoParcel)) {
          zzd.this.zztb();
        }
      }
    }, new zzlw.zza()
    {
      public void run()
      {
        zzd.this.zztb();
      }
    });
    return null;
  }
  
  public abstract void zztb();
  
  public abstract zzk zztc();
  
  @zzji
  public static final class zza
    extends zzd
  {
    private final Context mContext;
    
    public zza(Context paramContext, zzlw<AdRequestInfoParcel> paramzzlw, zzc.zza paramzza)
    {
      super(paramzza);
      this.mContext = paramContext;
    }
    
    public void zztb() {}
    
    public zzk zztc()
    {
      zzdk localzzdk = new zzdk((String)zzdr.zzbcx.get());
      return zzjl.zza(this.mContext, localzzdk, zzjk.zzti());
    }
  }
  
  @zzji
  public static class zzb
    extends zzd
    implements zze.zzb, zze.zzc
  {
    private Context mContext;
    private final Object zzako = new Object();
    private VersionInfoParcel zzanu;
    private zzlw<AdRequestInfoParcel> zzcjm;
    private final zzc.zza zzcjn;
    protected zze zzcjq;
    private boolean zzcjr;
    
    public zzb(Context paramContext, VersionInfoParcel paramVersionInfoParcel, zzlw<AdRequestInfoParcel> paramzzlw, zzc.zza paramzza)
    {
      super(paramzza);
      this.mContext = paramContext;
      this.zzanu = paramVersionInfoParcel;
      this.zzcjm = paramzzlw;
      this.zzcjn = paramzza;
      if (((Boolean)zzdr.zzbek.get()).booleanValue()) {
        this.zzcjr = true;
      }
      for (paramVersionInfoParcel = zzu.zzhc().zzwj();; paramVersionInfoParcel = paramContext.getMainLooper())
      {
        this.zzcjq = new zze(paramContext, paramVersionInfoParcel, this, this, this.zzanu.zzcyb);
        connect();
        return;
      }
    }
    
    protected void connect()
    {
      this.zzcjq.zzavd();
    }
    
    public void onConnected(Bundle paramBundle)
    {
      paramBundle = (Void)zzrz();
    }
    
    public void onConnectionFailed(@NonNull ConnectionResult paramConnectionResult)
    {
      zzkx.zzdg("Cannot connect to remote service, fallback to local instance.");
      zztd().zzrz();
      paramConnectionResult = new Bundle();
      paramConnectionResult.putString("action", "gms_connection_failed_fallback_to_local");
      zzu.zzgm().zzb(this.mContext, this.zzanu.zzda, "gmob-apps", paramConnectionResult, true);
    }
    
    public void onConnectionSuspended(int paramInt)
    {
      zzkx.zzdg("Disconnected from remote ad request service.");
    }
    
    public void zztb()
    {
      synchronized (this.zzako)
      {
        if ((this.zzcjq.isConnected()) || (this.zzcjq.isConnecting())) {
          this.zzcjq.disconnect();
        }
        Binder.flushPendingCommands();
        if (this.zzcjr)
        {
          zzu.zzhc().zzwk();
          this.zzcjr = false;
        }
        return;
      }
    }
    
    /* Error */
    public zzk zztc()
    {
      // Byte code:
      //   0: aload_0
      //   1: getfield 37	com/google/android/gms/ads/internal/request/zzd$zzb:zzako	Ljava/lang/Object;
      //   4: astore_1
      //   5: aload_1
      //   6: monitorenter
      //   7: aload_0
      //   8: getfield 90	com/google/android/gms/ads/internal/request/zzd$zzb:zzcjq	Lcom/google/android/gms/ads/internal/request/zze;
      //   11: invokevirtual 191	com/google/android/gms/ads/internal/request/zze:zzte	()Lcom/google/android/gms/ads/internal/request/zzk;
      //   14: astore_2
      //   15: aload_1
      //   16: monitorexit
      //   17: aload_2
      //   18: areturn
      //   19: aload_1
      //   20: monitorexit
      //   21: aconst_null
      //   22: areturn
      //   23: astore_2
      //   24: aload_1
      //   25: monitorexit
      //   26: aload_2
      //   27: athrow
      //   28: astore_2
      //   29: goto -10 -> 19
      //   32: astore_2
      //   33: goto -14 -> 19
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	36	0	this	zzb
      //   14	4	2	localzzk	zzk
      //   23	4	2	localObject2	Object
      //   28	1	2	localDeadObjectException	android.os.DeadObjectException
      //   32	1	2	localIllegalStateException	IllegalStateException
      // Exception table:
      //   from	to	target	type
      //   7	15	23	finally
      //   15	17	23	finally
      //   19	21	23	finally
      //   24	26	23	finally
      //   7	15	28	android/os/DeadObjectException
      //   7	15	32	java/lang/IllegalStateException
    }
    
    zzld zztd()
    {
      return new zzd.zza(this.mContext, this.zzcjm, this.zzcjn);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/request/zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */