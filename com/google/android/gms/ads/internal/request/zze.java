package com.google.android.gms.ads.internal.request;

import android.content.Context;
import android.os.DeadObjectException;
import android.os.IBinder;
import android.os.Looper;
import com.google.android.gms.common.internal.zze.zzb;
import com.google.android.gms.common.internal.zze.zzc;
import com.google.android.gms.internal.zzji;

@zzji
public class zze
  extends com.google.android.gms.common.internal.zze<zzk>
{
  final int zzcjs;
  
  public zze(Context paramContext, Looper paramLooper, zze.zzb paramzzb, zze.zzc paramzzc, int paramInt)
  {
    super(paramContext, paramLooper, 8, paramzzb, paramzzc, null);
    this.zzcjs = paramInt;
  }
  
  protected zzk zzbd(IBinder paramIBinder)
  {
    return zzk.zza.zzbe(paramIBinder);
  }
  
  protected String zzjx()
  {
    return "com.google.android.gms.ads.service.START";
  }
  
  protected String zzjy()
  {
    return "com.google.android.gms.ads.internal.request.IAdRequestService";
  }
  
  public zzk zzte()
    throws DeadObjectException
  {
    return (zzk)super.zzavg();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/request/zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */