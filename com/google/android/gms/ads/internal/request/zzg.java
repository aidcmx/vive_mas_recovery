package com.google.android.gms.ads.internal.request;

import com.google.android.gms.internal.zzji;
import java.lang.ref.WeakReference;

@zzji
public final class zzg
  extends zzl.zza
{
  private final WeakReference<zzc.zza> zzckz;
  
  public zzg(zzc.zza paramzza)
  {
    this.zzckz = new WeakReference(paramzza);
  }
  
  public void zzb(AdResponseParcel paramAdResponseParcel)
  {
    zzc.zza localzza = (zzc.zza)this.zzckz.get();
    if (localzza != null) {
      localzza.zzb(paramAdResponseParcel);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/request/zzg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */