package com.google.android.gms.ads.internal.request;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.ads.internal.reward.mediation.client.RewardItemParcel;
import com.google.android.gms.ads.internal.safebrowsing.SafeBrowsingConfigParcel;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.ArrayList;

public class zzh
  implements Parcelable.Creator<AdResponseParcel>
{
  static void zza(AdResponseParcel paramAdResponseParcel, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramAdResponseParcel.versionCode);
    zzb.zza(paramParcel, 2, paramAdResponseParcel.zzcbo, false);
    zzb.zza(paramParcel, 3, paramAdResponseParcel.body, false);
    zzb.zzb(paramParcel, 4, paramAdResponseParcel.zzbvk, false);
    zzb.zzc(paramParcel, 5, paramAdResponseParcel.errorCode);
    zzb.zzb(paramParcel, 6, paramAdResponseParcel.zzbvl, false);
    zzb.zza(paramParcel, 7, paramAdResponseParcel.zzcla);
    zzb.zza(paramParcel, 8, paramAdResponseParcel.zzclb);
    zzb.zza(paramParcel, 9, paramAdResponseParcel.zzclc);
    zzb.zzb(paramParcel, 10, paramAdResponseParcel.zzcld, false);
    zzb.zza(paramParcel, 11, paramAdResponseParcel.zzbvq);
    zzb.zzc(paramParcel, 12, paramAdResponseParcel.orientation);
    zzb.zza(paramParcel, 13, paramAdResponseParcel.zzcle, false);
    zzb.zza(paramParcel, 14, paramAdResponseParcel.zzclf);
    zzb.zza(paramParcel, 15, paramAdResponseParcel.zzclg, false);
    zzb.zza(paramParcel, 18, paramAdResponseParcel.zzclh);
    zzb.zza(paramParcel, 19, paramAdResponseParcel.zzcli, false);
    zzb.zza(paramParcel, 21, paramAdResponseParcel.zzclj, false);
    zzb.zza(paramParcel, 22, paramAdResponseParcel.zzclk);
    zzb.zza(paramParcel, 23, paramAdResponseParcel.zzazt);
    zzb.zza(paramParcel, 24, paramAdResponseParcel.zzckc);
    zzb.zza(paramParcel, 25, paramAdResponseParcel.zzcll);
    zzb.zza(paramParcel, 26, paramAdResponseParcel.zzclm);
    zzb.zza(paramParcel, 28, paramAdResponseParcel.zzcln, paramInt, false);
    zzb.zza(paramParcel, 29, paramAdResponseParcel.zzclo, false);
    zzb.zza(paramParcel, 30, paramAdResponseParcel.zzclp, false);
    zzb.zza(paramParcel, 31, paramAdResponseParcel.zzazu);
    zzb.zza(paramParcel, 32, paramAdResponseParcel.zzazv);
    zzb.zza(paramParcel, 33, paramAdResponseParcel.zzclq, paramInt, false);
    zzb.zzb(paramParcel, 34, paramAdResponseParcel.zzclr, false);
    zzb.zzb(paramParcel, 35, paramAdResponseParcel.zzcls, false);
    zzb.zza(paramParcel, 36, paramAdResponseParcel.zzclt);
    zzb.zza(paramParcel, 37, paramAdResponseParcel.zzclu, paramInt, false);
    zzb.zza(paramParcel, 38, paramAdResponseParcel.zzcks);
    zzb.zza(paramParcel, 39, paramAdResponseParcel.zzckt, false);
    zzb.zzb(paramParcel, 40, paramAdResponseParcel.zzbvn, false);
    zzb.zza(paramParcel, 42, paramAdResponseParcel.zzbvo);
    zzb.zza(paramParcel, 43, paramAdResponseParcel.zzclv, false);
    zzb.zza(paramParcel, 44, paramAdResponseParcel.zzclw, paramInt, false);
    zzb.zza(paramParcel, 45, paramAdResponseParcel.zzclx, false);
    zzb.zza(paramParcel, 46, paramAdResponseParcel.zzcly);
    zzb.zzaj(paramParcel, i);
  }
  
  public AdResponseParcel[] zzau(int paramInt)
  {
    return new AdResponseParcel[paramInt];
  }
  
  public AdResponseParcel zzn(Parcel paramParcel)
  {
    int m = zza.zzcr(paramParcel);
    int k = 0;
    String str11 = null;
    String str10 = null;
    ArrayList localArrayList6 = null;
    int j = 0;
    ArrayList localArrayList5 = null;
    long l4 = 0L;
    boolean bool13 = false;
    long l3 = 0L;
    ArrayList localArrayList4 = null;
    long l2 = 0L;
    int i = 0;
    String str9 = null;
    long l1 = 0L;
    String str8 = null;
    boolean bool12 = false;
    String str7 = null;
    String str6 = null;
    boolean bool11 = false;
    boolean bool10 = false;
    boolean bool9 = false;
    boolean bool8 = false;
    boolean bool7 = false;
    LargeParcelTeleporter localLargeParcelTeleporter = null;
    String str5 = null;
    String str4 = null;
    boolean bool6 = false;
    boolean bool5 = false;
    RewardItemParcel localRewardItemParcel = null;
    ArrayList localArrayList3 = null;
    ArrayList localArrayList2 = null;
    boolean bool4 = false;
    AutoClickProtectionConfigurationParcel localAutoClickProtectionConfigurationParcel = null;
    boolean bool3 = false;
    String str3 = null;
    ArrayList localArrayList1 = null;
    boolean bool2 = false;
    String str2 = null;
    SafeBrowsingConfigParcel localSafeBrowsingConfigParcel = null;
    String str1 = null;
    boolean bool1 = false;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.zzcq(paramParcel);
      switch (zza.zzgu(n))
      {
      case 16: 
      case 17: 
      case 20: 
      case 27: 
      case 41: 
      default: 
        zza.zzb(paramParcel, n);
        break;
      case 1: 
        k = zza.zzg(paramParcel, n);
        break;
      case 2: 
        str11 = zza.zzq(paramParcel, n);
        break;
      case 3: 
        str10 = zza.zzq(paramParcel, n);
        break;
      case 4: 
        localArrayList6 = zza.zzae(paramParcel, n);
        break;
      case 5: 
        j = zza.zzg(paramParcel, n);
        break;
      case 6: 
        localArrayList5 = zza.zzae(paramParcel, n);
        break;
      case 7: 
        l4 = zza.zzi(paramParcel, n);
        break;
      case 8: 
        bool13 = zza.zzc(paramParcel, n);
        break;
      case 9: 
        l3 = zza.zzi(paramParcel, n);
        break;
      case 10: 
        localArrayList4 = zza.zzae(paramParcel, n);
        break;
      case 11: 
        l2 = zza.zzi(paramParcel, n);
        break;
      case 12: 
        i = zza.zzg(paramParcel, n);
        break;
      case 13: 
        str9 = zza.zzq(paramParcel, n);
        break;
      case 14: 
        l1 = zza.zzi(paramParcel, n);
        break;
      case 15: 
        str8 = zza.zzq(paramParcel, n);
        break;
      case 18: 
        bool12 = zza.zzc(paramParcel, n);
        break;
      case 19: 
        str7 = zza.zzq(paramParcel, n);
        break;
      case 21: 
        str6 = zza.zzq(paramParcel, n);
        break;
      case 22: 
        bool11 = zza.zzc(paramParcel, n);
        break;
      case 23: 
        bool10 = zza.zzc(paramParcel, n);
        break;
      case 24: 
        bool9 = zza.zzc(paramParcel, n);
        break;
      case 25: 
        bool8 = zza.zzc(paramParcel, n);
        break;
      case 26: 
        bool7 = zza.zzc(paramParcel, n);
        break;
      case 28: 
        localLargeParcelTeleporter = (LargeParcelTeleporter)zza.zza(paramParcel, n, LargeParcelTeleporter.CREATOR);
        break;
      case 29: 
        str5 = zza.zzq(paramParcel, n);
        break;
      case 30: 
        str4 = zza.zzq(paramParcel, n);
        break;
      case 31: 
        bool6 = zza.zzc(paramParcel, n);
        break;
      case 32: 
        bool5 = zza.zzc(paramParcel, n);
        break;
      case 33: 
        localRewardItemParcel = (RewardItemParcel)zza.zza(paramParcel, n, RewardItemParcel.CREATOR);
        break;
      case 34: 
        localArrayList3 = zza.zzae(paramParcel, n);
        break;
      case 35: 
        localArrayList2 = zza.zzae(paramParcel, n);
        break;
      case 36: 
        bool4 = zza.zzc(paramParcel, n);
        break;
      case 37: 
        localAutoClickProtectionConfigurationParcel = (AutoClickProtectionConfigurationParcel)zza.zza(paramParcel, n, AutoClickProtectionConfigurationParcel.CREATOR);
        break;
      case 38: 
        bool3 = zza.zzc(paramParcel, n);
        break;
      case 39: 
        str3 = zza.zzq(paramParcel, n);
        break;
      case 40: 
        localArrayList1 = zza.zzae(paramParcel, n);
        break;
      case 42: 
        bool2 = zza.zzc(paramParcel, n);
        break;
      case 43: 
        str2 = zza.zzq(paramParcel, n);
        break;
      case 44: 
        localSafeBrowsingConfigParcel = (SafeBrowsingConfigParcel)zza.zza(paramParcel, n, SafeBrowsingConfigParcel.CREATOR);
        break;
      case 45: 
        str1 = zza.zzq(paramParcel, n);
        break;
      case 46: 
        bool1 = zza.zzc(paramParcel, n);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza(37 + "Overread allowed size end=" + m, paramParcel);
    }
    return new AdResponseParcel(k, str11, str10, localArrayList6, j, localArrayList5, l4, bool13, l3, localArrayList4, l2, i, str9, l1, str8, bool12, str7, str6, bool11, bool10, bool9, bool8, bool7, localLargeParcelTeleporter, str5, str4, bool6, bool5, localRewardItemParcel, localArrayList3, localArrayList2, bool4, localAutoClickProtectionConfigurationParcel, bool3, str3, localArrayList1, bool2, str2, localSafeBrowsingConfigParcel, str1, bool1);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/request/zzh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */