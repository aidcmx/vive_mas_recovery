package com.google.android.gms.ads.internal.request;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.util.zze;
import com.google.android.gms.internal.zzdn;
import com.google.android.gms.internal.zzdr;
import com.google.android.gms.internal.zzfe;
import com.google.android.gms.internal.zzff;
import com.google.android.gms.internal.zzfj;
import com.google.android.gms.internal.zzge;
import com.google.android.gms.internal.zzgh;
import com.google.android.gms.internal.zzgh.zzc;
import com.google.android.gms.internal.zzgi;
import com.google.android.gms.internal.zzji;
import com.google.android.gms.internal.zzjj;
import com.google.android.gms.internal.zzjm;
import com.google.android.gms.internal.zzjs;
import com.google.android.gms.internal.zzko.zza;
import com.google.android.gms.internal.zzkw;
import com.google.android.gms.internal.zzkx;
import com.google.android.gms.internal.zzlb;
import com.google.android.gms.internal.zzlg;
import com.google.android.gms.internal.zzlw.zza;
import com.google.android.gms.internal.zzlw.zzc;
import com.google.android.gms.internal.zzmd;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.json.JSONException;
import org.json.JSONObject;

@zzji
public class zzn
  extends zzkw
{
  private static final Object zzaox;
  private static zzgh zzchl = null;
  static final long zzcmk = TimeUnit.SECONDS.toMillis(10L);
  static boolean zzcml;
  private static zzff zzcmm = null;
  private static zzfj zzcmn = null;
  private static zzfe zzcmo = null;
  private final Context mContext;
  private final Object zzcgi = new Object();
  private final zza.zza zzcjh;
  private final AdRequestInfoParcel.zza zzcji;
  private zzgh.zzc zzcmp;
  
  static
  {
    zzaox = new Object();
    zzcml = false;
  }
  
  public zzn(Context paramContext, AdRequestInfoParcel.zza paramzza, zza.zza arg3)
  {
    super(true);
    this.zzcjh = ???;
    this.mContext = paramContext;
    this.zzcji = paramzza;
    synchronized (zzaox)
    {
      if (!zzcml)
      {
        zzcmn = new zzfj();
        zzcmm = new zzff(paramContext.getApplicationContext(), paramzza.zzari);
        zzcmo = new zzc();
        zzchl = new zzgh(this.mContext.getApplicationContext(), this.zzcji.zzari, (String)zzdr.zzbcx.get(), new zzb(), new zza());
        zzcml = true;
      }
      return;
    }
  }
  
  private JSONObject zza(AdRequestInfoParcel paramAdRequestInfoParcel, String paramString)
  {
    Bundle localBundle = paramAdRequestInfoParcel.zzcju.extras.getBundle("sdk_less_server_data");
    if (localBundle == null) {}
    JSONObject localJSONObject;
    do
    {
      return null;
      localJSONObject = zzjm.zza(this.mContext, new zzjj().zzf(paramAdRequestInfoParcel).zza(zzu.zzgv().zzv(this.mContext)));
    } while (localJSONObject == null);
    try
    {
      paramAdRequestInfoParcel = AdvertisingIdClient.getAdvertisingIdInfo(this.mContext);
      localHashMap = new HashMap();
      localHashMap.put("request_id", paramString);
      localHashMap.put("request_param", localJSONObject);
      localHashMap.put("data", localBundle);
      if (paramAdRequestInfoParcel != null)
      {
        localHashMap.put("adid", paramAdRequestInfoParcel.getId());
        if (!paramAdRequestInfoParcel.isLimitAdTrackingEnabled()) {
          break label165;
        }
        i = 1;
        localHashMap.put("lat", Integer.valueOf(i));
      }
    }
    catch (GooglePlayServicesRepairableException paramAdRequestInfoParcel)
    {
      for (;;)
      {
        try
        {
          HashMap localHashMap;
          paramAdRequestInfoParcel = zzu.zzgm().zzap(localHashMap);
          return paramAdRequestInfoParcel;
        }
        catch (JSONException paramAdRequestInfoParcel)
        {
          int i;
          return null;
        }
        paramAdRequestInfoParcel = paramAdRequestInfoParcel;
        zzkx.zzc("Cannot get advertising id info", paramAdRequestInfoParcel);
        paramAdRequestInfoParcel = null;
        continue;
        i = 0;
      }
    }
    catch (IllegalStateException paramAdRequestInfoParcel)
    {
      for (;;) {}
    }
    catch (GooglePlayServicesNotAvailableException paramAdRequestInfoParcel)
    {
      for (;;) {}
    }
    catch (IOException paramAdRequestInfoParcel)
    {
      label165:
      for (;;) {}
    }
  }
  
  protected static void zzb(zzge paramzzge)
  {
    paramzzge.zza("/loadAd", zzcmn);
    paramzzge.zza("/fetchHttpRequest", zzcmm);
    paramzzge.zza("/invalidRequest", zzcmo);
  }
  
  protected static void zzc(zzge paramzzge)
  {
    paramzzge.zzb("/loadAd", zzcmn);
    paramzzge.zzb("/fetchHttpRequest", zzcmm);
    paramzzge.zzb("/invalidRequest", zzcmo);
  }
  
  private AdResponseParcel zze(AdRequestInfoParcel paramAdRequestInfoParcel)
  {
    final Object localObject = zzu.zzgm().zzvr();
    final JSONObject localJSONObject = zza(paramAdRequestInfoParcel, (String)localObject);
    if (localJSONObject == null) {
      paramAdRequestInfoParcel = new AdResponseParcel(0);
    }
    for (;;)
    {
      return paramAdRequestInfoParcel;
      long l1 = zzu.zzgs().elapsedRealtime();
      Future localFuture = zzcmn.zzbd((String)localObject);
      zza.zzcxr.post(new Runnable()
      {
        public void run()
        {
          zzn.zza(zzn.this, zzn.zztg().zzny());
          zzn.zzb(zzn.this).zza(new zzlw.zzc()new zzlw.zza
          {
            public void zzb(zzgi paramAnonymous2zzgi)
            {
              try
              {
                paramAnonymous2zzgi.zza("AFMA_getAdapterLessMediationAd", zzn.2.this.zzcmr);
                return;
              }
              catch (Exception paramAnonymous2zzgi)
              {
                zzkx.zzb("Error requesting an ad url", paramAnonymous2zzgi);
                zzn.zztf().zzbe(zzn.2.this.zzcms);
              }
            }
          }, new zzlw.zza()
          {
            public void run()
            {
              zzn.zztf().zzbe(zzn.2.this.zzcms);
            }
          });
        }
      });
      long l2 = zzcmk;
      long l3 = zzu.zzgs().elapsedRealtime();
      try
      {
        localObject = (JSONObject)localFuture.get(l2 - (l3 - l1), TimeUnit.MILLISECONDS);
        if (localObject == null) {
          return new AdResponseParcel(-1);
        }
      }
      catch (InterruptedException paramAdRequestInfoParcel)
      {
        return new AdResponseParcel(-1);
      }
      catch (TimeoutException paramAdRequestInfoParcel)
      {
        return new AdResponseParcel(2);
      }
      catch (ExecutionException paramAdRequestInfoParcel)
      {
        return new AdResponseParcel(0);
        localObject = zzjm.zza(this.mContext, paramAdRequestInfoParcel, ((JSONObject)localObject).toString());
        paramAdRequestInfoParcel = (AdRequestInfoParcel)localObject;
        if (((AdResponseParcel)localObject).errorCode == -3) {
          continue;
        }
        paramAdRequestInfoParcel = (AdRequestInfoParcel)localObject;
        if (!TextUtils.isEmpty(((AdResponseParcel)localObject).body)) {
          continue;
        }
        return new AdResponseParcel(3);
      }
      catch (CancellationException paramAdRequestInfoParcel)
      {
        for (;;) {}
      }
    }
  }
  
  public void onStop()
  {
    synchronized (this.zzcgi)
    {
      zza.zzcxr.post(new Runnable()
      {
        public void run()
        {
          if (zzn.zzb(zzn.this) != null)
          {
            zzn.zzb(zzn.this).release();
            zzn.zza(zzn.this, null);
          }
        }
      });
      return;
    }
  }
  
  public void zzfp()
  {
    zzkx.zzdg("SdkLessAdLoaderBackgroundTask started.");
    final Object localObject = new AdRequestInfoParcel(this.zzcji, null, -1L);
    AdResponseParcel localAdResponseParcel = zze((AdRequestInfoParcel)localObject);
    long l = zzu.zzgs().elapsedRealtime();
    localObject = new zzko.zza((AdRequestInfoParcel)localObject, localAdResponseParcel, null, null, localAdResponseParcel.errorCode, l, localAdResponseParcel.zzclf, null);
    zza.zzcxr.post(new Runnable()
    {
      public void run()
      {
        zzn.zza(zzn.this).zza(localObject);
        if (zzn.zzb(zzn.this) != null)
        {
          zzn.zzb(zzn.this).release();
          zzn.zza(zzn.this, null);
        }
      }
    });
  }
  
  public static class zza
    implements zzlg<zzge>
  {
    public void zza(zzge paramzzge)
    {
      zzn.zzc(paramzzge);
    }
  }
  
  public static class zzb
    implements zzlg<zzge>
  {
    public void zza(zzge paramzzge)
    {
      zzn.zzb(paramzzge);
    }
  }
  
  public static class zzc
    implements zzfe
  {
    public void zza(zzmd paramzzmd, Map<String, String> paramMap)
    {
      String str = (String)paramMap.get("request_id");
      paramzzmd = String.valueOf((String)paramMap.get("errors"));
      if (paramzzmd.length() != 0) {}
      for (paramzzmd = "Invalid request: ".concat(paramzzmd);; paramzzmd = new String("Invalid request: "))
      {
        zzkx.zzdi(paramzzmd);
        zzn.zztf().zzbe(str);
        return;
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/request/zzn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */