package com.google.android.gms.ads.internal.reward.client;

import android.os.RemoteException;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.internal.zzji;

@zzji
public class zze
  implements RewardItem
{
  private final zza zzcrc;
  
  public zze(zza paramzza)
  {
    this.zzcrc = paramzza;
  }
  
  public int getAmount()
  {
    if (this.zzcrc == null) {
      return 0;
    }
    try
    {
      int i = this.zzcrc.getAmount();
      return i;
    }
    catch (RemoteException localRemoteException)
    {
      zzb.zzc("Could not forward getAmount to RewardItem", localRemoteException);
    }
    return 0;
  }
  
  public String getType()
  {
    if (this.zzcrc == null) {
      return null;
    }
    try
    {
      String str = this.zzcrc.getType();
      return str;
    }
    catch (RemoteException localRemoteException)
    {
      zzb.zzc("Could not forward getType to RewardItem", localRemoteException);
    }
    return null;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/reward/client/zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */