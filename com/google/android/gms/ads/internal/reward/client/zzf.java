package com.google.android.gms.ads.internal.reward.client;

import android.content.Context;
import android.os.IBinder;
import android.os.RemoteException;
import com.google.android.gms.dynamic.zzd;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.dynamic.zzg;
import com.google.android.gms.dynamic.zzg.zza;
import com.google.android.gms.internal.zzgz;
import com.google.android.gms.internal.zzji;

@zzji
public class zzf
  extends zzg<zzc>
{
  public zzf()
  {
    super("com.google.android.gms.ads.reward.RewardedVideoAdCreatorImpl");
  }
  
  public zzb zzb(Context paramContext, zzgz paramzzgz)
  {
    try
    {
      zzd localzzd = zze.zzac(paramContext);
      paramContext = zzb.zza.zzbh(((zzc)zzcr(paramContext)).zza(localzzd, paramzzgz, 9877000));
      return paramContext;
    }
    catch (RemoteException paramContext)
    {
      com.google.android.gms.ads.internal.util.client.zzb.zzc("Could not get remote RewardedVideoAd.", paramContext);
      return null;
    }
    catch (zzg.zza paramContext)
    {
      for (;;) {}
    }
  }
  
  protected zzc zzbk(IBinder paramIBinder)
  {
    return zzc.zza.zzbi(paramIBinder);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/reward/client/zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */