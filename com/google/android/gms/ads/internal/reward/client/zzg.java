package com.google.android.gms.ads.internal.reward.client;

import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.google.android.gms.internal.zzji;

@zzji
public class zzg
  extends zzd.zza
{
  private final RewardedVideoAdListener zzgj;
  
  public zzg(RewardedVideoAdListener paramRewardedVideoAdListener)
  {
    this.zzgj = paramRewardedVideoAdListener;
  }
  
  public void onRewardedVideoAdClosed()
  {
    if (this.zzgj != null) {
      this.zzgj.onRewardedVideoAdClosed();
    }
  }
  
  public void onRewardedVideoAdFailedToLoad(int paramInt)
  {
    if (this.zzgj != null) {
      this.zzgj.onRewardedVideoAdFailedToLoad(paramInt);
    }
  }
  
  public void onRewardedVideoAdLeftApplication()
  {
    if (this.zzgj != null) {
      this.zzgj.onRewardedVideoAdLeftApplication();
    }
  }
  
  public void onRewardedVideoAdLoaded()
  {
    if (this.zzgj != null) {
      this.zzgj.onRewardedVideoAdLoaded();
    }
  }
  
  public void onRewardedVideoAdOpened()
  {
    if (this.zzgj != null) {
      this.zzgj.onRewardedVideoAdOpened();
    }
  }
  
  public void onRewardedVideoStarted()
  {
    if (this.zzgj != null) {
      this.zzgj.onRewardedVideoStarted();
    }
  }
  
  public void zza(zza paramzza)
  {
    if (this.zzgj != null) {
      this.zzgj.onRewarded(new zze(paramzza));
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/reward/client/zzg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */