package com.google.android.gms.ads.internal.reward.client;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.internal.client.zzh;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.internal.zzji;

@zzji
public class zzi
  implements RewardedVideoAd
{
  private final Context mContext;
  private final Object zzako = new Object();
  private final zzb zzcrd;
  private RewardedVideoAdListener zzgj;
  
  public zzi(Context paramContext, zzb paramzzb)
  {
    this.zzcrd = paramzzb;
    this.mContext = paramContext;
  }
  
  public void destroy()
  {
    destroy(null);
  }
  
  public void destroy(Context paramContext)
  {
    synchronized (this.zzako)
    {
      if (this.zzcrd == null) {
        return;
      }
    }
    try
    {
      this.zzcrd.zzh(zze.zzac(paramContext));
      return;
      paramContext = finally;
      throw paramContext;
    }
    catch (RemoteException paramContext)
    {
      for (;;)
      {
        com.google.android.gms.ads.internal.util.client.zzb.zzc("Could not forward destroy to RewardedVideoAd", paramContext);
      }
    }
  }
  
  public RewardedVideoAdListener getRewardedVideoAdListener()
  {
    synchronized (this.zzako)
    {
      RewardedVideoAdListener localRewardedVideoAdListener = this.zzgj;
      return localRewardedVideoAdListener;
    }
  }
  
  public String getUserId()
  {
    com.google.android.gms.ads.internal.util.client.zzb.zzdi("RewardedVideoAd.getUserId() is deprecated. Please do not call this method.");
    return null;
  }
  
  public boolean isLoaded()
  {
    boolean bool;
    synchronized (this.zzako)
    {
      if (this.zzcrd == null) {
        return false;
      }
    }
    return false;
  }
  
  public void loadAd(String paramString, AdRequest paramAdRequest)
  {
    synchronized (this.zzako)
    {
      if (this.zzcrd == null) {
        return;
      }
    }
    try
    {
      this.zzcrd.zza(zzh.zzkb().zza(this.mContext, paramAdRequest.zzdt(), paramString));
      return;
      paramString = finally;
      throw paramString;
    }
    catch (RemoteException paramString)
    {
      for (;;)
      {
        com.google.android.gms.ads.internal.util.client.zzb.zzc("Could not forward loadAd to RewardedVideoAd", paramString);
      }
    }
  }
  
  public void pause()
  {
    pause(null);
  }
  
  public void pause(Context paramContext)
  {
    synchronized (this.zzako)
    {
      if (this.zzcrd == null) {
        return;
      }
    }
    try
    {
      this.zzcrd.zzf(zze.zzac(paramContext));
      return;
      paramContext = finally;
      throw paramContext;
    }
    catch (RemoteException paramContext)
    {
      for (;;)
      {
        com.google.android.gms.ads.internal.util.client.zzb.zzc("Could not forward pause to RewardedVideoAd", paramContext);
      }
    }
  }
  
  public void resume()
  {
    resume(null);
  }
  
  public void resume(Context paramContext)
  {
    synchronized (this.zzako)
    {
      if (this.zzcrd == null) {
        return;
      }
    }
    try
    {
      this.zzcrd.zzg(zze.zzac(paramContext));
      return;
      paramContext = finally;
      throw paramContext;
    }
    catch (RemoteException paramContext)
    {
      for (;;)
      {
        com.google.android.gms.ads.internal.util.client.zzb.zzc("Could not forward resume to RewardedVideoAd", paramContext);
      }
    }
  }
  
  public void setRewardedVideoAdListener(RewardedVideoAdListener paramRewardedVideoAdListener)
  {
    synchronized (this.zzako)
    {
      this.zzgj = paramRewardedVideoAdListener;
      zzb localzzb = this.zzcrd;
      if (localzzb != null) {}
      try
      {
        this.zzcrd.zza(new zzg(paramRewardedVideoAdListener));
        return;
      }
      catch (RemoteException paramRewardedVideoAdListener)
      {
        for (;;)
        {
          com.google.android.gms.ads.internal.util.client.zzb.zzc("Could not forward setRewardedVideoAdListener to RewardedVideoAd", paramRewardedVideoAdListener);
        }
      }
    }
  }
  
  public void setUserId(String paramString)
  {
    com.google.android.gms.ads.internal.util.client.zzb.zzdi("RewardedVideoAd.setUserId() is deprecated. Please do not call this method.");
  }
  
  public void show()
  {
    synchronized (this.zzako)
    {
      if (this.zzcrd == null) {
        return;
      }
    }
    try
    {
      this.zzcrd.show();
      return;
      localObject2 = finally;
      throw ((Throwable)localObject2);
    }
    catch (RemoteException localRemoteException)
    {
      for (;;)
      {
        com.google.android.gms.ads.internal.util.client.zzb.zzc("Could not forward show to RewardedVideoAd", localRemoteException);
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/reward/client/zzi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */