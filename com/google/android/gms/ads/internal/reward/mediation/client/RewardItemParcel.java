package com.google.android.gms.ads.internal.reward.mediation.client;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.internal.zzji;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@zzji
public final class RewardItemParcel
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<RewardItemParcel> CREATOR = new zzc();
  public final String type;
  public final int versionCode;
  public final int zzcsc;
  
  public RewardItemParcel(int paramInt1, String paramString, int paramInt2)
  {
    this.versionCode = paramInt1;
    this.type = paramString;
    this.zzcsc = paramInt2;
  }
  
  public RewardItemParcel(RewardItem paramRewardItem)
  {
    this(1, paramRewardItem.getType(), paramRewardItem.getAmount());
  }
  
  public RewardItemParcel(String paramString, int paramInt)
  {
    this(1, paramString, paramInt);
  }
  
  @Nullable
  public static RewardItemParcel zza(JSONArray paramJSONArray)
    throws JSONException
  {
    if ((paramJSONArray == null) || (paramJSONArray.length() == 0)) {
      return null;
    }
    return new RewardItemParcel(paramJSONArray.getJSONObject(0).optString("rb_type"), paramJSONArray.getJSONObject(0).optInt("rb_amount"));
  }
  
  @Nullable
  public static RewardItemParcel zzct(@Nullable String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {
      return null;
    }
    try
    {
      paramString = zza(new JSONArray(paramString));
      return paramString;
    }
    catch (JSONException paramString) {}
    return null;
  }
  
  public boolean equals(Object paramObject)
  {
    if ((paramObject == null) || (!(paramObject instanceof RewardItemParcel))) {}
    do
    {
      return false;
      paramObject = (RewardItemParcel)paramObject;
    } while ((!zzz.equal(this.type, ((RewardItemParcel)paramObject).type)) || (!zzz.equal(Integer.valueOf(this.zzcsc), Integer.valueOf(((RewardItemParcel)paramObject).zzcsc))));
    return true;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.type, Integer.valueOf(this.zzcsc) });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzc.zza(this, paramParcel, paramInt);
  }
  
  public JSONArray zzue()
    throws JSONException
  {
    JSONObject localJSONObject = new JSONObject();
    localJSONObject.put("rb_type", this.type);
    localJSONObject.put("rb_amount", this.zzcsc);
    JSONArray localJSONArray = new JSONArray();
    localJSONArray.put(localJSONObject);
    return localJSONArray;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/reward/mediation/client/RewardItemParcel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */