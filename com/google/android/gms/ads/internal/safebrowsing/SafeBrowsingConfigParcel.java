package com.google.android.gms.ads.internal.safebrowsing;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.internal.zzji;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@zzji
public class SafeBrowsingConfigParcel
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<SafeBrowsingConfigParcel> CREATOR = new zzb();
  public final int versionCode;
  public final String zzcsd;
  public final String zzcse;
  public final boolean zzcsf;
  public final boolean zzcsg;
  public final List<String> zzcsh;
  
  public SafeBrowsingConfigParcel(int paramInt, String paramString1, String paramString2, boolean paramBoolean1, boolean paramBoolean2, List<String> paramList)
  {
    this.versionCode = paramInt;
    this.zzcsd = paramString1;
    this.zzcse = paramString2;
    this.zzcsf = paramBoolean1;
    this.zzcsg = paramBoolean2;
    this.zzcsh = paramList;
  }
  
  @Nullable
  public static SafeBrowsingConfigParcel zzj(JSONObject paramJSONObject)
    throws JSONException
  {
    int i = 0;
    if (paramJSONObject == null) {
      return null;
    }
    String str1 = paramJSONObject.optString("click_string", "");
    String str2 = paramJSONObject.optString("report_url", "");
    boolean bool1 = paramJSONObject.optBoolean("rendered_ad_enabled", false);
    boolean bool2 = paramJSONObject.optBoolean("non_malicious_reporting_enabled", false);
    Object localObject = paramJSONObject.optJSONArray("allowed_headers");
    paramJSONObject = (JSONObject)localObject;
    if (localObject == null) {
      paramJSONObject = new JSONArray();
    }
    localObject = new ArrayList();
    while (i < paramJSONObject.length())
    {
      String str3 = paramJSONObject.optString(i);
      if (!TextUtils.isEmpty(str3)) {
        ((ArrayList)localObject).add(str3.toLowerCase(Locale.ENGLISH));
      }
      i += 1;
    }
    return new SafeBrowsingConfigParcel(2, str1, str2, bool1, bool2, (List)localObject);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzb.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/safebrowsing/SafeBrowsingConfigParcel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */