package com.google.android.gms.ads.internal.safebrowsing;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import java.util.ArrayList;

public class zzb
  implements Parcelable.Creator<SafeBrowsingConfigParcel>
{
  static void zza(SafeBrowsingConfigParcel paramSafeBrowsingConfigParcel, Parcel paramParcel, int paramInt)
  {
    paramInt = com.google.android.gms.common.internal.safeparcel.zzb.zzcs(paramParcel);
    com.google.android.gms.common.internal.safeparcel.zzb.zzc(paramParcel, 1, paramSafeBrowsingConfigParcel.versionCode);
    com.google.android.gms.common.internal.safeparcel.zzb.zza(paramParcel, 2, paramSafeBrowsingConfigParcel.zzcsd, false);
    com.google.android.gms.common.internal.safeparcel.zzb.zza(paramParcel, 3, paramSafeBrowsingConfigParcel.zzcse, false);
    com.google.android.gms.common.internal.safeparcel.zzb.zza(paramParcel, 4, paramSafeBrowsingConfigParcel.zzcsf);
    com.google.android.gms.common.internal.safeparcel.zzb.zza(paramParcel, 5, paramSafeBrowsingConfigParcel.zzcsg);
    com.google.android.gms.common.internal.safeparcel.zzb.zzb(paramParcel, 6, paramSafeBrowsingConfigParcel.zzcsh, false);
    com.google.android.gms.common.internal.safeparcel.zzb.zzaj(paramParcel, paramInt);
  }
  
  public SafeBrowsingConfigParcel[] zzbe(int paramInt)
  {
    return new SafeBrowsingConfigParcel[paramInt];
  }
  
  public SafeBrowsingConfigParcel zzu(Parcel paramParcel)
  {
    ArrayList localArrayList = null;
    boolean bool1 = false;
    int j = zza.zzcr(paramParcel);
    boolean bool2 = false;
    String str1 = null;
    String str2 = null;
    int i = 0;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.zzcq(paramParcel);
      switch (zza.zzgu(k))
      {
      default: 
        zza.zzb(paramParcel, k);
        break;
      case 1: 
        i = zza.zzg(paramParcel, k);
        break;
      case 2: 
        str2 = zza.zzq(paramParcel, k);
        break;
      case 3: 
        str1 = zza.zzq(paramParcel, k);
        break;
      case 4: 
        bool2 = zza.zzc(paramParcel, k);
        break;
      case 5: 
        bool1 = zza.zzc(paramParcel, k);
        break;
      case 6: 
        localArrayList = zza.zzae(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new SafeBrowsingConfigParcel(i, str2, str1, bool2, bool1, localArrayList);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/safebrowsing/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */