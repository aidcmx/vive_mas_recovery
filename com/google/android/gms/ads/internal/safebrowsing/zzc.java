package com.google.android.gms.ads.internal.safebrowsing;

import android.view.View;
import java.util.Map;

public abstract interface zzc
{
  public abstract void zzb(String paramString, Map<String, String> paramMap);
  
  public abstract void zzcu(String paramString);
  
  public abstract void zzp(View paramView);
  
  public abstract void zzuf();
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/safebrowsing/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */