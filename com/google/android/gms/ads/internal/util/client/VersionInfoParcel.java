package com.google.android.gms.ads.internal.util.client;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.internal.zzji;

@zzji
public final class VersionInfoParcel
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<VersionInfoParcel> CREATOR = new zzd();
  public final int versionCode;
  public int zzcya;
  public int zzcyb;
  public boolean zzcyc;
  public String zzda;
  
  public VersionInfoParcel(int paramInt1, int paramInt2, boolean paramBoolean)
  {
    this(paramInt1, paramInt2, paramBoolean, false);
  }
  
  public VersionInfoParcel(int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2) {}
  
  VersionInfoParcel(int paramInt1, String paramString, int paramInt2, int paramInt3, boolean paramBoolean)
  {
    this.versionCode = paramInt1;
    this.zzda = paramString;
    this.zzcya = paramInt2;
    this.zzcyb = paramInt3;
    this.zzcyc = paramBoolean;
  }
  
  public static VersionInfoParcel zzwr()
  {
    return new VersionInfoParcel(9877208, 9877208, true);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzd.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/util/client/VersionInfoParcel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */