package com.google.android.gms.ads.internal.util.client;

import android.util.Log;
import com.google.android.gms.internal.zzji;

@zzji
public class zzb
{
  public static void e(String paramString)
  {
    if (zzbi(6)) {
      Log.e("Ads", paramString);
    }
  }
  
  public static void zza(String paramString, Throwable paramThrowable)
  {
    if (zzbi(3)) {
      Log.d("Ads", paramString, paramThrowable);
    }
  }
  
  public static void zzb(String paramString, Throwable paramThrowable)
  {
    if (zzbi(6)) {
      Log.e("Ads", paramString, paramThrowable);
    }
  }
  
  public static boolean zzbi(int paramInt)
  {
    return (paramInt >= 5) || (Log.isLoggable("Ads", paramInt));
  }
  
  public static void zzc(String paramString, Throwable paramThrowable)
  {
    if (zzbi(5)) {
      Log.w("Ads", paramString, paramThrowable);
    }
  }
  
  public static void zzdg(String paramString)
  {
    if (zzbi(3)) {
      Log.d("Ads", paramString);
    }
  }
  
  public static void zzdh(String paramString)
  {
    if (zzbi(4)) {
      Log.i("Ads", paramString);
    }
  }
  
  public static void zzdi(String paramString)
  {
    if (zzbi(5)) {
      Log.w("Ads", paramString);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/util/client/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */