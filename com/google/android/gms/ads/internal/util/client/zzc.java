package com.google.android.gms.ads.internal.util.client;

import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import com.google.android.gms.ads.internal.client.zzm;
import com.google.android.gms.internal.zzji;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

@zzji
public class zzc
  implements zza.zza
{
  @Nullable
  private final String zzbre;
  
  public zzc()
  {
    this(null);
  }
  
  public zzc(@Nullable String paramString)
  {
    this.zzbre = paramString;
  }
  
  @WorkerThread
  public void zzv(String paramString)
  {
    for (;;)
    {
      try
      {
        localObject1 = String.valueOf(paramString);
        if (((String)localObject1).length() != 0)
        {
          localObject1 = "Pinging URL: ".concat((String)localObject1);
          zzb.zzdg((String)localObject1);
          localObject1 = (HttpURLConnection)new URL(paramString).openConnection();
        }
      }
      catch (IndexOutOfBoundsException localIndexOutOfBoundsException)
      {
        Object localObject1;
        int i;
        str1 = String.valueOf(localIndexOutOfBoundsException.getMessage());
        zzb.zzdi(String.valueOf(paramString).length() + 32 + String.valueOf(str1).length() + "Error while parsing ping URL: " + paramString + ". " + str1);
        return;
      }
      catch (IOException localIOException)
      {
        String str1;
        String str2 = String.valueOf(localIOException.getMessage());
        zzb.zzdi(String.valueOf(paramString).length() + 27 + String.valueOf(str2).length() + "Error while pinging URL: " + paramString + ". " + str2);
        return;
      }
      catch (RuntimeException localRuntimeException)
      {
        String str3 = String.valueOf(localRuntimeException.getMessage());
        zzb.zzdi(String.valueOf(paramString).length() + 27 + String.valueOf(str3).length() + "Error while pinging URL: " + paramString + ". " + str3);
      }
      try
      {
        zzm.zzkr().zza(true, (HttpURLConnection)localObject1, this.zzbre);
        i = ((HttpURLConnection)localObject1).getResponseCode();
        if ((i < 200) || (i >= 300)) {
          zzb.zzdi(String.valueOf(paramString).length() + 65 + "Received non-success response code " + i + " from pinging URL: " + paramString);
        }
        ((HttpURLConnection)localObject1).disconnect();
        return;
      }
      finally
      {
        str1.disconnect();
      }
      localObject1 = new String("Pinging URL: ");
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/util/client/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */