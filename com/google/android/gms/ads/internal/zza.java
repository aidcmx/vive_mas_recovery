package com.google.android.gms.ads.internal;

import android.content.Context;
import android.os.Debug;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.client.ThinAdSizeParcel;
import com.google.android.gms.ads.internal.client.VideoOptionsParcel;
import com.google.android.gms.ads.internal.client.zzab;
import com.google.android.gms.ads.internal.client.zzf;
import com.google.android.gms.ads.internal.client.zzm;
import com.google.android.gms.ads.internal.client.zzq;
import com.google.android.gms.ads.internal.client.zzu.zza;
import com.google.android.gms.ads.internal.client.zzw;
import com.google.android.gms.ads.internal.client.zzy;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import com.google.android.gms.ads.internal.request.zza.zza;
import com.google.android.gms.ads.internal.reward.mediation.client.RewardItemParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.util.zzi;
import com.google.android.gms.internal.zzco;
import com.google.android.gms.internal.zzcz;
import com.google.android.gms.internal.zzdn;
import com.google.android.gms.internal.zzdr;
import com.google.android.gms.internal.zzdt;
import com.google.android.gms.internal.zzdx;
import com.google.android.gms.internal.zzdz;
import com.google.android.gms.internal.zzed;
import com.google.android.gms.internal.zzfa;
import com.google.android.gms.internal.zzig;
import com.google.android.gms.internal.zzik;
import com.google.android.gms.internal.zziu.zza;
import com.google.android.gms.internal.zzji;
import com.google.android.gms.internal.zzjz;
import com.google.android.gms.internal.zzko;
import com.google.android.gms.internal.zzko.zza;
import com.google.android.gms.internal.zzkp;
import com.google.android.gms.internal.zzkr;
import com.google.android.gms.internal.zzkt;
import com.google.android.gms.internal.zzku;
import com.google.android.gms.internal.zzkx;
import com.google.android.gms.internal.zzlb;
import com.google.android.gms.internal.zzlc;
import com.google.android.gms.internal.zzlf;
import com.google.android.gms.internal.zzmd;
import com.google.android.gms.internal.zzme;
import java.util.HashSet;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CountDownLatch;

@zzji
public abstract class zza
  extends zzu.zza
  implements com.google.android.gms.ads.internal.client.zza, com.google.android.gms.ads.internal.overlay.zzp, zza.zza, zzfa, zziu.zza, zzkt
{
  protected zzdz zzalt;
  protected zzdx zzalu;
  protected zzdx zzalv;
  protected boolean zzalw = false;
  protected final zzr zzalx;
  protected final zzv zzaly;
  @Nullable
  protected transient AdRequestParcel zzalz;
  protected final zzco zzama;
  protected final zzd zzamb;
  
  zza(zzv paramzzv, @Nullable zzr paramzzr, zzd paramzzd)
  {
    this.zzaly = paramzzv;
    if (paramzzr != null) {}
    for (;;)
    {
      this.zzalx = paramzzr;
      this.zzamb = paramzzd;
      zzu.zzgm().zzz(this.zzaly.zzahs);
      zzu.zzgq().zzc(this.zzaly.zzahs, this.zzaly.zzari);
      zzu.zzgr().initialize(this.zzaly.zzahs);
      this.zzama = zzu.zzgq().zzvg();
      zzu.zzgp().initialize(this.zzaly.zzahs);
      zzed();
      return;
      paramzzr = new zzr(this);
    }
  }
  
  private AdRequestParcel zza(AdRequestParcel paramAdRequestParcel)
  {
    AdRequestParcel localAdRequestParcel = paramAdRequestParcel;
    if (zzi.zzcj(this.zzaly.zzahs))
    {
      localAdRequestParcel = paramAdRequestParcel;
      if (paramAdRequestParcel.zzayt != null) {
        localAdRequestParcel = new zzf(paramAdRequestParcel).zza(null).zzka();
      }
    }
    return localAdRequestParcel;
  }
  
  private TimerTask zza(final Timer paramTimer, final CountDownLatch paramCountDownLatch)
  {
    new TimerTask()
    {
      public void run()
      {
        if (((Integer)zzdr.zzbjn.get()).intValue() != paramCountDownLatch.getCount())
        {
          zzkx.zzdg("Stopping method tracing");
          Debug.stopMethodTracing();
          if (paramCountDownLatch.getCount() == 0L)
          {
            paramTimer.cancel();
            return;
          }
        }
        String str = String.valueOf(zza.this.zzaly.zzahs.getPackageName()).concat("_adsTrace_");
        try
        {
          zzkx.zzdg("Starting method tracing");
          paramCountDownLatch.countDown();
          long l = zzu.zzgs().currentTimeMillis();
          Debug.startMethodTracing(String.valueOf(str).length() + 20 + str + l, ((Integer)zzdr.zzbjo.get()).intValue());
          return;
        }
        catch (Exception localException)
        {
          zzkx.zzc("Exception occurred while starting method tracing.", localException);
        }
      }
    };
  }
  
  private void zzd(zzko paramzzko)
  {
    if ((!zzu.zzgu().zzwg()) || (paramzzko.zzcst) || (TextUtils.isEmpty(paramzzko.zzclx))) {
      return;
    }
    zzkx.zzdg("Sending troubleshooting signals to the server.");
    zzu.zzgu().zza(this.zzaly.zzahs, this.zzaly.zzari.zzda, paramzzko.zzclx, this.zzaly.zzarg);
    paramzzko.zzcst = true;
  }
  
  private void zzed()
  {
    if (((Boolean)zzdr.zzbjl.get()).booleanValue())
    {
      Timer localTimer = new Timer();
      localTimer.schedule(zza(localTimer, new CountDownLatch(((Integer)zzdr.zzbjn.get()).intValue())), 0L, ((Long)zzdr.zzbjm.get()).longValue());
    }
  }
  
  public void destroy()
  {
    zzaa.zzhs("destroy must be called on the main UI thread.");
    this.zzalx.cancel();
    this.zzama.zzk(this.zzaly.zzarn);
    this.zzaly.destroy();
  }
  
  public boolean isLoading()
  {
    return this.zzalw;
  }
  
  public boolean isReady()
  {
    zzaa.zzhs("isLoaded must be called on the main UI thread.");
    return (this.zzaly.zzark == null) && (this.zzaly.zzarl == null) && (this.zzaly.zzarn != null);
  }
  
  public void onAdClicked()
  {
    if (this.zzaly.zzarn == null) {
      zzkx.zzdi("Ad state was null when trying to ping click URLs.");
    }
    do
    {
      return;
      zzkx.zzdg("Pinging click URLs.");
      if (this.zzaly.zzarp != null) {
        this.zzaly.zzarp.zzuh();
      }
      if (this.zzaly.zzarn.zzbvk != null) {
        zzu.zzgm().zza(this.zzaly.zzahs, this.zzaly.zzari.zzda, this.zzaly.zzarn.zzbvk);
      }
    } while (this.zzaly.zzarq == null);
    try
    {
      this.zzaly.zzarq.onAdClicked();
      return;
    }
    catch (RemoteException localRemoteException)
    {
      zzkx.zzc("Could not notify onAdClicked event.", localRemoteException);
    }
  }
  
  public void onAppEvent(String paramString1, @Nullable String paramString2)
  {
    if (this.zzaly.zzars != null) {}
    try
    {
      this.zzaly.zzars.onAppEvent(paramString1, paramString2);
      return;
    }
    catch (RemoteException paramString1)
    {
      zzkx.zzc("Could not call the AppEventListener.", paramString1);
    }
  }
  
  public void pause()
  {
    zzaa.zzhs("pause must be called on the main UI thread.");
  }
  
  public void resume()
  {
    zzaa.zzhs("resume must be called on the main UI thread.");
  }
  
  public void setManualImpressionsEnabled(boolean paramBoolean)
  {
    throw new UnsupportedOperationException("Attempt to call setManualImpressionsEnabled for an unsupported ad type.");
  }
  
  public void setUserId(String paramString)
  {
    zzkx.zzdi("RewardedVideoAd.setUserId() is deprecated. Please do not call this method.");
  }
  
  public void stopLoading()
  {
    zzaa.zzhs("stopLoading must be called on the main UI thread.");
    this.zzalw = false;
    this.zzaly.zzi(true);
  }
  
  public void zza(AdSizeParcel paramAdSizeParcel)
  {
    zzaa.zzhs("setAdSize must be called on the main UI thread.");
    this.zzaly.zzarm = paramAdSizeParcel;
    if ((this.zzaly.zzarn != null) && (this.zzaly.zzarn.zzcbm != null) && (this.zzaly.zzasi == 0)) {
      this.zzaly.zzarn.zzcbm.zza(paramAdSizeParcel);
    }
    if (this.zzaly.zzarj == null) {
      return;
    }
    if (this.zzaly.zzarj.getChildCount() > 1) {
      this.zzaly.zzarj.removeView(this.zzaly.zzarj.getNextView());
    }
    this.zzaly.zzarj.setMinimumWidth(paramAdSizeParcel.widthPixels);
    this.zzaly.zzarj.setMinimumHeight(paramAdSizeParcel.heightPixels);
    this.zzaly.zzarj.requestLayout();
  }
  
  public void zza(@Nullable VideoOptionsParcel paramVideoOptionsParcel)
  {
    zzaa.zzhs("setVideoOptions must be called on the main UI thread.");
    this.zzaly.zzasb = paramVideoOptionsParcel;
  }
  
  public void zza(com.google.android.gms.ads.internal.client.zzp paramzzp)
  {
    zzaa.zzhs("setAdListener must be called on the main UI thread.");
    this.zzaly.zzarq = paramzzp;
  }
  
  public void zza(zzq paramzzq)
  {
    zzaa.zzhs("setAdListener must be called on the main UI thread.");
    this.zzaly.zzarr = paramzzq;
  }
  
  public void zza(zzw paramzzw)
  {
    zzaa.zzhs("setAppEventListener must be called on the main UI thread.");
    this.zzaly.zzars = paramzzw;
  }
  
  public void zza(zzy paramzzy)
  {
    zzaa.zzhs("setCorrelationIdProvider must be called on the main UI thread");
    this.zzaly.zzart = paramzzy;
  }
  
  public void zza(com.google.android.gms.ads.internal.reward.client.zzd paramzzd)
  {
    zzaa.zzhs("setRewardedVideoAdListener can only be called from the UI thread.");
    this.zzaly.zzasd = paramzzd;
  }
  
  protected void zza(@Nullable RewardItemParcel paramRewardItemParcel)
  {
    if (this.zzaly.zzasd == null) {
      return;
    }
    String str = "";
    int i = 0;
    if (paramRewardItemParcel != null) {}
    try
    {
      str = paramRewardItemParcel.type;
      i = paramRewardItemParcel.zzcsc;
      this.zzaly.zzasd.zza(new zzjz(str, i));
      return;
    }
    catch (RemoteException paramRewardItemParcel)
    {
      zzkx.zzc("Could not call RewardedVideoAdListener.onRewarded().", paramRewardItemParcel);
    }
  }
  
  public void zza(zzed paramzzed)
  {
    throw new IllegalStateException("setOnCustomRenderedAdLoadedListener is not supported for current ad type");
  }
  
  public void zza(zzig paramzzig)
  {
    throw new IllegalStateException("setInAppPurchaseListener is not supported for current ad type");
  }
  
  public void zza(zzik paramzzik, String paramString)
  {
    throw new IllegalStateException("setPlayStorePurchaseParams is not supported for current ad type");
  }
  
  public void zza(zzko.zza paramzza)
  {
    if ((paramzza.zzcsu.zzclf != -1L) && (!TextUtils.isEmpty(paramzza.zzcsu.zzclo)))
    {
      long l = zzx(paramzza.zzcsu.zzclo);
      if (l != -1L)
      {
        zzdx localzzdx = this.zzalt.zzc(l + paramzza.zzcsu.zzclf);
        this.zzalt.zza(localzzdx, new String[] { "stc" });
      }
    }
    this.zzalt.zzaz(paramzza.zzcsu.zzclo);
    this.zzalt.zza(this.zzalu, new String[] { "arf" });
    this.zzalv = this.zzalt.zzlz();
    this.zzalt.zzg("gqi", paramzza.zzcsu.zzclp);
    this.zzaly.zzark = null;
    this.zzaly.zzaro = paramzza;
    zza(paramzza, this.zzalt);
  }
  
  protected abstract void zza(zzko.zza paramzza, zzdz paramzzdz);
  
  public void zza(HashSet<zzkp> paramHashSet)
  {
    this.zzaly.zza(paramHashSet);
  }
  
  protected abstract boolean zza(AdRequestParcel paramAdRequestParcel, zzdz paramzzdz);
  
  boolean zza(zzko paramzzko)
  {
    return false;
  }
  
  protected abstract boolean zza(@Nullable zzko paramzzko1, zzko paramzzko2);
  
  protected void zzb(View paramView)
  {
    zzv.zza localzza = this.zzaly.zzarj;
    if (localzza != null) {
      localzza.addView(paramView, zzu.zzgo().zzvz());
    }
  }
  
  public void zzb(zzko paramzzko)
  {
    this.zzalt.zza(this.zzalv, new String[] { "awr" });
    this.zzaly.zzarl = null;
    if ((paramzzko.errorCode != -2) && (paramzzko.errorCode != 3)) {
      zzu.zzgq().zzb(this.zzaly.zzhl());
    }
    if (paramzzko.errorCode == -1)
    {
      this.zzalw = false;
      return;
    }
    if (zza(paramzzko)) {
      zzkx.zzdg("Ad refresh scheduled.");
    }
    if (paramzzko.errorCode != -2)
    {
      zzh(paramzzko.errorCode);
      return;
    }
    if (this.zzaly.zzasg == null) {
      this.zzaly.zzasg = new zzku(this.zzaly.zzarg);
    }
    this.zzama.zzj(this.zzaly.zzarn);
    zzdz localzzdz;
    if (zza(this.zzaly.zzarn, paramzzko))
    {
      this.zzaly.zzarn = paramzzko;
      this.zzaly.zzhu();
      localzzdz = this.zzalt;
      if (!this.zzaly.zzarn.zzic()) {
        break label394;
      }
      str = "1";
      label203:
      localzzdz.zzg("is_mraid", str);
      localzzdz = this.zzalt;
      if (!this.zzaly.zzarn.zzclb) {
        break label401;
      }
      str = "1";
      label233:
      localzzdz.zzg("is_mediation", str);
      if ((this.zzaly.zzarn.zzcbm != null) && (this.zzaly.zzarn.zzcbm.zzxc() != null))
      {
        localzzdz = this.zzalt;
        if (!this.zzaly.zzarn.zzcbm.zzxc().zzxy()) {
          break label408;
        }
      }
    }
    label394:
    label401:
    label408:
    for (String str = "1";; str = "0")
    {
      localzzdz.zzg("is_delay_pl", str);
      this.zzalt.zza(this.zzalu, new String[] { "ttc" });
      if (zzu.zzgq().zzuu() != null) {
        zzu.zzgq().zzuu().zza(this.zzalt);
      }
      if (this.zzaly.zzhp()) {
        zzen();
      }
      if (paramzzko.zzbvn == null) {
        break;
      }
      zzu.zzgm().zza(this.zzaly.zzahs, paramzzko.zzbvn);
      return;
      str = "0";
      break label203;
      str = "0";
      break label233;
    }
  }
  
  public boolean zzb(AdRequestParcel paramAdRequestParcel)
  {
    zzaa.zzhs("loadAd must be called on the main UI thread.");
    zzu.zzgr().zzjt();
    if (((Boolean)zzdr.zzbge.get()).booleanValue()) {
      AdRequestParcel.zzj(paramAdRequestParcel);
    }
    paramAdRequestParcel = zza(paramAdRequestParcel);
    if ((this.zzaly.zzark != null) || (this.zzaly.zzarl != null))
    {
      if (this.zzalz != null) {
        zzkx.zzdi("Aborting last ad request since another ad request is already in progress. The current request object will still be cached for future refreshes.");
      }
      for (;;)
      {
        this.zzalz = paramAdRequestParcel;
        return false;
        zzkx.zzdi("Loading already in progress, saving this object for future refreshes.");
      }
    }
    zzkx.zzdh("Starting ad request.");
    zzee();
    this.zzalu = this.zzalt.zzlz();
    if (!paramAdRequestParcel.zzayo)
    {
      String str = String.valueOf(zzm.zzkr().zzao(this.zzaly.zzahs));
      zzkx.zzdh(String.valueOf(str).length() + 71 + "Use AdRequest.Builder.addTestDevice(\"" + str + "\") to get test ads on this device.");
    }
    this.zzalx.zzg(paramAdRequestParcel);
    this.zzalw = zza(paramAdRequestParcel, this.zzalt);
    return this.zzalw;
  }
  
  protected void zzc(@Nullable zzko paramzzko)
  {
    if (paramzzko == null) {
      zzkx.zzdi("Ad state was null when trying to ping impression URLs.");
    }
    do
    {
      return;
      zzkx.zzdg("Pinging Impression URLs.");
      if (this.zzaly.zzarp != null) {
        this.zzaly.zzarp.zzug();
      }
    } while ((paramzzko.zzbvl == null) || (paramzzko.zzcsr));
    zzu.zzgm().zza(this.zzaly.zzahs, this.zzaly.zzari.zzda, paramzzko.zzbvl);
    paramzzko.zzcsr = true;
    zzd(paramzzko);
  }
  
  protected boolean zzc(AdRequestParcel paramAdRequestParcel)
  {
    if (this.zzaly.zzarj == null) {
      return false;
    }
    paramAdRequestParcel = this.zzaly.zzarj.getParent();
    if (!(paramAdRequestParcel instanceof View)) {
      return false;
    }
    paramAdRequestParcel = (View)paramAdRequestParcel;
    return zzu.zzgm().zza(paramAdRequestParcel, paramAdRequestParcel.getContext());
  }
  
  public void zzd(AdRequestParcel paramAdRequestParcel)
  {
    if (zzc(paramAdRequestParcel))
    {
      zzb(paramAdRequestParcel);
      return;
    }
    zzkx.zzdh("Ad is not visible. Not refreshing ad.");
    this.zzalx.zzh(paramAdRequestParcel);
  }
  
  public zzd zzec()
  {
    return this.zzamb;
  }
  
  public void zzee()
  {
    this.zzalt = new zzdz(((Boolean)zzdr.zzbeq.get()).booleanValue(), "load_ad", this.zzaly.zzarm.zzazq);
    this.zzalu = new zzdx(-1L, null, null);
    this.zzalv = new zzdx(-1L, null, null);
  }
  
  public com.google.android.gms.dynamic.zzd zzef()
  {
    zzaa.zzhs("getAdFrame must be called on the main UI thread.");
    return com.google.android.gms.dynamic.zze.zzac(this.zzaly.zzarj);
  }
  
  @Nullable
  public AdSizeParcel zzeg()
  {
    zzaa.zzhs("getAdSize must be called on the main UI thread.");
    if (this.zzaly.zzarm == null) {
      return null;
    }
    return new ThinAdSizeParcel(this.zzaly.zzarm);
  }
  
  public void zzeh()
  {
    zzel();
  }
  
  public void zzei()
  {
    zzaa.zzhs("recordManualImpression must be called on the main UI thread.");
    if (this.zzaly.zzarn == null) {
      zzkx.zzdi("Ad state was null when trying to ping manual tracking URLs.");
    }
    do
    {
      return;
      zzkx.zzdg("Pinging manual tracking URLs.");
    } while ((this.zzaly.zzarn.zzcld == null) || (this.zzaly.zzarn.zzcss));
    zzu.zzgm().zza(this.zzaly.zzahs, this.zzaly.zzari.zzda, this.zzaly.zzarn.zzcld);
    this.zzaly.zzarn.zzcss = true;
    zzd(this.zzaly.zzarn);
  }
  
  public zzab zzej()
  {
    return null;
  }
  
  protected void zzek()
  {
    zzkx.zzdh("Ad closing.");
    if (this.zzaly.zzarr != null) {}
    try
    {
      this.zzaly.zzarr.onAdClosed();
      if (this.zzaly.zzasd == null) {}
    }
    catch (RemoteException localRemoteException1)
    {
      for (;;)
      {
        try
        {
          this.zzaly.zzasd.onRewardedVideoAdClosed();
          return;
        }
        catch (RemoteException localRemoteException2)
        {
          zzkx.zzc("Could not call RewardedVideoAdListener.onRewardedVideoAdClosed().", localRemoteException2);
        }
        localRemoteException1 = localRemoteException1;
        zzkx.zzc("Could not call AdListener.onAdClosed().", localRemoteException1);
      }
    }
  }
  
  protected void zzel()
  {
    zzkx.zzdh("Ad leaving application.");
    if (this.zzaly.zzarr != null) {}
    try
    {
      this.zzaly.zzarr.onAdLeftApplication();
      if (this.zzaly.zzasd == null) {}
    }
    catch (RemoteException localRemoteException1)
    {
      for (;;)
      {
        try
        {
          this.zzaly.zzasd.onRewardedVideoAdLeftApplication();
          return;
        }
        catch (RemoteException localRemoteException2)
        {
          zzkx.zzc("Could not call  RewardedVideoAdListener.onRewardedVideoAdLeftApplication().", localRemoteException2);
        }
        localRemoteException1 = localRemoteException1;
        zzkx.zzc("Could not call AdListener.onAdLeftApplication().", localRemoteException1);
      }
    }
  }
  
  protected void zzem()
  {
    zzkx.zzdh("Ad opening.");
    if (this.zzaly.zzarr != null) {}
    try
    {
      this.zzaly.zzarr.onAdOpened();
      if (this.zzaly.zzasd == null) {}
    }
    catch (RemoteException localRemoteException1)
    {
      for (;;)
      {
        try
        {
          this.zzaly.zzasd.onRewardedVideoAdOpened();
          return;
        }
        catch (RemoteException localRemoteException2)
        {
          zzkx.zzc("Could not call RewardedVideoAdListener.onRewardedVideoAdOpened().", localRemoteException2);
        }
        localRemoteException1 = localRemoteException1;
        zzkx.zzc("Could not call AdListener.onAdOpened().", localRemoteException1);
      }
    }
  }
  
  protected void zzen()
  {
    zzkx.zzdh("Ad finished loading.");
    this.zzalw = false;
    if (this.zzaly.zzarr != null) {}
    try
    {
      this.zzaly.zzarr.onAdLoaded();
      if (this.zzaly.zzasd == null) {}
    }
    catch (RemoteException localRemoteException1)
    {
      for (;;)
      {
        try
        {
          this.zzaly.zzasd.onRewardedVideoAdLoaded();
          return;
        }
        catch (RemoteException localRemoteException2)
        {
          zzkx.zzc("Could not call RewardedVideoAdListener.onRewardedVideoAdLoaded().", localRemoteException2);
        }
        localRemoteException1 = localRemoteException1;
        zzkx.zzc("Could not call AdListener.onAdLoaded().", localRemoteException1);
      }
    }
  }
  
  protected void zzeo()
  {
    if (this.zzaly.zzasd == null) {
      return;
    }
    try
    {
      this.zzaly.zzasd.onRewardedVideoStarted();
      return;
    }
    catch (RemoteException localRemoteException)
    {
      zzkx.zzc("Could not call RewardedVideoAdListener.onVideoStarted().", localRemoteException);
    }
  }
  
  protected void zzh(int paramInt)
  {
    zzkx.zzdi(30 + "Failed to load ad: " + paramInt);
    this.zzalw = false;
    if (this.zzaly.zzarr != null) {}
    try
    {
      this.zzaly.zzarr.onAdFailedToLoad(paramInt);
      if (this.zzaly.zzasd == null) {}
    }
    catch (RemoteException localRemoteException1)
    {
      for (;;)
      {
        try
        {
          this.zzaly.zzasd.onRewardedVideoAdFailedToLoad(paramInt);
          return;
        }
        catch (RemoteException localRemoteException2)
        {
          zzkx.zzc("Could not call RewardedVideoAdListener.onRewardedVideoAdFailedToLoad().", localRemoteException2);
        }
        localRemoteException1 = localRemoteException1;
        zzkx.zzc("Could not call AdListener.onAdFailedToLoad().", localRemoteException1);
      }
    }
  }
  
  long zzx(String paramString)
  {
    int k = paramString.indexOf("ufe");
    int j = paramString.indexOf(',', k);
    int i = j;
    if (j == -1) {
      i = paramString.length();
    }
    try
    {
      long l = Long.parseLong(paramString.substring(k + 4, i));
      return l;
    }
    catch (IndexOutOfBoundsException paramString)
    {
      zzkx.zzdi("Invalid index for Url fetch time in CSI latency info.");
      return -1L;
    }
    catch (NumberFormatException paramString)
    {
      for (;;)
      {
        zzkx.zzdi("Cannot find valid format of Url fetch time in CSI latency info.");
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */