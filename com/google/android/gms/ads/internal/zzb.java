package com.google.android.gms.ads.internal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.support.v4.util.SimpleArrayMap;
import android.util.DisplayMetrics;
import android.webkit.CookieManager;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.client.zzm;
import com.google.android.gms.ads.internal.client.zzy;
import com.google.android.gms.ads.internal.purchase.GInAppPurchaseManagerInfoParcel;
import com.google.android.gms.ads.internal.purchase.zzc;
import com.google.android.gms.ads.internal.purchase.zzf;
import com.google.android.gms.ads.internal.purchase.zzi;
import com.google.android.gms.ads.internal.purchase.zzj;
import com.google.android.gms.ads.internal.purchase.zzk;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel.zza;
import com.google.android.gms.ads.internal.request.CapabilityParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.internal.zzco;
import com.google.android.gms.internal.zzdn;
import com.google.android.gms.internal.zzdr;
import com.google.android.gms.internal.zzdz;
import com.google.android.gms.internal.zzfg;
import com.google.android.gms.internal.zzfr;
import com.google.android.gms.internal.zzgp;
import com.google.android.gms.internal.zzgq;
import com.google.android.gms.internal.zzgr;
import com.google.android.gms.internal.zzgs;
import com.google.android.gms.internal.zzgv;
import com.google.android.gms.internal.zzgz;
import com.google.android.gms.internal.zzha;
import com.google.android.gms.internal.zzid;
import com.google.android.gms.internal.zzig;
import com.google.android.gms.internal.zzik;
import com.google.android.gms.internal.zzji;
import com.google.android.gms.internal.zzko;
import com.google.android.gms.internal.zzkp;
import com.google.android.gms.internal.zzkq;
import com.google.android.gms.internal.zzkr;
import com.google.android.gms.internal.zzku;
import com.google.android.gms.internal.zzkw;
import com.google.android.gms.internal.zzkx;
import com.google.android.gms.internal.zzlb;
import com.google.android.gms.internal.zzlc;
import com.google.android.gms.internal.zzlf;
import com.google.android.gms.internal.zzmd;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Future;

@zzji
public abstract class zzb
  extends zza
  implements com.google.android.gms.ads.internal.overlay.zzg, zzj, zzs, zzfg, zzgr
{
  private final Messenger mMessenger;
  protected final zzgz zzamf;
  protected transient boolean zzamg;
  
  public zzb(Context paramContext, AdSizeParcel paramAdSizeParcel, String paramString, zzgz paramzzgz, VersionInfoParcel paramVersionInfoParcel, zzd paramzzd)
  {
    this(new zzv(paramContext, paramAdSizeParcel, paramString, paramVersionInfoParcel), paramzzgz, null, paramzzd);
  }
  
  protected zzb(zzv paramzzv, zzgz paramzzgz, @Nullable zzr paramzzr, zzd paramzzd)
  {
    super(paramzzv, paramzzr, paramzzd);
    this.zzamf = paramzzgz;
    this.mMessenger = new Messenger(new zzid(this.zzaly.zzahs));
    this.zzamg = false;
  }
  
  private AdRequestInfoParcel.zza zza(AdRequestParcel paramAdRequestParcel, Bundle paramBundle, zzkq paramzzkq)
  {
    ApplicationInfo localApplicationInfo = this.zzaly.zzahs.getApplicationInfo();
    DisplayMetrics localDisplayMetrics;
    Object localObject1;
    String str2;
    String str3;
    long l1;
    String str4;
    Bundle localBundle;
    ArrayList localArrayList;
    PackageInfo localPackageInfo2;
    try
    {
      PackageInfo localPackageInfo1 = this.zzaly.zzahs.getPackageManager().getPackageInfo(localApplicationInfo.packageName, 0);
      localDisplayMetrics = this.zzaly.zzahs.getResources().getDisplayMetrics();
      Object localObject2 = null;
      localObject1 = localObject2;
      if (this.zzaly.zzarj != null)
      {
        localObject1 = localObject2;
        if (this.zzaly.zzarj.getParent() != null)
        {
          localObject1 = new int[2];
          this.zzaly.zzarj.getLocationOnScreen((int[])localObject1);
          int k = localObject1[0];
          int m = localObject1[1];
          int n = this.zzaly.zzarj.getWidth();
          int i1 = this.zzaly.zzarj.getHeight();
          int j = 0;
          i = j;
          if (this.zzaly.zzarj.isShown())
          {
            i = j;
            if (k + n > 0)
            {
              i = j;
              if (m + i1 > 0)
              {
                i = j;
                if (k <= localDisplayMetrics.widthPixels)
                {
                  i = j;
                  if (m <= localDisplayMetrics.heightPixels) {
                    i = 1;
                  }
                }
              }
            }
          }
          localObject1 = new Bundle(5);
          ((Bundle)localObject1).putInt("x", k);
          ((Bundle)localObject1).putInt("y", m);
          ((Bundle)localObject1).putInt("width", n);
          ((Bundle)localObject1).putInt("height", i1);
          ((Bundle)localObject1).putInt("visible", i);
        }
      }
      str2 = zzu.zzgq().zzus();
      this.zzaly.zzarp = new zzkp(str2, this.zzaly.zzarg);
      this.zzaly.zzarp.zzt(paramAdRequestParcel);
      str3 = zzu.zzgm().zza(this.zzaly.zzahs, this.zzaly.zzarj, this.zzaly.zzarm);
      l2 = 0L;
      l1 = l2;
      if (this.zzaly.zzart == null) {}
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      try
      {
        l1 = this.zzaly.zzart.getValue();
        str4 = UUID.randomUUID().toString();
        localBundle = zzu.zzgq().zza(this.zzaly.zzahs, this, str2);
        localArrayList = new ArrayList();
        int i = 0;
        while (i < this.zzaly.zzarz.size())
        {
          localArrayList.add((String)this.zzaly.zzarz.keyAt(i));
          i += 1;
          continue;
          localNameNotFoundException = localNameNotFoundException;
          localPackageInfo2 = null;
        }
      }
      catch (RemoteException localRemoteException)
      {
        for (;;)
        {
          long l2;
          zzkx.zzdi("Cannot get correlation id, default to 0.");
          l1 = l2;
        }
      }
    }
    boolean bool1;
    if (this.zzaly.zzaru != null)
    {
      bool1 = true;
      if ((this.zzaly.zzarv == null) || (!zzu.zzgq().zzvi())) {
        break label815;
      }
    }
    label815:
    for (boolean bool2 = true;; bool2 = false)
    {
      Object localObject3 = this.zzamb.zzamr;
      localObject3 = this.zzaly.zzahs;
      String str1 = "";
      localObject3 = str1;
      if (((Boolean)zzdr.zzbkn.get()).booleanValue())
      {
        zzkx.zzdg("Getting webview cookie from CookieManager.");
        CookieManager localCookieManager = zzu.zzgo().zzal(this.zzaly.zzahs);
        localObject3 = str1;
        if (localCookieManager != null) {
          localObject3 = localCookieManager.getCookie("googleads.g.doubleclick.net");
        }
      }
      str1 = null;
      if (paramzzkq != null) {
        str1 = paramzzkq.zzuo();
      }
      return new AdRequestInfoParcel.zza((Bundle)localObject1, paramAdRequestParcel, this.zzaly.zzarm, this.zzaly.zzarg, localApplicationInfo, localPackageInfo2, str2, zzu.zzgq().getSessionId(), this.zzaly.zzari, localBundle, this.zzaly.zzase, localArrayList, paramBundle, zzu.zzgq().zzuw(), this.mMessenger, localDisplayMetrics.widthPixels, localDisplayMetrics.heightPixels, localDisplayMetrics.density, str3, l1, str4, zzdr.zzlq(), this.zzaly.zzarf, this.zzaly.zzasa, new CapabilityParcel(bool1, bool2, false), this.zzaly.zzht(), zzu.zzgm().zzfr(), zzu.zzgm().zzft(), zzu.zzgm().zzai(this.zzaly.zzahs), zzu.zzgm().zzt(this.zzaly.zzarj), this.zzaly.zzahs instanceof Activity, zzu.zzgq().zzvb(), (String)localObject3, str1, zzu.zzgq().zzve(), zzu.zzhj().zzni(), zzu.zzgm().zzvv(), zzu.zzgu().zzwf());
      bool1 = false;
      break;
    }
  }
  
  public String getMediationAdapterClassName()
  {
    if (this.zzaly.zzarn == null) {
      return null;
    }
    return this.zzaly.zzarn.zzbwo;
  }
  
  public void onAdClicked()
  {
    if (this.zzaly.zzarn == null)
    {
      zzkx.zzdi("Ad state was null when trying to ping click URLs.");
      return;
    }
    if ((this.zzaly.zzarn.zzcsk != null) && (this.zzaly.zzarn.zzcsk.zzbvk != null)) {
      zzu.zzhf().zza(this.zzaly.zzahs, this.zzaly.zzari.zzda, this.zzaly.zzarn, this.zzaly.zzarg, false, this.zzaly.zzarn.zzcsk.zzbvk);
    }
    if ((this.zzaly.zzarn.zzbwm != null) && (this.zzaly.zzarn.zzbwm.zzbux != null)) {
      zzu.zzhf().zza(this.zzaly.zzahs, this.zzaly.zzari.zzda, this.zzaly.zzarn, this.zzaly.zzarg, false, this.zzaly.zzarn.zzbwm.zzbux);
    }
    super.onAdClicked();
  }
  
  public void onPause()
  {
    this.zzama.zzl(this.zzaly.zzarn);
  }
  
  public void onResume()
  {
    this.zzama.zzm(this.zzaly.zzarn);
  }
  
  public void pause()
  {
    zzaa.zzhs("pause must be called on the main UI thread.");
    if ((this.zzaly.zzarn != null) && (this.zzaly.zzarn.zzcbm != null) && (this.zzaly.zzhp())) {
      zzu.zzgo().zzl(this.zzaly.zzarn.zzcbm);
    }
    if ((this.zzaly.zzarn != null) && (this.zzaly.zzarn.zzbwn != null)) {}
    try
    {
      this.zzaly.zzarn.zzbwn.pause();
      this.zzama.zzl(this.zzaly.zzarn);
      this.zzalx.pause();
      return;
    }
    catch (RemoteException localRemoteException)
    {
      for (;;)
      {
        zzkx.zzdi("Could not pause mediation adapter.");
      }
    }
  }
  
  public void recordImpression()
  {
    zza(this.zzaly.zzarn, false);
  }
  
  public void resume()
  {
    zzaa.zzhs("resume must be called on the main UI thread.");
    Object localObject2 = null;
    Object localObject1 = localObject2;
    if (this.zzaly.zzarn != null)
    {
      localObject1 = localObject2;
      if (this.zzaly.zzarn.zzcbm != null) {
        localObject1 = this.zzaly.zzarn.zzcbm;
      }
    }
    if ((localObject1 != null) && (this.zzaly.zzhp())) {
      zzu.zzgo().zzm(this.zzaly.zzarn.zzcbm);
    }
    if ((this.zzaly.zzarn != null) && (this.zzaly.zzarn.zzbwn != null)) {}
    try
    {
      this.zzaly.zzarn.zzbwn.resume();
      if ((localObject1 == null) || (!((zzmd)localObject1).zzxj())) {
        this.zzalx.resume();
      }
      this.zzama.zzm(this.zzaly.zzarn);
      return;
    }
    catch (RemoteException localRemoteException)
    {
      for (;;)
      {
        zzkx.zzdi("Could not resume mediation adapter.");
      }
    }
  }
  
  public void showInterstitial()
  {
    throw new IllegalStateException("showInterstitial is not supported for current ad type");
  }
  
  public void zza(zzig paramzzig)
  {
    zzaa.zzhs("setInAppPurchaseListener must be called on the main UI thread.");
    this.zzaly.zzaru = paramzzig;
  }
  
  public void zza(zzik paramzzik, @Nullable String paramString)
  {
    zzaa.zzhs("setPlayStorePurchaseParams must be called on the main UI thread.");
    this.zzaly.zzasf = new zzk(paramString);
    this.zzaly.zzarv = paramzzik;
    if ((!zzu.zzgq().zzuv()) && (paramzzik != null)) {
      paramzzik = (Future)new zzc(this.zzaly.zzahs, this.zzaly.zzarv, this.zzaly.zzasf).zzrz();
    }
  }
  
  protected void zza(@Nullable zzko paramzzko, boolean paramBoolean)
  {
    if (paramzzko == null) {
      zzkx.zzdi("Ad state was null when trying to ping impression URLs.");
    }
    do
    {
      return;
      super.zzc(paramzzko);
      if ((paramzzko.zzcsk != null) && (paramzzko.zzcsk.zzbvl != null)) {
        zzu.zzhf().zza(this.zzaly.zzahs, this.zzaly.zzari.zzda, paramzzko, this.zzaly.zzarg, paramBoolean, paramzzko.zzcsk.zzbvl);
      }
    } while ((paramzzko.zzbwm == null) || (paramzzko.zzbwm.zzbuy == null));
    zzu.zzhf().zza(this.zzaly.zzahs, this.zzaly.zzari.zzda, paramzzko, this.zzaly.zzarg, paramBoolean, paramzzko.zzbwm.zzbuy);
  }
  
  public void zza(String paramString, ArrayList<String> paramArrayList)
  {
    paramArrayList = new com.google.android.gms.ads.internal.purchase.zzd(paramString, paramArrayList, this.zzaly.zzahs, this.zzaly.zzari.zzda);
    if (this.zzaly.zzaru == null)
    {
      zzkx.zzdi("InAppPurchaseListener is not set. Try to launch default purchase flow.");
      if (!zzm.zzkr().zzap(this.zzaly.zzahs))
      {
        zzkx.zzdi("Google Play Service unavailable, cannot launch default purchase flow.");
        return;
      }
      if (this.zzaly.zzarv == null)
      {
        zzkx.zzdi("PlayStorePurchaseListener is not set.");
        return;
      }
      if (this.zzaly.zzasf == null)
      {
        zzkx.zzdi("PlayStorePurchaseVerifier is not initialized.");
        return;
      }
      if (this.zzaly.zzasj)
      {
        zzkx.zzdi("An in-app purchase request is already in progress, abort");
        return;
      }
      this.zzaly.zzasj = true;
      try
      {
        if (!this.zzaly.zzarv.isValidPurchase(paramString))
        {
          this.zzaly.zzasj = false;
          return;
        }
      }
      catch (RemoteException paramString)
      {
        zzkx.zzdi("Could not start In-App purchase.");
        this.zzaly.zzasj = false;
        return;
      }
      zzu.zzha().zza(this.zzaly.zzahs, this.zzaly.zzari.zzcyc, new GInAppPurchaseManagerInfoParcel(this.zzaly.zzahs, this.zzaly.zzasf, paramArrayList, this));
      return;
    }
    try
    {
      this.zzaly.zzaru.zza(paramArrayList);
      return;
    }
    catch (RemoteException paramString)
    {
      zzkx.zzdi("Could not start In-App purchase.");
    }
  }
  
  public void zza(String paramString, boolean paramBoolean, int paramInt, final Intent paramIntent, zzf paramzzf)
  {
    try
    {
      if (this.zzaly.zzarv != null) {
        this.zzaly.zzarv.zza(new com.google.android.gms.ads.internal.purchase.zzg(this.zzaly.zzahs, paramString, paramBoolean, paramInt, paramIntent, paramzzf));
      }
      zzlb.zzcvl.postDelayed(new Runnable()
      {
        public void run()
        {
          int i = zzu.zzha().zzd(paramIntent);
          zzu.zzha();
          if ((i == 0) && (zzb.this.zzaly.zzarn != null) && (zzb.this.zzaly.zzarn.zzcbm != null) && (zzb.this.zzaly.zzarn.zzcbm.zzxa() != null)) {
            zzb.this.zzaly.zzarn.zzcbm.zzxa().close();
          }
          zzb.this.zzaly.zzasj = false;
        }
      }, 500L);
      return;
    }
    catch (RemoteException paramString)
    {
      for (;;)
      {
        zzkx.zzdi("Fail to invoke PlayStorePurchaseListener.");
      }
    }
  }
  
  public boolean zza(AdRequestParcel paramAdRequestParcel, zzdz paramzzdz)
  {
    if (!zzep()) {
      return false;
    }
    Bundle localBundle = zzu.zzgm().zzak(this.zzaly.zzahs);
    this.zzalx.cancel();
    this.zzaly.zzasi = 0;
    zzkq localzzkq = null;
    if (((Boolean)zzdr.zzbjv.get()).booleanValue())
    {
      localzzkq = zzu.zzgq().zzvf();
      zzu.zzhi().zza(this.zzaly.zzahs, this.zzaly.zzari, false, localzzkq, localzzkq.zzup(), this.zzaly.zzarg);
    }
    paramAdRequestParcel = zza(paramAdRequestParcel, localBundle, localzzkq);
    paramzzdz.zzg("seq_num", paramAdRequestParcel.zzcjx);
    paramzzdz.zzg("request_id", paramAdRequestParcel.zzcki);
    paramzzdz.zzg("session_id", paramAdRequestParcel.zzcjy);
    if (paramAdRequestParcel.zzcjv != null) {
      paramzzdz.zzg("app_version", String.valueOf(paramAdRequestParcel.zzcjv.versionCode));
    }
    this.zzaly.zzark = zzu.zzgi().zza(this.zzaly.zzahs, paramAdRequestParcel, this.zzaly.zzarh, this);
    return true;
  }
  
  protected boolean zza(AdRequestParcel paramAdRequestParcel, zzko paramzzko, boolean paramBoolean)
  {
    if ((!paramBoolean) && (this.zzaly.zzhp()))
    {
      if (paramzzko.zzbvq <= 0L) {
        break label43;
      }
      this.zzalx.zza(paramAdRequestParcel, paramzzko.zzbvq);
    }
    for (;;)
    {
      return this.zzalx.zzfy();
      label43:
      if ((paramzzko.zzcsk != null) && (paramzzko.zzcsk.zzbvq > 0L)) {
        this.zzalx.zza(paramAdRequestParcel, paramzzko.zzcsk.zzbvq);
      } else if ((!paramzzko.zzclb) && (paramzzko.errorCode == 2)) {
        this.zzalx.zzh(paramAdRequestParcel);
      }
    }
  }
  
  boolean zza(zzko paramzzko)
  {
    boolean bool = false;
    Object localObject;
    if (this.zzalz != null)
    {
      localObject = this.zzalz;
      this.zzalz = null;
    }
    for (;;)
    {
      return zza((AdRequestParcel)localObject, paramzzko, bool);
      AdRequestParcel localAdRequestParcel = paramzzko.zzcju;
      localObject = localAdRequestParcel;
      if (localAdRequestParcel.extras != null)
      {
        bool = localAdRequestParcel.extras.getBoolean("_noRefresh", false);
        localObject = localAdRequestParcel;
      }
    }
  }
  
  protected boolean zza(@Nullable zzko paramzzko1, zzko paramzzko2)
  {
    int i = 0;
    if ((paramzzko1 != null) && (paramzzko1.zzbwp != null)) {
      paramzzko1.zzbwp.zza(null);
    }
    if (paramzzko2.zzbwp != null) {
      paramzzko2.zzbwp.zza(this);
    }
    int j;
    if (paramzzko2.zzcsk != null)
    {
      j = paramzzko2.zzcsk.zzbvw;
      i = paramzzko2.zzcsk.zzbvx;
    }
    for (;;)
    {
      this.zzaly.zzasg.zzj(j, i);
      return true;
      j = 0;
    }
  }
  
  public void zzb(zzko paramzzko)
  {
    super.zzb(paramzzko);
    if (paramzzko.zzbwm != null)
    {
      zzkx.zzdg("Disable the debug gesture detector on the mediation ad frame.");
      if (this.zzaly.zzarj != null) {
        this.zzaly.zzarj.zzhx();
      }
      zzkx.zzdg("Pinging network fill URLs.");
      zzu.zzhf().zza(this.zzaly.zzahs, this.zzaly.zzari.zzda, paramzzko, this.zzaly.zzarg, false, paramzzko.zzbwm.zzbuz);
      if ((paramzzko.zzcsk != null) && (paramzzko.zzcsk.zzbvn != null) && (paramzzko.zzcsk.zzbvn.size() > 0))
      {
        zzkx.zzdg("Pinging urls remotely");
        zzu.zzgm().zza(this.zzaly.zzahs, paramzzko.zzcsk.zzbvn);
      }
    }
    for (;;)
    {
      if ((paramzzko.errorCode == 3) && (paramzzko.zzcsk != null) && (paramzzko.zzcsk.zzbvm != null))
      {
        zzkx.zzdg("Pinging no fill URLs.");
        zzu.zzhf().zza(this.zzaly.zzahs, this.zzaly.zzari.zzda, paramzzko, this.zzaly.zzarg, false, paramzzko.zzcsk.zzbvm);
      }
      return;
      zzkx.zzdg("Enable the debug gesture detector on the admob ad frame.");
      if (this.zzaly.zzarj != null) {
        this.zzaly.zzarj.zzhw();
      }
    }
  }
  
  protected boolean zzc(AdRequestParcel paramAdRequestParcel)
  {
    return (super.zzc(paramAdRequestParcel)) && (!this.zzamg);
  }
  
  protected boolean zzep()
  {
    boolean bool = true;
    if ((!zzu.zzgm().zza(this.zzaly.zzahs.getPackageManager(), this.zzaly.zzahs.getPackageName(), "android.permission.INTERNET")) || (!zzu.zzgm().zzy(this.zzaly.zzahs))) {
      bool = false;
    }
    return bool;
  }
  
  public void zzeq()
  {
    this.zzama.zzj(this.zzaly.zzarn);
    this.zzamg = false;
    zzek();
    this.zzaly.zzarp.zzui();
  }
  
  public void zzer()
  {
    this.zzamg = true;
    zzem();
  }
  
  public void zzes()
  {
    onAdClicked();
  }
  
  public void zzet()
  {
    zzeq();
  }
  
  public void zzeu()
  {
    zzeh();
  }
  
  public void zzev()
  {
    zzer();
  }
  
  public void zzew()
  {
    if (this.zzaly.zzarn != null)
    {
      String str = this.zzaly.zzarn.zzbwo;
      zzkx.zzdi(String.valueOf(str).length() + 74 + "Mediation adapter " + str + " refreshed, but mediation adapters should never refresh.");
    }
    zza(this.zzaly.zzarn, true);
    zzen();
  }
  
  public void zzex()
  {
    recordImpression();
  }
  
  public void zzey()
  {
    zzu.zzgm().runOnUiThread(new Runnable()
    {
      public void run()
      {
        zzb.this.zzalx.pause();
      }
    });
  }
  
  public void zzez()
  {
    zzu.zzgm().runOnUiThread(new Runnable()
    {
      public void run()
      {
        zzb.this.zzalx.resume();
      }
    });
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */