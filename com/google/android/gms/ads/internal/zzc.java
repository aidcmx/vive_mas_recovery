package com.google.android.gms.ads.internal;

import android.content.Context;
import android.os.Handler;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.internal.zzco;
import com.google.android.gms.internal.zzdn;
import com.google.android.gms.internal.zzdr;
import com.google.android.gms.internal.zzdz;
import com.google.android.gms.internal.zzea;
import com.google.android.gms.internal.zzec;
import com.google.android.gms.internal.zzed;
import com.google.android.gms.internal.zzfe;
import com.google.android.gms.internal.zzgi;
import com.google.android.gms.internal.zzgz;
import com.google.android.gms.internal.zzhw;
import com.google.android.gms.internal.zziu;
import com.google.android.gms.internal.zzji;
import com.google.android.gms.internal.zzko;
import com.google.android.gms.internal.zzko.zza;
import com.google.android.gms.internal.zzkx;
import com.google.android.gms.internal.zzlb;
import com.google.android.gms.internal.zzle;
import com.google.android.gms.internal.zzmd;
import com.google.android.gms.internal.zzme;
import com.google.android.gms.internal.zzmf;
import java.util.Map;

@zzji
public abstract class zzc
  extends zzb
  implements zzh, zzhw
{
  public zzc(Context paramContext, AdSizeParcel paramAdSizeParcel, String paramString, zzgz paramzzgz, VersionInfoParcel paramVersionInfoParcel, zzd paramzzd)
  {
    super(paramContext, paramAdSizeParcel, paramString, paramzzgz, paramVersionInfoParcel, paramzzd);
  }
  
  protected zzmd zza(zzko.zza paramzza, @Nullable zze paramzze, @Nullable com.google.android.gms.ads.internal.safebrowsing.zzc paramzzc)
  {
    zzmd localzzmd1 = null;
    View localView = this.zzaly.zzarj.getNextView();
    if ((localView instanceof zzmd))
    {
      localzzmd1 = (zzmd)localView;
      if (!((Boolean)zzdr.zzbft.get()).booleanValue()) {
        break label223;
      }
      zzkx.zzdg("Reusing webview...");
      localzzmd1.zza(this.zzaly.zzahs, this.zzaly.zzarm, this.zzalt);
    }
    for (;;)
    {
      zzmd localzzmd2 = localzzmd1;
      if (localzzmd1 == null)
      {
        if (localView != null) {
          this.zzaly.zzarj.removeView(localView);
        }
        localzzmd1 = zzu.zzgn().zza(this.zzaly.zzahs, this.zzaly.zzarm, false, false, this.zzaly.zzarh, this.zzaly.zzari, this.zzalt, this, this.zzamb);
        localzzmd2 = localzzmd1;
        if (this.zzaly.zzarm.zzazs == null)
        {
          zzb(localzzmd1.getView());
          localzzmd2 = localzzmd1;
        }
      }
      localzzmd2.zzxc().zza(this, this, this, this, false, this, null, paramzze, this, paramzzc);
      zza(localzzmd2);
      localzzmd2.zzdk(paramzza.zzcmx.zzcki);
      return localzzmd2;
      label223:
      localzzmd1.destroy();
      localzzmd1 = null;
    }
  }
  
  public void zza(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    zzem();
  }
  
  public void zza(zzed paramzzed)
  {
    zzaa.zzhs("setOnCustomRenderedAdLoadedListener must be called on the main UI thread.");
    this.zzaly.zzasc = paramzzed;
  }
  
  protected void zza(zzgi paramzzgi)
  {
    paramzzgi.zza("/trackActiveViewUnit", new zzfe()
    {
      public void zza(zzmd paramAnonymouszzmd, Map<String, String> paramAnonymousMap)
      {
        if (zzc.this.zzaly.zzarn != null)
        {
          zzc.this.zzama.zza(zzc.this.zzaly.zzarm, zzc.this.zzaly.zzarn, paramAnonymouszzmd.getView(), paramAnonymouszzmd);
          return;
        }
        zzkx.zzdi("Request to enable ActiveView before adState is available.");
      }
    });
  }
  
  protected void zza(final zzko.zza paramzza, final zzdz paramzzdz)
  {
    if (paramzza.errorCode != -2)
    {
      zzlb.zzcvl.post(new Runnable()
      {
        public void run()
        {
          zzc.this.zzb(new zzko(paramzza, null, null, null, null, null, null, null));
        }
      });
      return;
    }
    if (paramzza.zzarm != null) {
      this.zzaly.zzarm = paramzza.zzarm;
    }
    if ((paramzza.zzcsu.zzclb) && (!paramzza.zzcsu.zzazv))
    {
      this.zzaly.zzasi = 0;
      this.zzaly.zzarl = zzu.zzgl().zza(this.zzaly.zzahs, this, paramzza, this.zzaly.zzarh, null, this.zzamf, this, paramzzdz);
      return;
    }
    Object localObject = this.zzamb.zzams;
    localObject = this.zzaly.zzahs;
    localObject = paramzza.zzcsu;
    zzlb.zzcvl.post(new Runnable()
    {
      public void run()
      {
        if ((paramzza.zzcsu.zzclk) && (zzc.this.zzaly.zzasc != null))
        {
          Object localObject = null;
          if (paramzza.zzcsu.zzcbo != null) {
            localObject = zzu.zzgm().zzcz(paramzza.zzcsu.zzcbo);
          }
          localObject = new zzea(zzc.this, (String)localObject, paramzza.zzcsu.body);
          zzc.this.zzaly.zzasi = 1;
          try
          {
            zzc.this.zzalw = false;
            zzc.this.zzaly.zzasc.zza((zzec)localObject);
            return;
          }
          catch (RemoteException localRemoteException)
          {
            zzkx.zzc("Could not call the onCustomRenderedAdLoadedListener.", localRemoteException);
            zzc.this.zzalw = true;
          }
        }
        final zze localzze = new zze(zzc.this.zzaly.zzahs, paramzza);
        zzmd localzzmd = zzc.this.zza(paramzza, localzze, this.zzaml);
        localzzmd.setOnTouchListener(new View.OnTouchListener()
        {
          public boolean onTouch(View paramAnonymous2View, MotionEvent paramAnonymous2MotionEvent)
          {
            localzze.recordClick();
            return false;
          }
        });
        localzzmd.setOnClickListener(new View.OnClickListener()
        {
          public void onClick(View paramAnonymous2View)
          {
            localzze.recordClick();
          }
        });
        zzc.this.zzaly.zzasi = 0;
        zzc.this.zzaly.zzarl = zzu.zzgl().zza(zzc.this.zzaly.zzahs, zzc.this, paramzza, zzc.this.zzaly.zzarh, localzzmd, zzc.this.zzamf, zzc.this, paramzzdz);
      }
    });
  }
  
  protected boolean zza(@Nullable zzko paramzzko1, zzko paramzzko2)
  {
    if ((this.zzaly.zzhp()) && (this.zzaly.zzarj != null)) {
      this.zzaly.zzarj.zzhv().zzdc(paramzzko2.zzclg);
    }
    return super.zza(paramzzko1, paramzzko2);
  }
  
  public void zzc(View paramView)
  {
    this.zzaly.zzash = paramView;
    zzb(new zzko(this.zzaly.zzaro, null, null, null, null, null, null, null));
  }
  
  public void zzfa()
  {
    onAdClicked();
  }
  
  public void zzfb()
  {
    recordImpression();
    zzei();
  }
  
  public void zzfc()
  {
    zzek();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */