package com.google.android.gms.ads.internal;

import com.google.android.gms.ads.internal.overlay.zzj;
import com.google.android.gms.ads.internal.overlay.zzm;
import com.google.android.gms.ads.internal.overlay.zzn;
import com.google.android.gms.ads.internal.overlay.zzt;
import com.google.android.gms.ads.internal.safebrowsing.zza;
import com.google.android.gms.internal.zzfb;
import com.google.android.gms.internal.zzfu;
import com.google.android.gms.internal.zzji;

@zzji
public class zzd
{
  public final zzfu zzamp;
  public final zzj zzamq;
  public final zzm zzamr;
  public final com.google.android.gms.ads.internal.safebrowsing.zzd zzams;
  
  public zzd(zzfu paramzzfu, zzj paramzzj, zzm paramzzm, com.google.android.gms.ads.internal.safebrowsing.zzd paramzzd)
  {
    this.zzamp = paramzzfu;
    this.zzamq = paramzzj;
    this.zzamr = paramzzm;
    this.zzams = paramzzd;
  }
  
  public static zzd zzfd()
  {
    return new zzd(new zzfb(), new zzn(), new zzt(), new zza());
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */