package com.google.android.gms.ads.internal;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import com.google.android.gms.ads.internal.request.AutoClickProtectionConfigurationParcel;
import com.google.android.gms.internal.zzji;
import com.google.android.gms.internal.zzko.zza;
import com.google.android.gms.internal.zzkx;
import com.google.android.gms.internal.zzlb;
import java.util.Iterator;
import java.util.List;

@zzji
public class zze
{
  private final Context mContext;
  private final AutoClickProtectionConfigurationParcel zzamt;
  private boolean zzamu;
  
  public zze(Context paramContext)
  {
    this(paramContext, false);
  }
  
  public zze(Context paramContext, @Nullable zzko.zza paramzza)
  {
    this.mContext = paramContext;
    if ((paramzza != null) && (paramzza.zzcsu.zzclu != null))
    {
      this.zzamt = paramzza.zzcsu.zzclu;
      return;
    }
    this.zzamt = new AutoClickProtectionConfigurationParcel();
  }
  
  public zze(Context paramContext, boolean paramBoolean)
  {
    this.mContext = paramContext;
    this.zzamt = new AutoClickProtectionConfigurationParcel(paramBoolean);
  }
  
  public void recordClick()
  {
    this.zzamu = true;
  }
  
  public boolean zzfe()
  {
    return (!this.zzamt.zzclz) || (this.zzamu);
  }
  
  public void zzy(@Nullable String paramString)
  {
    if (paramString != null) {}
    for (;;)
    {
      zzkx.zzdh("Action was blocked because no touch was detected.");
      if ((!this.zzamt.zzclz) || (this.zzamt.zzcma == null)) {
        break;
      }
      Iterator localIterator = this.zzamt.zzcma.iterator();
      while (localIterator.hasNext())
      {
        String str = (String)localIterator.next();
        if (!TextUtils.isEmpty(str))
        {
          str = str.replace("{NAVIGATION_URL}", Uri.encode(paramString));
          zzu.zzgm().zzc(this.mContext, "", str);
        }
      }
      paramString = "";
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */