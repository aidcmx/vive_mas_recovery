package com.google.android.gms.ads.internal;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.ViewTreeObserver.OnScrollChangedListener;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.client.VideoOptionsParcel;
import com.google.android.gms.ads.internal.client.zzab;
import com.google.android.gms.ads.internal.client.zzm;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.util.zzs;
import com.google.android.gms.internal.zzco;
import com.google.android.gms.internal.zzcu;
import com.google.android.gms.internal.zzdn;
import com.google.android.gms.internal.zzdr;
import com.google.android.gms.internal.zzgz;
import com.google.android.gms.internal.zzji;
import com.google.android.gms.internal.zzko;
import com.google.android.gms.internal.zzko.zza;
import com.google.android.gms.internal.zzkx;
import com.google.android.gms.internal.zzlb;
import com.google.android.gms.internal.zzly;
import com.google.android.gms.internal.zzmd;
import com.google.android.gms.internal.zzme;
import com.google.android.gms.internal.zzme.zzc;
import com.google.android.gms.internal.zzme.zze;
import com.google.android.gms.internal.zzmi;
import java.util.List;

@zzji
public class zzf
  extends zzc
  implements ViewTreeObserver.OnGlobalLayoutListener, ViewTreeObserver.OnScrollChangedListener
{
  private boolean zzamv;
  
  public zzf(Context paramContext, AdSizeParcel paramAdSizeParcel, String paramString, zzgz paramzzgz, VersionInfoParcel paramVersionInfoParcel, zzd paramzzd)
  {
    super(paramContext, paramAdSizeParcel, paramString, paramzzgz, paramVersionInfoParcel, paramzzd);
  }
  
  private AdSizeParcel zzb(zzko.zza paramzza)
  {
    if (paramzza.zzcsu.zzazu) {
      return this.zzaly.zzarm;
    }
    paramzza = paramzza.zzcsu.zzcle;
    if (paramzza != null)
    {
      paramzza = paramzza.split("[xX]");
      paramzza[0] = paramzza[0].trim();
      paramzza[1] = paramzza[1].trim();
    }
    for (paramzza = new AdSize(Integer.parseInt(paramzza[0]), Integer.parseInt(paramzza[1]));; paramzza = this.zzaly.zzarm.zzkd()) {
      return new AdSizeParcel(this.zzaly.zzahs, paramzza);
    }
  }
  
  private boolean zzb(@Nullable zzko paramzzko1, zzko paramzzko2)
  {
    View localView1;
    if (paramzzko2.zzclb)
    {
      localView1 = zzn.zzg(paramzzko2);
      if (localView1 == null)
      {
        zzkx.zzdi("Could not get mediation view");
        return false;
      }
      View localView2 = this.zzaly.zzarj.getNextView();
      if (localView2 != null)
      {
        if ((localView2 instanceof zzmd)) {
          ((zzmd)localView2).destroy();
        }
        this.zzaly.zzarj.removeView(localView2);
      }
      if (zzn.zzh(paramzzko2)) {}
    }
    for (;;)
    {
      try
      {
        zzb(localView1);
        if (this.zzaly.zzarj.getChildCount() > 1) {
          this.zzaly.zzarj.showNext();
        }
        if (paramzzko1 != null)
        {
          paramzzko1 = this.zzaly.zzarj.getNextView();
          if (!(paramzzko1 instanceof zzmd)) {
            break label271;
          }
          ((zzmd)paramzzko1).zza(this.zzaly.zzahs, this.zzaly.zzarm, this.zzalt);
          this.zzaly.zzho();
        }
        this.zzaly.zzarj.setVisibility(0);
        return true;
      }
      catch (Throwable paramzzko1)
      {
        zzkx.zzc("Could not add mediation view to view hierarchy.", paramzzko1);
        return false;
      }
      if ((paramzzko2.zzcsm != null) && (paramzzko2.zzcbm != null))
      {
        paramzzko2.zzcbm.zza(paramzzko2.zzcsm);
        this.zzaly.zzarj.removeAllViews();
        this.zzaly.zzarj.setMinimumWidth(paramzzko2.zzcsm.widthPixels);
        this.zzaly.zzarj.setMinimumHeight(paramzzko2.zzcsm.heightPixels);
        zzb(paramzzko2.zzcbm.getView());
        continue;
        label271:
        if (paramzzko1 != null) {
          this.zzaly.zzarj.removeView(paramzzko1);
        }
      }
    }
  }
  
  private void zze(final zzko paramzzko)
  {
    if (!zzs.zzayq()) {}
    do
    {
      do
      {
        return;
        if (!this.zzaly.zzhp()) {
          break;
        }
      } while (paramzzko.zzcbm == null);
      if (paramzzko.zzcsi != null) {
        this.zzama.zza(this.zzaly.zzarm, paramzzko);
      }
      if (paramzzko.zzic())
      {
        new zzcu(this.zzaly.zzahs, paramzzko.zzcbm.getView()).zza(paramzzko.zzcbm);
        return;
      }
      paramzzko.zzcbm.zzxc().zza(new zzme.zzc()
      {
        public void zzfg()
        {
          new zzcu(zzf.this.zzaly.zzahs, paramzzko.zzcbm.getView()).zza(paramzzko.zzcbm);
        }
      });
      return;
    } while ((this.zzaly.zzash == null) || (paramzzko.zzcsi == null));
    this.zzama.zza(this.zzaly.zzarm, paramzzko, this.zzaly.zzash);
  }
  
  public void onGlobalLayout()
  {
    zzf(this.zzaly.zzarn);
  }
  
  public void onScrollChanged()
  {
    zzf(this.zzaly.zzarn);
  }
  
  public void setManualImpressionsEnabled(boolean paramBoolean)
  {
    zzaa.zzhs("setManualImpressionsEnabled must be called from the main thread.");
    this.zzamv = paramBoolean;
  }
  
  public void showInterstitial()
  {
    throw new IllegalStateException("Interstitial is NOT supported by BannerAdManager.");
  }
  
  protected zzmd zza(zzko.zza paramzza, @Nullable zze paramzze, @Nullable com.google.android.gms.ads.internal.safebrowsing.zzc paramzzc)
  {
    if ((this.zzaly.zzarm.zzazs == null) && (this.zzaly.zzarm.zzazu)) {
      this.zzaly.zzarm = zzb(paramzza);
    }
    return super.zza(paramzza, paramzze, paramzzc);
  }
  
  protected void zza(@Nullable zzko paramzzko, boolean paramBoolean)
  {
    super.zza(paramzzko, paramBoolean);
    if (zzn.zzh(paramzzko)) {
      zzn.zza(paramzzko, new zza());
    }
  }
  
  public boolean zza(@Nullable zzko paramzzko1, final zzko paramzzko2)
  {
    if (!super.zza(paramzzko1, paramzzko2)) {
      return false;
    }
    if ((this.zzaly.zzhp()) && (!zzb(paramzzko1, paramzzko2)))
    {
      zzh(0);
      return false;
    }
    final Object localObject;
    if (paramzzko2.zzclt)
    {
      zzf(paramzzko2);
      zzu.zzhk().zza(this.zzaly.zzarj, this);
      zzu.zzhk().zza(this.zzaly.zzarj, this);
      if (!paramzzko2.zzcsj)
      {
        localObject = new Runnable()
        {
          public void run()
          {
            zzf.this.zzf(zzf.this.zzaly.zzarn);
          }
        };
        if (paramzzko2.zzcbm == null) {
          break label205;
        }
        paramzzko1 = paramzzko2.zzcbm.zzxc();
        if (paramzzko1 != null) {
          paramzzko1.zza(new zzme.zze()
          {
            public void zzff()
            {
              if (!paramzzko2.zzcsj)
              {
                zzu.zzgm();
                zzlb.zzb(localObject);
              }
            }
          });
        }
      }
      label128:
      if (paramzzko2.zzcbm == null) {
        break label244;
      }
      localObject = paramzzko2.zzcbm.zzxn();
      zzme localzzme = paramzzko2.zzcbm.zzxc();
      paramzzko1 = (zzko)localObject;
      if (localzzme != null) {
        localzzme.zzya();
      }
    }
    label205:
    label244:
    for (paramzzko1 = (zzko)localObject;; paramzzko1 = null)
    {
      if ((this.zzaly.zzasb != null) && (paramzzko1 != null)) {
        paramzzko1.zzaq(this.zzaly.zzasb.zzbck);
      }
      zze(paramzzko2);
      return true;
      paramzzko1 = null;
      break;
      if ((this.zzaly.zzhq()) && (!((Boolean)zzdr.zzbiw.get()).booleanValue())) {
        break label128;
      }
      zza(paramzzko2, false);
      break label128;
    }
  }
  
  public boolean zzb(AdRequestParcel paramAdRequestParcel)
  {
    return super.zzb(zze(paramAdRequestParcel));
  }
  
  AdRequestParcel zze(AdRequestParcel paramAdRequestParcel)
  {
    if (paramAdRequestParcel.zzayq == this.zzamv) {
      return paramAdRequestParcel;
    }
    int i = paramAdRequestParcel.versionCode;
    long l = paramAdRequestParcel.zzayl;
    Bundle localBundle = paramAdRequestParcel.extras;
    int j = paramAdRequestParcel.zzaym;
    List localList = paramAdRequestParcel.zzayn;
    boolean bool2 = paramAdRequestParcel.zzayo;
    int k = paramAdRequestParcel.zzayp;
    if ((paramAdRequestParcel.zzayq) || (this.zzamv)) {}
    for (boolean bool1 = true;; bool1 = false) {
      return new AdRequestParcel(i, l, localBundle, j, localList, bool2, k, bool1, paramAdRequestParcel.zzayr, paramAdRequestParcel.zzays, paramAdRequestParcel.zzayt, paramAdRequestParcel.zzayu, paramAdRequestParcel.zzayv, paramAdRequestParcel.zzayw, paramAdRequestParcel.zzayx, paramAdRequestParcel.zzayy, paramAdRequestParcel.zzayz, paramAdRequestParcel.zzaza);
    }
  }
  
  @Nullable
  public zzab zzej()
  {
    zzaa.zzhs("getVideoController must be called from the main thread.");
    if ((this.zzaly.zzarn != null) && (this.zzaly.zzarn.zzcbm != null)) {
      return this.zzaly.zzarn.zzcbm.zzxn();
    }
    return null;
  }
  
  protected boolean zzep()
  {
    boolean bool = true;
    if (!zzu.zzgm().zza(this.zzaly.zzahs.getPackageManager(), this.zzaly.zzahs.getPackageName(), "android.permission.INTERNET"))
    {
      zzm.zzkr().zza(this.zzaly.zzarj, this.zzaly.zzarm, "Missing internet permission in AndroidManifest.xml.", "Missing internet permission in AndroidManifest.xml. You must have the following declaration: <uses-permission android:name=\"android.permission.INTERNET\" />");
      bool = false;
    }
    if (!zzu.zzgm().zzy(this.zzaly.zzahs))
    {
      zzm.zzkr().zza(this.zzaly.zzarj, this.zzaly.zzarm, "Missing AdActivity with android:configChanges in AndroidManifest.xml.", "Missing AdActivity with android:configChanges in AndroidManifest.xml. You must have the following declaration within the <application> element: <activity android:name=\"com.google.android.gms.ads.AdActivity\" android:configChanges=\"keyboard|keyboardHidden|orientation|screenLayout|uiMode|screenSize|smallestScreenSize\" />");
      bool = false;
    }
    if ((!bool) && (this.zzaly.zzarj != null)) {
      this.zzaly.zzarj.setVisibility(0);
    }
    return bool;
  }
  
  void zzf(@Nullable zzko paramzzko)
  {
    if (paramzzko == null) {}
    while ((paramzzko.zzcsj) || (this.zzaly.zzarj == null) || (!zzu.zzgm().zza(this.zzaly.zzarj, this.zzaly.zzahs)) || (!this.zzaly.zzarj.getGlobalVisibleRect(new Rect(), null))) {
      return;
    }
    if ((paramzzko != null) && (paramzzko.zzcbm != null) && (paramzzko.zzcbm.zzxc() != null)) {
      paramzzko.zzcbm.zzxc().zza(null);
    }
    zza(paramzzko, false);
    paramzzko.zzcsj = true;
  }
  
  public class zza
  {
    public zza() {}
    
    public void onClick()
    {
      zzf.this.onAdClicked();
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */