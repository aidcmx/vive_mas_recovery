package com.google.android.gms.ads.internal;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.common.util.zze;
import com.google.android.gms.internal.zzdn;
import com.google.android.gms.internal.zzdr;
import com.google.android.gms.internal.zzfe;
import com.google.android.gms.internal.zzgh;
import com.google.android.gms.internal.zzgh.zzc;
import com.google.android.gms.internal.zzgi;
import com.google.android.gms.internal.zzji;
import com.google.android.gms.internal.zzkq;
import com.google.android.gms.internal.zzkr;
import com.google.android.gms.internal.zzkx;
import com.google.android.gms.internal.zzlb;
import com.google.android.gms.internal.zzlw.zzb;
import com.google.android.gms.internal.zzlw.zzc;
import com.google.android.gms.internal.zzmd;
import java.util.Map;
import org.json.JSONObject;

@zzji
public class zzg
{
  private Context mContext;
  private final Object zzako = new Object();
  public final zzfe zzana = new zzfe()
  {
    public void zza(zzmd paramAnonymouszzmd, Map<String, String> paramAnonymousMap)
    {
      paramAnonymouszzmd.zzb("/appSettingsFetched", this);
      paramAnonymouszzmd = zzg.zza(zzg.this);
      if (paramAnonymousMap != null) {}
      try
      {
        if ("true".equalsIgnoreCase((String)paramAnonymousMap.get("isSuccessful")))
        {
          paramAnonymousMap = (String)paramAnonymousMap.get("appSettingsJson");
          zzu.zzgq().zzd(zzg.zzb(zzg.this), paramAnonymousMap);
        }
        return;
      }
      finally {}
    }
  };
  
  private static boolean zza(@Nullable zzkq paramzzkq)
  {
    if (paramzzkq == null) {
      return true;
    }
    long l = paramzzkq.zzum();
    int i;
    if (zzu.zzgs().currentTimeMillis() - l > ((Long)zzdr.zzbjx.get()).longValue())
    {
      i = 1;
      if ((i == 0) && (paramzzkq.zzun())) {
        break label61;
      }
    }
    label61:
    for (boolean bool = true;; bool = false)
    {
      return bool;
      i = 0;
      break;
    }
  }
  
  public void zza(final Context paramContext, final VersionInfoParcel paramVersionInfoParcel, final boolean paramBoolean, @Nullable zzkq paramzzkq, final String paramString1, @Nullable final String paramString2)
  {
    if (!zza(paramzzkq)) {
      return;
    }
    if (paramContext == null)
    {
      zzkx.zzdi("Context not provided to fetch application settings");
      return;
    }
    if ((TextUtils.isEmpty(paramString1)) && (TextUtils.isEmpty(paramString2)))
    {
      zzkx.zzdi("App settings could not be fetched. Required parameters missing");
      return;
    }
    this.mContext = paramContext;
    paramVersionInfoParcel = zzu.zzgm().zzd(paramContext, paramVersionInfoParcel);
    zzlb.zzcvl.post(new Runnable()
    {
      public void run()
      {
        paramVersionInfoParcel.zzny().zza(new zzlw.zzc()new zzlw.zzb
        {
          public void zzb(zzgi paramAnonymous2zzgi)
          {
            paramAnonymous2zzgi.zza("/appSettingsFetched", zzg.this.zzana);
            try
            {
              JSONObject localJSONObject = new JSONObject();
              if (!TextUtils.isEmpty(zzg.2.this.zzand)) {
                localJSONObject.put("app_id", zzg.2.this.zzand);
              }
              for (;;)
              {
                localJSONObject.put("is_init", zzg.2.this.zzanf);
                localJSONObject.put("pn", zzg.2.this.zzang.getPackageName());
                paramAnonymous2zzgi.zza("AFMA_fetchAppSettings", localJSONObject);
                return;
                if (!TextUtils.isEmpty(zzg.2.this.zzane)) {
                  localJSONObject.put("ad_unit_id", zzg.2.this.zzane);
                }
              }
              return;
            }
            catch (Exception localException)
            {
              paramAnonymous2zzgi.zzb("/appSettingsFetched", zzg.this.zzana);
              zzkx.zzb("Error requesting application settings", localException);
            }
          }
        }, new zzlw.zzb());
      }
    });
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/zzg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */