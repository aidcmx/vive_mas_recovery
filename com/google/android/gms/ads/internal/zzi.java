package com.google.android.gms.ads.internal;

import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import com.google.android.gms.ads.internal.client.zzm;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.internal.zzaq;
import com.google.android.gms.internal.zzau;
import com.google.android.gms.internal.zzdn;
import com.google.android.gms.internal.zzdr;
import com.google.android.gms.internal.zzji;
import com.google.android.gms.internal.zzkx;
import com.google.android.gms.internal.zzla;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;

@zzji
class zzi
  implements zzaq, Runnable
{
  private zzv zzaly;
  private final List<Object[]> zzani = new Vector();
  private final AtomicReference<zzaq> zzanj = new AtomicReference();
  CountDownLatch zzank = new CountDownLatch(1);
  
  public zzi(zzv paramzzv)
  {
    this.zzaly = paramzzv;
    if (zzm.zzkr().zzwq())
    {
      zzla.zza(this);
      return;
    }
    run();
  }
  
  private void zzfi()
  {
    if (this.zzani.isEmpty()) {
      return;
    }
    Iterator localIterator = this.zzani.iterator();
    while (localIterator.hasNext())
    {
      Object[] arrayOfObject = (Object[])localIterator.next();
      if (arrayOfObject.length == 1) {
        ((zzaq)this.zzanj.get()).zza((MotionEvent)arrayOfObject[0]);
      } else if (arrayOfObject.length == 3) {
        ((zzaq)this.zzanj.get()).zza(((Integer)arrayOfObject[0]).intValue(), ((Integer)arrayOfObject[1]).intValue(), ((Integer)arrayOfObject[2]).intValue());
      }
    }
    this.zzani.clear();
  }
  
  private Context zzg(Context paramContext)
  {
    if (!((Boolean)zzdr.zzbdj.get()).booleanValue()) {}
    Context localContext;
    do
    {
      return paramContext;
      localContext = paramContext.getApplicationContext();
    } while (localContext == null);
    return localContext;
  }
  
  public void run()
  {
    label94:
    for (;;)
    {
      try
      {
        if (((Boolean)zzdr.zzbef.get()).booleanValue()) {
          if (this.zzaly.zzari.zzcyc)
          {
            break label94;
            zza(zzd(this.zzaly.zzari.zzda, zzg(this.zzaly.zzahs), bool));
          }
          else
          {
            bool = false;
            continue;
          }
        }
        boolean bool = true;
      }
      finally
      {
        this.zzank.countDown();
        this.zzaly = null;
      }
    }
  }
  
  public String zza(Context paramContext, String paramString, View paramView)
  {
    if (zzfh())
    {
      zzaq localzzaq = (zzaq)this.zzanj.get();
      if (localzzaq != null)
      {
        zzfi();
        return localzzaq.zza(zzg(paramContext), paramString, paramView);
      }
    }
    return "";
  }
  
  public String zza(Context paramContext, byte[] paramArrayOfByte)
  {
    if (zzfh())
    {
      paramArrayOfByte = (zzaq)this.zzanj.get();
      if (paramArrayOfByte != null)
      {
        zzfi();
        return paramArrayOfByte.zzb(zzg(paramContext));
      }
    }
    return "";
  }
  
  public void zza(int paramInt1, int paramInt2, int paramInt3)
  {
    zzaq localzzaq = (zzaq)this.zzanj.get();
    if (localzzaq != null)
    {
      zzfi();
      localzzaq.zza(paramInt1, paramInt2, paramInt3);
      return;
    }
    this.zzani.add(new Object[] { Integer.valueOf(paramInt1), Integer.valueOf(paramInt2), Integer.valueOf(paramInt3) });
  }
  
  public void zza(MotionEvent paramMotionEvent)
  {
    zzaq localzzaq = (zzaq)this.zzanj.get();
    if (localzzaq != null)
    {
      zzfi();
      localzzaq.zza(paramMotionEvent);
      return;
    }
    this.zzani.add(new Object[] { paramMotionEvent });
  }
  
  protected void zza(zzaq paramzzaq)
  {
    this.zzanj.set(paramzzaq);
  }
  
  public String zzb(Context paramContext)
  {
    return zza(paramContext, null);
  }
  
  protected zzaq zzd(String paramString, Context paramContext, boolean paramBoolean)
  {
    return zzau.zza(paramString, paramContext, paramBoolean);
  }
  
  protected boolean zzfh()
  {
    try
    {
      this.zzank.await();
      return true;
    }
    catch (InterruptedException localInterruptedException)
    {
      zzkx.zzc("Interrupted during GADSignals creation.", localInterruptedException);
    }
    return false;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/zzi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */