package com.google.android.gms.ads.internal;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.util.SimpleArrayMap;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.client.zzr.zza;
import com.google.android.gms.ads.internal.client.zzy;
import com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.internal.zzeq;
import com.google.android.gms.internal.zzer;
import com.google.android.gms.internal.zzes;
import com.google.android.gms.internal.zzet;
import com.google.android.gms.internal.zzgz;
import com.google.android.gms.internal.zzji;
import com.google.android.gms.internal.zzlb;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

@zzji
public class zzj
  extends zzr.zza
{
  private final Context mContext;
  private final Object zzako = new Object();
  private final zzd zzamb;
  private final zzgz zzamf;
  private final com.google.android.gms.ads.internal.client.zzq zzanl;
  @Nullable
  private final zzeq zzanm;
  @Nullable
  private final zzer zzann;
  private final SimpleArrayMap<String, zzet> zzano;
  private final SimpleArrayMap<String, zzes> zzanp;
  private final NativeAdOptionsParcel zzanq;
  private final List<String> zzanr;
  private final zzy zzans;
  private final String zzant;
  private final VersionInfoParcel zzanu;
  @Nullable
  private WeakReference<zzq> zzanv;
  
  zzj(Context paramContext, String paramString, zzgz paramzzgz, VersionInfoParcel paramVersionInfoParcel, com.google.android.gms.ads.internal.client.zzq paramzzq, zzeq paramzzeq, zzer paramzzer, SimpleArrayMap<String, zzet> paramSimpleArrayMap, SimpleArrayMap<String, zzes> paramSimpleArrayMap1, NativeAdOptionsParcel paramNativeAdOptionsParcel, zzy paramzzy, zzd paramzzd)
  {
    this.mContext = paramContext;
    this.zzant = paramString;
    this.zzamf = paramzzgz;
    this.zzanu = paramVersionInfoParcel;
    this.zzanl = paramzzq;
    this.zzann = paramzzer;
    this.zzanm = paramzzeq;
    this.zzano = paramSimpleArrayMap;
    this.zzanp = paramSimpleArrayMap1;
    this.zzanq = paramNativeAdOptionsParcel;
    this.zzanr = zzfj();
    this.zzans = paramzzy;
    this.zzamb = paramzzd;
  }
  
  private List<String> zzfj()
  {
    ArrayList localArrayList = new ArrayList();
    if (this.zzann != null) {
      localArrayList.add("1");
    }
    if (this.zzanm != null) {
      localArrayList.add("2");
    }
    if (this.zzano.size() > 0) {
      localArrayList.add("3");
    }
    return localArrayList;
  }
  
  @Nullable
  public String getMediationAdapterClassName()
  {
    for (;;)
    {
      synchronized (this.zzako)
      {
        if (this.zzanv != null)
        {
          Object localObject1 = (zzq)this.zzanv.get();
          if (localObject1 != null)
          {
            localObject1 = ((zzq)localObject1).getMediationAdapterClassName();
            return (String)localObject1;
          }
        }
        else
        {
          return null;
        }
      }
      Object localObject3 = null;
    }
  }
  
  public boolean isLoading()
  {
    for (;;)
    {
      synchronized (this.zzako)
      {
        if (this.zzanv != null)
        {
          zzq localzzq = (zzq)this.zzanv.get();
          if (localzzq != null)
          {
            bool = localzzq.isLoading();
            return bool;
          }
        }
        else
        {
          return false;
        }
      }
      boolean bool = false;
    }
  }
  
  protected void runOnUiThread(Runnable paramRunnable)
  {
    zzlb.zzcvl.post(paramRunnable);
  }
  
  public void zzf(final AdRequestParcel paramAdRequestParcel)
  {
    runOnUiThread(new Runnable()
    {
      public void run()
      {
        synchronized (zzj.zza(zzj.this))
        {
          zzq localzzq = zzj.this.zzfk();
          zzj.zza(zzj.this, new WeakReference(localzzq));
          localzzq.zzb(zzj.zzb(zzj.this));
          localzzq.zzb(zzj.zzc(zzj.this));
          localzzq.zza(zzj.zzd(zzj.this));
          localzzq.zza(zzj.zze(zzj.this));
          localzzq.zzb(zzj.zzf(zzj.this));
          localzzq.zzb(zzj.zzg(zzj.this));
          localzzq.zzb(zzj.zzh(zzj.this));
          localzzq.zza(zzj.zzi(zzj.this));
          localzzq.zzb(paramAdRequestParcel);
          return;
        }
      }
    });
  }
  
  protected zzq zzfk()
  {
    return new zzq(this.mContext, this.zzamb, AdSizeParcel.zzj(this.mContext), this.zzant, this.zzamf, this.zzanu);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/zzj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */