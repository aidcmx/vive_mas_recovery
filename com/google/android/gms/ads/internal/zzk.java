package com.google.android.gms.ads.internal;

import android.content.Context;
import android.support.v4.util.SimpleArrayMap;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.client.zzq;
import com.google.android.gms.ads.internal.client.zzr;
import com.google.android.gms.ads.internal.client.zzs.zza;
import com.google.android.gms.ads.internal.client.zzy;
import com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.internal.zzeq;
import com.google.android.gms.internal.zzer;
import com.google.android.gms.internal.zzes;
import com.google.android.gms.internal.zzet;
import com.google.android.gms.internal.zzgz;
import com.google.android.gms.internal.zzji;

@zzji
public class zzk
  extends zzs.zza
{
  private final Context mContext;
  private final zzd zzamb;
  private final zzgz zzamf;
  private zzq zzanl;
  private NativeAdOptionsParcel zzanq;
  private zzy zzans;
  private final String zzant;
  private final VersionInfoParcel zzanu;
  private zzeq zzany;
  private zzer zzanz;
  private SimpleArrayMap<String, zzes> zzaoa;
  private SimpleArrayMap<String, zzet> zzaob;
  
  public zzk(Context paramContext, String paramString, zzgz paramzzgz, VersionInfoParcel paramVersionInfoParcel, zzd paramzzd)
  {
    this.mContext = paramContext;
    this.zzant = paramString;
    this.zzamf = paramzzgz;
    this.zzanu = paramVersionInfoParcel;
    this.zzaob = new SimpleArrayMap();
    this.zzaoa = new SimpleArrayMap();
    this.zzamb = paramzzd;
  }
  
  public void zza(NativeAdOptionsParcel paramNativeAdOptionsParcel)
  {
    this.zzanq = paramNativeAdOptionsParcel;
  }
  
  public void zza(zzeq paramzzeq)
  {
    this.zzany = paramzzeq;
  }
  
  public void zza(zzer paramzzer)
  {
    this.zzanz = paramzzer;
  }
  
  public void zza(String paramString, zzet paramzzet, zzes paramzzes)
  {
    if (TextUtils.isEmpty(paramString)) {
      throw new IllegalArgumentException("Custom template ID for native custom template ad is empty. Please provide a valid template id.");
    }
    this.zzaob.put(paramString, paramzzet);
    this.zzaoa.put(paramString, paramzzes);
  }
  
  public void zzb(zzq paramzzq)
  {
    this.zzanl = paramzzq;
  }
  
  public void zzb(zzy paramzzy)
  {
    this.zzans = paramzzy;
  }
  
  public zzr zzfl()
  {
    return new zzj(this.mContext, this.zzant, this.zzamf, this.zzanu, this.zzanl, this.zzany, this.zzanz, this.zzaob, this.zzaoa, this.zzanq, this.zzans, this.zzamb);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/zzk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */