package com.google.android.gms.ads.internal;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.Window;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import com.google.android.gms.ads.internal.reward.mediation.client.RewardItemParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.util.zzs;
import com.google.android.gms.internal.zzco;
import com.google.android.gms.internal.zzcu;
import com.google.android.gms.internal.zzdn;
import com.google.android.gms.internal.zzdr;
import com.google.android.gms.internal.zzdz;
import com.google.android.gms.internal.zzfi;
import com.google.android.gms.internal.zzfn;
import com.google.android.gms.internal.zzfn.zza;
import com.google.android.gms.internal.zzgp;
import com.google.android.gms.internal.zzgq;
import com.google.android.gms.internal.zzgz;
import com.google.android.gms.internal.zzha;
import com.google.android.gms.internal.zzji;
import com.google.android.gms.internal.zzjm;
import com.google.android.gms.internal.zzko;
import com.google.android.gms.internal.zzko.zza;
import com.google.android.gms.internal.zzkw;
import com.google.android.gms.internal.zzkx;
import com.google.android.gms.internal.zzlb;
import com.google.android.gms.internal.zzlc;
import com.google.android.gms.internal.zzlk;
import com.google.android.gms.internal.zzmd;
import com.google.android.gms.internal.zzme;
import com.google.android.gms.internal.zzme.zzc;
import com.google.android.gms.internal.zzmf;
import java.util.Collections;
import java.util.concurrent.Future;
import org.json.JSONException;
import org.json.JSONObject;

@zzji
public class zzl
  extends zzc
  implements zzfi, zzfn.zza
{
  protected transient boolean zzaoc = false;
  private int zzaod = -1;
  private boolean zzaoe;
  private float zzaof;
  
  public zzl(Context paramContext, AdSizeParcel paramAdSizeParcel, String paramString, zzgz paramzzgz, VersionInfoParcel paramVersionInfoParcel, zzd paramzzd)
  {
    super(paramContext, paramAdSizeParcel, paramString, paramzzgz, paramVersionInfoParcel, paramzzd);
  }
  
  private void zzb(Bundle paramBundle)
  {
    zzu.zzgm().zzb(this.zzaly.zzahs, this.zzaly.zzari.zzda, "gmob-apps", paramBundle, false);
  }
  
  static zzko.zza zzc(zzko.zza paramzza)
  {
    try
    {
      Object localObject1 = zzjm.zzc(paramzza.zzcsu).toString();
      Object localObject2 = new JSONObject();
      ((JSONObject)localObject2).put("pubid", paramzza.zzcmx.zzarg);
      localObject2 = ((JSONObject)localObject2).toString();
      localObject2 = new zzgp((String)localObject1, null, Collections.singletonList("com.google.ads.mediation.admob.AdMobAdapter"), null, null, Collections.emptyList(), Collections.emptyList(), (String)localObject2, null, Collections.emptyList(), Collections.emptyList(), null, null, null, null, null, Collections.emptyList());
      localObject1 = paramzza.zzcsu;
      localObject2 = new zzgq(Collections.singletonList(localObject2), -1L, Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), ((AdResponseParcel)localObject1).zzbvn, ((AdResponseParcel)localObject1).zzbvo, "", -1L, 0, 1, null, 0, -1, -1L, false);
      localObject1 = new AdResponseParcel(paramzza.zzcmx, ((AdResponseParcel)localObject1).zzcbo, ((AdResponseParcel)localObject1).body, Collections.emptyList(), Collections.emptyList(), ((AdResponseParcel)localObject1).zzcla, true, ((AdResponseParcel)localObject1).zzclc, Collections.emptyList(), ((AdResponseParcel)localObject1).zzbvq, ((AdResponseParcel)localObject1).orientation, ((AdResponseParcel)localObject1).zzcle, ((AdResponseParcel)localObject1).zzclf, ((AdResponseParcel)localObject1).zzclg, ((AdResponseParcel)localObject1).zzclh, ((AdResponseParcel)localObject1).zzcli, null, ((AdResponseParcel)localObject1).zzclk, ((AdResponseParcel)localObject1).zzazt, ((AdResponseParcel)localObject1).zzckc, ((AdResponseParcel)localObject1).zzcll, ((AdResponseParcel)localObject1).zzclm, ((AdResponseParcel)localObject1).zzclp, ((AdResponseParcel)localObject1).zzazu, ((AdResponseParcel)localObject1).zzazv, null, Collections.emptyList(), Collections.emptyList(), ((AdResponseParcel)localObject1).zzclt, ((AdResponseParcel)localObject1).zzclu, ((AdResponseParcel)localObject1).zzcks, ((AdResponseParcel)localObject1).zzckt, ((AdResponseParcel)localObject1).zzbvn, ((AdResponseParcel)localObject1).zzbvo, ((AdResponseParcel)localObject1).zzclv, null, ((AdResponseParcel)localObject1).zzclx, ((AdResponseParcel)localObject1).zzcly);
      return new zzko.zza(paramzza.zzcmx, (AdResponseParcel)localObject1, (zzgq)localObject2, paramzza.zzarm, paramzza.errorCode, paramzza.zzcso, paramzza.zzcsp, null);
    }
    catch (JSONException localJSONException)
    {
      zzkx.zzb("Unable to generate ad state for an interstitial ad with pooling.", localJSONException);
    }
    return paramzza;
  }
  
  public void showInterstitial()
  {
    zzaa.zzhs("showInterstitial must be called on the main UI thread.");
    if (this.zzaly.zzarn == null)
    {
      zzkx.zzdi("The interstitial has not loaded.");
      return;
    }
    if (((Boolean)zzdr.zzbgx.get()).booleanValue()) {
      if (this.zzaly.zzahs.getApplicationContext() == null) {
        break label235;
      }
    }
    label235:
    for (String str = this.zzaly.zzahs.getApplicationContext().getPackageName();; localObject = this.zzaly.zzahs.getPackageName())
    {
      Bundle localBundle;
      if (!this.zzaoc)
      {
        zzkx.zzdi("It is not recommended to show an interstitial before onAdLoaded completes.");
        localBundle = new Bundle();
        localBundle.putString("appid", str);
        localBundle.putString("action", "show_interstitial_before_load_finish");
        zzb(localBundle);
      }
      if (!zzu.zzgm().zzae(this.zzaly.zzahs))
      {
        zzkx.zzdi("It is not recommended to show an interstitial when app is not in foreground.");
        localBundle = new Bundle();
        localBundle.putString("appid", str);
        localBundle.putString("action", "show_interstitial_app_not_in_foreground");
        zzb(localBundle);
      }
      if (this.zzaly.zzhq()) {
        break;
      }
      if ((!this.zzaly.zzarn.zzclb) || (this.zzaly.zzarn.zzbwn == null)) {
        break label249;
      }
      try
      {
        this.zzaly.zzarn.zzbwn.showInterstitial();
        return;
      }
      catch (RemoteException localRemoteException)
      {
        zzkx.zzc("Could not show interstitial.", localRemoteException);
        zzfn();
        return;
      }
    }
    label249:
    if (this.zzaly.zzarn.zzcbm == null)
    {
      zzkx.zzdi("The interstitial failed to load.");
      return;
    }
    if (this.zzaly.zzarn.zzcbm.zzxg())
    {
      zzkx.zzdi("The interstitial is already showing.");
      return;
    }
    this.zzaly.zzarn.zzcbm.zzak(true);
    if (this.zzaly.zzarn.zzcsi != null) {
      this.zzama.zza(this.zzaly.zzarm, this.zzaly.zzarn);
    }
    if (zzs.zzayq())
    {
      localObject = this.zzaly.zzarn;
      if (((zzko)localObject).zzic()) {
        new zzcu(this.zzaly.zzahs, ((zzko)localObject).zzcbm.getView()).zza(((zzko)localObject).zzcbm);
      }
    }
    else
    {
      if (!this.zzaly.zzaok) {
        break label493;
      }
    }
    label493:
    for (final Object localObject = zzu.zzgm().zzaf(this.zzaly.zzahs);; localObject = null)
    {
      this.zzaod = zzu.zzhh().zzb((Bitmap)localObject);
      if ((!((Boolean)zzdr.zzbip.get()).booleanValue()) || (localObject == null)) {
        break label498;
      }
      localObject = (Future)new zza(this.zzaod).zzrz();
      return;
      ((zzko)localObject).zzcbm.zzxc().zza(new zzme.zzc()
      {
        public void zzfg()
        {
          new zzcu(zzl.this.zzaly.zzahs, localObject.zzcbm.getView()).zza(localObject.zzcbm);
        }
      });
      break;
    }
    label498:
    localObject = new InterstitialAdParameterParcel(this.zzaly.zzaok, zzfm(), false, 0.0F, -1);
    int j = this.zzaly.zzarn.zzcbm.getRequestedOrientation();
    int i = j;
    if (j == -1) {
      i = this.zzaly.zzarn.orientation;
    }
    localObject = new AdOverlayInfoParcel(this, this, this, this.zzaly.zzarn.zzcbm, i, this.zzaly.zzari, this.zzaly.zzarn.zzclg, (InterstitialAdParameterParcel)localObject);
    zzu.zzgk().zza(this.zzaly.zzahs, (AdOverlayInfoParcel)localObject);
  }
  
  protected zzmd zza(zzko.zza paramzza, @Nullable zze paramzze, @Nullable com.google.android.gms.ads.internal.safebrowsing.zzc paramzzc)
  {
    zzmd localzzmd = zzu.zzgn().zza(this.zzaly.zzahs, this.zzaly.zzarm, false, false, this.zzaly.zzarh, this.zzaly.zzari, this.zzalt, this, this.zzamb);
    localzzmd.zzxc().zza(this, null, this, this, ((Boolean)zzdr.zzbfn.get()).booleanValue(), this, this, paramzze, null, paramzzc);
    zza(localzzmd);
    localzzmd.zzdk(paramzza.zzcmx.zzcki);
    zzfn.zza(localzzmd, this);
    return localzzmd;
  }
  
  public void zza(zzko.zza paramzza, zzdz paramzzdz)
  {
    int j = 1;
    if (!((Boolean)zzdr.zzbgg.get()).booleanValue())
    {
      super.zza(paramzza, paramzzdz);
      return;
    }
    if (paramzza.errorCode != -2)
    {
      super.zza(paramzza, paramzzdz);
      return;
    }
    Bundle localBundle = paramzza.zzcmx.zzcju.zzayv.getBundle("com.google.ads.mediation.admob.AdMobAdapter");
    int i;
    if ((localBundle == null) || (!localBundle.containsKey("gw")))
    {
      i = 1;
      if (paramzza.zzcsu.zzclb) {
        break label124;
      }
    }
    for (;;)
    {
      if ((i != 0) && (j != 0)) {
        this.zzaly.zzaro = zzc(paramzza);
      }
      super.zza(this.zzaly.zzaro, paramzzdz);
      return;
      i = 0;
      break;
      label124:
      j = 0;
    }
  }
  
  public void zza(boolean paramBoolean, float paramFloat)
  {
    this.zzaoe = paramBoolean;
    this.zzaof = paramFloat;
  }
  
  public boolean zza(AdRequestParcel paramAdRequestParcel, zzdz paramzzdz)
  {
    if (this.zzaly.zzarn != null)
    {
      zzkx.zzdi("An interstitial is already loading. Aborting.");
      return false;
    }
    return super.zza(paramAdRequestParcel, paramzzdz);
  }
  
  protected boolean zza(AdRequestParcel paramAdRequestParcel, zzko paramzzko, boolean paramBoolean)
  {
    if ((this.zzaly.zzhp()) && (paramzzko.zzcbm != null)) {
      zzu.zzgo().zzl(paramzzko.zzcbm);
    }
    return this.zzalx.zzfy();
  }
  
  public boolean zza(@Nullable zzko paramzzko1, zzko paramzzko2)
  {
    if (!super.zza(paramzzko1, paramzzko2)) {
      return false;
    }
    if ((!this.zzaly.zzhp()) && (this.zzaly.zzash != null) && (paramzzko2.zzcsi != null)) {
      this.zzama.zza(this.zzaly.zzarm, paramzzko2, this.zzaly.zzash);
    }
    return true;
  }
  
  public void zzb(RewardItemParcel paramRewardItemParcel)
  {
    RewardItemParcel localRewardItemParcel = paramRewardItemParcel;
    if (this.zzaly.zzarn != null)
    {
      if (this.zzaly.zzarn.zzcls != null) {
        zzu.zzgm().zza(this.zzaly.zzahs, this.zzaly.zzari.zzda, this.zzaly.zzarn.zzcls);
      }
      localRewardItemParcel = paramRewardItemParcel;
      if (this.zzaly.zzarn.zzclq != null) {
        localRewardItemParcel = this.zzaly.zzarn.zzclq;
      }
    }
    zza(localRewardItemParcel);
  }
  
  protected void zzek()
  {
    zzfn();
    super.zzek();
  }
  
  protected void zzen()
  {
    super.zzen();
    this.zzaoc = true;
  }
  
  public void zzer()
  {
    recordImpression();
    super.zzer();
    if ((this.zzaly.zzarn != null) && (this.zzaly.zzarn.zzcbm != null))
    {
      zzme localzzme = this.zzaly.zzarn.zzcbm.zzxc();
      if (localzzme != null) {
        localzzme.zzya();
      }
    }
  }
  
  protected boolean zzfm()
  {
    if (!(this.zzaly.zzahs instanceof Activity)) {}
    Window localWindow;
    do
    {
      return false;
      localWindow = ((Activity)this.zzaly.zzahs).getWindow();
    } while ((localWindow == null) || (localWindow.getDecorView() == null));
    Rect localRect1 = new Rect();
    Rect localRect2 = new Rect();
    localWindow.getDecorView().getGlobalVisibleRect(localRect1, null);
    localWindow.getDecorView().getWindowVisibleDisplayFrame(localRect2);
    if ((localRect1.bottom != 0) && (localRect2.bottom != 0) && (localRect1.top == localRect2.top)) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public void zzfn()
  {
    zzu.zzhh().zzb(Integer.valueOf(this.zzaod));
    if (this.zzaly.zzhp())
    {
      this.zzaly.zzhm();
      this.zzaly.zzarn = null;
      this.zzaly.zzaok = false;
      this.zzaoc = false;
    }
  }
  
  public void zzfo()
  {
    if ((this.zzaly.zzarn != null) && (this.zzaly.zzarn.zzcsn != null)) {
      zzu.zzgm().zza(this.zzaly.zzahs, this.zzaly.zzari.zzda, this.zzaly.zzarn.zzcsn);
    }
    zzeo();
  }
  
  public void zzg(boolean paramBoolean)
  {
    this.zzaly.zzaok = paramBoolean;
  }
  
  @zzji
  private class zza
    extends zzkw
  {
    private final int zzaoh;
    
    public zza(int paramInt)
    {
      this.zzaoh = paramInt;
    }
    
    public void onStop() {}
    
    public void zzfp()
    {
      boolean bool1 = zzl.this.zzaly.zzaok;
      boolean bool2 = zzl.this.zzfm();
      boolean bool3 = zzl.zza(zzl.this);
      float f = zzl.zzb(zzl.this);
      int i;
      final Object localObject;
      if (zzl.this.zzaly.zzaok)
      {
        i = this.zzaoh;
        localObject = new InterstitialAdParameterParcel(bool1, bool2, bool3, f, i);
        i = zzl.this.zzaly.zzarn.zzcbm.getRequestedOrientation();
        if (i != -1) {
          break label192;
        }
        i = zzl.this.zzaly.zzarn.orientation;
      }
      label192:
      for (;;)
      {
        localObject = new AdOverlayInfoParcel(zzl.this, zzl.this, zzl.this, zzl.this.zzaly.zzarn.zzcbm, i, zzl.this.zzaly.zzari, zzl.this.zzaly.zzarn.zzclg, (InterstitialAdParameterParcel)localObject);
        zzlb.zzcvl.post(new Runnable()
        {
          public void run()
          {
            zzu.zzgk().zza(zzl.this.zzaly.zzahs, localObject);
          }
        });
        return;
        i = -1;
        break;
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/zzl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */