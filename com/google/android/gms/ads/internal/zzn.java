package com.google.android.gms.ads.internal;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.view.View.OnClickListener;
import com.google.android.gms.ads.internal.formats.zzd;
import com.google.android.gms.internal.zzeg;
import com.google.android.gms.internal.zzeg.zza;
import com.google.android.gms.internal.zzfe;
import com.google.android.gms.internal.zzgp;
import com.google.android.gms.internal.zzgu;
import com.google.android.gms.internal.zzha;
import com.google.android.gms.internal.zzhd;
import com.google.android.gms.internal.zzhe;
import com.google.android.gms.internal.zzji;
import com.google.android.gms.internal.zzko;
import com.google.android.gms.internal.zzkx;
import com.google.android.gms.internal.zzmd;
import com.google.android.gms.internal.zzme;
import com.google.android.gms.internal.zzme.zza;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@zzji
public class zzn
{
  private static zzd zza(zzhd paramzzhd)
    throws RemoteException
  {
    return new zzd(paramzzhd.getHeadline(), paramzzhd.getImages(), paramzzhd.getBody(), paramzzhd.zzmo(), paramzzhd.getCallToAction(), paramzzhd.getStarRating(), paramzzhd.getStore(), paramzzhd.getPrice(), null, paramzzhd.getExtras(), null, null);
  }
  
  private static com.google.android.gms.ads.internal.formats.zze zza(zzhe paramzzhe)
    throws RemoteException
  {
    return new com.google.android.gms.ads.internal.formats.zze(paramzzhe.getHeadline(), paramzzhe.getImages(), paramzzhe.getBody(), paramzzhe.zzmt(), paramzzhe.getCallToAction(), paramzzhe.getAdvertiser(), null, paramzzhe.getExtras());
  }
  
  static zzfe zza(@Nullable zzhd paramzzhd, @Nullable final zzhe paramzzhe, final zzf.zza paramzza)
  {
    new zzfe()
    {
      public void zza(zzmd paramAnonymouszzmd, Map<String, String> paramAnonymousMap)
      {
        paramAnonymousMap = paramAnonymouszzmd.getView();
        if (paramAnonymousMap == null) {}
        do
        {
          return;
          try
          {
            if (zzn.this == null) {
              continue;
            }
            if (!zzn.this.getOverrideClickHandling())
            {
              zzn.this.zzk(com.google.android.gms.dynamic.zze.zzac(paramAnonymousMap));
              paramzza.onClick();
              return;
            }
          }
          catch (RemoteException paramAnonymouszzmd)
          {
            zzkx.zzc("Unable to call handleClick on mapper", paramAnonymouszzmd);
            return;
          }
          zzn.zzb(paramAnonymouszzmd);
          return;
        } while (paramzzhe == null);
        if (!paramzzhe.getOverrideClickHandling())
        {
          paramzzhe.zzk(com.google.android.gms.dynamic.zze.zzac(paramAnonymousMap));
          paramzza.onClick();
          return;
        }
        zzn.zzb(paramAnonymouszzmd);
      }
    };
  }
  
  static zzfe zza(CountDownLatch paramCountDownLatch)
  {
    new zzfe()
    {
      public void zza(zzmd paramAnonymouszzmd, Map<String, String> paramAnonymousMap)
      {
        zzn.this.countDown();
        paramAnonymouszzmd.getView().setVisibility(0);
      }
    };
  }
  
  private static String zza(@Nullable Bitmap paramBitmap)
  {
    Object localObject = new ByteArrayOutputStream();
    if (paramBitmap == null)
    {
      zzkx.zzdi("Bitmap is null. Returning empty string");
      return "";
    }
    paramBitmap.compress(Bitmap.CompressFormat.PNG, 100, (OutputStream)localObject);
    localObject = Base64.encodeToString(((ByteArrayOutputStream)localObject).toByteArray(), 0);
    paramBitmap = String.valueOf("data:image/png;base64,");
    localObject = String.valueOf(localObject);
    if (((String)localObject).length() != 0) {
      return paramBitmap.concat((String)localObject);
    }
    return new String(paramBitmap);
  }
  
  static String zza(@Nullable zzeg paramzzeg)
  {
    if (paramzzeg == null)
    {
      zzkx.zzdi("Image is null. Returning empty string");
      return "";
    }
    try
    {
      Object localObject = paramzzeg.getUri();
      if (localObject != null)
      {
        localObject = ((Uri)localObject).toString();
        return (String)localObject;
      }
    }
    catch (RemoteException localRemoteException)
    {
      zzkx.zzdi("Unable to get image uri. Trying data uri next");
    }
    return zzb(paramzzeg);
  }
  
  private static JSONObject zza(@Nullable Bundle paramBundle, String paramString)
    throws JSONException
  {
    JSONObject localJSONObject = new JSONObject();
    if ((paramBundle == null) || (TextUtils.isEmpty(paramString))) {
      return localJSONObject;
    }
    paramString = new JSONObject(paramString);
    Iterator localIterator = paramString.keys();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      if (paramBundle.containsKey(str)) {
        if ("image".equals(paramString.getString(str)))
        {
          Object localObject = paramBundle.get(str);
          if ((localObject instanceof Bitmap)) {
            localJSONObject.put(str, zza((Bitmap)localObject));
          } else {
            zzkx.zzdi("Invalid type. An image type extra should return a bitmap");
          }
        }
        else if ((paramBundle.get(str) instanceof Bitmap))
        {
          zzkx.zzdi("Invalid asset type. Bitmap should be returned only for image type");
        }
        else
        {
          localJSONObject.put(str, String.valueOf(paramBundle.get(str)));
        }
      }
    }
    return localJSONObject;
  }
  
  public static void zza(@Nullable zzko paramzzko, zzf.zza paramzza)
  {
    zzhe localzzhe = null;
    zzmd localzzmd;
    if ((paramzzko != null) && (zzh(paramzzko)))
    {
      localzzmd = paramzzko.zzcbm;
      if (localzzmd == null) {
        break label43;
      }
    }
    label43:
    for (View localView = localzzmd.getView(); localView == null; localView = null)
    {
      zzkx.zzdi("AdWebView is null");
      return;
    }
    List localList;
    for (;;)
    {
      try
      {
        if (paramzzko.zzbwm != null)
        {
          localList = paramzzko.zzbwm.zzbvg;
          if ((localList != null) && (!localList.isEmpty())) {
            break;
          }
          zzkx.zzdi("No template ids present in mediation response");
          return;
        }
      }
      catch (RemoteException paramzzko)
      {
        zzkx.zzc("Error occurred while recording impression and registering for clicks", paramzzko);
        return;
      }
      localList = null;
    }
    if (paramzzko.zzbwn != null) {}
    for (zzhd localzzhd = paramzzko.zzbwn.zzom();; localzzhd = null)
    {
      if (paramzzko.zzbwn != null) {
        localzzhe = paramzzko.zzbwn.zzon();
      }
      if ((localList.contains("2")) && (localzzhd != null))
      {
        localzzhd.zzl(com.google.android.gms.dynamic.zze.zzac(localView));
        if (!localzzhd.getOverrideImpressionRecording()) {
          localzzhd.recordImpression();
        }
        localzzmd.zzxc().zza("/nativeExpressViewClicked", zza(localzzhd, null, paramzza));
        return;
      }
      if ((localList.contains("1")) && (localzzhe != null))
      {
        localzzhe.zzl(com.google.android.gms.dynamic.zze.zzac(localView));
        if (!localzzhe.getOverrideImpressionRecording()) {
          localzzhe.recordImpression();
        }
        localzzmd.zzxc().zza("/nativeExpressViewClicked", zza(null, localzzhe, paramzza));
        return;
      }
      zzkx.zzdi("No matching template id and mapper");
      return;
    }
  }
  
  private static void zza(zzmd paramzzmd)
  {
    View.OnClickListener localOnClickListener = paramzzmd.zzxr();
    if (localOnClickListener != null) {
      localOnClickListener.onClick(paramzzmd.getView());
    }
  }
  
  private static void zza(final zzmd paramzzmd, zzd paramzzd, final String paramString)
  {
    paramzzmd.zzxc().zza(new zzme.zza()
    {
      public void zza(zzmd paramAnonymouszzmd, boolean paramAnonymousBoolean)
      {
        try
        {
          paramAnonymouszzmd = new JSONObject();
          paramAnonymouszzmd.put("headline", zzn.this.getHeadline());
          paramAnonymouszzmd.put("body", zzn.this.getBody());
          paramAnonymouszzmd.put("call_to_action", zzn.this.getCallToAction());
          paramAnonymouszzmd.put("price", zzn.this.getPrice());
          paramAnonymouszzmd.put("star_rating", String.valueOf(zzn.this.getStarRating()));
          paramAnonymouszzmd.put("store", zzn.this.getStore());
          paramAnonymouszzmd.put("icon", zzn.zza(zzn.this.zzmo()));
          localObject1 = new JSONArray();
          Object localObject2 = zzn.this.getImages();
          if (localObject2 != null)
          {
            localObject2 = ((List)localObject2).iterator();
            while (((Iterator)localObject2).hasNext()) {
              ((JSONArray)localObject1).put(zzn.zza(zzn.zzf(((Iterator)localObject2).next())));
            }
          }
          paramAnonymouszzmd.put("images", localObject1);
        }
        catch (JSONException paramAnonymouszzmd)
        {
          zzkx.zzc("Exception occurred when loading assets", paramAnonymouszzmd);
          return;
        }
        paramAnonymouszzmd.put("extras", zzn.zzb(zzn.this.getExtras(), paramString));
        Object localObject1 = new JSONObject();
        ((JSONObject)localObject1).put("assets", paramAnonymouszzmd);
        ((JSONObject)localObject1).put("template_id", "2");
        paramzzmd.zza("google.afma.nativeExpressAds.loadAssets", (JSONObject)localObject1);
      }
    });
  }
  
  private static void zza(final zzmd paramzzmd, com.google.android.gms.ads.internal.formats.zze paramzze, final String paramString)
  {
    paramzzmd.zzxc().zza(new zzme.zza()
    {
      public void zza(zzmd paramAnonymouszzmd, boolean paramAnonymousBoolean)
      {
        try
        {
          paramAnonymouszzmd = new JSONObject();
          paramAnonymouszzmd.put("headline", zzn.this.getHeadline());
          paramAnonymouszzmd.put("body", zzn.this.getBody());
          paramAnonymouszzmd.put("call_to_action", zzn.this.getCallToAction());
          paramAnonymouszzmd.put("advertiser", zzn.this.getAdvertiser());
          paramAnonymouszzmd.put("logo", zzn.zza(zzn.this.zzmt()));
          localObject1 = new JSONArray();
          Object localObject2 = zzn.this.getImages();
          if (localObject2 != null)
          {
            localObject2 = ((List)localObject2).iterator();
            while (((Iterator)localObject2).hasNext()) {
              ((JSONArray)localObject1).put(zzn.zza(zzn.zzf(((Iterator)localObject2).next())));
            }
          }
          paramAnonymouszzmd.put("images", localObject1);
        }
        catch (JSONException paramAnonymouszzmd)
        {
          zzkx.zzc("Exception occurred when loading assets", paramAnonymouszzmd);
          return;
        }
        paramAnonymouszzmd.put("extras", zzn.zzb(zzn.this.getExtras(), paramString));
        Object localObject1 = new JSONObject();
        ((JSONObject)localObject1).put("assets", paramAnonymouszzmd);
        ((JSONObject)localObject1).put("template_id", "1");
        paramzzmd.zza("google.afma.nativeExpressAds.loadAssets", (JSONObject)localObject1);
      }
    });
  }
  
  private static void zza(zzmd paramzzmd, CountDownLatch paramCountDownLatch)
  {
    paramzzmd.zzxc().zza("/nativeExpressAssetsLoaded", zza(paramCountDownLatch));
    paramzzmd.zzxc().zza("/nativeExpressAssetsLoadingFailed", zzb(paramCountDownLatch));
  }
  
  public static boolean zza(zzmd paramzzmd, zzgu paramzzgu, CountDownLatch paramCountDownLatch)
  {
    boolean bool1 = false;
    try
    {
      boolean bool2 = zzb(paramzzmd, paramzzgu, paramCountDownLatch);
      bool1 = bool2;
    }
    catch (RemoteException paramzzmd)
    {
      for (;;)
      {
        zzkx.zzc("Unable to invoke load assets", paramzzmd);
      }
    }
    catch (RuntimeException paramzzmd)
    {
      paramCountDownLatch.countDown();
      throw paramzzmd;
    }
    if (!bool1) {
      paramCountDownLatch.countDown();
    }
    return bool1;
  }
  
  static zzfe zzb(CountDownLatch paramCountDownLatch)
  {
    new zzfe()
    {
      public void zza(zzmd paramAnonymouszzmd, Map<String, String> paramAnonymousMap)
      {
        zzkx.zzdi("Adapter returned an ad, but assets substitution failed");
        zzn.this.countDown();
        paramAnonymouszzmd.destroy();
      }
    };
  }
  
  private static String zzb(zzeg paramzzeg)
  {
    try
    {
      paramzzeg = paramzzeg.zzmn();
      if (paramzzeg == null)
      {
        zzkx.zzdi("Drawable is null. Returning empty string");
        return "";
      }
      paramzzeg = (Drawable)com.google.android.gms.dynamic.zze.zzae(paramzzeg);
      if (!(paramzzeg instanceof BitmapDrawable))
      {
        zzkx.zzdi("Drawable is not an instance of BitmapDrawable. Returning empty string");
        return "";
      }
    }
    catch (RemoteException paramzzeg)
    {
      zzkx.zzdi("Unable to get drawable. Returning empty string");
      return "";
    }
    return zza(((BitmapDrawable)paramzzeg).getBitmap());
  }
  
  private static boolean zzb(zzmd paramzzmd, zzgu paramzzgu, CountDownLatch paramCountDownLatch)
    throws RemoteException
  {
    Object localObject = paramzzmd.getView();
    if (localObject == null)
    {
      zzkx.zzdi("AdWebView is null");
      return false;
    }
    ((View)localObject).setVisibility(4);
    localObject = paramzzgu.zzbwm.zzbvg;
    if ((localObject == null) || (((List)localObject).isEmpty()))
    {
      zzkx.zzdi("No template ids present in mediation response");
      return false;
    }
    zza(paramzzmd, paramCountDownLatch);
    paramCountDownLatch = paramzzgu.zzbwn.zzom();
    zzhe localzzhe = paramzzgu.zzbwn.zzon();
    if ((((List)localObject).contains("2")) && (paramCountDownLatch != null))
    {
      zza(paramzzmd, zza(paramCountDownLatch), paramzzgu.zzbwm.zzbvf);
      paramCountDownLatch = paramzzgu.zzbwm.zzbvd;
      paramzzgu = paramzzgu.zzbwm.zzbve;
      if (paramzzgu == null) {
        break label190;
      }
      paramzzmd.loadDataWithBaseURL(paramzzgu, paramCountDownLatch, "text/html", "UTF-8", null);
    }
    for (;;)
    {
      return true;
      if ((((List)localObject).contains("1")) && (localzzhe != null))
      {
        zza(paramzzmd, zza(localzzhe), paramzzgu.zzbwm.zzbvf);
        break;
      }
      zzkx.zzdi("No matching template id and mapper");
      return false;
      label190:
      paramzzmd.loadData(paramCountDownLatch, "text/html", "UTF-8");
    }
  }
  
  @Nullable
  private static zzeg zze(Object paramObject)
  {
    if ((paramObject instanceof IBinder)) {
      return zzeg.zza.zzab((IBinder)paramObject);
    }
    return null;
  }
  
  @Nullable
  public static View zzg(@Nullable zzko paramzzko)
  {
    if (paramzzko == null)
    {
      zzkx.e("AdState is null");
      return null;
    }
    if ((zzh(paramzzko)) && (paramzzko.zzcbm != null)) {
      return paramzzko.zzcbm.getView();
    }
    for (;;)
    {
      try
      {
        if (paramzzko.zzbwn != null)
        {
          paramzzko = paramzzko.zzbwn.getView();
          if (paramzzko == null)
          {
            zzkx.zzdi("View in mediation adapter is null.");
            return null;
          }
          paramzzko = (View)com.google.android.gms.dynamic.zze.zzae(paramzzko);
          return paramzzko;
        }
      }
      catch (RemoteException paramzzko)
      {
        zzkx.zzc("Could not get View from mediation adapter.", paramzzko);
        return null;
      }
      paramzzko = null;
    }
  }
  
  public static boolean zzh(@Nullable zzko paramzzko)
  {
    return (paramzzko != null) && (paramzzko.zzclb) && (paramzzko.zzbwm != null) && (paramzzko.zzbwm.zzbvd != null);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/zzn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */