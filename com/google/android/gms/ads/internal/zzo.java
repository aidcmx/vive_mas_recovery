package com.google.android.gms.ads.internal;

import android.content.Context;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.cache.zza;
import com.google.android.gms.ads.internal.client.zzz.zza;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.dynamic.zzd;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.internal.zzdn;
import com.google.android.gms.internal.zzdr;
import com.google.android.gms.internal.zzji;
import com.google.android.gms.internal.zzkr;
import com.google.android.gms.internal.zzkx;
import com.google.android.gms.internal.zzle;

@zzji
public class zzo
  extends zzz.zza
{
  private static final Object zzaox = new Object();
  @Nullable
  private static zzo zzaoy;
  private final Context mContext;
  private final Object zzako = new Object();
  private boolean zzaoz;
  private boolean zzapa;
  private float zzapb = -1.0F;
  private VersionInfoParcel zzapc;
  
  zzo(Context paramContext, VersionInfoParcel paramVersionInfoParcel)
  {
    this.mContext = paramContext;
    this.zzapc = paramVersionInfoParcel;
    this.zzaoz = false;
  }
  
  public static zzo zza(Context paramContext, VersionInfoParcel paramVersionInfoParcel)
  {
    synchronized (zzaox)
    {
      if (zzaoy == null) {
        zzaoy = new zzo(paramContext.getApplicationContext(), paramVersionInfoParcel);
      }
      paramContext = zzaoy;
      return paramContext;
    }
  }
  
  @Nullable
  public static zzo zzfq()
  {
    synchronized (zzaox)
    {
      zzo localzzo = zzaoy;
      return localzzo;
    }
  }
  
  public void initialize()
  {
    synchronized (zzaox)
    {
      if (this.zzaoz)
      {
        zzkx.zzdi("Mobile ads is initialized already.");
        return;
      }
      this.zzaoz = true;
      zzdr.initialize(this.mContext);
      zzu.zzgq().zzc(this.mContext, this.zzapc);
      zzu.zzgr().initialize(this.mContext);
      return;
    }
  }
  
  public void setAppMuted(boolean paramBoolean)
  {
    synchronized (this.zzako)
    {
      this.zzapa = paramBoolean;
      return;
    }
  }
  
  public void setAppVolume(float paramFloat)
  {
    synchronized (this.zzako)
    {
      this.zzapb = paramFloat;
      return;
    }
  }
  
  public void zzb(zzd paramzzd, String paramString)
  {
    paramzzd = zzc(paramzzd, paramString);
    if (paramzzd == null)
    {
      zzkx.e("Context is null. Failed to open debug menu.");
      return;
    }
    paramzzd.showDialog();
  }
  
  @Nullable
  protected zzle zzc(zzd paramzzd, String paramString)
  {
    if (paramzzd == null) {
      return null;
    }
    paramzzd = (Context)zze.zzae(paramzzd);
    if (paramzzd == null) {
      return null;
    }
    paramzzd = new zzle(paramzzd);
    paramzzd.setAdUnitId(paramString);
    return paramzzd;
  }
  
  public float zzfr()
  {
    synchronized (this.zzako)
    {
      float f = this.zzapb;
      return f;
    }
  }
  
  public boolean zzfs()
  {
    for (;;)
    {
      synchronized (this.zzako)
      {
        if (this.zzapb >= 0.0F)
        {
          bool = true;
          return bool;
        }
      }
      boolean bool = false;
    }
  }
  
  public boolean zzft()
  {
    synchronized (this.zzako)
    {
      boolean bool = this.zzapa;
      return bool;
    }
  }
  
  public void zzz(String paramString)
  {
    zzdr.initialize(this.mContext);
    if ((!TextUtils.isEmpty(paramString)) && (((Boolean)zzdr.zzbjv.get()).booleanValue())) {
      zzu.zzhi().zza(this.mContext, this.zzapc, true, null, paramString, null);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/zzo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */