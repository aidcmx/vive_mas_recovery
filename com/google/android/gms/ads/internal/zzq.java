package com.google.android.gms.ads.internal;

import android.content.Context;
import android.os.Handler;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.support.v4.util.SimpleArrayMap;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.client.VideoOptionsParcel;
import com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel;
import com.google.android.gms.ads.internal.formats.zze;
import com.google.android.gms.ads.internal.formats.zzf;
import com.google.android.gms.ads.internal.formats.zzg;
import com.google.android.gms.ads.internal.formats.zzh;
import com.google.android.gms.ads.internal.formats.zzi;
import com.google.android.gms.ads.internal.formats.zzi.zza;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.internal.zzco;
import com.google.android.gms.internal.zzdz;
import com.google.android.gms.internal.zzed;
import com.google.android.gms.internal.zzeg;
import com.google.android.gms.internal.zzeq;
import com.google.android.gms.internal.zzer;
import com.google.android.gms.internal.zzes;
import com.google.android.gms.internal.zzet;
import com.google.android.gms.internal.zzgz;
import com.google.android.gms.internal.zzha;
import com.google.android.gms.internal.zzhd;
import com.google.android.gms.internal.zzhe;
import com.google.android.gms.internal.zzig;
import com.google.android.gms.internal.zziu;
import com.google.android.gms.internal.zzji;
import com.google.android.gms.internal.zzko;
import com.google.android.gms.internal.zzko.zza;
import com.google.android.gms.internal.zzkr;
import com.google.android.gms.internal.zzkx;
import com.google.android.gms.internal.zzlb;
import com.google.android.gms.internal.zzmd;
import com.google.android.gms.internal.zzmi;
import java.util.List;

@zzji
public class zzq
  extends zzb
{
  private zzmd zzapd;
  
  public zzq(Context paramContext, zzd paramzzd, AdSizeParcel paramAdSizeParcel, String paramString, zzgz paramzzgz, VersionInfoParcel paramVersionInfoParcel)
  {
    super(paramContext, paramAdSizeParcel, paramString, paramzzgz, paramVersionInfoParcel, paramzzd);
  }
  
  private static com.google.android.gms.ads.internal.formats.zzd zza(zzhd paramzzhd)
    throws RemoteException
  {
    String str1 = paramzzhd.getHeadline();
    List localList = paramzzhd.getImages();
    String str2 = paramzzhd.getBody();
    if (paramzzhd.zzmo() != null) {}
    for (zzeg localzzeg = paramzzhd.zzmo();; localzzeg = null) {
      return new com.google.android.gms.ads.internal.formats.zzd(str1, localList, str2, localzzeg, paramzzhd.getCallToAction(), paramzzhd.getStarRating(), paramzzhd.getStore(), paramzzhd.getPrice(), null, paramzzhd.getExtras(), paramzzhd.zzej(), null);
    }
  }
  
  private static zze zza(zzhe paramzzhe)
    throws RemoteException
  {
    String str1 = paramzzhe.getHeadline();
    List localList = paramzzhe.getImages();
    String str2 = paramzzhe.getBody();
    if (paramzzhe.zzmt() != null) {}
    for (zzeg localzzeg = paramzzhe.zzmt();; localzzeg = null) {
      return new zze(str1, localList, str2, localzzeg, paramzzhe.getCallToAction(), paramzzhe.getAdvertiser(), null, paramzzhe.getExtras());
    }
  }
  
  private void zza(final com.google.android.gms.ads.internal.formats.zzd paramzzd)
  {
    zzlb.zzcvl.post(new Runnable()
    {
      public void run()
      {
        try
        {
          if (zzq.this.zzaly.zzarw != null) {
            zzq.this.zzaly.zzarw.zza(paramzzd);
          }
          return;
        }
        catch (RemoteException localRemoteException)
        {
          zzkx.zzc("Could not call OnAppInstallAdLoadedListener.onAppInstallAdLoaded().", localRemoteException);
        }
      }
    });
  }
  
  private void zza(final zze paramzze)
  {
    zzlb.zzcvl.post(new Runnable()
    {
      public void run()
      {
        try
        {
          if (zzq.this.zzaly.zzarx != null) {
            zzq.this.zzaly.zzarx.zza(paramzze);
          }
          return;
        }
        catch (RemoteException localRemoteException)
        {
          zzkx.zzc("Could not call OnContentAdLoadedListener.onContentAdLoaded().", localRemoteException);
        }
      }
    });
  }
  
  private void zza(final zzko paramzzko, final String paramString)
  {
    zzlb.zzcvl.post(new Runnable()
    {
      public void run()
      {
        try
        {
          ((zzet)zzq.this.zzaly.zzarz.get(paramString)).zza((zzf)paramzzko.zzcsq);
          return;
        }
        catch (RemoteException localRemoteException)
        {
          zzkx.zzc("Could not call onCustomTemplateAdLoadedListener.onCustomTemplateAdLoaded().", localRemoteException);
        }
      }
    });
  }
  
  public void pause()
  {
    throw new IllegalStateException("Native Ad DOES NOT support pause().");
  }
  
  public void resume()
  {
    throw new IllegalStateException("Native Ad DOES NOT support resume().");
  }
  
  public void showInterstitial()
  {
    throw new IllegalStateException("Interstitial is NOT supported by NativeAdManager.");
  }
  
  public void zza(SimpleArrayMap<String, zzet> paramSimpleArrayMap)
  {
    zzaa.zzhs("setOnCustomTemplateAdLoadedListeners must be called on the main UI thread.");
    this.zzaly.zzarz = paramSimpleArrayMap;
  }
  
  public void zza(zzg paramzzg)
  {
    if (this.zzapd != null) {
      this.zzapd.zzb(paramzzg);
    }
  }
  
  public void zza(zzi paramzzi)
  {
    if (this.zzaly.zzarn.zzcsi != null) {
      zzu.zzgq().zzvg().zza(this.zzaly.zzarm, this.zzaly.zzarn, paramzzi);
    }
  }
  
  public void zza(zzed paramzzed)
  {
    throw new IllegalStateException("CustomRendering is NOT supported by NativeAdManager.");
  }
  
  public void zza(zzig paramzzig)
  {
    throw new IllegalStateException("In App Purchase is NOT supported by NativeAdManager.");
  }
  
  public void zza(final zzko.zza paramzza, zzdz paramzzdz)
  {
    if (paramzza.zzarm != null) {
      this.zzaly.zzarm = paramzza.zzarm;
    }
    if (paramzza.errorCode != -2)
    {
      zzlb.zzcvl.post(new Runnable()
      {
        public void run()
        {
          zzq.this.zzb(new zzko(paramzza, null, null, null, null, null, null, null));
        }
      });
      return;
    }
    this.zzaly.zzasi = 0;
    this.zzaly.zzarl = zzu.zzgl().zza(this.zzaly.zzahs, this, paramzza, this.zzaly.zzarh, null, this.zzamf, this, paramzzdz);
    paramzza = String.valueOf(this.zzaly.zzarl.getClass().getName());
    if (paramzza.length() != 0) {}
    for (paramzza = "AdRenderer: ".concat(paramzza);; paramzza = new String("AdRenderer: "))
    {
      zzkx.zzdg(paramzza);
      return;
    }
  }
  
  protected boolean zza(AdRequestParcel paramAdRequestParcel, zzko paramzzko, boolean paramBoolean)
  {
    return this.zzalx.zzfy();
  }
  
  protected boolean zza(zzko paramzzko1, zzko paramzzko2)
  {
    zzb(null);
    if (!this.zzaly.zzhp()) {
      throw new IllegalStateException("Native ad DOES NOT have custom rendering mode.");
    }
    if (paramzzko2.zzclb) {}
    for (;;)
    {
      try
      {
        if (paramzzko2.zzbwn == null) {
          continue;
        }
        localObject1 = paramzzko2.zzbwn.zzom();
        if (paramzzko2.zzbwn == null) {
          continue;
        }
        localObject2 = paramzzko2.zzbwn.zzon();
        if ((localObject1 == null) || (this.zzaly.zzarw == null)) {
          continue;
        }
        localObject2 = zza((zzhd)localObject1);
        ((com.google.android.gms.ads.internal.formats.zzd)localObject2).zzb(new zzh(this.zzaly.zzahs, this, this.zzaly.zzarh, (zzhd)localObject1, (zzi.zza)localObject2));
        zza((com.google.android.gms.ads.internal.formats.zzd)localObject2);
      }
      catch (RemoteException localRemoteException)
      {
        Object localObject1;
        Object localObject2;
        zzkx.zzc("Failed to get native ad mapper", localRemoteException);
        continue;
        zzkx.zzdi("No matching mapper/listener for retrieved native ad template.");
        zzh(0);
        return false;
      }
      return super.zza(paramzzko1, paramzzko2);
      localObject1 = null;
      continue;
      localObject2 = null;
      continue;
      if ((localObject2 != null) && (this.zzaly.zzarx != null))
      {
        localObject1 = zza((zzhe)localObject2);
        ((zze)localObject1).zzb(new zzh(this.zzaly.zzahs, this, this.zzaly.zzarh, (zzhe)localObject2, (zzi.zza)localObject1));
        zza((zze)localObject1);
      }
      else
      {
        zzi.zza localzza = paramzzko2.zzcsq;
        if (((localzza instanceof zze)) && (this.zzaly.zzarx != null))
        {
          zza((zze)paramzzko2.zzcsq);
        }
        else if (((localzza instanceof com.google.android.gms.ads.internal.formats.zzd)) && (this.zzaly.zzarw != null))
        {
          zza((com.google.android.gms.ads.internal.formats.zzd)paramzzko2.zzcsq);
        }
        else
        {
          if ((!(localzza instanceof zzf)) || (this.zzaly.zzarz == null) || (this.zzaly.zzarz.get(((zzf)localzza).getCustomTemplateId()) == null)) {
            break;
          }
          zza(paramzzko2, ((zzf)localzza).getCustomTemplateId());
        }
      }
    }
    zzkx.zzdi("No matching listener for retrieved native ad template.");
    zzh(0);
    return false;
  }
  
  @Nullable
  public zzes zzaa(String paramString)
  {
    zzaa.zzhs("getOnCustomClickListener must be called on the main UI thread.");
    return (zzes)this.zzaly.zzary.get(paramString);
  }
  
  public void zzb(SimpleArrayMap<String, zzes> paramSimpleArrayMap)
  {
    zzaa.zzhs("setOnCustomClickListener must be called on the main UI thread.");
    this.zzaly.zzary = paramSimpleArrayMap;
  }
  
  public void zzb(NativeAdOptionsParcel paramNativeAdOptionsParcel)
  {
    zzaa.zzhs("setNativeAdOptions must be called on the main UI thread.");
    this.zzaly.zzasa = paramNativeAdOptionsParcel;
  }
  
  public void zzb(zzeq paramzzeq)
  {
    zzaa.zzhs("setOnAppInstallAdLoadedListener must be called on the main UI thread.");
    this.zzaly.zzarw = paramzzeq;
  }
  
  public void zzb(zzer paramzzer)
  {
    zzaa.zzhs("setOnContentAdLoadedListener must be called on the main UI thread.");
    this.zzaly.zzarx = paramzzer;
  }
  
  public void zzb(@Nullable List<String> paramList)
  {
    zzaa.zzhs("setNativeTemplates must be called on the main UI thread.");
    this.zzaly.zzase = paramList;
  }
  
  public void zzc(zzmd paramzzmd)
  {
    this.zzapd = paramzzmd;
  }
  
  public void zzfu()
  {
    if ((this.zzaly.zzarn != null) && (this.zzapd != null))
    {
      zzu.zzgq().zzvg().zza(this.zzaly.zzarm, this.zzaly.zzarn, this.zzapd.getView(), this.zzapd);
      return;
    }
    zzkx.zzdi("Request to enable ActiveView before adState is available.");
  }
  
  public SimpleArrayMap<String, zzet> zzfv()
  {
    zzaa.zzhs("getOnCustomTemplateAdLoadedListeners must be called on the main UI thread.");
    return this.zzaly.zzarz;
  }
  
  public void zzfw()
  {
    if (this.zzapd != null)
    {
      this.zzapd.destroy();
      this.zzapd = null;
    }
  }
  
  public void zzfx()
  {
    if ((this.zzapd != null) && (this.zzapd.zzxn() != null) && (this.zzaly.zzasa != null) && (this.zzaly.zzasa.zzbon != null)) {
      this.zzapd.zzxn().zzaq(this.zzaly.zzasa.zzbon.zzbck);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/zzq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */