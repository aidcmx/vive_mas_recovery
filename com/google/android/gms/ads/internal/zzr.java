package com.google.android.gms.ads.internal;

import android.os.Handler;
import android.support.annotation.Nullable;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.internal.zzji;
import com.google.android.gms.internal.zzkx;
import com.google.android.gms.internal.zzlb;
import java.lang.ref.WeakReference;

@zzji
public class zzr
{
  private final zza zzapi;
  @Nullable
  private AdRequestParcel zzapj;
  private boolean zzapk = false;
  private boolean zzapl = false;
  private long zzapm = 0L;
  private final Runnable zzw;
  
  public zzr(zza paramzza)
  {
    this(paramzza, new zza(zzlb.zzcvl));
  }
  
  zzr(zza paramzza, zza paramzza1)
  {
    this.zzapi = paramzza1;
    this.zzw = new Runnable()
    {
      public void run()
      {
        zzr.zza(zzr.this, false);
        zza localzza = (zza)this.zzapn.get();
        if (localzza != null) {
          localzza.zzd(zzr.zza(zzr.this));
        }
      }
    };
  }
  
  public void cancel()
  {
    this.zzapk = false;
    this.zzapi.removeCallbacks(this.zzw);
  }
  
  public void pause()
  {
    this.zzapl = true;
    if (this.zzapk) {
      this.zzapi.removeCallbacks(this.zzw);
    }
  }
  
  public void resume()
  {
    this.zzapl = false;
    if (this.zzapk)
    {
      this.zzapk = false;
      zza(this.zzapj, this.zzapm);
    }
  }
  
  public void zza(AdRequestParcel paramAdRequestParcel, long paramLong)
  {
    if (this.zzapk) {
      zzkx.zzdi("An ad refresh is already scheduled.");
    }
    do
    {
      return;
      this.zzapj = paramAdRequestParcel;
      this.zzapk = true;
      this.zzapm = paramLong;
    } while (this.zzapl);
    zzkx.zzdh(65 + "Scheduling ad refresh " + paramLong + " milliseconds from now.");
    this.zzapi.postDelayed(this.zzw, paramLong);
  }
  
  public boolean zzfy()
  {
    return this.zzapk;
  }
  
  public void zzg(AdRequestParcel paramAdRequestParcel)
  {
    this.zzapj = paramAdRequestParcel;
  }
  
  public void zzh(AdRequestParcel paramAdRequestParcel)
  {
    zza(paramAdRequestParcel, 60000L);
  }
  
  public static class zza
  {
    private final Handler mHandler;
    
    public zza(Handler paramHandler)
    {
      this.mHandler = paramHandler;
    }
    
    public boolean postDelayed(Runnable paramRunnable, long paramLong)
    {
      return this.mHandler.postDelayed(paramRunnable, paramLong);
    }
    
    public void removeCallbacks(Runnable paramRunnable)
    {
      this.mHandler.removeCallbacks(paramRunnable);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/zzr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */