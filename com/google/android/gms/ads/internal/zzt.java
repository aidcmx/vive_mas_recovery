package com.google.android.gms.ads.internal;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.net.Uri.Builder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.client.SearchAdRequestParcel;
import com.google.android.gms.ads.internal.client.VideoOptionsParcel;
import com.google.android.gms.ads.internal.client.zzab;
import com.google.android.gms.ads.internal.client.zzm;
import com.google.android.gms.ads.internal.client.zzp;
import com.google.android.gms.ads.internal.client.zzq;
import com.google.android.gms.ads.internal.client.zzu.zza;
import com.google.android.gms.ads.internal.client.zzw;
import com.google.android.gms.ads.internal.client.zzy;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.internal.zzcf;
import com.google.android.gms.internal.zzcg;
import com.google.android.gms.internal.zzdn;
import com.google.android.gms.internal.zzdr;
import com.google.android.gms.internal.zzed;
import com.google.android.gms.internal.zzig;
import com.google.android.gms.internal.zzik;
import com.google.android.gms.internal.zzji;
import com.google.android.gms.internal.zzkx;
import com.google.android.gms.internal.zzla;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@zzji
public class zzt
  extends zzu.zza
{
  private final Context mContext;
  @Nullable
  private zzq zzanl;
  private final VersionInfoParcel zzanu;
  private final AdSizeParcel zzapp;
  private final Future<zzcf> zzapq;
  private final zzb zzapr;
  @Nullable
  private WebView zzaps;
  @Nullable
  private zzcf zzapt;
  private AsyncTask<Void, Void, String> zzapu;
  
  public zzt(Context paramContext, AdSizeParcel paramAdSizeParcel, String paramString, VersionInfoParcel paramVersionInfoParcel)
  {
    this.mContext = paramContext;
    this.zzanu = paramVersionInfoParcel;
    this.zzapp = paramAdSizeParcel;
    this.zzaps = new WebView(this.mContext);
    this.zzapq = zzgc();
    this.zzapr = new zzb(paramString);
    zzfz();
  }
  
  private String zzac(String paramString)
  {
    if (this.zzapt == null) {
      return paramString;
    }
    paramString = Uri.parse(paramString);
    try
    {
      Uri localUri = this.zzapt.zzd(paramString, this.mContext);
      paramString = localUri;
    }
    catch (RemoteException localRemoteException)
    {
      for (;;)
      {
        zzkx.zzc("Unable to process ad data", localRemoteException);
      }
    }
    catch (zzcg localzzcg)
    {
      for (;;)
      {
        zzkx.zzc("Unable to parse ad click url", localzzcg);
      }
    }
    return paramString.toString();
  }
  
  private void zzad(String paramString)
  {
    Intent localIntent = new Intent("android.intent.action.VIEW");
    localIntent.setData(Uri.parse(paramString));
    this.mContext.startActivity(localIntent);
  }
  
  private void zzfz()
  {
    zzj(0);
    this.zzaps.setVerticalScrollBarEnabled(false);
    this.zzaps.getSettings().setJavaScriptEnabled(true);
    this.zzaps.setWebViewClient(new WebViewClient()
    {
      public void onReceivedError(WebView paramAnonymousWebView, WebResourceRequest paramAnonymousWebResourceRequest, WebResourceError paramAnonymousWebResourceError)
      {
        if (zzt.zza(zzt.this) != null) {}
        try
        {
          zzt.zza(zzt.this).onAdFailedToLoad(0);
          return;
        }
        catch (RemoteException paramAnonymousWebView)
        {
          zzkx.zzc("Could not call AdListener.onAdFailedToLoad().", paramAnonymousWebView);
        }
      }
      
      public boolean shouldOverrideUrlLoading(WebView paramAnonymousWebView, String paramAnonymousString)
      {
        if (paramAnonymousString.startsWith(zzt.this.zzgb())) {
          return false;
        }
        if (paramAnonymousString.startsWith((String)zzdr.zzbka.get()))
        {
          if (zzt.zza(zzt.this) != null) {}
          try
          {
            zzt.zza(zzt.this).onAdFailedToLoad(3);
            zzt.this.zzj(0);
            return true;
          }
          catch (RemoteException paramAnonymousWebView)
          {
            for (;;)
            {
              zzkx.zzc("Could not call AdListener.onAdFailedToLoad().", paramAnonymousWebView);
            }
          }
        }
        if (paramAnonymousString.startsWith((String)zzdr.zzbkb.get()))
        {
          if (zzt.zza(zzt.this) != null) {}
          try
          {
            zzt.zza(zzt.this).onAdFailedToLoad(0);
            zzt.this.zzj(0);
            return true;
          }
          catch (RemoteException paramAnonymousWebView)
          {
            for (;;)
            {
              zzkx.zzc("Could not call AdListener.onAdFailedToLoad().", paramAnonymousWebView);
            }
          }
        }
        if (paramAnonymousString.startsWith((String)zzdr.zzbkc.get()))
        {
          if (zzt.zza(zzt.this) != null) {}
          try
          {
            zzt.zza(zzt.this).onAdLoaded();
            int i = zzt.this.zzab(paramAnonymousString);
            zzt.this.zzj(i);
            return true;
          }
          catch (RemoteException paramAnonymousWebView)
          {
            for (;;)
            {
              zzkx.zzc("Could not call AdListener.onAdLoaded().", paramAnonymousWebView);
            }
          }
        }
        if (paramAnonymousString.startsWith("gmsg://")) {
          return true;
        }
        if (zzt.zza(zzt.this) != null) {}
        try
        {
          zzt.zza(zzt.this).onAdLeftApplication();
          paramAnonymousWebView = zzt.zza(zzt.this, paramAnonymousString);
          zzt.zzb(zzt.this, paramAnonymousWebView);
          return true;
        }
        catch (RemoteException paramAnonymousWebView)
        {
          for (;;)
          {
            zzkx.zzc("Could not call AdListener.onAdLeftApplication().", paramAnonymousWebView);
          }
        }
      }
    });
    this.zzaps.setOnTouchListener(new View.OnTouchListener()
    {
      public boolean onTouch(View paramAnonymousView, MotionEvent paramAnonymousMotionEvent)
      {
        if (zzt.zzb(zzt.this) != null) {}
        try
        {
          zzt.zzb(zzt.this).zza(paramAnonymousMotionEvent);
          return false;
        }
        catch (RemoteException paramAnonymousView)
        {
          for (;;)
          {
            zzkx.zzc("Unable to process ad data", paramAnonymousView);
          }
        }
      }
    });
  }
  
  private Future<zzcf> zzgc()
  {
    zzla.zza(new Callable()
    {
      public zzcf zzgd()
        throws Exception
      {
        return new zzcf(zzt.zzc(zzt.this).zzda, zzt.zzd(zzt.this), false);
      }
    });
  }
  
  public void destroy()
    throws RemoteException
  {
    zzaa.zzhs("destroy must be called on the main UI thread.");
    this.zzapu.cancel(true);
    this.zzapq.cancel(true);
    this.zzaps.destroy();
    this.zzaps = null;
  }
  
  @Nullable
  public String getMediationAdapterClassName()
    throws RemoteException
  {
    return null;
  }
  
  public boolean isLoading()
    throws RemoteException
  {
    return false;
  }
  
  public boolean isReady()
    throws RemoteException
  {
    return false;
  }
  
  public void pause()
    throws RemoteException
  {
    zzaa.zzhs("pause must be called on the main UI thread.");
  }
  
  public void resume()
    throws RemoteException
  {
    zzaa.zzhs("resume must be called on the main UI thread.");
  }
  
  public void setManualImpressionsEnabled(boolean paramBoolean)
    throws RemoteException
  {}
  
  public void setUserId(String paramString)
    throws RemoteException
  {
    throw new IllegalStateException("Unused method");
  }
  
  public void showInterstitial()
    throws RemoteException
  {
    throw new IllegalStateException("Unused method");
  }
  
  public void stopLoading()
    throws RemoteException
  {}
  
  public void zza(AdSizeParcel paramAdSizeParcel)
    throws RemoteException
  {
    throw new IllegalStateException("AdSize must be set before initialization");
  }
  
  public void zza(VideoOptionsParcel paramVideoOptionsParcel)
  {
    throw new IllegalStateException("Unused method");
  }
  
  public void zza(zzp paramzzp)
    throws RemoteException
  {
    throw new IllegalStateException("Unused method");
  }
  
  public void zza(zzq paramzzq)
    throws RemoteException
  {
    this.zzanl = paramzzq;
  }
  
  public void zza(zzw paramzzw)
    throws RemoteException
  {
    throw new IllegalStateException("Unused method");
  }
  
  public void zza(zzy paramzzy)
    throws RemoteException
  {
    throw new IllegalStateException("Unused method");
  }
  
  public void zza(com.google.android.gms.ads.internal.reward.client.zzd paramzzd)
    throws RemoteException
  {
    throw new IllegalStateException("Unused method");
  }
  
  public void zza(zzed paramzzed)
    throws RemoteException
  {
    throw new IllegalStateException("Unused method");
  }
  
  public void zza(zzig paramzzig)
    throws RemoteException
  {
    throw new IllegalStateException("Unused method");
  }
  
  public void zza(zzik paramzzik, String paramString)
    throws RemoteException
  {
    throw new IllegalStateException("Unused method");
  }
  
  int zzab(String paramString)
  {
    paramString = Uri.parse(paramString).getQueryParameter("height");
    if (TextUtils.isEmpty(paramString)) {
      return 0;
    }
    try
    {
      int i = zzm.zzkr().zzb(this.mContext, Integer.parseInt(paramString));
      return i;
    }
    catch (NumberFormatException paramString) {}
    return 0;
  }
  
  public boolean zzb(AdRequestParcel paramAdRequestParcel)
    throws RemoteException
  {
    zzaa.zzb(this.zzaps, "This Search Ad has already been torn down");
    this.zzapr.zzi(paramAdRequestParcel);
    this.zzapu = new zza(null).execute(new Void[0]);
    return true;
  }
  
  public com.google.android.gms.dynamic.zzd zzef()
    throws RemoteException
  {
    zzaa.zzhs("getAdFrame must be called on the main UI thread.");
    return zze.zzac(this.zzaps);
  }
  
  public AdSizeParcel zzeg()
    throws RemoteException
  {
    return this.zzapp;
  }
  
  public void zzei()
    throws RemoteException
  {
    throw new IllegalStateException("Unused method");
  }
  
  @Nullable
  public zzab zzej()
  {
    return null;
  }
  
  String zzga()
  {
    Object localObject1 = new Uri.Builder();
    ((Uri.Builder)localObject1).scheme("https://").appendEncodedPath((String)zzdr.zzbkd.get());
    ((Uri.Builder)localObject1).appendQueryParameter("query", this.zzapr.getQuery());
    ((Uri.Builder)localObject1).appendQueryParameter("pubId", this.zzapr.zzgf());
    Object localObject3 = this.zzapr.zzgg();
    Iterator localIterator = ((Map)localObject3).keySet().iterator();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      ((Uri.Builder)localObject1).appendQueryParameter(str, (String)((Map)localObject3).get(str));
    }
    localObject3 = ((Uri.Builder)localObject1).build();
    if (this.zzapt != null) {}
    try
    {
      localObject1 = this.zzapt.zzc((Uri)localObject3, this.mContext);
      localObject3 = String.valueOf(zzgb());
      localObject1 = String.valueOf(((Uri)localObject1).getEncodedQuery());
      return String.valueOf(localObject3).length() + 1 + String.valueOf(localObject1).length() + (String)localObject3 + "#" + (String)localObject1;
    }
    catch (RemoteException localRemoteException)
    {
      for (;;)
      {
        zzkx.zzc("Unable to process ad data", localRemoteException);
        Object localObject2 = localObject3;
      }
    }
    catch (zzcg localzzcg)
    {
      for (;;) {}
    }
  }
  
  String zzgb()
  {
    String str1 = this.zzapr.zzge();
    if (TextUtils.isEmpty(str1)) {
      str1 = "www.google.com";
    }
    for (;;)
    {
      String str2 = String.valueOf("https://");
      String str3 = (String)zzdr.zzbkd.get();
      return String.valueOf(str2).length() + 0 + String.valueOf(str1).length() + String.valueOf(str3).length() + str2 + str1 + str3;
    }
  }
  
  void zzj(int paramInt)
  {
    if (this.zzaps == null) {
      return;
    }
    ViewGroup.LayoutParams localLayoutParams = new ViewGroup.LayoutParams(-1, paramInt);
    this.zzaps.setLayoutParams(localLayoutParams);
  }
  
  private class zza
    extends AsyncTask<Void, Void, String>
  {
    private zza() {}
    
    protected String zza(Void... paramVarArgs)
    {
      try
      {
        zzt.zza(zzt.this, (zzcf)zzt.zze(zzt.this).get(((Long)zzdr.zzbkf.get()).longValue(), TimeUnit.MILLISECONDS));
        return zzt.this.zzga();
      }
      catch (InterruptedException paramVarArgs)
      {
        for (;;)
        {
          zzkx.zzc("Failed to load ad data", paramVarArgs);
        }
      }
      catch (TimeoutException paramVarArgs)
      {
        for (;;)
        {
          zzkx.zzdi("Timed out waiting for ad data");
        }
      }
      catch (ExecutionException paramVarArgs)
      {
        for (;;) {}
      }
    }
    
    protected void zzae(String paramString)
    {
      if ((zzt.zzf(zzt.this) != null) && (paramString != null)) {
        zzt.zzf(zzt.this).loadUrl(paramString);
      }
    }
  }
  
  private static class zzb
  {
    private final String zzapw;
    private final Map<String, String> zzapx;
    private String zzapy;
    private String zzapz;
    
    public zzb(String paramString)
    {
      this.zzapw = paramString;
      this.zzapx = new TreeMap();
    }
    
    public String getQuery()
    {
      return this.zzapy;
    }
    
    public String zzge()
    {
      return this.zzapz;
    }
    
    public String zzgf()
    {
      return this.zzapw;
    }
    
    public Map<String, String> zzgg()
    {
      return this.zzapx;
    }
    
    public void zzi(AdRequestParcel paramAdRequestParcel)
    {
      this.zzapy = paramAdRequestParcel.zzays.zzbcj;
      if (paramAdRequestParcel.zzayv != null) {}
      for (paramAdRequestParcel = paramAdRequestParcel.zzayv.getBundle(AdMobAdapter.class.getName());; paramAdRequestParcel = null)
      {
        if (paramAdRequestParcel == null) {}
        for (;;)
        {
          return;
          String str1 = (String)zzdr.zzbke.get();
          Iterator localIterator = paramAdRequestParcel.keySet().iterator();
          while (localIterator.hasNext())
          {
            String str2 = (String)localIterator.next();
            if (str1.equals(str2)) {
              this.zzapz = paramAdRequestParcel.getString(str2);
            } else if (str2.startsWith("csa_")) {
              this.zzapx.put(str2.substring("csa_".length()), paramAdRequestParcel.getString(str2));
            }
          }
        }
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/zzt.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */