package com.google.android.gms.ads.internal;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.support.v4.util.SimpleArrayMap;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.ViewTreeObserver.OnScrollChangedListener;
import android.widget.ViewSwitcher;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.client.VideoOptionsParcel;
import com.google.android.gms.ads.internal.client.zzm;
import com.google.android.gms.ads.internal.client.zzp;
import com.google.android.gms.ads.internal.client.zzq;
import com.google.android.gms.ads.internal.client.zzw;
import com.google.android.gms.ads.internal.client.zzy;
import com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel;
import com.google.android.gms.ads.internal.purchase.zzk;
import com.google.android.gms.ads.internal.reward.client.zzd;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.internal.zzav;
import com.google.android.gms.internal.zzdr;
import com.google.android.gms.internal.zzdt;
import com.google.android.gms.internal.zzed;
import com.google.android.gms.internal.zzeq;
import com.google.android.gms.internal.zzer;
import com.google.android.gms.internal.zzes;
import com.google.android.gms.internal.zzet;
import com.google.android.gms.internal.zzha;
import com.google.android.gms.internal.zzig;
import com.google.android.gms.internal.zzik;
import com.google.android.gms.internal.zzji;
import com.google.android.gms.internal.zzko;
import com.google.android.gms.internal.zzko.zza;
import com.google.android.gms.internal.zzkp;
import com.google.android.gms.internal.zzkr;
import com.google.android.gms.internal.zzku;
import com.google.android.gms.internal.zzkw;
import com.google.android.gms.internal.zzkx;
import com.google.android.gms.internal.zzld;
import com.google.android.gms.internal.zzle;
import com.google.android.gms.internal.zzlm;
import com.google.android.gms.internal.zzlp;
import com.google.android.gms.internal.zzmd;
import com.google.android.gms.internal.zzme;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

@zzji
public final class zzv
  implements ViewTreeObserver.OnGlobalLayoutListener, ViewTreeObserver.OnScrollChangedListener
{
  public final Context zzahs;
  boolean zzaok = false;
  final String zzarf;
  public String zzarg;
  final zzav zzarh;
  public final VersionInfoParcel zzari;
  @Nullable
  zza zzarj;
  @Nullable
  public zzkw zzark;
  @Nullable
  public zzld zzarl;
  public AdSizeParcel zzarm;
  @Nullable
  public zzko zzarn;
  public zzko.zza zzaro;
  @Nullable
  public zzkp zzarp;
  @Nullable
  zzp zzarq;
  @Nullable
  zzq zzarr;
  @Nullable
  zzw zzars;
  @Nullable
  zzy zzart;
  @Nullable
  zzig zzaru;
  @Nullable
  zzik zzarv;
  @Nullable
  zzeq zzarw;
  @Nullable
  zzer zzarx;
  SimpleArrayMap<String, zzes> zzary;
  SimpleArrayMap<String, zzet> zzarz;
  NativeAdOptionsParcel zzasa;
  @Nullable
  VideoOptionsParcel zzasb;
  @Nullable
  zzed zzasc;
  @Nullable
  zzd zzasd;
  @Nullable
  List<String> zzase;
  @Nullable
  zzk zzasf;
  @Nullable
  public zzku zzasg = null;
  @Nullable
  View zzash = null;
  public int zzasi = 0;
  boolean zzasj = false;
  private HashSet<zzkp> zzask = null;
  private int zzasl = -1;
  private int zzasm = -1;
  private zzlm zzasn;
  private boolean zzaso = true;
  private boolean zzasp = true;
  private boolean zzasq = false;
  
  public zzv(Context paramContext, AdSizeParcel paramAdSizeParcel, String paramString, VersionInfoParcel paramVersionInfoParcel)
  {
    this(paramContext, paramAdSizeParcel, paramString, paramVersionInfoParcel, null);
  }
  
  zzv(Context paramContext, AdSizeParcel paramAdSizeParcel, String paramString, VersionInfoParcel paramVersionInfoParcel, zzav paramzzav)
  {
    zzdr.initialize(paramContext);
    if (zzu.zzgq().zzuu() != null)
    {
      List localList = zzdr.zzlr();
      if (paramVersionInfoParcel.zzcya != 0) {
        localList.add(Integer.toString(paramVersionInfoParcel.zzcya));
      }
      zzu.zzgq().zzuu().zzc(localList);
    }
    this.zzarf = UUID.randomUUID().toString();
    if ((paramAdSizeParcel.zzazr) || (paramAdSizeParcel.zzazt))
    {
      this.zzarj = null;
      this.zzarm = paramAdSizeParcel;
      this.zzarg = paramString;
      this.zzahs = paramContext;
      this.zzari = paramVersionInfoParcel;
      if (paramzzav == null) {
        break label247;
      }
    }
    for (;;)
    {
      this.zzarh = paramzzav;
      this.zzasn = new zzlm(200L);
      this.zzarz = new SimpleArrayMap();
      return;
      this.zzarj = new zza(paramContext, paramString, this, this);
      this.zzarj.setMinimumWidth(paramAdSizeParcel.widthPixels);
      this.zzarj.setMinimumHeight(paramAdSizeParcel.heightPixels);
      this.zzarj.setVisibility(4);
      break;
      label247:
      paramzzav = new zzav(new zzi(this));
    }
  }
  
  private void zzh(boolean paramBoolean)
  {
    boolean bool = true;
    if ((this.zzarj == null) || (this.zzarn == null) || (this.zzarn.zzcbm == null) || (this.zzarn.zzcbm.zzxc() == null)) {}
    while ((paramBoolean) && (!this.zzasn.tryAcquire())) {
      return;
    }
    Object localObject;
    int i;
    int j;
    if (this.zzarn.zzcbm.zzxc().zzic())
    {
      localObject = new int[2];
      this.zzarj.getLocationOnScreen((int[])localObject);
      i = zzm.zzkr().zzc(this.zzahs, localObject[0]);
      j = zzm.zzkr().zzc(this.zzahs, localObject[1]);
      if ((i != this.zzasl) || (j != this.zzasm))
      {
        this.zzasl = i;
        this.zzasm = j;
        localObject = this.zzarn.zzcbm.zzxc();
        i = this.zzasl;
        j = this.zzasm;
        if (paramBoolean) {
          break label189;
        }
      }
    }
    label189:
    for (paramBoolean = bool;; paramBoolean = false)
    {
      ((zzme)localObject).zza(i, j, paramBoolean);
      zzhs();
      return;
    }
  }
  
  private void zzhs()
  {
    if (this.zzarj == null) {}
    Rect localRect1;
    Rect localRect2;
    do
    {
      View localView;
      do
      {
        return;
        localView = this.zzarj.getRootView().findViewById(16908290);
      } while (localView == null);
      localRect1 = new Rect();
      localRect2 = new Rect();
      this.zzarj.getGlobalVisibleRect(localRect1);
      localView.getGlobalVisibleRect(localRect2);
      if (localRect1.top != localRect2.top) {
        this.zzaso = false;
      }
    } while (localRect1.bottom == localRect2.bottom);
    this.zzasp = false;
  }
  
  public void destroy()
  {
    zzhr();
    this.zzarr = null;
    this.zzars = null;
    this.zzarv = null;
    this.zzaru = null;
    this.zzasc = null;
    this.zzart = null;
    zzi(false);
    if (this.zzarj != null) {
      this.zzarj.removeAllViews();
    }
    zzhm();
    zzho();
    this.zzarn = null;
  }
  
  public void onGlobalLayout()
  {
    zzh(false);
  }
  
  public void onScrollChanged()
  {
    zzh(true);
    this.zzasq = true;
  }
  
  public void zza(HashSet<zzkp> paramHashSet)
  {
    this.zzask = paramHashSet;
  }
  
  public HashSet<zzkp> zzhl()
  {
    return this.zzask;
  }
  
  public void zzhm()
  {
    if ((this.zzarn != null) && (this.zzarn.zzcbm != null)) {
      this.zzarn.zzcbm.destroy();
    }
  }
  
  public void zzhn()
  {
    if ((this.zzarn != null) && (this.zzarn.zzcbm != null)) {
      this.zzarn.zzcbm.stopLoading();
    }
  }
  
  public void zzho()
  {
    if ((this.zzarn != null) && (this.zzarn.zzbwn != null)) {}
    try
    {
      this.zzarn.zzbwn.destroy();
      return;
    }
    catch (RemoteException localRemoteException)
    {
      zzkx.zzdi("Could not destroy mediation adapter.");
    }
  }
  
  public boolean zzhp()
  {
    return this.zzasi == 0;
  }
  
  public boolean zzhq()
  {
    return this.zzasi == 1;
  }
  
  public void zzhr()
  {
    if (this.zzarj != null) {
      this.zzarj.zzhr();
    }
  }
  
  public String zzht()
  {
    if ((this.zzaso) && (this.zzasp)) {
      return "";
    }
    if (this.zzaso)
    {
      if (this.zzasq) {
        return "top-scrollable";
      }
      return "top-locked";
    }
    if (this.zzasp)
    {
      if (this.zzasq) {
        return "bottom-scrollable";
      }
      return "bottom-locked";
    }
    return "";
  }
  
  public void zzhu()
  {
    if (this.zzarp == null) {
      return;
    }
    if (this.zzarn != null)
    {
      this.zzarp.zzm(this.zzarn.zzcso);
      this.zzarp.zzn(this.zzarn.zzcsp);
      this.zzarp.zzae(this.zzarn.zzclb);
    }
    this.zzarp.zzad(this.zzarm.zzazr);
  }
  
  public void zzi(boolean paramBoolean)
  {
    if (this.zzasi == 0) {
      zzhn();
    }
    if (this.zzark != null) {
      this.zzark.cancel();
    }
    if (this.zzarl != null) {
      this.zzarl.cancel();
    }
    if (paramBoolean) {
      this.zzarn = null;
    }
  }
  
  public static class zza
    extends ViewSwitcher
  {
    private final zzle zzasr;
    @Nullable
    private final zzlp zzass;
    private boolean zzast;
    
    public zza(Context paramContext, String paramString, ViewTreeObserver.OnGlobalLayoutListener paramOnGlobalLayoutListener, ViewTreeObserver.OnScrollChangedListener paramOnScrollChangedListener)
    {
      super();
      this.zzasr = new zzle(paramContext);
      this.zzasr.setAdUnitId(paramString);
      this.zzast = true;
      if ((paramContext instanceof Activity)) {}
      for (this.zzass = new zzlp((Activity)paramContext, this, paramOnGlobalLayoutListener, paramOnScrollChangedListener);; this.zzass = new zzlp(null, this, paramOnGlobalLayoutListener, paramOnScrollChangedListener))
      {
        this.zzass.zzwl();
        return;
      }
    }
    
    protected void onAttachedToWindow()
    {
      super.onAttachedToWindow();
      if (this.zzass != null) {
        this.zzass.onAttachedToWindow();
      }
    }
    
    protected void onDetachedFromWindow()
    {
      super.onDetachedFromWindow();
      if (this.zzass != null) {
        this.zzass.onDetachedFromWindow();
      }
    }
    
    public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
    {
      if (this.zzast) {
        this.zzasr.zzg(paramMotionEvent);
      }
      return false;
    }
    
    public void removeAllViews()
    {
      Object localObject = new ArrayList();
      int i = 0;
      while (i < getChildCount())
      {
        View localView = getChildAt(i);
        if ((localView != null) && ((localView instanceof zzmd))) {
          ((List)localObject).add((zzmd)localView);
        }
        i += 1;
      }
      super.removeAllViews();
      localObject = ((List)localObject).iterator();
      while (((Iterator)localObject).hasNext()) {
        ((zzmd)((Iterator)localObject).next()).destroy();
      }
    }
    
    public void zzhr()
    {
      zzkx.v("Disable position monitoring on adFrame.");
      if (this.zzass != null) {
        this.zzass.zzwm();
      }
    }
    
    public zzle zzhv()
    {
      return this.zzasr;
    }
    
    public void zzhw()
    {
      zzkx.v("Enable debug gesture detector on adFrame.");
      this.zzast = true;
    }
    
    public void zzhx()
    {
      zzkx.v("Disable debug gesture detector on adFrame.");
      this.zzast = false;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/internal/zzv.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */