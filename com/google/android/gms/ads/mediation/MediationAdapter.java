package com.google.android.gms.ads.mediation;

import android.os.Bundle;

public abstract interface MediationAdapter
{
  public abstract void onDestroy();
  
  public abstract void onPause();
  
  public abstract void onResume();
  
  public static class zza
  {
    private int P;
    
    public zza zzbk(int paramInt)
    {
      this.P = paramInt;
      return this;
    }
    
    public Bundle zzys()
    {
      Bundle localBundle = new Bundle();
      localBundle.putInt("capabilities", this.P);
      return localBundle;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/mediation/MediationAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */