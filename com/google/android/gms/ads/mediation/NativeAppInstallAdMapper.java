package com.google.android.gms.ads.mediation;

import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.formats.NativeAd.Image;
import java.util.List;

public abstract class NativeAppInstallAdMapper
  extends NativeAdMapper
{
  private NativeAd.Image Q;
  private VideoController zzbbc;
  private String zzbmy;
  private List<NativeAd.Image> zzbmz;
  private String zzbna;
  private String zzbnc;
  private double zzbnd;
  private String zzbne;
  private String zzbnf;
  
  public final String getBody()
  {
    return this.zzbna;
  }
  
  public final String getCallToAction()
  {
    return this.zzbnc;
  }
  
  public final String getHeadline()
  {
    return this.zzbmy;
  }
  
  public final NativeAd.Image getIcon()
  {
    return this.Q;
  }
  
  public final List<NativeAd.Image> getImages()
  {
    return this.zzbmz;
  }
  
  public final String getPrice()
  {
    return this.zzbnf;
  }
  
  public final double getStarRating()
  {
    return this.zzbnd;
  }
  
  public final String getStore()
  {
    return this.zzbne;
  }
  
  public final VideoController getVideoController()
  {
    return this.zzbbc;
  }
  
  public final void setBody(String paramString)
  {
    this.zzbna = paramString;
  }
  
  public final void setCallToAction(String paramString)
  {
    this.zzbnc = paramString;
  }
  
  public final void setHeadline(String paramString)
  {
    this.zzbmy = paramString;
  }
  
  public final void setIcon(NativeAd.Image paramImage)
  {
    this.Q = paramImage;
  }
  
  public final void setImages(List<NativeAd.Image> paramList)
  {
    this.zzbmz = paramList;
  }
  
  public final void setPrice(String paramString)
  {
    this.zzbnf = paramString;
  }
  
  public final void setStarRating(double paramDouble)
  {
    this.zzbnd = paramDouble;
  }
  
  public final void setStore(String paramString)
  {
    this.zzbne = paramString;
  }
  
  public final void zza(VideoController paramVideoController)
  {
    this.zzbbc = paramVideoController;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/mediation/NativeAppInstallAdMapper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */