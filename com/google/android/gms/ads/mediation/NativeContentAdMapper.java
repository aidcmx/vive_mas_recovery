package com.google.android.gms.ads.mediation;

import com.google.android.gms.ads.formats.NativeAd.Image;
import java.util.List;

public abstract class NativeContentAdMapper
  extends NativeAdMapper
{
  private NativeAd.Image R;
  private String zzbmy;
  private List<NativeAd.Image> zzbmz;
  private String zzbna;
  private String zzbnc;
  private String zzbnl;
  
  public final String getAdvertiser()
  {
    return this.zzbnl;
  }
  
  public final String getBody()
  {
    return this.zzbna;
  }
  
  public final String getCallToAction()
  {
    return this.zzbnc;
  }
  
  public final String getHeadline()
  {
    return this.zzbmy;
  }
  
  public final List<NativeAd.Image> getImages()
  {
    return this.zzbmz;
  }
  
  public final NativeAd.Image getLogo()
  {
    return this.R;
  }
  
  public final void setAdvertiser(String paramString)
  {
    this.zzbnl = paramString;
  }
  
  public final void setBody(String paramString)
  {
    this.zzbna = paramString;
  }
  
  public final void setCallToAction(String paramString)
  {
    this.zzbnc = paramString;
  }
  
  public final void setHeadline(String paramString)
  {
    this.zzbmy = paramString;
  }
  
  public final void setImages(List<NativeAd.Image> paramList)
  {
    this.zzbmz = paramList;
  }
  
  public final void setLogo(NativeAd.Image paramImage)
  {
    this.R = paramImage;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/mediation/NativeContentAdMapper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */