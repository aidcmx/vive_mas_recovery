package com.google.android.gms.ads.mediation;

import android.content.Context;

public abstract interface OnContextChangedListener
{
  public abstract void onContextChanged(Context paramContext);
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/mediation/OnContextChangedListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */