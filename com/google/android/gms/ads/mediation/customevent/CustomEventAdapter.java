package com.google.android.gms.ads.mediation.customevent;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.ads.mediation.MediationAdRequest;
import com.google.android.gms.ads.mediation.MediationBannerAdapter;
import com.google.android.gms.ads.mediation.MediationBannerListener;
import com.google.android.gms.ads.mediation.MediationInterstitialAdapter;
import com.google.android.gms.ads.mediation.MediationInterstitialListener;
import com.google.android.gms.ads.mediation.MediationNativeAdapter;
import com.google.android.gms.ads.mediation.MediationNativeListener;
import com.google.android.gms.ads.mediation.NativeAdMapper;
import com.google.android.gms.ads.mediation.NativeMediationAdRequest;
import com.google.android.gms.common.annotation.KeepName;

@KeepName
public final class CustomEventAdapter
  implements MediationBannerAdapter, MediationInterstitialAdapter, MediationNativeAdapter
{
  CustomEventBanner S;
  CustomEventInterstitial T;
  CustomEventNative U;
  private View zzgw;
  
  private void zza(View paramView)
  {
    this.zzgw = paramView;
  }
  
  private static <T> T zzj(String paramString)
  {
    try
    {
      Object localObject = Class.forName(paramString).newInstance();
      return (T)localObject;
    }
    catch (Throwable localThrowable)
    {
      String str = String.valueOf(localThrowable.getMessage());
      zzb.zzdi(String.valueOf(paramString).length() + 46 + String.valueOf(str).length() + "Could not instantiate custom event adapter: " + paramString + ". " + str);
    }
    return null;
  }
  
  public View getBannerView()
  {
    return this.zzgw;
  }
  
  public void onDestroy()
  {
    if (this.S != null) {
      this.S.onDestroy();
    }
    if (this.T != null) {
      this.T.onDestroy();
    }
    if (this.U != null) {
      this.U.onDestroy();
    }
  }
  
  public void onPause()
  {
    if (this.S != null) {
      this.S.onPause();
    }
    if (this.T != null) {
      this.T.onPause();
    }
    if (this.U != null) {
      this.U.onPause();
    }
  }
  
  public void onResume()
  {
    if (this.S != null) {
      this.S.onResume();
    }
    if (this.T != null) {
      this.T.onResume();
    }
    if (this.U != null) {
      this.U.onResume();
    }
  }
  
  public void requestBannerAd(Context paramContext, MediationBannerListener paramMediationBannerListener, Bundle paramBundle1, AdSize paramAdSize, MediationAdRequest paramMediationAdRequest, Bundle paramBundle2)
  {
    this.S = ((CustomEventBanner)zzj(paramBundle1.getString("class_name")));
    if (this.S == null)
    {
      paramMediationBannerListener.onAdFailedToLoad(this, 0);
      return;
    }
    if (paramBundle2 == null) {}
    for (paramBundle2 = null;; paramBundle2 = paramBundle2.getBundle(paramBundle1.getString("class_name")))
    {
      this.S.requestBannerAd(paramContext, new zza(this, paramMediationBannerListener), paramBundle1.getString("parameter"), paramAdSize, paramMediationAdRequest, paramBundle2);
      return;
    }
  }
  
  public void requestInterstitialAd(Context paramContext, MediationInterstitialListener paramMediationInterstitialListener, Bundle paramBundle1, MediationAdRequest paramMediationAdRequest, Bundle paramBundle2)
  {
    this.T = ((CustomEventInterstitial)zzj(paramBundle1.getString("class_name")));
    if (this.T == null)
    {
      paramMediationInterstitialListener.onAdFailedToLoad(this, 0);
      return;
    }
    if (paramBundle2 == null) {}
    for (paramBundle2 = null;; paramBundle2 = paramBundle2.getBundle(paramBundle1.getString("class_name")))
    {
      this.T.requestInterstitialAd(paramContext, zza(paramMediationInterstitialListener), paramBundle1.getString("parameter"), paramMediationAdRequest, paramBundle2);
      return;
    }
  }
  
  public void requestNativeAd(Context paramContext, MediationNativeListener paramMediationNativeListener, Bundle paramBundle1, NativeMediationAdRequest paramNativeMediationAdRequest, Bundle paramBundle2)
  {
    this.U = ((CustomEventNative)zzj(paramBundle1.getString("class_name")));
    if (this.U == null)
    {
      paramMediationNativeListener.onAdFailedToLoad(this, 0);
      return;
    }
    if (paramBundle2 == null) {}
    for (paramBundle2 = null;; paramBundle2 = paramBundle2.getBundle(paramBundle1.getString("class_name")))
    {
      this.U.requestNativeAd(paramContext, new zzc(this, paramMediationNativeListener), paramBundle1.getString("parameter"), paramNativeMediationAdRequest, paramBundle2);
      return;
    }
  }
  
  public void showInterstitial()
  {
    this.T.showInterstitial();
  }
  
  zzb zza(MediationInterstitialListener paramMediationInterstitialListener)
  {
    return new zzb(this, paramMediationInterstitialListener);
  }
  
  static final class zza
    implements CustomEventBannerListener
  {
    private final CustomEventAdapter V;
    private final MediationBannerListener zzgo;
    
    public zza(CustomEventAdapter paramCustomEventAdapter, MediationBannerListener paramMediationBannerListener)
    {
      this.V = paramCustomEventAdapter;
      this.zzgo = paramMediationBannerListener;
    }
    
    public void onAdClicked()
    {
      zzb.zzdg("Custom event adapter called onAdClicked.");
      this.zzgo.onAdClicked(this.V);
    }
    
    public void onAdClosed()
    {
      zzb.zzdg("Custom event adapter called onAdClosed.");
      this.zzgo.onAdClosed(this.V);
    }
    
    public void onAdFailedToLoad(int paramInt)
    {
      zzb.zzdg("Custom event adapter called onAdFailedToLoad.");
      this.zzgo.onAdFailedToLoad(this.V, paramInt);
    }
    
    public void onAdLeftApplication()
    {
      zzb.zzdg("Custom event adapter called onAdLeftApplication.");
      this.zzgo.onAdLeftApplication(this.V);
    }
    
    public void onAdLoaded(View paramView)
    {
      zzb.zzdg("Custom event adapter called onAdLoaded.");
      CustomEventAdapter.zza(this.V, paramView);
      this.zzgo.onAdLoaded(this.V);
    }
    
    public void onAdOpened()
    {
      zzb.zzdg("Custom event adapter called onAdOpened.");
      this.zzgo.onAdOpened(this.V);
    }
  }
  
  class zzb
    implements CustomEventInterstitialListener
  {
    private final CustomEventAdapter V;
    private final MediationInterstitialListener zzgp;
    
    public zzb(CustomEventAdapter paramCustomEventAdapter, MediationInterstitialListener paramMediationInterstitialListener)
    {
      this.V = paramCustomEventAdapter;
      this.zzgp = paramMediationInterstitialListener;
    }
    
    public void onAdClicked()
    {
      zzb.zzdg("Custom event adapter called onAdClicked.");
      this.zzgp.onAdClicked(this.V);
    }
    
    public void onAdClosed()
    {
      zzb.zzdg("Custom event adapter called onAdClosed.");
      this.zzgp.onAdClosed(this.V);
    }
    
    public void onAdFailedToLoad(int paramInt)
    {
      zzb.zzdg("Custom event adapter called onFailedToReceiveAd.");
      this.zzgp.onAdFailedToLoad(this.V, paramInt);
    }
    
    public void onAdLeftApplication()
    {
      zzb.zzdg("Custom event adapter called onAdLeftApplication.");
      this.zzgp.onAdLeftApplication(this.V);
    }
    
    public void onAdLoaded()
    {
      zzb.zzdg("Custom event adapter called onReceivedAd.");
      this.zzgp.onAdLoaded(CustomEventAdapter.this);
    }
    
    public void onAdOpened()
    {
      zzb.zzdg("Custom event adapter called onAdOpened.");
      this.zzgp.onAdOpened(this.V);
    }
  }
  
  static class zzc
    implements CustomEventNativeListener
  {
    private final CustomEventAdapter V;
    private final MediationNativeListener zzgq;
    
    public zzc(CustomEventAdapter paramCustomEventAdapter, MediationNativeListener paramMediationNativeListener)
    {
      this.V = paramCustomEventAdapter;
      this.zzgq = paramMediationNativeListener;
    }
    
    public void onAdClicked()
    {
      zzb.zzdg("Custom event adapter called onAdClicked.");
      this.zzgq.onAdClicked(this.V);
    }
    
    public void onAdClosed()
    {
      zzb.zzdg("Custom event adapter called onAdClosed.");
      this.zzgq.onAdClosed(this.V);
    }
    
    public void onAdFailedToLoad(int paramInt)
    {
      zzb.zzdg("Custom event adapter called onAdFailedToLoad.");
      this.zzgq.onAdFailedToLoad(this.V, paramInt);
    }
    
    public void onAdImpression()
    {
      zzb.zzdg("Custom event adapter called onAdImpression.");
      this.zzgq.onAdImpression(this.V);
    }
    
    public void onAdLeftApplication()
    {
      zzb.zzdg("Custom event adapter called onAdLeftApplication.");
      this.zzgq.onAdLeftApplication(this.V);
    }
    
    public void onAdLoaded(NativeAdMapper paramNativeAdMapper)
    {
      zzb.zzdg("Custom event adapter called onAdLoaded.");
      this.zzgq.onAdLoaded(this.V, paramNativeAdMapper);
    }
    
    public void onAdOpened()
    {
      zzb.zzdg("Custom event adapter called onAdOpened.");
      this.zzgq.onAdOpened(this.V);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/mediation/customevent/CustomEventAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */