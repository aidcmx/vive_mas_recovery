package com.google.android.gms.ads.purchase;

public abstract interface InAppPurchaseListener
{
  public abstract void onInAppPurchaseRequested(InAppPurchase paramInAppPurchase);
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/purchase/InAppPurchaseListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */