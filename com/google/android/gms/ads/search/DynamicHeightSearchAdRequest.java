package com.google.android.gms.ads.search;

import android.content.Context;
import android.os.Bundle;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.internal.client.zzad;
import com.google.android.gms.ads.mediation.MediationAdapter;
import com.google.android.gms.ads.mediation.NetworkExtras;
import com.google.android.gms.ads.mediation.customevent.CustomEvent;

public final class DynamicHeightSearchAdRequest
{
  private final SearchAdRequest Z;
  
  private DynamicHeightSearchAdRequest(Builder paramBuilder)
  {
    this.Z = Builder.zza(paramBuilder).build();
  }
  
  public <T extends CustomEvent> Bundle getCustomEventExtrasBundle(Class<T> paramClass)
  {
    return this.Z.getCustomEventExtrasBundle(paramClass);
  }
  
  @Deprecated
  public <T extends NetworkExtras> T getNetworkExtras(Class<T> paramClass)
  {
    return this.Z.getNetworkExtras(paramClass);
  }
  
  public <T extends MediationAdapter> Bundle getNetworkExtrasBundle(Class<T> paramClass)
  {
    return this.Z.getNetworkExtrasBundle(paramClass);
  }
  
  public String getQuery()
  {
    return this.Z.getQuery();
  }
  
  public boolean isTestDevice(Context paramContext)
  {
    return this.Z.isTestDevice(paramContext);
  }
  
  zzad zzdt()
  {
    return this.Z.zzdt();
  }
  
  public static final class Builder
  {
    private final SearchAdRequest.Builder aa = new SearchAdRequest.Builder();
    private final Bundle ab = new Bundle();
    
    public Builder addCustomEventExtrasBundle(Class<? extends CustomEvent> paramClass, Bundle paramBundle)
    {
      this.aa.addCustomEventExtrasBundle(paramClass, paramBundle);
      return this;
    }
    
    public Builder addNetworkExtras(NetworkExtras paramNetworkExtras)
    {
      this.aa.addNetworkExtras(paramNetworkExtras);
      return this;
    }
    
    public Builder addNetworkExtrasBundle(Class<? extends MediationAdapter> paramClass, Bundle paramBundle)
    {
      this.aa.addNetworkExtrasBundle(paramClass, paramBundle);
      return this;
    }
    
    public DynamicHeightSearchAdRequest build()
    {
      this.aa.addNetworkExtrasBundle(AdMobAdapter.class, this.ab);
      return new DynamicHeightSearchAdRequest(this, null);
    }
    
    public Builder setAdBorderSelectors(String paramString)
    {
      this.ab.putString("csa_adBorderSelectors", paramString);
      return this;
    }
    
    public Builder setAdTest(boolean paramBoolean)
    {
      Bundle localBundle = this.ab;
      if (paramBoolean) {}
      for (String str = "on";; str = "off")
      {
        localBundle.putString("csa_adtest", str);
        return this;
      }
    }
    
    public Builder setAdjustableLineHeight(int paramInt)
    {
      this.ab.putString("csa_adjustableLineHeight", Integer.toString(paramInt));
      return this;
    }
    
    public Builder setAdvancedOptionValue(String paramString1, String paramString2)
    {
      this.ab.putString(paramString1, paramString2);
      return this;
    }
    
    public Builder setAttributionSpacingBelow(int paramInt)
    {
      this.ab.putString("csa_attributionSpacingBelow", Integer.toString(paramInt));
      return this;
    }
    
    public Builder setBorderSelections(String paramString)
    {
      this.ab.putString("csa_borderSelections", paramString);
      return this;
    }
    
    public Builder setChannel(String paramString)
    {
      this.ab.putString("csa_channel", paramString);
      return this;
    }
    
    public Builder setColorAdBorder(String paramString)
    {
      this.ab.putString("csa_colorAdBorder", paramString);
      return this;
    }
    
    public Builder setColorAdSeparator(String paramString)
    {
      this.ab.putString("csa_colorAdSeparator", paramString);
      return this;
    }
    
    public Builder setColorAnnotation(String paramString)
    {
      this.ab.putString("csa_colorAnnotation", paramString);
      return this;
    }
    
    public Builder setColorAttribution(String paramString)
    {
      this.ab.putString("csa_colorAttribution", paramString);
      return this;
    }
    
    public Builder setColorBackground(String paramString)
    {
      this.ab.putString("csa_colorBackground", paramString);
      return this;
    }
    
    public Builder setColorBorder(String paramString)
    {
      this.ab.putString("csa_colorBorder", paramString);
      return this;
    }
    
    public Builder setColorDomainLink(String paramString)
    {
      this.ab.putString("csa_colorDomainLink", paramString);
      return this;
    }
    
    public Builder setColorText(String paramString)
    {
      this.ab.putString("csa_colorText", paramString);
      return this;
    }
    
    public Builder setColorTitleLink(String paramString)
    {
      this.ab.putString("csa_colorTitleLink", paramString);
      return this;
    }
    
    public Builder setCssWidth(int paramInt)
    {
      this.ab.putString("csa_width", Integer.toString(paramInt));
      return this;
    }
    
    public Builder setDetailedAttribution(boolean paramBoolean)
    {
      this.ab.putString("csa_detailedAttribution", Boolean.toString(paramBoolean));
      return this;
    }
    
    @Deprecated
    public Builder setFontFamily(int paramInt)
    {
      return setFontFamily(Integer.toString(paramInt));
    }
    
    public Builder setFontFamily(String paramString)
    {
      this.ab.putString("csa_fontFamily", paramString);
      return this;
    }
    
    public Builder setFontFamilyAttribution(String paramString)
    {
      this.ab.putString("csa_fontFamilyAttribution", paramString);
      return this;
    }
    
    public Builder setFontSizeAnnotation(int paramInt)
    {
      this.ab.putString("csa_fontSizeAnnotation", Integer.toString(paramInt));
      return this;
    }
    
    public Builder setFontSizeAttribution(int paramInt)
    {
      this.ab.putString("csa_fontSizeAttribution", Integer.toString(paramInt));
      return this;
    }
    
    public Builder setFontSizeDescription(int paramInt)
    {
      this.ab.putString("csa_fontSizeDescription", Integer.toString(paramInt));
      return this;
    }
    
    public Builder setFontSizeDomainLink(int paramInt)
    {
      this.ab.putString("csa_fontSizeDomainLink", Integer.toString(paramInt));
      return this;
    }
    
    public Builder setFontSizeTitle(int paramInt)
    {
      this.ab.putString("csa_fontSizeTitle", Integer.toString(paramInt));
      return this;
    }
    
    public Builder setHostLanguage(String paramString)
    {
      this.ab.putString("csa_hl", paramString);
      return this;
    }
    
    public Builder setIsClickToCallEnabled(boolean paramBoolean)
    {
      this.ab.putString("csa_clickToCall", Boolean.toString(paramBoolean));
      return this;
    }
    
    public Builder setIsLocationEnabled(boolean paramBoolean)
    {
      this.ab.putString("csa_location", Boolean.toString(paramBoolean));
      return this;
    }
    
    public Builder setIsPlusOnesEnabled(boolean paramBoolean)
    {
      this.ab.putString("csa_plusOnes", Boolean.toString(paramBoolean));
      return this;
    }
    
    public Builder setIsSellerRatingsEnabled(boolean paramBoolean)
    {
      this.ab.putString("csa_sellerRatings", Boolean.toString(paramBoolean));
      return this;
    }
    
    public Builder setIsSiteLinksEnabled(boolean paramBoolean)
    {
      this.ab.putString("csa_siteLinks", Boolean.toString(paramBoolean));
      return this;
    }
    
    public Builder setIsTitleBold(boolean paramBoolean)
    {
      this.ab.putString("csa_titleBold", Boolean.toString(paramBoolean));
      return this;
    }
    
    public Builder setIsTitleUnderlined(boolean paramBoolean)
    {
      Bundle localBundle = this.ab;
      if (!paramBoolean) {}
      for (paramBoolean = true;; paramBoolean = false)
      {
        localBundle.putString("csa_noTitleUnderline", Boolean.toString(paramBoolean));
        return this;
      }
    }
    
    public Builder setLocationColor(String paramString)
    {
      this.ab.putString("csa_colorLocation", paramString);
      return this;
    }
    
    public Builder setLocationFontSize(int paramInt)
    {
      this.ab.putString("csa_fontSizeLocation", Integer.toString(paramInt));
      return this;
    }
    
    public Builder setLongerHeadlines(boolean paramBoolean)
    {
      this.ab.putString("csa_longerHeadlines", Boolean.toString(paramBoolean));
      return this;
    }
    
    public Builder setNumber(int paramInt)
    {
      this.ab.putString("csa_number", Integer.toString(paramInt));
      return this;
    }
    
    public Builder setPage(int paramInt)
    {
      this.ab.putString("csa_adPage", Integer.toString(paramInt));
      return this;
    }
    
    public Builder setQuery(String paramString)
    {
      this.aa.setQuery(paramString);
      return this;
    }
    
    public Builder setVerticalSpacing(int paramInt)
    {
      this.ab.putString("csa_verticalSpacing", Integer.toString(paramInt));
      return this;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/search/DynamicHeightSearchAdRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */