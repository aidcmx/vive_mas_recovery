package com.google.android.gms.ads.search;

import android.content.Context;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import com.google.android.gms.ads.internal.client.zzad;
import com.google.android.gms.ads.internal.client.zzad.zza;
import com.google.android.gms.ads.mediation.MediationAdapter;
import com.google.android.gms.ads.mediation.NetworkExtras;
import com.google.android.gms.ads.mediation.customevent.CustomEvent;

public final class SearchAdRequest
{
  public static final int BORDER_TYPE_DASHED = 1;
  public static final int BORDER_TYPE_DOTTED = 2;
  public static final int BORDER_TYPE_NONE = 0;
  public static final int BORDER_TYPE_SOLID = 3;
  public static final int CALL_BUTTON_COLOR_DARK = 2;
  public static final int CALL_BUTTON_COLOR_LIGHT = 0;
  public static final int CALL_BUTTON_COLOR_MEDIUM = 1;
  public static final String DEVICE_ID_EMULATOR = zzad.DEVICE_ID_EMULATOR;
  public static final int ERROR_CODE_INTERNAL_ERROR = 0;
  public static final int ERROR_CODE_INVALID_REQUEST = 1;
  public static final int ERROR_CODE_NETWORK_ERROR = 2;
  public static final int ERROR_CODE_NO_FILL = 3;
  private final int ac;
  private final int ad;
  private final int ae;
  private final int af;
  private final int ag;
  private final int ah;
  private final int ai;
  private final String aj;
  private final int ak;
  private final String al;
  private final int am;
  private final int an;
  private final int mBackgroundColor;
  private final zzad zzakf;
  private final String zzapy;
  
  private SearchAdRequest(Builder paramBuilder)
  {
    this.ac = Builder.zza(paramBuilder);
    this.mBackgroundColor = Builder.zzb(paramBuilder);
    this.ad = Builder.zzc(paramBuilder);
    this.ae = Builder.zzd(paramBuilder);
    this.af = Builder.zze(paramBuilder);
    this.ag = Builder.zzf(paramBuilder);
    this.ah = Builder.zzg(paramBuilder);
    this.ai = Builder.zzh(paramBuilder);
    this.aj = Builder.zzi(paramBuilder);
    this.ak = Builder.zzj(paramBuilder);
    this.al = Builder.zzk(paramBuilder);
    this.am = Builder.zzl(paramBuilder);
    this.an = Builder.zzm(paramBuilder);
    this.zzapy = Builder.zzn(paramBuilder);
    this.zzakf = new zzad(Builder.zzo(paramBuilder), this);
  }
  
  public int getAnchorTextColor()
  {
    return this.ac;
  }
  
  public int getBackgroundColor()
  {
    return this.mBackgroundColor;
  }
  
  public int getBackgroundGradientBottom()
  {
    return this.ad;
  }
  
  public int getBackgroundGradientTop()
  {
    return this.ae;
  }
  
  public int getBorderColor()
  {
    return this.af;
  }
  
  public int getBorderThickness()
  {
    return this.ag;
  }
  
  public int getBorderType()
  {
    return this.ah;
  }
  
  public int getCallButtonColor()
  {
    return this.ai;
  }
  
  public String getCustomChannels()
  {
    return this.aj;
  }
  
  public <T extends CustomEvent> Bundle getCustomEventExtrasBundle(Class<T> paramClass)
  {
    return this.zzakf.getCustomEventExtrasBundle(paramClass);
  }
  
  public int getDescriptionTextColor()
  {
    return this.ak;
  }
  
  public String getFontFace()
  {
    return this.al;
  }
  
  public int getHeaderTextColor()
  {
    return this.am;
  }
  
  public int getHeaderTextSize()
  {
    return this.an;
  }
  
  public Location getLocation()
  {
    return this.zzakf.getLocation();
  }
  
  @Deprecated
  public <T extends NetworkExtras> T getNetworkExtras(Class<T> paramClass)
  {
    return this.zzakf.getNetworkExtras(paramClass);
  }
  
  public <T extends MediationAdapter> Bundle getNetworkExtrasBundle(Class<T> paramClass)
  {
    return this.zzakf.getNetworkExtrasBundle(paramClass);
  }
  
  public String getQuery()
  {
    return this.zzapy;
  }
  
  public boolean isTestDevice(Context paramContext)
  {
    return this.zzakf.isTestDevice(paramContext);
  }
  
  zzad zzdt()
  {
    return this.zzakf;
  }
  
  public static final class Builder
  {
    private int ac;
    private int ad;
    private int ae;
    private int af;
    private int ag;
    private int ah = 0;
    private int ai;
    private String aj;
    private int ak;
    private String al;
    private int am;
    private int an;
    private int mBackgroundColor;
    private final zzad.zza zzakg = new zzad.zza();
    private String zzapy;
    
    public Builder addCustomEventExtrasBundle(Class<? extends CustomEvent> paramClass, Bundle paramBundle)
    {
      this.zzakg.zzb(paramClass, paramBundle);
      return this;
    }
    
    public Builder addNetworkExtras(NetworkExtras paramNetworkExtras)
    {
      this.zzakg.zza(paramNetworkExtras);
      return this;
    }
    
    public Builder addNetworkExtrasBundle(Class<? extends MediationAdapter> paramClass, Bundle paramBundle)
    {
      this.zzakg.zza(paramClass, paramBundle);
      return this;
    }
    
    public Builder addTestDevice(String paramString)
    {
      this.zzakg.zzan(paramString);
      return this;
    }
    
    public SearchAdRequest build()
    {
      return new SearchAdRequest(this, null);
    }
    
    public Builder setAnchorTextColor(int paramInt)
    {
      this.ac = paramInt;
      return this;
    }
    
    public Builder setBackgroundColor(int paramInt)
    {
      this.mBackgroundColor = paramInt;
      this.ad = Color.argb(0, 0, 0, 0);
      this.ae = Color.argb(0, 0, 0, 0);
      return this;
    }
    
    public Builder setBackgroundGradient(int paramInt1, int paramInt2)
    {
      this.mBackgroundColor = Color.argb(0, 0, 0, 0);
      this.ad = paramInt2;
      this.ae = paramInt1;
      return this;
    }
    
    public Builder setBorderColor(int paramInt)
    {
      this.af = paramInt;
      return this;
    }
    
    public Builder setBorderThickness(int paramInt)
    {
      this.ag = paramInt;
      return this;
    }
    
    public Builder setBorderType(int paramInt)
    {
      this.ah = paramInt;
      return this;
    }
    
    public Builder setCallButtonColor(int paramInt)
    {
      this.ai = paramInt;
      return this;
    }
    
    public Builder setCustomChannels(String paramString)
    {
      this.aj = paramString;
      return this;
    }
    
    public Builder setDescriptionTextColor(int paramInt)
    {
      this.ak = paramInt;
      return this;
    }
    
    public Builder setFontFace(String paramString)
    {
      this.al = paramString;
      return this;
    }
    
    public Builder setHeaderTextColor(int paramInt)
    {
      this.am = paramInt;
      return this;
    }
    
    public Builder setHeaderTextSize(int paramInt)
    {
      this.an = paramInt;
      return this;
    }
    
    public Builder setLocation(Location paramLocation)
    {
      this.zzakg.zzb(paramLocation);
      return this;
    }
    
    public Builder setQuery(String paramString)
    {
      this.zzapy = paramString;
      return this;
    }
    
    public Builder setRequestAgent(String paramString)
    {
      this.zzakg.zzar(paramString);
      return this;
    }
    
    public Builder tagForChildDirectedTreatment(boolean paramBoolean)
    {
      this.zzakg.zzo(paramBoolean);
      return this;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/search/SearchAdRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */