package com.google.android.gms.ads.search;

import android.content.Context;
import android.support.annotation.RequiresPermission;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.internal.client.zzae;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.internal.zzji;

@zzji
public final class SearchAdView
  extends ViewGroup
{
  private final zzae zzakk;
  
  public SearchAdView(Context paramContext)
  {
    super(paramContext);
    this.zzakk = new zzae(this);
  }
  
  public SearchAdView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    this.zzakk = new zzae(this, paramAttributeSet, false);
  }
  
  public SearchAdView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    this.zzakk = new zzae(this, paramAttributeSet, false);
  }
  
  public void destroy()
  {
    this.zzakk.destroy();
  }
  
  public AdListener getAdListener()
  {
    return this.zzakk.getAdListener();
  }
  
  public AdSize getAdSize()
  {
    return this.zzakk.getAdSize();
  }
  
  public String getAdUnitId()
  {
    return this.zzakk.getAdUnitId();
  }
  
  @RequiresPermission("android.permission.INTERNET")
  public void loadAd(DynamicHeightSearchAdRequest paramDynamicHeightSearchAdRequest)
  {
    if (!AdSize.SEARCH.equals(getAdSize())) {
      throw new IllegalStateException("You must use AdSize.SEARCH for a DynamicHeightSearchAdRequest");
    }
    this.zzakk.zza(paramDynamicHeightSearchAdRequest.zzdt());
  }
  
  @RequiresPermission("android.permission.INTERNET")
  public void loadAd(SearchAdRequest paramSearchAdRequest)
  {
    this.zzakk.zza(paramSearchAdRequest.zzdt());
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    View localView = getChildAt(0);
    if ((localView != null) && (localView.getVisibility() != 8))
    {
      int i = localView.getMeasuredWidth();
      int j = localView.getMeasuredHeight();
      paramInt1 = (paramInt3 - paramInt1 - i) / 2;
      paramInt2 = (paramInt4 - paramInt2 - j) / 2;
      localView.layout(paramInt1, paramInt2, i + paramInt1, j + paramInt2);
    }
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    int i = 0;
    Object localObject1 = getChildAt(0);
    int j;
    if ((localObject1 != null) && (((View)localObject1).getVisibility() != 8))
    {
      measureChild((View)localObject1, paramInt1, paramInt2);
      j = ((View)localObject1).getMeasuredWidth();
      i = ((View)localObject1).getMeasuredHeight();
    }
    for (;;)
    {
      j = Math.max(j, getSuggestedMinimumWidth());
      i = Math.max(i, getSuggestedMinimumHeight());
      setMeasuredDimension(View.resolveSize(j, paramInt1), View.resolveSize(i, paramInt2));
      return;
      try
      {
        localObject1 = getAdSize();
        if (localObject1 != null)
        {
          Context localContext = getContext();
          j = ((AdSize)localObject1).getWidthInPixels(localContext);
          i = ((AdSize)localObject1).getHeightInPixels(localContext);
        }
      }
      catch (NullPointerException localNullPointerException)
      {
        for (;;)
        {
          zzb.zzb("Unable to retrieve ad size.", localNullPointerException);
          Object localObject2 = null;
        }
        j = 0;
      }
    }
  }
  
  public void pause()
  {
    this.zzakk.pause();
  }
  
  public void resume()
  {
    this.zzakk.resume();
  }
  
  public void setAdListener(AdListener paramAdListener)
  {
    this.zzakk.setAdListener(paramAdListener);
  }
  
  public void setAdSize(AdSize paramAdSize)
  {
    this.zzakk.setAdSizes(new AdSize[] { paramAdSize });
  }
  
  public void setAdUnitId(String paramString)
  {
    this.zzakk.setAdUnitId(paramString);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/ads/search/SearchAdView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */