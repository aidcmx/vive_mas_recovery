package com.google.android.gms.analytics;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.RequiresPermission;
import android.text.TextUtils;
import com.google.android.gms.analytics.internal.zzaf;
import com.google.android.gms.analytics.internal.zzao;
import com.google.android.gms.analytics.internal.zzf;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.internal.zzxr;

public class CampaignTrackingReceiver
  extends BroadcastReceiver
{
  static zzxr ax;
  static Boolean ay;
  static Object zzaox = new Object();
  
  public static boolean zzat(Context paramContext)
  {
    zzaa.zzy(paramContext);
    if (ay != null) {
      return ay.booleanValue();
    }
    boolean bool = zzao.zza(paramContext, "com.google.android.gms.analytics.CampaignTrackingReceiver", true);
    ay = Boolean.valueOf(bool);
    return bool;
  }
  
  @RequiresPermission(allOf={"android.permission.INTERNET", "android.permission.ACCESS_NETWORK_STATE"})
  public void onReceive(Context paramContext, Intent arg2)
  {
    Object localObject = zzf.zzaw(paramContext);
    localzzaf = ((zzf)localObject).zzaca();
    if (??? == null)
    {
      localzzaf.zzev("CampaignTrackingReceiver received null intent");
      return;
    }
    String str = ???.getStringExtra("referrer");
    ??? = ???.getAction();
    localzzaf.zza("CampaignTrackingReceiver received", ???);
    if ((!"com.android.vending.INSTALL_REFERRER".equals(???)) || (TextUtils.isEmpty(str)))
    {
      localzzaf.zzev("CampaignTrackingReceiver received unexpected intent without referrer extra");
      return;
    }
    boolean bool = CampaignTrackingService.zzau(paramContext);
    if (!bool) {
      localzzaf.zzev("CampaignTrackingService not registered or disabled. Installation tracking not possible. See http://goo.gl/8Rd3yj for instructions.");
    }
    zzp(paramContext, str);
    ((zzf)localObject).zzacb();
    ??? = zzyy();
    zzaa.zzy(???);
    localObject = new Intent(paramContext, ???);
    ((Intent)localObject).putExtra("referrer", str);
    synchronized (zzaox)
    {
      paramContext.startService((Intent)localObject);
      if (!bool) {
        return;
      }
    }
    try
    {
      if (ax == null)
      {
        ax = new zzxr(paramContext, 1, "Analytics campaign WakeLock");
        ax.setReferenceCounted(false);
      }
      ax.acquire(1000L);
    }
    catch (SecurityException paramContext)
    {
      for (;;)
      {
        localzzaf.zzev("CampaignTrackingService service at risk of not starting. For more reliable installation campaign reports, add the WAKE_LOCK permission to your manifest. See http://goo.gl/8Rd3yj for instructions.");
      }
    }
  }
  
  protected void zzp(Context paramContext, String paramString) {}
  
  protected Class<? extends CampaignTrackingService> zzyy()
  {
    return CampaignTrackingService.class;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/analytics/CampaignTrackingReceiver.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */