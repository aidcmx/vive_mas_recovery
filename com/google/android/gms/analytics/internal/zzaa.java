package com.google.android.gms.analytics.internal;

public class zzaa
  implements zzp
{
  public String bN;
  public String bO;
  public String ff;
  public int fg = -1;
  public int fh = -1;
  
  public String zzaae()
  {
    return this.bN;
  }
  
  public String zzaaf()
  {
    return this.bO;
  }
  
  public boolean zzafq()
  {
    return this.bN != null;
  }
  
  public boolean zzafr()
  {
    return this.bO != null;
  }
  
  public boolean zzafs()
  {
    return this.ff != null;
  }
  
  public String zzaft()
  {
    return this.ff;
  }
  
  public boolean zzafu()
  {
    return this.fg >= 0;
  }
  
  public int zzafv()
  {
    return this.fg;
  }
  
  public boolean zzafw()
  {
    return this.fh != -1;
  }
  
  public boolean zzafx()
  {
    return this.fh == 1;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/analytics/internal/zzaa.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */