package com.google.android.gms.analytics.internal;

import com.google.android.gms.common.util.zze;

public class zzad
{
  private final String cd;
  private final long fo;
  private final int fp;
  private double fq;
  private long fr;
  private final Object fs = new Object();
  private final zze zzaql;
  
  public zzad(int paramInt, long paramLong, String paramString, zze paramzze)
  {
    this.fp = paramInt;
    this.fq = this.fp;
    this.fo = paramLong;
    this.cd = paramString;
    this.zzaql = paramzze;
  }
  
  public zzad(String paramString, zze paramzze)
  {
    this(60, 2000L, paramString, paramzze);
  }
  
  public boolean zzagf()
  {
    synchronized (this.fs)
    {
      long l = this.zzaql.currentTimeMillis();
      if (this.fq < this.fp)
      {
        double d = (l - this.fr) / this.fo;
        if (d > 0.0D) {
          this.fq = Math.min(this.fp, d + this.fq);
        }
      }
      this.fr = l;
      if (this.fq >= 1.0D)
      {
        this.fq -= 1.0D;
        return true;
      }
      String str = this.cd;
      zzae.zzdi(String.valueOf(str).length() + 34 + "Excessive " + str + " detected; call ignored.");
      return false;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/analytics/internal/zzad.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */