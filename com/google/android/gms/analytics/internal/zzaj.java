package com.google.android.gms.analytics.internal;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.RequiresPermission;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.internal.zzxr;

public final class zzaj
{
  static zzxr ax;
  static Boolean ay;
  static Object zzaox = new Object();
  
  public static boolean zzat(Context paramContext)
  {
    zzaa.zzy(paramContext);
    if (ay != null) {
      return ay.booleanValue();
    }
    boolean bool = zzao.zza(paramContext, "com.google.android.gms.analytics.AnalyticsReceiver", false);
    ay = Boolean.valueOf(bool);
    return bool;
  }
  
  @RequiresPermission(allOf={"android.permission.INTERNET", "android.permission.ACCESS_NETWORK_STATE"})
  public void onReceive(Context paramContext, Intent arg2)
  {
    Object localObject = zzf.zzaw(paramContext);
    localzzaf = ((zzf)localObject).zzaca();
    if (??? == null) {
      localzzaf.zzev("AnalyticsReceiver called with null intent");
    }
    do
    {
      return;
      ??? = ???.getAction();
      ((zzf)localObject).zzacb();
      localzzaf.zza("Local AnalyticsReceiver got", ???);
    } while (!"com.google.android.gms.analytics.ANALYTICS_DISPATCH".equals(???));
    boolean bool = zzak.zzau(paramContext);
    localObject = new Intent("com.google.android.gms.analytics.ANALYTICS_DISPATCH");
    ((Intent)localObject).setComponent(new ComponentName(paramContext, "com.google.android.gms.analytics.AnalyticsService"));
    ((Intent)localObject).setAction("com.google.android.gms.analytics.ANALYTICS_DISPATCH");
    synchronized (zzaox)
    {
      paramContext.startService((Intent)localObject);
      if (!bool) {
        return;
      }
    }
    try
    {
      if (ax == null)
      {
        ax = new zzxr(paramContext, 1, "Analytics WakeLock");
        ax.setReferenceCounted(false);
      }
      ax.acquire(1000L);
    }
    catch (SecurityException paramContext)
    {
      for (;;)
      {
        localzzaf.zzev("Analytics service at risk of not starting. For more reliable analytics, add the WAKE_LOCK permission to your manifest. See http://goo.gl/8Rd3yj for instructions.");
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/analytics/internal/zzaj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */