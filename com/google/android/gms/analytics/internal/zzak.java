package com.google.android.gms.analytics.internal;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.annotation.RequiresPermission;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.internal.zzxr;

public final class zzak
{
  private static Boolean az;
  private final zza fL;
  private final Context mContext;
  private final Handler mHandler;
  
  public zzak(zza paramzza)
  {
    this.mContext = paramzza.getContext();
    zzaa.zzy(this.mContext);
    this.fL = paramzza;
    this.mHandler = new Handler();
  }
  
  public static boolean zzau(Context paramContext)
  {
    zzaa.zzy(paramContext);
    if (az != null) {
      return az.booleanValue();
    }
    boolean bool = zzao.zzr(paramContext, "com.google.android.gms.analytics.AnalyticsService");
    az = Boolean.valueOf(bool);
    return bool;
  }
  
  private void zzyz()
  {
    try
    {
      synchronized (zzaj.zzaox)
      {
        zzxr localzzxr = zzaj.ax;
        if ((localzzxr != null) && (localzzxr.isHeld())) {
          localzzxr.release();
        }
        return;
      }
      return;
    }
    catch (SecurityException localSecurityException) {}
  }
  
  @RequiresPermission(allOf={"android.permission.INTERNET", "android.permission.ACCESS_NETWORK_STATE"})
  public void onCreate()
  {
    zzf localzzf = zzf.zzaw(this.mContext);
    zzaf localzzaf = localzzf.zzaca();
    localzzf.zzacb();
    localzzaf.zzes("Local AnalyticsService is starting up");
  }
  
  @RequiresPermission(allOf={"android.permission.INTERNET", "android.permission.ACCESS_NETWORK_STATE"})
  public void onDestroy()
  {
    zzf localzzf = zzf.zzaw(this.mContext);
    zzaf localzzaf = localzzf.zzaca();
    localzzf.zzacb();
    localzzaf.zzes("Local AnalyticsService is shutting down");
  }
  
  @RequiresPermission(allOf={"android.permission.INTERNET", "android.permission.ACCESS_NETWORK_STATE"})
  public int onStartCommand(Intent paramIntent, int paramInt1, final int paramInt2)
  {
    zzyz();
    final zzf localzzf = zzf.zzaw(this.mContext);
    final zzaf localzzaf = localzzf.zzaca();
    if (paramIntent == null) {
      localzzaf.zzev("AnalyticsService started with null intent");
    }
    do
    {
      return 2;
      paramIntent = paramIntent.getAction();
      localzzf.zzacb();
      localzzaf.zza("Local AnalyticsService called. startId, action", Integer.valueOf(paramInt2), paramIntent);
    } while (!"com.google.android.gms.analytics.ANALYTICS_DISPATCH".equals(paramIntent));
    localzzf.zzzg().zza(new zzw()
    {
      public void zzf(Throwable paramAnonymousThrowable)
      {
        zzak.zzb(zzak.this).post(new Runnable()
        {
          public void run()
          {
            if (zzak.zza(zzak.this).callServiceStopSelfResult(zzak.1.this.aB))
            {
              zzak.1.this.fM.zzacb();
              zzak.1.this.aA.zzes("Local AnalyticsService processed last dispatch request");
            }
          }
        });
      }
    });
    return 2;
  }
  
  public static abstract interface zza
  {
    public abstract boolean callServiceStopSelfResult(int paramInt);
    
    public abstract Context getContext();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/analytics/internal/zzak.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */