package com.google.android.gms.analytics.internal;

import android.app.Activity;
import java.util.HashMap;
import java.util.Map;

public class zzan
  implements zzp
{
  public String at;
  public double fQ = -1.0D;
  public int fR = -1;
  public int fS = -1;
  public int fT = -1;
  public int fU = -1;
  public Map<String, String> fV = new HashMap();
  
  public int getSessionTimeout()
  {
    return this.fR;
  }
  
  public String getTrackingId()
  {
    return this.at;
  }
  
  public boolean zzahc()
  {
    return this.at != null;
  }
  
  public boolean zzahd()
  {
    return this.fQ >= 0.0D;
  }
  
  public double zzahe()
  {
    return this.fQ;
  }
  
  public boolean zzahf()
  {
    return this.fR >= 0;
  }
  
  public boolean zzahg()
  {
    return this.fS != -1;
  }
  
  public boolean zzahh()
  {
    return this.fS == 1;
  }
  
  public boolean zzahi()
  {
    return this.fT != -1;
  }
  
  public boolean zzahj()
  {
    return this.fT == 1;
  }
  
  public boolean zzahk()
  {
    return this.fU == 1;
  }
  
  public String zzfh(String paramString)
  {
    String str = (String)this.fV.get(paramString);
    if (str != null) {
      return str;
    }
    return paramString;
  }
  
  public String zzr(Activity paramActivity)
  {
    return zzfh(paramActivity.getClass().getCanonicalName());
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/analytics/internal/zzan.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */