package com.google.android.gms.analytics.internal;

import com.google.android.gms.analytics.zzi;
import com.google.android.gms.internal.zzms;

public class zzk
  extends zzd
{
  private final zzms bn = new zzms();
  
  zzk(zzf paramzzf)
  {
    super(paramzzf);
  }
  
  public zzms zzadg()
  {
    zzacj();
    return this.bn;
  }
  
  public void zzzc()
  {
    Object localObject = zzzh();
    String str = ((zzap)localObject).zzaae();
    if (str != null) {
      this.bn.setAppName(str);
    }
    localObject = ((zzap)localObject).zzaaf();
    if (localObject != null) {
      this.bn.setAppVersion((String)localObject);
    }
  }
  
  protected void zzzy()
  {
    zzacc().zzzv().zza(this.bn);
    zzzc();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/analytics/internal/zzk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */