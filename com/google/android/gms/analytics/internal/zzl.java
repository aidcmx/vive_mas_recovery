package com.google.android.gms.analytics.internal;

import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.text.TextUtils;
import android.util.Pair;
import com.google.android.gms.analytics.CampaignTrackingReceiver;
import com.google.android.gms.analytics.CampaignTrackingService;
import com.google.android.gms.analytics.zza;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.internal.zzms;
import com.google.android.gms.internal.zzmt;
import com.google.android.gms.internal.zzmw;
import com.google.android.gms.internal.zznb;
import com.google.android.gms.internal.zzsy;
import com.google.android.gms.internal.zzsz;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

class zzl
  extends zzd
{
  private final zzj dC;
  private final zzah dD;
  private final zzag dE;
  private final zzi dF;
  private long dG;
  private final zzt dH;
  private final zzt dI;
  private final zzal dJ;
  private long dK;
  private boolean dL;
  private boolean mStarted;
  
  protected zzl(zzf paramzzf, zzg paramzzg)
  {
    super(paramzzf);
    zzaa.zzy(paramzzg);
    this.dG = Long.MIN_VALUE;
    this.dE = paramzzg.zzk(paramzzf);
    this.dC = paramzzg.zzm(paramzzf);
    this.dD = paramzzg.zzn(paramzzf);
    this.dF = paramzzg.zzo(paramzzf);
    this.dJ = new zzal(zzabz());
    this.dH = new zzt(paramzzf)
    {
      public void run()
      {
        zzl.zza(zzl.this);
      }
    };
    this.dI = new zzt(paramzzf)
    {
      public void run()
      {
        zzl.zzb(zzl.this);
      }
    };
  }
  
  private void zza(zzh paramzzh, zzmt paramzzmt)
  {
    zzaa.zzy(paramzzh);
    zzaa.zzy(paramzzmt);
    Object localObject1 = new zza(zzabx());
    ((zza)localObject1).zzdr(paramzzh.zzacs());
    ((zza)localObject1).enableAdvertisingIdCollection(paramzzh.zzact());
    localObject1 = ((zza)localObject1).zzyu();
    zznb localzznb = (zznb)((com.google.android.gms.analytics.zze)localObject1).zzb(zznb.class);
    localzznb.zzeh("data");
    localzznb.zzat(true);
    ((com.google.android.gms.analytics.zze)localObject1).zza(paramzzmt);
    zzmw localzzmw = (zzmw)((com.google.android.gms.analytics.zze)localObject1).zzb(zzmw.class);
    zzms localzzms = (zzms)((com.google.android.gms.analytics.zze)localObject1).zzb(zzms.class);
    Iterator localIterator = paramzzh.zzmc().entrySet().iterator();
    while (localIterator.hasNext())
    {
      Object localObject2 = (Map.Entry)localIterator.next();
      String str = (String)((Map.Entry)localObject2).getKey();
      localObject2 = (String)((Map.Entry)localObject2).getValue();
      if ("an".equals(str)) {
        localzzms.setAppName((String)localObject2);
      } else if ("av".equals(str)) {
        localzzms.setAppVersion((String)localObject2);
      } else if ("aid".equals(str)) {
        localzzms.setAppId((String)localObject2);
      } else if ("aiid".equals(str)) {
        localzzms.setAppInstallerId((String)localObject2);
      } else if ("uid".equals(str)) {
        localzznb.setUserId((String)localObject2);
      } else {
        localzzmw.set(str, (String)localObject2);
      }
    }
    zzb("Sending installation campaign to", paramzzh.zzacs(), paramzzmt);
    ((com.google.android.gms.analytics.zze)localObject1).zzp(zzace().zzago());
    ((com.google.android.gms.analytics.zze)localObject1).zzzm();
  }
  
  private void zzadh()
  {
    zzzx();
    Context localContext = zzabx().getContext();
    if (!zzaj.zzat(localContext)) {
      zzev("AnalyticsReceiver is not registered or is disabled. Register the receiver for reliable dispatching on non-Google Play devices. See http://goo.gl/8Rd3yj for instructions.");
    }
    do
    {
      while (!CampaignTrackingReceiver.zzat(localContext))
      {
        zzev("CampaignTrackingReceiver is not registered, not exported or is disabled. Installation campaign tracking is not possible. See http://goo.gl/8Rd3yj for instructions.");
        return;
        if (!zzak.zzau(localContext)) {
          zzew("AnalyticsService is not registered or is disabled. Analytics service at risk of not starting. See http://goo.gl/8Rd3yj for instructions.");
        }
      }
    } while (CampaignTrackingService.zzau(localContext));
    zzev("CampaignTrackingService is not registered or is disabled. Installation campaign tracking is not possible. See http://goo.gl/8Rd3yj for instructions.");
  }
  
  private void zzadj()
  {
    zzb(new zzw()
    {
      public void zzf(Throwable paramAnonymousThrowable)
      {
        zzl.this.zzadp();
      }
    });
  }
  
  private void zzadk()
  {
    try
    {
      this.dC.zzadb();
      zzadp();
      zzt localzzt = this.dI;
      zzacb();
      localzzt.zzx(86400000L);
      return;
    }
    catch (SQLiteException localSQLiteException)
    {
      for (;;)
      {
        zzd("Failed to delete stale hits", localSQLiteException);
      }
    }
  }
  
  private boolean zzadq()
  {
    if (this.dL) {}
    do
    {
      return false;
      zzacb();
    } while (zzadw() <= 0L);
    return true;
  }
  
  private void zzadr()
  {
    zzv localzzv = zzacd();
    if (!localzzv.zzafn()) {}
    long l;
    do
    {
      do
      {
        return;
      } while (localzzv.zzfy());
      l = zzadc();
    } while ((l == 0L) || (Math.abs(zzabz().currentTimeMillis() - l) > zzacb().zzaeo()));
    zza("Dispatch alarm scheduled (ms)", Long.valueOf(zzacb().zzaen()));
    localzzv.schedule();
  }
  
  private void zzads()
  {
    zzadr();
    long l2 = zzadw();
    long l1 = zzace().zzagq();
    if (l1 != 0L)
    {
      l1 = l2 - Math.abs(zzabz().currentTimeMillis() - l1);
      if (l1 <= 0L) {}
    }
    for (;;)
    {
      zza("Dispatch scheduled (ms)", Long.valueOf(l1));
      if (!this.dH.zzfy()) {
        break;
      }
      l1 = Math.max(1L, l1 + this.dH.zzafk());
      this.dH.zzy(l1);
      return;
      l1 = Math.min(zzacb().zzael(), l2);
      continue;
      l1 = Math.min(zzacb().zzael(), l2);
    }
    this.dH.zzx(l1);
  }
  
  private void zzadt()
  {
    zzadu();
    zzadv();
  }
  
  private void zzadu()
  {
    if (this.dH.zzfy()) {
      zzes("All hits dispatched or no network/service. Going to power save mode");
    }
    this.dH.cancel();
  }
  
  private void zzadv()
  {
    zzv localzzv = zzacd();
    if (localzzv.zzfy()) {
      localzzv.cancel();
    }
  }
  
  private boolean zzez(String paramString)
  {
    return zzsz.zzco(getContext()).checkCallingOrSelfPermission(paramString) == 0;
  }
  
  protected void onServiceConnected()
  {
    zzzx();
    zzacb();
    zzadm();
  }
  
  void start()
  {
    zzacj();
    if (!this.mStarted) {}
    for (boolean bool = true;; bool = false)
    {
      zzaa.zza(bool, "Analytics backend already started");
      this.mStarted = true;
      zzacc().zzg(new Runnable()
      {
        public void run()
        {
          zzl.this.zzadi();
        }
      });
      return;
    }
  }
  
  /* Error */
  public long zza(zzh paramzzh, boolean paramBoolean)
  {
    // Byte code:
    //   0: aload_1
    //   1: invokestatic 42	com/google/android/gms/common/internal/zzaa:zzy	(Ljava/lang/Object;)Ljava/lang/Object;
    //   4: pop
    //   5: aload_0
    //   6: invokevirtual 434	com/google/android/gms/analytics/internal/zzl:zzacj	()V
    //   9: aload_0
    //   10: invokevirtual 245	com/google/android/gms/analytics/internal/zzl:zzzx	()V
    //   13: aload_0
    //   14: getfield 60	com/google/android/gms/analytics/internal/zzl:dC	Lcom/google/android/gms/analytics/internal/zzj;
    //   17: invokevirtual 456	com/google/android/gms/analytics/internal/zzj:beginTransaction	()V
    //   20: aload_0
    //   21: getfield 60	com/google/android/gms/analytics/internal/zzl:dC	Lcom/google/android/gms/analytics/internal/zzj;
    //   24: aload_1
    //   25: invokevirtual 459	com/google/android/gms/analytics/internal/zzh:zzacr	()J
    //   28: aload_1
    //   29: invokevirtual 462	com/google/android/gms/analytics/internal/zzh:zzze	()Ljava/lang/String;
    //   32: invokevirtual 465	com/google/android/gms/analytics/internal/zzj:zza	(JLjava/lang/String;)V
    //   35: aload_0
    //   36: getfield 60	com/google/android/gms/analytics/internal/zzl:dC	Lcom/google/android/gms/analytics/internal/zzj;
    //   39: aload_1
    //   40: invokevirtual 459	com/google/android/gms/analytics/internal/zzh:zzacr	()J
    //   43: aload_1
    //   44: invokevirtual 462	com/google/android/gms/analytics/internal/zzh:zzze	()Ljava/lang/String;
    //   47: aload_1
    //   48: invokevirtual 107	com/google/android/gms/analytics/internal/zzh:zzacs	()Ljava/lang/String;
    //   51: invokevirtual 468	com/google/android/gms/analytics/internal/zzj:zza	(JLjava/lang/String;Ljava/lang/String;)J
    //   54: lstore_3
    //   55: iload_2
    //   56: ifne +32 -> 88
    //   59: aload_1
    //   60: lload_3
    //   61: invokevirtual 471	com/google/android/gms/analytics/internal/zzh:zzr	(J)V
    //   64: aload_0
    //   65: getfield 60	com/google/android/gms/analytics/internal/zzl:dC	Lcom/google/android/gms/analytics/internal/zzj;
    //   68: aload_1
    //   69: invokevirtual 474	com/google/android/gms/analytics/internal/zzj:zzb	(Lcom/google/android/gms/analytics/internal/zzh;)V
    //   72: aload_0
    //   73: getfield 60	com/google/android/gms/analytics/internal/zzl:dC	Lcom/google/android/gms/analytics/internal/zzj;
    //   76: invokevirtual 477	com/google/android/gms/analytics/internal/zzj:setTransactionSuccessful	()V
    //   79: aload_0
    //   80: getfield 60	com/google/android/gms/analytics/internal/zzl:dC	Lcom/google/android/gms/analytics/internal/zzj;
    //   83: invokevirtual 480	com/google/android/gms/analytics/internal/zzj:endTransaction	()V
    //   86: lload_3
    //   87: lreturn
    //   88: aload_1
    //   89: lconst_1
    //   90: lload_3
    //   91: ladd
    //   92: invokevirtual 471	com/google/android/gms/analytics/internal/zzh:zzr	(J)V
    //   95: goto -31 -> 64
    //   98: astore_1
    //   99: aload_0
    //   100: ldc_w 482
    //   103: aload_1
    //   104: invokevirtual 485	com/google/android/gms/analytics/internal/zzl:zze	(Ljava/lang/String;Ljava/lang/Object;)V
    //   107: aload_0
    //   108: getfield 60	com/google/android/gms/analytics/internal/zzl:dC	Lcom/google/android/gms/analytics/internal/zzj;
    //   111: invokevirtual 480	com/google/android/gms/analytics/internal/zzj:endTransaction	()V
    //   114: ldc2_w 486
    //   117: lreturn
    //   118: astore_1
    //   119: aload_0
    //   120: ldc_w 489
    //   123: aload_1
    //   124: invokevirtual 485	com/google/android/gms/analytics/internal/zzl:zze	(Ljava/lang/String;Ljava/lang/Object;)V
    //   127: lload_3
    //   128: lreturn
    //   129: astore_1
    //   130: aload_0
    //   131: ldc_w 489
    //   134: aload_1
    //   135: invokevirtual 485	com/google/android/gms/analytics/internal/zzl:zze	(Ljava/lang/String;Ljava/lang/Object;)V
    //   138: goto -24 -> 114
    //   141: astore_1
    //   142: aload_0
    //   143: getfield 60	com/google/android/gms/analytics/internal/zzl:dC	Lcom/google/android/gms/analytics/internal/zzj;
    //   146: invokevirtual 480	com/google/android/gms/analytics/internal/zzj:endTransaction	()V
    //   149: aload_1
    //   150: athrow
    //   151: astore 5
    //   153: aload_0
    //   154: ldc_w 489
    //   157: aload 5
    //   159: invokevirtual 485	com/google/android/gms/analytics/internal/zzl:zze	(Ljava/lang/String;Ljava/lang/Object;)V
    //   162: goto -13 -> 149
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	165	0	this	zzl
    //   0	165	1	paramzzh	zzh
    //   0	165	2	paramBoolean	boolean
    //   54	74	3	l	long
    //   151	7	5	localSQLiteException	SQLiteException
    // Exception table:
    //   from	to	target	type
    //   13	55	98	android/database/sqlite/SQLiteException
    //   59	64	98	android/database/sqlite/SQLiteException
    //   64	79	98	android/database/sqlite/SQLiteException
    //   88	95	98	android/database/sqlite/SQLiteException
    //   79	86	118	android/database/sqlite/SQLiteException
    //   107	114	129	android/database/sqlite/SQLiteException
    //   13	55	141	finally
    //   59	64	141	finally
    //   64	79	141	finally
    //   88	95	141	finally
    //   99	107	141	finally
    //   142	149	151	android/database/sqlite/SQLiteException
  }
  
  public void zza(zzab paramzzab)
  {
    zzaa.zzy(paramzzab);
    com.google.android.gms.analytics.zzi.zzzx();
    zzacj();
    if (this.dL) {
      zzet("Hit delivery not possible. Missing network permissions. See http://goo.gl/8Rd3yj for instructions");
    }
    for (;;)
    {
      paramzzab = zzf(paramzzab);
      zzadl();
      if (!this.dF.zzb(paramzzab)) {
        break;
      }
      zzet("Hit sent to the device AnalyticsService for delivery");
      return;
      zza("Delivering hit", paramzzab);
    }
    zzacb();
    try
    {
      this.dC.zzc(paramzzab);
      zzadp();
      return;
    }
    catch (SQLiteException localSQLiteException)
    {
      zze("Delivery failed to save hit to a database", localSQLiteException);
      zzaca().zza(paramzzab, "deliver: failed to insert hit to database");
    }
  }
  
  public void zza(zzw paramzzw, long paramLong)
  {
    com.google.android.gms.analytics.zzi.zzzx();
    zzacj();
    long l1 = -1L;
    long l2 = zzace().zzagq();
    if (l2 != 0L) {
      l1 = Math.abs(zzabz().currentTimeMillis() - l2);
    }
    zzb("Dispatching local hits. Elapsed time since last dispatch (ms)", Long.valueOf(l1));
    zzacb();
    zzadl();
    try
    {
      zzadn();
      zzace().zzagr();
      zzadp();
      if (paramzzw != null) {
        paramzzw.zzf(null);
      }
      if (this.dK != paramLong) {
        this.dE.zzagj();
      }
      return;
    }
    catch (Throwable localThrowable)
    {
      do
      {
        zze("Local dispatch failed", localThrowable);
        zzace().zzagr();
        zzadp();
      } while (paramzzw == null);
      paramzzw.zzf(localThrowable);
    }
  }
  
  public void zzabr()
  {
    com.google.android.gms.analytics.zzi.zzzx();
    zzacj();
    zzacb();
    zzes("Delete all hits from local store");
    try
    {
      this.dC.zzacz();
      this.dC.zzada();
      zzadp();
      zzadl();
      if (this.dF.zzacv()) {
        zzes("Device service unavailable. Can't clear hits stored on the device service.");
      }
      return;
    }
    catch (SQLiteException localSQLiteException)
    {
      for (;;)
      {
        zzd("Failed to delete hits from store", localSQLiteException);
      }
    }
  }
  
  public void zzabu()
  {
    com.google.android.gms.analytics.zzi.zzzx();
    zzacj();
    zzes("Service disconnected");
  }
  
  void zzabw()
  {
    zzzx();
    this.dK = zzabz().currentTimeMillis();
  }
  
  public long zzadc()
  {
    com.google.android.gms.analytics.zzi.zzzx();
    zzacj();
    try
    {
      long l = this.dC.zzadc();
      return l;
    }
    catch (SQLiteException localSQLiteException)
    {
      zze("Failed to get min/max hit times from local store", localSQLiteException);
    }
    return 0L;
  }
  
  protected void zzadi()
  {
    zzacj();
    zzacb();
    zzadh();
    zzace().zzago();
    if (!zzez("android.permission.ACCESS_NETWORK_STATE"))
    {
      zzew("Missing required android.permission.ACCESS_NETWORK_STATE. Google Analytics disabled. See http://goo.gl/8Rd3yj for instructions");
      zzadx();
    }
    if (!zzez("android.permission.INTERNET"))
    {
      zzew("Missing required android.permission.INTERNET. Google Analytics disabled. See http://goo.gl/8Rd3yj for instructions");
      zzadx();
    }
    if (zzak.zzau(getContext())) {
      zzes("AnalyticsService registered in the app manifest and enabled");
    }
    for (;;)
    {
      if (!this.dL)
      {
        zzacb();
        if (!this.dC.isEmpty()) {
          zzadl();
        }
      }
      zzadp();
      return;
      zzacb();
      zzev("AnalyticsService not registered in the app manifest. Hits might not be delivered reliably. See http://goo.gl/8Rd3yj for instructions.");
    }
  }
  
  protected void zzadl()
  {
    if (this.dL) {}
    do
    {
      long l;
      do
      {
        do
        {
          return;
        } while ((!zzacb().zzaeg()) || (this.dF.isConnected()));
        l = zzacb().zzafb();
      } while (!this.dJ.zzz(l));
      this.dJ.start();
      zzes("Connecting to service");
    } while (!this.dF.connect());
    zzes("Connected to service");
    this.dJ.clear();
    onServiceConnected();
  }
  
  public void zzadm()
  {
    com.google.android.gms.analytics.zzi.zzzx();
    zzacj();
    zzaby();
    if (!zzacb().zzaeg()) {
      zzev("Service client disabled. Can't dispatch local hits to device AnalyticsService");
    }
    if (!this.dF.isConnected()) {
      zzes("Service not connected");
    }
    while (this.dC.isEmpty()) {
      return;
    }
    zzes("Dispatching local hits to device AnalyticsService");
    for (;;)
    {
      try
      {
        List localList = this.dC.zzt(zzacb().zzaep());
        if (!localList.isEmpty()) {
          break label126;
        }
        zzadp();
        return;
      }
      catch (SQLiteException localSQLiteException1)
      {
        zze("Failed to read hits from store", localSQLiteException1);
        zzadt();
        return;
      }
      label107:
      Object localObject;
      localSQLiteException1.remove(localObject);
      try
      {
        this.dC.zzu(((zzab)localObject).zzafz());
        label126:
        if (!localSQLiteException1.isEmpty())
        {
          localObject = (zzab)localSQLiteException1.get(0);
          if (this.dF.zzb((zzab)localObject)) {
            break label107;
          }
          zzadp();
          return;
        }
      }
      catch (SQLiteException localSQLiteException2)
      {
        zze("Failed to remove hit that was send for delivery", localSQLiteException2);
        zzadt();
      }
    }
  }
  
  protected boolean zzadn()
  {
    int j = 1;
    com.google.android.gms.analytics.zzi.zzzx();
    zzacj();
    zzes("Dispatching a batch of local hits");
    int i;
    if (!this.dF.isConnected())
    {
      zzacb();
      i = 1;
      if (this.dD.zzagk()) {
        break label65;
      }
    }
    for (;;)
    {
      if ((i == 0) || (j == 0)) {
        break label70;
      }
      zzes("No network or service available. Will retry later");
      return false;
      i = 0;
      break;
      label65:
      j = 0;
    }
    label70:
    long l3 = Math.max(zzacb().zzaep(), zzacb().zzaeq());
    ArrayList localArrayList = new ArrayList();
    l1 = 0L;
    for (;;)
    {
      try
      {
        this.dC.beginTransaction();
        localArrayList.clear();
        try
        {
          localList = this.dC.zzt(l3);
          if (localList.isEmpty())
          {
            zzes("Store is empty, nothing to dispatch");
            zzadt();
            try
            {
              this.dC.setTransactionSuccessful();
              this.dC.endTransaction();
              return false;
            }
            catch (SQLiteException localSQLiteException1)
            {
              zze("Failed to commit local dispatch transaction", localSQLiteException1);
              zzadt();
              return false;
            }
          }
          zza("Hits loaded from store. count", Integer.valueOf(localList.size()));
          localObject2 = localList.iterator();
          if (((Iterator)localObject2).hasNext())
          {
            if (((zzab)((Iterator)localObject2).next()).zzafz() != l1) {
              continue;
            }
            zzd("Database contains successfully uploaded hit", Long.valueOf(l1), Integer.valueOf(localList.size()));
            zzadt();
            try
            {
              this.dC.setTransactionSuccessful();
              this.dC.endTransaction();
              return false;
            }
            catch (SQLiteException localSQLiteException2)
            {
              zze("Failed to commit local dispatch transaction", localSQLiteException2);
              zzadt();
              return false;
            }
          }
          zzacb();
        }
        catch (SQLiteException localSQLiteException3)
        {
          zzd("Failed to read hits from persisted store", localSQLiteException3);
          zzadt();
          try
          {
            this.dC.setTransactionSuccessful();
            this.dC.endTransaction();
            return false;
          }
          catch (SQLiteException localSQLiteException4)
          {
            zze("Failed to commit local dispatch transaction", localSQLiteException4);
            zzadt();
            return false;
          }
          l2 = l1;
          if (!this.dF.isConnected()) {
            continue;
          }
        }
        zzes("Service connected, sending hits to the service");
        l2 = l1;
        if (localList.isEmpty()) {
          continue;
        }
        localObject2 = (zzab)localList.get(0);
        if (this.dF.zzb((zzab)localObject2)) {
          continue;
        }
      }
      finally
      {
        long l2;
        try
        {
          List localList;
          Object localObject2;
          this.dC.setTransactionSuccessful();
          this.dC.endTransaction();
          throw ((Throwable)localObject1);
        }
        catch (SQLiteException localSQLiteException11)
        {
          zze("Failed to commit local dispatch transaction", localSQLiteException11);
          zzadt();
          return false;
        }
        l1 = l2;
        continue;
      }
      l2 = l1;
      if (this.dD.zzagk())
      {
        localList = this.dD.zzt(localList);
        localObject2 = localList.iterator();
        if (((Iterator)localObject2).hasNext())
        {
          l1 = Math.max(l1, ((Long)((Iterator)localObject2).next()).longValue());
          continue;
          l1 = Math.max(l1, ((zzab)localObject2).zzafz());
          localList.remove(localObject2);
          zzb("Hit sent do device AnalyticsService for delivery", localObject2);
          try
          {
            this.dC.zzu(((zzab)localObject2).zzafz());
            localSQLiteException4.add(Long.valueOf(((zzab)localObject2).zzafz()));
          }
          catch (SQLiteException localSQLiteException5)
          {
            zze("Failed to remove hit that was send for delivery", localSQLiteException5);
            zzadt();
            try
            {
              this.dC.setTransactionSuccessful();
              this.dC.endTransaction();
              return false;
            }
            catch (SQLiteException localSQLiteException6)
            {
              zze("Failed to commit local dispatch transaction", localSQLiteException6);
              zzadt();
              return false;
            }
          }
        }
      }
      try
      {
        this.dC.zzr(localList);
        localSQLiteException6.addAll(localList);
        l2 = l1;
        boolean bool = localSQLiteException6.isEmpty();
        if (bool) {
          try
          {
            this.dC.setTransactionSuccessful();
            this.dC.endTransaction();
            return false;
          }
          catch (SQLiteException localSQLiteException7)
          {
            zze("Failed to commit local dispatch transaction", localSQLiteException7);
            zzadt();
            return false;
          }
        }
      }
      catch (SQLiteException localSQLiteException8)
      {
        zze("Failed to remove successfully uploaded hits", localSQLiteException8);
        zzadt();
        try
        {
          this.dC.setTransactionSuccessful();
          this.dC.endTransaction();
          return false;
        }
        catch (SQLiteException localSQLiteException9)
        {
          zze("Failed to commit local dispatch transaction", localSQLiteException9);
          zzadt();
          return false;
        }
        try
        {
          this.dC.setTransactionSuccessful();
          this.dC.endTransaction();
          l1 = l2;
        }
        catch (SQLiteException localSQLiteException10)
        {
          zze("Failed to commit local dispatch transaction", localSQLiteException10);
          zzadt();
          return false;
        }
      }
    }
  }
  
  public void zzado()
  {
    com.google.android.gms.analytics.zzi.zzzx();
    zzacj();
    zzet("Sync dispatching local hits");
    long l = this.dK;
    zzacb();
    zzadl();
    try
    {
      zzadn();
      zzace().zzagr();
      zzadp();
      if (this.dK != l) {
        this.dE.zzagj();
      }
      return;
    }
    catch (Throwable localThrowable)
    {
      zze("Sync local dispatch failed", localThrowable);
      zzadp();
    }
  }
  
  public void zzadp()
  {
    zzabx().zzzx();
    zzacj();
    if (!zzadq())
    {
      this.dE.unregister();
      zzadt();
      return;
    }
    if (this.dC.isEmpty())
    {
      this.dE.unregister();
      zzadt();
      return;
    }
    if (!((Boolean)zzy.eU.get()).booleanValue()) {
      this.dE.zzagh();
    }
    for (boolean bool = this.dE.isConnected(); bool; bool = true)
    {
      zzads();
      return;
    }
    zzadt();
    zzadr();
  }
  
  public long zzadw()
  {
    long l;
    if (this.dG != Long.MIN_VALUE) {
      l = this.dG;
    }
    do
    {
      return l;
      l = zzacb().zzaem();
    } while (!zzzh().zzafu());
    return zzzh().zzahl() * 1000L;
  }
  
  public void zzadx()
  {
    zzacj();
    zzzx();
    this.dL = true;
    this.dF.disconnect();
    zzadp();
  }
  
  public void zzaw(boolean paramBoolean)
  {
    zzadp();
  }
  
  public void zzb(zzw paramzzw)
  {
    zza(paramzzw, this.dK);
  }
  
  protected void zzc(zzh paramzzh)
  {
    zzzx();
    zzb("Sending first hit to property", paramzzh.zzacs());
    if (zzace().zzagp().zzz(zzacb().zzafi())) {}
    do
    {
      return;
      localObject = zzace().zzags();
    } while (TextUtils.isEmpty((CharSequence)localObject));
    Object localObject = zzao.zza(zzaca(), (String)localObject);
    zzb("Found relevant installation campaign", localObject);
    zza(paramzzh, (zzmt)localObject);
  }
  
  zzab zzf(zzab paramzzab)
  {
    if (!TextUtils.isEmpty(paramzzab.zzage())) {}
    do
    {
      return paramzzab;
      localObject2 = zzace().zzagt().zzagw();
    } while (localObject2 == null);
    Object localObject1 = (Long)((Pair)localObject2).second;
    Object localObject2 = (String)((Pair)localObject2).first;
    localObject1 = String.valueOf(localObject1);
    localObject1 = String.valueOf(localObject1).length() + 1 + String.valueOf(localObject2).length() + (String)localObject1 + ":" + (String)localObject2;
    localObject2 = new HashMap(paramzzab.zzmc());
    ((Map)localObject2).put("_m", localObject1);
    return zzab.zza(this, paramzzab, (Map)localObject2);
  }
  
  public void zzfa(String paramString)
  {
    zzaa.zzib(paramString);
    zzzx();
    zzaby();
    zzmt localzzmt = zzao.zza(zzaca(), paramString);
    if (localzzmt == null) {
      zzd("Parsing failed. Ignoring invalid campaign data", paramString);
    }
    for (;;)
    {
      return;
      String str = zzace().zzags();
      if (paramString.equals(str))
      {
        zzev("Ignoring duplicate install campaign");
        return;
      }
      if (!TextUtils.isEmpty(str))
      {
        zzd("Ignoring multiple install campaigns. original, new", str, paramString);
        return;
      }
      zzace().zzff(paramString);
      if (zzace().zzagp().zzz(zzacb().zzafi()))
      {
        zzd("Campaign received too late, ignoring", localzzmt);
        return;
      }
      zzb("Received installation campaign", localzzmt);
      paramString = this.dC.zzv(0L).iterator();
      while (paramString.hasNext()) {
        zza((zzh)paramString.next(), localzzmt);
      }
    }
  }
  
  public void zzw(long paramLong)
  {
    com.google.android.gms.analytics.zzi.zzzx();
    zzacj();
    long l = paramLong;
    if (paramLong < 0L) {
      l = 0L;
    }
    this.dG = l;
    zzadp();
  }
  
  protected void zzzy()
  {
    this.dC.initialize();
    this.dD.initialize();
    this.dF.initialize();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/analytics/internal/zzl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */