package com.google.android.gms.analytics.internal;

public enum zzm
{
  private zzm() {}
  
  public static zzm zzfb(String paramString)
  {
    if ("BATCH_BY_SESSION".equalsIgnoreCase(paramString)) {
      return dO;
    }
    if ("BATCH_BY_TIME".equalsIgnoreCase(paramString)) {
      return dP;
    }
    if ("BATCH_BY_BRUTE_FORCE".equalsIgnoreCase(paramString)) {
      return dQ;
    }
    if ("BATCH_BY_COUNT".equalsIgnoreCase(paramString)) {
      return dR;
    }
    if ("BATCH_BY_SIZE".equalsIgnoreCase(paramString)) {
      return dS;
    }
    return dN;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/analytics/internal/zzm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */