package com.google.android.gms.analytics.internal;

public enum zzo
{
  private zzo() {}
  
  public static zzo zzfc(String paramString)
  {
    if ("GZIP".equalsIgnoreCase(paramString)) {
      return dY;
    }
    return dX;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/analytics/internal/zzo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */