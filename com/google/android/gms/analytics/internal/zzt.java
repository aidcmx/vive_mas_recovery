package com.google.android.gms.analytics.internal;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.google.android.gms.analytics.zzi;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.util.zze;

abstract class zzt
{
  private static volatile Handler ef;
  private final zzf cQ;
  private volatile long eg;
  private final Runnable zzw;
  
  zzt(zzf paramzzf)
  {
    zzaa.zzy(paramzzf);
    this.cQ = paramzzf;
    this.zzw = new Runnable()
    {
      public void run()
      {
        if (Looper.myLooper() == Looper.getMainLooper()) {
          zzt.zza(zzt.this).zzacc().zzg(this);
        }
        boolean bool;
        do
        {
          return;
          bool = zzt.this.zzfy();
          zzt.zza(zzt.this, 0L);
        } while ((!bool) || (zzt.zzb(zzt.this)));
        zzt.this.run();
      }
    };
  }
  
  private Handler getHandler()
  {
    if (ef != null) {
      return ef;
    }
    try
    {
      if (ef == null) {
        ef = new Handler(this.cQ.getContext().getMainLooper());
      }
      Handler localHandler = ef;
      return localHandler;
    }
    finally {}
  }
  
  public void cancel()
  {
    this.eg = 0L;
    getHandler().removeCallbacks(this.zzw);
  }
  
  public abstract void run();
  
  public long zzafk()
  {
    if (this.eg == 0L) {
      return 0L;
    }
    return Math.abs(this.cQ.zzabz().currentTimeMillis() - this.eg);
  }
  
  public boolean zzfy()
  {
    return this.eg != 0L;
  }
  
  public void zzx(long paramLong)
  {
    cancel();
    if (paramLong >= 0L)
    {
      this.eg = this.cQ.zzabz().currentTimeMillis();
      if (!getHandler().postDelayed(this.zzw, paramLong)) {
        this.cQ.zzaca().zze("Failed to schedule delayed post. time", Long.valueOf(paramLong));
      }
    }
  }
  
  public void zzy(long paramLong)
  {
    long l = 0L;
    if (!zzfy()) {
      return;
    }
    if (paramLong < 0L)
    {
      cancel();
      return;
    }
    paramLong -= Math.abs(this.cQ.zzabz().currentTimeMillis() - this.eg);
    if (paramLong < 0L) {
      paramLong = l;
    }
    for (;;)
    {
      getHandler().removeCallbacks(this.zzw);
      if (getHandler().postDelayed(this.zzw, paramLong)) {
        break;
      }
      this.cQ.zzaca().zze("Failed to adjust delayed post. time", Long.valueOf(paramLong));
      return;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/analytics/internal/zzt.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */