package com.google.android.gms.analytics.internal;

import com.google.android.gms.analytics.zzi;
import com.google.android.gms.internal.zzmx;

public class zzu
  extends zzd
{
  zzu(zzf paramzzf)
  {
    super(paramzzf);
  }
  
  public zzmx zzafl()
  {
    zzacj();
    return zzacc().zzzw();
  }
  
  public String zzafm()
  {
    zzacj();
    zzmx localzzmx = zzafl();
    int i = localzzmx.zzaar();
    int j = localzzmx.zzaas();
    return 23 + i + "x" + j;
  }
  
  protected void zzzy() {}
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/analytics/internal/zzu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */