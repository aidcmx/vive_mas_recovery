package com.google.android.gms.analytics.internal;

public class zzz
  extends zzq<zzaa>
{
  public zzz(zzf paramzzf)
  {
    super(paramzzf, new zza(paramzzf));
  }
  
  private static class zza
    implements zzq.zza<zzaa>
  {
    private final zzf cQ;
    private final zzaa fe;
    
    public zza(zzf paramzzf)
    {
      this.cQ = paramzzf;
      this.fe = new zzaa();
    }
    
    public zzaa zzafp()
    {
      return this.fe;
    }
    
    public void zzd(String paramString, int paramInt)
    {
      if ("ga_dispatchPeriod".equals(paramString))
      {
        this.fe.fg = paramInt;
        return;
      }
      this.cQ.zzaca().zzd("Int xml configuration name not recognized", paramString);
    }
    
    public void zze(String paramString, boolean paramBoolean)
    {
      if ("ga_dryRun".equals(paramString))
      {
        paramString = this.fe;
        if (paramBoolean) {}
        for (int i = 1;; i = 0)
        {
          paramString.fh = i;
          return;
        }
      }
      this.cQ.zzaca().zzd("Bool xml configuration name not recognized", paramString);
    }
    
    public void zzo(String paramString1, String paramString2) {}
    
    public void zzp(String paramString1, String paramString2)
    {
      if ("ga_appName".equals(paramString1))
      {
        this.fe.bN = paramString2;
        return;
      }
      if ("ga_appVersion".equals(paramString1))
      {
        this.fe.bO = paramString2;
        return;
      }
      if ("ga_logLevel".equals(paramString1))
      {
        this.fe.ff = paramString2;
        return;
      }
      this.cQ.zzaca().zzd("String xml configuration name not recognized", paramString1);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/analytics/internal/zzz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */