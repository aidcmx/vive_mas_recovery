package com.google.android.gms.appdatasearch;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;

public class Feature
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<Feature> CREATOR = new zze();
  final Bundle gs;
  public final int id;
  final int mVersionCode;
  
  Feature(int paramInt1, int paramInt2, Bundle paramBundle)
  {
    this.mVersionCode = paramInt1;
    this.id = paramInt2;
    this.gs = paramBundle;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = false;
    boolean bool1 = bool2;
    if ((paramObject instanceof Feature))
    {
      paramObject = (Feature)paramObject;
      bool1 = bool2;
      if (zzz.equal(Integer.valueOf(((Feature)paramObject).id), Integer.valueOf(this.id)))
      {
        bool1 = bool2;
        if (zzz.equal(((Feature)paramObject).gs, this.gs)) {
          bool1 = true;
        }
      }
    }
    return bool1;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { Integer.valueOf(this.id), this.gs });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zze.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/appdatasearch/Feature.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */