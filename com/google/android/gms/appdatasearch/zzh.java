package com.google.android.gms.appdatasearch;

import java.util.HashMap;
import java.util.Map;

public class zzh
{
  private static final String[] gB;
  private static final Map<String, Integer> gC;
  
  static
  {
    int i = 0;
    gB = new String[] { "text1", "text2", "icon", "intent_action", "intent_data", "intent_data_id", "intent_extra_data", "suggest_large_icon", "intent_activity", "thing_proto" };
    gC = new HashMap(gB.length);
    while (i < gB.length)
    {
      gC.put(gB[i], Integer.valueOf(i));
      i += 1;
    }
  }
  
  public static int zzahq()
  {
    return gB.length;
  }
  
  public static String zzcn(int paramInt)
  {
    if ((paramInt < 0) || (paramInt >= gB.length)) {
      return null;
    }
    return gB[paramInt];
  }
  
  public static int zzfq(String paramString)
  {
    Integer localInteger = (Integer)gC.get(paramString);
    if (localInteger == null) {
      throw new IllegalArgumentException(String.valueOf(paramString).length() + 44 + "[" + paramString + "] is not a valid global search section name");
    }
    return localInteger.intValue();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/appdatasearch/zzh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */