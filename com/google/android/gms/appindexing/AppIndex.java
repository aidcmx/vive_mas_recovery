package com.google.android.gms.appindexing;

import com.google.android.gms.appdatasearch.zza;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.ApiOptions.NoOptions;
import com.google.android.gms.internal.zznk;

public final class AppIndex
{
  public static final Api<Api.ApiOptions.NoOptions> API = zza.gb;
  public static final Api<Api.ApiOptions.NoOptions> APP_INDEX_API = zza.gb;
  public static final AppIndexApi AppIndexApi = new zznk();
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/appindexing/AppIndex.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */