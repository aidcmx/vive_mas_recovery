package com.google.android.gms.auth;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zzz;

public class AccountChangeEvent
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<AccountChangeEvent> CREATOR = new zza();
  final int hA;
  final String hB;
  final long hx;
  final String hy;
  final int hz;
  final int mVersion;
  
  AccountChangeEvent(int paramInt1, long paramLong, String paramString1, int paramInt2, int paramInt3, String paramString2)
  {
    this.mVersion = paramInt1;
    this.hx = paramLong;
    this.hy = ((String)zzaa.zzy(paramString1));
    this.hz = paramInt2;
    this.hA = paramInt3;
    this.hB = paramString2;
  }
  
  public AccountChangeEvent(long paramLong, String paramString1, int paramInt1, int paramInt2, String paramString2)
  {
    this.mVersion = 1;
    this.hx = paramLong;
    this.hy = ((String)zzaa.zzy(paramString1));
    this.hz = paramInt1;
    this.hA = paramInt2;
    this.hB = paramString2;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    do
    {
      return true;
      if (!(paramObject instanceof AccountChangeEvent)) {
        break;
      }
      paramObject = (AccountChangeEvent)paramObject;
    } while ((this.mVersion == ((AccountChangeEvent)paramObject).mVersion) && (this.hx == ((AccountChangeEvent)paramObject).hx) && (zzz.equal(this.hy, ((AccountChangeEvent)paramObject).hy)) && (this.hz == ((AccountChangeEvent)paramObject).hz) && (this.hA == ((AccountChangeEvent)paramObject).hA) && (zzz.equal(this.hB, ((AccountChangeEvent)paramObject).hB)));
    return false;
    return false;
  }
  
  public String getAccountName()
  {
    return this.hy;
  }
  
  public String getChangeData()
  {
    return this.hB;
  }
  
  public int getChangeType()
  {
    return this.hz;
  }
  
  public int getEventIndex()
  {
    return this.hA;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { Integer.valueOf(this.mVersion), Long.valueOf(this.hx), this.hy, Integer.valueOf(this.hz), Integer.valueOf(this.hA), this.hB });
  }
  
  public String toString()
  {
    String str1 = "UNKNOWN";
    switch (this.hz)
    {
    }
    for (;;)
    {
      String str2 = this.hy;
      String str3 = this.hB;
      int i = this.hA;
      return String.valueOf(str2).length() + 91 + String.valueOf(str1).length() + String.valueOf(str3).length() + "AccountChangeEvent {accountName = " + str2 + ", changeType = " + str1 + ", changeData = " + str3 + ", eventIndex = " + i + "}";
      str1 = "ADDED";
      continue;
      str1 = "REMOVED";
      continue;
      str1 = "RENAMED_TO";
      continue;
      str1 = "RENAMED_FROM";
    }
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zza.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/auth/AccountChangeEvent.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */