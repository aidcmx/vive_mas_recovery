package com.google.android.gms.auth;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public class AccountChangeEventsRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<AccountChangeEventsRequest> CREATOR = new zzb();
  Account gj;
  int hA;
  @Deprecated
  String hy;
  final int mVersion;
  
  public AccountChangeEventsRequest()
  {
    this.mVersion = 1;
  }
  
  AccountChangeEventsRequest(int paramInt1, int paramInt2, String paramString, Account paramAccount)
  {
    this.mVersion = paramInt1;
    this.hA = paramInt2;
    this.hy = paramString;
    if ((paramAccount == null) && (!TextUtils.isEmpty(paramString)))
    {
      this.gj = new Account(paramString, "com.google");
      return;
    }
    this.gj = paramAccount;
  }
  
  public Account getAccount()
  {
    return this.gj;
  }
  
  @Deprecated
  public String getAccountName()
  {
    return this.hy;
  }
  
  public int getEventIndex()
  {
    return this.hA;
  }
  
  public AccountChangeEventsRequest setAccount(Account paramAccount)
  {
    this.gj = paramAccount;
    return this;
  }
  
  @Deprecated
  public AccountChangeEventsRequest setAccountName(String paramString)
  {
    this.hy = paramString;
    return this;
  }
  
  public AccountChangeEventsRequest setEventIndex(int paramInt)
  {
    this.hA = paramInt;
    return this;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzb.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/auth/AccountChangeEventsRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */