package com.google.android.gms.auth;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zzz;
import java.util.List;

public class TokenData
  extends AbstractSafeParcelable
  implements ReflectedParcelable
{
  public static final Parcelable.Creator<TokenData> CREATOR = new zzf();
  private final String hN;
  private final Long hO;
  private final boolean hP;
  private final boolean hQ;
  private final List<String> hR;
  final int mVersionCode;
  
  TokenData(int paramInt, String paramString, Long paramLong, boolean paramBoolean1, boolean paramBoolean2, List<String> paramList)
  {
    this.mVersionCode = paramInt;
    this.hN = zzaa.zzib(paramString);
    this.hO = paramLong;
    this.hP = paramBoolean1;
    this.hQ = paramBoolean2;
    this.hR = paramList;
  }
  
  @Nullable
  public static TokenData zzd(Bundle paramBundle, String paramString)
  {
    paramBundle.setClassLoader(TokenData.class.getClassLoader());
    paramBundle = paramBundle.getBundle(paramString);
    if (paramBundle == null) {
      return null;
    }
    paramBundle.setClassLoader(TokenData.class.getClassLoader());
    return (TokenData)paramBundle.getParcelable("TokenData");
  }
  
  public boolean equals(Object paramObject)
  {
    if (!(paramObject instanceof TokenData)) {}
    do
    {
      return false;
      paramObject = (TokenData)paramObject;
    } while ((!TextUtils.equals(this.hN, ((TokenData)paramObject).hN)) || (!zzz.equal(this.hO, ((TokenData)paramObject).hO)) || (this.hP != ((TokenData)paramObject).hP) || (this.hQ != ((TokenData)paramObject).hQ) || (!zzz.equal(this.hR, ((TokenData)paramObject).hR)));
    return true;
  }
  
  public String getToken()
  {
    return this.hN;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.hN, this.hO, Boolean.valueOf(this.hP), Boolean.valueOf(this.hQ), this.hR });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzf.zza(this, paramParcel, paramInt);
  }
  
  @Nullable
  public Long zzahy()
  {
    return this.hO;
  }
  
  public boolean zzahz()
  {
    return this.hP;
  }
  
  public boolean zzaia()
  {
    return this.hQ;
  }
  
  @Nullable
  public List<String> zzaib()
  {
    return this.hR;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/auth/TokenData.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */