package com.google.android.gms.auth;

public class UserRecoverableNotifiedException
  extends GoogleAuthException
{
  public UserRecoverableNotifiedException(String paramString)
  {
    super(paramString);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/auth/UserRecoverableNotifiedException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */