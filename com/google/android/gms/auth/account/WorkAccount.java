package com.google.android.gms.auth.account;

import android.content.Context;
import android.os.Looper;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.ApiOptions.NoOptions;
import com.google.android.gms.common.api.Api.zza;
import com.google.android.gms.common.api.Api.zzf;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.internal.zznq;
import com.google.android.gms.internal.zznr;

public class WorkAccount
{
  public static final Api<Api.ApiOptions.NoOptions> API = new Api("WorkAccount.API", hh, hg);
  public static final WorkAccountApi WorkAccountApi = new zznq();
  private static final Api.zzf<zznr> hg = new Api.zzf();
  private static final Api.zza<zznr, Api.ApiOptions.NoOptions> hh = new Api.zza()
  {
    public zznr zzc(Context paramAnonymousContext, Looper paramAnonymousLooper, zzf paramAnonymouszzf, Api.ApiOptions.NoOptions paramAnonymousNoOptions, GoogleApiClient.ConnectionCallbacks paramAnonymousConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramAnonymousOnConnectionFailedListener)
    {
      return new zznr(paramAnonymousContext, paramAnonymousLooper, paramAnonymouszzf, paramAnonymousConnectionCallbacks, paramAnonymousOnConnectionFailedListener);
    }
  };
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/auth/account/WorkAccount.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */