package com.google.android.gms.auth.account;

import android.accounts.Account;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;

public abstract interface WorkAccountApi
{
  public abstract PendingResult<AddAccountResult> addWorkAccount(GoogleApiClient paramGoogleApiClient, String paramString);
  
  public abstract PendingResult<Result> removeWorkAccount(GoogleApiClient paramGoogleApiClient, Account paramAccount);
  
  public abstract void setWorkAuthenticatorEnabled(GoogleApiClient paramGoogleApiClient, boolean paramBoolean);
  
  public static abstract interface AddAccountResult
    extends Result
  {
    public abstract Account getAccount();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/auth/account/WorkAccountApi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */