package com.google.android.gms.auth.api;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.auth.api.credentials.CredentialsApi;
import com.google.android.gms.auth.api.credentials.PasswordSpecification;
import com.google.android.gms.auth.api.credentials.internal.zze;
import com.google.android.gms.auth.api.credentials.internal.zzg;
import com.google.android.gms.auth.api.proxy.ProxyApi;
import com.google.android.gms.auth.api.signin.GoogleSignInApi;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.internal.zzc;
import com.google.android.gms.auth.api.signin.internal.zzd;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.ApiOptions.NoOptions;
import com.google.android.gms.common.api.Api.ApiOptions.Optional;
import com.google.android.gms.common.api.Api.zza;
import com.google.android.gms.common.api.Api.zzf;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.internal.zzns;
import com.google.android.gms.internal.zznt;
import com.google.android.gms.internal.zznu;
import com.google.android.gms.internal.zzoc;
import java.util.Collections;
import java.util.List;

public final class Auth
{
  public static final Api<AuthCredentialsOptions> CREDENTIALS_API;
  public static final CredentialsApi CredentialsApi;
  public static final Api<GoogleSignInOptions> GOOGLE_SIGN_IN_API;
  public static final GoogleSignInApi GoogleSignInApi = new zzc();
  public static final Api<zzb> PROXY_API;
  public static final ProxyApi ProxyApi;
  public static final Api.zzf<zzg> hX = new Api.zzf();
  public static final Api.zzf<zznu> hY = new Api.zzf();
  public static final Api.zzf<zzd> hZ = new Api.zzf();
  private static final Api.zza<zzg, AuthCredentialsOptions> ia = new Api.zza()
  {
    public zzg zza(Context paramAnonymousContext, Looper paramAnonymousLooper, zzf paramAnonymouszzf, Auth.AuthCredentialsOptions paramAnonymousAuthCredentialsOptions, GoogleApiClient.ConnectionCallbacks paramAnonymousConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramAnonymousOnConnectionFailedListener)
    {
      return new zzg(paramAnonymousContext, paramAnonymousLooper, paramAnonymouszzf, paramAnonymousAuthCredentialsOptions, paramAnonymousConnectionCallbacks, paramAnonymousOnConnectionFailedListener);
    }
  };
  private static final Api.zza<zznu, Api.ApiOptions.NoOptions> ib = new Api.zza()
  {
    public zznu zzd(Context paramAnonymousContext, Looper paramAnonymousLooper, zzf paramAnonymouszzf, Api.ApiOptions.NoOptions paramAnonymousNoOptions, GoogleApiClient.ConnectionCallbacks paramAnonymousConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramAnonymousOnConnectionFailedListener)
    {
      return new zznu(paramAnonymousContext, paramAnonymousLooper, paramAnonymouszzf, paramAnonymousConnectionCallbacks, paramAnonymousOnConnectionFailedListener);
    }
  };
  private static final Api.zza<zzd, GoogleSignInOptions> ic = new Api.zza()
  {
    public zzd zza(Context paramAnonymousContext, Looper paramAnonymousLooper, zzf paramAnonymouszzf, @Nullable GoogleSignInOptions paramAnonymousGoogleSignInOptions, GoogleApiClient.ConnectionCallbacks paramAnonymousConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramAnonymousOnConnectionFailedListener)
    {
      return new zzd(paramAnonymousContext, paramAnonymousLooper, paramAnonymouszzf, paramAnonymousGoogleSignInOptions, paramAnonymousConnectionCallbacks, paramAnonymousOnConnectionFailedListener);
    }
    
    public List<Scope> zza(@Nullable GoogleSignInOptions paramAnonymousGoogleSignInOptions)
    {
      if (paramAnonymousGoogleSignInOptions == null) {
        return Collections.emptyList();
      }
      return paramAnonymousGoogleSignInOptions.zzait();
    }
  };
  public static final Api<Api.ApiOptions.NoOptions> ie;
  public static final zzns jdField_if;
  
  static
  {
    PROXY_API = zza.API;
    CREDENTIALS_API = new Api("Auth.CREDENTIALS_API", ia, hX);
    GOOGLE_SIGN_IN_API = new Api("Auth.GOOGLE_SIGN_IN_API", ic, hZ);
    ie = new Api("Auth.ACCOUNT_STATUS_API", ib, hY);
    ProxyApi = new zzoc();
    CredentialsApi = new zze();
    if = new zznt();
  }
  
  public static final class AuthCredentialsOptions
    implements Api.ApiOptions.Optional
  {
    private final String ig;
    private final PasswordSpecification ih;
    
    public Bundle zzahv()
    {
      Bundle localBundle = new Bundle();
      localBundle.putString("consumer_package", this.ig);
      localBundle.putParcelable("password_specification", this.ih);
      return localBundle;
    }
    
    public PasswordSpecification zzaid()
    {
      return this.ih;
    }
    
    public static class Builder
    {
      @NonNull
      private PasswordSpecification ih = PasswordSpecification.iG;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/auth/api/Auth.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */