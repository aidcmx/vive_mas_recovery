package com.google.android.gms.auth.api.credentials;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.android.gms.auth.api.credentials.internal.zzb;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zzz;
import java.util.Collections;
import java.util.List;

public class Credential
  extends AbstractSafeParcelable
  implements ReflectedParcelable
{
  public static final Parcelable.Creator<Credential> CREATOR = new zza();
  public static final String EXTRA_KEY = "com.google.android.gms.credentials.Credential";
  @Nullable
  private final Uri il;
  private final List<IdToken> im;
  @Nullable
  private final String io;
  @Nullable
  private final String ip;
  @Nullable
  private final String iq;
  @Nullable
  private final String ir;
  @Nullable
  private final String is;
  @Nullable
  private final String it;
  @Nullable
  private final String mName;
  final int mVersionCode;
  private final String zzboa;
  
  Credential(int paramInt, String paramString1, String paramString2, Uri paramUri, List<IdToken> paramList, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8)
  {
    this.mVersionCode = paramInt;
    paramString1 = ((String)zzaa.zzb(paramString1, "credential identifier cannot be null")).trim();
    zzaa.zzh(paramString1, "credential identifier cannot be empty");
    this.zzboa = paramString1;
    paramString1 = paramString2;
    if (paramString2 != null)
    {
      paramString1 = paramString2;
      if (TextUtils.isEmpty(paramString2.trim())) {
        paramString1 = null;
      }
    }
    this.mName = paramString1;
    this.il = paramUri;
    if (paramList == null) {}
    for (paramString1 = Collections.emptyList();; paramString1 = Collections.unmodifiableList(paramList))
    {
      this.im = paramString1;
      this.io = paramString3;
      if ((paramString3 == null) || (!paramString3.isEmpty())) {
        break;
      }
      throw new IllegalArgumentException("password cannot be empty");
    }
    if (!TextUtils.isEmpty(paramString4)) {
      zzb.zzfx(paramString4);
    }
    this.ip = paramString4;
    this.iq = paramString5;
    this.ir = paramString6;
    this.is = paramString7;
    this.it = paramString8;
    if ((!TextUtils.isEmpty(this.io)) && (!TextUtils.isEmpty(this.ip))) {
      throw new IllegalStateException("password and accountType cannot both be set");
    }
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    do
    {
      return true;
      if (!(paramObject instanceof Credential)) {
        return false;
      }
      paramObject = (Credential)paramObject;
    } while ((TextUtils.equals(this.zzboa, ((Credential)paramObject).zzboa)) && (TextUtils.equals(this.mName, ((Credential)paramObject).mName)) && (zzz.equal(this.il, ((Credential)paramObject).il)) && (TextUtils.equals(this.io, ((Credential)paramObject).io)) && (TextUtils.equals(this.ip, ((Credential)paramObject).ip)) && (TextUtils.equals(this.iq, ((Credential)paramObject).iq)));
    return false;
  }
  
  @Nullable
  public String getAccountType()
  {
    return this.ip;
  }
  
  @Nullable
  public String getFamilyName()
  {
    return this.it;
  }
  
  @Nullable
  public String getGeneratedPassword()
  {
    return this.iq;
  }
  
  @Nullable
  public String getGivenName()
  {
    return this.is;
  }
  
  public String getId()
  {
    return this.zzboa;
  }
  
  public List<IdToken> getIdTokens()
  {
    return this.im;
  }
  
  @Nullable
  public String getName()
  {
    return this.mName;
  }
  
  @Nullable
  public String getPassword()
  {
    return this.io;
  }
  
  @Nullable
  public Uri getProfilePictureUri()
  {
    return this.il;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.zzboa, this.mName, this.il, this.io, this.ip, this.iq });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zza.zza(this, paramParcel, paramInt);
  }
  
  public String zzaif()
  {
    return this.ir;
  }
  
  public static class Builder
  {
    private Uri il;
    private List<IdToken> im;
    private String io;
    private String ip;
    private String iq;
    private String ir;
    private String is;
    private String it;
    private String mName;
    private final String zzboa;
    
    public Builder(Credential paramCredential)
    {
      this.zzboa = Credential.zza(paramCredential);
      this.mName = Credential.zzb(paramCredential);
      this.il = Credential.zzc(paramCredential);
      this.im = Credential.zzd(paramCredential);
      this.io = Credential.zze(paramCredential);
      this.ip = Credential.zzf(paramCredential);
      this.iq = Credential.zzg(paramCredential);
      this.ir = Credential.zzh(paramCredential);
      this.is = Credential.zzi(paramCredential);
      this.it = Credential.zzj(paramCredential);
    }
    
    public Builder(String paramString)
    {
      this.zzboa = paramString;
    }
    
    public Credential build()
    {
      return new Credential(4, this.zzboa, this.mName, this.il, this.im, this.io, this.ip, this.iq, this.ir, this.is, this.it);
    }
    
    public Builder setAccountType(String paramString)
    {
      this.ip = paramString;
      return this;
    }
    
    public Builder setName(String paramString)
    {
      this.mName = paramString;
      return this;
    }
    
    public Builder setPassword(String paramString)
    {
      this.io = paramString;
      return this;
    }
    
    public Builder setProfilePictureUri(Uri paramUri)
    {
      this.il = paramUri;
      return this;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/auth/api/credentials/Credential.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */