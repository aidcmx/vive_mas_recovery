package com.google.android.gms.auth.api.credentials;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public final class CredentialPickerConfig
  extends AbstractSafeParcelable
  implements ReflectedParcelable
{
  public static final Parcelable.Creator<CredentialPickerConfig> CREATOR = new zzb();
  private final boolean iu;
  @Deprecated
  private final boolean iv;
  private final int iw;
  private final boolean mShowCancelButton;
  final int mVersionCode;
  
  CredentialPickerConfig(int paramInt1, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, int paramInt2)
  {
    this.mVersionCode = paramInt1;
    this.iu = paramBoolean1;
    this.mShowCancelButton = paramBoolean2;
    if (paramInt1 < 2)
    {
      this.iv = paramBoolean3;
      if (paramBoolean3) {}
      for (paramInt1 = i;; paramInt1 = 1)
      {
        this.iw = paramInt1;
        return;
      }
    }
    if (paramInt2 == 3) {}
    for (paramBoolean1 = bool;; paramBoolean1 = false)
    {
      this.iv = paramBoolean1;
      this.iw = paramInt2;
      return;
    }
  }
  
  private CredentialPickerConfig(Builder paramBuilder)
  {
    this(2, Builder.zza(paramBuilder), Builder.zzb(paramBuilder), false, Builder.zzc(paramBuilder));
  }
  
  @Deprecated
  public boolean isForNewAccount()
  {
    return this.iw == 3;
  }
  
  public boolean shouldShowAddAccountButton()
  {
    return this.iu;
  }
  
  public boolean shouldShowCancelButton()
  {
    return this.mShowCancelButton;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzb.zza(this, paramParcel, paramInt);
  }
  
  int zzaig()
  {
    return this.iw;
  }
  
  public static class Builder
  {
    private boolean iu = false;
    private int ix = 1;
    private boolean mShowCancelButton = true;
    
    public CredentialPickerConfig build()
    {
      return new CredentialPickerConfig(this, null);
    }
    
    @Deprecated
    public Builder setForNewAccount(boolean paramBoolean)
    {
      if (paramBoolean) {}
      for (int i = 3;; i = 1)
      {
        this.ix = i;
        return this;
      }
    }
    
    public Builder setPrompt(int paramInt)
    {
      this.ix = paramInt;
      return this;
    }
    
    public Builder setShowAddAccountButton(boolean paramBoolean)
    {
      this.iu = paramBoolean;
      return this;
    }
    
    public Builder setShowCancelButton(boolean paramBoolean)
    {
      this.mShowCancelButton = paramBoolean;
      return this;
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface Prompt
  {
    public static final int CONTINUE = 1;
    public static final int SIGN_IN = 2;
    public static final int SIGN_UP = 3;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/auth/api/credentials/CredentialPickerConfig.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */