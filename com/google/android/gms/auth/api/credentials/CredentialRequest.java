package com.google.android.gms.auth.api.credentials;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;

public final class CredentialRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<CredentialRequest> CREATOR = new zzc();
  private final CredentialPickerConfig iA;
  private final CredentialPickerConfig iB;
  private final boolean iy;
  private final String[] iz;
  final int mVersionCode;
  
  CredentialRequest(int paramInt, boolean paramBoolean, String[] paramArrayOfString, CredentialPickerConfig paramCredentialPickerConfig1, CredentialPickerConfig paramCredentialPickerConfig2)
  {
    this.mVersionCode = paramInt;
    this.iy = paramBoolean;
    this.iz = ((String[])zzaa.zzy(paramArrayOfString));
    paramArrayOfString = paramCredentialPickerConfig1;
    if (paramCredentialPickerConfig1 == null) {
      paramArrayOfString = new CredentialPickerConfig.Builder().build();
    }
    this.iA = paramArrayOfString;
    paramArrayOfString = paramCredentialPickerConfig2;
    if (paramCredentialPickerConfig2 == null) {
      paramArrayOfString = new CredentialPickerConfig.Builder().build();
    }
    this.iB = paramArrayOfString;
  }
  
  private CredentialRequest(Builder paramBuilder)
  {
    this(2, Builder.zza(paramBuilder), Builder.zzb(paramBuilder), Builder.zzc(paramBuilder), Builder.zzd(paramBuilder));
  }
  
  @NonNull
  public String[] getAccountTypes()
  {
    return this.iz;
  }
  
  @NonNull
  public CredentialPickerConfig getCredentialHintPickerConfig()
  {
    return this.iB;
  }
  
  @NonNull
  public CredentialPickerConfig getCredentialPickerConfig()
  {
    return this.iA;
  }
  
  @Deprecated
  public boolean getSupportsPasswordLogin()
  {
    return isPasswordLoginSupported();
  }
  
  public boolean isPasswordLoginSupported()
  {
    return this.iy;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzc.zza(this, paramParcel, paramInt);
  }
  
  public static final class Builder
  {
    private CredentialPickerConfig iA;
    private CredentialPickerConfig iB;
    private boolean iy;
    private String[] iz;
    
    public CredentialRequest build()
    {
      if (this.iz == null) {
        this.iz = new String[0];
      }
      if ((!this.iy) && (this.iz.length == 0)) {
        throw new IllegalStateException("At least one authentication method must be specified");
      }
      return new CredentialRequest(this, null);
    }
    
    public Builder setAccountTypes(String... paramVarArgs)
    {
      String[] arrayOfString = paramVarArgs;
      if (paramVarArgs == null) {
        arrayOfString = new String[0];
      }
      this.iz = arrayOfString;
      return this;
    }
    
    public Builder setCredentialHintPickerConfig(CredentialPickerConfig paramCredentialPickerConfig)
    {
      this.iB = paramCredentialPickerConfig;
      return this;
    }
    
    public Builder setCredentialPickerConfig(CredentialPickerConfig paramCredentialPickerConfig)
    {
      this.iA = paramCredentialPickerConfig;
      return this;
    }
    
    public Builder setPasswordLoginSupported(boolean paramBoolean)
    {
      this.iy = paramBoolean;
      return this;
    }
    
    @Deprecated
    public Builder setSupportsPasswordLogin(boolean paramBoolean)
    {
      return setPasswordLoginSupported(paramBoolean);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/auth/api/credentials/CredentialRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */