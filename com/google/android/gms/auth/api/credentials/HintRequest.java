package com.google.android.gms.auth.api.credentials;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;

public final class HintRequest
  extends AbstractSafeParcelable
  implements ReflectedParcelable
{
  public static final Parcelable.Creator<HintRequest> CREATOR = new zzd();
  private final CredentialPickerConfig iC;
  private final boolean iD;
  private final boolean iE;
  private final String[] iz;
  final int mVersionCode;
  
  HintRequest(int paramInt, CredentialPickerConfig paramCredentialPickerConfig, boolean paramBoolean1, boolean paramBoolean2, String[] paramArrayOfString)
  {
    this.mVersionCode = paramInt;
    this.iC = ((CredentialPickerConfig)zzaa.zzy(paramCredentialPickerConfig));
    this.iD = paramBoolean1;
    this.iE = paramBoolean2;
    this.iz = ((String[])zzaa.zzy(paramArrayOfString));
  }
  
  private HintRequest(Builder paramBuilder)
  {
    this(1, Builder.zza(paramBuilder), Builder.zzb(paramBuilder), Builder.zzc(paramBuilder), Builder.zzd(paramBuilder));
  }
  
  @NonNull
  public String[] getAccountTypes()
  {
    return this.iz;
  }
  
  @NonNull
  public CredentialPickerConfig getHintPickerConfig()
  {
    return this.iC;
  }
  
  public boolean isEmailAddressIdentifierSupported()
  {
    return this.iD;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzd.zza(this, paramParcel, paramInt);
  }
  
  public boolean zzaih()
  {
    return this.iE;
  }
  
  public static final class Builder
  {
    private CredentialPickerConfig iC = new CredentialPickerConfig.Builder().build();
    private boolean iD;
    private boolean iE;
    private String[] iz;
    
    public HintRequest build()
    {
      if (this.iz == null) {
        this.iz = new String[0];
      }
      if ((!this.iD) && (!this.iE) && (this.iz.length == 0)) {
        throw new IllegalStateException("At least one authentication method must be specified");
      }
      return new HintRequest(this, null);
    }
    
    public Builder setAccountTypes(String... paramVarArgs)
    {
      String[] arrayOfString = paramVarArgs;
      if (paramVarArgs == null) {
        arrayOfString = new String[0];
      }
      this.iz = arrayOfString;
      return this;
    }
    
    public Builder setEmailAddressIdentifierSupported(boolean paramBoolean)
    {
      this.iD = paramBoolean;
      return this;
    }
    
    public Builder setHintPickerConfig(@NonNull CredentialPickerConfig paramCredentialPickerConfig)
    {
      this.iC = ((CredentialPickerConfig)zzaa.zzy(paramCredentialPickerConfig));
      return this;
    }
    
    public Builder setPhoneNumberIdentifierSupported(boolean paramBoolean)
    {
      this.iE = paramBoolean;
      return this;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/auth/api/credentials/HintRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */