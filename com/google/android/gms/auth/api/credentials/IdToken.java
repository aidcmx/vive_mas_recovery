package com.google.android.gms.auth.api.credentials;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.google.android.gms.auth.api.credentials.internal.zzb;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;

public final class IdToken
  extends AbstractSafeParcelable
  implements ReflectedParcelable
{
  public static final Parcelable.Creator<IdToken> CREATOR = new zze();
  @NonNull
  private final String iF;
  @NonNull
  private final String ip;
  final int mVersionCode;
  
  IdToken(int paramInt, @NonNull String paramString1, @NonNull String paramString2)
  {
    zzb.zzfx(paramString1);
    if (!TextUtils.isEmpty(paramString2)) {}
    for (boolean bool = true;; bool = false)
    {
      zzaa.zzb(bool, "id token string cannot be null or empty");
      this.mVersionCode = paramInt;
      this.ip = paramString1;
      this.iF = paramString2;
      return;
    }
  }
  
  public IdToken(@NonNull String paramString1, @NonNull String paramString2)
  {
    this(1, paramString1, paramString2);
  }
  
  @NonNull
  public String getAccountType()
  {
    return this.ip;
  }
  
  @NonNull
  public String getIdToken()
  {
    return this.iF;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zze.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/auth/api/credentials/IdToken.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */