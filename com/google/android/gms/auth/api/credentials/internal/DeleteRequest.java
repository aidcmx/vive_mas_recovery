package com.google.android.gms.auth.api.credentials.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public final class DeleteRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<DeleteRequest> CREATOR = new zzh();
  private final Credential iP;
  final int mVersionCode;
  
  DeleteRequest(int paramInt, Credential paramCredential)
  {
    this.mVersionCode = paramInt;
    this.iP = paramCredential;
  }
  
  public DeleteRequest(Credential paramCredential)
  {
    this(1, paramCredential);
  }
  
  public Credential getCredential()
  {
    return this.iP;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzh.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/auth/api/credentials/internal/DeleteRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */