package com.google.android.gms.auth.api.credentials.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.auth.api.credentials.PasswordSpecification;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public final class GeneratePasswordRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<GeneratePasswordRequest> CREATOR = new zzi();
  private final PasswordSpecification ih;
  final int mVersionCode;
  
  GeneratePasswordRequest(int paramInt, PasswordSpecification paramPasswordSpecification)
  {
    this.mVersionCode = paramInt;
    this.ih = paramPasswordSpecification;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzi.zza(this, paramParcel, paramInt);
  }
  
  public PasswordSpecification zzaid()
  {
    return this.ih;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/auth/api/credentials/internal/GeneratePasswordRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */