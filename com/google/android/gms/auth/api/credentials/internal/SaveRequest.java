package com.google.android.gms.auth.api.credentials.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public final class SaveRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<SaveRequest> CREATOR = new zzl();
  private final Credential iP;
  final int mVersionCode;
  
  SaveRequest(int paramInt, Credential paramCredential)
  {
    this.mVersionCode = paramInt;
    this.iP = paramCredential;
  }
  
  public SaveRequest(Credential paramCredential)
  {
    this(1, paramCredential);
  }
  
  public Credential getCredential()
  {
    return this.iP;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzl.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/auth/api/credentials/internal/SaveRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */