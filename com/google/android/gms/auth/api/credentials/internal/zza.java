package com.google.android.gms.auth.api.credentials.internal;

import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.common.api.Status;

public class zza
  extends zzj.zza
{
  public void zza(Status paramStatus, Credential paramCredential)
  {
    throw new UnsupportedOperationException();
  }
  
  public void zza(Status paramStatus, String paramString)
  {
    throw new UnsupportedOperationException();
  }
  
  public void zzh(Status paramStatus)
  {
    throw new UnsupportedOperationException();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/auth/api/credentials/internal/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */