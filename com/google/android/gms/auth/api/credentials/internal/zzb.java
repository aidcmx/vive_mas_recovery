package com.google.android.gms.auth.api.credentials.internal;

import android.net.Uri;
import android.text.TextUtils;
import com.google.android.gms.common.internal.zzaa;

public class zzb
{
  public static String zzfx(String paramString)
  {
    boolean bool2 = false;
    if (!TextUtils.isEmpty(paramString)) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      zzaa.zzb(bool1, "account type cannot be empty");
      String str = Uri.parse(paramString).getScheme();
      if (!"http".equalsIgnoreCase(str))
      {
        bool1 = bool2;
        if (!"https".equalsIgnoreCase(str)) {}
      }
      else
      {
        bool1 = true;
      }
      zzaa.zzb(bool1, "Account type must be an http or https URI");
      return paramString;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/auth/api/credentials/internal/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */