package com.google.android.gms.auth.api.credentials.internal;

import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.auth.api.credentials.CredentialRequestResult;
import com.google.android.gms.common.api.Status;

public final class zzd
  implements CredentialRequestResult
{
  private final Status hv;
  private final Credential iP;
  
  public zzd(Status paramStatus, Credential paramCredential)
  {
    this.hv = paramStatus;
    this.iP = paramCredential;
  }
  
  public static zzd zzi(Status paramStatus)
  {
    return new zzd(paramStatus, null);
  }
  
  public Credential getCredential()
  {
    return this.iP;
  }
  
  public Status getStatus()
  {
    return this.hv;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/auth/api/credentials/internal/zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */