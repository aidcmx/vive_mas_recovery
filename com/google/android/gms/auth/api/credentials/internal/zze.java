package com.google.android.gms.auth.api.credentials.internal;

import android.app.PendingIntent;
import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.Auth.AuthCredentialsOptions;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.auth.api.credentials.CredentialRequest;
import com.google.android.gms.auth.api.credentials.CredentialRequestResult;
import com.google.android.gms.auth.api.credentials.CredentialsApi;
import com.google.android.gms.auth.api.credentials.HintRequest;
import com.google.android.gms.auth.api.credentials.PasswordSpecification;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.internal.zzqo.zzb;

public final class zze
  implements CredentialsApi
{
  private PasswordSpecification zza(GoogleApiClient paramGoogleApiClient)
  {
    paramGoogleApiClient = ((zzg)paramGoogleApiClient.zza(Auth.hX)).zzaim();
    if ((paramGoogleApiClient != null) && (paramGoogleApiClient.zzaid() != null)) {
      return paramGoogleApiClient.zzaid();
    }
    return PasswordSpecification.iG;
  }
  
  public PendingResult<Status> delete(GoogleApiClient paramGoogleApiClient, final Credential paramCredential)
  {
    paramGoogleApiClient.zzb(new zzf(paramGoogleApiClient)
    {
      protected void zza(Context paramAnonymousContext, zzk paramAnonymouszzk)
        throws RemoteException
      {
        paramAnonymouszzk.zza(new zze.zza(this), new DeleteRequest(paramCredential));
      }
      
      protected Status zzb(Status paramAnonymousStatus)
      {
        return paramAnonymousStatus;
      }
    });
  }
  
  public PendingResult<Status> disableAutoSignIn(GoogleApiClient paramGoogleApiClient)
  {
    paramGoogleApiClient.zzb(new zzf(paramGoogleApiClient)
    {
      protected void zza(Context paramAnonymousContext, zzk paramAnonymouszzk)
        throws RemoteException
      {
        paramAnonymouszzk.zza(new zze.zza(this));
      }
      
      protected Status zzb(Status paramAnonymousStatus)
      {
        return paramAnonymousStatus;
      }
    });
  }
  
  public PendingIntent getHintPickerIntent(GoogleApiClient paramGoogleApiClient, HintRequest paramHintRequest)
  {
    zzaa.zzb(paramGoogleApiClient, "client must not be null");
    zzaa.zzb(paramHintRequest, "request must not be null");
    zzaa.zzb(paramGoogleApiClient.zza(Auth.CREDENTIALS_API), "Auth.CREDENTIALS_API must be added to GoogleApiClient to use this API");
    paramHintRequest = zzc.zza(paramGoogleApiClient.getContext(), paramHintRequest, zza(paramGoogleApiClient));
    return PendingIntent.getActivity(paramGoogleApiClient.getContext(), 2000, paramHintRequest, 268435456);
  }
  
  public PendingResult<CredentialRequestResult> request(GoogleApiClient paramGoogleApiClient, final CredentialRequest paramCredentialRequest)
  {
    paramGoogleApiClient.zza(new zzf(paramGoogleApiClient)
    {
      protected void zza(Context paramAnonymousContext, zzk paramAnonymouszzk)
        throws RemoteException
      {
        paramAnonymouszzk.zza(new zza()
        {
          public void zza(Status paramAnonymous2Status, Credential paramAnonymous2Credential)
          {
            zze.1.this.zzc(new zzd(paramAnonymous2Status, paramAnonymous2Credential));
          }
          
          public void zzh(Status paramAnonymous2Status)
          {
            zze.1.this.zzc(zzd.zzi(paramAnonymous2Status));
          }
        }, paramCredentialRequest);
      }
      
      protected CredentialRequestResult zzj(Status paramAnonymousStatus)
      {
        return zzd.zzi(paramAnonymousStatus);
      }
    });
  }
  
  public PendingResult<Status> save(GoogleApiClient paramGoogleApiClient, final Credential paramCredential)
  {
    paramGoogleApiClient.zzb(new zzf(paramGoogleApiClient)
    {
      protected void zza(Context paramAnonymousContext, zzk paramAnonymouszzk)
        throws RemoteException
      {
        paramAnonymouszzk.zza(new zze.zza(this), new SaveRequest(paramCredential));
      }
      
      protected Status zzb(Status paramAnonymousStatus)
      {
        return paramAnonymousStatus;
      }
    });
  }
  
  private static class zza
    extends zza
  {
    private zzqo.zzb<Status> iU;
    
    zza(zzqo.zzb<Status> paramzzb)
    {
      this.iU = paramzzb;
    }
    
    public void zzh(Status paramStatus)
    {
      this.iU.setResult(paramStatus);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/auth/api/credentials/internal/zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */