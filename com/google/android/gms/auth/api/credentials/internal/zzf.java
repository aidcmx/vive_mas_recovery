package com.google.android.gms.auth.api.credentials.internal;

import android.content.Context;
import android.os.DeadObjectException;
import android.os.RemoteException;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.internal.zzqo.zza;

abstract class zzf<R extends Result>
  extends zzqo.zza<R, zzg>
{
  zzf(GoogleApiClient paramGoogleApiClient)
  {
    super(Auth.CREDENTIALS_API, paramGoogleApiClient);
  }
  
  protected abstract void zza(Context paramContext, zzk paramzzk)
    throws DeadObjectException, RemoteException;
  
  protected final void zza(zzg paramzzg)
    throws DeadObjectException, RemoteException
  {
    zza(paramzzg.getContext(), (zzk)paramzzg.zzavg());
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/auth/api/credentials/internal/zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */