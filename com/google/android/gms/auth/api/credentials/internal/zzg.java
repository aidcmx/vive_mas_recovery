package com.google.android.gms.auth.api.credentials.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.Nullable;
import com.google.android.gms.auth.api.Auth.AuthCredentialsOptions;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzj;

public final class zzg
  extends zzj<zzk>
{
  @Nullable
  private final Auth.AuthCredentialsOptions iV;
  
  public zzg(Context paramContext, Looper paramLooper, zzf paramzzf, Auth.AuthCredentialsOptions paramAuthCredentialsOptions, GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
  {
    super(paramContext, paramLooper, 68, paramzzf, paramConnectionCallbacks, paramOnConnectionFailedListener);
    this.iV = paramAuthCredentialsOptions;
  }
  
  protected Bundle zzahv()
  {
    if (this.iV == null) {
      return new Bundle();
    }
    return this.iV.zzahv();
  }
  
  Auth.AuthCredentialsOptions zzaim()
  {
    return this.iV;
  }
  
  protected zzk zzce(IBinder paramIBinder)
  {
    return zzk.zza.zzcg(paramIBinder);
  }
  
  protected String zzjx()
  {
    return "com.google.android.gms.auth.api.credentials.service.START";
  }
  
  protected String zzjy()
  {
    return "com.google.android.gms.auth.api.credentials.internal.ICredentialsService";
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/auth/api/credentials/internal/zzg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */