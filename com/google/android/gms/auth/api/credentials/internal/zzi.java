package com.google.android.gms.auth.api.credentials.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.auth.api.credentials.PasswordSpecification;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzi
  implements Parcelable.Creator<GeneratePasswordRequest>
{
  static void zza(GeneratePasswordRequest paramGeneratePasswordRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zza(paramParcel, 1, paramGeneratePasswordRequest.zzaid(), paramInt, false);
    zzb.zzc(paramParcel, 1000, paramGeneratePasswordRequest.mVersionCode);
    zzb.zzaj(paramParcel, i);
  }
  
  public GeneratePasswordRequest zzaq(Parcel paramParcel)
  {
    int j = zza.zzcr(paramParcel);
    int i = 0;
    PasswordSpecification localPasswordSpecification = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.zzcq(paramParcel);
      switch (zza.zzgu(k))
      {
      default: 
        zza.zzb(paramParcel, k);
        break;
      case 1: 
        localPasswordSpecification = (PasswordSpecification)zza.zza(paramParcel, k, PasswordSpecification.CREATOR);
        break;
      case 1000: 
        i = zza.zzg(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new GeneratePasswordRequest(i, localPasswordSpecification);
  }
  
  public GeneratePasswordRequest[] zzde(int paramInt)
  {
    return new GeneratePasswordRequest[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/auth/api/credentials/internal/zzi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */