package com.google.android.gms.auth.api.credentials;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;

public class zzb
  implements Parcelable.Creator<CredentialPickerConfig>
{
  static void zza(CredentialPickerConfig paramCredentialPickerConfig, Parcel paramParcel, int paramInt)
  {
    paramInt = com.google.android.gms.common.internal.safeparcel.zzb.zzcs(paramParcel);
    com.google.android.gms.common.internal.safeparcel.zzb.zza(paramParcel, 1, paramCredentialPickerConfig.shouldShowAddAccountButton());
    com.google.android.gms.common.internal.safeparcel.zzb.zza(paramParcel, 2, paramCredentialPickerConfig.shouldShowCancelButton());
    com.google.android.gms.common.internal.safeparcel.zzb.zza(paramParcel, 3, paramCredentialPickerConfig.isForNewAccount());
    com.google.android.gms.common.internal.safeparcel.zzb.zzc(paramParcel, 4, paramCredentialPickerConfig.zzaig());
    com.google.android.gms.common.internal.safeparcel.zzb.zzc(paramParcel, 1000, paramCredentialPickerConfig.mVersionCode);
    com.google.android.gms.common.internal.safeparcel.zzb.zzaj(paramParcel, paramInt);
  }
  
  public CredentialPickerConfig zzak(Parcel paramParcel)
  {
    int i = 0;
    int k = zza.zzcr(paramParcel);
    boolean bool1 = false;
    boolean bool2 = false;
    boolean bool3 = false;
    int j = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.zzcq(paramParcel);
      switch (zza.zzgu(m))
      {
      default: 
        zza.zzb(paramParcel, m);
        break;
      case 1: 
        bool3 = zza.zzc(paramParcel, m);
        break;
      case 2: 
        bool2 = zza.zzc(paramParcel, m);
        break;
      case 3: 
        bool1 = zza.zzc(paramParcel, m);
        break;
      case 4: 
        i = zza.zzg(paramParcel, m);
        break;
      case 1000: 
        j = zza.zzg(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza(37 + "Overread allowed size end=" + k, paramParcel);
    }
    return new CredentialPickerConfig(j, bool3, bool2, bool1, i);
  }
  
  public CredentialPickerConfig[] zzcy(int paramInt)
  {
    return new CredentialPickerConfig[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/auth/api/credentials/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */