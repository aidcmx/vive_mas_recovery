package com.google.android.gms.auth.api.credentials;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzd
  implements Parcelable.Creator<HintRequest>
{
  static void zza(HintRequest paramHintRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zza(paramParcel, 1, paramHintRequest.getHintPickerConfig(), paramInt, false);
    zzb.zza(paramParcel, 2, paramHintRequest.isEmailAddressIdentifierSupported());
    zzb.zza(paramParcel, 3, paramHintRequest.zzaih());
    zzb.zza(paramParcel, 4, paramHintRequest.getAccountTypes(), false);
    zzb.zzc(paramParcel, 1000, paramHintRequest.mVersionCode);
    zzb.zzaj(paramParcel, i);
  }
  
  public HintRequest zzam(Parcel paramParcel)
  {
    String[] arrayOfString = null;
    boolean bool1 = false;
    int j = zza.zzcr(paramParcel);
    boolean bool2 = false;
    CredentialPickerConfig localCredentialPickerConfig = null;
    int i = 0;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.zzcq(paramParcel);
      switch (zza.zzgu(k))
      {
      default: 
        zza.zzb(paramParcel, k);
        break;
      case 1: 
        localCredentialPickerConfig = (CredentialPickerConfig)zza.zza(paramParcel, k, CredentialPickerConfig.CREATOR);
        break;
      case 2: 
        bool2 = zza.zzc(paramParcel, k);
        break;
      case 3: 
        bool1 = zza.zzc(paramParcel, k);
        break;
      case 4: 
        arrayOfString = zza.zzac(paramParcel, k);
        break;
      case 1000: 
        i = zza.zzg(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new HintRequest(i, localCredentialPickerConfig, bool2, bool1, arrayOfString);
  }
  
  public HintRequest[] zzda(int paramInt)
  {
    return new HintRequest[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/auth/api/credentials/zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */