package com.google.android.gms.auth.api.credentials;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.ArrayList;

public class zzf
  implements Parcelable.Creator<PasswordSpecification>
{
  static void zza(PasswordSpecification paramPasswordSpecification, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.zzcs(paramParcel);
    zzb.zza(paramParcel, 1, paramPasswordSpecification.iI, false);
    zzb.zzb(paramParcel, 2, paramPasswordSpecification.iJ, false);
    zzb.zza(paramParcel, 3, paramPasswordSpecification.iK, false);
    zzb.zzc(paramParcel, 4, paramPasswordSpecification.iL);
    zzb.zzc(paramParcel, 5, paramPasswordSpecification.iM);
    zzb.zzc(paramParcel, 1000, paramPasswordSpecification.mVersionCode);
    zzb.zzaj(paramParcel, paramInt);
  }
  
  public PasswordSpecification zzao(Parcel paramParcel)
  {
    ArrayList localArrayList1 = null;
    int i = 0;
    int m = zza.zzcr(paramParcel);
    int j = 0;
    ArrayList localArrayList2 = null;
    String str = null;
    int k = 0;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.zzcq(paramParcel);
      switch (zza.zzgu(n))
      {
      default: 
        zza.zzb(paramParcel, n);
        break;
      case 1: 
        str = zza.zzq(paramParcel, n);
        break;
      case 2: 
        localArrayList2 = zza.zzae(paramParcel, n);
        break;
      case 3: 
        localArrayList1 = zza.zzad(paramParcel, n);
        break;
      case 4: 
        j = zza.zzg(paramParcel, n);
        break;
      case 5: 
        i = zza.zzg(paramParcel, n);
        break;
      case 1000: 
        k = zza.zzg(paramParcel, n);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza(37 + "Overread allowed size end=" + m, paramParcel);
    }
    return new PasswordSpecification(k, str, localArrayList2, localArrayList1, j, i);
  }
  
  public PasswordSpecification[] zzdc(int paramInt)
  {
    return new PasswordSpecification[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/auth/api/credentials/zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */