package com.google.android.gms.auth.api.signin;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;

public class GoogleSignInResult
  implements Result
{
  private Status hv;
  private GoogleSignInAccount jx;
  
  public GoogleSignInResult(@Nullable GoogleSignInAccount paramGoogleSignInAccount, @NonNull Status paramStatus)
  {
    this.jx = paramGoogleSignInAccount;
    this.hv = paramStatus;
  }
  
  @Nullable
  public GoogleSignInAccount getSignInAccount()
  {
    return this.jx;
  }
  
  @NonNull
  public Status getStatus()
  {
    return this.hv;
  }
  
  public boolean isSuccess()
  {
    return this.hv.isSuccess();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/auth/api/signin/GoogleSignInResult.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */