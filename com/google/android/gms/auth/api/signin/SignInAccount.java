package com.google.android.gms.auth.api.signin;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;

public class SignInAccount
  extends AbstractSafeParcelable
  implements ReflectedParcelable
{
  public static final Parcelable.Creator<SignInAccount> CREATOR = new zzc();
  @Deprecated
  String ck;
  @Deprecated
  String jg;
  private GoogleSignInAccount jy;
  final int versionCode;
  
  SignInAccount(int paramInt, String paramString1, GoogleSignInAccount paramGoogleSignInAccount, String paramString2)
  {
    this.versionCode = paramInt;
    this.jy = paramGoogleSignInAccount;
    this.jg = zzaa.zzh(paramString1, "8.3 and 8.4 SDKs require non-null email");
    this.ck = zzaa.zzh(paramString2, "8.3 and 8.4 SDKs require non-null userId");
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzc.zza(this, paramParcel, paramInt);
  }
  
  public GoogleSignInAccount zzaiz()
  {
    return this.jy;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/auth/api/signin/SignInAccount.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */