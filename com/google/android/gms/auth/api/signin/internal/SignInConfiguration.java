package com.google.android.gms.auth.api.signin.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;

public final class SignInConfiguration
  extends AbstractSafeParcelable
  implements ReflectedParcelable
{
  public static final Parcelable.Creator<SignInConfiguration> CREATOR = new zzj();
  private final String jK;
  private GoogleSignInOptions jL;
  final int versionCode;
  
  SignInConfiguration(int paramInt, String paramString, GoogleSignInOptions paramGoogleSignInOptions)
  {
    this.versionCode = paramInt;
    this.jK = zzaa.zzib(paramString);
    this.jL = paramGoogleSignInOptions;
  }
  
  public SignInConfiguration(String paramString, GoogleSignInOptions paramGoogleSignInOptions)
  {
    this(3, paramString, paramGoogleSignInOptions);
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {}
    for (;;)
    {
      return false;
      try
      {
        paramObject = (SignInConfiguration)paramObject;
        if (this.jK.equals(((SignInConfiguration)paramObject).zzajj())) {
          if (this.jL == null)
          {
            if (((SignInConfiguration)paramObject).zzajk() != null) {
              continue;
            }
          }
          else
          {
            boolean bool = this.jL.equals(((SignInConfiguration)paramObject).zzajk());
            if (!bool) {
              continue;
            }
          }
        }
      }
      catch (ClassCastException paramObject)
      {
        return false;
      }
    }
    return true;
  }
  
  public int hashCode()
  {
    return new zze().zzq(this.jK).zzq(this.jL).zzajf();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzj.zza(this, paramParcel, paramInt);
  }
  
  public String zzajj()
  {
    return this.jK;
  }
  
  public GoogleSignInOptions zzajk()
  {
    return this.jL;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/auth/api/signin/internal/SignInConfiguration.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */