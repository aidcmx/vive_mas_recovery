package com.google.android.gms.auth.api.signin.internal;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.SignInAccount;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;

@KeepName
public class SignInHubActivity
  extends FragmentActivity
{
  private zzk jM;
  private SignInConfiguration jN;
  private boolean jO;
  private int jP;
  private Intent jQ;
  
  private void zza(int paramInt, Intent paramIntent)
  {
    if (paramIntent != null)
    {
      Object localObject = (SignInAccount)paramIntent.getParcelableExtra("signInAccount");
      if ((localObject != null) && (((SignInAccount)localObject).zzaiz() != null))
      {
        localObject = ((SignInAccount)localObject).zzaiz();
        this.jM.zzb((GoogleSignInAccount)localObject, this.jN.zzajk());
        paramIntent.removeExtra("signInAccount");
        paramIntent.putExtra("googleSignInAccount", (Parcelable)localObject);
        this.jO = true;
        this.jP = paramInt;
        this.jQ = paramIntent;
        zzajl();
        return;
      }
      if (paramIntent.hasExtra("errorCode"))
      {
        zzdn(paramIntent.getIntExtra("errorCode", 8));
        return;
      }
    }
    zzdn(8);
  }
  
  private void zzajl()
  {
    getSupportLoaderManager().initLoader(0, null, new zza(null));
  }
  
  private void zzdn(int paramInt)
  {
    Status localStatus = new Status(paramInt);
    Intent localIntent = new Intent();
    localIntent.putExtra("googleSignInStatus", localStatus);
    setResult(0, localIntent);
    finish();
  }
  
  private void zzj(Intent paramIntent)
  {
    paramIntent.setPackage("com.google.android.gms");
    paramIntent.putExtra("config", this.jN);
    try
    {
      startActivityForResult(paramIntent, 40962);
      return;
    }
    catch (ActivityNotFoundException paramIntent)
    {
      Log.w("AuthSignInClient", "Could not launch sign in Intent. Google Play Service is probably being updated...");
      zzdn(8);
    }
  }
  
  public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
  {
    return true;
  }
  
  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    setResult(0);
    switch (paramInt1)
    {
    default: 
      return;
    }
    zza(paramInt2, paramIntent);
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    this.jM = zzk.zzba(this);
    Intent localIntent = getIntent();
    String str;
    if (!"com.google.android.gms.auth.GOOGLE_SIGN_IN".equals(localIntent.getAction()))
    {
      str = String.valueOf(localIntent.getAction());
      if (str.length() != 0)
      {
        str = "Unknown action: ".concat(str);
        Log.e("AuthSignInClient", str);
        finish();
      }
    }
    else
    {
      this.jN = ((SignInConfiguration)localIntent.getParcelableExtra("config"));
      if (this.jN != null) {
        break label114;
      }
      Log.e("AuthSignInClient", "Activity started with invalid configuration.");
      setResult(0);
      finish();
    }
    label114:
    do
    {
      return;
      str = new String("Unknown action: ");
      break;
      if (paramBundle == null)
      {
        zzj(new Intent("com.google.android.gms.auth.GOOGLE_SIGN_IN"));
        return;
      }
      this.jO = paramBundle.getBoolean("signingInGoogleApiClients");
    } while (!this.jO);
    this.jP = paramBundle.getInt("signInResultCode");
    this.jQ = ((Intent)paramBundle.getParcelable("signInResultData"));
    zzajl();
  }
  
  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putBoolean("signingInGoogleApiClients", this.jO);
    if (this.jO)
    {
      paramBundle.putInt("signInResultCode", this.jP);
      paramBundle.putParcelable("signInResultData", this.jQ);
    }
  }
  
  private class zza
    implements LoaderManager.LoaderCallbacks<Void>
  {
    private zza() {}
    
    public Loader<Void> onCreateLoader(int paramInt, Bundle paramBundle)
    {
      return new zzb(SignInHubActivity.this, GoogleApiClient.zzarc());
    }
    
    public void onLoaderReset(Loader<Void> paramLoader) {}
    
    public void zza(Loader<Void> paramLoader, Void paramVoid)
    {
      SignInHubActivity.this.setResult(SignInHubActivity.zza(SignInHubActivity.this), SignInHubActivity.zzb(SignInHubActivity.this));
      SignInHubActivity.this.finish();
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/auth/api/signin/internal/SignInHubActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */