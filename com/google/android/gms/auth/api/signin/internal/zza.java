package com.google.android.gms.auth.api.signin.internal;

import android.os.RemoteException;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Status;

public class zza
  extends zzg.zza
{
  public void zza(GoogleSignInAccount paramGoogleSignInAccount, Status paramStatus)
    throws RemoteException
  {
    throw new UnsupportedOperationException();
  }
  
  public void zzl(Status paramStatus)
    throws RemoteException
  {
    throw new UnsupportedOperationException();
  }
  
  public void zzm(Status paramStatus)
    throws RemoteException
  {
    throw new UnsupportedOperationException();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/auth/api/signin/internal/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */