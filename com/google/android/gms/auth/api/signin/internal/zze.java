package com.google.android.gms.auth.api.signin.internal;

public class zze
{
  static int jI = 31;
  private int jJ = 1;
  
  public int zzajf()
  {
    return this.jJ;
  }
  
  public zze zzbe(boolean paramBoolean)
  {
    int j = jI;
    int k = this.jJ;
    if (paramBoolean) {}
    for (int i = 1;; i = 0)
    {
      this.jJ = (i + k * j);
      return this;
    }
  }
  
  public zze zzq(Object paramObject)
  {
    int j = jI;
    int k = this.jJ;
    if (paramObject == null) {}
    for (int i = 0;; i = paramObject.hashCode())
    {
      this.jJ = (i + k * j);
      return this;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/auth/api/signin/internal/zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */