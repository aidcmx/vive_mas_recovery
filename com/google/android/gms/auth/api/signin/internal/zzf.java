package com.google.android.gms.auth.api.signin.internal;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract interface zzf
  extends IInterface
{
  public abstract void zzajg()
    throws RemoteException;
  
  public static abstract class zza
    extends Binder
    implements zzf
  {
    public zza()
    {
      attachInterface(this, "com.google.android.gms.auth.api.signin.internal.IRevocationService");
    }
    
    public IBinder asBinder()
    {
      return this;
    }
    
    public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
      throws RemoteException
    {
      switch (paramInt1)
      {
      default: 
        return super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
      case 1598968902: 
        paramParcel2.writeString("com.google.android.gms.auth.api.signin.internal.IRevocationService");
        return true;
      }
      paramParcel1.enforceInterface("com.google.android.gms.auth.api.signin.internal.IRevocationService");
      zzajg();
      return true;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/auth/api/signin/internal/zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */