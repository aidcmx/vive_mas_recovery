package com.google.android.gms.auth.api.signin.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzj
  implements Parcelable.Creator<SignInConfiguration>
{
  static void zza(SignInConfiguration paramSignInConfiguration, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramSignInConfiguration.versionCode);
    zzb.zza(paramParcel, 2, paramSignInConfiguration.zzajj(), false);
    zzb.zza(paramParcel, 5, paramSignInConfiguration.zzajk(), paramInt, false);
    zzb.zzaj(paramParcel, i);
  }
  
  public SignInConfiguration zzay(Parcel paramParcel)
  {
    GoogleSignInOptions localGoogleSignInOptions = null;
    int j = zza.zzcr(paramParcel);
    int i = 0;
    String str = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.zzcq(paramParcel);
      switch (zza.zzgu(k))
      {
      case 3: 
      case 4: 
      default: 
        zza.zzb(paramParcel, k);
        break;
      case 1: 
        i = zza.zzg(paramParcel, k);
        break;
      case 2: 
        str = zza.zzq(paramParcel, k);
        break;
      case 5: 
        localGoogleSignInOptions = (GoogleSignInOptions)zza.zza(paramParcel, k, GoogleSignInOptions.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new SignInConfiguration(i, str, localGoogleSignInOptions);
  }
  
  public SignInConfiguration[] zzdm(int paramInt)
  {
    return new SignInConfiguration[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/auth/api/signin/internal/zzj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */