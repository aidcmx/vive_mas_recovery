package com.google.android.gms.auth.api.signin;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzc
  implements Parcelable.Creator<SignInAccount>
{
  static void zza(SignInAccount paramSignInAccount, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramSignInAccount.versionCode);
    zzb.zza(paramParcel, 4, paramSignInAccount.jg, false);
    zzb.zza(paramParcel, 7, paramSignInAccount.zzaiz(), paramInt, false);
    zzb.zza(paramParcel, 8, paramSignInAccount.ck, false);
    zzb.zzaj(paramParcel, i);
  }
  
  public SignInAccount zzax(Parcel paramParcel)
  {
    int j = zza.zzcr(paramParcel);
    int i = 0;
    Object localObject1 = "";
    Object localObject2 = null;
    String str = "";
    if (paramParcel.dataPosition() < j)
    {
      int k = zza.zzcq(paramParcel);
      Object localObject3;
      switch (zza.zzgu(k))
      {
      case 2: 
      case 3: 
      case 5: 
      case 6: 
      default: 
        zza.zzb(paramParcel, k);
        localObject3 = localObject2;
        localObject2 = localObject1;
        localObject1 = localObject3;
      }
      for (;;)
      {
        localObject3 = localObject2;
        localObject2 = localObject1;
        localObject1 = localObject3;
        break;
        i = zza.zzg(paramParcel, k);
        localObject3 = localObject1;
        localObject1 = localObject2;
        localObject2 = localObject3;
        continue;
        localObject3 = zza.zzq(paramParcel, k);
        localObject1 = localObject2;
        localObject2 = localObject3;
        continue;
        localObject3 = (GoogleSignInAccount)zza.zza(paramParcel, k, GoogleSignInAccount.CREATOR);
        localObject2 = localObject1;
        localObject1 = localObject3;
        continue;
        str = zza.zzq(paramParcel, k);
        localObject3 = localObject1;
        localObject1 = localObject2;
        localObject2 = localObject3;
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new SignInAccount(i, (String)localObject1, (GoogleSignInAccount)localObject2, str);
  }
  
  public SignInAccount[] zzdl(int paramInt)
  {
    return new SignInAccount[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/auth/api/signin/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */