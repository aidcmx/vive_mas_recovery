package com.google.android.gms.auth.api;

import android.content.Context;
import android.os.Looper;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.zza;
import com.google.android.gms.common.api.Api.zzf;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.internal.zzny;

public final class zza
{
  public static final Api<zzb> API = new Api("Auth.PROXY_API", ij, ii);
  public static final Api.zzf<zzny> ii = new Api.zzf();
  private static final Api.zza<zzny, zzb> ij = new Api.zza()
  {
    public zzny zza(Context paramAnonymousContext, Looper paramAnonymousLooper, zzf paramAnonymouszzf, zzb paramAnonymouszzb, GoogleApiClient.ConnectionCallbacks paramAnonymousConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramAnonymousOnConnectionFailedListener)
    {
      return new zzny(paramAnonymousContext, paramAnonymousLooper, paramAnonymouszzf, paramAnonymouszzb, paramAnonymousConnectionCallbacks, paramAnonymousOnConnectionFailedListener);
    }
  };
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/auth/api/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */