package com.google.android.gms.auth.api;

import android.os.Bundle;
import com.google.android.gms.common.api.Api.ApiOptions.Optional;

public class zzb
  implements Api.ApiOptions.Optional
{
  private final Bundle ik;
  
  public Bundle zzaie()
  {
    return new Bundle(this.ik);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/auth/api/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */