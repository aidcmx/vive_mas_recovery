package com.google.android.gms.auth;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.TargetApi;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.SystemClock;
import android.support.annotation.RequiresPermission;
import android.text.TextUtils;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.internal.zzbz;
import com.google.android.gms.internal.zzbz.zza;
import com.google.android.gms.internal.zzoe;
import com.google.android.gms.internal.zzsu;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

public class zze
{
  public static final int CHANGE_TYPE_ACCOUNT_ADDED = 1;
  public static final int CHANGE_TYPE_ACCOUNT_REMOVED = 2;
  public static final int CHANGE_TYPE_ACCOUNT_RENAMED_FROM = 3;
  public static final int CHANGE_TYPE_ACCOUNT_RENAMED_TO = 4;
  public static final String GOOGLE_ACCOUNT_TYPE = "com.google";
  public static final String KEY_ANDROID_PACKAGE_NAME;
  public static final String KEY_CALLER_UID;
  public static final String KEY_SUPPRESS_PROGRESS_SCREEN = "suppressProgressScreen";
  public static final String WORK_ACCOUNT_TYPE = "com.google.work";
  private static final String[] hC = { "com.google", "com.google.work", "cn.google" };
  private static final ComponentName hD;
  private static final ComponentName hE;
  private static final zzsu hF;
  
  static
  {
    if (Build.VERSION.SDK_INT >= 11)
    {
      KEY_CALLER_UID = "callerUid";
      if (Build.VERSION.SDK_INT < 14) {
        break label95;
      }
    }
    label95:
    for (;;)
    {
      KEY_ANDROID_PACKAGE_NAME = "androidPackageName";
      hD = new ComponentName("com.google.android.gms", "com.google.android.gms.auth.GetToken");
      hE = new ComponentName("com.google.android.gms", "com.google.android.gms.recovery.RecoveryService");
      hF = zzd.zzb(new String[] { "GoogleAuthUtil" });
      return;
      break;
    }
  }
  
  public static void clearToken(Context paramContext, String paramString)
    throws GooglePlayServicesAvailabilityException, GoogleAuthException, IOException
  {
    zzaa.zzht("Calling this from your main thread can lead to deadlock");
    zzaz(paramContext);
    final Bundle localBundle = new Bundle();
    String str = paramContext.getApplicationInfo().packageName;
    localBundle.putString("clientPackageName", str);
    if (!localBundle.containsKey(KEY_ANDROID_PACKAGE_NAME)) {
      localBundle.putString(KEY_ANDROID_PACKAGE_NAME, str);
    }
    paramString = new zza()
    {
      public Void zzbv(IBinder paramAnonymousIBinder)
        throws RemoteException, IOException, GoogleAuthException
      {
        paramAnonymousIBinder = (Bundle)zze.zzo(zzbz.zza.zza(paramAnonymousIBinder).zza(zze.this, localBundle));
        String str = paramAnonymousIBinder.getString("Error");
        if (!paramAnonymousIBinder.getBoolean("booleanResult")) {
          throw new GoogleAuthException(str);
        }
        return null;
      }
    };
    zza(paramContext, hD, paramString);
  }
  
  public static List<AccountChangeEvent> getAccountChangeEvents(Context paramContext, final int paramInt, String paramString)
    throws GoogleAuthException, IOException
  {
    zzaa.zzh(paramString, "accountName must be provided");
    zzaa.zzht("Calling this from your main thread can lead to deadlock");
    zzaz(paramContext);
    paramString = new zza()
    {
      public List<AccountChangeEvent> zzbw(IBinder paramAnonymousIBinder)
        throws RemoteException, IOException, GoogleAuthException
      {
        return ((AccountChangeEventsResponse)zze.zzo(zzbz.zza.zza(paramAnonymousIBinder).zza(new AccountChangeEventsRequest().setAccountName(zze.this).setEventIndex(paramInt)))).getEvents();
      }
    };
    return (List)zza(paramContext, hD, paramString);
  }
  
  public static String getAccountId(Context paramContext, String paramString)
    throws GoogleAuthException, IOException
  {
    zzaa.zzh(paramString, "accountName must be provided");
    zzaa.zzht("Calling this from your main thread can lead to deadlock");
    zzaz(paramContext);
    return getToken(paramContext, paramString, "^^_account_id_^^", new Bundle());
  }
  
  public static String getToken(Context paramContext, Account paramAccount, String paramString)
    throws IOException, UserRecoverableAuthException, GoogleAuthException
  {
    return getToken(paramContext, paramAccount, paramString, new Bundle());
  }
  
  public static String getToken(Context paramContext, Account paramAccount, String paramString, Bundle paramBundle)
    throws IOException, UserRecoverableAuthException, GoogleAuthException
  {
    zzc(paramAccount);
    return zzc(paramContext, paramAccount, paramString, paramBundle).getToken();
  }
  
  @Deprecated
  public static String getToken(Context paramContext, String paramString1, String paramString2)
    throws IOException, UserRecoverableAuthException, GoogleAuthException
  {
    return getToken(paramContext, new Account(paramString1, "com.google"), paramString2);
  }
  
  @Deprecated
  public static String getToken(Context paramContext, String paramString1, String paramString2, Bundle paramBundle)
    throws IOException, UserRecoverableAuthException, GoogleAuthException
  {
    return getToken(paramContext, new Account(paramString1, "com.google"), paramString2, paramBundle);
  }
  
  @Deprecated
  @RequiresPermission("android.permission.MANAGE_ACCOUNTS")
  public static void invalidateToken(Context paramContext, String paramString)
  {
    AccountManager.get(paramContext).invalidateAuthToken("com.google", paramString);
  }
  
  @TargetApi(23)
  public static Bundle removeAccount(Context paramContext, Account paramAccount)
    throws GoogleAuthException, IOException
  {
    zzaa.zzy(paramContext);
    zzc(paramAccount);
    zzaz(paramContext);
    paramAccount = new zza()
    {
      public Bundle zzbx(IBinder paramAnonymousIBinder)
        throws RemoteException, IOException, GoogleAuthException
      {
        return (Bundle)zze.zzo(zzbz.zza.zza(paramAnonymousIBinder).zza(zze.this));
      }
    };
    return (Bundle)zza(paramContext, hD, paramAccount);
  }
  
  /* Error */
  private static <T> T zza(Context paramContext, ComponentName paramComponentName, zza<T> paramzza)
    throws IOException, GoogleAuthException
  {
    // Byte code:
    //   0: new 222	com/google/android/gms/common/zza
    //   3: dup
    //   4: invokespecial 223	com/google/android/gms/common/zza:<init>	()V
    //   7: astore_3
    //   8: aload_0
    //   9: invokestatic 229	com/google/android/gms/common/internal/zzl:zzcc	(Landroid/content/Context;)Lcom/google/android/gms/common/internal/zzl;
    //   12: astore 4
    //   14: aload 4
    //   16: aload_1
    //   17: aload_3
    //   18: ldc 82
    //   20: invokevirtual 232	com/google/android/gms/common/internal/zzl:zza	(Landroid/content/ComponentName;Landroid/content/ServiceConnection;Ljava/lang/String;)Z
    //   23: ifeq +70 -> 93
    //   26: aload_2
    //   27: aload_3
    //   28: invokevirtual 236	com/google/android/gms/common/zza:zzaqk	()Landroid/os/IBinder;
    //   31: invokeinterface 240 2 0
    //   36: astore_0
    //   37: aload 4
    //   39: aload_1
    //   40: aload_3
    //   41: ldc 82
    //   43: invokevirtual 243	com/google/android/gms/common/internal/zzl:zzb	(Landroid/content/ComponentName;Landroid/content/ServiceConnection;Ljava/lang/String;)V
    //   46: aload_0
    //   47: areturn
    //   48: astore_0
    //   49: getstatic 90	com/google/android/gms/auth/zze:hF	Lcom/google/android/gms/internal/zzsu;
    //   52: ldc 82
    //   54: iconst_2
    //   55: anewarray 4	java/lang/Object
    //   58: dup
    //   59: iconst_0
    //   60: ldc -11
    //   62: aastore
    //   63: dup
    //   64: iconst_1
    //   65: aload_0
    //   66: aastore
    //   67: invokevirtual 251	com/google/android/gms/internal/zzsu:zze	(Ljava/lang/String;[Ljava/lang/Object;)V
    //   70: new 101	java/io/IOException
    //   73: dup
    //   74: ldc -11
    //   76: aload_0
    //   77: invokespecial 254	java/io/IOException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   80: athrow
    //   81: astore_0
    //   82: aload 4
    //   84: aload_1
    //   85: aload_3
    //   86: ldc 82
    //   88: invokevirtual 243	com/google/android/gms/common/internal/zzl:zzb	(Landroid/content/ComponentName;Landroid/content/ServiceConnection;Ljava/lang/String;)V
    //   91: aload_0
    //   92: athrow
    //   93: new 101	java/io/IOException
    //   96: dup
    //   97: ldc_w 256
    //   100: invokespecial 258	java/io/IOException:<init>	(Ljava/lang/String;)V
    //   103: athrow
    //   104: astore_0
    //   105: goto -56 -> 49
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	108	0	paramContext	Context
    //   0	108	1	paramComponentName	ComponentName
    //   0	108	2	paramzza	zza<T>
    //   7	79	3	localzza	com.google.android.gms.common.zza
    //   12	71	4	localzzl	com.google.android.gms.common.internal.zzl
    // Exception table:
    //   from	to	target	type
    //   26	37	48	java/lang/InterruptedException
    //   26	37	81	finally
    //   49	81	81	finally
    //   26	37	104	android/os/RemoteException
  }
  
  private static void zzaz(Context paramContext)
    throws GoogleAuthException
  {
    try
    {
      com.google.android.gms.common.zze.zzaz(paramContext.getApplicationContext());
      return;
    }
    catch (GooglePlayServicesRepairableException paramContext)
    {
      throw new GooglePlayServicesAvailabilityException(paramContext.getConnectionStatusCode(), paramContext.getMessage(), paramContext.getIntent());
    }
    catch (GooglePlayServicesNotAvailableException paramContext)
    {
      throw new GoogleAuthException(paramContext.getMessage());
    }
  }
  
  public static TokenData zzc(Context paramContext, Account paramAccount, final String paramString, final Bundle paramBundle)
    throws IOException, UserRecoverableAuthException, GoogleAuthException
  {
    zzaa.zzht("Calling this from your main thread can lead to deadlock");
    zzaa.zzh(paramString, "Scope cannot be empty or null.");
    zzc(paramAccount);
    zzaz(paramContext);
    if (paramBundle == null) {}
    for (paramBundle = new Bundle();; paramBundle = new Bundle(paramBundle))
    {
      String str = paramContext.getApplicationInfo().packageName;
      paramBundle.putString("clientPackageName", str);
      if (TextUtils.isEmpty(paramBundle.getString(KEY_ANDROID_PACKAGE_NAME))) {
        paramBundle.putString(KEY_ANDROID_PACKAGE_NAME, str);
      }
      paramBundle.putLong("service_connection_start_time_millis", SystemClock.elapsedRealtime());
      paramAccount = new zza()
      {
        public TokenData zzbt(IBinder paramAnonymousIBinder)
          throws RemoteException, IOException, GoogleAuthException
        {
          Object localObject1 = (Bundle)zze.zzo(zzbz.zza.zza(paramAnonymousIBinder).zza(zze.this, paramString, paramBundle));
          paramAnonymousIBinder = TokenData.zzd((Bundle)localObject1, "tokenDetails");
          if (paramAnonymousIBinder != null) {
            return paramAnonymousIBinder;
          }
          paramAnonymousIBinder = ((Bundle)localObject1).getString("Error");
          localObject1 = (Intent)((Bundle)localObject1).getParcelable("userRecoveryIntent");
          Object localObject2 = zzoe.zzgi(paramAnonymousIBinder);
          if (zzoe.zza((zzoe)localObject2))
          {
            zzsu localzzsu = zze.zzahx();
            localObject2 = String.valueOf(localObject2);
            localzzsu.zzf("GoogleAuthUtil", new Object[] { String.valueOf(localObject2).length() + 31 + "isUserRecoverableError status: " + (String)localObject2 });
            throw new UserRecoverableAuthException(paramAnonymousIBinder, (Intent)localObject1);
          }
          if (zzoe.zzb((zzoe)localObject2)) {
            throw new IOException(paramAnonymousIBinder);
          }
          throw new GoogleAuthException(paramAnonymousIBinder);
        }
      };
      return (TokenData)zza(paramContext, hD, paramAccount);
    }
  }
  
  private static void zzc(Account paramAccount)
  {
    if (paramAccount == null) {
      throw new IllegalArgumentException("Account cannot be null");
    }
    if (TextUtils.isEmpty(paramAccount.name)) {
      throw new IllegalArgumentException("Account name cannot be empty!");
    }
    String[] arrayOfString = hC;
    int j = arrayOfString.length;
    int i = 0;
    while (i < j)
    {
      if (arrayOfString[i].equals(paramAccount.type)) {
        return;
      }
      i += 1;
    }
    throw new IllegalArgumentException("Account type not supported");
  }
  
  static void zzi(Intent paramIntent)
  {
    if (paramIntent == null) {
      throw new IllegalArgumentException("Callback cannot be null.");
    }
    paramIntent = paramIntent.toUri(1);
    try
    {
      Intent.parseUri(paramIntent, 1);
      return;
    }
    catch (URISyntaxException paramIntent)
    {
      throw new IllegalArgumentException("Parameter callback contains invalid data. It must be serializable using toUri() and parseUri().");
    }
  }
  
  private static <T> T zzn(T paramT)
    throws IOException
  {
    if (paramT == null)
    {
      hF.zzf("GoogleAuthUtil", new Object[] { "Binder call returned null." });
      throw new IOException("Service unavailable.");
    }
    return paramT;
  }
  
  private static abstract interface zza<T>
  {
    public abstract T zzbu(IBinder paramIBinder)
      throws RemoteException, IOException, GoogleAuthException;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/auth/zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */