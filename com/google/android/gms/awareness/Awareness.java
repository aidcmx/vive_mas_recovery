package com.google.android.gms.awareness;

import com.google.android.gms.common.api.Api;
import com.google.android.gms.contextmanager.zzc;

public final class Awareness
{
  public static final Api<AwarenessOptions> API = zzc.API;
  public static final FenceApi FenceApi = zzc.FenceApi;
  public static final SnapshotApi SnapshotApi = zzc.SnapshotApi;
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/awareness/Awareness.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */