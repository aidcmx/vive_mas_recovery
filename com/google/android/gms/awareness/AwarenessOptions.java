package com.google.android.gms.awareness;

import com.google.android.gms.common.api.Api.ApiOptions.Optional;
import com.google.android.gms.common.internal.zzaa;

public class AwarenessOptions
  implements Api.ApiOptions.Optional
{
  public static final int NO_UID = -1;
  private final String kY;
  private final int kZ;
  private final String la;
  private final String lb;
  private final int lc;
  
  protected AwarenessOptions(String paramString1, int paramInt1, String paramString2, String paramString3, int paramInt2)
  {
    this.kY = paramString1;
    this.kZ = paramInt1;
    this.la = paramString2;
    this.lb = paramString3;
    this.lc = paramInt2;
  }
  
  public static AwarenessOptions create1p(String paramString)
  {
    zzaa.zzib(paramString);
    return new AwarenessOptions(paramString, 1, null, null, -1);
  }
  
  public String zzajp()
  {
    return this.kY;
  }
  
  public int zzajq()
  {
    return this.kZ;
  }
  
  public String zzajr()
  {
    return this.la;
  }
  
  public String zzajs()
  {
    return this.lb;
  }
  
  public int zzajt()
  {
    return this.lc;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/awareness/AwarenessOptions.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */