package com.google.android.gms.awareness;

import com.google.android.gms.awareness.fence.FenceQueryRequest;
import com.google.android.gms.awareness.fence.FenceQueryResult;
import com.google.android.gms.awareness.fence.FenceUpdateRequest;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;

public abstract interface FenceApi
{
  public abstract PendingResult<FenceQueryResult> queryFences(GoogleApiClient paramGoogleApiClient, FenceQueryRequest paramFenceQueryRequest);
  
  public abstract PendingResult<Status> updateFences(GoogleApiClient paramGoogleApiClient, FenceUpdateRequest paramFenceUpdateRequest);
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/awareness/FenceApi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */