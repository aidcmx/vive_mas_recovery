package com.google.android.gms.awareness;

import android.support.annotation.RequiresPermission;
import com.google.android.gms.awareness.snapshot.BeaconStateResult;
import com.google.android.gms.awareness.snapshot.DetectedActivityResult;
import com.google.android.gms.awareness.snapshot.HeadphoneStateResult;
import com.google.android.gms.awareness.snapshot.LocationResult;
import com.google.android.gms.awareness.snapshot.PlacesResult;
import com.google.android.gms.awareness.snapshot.WeatherResult;
import com.google.android.gms.awareness.state.BeaconState.TypeFilter;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import java.util.Collection;

public abstract interface SnapshotApi
{
  @RequiresPermission("android.permission.ACCESS_FINE_LOCATION")
  public abstract PendingResult<BeaconStateResult> getBeaconState(GoogleApiClient paramGoogleApiClient, Collection<BeaconState.TypeFilter> paramCollection);
  
  @RequiresPermission("android.permission.ACCESS_FINE_LOCATION")
  public abstract PendingResult<BeaconStateResult> getBeaconState(GoogleApiClient paramGoogleApiClient, BeaconState.TypeFilter... paramVarArgs);
  
  @RequiresPermission("com.google.android.gms.permission.ACTIVITY_RECOGNITION")
  public abstract PendingResult<DetectedActivityResult> getDetectedActivity(GoogleApiClient paramGoogleApiClient);
  
  public abstract PendingResult<HeadphoneStateResult> getHeadphoneState(GoogleApiClient paramGoogleApiClient);
  
  @RequiresPermission("android.permission.ACCESS_FINE_LOCATION")
  public abstract PendingResult<LocationResult> getLocation(GoogleApiClient paramGoogleApiClient);
  
  @RequiresPermission("android.permission.ACCESS_FINE_LOCATION")
  public abstract PendingResult<PlacesResult> getPlaces(GoogleApiClient paramGoogleApiClient);
  
  @RequiresPermission("android.permission.ACCESS_FINE_LOCATION")
  public abstract PendingResult<WeatherResult> getWeather(GoogleApiClient paramGoogleApiClient);
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/awareness/SnapshotApi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */