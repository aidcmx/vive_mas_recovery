package com.google.android.gms.awareness.fence;

import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.contextmanager.fence.internal.ContextFenceStub;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public abstract class AwarenessFence
  extends AbstractSafeParcelable
{
  public static AwarenessFence and(Collection<AwarenessFence> paramCollection)
  {
    if ((paramCollection != null) && (!paramCollection.isEmpty())) {}
    for (boolean bool = true;; bool = false)
    {
      zzaa.zzbt(bool);
      return ContextFenceStub.zzg(zzd(paramCollection));
    }
  }
  
  public static AwarenessFence and(AwarenessFence... paramVarArgs)
  {
    if ((paramVarArgs != null) && (paramVarArgs.length > 0)) {}
    for (boolean bool = true;; bool = false)
    {
      zzaa.zzbt(bool);
      return ContextFenceStub.zzg(zza(paramVarArgs));
    }
  }
  
  public static AwarenessFence not(AwarenessFence paramAwarenessFence)
  {
    zzaa.zzy(paramAwarenessFence);
    return ContextFenceStub.zza((ContextFenceStub)paramAwarenessFence);
  }
  
  public static AwarenessFence or(Collection<AwarenessFence> paramCollection)
  {
    if ((paramCollection != null) && (!paramCollection.isEmpty())) {}
    for (boolean bool = true;; bool = false)
    {
      zzaa.zzbt(bool);
      return ContextFenceStub.zzh(zzd(paramCollection));
    }
  }
  
  public static AwarenessFence or(AwarenessFence... paramVarArgs)
  {
    if ((paramVarArgs != null) && (paramVarArgs.length > 0)) {}
    for (boolean bool = true;; bool = false)
    {
      zzaa.zzbt(bool);
      return ContextFenceStub.zzh(zza(paramVarArgs));
    }
  }
  
  private static ArrayList<ContextFenceStub> zza(AwarenessFence[] paramArrayOfAwarenessFence)
  {
    ArrayList localArrayList = new ArrayList(paramArrayOfAwarenessFence.length);
    int j = paramArrayOfAwarenessFence.length;
    int i = 0;
    while (i < j)
    {
      localArrayList.add((ContextFenceStub)paramArrayOfAwarenessFence[i]);
      i += 1;
    }
    return localArrayList;
  }
  
  private static ArrayList<ContextFenceStub> zzd(Collection<AwarenessFence> paramCollection)
  {
    ArrayList localArrayList = new ArrayList(paramCollection.size());
    paramCollection = paramCollection.iterator();
    while (paramCollection.hasNext()) {
      localArrayList.add((ContextFenceStub)paramCollection.next());
    }
    return localArrayList;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/awareness/fence/AwarenessFence.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */