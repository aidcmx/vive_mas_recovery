package com.google.android.gms.awareness.fence;

import android.support.annotation.RequiresPermission;
import com.google.android.gms.awareness.state.BeaconState.TypeFilter;
import com.google.android.gms.contextmanager.fence.internal.ContextFenceStub;
import com.google.android.gms.contextmanager.fence.internal.zzd;
import java.util.Collection;

public final class BeaconFence
{
  @RequiresPermission("android.permission.ACCESS_FINE_LOCATION")
  public static AwarenessFence found(Collection<BeaconState.TypeFilter> paramCollection)
  {
    if ((paramCollection == null) || (paramCollection.isEmpty())) {
      return found(new BeaconState.TypeFilter[0]);
    }
    return found((BeaconState.TypeFilter[])paramCollection.toArray(new BeaconState.TypeFilter[paramCollection.size()]));
  }
  
  @RequiresPermission("android.permission.ACCESS_FINE_LOCATION")
  public static AwarenessFence found(BeaconState.TypeFilter... paramVarArgs)
  {
    return ContextFenceStub.zza(zzd.zzb(paramVarArgs));
  }
  
  @RequiresPermission("android.permission.ACCESS_FINE_LOCATION")
  public static AwarenessFence lost(Collection<BeaconState.TypeFilter> paramCollection)
  {
    if ((paramCollection == null) || (paramCollection.isEmpty())) {
      return lost(new BeaconState.TypeFilter[0]);
    }
    return lost((BeaconState.TypeFilter[])paramCollection.toArray(new BeaconState.TypeFilter[paramCollection.size()]));
  }
  
  @RequiresPermission("android.permission.ACCESS_FINE_LOCATION")
  public static AwarenessFence lost(BeaconState.TypeFilter... paramVarArgs)
  {
    return ContextFenceStub.zza(zzd.zzc(paramVarArgs));
  }
  
  @RequiresPermission("android.permission.ACCESS_FINE_LOCATION")
  public static AwarenessFence near(Collection<BeaconState.TypeFilter> paramCollection)
  {
    if ((paramCollection == null) || (paramCollection.isEmpty())) {
      return near(new BeaconState.TypeFilter[0]);
    }
    return near((BeaconState.TypeFilter[])paramCollection.toArray(new BeaconState.TypeFilter[paramCollection.size()]));
  }
  
  @RequiresPermission("android.permission.ACCESS_FINE_LOCATION")
  public static AwarenessFence near(BeaconState.TypeFilter... paramVarArgs)
  {
    return ContextFenceStub.zza(zzd.zzd(paramVarArgs));
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/awareness/fence/BeaconFence.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */