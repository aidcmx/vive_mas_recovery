package com.google.android.gms.awareness.fence;

import android.support.annotation.RequiresPermission;
import com.google.android.gms.contextmanager.fence.internal.ContextFenceStub;
import com.google.android.gms.contextmanager.fence.internal.zza;

public final class DetectedActivityFence
{
  public static final int IN_VEHICLE = 0;
  public static final int ON_BICYCLE = 1;
  public static final int ON_FOOT = 2;
  public static final int RUNNING = 8;
  public static final int STILL = 3;
  public static final int TILTING = 5;
  public static final int UNKNOWN = 4;
  public static final int WALKING = 7;
  
  @RequiresPermission("com.google.android.gms.permission.ACTIVITY_RECOGNITION")
  public static AwarenessFence during(int... paramVarArgs)
  {
    return ContextFenceStub.zza(zza.zzb(paramVarArgs));
  }
  
  @RequiresPermission("com.google.android.gms.permission.ACTIVITY_RECOGNITION")
  public static AwarenessFence starting(int... paramVarArgs)
  {
    return ContextFenceStub.zza(zza.zza(2, paramVarArgs));
  }
  
  @RequiresPermission("com.google.android.gms.permission.ACTIVITY_RECOGNITION")
  public static AwarenessFence stopping(int... paramVarArgs)
  {
    return ContextFenceStub.zza(zza.zza(3, paramVarArgs));
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/awareness/fence/DetectedActivityFence.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */