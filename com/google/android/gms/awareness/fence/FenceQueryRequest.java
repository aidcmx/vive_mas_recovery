package com.google.android.gms.awareness.fence;

import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.contextmanager.fence.internal.FenceQueryRequestImpl;
import java.util.Collection;
import java.util.Iterator;

public abstract class FenceQueryRequest
  extends AbstractSafeParcelable
{
  public static FenceQueryRequest all()
  {
    return new FenceQueryRequestImpl();
  }
  
  public static FenceQueryRequest forFences(Collection<String> paramCollection)
  {
    zzaa.zzy(paramCollection);
    Iterator localIterator = paramCollection.iterator();
    while (localIterator.hasNext()) {
      zzaa.zzib((String)localIterator.next());
    }
    return new FenceQueryRequestImpl(paramCollection);
  }
  
  public static FenceQueryRequest forFences(String... paramVarArgs)
  {
    zzaa.zzy(paramVarArgs);
    int j = paramVarArgs.length;
    int i = 0;
    while (i < j)
    {
      zzaa.zzib(paramVarArgs[i]);
      i += 1;
    }
    return new FenceQueryRequestImpl(paramVarArgs);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/awareness/fence/FenceQueryRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */