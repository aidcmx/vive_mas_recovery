package com.google.android.gms.awareness.fence;

import android.content.Intent;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.contextmanager.fence.internal.FenceStateImpl;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public abstract class FenceState
  extends AbstractSafeParcelable
{
  public static final int FALSE = 1;
  public static final int TRUE = 2;
  public static final int UNKNOWN = 0;
  
  public static FenceState extract(Intent paramIntent)
  {
    return FenceStateImpl.extract(paramIntent);
  }
  
  public abstract int getCurrentState();
  
  public abstract String getFenceKey();
  
  public abstract long getLastFenceUpdateTimeMillis();
  
  public abstract int getPreviousState();
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface State {}
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/awareness/fence/FenceState.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */