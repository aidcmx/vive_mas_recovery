package com.google.android.gms.awareness.fence;

import java.util.Set;

public abstract interface FenceStateMap
{
  public abstract Set<String> getFenceKeys();
  
  public abstract FenceState getFenceState(String paramString);
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/awareness/fence/FenceStateMap.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */