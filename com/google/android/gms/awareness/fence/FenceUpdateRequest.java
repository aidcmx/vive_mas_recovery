package com.google.android.gms.awareness.fence;

import android.app.PendingIntent;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.contextmanager.fence.internal.ContextFenceStub;
import com.google.android.gms.contextmanager.fence.internal.FenceUpdateRequestImpl;
import com.google.android.gms.contextmanager.fence.internal.UpdateFenceOperation;
import java.util.ArrayList;

public abstract interface FenceUpdateRequest
{
  public static class Builder
  {
    private final ArrayList<UpdateFenceOperation> ld = new ArrayList();
    
    public Builder addFence(String paramString, AwarenessFence paramAwarenessFence, PendingIntent paramPendingIntent)
    {
      zzaa.zzib(paramString);
      zzaa.zzy(paramAwarenessFence);
      zzaa.zzy(paramPendingIntent);
      this.ld.add(UpdateFenceOperation.zza(paramString, (ContextFenceStub)paramAwarenessFence, paramPendingIntent));
      return this;
    }
    
    public FenceUpdateRequest build()
    {
      return new FenceUpdateRequestImpl(this.ld);
    }
    
    public Builder removeFence(PendingIntent paramPendingIntent)
    {
      zzaa.zzy(paramPendingIntent);
      this.ld.add(UpdateFenceOperation.zza(paramPendingIntent));
      return this;
    }
    
    public Builder removeFence(String paramString)
    {
      zzaa.zzib(paramString);
      this.ld.add(UpdateFenceOperation.zzim(paramString));
      return this;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/awareness/fence/FenceUpdateRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */