package com.google.android.gms.awareness.fence;

import com.google.android.gms.contextmanager.fence.internal.ContextFenceStub;
import com.google.android.gms.contextmanager.fence.internal.zzb;

public final class HeadphoneFence
{
  public static AwarenessFence during(int paramInt)
  {
    return ContextFenceStub.zza(zzb.zzht(paramInt));
  }
  
  public static AwarenessFence pluggingIn()
  {
    return ContextFenceStub.zza(zzb.zzazz());
  }
  
  public static AwarenessFence unplugging()
  {
    return ContextFenceStub.zza(zzb.zzbaa());
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/awareness/fence/HeadphoneFence.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */