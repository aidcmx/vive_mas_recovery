package com.google.android.gms.awareness.fence;

import android.support.annotation.RequiresPermission;
import com.google.android.gms.contextmanager.fence.internal.ContextFenceStub;
import com.google.android.gms.contextmanager.fence.internal.zzn;

public final class LocationFence
{
  @RequiresPermission("android.permission.ACCESS_FINE_LOCATION")
  public static AwarenessFence entering(double paramDouble1, double paramDouble2, double paramDouble3)
  {
    return ContextFenceStub.zza(zzn.zza((int)(paramDouble1 * 1.0E7D), (int)(1.0E7D * paramDouble2), paramDouble3));
  }
  
  @RequiresPermission("android.permission.ACCESS_FINE_LOCATION")
  public static AwarenessFence exiting(double paramDouble1, double paramDouble2, double paramDouble3)
  {
    return ContextFenceStub.zza(zzn.zzb((int)(paramDouble1 * 1.0E7D), (int)(1.0E7D * paramDouble2), paramDouble3));
  }
  
  @RequiresPermission("android.permission.ACCESS_FINE_LOCATION")
  public static AwarenessFence in(double paramDouble1, double paramDouble2, double paramDouble3, long paramLong)
  {
    return ContextFenceStub.zza(zzn.zza((int)(paramDouble1 * 1.0E7D), (int)(1.0E7D * paramDouble2), paramDouble3, paramLong));
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/awareness/fence/LocationFence.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */