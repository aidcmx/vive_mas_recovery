package com.google.android.gms.awareness.fence;

import com.google.android.gms.contextmanager.fence.internal.ContextFenceStub;
import com.google.android.gms.contextmanager.fence.internal.zzp;
import java.util.TimeZone;

public final class TimeFence
{
  public static AwarenessFence inDailyInterval(TimeZone paramTimeZone, long paramLong1, long paramLong2)
  {
    return ContextFenceStub.zza(zzp.zza(2, paramTimeZone, paramLong1, paramLong2));
  }
  
  public static AwarenessFence inFridayInterval(TimeZone paramTimeZone, long paramLong1, long paramLong2)
  {
    return ContextFenceStub.zza(zzp.zza(10, paramTimeZone, paramLong1, paramLong2));
  }
  
  public static AwarenessFence inInterval(long paramLong1, long paramLong2)
  {
    return ContextFenceStub.zza(zzp.zze(paramLong1, paramLong2));
  }
  
  public static AwarenessFence inMondayInterval(TimeZone paramTimeZone, long paramLong1, long paramLong2)
  {
    return ContextFenceStub.zza(zzp.zza(6, paramTimeZone, paramLong1, paramLong2));
  }
  
  public static AwarenessFence inSaturdayInterval(TimeZone paramTimeZone, long paramLong1, long paramLong2)
  {
    return ContextFenceStub.zza(zzp.zza(11, paramTimeZone, paramLong1, paramLong2));
  }
  
  public static AwarenessFence inSundayInterval(TimeZone paramTimeZone, long paramLong1, long paramLong2)
  {
    return ContextFenceStub.zza(zzp.zza(5, paramTimeZone, paramLong1, paramLong2));
  }
  
  public static AwarenessFence inThursdayInterval(TimeZone paramTimeZone, long paramLong1, long paramLong2)
  {
    return ContextFenceStub.zza(zzp.zza(9, paramTimeZone, paramLong1, paramLong2));
  }
  
  public static AwarenessFence inTuesdayInterval(TimeZone paramTimeZone, long paramLong1, long paramLong2)
  {
    return ContextFenceStub.zza(zzp.zza(7, paramTimeZone, paramLong1, paramLong2));
  }
  
  public static AwarenessFence inWednesdayInterval(TimeZone paramTimeZone, long paramLong1, long paramLong2)
  {
    return ContextFenceStub.zza(zzp.zza(8, paramTimeZone, paramLong1, paramLong2));
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/awareness/fence/TimeFence.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */