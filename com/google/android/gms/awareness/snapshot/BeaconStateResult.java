package com.google.android.gms.awareness.snapshot;

import com.google.android.gms.awareness.state.BeaconState;
import com.google.android.gms.common.api.Result;

public abstract interface BeaconStateResult
  extends Result
{
  public abstract BeaconState getBeaconState();
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/awareness/snapshot/BeaconStateResult.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */