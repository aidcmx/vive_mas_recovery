package com.google.android.gms.awareness.snapshot;

import com.google.android.gms.common.api.Result;
import com.google.android.gms.location.ActivityRecognitionResult;

public abstract interface DetectedActivityResult
  extends Result
{
  public abstract ActivityRecognitionResult getActivityRecognitionResult();
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/awareness/snapshot/DetectedActivityResult.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */