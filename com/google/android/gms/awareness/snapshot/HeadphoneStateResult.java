package com.google.android.gms.awareness.snapshot;

import com.google.android.gms.awareness.state.HeadphoneState;
import com.google.android.gms.common.api.Result;

public abstract interface HeadphoneStateResult
  extends Result
{
  public abstract HeadphoneState getHeadphoneState();
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/awareness/snapshot/HeadphoneStateResult.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */