package com.google.android.gms.awareness.snapshot;

import android.location.Location;
import com.google.android.gms.common.api.Result;

public abstract interface LocationResult
  extends Result
{
  public abstract Location getLocation();
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/awareness/snapshot/LocationResult.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */