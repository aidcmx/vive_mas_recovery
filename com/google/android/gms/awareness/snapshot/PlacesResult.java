package com.google.android.gms.awareness.snapshot;

import com.google.android.gms.common.api.Result;
import com.google.android.gms.location.places.PlaceLikelihood;
import java.util.List;

public abstract interface PlacesResult
  extends Result
{
  public abstract List<PlaceLikelihood> getPlaceLikelihoods();
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/awareness/snapshot/PlacesResult.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */