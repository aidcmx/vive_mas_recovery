package com.google.android.gms.awareness.snapshot;

import com.google.android.gms.awareness.state.Weather;
import com.google.android.gms.common.api.Result;

public abstract interface WeatherResult
  extends Result
{
  public abstract Weather getWeather();
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/awareness/snapshot/WeatherResult.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */