package com.google.android.gms.awareness.snapshot.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.google.android.gms.awareness.state.BeaconState;
import com.google.android.gms.awareness.state.BeaconState.BeaconInfo;
import com.google.android.gms.awareness.state.BeaconState.TypeFilter;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.internal.zzaqw.zza;
import com.google.android.gms.internal.zzarz;
import com.google.android.gms.internal.zzasa;
import com.google.android.gms.internal.zzcd;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public final class BeaconStateImpl
  extends AbstractSafeParcelable
  implements BeaconState
{
  public static final Parcelable.Creator<BeaconStateImpl> CREATOR = new zzb();
  private final ArrayList<BeaconInfoImpl> le;
  private final int mVersionCode;
  
  BeaconStateImpl(int paramInt, ArrayList<BeaconInfoImpl> paramArrayList)
  {
    this.mVersionCode = paramInt;
    this.le = paramArrayList;
  }
  
  public List<BeaconState.BeaconInfo> getBeaconInfo()
  {
    return this.le;
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public String toString()
  {
    if ((this.le == null) || (this.le.isEmpty())) {
      return "BeaconState: empty";
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("BeaconState: ");
    Iterator localIterator = this.le.iterator();
    while (localIterator.hasNext()) {
      localStringBuilder.append((BeaconState.BeaconInfo)localIterator.next());
    }
    return localStringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzb.zza(this, paramParcel, paramInt);
  }
  
  ArrayList<BeaconInfoImpl> zzaju()
  {
    return this.le;
  }
  
  public static final class BeaconInfoImpl
    extends AbstractSafeParcelable
    implements BeaconState.BeaconInfo
  {
    public static final Parcelable.Creator<BeaconInfoImpl> CREATOR = new zza();
    private final String lf;
    private final byte[] lg;
    private final int mVersionCode;
    private final String zzcpo;
    
    BeaconInfoImpl(int paramInt, String paramString1, String paramString2, byte[] paramArrayOfByte)
    {
      this.mVersionCode = paramInt;
      this.lf = paramString1;
      this.zzcpo = paramString2;
      this.lg = paramArrayOfByte;
    }
    
    public byte[] getContent()
    {
      return this.lg;
    }
    
    public String getNamespace()
    {
      return this.lf;
    }
    
    public String getType()
    {
      return this.zzcpo;
    }
    
    int getVersionCode()
    {
      return this.mVersionCode;
    }
    
    public String toString()
    {
      if (this.lg == null) {}
      for (String str1 = "<null>";; str1 = new String(this.lg))
      {
        String str2 = this.lf;
        String str3 = this.zzcpo;
        return String.valueOf(str2).length() + 6 + String.valueOf(str3).length() + String.valueOf(str1).length() + "(" + str2 + ", " + str3 + ", " + str1 + ")";
      }
    }
    
    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
      zza.zza(this, paramParcel, paramInt);
    }
  }
  
  public static class TypeFilterImpl
    extends BeaconState.TypeFilter
  {
    public static final Parcelable.Creator<TypeFilterImpl> CREATOR = new zzn();
    private final zzaqw.zza lh;
    private final int mVersionCode;
    
    TypeFilterImpl(int paramInt, byte[] paramArrayOfByte)
    {
      this.mVersionCode = paramInt;
      Object localObject = null;
      try
      {
        paramArrayOfByte = zzaqw.zza.zzaz(paramArrayOfByte);
        this.lh = paramArrayOfByte;
        return;
      }
      catch (zzarz paramArrayOfByte)
      {
        for (;;)
        {
          zzcd.zzd("BeaconStateImpl", "Could not deserialize BeaconFence.BeaconTypeFilter");
          paramArrayOfByte = (byte[])localObject;
        }
      }
    }
    
    public TypeFilterImpl(String paramString1, String paramString2)
    {
      this.mVersionCode = 1;
      this.lh = new zzaqw.zza();
      this.lh.EY = zzaa.zzib(paramString1);
      this.lh.type = zzaa.zzib(paramString2);
    }
    
    public TypeFilterImpl(String paramString1, String paramString2, byte[] paramArrayOfByte)
    {
      this.mVersionCode = 1;
      this.lh = new zzaqw.zza();
      this.lh.EY = zzaa.zzib(paramString1);
      this.lh.type = zzaa.zzib(paramString2);
      this.lh.content = ((byte[])zzaa.zzy(paramArrayOfByte));
    }
    
    public boolean equals(Object paramObject)
    {
      if (this == paramObject) {}
      do
      {
        return true;
        if (!(paramObject instanceof TypeFilterImpl)) {
          return false;
        }
        paramObject = (TypeFilterImpl)paramObject;
      } while ((TextUtils.equals(getNamespace(), ((TypeFilterImpl)paramObject).getNamespace())) && (TextUtils.equals(getType(), ((TypeFilterImpl)paramObject).getType())) && (Arrays.equals(getContent(), ((TypeFilterImpl)paramObject).getContent())));
      return false;
    }
    
    public byte[] getContent()
    {
      if ((this.lh == null) || (this.lh.content == null) || (this.lh.content.length == 0)) {
        return null;
      }
      return this.lh.content;
    }
    
    public String getNamespace()
    {
      if (this.lh == null) {
        return null;
      }
      return this.lh.EY;
    }
    
    public String getType()
    {
      if (this.lh == null) {
        return null;
      }
      return this.lh.type;
    }
    
    int getVersionCode()
    {
      return this.mVersionCode;
    }
    
    public int hashCode()
    {
      int i = 0;
      String str1 = getNamespace();
      String str2 = getType();
      if (getContent() == null) {}
      for (;;)
      {
        return zzz.hashCode(new Object[] { str1, str2, Integer.valueOf(i) });
        i = Arrays.hashCode(getContent());
      }
    }
    
    public String toString()
    {
      String str2 = String.valueOf(getNamespace());
      String str3 = String.valueOf(getType());
      if (getContent() == null) {}
      for (String str1 = "null";; str1 = new String(getContent())) {
        return String.valueOf(str2).length() + 4 + String.valueOf(str3).length() + String.valueOf(str1).length() + "(" + str2 + "," + str3 + "," + str1 + ")";
      }
    }
    
    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
      zzn.zza(this, paramParcel, paramInt);
    }
    
    public zzaqw.zza zzajv()
    {
      return this.lh;
    }
    
    byte[] zzajw()
    {
      return zzasa.zzf(this.lh);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/awareness/snapshot/internal/BeaconStateImpl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */