package com.google.android.gms.awareness.snapshot.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public class DayAttributesImpl
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<DayAttributesImpl> CREATOR = new zzc();
  private final int[] li;
  private final int mVersionCode;
  
  DayAttributesImpl(int paramInt, int[] paramArrayOfInt)
  {
    this.mVersionCode = paramInt;
    this.li = paramArrayOfInt;
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Attributes=");
    if (this.li == null) {
      localStringBuilder.append("unknown");
    }
    for (;;)
    {
      return localStringBuilder.toString();
      localStringBuilder.append("[");
      int[] arrayOfInt = this.li;
      int k = arrayOfInt.length;
      int j = 1;
      int i = 0;
      while (i < k)
      {
        int m = arrayOfInt[i];
        if (j == 0) {
          localStringBuilder.append(", ");
        }
        localStringBuilder.append(m);
        i += 1;
        j = 0;
      }
      localStringBuilder.append("]");
    }
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzc.zza(this, paramParcel, paramInt);
  }
  
  public int[] zzajx()
  {
    return this.li;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/awareness/snapshot/internal/DayAttributesImpl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */