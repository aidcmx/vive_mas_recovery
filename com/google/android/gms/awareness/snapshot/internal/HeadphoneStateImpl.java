package com.google.android.gms.awareness.snapshot.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.awareness.state.HeadphoneState;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public final class HeadphoneStateImpl
  extends AbstractSafeParcelable
  implements HeadphoneState
{
  public static final Parcelable.Creator<HeadphoneStateImpl> CREATOR = new zzd();
  private final int lj;
  private final int mVersionCode;
  
  HeadphoneStateImpl(int paramInt1, int paramInt2)
  {
    this.mVersionCode = paramInt1;
    this.lj = paramInt2;
  }
  
  public int getState()
  {
    return this.lj;
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public String toString()
  {
    return Integer.toString(this.lj);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzd.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/awareness/snapshot/internal/HeadphoneStateImpl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */