package com.google.android.gms.awareness.snapshot.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public class NetworkStateImpl
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<NetworkStateImpl> CREATOR = new zze();
  private final int lk;
  private final int ll;
  private final int mVersionCode;
  
  NetworkStateImpl(int paramInt1, int paramInt2, int paramInt3)
  {
    this.mVersionCode = paramInt1;
    this.lk = paramInt2;
    this.ll = paramInt3;
  }
  
  public static String zzds(int paramInt)
  {
    return Integer.toString(paramInt);
  }
  
  public static String zzdt(int paramInt)
  {
    return Integer.toString(paramInt);
  }
  
  public int getConnectionState()
  {
    return this.lk;
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public String toString()
  {
    String str1 = String.valueOf(zzds(this.lk));
    String str2 = String.valueOf(zzdt(this.ll));
    return String.valueOf(str1).length() + 41 + String.valueOf(str2).length() + "ConnectionState = " + str1 + " NetworkMeteredState = " + str2;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zze.zza(this, paramParcel, paramInt);
  }
  
  int zzajy()
  {
    return this.ll;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/awareness/snapshot/internal/NetworkStateImpl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */