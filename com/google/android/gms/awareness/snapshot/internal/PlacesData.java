package com.google.android.gms.awareness.snapshot.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.location.places.PlaceLikelihood;
import com.google.android.gms.location.places.internal.PlaceLikelihoodEntity;
import java.util.ArrayList;
import java.util.List;

public final class PlacesData
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<PlacesData> CREATOR = new zzg();
  private final ArrayList<PlaceLikelihoodEntity> lm;
  private final int mVersionCode;
  
  PlacesData(int paramInt, ArrayList<PlaceLikelihoodEntity> paramArrayList)
  {
    this.mVersionCode = paramInt;
    this.lm = paramArrayList;
  }
  
  public List<PlaceLikelihood> getPlaceLikelihoods()
  {
    return this.lm;
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzg.zza(this, paramParcel, paramInt);
  }
  
  ArrayList<PlaceLikelihoodEntity> zzajz()
  {
    return this.lm;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/awareness/snapshot/internal/PlacesData.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */