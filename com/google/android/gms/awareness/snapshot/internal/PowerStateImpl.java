package com.google.android.gms.awareness.snapshot.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public class PowerStateImpl
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<PowerStateImpl> CREATOR = new zzh();
  private final int ln;
  private final double lo;
  private final int mVersionCode;
  
  PowerStateImpl(int paramInt1, int paramInt2, double paramDouble)
  {
    this.mVersionCode = paramInt1;
    this.ln = paramInt2;
    this.lo = paramDouble;
  }
  
  public static String zzdw(int paramInt)
  {
    return Integer.toString(paramInt);
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public String toString()
  {
    String str = String.valueOf(zzdw(this.ln));
    double d = this.lo;
    return String.valueOf(str).length() + 69 + "PowerConnectionState = " + str + " Battery Percentage = " + d;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzh.zza(this, paramParcel, paramInt);
  }
  
  public int zzaka()
  {
    return this.ln;
  }
  
  public double zzakb()
  {
    return this.lo;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/awareness/snapshot/internal/PowerStateImpl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */