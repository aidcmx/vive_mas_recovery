package com.google.android.gms.awareness.snapshot.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public final class ScreenStateImpl
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<ScreenStateImpl> CREATOR = new zzi();
  private final int lp;
  private final int mVersionCode;
  
  ScreenStateImpl(int paramInt1, int paramInt2)
  {
    this.mVersionCode = paramInt1;
    this.lp = paramInt2;
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public String toString()
  {
    if (this.lp == 1) {
      return "ScreenState: SCREEN_OFF";
    }
    if (this.lp == 2) {
      return "ScreenState: SCREEN_ON";
    }
    return "ScreenState: UNKNOWN";
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzi.zza(this, paramParcel, paramInt);
  }
  
  public int zzakc()
  {
    return this.lp;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/awareness/snapshot/internal/ScreenStateImpl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */