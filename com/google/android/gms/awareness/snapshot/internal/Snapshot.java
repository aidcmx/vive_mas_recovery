package com.google.android.gms.awareness.snapshot.internal;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.location.ActivityRecognitionResult;

public final class Snapshot
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<Snapshot> CREATOR = new zzk();
  private final ActivityRecognitionResult lq;
  private final BeaconStateImpl lr;
  private final HeadphoneStateImpl ls;
  private final NetworkStateImpl lt;
  private final DataHolder lu;
  private final PowerStateImpl lv;
  private final ScreenStateImpl lw;
  private final WeatherImpl lx;
  private final DayAttributesImpl ly;
  private final int mVersionCode;
  private final Location zzgv;
  
  Snapshot(int paramInt, ActivityRecognitionResult paramActivityRecognitionResult, BeaconStateImpl paramBeaconStateImpl, HeadphoneStateImpl paramHeadphoneStateImpl, Location paramLocation, NetworkStateImpl paramNetworkStateImpl, DataHolder paramDataHolder, PowerStateImpl paramPowerStateImpl, ScreenStateImpl paramScreenStateImpl, WeatherImpl paramWeatherImpl, DayAttributesImpl paramDayAttributesImpl)
  {
    this.mVersionCode = paramInt;
    this.lq = paramActivityRecognitionResult;
    this.lr = paramBeaconStateImpl;
    this.ls = paramHeadphoneStateImpl;
    this.zzgv = paramLocation;
    this.lt = paramNetworkStateImpl;
    this.lu = paramDataHolder;
    this.lv = paramPowerStateImpl;
    this.lw = paramScreenStateImpl;
    this.lx = paramWeatherImpl;
    this.ly = paramDayAttributesImpl;
  }
  
  public ActivityRecognitionResult getActivityRecognitionResult()
  {
    return this.lq;
  }
  
  public Location getLocation()
  {
    return this.zzgv;
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzk.zza(this, paramParcel, paramInt);
  }
  
  public BeaconStateImpl zzakd()
  {
    return this.lr;
  }
  
  public HeadphoneStateImpl zzake()
  {
    return this.ls;
  }
  
  public NetworkStateImpl zzakf()
  {
    return this.lt;
  }
  
  public DataHolder zzakg()
  {
    return this.lu;
  }
  
  public PowerStateImpl zzakh()
  {
    return this.lv;
  }
  
  public ScreenStateImpl zzaki()
  {
    return this.lw;
  }
  
  public WeatherImpl zzakj()
  {
    return this.lx;
  }
  
  public DayAttributesImpl zzakk()
  {
    return this.ly;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/awareness/snapshot/internal/Snapshot.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */