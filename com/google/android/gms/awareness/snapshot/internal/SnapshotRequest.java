package com.google.android.gms.awareness.snapshot.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import java.util.ArrayList;
import java.util.Iterator;

public class SnapshotRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<SnapshotRequest> CREATOR = new zzl();
  private static final int[] lL = { 10002, 10003, 10004, 10005, 10006, 10007, 10008 };
  private final int lM;
  private final ArrayList<BeaconStateImpl.TypeFilterImpl> lN;
  private final int mVersionCode;
  
  SnapshotRequest(int paramInt1, int paramInt2, ArrayList<BeaconStateImpl.TypeFilterImpl> paramArrayList)
  {
    this.mVersionCode = paramInt1;
    this.lM = paramInt2;
    this.lN = paramArrayList;
  }
  
  public SnapshotRequest(int paramInt, ArrayList<BeaconStateImpl.TypeFilterImpl> paramArrayList)
  {
    this(1, paramInt, paramArrayList);
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = false;
    boolean bool1;
    if (this == paramObject) {
      bool1 = true;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (!(paramObject instanceof SnapshotRequest));
      paramObject = (SnapshotRequest)paramObject;
      bool1 = bool2;
    } while (this.lM != ((SnapshotRequest)paramObject).zzakl());
    int i;
    if (this.lN == null)
    {
      i = 1;
      label54:
      if (((SnapshotRequest)paramObject).zzakm() != null) {
        break label151;
      }
    }
    label151:
    for (int j = 1;; j = 0)
    {
      bool1 = bool2;
      if ((i ^ j) != 0) {
        break;
      }
      if (this.lN == null) {
        break label156;
      }
      bool1 = bool2;
      if (this.lN.size() != ((SnapshotRequest)paramObject).zzakm().size()) {
        break;
      }
      Iterator localIterator = this.lN.iterator();
      BeaconStateImpl.TypeFilterImpl localTypeFilterImpl;
      do
      {
        if (!localIterator.hasNext()) {
          break;
        }
        localTypeFilterImpl = (BeaconStateImpl.TypeFilterImpl)localIterator.next();
      } while (((SnapshotRequest)paramObject).zzakm().contains(localTypeFilterImpl));
      return false;
      i = 0;
      break label54;
    }
    label156:
    return true;
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    if (this.lN != null)
    {
      Iterator localIterator = this.lN.iterator();
      for (int i = 0;; i = ((BeaconStateImpl.TypeFilterImpl)localIterator.next()).hashCode() * 13 + i)
      {
        j = i;
        if (!localIterator.hasNext()) {
          break;
        }
      }
    }
    int j = 0;
    return zzz.hashCode(new Object[] { Integer.valueOf(this.lM), Integer.valueOf(j) });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzl.zza(this, paramParcel, paramInt);
  }
  
  public int zzakl()
  {
    return this.lM;
  }
  
  public ArrayList<BeaconStateImpl.TypeFilterImpl> zzakm()
  {
    return this.lN;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/awareness/snapshot/internal/SnapshotRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */