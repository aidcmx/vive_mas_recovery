package com.google.android.gms.awareness.snapshot.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.awareness.state.Weather;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.internal.zzcd;

public class WeatherImpl
  extends AbstractSafeParcelable
  implements Weather
{
  public static final Parcelable.Creator<WeatherImpl> CREATOR = new zzo();
  private final float lO;
  private final float lP;
  private final float lQ;
  private final int lR;
  private final int[] lS;
  private final int mVersionCode;
  
  WeatherImpl(int paramInt1, float paramFloat1, float paramFloat2, float paramFloat3, int paramInt2, int[] paramArrayOfInt)
  {
    this.mVersionCode = paramInt1;
    this.lO = paramFloat1;
    this.lP = paramFloat2;
    this.lQ = paramFloat3;
    this.lR = paramInt2;
    this.lS = paramArrayOfInt;
  }
  
  static float zza(int paramInt, float paramFloat)
  {
    float f = paramFloat;
    switch (paramInt)
    {
    default: 
      zzcd.zza("WeatherImpl", "Invalid temperature unit %s", Integer.valueOf(paramInt));
      throw new IllegalArgumentException("Invalid temperature unit");
    case 2: 
      f = zzc(paramFloat);
    }
    return f;
  }
  
  private static float zzc(float paramFloat)
  {
    return 5.0F * (paramFloat - 32.0F) / 9.0F;
  }
  
  public int[] getConditions()
  {
    return this.lS;
  }
  
  public float getDewPoint(int paramInt)
  {
    return zza(paramInt, this.lQ);
  }
  
  public float getFeelsLikeTemperature(int paramInt)
  {
    return zza(paramInt, this.lP);
  }
  
  public int getHumidity()
  {
    return this.lR;
  }
  
  public float getTemperature(int paramInt)
  {
    return zza(paramInt, this.lO);
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Temp=").append(getTemperature(1)).append("F/").append(getTemperature(2)).append("C, Feels=").append(getFeelsLikeTemperature(1)).append("F/").append(getFeelsLikeTemperature(2)).append("C, Dew=").append(getDewPoint(1)).append("F/").append(getDewPoint(2)).append("C, Humidity=").append(getHumidity()).append(", Condition=");
    if (getConditions() == null) {
      localStringBuilder.append("unknown");
    }
    for (;;)
    {
      return localStringBuilder.toString();
      localStringBuilder.append("[");
      int[] arrayOfInt = getConditions();
      int k = arrayOfInt.length;
      int j = 1;
      int i = 0;
      while (i < k)
      {
        int m = arrayOfInt[i];
        if (j == 0) {
          localStringBuilder.append(",");
        }
        localStringBuilder.append(m);
        i += 1;
        j = 0;
      }
      localStringBuilder.append("]");
    }
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzo.zza(this, paramParcel, paramInt);
  }
  
  public float zzako()
  {
    return this.lQ;
  }
  
  public float zzakp()
  {
    return this.lP;
  }
  
  public float zzakq()
  {
    return this.lO;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/awareness/snapshot/internal/WeatherImpl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */