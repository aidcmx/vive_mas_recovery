package com.google.android.gms.awareness.snapshot.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zza
  implements Parcelable.Creator<BeaconStateImpl.BeaconInfoImpl>
{
  static void zza(BeaconStateImpl.BeaconInfoImpl paramBeaconInfoImpl, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramBeaconInfoImpl.getVersionCode());
    zzb.zza(paramParcel, 2, paramBeaconInfoImpl.getNamespace(), false);
    zzb.zza(paramParcel, 3, paramBeaconInfoImpl.getType(), false);
    zzb.zza(paramParcel, 4, paramBeaconInfoImpl.getContent(), false);
    zzb.zzaj(paramParcel, paramInt);
  }
  
  public BeaconStateImpl.BeaconInfoImpl zzaz(Parcel paramParcel)
  {
    byte[] arrayOfByte = null;
    int j = com.google.android.gms.common.internal.safeparcel.zza.zzcr(paramParcel);
    int i = 0;
    String str2 = null;
    String str1 = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = com.google.android.gms.common.internal.safeparcel.zza.zzcq(paramParcel);
      switch (com.google.android.gms.common.internal.safeparcel.zza.zzgu(k))
      {
      default: 
        com.google.android.gms.common.internal.safeparcel.zza.zzb(paramParcel, k);
        break;
      case 1: 
        i = com.google.android.gms.common.internal.safeparcel.zza.zzg(paramParcel, k);
        break;
      case 2: 
        str1 = com.google.android.gms.common.internal.safeparcel.zza.zzq(paramParcel, k);
        break;
      case 3: 
        str2 = com.google.android.gms.common.internal.safeparcel.zza.zzq(paramParcel, k);
        break;
      case 4: 
        arrayOfByte = com.google.android.gms.common.internal.safeparcel.zza.zzt(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new BeaconStateImpl.BeaconInfoImpl(i, str1, str2, arrayOfByte);
  }
  
  public BeaconStateImpl.BeaconInfoImpl[] zzdo(int paramInt)
  {
    return new BeaconStateImpl.BeaconInfoImpl[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/awareness/snapshot/internal/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */