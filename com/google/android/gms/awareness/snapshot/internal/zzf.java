package com.google.android.gms.awareness.snapshot.internal;

import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.zzd;

public class zzf
  extends zzd<PlacesData>
{
  public zzf(DataHolder paramDataHolder)
  {
    super(paramDataHolder, PlacesData.CREATOR);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/awareness/snapshot/internal/zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */