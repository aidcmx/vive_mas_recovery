package com.google.android.gms.awareness.snapshot.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzh
  implements Parcelable.Creator<PowerStateImpl>
{
  static void zza(PowerStateImpl paramPowerStateImpl, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramPowerStateImpl.getVersionCode());
    zzb.zzc(paramParcel, 2, paramPowerStateImpl.zzaka());
    zzb.zza(paramParcel, 3, paramPowerStateImpl.zzakb());
    zzb.zzaj(paramParcel, paramInt);
  }
  
  public PowerStateImpl zzbf(Parcel paramParcel)
  {
    int j = 0;
    int k = zza.zzcr(paramParcel);
    double d = 0.0D;
    int i = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.zzcq(paramParcel);
      switch (zza.zzgu(m))
      {
      default: 
        zza.zzb(paramParcel, m);
        break;
      case 1: 
        i = zza.zzg(paramParcel, m);
        break;
      case 2: 
        j = zza.zzg(paramParcel, m);
        break;
      case 3: 
        d = zza.zzn(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza(37 + "Overread allowed size end=" + k, paramParcel);
    }
    return new PowerStateImpl(i, j, d);
  }
  
  public PowerStateImpl[] zzdx(int paramInt)
  {
    return new PowerStateImpl[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/awareness/snapshot/internal/zzh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */