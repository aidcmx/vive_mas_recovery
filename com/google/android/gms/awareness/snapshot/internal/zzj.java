package com.google.android.gms.awareness.snapshot.internal;

import android.location.Location;
import android.os.RemoteException;
import com.google.android.gms.awareness.SnapshotApi;
import com.google.android.gms.awareness.snapshot.BeaconStateResult;
import com.google.android.gms.awareness.snapshot.DetectedActivityResult;
import com.google.android.gms.awareness.snapshot.HeadphoneStateResult;
import com.google.android.gms.awareness.snapshot.LocationResult;
import com.google.android.gms.awareness.snapshot.PlacesResult;
import com.google.android.gms.awareness.snapshot.WeatherResult;
import com.google.android.gms.awareness.state.BeaconState;
import com.google.android.gms.awareness.state.BeaconState.TypeFilter;
import com.google.android.gms.awareness.state.HeadphoneState;
import com.google.android.gms.awareness.state.Weather;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.contextmanager.internal.zzc.zzb;
import com.google.android.gms.internal.zzcc;
import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.places.PlaceLikelihood;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class zzj
  implements SnapshotApi
{
  private PendingResult<BeaconStateResult> zza(GoogleApiClient paramGoogleApiClient, ArrayList<BeaconStateImpl.TypeFilterImpl> paramArrayList)
  {
    new zzcc(paramGoogleApiClient.zza(zza(paramGoogleApiClient, 10003, paramArrayList)))
    {
      protected BeaconStateResult zzf(final zzm paramAnonymouszzm)
      {
        new BeaconStateResult()
        {
          public BeaconState getBeaconState()
          {
            if (paramAnonymouszzm.zzakn() == null) {
              return null;
            }
            return paramAnonymouszzm.zzakn().zzakd();
          }
          
          public Status getStatus()
          {
            return paramAnonymouszzm.getStatus();
          }
        };
      }
    };
  }
  
  private static zzc.zzb zza(GoogleApiClient paramGoogleApiClient, final int paramInt)
  {
    new zzc.zzb(paramGoogleApiClient)
    {
      protected void zza(com.google.android.gms.contextmanager.internal.zzd paramAnonymouszzd)
        throws RemoteException
      {
        paramAnonymouszzd.zza(this, new SnapshotRequest(paramInt, null));
      }
    };
  }
  
  private static zzc.zzb zza(GoogleApiClient paramGoogleApiClient, final int paramInt, final ArrayList<BeaconStateImpl.TypeFilterImpl> paramArrayList)
  {
    new zzc.zzb(paramGoogleApiClient)
    {
      protected void zza(com.google.android.gms.contextmanager.internal.zzd paramAnonymouszzd)
        throws RemoteException
      {
        paramAnonymouszzd.zza(this, new SnapshotRequest(paramInt, paramArrayList));
      }
    };
  }
  
  private static ArrayList<BeaconStateImpl.TypeFilterImpl> zza(BeaconState.TypeFilter... paramVarArgs)
  {
    int i = 0;
    zzaa.zzb(paramVarArgs, "beaconTypes cannot be null");
    if (paramVarArgs.length > 0) {}
    ArrayList localArrayList;
    for (boolean bool = true;; bool = false)
    {
      zzaa.zzb(bool, "beaconTypes must not be empty");
      localArrayList = new ArrayList();
      int j = paramVarArgs.length;
      while (i < j)
      {
        localArrayList.add((BeaconStateImpl.TypeFilterImpl)paramVarArgs[i]);
        i += 1;
      }
    }
    return localArrayList;
  }
  
  private static ArrayList<BeaconStateImpl.TypeFilterImpl> zze(Collection<BeaconState.TypeFilter> paramCollection)
  {
    zzaa.zzb(paramCollection, "beaconTypes cannot be null");
    if (paramCollection.size() > 0) {}
    ArrayList localArrayList;
    for (boolean bool = true;; bool = false)
    {
      zzaa.zzb(bool, "beaconTypes must not be empty");
      localArrayList = new ArrayList();
      paramCollection = paramCollection.iterator();
      while (paramCollection.hasNext()) {
        localArrayList.add((BeaconStateImpl.TypeFilterImpl)paramCollection.next());
      }
    }
    return localArrayList;
  }
  
  public PendingResult<BeaconStateResult> getBeaconState(GoogleApiClient paramGoogleApiClient, Collection<BeaconState.TypeFilter> paramCollection)
  {
    return zza(paramGoogleApiClient, zze(paramCollection));
  }
  
  public PendingResult<BeaconStateResult> getBeaconState(GoogleApiClient paramGoogleApiClient, BeaconState.TypeFilter... paramVarArgs)
  {
    return zza(paramGoogleApiClient, zza(paramVarArgs));
  }
  
  public PendingResult<DetectedActivityResult> getDetectedActivity(GoogleApiClient paramGoogleApiClient)
  {
    new zzcc(paramGoogleApiClient.zza(zza(paramGoogleApiClient, 10002)))
    {
      protected DetectedActivityResult zza(final zzm paramAnonymouszzm)
      {
        new DetectedActivityResult()
        {
          public ActivityRecognitionResult getActivityRecognitionResult()
          {
            if (paramAnonymouszzm.zzakn() == null) {
              return null;
            }
            return paramAnonymouszzm.zzakn().getActivityRecognitionResult();
          }
          
          public Status getStatus()
          {
            return paramAnonymouszzm.getStatus();
          }
        };
      }
    };
  }
  
  public PendingResult<HeadphoneStateResult> getHeadphoneState(GoogleApiClient paramGoogleApiClient)
  {
    new zzcc(paramGoogleApiClient.zza(zza(paramGoogleApiClient, 10004)))
    {
      protected HeadphoneStateResult zzb(final zzm paramAnonymouszzm)
      {
        new HeadphoneStateResult()
        {
          public HeadphoneState getHeadphoneState()
          {
            if (paramAnonymouszzm.zzakn() == null) {
              return null;
            }
            return paramAnonymouszzm.zzakn().zzake();
          }
          
          public Status getStatus()
          {
            return paramAnonymouszzm.getStatus();
          }
        };
      }
    };
  }
  
  public PendingResult<LocationResult> getLocation(GoogleApiClient paramGoogleApiClient)
  {
    new zzcc(paramGoogleApiClient.zza(zza(paramGoogleApiClient, 10005)))
    {
      protected LocationResult zzc(final zzm paramAnonymouszzm)
      {
        new LocationResult()
        {
          public Location getLocation()
          {
            if (paramAnonymouszzm.zzakn() == null) {
              return null;
            }
            return paramAnonymouszzm.zzakn().getLocation();
          }
          
          public Status getStatus()
          {
            return paramAnonymouszzm.getStatus();
          }
        };
      }
    };
  }
  
  public PendingResult<PlacesResult> getPlaces(GoogleApiClient paramGoogleApiClient)
  {
    new zzcc(paramGoogleApiClient.zza(zza(paramGoogleApiClient, 10006)))
    {
      protected PlacesResult zzd(final zzm paramAnonymouszzm)
      {
        new PlacesResult()
        {
          private boolean lE = false;
          private List<PlaceLikelihood> lF = null;
          
          public List<PlaceLikelihood> getPlaceLikelihoods()
          {
            List localList = null;
            if (this.lE) {
              localObject1 = this.lF;
            }
            DataHolder localDataHolder;
            do
            {
              do
              {
                return (List<PlaceLikelihood>)localObject1;
                this.lE = true;
                localObject1 = localList;
              } while (paramAnonymouszzm.zzakn() == null);
              localDataHolder = paramAnonymouszzm.zzakn().zzakg();
              localObject1 = localList;
            } while (localDataHolder == null);
            Object localObject1 = new zzf(localDataHolder);
            try
            {
              int i = ((zzf)localObject1).getCount();
              if (i <= 0) {
                return null;
              }
              this.lF = ((PlacesData)((com.google.android.gms.common.data.zzd)localObject1).get(0)).getPlaceLikelihoods();
              localList = this.lF;
              return localList;
            }
            finally
            {
              ((zzf)localObject1).release();
            }
          }
          
          public Status getStatus()
          {
            return paramAnonymouszzm.getStatus();
          }
        };
      }
    };
  }
  
  public PendingResult<WeatherResult> getWeather(GoogleApiClient paramGoogleApiClient)
  {
    new zzcc(paramGoogleApiClient.zza(zza(paramGoogleApiClient, 10007)))
    {
      protected WeatherResult zze(final zzm paramAnonymouszzm)
      {
        new WeatherResult()
        {
          public Status getStatus()
          {
            return paramAnonymouszzm.getStatus();
          }
          
          public Weather getWeather()
          {
            if (paramAnonymouszzm.zzakn() == null) {
              return null;
            }
            return paramAnonymouszzm.zzakn().zzakj();
          }
        };
      }
    };
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/awareness/snapshot/internal/zzj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */