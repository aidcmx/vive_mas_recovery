package com.google.android.gms.awareness.snapshot.internal;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.location.ActivityRecognitionResult;

public class zzk
  implements Parcelable.Creator<Snapshot>
{
  static void zza(Snapshot paramSnapshot, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramSnapshot.getVersionCode());
    zzb.zza(paramParcel, 2, paramSnapshot.getActivityRecognitionResult(), paramInt, false);
    zzb.zza(paramParcel, 3, paramSnapshot.zzakd(), paramInt, false);
    zzb.zza(paramParcel, 4, paramSnapshot.zzake(), paramInt, false);
    zzb.zza(paramParcel, 5, paramSnapshot.getLocation(), paramInt, false);
    zzb.zza(paramParcel, 6, paramSnapshot.zzakf(), paramInt, false);
    zzb.zza(paramParcel, 7, paramSnapshot.zzakg(), paramInt, false);
    zzb.zza(paramParcel, 8, paramSnapshot.zzakh(), paramInt, false);
    zzb.zza(paramParcel, 9, paramSnapshot.zzaki(), paramInt, false);
    zzb.zza(paramParcel, 10, paramSnapshot.zzakj(), paramInt, false);
    zzb.zza(paramParcel, 11, paramSnapshot.zzakk(), paramInt, false);
    zzb.zzaj(paramParcel, i);
  }
  
  public Snapshot zzbh(Parcel paramParcel)
  {
    DayAttributesImpl localDayAttributesImpl = null;
    int j = zza.zzcr(paramParcel);
    int i = 0;
    WeatherImpl localWeatherImpl = null;
    ScreenStateImpl localScreenStateImpl = null;
    PowerStateImpl localPowerStateImpl = null;
    DataHolder localDataHolder = null;
    NetworkStateImpl localNetworkStateImpl = null;
    Location localLocation = null;
    HeadphoneStateImpl localHeadphoneStateImpl = null;
    BeaconStateImpl localBeaconStateImpl = null;
    ActivityRecognitionResult localActivityRecognitionResult = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.zzcq(paramParcel);
      switch (zza.zzgu(k))
      {
      default: 
        zza.zzb(paramParcel, k);
        break;
      case 1: 
        i = zza.zzg(paramParcel, k);
        break;
      case 2: 
        localActivityRecognitionResult = (ActivityRecognitionResult)zza.zza(paramParcel, k, ActivityRecognitionResult.CREATOR);
        break;
      case 3: 
        localBeaconStateImpl = (BeaconStateImpl)zza.zza(paramParcel, k, BeaconStateImpl.CREATOR);
        break;
      case 4: 
        localHeadphoneStateImpl = (HeadphoneStateImpl)zza.zza(paramParcel, k, HeadphoneStateImpl.CREATOR);
        break;
      case 5: 
        localLocation = (Location)zza.zza(paramParcel, k, Location.CREATOR);
        break;
      case 6: 
        localNetworkStateImpl = (NetworkStateImpl)zza.zza(paramParcel, k, NetworkStateImpl.CREATOR);
        break;
      case 7: 
        localDataHolder = (DataHolder)zza.zza(paramParcel, k, DataHolder.CREATOR);
        break;
      case 8: 
        localPowerStateImpl = (PowerStateImpl)zza.zza(paramParcel, k, PowerStateImpl.CREATOR);
        break;
      case 9: 
        localScreenStateImpl = (ScreenStateImpl)zza.zza(paramParcel, k, ScreenStateImpl.CREATOR);
        break;
      case 10: 
        localWeatherImpl = (WeatherImpl)zza.zza(paramParcel, k, WeatherImpl.CREATOR);
        break;
      case 11: 
        localDayAttributesImpl = (DayAttributesImpl)zza.zza(paramParcel, k, DayAttributesImpl.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new Snapshot(i, localActivityRecognitionResult, localBeaconStateImpl, localHeadphoneStateImpl, localLocation, localNetworkStateImpl, localDataHolder, localPowerStateImpl, localScreenStateImpl, localWeatherImpl, localDayAttributesImpl);
  }
  
  public Snapshot[] zzdz(int paramInt)
  {
    return new Snapshot[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/awareness/snapshot/internal/zzk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */