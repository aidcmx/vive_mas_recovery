package com.google.android.gms.awareness.snapshot.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzn
  implements Parcelable.Creator<BeaconStateImpl.TypeFilterImpl>
{
  static void zza(BeaconStateImpl.TypeFilterImpl paramTypeFilterImpl, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramTypeFilterImpl.getVersionCode());
    zzb.zza(paramParcel, 2, paramTypeFilterImpl.zzajw(), false);
    zzb.zzaj(paramParcel, paramInt);
  }
  
  public BeaconStateImpl.TypeFilterImpl zzbj(Parcel paramParcel)
  {
    int j = zza.zzcr(paramParcel);
    int i = 0;
    byte[] arrayOfByte = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.zzcq(paramParcel);
      switch (zza.zzgu(k))
      {
      default: 
        zza.zzb(paramParcel, k);
        break;
      case 1: 
        i = zza.zzg(paramParcel, k);
        break;
      case 2: 
        arrayOfByte = zza.zzt(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new BeaconStateImpl.TypeFilterImpl(i, arrayOfByte);
  }
  
  public BeaconStateImpl.TypeFilterImpl[] zzeb(int paramInt)
  {
    return new BeaconStateImpl.TypeFilterImpl[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/awareness/snapshot/internal/zzn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */