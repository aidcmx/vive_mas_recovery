package com.google.android.gms.awareness.snapshot.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzo
  implements Parcelable.Creator<WeatherImpl>
{
  static void zza(WeatherImpl paramWeatherImpl, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramWeatherImpl.getVersionCode());
    zzb.zza(paramParcel, 2, paramWeatherImpl.zzakq());
    zzb.zza(paramParcel, 3, paramWeatherImpl.zzakp());
    zzb.zza(paramParcel, 4, paramWeatherImpl.zzako());
    zzb.zzc(paramParcel, 5, paramWeatherImpl.getHumidity());
    zzb.zza(paramParcel, 6, paramWeatherImpl.getConditions(), false);
    zzb.zzaj(paramParcel, paramInt);
  }
  
  public WeatherImpl zzbk(Parcel paramParcel)
  {
    int i = 0;
    float f1 = 0.0F;
    int k = zza.zzcr(paramParcel);
    int[] arrayOfInt = null;
    float f2 = 0.0F;
    float f3 = 0.0F;
    int j = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.zzcq(paramParcel);
      switch (zza.zzgu(m))
      {
      default: 
        zza.zzb(paramParcel, m);
        break;
      case 1: 
        j = zza.zzg(paramParcel, m);
        break;
      case 2: 
        f3 = zza.zzl(paramParcel, m);
        break;
      case 3: 
        f2 = zza.zzl(paramParcel, m);
        break;
      case 4: 
        f1 = zza.zzl(paramParcel, m);
        break;
      case 5: 
        i = zza.zzg(paramParcel, m);
        break;
      case 6: 
        arrayOfInt = zza.zzw(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza(37 + "Overread allowed size end=" + k, paramParcel);
    }
    return new WeatherImpl(j, f3, f2, f1, i, arrayOfInt);
  }
  
  public WeatherImpl[] zzec(int paramInt)
  {
    return new WeatherImpl[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/awareness/snapshot/internal/zzo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */