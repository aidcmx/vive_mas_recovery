package com.google.android.gms.awareness.state;

import com.google.android.gms.awareness.snapshot.internal.BeaconStateImpl.TypeFilterImpl;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.util.List;

public abstract interface BeaconState
{
  public abstract List<BeaconInfo> getBeaconInfo();
  
  public static abstract interface BeaconInfo
  {
    public abstract byte[] getContent();
    
    public abstract String getNamespace();
    
    public abstract String getType();
  }
  
  public static abstract class TypeFilter
    extends AbstractSafeParcelable
  {
    public static TypeFilter with(String paramString1, String paramString2)
    {
      return new BeaconStateImpl.TypeFilterImpl(paramString1, paramString2);
    }
    
    public static TypeFilter with(String paramString1, String paramString2, byte[] paramArrayOfByte)
    {
      return new BeaconStateImpl.TypeFilterImpl(paramString1, paramString2, paramArrayOfByte);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/awareness/state/BeaconState.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */