package com.google.android.gms.awareness.state;

import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public abstract interface HeadphoneState
{
  public static final int PLUGGED_IN = 1;
  public static final int UNPLUGGED = 2;
  
  public abstract int getState();
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface State {}
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/awareness/state/HeadphoneState.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */