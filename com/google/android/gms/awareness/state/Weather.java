package com.google.android.gms.awareness.state;

import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public abstract interface Weather
{
  public static final int CELSIUS = 2;
  public static final int CONDITION_CLEAR = 1;
  public static final int CONDITION_CLOUDY = 2;
  public static final int CONDITION_FOGGY = 3;
  public static final int CONDITION_HAZY = 4;
  public static final int CONDITION_ICY = 5;
  public static final int CONDITION_RAINY = 6;
  public static final int CONDITION_SNOWY = 7;
  public static final int CONDITION_STORMY = 8;
  public static final int CONDITION_UNKNOWN = 0;
  public static final int CONDITION_WINDY = 9;
  public static final int FAHRENHEIT = 1;
  
  public abstract int[] getConditions();
  
  public abstract float getDewPoint(int paramInt);
  
  public abstract float getFeelsLikeTemperature(int paramInt);
  
  public abstract int getHumidity();
  
  public abstract float getTemperature(int paramInt);
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface TemperatureUnit {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface WeatherCondition {}
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/awareness/state/Weather.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */