package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public class AdBreakInfo
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<AdBreakInfo> CREATOR = new zza();
  private final long lT;
  private final int mVersionCode;
  
  AdBreakInfo(int paramInt, long paramLong)
  {
    this.mVersionCode = paramInt;
    this.lT = paramLong;
  }
  
  public long getPlaybackPositionInMs()
  {
    return this.lT;
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zza.zza(this, paramParcel, paramInt);
  }
  
  public static class Builder
  {
    private long lT = 0L;
    
    public Builder(long paramLong)
    {
      this.lT = paramLong;
    }
    
    public AdBreakInfo build()
    {
      return new AdBreakInfo(1, this.lT);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/AdBreakInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */