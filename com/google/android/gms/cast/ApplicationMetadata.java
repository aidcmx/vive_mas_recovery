package com.google.android.gms.cast;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.cast.internal.zzf;
import com.google.android.gms.common.images.WebImage;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ApplicationMetadata
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<ApplicationMetadata> CREATOR = new zzb();
  String lU;
  List<String> lV;
  String lW;
  Uri lX;
  String mName;
  private final int mVersionCode;
  List<WebImage> zzbmz;
  
  private ApplicationMetadata()
  {
    this.mVersionCode = 1;
    this.zzbmz = new ArrayList();
    this.lV = new ArrayList();
  }
  
  ApplicationMetadata(int paramInt, String paramString1, String paramString2, List<WebImage> paramList, List<String> paramList1, String paramString3, Uri paramUri)
  {
    this.mVersionCode = paramInt;
    this.lU = paramString1;
    this.mName = paramString2;
    this.zzbmz = paramList;
    this.lV = paramList1;
    this.lW = paramString3;
    this.lX = paramUri;
  }
  
  public boolean areNamespacesSupported(List<String> paramList)
  {
    return (this.lV != null) && (this.lV.containsAll(paramList));
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    do
    {
      return true;
      if (!(paramObject instanceof ApplicationMetadata)) {
        return false;
      }
      paramObject = (ApplicationMetadata)paramObject;
    } while ((zzf.zza(this.lU, ((ApplicationMetadata)paramObject).lU)) && (zzf.zza(this.zzbmz, ((ApplicationMetadata)paramObject).zzbmz)) && (zzf.zza(this.mName, ((ApplicationMetadata)paramObject).mName)) && (zzf.zza(this.lV, ((ApplicationMetadata)paramObject).lV)) && (zzf.zza(this.lW, ((ApplicationMetadata)paramObject).lW)) && (zzf.zza(this.lX, ((ApplicationMetadata)paramObject).lX)));
    return false;
  }
  
  public String getApplicationId()
  {
    return this.lU;
  }
  
  public List<WebImage> getImages()
  {
    return this.zzbmz;
  }
  
  public String getName()
  {
    return this.mName;
  }
  
  public String getSenderAppIdentifier()
  {
    return this.lW;
  }
  
  public List<String> getSupportedNamespaces()
  {
    return Collections.unmodifiableList(this.lV);
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { Integer.valueOf(this.mVersionCode), this.lU, this.mName, this.zzbmz, this.lV, this.lW, this.lX });
  }
  
  public boolean isNamespaceSupported(String paramString)
  {
    return (this.lV != null) && (this.lV.contains(paramString));
  }
  
  public String toString()
  {
    int j = 0;
    StringBuilder localStringBuilder = new StringBuilder().append("applicationId: ").append(this.lU).append(", name: ").append(this.mName).append(", images.count: ");
    if (this.zzbmz == null)
    {
      i = 0;
      localStringBuilder = localStringBuilder.append(i).append(", namespaces.count: ");
      if (this.lV != null) {
        break label114;
      }
    }
    label114:
    for (int i = j;; i = this.lV.size())
    {
      return i + ", senderAppIdentifier: " + this.lW + ", senderAppLaunchUrl: " + this.lX;
      i = this.zzbmz.size();
      break;
    }
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzb.zza(this, paramParcel, paramInt);
  }
  
  public Uri zzakr()
  {
    return this.lX;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/ApplicationMetadata.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */