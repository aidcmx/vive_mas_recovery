package com.google.android.gms.cast;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.cast.internal.zzf;
import com.google.android.gms.common.images.WebImage;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class CastDevice
  extends AbstractSafeParcelable
  implements ReflectedParcelable
{
  public static final int CAPABILITY_AUDIO_IN = 8;
  public static final int CAPABILITY_AUDIO_OUT = 4;
  public static final int CAPABILITY_MULTIZONE_GROUP = 32;
  public static final int CAPABILITY_VIDEO_IN = 2;
  public static final int CAPABILITY_VIDEO_OUT = 1;
  public static final Parcelable.Creator<CastDevice> CREATOR = new zzc();
  private final int mVersionCode;
  private String ml;
  String mm;
  private Inet4Address mn;
  private String mo;
  private String mp;
  private String mq;
  private int mr;
  private List<WebImage> ms;
  private int mt;
  private String mu;
  private int zzbtt;
  
  CastDevice(int paramInt1, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, int paramInt2, List<WebImage> paramList, int paramInt3, int paramInt4, String paramString6)
  {
    this.mVersionCode = paramInt1;
    this.ml = zzgj(paramString1);
    this.mm = zzgj(paramString2);
    if (!TextUtils.isEmpty(this.mm)) {}
    try
    {
      paramString1 = InetAddress.getByName(this.mm);
      if ((paramString1 instanceof Inet4Address)) {
        this.mn = ((Inet4Address)paramString1);
      }
      this.mo = zzgj(paramString3);
      this.mp = zzgj(paramString4);
      this.mq = zzgj(paramString5);
      this.mr = paramInt2;
      if (paramList != null)
      {
        this.ms = paramList;
        this.mt = paramInt3;
        this.zzbtt = paramInt4;
        this.mu = zzgj(paramString6);
        return;
      }
    }
    catch (UnknownHostException paramString2)
    {
      for (;;)
      {
        paramString1 = this.mm;
        paramString2 = String.valueOf(paramString2.getMessage());
        Log.i("CastDevice", String.valueOf(paramString1).length() + 48 + String.valueOf(paramString2).length() + "Unable to convert host address (" + paramString1 + ") to ipaddress: " + paramString2);
        continue;
        paramList = new ArrayList();
      }
    }
  }
  
  public static CastDevice getFromBundle(Bundle paramBundle)
  {
    if (paramBundle == null) {
      return null;
    }
    paramBundle.setClassLoader(CastDevice.class.getClassLoader());
    return (CastDevice)paramBundle.getParcelable("com.google.android.gms.cast.EXTRA_CAST_DEVICE");
  }
  
  private static String zzgj(String paramString)
  {
    String str = paramString;
    if (paramString == null) {
      str = "";
    }
    return str;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    do
    {
      do
      {
        return true;
        if (!(paramObject instanceof CastDevice)) {
          return false;
        }
        paramObject = (CastDevice)paramObject;
        if (this.ml != null) {
          break;
        }
      } while (((CastDevice)paramObject).ml == null);
      return false;
    } while ((zzf.zza(this.ml, ((CastDevice)paramObject).ml)) && (zzf.zza(this.mn, ((CastDevice)paramObject).mn)) && (zzf.zza(this.mp, ((CastDevice)paramObject).mp)) && (zzf.zza(this.mo, ((CastDevice)paramObject).mo)) && (zzf.zza(this.mq, ((CastDevice)paramObject).mq)) && (this.mr == ((CastDevice)paramObject).mr) && (zzf.zza(this.ms, ((CastDevice)paramObject).ms)) && (this.mt == ((CastDevice)paramObject).mt) && (this.zzbtt == ((CastDevice)paramObject).zzbtt) && (zzf.zza(this.mu, ((CastDevice)paramObject).mu)));
    return false;
  }
  
  public int getCapabilities()
  {
    return this.mt;
  }
  
  public String getDeviceId()
  {
    if (this.ml.startsWith("__cast_nearby__")) {
      return this.ml.substring("__cast_nearby__".length() + 1);
    }
    return this.ml;
  }
  
  public String getDeviceVersion()
  {
    return this.mq;
  }
  
  public String getFriendlyName()
  {
    return this.mo;
  }
  
  public WebImage getIcon(int paramInt1, int paramInt2)
  {
    Object localObject1 = null;
    if (this.ms.isEmpty()) {
      return null;
    }
    if ((paramInt1 <= 0) || (paramInt2 <= 0)) {
      return (WebImage)this.ms.get(0);
    }
    Iterator localIterator = this.ms.iterator();
    Object localObject2 = null;
    WebImage localWebImage;
    int i;
    int j;
    if (localIterator.hasNext())
    {
      localWebImage = (WebImage)localIterator.next();
      i = localWebImage.getWidth();
      j = localWebImage.getHeight();
      if ((i >= paramInt1) && (j >= paramInt2))
      {
        if ((localObject2 != null) && ((((WebImage)localObject2).getWidth() <= i) || (((WebImage)localObject2).getHeight() <= j))) {
          break label210;
        }
        localObject2 = localWebImage;
      }
    }
    label210:
    for (;;)
    {
      break;
      if ((i < paramInt1) && (j < paramInt2) && ((localObject1 == null) || ((((WebImage)localObject1).getWidth() < i) && (((WebImage)localObject1).getHeight() < j))))
      {
        localObject1 = localWebImage;
        continue;
        if (localObject2 != null) {}
        for (;;)
        {
          return (WebImage)localObject2;
          if (localObject1 != null) {
            localObject2 = localObject1;
          } else {
            localObject2 = (WebImage)this.ms.get(0);
          }
        }
      }
    }
  }
  
  public List<WebImage> getIcons()
  {
    return Collections.unmodifiableList(this.ms);
  }
  
  public Inet4Address getIpAddress()
  {
    return this.mn;
  }
  
  public String getModelName()
  {
    return this.mp;
  }
  
  public int getServicePort()
  {
    return this.mr;
  }
  
  public int getStatus()
  {
    return this.zzbtt;
  }
  
  public int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public boolean hasCapabilities(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null) {
      return false;
    }
    int j = paramArrayOfInt.length;
    int i = 0;
    for (;;)
    {
      if (i >= j) {
        break label33;
      }
      if (!hasCapability(paramArrayOfInt[i])) {
        break;
      }
      i += 1;
    }
    label33:
    return true;
  }
  
  public boolean hasCapability(int paramInt)
  {
    return (this.mt & paramInt) == paramInt;
  }
  
  public boolean hasIcons()
  {
    return !this.ms.isEmpty();
  }
  
  public int hashCode()
  {
    if (this.ml == null) {
      return 0;
    }
    return this.ml.hashCode();
  }
  
  public boolean isOnLocalNetwork()
  {
    return !this.ml.startsWith("__cast_nearby__");
  }
  
  public boolean isSameDevice(CastDevice paramCastDevice)
  {
    if (paramCastDevice == null) {}
    do
    {
      return false;
      if (this.ml != null) {
        break;
      }
    } while (paramCastDevice.ml != null);
    return true;
    return zzf.zza(this.ml, paramCastDevice.ml);
  }
  
  public void putInBundle(Bundle paramBundle)
  {
    if (paramBundle == null) {
      return;
    }
    paramBundle.putParcelable("com.google.android.gms.cast.EXTRA_CAST_DEVICE", this);
  }
  
  public String toString()
  {
    return String.format("\"%s\" (%s)", new Object[] { this.mo, this.ml });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzc.zza(this, paramParcel, paramInt);
  }
  
  public String zzaks()
  {
    return this.ml;
  }
  
  public String zzakt()
  {
    return this.mu;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/CastDevice.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */