package com.google.android.gms.cast;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.view.Display;
import com.google.android.gms.cast.internal.zzh;
import com.google.android.gms.cast.internal.zzl;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.ApiOptions.HasOptions;
import com.google.android.gms.common.api.Api.zza;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.internal.zzpx;
import com.google.android.gms.internal.zzpy;
import com.google.android.gms.internal.zzvq;
import java.lang.annotation.Annotation;

public final class CastRemoteDisplay
{
  public static final Api<CastRemoteDisplayOptions> API = new Api("CastRemoteDisplay.API", hh, zzl.vm);
  public static final int CONFIGURATION_INTERACTIVE_NONREALTIME = 2;
  public static final int CONFIGURATION_INTERACTIVE_REALTIME = 1;
  public static final int CONFIGURATION_NONINTERACTIVE = 3;
  public static final CastRemoteDisplayApi CastRemoteDisplayApi = new zzpx(API);
  private static final Api.zza<zzpy, CastRemoteDisplayOptions> hh = new Api.zza()
  {
    public zzpy zza(Context paramAnonymousContext, Looper paramAnonymousLooper, zzf paramAnonymouszzf, CastRemoteDisplay.CastRemoteDisplayOptions paramAnonymousCastRemoteDisplayOptions, GoogleApiClient.ConnectionCallbacks paramAnonymousConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramAnonymousOnConnectionFailedListener)
    {
      Bundle localBundle = new Bundle();
      localBundle.putInt("configuration", paramAnonymousCastRemoteDisplayOptions.mw);
      return new zzpy(paramAnonymousContext, paramAnonymousLooper, paramAnonymouszzf, paramAnonymousCastRemoteDisplayOptions.md, localBundle, paramAnonymousCastRemoteDisplayOptions.mv, paramAnonymousConnectionCallbacks, paramAnonymousOnConnectionFailedListener);
    }
  };
  
  public static final boolean isRemoteDisplaySdkSupported(Context paramContext)
  {
    zzh.initialize(paramContext);
    return ((Boolean)zzh.vk.get()).booleanValue();
  }
  
  public static final class CastRemoteDisplayOptions
    implements Api.ApiOptions.HasOptions
  {
    final CastDevice md;
    final CastRemoteDisplay.CastRemoteDisplaySessionCallbacks mv;
    final int mw;
    
    private CastRemoteDisplayOptions(Builder paramBuilder)
    {
      this.md = paramBuilder.mg;
      this.mv = paramBuilder.mx;
      this.mw = paramBuilder.my;
    }
    
    public static final class Builder
    {
      CastDevice mg;
      CastRemoteDisplay.CastRemoteDisplaySessionCallbacks mx;
      int my;
      
      public Builder(CastDevice paramCastDevice, CastRemoteDisplay.CastRemoteDisplaySessionCallbacks paramCastRemoteDisplaySessionCallbacks)
      {
        zzaa.zzb(paramCastDevice, "CastDevice parameter cannot be null");
        this.mg = paramCastDevice;
        this.mx = paramCastRemoteDisplaySessionCallbacks;
        this.my = 2;
      }
      
      public CastRemoteDisplay.CastRemoteDisplayOptions build()
      {
        return new CastRemoteDisplay.CastRemoteDisplayOptions(this, null);
      }
      
      public Builder setConfigPreset(@CastRemoteDisplay.Configuration int paramInt)
      {
        this.my = paramInt;
        return this;
      }
    }
  }
  
  public static abstract interface CastRemoteDisplaySessionCallbacks
  {
    public abstract void onRemoteDisplayEnded(Status paramStatus);
  }
  
  public static abstract interface CastRemoteDisplaySessionResult
    extends Result
  {
    public abstract Display getPresentationDisplay();
  }
  
  public static @interface Configuration {}
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/CastRemoteDisplay.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */