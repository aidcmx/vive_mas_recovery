package com.google.android.gms.cast;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ServiceInfo;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat.Builder;
import android.support.v7.media.MediaRouteSelector.Builder;
import android.support.v7.media.MediaRouter;
import android.support.v7.media.MediaRouter.Callback;
import android.support.v7.media.MediaRouter.RouteInfo;
import android.text.TextUtils;
import android.view.Display;
import com.google.android.gms.R.drawable;
import com.google.android.gms.R.id;
import com.google.android.gms.R.string;
import com.google.android.gms.cast.internal.zzm;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.Builder;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzaa;
import java.util.concurrent.atomic.AtomicBoolean;

@TargetApi(19)
public abstract class CastRemoteDisplayLocalService
  extends Service
{
  private static final int mA = zzakv();
  private static final Object mB = new Object();
  private static AtomicBoolean mC = new AtomicBoolean(false);
  private static CastRemoteDisplayLocalService mQ;
  private static final zzm mz = new zzm("CastRemoteDisplayLocalService");
  private String lU;
  private GoogleApiClient mD;
  private CastRemoteDisplay.CastRemoteDisplaySessionCallbacks mE;
  private Callbacks mF;
  private zzb mG;
  private NotificationSettings mH;
  private Handler mHandler;
  private boolean mI;
  private PendingIntent mJ;
  private CastDevice mK;
  private Context mL;
  private ServiceConnection mM;
  private MediaRouter mN;
  private Notification mNotification;
  private boolean mO = false;
  private final MediaRouter.Callback mP = new MediaRouter.Callback()
  {
    public void onRouteUnselected(MediaRouter paramAnonymousMediaRouter, MediaRouter.RouteInfo paramAnonymousRouteInfo)
    {
      CastRemoteDisplayLocalService.zza(CastRemoteDisplayLocalService.this, "onRouteUnselected");
      if (CastRemoteDisplayLocalService.zza(CastRemoteDisplayLocalService.this) == null)
      {
        CastRemoteDisplayLocalService.zza(CastRemoteDisplayLocalService.this, "onRouteUnselected, no device was selected");
        return;
      }
      if (!CastDevice.getFromBundle(paramAnonymousRouteInfo.getExtras()).getDeviceId().equals(CastRemoteDisplayLocalService.zza(CastRemoteDisplayLocalService.this).getDeviceId()))
      {
        CastRemoteDisplayLocalService.zza(CastRemoteDisplayLocalService.this, "onRouteUnselected, device does not match");
        return;
      }
      CastRemoteDisplayLocalService.stopService();
    }
  };
  private final IBinder mR = new zza(null);
  private Display zzccw;
  
  public static CastRemoteDisplayLocalService getInstance()
  {
    synchronized (mB)
    {
      CastRemoteDisplayLocalService localCastRemoteDisplayLocalService = mQ;
      return localCastRemoteDisplayLocalService;
    }
  }
  
  protected static void setDebugEnabled()
  {
    mz.zzbq(true);
  }
  
  public static void startService(Context paramContext, Class<? extends CastRemoteDisplayLocalService> paramClass, String paramString, CastDevice paramCastDevice, NotificationSettings paramNotificationSettings, Callbacks paramCallbacks)
  {
    startServiceWithOptions(paramContext, paramClass, paramString, paramCastDevice, new Options(), paramNotificationSettings, paramCallbacks);
  }
  
  public static void startServiceWithOptions(@NonNull final Context paramContext, @NonNull Class<? extends CastRemoteDisplayLocalService> paramClass, @NonNull String paramString, @NonNull final CastDevice paramCastDevice, @NonNull final Options paramOptions, @NonNull final NotificationSettings paramNotificationSettings, @NonNull final Callbacks paramCallbacks)
  {
    mz.zzb("Starting Service", new Object[0]);
    synchronized (mB)
    {
      if (mQ != null)
      {
        mz.zzf("An existing service had not been stopped before starting one", new Object[0]);
        zzbh(true);
      }
      zza(paramContext, paramClass);
      zzaa.zzb(paramContext, "activityContext is required.");
      zzaa.zzb(paramClass, "serviceClass is required.");
      zzaa.zzb(paramString, "applicationId is required.");
      zzaa.zzb(paramCastDevice, "device is required.");
      zzaa.zzb(paramOptions, "options is required.");
      zzaa.zzb(paramNotificationSettings, "notificationSettings is required.");
      zzaa.zzb(paramCallbacks, "callbacks is required.");
      if ((NotificationSettings.zzb(paramNotificationSettings) == null) && (NotificationSettings.zze(paramNotificationSettings) == null)) {
        throw new IllegalArgumentException("notificationSettings: Either the notification or the notificationPendingIntent must be provided");
      }
    }
    if (mC.getAndSet(true))
    {
      mz.zzc("Service is already being started, startService has been called twice", new Object[0]);
      return;
    }
    paramClass = new Intent(paramContext, paramClass);
    paramContext.startService(paramClass);
    paramContext.bindService(paramClass, new ServiceConnection()
    {
      public void onServiceConnected(ComponentName paramAnonymousComponentName, IBinder paramAnonymousIBinder)
      {
        paramAnonymousComponentName = ((CastRemoteDisplayLocalService.zza)paramAnonymousIBinder).zzalh();
        if ((paramAnonymousComponentName == null) || (!CastRemoteDisplayLocalService.zza(paramAnonymousComponentName, CastRemoteDisplayLocalService.this, paramCastDevice, paramOptions, paramNotificationSettings, paramContext, this, paramCallbacks)))
        {
          CastRemoteDisplayLocalService.zzald().zzc("Connected but unable to get the service instance", new Object[0]);
          paramCallbacks.onRemoteDisplaySessionError(new Status(2200));
          CastRemoteDisplayLocalService.zzale().set(false);
        }
        try
        {
          paramContext.unbindService(this);
          return;
        }
        catch (IllegalArgumentException paramAnonymousComponentName)
        {
          CastRemoteDisplayLocalService.zzald().zzb("No need to unbind service, already unbound", new Object[0]);
        }
      }
      
      public void onServiceDisconnected(ComponentName paramAnonymousComponentName)
      {
        CastRemoteDisplayLocalService.zzald().zzb("onServiceDisconnected", new Object[0]);
        paramCallbacks.onRemoteDisplaySessionError(new Status(2201, "Service Disconnected"));
        CastRemoteDisplayLocalService.zzale().set(false);
        try
        {
          paramContext.unbindService(this);
          return;
        }
        catch (IllegalArgumentException paramAnonymousComponentName)
        {
          CastRemoteDisplayLocalService.zzald().zzb("No need to unbind service, already unbound", new Object[0]);
        }
      }
    }, 64);
  }
  
  public static void stopService()
  {
    zzbh(false);
  }
  
  private GoogleApiClient zza(CastDevice paramCastDevice, Options paramOptions)
  {
    paramCastDevice = new CastRemoteDisplay.CastRemoteDisplayOptions.Builder(paramCastDevice, this.mE);
    if (paramOptions != null) {
      paramCastDevice.setConfigPreset(paramOptions.mw);
    }
    new GoogleApiClient.Builder(this, new GoogleApiClient.ConnectionCallbacks()new GoogleApiClient.OnConnectionFailedListener
    {
      public void onConnected(Bundle paramAnonymousBundle)
      {
        CastRemoteDisplayLocalService.zza(CastRemoteDisplayLocalService.this, "onConnected");
        CastRemoteDisplayLocalService.zzf(CastRemoteDisplayLocalService.this);
      }
      
      public void onConnectionSuspended(int paramAnonymousInt)
      {
        CastRemoteDisplayLocalService.zzald().zzf(String.format("[Instance: %s] ConnectionSuspended %d", new Object[] { this, Integer.valueOf(paramAnonymousInt) }), new Object[0]);
      }
    }, new GoogleApiClient.OnConnectionFailedListener()
    {
      public void onConnectionFailed(ConnectionResult paramAnonymousConnectionResult)
      {
        CastRemoteDisplayLocalService localCastRemoteDisplayLocalService = CastRemoteDisplayLocalService.this;
        paramAnonymousConnectionResult = String.valueOf(paramAnonymousConnectionResult);
        CastRemoteDisplayLocalService.zzb(localCastRemoteDisplayLocalService, String.valueOf(paramAnonymousConnectionResult).length() + 19 + "Connection failed: " + paramAnonymousConnectionResult);
        CastRemoteDisplayLocalService.zzc(CastRemoteDisplayLocalService.this);
      }
    }).addApi(CastRemoteDisplay.API, paramCastDevice.build()).build();
  }
  
  private static void zza(Context paramContext, Class<? extends CastRemoteDisplayLocalService> paramClass)
  {
    try
    {
      paramClass = new ComponentName(paramContext, paramClass);
      paramContext = paramContext.getPackageManager().getServiceInfo(paramClass, 128);
      if ((paramContext != null) && (paramContext.exported)) {
        throw new IllegalStateException("The service must not be exported, verify the manifest configuration");
      }
    }
    catch (PackageManager.NameNotFoundException paramContext)
    {
      throw new IllegalStateException("Service not found, did you forget to configure it in the manifest?");
    }
  }
  
  private void zza(Display paramDisplay)
  {
    this.zzccw = paramDisplay;
    if (this.mI)
    {
      this.mNotification = zzbi(true);
      startForeground(mA, this.mNotification);
    }
    if (this.mF != null)
    {
      this.mF.onRemoteDisplaySessionStarted(this);
      this.mF = null;
    }
    onCreatePresentation(this.zzccw);
  }
  
  private void zza(NotificationSettings paramNotificationSettings)
  {
    zzaa.zzhs("updateNotificationSettingsInternal must be called on the main thread");
    if (this.mH == null) {
      throw new IllegalStateException("No current notification settings to update");
    }
    if (this.mI)
    {
      if (NotificationSettings.zzb(paramNotificationSettings) != null) {
        throw new IllegalStateException("Current mode is default notification, notification attribute must not be provided");
      }
      if (NotificationSettings.zze(paramNotificationSettings) != null) {
        NotificationSettings.zza(this.mH, NotificationSettings.zze(paramNotificationSettings));
      }
      if (!TextUtils.isEmpty(NotificationSettings.zzc(paramNotificationSettings))) {
        NotificationSettings.zza(this.mH, NotificationSettings.zzc(paramNotificationSettings));
      }
      if (!TextUtils.isEmpty(NotificationSettings.zzd(paramNotificationSettings))) {
        NotificationSettings.zzb(this.mH, NotificationSettings.zzd(paramNotificationSettings));
      }
      this.mNotification = zzbi(true);
    }
    for (;;)
    {
      startForeground(mA, this.mNotification);
      return;
      zzaa.zzb(NotificationSettings.zzb(paramNotificationSettings), "notification is required.");
      this.mNotification = NotificationSettings.zzb(paramNotificationSettings);
      NotificationSettings.zza(this.mH, this.mNotification);
    }
  }
  
  private boolean zza(String paramString, CastDevice paramCastDevice, Options paramOptions, NotificationSettings paramNotificationSettings, Context paramContext, ServiceConnection paramServiceConnection, Callbacks paramCallbacks)
  {
    zzet("startRemoteDisplaySession");
    zzaa.zzhs("Starting the Cast Remote Display must be done on the main thread");
    for (;;)
    {
      synchronized (mB)
      {
        if (mQ != null)
        {
          mz.zzf("An existing service had not been stopped before starting one", new Object[0]);
          return false;
        }
        mQ = this;
        this.mF = paramCallbacks;
        this.lU = paramString;
        this.mK = paramCastDevice;
        this.mL = paramContext;
        this.mM = paramServiceConnection;
        this.mN = MediaRouter.getInstance(getApplicationContext());
        paramString = new MediaRouteSelector.Builder().addControlCategory(CastMediaControlIntent.categoryForCast(this.lU)).build();
        zzet("addMediaRouterCallback");
        this.mN.addCallback(paramString, this.mP, 4);
        this.mE = new CastRemoteDisplay.CastRemoteDisplaySessionCallbacks()
        {
          public void onRemoteDisplayEnded(Status paramAnonymousStatus)
          {
            CastRemoteDisplayLocalService.zzald().zzb(String.format("Cast screen has ended: %d", new Object[] { Integer.valueOf(paramAnonymousStatus.getStatusCode()) }), new Object[0]);
            CastRemoteDisplayLocalService.zzbj(false);
          }
        };
        this.mNotification = NotificationSettings.zzb(paramNotificationSettings);
        this.mG = new zzb(null);
        registerReceiver(this.mG, new IntentFilter("com.google.android.gms.cast.remote_display.ACTION_NOTIFICATION_DISCONNECT"));
        this.mH = new NotificationSettings(paramNotificationSettings, null);
        if (NotificationSettings.zzb(this.mH) == null)
        {
          this.mI = true;
          this.mNotification = zzbi(false);
          startForeground(mA, this.mNotification);
          this.mD = zza(paramCastDevice, paramOptions);
          this.mD.connect();
          if (this.mF != null) {
            this.mF.onServiceCreated(this);
          }
          return true;
        }
      }
      this.mI = false;
      this.mNotification = NotificationSettings.zzb(this.mH);
    }
  }
  
  private static int zzakv()
  {
    return R.id.cast_notification_id;
  }
  
  private void zzakw()
  {
    if (this.mN != null)
    {
      zzaa.zzhs("CastRemoteDisplayLocalService calls must be done on the main thread");
      zzet("removeMediaRouterCallback");
      this.mN.removeCallback(this.mP);
    }
  }
  
  private void zzakx()
  {
    zzet("startRemoteDisplay");
    if ((this.mD == null) || (!this.mD.isConnected()))
    {
      mz.zzc("Unable to start the remote display as the API client is not ready", new Object[0]);
      return;
    }
    CastRemoteDisplay.CastRemoteDisplayApi.startRemoteDisplay(this.mD, this.lU).setResultCallback(new ResultCallback()
    {
      public void zza(CastRemoteDisplay.CastRemoteDisplaySessionResult paramAnonymousCastRemoteDisplaySessionResult)
      {
        if (!paramAnonymousCastRemoteDisplaySessionResult.getStatus().isSuccess())
        {
          CastRemoteDisplayLocalService.zzald().zzc("Connection was not successful", new Object[0]);
          CastRemoteDisplayLocalService.zzc(CastRemoteDisplayLocalService.this);
          return;
        }
        CastRemoteDisplayLocalService.zzald().zzb("startRemoteDisplay successful", new Object[0]);
        synchronized (CastRemoteDisplayLocalService.zzalf())
        {
          if (CastRemoteDisplayLocalService.zzalg() == null)
          {
            CastRemoteDisplayLocalService.zzald().zzb("Remote Display started but session already cancelled", new Object[0]);
            CastRemoteDisplayLocalService.zzc(CastRemoteDisplayLocalService.this);
            return;
          }
        }
        paramAnonymousCastRemoteDisplaySessionResult = paramAnonymousCastRemoteDisplaySessionResult.getPresentationDisplay();
        if (paramAnonymousCastRemoteDisplaySessionResult != null) {
          CastRemoteDisplayLocalService.zza(CastRemoteDisplayLocalService.this, paramAnonymousCastRemoteDisplaySessionResult);
        }
        for (;;)
        {
          CastRemoteDisplayLocalService.zzale().set(false);
          if ((CastRemoteDisplayLocalService.zzd(CastRemoteDisplayLocalService.this) == null) || (CastRemoteDisplayLocalService.zze(CastRemoteDisplayLocalService.this) == null)) {
            break;
          }
          try
          {
            CastRemoteDisplayLocalService.zzd(CastRemoteDisplayLocalService.this).unbindService(CastRemoteDisplayLocalService.zze(CastRemoteDisplayLocalService.this));
            CastRemoteDisplayLocalService.zza(CastRemoteDisplayLocalService.this, null);
            CastRemoteDisplayLocalService.zza(CastRemoteDisplayLocalService.this, null);
            return;
            CastRemoteDisplayLocalService.zzald().zzc("Cast Remote Display session created without display", new Object[0]);
          }
          catch (IllegalArgumentException paramAnonymousCastRemoteDisplaySessionResult)
          {
            for (;;)
            {
              CastRemoteDisplayLocalService.zzald().zzb("No need to unbind service, already unbound", new Object[0]);
            }
          }
        }
      }
    });
  }
  
  private void zzaky()
  {
    zzet("stopRemoteDisplay");
    if ((this.mD == null) || (!this.mD.isConnected()))
    {
      mz.zzc("Unable to stop the remote display as the API client is not ready", new Object[0]);
      return;
    }
    CastRemoteDisplay.CastRemoteDisplayApi.stopRemoteDisplay(this.mD).setResultCallback(new ResultCallback()
    {
      public void zza(CastRemoteDisplay.CastRemoteDisplaySessionResult paramAnonymousCastRemoteDisplaySessionResult)
      {
        if (!paramAnonymousCastRemoteDisplaySessionResult.getStatus().isSuccess()) {
          CastRemoteDisplayLocalService.zza(CastRemoteDisplayLocalService.this, "Unable to stop the remote display, result unsuccessful");
        }
        for (;;)
        {
          CastRemoteDisplayLocalService.zzb(CastRemoteDisplayLocalService.this, null);
          return;
          CastRemoteDisplayLocalService.zza(CastRemoteDisplayLocalService.this, "remote display stopped");
        }
      }
    });
  }
  
  private void zzakz()
  {
    if (this.mF != null)
    {
      this.mF.onRemoteDisplaySessionError(new Status(2200));
      this.mF = null;
    }
    stopService();
  }
  
  private void zzala()
  {
    zzet("stopRemoteDisplaySession");
    zzaky();
    onDismissPresentation();
  }
  
  private void zzalb()
  {
    zzet("Stopping the remote display Service");
    stopForeground(true);
    stopSelf();
  }
  
  private PendingIntent zzalc()
  {
    if (this.mJ == null)
    {
      Intent localIntent = new Intent("com.google.android.gms.cast.remote_display.ACTION_NOTIFICATION_DISCONNECT");
      localIntent.setPackage(this.mL.getPackageName());
      this.mJ = PendingIntent.getBroadcast(this, 0, localIntent, 268435456);
    }
    return this.mJ;
  }
  
  private void zzbf(final boolean paramBoolean)
  {
    if (this.mHandler != null)
    {
      if (Looper.myLooper() != Looper.getMainLooper()) {
        this.mHandler.post(new Runnable()
        {
          public void run()
          {
            CastRemoteDisplayLocalService.zza(CastRemoteDisplayLocalService.this, paramBoolean);
          }
        });
      }
    }
    else {
      return;
    }
    zzbg(paramBoolean);
  }
  
  private void zzbg(boolean paramBoolean)
  {
    zzet("Stopping Service");
    zzaa.zzhs("stopServiceInstanceInternal must be called on the main thread");
    if ((!paramBoolean) && (this.mN != null))
    {
      zzet("Setting default route");
      this.mN.selectRoute(this.mN.getDefaultRoute());
    }
    if (this.mG != null)
    {
      zzet("Unregistering notification receiver");
      unregisterReceiver(this.mG);
    }
    zzala();
    zzalb();
    zzakw();
    if (this.mD != null)
    {
      this.mD.disconnect();
      this.mD = null;
    }
    if ((this.mL != null) && (this.mM != null)) {}
    try
    {
      this.mL.unbindService(this.mM);
      this.mM = null;
      this.mL = null;
      this.lU = null;
      this.mD = null;
      this.mNotification = null;
      this.zzccw = null;
      return;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      for (;;)
      {
        zzet("No need to unbind service, already unbound");
      }
    }
  }
  
  private static void zzbh(boolean paramBoolean)
  {
    mz.zzb("Stopping Service", new Object[0]);
    mC.set(false);
    synchronized (mB)
    {
      if (mQ == null)
      {
        mz.zzc("Service is already being stopped", new Object[0]);
        return;
      }
      CastRemoteDisplayLocalService localCastRemoteDisplayLocalService = mQ;
      mQ = null;
      localCastRemoteDisplayLocalService.zzbf(paramBoolean);
      return;
    }
  }
  
  private Notification zzbi(boolean paramBoolean)
  {
    zzet("createDefaultNotification");
    int k = getApplicationInfo().labelRes;
    String str3 = NotificationSettings.zzc(this.mH);
    String str2 = NotificationSettings.zzd(this.mH);
    int j;
    int i;
    String str1;
    if (paramBoolean)
    {
      j = R.string.cast_notification_connected_message;
      i = R.drawable.cast_ic_notification_on;
      str1 = str3;
      if (TextUtils.isEmpty(str3)) {
        str1 = getString(k);
      }
      if (!TextUtils.isEmpty(str2)) {
        break label163;
      }
      str2 = getString(j, new Object[] { this.mK.getFriendlyName() });
    }
    label163:
    for (;;)
    {
      return new NotificationCompat.Builder(this).setContentTitle(str1).setContentText(str2).setContentIntent(NotificationSettings.zze(this.mH)).setSmallIcon(i).setOngoing(true).addAction(17301560, getString(R.string.cast_notification_disconnect), zzalc()).build();
      j = R.string.cast_notification_connecting_message;
      i = R.drawable.cast_ic_notification_connecting;
      break;
    }
  }
  
  private void zzet(String paramString)
  {
    mz.zzb("[Instance: %s] %s", new Object[] { this, paramString });
  }
  
  private void zzew(String paramString)
  {
    mz.zzc("[Instance: %s] %s", new Object[] { this, paramString });
  }
  
  protected Display getDisplay()
  {
    return this.zzccw;
  }
  
  public IBinder onBind(Intent paramIntent)
  {
    zzet("onBind");
    return this.mR;
  }
  
  public void onCreate()
  {
    zzet("onCreate");
    super.onCreate();
    this.mHandler = new Handler(getMainLooper());
    this.mHandler.postDelayed(new Runnable()
    {
      public void run()
      {
        CastRemoteDisplayLocalService localCastRemoteDisplayLocalService = CastRemoteDisplayLocalService.this;
        boolean bool = CastRemoteDisplayLocalService.zzb(CastRemoteDisplayLocalService.this);
        CastRemoteDisplayLocalService.zza(localCastRemoteDisplayLocalService, 59 + "onCreate after delay. The local service been started: " + bool);
        if (!CastRemoteDisplayLocalService.zzb(CastRemoteDisplayLocalService.this))
        {
          CastRemoteDisplayLocalService.zzb(CastRemoteDisplayLocalService.this, "The local service has not been been started, stopping it");
          CastRemoteDisplayLocalService.this.stopSelf();
        }
      }
    }, 100L);
  }
  
  public abstract void onCreatePresentation(Display paramDisplay);
  
  public abstract void onDismissPresentation();
  
  public int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2)
  {
    zzet("onStartCommand");
    this.mO = true;
    return 2;
  }
  
  public void updateNotificationSettings(final NotificationSettings paramNotificationSettings)
  {
    zzaa.zzb(paramNotificationSettings, "notificationSettings is required.");
    zzaa.zzb(this.mHandler, "Service is not ready yet.");
    this.mHandler.post(new Runnable()
    {
      public void run()
      {
        CastRemoteDisplayLocalService.zza(CastRemoteDisplayLocalService.this, paramNotificationSettings);
      }
    });
  }
  
  public static abstract interface Callbacks
  {
    public abstract void onRemoteDisplaySessionError(Status paramStatus);
    
    public abstract void onRemoteDisplaySessionStarted(CastRemoteDisplayLocalService paramCastRemoteDisplayLocalService);
    
    public abstract void onServiceCreated(CastRemoteDisplayLocalService paramCastRemoteDisplayLocalService);
  }
  
  public static final class NotificationSettings
  {
    private Notification mNotification;
    private PendingIntent mZ;
    private String na;
    private String nb;
    
    private NotificationSettings() {}
    
    private NotificationSettings(NotificationSettings paramNotificationSettings)
    {
      this.mNotification = paramNotificationSettings.mNotification;
      this.mZ = paramNotificationSettings.mZ;
      this.na = paramNotificationSettings.na;
      this.nb = paramNotificationSettings.nb;
    }
    
    public static final class Builder
    {
      private CastRemoteDisplayLocalService.NotificationSettings nc = new CastRemoteDisplayLocalService.NotificationSettings(null);
      
      public CastRemoteDisplayLocalService.NotificationSettings build()
      {
        if (CastRemoteDisplayLocalService.NotificationSettings.zzb(this.nc) != null)
        {
          if (!TextUtils.isEmpty(CastRemoteDisplayLocalService.NotificationSettings.zzc(this.nc))) {
            throw new IllegalArgumentException("notificationTitle requires using the default notification");
          }
          if (!TextUtils.isEmpty(CastRemoteDisplayLocalService.NotificationSettings.zzd(this.nc))) {
            throw new IllegalArgumentException("notificationText requires using the default notification");
          }
          if (CastRemoteDisplayLocalService.NotificationSettings.zze(this.nc) != null) {
            throw new IllegalArgumentException("notificationPendingIntent requires using the default notification");
          }
        }
        else if ((TextUtils.isEmpty(CastRemoteDisplayLocalService.NotificationSettings.zzc(this.nc))) && (TextUtils.isEmpty(CastRemoteDisplayLocalService.NotificationSettings.zzd(this.nc))) && (CastRemoteDisplayLocalService.NotificationSettings.zze(this.nc) == null))
        {
          throw new IllegalArgumentException("At least an argument must be provided");
        }
        return this.nc;
      }
      
      public Builder setNotification(Notification paramNotification)
      {
        CastRemoteDisplayLocalService.NotificationSettings.zza(this.nc, paramNotification);
        return this;
      }
      
      public Builder setNotificationPendingIntent(PendingIntent paramPendingIntent)
      {
        CastRemoteDisplayLocalService.NotificationSettings.zza(this.nc, paramPendingIntent);
        return this;
      }
      
      public Builder setNotificationText(String paramString)
      {
        CastRemoteDisplayLocalService.NotificationSettings.zzb(this.nc, paramString);
        return this;
      }
      
      public Builder setNotificationTitle(String paramString)
      {
        CastRemoteDisplayLocalService.NotificationSettings.zza(this.nc, paramString);
        return this;
      }
    }
  }
  
  public static class Options
  {
    @CastRemoteDisplay.Configuration
    int mw = 2;
    
    public int getConfigPreset()
    {
      return this.mw;
    }
    
    public void setConfigPreset(@CastRemoteDisplay.Configuration int paramInt)
    {
      this.mw = paramInt;
    }
  }
  
  private class zza
    extends Binder
  {
    private zza() {}
    
    CastRemoteDisplayLocalService zzalh()
    {
      return CastRemoteDisplayLocalService.this;
    }
  }
  
  private static final class zzb
    extends BroadcastReceiver
  {
    public void onReceive(Context paramContext, Intent paramIntent)
    {
      if (paramIntent.getAction().equals("com.google.android.gms.cast.remote_display.ACTION_NOTIFICATION_DISCONNECT"))
      {
        CastRemoteDisplayLocalService.zzald().zzb("disconnecting", new Object[0]);
        CastRemoteDisplayLocalService.stopService();
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/CastRemoteDisplayLocalService.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */