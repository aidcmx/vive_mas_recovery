package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.cast.internal.zzf;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import java.util.Locale;

public class LaunchOptions
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<LaunchOptions> CREATOR = new zze();
  private String bZ;
  private final int mVersionCode;
  private boolean ne;
  
  public LaunchOptions()
  {
    this(1, false, zzf.zzb(Locale.getDefault()));
  }
  
  LaunchOptions(int paramInt, boolean paramBoolean, String paramString)
  {
    this.mVersionCode = paramInt;
    this.ne = paramBoolean;
    this.bZ = paramString;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    do
    {
      return true;
      if (!(paramObject instanceof LaunchOptions)) {
        return false;
      }
      paramObject = (LaunchOptions)paramObject;
    } while ((this.ne == ((LaunchOptions)paramObject).ne) && (zzf.zza(this.bZ, ((LaunchOptions)paramObject).bZ)));
    return false;
  }
  
  public String getLanguage()
  {
    return this.bZ;
  }
  
  public boolean getRelaunchIfRunning()
  {
    return this.ne;
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { Boolean.valueOf(this.ne), this.bZ });
  }
  
  public void setLanguage(String paramString)
  {
    this.bZ = paramString;
  }
  
  public void setRelaunchIfRunning(boolean paramBoolean)
  {
    this.ne = paramBoolean;
  }
  
  public String toString()
  {
    return String.format("LaunchOptions(relaunchIfRunning=%b, language=%s)", new Object[] { Boolean.valueOf(this.ne), this.bZ });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zze.zza(this, paramParcel, paramInt);
  }
  
  public static final class Builder
  {
    private LaunchOptions nf = new LaunchOptions();
    
    public LaunchOptions build()
    {
      return this.nf;
    }
    
    public Builder setLocale(Locale paramLocale)
    {
      this.nf.setLanguage(zzf.zzb(paramLocale));
      return this;
    }
    
    public Builder setRelaunchIfRunning(boolean paramBoolean)
    {
      this.nf.setRelaunchIfRunning(paramBoolean);
      return this;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/LaunchOptions.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */