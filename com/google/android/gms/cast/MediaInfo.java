package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.util.zzp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class MediaInfo
  extends AbstractSafeParcelable
  implements ReflectedParcelable
{
  public static final Parcelable.Creator<MediaInfo> CREATOR = new zzf();
  public static final int STREAM_TYPE_BUFFERED = 1;
  public static final int STREAM_TYPE_INVALID = -1;
  public static final int STREAM_TYPE_LIVE = 2;
  public static final int STREAM_TYPE_NONE = 0;
  public static final long UNKNOWN_DURATION = -1L;
  private final int mVersionCode;
  private final String ng;
  private int nh;
  private String ni;
  private MediaMetadata nj;
  private long nk;
  private List<MediaTrack> nl;
  private TextTrackStyle nm;
  String nn;
  private List<AdBreakInfo> no;
  private JSONObject np;
  
  MediaInfo(int paramInt1, String paramString1, int paramInt2, String paramString2, MediaMetadata paramMediaMetadata, long paramLong, List<MediaTrack> paramList, TextTrackStyle paramTextTrackStyle, String paramString3, List<AdBreakInfo> paramList1)
  {
    this.mVersionCode = paramInt1;
    this.ng = paramString1;
    this.nh = paramInt2;
    this.ni = paramString2;
    this.nj = paramMediaMetadata;
    this.nk = paramLong;
    this.nl = paramList;
    this.nm = paramTextTrackStyle;
    this.nn = paramString3;
    if (this.nn != null) {}
    for (;;)
    {
      try
      {
        this.np = new JSONObject(this.nn);
        this.no = paramList1;
        return;
      }
      catch (JSONException paramString1)
      {
        this.np = null;
        this.nn = null;
        continue;
      }
      this.np = null;
    }
  }
  
  MediaInfo(String paramString)
    throws IllegalArgumentException
  {
    this(1, paramString, -1, null, null, -1L, null, null, null, null);
    if (TextUtils.isEmpty(paramString)) {
      throw new IllegalArgumentException("content ID cannot be null or empty");
    }
  }
  
  MediaInfo(JSONObject paramJSONObject)
    throws JSONException
  {
    this(1, paramJSONObject.getString("contentId"), -1, null, null, -1L, null, null, null, null);
    Object localObject1 = paramJSONObject.getString("streamType");
    if ("NONE".equals(localObject1)) {
      this.nh = 0;
    }
    Object localObject2;
    for (;;)
    {
      this.ni = paramJSONObject.getString("contentType");
      if (paramJSONObject.has("metadata"))
      {
        localObject1 = paramJSONObject.getJSONObject("metadata");
        this.nj = new MediaMetadata(((JSONObject)localObject1).getInt("metadataType"));
        this.nj.zzl((JSONObject)localObject1);
      }
      this.nk = -1L;
      if ((paramJSONObject.has("duration")) && (!paramJSONObject.isNull("duration")))
      {
        double d = paramJSONObject.optDouble("duration", 0.0D);
        if ((!Double.isNaN(d)) && (!Double.isInfinite(d))) {
          this.nk = com.google.android.gms.cast.internal.zzf.zzf(d);
        }
      }
      if (!paramJSONObject.has("tracks")) {
        break;
      }
      this.nl = new ArrayList();
      localObject1 = paramJSONObject.getJSONArray("tracks");
      int i = 0;
      while (i < ((JSONArray)localObject1).length())
      {
        localObject2 = new MediaTrack(((JSONArray)localObject1).getJSONObject(i));
        this.nl.add(localObject2);
        i += 1;
      }
      if ("BUFFERED".equals(localObject1)) {
        this.nh = 1;
      } else if ("LIVE".equals(localObject1)) {
        this.nh = 2;
      } else {
        this.nh = -1;
      }
    }
    this.nl = null;
    if (paramJSONObject.has("textTrackStyle"))
    {
      localObject1 = paramJSONObject.getJSONObject("textTrackStyle");
      localObject2 = new TextTrackStyle();
      ((TextTrackStyle)localObject2).zzl((JSONObject)localObject1);
    }
    for (this.nm = ((TextTrackStyle)localObject2);; this.nm = null)
    {
      this.np = paramJSONObject.optJSONObject("customData");
      return;
    }
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = true;
    boolean bool3 = false;
    if (this == paramObject) {
      bool1 = true;
    }
    int i;
    int j;
    label51:
    do
    {
      do
      {
        do
        {
          return bool1;
          bool1 = bool3;
        } while (!(paramObject instanceof MediaInfo));
        paramObject = (MediaInfo)paramObject;
        if (this.np != null) {
          break;
        }
        i = 1;
        if (((MediaInfo)paramObject).np != null) {
          break label169;
        }
        j = 1;
        bool1 = bool3;
      } while (i != j);
      if ((this.np == null) || (((MediaInfo)paramObject).np == null)) {
        break;
      }
      bool1 = bool3;
    } while (!zzp.zzf(this.np, ((MediaInfo)paramObject).np));
    if ((com.google.android.gms.cast.internal.zzf.zza(this.ng, ((MediaInfo)paramObject).ng)) && (this.nh == ((MediaInfo)paramObject).nh) && (com.google.android.gms.cast.internal.zzf.zza(this.ni, ((MediaInfo)paramObject).ni)) && (com.google.android.gms.cast.internal.zzf.zza(this.nj, ((MediaInfo)paramObject).nj)) && (this.nk == ((MediaInfo)paramObject).nk)) {}
    for (boolean bool1 = bool2;; bool1 = false)
    {
      return bool1;
      i = 0;
      break;
      label169:
      j = 0;
      break label51;
    }
  }
  
  public List<AdBreakInfo> getAdBreaks()
  {
    return this.no;
  }
  
  public String getContentId()
  {
    return this.ng;
  }
  
  public String getContentType()
  {
    return this.ni;
  }
  
  public JSONObject getCustomData()
  {
    return this.np;
  }
  
  public List<MediaTrack> getMediaTracks()
  {
    return this.nl;
  }
  
  public MediaMetadata getMetadata()
  {
    return this.nj;
  }
  
  public long getStreamDuration()
  {
    return this.nk;
  }
  
  public int getStreamType()
  {
    return this.nh;
  }
  
  public TextTrackStyle getTextTrackStyle()
  {
    return this.nm;
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.ng, Integer.valueOf(this.nh), this.ni, this.nj, Long.valueOf(this.nk), String.valueOf(this.np) });
  }
  
  void setContentType(String paramString)
    throws IllegalArgumentException
  {
    if (TextUtils.isEmpty(paramString)) {
      throw new IllegalArgumentException("content type cannot be null or empty");
    }
    this.ni = paramString;
  }
  
  void setCustomData(JSONObject paramJSONObject)
  {
    this.np = paramJSONObject;
  }
  
  void setStreamType(int paramInt)
    throws IllegalArgumentException
  {
    if ((paramInt < -1) || (paramInt > 2)) {
      throw new IllegalArgumentException("invalid stream type");
    }
    this.nh = paramInt;
  }
  
  public void setTextTrackStyle(TextTrackStyle paramTextTrackStyle)
  {
    this.nm = paramTextTrackStyle;
  }
  
  public JSONObject toJson()
  {
    JSONObject localJSONObject = new JSONObject();
    for (;;)
    {
      try
      {
        localJSONObject.put("contentId", this.ng);
        switch (this.nh)
        {
        default: 
          localJSONObject.put("streamType", localObject);
          if (this.ni != null) {
            localJSONObject.put("contentType", this.ni);
          }
          if (this.nj != null) {
            localJSONObject.put("metadata", this.nj.toJson());
          }
          if (this.nk <= -1L)
          {
            localJSONObject.put("duration", JSONObject.NULL);
            if (this.nl == null) {
              continue;
            }
            localObject = new JSONArray();
            Iterator localIterator = this.nl.iterator();
            if (localIterator.hasNext())
            {
              ((JSONArray)localObject).put(((MediaTrack)localIterator.next()).toJson());
              continue;
            }
          }
          else
          {
            localJSONObject.put("duration", com.google.android.gms.cast.internal.zzf.zzaf(this.nk));
            continue;
          }
          localJSONObject.put("tracks", localObject);
          if (this.nm != null) {
            localJSONObject.put("textTrackStyle", this.nm.toJson());
          }
          if (this.np == null) {
            break label239;
          }
          localJSONObject.put("customData", this.np);
          return localJSONObject;
        }
      }
      catch (JSONException localJSONException) {}
      Object localObject = "NONE";
      continue;
      label239:
      return localJSONObject;
      String str = "BUFFERED";
      continue;
      str = "LIVE";
    }
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    if (this.np == null) {}
    for (String str = null;; str = this.np.toString())
    {
      this.nn = str;
      zzf.zza(this, paramParcel, paramInt);
      return;
    }
  }
  
  void zza(MediaMetadata paramMediaMetadata)
  {
    this.nj = paramMediaMetadata;
  }
  
  void zzab(long paramLong)
    throws IllegalArgumentException
  {
    if ((paramLong < 0L) && (paramLong != -1L)) {
      throw new IllegalArgumentException("Invalid stream duration");
    }
    this.nk = paramLong;
  }
  
  void zzali()
    throws IllegalArgumentException
  {
    if (TextUtils.isEmpty(this.ng)) {
      throw new IllegalArgumentException("content ID cannot be null or empty");
    }
    if (TextUtils.isEmpty(this.ni)) {
      throw new IllegalArgumentException("content type cannot be null or empty");
    }
    if (this.nh == -1) {
      throw new IllegalArgumentException("a valid stream type must be specified");
    }
  }
  
  void zzx(List<MediaTrack> paramList)
  {
    this.nl = paramList;
  }
  
  public void zzy(List<AdBreakInfo> paramList)
  {
    this.no = paramList;
  }
  
  public static class Builder
  {
    private final MediaInfo nq;
    
    public Builder(String paramString)
      throws IllegalArgumentException
    {
      if (TextUtils.isEmpty(paramString)) {
        throw new IllegalArgumentException("Content ID cannot be empty");
      }
      this.nq = new MediaInfo(paramString);
    }
    
    public MediaInfo build()
      throws IllegalArgumentException
    {
      this.nq.zzali();
      return this.nq;
    }
    
    public Builder setContentType(String paramString)
      throws IllegalArgumentException
    {
      this.nq.setContentType(paramString);
      return this;
    }
    
    public Builder setCustomData(JSONObject paramJSONObject)
    {
      this.nq.setCustomData(paramJSONObject);
      return this;
    }
    
    public Builder setMediaTracks(List<MediaTrack> paramList)
    {
      this.nq.zzx(paramList);
      return this;
    }
    
    public Builder setMetadata(MediaMetadata paramMediaMetadata)
    {
      this.nq.zza(paramMediaMetadata);
      return this;
    }
    
    public Builder setStreamDuration(long paramLong)
      throws IllegalArgumentException
    {
      this.nq.zzab(paramLong);
      return this;
    }
    
    public Builder setStreamType(int paramInt)
      throws IllegalArgumentException
    {
      this.nq.setStreamType(paramInt);
      return this;
    }
    
    public Builder setTextTrackStyle(TextTrackStyle paramTextTrackStyle)
    {
      this.nq.setTextTrackStyle(paramTextTrackStyle);
      return this;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/MediaInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */