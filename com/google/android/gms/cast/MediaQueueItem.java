package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.cast.internal.zzf;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.util.zzp;
import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MediaQueueItem
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<MediaQueueItem> CREATOR = new zzh();
  public static final double DEFAULT_PLAYBACK_DURATION = Double.POSITIVE_INFINITY;
  public static final int INVALID_ITEM_ID = 0;
  private final int mVersionCode;
  private boolean nA;
  private double nB;
  private double nC;
  private double nD;
  private long[] nE;
  String nn;
  private JSONObject np;
  private MediaInfo ny;
  private int nz;
  
  MediaQueueItem(int paramInt1, MediaInfo paramMediaInfo, int paramInt2, boolean paramBoolean, double paramDouble1, double paramDouble2, double paramDouble3, long[] paramArrayOfLong, String paramString)
  {
    this.mVersionCode = paramInt1;
    this.ny = paramMediaInfo;
    this.nz = paramInt2;
    this.nA = paramBoolean;
    this.nB = paramDouble1;
    this.nC = paramDouble2;
    this.nD = paramDouble3;
    this.nE = paramArrayOfLong;
    this.nn = paramString;
    if (this.nn != null) {
      try
      {
        this.np = new JSONObject(this.nn);
        return;
      }
      catch (JSONException paramMediaInfo)
      {
        this.np = null;
        this.nn = null;
        return;
      }
    }
    this.np = null;
  }
  
  private MediaQueueItem(MediaInfo paramMediaInfo)
    throws IllegalArgumentException
  {
    this(1, paramMediaInfo, 0, true, 0.0D, Double.POSITIVE_INFINITY, 0.0D, null, null);
    if (paramMediaInfo == null) {
      throw new IllegalArgumentException("media cannot be null.");
    }
  }
  
  private MediaQueueItem(MediaQueueItem paramMediaQueueItem)
    throws IllegalArgumentException
  {
    this(1, paramMediaQueueItem.getMedia(), paramMediaQueueItem.getItemId(), paramMediaQueueItem.getAutoplay(), paramMediaQueueItem.getStartTime(), paramMediaQueueItem.getPlaybackDuration(), paramMediaQueueItem.getPreloadTime(), paramMediaQueueItem.getActiveTrackIds(), null);
    if (this.ny == null) {
      throw new IllegalArgumentException("media cannot be null.");
    }
    this.np = paramMediaQueueItem.getCustomData();
  }
  
  MediaQueueItem(JSONObject paramJSONObject)
    throws JSONException
  {
    this(1, null, 0, true, 0.0D, Double.POSITIVE_INFINITY, 0.0D, null, null);
    zzm(paramJSONObject);
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = true;
    boolean bool3 = false;
    if (this == paramObject) {
      bool1 = true;
    }
    int i;
    int j;
    label51:
    do
    {
      do
      {
        do
        {
          return bool1;
          bool1 = bool3;
        } while (!(paramObject instanceof MediaQueueItem));
        paramObject = (MediaQueueItem)paramObject;
        if (this.np != null) {
          break;
        }
        i = 1;
        if (((MediaQueueItem)paramObject).np != null) {
          break label190;
        }
        j = 1;
        bool1 = bool3;
      } while (i != j);
      if ((this.np == null) || (((MediaQueueItem)paramObject).np == null)) {
        break;
      }
      bool1 = bool3;
    } while (!zzp.zzf(this.np, ((MediaQueueItem)paramObject).np));
    if ((zzf.zza(this.ny, ((MediaQueueItem)paramObject).ny)) && (this.nz == ((MediaQueueItem)paramObject).nz) && (this.nA == ((MediaQueueItem)paramObject).nA) && (this.nB == ((MediaQueueItem)paramObject).nB) && (this.nC == ((MediaQueueItem)paramObject).nC) && (this.nD == ((MediaQueueItem)paramObject).nD) && (Arrays.equals(this.nE, ((MediaQueueItem)paramObject).nE))) {}
    for (boolean bool1 = bool2;; bool1 = false)
    {
      return bool1;
      i = 0;
      break;
      label190:
      j = 0;
      break label51;
    }
  }
  
  public long[] getActiveTrackIds()
  {
    return this.nE;
  }
  
  public boolean getAutoplay()
  {
    return this.nA;
  }
  
  public JSONObject getCustomData()
  {
    return this.np;
  }
  
  public int getItemId()
  {
    return this.nz;
  }
  
  public MediaInfo getMedia()
  {
    return this.ny;
  }
  
  public double getPlaybackDuration()
  {
    return this.nC;
  }
  
  public double getPreloadTime()
  {
    return this.nD;
  }
  
  public double getStartTime()
  {
    return this.nB;
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.ny, Integer.valueOf(this.nz), Boolean.valueOf(this.nA), Double.valueOf(this.nB), Double.valueOf(this.nC), Double.valueOf(this.nD), Integer.valueOf(Arrays.hashCode(this.nE)), String.valueOf(this.np) });
  }
  
  void setCustomData(JSONObject paramJSONObject)
  {
    this.np = paramJSONObject;
  }
  
  public JSONObject toJson()
  {
    JSONObject localJSONObject = new JSONObject();
    try
    {
      localJSONObject.put("media", this.ny.toJson());
      if (this.nz != 0) {
        localJSONObject.put("itemId", this.nz);
      }
      localJSONObject.put("autoplay", this.nA);
      localJSONObject.put("startTime", this.nB);
      if (this.nC != Double.POSITIVE_INFINITY) {
        localJSONObject.put("playbackDuration", this.nC);
      }
      localJSONObject.put("preloadTime", this.nD);
      if (this.nE != null)
      {
        JSONArray localJSONArray = new JSONArray();
        long[] arrayOfLong = this.nE;
        int j = arrayOfLong.length;
        int i = 0;
        while (i < j)
        {
          localJSONArray.put(arrayOfLong[i]);
          i += 1;
        }
        localJSONObject.put("activeTrackIds", localJSONArray);
      }
      if (this.np != null) {
        localJSONObject.put("customData", this.np);
      }
      return localJSONObject;
    }
    catch (JSONException localJSONException) {}
    return localJSONObject;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    if (this.np == null) {}
    for (String str = null;; str = this.np.toString())
    {
      this.nn = str;
      zzh.zza(this, paramParcel, paramInt);
      return;
    }
  }
  
  void zza(long[] paramArrayOfLong)
  {
    this.nE = paramArrayOfLong;
  }
  
  void zzali()
    throws IllegalArgumentException
  {
    if (this.ny == null) {
      throw new IllegalArgumentException("media cannot be null.");
    }
    if ((Double.isNaN(this.nB)) || (this.nB < 0.0D)) {
      throw new IllegalArgumentException("startTime cannot be negative or NaN.");
    }
    if (Double.isNaN(this.nC)) {
      throw new IllegalArgumentException("playbackDuration cannot be NaN.");
    }
    if ((Double.isNaN(this.nD)) || (this.nD < 0.0D)) {
      throw new IllegalArgumentException("preloadTime cannot be negative or Nan.");
    }
  }
  
  void zzbk(boolean paramBoolean)
  {
    this.nA = paramBoolean;
  }
  
  void zzc(double paramDouble)
    throws IllegalArgumentException
  {
    if ((Double.isNaN(paramDouble)) || (paramDouble < 0.0D)) {
      throw new IllegalArgumentException("startTime cannot be negative or NaN.");
    }
    this.nB = paramDouble;
  }
  
  void zzd(double paramDouble)
    throws IllegalArgumentException
  {
    if (Double.isNaN(paramDouble)) {
      throw new IllegalArgumentException("playbackDuration cannot be NaN.");
    }
    this.nC = paramDouble;
  }
  
  void zze(double paramDouble)
    throws IllegalArgumentException
  {
    if ((Double.isNaN(paramDouble)) || (paramDouble < 0.0D)) {
      throw new IllegalArgumentException("preloadTime cannot be negative or NaN.");
    }
    this.nD = paramDouble;
  }
  
  void zzek(int paramInt)
  {
    this.nz = paramInt;
  }
  
  public boolean zzm(JSONObject paramJSONObject)
    throws JSONException
  {
    if (paramJSONObject.has("media")) {
      this.ny = new MediaInfo(paramJSONObject.getJSONObject("media"));
    }
    for (boolean bool2 = true;; bool2 = false)
    {
      boolean bool1 = bool2;
      int i;
      if (paramJSONObject.has("itemId"))
      {
        i = paramJSONObject.getInt("itemId");
        bool1 = bool2;
        if (this.nz != i)
        {
          this.nz = i;
          bool1 = true;
        }
      }
      bool2 = bool1;
      if (paramJSONObject.has("autoplay"))
      {
        boolean bool3 = paramJSONObject.getBoolean("autoplay");
        bool2 = bool1;
        if (this.nA != bool3)
        {
          this.nA = bool3;
          bool2 = true;
        }
      }
      bool1 = bool2;
      double d;
      if (paramJSONObject.has("startTime"))
      {
        d = paramJSONObject.getDouble("startTime");
        bool1 = bool2;
        if (Math.abs(d - this.nB) > 1.0E-7D)
        {
          this.nB = d;
          bool1 = true;
        }
      }
      bool2 = bool1;
      if (paramJSONObject.has("playbackDuration"))
      {
        d = paramJSONObject.getDouble("playbackDuration");
        bool2 = bool1;
        if (Math.abs(d - this.nC) > 1.0E-7D)
        {
          this.nC = d;
          bool2 = true;
        }
      }
      bool1 = bool2;
      if (paramJSONObject.has("preloadTime"))
      {
        d = paramJSONObject.getDouble("preloadTime");
        bool1 = bool2;
        if (Math.abs(d - this.nD) > 1.0E-7D)
        {
          this.nD = d;
          bool1 = true;
        }
      }
      int j;
      long[] arrayOfLong;
      if (paramJSONObject.has("activeTrackIds"))
      {
        JSONArray localJSONArray = paramJSONObject.getJSONArray("activeTrackIds");
        j = localJSONArray.length();
        arrayOfLong = new long[j];
        i = 0;
        while (i < j)
        {
          arrayOfLong[i] = localJSONArray.getLong(i);
          i += 1;
        }
        if (this.nE == null) {
          i = 1;
        }
      }
      for (;;)
      {
        if (i != 0)
        {
          this.nE = arrayOfLong;
          bool1 = true;
        }
        if (paramJSONObject.has("customData"))
        {
          this.np = paramJSONObject.getJSONObject("customData");
          return true;
          if (this.nE.length != j)
          {
            i = 1;
          }
          else
          {
            i = 0;
            for (;;)
            {
              if (i >= j) {
                break label425;
              }
              if (this.nE[i] != arrayOfLong[i])
              {
                i = 1;
                break;
              }
              i += 1;
            }
          }
        }
        else
        {
          return bool1;
          label425:
          i = 0;
          continue;
          i = 0;
          arrayOfLong = null;
        }
      }
    }
  }
  
  public static class Builder
  {
    private final MediaQueueItem nF;
    
    public Builder(MediaInfo paramMediaInfo)
      throws IllegalArgumentException
    {
      this.nF = new MediaQueueItem(paramMediaInfo, null);
    }
    
    public Builder(MediaQueueItem paramMediaQueueItem)
      throws IllegalArgumentException
    {
      this.nF = new MediaQueueItem(paramMediaQueueItem, null);
    }
    
    public Builder(JSONObject paramJSONObject)
      throws JSONException
    {
      this.nF = new MediaQueueItem(paramJSONObject);
    }
    
    public MediaQueueItem build()
    {
      this.nF.zzali();
      return this.nF;
    }
    
    public Builder clearItemId()
    {
      this.nF.zzek(0);
      return this;
    }
    
    public Builder setActiveTrackIds(long[] paramArrayOfLong)
    {
      this.nF.zza(paramArrayOfLong);
      return this;
    }
    
    public Builder setAutoplay(boolean paramBoolean)
    {
      this.nF.zzbk(paramBoolean);
      return this;
    }
    
    public Builder setCustomData(JSONObject paramJSONObject)
    {
      this.nF.setCustomData(paramJSONObject);
      return this;
    }
    
    public Builder setPlaybackDuration(double paramDouble)
    {
      this.nF.zzd(paramDouble);
      return this;
    }
    
    public Builder setPreloadTime(double paramDouble)
      throws IllegalArgumentException
    {
      this.nF.zze(paramDouble);
      return this;
    }
    
    public Builder setStartTime(double paramDouble)
      throws IllegalArgumentException
    {
      this.nF.zzc(paramDouble);
      return this;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/MediaQueueItem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */