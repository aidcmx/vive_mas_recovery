package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.util.SparseArray;
import com.google.android.gms.cast.internal.zzf;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.util.zzp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class MediaStatus
  extends AbstractSafeParcelable
{
  public static final long COMMAND_PAUSE = 1L;
  public static final long COMMAND_SEEK = 2L;
  public static final long COMMAND_SET_VOLUME = 4L;
  public static final long COMMAND_SKIP_BACKWARD = 32L;
  public static final long COMMAND_SKIP_FORWARD = 16L;
  public static final long COMMAND_TOGGLE_MUTE = 8L;
  public static final Parcelable.Creator<MediaStatus> CREATOR = new zzi();
  public static final int IDLE_REASON_CANCELED = 2;
  public static final int IDLE_REASON_ERROR = 4;
  public static final int IDLE_REASON_FINISHED = 1;
  public static final int IDLE_REASON_INTERRUPTED = 3;
  public static final int IDLE_REASON_NONE = 0;
  public static final int PLAYER_STATE_BUFFERING = 4;
  public static final int PLAYER_STATE_IDLE = 1;
  public static final int PLAYER_STATE_PAUSED = 3;
  public static final int PLAYER_STATE_PLAYING = 2;
  public static final int PLAYER_STATE_UNKNOWN = 0;
  public static final int REPEAT_MODE_REPEAT_ALL = 1;
  public static final int REPEAT_MODE_REPEAT_ALL_AND_SHUFFLE = 3;
  public static final int REPEAT_MODE_REPEAT_OFF = 0;
  public static final int REPEAT_MODE_REPEAT_SINGLE = 2;
  private final int mVersionCode;
  private long[] nE;
  private long nG;
  private int nH;
  private double nI;
  private int nJ;
  private int nK;
  private long nL;
  long nM;
  private double nN;
  private boolean nO;
  private int nP;
  private int nQ;
  int nR;
  final ArrayList<MediaQueueItem> nS = new ArrayList();
  private boolean nT;
  private final SparseArray<Integer> nU = new SparseArray();
  String nn;
  private JSONObject np;
  private MediaInfo nq;
  
  MediaStatus(int paramInt1, MediaInfo paramMediaInfo, long paramLong1, int paramInt2, double paramDouble1, int paramInt3, int paramInt4, long paramLong2, long paramLong3, double paramDouble2, boolean paramBoolean1, long[] paramArrayOfLong, int paramInt5, int paramInt6, String paramString, int paramInt7, List<MediaQueueItem> paramList, boolean paramBoolean2)
  {
    this.mVersionCode = paramInt1;
    this.nq = paramMediaInfo;
    this.nG = paramLong1;
    this.nH = paramInt2;
    this.nI = paramDouble1;
    this.nJ = paramInt3;
    this.nK = paramInt4;
    this.nL = paramLong2;
    this.nM = paramLong3;
    this.nN = paramDouble2;
    this.nO = paramBoolean1;
    this.nE = paramArrayOfLong;
    this.nP = paramInt5;
    this.nQ = paramInt6;
    this.nn = paramString;
    if (this.nn != null) {}
    for (;;)
    {
      try
      {
        this.np = new JSONObject(this.nn);
        this.nR = paramInt7;
        if ((paramList != null) && (!paramList.isEmpty())) {
          zza((MediaQueueItem[])paramList.toArray(new MediaQueueItem[paramList.size()]));
        }
        this.nT = paramBoolean2;
        return;
      }
      catch (JSONException paramMediaInfo)
      {
        this.np = null;
        this.nn = null;
        continue;
      }
      this.np = null;
    }
  }
  
  public MediaStatus(JSONObject paramJSONObject)
    throws JSONException
  {
    this(1, null, 0L, 0, 0.0D, 0, 0, 0L, 0L, 0.0D, false, null, 0, 0, null, 0, null, false);
    zza(paramJSONObject, 0);
  }
  
  private void zza(MediaQueueItem[] paramArrayOfMediaQueueItem)
  {
    this.nS.clear();
    this.nU.clear();
    int i = 0;
    while (i < paramArrayOfMediaQueueItem.length)
    {
      MediaQueueItem localMediaQueueItem = paramArrayOfMediaQueueItem[i];
      this.nS.add(localMediaQueueItem);
      this.nU.put(localMediaQueueItem.getItemId(), Integer.valueOf(i));
      i += 1;
    }
  }
  
  private boolean zza(MediaStatus paramMediaStatus)
  {
    return (this.np == null) || (paramMediaStatus.np == null) || (zzp.zzf(this.np, paramMediaStatus.np));
  }
  
  private void zzalk()
  {
    this.nR = 0;
    this.nS.clear();
    this.nU.clear();
  }
  
  private boolean zzf(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    boolean bool = true;
    if (paramInt1 != 1) {
      bool = false;
    }
    do
    {
      do
      {
        return bool;
        switch (paramInt2)
        {
        default: 
          return true;
        }
      } while (paramInt3 == 0);
      return false;
    } while (paramInt4 != 2);
    return false;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    label51:
    label56:
    do
    {
      return true;
      if (!(paramObject instanceof MediaStatus)) {
        return false;
      }
      paramObject = (MediaStatus)paramObject;
      int i;
      if (this.np == null)
      {
        i = 1;
        if (((MediaStatus)paramObject).np != null) {
          break label51;
        }
      }
      for (int j = 1;; j = 0)
      {
        if (i == j) {
          break label56;
        }
        return false;
        i = 0;
        break;
      }
    } while ((this.nG == ((MediaStatus)paramObject).nG) && (this.nH == ((MediaStatus)paramObject).nH) && (this.nI == ((MediaStatus)paramObject).nI) && (this.nJ == ((MediaStatus)paramObject).nJ) && (this.nK == ((MediaStatus)paramObject).nK) && (this.nL == ((MediaStatus)paramObject).nL) && (this.nN == ((MediaStatus)paramObject).nN) && (this.nO == ((MediaStatus)paramObject).nO) && (this.nP == ((MediaStatus)paramObject).nP) && (this.nQ == ((MediaStatus)paramObject).nQ) && (this.nR == ((MediaStatus)paramObject).nR) && (Arrays.equals(this.nE, ((MediaStatus)paramObject).nE)) && (zzf.zza(Long.valueOf(this.nM), Long.valueOf(((MediaStatus)paramObject).nM))) && (zzf.zza(this.nS, ((MediaStatus)paramObject).nS)) && (zzf.zza(this.nq, ((MediaStatus)paramObject).nq)) && (zza((MediaStatus)paramObject)) && (this.nT == ((MediaStatus)paramObject).isPlayingAd()));
    return false;
  }
  
  public long[] getActiveTrackIds()
  {
    return this.nE;
  }
  
  public int getCurrentItemId()
  {
    return this.nH;
  }
  
  public JSONObject getCustomData()
  {
    return this.np;
  }
  
  public int getIdleReason()
  {
    return this.nK;
  }
  
  public Integer getIndexById(int paramInt)
  {
    return (Integer)this.nU.get(paramInt);
  }
  
  public MediaQueueItem getItemById(int paramInt)
  {
    Integer localInteger = (Integer)this.nU.get(paramInt);
    if (localInteger == null) {
      return null;
    }
    return (MediaQueueItem)this.nS.get(localInteger.intValue());
  }
  
  public MediaQueueItem getItemByIndex(int paramInt)
  {
    if ((paramInt < 0) || (paramInt >= this.nS.size())) {
      return null;
    }
    return (MediaQueueItem)this.nS.get(paramInt);
  }
  
  public int getLoadingItemId()
  {
    return this.nP;
  }
  
  public MediaInfo getMediaInfo()
  {
    return this.nq;
  }
  
  public double getPlaybackRate()
  {
    return this.nI;
  }
  
  public int getPlayerState()
  {
    return this.nJ;
  }
  
  public int getPreloadedItemId()
  {
    return this.nQ;
  }
  
  public MediaQueueItem getQueueItem(int paramInt)
  {
    return getItemByIndex(paramInt);
  }
  
  public MediaQueueItem getQueueItemById(int paramInt)
  {
    return getItemById(paramInt);
  }
  
  public int getQueueItemCount()
  {
    return this.nS.size();
  }
  
  public List<MediaQueueItem> getQueueItems()
  {
    return this.nS;
  }
  
  public int getQueueRepeatMode()
  {
    return this.nR;
  }
  
  public long getStreamPosition()
  {
    return this.nL;
  }
  
  public double getStreamVolume()
  {
    return this.nN;
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.nq, Long.valueOf(this.nG), Integer.valueOf(this.nH), Double.valueOf(this.nI), Integer.valueOf(this.nJ), Integer.valueOf(this.nK), Long.valueOf(this.nL), Long.valueOf(this.nM), Double.valueOf(this.nN), Boolean.valueOf(this.nO), Integer.valueOf(Arrays.hashCode(this.nE)), Integer.valueOf(this.nP), Integer.valueOf(this.nQ), this.np, Integer.valueOf(this.nR), this.nS, Boolean.valueOf(this.nT) });
  }
  
  public boolean isMediaCommandSupported(long paramLong)
  {
    return (this.nM & paramLong) != 0L;
  }
  
  public boolean isMute()
  {
    return this.nO;
  }
  
  public boolean isPlayingAd()
  {
    return this.nT;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    if (this.np == null) {}
    for (String str = null;; str = this.np.toString())
    {
      this.nn = str;
      zzi.zza(this, paramParcel, paramInt);
      return;
    }
  }
  
  public int zza(JSONObject paramJSONObject, int paramInt)
    throws JSONException
  {
    int i1 = 2;
    int n = 1;
    long l = paramJSONObject.getLong("mediaSessionId");
    if (l != this.nG) {
      this.nG = l;
    }
    for (int j = 1;; j = 0)
    {
      int k = j;
      Object localObject;
      int i;
      if (paramJSONObject.has("playerState"))
      {
        localObject = paramJSONObject.getString("playerState");
        if (!((String)localObject).equals("IDLE")) {
          break label480;
        }
        i = 1;
      }
      for (;;)
      {
        int m = j;
        if (i != this.nJ)
        {
          this.nJ = i;
          m = j | 0x2;
        }
        k = m;
        if (i == 1)
        {
          k = m;
          if (paramJSONObject.has("idleReason"))
          {
            localObject = paramJSONObject.getString("idleReason");
            if (!((String)localObject).equals("CANCELLED")) {
              break label531;
            }
            i = i1;
          }
        }
        for (;;)
        {
          label144:
          k = m;
          if (i != this.nK)
          {
            this.nK = i;
            k = m | 0x2;
          }
          i = k;
          double d;
          if (paramJSONObject.has("playbackRate"))
          {
            d = paramJSONObject.getDouble("playbackRate");
            i = k;
            if (this.nI != d)
            {
              this.nI = d;
              i = k | 0x2;
            }
          }
          k = i;
          if (paramJSONObject.has("currentTime"))
          {
            k = i;
            if ((paramInt & 0x2) == 0)
            {
              l = zzf.zzf(paramJSONObject.getDouble("currentTime"));
              k = i;
              if (l != this.nL)
              {
                this.nL = l;
                k = i | 0x2;
              }
            }
          }
          j = k;
          if (paramJSONObject.has("supportedMediaCommands"))
          {
            l = paramJSONObject.getLong("supportedMediaCommands");
            j = k;
            if (l != this.nM)
            {
              this.nM = l;
              j = k | 0x2;
            }
          }
          i = j;
          if (paramJSONObject.has("volume"))
          {
            i = j;
            if ((paramInt & 0x1) == 0)
            {
              localObject = paramJSONObject.getJSONObject("volume");
              d = ((JSONObject)localObject).getDouble("level");
              paramInt = j;
              if (d != this.nN)
              {
                this.nN = d;
                paramInt = j | 0x2;
              }
              boolean bool = ((JSONObject)localObject).getBoolean("muted");
              i = paramInt;
              if (bool != this.nO)
              {
                this.nO = bool;
                i = paramInt | 0x2;
              }
            }
          }
          if (paramJSONObject.has("activeTrackIds"))
          {
            JSONArray localJSONArray = paramJSONObject.getJSONArray("activeTrackIds");
            k = localJSONArray.length();
            localObject = new long[k];
            paramInt = 0;
            for (;;)
            {
              if (paramInt < k)
              {
                localObject[paramInt] = localJSONArray.getLong(paramInt);
                paramInt += 1;
                continue;
                label480:
                if (((String)localObject).equals("PLAYING"))
                {
                  i = 2;
                  break;
                }
                if (((String)localObject).equals("PAUSED"))
                {
                  i = 3;
                  break;
                }
                if (!((String)localObject).equals("BUFFERING")) {
                  break label1016;
                }
                i = 4;
                break;
                label531:
                if (((String)localObject).equals("INTERRUPTED"))
                {
                  i = 3;
                  break label144;
                }
                if (((String)localObject).equals("FINISHED"))
                {
                  i = 1;
                  break label144;
                }
                if (!((String)localObject).equals("ERROR")) {
                  break label1010;
                }
                i = 4;
                break label144;
              }
            }
            if (this.nE == null) {
              paramInt = n;
            }
          }
          for (;;)
          {
            if (paramInt != 0) {
              this.nE = ((long[])localObject);
            }
            j = paramInt;
            for (;;)
            {
              label605:
              paramInt = i;
              if (j != 0)
              {
                this.nE = ((long[])localObject);
                paramInt = i | 0x2;
              }
              i = paramInt;
              if (paramJSONObject.has("customData"))
              {
                this.np = paramJSONObject.getJSONObject("customData");
                this.nn = null;
                i = paramInt | 0x2;
              }
              paramInt = i;
              if (paramJSONObject.has("media"))
              {
                localObject = paramJSONObject.getJSONObject("media");
                this.nq = new MediaInfo((JSONObject)localObject);
                i |= 0x2;
                paramInt = i;
                if (((JSONObject)localObject).has("metadata")) {
                  paramInt = i | 0x4;
                }
              }
              i = paramInt;
              if (paramJSONObject.has("currentItemId"))
              {
                j = paramJSONObject.getInt("currentItemId");
                i = paramInt;
                if (this.nH != j)
                {
                  this.nH = j;
                  i = paramInt | 0x2;
                }
              }
              paramInt = paramJSONObject.optInt("preloadedItemId", 0);
              j = i;
              if (this.nQ != paramInt)
              {
                this.nQ = paramInt;
                j = i | 0x10;
              }
              i = paramJSONObject.optInt("loadingItemId", 0);
              paramInt = j;
              if (this.nP != i)
              {
                this.nP = i;
                paramInt = j | 0x2;
              }
              if (this.nq == null)
              {
                i = -1;
                label839:
                if (zzf(this.nJ, this.nK, this.nP, i)) {
                  break label959;
                }
                i = paramInt;
                if (zzn(paramJSONObject)) {
                  i = paramInt | 0x8;
                }
              }
              label959:
              do
              {
                return i;
                paramInt = n;
                if (this.nE.length != k) {
                  break;
                }
                j = 0;
                for (;;)
                {
                  if (j >= k) {
                    break label1005;
                  }
                  paramInt = n;
                  if (this.nE[j] != localObject[j]) {
                    break;
                  }
                  j += 1;
                }
                if (this.nE == null) {
                  break label996;
                }
                j = 1;
                localObject = null;
                break label605;
                i = this.nq.getStreamType();
                break label839;
                this.nH = 0;
                this.nP = 0;
                this.nQ = 0;
                i = paramInt;
              } while (this.nS.isEmpty());
              zzalk();
              return paramInt | 0x8;
              label996:
              localObject = null;
              j = 0;
            }
            label1005:
            paramInt = 0;
          }
          label1010:
          i = 0;
        }
        label1016:
        i = 0;
      }
    }
  }
  
  public long zzalj()
  {
    return this.nG;
  }
  
  public void zzbl(boolean paramBoolean)
  {
    this.nT = paramBoolean;
  }
  
  boolean zzn(JSONObject paramJSONObject)
    throws JSONException
  {
    int j = 2;
    boolean bool2 = true;
    Object localObject;
    int i;
    if (paramJSONObject.has("repeatMode"))
    {
      int k = this.nR;
      localObject = paramJSONObject.getString("repeatMode");
      i = -1;
      switch (((String)localObject).hashCode())
      {
      default: 
        switch (i)
        {
        default: 
          j = k;
        case 2: 
          label115:
          if (this.nR != j) {
            this.nR = j;
          }
          break;
        }
        break;
      }
    }
    for (boolean bool1 = true;; bool1 = false)
    {
      if (paramJSONObject.has("items"))
      {
        paramJSONObject = paramJSONObject.getJSONArray("items");
        j = paramJSONObject.length();
        localObject = new SparseArray();
        i = 0;
        for (;;)
        {
          if (i < j)
          {
            ((SparseArray)localObject).put(i, Integer.valueOf(paramJSONObject.getJSONObject(i).getInt("itemId")));
            i += 1;
            continue;
            if (!((String)localObject).equals("REPEAT_OFF")) {
              break;
            }
            i = 0;
            break;
            if (!((String)localObject).equals("REPEAT_ALL")) {
              break;
            }
            i = 1;
            break;
            if (!((String)localObject).equals("REPEAT_SINGLE")) {
              break;
            }
            i = 2;
            break;
            if (!((String)localObject).equals("REPEAT_ALL_AND_SHUFFLE")) {
              break;
            }
            i = 3;
            break;
            j = 0;
            break label115;
            j = 1;
            break label115;
            j = 3;
            break label115;
          }
        }
        MediaQueueItem[] arrayOfMediaQueueItem = new MediaQueueItem[j];
        i = 0;
        Integer localInteger;
        JSONObject localJSONObject;
        boolean bool3;
        if (i < j)
        {
          localInteger = (Integer)((SparseArray)localObject).get(i);
          localJSONObject = paramJSONObject.getJSONObject(i);
          MediaQueueItem localMediaQueueItem = getItemById(localInteger.intValue());
          if (localMediaQueueItem != null)
          {
            bool3 = localMediaQueueItem.zzm(localJSONObject);
            arrayOfMediaQueueItem[i] = localMediaQueueItem;
            if (i == getIndexById(localInteger.intValue()).intValue()) {
              break label456;
            }
            bool1 = true;
          }
        }
        for (;;)
        {
          i += 1;
          break;
          if (localInteger.intValue() == this.nH)
          {
            arrayOfMediaQueueItem[i] = new MediaQueueItem.Builder(this.nq).build();
            arrayOfMediaQueueItem[i].zzm(localJSONObject);
            bool1 = true;
          }
          else
          {
            arrayOfMediaQueueItem[i] = new MediaQueueItem(localJSONObject);
            bool1 = true;
            continue;
            if (this.nS.size() != j) {
              bool1 = bool2;
            }
            for (;;)
            {
              zza(arrayOfMediaQueueItem);
              return bool1;
            }
            label456:
            bool1 |= bool3;
          }
        }
      }
      return bool1;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/MediaStatus.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */