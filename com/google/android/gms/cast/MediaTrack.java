package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.google.android.gms.cast.internal.zzf;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.util.zzp;
import java.util.Locale;
import org.json.JSONException;
import org.json.JSONObject;

public final class MediaTrack
  extends AbstractSafeParcelable
  implements ReflectedParcelable
{
  public static final Parcelable.Creator<MediaTrack> CREATOR = new zzj();
  public static final int SUBTYPE_CAPTIONS = 2;
  public static final int SUBTYPE_CHAPTERS = 4;
  public static final int SUBTYPE_DESCRIPTIONS = 3;
  public static final int SUBTYPE_METADATA = 5;
  public static final int SUBTYPE_NONE = 0;
  public static final int SUBTYPE_SUBTITLES = 1;
  public static final int SUBTYPE_UNKNOWN = -1;
  public static final int TYPE_AUDIO = 2;
  public static final int TYPE_TEXT = 1;
  public static final int TYPE_UNKNOWN = 0;
  public static final int TYPE_VIDEO = 3;
  private String bZ;
  private long hx;
  private String mName;
  private final int mVersionCode;
  private int nV;
  private int nW;
  private String ng;
  private String ni;
  String nn;
  private JSONObject np;
  
  MediaTrack(int paramInt1, long paramLong, int paramInt2, String paramString1, String paramString2, String paramString3, String paramString4, int paramInt3, String paramString5)
  {
    this.mVersionCode = paramInt1;
    this.hx = paramLong;
    this.nV = paramInt2;
    this.ng = paramString1;
    this.ni = paramString2;
    this.mName = paramString3;
    this.bZ = paramString4;
    this.nW = paramInt3;
    this.nn = paramString5;
    if (this.nn != null) {
      try
      {
        this.np = new JSONObject(this.nn);
        return;
      }
      catch (JSONException paramString1)
      {
        this.np = null;
        this.nn = null;
        return;
      }
    }
    this.np = null;
  }
  
  MediaTrack(long paramLong, int paramInt)
    throws IllegalArgumentException
  {
    this(1, 0L, 0, null, null, null, null, -1, null);
    this.hx = paramLong;
    if ((paramInt <= 0) || (paramInt > 3)) {
      throw new IllegalArgumentException(24 + "invalid type " + paramInt);
    }
    this.nV = paramInt;
  }
  
  MediaTrack(JSONObject paramJSONObject)
    throws JSONException
  {
    this(1, 0L, 0, null, null, null, null, -1, null);
    zzl(paramJSONObject);
  }
  
  private void zzl(JSONObject paramJSONObject)
    throws JSONException
  {
    this.hx = paramJSONObject.getLong("trackId");
    String str = paramJSONObject.getString("type");
    if ("TEXT".equals(str))
    {
      this.nV = 1;
      this.ng = paramJSONObject.optString("trackContentId", null);
      this.ni = paramJSONObject.optString("trackContentType", null);
      this.mName = paramJSONObject.optString("name", null);
      this.bZ = paramJSONObject.optString("language", null);
      if (!paramJSONObject.has("subtype")) {
        break label300;
      }
      str = paramJSONObject.getString("subtype");
      if (!"SUBTITLES".equals(str)) {
        break label191;
      }
      this.nW = 1;
    }
    for (;;)
    {
      this.np = paramJSONObject.optJSONObject("customData");
      return;
      if ("AUDIO".equals(str))
      {
        this.nV = 2;
        break;
      }
      if ("VIDEO".equals(str))
      {
        this.nV = 3;
        break;
      }
      paramJSONObject = String.valueOf(str);
      if (paramJSONObject.length() != 0) {}
      for (paramJSONObject = "invalid type: ".concat(paramJSONObject);; paramJSONObject = new String("invalid type: ")) {
        throw new JSONException(paramJSONObject);
      }
      label191:
      if ("CAPTIONS".equals(str))
      {
        this.nW = 2;
      }
      else if ("DESCRIPTIONS".equals(str))
      {
        this.nW = 3;
      }
      else if ("CHAPTERS".equals(str))
      {
        this.nW = 4;
      }
      else if ("METADATA".equals(str))
      {
        this.nW = 5;
      }
      else
      {
        paramJSONObject = String.valueOf(str);
        if (paramJSONObject.length() != 0) {}
        for (paramJSONObject = "invalid subtype: ".concat(paramJSONObject);; paramJSONObject = new String("invalid subtype: ")) {
          throw new JSONException(paramJSONObject);
        }
        label300:
        this.nW = 0;
      }
    }
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = true;
    boolean bool3 = false;
    if (this == paramObject) {
      bool1 = true;
    }
    int i;
    int j;
    label51:
    do
    {
      do
      {
        do
        {
          return bool1;
          bool1 = bool3;
        } while (!(paramObject instanceof MediaTrack));
        paramObject = (MediaTrack)paramObject;
        if (this.np != null) {
          break;
        }
        i = 1;
        if (((MediaTrack)paramObject).np != null) {
          break label194;
        }
        j = 1;
        bool1 = bool3;
      } while (i != j);
      if ((this.np == null) || (((MediaTrack)paramObject).np == null)) {
        break;
      }
      bool1 = bool3;
    } while (!zzp.zzf(this.np, ((MediaTrack)paramObject).np));
    if ((this.hx == ((MediaTrack)paramObject).hx) && (this.nV == ((MediaTrack)paramObject).nV) && (zzf.zza(this.ng, ((MediaTrack)paramObject).ng)) && (zzf.zza(this.ni, ((MediaTrack)paramObject).ni)) && (zzf.zza(this.mName, ((MediaTrack)paramObject).mName)) && (zzf.zza(this.bZ, ((MediaTrack)paramObject).bZ)) && (this.nW == ((MediaTrack)paramObject).nW)) {}
    for (boolean bool1 = bool2;; bool1 = false)
    {
      return bool1;
      i = 0;
      break;
      label194:
      j = 0;
      break label51;
    }
  }
  
  public String getContentId()
  {
    return this.ng;
  }
  
  public String getContentType()
  {
    return this.ni;
  }
  
  public JSONObject getCustomData()
  {
    return this.np;
  }
  
  public long getId()
  {
    return this.hx;
  }
  
  public String getLanguage()
  {
    return this.bZ;
  }
  
  public String getName()
  {
    return this.mName;
  }
  
  public int getSubtype()
  {
    return this.nW;
  }
  
  public int getType()
  {
    return this.nV;
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { Long.valueOf(this.hx), Integer.valueOf(this.nV), this.ng, this.ni, this.mName, this.bZ, Integer.valueOf(this.nW), this.np });
  }
  
  public void setContentId(String paramString)
  {
    this.ng = paramString;
  }
  
  public void setContentType(String paramString)
  {
    this.ni = paramString;
  }
  
  void setCustomData(JSONObject paramJSONObject)
  {
    this.np = paramJSONObject;
  }
  
  void setLanguage(String paramString)
  {
    this.bZ = paramString;
  }
  
  void setName(String paramString)
  {
    this.mName = paramString;
  }
  
  public JSONObject toJson()
  {
    JSONObject localJSONObject = new JSONObject();
    try
    {
      localJSONObject.put("trackId", this.hx);
      switch (this.nV)
      {
      case 1: 
        if (this.ng != null) {
          localJSONObject.put("trackContentId", this.ng);
        }
        if (this.ni != null) {
          localJSONObject.put("trackContentType", this.ni);
        }
        if (this.mName != null) {
          localJSONObject.put("name", this.mName);
        }
        if (!TextUtils.isEmpty(this.bZ)) {
          localJSONObject.put("language", this.bZ);
        }
        switch (this.nW)
        {
        }
        break;
      }
      for (;;)
      {
        if (this.np == null) {
          break label282;
        }
        localJSONObject.put("customData", this.np);
        return localJSONObject;
        localJSONObject.put("type", "TEXT");
        break;
        localJSONObject.put("type", "AUDIO");
        break;
        localJSONObject.put("type", "VIDEO");
        break;
        localJSONObject.put("subtype", "SUBTITLES");
        continue;
        localJSONObject.put("subtype", "CAPTIONS");
        continue;
        localJSONObject.put("subtype", "DESCRIPTIONS");
        continue;
        localJSONObject.put("subtype", "CHAPTERS");
        continue;
        localJSONObject.put("subtype", "METADATA");
        continue;
        break;
      }
      label282:
      return localJSONObject;
    }
    catch (JSONException localJSONException) {}
    return localJSONObject;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    if (this.np == null) {}
    for (String str = null;; str = this.np.toString())
    {
      this.nn = str;
      zzj.zza(this, paramParcel, paramInt);
      return;
    }
  }
  
  void zzen(int paramInt)
    throws IllegalArgumentException
  {
    if ((paramInt <= -1) || (paramInt > 5)) {
      throw new IllegalArgumentException(27 + "invalid subtype " + paramInt);
    }
    if ((paramInt != 0) && (this.nV != 1)) {
      throw new IllegalArgumentException("subtypes are only valid for text tracks");
    }
    this.nW = paramInt;
  }
  
  public static class Builder
  {
    private final MediaTrack nX;
    
    public Builder(long paramLong, int paramInt)
      throws IllegalArgumentException
    {
      this.nX = new MediaTrack(paramLong, paramInt);
    }
    
    public MediaTrack build()
    {
      return this.nX;
    }
    
    public Builder setContentId(String paramString)
    {
      this.nX.setContentId(paramString);
      return this;
    }
    
    public Builder setContentType(String paramString)
    {
      this.nX.setContentType(paramString);
      return this;
    }
    
    public Builder setCustomData(JSONObject paramJSONObject)
    {
      this.nX.setCustomData(paramJSONObject);
      return this;
    }
    
    public Builder setLanguage(String paramString)
    {
      this.nX.setLanguage(paramString);
      return this;
    }
    
    public Builder setLanguage(Locale paramLocale)
    {
      this.nX.setLanguage(zzf.zzb(paramLocale));
      return this;
    }
    
    public Builder setName(String paramString)
    {
      this.nX.setName(paramString);
      return this;
    }
    
    public Builder setSubtype(int paramInt)
      throws IllegalArgumentException
    {
      this.nX.zzen(paramInt);
      return this;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/MediaTrack.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */