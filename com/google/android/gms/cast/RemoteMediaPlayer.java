package com.google.android.gms.cast;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import com.google.android.gms.cast.internal.zzb;
import com.google.android.gms.cast.internal.zze;
import com.google.android.gms.cast.internal.zzn;
import com.google.android.gms.cast.internal.zzn.zza;
import com.google.android.gms.cast.internal.zzo;
import com.google.android.gms.cast.internal.zzp;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import java.io.IOException;
import org.json.JSONObject;

@SuppressLint({"MissingRemoteException"})
public class RemoteMediaPlayer
  implements Cast.MessageReceivedCallback
{
  public static final String NAMESPACE = zzn.NAMESPACE;
  public static final int RESUME_STATE_PAUSE = 2;
  public static final int RESUME_STATE_PLAY = 1;
  public static final int RESUME_STATE_UNCHANGED = 0;
  public static final int STATUS_CANCELED = 2101;
  public static final int STATUS_FAILED = 2100;
  public static final int STATUS_REPLACED = 2103;
  public static final int STATUS_SUCCEEDED = 0;
  public static final int STATUS_TIMED_OUT = 2102;
  private final zzn nY;
  private final zza nZ;
  private OnPreloadStatusUpdatedListener oa;
  private OnQueueStatusUpdatedListener ob;
  private OnMetadataUpdatedListener oc;
  private OnStatusUpdatedListener od;
  private final Object zzako = new Object();
  
  public RemoteMediaPlayer()
  {
    this(new zzn(null));
  }
  
  RemoteMediaPlayer(zzn paramzzn)
  {
    this.nY = paramzzn;
    this.nY.zza(new zzn.zza()
    {
      public void onMetadataUpdated()
      {
        RemoteMediaPlayer.zzb(RemoteMediaPlayer.this);
      }
      
      public void onPreloadStatusUpdated()
      {
        RemoteMediaPlayer.zzd(RemoteMediaPlayer.this);
      }
      
      public void onQueueStatusUpdated()
      {
        RemoteMediaPlayer.zzc(RemoteMediaPlayer.this);
      }
      
      public void onStatusUpdated()
      {
        RemoteMediaPlayer.zza(RemoteMediaPlayer.this);
      }
    });
    this.nZ = new zza();
    this.nY.zza(this.nZ);
  }
  
  private void onMetadataUpdated()
  {
    if (this.oc != null) {
      this.oc.onMetadataUpdated();
    }
  }
  
  private void onPreloadStatusUpdated()
  {
    if (this.oa != null) {
      this.oa.onPreloadStatusUpdated();
    }
  }
  
  private void onQueueStatusUpdated()
  {
    if (this.ob != null) {
      this.ob.onQueueStatusUpdated();
    }
  }
  
  private void onStatusUpdated()
  {
    if (this.od != null) {
      this.od.onStatusUpdated();
    }
  }
  
  private int zzep(int paramInt)
  {
    MediaStatus localMediaStatus = getMediaStatus();
    int i = 0;
    while (i < localMediaStatus.getQueueItemCount())
    {
      if (localMediaStatus.getQueueItem(i).getItemId() == paramInt) {
        return i;
      }
      i += 1;
    }
    return -1;
  }
  
  public long getApproximateStreamPosition()
  {
    synchronized (this.zzako)
    {
      long l = this.nY.getApproximateStreamPosition();
      return l;
    }
  }
  
  public MediaInfo getMediaInfo()
  {
    synchronized (this.zzako)
    {
      MediaInfo localMediaInfo = this.nY.getMediaInfo();
      return localMediaInfo;
    }
  }
  
  public MediaStatus getMediaStatus()
  {
    synchronized (this.zzako)
    {
      MediaStatus localMediaStatus = this.nY.getMediaStatus();
      return localMediaStatus;
    }
  }
  
  public String getNamespace()
  {
    return this.nY.getNamespace();
  }
  
  public long getStreamDuration()
  {
    synchronized (this.zzako)
    {
      long l = this.nY.getStreamDuration();
      return l;
    }
  }
  
  public PendingResult<MediaChannelResult> load(GoogleApiClient paramGoogleApiClient, MediaInfo paramMediaInfo)
  {
    return load(paramGoogleApiClient, paramMediaInfo, true, 0L, null, null);
  }
  
  public PendingResult<MediaChannelResult> load(GoogleApiClient paramGoogleApiClient, MediaInfo paramMediaInfo, boolean paramBoolean)
  {
    return load(paramGoogleApiClient, paramMediaInfo, paramBoolean, 0L, null, null);
  }
  
  public PendingResult<MediaChannelResult> load(GoogleApiClient paramGoogleApiClient, MediaInfo paramMediaInfo, boolean paramBoolean, long paramLong)
  {
    return load(paramGoogleApiClient, paramMediaInfo, paramBoolean, paramLong, null, null);
  }
  
  public PendingResult<MediaChannelResult> load(GoogleApiClient paramGoogleApiClient, MediaInfo paramMediaInfo, boolean paramBoolean, long paramLong, JSONObject paramJSONObject)
  {
    return load(paramGoogleApiClient, paramMediaInfo, paramBoolean, paramLong, null, paramJSONObject);
  }
  
  public PendingResult<MediaChannelResult> load(final GoogleApiClient paramGoogleApiClient, final MediaInfo paramMediaInfo, final boolean paramBoolean, final long paramLong, long[] paramArrayOfLong, final JSONObject paramJSONObject)
  {
    paramGoogleApiClient.zzb(new zzb(paramGoogleApiClient)
    {
      protected void zza(zze arg1)
      {
        synchronized (RemoteMediaPlayer.zze(RemoteMediaPlayer.this))
        {
          RemoteMediaPlayer.zzf(RemoteMediaPlayer.this).zzc(paramGoogleApiClient);
          try
          {
            RemoteMediaPlayer.zzg(RemoteMediaPlayer.this).zza(this.oG, paramMediaInfo, paramBoolean, paramLong, paramJSONObject, this.om);
          }
          catch (IOException localIOException)
          {
            for (;;)
            {
              zzc((RemoteMediaPlayer.MediaChannelResult)zzc(new Status(2100)));
              RemoteMediaPlayer.zzf(RemoteMediaPlayer.this).zzc(null);
            }
            localObject1 = finally;
            throw ((Throwable)localObject1);
          }
          finally
          {
            RemoteMediaPlayer.zzf(RemoteMediaPlayer.this).zzc(null);
          }
          return;
        }
      }
    });
  }
  
  public void onMessageReceived(CastDevice paramCastDevice, String paramString1, String paramString2)
  {
    this.nY.zzgt(paramString2);
  }
  
  public PendingResult<MediaChannelResult> pause(GoogleApiClient paramGoogleApiClient)
  {
    return pause(paramGoogleApiClient, null);
  }
  
  public PendingResult<MediaChannelResult> pause(final GoogleApiClient paramGoogleApiClient, final JSONObject paramJSONObject)
  {
    paramGoogleApiClient.zzb(new zzb(paramGoogleApiClient)
    {
      /* Error */
      protected void zza(zze paramAnonymouszze)
      {
        // Byte code:
        //   0: aload_0
        //   1: getfield 19	com/google/android/gms/cast/RemoteMediaPlayer$17:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   4: invokestatic 37	com/google/android/gms/cast/RemoteMediaPlayer:zze	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Ljava/lang/Object;
        //   7: astore_1
        //   8: aload_1
        //   9: monitorenter
        //   10: aload_0
        //   11: getfield 19	com/google/android/gms/cast/RemoteMediaPlayer$17:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   14: invokestatic 41	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   17: aload_0
        //   18: getfield 21	com/google/android/gms/cast/RemoteMediaPlayer$17:of	Lcom/google/android/gms/common/api/GoogleApiClient;
        //   21: invokevirtual 46	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   24: aload_0
        //   25: getfield 19	com/google/android/gms/cast/RemoteMediaPlayer$17:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   28: invokestatic 50	com/google/android/gms/cast/RemoteMediaPlayer:zzg	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/internal/zzn;
        //   31: aload_0
        //   32: getfield 54	com/google/android/gms/cast/RemoteMediaPlayer$17:oG	Lcom/google/android/gms/cast/internal/zzp;
        //   35: aload_0
        //   36: getfield 23	com/google/android/gms/cast/RemoteMediaPlayer$17:om	Lorg/json/JSONObject;
        //   39: invokevirtual 59	com/google/android/gms/cast/internal/zzn:zza	(Lcom/google/android/gms/cast/internal/zzp;Lorg/json/JSONObject;)J
        //   42: pop2
        //   43: aload_0
        //   44: getfield 19	com/google/android/gms/cast/RemoteMediaPlayer$17:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   47: invokestatic 41	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   50: aconst_null
        //   51: invokevirtual 46	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   54: aload_1
        //   55: monitorexit
        //   56: return
        //   57: astore_2
        //   58: aload_0
        //   59: aload_0
        //   60: new 61	com/google/android/gms/common/api/Status
        //   63: dup
        //   64: sipush 2100
        //   67: invokespecial 64	com/google/android/gms/common/api/Status:<init>	(I)V
        //   70: invokevirtual 67	com/google/android/gms/cast/RemoteMediaPlayer$zzb:zzc	(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Result;
        //   73: checkcast 69	com/google/android/gms/cast/RemoteMediaPlayer$MediaChannelResult
        //   76: invokevirtual 72	com/google/android/gms/cast/RemoteMediaPlayer$17:zzc	(Lcom/google/android/gms/common/api/Result;)V
        //   79: aload_0
        //   80: getfield 19	com/google/android/gms/cast/RemoteMediaPlayer$17:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   83: invokestatic 41	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   86: aconst_null
        //   87: invokevirtual 46	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   90: goto -36 -> 54
        //   93: astore_2
        //   94: aload_1
        //   95: monitorexit
        //   96: aload_2
        //   97: athrow
        //   98: astore_2
        //   99: aload_0
        //   100: getfield 19	com/google/android/gms/cast/RemoteMediaPlayer$17:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   103: invokestatic 41	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   106: aconst_null
        //   107: invokevirtual 46	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   110: aload_2
        //   111: athrow
        //   112: astore_2
        //   113: goto -55 -> 58
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	116	0	this	17
        //   0	116	1	paramAnonymouszze	zze
        //   57	1	2	localzzb	com.google.android.gms.cast.internal.zzn.zzb
        //   93	4	2	localObject1	Object
        //   98	13	2	localObject2	Object
        //   112	1	2	localIOException	IOException
        // Exception table:
        //   from	to	target	type
        //   24	43	57	com/google/android/gms/cast/internal/zzn$zzb
        //   10	24	93	finally
        //   43	54	93	finally
        //   54	56	93	finally
        //   79	90	93	finally
        //   94	96	93	finally
        //   99	112	93	finally
        //   24	43	98	finally
        //   58	79	98	finally
        //   24	43	112	java/io/IOException
      }
    });
  }
  
  public PendingResult<MediaChannelResult> play(GoogleApiClient paramGoogleApiClient)
  {
    return play(paramGoogleApiClient, null);
  }
  
  public PendingResult<MediaChannelResult> play(final GoogleApiClient paramGoogleApiClient, final JSONObject paramJSONObject)
  {
    paramGoogleApiClient.zzb(new zzb(paramGoogleApiClient)
    {
      /* Error */
      protected void zza(zze paramAnonymouszze)
      {
        // Byte code:
        //   0: aload_0
        //   1: getfield 19	com/google/android/gms/cast/RemoteMediaPlayer$19:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   4: invokestatic 37	com/google/android/gms/cast/RemoteMediaPlayer:zze	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Ljava/lang/Object;
        //   7: astore_1
        //   8: aload_1
        //   9: monitorenter
        //   10: aload_0
        //   11: getfield 19	com/google/android/gms/cast/RemoteMediaPlayer$19:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   14: invokestatic 41	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   17: aload_0
        //   18: getfield 21	com/google/android/gms/cast/RemoteMediaPlayer$19:of	Lcom/google/android/gms/common/api/GoogleApiClient;
        //   21: invokevirtual 46	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   24: aload_0
        //   25: getfield 19	com/google/android/gms/cast/RemoteMediaPlayer$19:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   28: invokestatic 50	com/google/android/gms/cast/RemoteMediaPlayer:zzg	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/internal/zzn;
        //   31: aload_0
        //   32: getfield 54	com/google/android/gms/cast/RemoteMediaPlayer$19:oG	Lcom/google/android/gms/cast/internal/zzp;
        //   35: aload_0
        //   36: getfield 23	com/google/android/gms/cast/RemoteMediaPlayer$19:om	Lorg/json/JSONObject;
        //   39: invokevirtual 59	com/google/android/gms/cast/internal/zzn:zzc	(Lcom/google/android/gms/cast/internal/zzp;Lorg/json/JSONObject;)J
        //   42: pop2
        //   43: aload_0
        //   44: getfield 19	com/google/android/gms/cast/RemoteMediaPlayer$19:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   47: invokestatic 41	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   50: aconst_null
        //   51: invokevirtual 46	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   54: aload_1
        //   55: monitorexit
        //   56: return
        //   57: astore_2
        //   58: aload_0
        //   59: aload_0
        //   60: new 61	com/google/android/gms/common/api/Status
        //   63: dup
        //   64: sipush 2100
        //   67: invokespecial 64	com/google/android/gms/common/api/Status:<init>	(I)V
        //   70: invokevirtual 67	com/google/android/gms/cast/RemoteMediaPlayer$zzb:zzc	(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Result;
        //   73: checkcast 69	com/google/android/gms/cast/RemoteMediaPlayer$MediaChannelResult
        //   76: invokevirtual 72	com/google/android/gms/cast/RemoteMediaPlayer$19:zzc	(Lcom/google/android/gms/common/api/Result;)V
        //   79: aload_0
        //   80: getfield 19	com/google/android/gms/cast/RemoteMediaPlayer$19:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   83: invokestatic 41	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   86: aconst_null
        //   87: invokevirtual 46	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   90: goto -36 -> 54
        //   93: astore_2
        //   94: aload_1
        //   95: monitorexit
        //   96: aload_2
        //   97: athrow
        //   98: astore_2
        //   99: aload_0
        //   100: getfield 19	com/google/android/gms/cast/RemoteMediaPlayer$19:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   103: invokestatic 41	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   106: aconst_null
        //   107: invokevirtual 46	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   110: aload_2
        //   111: athrow
        //   112: astore_2
        //   113: goto -55 -> 58
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	116	0	this	19
        //   0	116	1	paramAnonymouszze	zze
        //   57	1	2	localzzb	com.google.android.gms.cast.internal.zzn.zzb
        //   93	4	2	localObject1	Object
        //   98	13	2	localObject2	Object
        //   112	1	2	localIOException	IOException
        // Exception table:
        //   from	to	target	type
        //   24	43	57	com/google/android/gms/cast/internal/zzn$zzb
        //   10	24	93	finally
        //   43	54	93	finally
        //   54	56	93	finally
        //   79	90	93	finally
        //   94	96	93	finally
        //   99	112	93	finally
        //   24	43	98	finally
        //   58	79	98	finally
        //   24	43	112	java/io/IOException
      }
    });
  }
  
  public PendingResult<MediaChannelResult> queueAppendItem(GoogleApiClient paramGoogleApiClient, MediaQueueItem paramMediaQueueItem, JSONObject paramJSONObject)
    throws IllegalArgumentException
  {
    return queueInsertItems(paramGoogleApiClient, new MediaQueueItem[] { paramMediaQueueItem }, 0, paramJSONObject);
  }
  
  public PendingResult<MediaChannelResult> queueInsertAndPlayItem(final GoogleApiClient paramGoogleApiClient, final MediaQueueItem paramMediaQueueItem, final int paramInt, final long paramLong, JSONObject paramJSONObject)
  {
    paramGoogleApiClient.zzb(new zzb(paramGoogleApiClient)
    {
      /* Error */
      protected void zza(zze paramAnonymouszze)
      {
        // Byte code:
        //   0: aload_0
        //   1: getfield 25	com/google/android/gms/cast/RemoteMediaPlayer$6:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   4: invokestatic 49	com/google/android/gms/cast/RemoteMediaPlayer:zze	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Ljava/lang/Object;
        //   7: astore_1
        //   8: aload_1
        //   9: monitorenter
        //   10: aload_0
        //   11: getfield 25	com/google/android/gms/cast/RemoteMediaPlayer$6:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   14: invokestatic 53	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   17: aload_0
        //   18: getfield 27	com/google/android/gms/cast/RemoteMediaPlayer$6:of	Lcom/google/android/gms/common/api/GoogleApiClient;
        //   21: invokevirtual 58	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   24: aload_0
        //   25: getfield 25	com/google/android/gms/cast/RemoteMediaPlayer$6:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   28: invokestatic 62	com/google/android/gms/cast/RemoteMediaPlayer:zzg	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/internal/zzn;
        //   31: astore 5
        //   33: aload_0
        //   34: getfield 66	com/google/android/gms/cast/RemoteMediaPlayer$6:oG	Lcom/google/android/gms/cast/internal/zzp;
        //   37: astore 6
        //   39: aload_0
        //   40: getfield 29	com/google/android/gms/cast/RemoteMediaPlayer$6:op	Lcom/google/android/gms/cast/MediaQueueItem;
        //   43: astore 7
        //   45: aload_0
        //   46: getfield 31	com/google/android/gms/cast/RemoteMediaPlayer$6:oo	I
        //   49: istore_2
        //   50: aload_0
        //   51: getfield 33	com/google/android/gms/cast/RemoteMediaPlayer$6:ol	J
        //   54: lstore_3
        //   55: aload_0
        //   56: getfield 35	com/google/android/gms/cast/RemoteMediaPlayer$6:om	Lorg/json/JSONObject;
        //   59: astore 8
        //   61: aload 5
        //   63: aload 6
        //   65: iconst_1
        //   66: anewarray 68	com/google/android/gms/cast/MediaQueueItem
        //   69: dup
        //   70: iconst_0
        //   71: aload 7
        //   73: aastore
        //   74: iload_2
        //   75: iconst_0
        //   76: iconst_0
        //   77: lload_3
        //   78: aload 8
        //   80: invokevirtual 73	com/google/android/gms/cast/internal/zzn:zza	(Lcom/google/android/gms/cast/internal/zzp;[Lcom/google/android/gms/cast/MediaQueueItem;IIIJLorg/json/JSONObject;)J
        //   83: pop2
        //   84: aload_0
        //   85: getfield 25	com/google/android/gms/cast/RemoteMediaPlayer$6:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   88: invokestatic 53	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   91: aconst_null
        //   92: invokevirtual 58	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   95: aload_1
        //   96: monitorexit
        //   97: return
        //   98: astore 5
        //   100: aload_0
        //   101: aload_0
        //   102: new 75	com/google/android/gms/common/api/Status
        //   105: dup
        //   106: sipush 2100
        //   109: invokespecial 78	com/google/android/gms/common/api/Status:<init>	(I)V
        //   112: invokevirtual 81	com/google/android/gms/cast/RemoteMediaPlayer$zzb:zzc	(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Result;
        //   115: checkcast 83	com/google/android/gms/cast/RemoteMediaPlayer$MediaChannelResult
        //   118: invokevirtual 86	com/google/android/gms/cast/RemoteMediaPlayer$6:zzc	(Lcom/google/android/gms/common/api/Result;)V
        //   121: aload_0
        //   122: getfield 25	com/google/android/gms/cast/RemoteMediaPlayer$6:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   125: invokestatic 53	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   128: aconst_null
        //   129: invokevirtual 58	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   132: goto -37 -> 95
        //   135: astore 5
        //   137: aload_1
        //   138: monitorexit
        //   139: aload 5
        //   141: athrow
        //   142: astore 5
        //   144: aload_0
        //   145: getfield 25	com/google/android/gms/cast/RemoteMediaPlayer$6:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   148: invokestatic 53	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   151: aconst_null
        //   152: invokevirtual 58	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   155: aload 5
        //   157: athrow
        //   158: astore 5
        //   160: goto -60 -> 100
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	163	0	this	6
        //   0	163	1	paramAnonymouszze	zze
        //   49	26	2	i	int
        //   54	24	3	l	long
        //   31	31	5	localzzn	zzn
        //   98	1	5	localzzb	com.google.android.gms.cast.internal.zzn.zzb
        //   135	5	5	localObject1	Object
        //   142	14	5	localObject2	Object
        //   158	1	5	localIOException	IOException
        //   37	27	6	localzzp	zzp
        //   43	29	7	localMediaQueueItem	MediaQueueItem
        //   59	20	8	localJSONObject	JSONObject
        // Exception table:
        //   from	to	target	type
        //   24	84	98	com/google/android/gms/cast/internal/zzn$zzb
        //   10	24	135	finally
        //   84	95	135	finally
        //   95	97	135	finally
        //   121	132	135	finally
        //   137	139	135	finally
        //   144	158	135	finally
        //   24	84	142	finally
        //   100	121	142	finally
        //   24	84	158	java/io/IOException
      }
    });
  }
  
  public PendingResult<MediaChannelResult> queueInsertAndPlayItem(GoogleApiClient paramGoogleApiClient, MediaQueueItem paramMediaQueueItem, int paramInt, JSONObject paramJSONObject)
  {
    return queueInsertAndPlayItem(paramGoogleApiClient, paramMediaQueueItem, paramInt, -1L, paramJSONObject);
  }
  
  public PendingResult<MediaChannelResult> queueInsertItems(final GoogleApiClient paramGoogleApiClient, final MediaQueueItem[] paramArrayOfMediaQueueItem, final int paramInt, final JSONObject paramJSONObject)
    throws IllegalArgumentException
  {
    paramGoogleApiClient.zzb(new zzb(paramGoogleApiClient)
    {
      /* Error */
      protected void zza(zze paramAnonymouszze)
      {
        // Byte code:
        //   0: aload_0
        //   1: getfield 23	com/google/android/gms/cast/RemoteMediaPlayer$5:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   4: invokestatic 45	com/google/android/gms/cast/RemoteMediaPlayer:zze	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Ljava/lang/Object;
        //   7: astore_1
        //   8: aload_1
        //   9: monitorenter
        //   10: aload_0
        //   11: getfield 23	com/google/android/gms/cast/RemoteMediaPlayer$5:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   14: invokestatic 49	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   17: aload_0
        //   18: getfield 25	com/google/android/gms/cast/RemoteMediaPlayer$5:of	Lcom/google/android/gms/common/api/GoogleApiClient;
        //   21: invokevirtual 54	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   24: aload_0
        //   25: getfield 23	com/google/android/gms/cast/RemoteMediaPlayer$5:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   28: invokestatic 58	com/google/android/gms/cast/RemoteMediaPlayer:zzg	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/internal/zzn;
        //   31: aload_0
        //   32: getfield 62	com/google/android/gms/cast/RemoteMediaPlayer$5:oG	Lcom/google/android/gms/cast/internal/zzp;
        //   35: aload_0
        //   36: getfield 27	com/google/android/gms/cast/RemoteMediaPlayer$5:on	[Lcom/google/android/gms/cast/MediaQueueItem;
        //   39: aload_0
        //   40: getfield 29	com/google/android/gms/cast/RemoteMediaPlayer$5:oo	I
        //   43: iconst_0
        //   44: iconst_m1
        //   45: ldc2_w 63
        //   48: aload_0
        //   49: getfield 31	com/google/android/gms/cast/RemoteMediaPlayer$5:om	Lorg/json/JSONObject;
        //   52: invokevirtual 69	com/google/android/gms/cast/internal/zzn:zza	(Lcom/google/android/gms/cast/internal/zzp;[Lcom/google/android/gms/cast/MediaQueueItem;IIIJLorg/json/JSONObject;)J
        //   55: pop2
        //   56: aload_0
        //   57: getfield 23	com/google/android/gms/cast/RemoteMediaPlayer$5:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   60: invokestatic 49	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   63: aconst_null
        //   64: invokevirtual 54	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   67: aload_1
        //   68: monitorexit
        //   69: return
        //   70: astore_2
        //   71: aload_0
        //   72: aload_0
        //   73: new 71	com/google/android/gms/common/api/Status
        //   76: dup
        //   77: sipush 2100
        //   80: invokespecial 74	com/google/android/gms/common/api/Status:<init>	(I)V
        //   83: invokevirtual 77	com/google/android/gms/cast/RemoteMediaPlayer$zzb:zzc	(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Result;
        //   86: checkcast 79	com/google/android/gms/cast/RemoteMediaPlayer$MediaChannelResult
        //   89: invokevirtual 82	com/google/android/gms/cast/RemoteMediaPlayer$5:zzc	(Lcom/google/android/gms/common/api/Result;)V
        //   92: aload_0
        //   93: getfield 23	com/google/android/gms/cast/RemoteMediaPlayer$5:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   96: invokestatic 49	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   99: aconst_null
        //   100: invokevirtual 54	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   103: goto -36 -> 67
        //   106: astore_2
        //   107: aload_1
        //   108: monitorexit
        //   109: aload_2
        //   110: athrow
        //   111: astore_2
        //   112: aload_0
        //   113: getfield 23	com/google/android/gms/cast/RemoteMediaPlayer$5:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   116: invokestatic 49	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   119: aconst_null
        //   120: invokevirtual 54	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   123: aload_2
        //   124: athrow
        //   125: astore_2
        //   126: goto -55 -> 71
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	129	0	this	5
        //   0	129	1	paramAnonymouszze	zze
        //   70	1	2	localzzb	com.google.android.gms.cast.internal.zzn.zzb
        //   106	4	2	localObject1	Object
        //   111	13	2	localObject2	Object
        //   125	1	2	localIOException	IOException
        // Exception table:
        //   from	to	target	type
        //   24	56	70	com/google/android/gms/cast/internal/zzn$zzb
        //   10	24	106	finally
        //   56	67	106	finally
        //   67	69	106	finally
        //   92	103	106	finally
        //   107	109	106	finally
        //   112	125	106	finally
        //   24	56	111	finally
        //   71	92	111	finally
        //   24	56	125	java/io/IOException
      }
    });
  }
  
  public PendingResult<MediaChannelResult> queueJumpToItem(final GoogleApiClient paramGoogleApiClient, final int paramInt, final long paramLong, JSONObject paramJSONObject)
  {
    paramGoogleApiClient.zzb(new zzb(paramGoogleApiClient)
    {
      /* Error */
      protected void zza(zze paramAnonymouszze)
      {
        // Byte code:
        //   0: aload_0
        //   1: getfield 23	com/google/android/gms/cast/RemoteMediaPlayer$15:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   4: invokestatic 45	com/google/android/gms/cast/RemoteMediaPlayer:zze	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Ljava/lang/Object;
        //   7: astore_1
        //   8: aload_1
        //   9: monitorenter
        //   10: aload_0
        //   11: getfield 23	com/google/android/gms/cast/RemoteMediaPlayer$15:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   14: aload_0
        //   15: getfield 25	com/google/android/gms/cast/RemoteMediaPlayer$15:ow	I
        //   18: invokestatic 48	com/google/android/gms/cast/RemoteMediaPlayer:zza	(Lcom/google/android/gms/cast/RemoteMediaPlayer;I)I
        //   21: iconst_m1
        //   22: if_icmpne +25 -> 47
        //   25: aload_0
        //   26: aload_0
        //   27: new 50	com/google/android/gms/common/api/Status
        //   30: dup
        //   31: iconst_0
        //   32: invokespecial 53	com/google/android/gms/common/api/Status:<init>	(I)V
        //   35: invokevirtual 57	com/google/android/gms/cast/RemoteMediaPlayer$zzb:zzc	(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Result;
        //   38: checkcast 59	com/google/android/gms/cast/RemoteMediaPlayer$MediaChannelResult
        //   41: invokevirtual 62	com/google/android/gms/cast/RemoteMediaPlayer$15:zzc	(Lcom/google/android/gms/common/api/Result;)V
        //   44: aload_1
        //   45: monitorexit
        //   46: return
        //   47: aload_0
        //   48: getfield 23	com/google/android/gms/cast/RemoteMediaPlayer$15:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   51: invokestatic 66	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   54: aload_0
        //   55: getfield 27	com/google/android/gms/cast/RemoteMediaPlayer$15:of	Lcom/google/android/gms/common/api/GoogleApiClient;
        //   58: invokevirtual 70	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   61: aload_0
        //   62: getfield 23	com/google/android/gms/cast/RemoteMediaPlayer$15:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   65: invokestatic 74	com/google/android/gms/cast/RemoteMediaPlayer:zzg	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/internal/zzn;
        //   68: aload_0
        //   69: getfield 78	com/google/android/gms/cast/RemoteMediaPlayer$15:oG	Lcom/google/android/gms/cast/internal/zzp;
        //   72: aload_0
        //   73: getfield 25	com/google/android/gms/cast/RemoteMediaPlayer$15:ow	I
        //   76: aload_0
        //   77: getfield 29	com/google/android/gms/cast/RemoteMediaPlayer$15:ol	J
        //   80: aconst_null
        //   81: iconst_0
        //   82: aconst_null
        //   83: aload_0
        //   84: getfield 31	com/google/android/gms/cast/RemoteMediaPlayer$15:om	Lorg/json/JSONObject;
        //   87: invokevirtual 83	com/google/android/gms/cast/internal/zzn:zza	(Lcom/google/android/gms/cast/internal/zzp;IJ[Lcom/google/android/gms/cast/MediaQueueItem;ILjava/lang/Integer;Lorg/json/JSONObject;)J
        //   90: pop2
        //   91: aload_0
        //   92: getfield 23	com/google/android/gms/cast/RemoteMediaPlayer$15:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   95: invokestatic 66	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   98: aconst_null
        //   99: invokevirtual 70	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   102: aload_1
        //   103: monitorexit
        //   104: return
        //   105: astore_2
        //   106: aload_1
        //   107: monitorexit
        //   108: aload_2
        //   109: athrow
        //   110: astore_2
        //   111: aload_0
        //   112: aload_0
        //   113: new 50	com/google/android/gms/common/api/Status
        //   116: dup
        //   117: sipush 2100
        //   120: invokespecial 53	com/google/android/gms/common/api/Status:<init>	(I)V
        //   123: invokevirtual 57	com/google/android/gms/cast/RemoteMediaPlayer$zzb:zzc	(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Result;
        //   126: checkcast 59	com/google/android/gms/cast/RemoteMediaPlayer$MediaChannelResult
        //   129: invokevirtual 62	com/google/android/gms/cast/RemoteMediaPlayer$15:zzc	(Lcom/google/android/gms/common/api/Result;)V
        //   132: aload_0
        //   133: getfield 23	com/google/android/gms/cast/RemoteMediaPlayer$15:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   136: invokestatic 66	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   139: aconst_null
        //   140: invokevirtual 70	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   143: goto -41 -> 102
        //   146: astore_2
        //   147: aload_0
        //   148: getfield 23	com/google/android/gms/cast/RemoteMediaPlayer$15:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   151: invokestatic 66	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   154: aconst_null
        //   155: invokevirtual 70	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   158: aload_2
        //   159: athrow
        //   160: astore_2
        //   161: goto -50 -> 111
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	164	0	this	15
        //   0	164	1	paramAnonymouszze	zze
        //   105	4	2	localObject1	Object
        //   110	1	2	localzzb	com.google.android.gms.cast.internal.zzn.zzb
        //   146	13	2	localObject2	Object
        //   160	1	2	localIOException	IOException
        // Exception table:
        //   from	to	target	type
        //   10	46	105	finally
        //   47	61	105	finally
        //   91	102	105	finally
        //   102	104	105	finally
        //   106	108	105	finally
        //   132	143	105	finally
        //   147	160	105	finally
        //   61	91	110	com/google/android/gms/cast/internal/zzn$zzb
        //   61	91	146	finally
        //   111	132	146	finally
        //   61	91	160	java/io/IOException
      }
    });
  }
  
  public PendingResult<MediaChannelResult> queueJumpToItem(GoogleApiClient paramGoogleApiClient, int paramInt, JSONObject paramJSONObject)
  {
    return queueJumpToItem(paramGoogleApiClient, paramInt, -1L, paramJSONObject);
  }
  
  public PendingResult<MediaChannelResult> queueLoad(final GoogleApiClient paramGoogleApiClient, final MediaQueueItem[] paramArrayOfMediaQueueItem, final int paramInt1, final int paramInt2, final long paramLong, JSONObject paramJSONObject)
    throws IllegalArgumentException
  {
    paramGoogleApiClient.zzb(new zzb(paramGoogleApiClient)
    {
      protected void zza(zze arg1)
      {
        synchronized (RemoteMediaPlayer.zze(RemoteMediaPlayer.this))
        {
          RemoteMediaPlayer.zzf(RemoteMediaPlayer.this).zzc(paramGoogleApiClient);
          try
          {
            RemoteMediaPlayer.zzg(RemoteMediaPlayer.this).zza(this.oG, paramArrayOfMediaQueueItem, paramInt1, paramInt2, paramLong, this.om);
          }
          catch (IOException localIOException)
          {
            for (;;)
            {
              zzc((RemoteMediaPlayer.MediaChannelResult)zzc(new Status(2100)));
              RemoteMediaPlayer.zzf(RemoteMediaPlayer.this).zzc(null);
            }
            localObject1 = finally;
            throw ((Throwable)localObject1);
          }
          finally
          {
            RemoteMediaPlayer.zzf(RemoteMediaPlayer.this).zzc(null);
          }
          return;
        }
      }
    });
  }
  
  public PendingResult<MediaChannelResult> queueLoad(GoogleApiClient paramGoogleApiClient, MediaQueueItem[] paramArrayOfMediaQueueItem, int paramInt1, int paramInt2, JSONObject paramJSONObject)
    throws IllegalArgumentException
  {
    return queueLoad(paramGoogleApiClient, paramArrayOfMediaQueueItem, paramInt1, paramInt2, -1L, paramJSONObject);
  }
  
  public PendingResult<MediaChannelResult> queueMoveItemToNewIndex(final GoogleApiClient paramGoogleApiClient, final int paramInt1, final int paramInt2, final JSONObject paramJSONObject)
  {
    paramGoogleApiClient.zzb(new zzb(paramGoogleApiClient)
    {
      /* Error */
      protected void zza(zze paramAnonymouszze)
      {
        // Byte code:
        //   0: iconst_0
        //   1: istore_3
        //   2: aload_0
        //   3: getfield 22	com/google/android/gms/cast/RemoteMediaPlayer$16:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   6: invokestatic 44	com/google/android/gms/cast/RemoteMediaPlayer:zze	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Ljava/lang/Object;
        //   9: astore_1
        //   10: aload_1
        //   11: monitorenter
        //   12: aload_0
        //   13: getfield 22	com/google/android/gms/cast/RemoteMediaPlayer$16:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   16: aload_0
        //   17: getfield 24	com/google/android/gms/cast/RemoteMediaPlayer$16:ow	I
        //   20: invokestatic 47	com/google/android/gms/cast/RemoteMediaPlayer:zza	(Lcom/google/android/gms/cast/RemoteMediaPlayer;I)I
        //   23: istore_2
        //   24: iload_2
        //   25: iconst_m1
        //   26: if_icmpne +25 -> 51
        //   29: aload_0
        //   30: aload_0
        //   31: new 49	com/google/android/gms/common/api/Status
        //   34: dup
        //   35: iconst_0
        //   36: invokespecial 52	com/google/android/gms/common/api/Status:<init>	(I)V
        //   39: invokevirtual 56	com/google/android/gms/cast/RemoteMediaPlayer$zzb:zzc	(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Result;
        //   42: checkcast 58	com/google/android/gms/cast/RemoteMediaPlayer$MediaChannelResult
        //   45: invokevirtual 61	com/google/android/gms/cast/RemoteMediaPlayer$16:zzc	(Lcom/google/android/gms/common/api/Result;)V
        //   48: aload_1
        //   49: monitorexit
        //   50: return
        //   51: aload_0
        //   52: getfield 26	com/google/android/gms/cast/RemoteMediaPlayer$16:ox	I
        //   55: ifge +56 -> 111
        //   58: aload_0
        //   59: aload_0
        //   60: new 49	com/google/android/gms/common/api/Status
        //   63: dup
        //   64: sipush 2001
        //   67: getstatic 67	java/util/Locale:ROOT	Ljava/util/Locale;
        //   70: ldc 69
        //   72: iconst_1
        //   73: anewarray 71	java/lang/Object
        //   76: dup
        //   77: iconst_0
        //   78: aload_0
        //   79: getfield 26	com/google/android/gms/cast/RemoteMediaPlayer$16:ox	I
        //   82: invokestatic 77	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //   85: aastore
        //   86: invokestatic 83	java/lang/String:format	(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
        //   89: invokespecial 86	com/google/android/gms/common/api/Status:<init>	(ILjava/lang/String;)V
        //   92: invokevirtual 56	com/google/android/gms/cast/RemoteMediaPlayer$zzb:zzc	(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Result;
        //   95: checkcast 58	com/google/android/gms/cast/RemoteMediaPlayer$MediaChannelResult
        //   98: invokevirtual 61	com/google/android/gms/cast/RemoteMediaPlayer$16:zzc	(Lcom/google/android/gms/common/api/Result;)V
        //   101: aload_1
        //   102: monitorexit
        //   103: return
        //   104: astore 4
        //   106: aload_1
        //   107: monitorexit
        //   108: aload 4
        //   110: athrow
        //   111: iload_2
        //   112: aload_0
        //   113: getfield 26	com/google/android/gms/cast/RemoteMediaPlayer$16:ox	I
        //   116: if_icmpne +25 -> 141
        //   119: aload_0
        //   120: aload_0
        //   121: new 49	com/google/android/gms/common/api/Status
        //   124: dup
        //   125: iconst_0
        //   126: invokespecial 52	com/google/android/gms/common/api/Status:<init>	(I)V
        //   129: invokevirtual 56	com/google/android/gms/cast/RemoteMediaPlayer$zzb:zzc	(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Result;
        //   132: checkcast 58	com/google/android/gms/cast/RemoteMediaPlayer$MediaChannelResult
        //   135: invokevirtual 61	com/google/android/gms/cast/RemoteMediaPlayer$16:zzc	(Lcom/google/android/gms/common/api/Result;)V
        //   138: aload_1
        //   139: monitorexit
        //   140: return
        //   141: aload_0
        //   142: getfield 26	com/google/android/gms/cast/RemoteMediaPlayer$16:ox	I
        //   145: iload_2
        //   146: if_icmple +108 -> 254
        //   149: aload_0
        //   150: getfield 26	com/google/android/gms/cast/RemoteMediaPlayer$16:ox	I
        //   153: iconst_1
        //   154: iadd
        //   155: istore_2
        //   156: aload_0
        //   157: getfield 22	com/google/android/gms/cast/RemoteMediaPlayer$16:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   160: invokevirtual 90	com/google/android/gms/cast/RemoteMediaPlayer:getMediaStatus	()Lcom/google/android/gms/cast/MediaStatus;
        //   163: iload_2
        //   164: invokevirtual 96	com/google/android/gms/cast/MediaStatus:getQueueItem	(I)Lcom/google/android/gms/cast/MediaQueueItem;
        //   167: astore 4
        //   169: iload_3
        //   170: istore_2
        //   171: aload 4
        //   173: ifnull +9 -> 182
        //   176: aload 4
        //   178: invokevirtual 102	com/google/android/gms/cast/MediaQueueItem:getItemId	()I
        //   181: istore_2
        //   182: aload_0
        //   183: getfield 22	com/google/android/gms/cast/RemoteMediaPlayer$16:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   186: invokestatic 106	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   189: aload_0
        //   190: getfield 28	com/google/android/gms/cast/RemoteMediaPlayer$16:of	Lcom/google/android/gms/common/api/GoogleApiClient;
        //   193: invokevirtual 110	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   196: aload_0
        //   197: getfield 22	com/google/android/gms/cast/RemoteMediaPlayer$16:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   200: invokestatic 114	com/google/android/gms/cast/RemoteMediaPlayer:zzg	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/internal/zzn;
        //   203: astore 4
        //   205: aload_0
        //   206: getfield 118	com/google/android/gms/cast/RemoteMediaPlayer$16:oG	Lcom/google/android/gms/cast/internal/zzp;
        //   209: astore 5
        //   211: aload_0
        //   212: getfield 24	com/google/android/gms/cast/RemoteMediaPlayer$16:ow	I
        //   215: istore_3
        //   216: aload_0
        //   217: getfield 30	com/google/android/gms/cast/RemoteMediaPlayer$16:om	Lorg/json/JSONObject;
        //   220: astore 6
        //   222: aload 4
        //   224: aload 5
        //   226: iconst_1
        //   227: newarray <illegal type>
        //   229: dup
        //   230: iconst_0
        //   231: iload_3
        //   232: iastore
        //   233: iload_2
        //   234: aload 6
        //   236: invokevirtual 123	com/google/android/gms/cast/internal/zzn:zza	(Lcom/google/android/gms/cast/internal/zzp;[IILorg/json/JSONObject;)J
        //   239: pop2
        //   240: aload_0
        //   241: getfield 22	com/google/android/gms/cast/RemoteMediaPlayer$16:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   244: invokestatic 106	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   247: aconst_null
        //   248: invokevirtual 110	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   251: aload_1
        //   252: monitorexit
        //   253: return
        //   254: aload_0
        //   255: getfield 26	com/google/android/gms/cast/RemoteMediaPlayer$16:ox	I
        //   258: istore_2
        //   259: goto -103 -> 156
        //   262: astore 4
        //   264: aload_0
        //   265: aload_0
        //   266: new 49	com/google/android/gms/common/api/Status
        //   269: dup
        //   270: sipush 2100
        //   273: invokespecial 52	com/google/android/gms/common/api/Status:<init>	(I)V
        //   276: invokevirtual 56	com/google/android/gms/cast/RemoteMediaPlayer$zzb:zzc	(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Result;
        //   279: checkcast 58	com/google/android/gms/cast/RemoteMediaPlayer$MediaChannelResult
        //   282: invokevirtual 61	com/google/android/gms/cast/RemoteMediaPlayer$16:zzc	(Lcom/google/android/gms/common/api/Result;)V
        //   285: aload_0
        //   286: getfield 22	com/google/android/gms/cast/RemoteMediaPlayer$16:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   289: invokestatic 106	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   292: aconst_null
        //   293: invokevirtual 110	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   296: goto -45 -> 251
        //   299: astore 4
        //   301: aload_0
        //   302: getfield 22	com/google/android/gms/cast/RemoteMediaPlayer$16:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   305: invokestatic 106	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   308: aconst_null
        //   309: invokevirtual 110	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   312: aload 4
        //   314: athrow
        //   315: astore 4
        //   317: goto -53 -> 264
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	320	0	this	16
        //   0	320	1	paramAnonymouszze	zze
        //   23	236	2	i	int
        //   1	231	3	j	int
        //   104	5	4	localObject1	Object
        //   167	56	4	localObject2	Object
        //   262	1	4	localzzb	com.google.android.gms.cast.internal.zzn.zzb
        //   299	14	4	localObject3	Object
        //   315	1	4	localIOException	IOException
        //   209	16	5	localzzp	zzp
        //   220	15	6	localJSONObject	JSONObject
        // Exception table:
        //   from	to	target	type
        //   12	24	104	finally
        //   29	50	104	finally
        //   51	103	104	finally
        //   106	108	104	finally
        //   111	140	104	finally
        //   141	156	104	finally
        //   156	169	104	finally
        //   176	182	104	finally
        //   182	196	104	finally
        //   240	251	104	finally
        //   251	253	104	finally
        //   254	259	104	finally
        //   285	296	104	finally
        //   301	315	104	finally
        //   196	240	262	com/google/android/gms/cast/internal/zzn$zzb
        //   196	240	299	finally
        //   264	285	299	finally
        //   196	240	315	java/io/IOException
      }
    });
  }
  
  public PendingResult<MediaChannelResult> queueNext(final GoogleApiClient paramGoogleApiClient, final JSONObject paramJSONObject)
  {
    paramGoogleApiClient.zzb(new zzb(paramGoogleApiClient)
    {
      /* Error */
      protected void zza(zze paramAnonymouszze)
      {
        // Byte code:
        //   0: aload_0
        //   1: getfield 19	com/google/android/gms/cast/RemoteMediaPlayer$11:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   4: invokestatic 37	com/google/android/gms/cast/RemoteMediaPlayer:zze	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Ljava/lang/Object;
        //   7: astore_1
        //   8: aload_1
        //   9: monitorenter
        //   10: aload_0
        //   11: getfield 19	com/google/android/gms/cast/RemoteMediaPlayer$11:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   14: invokestatic 41	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   17: aload_0
        //   18: getfield 21	com/google/android/gms/cast/RemoteMediaPlayer$11:of	Lcom/google/android/gms/common/api/GoogleApiClient;
        //   21: invokevirtual 46	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   24: aload_0
        //   25: getfield 19	com/google/android/gms/cast/RemoteMediaPlayer$11:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   28: invokestatic 50	com/google/android/gms/cast/RemoteMediaPlayer:zzg	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/internal/zzn;
        //   31: aload_0
        //   32: getfield 54	com/google/android/gms/cast/RemoteMediaPlayer$11:oG	Lcom/google/android/gms/cast/internal/zzp;
        //   35: iconst_0
        //   36: ldc2_w 55
        //   39: aconst_null
        //   40: iconst_1
        //   41: aconst_null
        //   42: aload_0
        //   43: getfield 23	com/google/android/gms/cast/RemoteMediaPlayer$11:om	Lorg/json/JSONObject;
        //   46: invokevirtual 61	com/google/android/gms/cast/internal/zzn:zza	(Lcom/google/android/gms/cast/internal/zzp;IJ[Lcom/google/android/gms/cast/MediaQueueItem;ILjava/lang/Integer;Lorg/json/JSONObject;)J
        //   49: pop2
        //   50: aload_0
        //   51: getfield 19	com/google/android/gms/cast/RemoteMediaPlayer$11:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   54: invokestatic 41	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   57: aconst_null
        //   58: invokevirtual 46	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   61: aload_1
        //   62: monitorexit
        //   63: return
        //   64: astore_2
        //   65: aload_0
        //   66: aload_0
        //   67: new 63	com/google/android/gms/common/api/Status
        //   70: dup
        //   71: sipush 2100
        //   74: invokespecial 66	com/google/android/gms/common/api/Status:<init>	(I)V
        //   77: invokevirtual 69	com/google/android/gms/cast/RemoteMediaPlayer$zzb:zzc	(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Result;
        //   80: checkcast 71	com/google/android/gms/cast/RemoteMediaPlayer$MediaChannelResult
        //   83: invokevirtual 74	com/google/android/gms/cast/RemoteMediaPlayer$11:zzc	(Lcom/google/android/gms/common/api/Result;)V
        //   86: aload_0
        //   87: getfield 19	com/google/android/gms/cast/RemoteMediaPlayer$11:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   90: invokestatic 41	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   93: aconst_null
        //   94: invokevirtual 46	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   97: goto -36 -> 61
        //   100: astore_2
        //   101: aload_1
        //   102: monitorexit
        //   103: aload_2
        //   104: athrow
        //   105: astore_2
        //   106: aload_0
        //   107: getfield 19	com/google/android/gms/cast/RemoteMediaPlayer$11:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   110: invokestatic 41	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   113: aconst_null
        //   114: invokevirtual 46	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   117: aload_2
        //   118: athrow
        //   119: astore_2
        //   120: goto -55 -> 65
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	123	0	this	11
        //   0	123	1	paramAnonymouszze	zze
        //   64	1	2	localzzb	com.google.android.gms.cast.internal.zzn.zzb
        //   100	4	2	localObject1	Object
        //   105	13	2	localObject2	Object
        //   119	1	2	localIOException	IOException
        // Exception table:
        //   from	to	target	type
        //   24	50	64	com/google/android/gms/cast/internal/zzn$zzb
        //   10	24	100	finally
        //   50	61	100	finally
        //   61	63	100	finally
        //   86	97	100	finally
        //   101	103	100	finally
        //   106	119	100	finally
        //   24	50	105	finally
        //   65	86	105	finally
        //   24	50	119	java/io/IOException
      }
    });
  }
  
  public PendingResult<MediaChannelResult> queuePrev(final GoogleApiClient paramGoogleApiClient, final JSONObject paramJSONObject)
  {
    paramGoogleApiClient.zzb(new zzb(paramGoogleApiClient)
    {
      /* Error */
      protected void zza(zze paramAnonymouszze)
      {
        // Byte code:
        //   0: aload_0
        //   1: getfield 19	com/google/android/gms/cast/RemoteMediaPlayer$10:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   4: invokestatic 37	com/google/android/gms/cast/RemoteMediaPlayer:zze	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Ljava/lang/Object;
        //   7: astore_1
        //   8: aload_1
        //   9: monitorenter
        //   10: aload_0
        //   11: getfield 19	com/google/android/gms/cast/RemoteMediaPlayer$10:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   14: invokestatic 41	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   17: aload_0
        //   18: getfield 21	com/google/android/gms/cast/RemoteMediaPlayer$10:of	Lcom/google/android/gms/common/api/GoogleApiClient;
        //   21: invokevirtual 46	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   24: aload_0
        //   25: getfield 19	com/google/android/gms/cast/RemoteMediaPlayer$10:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   28: invokestatic 50	com/google/android/gms/cast/RemoteMediaPlayer:zzg	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/internal/zzn;
        //   31: aload_0
        //   32: getfield 54	com/google/android/gms/cast/RemoteMediaPlayer$10:oG	Lcom/google/android/gms/cast/internal/zzp;
        //   35: iconst_0
        //   36: ldc2_w 55
        //   39: aconst_null
        //   40: iconst_m1
        //   41: aconst_null
        //   42: aload_0
        //   43: getfield 23	com/google/android/gms/cast/RemoteMediaPlayer$10:om	Lorg/json/JSONObject;
        //   46: invokevirtual 61	com/google/android/gms/cast/internal/zzn:zza	(Lcom/google/android/gms/cast/internal/zzp;IJ[Lcom/google/android/gms/cast/MediaQueueItem;ILjava/lang/Integer;Lorg/json/JSONObject;)J
        //   49: pop2
        //   50: aload_0
        //   51: getfield 19	com/google/android/gms/cast/RemoteMediaPlayer$10:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   54: invokestatic 41	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   57: aconst_null
        //   58: invokevirtual 46	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   61: aload_1
        //   62: monitorexit
        //   63: return
        //   64: astore_2
        //   65: aload_0
        //   66: aload_0
        //   67: new 63	com/google/android/gms/common/api/Status
        //   70: dup
        //   71: sipush 2100
        //   74: invokespecial 66	com/google/android/gms/common/api/Status:<init>	(I)V
        //   77: invokevirtual 69	com/google/android/gms/cast/RemoteMediaPlayer$zzb:zzc	(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Result;
        //   80: checkcast 71	com/google/android/gms/cast/RemoteMediaPlayer$MediaChannelResult
        //   83: invokevirtual 74	com/google/android/gms/cast/RemoteMediaPlayer$10:zzc	(Lcom/google/android/gms/common/api/Result;)V
        //   86: aload_0
        //   87: getfield 19	com/google/android/gms/cast/RemoteMediaPlayer$10:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   90: invokestatic 41	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   93: aconst_null
        //   94: invokevirtual 46	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   97: goto -36 -> 61
        //   100: astore_2
        //   101: aload_1
        //   102: monitorexit
        //   103: aload_2
        //   104: athrow
        //   105: astore_2
        //   106: aload_0
        //   107: getfield 19	com/google/android/gms/cast/RemoteMediaPlayer$10:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   110: invokestatic 41	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   113: aconst_null
        //   114: invokevirtual 46	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   117: aload_2
        //   118: athrow
        //   119: astore_2
        //   120: goto -55 -> 65
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	123	0	this	10
        //   0	123	1	paramAnonymouszze	zze
        //   64	1	2	localzzb	com.google.android.gms.cast.internal.zzn.zzb
        //   100	4	2	localObject1	Object
        //   105	13	2	localObject2	Object
        //   119	1	2	localIOException	IOException
        // Exception table:
        //   from	to	target	type
        //   24	50	64	com/google/android/gms/cast/internal/zzn$zzb
        //   10	24	100	finally
        //   50	61	100	finally
        //   61	63	100	finally
        //   86	97	100	finally
        //   101	103	100	finally
        //   106	119	100	finally
        //   24	50	105	finally
        //   65	86	105	finally
        //   24	50	119	java/io/IOException
      }
    });
  }
  
  public PendingResult<MediaChannelResult> queueRemoveItem(final GoogleApiClient paramGoogleApiClient, final int paramInt, final JSONObject paramJSONObject)
  {
    paramGoogleApiClient.zzb(new zzb(paramGoogleApiClient)
    {
      /* Error */
      protected void zza(zze paramAnonymouszze)
      {
        // Byte code:
        //   0: aload_0
        //   1: getfield 21	com/google/android/gms/cast/RemoteMediaPlayer$14:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   4: invokestatic 41	com/google/android/gms/cast/RemoteMediaPlayer:zze	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Ljava/lang/Object;
        //   7: astore_1
        //   8: aload_1
        //   9: monitorenter
        //   10: aload_0
        //   11: getfield 21	com/google/android/gms/cast/RemoteMediaPlayer$14:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   14: aload_0
        //   15: getfield 23	com/google/android/gms/cast/RemoteMediaPlayer$14:ow	I
        //   18: invokestatic 44	com/google/android/gms/cast/RemoteMediaPlayer:zza	(Lcom/google/android/gms/cast/RemoteMediaPlayer;I)I
        //   21: iconst_m1
        //   22: if_icmpne +25 -> 47
        //   25: aload_0
        //   26: aload_0
        //   27: new 46	com/google/android/gms/common/api/Status
        //   30: dup
        //   31: iconst_0
        //   32: invokespecial 49	com/google/android/gms/common/api/Status:<init>	(I)V
        //   35: invokevirtual 53	com/google/android/gms/cast/RemoteMediaPlayer$zzb:zzc	(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Result;
        //   38: checkcast 55	com/google/android/gms/cast/RemoteMediaPlayer$MediaChannelResult
        //   41: invokevirtual 58	com/google/android/gms/cast/RemoteMediaPlayer$14:zzc	(Lcom/google/android/gms/common/api/Result;)V
        //   44: aload_1
        //   45: monitorexit
        //   46: return
        //   47: aload_0
        //   48: getfield 21	com/google/android/gms/cast/RemoteMediaPlayer$14:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   51: invokestatic 62	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   54: aload_0
        //   55: getfield 25	com/google/android/gms/cast/RemoteMediaPlayer$14:of	Lcom/google/android/gms/common/api/GoogleApiClient;
        //   58: invokevirtual 66	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   61: aload_0
        //   62: getfield 21	com/google/android/gms/cast/RemoteMediaPlayer$14:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   65: invokestatic 70	com/google/android/gms/cast/RemoteMediaPlayer:zzg	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/internal/zzn;
        //   68: astore_3
        //   69: aload_0
        //   70: getfield 74	com/google/android/gms/cast/RemoteMediaPlayer$14:oG	Lcom/google/android/gms/cast/internal/zzp;
        //   73: astore 4
        //   75: aload_0
        //   76: getfield 23	com/google/android/gms/cast/RemoteMediaPlayer$14:ow	I
        //   79: istore_2
        //   80: aload_0
        //   81: getfield 27	com/google/android/gms/cast/RemoteMediaPlayer$14:om	Lorg/json/JSONObject;
        //   84: astore 5
        //   86: aload_3
        //   87: aload 4
        //   89: iconst_1
        //   90: newarray <illegal type>
        //   92: dup
        //   93: iconst_0
        //   94: iload_2
        //   95: iastore
        //   96: aload 5
        //   98: invokevirtual 79	com/google/android/gms/cast/internal/zzn:zza	(Lcom/google/android/gms/cast/internal/zzp;[ILorg/json/JSONObject;)J
        //   101: pop2
        //   102: aload_0
        //   103: getfield 21	com/google/android/gms/cast/RemoteMediaPlayer$14:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   106: invokestatic 62	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   109: aconst_null
        //   110: invokevirtual 66	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   113: aload_1
        //   114: monitorexit
        //   115: return
        //   116: astore_3
        //   117: aload_1
        //   118: monitorexit
        //   119: aload_3
        //   120: athrow
        //   121: astore_3
        //   122: aload_0
        //   123: aload_0
        //   124: new 46	com/google/android/gms/common/api/Status
        //   127: dup
        //   128: sipush 2100
        //   131: invokespecial 49	com/google/android/gms/common/api/Status:<init>	(I)V
        //   134: invokevirtual 53	com/google/android/gms/cast/RemoteMediaPlayer$zzb:zzc	(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Result;
        //   137: checkcast 55	com/google/android/gms/cast/RemoteMediaPlayer$MediaChannelResult
        //   140: invokevirtual 58	com/google/android/gms/cast/RemoteMediaPlayer$14:zzc	(Lcom/google/android/gms/common/api/Result;)V
        //   143: aload_0
        //   144: getfield 21	com/google/android/gms/cast/RemoteMediaPlayer$14:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   147: invokestatic 62	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   150: aconst_null
        //   151: invokevirtual 66	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   154: goto -41 -> 113
        //   157: astore_3
        //   158: aload_0
        //   159: getfield 21	com/google/android/gms/cast/RemoteMediaPlayer$14:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   162: invokestatic 62	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   165: aconst_null
        //   166: invokevirtual 66	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   169: aload_3
        //   170: athrow
        //   171: astore_3
        //   172: goto -50 -> 122
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	175	0	this	14
        //   0	175	1	paramAnonymouszze	zze
        //   79	16	2	i	int
        //   68	19	3	localzzn	zzn
        //   116	4	3	localObject1	Object
        //   121	1	3	localzzb	com.google.android.gms.cast.internal.zzn.zzb
        //   157	13	3	localObject2	Object
        //   171	1	3	localIOException	IOException
        //   73	15	4	localzzp	zzp
        //   84	13	5	localJSONObject	JSONObject
        // Exception table:
        //   from	to	target	type
        //   10	46	116	finally
        //   47	61	116	finally
        //   102	113	116	finally
        //   113	115	116	finally
        //   117	119	116	finally
        //   143	154	116	finally
        //   158	171	116	finally
        //   61	102	121	com/google/android/gms/cast/internal/zzn$zzb
        //   61	102	157	finally
        //   122	143	157	finally
        //   61	102	171	java/io/IOException
      }
    });
  }
  
  public PendingResult<MediaChannelResult> queueRemoveItems(final GoogleApiClient paramGoogleApiClient, final int[] paramArrayOfInt, final JSONObject paramJSONObject)
    throws IllegalArgumentException
  {
    paramGoogleApiClient.zzb(new zzb(paramGoogleApiClient)
    {
      /* Error */
      protected void zza(zze paramAnonymouszze)
      {
        // Byte code:
        //   0: aload_0
        //   1: getfield 21	com/google/android/gms/cast/RemoteMediaPlayer$8:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   4: invokestatic 41	com/google/android/gms/cast/RemoteMediaPlayer:zze	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Ljava/lang/Object;
        //   7: astore_1
        //   8: aload_1
        //   9: monitorenter
        //   10: aload_0
        //   11: getfield 21	com/google/android/gms/cast/RemoteMediaPlayer$8:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   14: invokestatic 45	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   17: aload_0
        //   18: getfield 23	com/google/android/gms/cast/RemoteMediaPlayer$8:of	Lcom/google/android/gms/common/api/GoogleApiClient;
        //   21: invokevirtual 50	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   24: aload_0
        //   25: getfield 21	com/google/android/gms/cast/RemoteMediaPlayer$8:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   28: invokestatic 54	com/google/android/gms/cast/RemoteMediaPlayer:zzg	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/internal/zzn;
        //   31: aload_0
        //   32: getfield 58	com/google/android/gms/cast/RemoteMediaPlayer$8:oG	Lcom/google/android/gms/cast/internal/zzp;
        //   35: aload_0
        //   36: getfield 25	com/google/android/gms/cast/RemoteMediaPlayer$8:or	[I
        //   39: aload_0
        //   40: getfield 27	com/google/android/gms/cast/RemoteMediaPlayer$8:om	Lorg/json/JSONObject;
        //   43: invokevirtual 63	com/google/android/gms/cast/internal/zzn:zza	(Lcom/google/android/gms/cast/internal/zzp;[ILorg/json/JSONObject;)J
        //   46: pop2
        //   47: aload_0
        //   48: getfield 21	com/google/android/gms/cast/RemoteMediaPlayer$8:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   51: invokestatic 45	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   54: aconst_null
        //   55: invokevirtual 50	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   58: aload_1
        //   59: monitorexit
        //   60: return
        //   61: astore_2
        //   62: aload_0
        //   63: aload_0
        //   64: new 65	com/google/android/gms/common/api/Status
        //   67: dup
        //   68: sipush 2100
        //   71: invokespecial 68	com/google/android/gms/common/api/Status:<init>	(I)V
        //   74: invokevirtual 71	com/google/android/gms/cast/RemoteMediaPlayer$zzb:zzc	(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Result;
        //   77: checkcast 73	com/google/android/gms/cast/RemoteMediaPlayer$MediaChannelResult
        //   80: invokevirtual 76	com/google/android/gms/cast/RemoteMediaPlayer$8:zzc	(Lcom/google/android/gms/common/api/Result;)V
        //   83: aload_0
        //   84: getfield 21	com/google/android/gms/cast/RemoteMediaPlayer$8:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   87: invokestatic 45	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   90: aconst_null
        //   91: invokevirtual 50	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   94: goto -36 -> 58
        //   97: astore_2
        //   98: aload_1
        //   99: monitorexit
        //   100: aload_2
        //   101: athrow
        //   102: astore_2
        //   103: aload_0
        //   104: getfield 21	com/google/android/gms/cast/RemoteMediaPlayer$8:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   107: invokestatic 45	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   110: aconst_null
        //   111: invokevirtual 50	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   114: aload_2
        //   115: athrow
        //   116: astore_2
        //   117: goto -55 -> 62
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	120	0	this	8
        //   0	120	1	paramAnonymouszze	zze
        //   61	1	2	localzzb	com.google.android.gms.cast.internal.zzn.zzb
        //   97	4	2	localObject1	Object
        //   102	13	2	localObject2	Object
        //   116	1	2	localIOException	IOException
        // Exception table:
        //   from	to	target	type
        //   24	47	61	com/google/android/gms/cast/internal/zzn$zzb
        //   10	24	97	finally
        //   47	58	97	finally
        //   58	60	97	finally
        //   83	94	97	finally
        //   98	100	97	finally
        //   103	116	97	finally
        //   24	47	102	finally
        //   62	83	102	finally
        //   24	47	116	java/io/IOException
      }
    });
  }
  
  public PendingResult<MediaChannelResult> queueReorderItems(final GoogleApiClient paramGoogleApiClient, final int[] paramArrayOfInt, final int paramInt, final JSONObject paramJSONObject)
    throws IllegalArgumentException
  {
    paramGoogleApiClient.zzb(new zzb(paramGoogleApiClient)
    {
      /* Error */
      protected void zza(zze paramAnonymouszze)
      {
        // Byte code:
        //   0: aload_0
        //   1: getfield 23	com/google/android/gms/cast/RemoteMediaPlayer$9:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   4: invokestatic 45	com/google/android/gms/cast/RemoteMediaPlayer:zze	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Ljava/lang/Object;
        //   7: astore_1
        //   8: aload_1
        //   9: monitorenter
        //   10: aload_0
        //   11: getfield 23	com/google/android/gms/cast/RemoteMediaPlayer$9:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   14: invokestatic 49	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   17: aload_0
        //   18: getfield 25	com/google/android/gms/cast/RemoteMediaPlayer$9:of	Lcom/google/android/gms/common/api/GoogleApiClient;
        //   21: invokevirtual 54	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   24: aload_0
        //   25: getfield 23	com/google/android/gms/cast/RemoteMediaPlayer$9:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   28: invokestatic 58	com/google/android/gms/cast/RemoteMediaPlayer:zzg	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/internal/zzn;
        //   31: aload_0
        //   32: getfield 62	com/google/android/gms/cast/RemoteMediaPlayer$9:oG	Lcom/google/android/gms/cast/internal/zzp;
        //   35: aload_0
        //   36: getfield 27	com/google/android/gms/cast/RemoteMediaPlayer$9:os	[I
        //   39: aload_0
        //   40: getfield 29	com/google/android/gms/cast/RemoteMediaPlayer$9:oo	I
        //   43: aload_0
        //   44: getfield 31	com/google/android/gms/cast/RemoteMediaPlayer$9:om	Lorg/json/JSONObject;
        //   47: invokevirtual 67	com/google/android/gms/cast/internal/zzn:zza	(Lcom/google/android/gms/cast/internal/zzp;[IILorg/json/JSONObject;)J
        //   50: pop2
        //   51: aload_0
        //   52: getfield 23	com/google/android/gms/cast/RemoteMediaPlayer$9:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   55: invokestatic 49	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   58: aconst_null
        //   59: invokevirtual 54	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   62: aload_1
        //   63: monitorexit
        //   64: return
        //   65: astore_2
        //   66: aload_0
        //   67: aload_0
        //   68: new 69	com/google/android/gms/common/api/Status
        //   71: dup
        //   72: sipush 2100
        //   75: invokespecial 72	com/google/android/gms/common/api/Status:<init>	(I)V
        //   78: invokevirtual 75	com/google/android/gms/cast/RemoteMediaPlayer$zzb:zzc	(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Result;
        //   81: checkcast 77	com/google/android/gms/cast/RemoteMediaPlayer$MediaChannelResult
        //   84: invokevirtual 80	com/google/android/gms/cast/RemoteMediaPlayer$9:zzc	(Lcom/google/android/gms/common/api/Result;)V
        //   87: aload_0
        //   88: getfield 23	com/google/android/gms/cast/RemoteMediaPlayer$9:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   91: invokestatic 49	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   94: aconst_null
        //   95: invokevirtual 54	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   98: goto -36 -> 62
        //   101: astore_2
        //   102: aload_1
        //   103: monitorexit
        //   104: aload_2
        //   105: athrow
        //   106: astore_2
        //   107: aload_0
        //   108: getfield 23	com/google/android/gms/cast/RemoteMediaPlayer$9:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   111: invokestatic 49	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   114: aconst_null
        //   115: invokevirtual 54	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   118: aload_2
        //   119: athrow
        //   120: astore_2
        //   121: goto -55 -> 66
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	124	0	this	9
        //   0	124	1	paramAnonymouszze	zze
        //   65	1	2	localzzb	com.google.android.gms.cast.internal.zzn.zzb
        //   101	4	2	localObject1	Object
        //   106	13	2	localObject2	Object
        //   120	1	2	localIOException	IOException
        // Exception table:
        //   from	to	target	type
        //   24	51	65	com/google/android/gms/cast/internal/zzn$zzb
        //   10	24	101	finally
        //   51	62	101	finally
        //   62	64	101	finally
        //   87	98	101	finally
        //   102	104	101	finally
        //   107	120	101	finally
        //   24	51	106	finally
        //   66	87	106	finally
        //   24	51	120	java/io/IOException
      }
    });
  }
  
  public PendingResult<MediaChannelResult> queueSetRepeatMode(final GoogleApiClient paramGoogleApiClient, final int paramInt, final JSONObject paramJSONObject)
  {
    paramGoogleApiClient.zzb(new zzb(paramGoogleApiClient)
    {
      /* Error */
      protected void zza(zze paramAnonymouszze)
      {
        // Byte code:
        //   0: aload_0
        //   1: getfield 21	com/google/android/gms/cast/RemoteMediaPlayer$13:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   4: invokestatic 41	com/google/android/gms/cast/RemoteMediaPlayer:zze	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Ljava/lang/Object;
        //   7: astore_1
        //   8: aload_1
        //   9: monitorenter
        //   10: aload_0
        //   11: getfield 21	com/google/android/gms/cast/RemoteMediaPlayer$13:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   14: invokestatic 45	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   17: aload_0
        //   18: getfield 23	com/google/android/gms/cast/RemoteMediaPlayer$13:of	Lcom/google/android/gms/common/api/GoogleApiClient;
        //   21: invokevirtual 50	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   24: aload_0
        //   25: getfield 21	com/google/android/gms/cast/RemoteMediaPlayer$13:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   28: invokestatic 54	com/google/android/gms/cast/RemoteMediaPlayer:zzg	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/internal/zzn;
        //   31: aload_0
        //   32: getfield 58	com/google/android/gms/cast/RemoteMediaPlayer$13:oG	Lcom/google/android/gms/cast/internal/zzp;
        //   35: iconst_0
        //   36: ldc2_w 59
        //   39: aconst_null
        //   40: iconst_0
        //   41: aload_0
        //   42: getfield 25	com/google/android/gms/cast/RemoteMediaPlayer$13:ok	I
        //   45: invokestatic 66	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //   48: aload_0
        //   49: getfield 27	com/google/android/gms/cast/RemoteMediaPlayer$13:om	Lorg/json/JSONObject;
        //   52: invokevirtual 71	com/google/android/gms/cast/internal/zzn:zza	(Lcom/google/android/gms/cast/internal/zzp;IJ[Lcom/google/android/gms/cast/MediaQueueItem;ILjava/lang/Integer;Lorg/json/JSONObject;)J
        //   55: pop2
        //   56: aload_0
        //   57: getfield 21	com/google/android/gms/cast/RemoteMediaPlayer$13:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   60: invokestatic 45	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   63: aconst_null
        //   64: invokevirtual 50	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   67: aload_1
        //   68: monitorexit
        //   69: return
        //   70: astore_2
        //   71: aload_0
        //   72: aload_0
        //   73: new 73	com/google/android/gms/common/api/Status
        //   76: dup
        //   77: sipush 2100
        //   80: invokespecial 76	com/google/android/gms/common/api/Status:<init>	(I)V
        //   83: invokevirtual 79	com/google/android/gms/cast/RemoteMediaPlayer$zzb:zzc	(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Result;
        //   86: checkcast 81	com/google/android/gms/cast/RemoteMediaPlayer$MediaChannelResult
        //   89: invokevirtual 84	com/google/android/gms/cast/RemoteMediaPlayer$13:zzc	(Lcom/google/android/gms/common/api/Result;)V
        //   92: aload_0
        //   93: getfield 21	com/google/android/gms/cast/RemoteMediaPlayer$13:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   96: invokestatic 45	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   99: aconst_null
        //   100: invokevirtual 50	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   103: goto -36 -> 67
        //   106: astore_2
        //   107: aload_1
        //   108: monitorexit
        //   109: aload_2
        //   110: athrow
        //   111: astore_2
        //   112: aload_0
        //   113: getfield 21	com/google/android/gms/cast/RemoteMediaPlayer$13:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   116: invokestatic 45	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   119: aconst_null
        //   120: invokevirtual 50	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   123: aload_2
        //   124: athrow
        //   125: astore_2
        //   126: goto -55 -> 71
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	129	0	this	13
        //   0	129	1	paramAnonymouszze	zze
        //   70	1	2	localzzb	com.google.android.gms.cast.internal.zzn.zzb
        //   106	4	2	localObject1	Object
        //   111	13	2	localObject2	Object
        //   125	1	2	localIOException	IOException
        // Exception table:
        //   from	to	target	type
        //   24	56	70	com/google/android/gms/cast/internal/zzn$zzb
        //   10	24	106	finally
        //   56	67	106	finally
        //   67	69	106	finally
        //   92	103	106	finally
        //   107	109	106	finally
        //   112	125	106	finally
        //   24	56	111	finally
        //   71	92	111	finally
        //   24	56	125	java/io/IOException
      }
    });
  }
  
  public PendingResult<MediaChannelResult> queueUpdateItems(final GoogleApiClient paramGoogleApiClient, final MediaQueueItem[] paramArrayOfMediaQueueItem, final JSONObject paramJSONObject)
  {
    paramGoogleApiClient.zzb(new zzb(paramGoogleApiClient)
    {
      /* Error */
      protected void zza(zze paramAnonymouszze)
      {
        // Byte code:
        //   0: aload_0
        //   1: getfield 21	com/google/android/gms/cast/RemoteMediaPlayer$7:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   4: invokestatic 41	com/google/android/gms/cast/RemoteMediaPlayer:zze	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Ljava/lang/Object;
        //   7: astore_1
        //   8: aload_1
        //   9: monitorenter
        //   10: aload_0
        //   11: getfield 21	com/google/android/gms/cast/RemoteMediaPlayer$7:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   14: invokestatic 45	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   17: aload_0
        //   18: getfield 23	com/google/android/gms/cast/RemoteMediaPlayer$7:of	Lcom/google/android/gms/common/api/GoogleApiClient;
        //   21: invokevirtual 50	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   24: aload_0
        //   25: getfield 21	com/google/android/gms/cast/RemoteMediaPlayer$7:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   28: invokestatic 54	com/google/android/gms/cast/RemoteMediaPlayer:zzg	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/internal/zzn;
        //   31: aload_0
        //   32: getfield 58	com/google/android/gms/cast/RemoteMediaPlayer$7:oG	Lcom/google/android/gms/cast/internal/zzp;
        //   35: iconst_0
        //   36: ldc2_w 59
        //   39: aload_0
        //   40: getfield 25	com/google/android/gms/cast/RemoteMediaPlayer$7:oq	[Lcom/google/android/gms/cast/MediaQueueItem;
        //   43: iconst_0
        //   44: aconst_null
        //   45: aload_0
        //   46: getfield 27	com/google/android/gms/cast/RemoteMediaPlayer$7:om	Lorg/json/JSONObject;
        //   49: invokevirtual 65	com/google/android/gms/cast/internal/zzn:zza	(Lcom/google/android/gms/cast/internal/zzp;IJ[Lcom/google/android/gms/cast/MediaQueueItem;ILjava/lang/Integer;Lorg/json/JSONObject;)J
        //   52: pop2
        //   53: aload_0
        //   54: getfield 21	com/google/android/gms/cast/RemoteMediaPlayer$7:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   57: invokestatic 45	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   60: aconst_null
        //   61: invokevirtual 50	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   64: aload_1
        //   65: monitorexit
        //   66: return
        //   67: astore_2
        //   68: aload_0
        //   69: aload_0
        //   70: new 67	com/google/android/gms/common/api/Status
        //   73: dup
        //   74: sipush 2100
        //   77: invokespecial 70	com/google/android/gms/common/api/Status:<init>	(I)V
        //   80: invokevirtual 73	com/google/android/gms/cast/RemoteMediaPlayer$zzb:zzc	(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Result;
        //   83: checkcast 75	com/google/android/gms/cast/RemoteMediaPlayer$MediaChannelResult
        //   86: invokevirtual 78	com/google/android/gms/cast/RemoteMediaPlayer$7:zzc	(Lcom/google/android/gms/common/api/Result;)V
        //   89: aload_0
        //   90: getfield 21	com/google/android/gms/cast/RemoteMediaPlayer$7:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   93: invokestatic 45	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   96: aconst_null
        //   97: invokevirtual 50	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   100: goto -36 -> 64
        //   103: astore_2
        //   104: aload_1
        //   105: monitorexit
        //   106: aload_2
        //   107: athrow
        //   108: astore_2
        //   109: aload_0
        //   110: getfield 21	com/google/android/gms/cast/RemoteMediaPlayer$7:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   113: invokestatic 45	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   116: aconst_null
        //   117: invokevirtual 50	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   120: aload_2
        //   121: athrow
        //   122: astore_2
        //   123: goto -55 -> 68
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	126	0	this	7
        //   0	126	1	paramAnonymouszze	zze
        //   67	1	2	localzzb	com.google.android.gms.cast.internal.zzn.zzb
        //   103	4	2	localObject1	Object
        //   108	13	2	localObject2	Object
        //   122	1	2	localIOException	IOException
        // Exception table:
        //   from	to	target	type
        //   24	53	67	com/google/android/gms/cast/internal/zzn$zzb
        //   10	24	103	finally
        //   53	64	103	finally
        //   64	66	103	finally
        //   89	100	103	finally
        //   104	106	103	finally
        //   109	122	103	finally
        //   24	53	108	finally
        //   68	89	108	finally
        //   24	53	122	java/io/IOException
      }
    });
  }
  
  public PendingResult<MediaChannelResult> requestStatus(final GoogleApiClient paramGoogleApiClient)
  {
    paramGoogleApiClient.zzb(new zzb(paramGoogleApiClient)
    {
      protected void zza(zze arg1)
      {
        synchronized (RemoteMediaPlayer.zze(RemoteMediaPlayer.this))
        {
          RemoteMediaPlayer.zzf(RemoteMediaPlayer.this).zzc(paramGoogleApiClient);
          try
          {
            RemoteMediaPlayer.zzg(RemoteMediaPlayer.this).zza(this.oG);
          }
          catch (IOException localIOException)
          {
            for (;;)
            {
              zzc((RemoteMediaPlayer.MediaChannelResult)zzc(new Status(2100)));
              RemoteMediaPlayer.zzf(RemoteMediaPlayer.this).zzc(null);
            }
            localObject1 = finally;
            throw ((Throwable)localObject1);
          }
          finally
          {
            RemoteMediaPlayer.zzf(RemoteMediaPlayer.this).zzc(null);
          }
          return;
        }
      }
    });
  }
  
  public PendingResult<MediaChannelResult> seek(GoogleApiClient paramGoogleApiClient, long paramLong)
  {
    return seek(paramGoogleApiClient, paramLong, 0, null);
  }
  
  public PendingResult<MediaChannelResult> seek(GoogleApiClient paramGoogleApiClient, long paramLong, int paramInt)
  {
    return seek(paramGoogleApiClient, paramLong, paramInt, null);
  }
  
  public PendingResult<MediaChannelResult> seek(final GoogleApiClient paramGoogleApiClient, final long paramLong, int paramInt, final JSONObject paramJSONObject)
  {
    paramGoogleApiClient.zzb(new zzb(paramGoogleApiClient)
    {
      /* Error */
      protected void zza(zze paramAnonymouszze)
      {
        // Byte code:
        //   0: aload_0
        //   1: getfield 23	com/google/android/gms/cast/RemoteMediaPlayer$20:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   4: invokestatic 45	com/google/android/gms/cast/RemoteMediaPlayer:zze	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Ljava/lang/Object;
        //   7: astore_1
        //   8: aload_1
        //   9: monitorenter
        //   10: aload_0
        //   11: getfield 23	com/google/android/gms/cast/RemoteMediaPlayer$20:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   14: invokestatic 49	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   17: aload_0
        //   18: getfield 25	com/google/android/gms/cast/RemoteMediaPlayer$20:of	Lcom/google/android/gms/common/api/GoogleApiClient;
        //   21: invokevirtual 54	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   24: aload_0
        //   25: getfield 23	com/google/android/gms/cast/RemoteMediaPlayer$20:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   28: invokestatic 58	com/google/android/gms/cast/RemoteMediaPlayer:zzg	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/internal/zzn;
        //   31: aload_0
        //   32: getfield 62	com/google/android/gms/cast/RemoteMediaPlayer$20:oG	Lcom/google/android/gms/cast/internal/zzp;
        //   35: aload_0
        //   36: getfield 27	com/google/android/gms/cast/RemoteMediaPlayer$20:oy	J
        //   39: aload_0
        //   40: getfield 29	com/google/android/gms/cast/RemoteMediaPlayer$20:oz	I
        //   43: aload_0
        //   44: getfield 31	com/google/android/gms/cast/RemoteMediaPlayer$20:om	Lorg/json/JSONObject;
        //   47: invokevirtual 67	com/google/android/gms/cast/internal/zzn:zza	(Lcom/google/android/gms/cast/internal/zzp;JILorg/json/JSONObject;)J
        //   50: pop2
        //   51: aload_0
        //   52: getfield 23	com/google/android/gms/cast/RemoteMediaPlayer$20:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   55: invokestatic 49	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   58: aconst_null
        //   59: invokevirtual 54	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   62: aload_1
        //   63: monitorexit
        //   64: return
        //   65: astore_2
        //   66: aload_0
        //   67: aload_0
        //   68: new 69	com/google/android/gms/common/api/Status
        //   71: dup
        //   72: sipush 2100
        //   75: invokespecial 72	com/google/android/gms/common/api/Status:<init>	(I)V
        //   78: invokevirtual 75	com/google/android/gms/cast/RemoteMediaPlayer$zzb:zzc	(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Result;
        //   81: checkcast 77	com/google/android/gms/cast/RemoteMediaPlayer$MediaChannelResult
        //   84: invokevirtual 80	com/google/android/gms/cast/RemoteMediaPlayer$20:zzc	(Lcom/google/android/gms/common/api/Result;)V
        //   87: aload_0
        //   88: getfield 23	com/google/android/gms/cast/RemoteMediaPlayer$20:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   91: invokestatic 49	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   94: aconst_null
        //   95: invokevirtual 54	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   98: goto -36 -> 62
        //   101: astore_2
        //   102: aload_1
        //   103: monitorexit
        //   104: aload_2
        //   105: athrow
        //   106: astore_2
        //   107: aload_0
        //   108: getfield 23	com/google/android/gms/cast/RemoteMediaPlayer$20:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   111: invokestatic 49	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   114: aconst_null
        //   115: invokevirtual 54	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   118: aload_2
        //   119: athrow
        //   120: astore_2
        //   121: goto -55 -> 66
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	124	0	this	20
        //   0	124	1	paramAnonymouszze	zze
        //   65	1	2	localzzb	com.google.android.gms.cast.internal.zzn.zzb
        //   101	4	2	localObject1	Object
        //   106	13	2	localObject2	Object
        //   120	1	2	localIOException	IOException
        // Exception table:
        //   from	to	target	type
        //   24	51	65	com/google/android/gms/cast/internal/zzn$zzb
        //   10	24	101	finally
        //   51	62	101	finally
        //   62	64	101	finally
        //   87	98	101	finally
        //   102	104	101	finally
        //   107	120	101	finally
        //   24	51	106	finally
        //   66	87	106	finally
        //   24	51	120	java/io/IOException
      }
    });
  }
  
  public PendingResult<MediaChannelResult> setActiveMediaTracks(final GoogleApiClient paramGoogleApiClient, final long[] paramArrayOfLong)
  {
    if (paramArrayOfLong == null) {
      throw new IllegalArgumentException("trackIds cannot be null");
    }
    paramGoogleApiClient.zzb(new zzb(paramGoogleApiClient)
    {
      /* Error */
      protected void zza(zze paramAnonymouszze)
      {
        // Byte code:
        //   0: aload_0
        //   1: getfield 19	com/google/android/gms/cast/RemoteMediaPlayer$2:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   4: invokestatic 37	com/google/android/gms/cast/RemoteMediaPlayer:zze	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Ljava/lang/Object;
        //   7: astore_1
        //   8: aload_1
        //   9: monitorenter
        //   10: aload_0
        //   11: getfield 19	com/google/android/gms/cast/RemoteMediaPlayer$2:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   14: invokestatic 41	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   17: aload_0
        //   18: getfield 21	com/google/android/gms/cast/RemoteMediaPlayer$2:of	Lcom/google/android/gms/common/api/GoogleApiClient;
        //   21: invokevirtual 46	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   24: aload_0
        //   25: getfield 19	com/google/android/gms/cast/RemoteMediaPlayer$2:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   28: invokestatic 50	com/google/android/gms/cast/RemoteMediaPlayer:zzg	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/internal/zzn;
        //   31: aload_0
        //   32: getfield 54	com/google/android/gms/cast/RemoteMediaPlayer$2:oG	Lcom/google/android/gms/cast/internal/zzp;
        //   35: aload_0
        //   36: getfield 23	com/google/android/gms/cast/RemoteMediaPlayer$2:og	[J
        //   39: invokevirtual 59	com/google/android/gms/cast/internal/zzn:zza	(Lcom/google/android/gms/cast/internal/zzp;[J)J
        //   42: pop2
        //   43: aload_0
        //   44: getfield 19	com/google/android/gms/cast/RemoteMediaPlayer$2:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   47: invokestatic 41	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   50: aconst_null
        //   51: invokevirtual 46	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   54: aload_1
        //   55: monitorexit
        //   56: return
        //   57: astore_2
        //   58: aload_0
        //   59: aload_0
        //   60: new 61	com/google/android/gms/common/api/Status
        //   63: dup
        //   64: sipush 2100
        //   67: invokespecial 64	com/google/android/gms/common/api/Status:<init>	(I)V
        //   70: invokevirtual 67	com/google/android/gms/cast/RemoteMediaPlayer$zzb:zzc	(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Result;
        //   73: checkcast 69	com/google/android/gms/cast/RemoteMediaPlayer$MediaChannelResult
        //   76: invokevirtual 72	com/google/android/gms/cast/RemoteMediaPlayer$2:zzc	(Lcom/google/android/gms/common/api/Result;)V
        //   79: aload_0
        //   80: getfield 19	com/google/android/gms/cast/RemoteMediaPlayer$2:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   83: invokestatic 41	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   86: aconst_null
        //   87: invokevirtual 46	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   90: goto -36 -> 54
        //   93: astore_2
        //   94: aload_1
        //   95: monitorexit
        //   96: aload_2
        //   97: athrow
        //   98: astore_2
        //   99: aload_0
        //   100: getfield 19	com/google/android/gms/cast/RemoteMediaPlayer$2:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   103: invokestatic 41	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   106: aconst_null
        //   107: invokevirtual 46	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   110: aload_2
        //   111: athrow
        //   112: astore_2
        //   113: goto -55 -> 58
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	116	0	this	2
        //   0	116	1	paramAnonymouszze	zze
        //   57	1	2	localzzb	com.google.android.gms.cast.internal.zzn.zzb
        //   93	4	2	localObject1	Object
        //   98	13	2	localObject2	Object
        //   112	1	2	localIOException	IOException
        // Exception table:
        //   from	to	target	type
        //   24	43	57	com/google/android/gms/cast/internal/zzn$zzb
        //   10	24	93	finally
        //   43	54	93	finally
        //   54	56	93	finally
        //   79	90	93	finally
        //   94	96	93	finally
        //   99	112	93	finally
        //   24	43	98	finally
        //   58	79	98	finally
        //   24	43	112	java/io/IOException
      }
    });
  }
  
  public void setOnMetadataUpdatedListener(OnMetadataUpdatedListener paramOnMetadataUpdatedListener)
  {
    this.oc = paramOnMetadataUpdatedListener;
  }
  
  public void setOnPreloadStatusUpdatedListener(OnPreloadStatusUpdatedListener paramOnPreloadStatusUpdatedListener)
  {
    this.oa = paramOnPreloadStatusUpdatedListener;
  }
  
  public void setOnQueueStatusUpdatedListener(OnQueueStatusUpdatedListener paramOnQueueStatusUpdatedListener)
  {
    this.ob = paramOnQueueStatusUpdatedListener;
  }
  
  public void setOnStatusUpdatedListener(OnStatusUpdatedListener paramOnStatusUpdatedListener)
  {
    this.od = paramOnStatusUpdatedListener;
  }
  
  public PendingResult<MediaChannelResult> setStreamMute(GoogleApiClient paramGoogleApiClient, boolean paramBoolean)
  {
    return setStreamMute(paramGoogleApiClient, paramBoolean, null);
  }
  
  public PendingResult<MediaChannelResult> setStreamMute(final GoogleApiClient paramGoogleApiClient, final boolean paramBoolean, final JSONObject paramJSONObject)
  {
    paramGoogleApiClient.zzb(new zzb(paramGoogleApiClient)
    {
      /* Error */
      protected void zza(zze paramAnonymouszze)
      {
        // Byte code:
        //   0: aload_0
        //   1: getfield 21	com/google/android/gms/cast/RemoteMediaPlayer$22:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   4: invokestatic 41	com/google/android/gms/cast/RemoteMediaPlayer:zze	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Ljava/lang/Object;
        //   7: astore_1
        //   8: aload_1
        //   9: monitorenter
        //   10: aload_0
        //   11: getfield 21	com/google/android/gms/cast/RemoteMediaPlayer$22:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   14: invokestatic 45	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   17: aload_0
        //   18: getfield 23	com/google/android/gms/cast/RemoteMediaPlayer$22:of	Lcom/google/android/gms/common/api/GoogleApiClient;
        //   21: invokevirtual 50	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   24: aload_0
        //   25: getfield 21	com/google/android/gms/cast/RemoteMediaPlayer$22:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   28: invokestatic 54	com/google/android/gms/cast/RemoteMediaPlayer:zzg	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/internal/zzn;
        //   31: aload_0
        //   32: getfield 58	com/google/android/gms/cast/RemoteMediaPlayer$22:oG	Lcom/google/android/gms/cast/internal/zzp;
        //   35: aload_0
        //   36: getfield 25	com/google/android/gms/cast/RemoteMediaPlayer$22:oB	Z
        //   39: aload_0
        //   40: getfield 27	com/google/android/gms/cast/RemoteMediaPlayer$22:om	Lorg/json/JSONObject;
        //   43: invokevirtual 63	com/google/android/gms/cast/internal/zzn:zza	(Lcom/google/android/gms/cast/internal/zzp;ZLorg/json/JSONObject;)J
        //   46: pop2
        //   47: aload_0
        //   48: getfield 21	com/google/android/gms/cast/RemoteMediaPlayer$22:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   51: invokestatic 45	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   54: aconst_null
        //   55: invokevirtual 50	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   58: aload_1
        //   59: monitorexit
        //   60: return
        //   61: astore_2
        //   62: aload_0
        //   63: aload_0
        //   64: new 65	com/google/android/gms/common/api/Status
        //   67: dup
        //   68: sipush 2100
        //   71: invokespecial 68	com/google/android/gms/common/api/Status:<init>	(I)V
        //   74: invokevirtual 71	com/google/android/gms/cast/RemoteMediaPlayer$zzb:zzc	(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Result;
        //   77: checkcast 73	com/google/android/gms/cast/RemoteMediaPlayer$MediaChannelResult
        //   80: invokevirtual 76	com/google/android/gms/cast/RemoteMediaPlayer$22:zzc	(Lcom/google/android/gms/common/api/Result;)V
        //   83: aload_0
        //   84: getfield 21	com/google/android/gms/cast/RemoteMediaPlayer$22:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   87: invokestatic 45	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   90: aconst_null
        //   91: invokevirtual 50	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   94: goto -36 -> 58
        //   97: astore_2
        //   98: aload_1
        //   99: monitorexit
        //   100: aload_2
        //   101: athrow
        //   102: astore_2
        //   103: aload_0
        //   104: getfield 21	com/google/android/gms/cast/RemoteMediaPlayer$22:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   107: invokestatic 45	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   110: aconst_null
        //   111: invokevirtual 50	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   114: aload_2
        //   115: athrow
        //   116: astore_2
        //   117: goto -55 -> 62
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	120	0	this	22
        //   0	120	1	paramAnonymouszze	zze
        //   61	1	2	localzzb	com.google.android.gms.cast.internal.zzn.zzb
        //   97	4	2	localObject1	Object
        //   102	13	2	localObject2	Object
        //   116	1	2	localIOException	IOException
        // Exception table:
        //   from	to	target	type
        //   24	47	61	com/google/android/gms/cast/internal/zzn$zzb
        //   10	24	97	finally
        //   47	58	97	finally
        //   58	60	97	finally
        //   83	94	97	finally
        //   98	100	97	finally
        //   103	116	97	finally
        //   24	47	102	finally
        //   62	83	102	finally
        //   24	47	116	java/io/IOException
      }
    });
  }
  
  public PendingResult<MediaChannelResult> setStreamVolume(GoogleApiClient paramGoogleApiClient, double paramDouble)
    throws IllegalArgumentException
  {
    return setStreamVolume(paramGoogleApiClient, paramDouble, null);
  }
  
  public PendingResult<MediaChannelResult> setStreamVolume(final GoogleApiClient paramGoogleApiClient, final double paramDouble, JSONObject paramJSONObject)
    throws IllegalArgumentException
  {
    if ((Double.isInfinite(paramDouble)) || (Double.isNaN(paramDouble))) {
      throw new IllegalArgumentException(41 + "Volume cannot be " + paramDouble);
    }
    paramGoogleApiClient.zzb(new zzb(paramGoogleApiClient)
    {
      /* Error */
      protected void zza(zze paramAnonymouszze)
      {
        // Byte code:
        //   0: aload_0
        //   1: getfield 21	com/google/android/gms/cast/RemoteMediaPlayer$21:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   4: invokestatic 43	com/google/android/gms/cast/RemoteMediaPlayer:zze	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Ljava/lang/Object;
        //   7: astore_1
        //   8: aload_1
        //   9: monitorenter
        //   10: aload_0
        //   11: getfield 21	com/google/android/gms/cast/RemoteMediaPlayer$21:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   14: invokestatic 47	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   17: aload_0
        //   18: getfield 23	com/google/android/gms/cast/RemoteMediaPlayer$21:of	Lcom/google/android/gms/common/api/GoogleApiClient;
        //   21: invokevirtual 52	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   24: aload_0
        //   25: getfield 21	com/google/android/gms/cast/RemoteMediaPlayer$21:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   28: invokestatic 56	com/google/android/gms/cast/RemoteMediaPlayer:zzg	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/internal/zzn;
        //   31: aload_0
        //   32: getfield 60	com/google/android/gms/cast/RemoteMediaPlayer$21:oG	Lcom/google/android/gms/cast/internal/zzp;
        //   35: aload_0
        //   36: getfield 25	com/google/android/gms/cast/RemoteMediaPlayer$21:oA	D
        //   39: aload_0
        //   40: getfield 27	com/google/android/gms/cast/RemoteMediaPlayer$21:om	Lorg/json/JSONObject;
        //   43: invokevirtual 65	com/google/android/gms/cast/internal/zzn:zza	(Lcom/google/android/gms/cast/internal/zzp;DLorg/json/JSONObject;)J
        //   46: pop2
        //   47: aload_0
        //   48: getfield 21	com/google/android/gms/cast/RemoteMediaPlayer$21:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   51: invokestatic 47	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   54: aconst_null
        //   55: invokevirtual 52	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   58: aload_1
        //   59: monitorexit
        //   60: return
        //   61: astore_2
        //   62: aload_0
        //   63: aload_0
        //   64: new 67	com/google/android/gms/common/api/Status
        //   67: dup
        //   68: sipush 2100
        //   71: invokespecial 70	com/google/android/gms/common/api/Status:<init>	(I)V
        //   74: invokevirtual 73	com/google/android/gms/cast/RemoteMediaPlayer$zzb:zzc	(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Result;
        //   77: checkcast 75	com/google/android/gms/cast/RemoteMediaPlayer$MediaChannelResult
        //   80: invokevirtual 78	com/google/android/gms/cast/RemoteMediaPlayer$21:zzc	(Lcom/google/android/gms/common/api/Result;)V
        //   83: aload_0
        //   84: getfield 21	com/google/android/gms/cast/RemoteMediaPlayer$21:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   87: invokestatic 47	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   90: aconst_null
        //   91: invokevirtual 52	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   94: goto -36 -> 58
        //   97: astore_2
        //   98: aload_1
        //   99: monitorexit
        //   100: aload_2
        //   101: athrow
        //   102: astore_2
        //   103: aload_0
        //   104: getfield 21	com/google/android/gms/cast/RemoteMediaPlayer$21:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   107: invokestatic 47	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   110: aconst_null
        //   111: invokevirtual 52	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   114: aload_2
        //   115: athrow
        //   116: astore_2
        //   117: goto -55 -> 62
        //   120: astore_2
        //   121: goto -59 -> 62
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	124	0	this	21
        //   0	124	1	paramAnonymouszze	zze
        //   61	1	2	localzzb	com.google.android.gms.cast.internal.zzn.zzb
        //   97	4	2	localObject1	Object
        //   102	13	2	localObject2	Object
        //   116	1	2	localIOException	IOException
        //   120	1	2	localIllegalArgumentException	IllegalArgumentException
        // Exception table:
        //   from	to	target	type
        //   24	47	61	com/google/android/gms/cast/internal/zzn$zzb
        //   10	24	97	finally
        //   47	58	97	finally
        //   58	60	97	finally
        //   83	94	97	finally
        //   98	100	97	finally
        //   103	116	97	finally
        //   24	47	102	finally
        //   62	83	102	finally
        //   24	47	116	java/io/IOException
        //   24	47	120	java/lang/IllegalArgumentException
      }
    });
  }
  
  public PendingResult<MediaChannelResult> setTextTrackStyle(final GoogleApiClient paramGoogleApiClient, final TextTrackStyle paramTextTrackStyle)
  {
    if (paramTextTrackStyle == null) {
      throw new IllegalArgumentException("trackStyle cannot be null");
    }
    paramGoogleApiClient.zzb(new zzb(paramGoogleApiClient)
    {
      /* Error */
      protected void zza(zze paramAnonymouszze)
      {
        // Byte code:
        //   0: aload_0
        //   1: getfield 19	com/google/android/gms/cast/RemoteMediaPlayer$3:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   4: invokestatic 37	com/google/android/gms/cast/RemoteMediaPlayer:zze	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Ljava/lang/Object;
        //   7: astore_1
        //   8: aload_1
        //   9: monitorenter
        //   10: aload_0
        //   11: getfield 19	com/google/android/gms/cast/RemoteMediaPlayer$3:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   14: invokestatic 41	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   17: aload_0
        //   18: getfield 21	com/google/android/gms/cast/RemoteMediaPlayer$3:of	Lcom/google/android/gms/common/api/GoogleApiClient;
        //   21: invokevirtual 46	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   24: aload_0
        //   25: getfield 19	com/google/android/gms/cast/RemoteMediaPlayer$3:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   28: invokestatic 50	com/google/android/gms/cast/RemoteMediaPlayer:zzg	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/internal/zzn;
        //   31: aload_0
        //   32: getfield 54	com/google/android/gms/cast/RemoteMediaPlayer$3:oG	Lcom/google/android/gms/cast/internal/zzp;
        //   35: aload_0
        //   36: getfield 23	com/google/android/gms/cast/RemoteMediaPlayer$3:oh	Lcom/google/android/gms/cast/TextTrackStyle;
        //   39: invokevirtual 59	com/google/android/gms/cast/internal/zzn:zza	(Lcom/google/android/gms/cast/internal/zzp;Lcom/google/android/gms/cast/TextTrackStyle;)J
        //   42: pop2
        //   43: aload_0
        //   44: getfield 19	com/google/android/gms/cast/RemoteMediaPlayer$3:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   47: invokestatic 41	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   50: aconst_null
        //   51: invokevirtual 46	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   54: aload_1
        //   55: monitorexit
        //   56: return
        //   57: astore_2
        //   58: aload_0
        //   59: aload_0
        //   60: new 61	com/google/android/gms/common/api/Status
        //   63: dup
        //   64: sipush 2100
        //   67: invokespecial 64	com/google/android/gms/common/api/Status:<init>	(I)V
        //   70: invokevirtual 67	com/google/android/gms/cast/RemoteMediaPlayer$zzb:zzc	(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Result;
        //   73: checkcast 69	com/google/android/gms/cast/RemoteMediaPlayer$MediaChannelResult
        //   76: invokevirtual 72	com/google/android/gms/cast/RemoteMediaPlayer$3:zzc	(Lcom/google/android/gms/common/api/Result;)V
        //   79: aload_0
        //   80: getfield 19	com/google/android/gms/cast/RemoteMediaPlayer$3:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   83: invokestatic 41	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   86: aconst_null
        //   87: invokevirtual 46	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   90: goto -36 -> 54
        //   93: astore_2
        //   94: aload_1
        //   95: monitorexit
        //   96: aload_2
        //   97: athrow
        //   98: astore_2
        //   99: aload_0
        //   100: getfield 19	com/google/android/gms/cast/RemoteMediaPlayer$3:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   103: invokestatic 41	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   106: aconst_null
        //   107: invokevirtual 46	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   110: aload_2
        //   111: athrow
        //   112: astore_2
        //   113: goto -55 -> 58
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	116	0	this	3
        //   0	116	1	paramAnonymouszze	zze
        //   57	1	2	localzzb	com.google.android.gms.cast.internal.zzn.zzb
        //   93	4	2	localObject1	Object
        //   98	13	2	localObject2	Object
        //   112	1	2	localIOException	IOException
        // Exception table:
        //   from	to	target	type
        //   24	43	57	com/google/android/gms/cast/internal/zzn$zzb
        //   10	24	93	finally
        //   43	54	93	finally
        //   54	56	93	finally
        //   79	90	93	finally
        //   94	96	93	finally
        //   99	112	93	finally
        //   24	43	98	finally
        //   58	79	98	finally
        //   24	43	112	java/io/IOException
      }
    });
  }
  
  public PendingResult<MediaChannelResult> stop(GoogleApiClient paramGoogleApiClient)
  {
    return stop(paramGoogleApiClient, null);
  }
  
  public PendingResult<MediaChannelResult> stop(final GoogleApiClient paramGoogleApiClient, final JSONObject paramJSONObject)
  {
    paramGoogleApiClient.zzb(new zzb(paramGoogleApiClient)
    {
      /* Error */
      protected void zza(zze paramAnonymouszze)
      {
        // Byte code:
        //   0: aload_0
        //   1: getfield 19	com/google/android/gms/cast/RemoteMediaPlayer$18:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   4: invokestatic 37	com/google/android/gms/cast/RemoteMediaPlayer:zze	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Ljava/lang/Object;
        //   7: astore_1
        //   8: aload_1
        //   9: monitorenter
        //   10: aload_0
        //   11: getfield 19	com/google/android/gms/cast/RemoteMediaPlayer$18:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   14: invokestatic 41	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   17: aload_0
        //   18: getfield 21	com/google/android/gms/cast/RemoteMediaPlayer$18:of	Lcom/google/android/gms/common/api/GoogleApiClient;
        //   21: invokevirtual 46	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   24: aload_0
        //   25: getfield 19	com/google/android/gms/cast/RemoteMediaPlayer$18:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   28: invokestatic 50	com/google/android/gms/cast/RemoteMediaPlayer:zzg	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/internal/zzn;
        //   31: aload_0
        //   32: getfield 54	com/google/android/gms/cast/RemoteMediaPlayer$18:oG	Lcom/google/android/gms/cast/internal/zzp;
        //   35: aload_0
        //   36: getfield 23	com/google/android/gms/cast/RemoteMediaPlayer$18:om	Lorg/json/JSONObject;
        //   39: invokevirtual 60	com/google/android/gms/cast/internal/zzn:zzb	(Lcom/google/android/gms/cast/internal/zzp;Lorg/json/JSONObject;)J
        //   42: pop2
        //   43: aload_0
        //   44: getfield 19	com/google/android/gms/cast/RemoteMediaPlayer$18:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   47: invokestatic 41	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   50: aconst_null
        //   51: invokevirtual 46	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   54: aload_1
        //   55: monitorexit
        //   56: return
        //   57: astore_2
        //   58: aload_0
        //   59: aload_0
        //   60: new 62	com/google/android/gms/common/api/Status
        //   63: dup
        //   64: sipush 2100
        //   67: invokespecial 65	com/google/android/gms/common/api/Status:<init>	(I)V
        //   70: invokevirtual 68	com/google/android/gms/cast/RemoteMediaPlayer$zzb:zzc	(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Result;
        //   73: checkcast 70	com/google/android/gms/cast/RemoteMediaPlayer$MediaChannelResult
        //   76: invokevirtual 73	com/google/android/gms/cast/RemoteMediaPlayer$18:zzc	(Lcom/google/android/gms/common/api/Result;)V
        //   79: aload_0
        //   80: getfield 19	com/google/android/gms/cast/RemoteMediaPlayer$18:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   83: invokestatic 41	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   86: aconst_null
        //   87: invokevirtual 46	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   90: goto -36 -> 54
        //   93: astore_2
        //   94: aload_1
        //   95: monitorexit
        //   96: aload_2
        //   97: athrow
        //   98: astore_2
        //   99: aload_0
        //   100: getfield 19	com/google/android/gms/cast/RemoteMediaPlayer$18:oe	Lcom/google/android/gms/cast/RemoteMediaPlayer;
        //   103: invokestatic 41	com/google/android/gms/cast/RemoteMediaPlayer:zzf	(Lcom/google/android/gms/cast/RemoteMediaPlayer;)Lcom/google/android/gms/cast/RemoteMediaPlayer$zza;
        //   106: aconst_null
        //   107: invokevirtual 46	com/google/android/gms/cast/RemoteMediaPlayer$zza:zzc	(Lcom/google/android/gms/common/api/GoogleApiClient;)V
        //   110: aload_2
        //   111: athrow
        //   112: astore_2
        //   113: goto -55 -> 58
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	116	0	this	18
        //   0	116	1	paramAnonymouszze	zze
        //   57	1	2	localzzb	com.google.android.gms.cast.internal.zzn.zzb
        //   93	4	2	localObject1	Object
        //   98	13	2	localObject2	Object
        //   112	1	2	localIOException	IOException
        // Exception table:
        //   from	to	target	type
        //   24	43	57	com/google/android/gms/cast/internal/zzn$zzb
        //   10	24	93	finally
        //   43	54	93	finally
        //   54	56	93	finally
        //   79	90	93	finally
        //   94	96	93	finally
        //   99	112	93	finally
        //   24	43	98	finally
        //   58	79	98	finally
        //   24	43	112	java/io/IOException
      }
    });
  }
  
  public static abstract interface MediaChannelResult
    extends Result
  {
    public abstract JSONObject getCustomData();
  }
  
  public static abstract interface OnMetadataUpdatedListener
  {
    public abstract void onMetadataUpdated();
  }
  
  public static abstract interface OnPreloadStatusUpdatedListener
  {
    public abstract void onPreloadStatusUpdated();
  }
  
  public static abstract interface OnQueueStatusUpdatedListener
  {
    public abstract void onQueueStatusUpdated();
  }
  
  public static abstract interface OnStatusUpdatedListener
  {
    public abstract void onStatusUpdated();
  }
  
  private class zza
    implements zzo
  {
    private GoogleApiClient oC;
    private long oD = 0L;
    
    public zza() {}
    
    public void zza(String paramString1, String paramString2, long paramLong, String paramString3)
      throws IOException
    {
      if (this.oC == null) {
        throw new IOException("No GoogleApiClient available");
      }
      Cast.CastApi.sendMessage(this.oC, paramString1, paramString2).setResultCallback(new zza(paramLong));
    }
    
    public long zzall()
    {
      long l = this.oD + 1L;
      this.oD = l;
      return l;
    }
    
    public void zzc(GoogleApiClient paramGoogleApiClient)
    {
      this.oC = paramGoogleApiClient;
    }
    
    private final class zza
      implements ResultCallback<Status>
    {
      private final long oE;
      
      zza(long paramLong)
      {
        this.oE = paramLong;
      }
      
      public void zzp(@NonNull Status paramStatus)
      {
        if (!paramStatus.isSuccess()) {
          RemoteMediaPlayer.zzg(RemoteMediaPlayer.this).zzb(this.oE, paramStatus.getStatusCode());
        }
      }
    }
  }
  
  static abstract class zzb
    extends zzb<RemoteMediaPlayer.MediaChannelResult>
  {
    zzp oG = new zzp()
    {
      public void zza(long paramAnonymousLong, int paramAnonymousInt, Object paramAnonymousObject)
      {
        if ((paramAnonymousObject instanceof JSONObject)) {}
        for (paramAnonymousObject = (JSONObject)paramAnonymousObject;; paramAnonymousObject = null)
        {
          RemoteMediaPlayer.zzb.this.zzc(new RemoteMediaPlayer.zzc(new Status(paramAnonymousInt), (JSONObject)paramAnonymousObject));
          return;
        }
      }
      
      public void zzac(long paramAnonymousLong)
      {
        RemoteMediaPlayer.zzb.this.zzc((RemoteMediaPlayer.MediaChannelResult)RemoteMediaPlayer.zzb.this.zzc(new Status(2103)));
      }
    };
    
    zzb(GoogleApiClient paramGoogleApiClient)
    {
      super();
    }
    
    protected void zza(zze paramzze) {}
    
    public RemoteMediaPlayer.MediaChannelResult zzq(final Status paramStatus)
    {
      new RemoteMediaPlayer.MediaChannelResult()
      {
        public JSONObject getCustomData()
        {
          return null;
        }
        
        public Status getStatus()
        {
          return paramStatus;
        }
      };
    }
  }
  
  private static final class zzc
    implements RemoteMediaPlayer.MediaChannelResult
  {
    private final Status hv;
    private final JSONObject np;
    
    zzc(Status paramStatus, JSONObject paramJSONObject)
    {
      this.hv = paramStatus;
      this.np = paramJSONObject;
    }
    
    public JSONObject getCustomData()
    {
      return this.np;
    }
    
    public Status getStatus()
    {
      return this.hv;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/RemoteMediaPlayer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */