package com.google.android.gms.cast;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.view.accessibility.CaptioningManager;
import android.view.accessibility.CaptioningManager.CaptionStyle;
import com.google.android.gms.cast.internal.zzf;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.util.zzp;
import com.google.android.gms.common.util.zzs;
import org.json.JSONException;
import org.json.JSONObject;

public final class TextTrackStyle
  extends AbstractSafeParcelable
{
  public static final int COLOR_UNSPECIFIED = 0;
  public static final Parcelable.Creator<TextTrackStyle> CREATOR = new zzk();
  public static final float DEFAULT_FONT_SCALE = 1.0F;
  public static final int EDGE_TYPE_DEPRESSED = 4;
  public static final int EDGE_TYPE_DROP_SHADOW = 2;
  public static final int EDGE_TYPE_NONE = 0;
  public static final int EDGE_TYPE_OUTLINE = 1;
  public static final int EDGE_TYPE_RAISED = 3;
  public static final int EDGE_TYPE_UNSPECIFIED = -1;
  public static final int FONT_FAMILY_CASUAL = 4;
  public static final int FONT_FAMILY_CURSIVE = 5;
  public static final int FONT_FAMILY_MONOSPACED_SANS_SERIF = 1;
  public static final int FONT_FAMILY_MONOSPACED_SERIF = 3;
  public static final int FONT_FAMILY_SANS_SERIF = 0;
  public static final int FONT_FAMILY_SERIF = 2;
  public static final int FONT_FAMILY_SMALL_CAPITALS = 6;
  public static final int FONT_FAMILY_UNSPECIFIED = -1;
  public static final int FONT_STYLE_BOLD = 1;
  public static final int FONT_STYLE_BOLD_ITALIC = 3;
  public static final int FONT_STYLE_ITALIC = 2;
  public static final int FONT_STYLE_NORMAL = 0;
  public static final int FONT_STYLE_UNSPECIFIED = -1;
  public static final int WINDOW_TYPE_NONE = 0;
  public static final int WINDOW_TYPE_NORMAL = 1;
  public static final int WINDOW_TYPE_ROUNDED = 2;
  public static final int WINDOW_TYPE_UNSPECIFIED = -1;
  private int mBackgroundColor;
  private final int mVersionCode;
  String nn;
  private JSONObject np;
  private float oI;
  private int oJ;
  private int oK;
  private int oL;
  private int oM;
  private int oN;
  private int oO;
  private String oP;
  private int oQ;
  private int oR;
  
  public TextTrackStyle()
  {
    this(1, 1.0F, 0, 0, -1, 0, -1, 0, 0, null, -1, -1, null);
  }
  
  TextTrackStyle(int paramInt1, float paramFloat, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, String paramString1, int paramInt9, int paramInt10, String paramString2)
  {
    this.mVersionCode = paramInt1;
    this.oI = paramFloat;
    this.oJ = paramInt2;
    this.mBackgroundColor = paramInt3;
    this.oK = paramInt4;
    this.oL = paramInt5;
    this.oM = paramInt6;
    this.oN = paramInt7;
    this.oO = paramInt8;
    this.oP = paramString1;
    this.oQ = paramInt9;
    this.oR = paramInt10;
    this.nn = paramString2;
    if (this.nn != null) {
      try
      {
        this.np = new JSONObject(this.nn);
        return;
      }
      catch (JSONException paramString1)
      {
        this.np = null;
        this.nn = null;
        return;
      }
    }
    this.np = null;
  }
  
  @TargetApi(19)
  public static TextTrackStyle fromSystemSettings(Context paramContext)
  {
    TextTrackStyle localTextTrackStyle = new TextTrackStyle();
    if (!zzs.zzayu()) {
      return localTextTrackStyle;
    }
    paramContext = (CaptioningManager)paramContext.getSystemService("captioning");
    localTextTrackStyle.setFontScale(paramContext.getFontScale());
    paramContext = paramContext.getUserStyle();
    localTextTrackStyle.setBackgroundColor(paramContext.backgroundColor);
    localTextTrackStyle.setForegroundColor(paramContext.foregroundColor);
    label117:
    boolean bool1;
    boolean bool2;
    switch (paramContext.edgeType)
    {
    default: 
      localTextTrackStyle.setEdgeType(0);
      localTextTrackStyle.setEdgeColor(paramContext.edgeColor);
      paramContext = paramContext.getTypeface();
      if (paramContext != null)
      {
        if (!Typeface.MONOSPACE.equals(paramContext)) {
          break label158;
        }
        localTextTrackStyle.setFontGenericFamily(1);
        bool1 = paramContext.isBold();
        bool2 = paramContext.isItalic();
        if ((!bool1) || (!bool2)) {
          break label202;
        }
        localTextTrackStyle.setFontStyle(3);
      }
      break;
    }
    for (;;)
    {
      return localTextTrackStyle;
      localTextTrackStyle.setEdgeType(1);
      break;
      localTextTrackStyle.setEdgeType(2);
      break;
      label158:
      if (Typeface.SANS_SERIF.equals(paramContext))
      {
        localTextTrackStyle.setFontGenericFamily(0);
        break label117;
      }
      if (Typeface.SERIF.equals(paramContext))
      {
        localTextTrackStyle.setFontGenericFamily(2);
        break label117;
      }
      localTextTrackStyle.setFontGenericFamily(0);
      break label117;
      label202:
      if (bool1) {
        localTextTrackStyle.setFontStyle(1);
      } else if (bool2) {
        localTextTrackStyle.setFontStyle(2);
      } else {
        localTextTrackStyle.setFontStyle(0);
      }
    }
  }
  
  private String zzaz(int paramInt)
  {
    return String.format("#%02X%02X%02X%02X", new Object[] { Integer.valueOf(Color.red(paramInt)), Integer.valueOf(Color.green(paramInt)), Integer.valueOf(Color.blue(paramInt)), Integer.valueOf(Color.alpha(paramInt)) });
  }
  
  private int zzgn(String paramString)
  {
    int j = 0;
    int i = j;
    if (paramString != null)
    {
      i = j;
      if (paramString.length() == 9)
      {
        i = j;
        if (paramString.charAt(0) != '#') {}
      }
    }
    try
    {
      i = Integer.parseInt(paramString.substring(1, 3), 16);
      j = Integer.parseInt(paramString.substring(3, 5), 16);
      int k = Integer.parseInt(paramString.substring(5, 7), 16);
      i = Color.argb(Integer.parseInt(paramString.substring(7, 9), 16), i, j, k);
      return i;
    }
    catch (NumberFormatException paramString) {}
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = true;
    boolean bool3 = false;
    if (this == paramObject) {
      bool1 = true;
    }
    int i;
    int j;
    label51:
    do
    {
      do
      {
        do
        {
          return bool1;
          bool1 = bool3;
        } while (!(paramObject instanceof TextTrackStyle));
        paramObject = (TextTrackStyle)paramObject;
        if (this.np != null) {
          break;
        }
        i = 1;
        if (((TextTrackStyle)paramObject).np != null) {
          break label218;
        }
        j = 1;
        bool1 = bool3;
      } while (i != j);
      if ((this.np == null) || (((TextTrackStyle)paramObject).np == null)) {
        break;
      }
      bool1 = bool3;
    } while (!zzp.zzf(this.np, ((TextTrackStyle)paramObject).np));
    if ((this.oI == ((TextTrackStyle)paramObject).oI) && (this.oJ == ((TextTrackStyle)paramObject).oJ) && (this.mBackgroundColor == ((TextTrackStyle)paramObject).mBackgroundColor) && (this.oK == ((TextTrackStyle)paramObject).oK) && (this.oL == ((TextTrackStyle)paramObject).oL) && (this.oM == ((TextTrackStyle)paramObject).oM) && (this.oO == ((TextTrackStyle)paramObject).oO) && (zzf.zza(this.oP, ((TextTrackStyle)paramObject).oP)) && (this.oQ == ((TextTrackStyle)paramObject).oQ) && (this.oR == ((TextTrackStyle)paramObject).oR)) {}
    for (boolean bool1 = bool2;; bool1 = false)
    {
      return bool1;
      i = 0;
      break;
      label218:
      j = 0;
      break label51;
    }
  }
  
  public int getBackgroundColor()
  {
    return this.mBackgroundColor;
  }
  
  public JSONObject getCustomData()
  {
    return this.np;
  }
  
  public int getEdgeColor()
  {
    return this.oL;
  }
  
  public int getEdgeType()
  {
    return this.oK;
  }
  
  public String getFontFamily()
  {
    return this.oP;
  }
  
  public int getFontGenericFamily()
  {
    return this.oQ;
  }
  
  public float getFontScale()
  {
    return this.oI;
  }
  
  public int getFontStyle()
  {
    return this.oR;
  }
  
  public int getForegroundColor()
  {
    return this.oJ;
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int getWindowColor()
  {
    return this.oN;
  }
  
  public int getWindowCornerRadius()
  {
    return this.oO;
  }
  
  public int getWindowType()
  {
    return this.oM;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { Float.valueOf(this.oI), Integer.valueOf(this.oJ), Integer.valueOf(this.mBackgroundColor), Integer.valueOf(this.oK), Integer.valueOf(this.oL), Integer.valueOf(this.oM), Integer.valueOf(this.oN), Integer.valueOf(this.oO), this.oP, Integer.valueOf(this.oQ), Integer.valueOf(this.oR), this.np });
  }
  
  public void setBackgroundColor(int paramInt)
  {
    this.mBackgroundColor = paramInt;
  }
  
  public void setCustomData(JSONObject paramJSONObject)
  {
    this.np = paramJSONObject;
  }
  
  public void setEdgeColor(int paramInt)
  {
    this.oL = paramInt;
  }
  
  public void setEdgeType(int paramInt)
  {
    if ((paramInt < 0) || (paramInt > 4)) {
      throw new IllegalArgumentException("invalid edgeType");
    }
    this.oK = paramInt;
  }
  
  public void setFontFamily(String paramString)
  {
    this.oP = paramString;
  }
  
  public void setFontGenericFamily(int paramInt)
  {
    if ((paramInt < 0) || (paramInt > 6)) {
      throw new IllegalArgumentException("invalid fontGenericFamily");
    }
    this.oQ = paramInt;
  }
  
  public void setFontScale(float paramFloat)
  {
    this.oI = paramFloat;
  }
  
  public void setFontStyle(int paramInt)
  {
    if ((paramInt < 0) || (paramInt > 3)) {
      throw new IllegalArgumentException("invalid fontStyle");
    }
    this.oR = paramInt;
  }
  
  public void setForegroundColor(int paramInt)
  {
    this.oJ = paramInt;
  }
  
  public void setWindowColor(int paramInt)
  {
    this.oN = paramInt;
  }
  
  public void setWindowCornerRadius(int paramInt)
  {
    if (paramInt < 0) {
      throw new IllegalArgumentException("invalid windowCornerRadius");
    }
    this.oO = paramInt;
  }
  
  public void setWindowType(int paramInt)
  {
    if ((paramInt < 0) || (paramInt > 2)) {
      throw new IllegalArgumentException("invalid windowType");
    }
    this.oM = paramInt;
  }
  
  public JSONObject toJson()
  {
    JSONObject localJSONObject = new JSONObject();
    try
    {
      localJSONObject.put("fontScale", this.oI);
      if (this.oJ != 0) {
        localJSONObject.put("foregroundColor", zzaz(this.oJ));
      }
      if (this.mBackgroundColor != 0) {
        localJSONObject.put("backgroundColor", zzaz(this.mBackgroundColor));
      }
      switch (this.oK)
      {
      case 0: 
        if (this.oL != 0) {
          localJSONObject.put("edgeColor", zzaz(this.oL));
        }
        switch (this.oM)
        {
        case 0: 
          label156:
          if (this.oN != 0) {
            localJSONObject.put("windowColor", zzaz(this.oN));
          }
          if (this.oM == 2) {
            localJSONObject.put("windowRoundedCornerRadius", this.oO);
          }
          if (this.oP != null) {
            localJSONObject.put("fontFamily", this.oP);
          }
          switch (this.oQ)
          {
          case 0: 
            label264:
            switch (this.oR)
            {
            }
            break;
          }
          break;
        }
        break;
      }
      for (;;)
      {
        if (this.np == null) {
          break label599;
        }
        localJSONObject.put("customData", this.np);
        return localJSONObject;
        localJSONObject.put("edgeType", "NONE");
        break;
        localJSONObject.put("edgeType", "OUTLINE");
        break;
        localJSONObject.put("edgeType", "DROP_SHADOW");
        break;
        localJSONObject.put("edgeType", "RAISED");
        break;
        localJSONObject.put("edgeType", "DEPRESSED");
        break;
        localJSONObject.put("windowType", "NONE");
        break label156;
        localJSONObject.put("windowType", "NORMAL");
        break label156;
        localJSONObject.put("windowType", "ROUNDED_CORNERS");
        break label156;
        localJSONObject.put("fontGenericFamily", "SANS_SERIF");
        break label264;
        localJSONObject.put("fontGenericFamily", "MONOSPACED_SANS_SERIF");
        break label264;
        localJSONObject.put("fontGenericFamily", "SERIF");
        break label264;
        localJSONObject.put("fontGenericFamily", "MONOSPACED_SERIF");
        break label264;
        localJSONObject.put("fontGenericFamily", "CASUAL");
        break label264;
        localJSONObject.put("fontGenericFamily", "CURSIVE");
        break label264;
        localJSONObject.put("fontGenericFamily", "SMALL_CAPITALS");
        break label264;
        localJSONObject.put("fontStyle", "NORMAL");
        continue;
        localJSONObject.put("fontStyle", "BOLD");
        continue;
        localJSONObject.put("fontStyle", "ITALIC");
        continue;
        localJSONObject.put("fontStyle", "BOLD_ITALIC");
        continue;
        break;
        break label156;
        break label264;
      }
      label599:
      return localJSONObject;
    }
    catch (JSONException localJSONException) {}
    return localJSONObject;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    if (this.np == null) {}
    for (String str = null;; str = this.np.toString())
    {
      this.nn = str;
      zzk.zza(this, paramParcel, paramInt);
      return;
    }
  }
  
  public void zzl(JSONObject paramJSONObject)
    throws JSONException
  {
    this.oI = ((float)paramJSONObject.optDouble("fontScale", 1.0D));
    this.oJ = zzgn(paramJSONObject.optString("foregroundColor"));
    this.mBackgroundColor = zzgn(paramJSONObject.optString("backgroundColor"));
    String str;
    if (paramJSONObject.has("edgeType"))
    {
      str = paramJSONObject.getString("edgeType");
      if ("NONE".equals(str)) {
        this.oK = 0;
      }
    }
    else
    {
      this.oL = zzgn(paramJSONObject.optString("edgeColor"));
      if (paramJSONObject.has("windowType"))
      {
        str = paramJSONObject.getString("windowType");
        if (!"NONE".equals(str)) {
          break label321;
        }
        this.oM = 0;
      }
      label124:
      this.oN = zzgn(paramJSONObject.optString("windowColor"));
      if (this.oM == 2) {
        this.oO = paramJSONObject.optInt("windowRoundedCornerRadius", 0);
      }
      this.oP = paramJSONObject.optString("fontFamily", null);
      if (paramJSONObject.has("fontGenericFamily"))
      {
        str = paramJSONObject.getString("fontGenericFamily");
        if (!"SANS_SERIF".equals(str)) {
          break label357;
        }
        this.oQ = 0;
      }
      label204:
      if (paramJSONObject.has("fontStyle"))
      {
        str = paramJSONObject.getString("fontStyle");
        if (!"NORMAL".equals(str)) {
          break label466;
        }
        this.oR = 0;
      }
    }
    for (;;)
    {
      this.np = paramJSONObject.optJSONObject("customData");
      return;
      if ("OUTLINE".equals(str))
      {
        this.oK = 1;
        break;
      }
      if ("DROP_SHADOW".equals(str))
      {
        this.oK = 2;
        break;
      }
      if ("RAISED".equals(str))
      {
        this.oK = 3;
        break;
      }
      if (!"DEPRESSED".equals(str)) {
        break;
      }
      this.oK = 4;
      break;
      label321:
      if ("NORMAL".equals(str))
      {
        this.oM = 1;
        break label124;
      }
      if (!"ROUNDED_CORNERS".equals(str)) {
        break label124;
      }
      this.oM = 2;
      break label124;
      label357:
      if ("MONOSPACED_SANS_SERIF".equals(str))
      {
        this.oQ = 1;
        break label204;
      }
      if ("SERIF".equals(str))
      {
        this.oQ = 2;
        break label204;
      }
      if ("MONOSPACED_SERIF".equals(str))
      {
        this.oQ = 3;
        break label204;
      }
      if ("CASUAL".equals(str))
      {
        this.oQ = 4;
        break label204;
      }
      if ("CURSIVE".equals(str))
      {
        this.oQ = 5;
        break label204;
      }
      if (!"SMALL_CAPITALS".equals(str)) {
        break label204;
      }
      this.oQ = 6;
      break label204;
      label466:
      if ("BOLD".equals(str)) {
        this.oR = 1;
      } else if ("ITALIC".equals(str)) {
        this.oR = 2;
      } else if ("BOLD_ITALIC".equals(str)) {
        this.oR = 3;
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/TextTrackStyle.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */