package com.google.android.gms.cast.framework;

public abstract interface AppVisibilityListener
{
  public abstract void onAppEnteredBackground();
  
  public abstract void onAppEnteredForeground();
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/framework/AppVisibilityListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */