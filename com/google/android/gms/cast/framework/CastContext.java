package com.google.android.gms.cast.framework;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.media.MediaRouteSelector;
import android.support.v7.media.MediaRouter;
import android.view.KeyEvent;
import com.google.android.gms.cast.internal.zzm;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.util.zzs;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.internal.zzof;
import com.google.android.gms.internal.zzog;
import com.google.android.gms.internal.zzoh;
import com.google.android.gms.internal.zzop;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public final class CastContext
{
  public static final String OPTIONS_PROVIDER_CLASS_NAME_KEY = "com.google.android.gms.cast.framework.OPTIONS_PROVIDER_CLASS_NAME";
  private static final zzm oT = new zzm("CastContext");
  private static zza oU;
  private static CastContext oV;
  private final zzg oW;
  private final SessionManager oX;
  private final zzd oY;
  private final CastOptions oZ;
  private zzop pa;
  private final Context zzcfo;
  
  private CastContext(Context paramContext, CastOptions paramCastOptions, List<SessionProvider> paramList)
  {
    this.zzcfo = paramContext.getApplicationContext();
    this.oZ = paramCastOptions;
    this.pa = new zzop(MediaRouter.getInstance(this.zzcfo));
    paramContext = new HashMap();
    Object localObject2 = new zzoh(this.zzcfo, paramCastOptions, this.pa);
    paramContext.put(((zzoh)localObject2).getCategory(), ((zzoh)localObject2).zzamk());
    if (paramList != null)
    {
      paramList = paramList.iterator();
      if (paramList.hasNext())
      {
        localObject2 = (SessionProvider)paramList.next();
        zzaa.zzb(localObject2, "Additional SessionProvider must not be null.");
        String str = zzaa.zzh(((SessionProvider)localObject2).getCategory(), "Category for SessionProvider must not be null or empty string.");
        if (!paramContext.containsKey(str)) {}
        for (boolean bool = true;; bool = false)
        {
          zzaa.zzb(bool, String.format("SessionProvider for category %s already added", new Object[] { str }));
          paramContext.put(str, ((SessionProvider)localObject2).zzamk());
          break;
        }
      }
    }
    this.oW = zzog.zza(this.zzcfo, paramCastOptions, this.pa, paramContext);
    try
    {
      paramContext = this.oW.zzalu();
      if (paramContext == null)
      {
        paramContext = null;
        this.oY = paramContext;
      }
    }
    catch (RemoteException paramContext)
    {
      try
      {
        for (;;)
        {
          paramContext = this.oW.zzalt();
          if (paramContext != null) {
            break;
          }
          paramContext = (Context)localObject1;
          this.oX = paramContext;
          return;
          paramContext = paramContext;
          oT.zzb(paramContext, "Unable to call %s on %s.", new Object[] { "getDiscoveryManagerImpl", zzg.class.getSimpleName() });
          paramContext = null;
        }
        paramContext = new zzd(paramContext);
      }
      catch (RemoteException paramContext)
      {
        for (;;)
        {
          oT.zzb(paramContext, "Unable to call %s on %s.", new Object[] { "getSessionManagerImpl", zzg.class.getSimpleName() });
          paramContext = null;
          continue;
          paramContext = new SessionManager(paramContext);
        }
      }
    }
  }
  
  public static CastContext getSharedInstance(@NonNull Context paramContext)
    throws IllegalStateException
  {
    zzaa.zzhs("getSharedInstance must be called from the main thread.");
    if (oV == null)
    {
      OptionsProvider localOptionsProvider = zzbb(paramContext.getApplicationContext());
      oV = new CastContext(paramContext, localOptionsProvider.getCastOptions(paramContext.getApplicationContext()), localOptionsProvider.getAdditionalSessionProviders(paramContext.getApplicationContext()));
      if (zzs.zzayq())
      {
        oU = new zza(paramContext.getApplicationContext());
        ((Application)paramContext.getApplicationContext()).registerActivityLifecycleCallbacks(oU);
      }
    }
    return oV;
  }
  
  private boolean zza(CastSession paramCastSession, double paramDouble, boolean paramBoolean)
  {
    double d = 1.0D;
    if (paramBoolean) {}
    try
    {
      paramDouble = paramCastSession.getVolume() + paramDouble;
      if (paramDouble <= 1.0D) {
        break label54;
      }
      paramDouble = d;
    }
    catch (IllegalStateException paramCastSession)
    {
      oT.zzc("Unable to call CastSession.setVolume(double).", new Object[] { paramCastSession });
      return true;
    }
    catch (IOException paramCastSession)
    {
      for (;;) {}
    }
    paramCastSession.setVolume(paramDouble);
    return true;
  }
  
  private static OptionsProvider zzbb(Context paramContext)
    throws IllegalStateException
  {
    try
    {
      paramContext = paramContext.getPackageManager().getApplicationInfo(paramContext.getPackageName(), 128).metaData.getString("com.google.android.gms.cast.framework.OPTIONS_PROVIDER_CLASS_NAME");
      if (paramContext == null) {
        throw new IllegalStateException("The fully qualified name of the implementation of OptionsProvider must be provided as a metadata in the AndroidManifest.xml with key com.google.android.gms.cast.framework.OPTIONS_PROVIDER_CLASS_NAME.");
      }
    }
    catch (PackageManager.NameNotFoundException paramContext)
    {
      throw new IllegalStateException("Failed to initialize CastContext.", paramContext);
      paramContext = (OptionsProvider)Class.forName(paramContext).newInstance();
      return paramContext;
    }
    catch (ClassNotFoundException paramContext)
    {
      for (;;) {}
    }
    catch (InstantiationException paramContext)
    {
      for (;;) {}
    }
    catch (NullPointerException paramContext)
    {
      for (;;) {}
    }
    catch (IllegalAccessException paramContext)
    {
      for (;;) {}
    }
  }
  
  public void addAppVisibilityListener(AppVisibilityListener paramAppVisibilityListener)
    throws IllegalStateException, NullPointerException
  {
    zzaa.zzhs("addAppVisibilityListener must be called from the main thread.");
    zzaa.zzy(paramAppVisibilityListener);
    try
    {
      this.oW.zza(new zza(paramAppVisibilityListener));
      return;
    }
    catch (RemoteException paramAppVisibilityListener)
    {
      oT.zzb(paramAppVisibilityListener, "Unable to call %s on %s.", new Object[] { "addVisibilityChangeListener", zzg.class.getSimpleName() });
    }
  }
  
  public void addCastStateListener(CastStateListener paramCastStateListener)
    throws IllegalStateException, NullPointerException
  {
    zzaa.zzhs("addCastStateListener must be called from the main thread.");
    zzaa.zzy(paramCastStateListener);
    this.oX.addCastStateListener(paramCastStateListener);
  }
  
  public CastOptions getCastOptions()
    throws IllegalStateException
  {
    zzaa.zzhs("getCastOptions must be called from the main thread.");
    return this.oZ;
  }
  
  public SessionManager getSessionManager()
    throws IllegalStateException
  {
    zzaa.zzhs("getSessionManager must be called from the main thread.");
    return this.oX;
  }
  
  public boolean isAppVisible()
    throws IllegalStateException
  {
    zzaa.zzhs("isAppVisible must be called from the main thread.");
    try
    {
      boolean bool = this.oW.isAppVisible();
      return bool;
    }
    catch (RemoteException localRemoteException)
    {
      oT.zzb(localRemoteException, "Unable to call %s on %s.", new Object[] { "isApplicationVisible", zzg.class.getSimpleName() });
    }
    return false;
  }
  
  public void onActivityPaused(Activity paramActivity)
  {
    zzaa.zzhs("onActivityPaused must be called from the main thread.");
    try
    {
      this.oW.zzy(zze.zzac(paramActivity));
      return;
    }
    catch (RemoteException paramActivity)
    {
      oT.zzb(paramActivity, "Unable to call %s on %s.", new Object[] { "onActivityPaused", zzg.class.getSimpleName() });
    }
  }
  
  public void onActivityResumed(Activity paramActivity)
  {
    zzaa.zzhs("onActivityResumed must be called from the main thread.");
    try
    {
      this.oW.zzx(zze.zzac(paramActivity));
      return;
    }
    catch (RemoteException paramActivity)
    {
      oT.zzb(paramActivity, "Unable to call %s on %s.", new Object[] { "onActivityResumed", zzg.class.getSimpleName() });
    }
  }
  
  public boolean onDispatchVolumeKeyEventBeforeJellyBean(KeyEvent paramKeyEvent)
  {
    zzaa.zzhs("onDispatchVolumeKeyEventBeforeJellyBean must be called from the main thread.");
    if (zzs.zzayr()) {}
    label12:
    CastSession localCastSession;
    double d;
    boolean bool;
    do
    {
      do
      {
        return false;
        localCastSession = this.oX.getCurrentCastSession();
      } while ((localCastSession == null) || (!localCastSession.isConnected()));
      d = getCastOptions().getVolumeDeltaBeforeIceCreamSandwich();
      if (paramKeyEvent.getAction() == 0) {}
      for (bool = true;; bool = false) {
        switch (paramKeyEvent.getKeyCode())
        {
        default: 
          return false;
        case 24: 
          if (!zza(localCastSession, d, bool)) {
            break label12;
          }
          return true;
        }
      }
    } while (!zza(localCastSession, -d, bool));
    return true;
  }
  
  public void registerLifecycleCallbacksBeforeIceCreamSandwich(@NonNull FragmentActivity paramFragmentActivity, Bundle paramBundle)
  {
    if (!zzs.zzayq()) {
      zzof.zza(paramFragmentActivity, paramBundle);
    }
  }
  
  public void removeAppVisibilityListener(AppVisibilityListener paramAppVisibilityListener)
    throws IllegalStateException
  {
    zzaa.zzhs("removeAppVisibilityListener must be called from the main thread.");
    if (paramAppVisibilityListener == null) {
      return;
    }
    try
    {
      this.oW.zzb(new zza(paramAppVisibilityListener));
      return;
    }
    catch (RemoteException paramAppVisibilityListener)
    {
      oT.zzb(paramAppVisibilityListener, "Unable to call %s on %s.", new Object[] { "addVisibilityChangeListener", zzg.class.getSimpleName() });
    }
  }
  
  public void removeCastStateListener(CastStateListener paramCastStateListener)
    throws IllegalStateException
  {
    zzaa.zzhs("addCastStateListener must be called from the main thread.");
    if (paramCastStateListener == null) {
      return;
    }
    this.oX.removeCastStateListener(paramCastStateListener);
  }
  
  public MediaRouteSelector zzalo()
    throws IllegalStateException
  {
    zzaa.zzhs("getMergedSelector must be called from the main thread.");
    try
    {
      MediaRouteSelector localMediaRouteSelector = MediaRouteSelector.fromBundle(this.oW.zzals());
      return localMediaRouteSelector;
    }
    catch (RemoteException localRemoteException)
    {
      oT.zzb(localRemoteException, "Unable to call %s on %s.", new Object[] { "getMergedSelectorAsBundle", zzg.class.getSimpleName() });
    }
    return null;
  }
  
  public zzd zzalp()
  {
    zzaa.zzhs("getDiscoveryManager must be called from the main thread.");
    return this.oY;
  }
  
  public com.google.android.gms.dynamic.zzd zzalq()
  {
    try
    {
      com.google.android.gms.dynamic.zzd localzzd = this.oW.zzalv();
      return localzzd;
    }
    catch (RemoteException localRemoteException)
    {
      oT.zzb(localRemoteException, "Unable to call %s on %s.", new Object[] { "getWrappedThis", zzg.class.getSimpleName() });
    }
    return null;
  }
  
  @TargetApi(14)
  private static class zza
    implements Application.ActivityLifecycleCallbacks
  {
    private Context zzcfo;
    
    public zza(Context paramContext)
    {
      this.zzcfo = paramContext.getApplicationContext();
    }
    
    public void onActivityCreated(Activity paramActivity, Bundle paramBundle) {}
    
    public void onActivityDestroyed(Activity paramActivity) {}
    
    public void onActivityPaused(Activity paramActivity)
    {
      CastContext.getSharedInstance(this.zzcfo).onActivityPaused(paramActivity);
    }
    
    public void onActivityResumed(Activity paramActivity)
    {
      CastContext.getSharedInstance(this.zzcfo).onActivityResumed(paramActivity);
    }
    
    public void onActivitySaveInstanceState(Activity paramActivity, Bundle paramBundle) {}
    
    public void onActivityStarted(Activity paramActivity) {}
    
    public void onActivityStopped(Activity paramActivity) {}
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/framework/CastContext.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */