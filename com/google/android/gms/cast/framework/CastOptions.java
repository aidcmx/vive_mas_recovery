package com.google.android.gms.cast.framework;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.google.android.gms.cast.LaunchOptions;
import com.google.android.gms.cast.framework.media.CastMediaOptions;
import com.google.android.gms.cast.framework.media.CastMediaOptions.Builder;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CastOptions
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<CastOptions> CREATOR = new zzb();
  private final int mVersionCode;
  private final LaunchOptions nf;
  private final String pb;
  private final List<String> pc;
  private final boolean pd;
  private final boolean pe;
  private final CastMediaOptions pf;
  private final boolean pg;
  private final double ph;
  
  CastOptions(int paramInt, String paramString, List<String> paramList, boolean paramBoolean1, LaunchOptions paramLaunchOptions, boolean paramBoolean2, CastMediaOptions paramCastMediaOptions, boolean paramBoolean3, double paramDouble)
  {
    this.mVersionCode = paramInt;
    String str = paramString;
    if (TextUtils.isEmpty(paramString)) {
      str = "";
    }
    this.pb = str;
    if (paramList == null) {}
    for (paramInt = 0;; paramInt = paramList.size())
    {
      this.pc = new ArrayList(paramInt);
      if (paramInt > 0) {
        this.pc.addAll(paramList);
      }
      this.pd = paramBoolean1;
      paramString = paramLaunchOptions;
      if (paramLaunchOptions == null) {
        paramString = new LaunchOptions();
      }
      this.nf = paramString;
      this.pe = paramBoolean2;
      this.pf = paramCastMediaOptions;
      this.pg = paramBoolean3;
      this.ph = paramDouble;
      return;
    }
  }
  
  public CastMediaOptions getCastMediaOptions()
  {
    return this.pf;
  }
  
  public boolean getEnableReconnectionService()
  {
    return this.pg;
  }
  
  public LaunchOptions getLaunchOptions()
  {
    return this.nf;
  }
  
  public String getReceiverApplicationId()
  {
    return this.pb;
  }
  
  public boolean getResumeSavedSession()
  {
    return this.pe;
  }
  
  public boolean getStopReceiverApplicationWhenEndingSession()
  {
    return this.pd;
  }
  
  public List<String> getSupportedNamespaces()
  {
    return Collections.unmodifiableList(this.pc);
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public double getVolumeDeltaBeforeIceCreamSandwich()
  {
    return this.ph;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzb.zza(this, paramParcel, paramInt);
  }
  
  public static final class Builder
  {
    private LaunchOptions nf = new LaunchOptions();
    private String pb;
    private List<String> pc = new ArrayList();
    private boolean pd;
    private boolean pe = true;
    private CastMediaOptions pf = new CastMediaOptions.Builder().build();
    private boolean pg = true;
    private double ph = 0.05000000074505806D;
    
    public CastOptions build()
    {
      return new CastOptions(1, this.pb, this.pc, this.pd, this.nf, this.pe, this.pf, this.pg, this.ph);
    }
    
    public Builder setCastMediaOptions(CastMediaOptions paramCastMediaOptions)
    {
      this.pf = paramCastMediaOptions;
      return this;
    }
    
    public Builder setEnableReconnectionService(boolean paramBoolean)
    {
      this.pg = paramBoolean;
      return this;
    }
    
    public Builder setLaunchOptions(LaunchOptions paramLaunchOptions)
    {
      this.nf = paramLaunchOptions;
      return this;
    }
    
    public Builder setReceiverApplicationId(String paramString)
    {
      this.pb = paramString;
      return this;
    }
    
    public Builder setResumeSavedSession(boolean paramBoolean)
    {
      this.pe = paramBoolean;
      return this;
    }
    
    public Builder setStopReceiverApplicationWhenEndingSession(boolean paramBoolean)
    {
      this.pd = paramBoolean;
      return this;
    }
    
    public Builder setSupportedNamespaces(List<String> paramList)
    {
      this.pc = paramList;
      return this;
    }
    
    public Builder setVolumeDeltaBeforeIceCreamSandwich(double paramDouble)
      throws IllegalArgumentException
    {
      if ((paramDouble <= 0.0D) || (paramDouble > 0.5D)) {
        throw new IllegalArgumentException("volumeDelta must be greater than 0 and less or equal to 0.5");
      }
      this.ph = paramDouble;
      return this;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/framework/CastOptions.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */