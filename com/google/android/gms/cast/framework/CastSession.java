package com.google.android.gms.cast.framework;

import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import com.google.android.gms.cast.ApplicationMetadata;
import com.google.android.gms.cast.Cast.ApplicationConnectionResult;
import com.google.android.gms.cast.Cast.CastApi;
import com.google.android.gms.cast.Cast.Listener;
import com.google.android.gms.cast.Cast.MessageReceivedCallback;
import com.google.android.gms.cast.CastDevice;
import com.google.android.gms.cast.LaunchOptions;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.cast.internal.zzm;
import com.google.android.gms.cast.internal.zzn;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.internal.zzog;
import com.google.android.gms.internal.zzoi;
import com.google.android.gms.internal.zzou;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class CastSession
  extends Session
{
  private static final zzm oT = new zzm("CastSession");
  private GoogleApiClient mD;
  private final Set<Cast.Listener> pi = new HashSet();
  private final zzh pj;
  private final Cast.CastApi pk;
  private final zzoi pl;
  private final zzou pm;
  private RemoteMediaClient pn;
  private CastDevice po;
  private Cast.ApplicationConnectionResult pp;
  private final Context zzcfo;
  
  public CastSession(Context paramContext, String paramString1, String paramString2, CastOptions paramCastOptions, Cast.CastApi paramCastApi, zzoi paramzzoi, zzou paramzzou)
  {
    super(paramContext, paramString1, paramString2);
    this.zzcfo = paramContext.getApplicationContext();
    this.pk = paramCastApi;
    this.pl = paramzzoi;
    this.pm = paramzzou;
    this.pj = zzog.zza(paramContext, paramCastOptions, zzalz(), new zzb(null));
  }
  
  private void zzes(int paramInt)
  {
    this.pm.zzey(paramInt);
    if (this.mD != null)
    {
      this.mD.disconnect();
      this.mD = null;
    }
    this.po = null;
    if (this.pn != null) {}
    try
    {
      this.pn.zzd(null);
      this.pn = null;
      this.pp = null;
      return;
    }
    catch (IOException localIOException)
    {
      for (;;)
      {
        oT.zza(localIOException, "Exception when setting GoogleApiClient.", new Object[0]);
      }
    }
  }
  
  private void zzk(Bundle paramBundle)
  {
    this.po = CastDevice.getFromBundle(paramBundle);
    if (this.po == null)
    {
      if (isResuming())
      {
        notifyFailedToResumeSession(8);
        return;
      }
      notifyFailedToStartSession(8);
      return;
    }
    if (this.mD != null)
    {
      this.mD.disconnect();
      this.mD = null;
    }
    oT.zzb("Acquiring a connection to Google Play Services for %s", new Object[] { this.po });
    paramBundle = new zzd(null);
    this.mD = this.pl.zza(this.zzcfo, this.po, new zzc(null), paramBundle, paramBundle);
    this.mD.connect();
  }
  
  public void addCastListener(Cast.Listener paramListener)
  {
    if (paramListener != null) {
      this.pi.add(paramListener);
    }
  }
  
  protected void end(boolean paramBoolean)
  {
    try
    {
      this.pj.zzb(paramBoolean, 0);
      notifySessionEnded(0);
      return;
    }
    catch (RemoteException localRemoteException)
    {
      for (;;)
      {
        oT.zzb(localRemoteException, "Unable to call %s on %s.", new Object[] { "disconnectFromDevice", zzh.class.getSimpleName() });
      }
    }
  }
  
  public int getActiveInputState()
    throws IllegalStateException
  {
    if (this.mD != null) {
      return this.pk.getActiveInputState(this.mD);
    }
    return -1;
  }
  
  public Cast.ApplicationConnectionResult getApplicationConnectionResult()
  {
    return this.pp;
  }
  
  public ApplicationMetadata getApplicationMetadata()
    throws IllegalStateException
  {
    if (this.mD != null) {
      return this.pk.getApplicationMetadata(this.mD);
    }
    return null;
  }
  
  public String getApplicationStatus()
    throws IllegalStateException
  {
    if (this.mD != null) {
      return this.pk.getApplicationStatus(this.mD);
    }
    return null;
  }
  
  public CastDevice getCastDevice()
  {
    return this.po;
  }
  
  public RemoteMediaClient getRemoteMediaClient()
  {
    return this.pn;
  }
  
  public long getSessionRemainingTimeMs()
  {
    if (this.pn == null) {
      return 0L;
    }
    return this.pn.getStreamDuration() - this.pn.getApproximateStreamPosition();
  }
  
  public int getStandbyState()
    throws IllegalStateException
  {
    if (this.mD != null) {
      return this.pk.getStandbyState(this.mD);
    }
    return -1;
  }
  
  public double getVolume()
    throws IllegalStateException
  {
    if (this.mD != null) {
      return this.pk.getVolume(this.mD);
    }
    return 0.0D;
  }
  
  public boolean isMute()
    throws IllegalStateException
  {
    if (this.mD != null) {
      return this.pk.isMute(this.mD);
    }
    return false;
  }
  
  public void removeCastListener(Cast.Listener paramListener)
  {
    if (paramListener != null) {
      this.pi.remove(paramListener);
    }
  }
  
  public void removeMessageReceivedCallbacks(String paramString)
    throws IOException, IllegalArgumentException
  {
    if (this.mD != null) {
      this.pk.removeMessageReceivedCallbacks(this.mD, paramString);
    }
  }
  
  public void requestStatus()
    throws IOException, IllegalStateException
  {
    if (this.mD != null) {
      this.pk.requestStatus(this.mD);
    }
  }
  
  protected void resume(Bundle paramBundle)
  {
    zzk(paramBundle);
  }
  
  public PendingResult<Status> sendMessage(String paramString1, String paramString2)
  {
    if (this.mD != null) {
      return this.pk.sendMessage(this.mD, paramString1, paramString2);
    }
    return null;
  }
  
  public void setMessageReceivedCallbacks(String paramString, Cast.MessageReceivedCallback paramMessageReceivedCallback)
    throws IOException, IllegalStateException
  {
    if (this.mD != null) {
      this.pk.setMessageReceivedCallbacks(this.mD, paramString, paramMessageReceivedCallback);
    }
  }
  
  public void setMute(boolean paramBoolean)
    throws IOException, IllegalStateException
  {
    if (this.mD != null) {
      this.pk.setMute(this.mD, paramBoolean);
    }
  }
  
  public void setVolume(double paramDouble)
    throws IOException
  {
    if (this.mD != null) {
      this.pk.setVolume(this.mD, paramDouble);
    }
  }
  
  protected void start(Bundle paramBundle)
  {
    zzk(paramBundle);
  }
  
  private class zza
    implements ResultCallback<Cast.ApplicationConnectionResult>
  {
    String pq;
    
    zza(String paramString)
    {
      this.pq = paramString;
    }
    
    public void zza(@NonNull Cast.ApplicationConnectionResult paramApplicationConnectionResult)
    {
      CastSession.zza(CastSession.this, paramApplicationConnectionResult);
      try
      {
        if (paramApplicationConnectionResult.getStatus().isSuccess())
        {
          CastSession.zzalr().zzb("%s() -> success result", new Object[] { this.pq });
          CastSession.zza(CastSession.this, new RemoteMediaClient(new zzn(null), CastSession.zzc(CastSession.this)));
          try
          {
            CastSession.zze(CastSession.this).zzd(CastSession.zzd(CastSession.this));
            CastSession.zze(CastSession.this).requestStatus();
            CastSession.zzf(CastSession.this).zza(CastSession.zze(CastSession.this), CastSession.this.getCastDevice());
            CastSession.zza(CastSession.this).zza(paramApplicationConnectionResult.getApplicationMetadata(), paramApplicationConnectionResult.getApplicationStatus(), paramApplicationConnectionResult.getSessionId(), paramApplicationConnectionResult.getWasLaunched());
            return;
          }
          catch (IOException localIOException)
          {
            for (;;)
            {
              CastSession.zzalr().zza(localIOException, "Exception when setting GoogleApiClient.", new Object[0]);
              CastSession.zza(CastSession.this, null);
            }
          }
        }
      }
      catch (RemoteException paramApplicationConnectionResult)
      {
        CastSession.zzalr().zzb(paramApplicationConnectionResult, "Unable to call %s on %s.", new Object[] { "methods", zzh.class.getSimpleName() });
        return;
      }
      tmp222_219[0] = this.pq;
      CastSession.zzalr().zzb("%s() -> failure result", tmp222_219);
      CastSession.zza(CastSession.this).zzet(paramApplicationConnectionResult.getStatus().getStatusCode());
    }
  }
  
  private class zzb
    extends zzf.zza
  {
    private zzb() {}
    
    public void zza(String paramString, LaunchOptions paramLaunchOptions)
    {
      CastSession.zzc(CastSession.this).launchApplication(CastSession.zzd(CastSession.this), paramString, paramLaunchOptions).setResultCallback(new CastSession.zza(CastSession.this, "launchApplication"));
    }
    
    public int zzalm()
    {
      return 9877208;
    }
    
    public void zzes(int paramInt)
    {
      CastSession.zza(CastSession.this, paramInt);
    }
    
    public void zzgo(String paramString)
    {
      CastSession.zzc(CastSession.this).stopApplication(CastSession.zzd(CastSession.this), paramString);
    }
    
    public void zzy(String paramString1, String paramString2)
    {
      CastSession.zzc(CastSession.this).joinApplication(CastSession.zzd(CastSession.this), paramString1, paramString2).setResultCallback(new CastSession.zza(CastSession.this, "joinApplication"));
    }
  }
  
  private class zzc
    extends Cast.Listener
  {
    private zzc() {}
    
    public void onActiveInputStateChanged(int paramInt)
    {
      Iterator localIterator = new HashSet(CastSession.zzb(CastSession.this)).iterator();
      while (localIterator.hasNext()) {
        ((Cast.Listener)localIterator.next()).onActiveInputStateChanged(paramInt);
      }
    }
    
    public void onApplicationDisconnected(int paramInt)
    {
      CastSession.zza(CastSession.this, paramInt);
      CastSession.this.notifySessionEnded(paramInt);
      Iterator localIterator = new HashSet(CastSession.zzb(CastSession.this)).iterator();
      while (localIterator.hasNext()) {
        ((Cast.Listener)localIterator.next()).onApplicationDisconnected(paramInt);
      }
    }
    
    public void onApplicationMetadataChanged(ApplicationMetadata paramApplicationMetadata)
    {
      Iterator localIterator = new HashSet(CastSession.zzb(CastSession.this)).iterator();
      while (localIterator.hasNext()) {
        ((Cast.Listener)localIterator.next()).onApplicationMetadataChanged(paramApplicationMetadata);
      }
    }
    
    public void onApplicationStatusChanged()
    {
      Iterator localIterator = new HashSet(CastSession.zzb(CastSession.this)).iterator();
      while (localIterator.hasNext()) {
        ((Cast.Listener)localIterator.next()).onApplicationStatusChanged();
      }
    }
    
    public void onStandbyStateChanged(int paramInt)
    {
      Iterator localIterator = new HashSet(CastSession.zzb(CastSession.this)).iterator();
      while (localIterator.hasNext()) {
        ((Cast.Listener)localIterator.next()).onStandbyStateChanged(paramInt);
      }
    }
    
    public void onVolumeChanged()
    {
      Iterator localIterator = new HashSet(CastSession.zzb(CastSession.this)).iterator();
      while (localIterator.hasNext()) {
        ((Cast.Listener)localIterator.next()).onVolumeChanged();
      }
    }
  }
  
  private class zzd
    implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener
  {
    private zzd() {}
    
    public void onConnected(Bundle paramBundle)
    {
      try
      {
        CastSession.zza(CastSession.this).onConnected(paramBundle);
        return;
      }
      catch (RemoteException paramBundle)
      {
        CastSession.zzalr().zzb(paramBundle, "Unable to call %s on %s.", new Object[] { "onConnected", zzh.class.getSimpleName() });
      }
    }
    
    public void onConnectionFailed(@NonNull ConnectionResult paramConnectionResult)
    {
      try
      {
        CastSession.zza(CastSession.this).onConnectionFailed(paramConnectionResult);
        return;
      }
      catch (RemoteException paramConnectionResult)
      {
        CastSession.zzalr().zzb(paramConnectionResult, "Unable to call %s on %s.", new Object[] { "onConnectionFailed", zzh.class.getSimpleName() });
      }
    }
    
    public void onConnectionSuspended(int paramInt)
    {
      try
      {
        CastSession.zza(CastSession.this).onConnectionSuspended(paramInt);
        return;
      }
      catch (RemoteException localRemoteException)
      {
        CastSession.zzalr().zzb(localRemoteException, "Unable to call %s on %s.", new Object[] { "onConnectionSuspended", zzh.class.getSimpleName() });
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/framework/CastSession.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */