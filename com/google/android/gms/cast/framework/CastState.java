package com.google.android.gms.cast.framework;

import java.util.Locale;

public final class CastState
{
  public static final int CONNECTED = 4;
  public static final int CONNECTING = 3;
  public static final int NOT_CONNECTED = 2;
  public static final int NO_DEVICES_AVAILABLE = 1;
  
  public static String toString(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return String.format(Locale.ROOT, "UNKNOWN_STATE(%d)", new Object[] { Integer.valueOf(paramInt) });
    case 1: 
      return "NO_DEVICES_AVAILABLE";
    case 2: 
      return "NOT_CONNECTED";
    case 3: 
      return "CONNECTING";
    }
    return "CONNECTED";
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/framework/CastState.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */