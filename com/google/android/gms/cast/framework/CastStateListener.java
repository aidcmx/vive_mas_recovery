package com.google.android.gms.cast.framework;

public abstract interface CastStateListener
{
  public abstract void onCastStateChanged(int paramInt);
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/framework/CastStateListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */