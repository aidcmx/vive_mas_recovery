package com.google.android.gms.cast.framework;

public abstract interface DiscoveryManagerListener
{
  public abstract void onDeviceAvailabilityChanged(boolean paramBoolean);
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/framework/DiscoveryManagerListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */