package com.google.android.gms.cast.framework;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.support.annotation.ColorRes;
import android.support.annotation.DimenRes;
import android.support.annotation.StringRes;
import android.support.v7.app.MediaRouteButton;
import android.view.MenuItem;
import android.view.View;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.util.zzs;
import com.google.android.gms.internal.zzom;
import com.google.android.gms.internal.zzon;

public abstract interface IntroductoryOverlay
{
  public abstract void remove();
  
  public abstract void show();
  
  public static class Builder
  {
    private Activity mActivity;
    private String pA;
    private View pu;
    private int pv;
    private String pw;
    private IntroductoryOverlay.OnOverlayDismissedListener px;
    private boolean py;
    private float pz;
    
    public Builder(Activity paramActivity, MediaRouteButton paramMediaRouteButton)
    {
      this.mActivity = ((Activity)zzaa.zzy(paramActivity));
      this.pu = ((View)zzaa.zzy(paramMediaRouteButton));
    }
    
    @TargetApi(11)
    public Builder(Activity paramActivity, MenuItem paramMenuItem)
    {
      this.mActivity = ((Activity)zzaa.zzy(paramActivity));
      if (zzs.zzayn()) {
        this.pu = ((MenuItem)zzaa.zzy(paramMenuItem)).getActionView();
      }
    }
    
    public IntroductoryOverlay build()
    {
      if (zzs.zzayr()) {
        return new zzom(this);
      }
      return new zzon(this);
    }
    
    public Activity getActivity()
    {
      return this.mActivity;
    }
    
    public Builder setButtonText(@StringRes int paramInt)
    {
      this.pA = this.mActivity.getResources().getString(paramInt);
      return this;
    }
    
    public Builder setButtonText(String paramString)
    {
      this.pA = paramString;
      return this;
    }
    
    public Builder setFocusRadius(float paramFloat)
    {
      this.pz = paramFloat;
      return this;
    }
    
    public Builder setFocusRadiusId(@DimenRes int paramInt)
    {
      this.pz = this.mActivity.getResources().getDimension(paramInt);
      return this;
    }
    
    public Builder setOnOverlayDismissedListener(IntroductoryOverlay.OnOverlayDismissedListener paramOnOverlayDismissedListener)
    {
      this.px = paramOnOverlayDismissedListener;
      return this;
    }
    
    public Builder setOverlayColor(@ColorRes int paramInt)
    {
      this.pv = this.mActivity.getResources().getColor(paramInt);
      return this;
    }
    
    public Builder setSingleTime()
    {
      this.py = true;
      return this;
    }
    
    public Builder setTitleText(@StringRes int paramInt)
    {
      this.pw = this.mActivity.getResources().getString(paramInt);
      return this;
    }
    
    public Builder setTitleText(String paramString)
    {
      this.pw = paramString;
      return this;
    }
    
    public View zzamd()
    {
      return this.pu;
    }
    
    public IntroductoryOverlay.OnOverlayDismissedListener zzame()
    {
      return this.px;
    }
    
    public int zzamf()
    {
      return this.pv;
    }
    
    public boolean zzamg()
    {
      return this.py;
    }
    
    public String zzamh()
    {
      return this.pw;
    }
    
    public String zzami()
    {
      return this.pA;
    }
    
    public float zzamj()
    {
      return this.pz;
    }
  }
  
  public static abstract interface OnOverlayDismissedListener
  {
    public abstract void onOverlayDismissed();
  }
  
  public static class zza
  {
    public static void zzbc(Context paramContext)
    {
      PreferenceManager.getDefaultSharedPreferences(paramContext).edit().putBoolean("googlecast-introOverlayShown", true).apply();
    }
    
    public static boolean zzbd(Context paramContext)
    {
      return PreferenceManager.getDefaultSharedPreferences(paramContext).getBoolean("googlecast-introOverlayShown", false);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/framework/IntroductoryOverlay.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */