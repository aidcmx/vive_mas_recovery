package com.google.android.gms.cast.framework;

import android.content.Context;
import java.util.List;

public abstract interface OptionsProvider
{
  public abstract List<SessionProvider> getAdditionalSessionProviders(Context paramContext);
  
  public abstract CastOptions getCastOptions(Context paramContext);
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/framework/OptionsProvider.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */