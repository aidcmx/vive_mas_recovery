package com.google.android.gms.cast.framework;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import com.google.android.gms.cast.internal.zzm;
import com.google.android.gms.internal.zzog;

public class ReconnectionService
  extends Service
{
  private static final zzm oT = new zzm("ReconnectionService");
  private zzl pB;
  
  public IBinder onBind(Intent paramIntent)
  {
    try
    {
      paramIntent = this.pB.onBind(paramIntent);
      return paramIntent;
    }
    catch (RemoteException paramIntent)
    {
      oT.zzb(paramIntent, "Unable to call %s on %s.", new Object[] { "onBind", zzl.class.getSimpleName() });
    }
    return null;
  }
  
  public void onCreate()
  {
    CastContext localCastContext = CastContext.getSharedInstance(this);
    this.pB = zzog.zza(this, localCastContext.getSessionManager().zzalq(), localCastContext.zzalp().zzalq());
    try
    {
      this.pB.onCreate();
      super.onCreate();
      return;
    }
    catch (RemoteException localRemoteException)
    {
      for (;;)
      {
        oT.zzb(localRemoteException, "Unable to call %s on %s.", new Object[] { "onCreate", zzl.class.getSimpleName() });
      }
    }
  }
  
  public void onDestroy()
  {
    try
    {
      this.pB.onDestroy();
      super.onDestroy();
      return;
    }
    catch (RemoteException localRemoteException)
    {
      for (;;)
      {
        oT.zzb(localRemoteException, "Unable to call %s on %s.", new Object[] { "onDestroy", zzl.class.getSimpleName() });
      }
    }
  }
  
  public int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2)
  {
    try
    {
      paramInt1 = this.pB.onStartCommand(paramIntent, paramInt1, paramInt2);
      return paramInt1;
    }
    catch (RemoteException paramIntent)
    {
      oT.zzb(paramIntent, "Unable to call %s on %s.", new Object[] { "onStartCommand", zzl.class.getSimpleName() });
    }
    return 1;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/framework/ReconnectionService.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */