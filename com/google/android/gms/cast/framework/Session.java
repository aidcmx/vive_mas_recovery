package com.google.android.gms.cast.framework;

import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.dynamic.zzd;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.internal.zzog;

public abstract class Session
{
  private static final com.google.android.gms.cast.internal.zzm oT = new com.google.android.gms.cast.internal.zzm("Session");
  private final zzm pC = zzog.zza(paramContext, paramString1, paramString2, this.pD);
  private final zza pD = new zza(null);
  
  protected Session(Context paramContext, String paramString1, String paramString2) {}
  
  protected abstract void end(boolean paramBoolean);
  
  public final String getCategory()
  {
    try
    {
      String str = this.pC.getCategory();
      return str;
    }
    catch (RemoteException localRemoteException)
    {
      oT.zzb(localRemoteException, "Unable to call %s on %s.", new Object[] { "getCategory", zzm.class.getSimpleName() });
    }
    return null;
  }
  
  public final String getSessionId()
  {
    try
    {
      String str = this.pC.getSessionId();
      return str;
    }
    catch (RemoteException localRemoteException)
    {
      oT.zzb(localRemoteException, "Unable to call %s on %s.", new Object[] { "getSessionId", zzm.class.getSimpleName() });
    }
    return null;
  }
  
  public long getSessionRemainingTimeMs()
  {
    return 0L;
  }
  
  public boolean isConnected()
  {
    try
    {
      boolean bool = this.pC.isConnected();
      return bool;
    }
    catch (RemoteException localRemoteException)
    {
      oT.zzb(localRemoteException, "Unable to call %s on %s.", new Object[] { "isConnected", zzm.class.getSimpleName() });
    }
    return false;
  }
  
  public boolean isConnecting()
  {
    try
    {
      boolean bool = this.pC.isConnecting();
      return bool;
    }
    catch (RemoteException localRemoteException)
    {
      oT.zzb(localRemoteException, "Unable to call %s on %s.", new Object[] { "isConnecting", zzm.class.getSimpleName() });
    }
    return false;
  }
  
  public boolean isDisconnected()
  {
    try
    {
      boolean bool = this.pC.isDisconnected();
      return bool;
    }
    catch (RemoteException localRemoteException)
    {
      oT.zzb(localRemoteException, "Unable to call %s on %s.", new Object[] { "isDisconnected", zzm.class.getSimpleName() });
    }
    return true;
  }
  
  public boolean isDisconnecting()
  {
    try
    {
      boolean bool = this.pC.isDisconnecting();
      return bool;
    }
    catch (RemoteException localRemoteException)
    {
      oT.zzb(localRemoteException, "Unable to call %s on %s.", new Object[] { "isDisconnecting", zzm.class.getSimpleName() });
    }
    return false;
  }
  
  public boolean isResuming()
  {
    try
    {
      boolean bool = this.pC.isResuming();
      return bool;
    }
    catch (RemoteException localRemoteException)
    {
      oT.zzb(localRemoteException, "Unable to call %s on %s.", new Object[] { "isResuming", zzm.class.getSimpleName() });
    }
    return false;
  }
  
  public boolean isSuspended()
  {
    try
    {
      boolean bool = this.pC.isSuspended();
      return bool;
    }
    catch (RemoteException localRemoteException)
    {
      oT.zzb(localRemoteException, "Unable to call %s on %s.", new Object[] { "isSuspended", zzm.class.getSimpleName() });
    }
    return false;
  }
  
  protected final void notifyFailedToResumeSession(int paramInt)
  {
    try
    {
      this.pC.notifyFailedToResumeSession(paramInt);
      return;
    }
    catch (RemoteException localRemoteException)
    {
      oT.zzb(localRemoteException, "Unable to call %s on %s.", new Object[] { "notifyFailedToResumeSession", zzm.class.getSimpleName() });
    }
  }
  
  protected final void notifyFailedToStartSession(int paramInt)
  {
    try
    {
      this.pC.notifyFailedToStartSession(paramInt);
      return;
    }
    catch (RemoteException localRemoteException)
    {
      oT.zzb(localRemoteException, "Unable to call %s on %s.", new Object[] { "notifyFailedToStartSession", zzm.class.getSimpleName() });
    }
  }
  
  protected final void notifySessionEnded(int paramInt)
  {
    try
    {
      this.pC.notifySessionEnded(paramInt);
      return;
    }
    catch (RemoteException localRemoteException)
    {
      oT.zzb(localRemoteException, "Unable to call %s on %s.", new Object[] { "notifySessionEnded", zzm.class.getSimpleName() });
    }
  }
  
  protected final void notifySessionResumed(boolean paramBoolean)
  {
    try
    {
      this.pC.notifySessionResumed(paramBoolean);
      return;
    }
    catch (RemoteException localRemoteException)
    {
      oT.zzb(localRemoteException, "Unable to call %s on %s.", new Object[] { "notifySessionResumed", zzm.class.getSimpleName() });
    }
  }
  
  protected final void notifySessionStarted(String paramString)
  {
    try
    {
      this.pC.notifySessionStarted(paramString);
      return;
    }
    catch (RemoteException paramString)
    {
      oT.zzb(paramString, "Unable to call %s on %s.", new Object[] { "notifySessionStarted", zzm.class.getSimpleName() });
    }
  }
  
  protected final void notifySessionSuspended(int paramInt)
  {
    try
    {
      this.pC.notifySessionSuspended(paramInt);
      return;
    }
    catch (RemoteException localRemoteException)
    {
      oT.zzb(localRemoteException, "Unable to call %s on %s.", new Object[] { "notifySessionSuspended", zzm.class.getSimpleName() });
    }
  }
  
  protected abstract void resume(Bundle paramBundle);
  
  protected abstract void start(Bundle paramBundle);
  
  public final zzd zzalz()
  {
    try
    {
      zzd localzzd = this.pC.zzalz();
      return localzzd;
    }
    catch (RemoteException localRemoteException)
    {
      oT.zzb(localRemoteException, "Unable to call %s on %s.", new Object[] { "getWrappedObject", zzm.class.getSimpleName() });
    }
    return null;
  }
  
  private class zza
    extends zzq.zza
  {
    private zza() {}
    
    public void end(boolean paramBoolean)
    {
      Session.this.end(paramBoolean);
    }
    
    public long getSessionRemainingTimeMs()
    {
      return Session.this.getSessionRemainingTimeMs();
    }
    
    public void resume(Bundle paramBundle)
    {
      Session.this.resume(paramBundle);
    }
    
    public void start(Bundle paramBundle)
    {
      Session.this.start(paramBundle);
    }
    
    public int zzalm()
    {
      return 9877208;
    }
    
    public zzd zzamc()
    {
      return zze.zzac(Session.this);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/framework/Session.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */