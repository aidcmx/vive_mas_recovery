package com.google.android.gms.cast.framework;

import android.os.RemoteException;
import com.google.android.gms.cast.internal.zzm;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.dynamic.zzd;
import com.google.android.gms.dynamic.zze;

public class SessionManager
{
  private static final zzm oT = new zzm("SessionManager");
  private final zzn pF;
  
  public SessionManager(zzn paramzzn)
  {
    this.pF = paramzzn;
  }
  
  public void addCastStateListener(CastStateListener paramCastStateListener)
    throws NullPointerException
  {
    zzaa.zzy(paramCastStateListener);
    try
    {
      this.pF.zza(new zzc(paramCastStateListener));
      return;
    }
    catch (RemoteException paramCastStateListener)
    {
      oT.zzb(paramCastStateListener, "Unable to call %s on %s.", new Object[] { "addCastStateListener", zzn.class.getSimpleName() });
    }
  }
  
  public void addSessionManagerListener(SessionManagerListener<Session> paramSessionManagerListener)
    throws NullPointerException
  {
    addSessionManagerListener(paramSessionManagerListener, Session.class);
  }
  
  public <T extends Session> void addSessionManagerListener(SessionManagerListener<T> paramSessionManagerListener, Class<T> paramClass)
    throws NullPointerException
  {
    zzaa.zzy(paramSessionManagerListener);
    zzaa.zzy(paramClass);
    try
    {
      this.pF.zza(new zzr(paramSessionManagerListener, paramClass));
      return;
    }
    catch (RemoteException paramSessionManagerListener)
    {
      oT.zzb(paramSessionManagerListener, "Unable to call %s on %s.", new Object[] { "addSessionManagerListener", zzn.class.getSimpleName() });
    }
  }
  
  public void endCurrentSession(boolean paramBoolean)
  {
    try
    {
      this.pF.zzb(true, paramBoolean);
      return;
    }
    catch (RemoteException localRemoteException)
    {
      oT.zzb(localRemoteException, "Unable to call %s on %s.", new Object[] { "endCurrentSession", zzn.class.getSimpleName() });
    }
  }
  
  public CastSession getCurrentCastSession()
  {
    Session localSession = getCurrentSession();
    if ((localSession != null) && ((localSession instanceof CastSession))) {
      return (CastSession)localSession;
    }
    return null;
  }
  
  public Session getCurrentSession()
  {
    try
    {
      Session localSession = (Session)zze.zzae(this.pF.zzamb());
      return localSession;
    }
    catch (RemoteException localRemoteException)
    {
      oT.zzb(localRemoteException, "Unable to call %s on %s.", new Object[] { "getWrappedCurrentSession", zzn.class.getSimpleName() });
    }
    return null;
  }
  
  public void removeCastStateListener(CastStateListener paramCastStateListener)
  {
    if (paramCastStateListener == null) {
      return;
    }
    try
    {
      this.pF.zzb(new zzc(paramCastStateListener));
      return;
    }
    catch (RemoteException paramCastStateListener)
    {
      oT.zzb(paramCastStateListener, "Unable to call %s on %s.", new Object[] { "removeCastStateListener", zzn.class.getSimpleName() });
    }
  }
  
  public void removeSessionManagerListener(SessionManagerListener<Session> paramSessionManagerListener)
  {
    removeSessionManagerListener(paramSessionManagerListener, Session.class);
  }
  
  public <T extends Session> void removeSessionManagerListener(SessionManagerListener<T> paramSessionManagerListener, Class paramClass)
  {
    zzaa.zzy(paramClass);
    if (paramSessionManagerListener == null) {
      return;
    }
    try
    {
      this.pF.zzb(new zzr(paramSessionManagerListener, paramClass));
      return;
    }
    catch (RemoteException paramSessionManagerListener)
    {
      oT.zzb(paramSessionManagerListener, "Unable to call %s on %s.", new Object[] { "removeSessionManagerListener", zzn.class.getSimpleName() });
    }
  }
  
  public zzd zzalq()
  {
    try
    {
      zzd localzzd = this.pF.zzalv();
      return localzzd;
    }
    catch (RemoteException localRemoteException)
    {
      oT.zzb(localRemoteException, "Unable to call %s on %s.", new Object[] { "getWrappedThis", zzn.class.getSimpleName() });
    }
    return null;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/framework/SessionManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */