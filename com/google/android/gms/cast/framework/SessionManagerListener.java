package com.google.android.gms.cast.framework;

public abstract interface SessionManagerListener<T extends Session>
{
  public abstract void onSessionEnded(T paramT, int paramInt);
  
  public abstract void onSessionEnding(T paramT);
  
  public abstract void onSessionResumeFailed(T paramT, int paramInt);
  
  public abstract void onSessionResumed(T paramT, boolean paramBoolean);
  
  public abstract void onSessionResuming(T paramT, String paramString);
  
  public abstract void onSessionStartFailed(T paramT, int paramInt);
  
  public abstract void onSessionStarted(T paramT, String paramString);
  
  public abstract void onSessionStarting(T paramT);
  
  public abstract void onSessionSuspended(T paramT, int paramInt);
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/framework/SessionManagerListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */