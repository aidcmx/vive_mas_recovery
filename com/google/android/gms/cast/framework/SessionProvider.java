package com.google.android.gms.cast.framework;

import android.content.Context;
import android.os.IBinder;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.dynamic.zzd;

public abstract class SessionProvider
{
  private final String mCategory;
  private final zza pI = new zza(null);
  private final Context zzatc;
  
  protected SessionProvider(Context paramContext, String paramString)
  {
    this.zzatc = ((Context)zzaa.zzy(paramContext)).getApplicationContext();
    this.mCategory = zzaa.zzib(paramString);
  }
  
  public abstract Session createSession(String paramString);
  
  public final String getCategory()
  {
    return this.mCategory;
  }
  
  public final Context getContext()
  {
    return this.zzatc;
  }
  
  public abstract boolean isSessionRecoverable();
  
  public IBinder zzamk()
  {
    return this.pI;
  }
  
  private class zza
    extends zzp.zza
  {
    private zza() {}
    
    public String getCategory()
    {
      return SessionProvider.this.getCategory();
    }
    
    public boolean isSessionRecoverable()
    {
      return SessionProvider.this.isSessionRecoverable();
    }
    
    public zzd zzgp(String paramString)
    {
      paramString = SessionProvider.this.createSession(paramString);
      if (paramString == null) {
        return null;
      }
      return paramString.zzalz();
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/framework/SessionProvider.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */