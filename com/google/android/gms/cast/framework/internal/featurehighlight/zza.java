package com.google.android.gms.cast.framework.internal.featurehighlight;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;
import android.support.v4.view.GestureDetectorCompat;
import android.util.AttributeSet;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnLayoutChangeListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import com.google.android.gms.R.id;
import com.google.android.gms.internal.zzagt;
import com.google.android.gms.internal.zzagy;

public final class zza
  extends ViewGroup
{
  private final int[] pY = new int[2];
  private final Rect pZ = new Rect();
  private final Rect qa = new Rect();
  private final zze qb;
  private final zzc qc;
  private zzb qd;
  @Nullable
  private View qe;
  @Nullable
  private Animator qf;
  private final zzd qg;
  private final GestureDetectorCompat qh;
  @Nullable
  private GestureDetectorCompat qi;
  private zza qj;
  private boolean qk;
  private View targetView;
  
  public zza(Context paramContext)
  {
    super(paramContext);
    setId(R.id.cast_featurehighlight_view);
    setWillNotDraw(false);
    this.qc = new zzc(paramContext);
    this.qc.setCallback(this);
    this.qb = new zze(paramContext);
    this.qb.setCallback(this);
    this.qg = new zzd(this);
    this.qh = new GestureDetectorCompat(paramContext, new GestureDetector.SimpleOnGestureListener()
    {
      public boolean onSingleTapUp(MotionEvent paramAnonymousMotionEvent)
      {
        float f1 = paramAnonymousMotionEvent.getX();
        float f2 = paramAnonymousMotionEvent.getY();
        if ((!zza.zza(zza.this, f1, f2)) || (!zza.zza(zza.this).zzd(f1, f2))) {
          zza.zzb(zza.this).dismiss();
        }
        return true;
      }
    });
    this.qh.setIsLongpressEnabled(false);
    setVisibility(8);
  }
  
  private void zza(Animator paramAnimator)
  {
    if (this.qf != null) {
      this.qf.cancel();
    }
    this.qf = paramAnimator;
    this.qf.start();
  }
  
  private void zza(int[] paramArrayOfInt, View paramView)
  {
    getLocationInWindow(paramArrayOfInt);
    int i = paramArrayOfInt[0];
    int j = paramArrayOfInt[1];
    paramView.getLocationInWindow(paramArrayOfInt);
    paramArrayOfInt[0] -= i;
    paramArrayOfInt[1] -= j;
  }
  
  private Animator zzamw()
  {
    ObjectAnimator localObjectAnimator = ObjectAnimator.ofFloat(this.qd.asView(), "alpha", new float[] { 0.0F, 1.0F }).setDuration(350L);
    localObjectAnimator.setInterpolator(zzagt.zzcnl());
    float f1 = this.pZ.exactCenterX();
    float f2 = this.qb.getCenterX();
    float f3 = this.pZ.exactCenterY();
    float f4 = this.qb.getCenterY();
    Animator localAnimator1 = this.qb.zze(f1 - f2, f3 - f4);
    Animator localAnimator2 = this.qc.zzamw();
    AnimatorSet localAnimatorSet = new AnimatorSet();
    localAnimatorSet.playTogether(new Animator[] { localObjectAnimator, localAnimator1, localAnimator2 });
    localAnimatorSet.addListener(new AnimatorListenerAdapter()
    {
      public void onAnimationEnd(Animator paramAnonymousAnimator)
      {
        zza.zza(zza.this, zza.zzc(zza.this));
        zza.zzd(zza.this).start();
      }
    });
    return localAnimatorSet;
  }
  
  private Animator zzamx()
  {
    return this.qc.zzamx();
  }
  
  private boolean zzc(float paramFloat1, float paramFloat2)
  {
    return this.qa.contains(Math.round(paramFloat1), Math.round(paramFloat2));
  }
  
  private Animator zzk(@Nullable final Runnable paramRunnable)
  {
    ObjectAnimator localObjectAnimator = ObjectAnimator.ofFloat(this.qd.asView(), "alpha", new float[] { 0.0F }).setDuration(200L);
    localObjectAnimator.setInterpolator(zzagt.zzcnm());
    Animator localAnimator1 = this.qb.zzamy();
    Animator localAnimator2 = this.qc.zzamy();
    AnimatorSet localAnimatorSet = new AnimatorSet();
    localAnimatorSet.playTogether(new Animator[] { localObjectAnimator, localAnimator1, localAnimator2 });
    localAnimatorSet.addListener(new AnimatorListenerAdapter()
    {
      public void onAnimationEnd(Animator paramAnonymousAnimator)
      {
        zza.this.setVisibility(8);
        zza.zza(zza.this, null);
        if (paramRunnable != null) {
          paramRunnable.run();
        }
      }
    });
    return localAnimatorSet;
  }
  
  private Animator zzl(@Nullable final Runnable paramRunnable)
  {
    ObjectAnimator localObjectAnimator = ObjectAnimator.ofFloat(this.qd.asView(), "alpha", new float[] { 0.0F }).setDuration(200L);
    localObjectAnimator.setInterpolator(zzagt.zzcnm());
    float f1 = this.pZ.exactCenterX();
    float f2 = this.qb.getCenterX();
    float f3 = this.pZ.exactCenterY();
    float f4 = this.qb.getCenterY();
    Animator localAnimator1 = this.qb.zzf(f1 - f2, f3 - f4);
    Animator localAnimator2 = this.qc.zzamz();
    AnimatorSet localAnimatorSet = new AnimatorSet();
    localAnimatorSet.playTogether(new Animator[] { localObjectAnimator, localAnimator1, localAnimator2 });
    localAnimatorSet.addListener(new AnimatorListenerAdapter()
    {
      public void onAnimationEnd(Animator paramAnonymousAnimator)
      {
        zza.this.setVisibility(8);
        zza.zza(zza.this, null);
        if (paramRunnable != null) {
          paramRunnable.run();
        }
      }
    });
    return localAnimatorSet;
  }
  
  protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
  {
    return paramLayoutParams instanceof ViewGroup.MarginLayoutParams;
  }
  
  protected ViewGroup.LayoutParams generateDefaultLayoutParams()
  {
    return new ViewGroup.MarginLayoutParams(-2, -2);
  }
  
  public ViewGroup.LayoutParams generateLayoutParams(AttributeSet paramAttributeSet)
  {
    return new ViewGroup.MarginLayoutParams(getContext(), paramAttributeSet);
  }
  
  protected ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
  {
    return new ViewGroup.MarginLayoutParams(paramLayoutParams);
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    paramCanvas.save();
    if (this.qe != null) {
      paramCanvas.clipRect(this.qa);
    }
    this.qb.draw(paramCanvas);
    this.qc.draw(paramCanvas);
    if (this.targetView != null)
    {
      if (this.targetView.getParent() != null)
      {
        Bitmap localBitmap = Bitmap.createBitmap(this.targetView.getWidth(), this.targetView.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas localCanvas = new Canvas(localBitmap);
        this.targetView.draw(localCanvas);
        int i = this.qb.getColor();
        int k = Color.red(i);
        int m = Color.green(i);
        int n = Color.blue(i);
        i = 0;
        while (i < localBitmap.getHeight())
        {
          int j = 0;
          while (j < localBitmap.getWidth())
          {
            int i1 = localBitmap.getPixel(j, i);
            if (Color.alpha(i1) != 0) {
              localBitmap.setPixel(j, i, Color.argb(Color.alpha(i1), k, m, n));
            }
            j += 1;
          }
          i += 1;
        }
        paramCanvas.drawBitmap(localBitmap, this.pZ.left, this.pZ.top, null);
      }
      paramCanvas.restore();
      return;
    }
    throw new IllegalStateException("Neither target view nor drawable was set");
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    if (this.targetView == null) {
      throw new IllegalStateException("Target view must be set before layout");
    }
    if (this.targetView.getParent() != null) {
      zza(this.pY, this.targetView);
    }
    this.pZ.set(this.pY[0], this.pY[1], this.pY[0] + this.targetView.getWidth(), this.pY[1] + this.targetView.getHeight());
    if (this.qe != null)
    {
      zza(this.pY, this.qe);
      this.qa.set(this.pY[0], this.pY[1], this.pY[0] + this.qe.getMeasuredWidth(), this.pY[1] + this.qe.getMeasuredHeight());
    }
    for (;;)
    {
      this.qb.setBounds(this.qa);
      this.qc.setBounds(this.qa);
      this.qg.zza(this.pZ, this.qa);
      return;
      this.qa.set(paramInt1, paramInt2, paramInt3, paramInt4);
    }
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    int i = View.MeasureSpec.getSize(paramInt1);
    int j = View.MeasureSpec.getSize(paramInt2);
    setMeasuredDimension(resolveSize(i, paramInt1), resolveSize(j, paramInt2));
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    int i = paramMotionEvent.getActionMasked();
    if (i == 0) {
      this.qk = this.pZ.contains((int)paramMotionEvent.getX(), (int)paramMotionEvent.getY());
    }
    if (this.qk)
    {
      MotionEvent localMotionEvent = paramMotionEvent;
      if (this.qi != null)
      {
        this.qi.onTouchEvent(paramMotionEvent);
        localMotionEvent = paramMotionEvent;
        if (i == 1)
        {
          localMotionEvent = MotionEvent.obtain(paramMotionEvent);
          localMotionEvent.setAction(3);
        }
      }
      if (this.targetView.getParent() != null) {
        this.targetView.onTouchEvent(localMotionEvent);
      }
      return true;
    }
    this.qh.onTouchEvent(paramMotionEvent);
    return true;
  }
  
  protected boolean verifyDrawable(Drawable paramDrawable)
  {
    return (super.verifyDrawable(paramDrawable)) || (paramDrawable == this.qb) || (paramDrawable == this.qc) || (paramDrawable == null);
  }
  
  public void zza(final View paramView1, @Nullable View paramView2, final boolean paramBoolean, final zza paramzza)
  {
    this.targetView = ((View)zzagy.zzy(paramView1));
    this.qe = paramView2;
    this.qj = ((zza)zzagy.zzy(paramzza));
    paramView1 = new GestureDetector.SimpleOnGestureListener()
    {
      public boolean onSingleTapUp(MotionEvent paramAnonymousMotionEvent)
      {
        if (paramView1.getParent() != null) {
          paramView1.performClick();
        }
        if (paramBoolean) {
          paramzza.zzamo();
        }
        return true;
      }
    };
    this.qi = new GestureDetectorCompat(getContext(), paramView1);
    this.qi.setIsLongpressEnabled(false);
    setVisibility(4);
  }
  
  public void zza(zzb paramzzb)
  {
    this.qd = ((zzb)zzagy.zzy(paramzzb));
    addView(paramzzb.asView(), 0);
  }
  
  public void zzamr()
  {
    if (this.targetView == null) {
      throw new IllegalStateException("Target view must be set before animation");
    }
    setVisibility(0);
    zza(zzamw());
  }
  
  @Nullable
  Drawable zzams()
  {
    return null;
  }
  
  View zzamt()
  {
    return this.qd.asView();
  }
  
  zze zzamu()
  {
    return this.qb;
  }
  
  zzc zzamv()
  {
    return this.qc;
  }
  
  public void zzeu(@ColorInt int paramInt)
  {
    this.qb.setColor(paramInt);
  }
  
  public void zzh(@Nullable final Runnable paramRunnable)
  {
    addOnLayoutChangeListener(new View.OnLayoutChangeListener()
    {
      public void onLayoutChange(View paramAnonymousView, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3, int paramAnonymousInt4, int paramAnonymousInt5, int paramAnonymousInt6, int paramAnonymousInt7, int paramAnonymousInt8)
      {
        if (paramRunnable != null) {
          paramRunnable.run();
        }
        zza.this.zzamr();
        zza.this.removeOnLayoutChangeListener(this);
      }
    });
  }
  
  public void zzi(@Nullable Runnable paramRunnable)
  {
    zza(zzl(paramRunnable));
  }
  
  public void zzj(@Nullable Runnable paramRunnable)
  {
    zza(zzk(paramRunnable));
  }
  
  public static abstract interface zza
  {
    public abstract void dismiss();
    
    public abstract void zzamo();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/framework/internal/featurehighlight/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */