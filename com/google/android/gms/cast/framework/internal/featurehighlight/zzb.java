package com.google.android.gms.cast.framework.internal.featurehighlight;

import android.support.annotation.Nullable;
import android.view.View;

public abstract interface zzb
{
  public abstract View asView();
  
  public abstract void setText(@Nullable CharSequence paramCharSequence1, @Nullable CharSequence paramCharSequence2);
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/framework/internal/featurehighlight/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */