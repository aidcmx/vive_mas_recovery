package com.google.android.gms.cast.framework.internal.featurehighlight;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.AnimatorSet.Builder;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorInt;
import com.google.android.gms.R.dimen;
import com.google.android.gms.R.integer;
import com.google.android.gms.internal.zzagr;
import com.google.android.gms.internal.zzagt;

class zzc
  extends Drawable
{
  private float centerX;
  private float centerY;
  private final Paint pU = new Paint();
  private float pV;
  private final Rect pZ = new Rect();
  private final Paint qu = new Paint();
  private final int qv;
  private final int qw;
  private float qx = 1.0F;
  
  public zzc(Context paramContext)
  {
    paramContext = paramContext.getResources();
    this.qv = paramContext.getDimensionPixelSize(R.dimen.cast_libraries_material_featurehighlight_inner_radius);
    this.qw = paramContext.getInteger(R.integer.cast_libraries_material_featurehighlight_pulse_base_alpha);
    this.pU.setAntiAlias(true);
    this.pU.setStyle(Paint.Style.FILL);
    this.qu.setAntiAlias(true);
    this.qu.setStyle(Paint.Style.FILL);
    setColor(-1);
  }
  
  public void draw(Canvas paramCanvas)
  {
    paramCanvas.drawCircle(this.centerX, this.centerY, this.pV * this.qx, this.pU);
  }
  
  public int getOpacity()
  {
    return -3;
  }
  
  public void setAlpha(int paramInt)
  {
    this.pU.setAlpha(paramInt);
    invalidateSelf();
  }
  
  public void setColor(@ColorInt int paramInt)
  {
    this.pU.setColor(paramInt);
    this.qu.setColor(paramInt);
    invalidateSelf();
  }
  
  public void setColorFilter(ColorFilter paramColorFilter)
  {
    this.pU.setColorFilter(paramColorFilter);
    invalidateSelf();
  }
  
  public Animator zzamw()
  {
    ObjectAnimator localObjectAnimator = ObjectAnimator.ofPropertyValuesHolder(this, new PropertyValuesHolder[] { PropertyValuesHolder.ofFloat("scale", new float[] { 0.0F, 1.0F }), PropertyValuesHolder.ofFloat("alpha", new float[] { 0.0F, 1.0F }) });
    localObjectAnimator.setInterpolator(zzagt.zzcnl());
    return localObjectAnimator.setDuration(350L);
  }
  
  public Animator zzamx()
  {
    AnimatorSet localAnimatorSet = new AnimatorSet();
    ObjectAnimator localObjectAnimator1 = ObjectAnimator.ofFloat(this, "scale", new float[] { 1.0F, 1.1F }).setDuration(500L);
    ObjectAnimator localObjectAnimator2 = ObjectAnimator.ofFloat(this, "scale", new float[] { 1.1F, 1.0F }).setDuration(500L);
    ObjectAnimator localObjectAnimator3 = ObjectAnimator.ofPropertyValuesHolder(this, new PropertyValuesHolder[] { PropertyValuesHolder.ofFloat("pulseScale", new float[] { 1.1F, 2.0F }), PropertyValuesHolder.ofFloat("pulseAlpha", new float[] { 1.0F, 0.0F }) }).setDuration(500L);
    localAnimatorSet.play(localObjectAnimator1);
    localAnimatorSet.play(localObjectAnimator2).with(localObjectAnimator3).after(localObjectAnimator1);
    localAnimatorSet.setInterpolator(zzagt.zzcnn());
    localAnimatorSet.setStartDelay(500L);
    localAnimatorSet.addListener(zzagr.zzc(localAnimatorSet));
    return localAnimatorSet;
  }
  
  public Animator zzamy()
  {
    return zzamz();
  }
  
  public Animator zzamz()
  {
    ObjectAnimator localObjectAnimator = ObjectAnimator.ofPropertyValuesHolder(this, new PropertyValuesHolder[] { PropertyValuesHolder.ofFloat("scale", new float[] { 0.0F }), PropertyValuesHolder.ofFloat("alpha", new float[] { 0.0F }), PropertyValuesHolder.ofFloat("pulseScale", new float[] { 0.0F }), PropertyValuesHolder.ofFloat("pulseAlpha", new float[] { 0.0F }) });
    localObjectAnimator.setInterpolator(zzagt.zzcnm());
    return localObjectAnimator.setDuration(200L);
  }
  
  public void zzc(Rect paramRect)
  {
    this.pZ.set(paramRect);
    this.centerX = this.pZ.exactCenterX();
    this.centerY = this.pZ.exactCenterY();
    this.pV = Math.max(this.qv, Math.max(this.pZ.width() / 2.0F, this.pZ.height() / 2.0F));
    invalidateSelf();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/framework/internal/featurehighlight/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */