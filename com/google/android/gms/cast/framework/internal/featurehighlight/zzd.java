package com.google.android.gms.cast.framework.internal.featurehighlight;

import android.content.res.Resources;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup.MarginLayoutParams;
import com.google.android.gms.R.dimen;
import com.google.android.gms.internal.zzagy;

class zzd
{
  private final int qA;
  private final int qB;
  private final int qC;
  private final zza qD;
  private final Rect qy = new Rect();
  private final int qz;
  
  zzd(zza paramzza)
  {
    this.qD = ((zza)zzagy.zzy(paramzza));
    paramzza = paramzza.getResources();
    this.qz = paramzza.getDimensionPixelSize(R.dimen.cast_libraries_material_featurehighlight_inner_radius);
    this.qA = paramzza.getDimensionPixelOffset(R.dimen.cast_libraries_material_featurehighlight_inner_margin);
    this.qB = paramzza.getDimensionPixelSize(R.dimen.cast_libraries_material_featurehighlight_text_max_width);
    this.qC = paramzza.getDimensionPixelSize(R.dimen.cast_libraries_material_featurehighlight_text_horizontal_offset);
  }
  
  private int zza(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    paramView = (ViewGroup.MarginLayoutParams)paramView.getLayoutParams();
    int j = paramInt3 / 2;
    int i;
    if (paramInt4 - paramInt1 <= paramInt2 - paramInt4)
    {
      i = 1;
      if (i == 0) {
        break label71;
      }
      paramInt4 = paramInt4 - j + this.qC;
      label45:
      if (paramInt4 - paramView.leftMargin >= paramInt1) {
        break label86;
      }
      paramInt1 += paramView.leftMargin;
    }
    label71:
    label86:
    do
    {
      return paramInt1;
      i = 0;
      break;
      paramInt4 = paramInt4 - j - this.qC;
      break label45;
      paramInt1 = paramInt4;
    } while (paramInt4 + paramInt3 + paramView.rightMargin <= paramInt2);
    return paramInt2 - paramInt3 - paramView.rightMargin;
  }
  
  private void zza(View paramView, int paramInt1, int paramInt2)
  {
    ViewGroup.MarginLayoutParams localMarginLayoutParams = (ViewGroup.MarginLayoutParams)paramView.getLayoutParams();
    paramView.measure(View.MeasureSpec.makeMeasureSpec(Math.min(paramInt1 - localMarginLayoutParams.leftMargin - localMarginLayoutParams.rightMargin, this.qB), 1073741824), View.MeasureSpec.makeMeasureSpec(paramInt2, Integer.MIN_VALUE));
  }
  
  private void zza(View paramView, Rect paramRect)
  {
    paramRect.set(paramView.getLeft(), paramView.getTop(), paramView.getRight(), paramView.getBottom());
  }
  
  private void zzb(View paramView, int paramInt1, int paramInt2)
  {
    paramView.layout(paramInt1, paramInt2, paramView.getMeasuredWidth() + paramInt1, paramView.getMeasuredHeight() + paramInt2);
  }
  
  private void zzc(View paramView, int paramInt1, int paramInt2)
  {
    paramView.layout(paramInt1, paramInt2 - paramView.getMeasuredHeight(), paramView.getMeasuredWidth() + paramInt1, paramInt2);
  }
  
  private int zzd(Rect paramRect)
  {
    Drawable localDrawable = this.qD.zzams();
    if (localDrawable != null) {}
    for (int i = localDrawable.getBounds().height();; i = paramRect.height()) {
      return Math.max(this.qz * 2, i);
    }
  }
  
  void zza(Rect paramRect1, Rect paramRect2)
  {
    int i = 0;
    View localView = this.qD.zzamt();
    if ((paramRect1.isEmpty()) || (paramRect2.isEmpty())) {
      localView.layout(0, 0, 0, 0);
    }
    for (;;)
    {
      zza(localView, this.qy);
      this.qD.zzamu().zzb(paramRect1, this.qy);
      this.qD.zzamv().zzc(paramRect1);
      return;
      int k = paramRect1.centerY();
      int j = paramRect1.centerX();
      if (k < paramRect2.centerY()) {
        i = 1;
      }
      int m = zzd(paramRect1);
      int n = m / 2;
      n = this.qA + (n + k);
      if (i != 0)
      {
        i = paramRect2.bottom;
        zza(localView, paramRect2.width(), i - n);
        zzb(localView, zza(localView, paramRect2.left, paramRect2.right, localView.getMeasuredWidth(), j), n);
      }
      else
      {
        i = k - m / 2 - this.qA;
        k = paramRect2.top;
        zza(localView, paramRect2.width(), i - k);
        zzc(localView, zza(localView, paramRect2.left, paramRect2.right, localView.getMeasuredWidth(), j), i);
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/framework/internal/featurehighlight/zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */