package com.google.android.gms.cast.framework.internal.featurehighlight;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorInt;
import android.support.v4.graphics.ColorUtils;
import android.util.TypedValue;
import com.google.android.gms.R.color;
import com.google.android.gms.R.dimen;
import com.google.android.gms.common.util.zzs;
import com.google.android.gms.internal.zzagt;
import com.google.android.gms.internal.zzagu;

class zze
  extends Drawable
{
  private float centerX;
  private float centerY;
  private final Paint pU = new Paint();
  private float pV;
  private final Rect pZ = new Rect();
  private final int qE;
  private final int qF;
  private final int qG;
  private float qH = 0.0F;
  private float qI = 0.0F;
  private int qJ = 244;
  private float qx = 1.0F;
  private final Rect qy = new Rect();
  
  public zze(Context paramContext)
  {
    if (zzs.zzayx()) {
      setColor(zzbg(paramContext));
    }
    for (;;)
    {
      this.pU.setAntiAlias(true);
      this.pU.setStyle(Paint.Style.FILL);
      paramContext = paramContext.getResources();
      this.qE = paramContext.getDimensionPixelSize(R.dimen.cast_libraries_material_featurehighlight_center_threshold);
      this.qF = paramContext.getDimensionPixelSize(R.dimen.cast_libraries_material_featurehighlight_center_horizontal_offset);
      this.qG = paramContext.getDimensionPixelSize(R.dimen.cast_libraries_material_featurehighlight_outer_padding);
      return;
      setColor(paramContext.getResources().getColor(R.color.cast_libraries_material_featurehighlight_outer_highlight_default_color));
    }
  }
  
  private float zza(float paramFloat1, float paramFloat2, Rect paramRect)
  {
    return (float)Math.ceil(zzagu.zza(paramFloat1, paramFloat2, paramRect.left, paramRect.top, paramRect.right, paramRect.bottom));
  }
  
  @TargetApi(21)
  private static int zzbg(Context paramContext)
  {
    TypedValue localTypedValue = new TypedValue();
    paramContext.getTheme().resolveAttribute(16843827, localTypedValue, true);
    return ColorUtils.setAlphaComponent(localTypedValue.data, 244);
  }
  
  public void draw(Canvas paramCanvas)
  {
    paramCanvas.drawCircle(this.centerX + 0.0F, this.centerY + 0.0F, this.pV * this.qx, this.pU);
  }
  
  public int getAlpha()
  {
    return this.pU.getAlpha();
  }
  
  public float getCenterX()
  {
    return this.centerX;
  }
  
  public float getCenterY()
  {
    return this.centerY;
  }
  
  @ColorInt
  public int getColor()
  {
    return this.pU.getColor();
  }
  
  public int getOpacity()
  {
    return -3;
  }
  
  public void setAlpha(int paramInt)
  {
    this.pU.setAlpha(paramInt);
    invalidateSelf();
  }
  
  public void setColor(@ColorInt int paramInt)
  {
    this.pU.setColor(paramInt);
    this.qJ = this.pU.getAlpha();
    invalidateSelf();
  }
  
  public void setColorFilter(ColorFilter paramColorFilter)
  {
    this.pU.setColorFilter(paramColorFilter);
    invalidateSelf();
  }
  
  public Animator zzamy()
  {
    ObjectAnimator localObjectAnimator = ObjectAnimator.ofPropertyValuesHolder(this, new PropertyValuesHolder[] { PropertyValuesHolder.ofFloat("scale", new float[] { 1.125F }), PropertyValuesHolder.ofInt("alpha", new int[] { 0 }) });
    localObjectAnimator.setInterpolator(zzagt.zzcnm());
    return localObjectAnimator.setDuration(200L);
  }
  
  public void zzb(Rect paramRect1, Rect paramRect2)
  {
    this.pZ.set(paramRect1);
    this.qy.set(paramRect2);
    float f1 = paramRect1.exactCenterX();
    float f2 = paramRect1.exactCenterY();
    Rect localRect = getBounds();
    if (Math.min(f2 - localRect.top, localRect.bottom - f2) < this.qE)
    {
      this.centerX = f1;
      this.centerY = f2;
      this.pV = (this.qG + Math.max(zza(this.centerX, this.centerY, paramRect1), zza(this.centerX, this.centerY, paramRect2)));
      invalidateSelf();
      return;
    }
    int i;
    if (f1 <= localRect.exactCenterX())
    {
      i = 1;
      label131:
      if (i == 0) {
        break label169;
      }
    }
    label169:
    for (f1 = paramRect2.exactCenterX() + this.qF;; f1 = paramRect2.exactCenterX() - this.qF)
    {
      this.centerX = f1;
      this.centerY = paramRect2.exactCenterY();
      break;
      i = 0;
      break label131;
    }
  }
  
  public boolean zzd(float paramFloat1, float paramFloat2)
  {
    return zzagu.zzb(paramFloat1, paramFloat2, this.centerX, this.centerY) < this.pV;
  }
  
  public Animator zze(float paramFloat1, float paramFloat2)
  {
    ObjectAnimator localObjectAnimator = ObjectAnimator.ofPropertyValuesHolder(this, new PropertyValuesHolder[] { PropertyValuesHolder.ofFloat("scale", new float[] { 0.0F, 1.0F }), PropertyValuesHolder.ofFloat("translationX", new float[] { paramFloat1, 0.0F }), PropertyValuesHolder.ofFloat("translationY", new float[] { paramFloat2, 0.0F }), PropertyValuesHolder.ofInt("alpha", new int[] { 0, this.qJ }) });
    localObjectAnimator.setInterpolator(zzagt.zzcnl());
    return localObjectAnimator.setDuration(350L);
  }
  
  public Animator zzf(float paramFloat1, float paramFloat2)
  {
    Object localObject = PropertyValuesHolder.ofFloat("scale", new float[] { 0.0F });
    PropertyValuesHolder localPropertyValuesHolder = PropertyValuesHolder.ofInt("alpha", new int[] { 0 });
    localObject = ObjectAnimator.ofPropertyValuesHolder(this, new PropertyValuesHolder[] { localObject, PropertyValuesHolder.ofFloat("translationX", new float[] { 0.0F, paramFloat1 }), PropertyValuesHolder.ofFloat("translationY", new float[] { 0.0F, paramFloat2 }), localPropertyValuesHolder });
    ((Animator)localObject).setInterpolator(zzagt.zzcnm());
    return ((Animator)localObject).setDuration(200L);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/framework/internal/featurehighlight/zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */