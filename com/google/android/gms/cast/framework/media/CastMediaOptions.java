package com.google.android.gms.cast.framework.media;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import com.google.android.gms.cast.internal.zzm;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.dynamic.zze;

public class CastMediaOptions
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<CastMediaOptions> CREATOR = new zza();
  private static final zzm oT = new zzm("CastMediaOptions");
  private final int mVersionCode;
  private final String qK;
  private final String qL;
  private final zzb qM;
  private final NotificationOptions qN;
  
  CastMediaOptions(int paramInt, String paramString1, String paramString2, IBinder paramIBinder, NotificationOptions paramNotificationOptions)
  {
    this.mVersionCode = paramInt;
    this.qK = paramString1;
    this.qL = paramString2;
    this.qM = zzb.zza.zzdc(paramIBinder);
    this.qN = paramNotificationOptions;
  }
  
  public String getExpandedControllerActivityClassName()
  {
    return this.qL;
  }
  
  public ImagePicker getImagePicker()
  {
    if (this.qM != null) {
      try
      {
        ImagePicker localImagePicker = (ImagePicker)zze.zzae(this.qM.zzanb());
        return localImagePicker;
      }
      catch (RemoteException localRemoteException)
      {
        oT.zzb(localRemoteException, "Unable to call %s on %s.", new Object[] { "getWrappedClientObject", zzb.class.getSimpleName() });
      }
    }
    return null;
  }
  
  public String getMediaIntentReceiverClassName()
  {
    return this.qK;
  }
  
  public NotificationOptions getNotificationOptions()
  {
    return this.qN;
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zza.zza(this, paramParcel, paramInt);
  }
  
  public IBinder zzana()
  {
    if (this.qM == null) {
      return null;
    }
    return this.qM.asBinder();
  }
  
  public static final class Builder
  {
    private String qK = MediaIntentReceiver.class.getName();
    private String qL;
    private NotificationOptions qN = new NotificationOptions.Builder().build();
    private ImagePicker qO;
    
    public CastMediaOptions build()
    {
      if (this.qO == null) {}
      for (IBinder localIBinder = null;; localIBinder = this.qO.zzanc().asBinder()) {
        return new CastMediaOptions(1, this.qK, this.qL, localIBinder, this.qN);
      }
    }
    
    public Builder setExpandedControllerActivityClassName(String paramString)
    {
      this.qL = paramString;
      return this;
    }
    
    public Builder setImagePicker(ImagePicker paramImagePicker)
    {
      this.qO = paramImagePicker;
      return this;
    }
    
    public Builder setMediaIntentReceiverClassName(String paramString)
    {
      this.qK = paramString;
      return this;
    }
    
    public Builder setNotificationOptions(NotificationOptions paramNotificationOptions)
    {
      this.qN = paramNotificationOptions;
      return this;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/framework/media/CastMediaOptions.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */