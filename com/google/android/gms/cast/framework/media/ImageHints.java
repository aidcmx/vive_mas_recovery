package com.google.android.gms.cast.framework.media;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public class ImageHints
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<ImageHints> CREATOR = new zzd();
  private final int mVersionCode;
  private final int nV;
  private final int qP;
  private final int qQ;
  
  public ImageHints(int paramInt1, int paramInt2, int paramInt3)
  {
    this(1, paramInt1, paramInt2, paramInt3);
  }
  
  ImageHints(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    this.mVersionCode = paramInt1;
    this.nV = paramInt2;
    this.qP = paramInt3;
    this.qQ = paramInt4;
  }
  
  public int getHeightInPixels()
  {
    return this.qQ;
  }
  
  public int getType()
  {
    return this.nV;
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int getWidthInPixels()
  {
    return this.qP;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzd.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/framework/media/ImageHints.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */