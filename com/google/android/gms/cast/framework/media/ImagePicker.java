package com.google.android.gms.cast.framework.media;

import android.support.annotation.NonNull;
import com.google.android.gms.cast.MediaMetadata;
import com.google.android.gms.common.images.WebImage;
import com.google.android.gms.dynamic.zzd;
import com.google.android.gms.dynamic.zze;
import java.util.List;

public class ImagePicker
{
  public static final int IMAGE_TYPE_EXPANDED_CONTROLLER_BACKGROUND = 4;
  public static final int IMAGE_TYPE_LOCK_SCREEN_BACKGROUND = 3;
  public static final int IMAGE_TYPE_MEDIA_ROUTE_CONTROLLER_DIALOG_BACKGROUND = 0;
  public static final int IMAGE_TYPE_MINI_CONTROLLER_THUMBNAIL = 2;
  public static final int IMAGE_TYPE_NOTIFICATION_THUMBNAIL = 1;
  public static final int IMAGE_TYPE_UNKNOWN = -1;
  private final zzb qR = new zza(null);
  
  @Deprecated
  public WebImage onPickImage(MediaMetadata paramMediaMetadata, int paramInt)
  {
    if ((paramMediaMetadata == null) || (!paramMediaMetadata.hasImages())) {
      return null;
    }
    return (WebImage)paramMediaMetadata.getImages().get(0);
  }
  
  public WebImage onPickImage(MediaMetadata paramMediaMetadata, @NonNull ImageHints paramImageHints)
  {
    return onPickImage(paramMediaMetadata, paramImageHints.getType());
  }
  
  public zzb zzanc()
  {
    return this.qR;
  }
  
  private class zza
    extends zzb.zza
  {
    private zza() {}
    
    public WebImage onPickImage(MediaMetadata paramMediaMetadata, int paramInt)
    {
      return ImagePicker.this.onPickImage(paramMediaMetadata, paramInt);
    }
    
    public WebImage zza(MediaMetadata paramMediaMetadata, ImageHints paramImageHints)
    {
      return ImagePicker.this.onPickImage(paramMediaMetadata, paramImageHints);
    }
    
    public int zzalm()
    {
      return 9877208;
    }
    
    public zzd zzanb()
    {
      return zze.zzac(ImagePicker.this);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/framework/media/ImagePicker.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */