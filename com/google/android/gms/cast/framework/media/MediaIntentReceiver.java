package com.google.android.gms.cast.framework.media;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.Session;
import com.google.android.gms.cast.framework.SessionManager;

public class MediaIntentReceiver
  extends BroadcastReceiver
{
  public static final String ACTION_DISCONNECT = "com.google.android.gms.cast.framework.action.DISCONNECT";
  public static final String ACTION_FORWARD = "com.google.android.gms.cast.framework.action.FORWARD";
  public static final String ACTION_REWIND = "com.google.android.gms.cast.framework.action.REWIND";
  public static final String ACTION_SKIP_NEXT = "com.google.android.gms.cast.framework.action.SKIP_NEXT";
  public static final String ACTION_SKIP_PREV = "com.google.android.gms.cast.framework.action.SKIP_PREV";
  public static final String ACTION_STOP_CASTING = "com.google.android.gms.cast.framework.action.STOP_CASTING";
  public static final String ACTION_TOGGLE_PLAYBACK = "com.google.android.gms.cast.framework.action.TOGGLE_PLAYBACK";
  public static final String EXTRA_SKIP_STEP_MS = "googlecast-extra_skip_step_ms";
  
  private void zza(CastSession paramCastSession, long paramLong)
  {
    if (paramLong == 0L) {}
    do
    {
      return;
      paramCastSession = zzh(paramCastSession);
    } while ((paramCastSession == null) || (paramCastSession.isLiveStream()) || (paramCastSession.isPlayingAd()));
    paramCastSession.seek(paramCastSession.getApproximateStreamPosition() + paramLong);
  }
  
  private void zzg(CastSession paramCastSession)
  {
    paramCastSession = zzh(paramCastSession);
    if (paramCastSession == null) {
      return;
    }
    paramCastSession.togglePlayback();
  }
  
  private RemoteMediaClient zzh(CastSession paramCastSession)
  {
    if ((paramCastSession == null) || (!paramCastSession.isConnected())) {
      return null;
    }
    return paramCastSession.getRemoteMediaClient();
  }
  
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    String str = paramIntent.getAction();
    if (str == null) {
      return;
    }
    paramContext = CastContext.getSharedInstance(paramContext).getSessionManager();
    int i = -1;
    switch (str.hashCode())
    {
    }
    for (;;)
    {
      switch (i)
      {
      default: 
        onReceiveOtherAction(str, paramIntent);
        return;
        if (str.equals("com.google.android.gms.cast.framework.action.TOGGLE_PLAYBACK"))
        {
          i = 0;
          continue;
          if (str.equals("com.google.android.gms.cast.framework.action.SKIP_NEXT"))
          {
            i = 1;
            continue;
            if (str.equals("com.google.android.gms.cast.framework.action.SKIP_PREV"))
            {
              i = 2;
              continue;
              if (str.equals("com.google.android.gms.cast.framework.action.FORWARD"))
              {
                i = 3;
                continue;
                if (str.equals("com.google.android.gms.cast.framework.action.REWIND"))
                {
                  i = 4;
                  continue;
                  if (str.equals("com.google.android.gms.cast.framework.action.STOP_CASTING"))
                  {
                    i = 5;
                    continue;
                    if (str.equals("com.google.android.gms.cast.framework.action.DISCONNECT"))
                    {
                      i = 6;
                      continue;
                      if (str.equals("android.intent.action.MEDIA_BUTTON")) {
                        i = 7;
                      }
                    }
                  }
                }
              }
            }
          }
        }
        break;
      }
    }
    onReceiveActionTogglePlayback(paramContext.getCurrentSession());
    return;
    onReceiveActionSkipNext(paramContext.getCurrentSession());
    return;
    onReceiveActionSkipPrev(paramContext.getCurrentSession());
    return;
    long l = paramIntent.getLongExtra("googlecast-extra_skip_step_ms", 0L);
    onReceiveActionForward(paramContext.getCurrentSession(), l);
    return;
    l = paramIntent.getLongExtra("googlecast-extra_skip_step_ms", 0L);
    onReceiveActionRewind(paramContext.getCurrentSession(), l);
    return;
    paramContext.endCurrentSession(true);
    return;
    paramContext.endCurrentSession(false);
    return;
    onReceiveActionMediaButton(paramContext.getCurrentSession(), paramIntent);
  }
  
  protected void onReceiveActionForward(Session paramSession, long paramLong)
  {
    if ((paramSession instanceof CastSession)) {
      zza((CastSession)paramSession, paramLong);
    }
  }
  
  protected void onReceiveActionMediaButton(Session paramSession, Intent paramIntent)
  {
    if ((!(paramSession instanceof CastSession)) || (!paramIntent.hasExtra("android.intent.extra.KEY_EVENT"))) {}
    do
    {
      return;
      paramIntent = (KeyEvent)paramIntent.getExtras().get("android.intent.extra.KEY_EVENT");
    } while ((paramIntent == null) || (paramIntent.getAction() != 0) || (paramIntent.getKeyCode() != 85));
    zzg((CastSession)paramSession);
  }
  
  protected void onReceiveActionRewind(Session paramSession, long paramLong)
  {
    if ((paramSession instanceof CastSession)) {
      zza((CastSession)paramSession, -paramLong);
    }
  }
  
  protected void onReceiveActionSkipNext(Session paramSession)
  {
    if ((paramSession instanceof CastSession))
    {
      paramSession = zzh((CastSession)paramSession);
      if ((paramSession != null) && (!paramSession.isPlayingAd())) {}
    }
    else
    {
      return;
    }
    paramSession.queueNext(null);
  }
  
  protected void onReceiveActionSkipPrev(Session paramSession)
  {
    if ((paramSession instanceof CastSession))
    {
      paramSession = zzh((CastSession)paramSession);
      if ((paramSession != null) && (!paramSession.isPlayingAd())) {}
    }
    else
    {
      return;
    }
    paramSession.queuePrev(null);
  }
  
  protected void onReceiveActionTogglePlayback(Session paramSession)
  {
    if ((paramSession instanceof CastSession)) {
      zzg((CastSession)paramSession);
    }
  }
  
  protected void onReceiveOtherAction(String paramString, Intent paramIntent) {}
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/framework/media/MediaIntentReceiver.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */