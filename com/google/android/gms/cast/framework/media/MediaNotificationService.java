package com.google.android.gms.cast.framework.media;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.CastOptions;
import com.google.android.gms.cast.internal.zzm;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.internal.zzog;

public class MediaNotificationService
  extends Service
{
  public static final String ACTION_UPDATE_NOTIFICATION = "com.google.android.gms.cast.framework.action.UPDATE_NOTIFICATION";
  private static final zzm oT = new zzm("MediaNotificationService");
  private zzc qT;
  
  public IBinder onBind(Intent paramIntent)
  {
    try
    {
      paramIntent = this.qT.onBind(paramIntent);
      return paramIntent;
    }
    catch (RemoteException paramIntent)
    {
      oT.zzb(paramIntent, "Unable to call %s on %s.", new Object[] { "onBind", zzc.class.getSimpleName() });
    }
    return null;
  }
  
  public void onCreate()
  {
    CastOptions localCastOptions = CastContext.getSharedInstance(this).getCastOptions();
    this.qT = zzog.zza(this, CastContext.getSharedInstance(this).zzalq(), zze.zzac(null), localCastOptions.getCastMediaOptions());
    try
    {
      this.qT.onCreate();
      super.onCreate();
      return;
    }
    catch (RemoteException localRemoteException)
    {
      for (;;)
      {
        oT.zzb(localRemoteException, "Unable to call %s on %s.", new Object[] { "onCreate", zzc.class.getSimpleName() });
      }
    }
  }
  
  public void onDestroy()
  {
    try
    {
      this.qT.onDestroy();
      super.onDestroy();
      return;
    }
    catch (RemoteException localRemoteException)
    {
      for (;;)
      {
        oT.zzb(localRemoteException, "Unable to call %s on %s.", new Object[] { "onDestroy", zzc.class.getSimpleName() });
      }
    }
  }
  
  public int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2)
  {
    try
    {
      paramInt1 = this.qT.onStartCommand(paramIntent, paramInt1, paramInt2);
      return paramInt1;
    }
    catch (RemoteException paramIntent)
    {
      oT.zzb(paramIntent, "Unable to call %s on %s.", new Object[] { "onStartCommand", zzc.class.getSimpleName() });
    }
    return 1;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/framework/media/MediaNotificationService.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */