package com.google.android.gms.cast.framework.media;

import android.net.Uri;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaMetadata;
import com.google.android.gms.common.images.WebImage;
import java.util.List;

public class MediaUtils
{
  public static Uri getImageUri(MediaInfo paramMediaInfo, int paramInt)
  {
    if (paramMediaInfo == null) {}
    do
    {
      return null;
      paramMediaInfo = paramMediaInfo.getMetadata();
    } while ((paramMediaInfo == null) || (paramMediaInfo.getImages() == null) || (paramMediaInfo.getImages().size() <= paramInt));
    return ((WebImage)paramMediaInfo.getImages().get(paramInt)).getUrl();
  }
  
  public static String getImageUrl(MediaInfo paramMediaInfo, int paramInt)
  {
    paramMediaInfo = getImageUri(paramMediaInfo, paramInt);
    if (paramMediaInfo == null) {
      return null;
    }
    return paramMediaInfo.toString();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/framework/media/MediaUtils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */