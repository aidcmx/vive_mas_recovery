package com.google.android.gms.cast.framework.media;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.R.dimen;
import com.google.android.gms.R.drawable;
import com.google.android.gms.R.string;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class NotificationOptions
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<NotificationOptions> CREATOR = new zze();
  public static final long SKIP_STEP_TEN_SECONDS_IN_MS = 10000L;
  public static final long SKIP_STEP_THIRTY_SECONDS_IN_MS = 30000L;
  private static final List<String> qU = Arrays.asList(new String[] { "com.google.android.gms.cast.framework.action.TOGGLE_PLAYBACK", "com.google.android.gms.cast.framework.action.STOP_CASTING" });
  private static final int[] qV = { 0, 1 };
  private final int mVersionCode;
  private final List<String> qW;
  private final int[] qX;
  private final long qY;
  private final String qZ;
  private final int rA;
  private final int ra;
  private final int rb;
  private final int rc;
  private final int rd;
  private final int re;
  private final int rf;
  private final int rg;
  private final int rh;
  private final int ri;
  private final int rj;
  private final int rk;
  private final int rl;
  private final int rm;
  private final int rn;
  private final int ro;
  private final int rp;
  private final int rq;
  private final int rr;
  private final int rs;
  private final int rt;
  private final int ru;
  private final int rv;
  private final int rw;
  private final int rx;
  private final int ry;
  private final int rz;
  
  public NotificationOptions(int paramInt1, List<String> paramList, int[] paramArrayOfInt, long paramLong, String paramString, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9, int paramInt10, int paramInt11, int paramInt12, int paramInt13, int paramInt14, int paramInt15, int paramInt16, int paramInt17, int paramInt18, int paramInt19, int paramInt20, int paramInt21, int paramInt22, int paramInt23, int paramInt24, int paramInt25, int paramInt26, int paramInt27, int paramInt28)
  {
    this.mVersionCode = paramInt1;
    if (paramList != null)
    {
      this.qW = new ArrayList(paramList);
      if (paramArrayOfInt == null) {
        break label222;
      }
    }
    label222:
    for (this.qX = Arrays.copyOf(paramArrayOfInt, paramArrayOfInt.length);; this.qX = null)
    {
      this.qY = paramLong;
      this.qZ = paramString;
      this.ra = paramInt2;
      this.rb = paramInt3;
      this.rc = paramInt4;
      this.rd = paramInt5;
      this.re = paramInt6;
      this.rf = paramInt7;
      this.rg = paramInt8;
      this.rh = paramInt9;
      this.ri = paramInt10;
      this.rj = paramInt11;
      this.rk = paramInt12;
      this.rl = paramInt13;
      this.rm = paramInt14;
      this.rn = paramInt15;
      this.ro = paramInt16;
      this.rp = paramInt17;
      this.rq = paramInt18;
      this.rr = paramInt19;
      this.rs = paramInt20;
      this.rt = paramInt21;
      this.ru = paramInt22;
      this.rv = paramInt23;
      this.rw = paramInt24;
      this.rx = paramInt25;
      this.ry = paramInt26;
      this.rz = paramInt27;
      this.rA = paramInt28;
      return;
      this.qW = null;
      break;
    }
  }
  
  public List<String> getActions()
  {
    return this.qW;
  }
  
  public int getCastingToDeviceStringResId()
  {
    return this.ro;
  }
  
  public int[] getCompatActionIndices()
  {
    return Arrays.copyOf(this.qX, this.qX.length);
  }
  
  public int getDisconnectDrawableResId()
  {
    return this.rm;
  }
  
  public int getForward10DrawableResId()
  {
    return this.rh;
  }
  
  public int getForward30DrawableResId()
  {
    return this.ri;
  }
  
  public int getForwardDrawableResId()
  {
    return this.rg;
  }
  
  public int getPauseDrawableResId()
  {
    return this.rc;
  }
  
  public int getPlayDrawableResId()
  {
    return this.rd;
  }
  
  public int getRewind10DrawableResId()
  {
    return this.rk;
  }
  
  public int getRewind30DrawableResId()
  {
    return this.rl;
  }
  
  public int getRewindDrawableResId()
  {
    return this.rj;
  }
  
  public int getSkipNextDrawableResId()
  {
    return this.re;
  }
  
  public int getSkipPrevDrawableResId()
  {
    return this.rf;
  }
  
  public long getSkipStepMs()
  {
    return this.qY;
  }
  
  public int getSmallIconDrawableResId()
  {
    return this.ra;
  }
  
  public int getStopLiveStreamDrawableResId()
  {
    return this.rb;
  }
  
  public int getStopLiveStreamTitleResId()
  {
    return this.rp;
  }
  
  public String getTargetActivityClassName()
  {
    return this.qZ;
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zze.zza(this, paramParcel, paramInt);
  }
  
  public int zzand()
  {
    return this.rn;
  }
  
  public int zzane()
  {
    return this.rq;
  }
  
  public int zzanf()
  {
    return this.rr;
  }
  
  public int zzang()
  {
    return this.rs;
  }
  
  public int zzanh()
  {
    return this.rt;
  }
  
  public int zzani()
  {
    return this.ru;
  }
  
  public int zzanj()
  {
    return this.rv;
  }
  
  public int zzank()
  {
    return this.rw;
  }
  
  public int zzanl()
  {
    return this.rx;
  }
  
  public int zzanm()
  {
    return this.ry;
  }
  
  public int zzann()
  {
    return this.rz;
  }
  
  public int zzano()
  {
    return this.rA;
  }
  
  public static final class Builder
  {
    private List<String> qW = NotificationOptions.zzanp();
    private int[] qX = NotificationOptions.zzanq();
    private long qY = 10000L;
    private String qZ;
    private int ra = R.drawable.cast_ic_notification_small_icon;
    private int rb = R.drawable.cast_ic_notification_stop_live_stream;
    private int rc = R.drawable.cast_ic_notification_pause;
    private int rd = R.drawable.cast_ic_notification_play;
    private int re = R.drawable.cast_ic_notification_skip_next;
    private int rf = R.drawable.cast_ic_notification_skip_prev;
    private int rg = R.drawable.cast_ic_notification_forward;
    private int rh = R.drawable.cast_ic_notification_forward10;
    private int ri = R.drawable.cast_ic_notification_forward30;
    private int rj = R.drawable.cast_ic_notification_rewind;
    private int rk = R.drawable.cast_ic_notification_rewind10;
    private int rl = R.drawable.cast_ic_notification_rewind30;
    private int rm = R.drawable.cast_ic_notification_disconnect;
    
    public NotificationOptions build()
    {
      return new NotificationOptions(1, this.qW, this.qX, this.qY, this.qZ, this.ra, this.rb, this.rc, this.rd, this.re, this.rf, this.rg, this.rh, this.ri, this.rj, this.rk, this.rl, this.rm, R.dimen.cast_notification_image_size, R.string.cast_casting_to_device, R.string.cast_stop_live_stream, R.string.cast_pause, R.string.cast_play, R.string.cast_skip_next, R.string.cast_skip_prev, R.string.cast_forward, R.string.cast_forward_10, R.string.cast_forward_30, R.string.cast_rewind, R.string.cast_rewind_10, R.string.cast_rewind_30, R.string.cast_disconnect);
    }
    
    public Builder setActions(List<String> paramList, int[] paramArrayOfInt)
    {
      if ((paramList == null) && (paramArrayOfInt != null)) {
        throw new IllegalArgumentException("When setting actions to null, you must also set compatActionIndices to null.");
      }
      if ((paramList != null) && (paramArrayOfInt == null)) {
        throw new IllegalArgumentException("When setting compatActionIndices to null, you must also set actions to null.");
      }
      if ((paramList != null) && (paramArrayOfInt != null))
      {
        int j = paramList.size();
        if (paramArrayOfInt.length > j) {
          throw new IllegalArgumentException(String.format(Locale.ROOT, "Invalid number of compat actions: %d > %d.", new Object[] { Integer.valueOf(paramArrayOfInt.length), Integer.valueOf(j) }));
        }
        int k = paramArrayOfInt.length;
        int i = 0;
        while (i < k)
        {
          int m = paramArrayOfInt[i];
          if ((m < 0) || (m >= j)) {
            throw new IllegalArgumentException(String.format(Locale.ROOT, "Index %d in compatActionIndices out of range: [0, %d]", new Object[] { Integer.valueOf(m), Integer.valueOf(j - 1) }));
          }
          i += 1;
        }
        this.qW = new ArrayList(paramList);
        this.qX = Arrays.copyOf(paramArrayOfInt, paramArrayOfInt.length);
        return this;
      }
      this.qW = NotificationOptions.zzanp();
      this.qX = NotificationOptions.zzanq();
      return this;
    }
    
    public Builder setDisconnectDrawableResId(int paramInt)
    {
      this.rm = paramInt;
      return this;
    }
    
    public Builder setForward10DrawableResId(int paramInt)
    {
      this.rh = paramInt;
      return this;
    }
    
    public Builder setForward30DrawableResId(int paramInt)
    {
      this.ri = paramInt;
      return this;
    }
    
    public Builder setForwardDrawableResId(int paramInt)
    {
      this.rg = paramInt;
      return this;
    }
    
    public Builder setPauseDrawableResId(int paramInt)
    {
      this.rc = paramInt;
      return this;
    }
    
    public Builder setPlayDrawableResId(int paramInt)
    {
      this.rd = paramInt;
      return this;
    }
    
    public Builder setRewind10DrawableResId(int paramInt)
    {
      this.rk = paramInt;
      return this;
    }
    
    public Builder setRewind30DrawableResId(int paramInt)
    {
      this.rl = paramInt;
      return this;
    }
    
    public Builder setRewindDrawableResId(int paramInt)
    {
      this.rj = paramInt;
      return this;
    }
    
    public Builder setSkipNextDrawableResId(int paramInt)
    {
      this.re = paramInt;
      return this;
    }
    
    public Builder setSkipPrevDrawableResId(int paramInt)
    {
      this.rf = paramInt;
      return this;
    }
    
    public Builder setSkipStepMs(long paramLong)
    {
      if (paramLong > 0L) {}
      for (boolean bool = true;; bool = false)
      {
        zzaa.zzb(bool, "skipStepMs must be positive.");
        this.qY = paramLong;
        return this;
      }
    }
    
    public Builder setSmallIconDrawableResId(int paramInt)
    {
      this.ra = paramInt;
      return this;
    }
    
    public Builder setStopLiveStreamDrawableResId(int paramInt)
    {
      this.rb = paramInt;
      return this;
    }
    
    public Builder setTargetActivityClassName(String paramString)
    {
      this.qZ = paramString;
      return this;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/framework/media/NotificationOptions.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */