package com.google.android.gms.cast.framework.media;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import com.google.android.gms.cast.AdBreakInfo;
import com.google.android.gms.cast.Cast.CastApi;
import com.google.android.gms.cast.Cast.MessageReceivedCallback;
import com.google.android.gms.cast.CastDevice;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaQueueItem;
import com.google.android.gms.cast.MediaStatus;
import com.google.android.gms.cast.TextTrackStyle;
import com.google.android.gms.cast.internal.zzb;
import com.google.android.gms.cast.internal.zze;
import com.google.android.gms.cast.internal.zzn;
import com.google.android.gms.cast.internal.zzn.zza;
import com.google.android.gms.cast.internal.zzn.zzb;
import com.google.android.gms.cast.internal.zzo;
import com.google.android.gms.cast.internal.zzp;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzaa;
import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import org.json.JSONObject;

public class RemoteMediaClient
  implements Cast.MessageReceivedCallback
{
  public static final String NAMESPACE = zzn.NAMESPACE;
  public static final int RESUME_STATE_PAUSE = 2;
  public static final int RESUME_STATE_PLAY = 1;
  public static final int RESUME_STATE_UNCHANGED = 0;
  public static final int STATUS_FAILED = 2100;
  public static final int STATUS_REPLACED = 2103;
  public static final int STATUS_SUCCEEDED = 0;
  private final Handler mHandler = new Handler(Looper.getMainLooper());
  private final List<Listener> mListeners = new CopyOnWriteArrayList();
  private final zzn nY;
  private final Cast.CastApi pk;
  private final zza rB = new zza();
  private GoogleApiClient rC;
  private final Map<ProgressListener, zzd> rD = new ConcurrentHashMap();
  private final Map<Long, zzd> rE = new ConcurrentHashMap();
  private ParseAdsInfoCallback rF;
  private final Object zzako = new Object();
  
  public RemoteMediaClient(@NonNull zzn paramzzn, @NonNull Cast.CastApi paramCastApi)
  {
    this.pk = paramCastApi;
    this.nY = ((zzn)zzaa.zzy(paramzzn));
    this.nY.zza(new zzn.zza()
    {
      private void zzant()
      {
        if (RemoteMediaClient.zzc(RemoteMediaClient.this) != null)
        {
          Object localObject = RemoteMediaClient.this.getMediaStatus();
          if (localObject != null)
          {
            ((MediaStatus)localObject).zzbl(RemoteMediaClient.zzc(RemoteMediaClient.this).parseIsPlayingAdFromMediaStatus((MediaStatus)localObject));
            localObject = RemoteMediaClient.zzc(RemoteMediaClient.this).parseAdBreaksFromMediaStatus((MediaStatus)localObject);
            MediaInfo localMediaInfo = RemoteMediaClient.this.getMediaInfo();
            if (localMediaInfo != null) {
              localMediaInfo.zzy((List)localObject);
            }
          }
        }
      }
      
      public void onMetadataUpdated()
      {
        zzant();
        Iterator localIterator = RemoteMediaClient.zzb(RemoteMediaClient.this).iterator();
        while (localIterator.hasNext()) {
          ((RemoteMediaClient.Listener)localIterator.next()).onMetadataUpdated();
        }
      }
      
      public void onPreloadStatusUpdated()
      {
        Iterator localIterator = RemoteMediaClient.zzb(RemoteMediaClient.this).iterator();
        while (localIterator.hasNext()) {
          ((RemoteMediaClient.Listener)localIterator.next()).onPreloadStatusUpdated();
        }
      }
      
      public void onQueueStatusUpdated()
      {
        Iterator localIterator = RemoteMediaClient.zzb(RemoteMediaClient.this).iterator();
        while (localIterator.hasNext()) {
          ((RemoteMediaClient.Listener)localIterator.next()).onQueueStatusUpdated();
        }
      }
      
      public void onStatusUpdated()
      {
        zzant();
        RemoteMediaClient.zza(RemoteMediaClient.this);
        Iterator localIterator = RemoteMediaClient.zzb(RemoteMediaClient.this).iterator();
        while (localIterator.hasNext()) {
          ((RemoteMediaClient.Listener)localIterator.next()).onStatusUpdated();
        }
      }
    });
    this.nY.zza(this.rB);
  }
  
  private zzb zza(zzb paramzzb)
  {
    try
    {
      this.rC.zzb(paramzzb);
      return paramzzb;
    }
    catch (IllegalStateException localIllegalStateException)
    {
      localIllegalStateException = localIllegalStateException;
      paramzzb.zzc((MediaChannelResult)paramzzb.zzc(new Status(2100)));
      return paramzzb;
    }
    finally {}
    return paramzzb;
  }
  
  private void zza(Set<ProgressListener> paramSet)
  {
    if ((isBuffering()) || (isPaused())) {}
    for (;;)
    {
      return;
      Object localObject = new HashSet(paramSet);
      if (isPlaying())
      {
        paramSet = ((Set)localObject).iterator();
        while (paramSet.hasNext()) {
          ((ProgressListener)paramSet.next()).onProgressUpdated(getApproximateStreamPosition(), getStreamDuration());
        }
      }
      else if (isLoadingNextItem())
      {
        paramSet = getLoadingItem();
        if ((paramSet != null) && (paramSet.getMedia() != null))
        {
          localObject = ((Set)localObject).iterator();
          while (((Iterator)localObject).hasNext()) {
            ((ProgressListener)((Iterator)localObject).next()).onProgressUpdated(0L, paramSet.getMedia().getStreamDuration());
          }
        }
      }
      else
      {
        paramSet = ((Set)localObject).iterator();
        while (paramSet.hasNext()) {
          ((ProgressListener)paramSet.next()).onProgressUpdated(0L, 0L);
        }
      }
    }
  }
  
  private void zzanr()
    throws IllegalStateException
  {
    if (this.rC == null) {
      throw new IllegalStateException("No connection");
    }
  }
  
  private void zzans()
  {
    Iterator localIterator = this.rE.values().iterator();
    label110:
    while (localIterator.hasNext())
    {
      zzd localzzd = (zzd)localIterator.next();
      if ((hasMediaSession()) && (!localzzd.isStarted())) {
        localzzd.start();
      }
      for (;;)
      {
        if ((!localzzd.isStarted()) || ((!isBuffering()) && (!isPaused()) && (!isLoadingNextItem()))) {
          break label110;
        }
        zza(zzd.zza(localzzd));
        break;
        if ((!hasMediaSession()) && (localzzd.isStarted())) {
          localzzd.stop();
        }
      }
    }
  }
  
  private int zzep(int paramInt)
  {
    MediaStatus localMediaStatus = getMediaStatus();
    int i = 0;
    while (i < localMediaStatus.getQueueItemCount())
    {
      if (localMediaStatus.getQueueItem(i).getItemId() == paramInt) {
        return i;
      }
      i += 1;
    }
    return -1;
  }
  
  public void addListener(Listener paramListener)
  {
    if (paramListener != null) {
      this.mListeners.add(paramListener);
    }
  }
  
  public boolean addProgressListener(ProgressListener paramProgressListener, long paramLong)
  {
    if ((paramProgressListener == null) || (this.rD.containsKey(paramProgressListener))) {
      return false;
    }
    zzd localzzd2 = (zzd)this.rE.get(Long.valueOf(paramLong));
    zzd localzzd1 = localzzd2;
    if (localzzd2 == null)
    {
      localzzd1 = new zzd(paramLong);
      this.rE.put(Long.valueOf(paramLong), localzzd1);
    }
    localzzd1.zza(paramProgressListener);
    this.rD.put(paramProgressListener, localzzd1);
    if (hasMediaSession()) {
      localzzd1.start();
    }
    return true;
  }
  
  public long getApproximateStreamPosition()
  {
    synchronized (this.zzako)
    {
      long l = this.nY.getApproximateStreamPosition();
      return l;
    }
  }
  
  public MediaQueueItem getCurrentItem()
  {
    MediaStatus localMediaStatus = getMediaStatus();
    if (localMediaStatus == null) {
      return null;
    }
    return localMediaStatus.getQueueItemById(localMediaStatus.getCurrentItemId());
  }
  
  public int getIdleReason()
  {
    for (;;)
    {
      synchronized (this.zzako)
      {
        MediaStatus localMediaStatus = getMediaStatus();
        if (localMediaStatus != null)
        {
          i = localMediaStatus.getIdleReason();
          return i;
        }
      }
      int i = 0;
    }
  }
  
  public MediaQueueItem getLoadingItem()
  {
    MediaStatus localMediaStatus = getMediaStatus();
    if (localMediaStatus == null) {
      return null;
    }
    return localMediaStatus.getQueueItemById(localMediaStatus.getLoadingItemId());
  }
  
  public MediaInfo getMediaInfo()
  {
    synchronized (this.zzako)
    {
      MediaInfo localMediaInfo = this.nY.getMediaInfo();
      return localMediaInfo;
    }
  }
  
  public MediaStatus getMediaStatus()
  {
    synchronized (this.zzako)
    {
      MediaStatus localMediaStatus = this.nY.getMediaStatus();
      return localMediaStatus;
    }
  }
  
  public String getNamespace()
  {
    return this.nY.getNamespace();
  }
  
  public int getPlayerState()
  {
    for (;;)
    {
      synchronized (this.zzako)
      {
        MediaStatus localMediaStatus = getMediaStatus();
        if (localMediaStatus != null)
        {
          i = localMediaStatus.getPlayerState();
          return i;
        }
      }
      int i = 1;
    }
  }
  
  public MediaQueueItem getPreloadedItem()
  {
    MediaStatus localMediaStatus = getMediaStatus();
    if (localMediaStatus == null) {
      return null;
    }
    return localMediaStatus.getQueueItemById(localMediaStatus.getPreloadedItemId());
  }
  
  public long getStreamDuration()
  {
    synchronized (this.zzako)
    {
      long l = this.nY.getStreamDuration();
      return l;
    }
  }
  
  public boolean hasMediaSession()
  {
    return (isBuffering()) || (isPlaying()) || (isPaused()) || (isLoadingNextItem());
  }
  
  public boolean isBuffering()
  {
    MediaStatus localMediaStatus = getMediaStatus();
    return (localMediaStatus != null) && (localMediaStatus.getPlayerState() == 4);
  }
  
  public boolean isLiveStream()
  {
    MediaInfo localMediaInfo = getMediaInfo();
    return (localMediaInfo != null) && (localMediaInfo.getStreamType() == 2);
  }
  
  public boolean isLoadingNextItem()
  {
    MediaStatus localMediaStatus = getMediaStatus();
    return (localMediaStatus != null) && (localMediaStatus.getLoadingItemId() != 0);
  }
  
  public boolean isPaused()
  {
    MediaStatus localMediaStatus = getMediaStatus();
    return (localMediaStatus != null) && ((localMediaStatus.getPlayerState() == 3) || ((isLiveStream()) && (getIdleReason() == 2)));
  }
  
  public boolean isPlaying()
  {
    MediaStatus localMediaStatus = getMediaStatus();
    return (localMediaStatus != null) && (localMediaStatus.getPlayerState() == 2);
  }
  
  public boolean isPlayingAd()
  {
    MediaStatus localMediaStatus = getMediaStatus();
    return (localMediaStatus != null) && (localMediaStatus.isPlayingAd());
  }
  
  public PendingResult<MediaChannelResult> load(MediaInfo paramMediaInfo)
  {
    return load(paramMediaInfo, true, 0L, null, null);
  }
  
  public PendingResult<MediaChannelResult> load(MediaInfo paramMediaInfo, boolean paramBoolean)
  {
    return load(paramMediaInfo, paramBoolean, 0L, null, null);
  }
  
  public PendingResult<MediaChannelResult> load(MediaInfo paramMediaInfo, boolean paramBoolean, long paramLong)
  {
    return load(paramMediaInfo, paramBoolean, paramLong, null, null);
  }
  
  public PendingResult<MediaChannelResult> load(MediaInfo paramMediaInfo, boolean paramBoolean, long paramLong, JSONObject paramJSONObject)
  {
    return load(paramMediaInfo, paramBoolean, paramLong, null, paramJSONObject);
  }
  
  public PendingResult<MediaChannelResult> load(final MediaInfo paramMediaInfo, final boolean paramBoolean, final long paramLong, long[] paramArrayOfLong, final JSONObject paramJSONObject)
  {
    zzanr();
    zza(new zzb(this.rC)
    {
      /* Error */
      protected void zza(zze arg1)
      {
        // Byte code:
        //   0: aload_0
        //   1: getfield 25	com/google/android/gms/cast/framework/media/RemoteMediaClient$12:rG	Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;
        //   4: invokestatic 49	com/google/android/gms/cast/framework/media/RemoteMediaClient:zzd	(Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;)Ljava/lang/Object;
        //   7: astore_1
        //   8: aload_1
        //   9: monitorenter
        //   10: aload_0
        //   11: getfield 25	com/google/android/gms/cast/framework/media/RemoteMediaClient$12:rG	Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;
        //   14: invokestatic 53	com/google/android/gms/cast/framework/media/RemoteMediaClient:zze	(Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;)Lcom/google/android/gms/cast/internal/zzn;
        //   17: aload_0
        //   18: getfield 57	com/google/android/gms/cast/framework/media/RemoteMediaClient$12:oG	Lcom/google/android/gms/cast/internal/zzp;
        //   21: aload_0
        //   22: getfield 27	com/google/android/gms/cast/framework/media/RemoteMediaClient$12:ot	Lcom/google/android/gms/cast/MediaInfo;
        //   25: aload_0
        //   26: getfield 29	com/google/android/gms/cast/framework/media/RemoteMediaClient$12:ou	Z
        //   29: aload_0
        //   30: getfield 31	com/google/android/gms/cast/framework/media/RemoteMediaClient$12:ol	J
        //   33: aload_0
        //   34: getfield 33	com/google/android/gms/cast/framework/media/RemoteMediaClient$12:ov	[J
        //   37: aload_0
        //   38: getfield 35	com/google/android/gms/cast/framework/media/RemoteMediaClient$12:om	Lorg/json/JSONObject;
        //   41: invokevirtual 62	com/google/android/gms/cast/internal/zzn:zza	(Lcom/google/android/gms/cast/internal/zzp;Lcom/google/android/gms/cast/MediaInfo;ZJ[JLorg/json/JSONObject;)J
        //   44: pop2
        //   45: aload_1
        //   46: monitorexit
        //   47: return
        //   48: aload_0
        //   49: aload_0
        //   50: new 64	com/google/android/gms/common/api/Status
        //   53: dup
        //   54: sipush 2100
        //   57: invokespecial 67	com/google/android/gms/common/api/Status:<init>	(I)V
        //   60: invokevirtual 71	com/google/android/gms/cast/framework/media/RemoteMediaClient$zzb:zzc	(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Result;
        //   63: checkcast 73	com/google/android/gms/cast/framework/media/RemoteMediaClient$MediaChannelResult
        //   66: invokevirtual 76	com/google/android/gms/cast/framework/media/RemoteMediaClient$12:zzc	(Lcom/google/android/gms/common/api/Result;)V
        //   69: goto -24 -> 45
        //   72: astore_2
        //   73: aload_1
        //   74: monitorexit
        //   75: aload_2
        //   76: athrow
        //   77: astore_2
        //   78: goto -30 -> 48
        //   81: astore_2
        //   82: goto -34 -> 48
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	85	0	this	12
        //   72	4	2	localObject	Object
        //   77	1	2	localIOException	IOException
        //   81	1	2	localIllegalStateException	IllegalStateException
        // Exception table:
        //   from	to	target	type
        //   10	45	72	finally
        //   45	47	72	finally
        //   48	69	72	finally
        //   73	75	72	finally
        //   10	45	77	java/io/IOException
        //   10	45	81	java/lang/IllegalStateException
      }
    });
  }
  
  public void onMessageReceived(CastDevice paramCastDevice, String paramString1, String paramString2)
  {
    this.nY.zzgt(paramString2);
  }
  
  public PendingResult<MediaChannelResult> pause()
  {
    return pause(null);
  }
  
  public PendingResult<MediaChannelResult> pause(final JSONObject paramJSONObject)
  {
    zzanr();
    zza(new zzb(this.rC)
    {
      /* Error */
      protected void zza(zze arg1)
      {
        // Byte code:
        //   0: aload_0
        //   1: getfield 17	com/google/android/gms/cast/framework/media/RemoteMediaClient$17:rG	Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;
        //   4: invokestatic 33	com/google/android/gms/cast/framework/media/RemoteMediaClient:zzd	(Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;)Ljava/lang/Object;
        //   7: astore_1
        //   8: aload_1
        //   9: monitorenter
        //   10: aload_0
        //   11: getfield 17	com/google/android/gms/cast/framework/media/RemoteMediaClient$17:rG	Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;
        //   14: invokestatic 37	com/google/android/gms/cast/framework/media/RemoteMediaClient:zze	(Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;)Lcom/google/android/gms/cast/internal/zzn;
        //   17: aload_0
        //   18: getfield 41	com/google/android/gms/cast/framework/media/RemoteMediaClient$17:oG	Lcom/google/android/gms/cast/internal/zzp;
        //   21: aload_0
        //   22: getfield 19	com/google/android/gms/cast/framework/media/RemoteMediaClient$17:om	Lorg/json/JSONObject;
        //   25: invokevirtual 46	com/google/android/gms/cast/internal/zzn:zza	(Lcom/google/android/gms/cast/internal/zzp;Lorg/json/JSONObject;)J
        //   28: pop2
        //   29: aload_1
        //   30: monitorexit
        //   31: return
        //   32: aload_0
        //   33: aload_0
        //   34: new 48	com/google/android/gms/common/api/Status
        //   37: dup
        //   38: sipush 2100
        //   41: invokespecial 51	com/google/android/gms/common/api/Status:<init>	(I)V
        //   44: invokevirtual 55	com/google/android/gms/cast/framework/media/RemoteMediaClient$zzb:zzc	(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Result;
        //   47: checkcast 57	com/google/android/gms/cast/framework/media/RemoteMediaClient$MediaChannelResult
        //   50: invokevirtual 60	com/google/android/gms/cast/framework/media/RemoteMediaClient$17:zzc	(Lcom/google/android/gms/common/api/Result;)V
        //   53: goto -24 -> 29
        //   56: astore_2
        //   57: aload_1
        //   58: monitorexit
        //   59: aload_2
        //   60: athrow
        //   61: astore_2
        //   62: goto -30 -> 32
        //   65: astore_2
        //   66: goto -34 -> 32
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	69	0	this	17
        //   56	4	2	localObject	Object
        //   61	1	2	localIOException	IOException
        //   65	1	2	localzzb	zzn.zzb
        // Exception table:
        //   from	to	target	type
        //   10	29	56	finally
        //   29	31	56	finally
        //   32	53	56	finally
        //   57	59	56	finally
        //   10	29	61	java/io/IOException
        //   10	29	65	com/google/android/gms/cast/internal/zzn$zzb
      }
    });
  }
  
  public PendingResult<MediaChannelResult> play()
  {
    return play(null);
  }
  
  public PendingResult<MediaChannelResult> play(final JSONObject paramJSONObject)
  {
    zzanr();
    zza(new zzb(this.rC)
    {
      /* Error */
      protected void zza(zze arg1)
      {
        // Byte code:
        //   0: aload_0
        //   1: getfield 17	com/google/android/gms/cast/framework/media/RemoteMediaClient$19:rG	Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;
        //   4: invokestatic 33	com/google/android/gms/cast/framework/media/RemoteMediaClient:zzd	(Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;)Ljava/lang/Object;
        //   7: astore_1
        //   8: aload_1
        //   9: monitorenter
        //   10: aload_0
        //   11: getfield 17	com/google/android/gms/cast/framework/media/RemoteMediaClient$19:rG	Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;
        //   14: invokestatic 37	com/google/android/gms/cast/framework/media/RemoteMediaClient:zze	(Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;)Lcom/google/android/gms/cast/internal/zzn;
        //   17: aload_0
        //   18: getfield 41	com/google/android/gms/cast/framework/media/RemoteMediaClient$19:oG	Lcom/google/android/gms/cast/internal/zzp;
        //   21: aload_0
        //   22: getfield 19	com/google/android/gms/cast/framework/media/RemoteMediaClient$19:om	Lorg/json/JSONObject;
        //   25: invokevirtual 47	com/google/android/gms/cast/internal/zzn:zzc	(Lcom/google/android/gms/cast/internal/zzp;Lorg/json/JSONObject;)J
        //   28: pop2
        //   29: aload_1
        //   30: monitorexit
        //   31: return
        //   32: aload_0
        //   33: aload_0
        //   34: new 49	com/google/android/gms/common/api/Status
        //   37: dup
        //   38: sipush 2100
        //   41: invokespecial 52	com/google/android/gms/common/api/Status:<init>	(I)V
        //   44: invokevirtual 55	com/google/android/gms/cast/framework/media/RemoteMediaClient$zzb:zzc	(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Result;
        //   47: checkcast 57	com/google/android/gms/cast/framework/media/RemoteMediaClient$MediaChannelResult
        //   50: invokevirtual 60	com/google/android/gms/cast/framework/media/RemoteMediaClient$19:zzc	(Lcom/google/android/gms/common/api/Result;)V
        //   53: goto -24 -> 29
        //   56: astore_2
        //   57: aload_1
        //   58: monitorexit
        //   59: aload_2
        //   60: athrow
        //   61: astore_2
        //   62: goto -30 -> 32
        //   65: astore_2
        //   66: goto -34 -> 32
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	69	0	this	19
        //   56	4	2	localObject	Object
        //   61	1	2	localIOException	IOException
        //   65	1	2	localzzb	zzn.zzb
        // Exception table:
        //   from	to	target	type
        //   10	29	56	finally
        //   29	31	56	finally
        //   32	53	56	finally
        //   57	59	56	finally
        //   10	29	61	java/io/IOException
        //   10	29	65	com/google/android/gms/cast/internal/zzn$zzb
      }
    });
  }
  
  public PendingResult<MediaChannelResult> queueAppendItem(MediaQueueItem paramMediaQueueItem, JSONObject paramJSONObject)
    throws IllegalArgumentException
  {
    return queueInsertItems(new MediaQueueItem[] { paramMediaQueueItem }, 0, paramJSONObject);
  }
  
  public PendingResult<MediaChannelResult> queueInsertAndPlayItem(final MediaQueueItem paramMediaQueueItem, final int paramInt, final long paramLong, JSONObject paramJSONObject)
  {
    zzanr();
    zza(new zzb(this.rC)
    {
      /* Error */
      protected void zza(zze arg1)
      {
        // Byte code:
        //   0: aload_0
        //   1: getfield 23	com/google/android/gms/cast/framework/media/RemoteMediaClient$6:rG	Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;
        //   4: invokestatic 45	com/google/android/gms/cast/framework/media/RemoteMediaClient:zzd	(Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;)Ljava/lang/Object;
        //   7: astore_1
        //   8: aload_1
        //   9: monitorenter
        //   10: aload_0
        //   11: getfield 23	com/google/android/gms/cast/framework/media/RemoteMediaClient$6:rG	Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;
        //   14: invokestatic 49	com/google/android/gms/cast/framework/media/RemoteMediaClient:zze	(Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;)Lcom/google/android/gms/cast/internal/zzn;
        //   17: astore 5
        //   19: aload_0
        //   20: getfield 53	com/google/android/gms/cast/framework/media/RemoteMediaClient$6:oG	Lcom/google/android/gms/cast/internal/zzp;
        //   23: astore 6
        //   25: aload_0
        //   26: getfield 25	com/google/android/gms/cast/framework/media/RemoteMediaClient$6:op	Lcom/google/android/gms/cast/MediaQueueItem;
        //   29: astore 7
        //   31: aload_0
        //   32: getfield 27	com/google/android/gms/cast/framework/media/RemoteMediaClient$6:oo	I
        //   35: istore_2
        //   36: aload_0
        //   37: getfield 29	com/google/android/gms/cast/framework/media/RemoteMediaClient$6:ol	J
        //   40: lstore_3
        //   41: aload_0
        //   42: getfield 31	com/google/android/gms/cast/framework/media/RemoteMediaClient$6:om	Lorg/json/JSONObject;
        //   45: astore 8
        //   47: aload 5
        //   49: aload 6
        //   51: iconst_1
        //   52: anewarray 55	com/google/android/gms/cast/MediaQueueItem
        //   55: dup
        //   56: iconst_0
        //   57: aload 7
        //   59: aastore
        //   60: iload_2
        //   61: iconst_0
        //   62: iconst_0
        //   63: lload_3
        //   64: aload 8
        //   66: invokevirtual 60	com/google/android/gms/cast/internal/zzn:zza	(Lcom/google/android/gms/cast/internal/zzp;[Lcom/google/android/gms/cast/MediaQueueItem;IIIJLorg/json/JSONObject;)J
        //   69: pop2
        //   70: aload_1
        //   71: monitorexit
        //   72: return
        //   73: aload_0
        //   74: aload_0
        //   75: new 62	com/google/android/gms/common/api/Status
        //   78: dup
        //   79: sipush 2100
        //   82: invokespecial 65	com/google/android/gms/common/api/Status:<init>	(I)V
        //   85: invokevirtual 69	com/google/android/gms/cast/framework/media/RemoteMediaClient$zzb:zzc	(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Result;
        //   88: checkcast 71	com/google/android/gms/cast/framework/media/RemoteMediaClient$MediaChannelResult
        //   91: invokevirtual 74	com/google/android/gms/cast/framework/media/RemoteMediaClient$6:zzc	(Lcom/google/android/gms/common/api/Result;)V
        //   94: goto -24 -> 70
        //   97: astore 5
        //   99: aload_1
        //   100: monitorexit
        //   101: aload 5
        //   103: athrow
        //   104: astore 5
        //   106: goto -33 -> 73
        //   109: astore 5
        //   111: goto -38 -> 73
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	114	0	this	6
        //   35	26	2	i	int
        //   40	24	3	l	long
        //   17	31	5	localzzn	zzn
        //   97	5	5	localObject	Object
        //   104	1	5	localIOException	IOException
        //   109	1	5	localzzb	zzn.zzb
        //   23	27	6	localzzp	zzp
        //   29	29	7	localMediaQueueItem	MediaQueueItem
        //   45	20	8	localJSONObject	JSONObject
        // Exception table:
        //   from	to	target	type
        //   10	70	97	finally
        //   70	72	97	finally
        //   73	94	97	finally
        //   99	101	97	finally
        //   10	70	104	java/io/IOException
        //   10	70	109	com/google/android/gms/cast/internal/zzn$zzb
      }
    });
  }
  
  public PendingResult<MediaChannelResult> queueInsertAndPlayItem(MediaQueueItem paramMediaQueueItem, int paramInt, JSONObject paramJSONObject)
  {
    return queueInsertAndPlayItem(paramMediaQueueItem, paramInt, -1L, paramJSONObject);
  }
  
  public PendingResult<MediaChannelResult> queueInsertItems(final MediaQueueItem[] paramArrayOfMediaQueueItem, final int paramInt, final JSONObject paramJSONObject)
    throws IllegalArgumentException
  {
    zzanr();
    zza(new zzb(this.rC)
    {
      /* Error */
      protected void zza(zze arg1)
      {
        // Byte code:
        //   0: aload_0
        //   1: getfield 21	com/google/android/gms/cast/framework/media/RemoteMediaClient$5:rG	Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;
        //   4: invokestatic 41	com/google/android/gms/cast/framework/media/RemoteMediaClient:zzd	(Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;)Ljava/lang/Object;
        //   7: astore_1
        //   8: aload_1
        //   9: monitorenter
        //   10: aload_0
        //   11: getfield 21	com/google/android/gms/cast/framework/media/RemoteMediaClient$5:rG	Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;
        //   14: invokestatic 45	com/google/android/gms/cast/framework/media/RemoteMediaClient:zze	(Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;)Lcom/google/android/gms/cast/internal/zzn;
        //   17: aload_0
        //   18: getfield 49	com/google/android/gms/cast/framework/media/RemoteMediaClient$5:oG	Lcom/google/android/gms/cast/internal/zzp;
        //   21: aload_0
        //   22: getfield 23	com/google/android/gms/cast/framework/media/RemoteMediaClient$5:on	[Lcom/google/android/gms/cast/MediaQueueItem;
        //   25: aload_0
        //   26: getfield 25	com/google/android/gms/cast/framework/media/RemoteMediaClient$5:oo	I
        //   29: iconst_0
        //   30: iconst_m1
        //   31: ldc2_w 50
        //   34: aload_0
        //   35: getfield 27	com/google/android/gms/cast/framework/media/RemoteMediaClient$5:om	Lorg/json/JSONObject;
        //   38: invokevirtual 56	com/google/android/gms/cast/internal/zzn:zza	(Lcom/google/android/gms/cast/internal/zzp;[Lcom/google/android/gms/cast/MediaQueueItem;IIIJLorg/json/JSONObject;)J
        //   41: pop2
        //   42: aload_1
        //   43: monitorexit
        //   44: return
        //   45: aload_0
        //   46: aload_0
        //   47: new 58	com/google/android/gms/common/api/Status
        //   50: dup
        //   51: sipush 2100
        //   54: invokespecial 61	com/google/android/gms/common/api/Status:<init>	(I)V
        //   57: invokevirtual 65	com/google/android/gms/cast/framework/media/RemoteMediaClient$zzb:zzc	(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Result;
        //   60: checkcast 67	com/google/android/gms/cast/framework/media/RemoteMediaClient$MediaChannelResult
        //   63: invokevirtual 70	com/google/android/gms/cast/framework/media/RemoteMediaClient$5:zzc	(Lcom/google/android/gms/common/api/Result;)V
        //   66: goto -24 -> 42
        //   69: astore_2
        //   70: aload_1
        //   71: monitorexit
        //   72: aload_2
        //   73: athrow
        //   74: astore_2
        //   75: goto -30 -> 45
        //   78: astore_2
        //   79: goto -34 -> 45
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	82	0	this	5
        //   69	4	2	localObject	Object
        //   74	1	2	localIOException	IOException
        //   78	1	2	localzzb	zzn.zzb
        // Exception table:
        //   from	to	target	type
        //   10	42	69	finally
        //   42	44	69	finally
        //   45	66	69	finally
        //   70	72	69	finally
        //   10	42	74	java/io/IOException
        //   10	42	78	com/google/android/gms/cast/internal/zzn$zzb
      }
    });
  }
  
  public PendingResult<MediaChannelResult> queueJumpToItem(final int paramInt, final long paramLong, JSONObject paramJSONObject)
  {
    zzanr();
    zza(new zzb(this.rC)
    {
      protected void zza(zze arg1)
      {
        synchronized (RemoteMediaClient.zzd(RemoteMediaClient.this))
        {
          if (RemoteMediaClient.zza(RemoteMediaClient.this, paramInt) == -1)
          {
            zzc((RemoteMediaClient.MediaChannelResult)zzc(new Status(0)));
            return;
          }
        }
        try
        {
          RemoteMediaClient.zze(RemoteMediaClient.this).zza(this.oG, paramInt, paramLong, null, 0, null, this.om);
          return;
          localObject = finally;
          throw ((Throwable)localObject);
        }
        catch (zzn.zzb localzzb)
        {
          for (;;)
          {
            zzc((RemoteMediaClient.MediaChannelResult)zzc(new Status(2100)));
          }
        }
        catch (IOException localIOException)
        {
          for (;;) {}
        }
      }
    });
  }
  
  public PendingResult<MediaChannelResult> queueJumpToItem(int paramInt, JSONObject paramJSONObject)
  {
    return queueJumpToItem(paramInt, -1L, paramJSONObject);
  }
  
  public PendingResult<MediaChannelResult> queueLoad(final MediaQueueItem[] paramArrayOfMediaQueueItem, final int paramInt1, final int paramInt2, final long paramLong, JSONObject paramJSONObject)
    throws IllegalArgumentException
  {
    zzanr();
    zza(new zzb(this.rC)
    {
      protected void zza(zze arg1)
      {
        synchronized (RemoteMediaClient.zzd(RemoteMediaClient.this))
        {
          try
          {
            RemoteMediaClient.zze(RemoteMediaClient.this).zza(this.oG, paramArrayOfMediaQueueItem, paramInt1, paramInt2, paramLong, this.om);
            return;
          }
          catch (IOException localIOException)
          {
            for (;;)
            {
              zzc((RemoteMediaClient.MediaChannelResult)zzc(new Status(2100)));
            }
          }
        }
      }
    });
  }
  
  public PendingResult<MediaChannelResult> queueLoad(MediaQueueItem[] paramArrayOfMediaQueueItem, int paramInt1, int paramInt2, JSONObject paramJSONObject)
    throws IllegalArgumentException
  {
    return queueLoad(paramArrayOfMediaQueueItem, paramInt1, paramInt2, -1L, paramJSONObject);
  }
  
  public PendingResult<MediaChannelResult> queueMoveItemToNewIndex(final int paramInt1, final int paramInt2, final JSONObject paramJSONObject)
  {
    zzanr();
    zza(new zzb(this.rC)
    {
      protected void zza(zze arg1)
      {
        int j = 0;
        int i;
        synchronized (RemoteMediaClient.zzd(RemoteMediaClient.this))
        {
          i = RemoteMediaClient.zza(RemoteMediaClient.this, paramInt1);
          if (i == -1)
          {
            zzc((RemoteMediaClient.MediaChannelResult)zzc(new Status(0)));
            return;
          }
          if (paramInt2 < 0)
          {
            zzc((RemoteMediaClient.MediaChannelResult)zzc(new Status(2001, String.format(Locale.ROOT, "Invalid request: Invalid newIndex %d.", new Object[] { Integer.valueOf(paramInt2) }))));
            return;
          }
        }
        if (i == paramInt2)
        {
          zzc((RemoteMediaClient.MediaChannelResult)zzc(new Status(0)));
          return;
        }
        Object localObject2;
        if (paramInt2 > i)
        {
          i = paramInt2 + 1;
          localObject2 = RemoteMediaClient.this.getMediaStatus().getQueueItem(i);
          i = j;
          if (localObject2 != null) {
            i = ((MediaQueueItem)localObject2).getItemId();
          }
        }
        for (;;)
        {
          try
          {
            localObject2 = RemoteMediaClient.zze(RemoteMediaClient.this);
            zzp localzzp = this.oG;
            j = paramInt1;
            JSONObject localJSONObject = paramJSONObject;
            ((zzn)localObject2).zza(localzzp, new int[] { j }, i, localJSONObject);
            return;
          }
          catch (IOException localIOException)
          {
            continue;
          }
          catch (zzn.zzb localzzb)
          {
            continue;
          }
          i = paramInt2;
          break;
          zzc((RemoteMediaClient.MediaChannelResult)zzc(new Status(2100)));
        }
      }
    });
  }
  
  public PendingResult<MediaChannelResult> queueNext(final JSONObject paramJSONObject)
  {
    zzanr();
    zza(new zzb(this.rC)
    {
      /* Error */
      protected void zza(zze arg1)
      {
        // Byte code:
        //   0: aload_0
        //   1: getfield 17	com/google/android/gms/cast/framework/media/RemoteMediaClient$11:rG	Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;
        //   4: invokestatic 33	com/google/android/gms/cast/framework/media/RemoteMediaClient:zzd	(Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;)Ljava/lang/Object;
        //   7: astore_1
        //   8: aload_1
        //   9: monitorenter
        //   10: aload_0
        //   11: getfield 17	com/google/android/gms/cast/framework/media/RemoteMediaClient$11:rG	Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;
        //   14: invokestatic 37	com/google/android/gms/cast/framework/media/RemoteMediaClient:zze	(Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;)Lcom/google/android/gms/cast/internal/zzn;
        //   17: aload_0
        //   18: getfield 41	com/google/android/gms/cast/framework/media/RemoteMediaClient$11:oG	Lcom/google/android/gms/cast/internal/zzp;
        //   21: iconst_0
        //   22: ldc2_w 42
        //   25: aconst_null
        //   26: iconst_1
        //   27: aconst_null
        //   28: aload_0
        //   29: getfield 19	com/google/android/gms/cast/framework/media/RemoteMediaClient$11:om	Lorg/json/JSONObject;
        //   32: invokevirtual 48	com/google/android/gms/cast/internal/zzn:zza	(Lcom/google/android/gms/cast/internal/zzp;IJ[Lcom/google/android/gms/cast/MediaQueueItem;ILjava/lang/Integer;Lorg/json/JSONObject;)J
        //   35: pop2
        //   36: aload_1
        //   37: monitorexit
        //   38: return
        //   39: aload_0
        //   40: aload_0
        //   41: new 50	com/google/android/gms/common/api/Status
        //   44: dup
        //   45: sipush 2100
        //   48: invokespecial 53	com/google/android/gms/common/api/Status:<init>	(I)V
        //   51: invokevirtual 57	com/google/android/gms/cast/framework/media/RemoteMediaClient$zzb:zzc	(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Result;
        //   54: checkcast 59	com/google/android/gms/cast/framework/media/RemoteMediaClient$MediaChannelResult
        //   57: invokevirtual 62	com/google/android/gms/cast/framework/media/RemoteMediaClient$11:zzc	(Lcom/google/android/gms/common/api/Result;)V
        //   60: goto -24 -> 36
        //   63: astore_2
        //   64: aload_1
        //   65: monitorexit
        //   66: aload_2
        //   67: athrow
        //   68: astore_2
        //   69: goto -30 -> 39
        //   72: astore_2
        //   73: goto -34 -> 39
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	76	0	this	11
        //   63	4	2	localObject	Object
        //   68	1	2	localIOException	IOException
        //   72	1	2	localzzb	zzn.zzb
        // Exception table:
        //   from	to	target	type
        //   10	36	63	finally
        //   36	38	63	finally
        //   39	60	63	finally
        //   64	66	63	finally
        //   10	36	68	java/io/IOException
        //   10	36	72	com/google/android/gms/cast/internal/zzn$zzb
      }
    });
  }
  
  public PendingResult<MediaChannelResult> queuePrev(final JSONObject paramJSONObject)
  {
    zzanr();
    zza(new zzb(this.rC)
    {
      /* Error */
      protected void zza(zze arg1)
      {
        // Byte code:
        //   0: aload_0
        //   1: getfield 17	com/google/android/gms/cast/framework/media/RemoteMediaClient$10:rG	Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;
        //   4: invokestatic 33	com/google/android/gms/cast/framework/media/RemoteMediaClient:zzd	(Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;)Ljava/lang/Object;
        //   7: astore_1
        //   8: aload_1
        //   9: monitorenter
        //   10: aload_0
        //   11: getfield 17	com/google/android/gms/cast/framework/media/RemoteMediaClient$10:rG	Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;
        //   14: invokestatic 37	com/google/android/gms/cast/framework/media/RemoteMediaClient:zze	(Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;)Lcom/google/android/gms/cast/internal/zzn;
        //   17: aload_0
        //   18: getfield 41	com/google/android/gms/cast/framework/media/RemoteMediaClient$10:oG	Lcom/google/android/gms/cast/internal/zzp;
        //   21: iconst_0
        //   22: ldc2_w 42
        //   25: aconst_null
        //   26: iconst_m1
        //   27: aconst_null
        //   28: aload_0
        //   29: getfield 19	com/google/android/gms/cast/framework/media/RemoteMediaClient$10:om	Lorg/json/JSONObject;
        //   32: invokevirtual 48	com/google/android/gms/cast/internal/zzn:zza	(Lcom/google/android/gms/cast/internal/zzp;IJ[Lcom/google/android/gms/cast/MediaQueueItem;ILjava/lang/Integer;Lorg/json/JSONObject;)J
        //   35: pop2
        //   36: aload_1
        //   37: monitorexit
        //   38: return
        //   39: aload_0
        //   40: aload_0
        //   41: new 50	com/google/android/gms/common/api/Status
        //   44: dup
        //   45: sipush 2100
        //   48: invokespecial 53	com/google/android/gms/common/api/Status:<init>	(I)V
        //   51: invokevirtual 57	com/google/android/gms/cast/framework/media/RemoteMediaClient$zzb:zzc	(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Result;
        //   54: checkcast 59	com/google/android/gms/cast/framework/media/RemoteMediaClient$MediaChannelResult
        //   57: invokevirtual 62	com/google/android/gms/cast/framework/media/RemoteMediaClient$10:zzc	(Lcom/google/android/gms/common/api/Result;)V
        //   60: goto -24 -> 36
        //   63: astore_2
        //   64: aload_1
        //   65: monitorexit
        //   66: aload_2
        //   67: athrow
        //   68: astore_2
        //   69: goto -30 -> 39
        //   72: astore_2
        //   73: goto -34 -> 39
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	76	0	this	10
        //   63	4	2	localObject	Object
        //   68	1	2	localIOException	IOException
        //   72	1	2	localzzb	zzn.zzb
        // Exception table:
        //   from	to	target	type
        //   10	36	63	finally
        //   36	38	63	finally
        //   39	60	63	finally
        //   64	66	63	finally
        //   10	36	68	java/io/IOException
        //   10	36	72	com/google/android/gms/cast/internal/zzn$zzb
      }
    });
  }
  
  public PendingResult<MediaChannelResult> queueRemoveItem(final int paramInt, final JSONObject paramJSONObject)
  {
    zzanr();
    zza(new zzb(this.rC)
    {
      protected void zza(zze arg1)
      {
        synchronized (RemoteMediaClient.zzd(RemoteMediaClient.this))
        {
          if (RemoteMediaClient.zza(RemoteMediaClient.this, paramInt) == -1)
          {
            zzc((RemoteMediaClient.MediaChannelResult)zzc(new Status(0)));
            return;
          }
        }
        try
        {
          zzn localzzn = RemoteMediaClient.zze(RemoteMediaClient.this);
          zzp localzzp = this.oG;
          int i = paramInt;
          JSONObject localJSONObject = paramJSONObject;
          localzzn.zza(localzzp, new int[] { i }, localJSONObject);
          return;
          localObject = finally;
          throw ((Throwable)localObject);
        }
        catch (zzn.zzb localzzb)
        {
          for (;;)
          {
            zzc((RemoteMediaClient.MediaChannelResult)zzc(new Status(2100)));
          }
        }
        catch (IOException localIOException)
        {
          for (;;) {}
        }
      }
    });
  }
  
  public PendingResult<MediaChannelResult> queueRemoveItems(final int[] paramArrayOfInt, final JSONObject paramJSONObject)
    throws IllegalArgumentException
  {
    zzanr();
    zza(new zzb(this.rC)
    {
      /* Error */
      protected void zza(zze arg1)
      {
        // Byte code:
        //   0: aload_0
        //   1: getfield 19	com/google/android/gms/cast/framework/media/RemoteMediaClient$8:rG	Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;
        //   4: invokestatic 37	com/google/android/gms/cast/framework/media/RemoteMediaClient:zzd	(Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;)Ljava/lang/Object;
        //   7: astore_1
        //   8: aload_1
        //   9: monitorenter
        //   10: aload_0
        //   11: getfield 19	com/google/android/gms/cast/framework/media/RemoteMediaClient$8:rG	Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;
        //   14: invokestatic 41	com/google/android/gms/cast/framework/media/RemoteMediaClient:zze	(Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;)Lcom/google/android/gms/cast/internal/zzn;
        //   17: aload_0
        //   18: getfield 45	com/google/android/gms/cast/framework/media/RemoteMediaClient$8:oG	Lcom/google/android/gms/cast/internal/zzp;
        //   21: aload_0
        //   22: getfield 21	com/google/android/gms/cast/framework/media/RemoteMediaClient$8:or	[I
        //   25: aload_0
        //   26: getfield 23	com/google/android/gms/cast/framework/media/RemoteMediaClient$8:om	Lorg/json/JSONObject;
        //   29: invokevirtual 50	com/google/android/gms/cast/internal/zzn:zza	(Lcom/google/android/gms/cast/internal/zzp;[ILorg/json/JSONObject;)J
        //   32: pop2
        //   33: aload_1
        //   34: monitorexit
        //   35: return
        //   36: aload_0
        //   37: aload_0
        //   38: new 52	com/google/android/gms/common/api/Status
        //   41: dup
        //   42: sipush 2100
        //   45: invokespecial 55	com/google/android/gms/common/api/Status:<init>	(I)V
        //   48: invokevirtual 59	com/google/android/gms/cast/framework/media/RemoteMediaClient$zzb:zzc	(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Result;
        //   51: checkcast 61	com/google/android/gms/cast/framework/media/RemoteMediaClient$MediaChannelResult
        //   54: invokevirtual 64	com/google/android/gms/cast/framework/media/RemoteMediaClient$8:zzc	(Lcom/google/android/gms/common/api/Result;)V
        //   57: goto -24 -> 33
        //   60: astore_2
        //   61: aload_1
        //   62: monitorexit
        //   63: aload_2
        //   64: athrow
        //   65: astore_2
        //   66: goto -30 -> 36
        //   69: astore_2
        //   70: goto -34 -> 36
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	73	0	this	8
        //   60	4	2	localObject	Object
        //   65	1	2	localIOException	IOException
        //   69	1	2	localzzb	zzn.zzb
        // Exception table:
        //   from	to	target	type
        //   10	33	60	finally
        //   33	35	60	finally
        //   36	57	60	finally
        //   61	63	60	finally
        //   10	33	65	java/io/IOException
        //   10	33	69	com/google/android/gms/cast/internal/zzn$zzb
      }
    });
  }
  
  public PendingResult<MediaChannelResult> queueReorderItems(final int[] paramArrayOfInt, final int paramInt, final JSONObject paramJSONObject)
    throws IllegalArgumentException
  {
    zzanr();
    zza(new zzb(this.rC)
    {
      /* Error */
      protected void zza(zze arg1)
      {
        // Byte code:
        //   0: aload_0
        //   1: getfield 21	com/google/android/gms/cast/framework/media/RemoteMediaClient$9:rG	Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;
        //   4: invokestatic 41	com/google/android/gms/cast/framework/media/RemoteMediaClient:zzd	(Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;)Ljava/lang/Object;
        //   7: astore_1
        //   8: aload_1
        //   9: monitorenter
        //   10: aload_0
        //   11: getfield 21	com/google/android/gms/cast/framework/media/RemoteMediaClient$9:rG	Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;
        //   14: invokestatic 45	com/google/android/gms/cast/framework/media/RemoteMediaClient:zze	(Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;)Lcom/google/android/gms/cast/internal/zzn;
        //   17: aload_0
        //   18: getfield 49	com/google/android/gms/cast/framework/media/RemoteMediaClient$9:oG	Lcom/google/android/gms/cast/internal/zzp;
        //   21: aload_0
        //   22: getfield 23	com/google/android/gms/cast/framework/media/RemoteMediaClient$9:os	[I
        //   25: aload_0
        //   26: getfield 25	com/google/android/gms/cast/framework/media/RemoteMediaClient$9:oo	I
        //   29: aload_0
        //   30: getfield 27	com/google/android/gms/cast/framework/media/RemoteMediaClient$9:om	Lorg/json/JSONObject;
        //   33: invokevirtual 54	com/google/android/gms/cast/internal/zzn:zza	(Lcom/google/android/gms/cast/internal/zzp;[IILorg/json/JSONObject;)J
        //   36: pop2
        //   37: aload_1
        //   38: monitorexit
        //   39: return
        //   40: aload_0
        //   41: aload_0
        //   42: new 56	com/google/android/gms/common/api/Status
        //   45: dup
        //   46: sipush 2100
        //   49: invokespecial 59	com/google/android/gms/common/api/Status:<init>	(I)V
        //   52: invokevirtual 63	com/google/android/gms/cast/framework/media/RemoteMediaClient$zzb:zzc	(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Result;
        //   55: checkcast 65	com/google/android/gms/cast/framework/media/RemoteMediaClient$MediaChannelResult
        //   58: invokevirtual 68	com/google/android/gms/cast/framework/media/RemoteMediaClient$9:zzc	(Lcom/google/android/gms/common/api/Result;)V
        //   61: goto -24 -> 37
        //   64: astore_2
        //   65: aload_1
        //   66: monitorexit
        //   67: aload_2
        //   68: athrow
        //   69: astore_2
        //   70: goto -30 -> 40
        //   73: astore_2
        //   74: goto -34 -> 40
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	77	0	this	9
        //   64	4	2	localObject	Object
        //   69	1	2	localIOException	IOException
        //   73	1	2	localzzb	zzn.zzb
        // Exception table:
        //   from	to	target	type
        //   10	37	64	finally
        //   37	39	64	finally
        //   40	61	64	finally
        //   65	67	64	finally
        //   10	37	69	java/io/IOException
        //   10	37	73	com/google/android/gms/cast/internal/zzn$zzb
      }
    });
  }
  
  public PendingResult<MediaChannelResult> queueSetRepeatMode(final int paramInt, final JSONObject paramJSONObject)
  {
    zzanr();
    zza(new zzb(this.rC)
    {
      /* Error */
      protected void zza(zze arg1)
      {
        // Byte code:
        //   0: aload_0
        //   1: getfield 19	com/google/android/gms/cast/framework/media/RemoteMediaClient$13:rG	Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;
        //   4: invokestatic 37	com/google/android/gms/cast/framework/media/RemoteMediaClient:zzd	(Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;)Ljava/lang/Object;
        //   7: astore_1
        //   8: aload_1
        //   9: monitorenter
        //   10: aload_0
        //   11: getfield 19	com/google/android/gms/cast/framework/media/RemoteMediaClient$13:rG	Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;
        //   14: invokestatic 41	com/google/android/gms/cast/framework/media/RemoteMediaClient:zze	(Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;)Lcom/google/android/gms/cast/internal/zzn;
        //   17: aload_0
        //   18: getfield 45	com/google/android/gms/cast/framework/media/RemoteMediaClient$13:oG	Lcom/google/android/gms/cast/internal/zzp;
        //   21: iconst_0
        //   22: ldc2_w 46
        //   25: aconst_null
        //   26: iconst_0
        //   27: aload_0
        //   28: getfield 21	com/google/android/gms/cast/framework/media/RemoteMediaClient$13:ok	I
        //   31: invokestatic 53	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //   34: aload_0
        //   35: getfield 23	com/google/android/gms/cast/framework/media/RemoteMediaClient$13:om	Lorg/json/JSONObject;
        //   38: invokevirtual 58	com/google/android/gms/cast/internal/zzn:zza	(Lcom/google/android/gms/cast/internal/zzp;IJ[Lcom/google/android/gms/cast/MediaQueueItem;ILjava/lang/Integer;Lorg/json/JSONObject;)J
        //   41: pop2
        //   42: aload_1
        //   43: monitorexit
        //   44: return
        //   45: aload_0
        //   46: aload_0
        //   47: new 60	com/google/android/gms/common/api/Status
        //   50: dup
        //   51: sipush 2100
        //   54: invokespecial 63	com/google/android/gms/common/api/Status:<init>	(I)V
        //   57: invokevirtual 67	com/google/android/gms/cast/framework/media/RemoteMediaClient$zzb:zzc	(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Result;
        //   60: checkcast 69	com/google/android/gms/cast/framework/media/RemoteMediaClient$MediaChannelResult
        //   63: invokevirtual 72	com/google/android/gms/cast/framework/media/RemoteMediaClient$13:zzc	(Lcom/google/android/gms/common/api/Result;)V
        //   66: goto -24 -> 42
        //   69: astore_2
        //   70: aload_1
        //   71: monitorexit
        //   72: aload_2
        //   73: athrow
        //   74: astore_2
        //   75: goto -30 -> 45
        //   78: astore_2
        //   79: goto -34 -> 45
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	82	0	this	13
        //   69	4	2	localObject	Object
        //   74	1	2	localIOException	IOException
        //   78	1	2	localzzb	zzn.zzb
        // Exception table:
        //   from	to	target	type
        //   10	42	69	finally
        //   42	44	69	finally
        //   45	66	69	finally
        //   70	72	69	finally
        //   10	42	74	java/io/IOException
        //   10	42	78	com/google/android/gms/cast/internal/zzn$zzb
      }
    });
  }
  
  public PendingResult<MediaChannelResult> queueUpdateItems(final MediaQueueItem[] paramArrayOfMediaQueueItem, final JSONObject paramJSONObject)
  {
    zzanr();
    zza(new zzb(this.rC)
    {
      /* Error */
      protected void zza(zze arg1)
      {
        // Byte code:
        //   0: aload_0
        //   1: getfield 19	com/google/android/gms/cast/framework/media/RemoteMediaClient$7:rG	Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;
        //   4: invokestatic 37	com/google/android/gms/cast/framework/media/RemoteMediaClient:zzd	(Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;)Ljava/lang/Object;
        //   7: astore_1
        //   8: aload_1
        //   9: monitorenter
        //   10: aload_0
        //   11: getfield 19	com/google/android/gms/cast/framework/media/RemoteMediaClient$7:rG	Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;
        //   14: invokestatic 41	com/google/android/gms/cast/framework/media/RemoteMediaClient:zze	(Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;)Lcom/google/android/gms/cast/internal/zzn;
        //   17: aload_0
        //   18: getfield 45	com/google/android/gms/cast/framework/media/RemoteMediaClient$7:oG	Lcom/google/android/gms/cast/internal/zzp;
        //   21: iconst_0
        //   22: ldc2_w 46
        //   25: aload_0
        //   26: getfield 21	com/google/android/gms/cast/framework/media/RemoteMediaClient$7:oq	[Lcom/google/android/gms/cast/MediaQueueItem;
        //   29: iconst_0
        //   30: aconst_null
        //   31: aload_0
        //   32: getfield 23	com/google/android/gms/cast/framework/media/RemoteMediaClient$7:om	Lorg/json/JSONObject;
        //   35: invokevirtual 52	com/google/android/gms/cast/internal/zzn:zza	(Lcom/google/android/gms/cast/internal/zzp;IJ[Lcom/google/android/gms/cast/MediaQueueItem;ILjava/lang/Integer;Lorg/json/JSONObject;)J
        //   38: pop2
        //   39: aload_1
        //   40: monitorexit
        //   41: return
        //   42: aload_0
        //   43: aload_0
        //   44: new 54	com/google/android/gms/common/api/Status
        //   47: dup
        //   48: sipush 2100
        //   51: invokespecial 57	com/google/android/gms/common/api/Status:<init>	(I)V
        //   54: invokevirtual 61	com/google/android/gms/cast/framework/media/RemoteMediaClient$zzb:zzc	(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Result;
        //   57: checkcast 63	com/google/android/gms/cast/framework/media/RemoteMediaClient$MediaChannelResult
        //   60: invokevirtual 66	com/google/android/gms/cast/framework/media/RemoteMediaClient$7:zzc	(Lcom/google/android/gms/common/api/Result;)V
        //   63: goto -24 -> 39
        //   66: astore_2
        //   67: aload_1
        //   68: monitorexit
        //   69: aload_2
        //   70: athrow
        //   71: astore_2
        //   72: goto -30 -> 42
        //   75: astore_2
        //   76: goto -34 -> 42
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	79	0	this	7
        //   66	4	2	localObject	Object
        //   71	1	2	localIOException	IOException
        //   75	1	2	localzzb	zzn.zzb
        // Exception table:
        //   from	to	target	type
        //   10	39	66	finally
        //   39	41	66	finally
        //   42	63	66	finally
        //   67	69	66	finally
        //   10	39	71	java/io/IOException
        //   10	39	75	com/google/android/gms/cast/internal/zzn$zzb
      }
    });
  }
  
  public void removeListener(Listener paramListener)
  {
    if (paramListener != null) {
      this.mListeners.remove(paramListener);
    }
  }
  
  public void removeProgressListener(ProgressListener paramProgressListener)
  {
    zzd localzzd = (zzd)this.rD.remove(paramProgressListener);
    if (localzzd != null)
    {
      localzzd.zzb(paramProgressListener);
      if (!localzzd.zzanv())
      {
        this.rE.remove(Long.valueOf(localzzd.zzanu()));
        localzzd.stop();
      }
    }
  }
  
  public PendingResult<MediaChannelResult> requestStatus()
  {
    zzanr();
    zza(new zzb(this.rC)
    {
      protected void zza(zze arg1)
      {
        synchronized (RemoteMediaClient.zzd(RemoteMediaClient.this))
        {
          try
          {
            RemoteMediaClient.zze(RemoteMediaClient.this).zza(this.oG);
            return;
          }
          catch (IOException localIOException)
          {
            for (;;)
            {
              zzc((RemoteMediaClient.MediaChannelResult)zzc(new Status(2100)));
            }
          }
        }
      }
    });
  }
  
  public PendingResult<MediaChannelResult> seek(long paramLong)
  {
    return seek(paramLong, 0, null);
  }
  
  public PendingResult<MediaChannelResult> seek(long paramLong, int paramInt)
  {
    return seek(paramLong, paramInt, null);
  }
  
  public PendingResult<MediaChannelResult> seek(final long paramLong, int paramInt, final JSONObject paramJSONObject)
  {
    zzanr();
    zza(new zzb(this.rC)
    {
      /* Error */
      protected void zza(zze arg1)
      {
        // Byte code:
        //   0: aload_0
        //   1: getfield 21	com/google/android/gms/cast/framework/media/RemoteMediaClient$20:rG	Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;
        //   4: invokestatic 41	com/google/android/gms/cast/framework/media/RemoteMediaClient:zzd	(Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;)Ljava/lang/Object;
        //   7: astore_1
        //   8: aload_1
        //   9: monitorenter
        //   10: aload_0
        //   11: getfield 21	com/google/android/gms/cast/framework/media/RemoteMediaClient$20:rG	Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;
        //   14: invokestatic 45	com/google/android/gms/cast/framework/media/RemoteMediaClient:zze	(Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;)Lcom/google/android/gms/cast/internal/zzn;
        //   17: aload_0
        //   18: getfield 49	com/google/android/gms/cast/framework/media/RemoteMediaClient$20:oG	Lcom/google/android/gms/cast/internal/zzp;
        //   21: aload_0
        //   22: getfield 23	com/google/android/gms/cast/framework/media/RemoteMediaClient$20:oy	J
        //   25: aload_0
        //   26: getfield 25	com/google/android/gms/cast/framework/media/RemoteMediaClient$20:oz	I
        //   29: aload_0
        //   30: getfield 27	com/google/android/gms/cast/framework/media/RemoteMediaClient$20:om	Lorg/json/JSONObject;
        //   33: invokevirtual 54	com/google/android/gms/cast/internal/zzn:zza	(Lcom/google/android/gms/cast/internal/zzp;JILorg/json/JSONObject;)J
        //   36: pop2
        //   37: aload_1
        //   38: monitorexit
        //   39: return
        //   40: aload_0
        //   41: aload_0
        //   42: new 56	com/google/android/gms/common/api/Status
        //   45: dup
        //   46: sipush 2100
        //   49: invokespecial 59	com/google/android/gms/common/api/Status:<init>	(I)V
        //   52: invokevirtual 63	com/google/android/gms/cast/framework/media/RemoteMediaClient$zzb:zzc	(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Result;
        //   55: checkcast 65	com/google/android/gms/cast/framework/media/RemoteMediaClient$MediaChannelResult
        //   58: invokevirtual 68	com/google/android/gms/cast/framework/media/RemoteMediaClient$20:zzc	(Lcom/google/android/gms/common/api/Result;)V
        //   61: goto -24 -> 37
        //   64: astore_2
        //   65: aload_1
        //   66: monitorexit
        //   67: aload_2
        //   68: athrow
        //   69: astore_2
        //   70: goto -30 -> 40
        //   73: astore_2
        //   74: goto -34 -> 40
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	77	0	this	20
        //   64	4	2	localObject	Object
        //   69	1	2	localIOException	IOException
        //   73	1	2	localzzb	zzn.zzb
        // Exception table:
        //   from	to	target	type
        //   10	37	64	finally
        //   37	39	64	finally
        //   40	61	64	finally
        //   65	67	64	finally
        //   10	37	69	java/io/IOException
        //   10	37	73	com/google/android/gms/cast/internal/zzn$zzb
      }
    });
  }
  
  public PendingResult<MediaChannelResult> setActiveMediaTracks(final long[] paramArrayOfLong)
  {
    zzanr();
    if (paramArrayOfLong == null) {
      throw new IllegalArgumentException("trackIds cannot be null");
    }
    zza(new zzb(this.rC)
    {
      /* Error */
      protected void zza(zze arg1)
      {
        // Byte code:
        //   0: aload_0
        //   1: getfield 17	com/google/android/gms/cast/framework/media/RemoteMediaClient$2:rG	Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;
        //   4: invokestatic 33	com/google/android/gms/cast/framework/media/RemoteMediaClient:zzd	(Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;)Ljava/lang/Object;
        //   7: astore_1
        //   8: aload_1
        //   9: monitorenter
        //   10: aload_0
        //   11: getfield 17	com/google/android/gms/cast/framework/media/RemoteMediaClient$2:rG	Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;
        //   14: invokestatic 37	com/google/android/gms/cast/framework/media/RemoteMediaClient:zze	(Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;)Lcom/google/android/gms/cast/internal/zzn;
        //   17: aload_0
        //   18: getfield 41	com/google/android/gms/cast/framework/media/RemoteMediaClient$2:oG	Lcom/google/android/gms/cast/internal/zzp;
        //   21: aload_0
        //   22: getfield 19	com/google/android/gms/cast/framework/media/RemoteMediaClient$2:og	[J
        //   25: invokevirtual 46	com/google/android/gms/cast/internal/zzn:zza	(Lcom/google/android/gms/cast/internal/zzp;[J)J
        //   28: pop2
        //   29: aload_1
        //   30: monitorexit
        //   31: return
        //   32: aload_0
        //   33: aload_0
        //   34: new 48	com/google/android/gms/common/api/Status
        //   37: dup
        //   38: sipush 2100
        //   41: invokespecial 51	com/google/android/gms/common/api/Status:<init>	(I)V
        //   44: invokevirtual 55	com/google/android/gms/cast/framework/media/RemoteMediaClient$zzb:zzc	(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Result;
        //   47: checkcast 57	com/google/android/gms/cast/framework/media/RemoteMediaClient$MediaChannelResult
        //   50: invokevirtual 60	com/google/android/gms/cast/framework/media/RemoteMediaClient$2:zzc	(Lcom/google/android/gms/common/api/Result;)V
        //   53: goto -24 -> 29
        //   56: astore_2
        //   57: aload_1
        //   58: monitorexit
        //   59: aload_2
        //   60: athrow
        //   61: astore_2
        //   62: goto -30 -> 32
        //   65: astore_2
        //   66: goto -34 -> 32
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	69	0	this	2
        //   56	4	2	localObject	Object
        //   61	1	2	localIOException	IOException
        //   65	1	2	localzzb	zzn.zzb
        // Exception table:
        //   from	to	target	type
        //   10	29	56	finally
        //   29	31	56	finally
        //   32	53	56	finally
        //   57	59	56	finally
        //   10	29	61	java/io/IOException
        //   10	29	65	com/google/android/gms/cast/internal/zzn$zzb
      }
    });
  }
  
  public void setParseAdsInfoCallback(ParseAdsInfoCallback paramParseAdsInfoCallback)
  {
    this.rF = paramParseAdsInfoCallback;
  }
  
  public PendingResult<MediaChannelResult> setStreamMute(boolean paramBoolean)
  {
    return setStreamMute(paramBoolean, null);
  }
  
  public PendingResult<MediaChannelResult> setStreamMute(final boolean paramBoolean, final JSONObject paramJSONObject)
  {
    zzanr();
    zza(new zzb(this.rC)
    {
      /* Error */
      protected void zza(zze arg1)
      {
        // Byte code:
        //   0: aload_0
        //   1: getfield 19	com/google/android/gms/cast/framework/media/RemoteMediaClient$22:rG	Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;
        //   4: invokestatic 37	com/google/android/gms/cast/framework/media/RemoteMediaClient:zzd	(Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;)Ljava/lang/Object;
        //   7: astore_1
        //   8: aload_1
        //   9: monitorenter
        //   10: aload_0
        //   11: getfield 19	com/google/android/gms/cast/framework/media/RemoteMediaClient$22:rG	Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;
        //   14: invokestatic 41	com/google/android/gms/cast/framework/media/RemoteMediaClient:zze	(Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;)Lcom/google/android/gms/cast/internal/zzn;
        //   17: aload_0
        //   18: getfield 45	com/google/android/gms/cast/framework/media/RemoteMediaClient$22:oG	Lcom/google/android/gms/cast/internal/zzp;
        //   21: aload_0
        //   22: getfield 21	com/google/android/gms/cast/framework/media/RemoteMediaClient$22:rH	Z
        //   25: aload_0
        //   26: getfield 23	com/google/android/gms/cast/framework/media/RemoteMediaClient$22:om	Lorg/json/JSONObject;
        //   29: invokevirtual 50	com/google/android/gms/cast/internal/zzn:zza	(Lcom/google/android/gms/cast/internal/zzp;ZLorg/json/JSONObject;)J
        //   32: pop2
        //   33: aload_1
        //   34: monitorexit
        //   35: return
        //   36: aload_0
        //   37: aload_0
        //   38: new 52	com/google/android/gms/common/api/Status
        //   41: dup
        //   42: sipush 2100
        //   45: invokespecial 55	com/google/android/gms/common/api/Status:<init>	(I)V
        //   48: invokevirtual 59	com/google/android/gms/cast/framework/media/RemoteMediaClient$zzb:zzc	(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Result;
        //   51: checkcast 61	com/google/android/gms/cast/framework/media/RemoteMediaClient$MediaChannelResult
        //   54: invokevirtual 64	com/google/android/gms/cast/framework/media/RemoteMediaClient$22:zzc	(Lcom/google/android/gms/common/api/Result;)V
        //   57: goto -24 -> 33
        //   60: astore_2
        //   61: aload_1
        //   62: monitorexit
        //   63: aload_2
        //   64: athrow
        //   65: astore_2
        //   66: goto -30 -> 36
        //   69: astore_2
        //   70: goto -34 -> 36
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	73	0	this	22
        //   60	4	2	localObject	Object
        //   65	1	2	localIOException	IOException
        //   69	1	2	localzzb	zzn.zzb
        // Exception table:
        //   from	to	target	type
        //   10	33	60	finally
        //   33	35	60	finally
        //   36	57	60	finally
        //   61	63	60	finally
        //   10	33	65	java/io/IOException
        //   10	33	69	com/google/android/gms/cast/internal/zzn$zzb
      }
    });
  }
  
  public PendingResult<MediaChannelResult> setStreamVolume(double paramDouble)
    throws IllegalArgumentException
  {
    return setStreamVolume(paramDouble, null);
  }
  
  public PendingResult<MediaChannelResult> setStreamVolume(final double paramDouble, JSONObject paramJSONObject)
    throws IllegalArgumentException
  {
    zzanr();
    if ((Double.isInfinite(paramDouble)) || (Double.isNaN(paramDouble))) {
      throw new IllegalArgumentException(41 + "Volume cannot be " + paramDouble);
    }
    zza(new zzb(this.rC)
    {
      /* Error */
      protected void zza(zze arg1)
      {
        // Byte code:
        //   0: aload_0
        //   1: getfield 19	com/google/android/gms/cast/framework/media/RemoteMediaClient$21:rG	Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;
        //   4: invokestatic 39	com/google/android/gms/cast/framework/media/RemoteMediaClient:zzd	(Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;)Ljava/lang/Object;
        //   7: astore_1
        //   8: aload_1
        //   9: monitorenter
        //   10: aload_0
        //   11: getfield 19	com/google/android/gms/cast/framework/media/RemoteMediaClient$21:rG	Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;
        //   14: invokestatic 43	com/google/android/gms/cast/framework/media/RemoteMediaClient:zze	(Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;)Lcom/google/android/gms/cast/internal/zzn;
        //   17: aload_0
        //   18: getfield 47	com/google/android/gms/cast/framework/media/RemoteMediaClient$21:oG	Lcom/google/android/gms/cast/internal/zzp;
        //   21: aload_0
        //   22: getfield 21	com/google/android/gms/cast/framework/media/RemoteMediaClient$21:oA	D
        //   25: aload_0
        //   26: getfield 23	com/google/android/gms/cast/framework/media/RemoteMediaClient$21:om	Lorg/json/JSONObject;
        //   29: invokevirtual 52	com/google/android/gms/cast/internal/zzn:zza	(Lcom/google/android/gms/cast/internal/zzp;DLorg/json/JSONObject;)J
        //   32: pop2
        //   33: aload_1
        //   34: monitorexit
        //   35: return
        //   36: aload_0
        //   37: aload_0
        //   38: new 54	com/google/android/gms/common/api/Status
        //   41: dup
        //   42: sipush 2100
        //   45: invokespecial 57	com/google/android/gms/common/api/Status:<init>	(I)V
        //   48: invokevirtual 61	com/google/android/gms/cast/framework/media/RemoteMediaClient$zzb:zzc	(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Result;
        //   51: checkcast 63	com/google/android/gms/cast/framework/media/RemoteMediaClient$MediaChannelResult
        //   54: invokevirtual 66	com/google/android/gms/cast/framework/media/RemoteMediaClient$21:zzc	(Lcom/google/android/gms/common/api/Result;)V
        //   57: goto -24 -> 33
        //   60: astore_2
        //   61: aload_1
        //   62: monitorexit
        //   63: aload_2
        //   64: athrow
        //   65: astore_2
        //   66: goto -30 -> 36
        //   69: astore_2
        //   70: goto -34 -> 36
        //   73: astore_2
        //   74: goto -38 -> 36
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	77	0	this	21
        //   60	4	2	localObject	Object
        //   65	1	2	localIOException	IOException
        //   69	1	2	localIllegalArgumentException	IllegalArgumentException
        //   73	1	2	localzzb	zzn.zzb
        // Exception table:
        //   from	to	target	type
        //   10	33	60	finally
        //   33	35	60	finally
        //   36	57	60	finally
        //   61	63	60	finally
        //   10	33	65	java/io/IOException
        //   10	33	69	java/lang/IllegalArgumentException
        //   10	33	73	com/google/android/gms/cast/internal/zzn$zzb
      }
    });
  }
  
  public PendingResult<MediaChannelResult> setTextTrackStyle(final TextTrackStyle paramTextTrackStyle)
  {
    zzanr();
    if (paramTextTrackStyle == null) {
      throw new IllegalArgumentException("trackStyle cannot be null");
    }
    zza(new zzb(this.rC)
    {
      /* Error */
      protected void zza(zze arg1)
      {
        // Byte code:
        //   0: aload_0
        //   1: getfield 17	com/google/android/gms/cast/framework/media/RemoteMediaClient$3:rG	Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;
        //   4: invokestatic 33	com/google/android/gms/cast/framework/media/RemoteMediaClient:zzd	(Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;)Ljava/lang/Object;
        //   7: astore_1
        //   8: aload_1
        //   9: monitorenter
        //   10: aload_0
        //   11: getfield 17	com/google/android/gms/cast/framework/media/RemoteMediaClient$3:rG	Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;
        //   14: invokestatic 37	com/google/android/gms/cast/framework/media/RemoteMediaClient:zze	(Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;)Lcom/google/android/gms/cast/internal/zzn;
        //   17: aload_0
        //   18: getfield 41	com/google/android/gms/cast/framework/media/RemoteMediaClient$3:oG	Lcom/google/android/gms/cast/internal/zzp;
        //   21: aload_0
        //   22: getfield 19	com/google/android/gms/cast/framework/media/RemoteMediaClient$3:oh	Lcom/google/android/gms/cast/TextTrackStyle;
        //   25: invokevirtual 46	com/google/android/gms/cast/internal/zzn:zza	(Lcom/google/android/gms/cast/internal/zzp;Lcom/google/android/gms/cast/TextTrackStyle;)J
        //   28: pop2
        //   29: aload_1
        //   30: monitorexit
        //   31: return
        //   32: aload_0
        //   33: aload_0
        //   34: new 48	com/google/android/gms/common/api/Status
        //   37: dup
        //   38: sipush 2100
        //   41: invokespecial 51	com/google/android/gms/common/api/Status:<init>	(I)V
        //   44: invokevirtual 55	com/google/android/gms/cast/framework/media/RemoteMediaClient$zzb:zzc	(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Result;
        //   47: checkcast 57	com/google/android/gms/cast/framework/media/RemoteMediaClient$MediaChannelResult
        //   50: invokevirtual 60	com/google/android/gms/cast/framework/media/RemoteMediaClient$3:zzc	(Lcom/google/android/gms/common/api/Result;)V
        //   53: goto -24 -> 29
        //   56: astore_2
        //   57: aload_1
        //   58: monitorexit
        //   59: aload_2
        //   60: athrow
        //   61: astore_2
        //   62: goto -30 -> 32
        //   65: astore_2
        //   66: goto -34 -> 32
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	69	0	this	3
        //   56	4	2	localObject	Object
        //   61	1	2	localIOException	IOException
        //   65	1	2	localzzb	zzn.zzb
        // Exception table:
        //   from	to	target	type
        //   10	29	56	finally
        //   29	31	56	finally
        //   32	53	56	finally
        //   57	59	56	finally
        //   10	29	61	java/io/IOException
        //   10	29	65	com/google/android/gms/cast/internal/zzn$zzb
      }
    });
  }
  
  public PendingResult<MediaChannelResult> stop()
  {
    return stop(null);
  }
  
  public PendingResult<MediaChannelResult> stop(final JSONObject paramJSONObject)
  {
    zzanr();
    zza(new zzb(this.rC)
    {
      /* Error */
      protected void zza(zze arg1)
      {
        // Byte code:
        //   0: aload_0
        //   1: getfield 17	com/google/android/gms/cast/framework/media/RemoteMediaClient$18:rG	Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;
        //   4: invokestatic 33	com/google/android/gms/cast/framework/media/RemoteMediaClient:zzd	(Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;)Ljava/lang/Object;
        //   7: astore_1
        //   8: aload_1
        //   9: monitorenter
        //   10: aload_0
        //   11: getfield 17	com/google/android/gms/cast/framework/media/RemoteMediaClient$18:rG	Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;
        //   14: invokestatic 37	com/google/android/gms/cast/framework/media/RemoteMediaClient:zze	(Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;)Lcom/google/android/gms/cast/internal/zzn;
        //   17: aload_0
        //   18: getfield 41	com/google/android/gms/cast/framework/media/RemoteMediaClient$18:oG	Lcom/google/android/gms/cast/internal/zzp;
        //   21: aload_0
        //   22: getfield 19	com/google/android/gms/cast/framework/media/RemoteMediaClient$18:om	Lorg/json/JSONObject;
        //   25: invokevirtual 47	com/google/android/gms/cast/internal/zzn:zzb	(Lcom/google/android/gms/cast/internal/zzp;Lorg/json/JSONObject;)J
        //   28: pop2
        //   29: aload_1
        //   30: monitorexit
        //   31: return
        //   32: aload_0
        //   33: aload_0
        //   34: new 49	com/google/android/gms/common/api/Status
        //   37: dup
        //   38: sipush 2100
        //   41: invokespecial 52	com/google/android/gms/common/api/Status:<init>	(I)V
        //   44: invokevirtual 56	com/google/android/gms/cast/framework/media/RemoteMediaClient$zzb:zzc	(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Result;
        //   47: checkcast 58	com/google/android/gms/cast/framework/media/RemoteMediaClient$MediaChannelResult
        //   50: invokevirtual 61	com/google/android/gms/cast/framework/media/RemoteMediaClient$18:zzc	(Lcom/google/android/gms/common/api/Result;)V
        //   53: goto -24 -> 29
        //   56: astore_2
        //   57: aload_1
        //   58: monitorexit
        //   59: aload_2
        //   60: athrow
        //   61: astore_2
        //   62: goto -30 -> 32
        //   65: astore_2
        //   66: goto -34 -> 32
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	69	0	this	18
        //   56	4	2	localObject	Object
        //   61	1	2	localIOException	IOException
        //   65	1	2	localzzb	zzn.zzb
        // Exception table:
        //   from	to	target	type
        //   10	29	56	finally
        //   29	31	56	finally
        //   32	53	56	finally
        //   57	59	56	finally
        //   10	29	61	java/io/IOException
        //   10	29	65	com/google/android/gms/cast/internal/zzn$zzb
      }
    });
  }
  
  public void togglePlayback()
  {
    int i = getPlayerState();
    if ((i == 4) || (i == 2))
    {
      pause();
      return;
    }
    play();
  }
  
  public void zzd(GoogleApiClient paramGoogleApiClient)
    throws IOException
  {
    if (this.rC == paramGoogleApiClient) {}
    do
    {
      return;
      if (this.rC != null)
      {
        this.nY.zzapk();
        this.pk.removeMessageReceivedCallbacks(this.rC, getNamespace());
        this.rB.zzc(null);
        this.mHandler.removeCallbacksAndMessages(null);
      }
      this.rC = paramGoogleApiClient;
    } while (this.rC == null);
    this.pk.setMessageReceivedCallbacks(this.rC, getNamespace(), this);
    this.rB.zzc(this.rC);
  }
  
  public static abstract interface Listener
  {
    public abstract void onMetadataUpdated();
    
    public abstract void onPreloadStatusUpdated();
    
    public abstract void onQueueStatusUpdated();
    
    public abstract void onSendingRemoteMediaRequest();
    
    public abstract void onStatusUpdated();
  }
  
  public static abstract interface MediaChannelResult
    extends Result
  {
    public abstract JSONObject getCustomData();
  }
  
  public static abstract interface ParseAdsInfoCallback
  {
    public abstract List<AdBreakInfo> parseAdBreaksFromMediaStatus(MediaStatus paramMediaStatus);
    
    public abstract boolean parseIsPlayingAdFromMediaStatus(MediaStatus paramMediaStatus);
  }
  
  public static abstract interface ProgressListener
  {
    public abstract void onProgressUpdated(long paramLong1, long paramLong2);
  }
  
  private class zza
    implements zzo
  {
    private GoogleApiClient oC;
    private long oD = 0L;
    
    public zza() {}
    
    public void zza(String paramString1, String paramString2, long paramLong, String paramString3)
      throws IOException
    {
      if (this.oC == null) {
        throw new IOException("No GoogleApiClient available");
      }
      paramString3 = RemoteMediaClient.zzb(RemoteMediaClient.this).iterator();
      while (paramString3.hasNext()) {
        ((RemoteMediaClient.Listener)paramString3.next()).onSendingRemoteMediaRequest();
      }
      RemoteMediaClient.zzf(RemoteMediaClient.this).sendMessage(this.oC, paramString1, paramString2).setResultCallback(new zza(paramLong));
    }
    
    public long zzall()
    {
      long l = this.oD + 1L;
      this.oD = l;
      return l;
    }
    
    public void zzc(GoogleApiClient paramGoogleApiClient)
    {
      this.oC = paramGoogleApiClient;
    }
    
    private final class zza
      implements ResultCallback<Status>
    {
      private final long oE;
      
      zza(long paramLong)
      {
        this.oE = paramLong;
      }
      
      public void zzp(@NonNull Status paramStatus)
      {
        if (!paramStatus.isSuccess()) {
          RemoteMediaClient.zze(RemoteMediaClient.this).zzb(this.oE, paramStatus.getStatusCode());
        }
      }
    }
  }
  
  static abstract class zzb
    extends zzb<RemoteMediaClient.MediaChannelResult>
  {
    zzp oG = new zzp()
    {
      public void zza(long paramAnonymousLong, int paramAnonymousInt, Object paramAnonymousObject)
      {
        if ((paramAnonymousObject instanceof JSONObject)) {}
        for (paramAnonymousObject = (JSONObject)paramAnonymousObject;; paramAnonymousObject = null)
        {
          RemoteMediaClient.zzb.this.zzc(new RemoteMediaClient.zzc(new Status(paramAnonymousInt), (JSONObject)paramAnonymousObject));
          return;
        }
      }
      
      public void zzac(long paramAnonymousLong)
      {
        RemoteMediaClient.zzb.this.zzc((RemoteMediaClient.MediaChannelResult)RemoteMediaClient.zzb.this.zzc(new Status(2103)));
      }
    };
    
    zzb(GoogleApiClient paramGoogleApiClient)
    {
      super();
    }
    
    protected void zza(zze paramzze) {}
    
    public RemoteMediaClient.MediaChannelResult zzr(final Status paramStatus)
    {
      new RemoteMediaClient.MediaChannelResult()
      {
        public JSONObject getCustomData()
        {
          return null;
        }
        
        public Status getStatus()
        {
          return paramStatus;
        }
      };
    }
  }
  
  private static final class zzc
    implements RemoteMediaClient.MediaChannelResult
  {
    private final Status hv;
    private final JSONObject np;
    
    zzc(Status paramStatus, JSONObject paramJSONObject)
    {
      this.hv = paramStatus;
      this.np = paramJSONObject;
    }
    
    public JSONObject getCustomData()
    {
      return this.np;
    }
    
    public Status getStatus()
    {
      return this.hv;
    }
  }
  
  private class zzd
  {
    private final Set<RemoteMediaClient.ProgressListener> rK = new HashSet();
    private final long rL;
    private final Runnable rM;
    private boolean rN;
    
    public zzd(long paramLong)
    {
      this.rL = paramLong;
      this.rM = new TimerTask()
      {
        public void run()
        {
          RemoteMediaClient.zza(RemoteMediaClient.this, RemoteMediaClient.zzd.zza(RemoteMediaClient.zzd.this));
          RemoteMediaClient.zzg(RemoteMediaClient.this).postDelayed(this, RemoteMediaClient.zzd.zzb(RemoteMediaClient.zzd.this));
        }
      };
    }
    
    public boolean isStarted()
    {
      return this.rN;
    }
    
    public void start()
    {
      RemoteMediaClient.zzg(RemoteMediaClient.this).removeCallbacks(this.rM);
      this.rN = true;
      RemoteMediaClient.zzg(RemoteMediaClient.this).postDelayed(this.rM, this.rL);
    }
    
    public void stop()
    {
      RemoteMediaClient.zzg(RemoteMediaClient.this).removeCallbacks(this.rM);
      this.rN = false;
    }
    
    public void zza(RemoteMediaClient.ProgressListener paramProgressListener)
    {
      this.rK.add(paramProgressListener);
    }
    
    public long zzanu()
    {
      return this.rL;
    }
    
    public boolean zzanv()
    {
      return !this.rK.isEmpty();
    }
    
    public void zzb(RemoteMediaClient.ProgressListener paramProgressListener)
    {
      this.rK.remove(paramProgressListener);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/framework/media/RemoteMediaClient.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */