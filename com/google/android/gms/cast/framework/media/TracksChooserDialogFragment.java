package com.google.android.gms.cast.framework.media;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;
import com.google.android.gms.R.id;
import com.google.android.gms.R.layout;
import com.google.android.gms.R.string;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaTrack;
import com.google.android.gms.cast.MediaTrack.Builder;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.SessionManager;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TracksChooserDialogFragment
  extends DialogFragment
{
  private Dialog mDialog;
  private long[] nE;
  private List<MediaTrack> rQ;
  private List<MediaTrack> rR;
  
  public static TracksChooserDialogFragment newInstance(MediaInfo paramMediaInfo, long[] paramArrayOfLong)
  {
    if (paramMediaInfo == null) {}
    Object localObject;
    do
    {
      do
      {
        return null;
        localObject = paramMediaInfo.getMediaTracks();
      } while (localObject == null);
      paramMediaInfo = zza((List)localObject, 2);
      localObject = zza((List)localObject, 1);
    } while ((paramMediaInfo.isEmpty()) && (((ArrayList)localObject).isEmpty()));
    TracksChooserDialogFragment localTracksChooserDialogFragment = new TracksChooserDialogFragment();
    Bundle localBundle = new Bundle();
    localBundle.putParcelableArrayList("extra_tracks_type_audio", paramMediaInfo);
    localBundle.putParcelableArrayList("extra_tracks_type_text", (ArrayList)localObject);
    localBundle.putLongArray("extra_active_track_ids", paramArrayOfLong);
    localTracksChooserDialogFragment.setArguments(localBundle);
    return localTracksChooserDialogFragment;
  }
  
  private static int zza(List<MediaTrack> paramList, long[] paramArrayOfLong, int paramInt)
  {
    if ((paramArrayOfLong == null) || (paramList == null)) {}
    for (;;)
    {
      return paramInt;
      int i = 0;
      while (i < paramList.size())
      {
        int k = paramArrayOfLong.length;
        int j = 0;
        while (j < k)
        {
          if (paramArrayOfLong[j] == ((MediaTrack)paramList.get(i)).getId()) {
            return i;
          }
          j += 1;
        }
        i += 1;
      }
    }
  }
  
  @NonNull
  private static ArrayList<MediaTrack> zza(List<MediaTrack> paramList, int paramInt)
  {
    ArrayList localArrayList = new ArrayList();
    if (paramList != null)
    {
      paramList = paramList.iterator();
      while (paramList.hasNext())
      {
        MediaTrack localMediaTrack = (MediaTrack)paramList.next();
        if (localMediaTrack.getType() == paramInt) {
          localArrayList.add(localMediaTrack);
        }
      }
    }
    return localArrayList;
  }
  
  private void zza(View paramView, zzf paramzzf1, zzf paramzzf2)
  {
    ListView localListView2 = (ListView)paramView.findViewById(R.id.text_list_view);
    ListView localListView1 = (ListView)paramView.findViewById(R.id.audio_list_view);
    TextView localTextView2 = (TextView)paramView.findViewById(R.id.text_empty_message);
    TextView localTextView1 = (TextView)paramView.findViewById(R.id.audio_empty_message);
    localListView2.setAdapter(paramzzf1);
    localListView1.setAdapter(paramzzf2);
    paramView = (TabHost)paramView.findViewById(R.id.tab_host);
    paramView.setup();
    TabHost.TabSpec localTabSpec = paramView.newTabSpec("textTab");
    if (paramzzf1.getCount() == 0)
    {
      localListView2.setVisibility(4);
      localTabSpec.setContent(R.id.text_empty_message);
      localTabSpec.setIndicator(getActivity().getString(R.string.cast_tracks_chooser_dialog_subtitles));
      paramView.addTab(localTabSpec);
      paramzzf1 = paramView.newTabSpec("audioTab");
      if (paramzzf2.getCount() != 0) {
        break label194;
      }
      localListView1.setVisibility(4);
      paramzzf1.setContent(R.id.audio_empty_message);
    }
    for (;;)
    {
      paramzzf1.setIndicator(getActivity().getString(R.string.cast_tracks_chooser_dialog_audio));
      paramView.addTab(paramzzf1);
      return;
      localTextView2.setVisibility(4);
      localTabSpec.setContent(R.id.text_list_view);
      break;
      label194:
      localTextView1.setVisibility(4);
      paramzzf1.setContent(R.id.audio_list_view);
    }
  }
  
  private void zza(zzf paramzzf1, zzf paramzzf2)
  {
    ArrayList localArrayList = new ArrayList();
    paramzzf1 = paramzzf1.zzanx();
    if (paramzzf1.getId() != -1L) {
      localArrayList.add(paramzzf1);
    }
    paramzzf1 = paramzzf2.zzanx();
    if (paramzzf1 != null) {
      localArrayList.add(paramzzf1);
    }
    paramzzf1 = CastContext.getSharedInstance(getContext()).getSessionManager().getCurrentCastSession();
    if ((paramzzf1 != null) && (paramzzf1.isConnected()))
    {
      paramzzf1 = paramzzf1.getRemoteMediaClient();
      if (paramzzf1 != null)
      {
        paramzzf2 = new long[localArrayList.size()];
        int i = 0;
        while (i < localArrayList.size())
        {
          paramzzf2[i] = ((MediaTrack)localArrayList.get(i)).getId();
          i += 1;
        }
        paramzzf1.setActiveMediaTracks(paramzzf2);
      }
    }
    if (this.mDialog != null)
    {
      this.mDialog.cancel();
      this.mDialog = null;
    }
  }
  
  private MediaTrack zzanw()
  {
    return new MediaTrack.Builder(-1L, 1).setName(getActivity().getString(R.string.cast_tracks_chooser_dialog_none)).setSubtype(2).setContentId("").build();
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setRetainInstance(true);
    this.rQ = getArguments().getParcelableArrayList("extra_tracks_type_text");
    this.rQ.add(0, zzanw());
    this.rR = getArguments().getParcelableArrayList("extra_tracks_type_audio");
    this.nE = getArguments().getLongArray("extra_active_track_ids");
  }
  
  @NonNull
  public Dialog onCreateDialog(final Bundle paramBundle)
  {
    int i = zza(this.rQ, this.nE, 0);
    int j = zza(this.rR, this.nE, -1);
    paramBundle = new zzf(getActivity(), this.rQ, i);
    final zzf localzzf = new zzf(getActivity(), this.rR, j);
    AlertDialog.Builder localBuilder = new AlertDialog.Builder(getActivity());
    View localView = getActivity().getLayoutInflater().inflate(R.layout.cast_tracks_chooser_dialog_layout, null);
    zza(localView, paramBundle, localzzf);
    localBuilder.setView(localView).setPositiveButton(getActivity().getString(R.string.cast_tracks_chooser_dialog_ok), new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        TracksChooserDialogFragment.zza(TracksChooserDialogFragment.this, paramBundle, localzzf);
      }
    }).setNegativeButton(R.string.cast_tracks_chooser_dialog_cancel, new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        if (TracksChooserDialogFragment.zza(TracksChooserDialogFragment.this) != null)
        {
          TracksChooserDialogFragment.zza(TracksChooserDialogFragment.this).cancel();
          TracksChooserDialogFragment.zza(TracksChooserDialogFragment.this, null);
        }
      }
    });
    if (this.mDialog != null)
    {
      this.mDialog.cancel();
      this.mDialog = null;
    }
    this.mDialog = localBuilder.create();
    return this.mDialog;
  }
  
  public void onDestroyView()
  {
    if ((getDialog() != null) && (getRetainInstance())) {
      getDialog().setDismissMessage(null);
    }
    super.onDestroyView();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/framework/media/TracksChooserDialogFragment.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */