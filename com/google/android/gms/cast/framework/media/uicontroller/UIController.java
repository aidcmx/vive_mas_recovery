package com.google.android.gms.cast.framework.media.uicontroller;

import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;

public abstract class UIController
{
  private RemoteMediaClient pn;
  
  protected RemoteMediaClient getRemoteMediaClient()
  {
    return this.pn;
  }
  
  public void onMediaStatusUpdated() {}
  
  public void onSendingRemoteMediaRequest() {}
  
  public void onSessionConnected(CastSession paramCastSession)
  {
    if (paramCastSession != null)
    {
      this.pn = paramCastSession.getRemoteMediaClient();
      return;
    }
    this.pn = null;
  }
  
  public void onSessionEnded()
  {
    this.pn = null;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/framework/media/uicontroller/UIController.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */