package com.google.android.gms.cast.framework.media.uicontroller;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import com.google.android.gms.R.string;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.Session;
import com.google.android.gms.cast.framework.SessionManager;
import com.google.android.gms.cast.framework.SessionManagerListener;
import com.google.android.gms.cast.framework.media.ImageHints;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.cast.framework.media.RemoteMediaClient.Listener;
import com.google.android.gms.internal.zzow;
import com.google.android.gms.internal.zzox;
import com.google.android.gms.internal.zzoy;
import com.google.android.gms.internal.zzoz;
import com.google.android.gms.internal.zzpb;
import com.google.android.gms.internal.zzpc;
import com.google.android.gms.internal.zzpd;
import com.google.android.gms.internal.zzpe;
import com.google.android.gms.internal.zzpf;
import com.google.android.gms.internal.zzpg;
import com.google.android.gms.internal.zzph;
import com.google.android.gms.internal.zzpi;
import com.google.android.gms.internal.zzpj;
import com.google.android.gms.internal.zzpk;
import com.google.android.gms.internal.zzpl;
import com.google.android.gms.internal.zzpm;
import com.google.android.gms.internal.zzpn;
import com.google.android.gms.internal.zzpo;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class UIMediaController
  implements SessionManagerListener<CastSession>, RemoteMediaClient.Listener
{
  private final Activity mActivity;
  private final SessionManager oX;
  private RemoteMediaClient pn;
  private final Map<View, List<UIController>> st = new HashMap();
  private final Set<zzpm> su = new HashSet();
  private RemoteMediaClient.Listener sv;
  
  public UIMediaController(Activity paramActivity)
  {
    this.mActivity = paramActivity;
    this.oX = CastContext.getSharedInstance(paramActivity).getSessionManager();
    this.oX.addSessionManagerListener(this, CastSession.class);
    zza(this.oX.getCurrentCastSession());
  }
  
  private void zza(View paramView, UIController paramUIController)
  {
    List localList = (List)this.st.get(paramView);
    Object localObject = localList;
    if (localList == null)
    {
      localObject = new ArrayList();
      this.st.put(paramView, localObject);
    }
    ((List)localObject).add(paramUIController);
    if (isActive())
    {
      paramUIController.onSessionConnected(this.oX.getCurrentCastSession());
      zzaog();
    }
  }
  
  private void zza(Session paramSession)
  {
    if ((isActive()) || (!(paramSession instanceof CastSession)) || (!paramSession.isConnected())) {}
    do
    {
      return;
      paramSession = (CastSession)paramSession;
      this.pn = paramSession.getRemoteMediaClient();
    } while (this.pn == null);
    this.pn.addListener(this);
    Iterator localIterator1 = this.st.values().iterator();
    while (localIterator1.hasNext())
    {
      Iterator localIterator2 = ((List)localIterator1.next()).iterator();
      while (localIterator2.hasNext()) {
        ((UIController)localIterator2.next()).onSessionConnected(paramSession);
      }
    }
    zzaog();
  }
  
  private void zzaof()
  {
    if (!isActive()) {
      return;
    }
    Iterator localIterator1 = this.st.values().iterator();
    while (localIterator1.hasNext())
    {
      Iterator localIterator2 = ((List)localIterator1.next()).iterator();
      while (localIterator2.hasNext()) {
        ((UIController)localIterator2.next()).onSessionEnded();
      }
    }
    this.pn.removeListener(this);
    this.pn = null;
  }
  
  private void zzaog()
  {
    Iterator localIterator1 = this.st.values().iterator();
    while (localIterator1.hasNext())
    {
      Iterator localIterator2 = ((List)localIterator1.next()).iterator();
      while (localIterator2.hasNext()) {
        ((UIController)localIterator2.next()).onMediaStatusUpdated();
      }
    }
  }
  
  @Deprecated
  public void bindImageViewToImageOfCurrentItem(ImageView paramImageView, int paramInt1, @DrawableRes int paramInt2)
  {
    zza(paramImageView, new zzoy(paramImageView, this.mActivity, new ImageHints(paramInt1, 0, 0), paramInt2, null));
  }
  
  @Deprecated
  public void bindImageViewToImageOfCurrentItem(ImageView paramImageView, int paramInt, View paramView)
  {
    zza(paramImageView, new zzoy(paramImageView, this.mActivity, new ImageHints(paramInt, 0, 0), 0, paramView));
  }
  
  public void bindImageViewToImageOfCurrentItem(ImageView paramImageView, @NonNull ImageHints paramImageHints, @DrawableRes int paramInt)
  {
    zza(paramImageView, new zzoy(paramImageView, this.mActivity, paramImageHints, paramInt, null));
  }
  
  public void bindImageViewToImageOfCurrentItem(ImageView paramImageView, @NonNull ImageHints paramImageHints, View paramView)
  {
    zza(paramImageView, new zzoy(paramImageView, this.mActivity, paramImageHints, 0, paramView));
  }
  
  @Deprecated
  public void bindImageViewToImageOfPreloadedItem(ImageView paramImageView, int paramInt1, @DrawableRes int paramInt2)
  {
    zza(paramImageView, new zzox(paramImageView, this.mActivity, new ImageHints(paramInt1, 0, 0), paramInt2));
  }
  
  public void bindImageViewToImageOfPreloadedItem(ImageView paramImageView, @NonNull ImageHints paramImageHints, @DrawableRes int paramInt)
  {
    zza(paramImageView, new zzox(paramImageView, this.mActivity, paramImageHints, paramInt));
  }
  
  public void bindImageViewToMuteToggle(ImageView paramImageView)
  {
    zza(paramImageView, new zzpe(paramImageView, this.mActivity));
  }
  
  public void bindImageViewToPlayPauseToggle(@NonNull ImageView paramImageView, @NonNull Drawable paramDrawable1, @NonNull Drawable paramDrawable2, Drawable paramDrawable3, View paramView, boolean paramBoolean)
  {
    zza(paramImageView, new zzpf(paramImageView, this.mActivity, paramDrawable1, paramDrawable2, paramDrawable3, paramView, paramBoolean));
  }
  
  public void bindProgressBar(ProgressBar paramProgressBar)
  {
    bindProgressBar(paramProgressBar, 1000L);
  }
  
  public void bindProgressBar(ProgressBar paramProgressBar, long paramLong)
  {
    zza(paramProgressBar, new zzpg(paramProgressBar, paramLong));
  }
  
  public void bindSeekBar(SeekBar paramSeekBar)
  {
    bindSeekBar(paramSeekBar, 1000L);
  }
  
  public void bindSeekBar(SeekBar paramSeekBar, long paramLong)
  {
    zza(paramSeekBar, new zzph(paramSeekBar, paramLong, new SeekBar.OnSeekBarChangeListener()
    {
      public void onProgressChanged(SeekBar paramAnonymousSeekBar, int paramAnonymousInt, boolean paramAnonymousBoolean)
      {
        if (paramAnonymousBoolean)
        {
          paramAnonymousSeekBar = UIMediaController.zza(UIMediaController.this).iterator();
          while (paramAnonymousSeekBar.hasNext()) {
            ((zzpm)paramAnonymousSeekBar.next()).zzad(paramAnonymousInt);
          }
        }
      }
      
      public void onStartTrackingTouch(SeekBar paramAnonymousSeekBar)
      {
        paramAnonymousSeekBar = UIMediaController.zza(UIMediaController.this).iterator();
        while (paramAnonymousSeekBar.hasNext()) {
          ((zzpm)paramAnonymousSeekBar.next()).zzbo(false);
        }
      }
      
      public void onStopTrackingTouch(SeekBar paramAnonymousSeekBar)
      {
        paramAnonymousSeekBar = UIMediaController.zza(UIMediaController.this).iterator();
        while (paramAnonymousSeekBar.hasNext()) {
          ((zzpm)paramAnonymousSeekBar.next()).zzbo(true);
        }
      }
    }));
  }
  
  public void bindTextViewToMetadataOfCurrentItem(TextView paramTextView, String paramString)
  {
    bindTextViewToMetadataOfCurrentItem(paramTextView, Collections.singletonList(paramString));
  }
  
  public void bindTextViewToMetadataOfCurrentItem(TextView paramTextView, List<String> paramList)
  {
    zza(paramTextView, new zzpd(paramTextView, paramList));
  }
  
  public void bindTextViewToMetadataOfPreloadedItem(TextView paramTextView, String paramString)
  {
    bindTextViewToMetadataOfPreloadedItem(paramTextView, Collections.singletonList(paramString));
  }
  
  public void bindTextViewToMetadataOfPreloadedItem(TextView paramTextView, List<String> paramList)
  {
    zza(paramTextView, new zzpc(paramTextView, paramList));
  }
  
  public void bindTextViewToStreamDuration(TextView paramTextView)
  {
    zza(paramTextView, new zzpl(paramTextView, this.mActivity.getString(R.string.cast_invalid_stream_duration_text), null));
  }
  
  public void bindTextViewToStreamDuration(TextView paramTextView, View paramView)
  {
    zza(paramTextView, new zzpl(paramTextView, this.mActivity.getString(R.string.cast_invalid_stream_duration_text), paramView));
  }
  
  public void bindTextViewToStreamPosition(TextView paramTextView, boolean paramBoolean)
  {
    bindTextViewToStreamPosition(paramTextView, paramBoolean, 1000L);
  }
  
  public void bindTextViewToStreamPosition(TextView paramTextView, boolean paramBoolean, long paramLong)
  {
    zzpm localzzpm = new zzpm(paramTextView, paramLong, this.mActivity.getString(R.string.cast_invalid_stream_position_text));
    if (paramBoolean) {
      this.su.add(localzzpm);
    }
    zza(paramTextView, localzzpm);
  }
  
  public void bindViewToClosedCaption(View paramView)
  {
    zza(paramView, new zzow(paramView, this.mActivity));
  }
  
  public void bindViewToForward(View paramView, long paramLong)
  {
    zza(paramView, new zzpi(paramView, paramLong));
  }
  
  public void bindViewToLaunchExpandedController(View paramView)
  {
    zza(paramView, new zzoz(paramView, this.mActivity));
  }
  
  public void bindViewToLoadingIndicator(View paramView)
  {
    zza(paramView, new zzpb(paramView));
  }
  
  public void bindViewToRewind(View paramView, long paramLong)
  {
    bindViewToForward(paramView, -paramLong);
  }
  
  public void bindViewToSkipNext(View paramView, int paramInt)
  {
    zza(paramView, new zzpj(paramView, paramInt));
  }
  
  public void bindViewToSkipPrev(View paramView, int paramInt)
  {
    zza(paramView, new zzpk(paramView, paramInt));
  }
  
  public void bindViewToUIController(View paramView, UIController paramUIController)
  {
    zza(paramView, paramUIController);
  }
  
  public void bindViewVisibilityToMediaSession(View paramView, int paramInt)
  {
    zza(paramView, new zzpo(paramView, paramInt));
  }
  
  public void bindViewVisibilityToPreloadingEvent(View paramView, int paramInt)
  {
    zza(paramView, new zzpn(paramView, paramInt));
  }
  
  public void dispose()
  {
    zzaof();
    this.st.clear();
    this.oX.removeSessionManagerListener(this, CastSession.class);
    this.sv = null;
  }
  
  public RemoteMediaClient getRemoteMediaClient()
  {
    return this.pn;
  }
  
  public boolean isActive()
  {
    return this.pn != null;
  }
  
  public void onMetadataUpdated()
  {
    zzaog();
    if (this.sv != null) {
      this.sv.onMetadataUpdated();
    }
  }
  
  public void onPreloadStatusUpdated()
  {
    zzaog();
    if (this.sv != null) {
      this.sv.onPreloadStatusUpdated();
    }
  }
  
  public void onQueueStatusUpdated()
  {
    zzaog();
    if (this.sv != null) {
      this.sv.onQueueStatusUpdated();
    }
  }
  
  public void onSendingRemoteMediaRequest()
  {
    Iterator localIterator1 = this.st.values().iterator();
    while (localIterator1.hasNext())
    {
      Iterator localIterator2 = ((List)localIterator1.next()).iterator();
      while (localIterator2.hasNext()) {
        ((UIController)localIterator2.next()).onSendingRemoteMediaRequest();
      }
    }
    if (this.sv != null) {
      this.sv.onSendingRemoteMediaRequest();
    }
  }
  
  public void onSessionEnded(CastSession paramCastSession, int paramInt)
  {
    zzaof();
  }
  
  public void onSessionEnding(CastSession paramCastSession) {}
  
  public void onSessionResumeFailed(CastSession paramCastSession, int paramInt)
  {
    zzaof();
  }
  
  public void onSessionResumed(CastSession paramCastSession, boolean paramBoolean)
  {
    zza(paramCastSession);
  }
  
  public void onSessionResuming(CastSession paramCastSession, String paramString) {}
  
  public void onSessionStartFailed(CastSession paramCastSession, int paramInt)
  {
    zzaof();
  }
  
  public void onSessionStarted(CastSession paramCastSession, String paramString)
  {
    zza(paramCastSession);
  }
  
  public void onSessionStarting(CastSession paramCastSession) {}
  
  public void onSessionSuspended(CastSession paramCastSession, int paramInt) {}
  
  public void onStatusUpdated()
  {
    zzaog();
    if (this.sv != null) {
      this.sv.onStatusUpdated();
    }
  }
  
  public void setPostRemoteMediaClientListener(RemoteMediaClient.Listener paramListener)
  {
    this.sv = paramListener;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/framework/media/uicontroller/UIMediaController.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */