package com.google.android.gms.cast.framework.media.widget;

import android.widget.ImageView;
import com.google.android.gms.cast.framework.media.uicontroller.UIMediaController;

public abstract interface ControlButtonsContainer
{
  public abstract ImageView getButtonImageViewAt(int paramInt)
    throws IndexOutOfBoundsException;
  
  public abstract int getButtonSlotCount();
  
  public abstract int getButtonTypeAt(int paramInt)
    throws IndexOutOfBoundsException;
  
  public abstract UIMediaController getUIMediaController();
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/framework/media/widget/ControlButtonsContainer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */