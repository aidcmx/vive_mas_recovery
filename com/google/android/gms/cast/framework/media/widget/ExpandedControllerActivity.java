package com.google.android.gms.cast.framework.media.widget;

import android.annotation.TargetApi;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.SeekBar;
import android.widget.TextView;
import com.google.android.gms.R.color;
import com.google.android.gms.R.dimen;
import com.google.android.gms.R.drawable;
import com.google.android.gms.R.id;
import com.google.android.gms.R.layout;
import com.google.android.gms.R.string;
import com.google.android.gms.R.style;
import com.google.android.gms.R.styleable;
import com.google.android.gms.cast.CastDevice;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaMetadata;
import com.google.android.gms.cast.MediaStatus;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.SessionManager;
import com.google.android.gms.cast.framework.SessionManagerListener;
import com.google.android.gms.cast.framework.media.ImageHints;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.cast.framework.media.RemoteMediaClient.Listener;
import com.google.android.gms.cast.framework.media.uicontroller.UIMediaController;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.util.zzs;
import com.google.android.gms.internal.zzov;
import com.google.android.gms.internal.zzpa;

public abstract class ExpandedControllerActivity
  extends AppCompatActivity
  implements ControlButtonsContainer
{
  private SessionManager oX;
  private SeekBar sM;
  private final RemoteMediaClient.Listener sv = new zza(null);
  private View tA;
  private UIMediaController tB;
  private final SessionManagerListener<CastSession> tp = new zzb(null);
  @DrawableRes
  private int tq;
  @ColorRes
  private int tr;
  @DrawableRes
  private int ts;
  @DrawableRes
  private int tt;
  private TextView tu;
  private ImageView tv;
  private ImageView tw;
  private zzov tx;
  private int[] ty;
  private ImageView[] tz = new ImageView[4];
  
  private RemoteMediaClient getRemoteMediaClient()
  {
    CastSession localCastSession = this.oX.getCurrentCastSession();
    if ((localCastSession != null) && (localCastSession.isConnected())) {
      return localCastSession.getRemoteMediaClient();
    }
    return null;
  }
  
  private zzov zza(RelativeLayout paramRelativeLayout)
  {
    zzov localzzov = new zzov(this);
    RelativeLayout.LayoutParams localLayoutParams = new RelativeLayout.LayoutParams(-1, -2);
    localLayoutParams.addRule(0, R.id.end_text);
    localLayoutParams.addRule(1, R.id.start_text);
    localLayoutParams.addRule(6, R.id.seek_bar);
    localLayoutParams.addRule(7, R.id.seek_bar);
    localLayoutParams.addRule(5, R.id.seek_bar);
    localLayoutParams.addRule(8, R.id.seek_bar);
    localzzov.setLayoutParams(localLayoutParams);
    localzzov.setPadding(this.sM.getPaddingLeft(), this.sM.getPaddingTop(), this.sM.getPaddingRight(), this.sM.getPaddingBottom());
    localzzov.setContentDescription(getResources().getString(R.string.cast_seek_bar));
    localzzov.setBackgroundColor(0);
    paramRelativeLayout.addView(localzzov);
    return localzzov;
  }
  
  private void zza(Toolbar paramToolbar)
  {
    setSupportActionBar(paramToolbar);
    if (getSupportActionBar() != null)
    {
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      getSupportActionBar().setHomeAsUpIndicator(R.drawable.quantum_ic_keyboard_arrow_down_white_36);
    }
  }
  
  private void zza(View paramView, int paramInt1, int paramInt2, UIMediaController paramUIMediaController)
  {
    paramView = (ImageView)paramView.findViewById(paramInt1);
    if (paramInt2 == R.id.cast_button_type_empty) {
      paramView.setVisibility(4);
    }
    do
    {
      do
      {
        return;
      } while (paramInt2 == R.id.cast_button_type_custom);
      if (paramInt2 == R.id.cast_button_type_play_pause_toggle)
      {
        zza(paramView, paramUIMediaController);
        return;
      }
      if (paramInt2 == R.id.cast_button_type_skip_previous)
      {
        zzb(paramView, paramUIMediaController);
        return;
      }
      if (paramInt2 == R.id.cast_button_type_skip_next)
      {
        zzc(paramView, paramUIMediaController);
        return;
      }
      if (paramInt2 == R.id.cast_button_type_rewind_30_seconds)
      {
        zzd(paramView, paramUIMediaController);
        return;
      }
      if (paramInt2 == R.id.cast_button_type_forward_30_seconds)
      {
        zze(paramView, paramUIMediaController);
        return;
      }
      if (paramInt2 == R.id.cast_button_type_mute_toggle)
      {
        zzf(paramView, paramUIMediaController);
        return;
      }
    } while (paramInt2 != R.id.cast_button_type_closed_caption);
    zzg(paramView, paramUIMediaController);
  }
  
  private void zza(View paramView, UIMediaController paramUIMediaController)
  {
    this.tv = ((ImageView)paramView.findViewById(R.id.background_image_view));
    this.tw = ((ImageView)paramView.findViewById(R.id.blurred_background_image_view));
    Object localObject1 = paramView.findViewById(R.id.background_place_holder_image_view);
    Object localObject2 = new DisplayMetrics();
    getWindowManager().getDefaultDisplay().getMetrics((DisplayMetrics)localObject2);
    paramUIMediaController.bindImageViewToImageOfCurrentItem(this.tv, new ImageHints(4, ((DisplayMetrics)localObject2).widthPixels, ((DisplayMetrics)localObject2).heightPixels), (View)localObject1);
    this.tu = ((TextView)paramView.findViewById(R.id.status_text));
    paramUIMediaController.bindViewToLoadingIndicator((ProgressBar)paramView.findViewById(R.id.loading_indicator));
    localObject1 = (TextView)paramView.findViewById(R.id.start_text);
    localObject2 = (TextView)paramView.findViewById(R.id.end_text);
    ImageView localImageView = (ImageView)paramView.findViewById(R.id.live_stream_indicator);
    this.sM = ((SeekBar)paramView.findViewById(R.id.seek_bar));
    zzaop();
    zzaor();
    SeekBar localSeekBar = (SeekBar)paramView.findViewById(R.id.live_stream_seek_bar);
    paramUIMediaController.bindTextViewToStreamPosition((TextView)localObject1, true);
    paramUIMediaController.bindTextViewToStreamDuration((TextView)localObject2, localImageView);
    paramUIMediaController.bindSeekBar(this.sM);
    paramUIMediaController.bindViewToUIController(localSeekBar, new zzpa(localSeekBar, this.sM));
    this.tz[0] = ((ImageView)paramView.findViewById(R.id.button_0));
    this.tz[1] = ((ImageView)paramView.findViewById(R.id.button_1));
    this.tz[2] = ((ImageView)paramView.findViewById(R.id.button_2));
    this.tz[3] = ((ImageView)paramView.findViewById(R.id.button_3));
    zza(paramView, R.id.button_0, this.ty[0], paramUIMediaController);
    zza(paramView, R.id.button_1, this.ty[1], paramUIMediaController);
    zza(paramView, R.id.button_play_pause_toggle, R.id.cast_button_type_play_pause_toggle, paramUIMediaController);
    zza(paramView, R.id.button_2, this.ty[2], paramUIMediaController);
    zza(paramView, R.id.button_3, this.ty[3], paramUIMediaController);
    this.tA = findViewById(R.id.ad_container);
    this.tx = zza((RelativeLayout)paramView.findViewById(R.id.seek_bar_controls));
  }
  
  private void zza(ImageView paramImageView, UIMediaController paramUIMediaController)
  {
    paramImageView.setBackgroundResource(this.tq);
    Drawable localDrawable1 = getResources().getDrawable(R.drawable.cast_ic_expanded_controller_pause);
    Drawable localDrawable2 = getResources().getDrawable(R.drawable.cast_ic_expanded_controller_play);
    Drawable localDrawable3 = getResources().getDrawable(R.drawable.cast_ic_expanded_controller_stop);
    paramImageView.setImageDrawable(localDrawable2);
    paramUIMediaController.bindImageViewToPlayPauseToggle(paramImageView, localDrawable2, localDrawable1, localDrawable3, null, false);
  }
  
  private void zzaon()
  {
    TypedArray localTypedArray = obtainStyledAttributes(new int[] { android.support.v7.appcompat.R.attr.selectableItemBackgroundBorderless, android.support.v7.appcompat.R.attr.colorControlActivated });
    this.tq = localTypedArray.getResourceId(0, 0);
    this.tr = localTypedArray.getResourceId(1, 0);
    localTypedArray.recycle();
  }
  
  private void zzaoo()
  {
    boolean bool = true;
    TypedArray localTypedArray1 = obtainStyledAttributes(null, R.styleable.CastExpandedController, com.google.android.gms.R.attr.castExpandedControllerStyle, R.style.CastExpandedController);
    this.ts = localTypedArray1.getResourceId(R.styleable.CastExpandedController_castSeekBarProgressDrawable, 0);
    this.tt = localTypedArray1.getResourceId(R.styleable.CastExpandedController_castSeekBarThumbDrawable, 0);
    int i = localTypedArray1.getResourceId(R.styleable.CastExpandedController_castControlButtons, 0);
    if (i != 0)
    {
      TypedArray localTypedArray2 = getResources().obtainTypedArray(i);
      if (localTypedArray2.length() == 4) {}
      for (;;)
      {
        zzaa.zzbt(bool);
        this.ty = new int[localTypedArray2.length()];
        i = 0;
        while (i < localTypedArray2.length())
        {
          this.ty[i] = localTypedArray2.getResourceId(i, 0);
          i += 1;
        }
        bool = false;
      }
      localTypedArray2.recycle();
    }
    for (;;)
    {
      localTypedArray1.recycle();
      return;
      this.ty = new int[] { R.id.cast_button_type_empty, R.id.cast_button_type_empty, R.id.cast_button_type_empty, R.id.cast_button_type_empty };
    }
  }
  
  private void zzaop()
  {
    Drawable localDrawable1 = getResources().getDrawable(this.ts);
    Object localObject1 = null;
    Object localObject2 = null;
    if (localDrawable1 != null)
    {
      localObject1 = localObject2;
      if (this.ts == R.drawable.cast_expanded_controller_seekbar_track)
      {
        localObject1 = zzaoq();
        localObject2 = (LayerDrawable)localDrawable1;
        Drawable localDrawable2 = DrawableCompat.wrap(((LayerDrawable)localObject2).findDrawableByLayerId(16908301));
        DrawableCompat.setTintList(localDrawable2, (ColorStateList)localObject1);
        ((LayerDrawable)localObject2).setDrawableByLayerId(16908301, localDrawable2);
        ((LayerDrawable)localObject2).findDrawableByLayerId(16908288).setColorFilter(getResources().getColor(R.color.cast_expanded_controller_seek_bar_progress_background_tint_color), PorterDuff.Mode.SRC_IN);
      }
      this.sM.setProgressDrawable(localDrawable1);
    }
    localDrawable1 = getResources().getDrawable(this.tt);
    if (localDrawable1 != null)
    {
      localObject2 = localDrawable1;
      if (this.tt == R.drawable.cast_expanded_controller_seekbar_thumb)
      {
        localObject2 = localObject1;
        if (localObject1 == null) {
          localObject2 = zzaoq();
        }
        localObject1 = DrawableCompat.wrap(localDrawable1);
        DrawableCompat.setTintList((Drawable)localObject1, (ColorStateList)localObject2);
        localObject2 = localObject1;
      }
      this.sM.setThumb((Drawable)localObject2);
    }
  }
  
  private ColorStateList zzaoq()
  {
    int i = getResources().getColor(this.tr);
    TypedValue localTypedValue = new TypedValue();
    getResources().getValue(R.dimen.cast_expanded_controller_seekbar_disabled_alpha, localTypedValue, true);
    int j = Color.argb((int)(localTypedValue.getFloat() * Color.alpha(i)), Color.red(i), Color.green(i), Color.blue(i));
    return new ColorStateList(new int[][] { { 16842910 }, { -16842910 } }, new int[] { i, j });
  }
  
  @TargetApi(21)
  private void zzaor()
  {
    if (zzs.zzayx()) {
      this.sM.setSplitTrack(false);
    }
  }
  
  @TargetApi(19)
  private void zzaos()
  {
    if (!zzs.zzayn()) {}
    do
    {
      return;
      int j = getWindow().getDecorView().getSystemUiVisibility();
      int i = j;
      if (zzs.zzayq()) {
        i = j ^ 0x2;
      }
      j = i;
      if (zzs.zzayr()) {
        j = i ^ 0x4;
      }
      i = j;
      if (zzs.zzayu()) {
        i = j ^ 0x1000;
      }
      getWindow().getDecorView().setSystemUiVisibility(i);
    } while (!zzs.zzayt());
    setImmersive(true);
  }
  
  private void zzaot()
  {
    Object localObject = getRemoteMediaClient();
    if ((localObject != null) && (((RemoteMediaClient)localObject).hasMediaSession()))
    {
      localObject = ((RemoteMediaClient)localObject).getMediaInfo();
      if (localObject != null)
      {
        localObject = ((MediaInfo)localObject).getMetadata();
        if (localObject != null)
        {
          ActionBar localActionBar = getSupportActionBar();
          if (localActionBar != null) {
            localActionBar.setTitle(((MediaMetadata)localObject).getString("com.google.android.gms.cast.metadata.TITLE"));
          }
        }
      }
    }
  }
  
  private void zzaou()
  {
    Object localObject = this.oX.getCurrentCastSession();
    if (localObject != null)
    {
      localObject = ((CastSession)localObject).getCastDevice();
      if (localObject != null)
      {
        localObject = ((CastDevice)localObject).getFriendlyName();
        if (!TextUtils.isEmpty((CharSequence)localObject))
        {
          this.tu.setText(getResources().getString(R.string.cast_casting_to_device, new Object[] { localObject }));
          return;
        }
      }
    }
    this.tu.setText("");
  }
  
  private void zzaov()
  {
    RemoteMediaClient localRemoteMediaClient = getRemoteMediaClient();
    int i;
    if (localRemoteMediaClient == null)
    {
      localObject = null;
      if ((localObject == null) || (!((MediaStatus)localObject).isPlayingAd())) {
        break label170;
      }
      i = 1;
      label24:
      if (i == 0) {
        break label175;
      }
      if ((zzs.zzays()) && (this.tw.getVisibility() == 8))
      {
        localObject = this.tv.getDrawable();
        if ((localObject != null) && ((localObject instanceof BitmapDrawable)))
        {
          localObject = ((BitmapDrawable)localObject).getBitmap();
          if (localObject != null)
          {
            localObject = zza.zza(this, (Bitmap)localObject, 0.25F, 7.5F);
            if (localObject != null)
            {
              this.tw.setImageBitmap((Bitmap)localObject);
              this.tw.setVisibility(0);
            }
          }
        }
      }
      this.sM.setEnabled(false);
      this.tA.setVisibility(0);
      label125:
      if (localRemoteMediaClient != null) {
        break label218;
      }
    }
    label170:
    label175:
    label218:
    for (Object localObject = null;; localObject = localRemoteMediaClient.getMediaInfo())
    {
      if (localObject != null)
      {
        this.tx.zzez(this.sM.getMax());
        this.tx.zzb(((MediaInfo)localObject).getAdBreaks(), -1);
      }
      return;
      localObject = localRemoteMediaClient.getMediaStatus();
      break;
      i = 0;
      break label24;
      this.sM.setEnabled(true);
      this.tA.setVisibility(8);
      if (!zzs.zzays()) {
        break label125;
      }
      this.tw.setVisibility(8);
      this.tw.setImageBitmap(null);
      break label125;
    }
  }
  
  private void zzb(ImageView paramImageView, UIMediaController paramUIMediaController)
  {
    paramImageView.setBackgroundResource(this.tq);
    paramImageView.setImageDrawable(getResources().getDrawable(R.drawable.cast_ic_expanded_controller_skip_previous));
    paramImageView.setContentDescription(getResources().getString(R.string.cast_skip_prev));
    paramUIMediaController.bindViewToSkipPrev(paramImageView, 0);
  }
  
  private void zzc(ImageView paramImageView, UIMediaController paramUIMediaController)
  {
    paramImageView.setBackgroundResource(this.tq);
    paramImageView.setImageDrawable(getResources().getDrawable(R.drawable.cast_ic_expanded_controller_skip_next));
    paramImageView.setContentDescription(getResources().getString(R.string.cast_skip_next));
    paramUIMediaController.bindViewToSkipNext(paramImageView, 0);
  }
  
  private void zzd(ImageView paramImageView, UIMediaController paramUIMediaController)
  {
    paramImageView.setBackgroundResource(this.tq);
    paramImageView.setImageDrawable(getResources().getDrawable(R.drawable.cast_ic_expanded_controller_rewind30));
    paramImageView.setContentDescription(getResources().getString(R.string.cast_rewind_30));
    paramUIMediaController.bindViewToRewind(paramImageView, 30000L);
  }
  
  private void zze(ImageView paramImageView, UIMediaController paramUIMediaController)
  {
    paramImageView.setBackgroundResource(this.tq);
    paramImageView.setImageDrawable(getResources().getDrawable(R.drawable.cast_ic_expanded_controller_forward30));
    paramImageView.setContentDescription(getResources().getString(R.string.cast_forward_30));
    paramUIMediaController.bindViewToForward(paramImageView, 30000L);
  }
  
  private void zzf(ImageView paramImageView, UIMediaController paramUIMediaController)
  {
    paramImageView.setBackgroundResource(this.tq);
    paramImageView.setImageDrawable(getResources().getDrawable(R.drawable.cast_ic_expanded_controller_mute));
    paramUIMediaController.bindImageViewToMuteToggle(paramImageView);
  }
  
  private void zzg(ImageView paramImageView, UIMediaController paramUIMediaController)
  {
    paramImageView.setBackgroundResource(this.tq);
    paramImageView.setImageDrawable(getResources().getDrawable(R.drawable.cast_ic_expanded_controller_closed_caption));
    paramUIMediaController.bindViewToClosedCaption(paramImageView);
  }
  
  public final ImageView getButtonImageViewAt(int paramInt)
    throws IndexOutOfBoundsException
  {
    return this.tz[paramInt];
  }
  
  public final int getButtonSlotCount()
  {
    return 4;
  }
  
  public final int getButtonTypeAt(int paramInt)
    throws IndexOutOfBoundsException
  {
    return this.ty[paramInt];
  }
  
  public SeekBar getSeekBar()
  {
    return this.sM;
  }
  
  public TextView getStatusTextView()
  {
    return this.tu;
  }
  
  public UIMediaController getUIMediaController()
  {
    return this.tB;
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    CastContext.getSharedInstance(this).registerLifecycleCallbacksBeforeIceCreamSandwich(this, paramBundle);
    this.oX = CastContext.getSharedInstance(this).getSessionManager();
    if (this.oX.getCurrentCastSession() == null) {
      finish();
    }
    this.tB = new UIMediaController(this);
    this.tB.setPostRemoteMediaClientListener(this.sv);
    setContentView(R.layout.cast_expanded_controller_activity);
    zzaon();
    zzaoo();
    zza(findViewById(R.id.expanded_controller_layout), this.tB);
    zza((Toolbar)findViewById(R.id.toolbar));
    zzaou();
    zzaot();
  }
  
  protected void onDestroy()
  {
    if (this.tB != null)
    {
      this.tB.setPostRemoteMediaClientListener(null);
      this.tB.dispose();
    }
    super.onDestroy();
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    if (paramMenuItem.getItemId() == 16908332) {
      finish();
    }
    return true;
  }
  
  protected void onPause()
  {
    CastContext.getSharedInstance(this).getSessionManager().removeSessionManagerListener(this.tp, CastSession.class);
    super.onPause();
  }
  
  protected void onResume()
  {
    CastContext.getSharedInstance(this).getSessionManager().addSessionManagerListener(this.tp, CastSession.class);
    CastSession localCastSession = CastContext.getSharedInstance(this).getSessionManager().getCurrentCastSession();
    if ((localCastSession == null) || ((!localCastSession.isConnected()) && (!localCastSession.isConnecting()))) {
      finish();
    }
    super.onResume();
  }
  
  public void onWindowFocusChanged(boolean paramBoolean)
  {
    super.onWindowFocusChanged(paramBoolean);
    if (paramBoolean) {
      zzaos();
    }
  }
  
  private class zza
    implements RemoteMediaClient.Listener
  {
    private zza() {}
    
    public void onMetadataUpdated()
    {
      ExpandedControllerActivity.zzd(ExpandedControllerActivity.this);
    }
    
    public void onPreloadStatusUpdated() {}
    
    public void onQueueStatusUpdated() {}
    
    public void onSendingRemoteMediaRequest()
    {
      ExpandedControllerActivity.zze(ExpandedControllerActivity.this).setText(ExpandedControllerActivity.this.getResources().getString(R.string.cast_expanded_controller_loading));
    }
    
    public void onStatusUpdated()
    {
      RemoteMediaClient localRemoteMediaClient = ExpandedControllerActivity.zza(ExpandedControllerActivity.this);
      if ((localRemoteMediaClient == null) || (!localRemoteMediaClient.hasMediaSession()))
      {
        ExpandedControllerActivity.this.finish();
        return;
      }
      ExpandedControllerActivity.zzb(ExpandedControllerActivity.this);
      ExpandedControllerActivity.zzc(ExpandedControllerActivity.this);
    }
  }
  
  private class zzb
    implements SessionManagerListener<CastSession>
  {
    private zzb() {}
    
    public void onSessionEnded(CastSession paramCastSession, int paramInt)
    {
      ExpandedControllerActivity.this.finish();
    }
    
    public void onSessionEnding(CastSession paramCastSession) {}
    
    public void onSessionResumeFailed(CastSession paramCastSession, int paramInt) {}
    
    public void onSessionResumed(CastSession paramCastSession, boolean paramBoolean) {}
    
    public void onSessionResuming(CastSession paramCastSession, String paramString) {}
    
    public void onSessionStartFailed(CastSession paramCastSession, int paramInt) {}
    
    public void onSessionStarted(CastSession paramCastSession, String paramString) {}
    
    public void onSessionStarting(CastSession paramCastSession) {}
    
    public void onSessionSuspended(CastSession paramCastSession, int paramInt) {}
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/framework/media/widget/ExpandedControllerActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */