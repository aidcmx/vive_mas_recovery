package com.google.android.gms.cast.framework.media.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.ColorUtils;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.google.android.gms.R.attr;
import com.google.android.gms.R.dimen;
import com.google.android.gms.R.drawable;
import com.google.android.gms.R.id;
import com.google.android.gms.R.layout;
import com.google.android.gms.R.string;
import com.google.android.gms.R.style;
import com.google.android.gms.R.styleable;
import com.google.android.gms.cast.framework.media.ImageHints;
import com.google.android.gms.cast.framework.media.uicontroller.UIMediaController;
import com.google.android.gms.cast.internal.zzm;
import com.google.android.gms.common.internal.zzaa;

public class MiniControllerFragment
  extends Fragment
  implements ControlButtonsContainer
{
  private static final zzm oT = new zzm("MiniControllerFragment");
  private UIMediaController tB;
  private boolean tD;
  private int tE;
  private int tF;
  private int tG;
  private int tH;
  private int tI;
  private int tJ;
  private int[] ty;
  private ImageView[] tz = new ImageView[3];
  
  private void zza(Context paramContext, AttributeSet paramAttributeSet)
  {
    boolean bool = true;
    int j = 0;
    if (this.ty != null) {
      return;
    }
    paramAttributeSet = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.CastMiniController, R.attr.castMiniControllerStyle, R.style.CastMiniController);
    this.tD = paramAttributeSet.getBoolean(R.styleable.CastMiniController_castShowImageThumbnail, true);
    this.tE = paramAttributeSet.getResourceId(R.styleable.CastMiniController_castTitleTextAppearance, 0);
    this.tF = paramAttributeSet.getResourceId(R.styleable.CastMiniController_castSubtitleTextAppearance, 0);
    this.tG = paramAttributeSet.getResourceId(R.styleable.CastMiniController_castBackground, 0);
    this.tH = paramAttributeSet.getColor(R.styleable.CastMiniController_castProgressBarColor, 0);
    this.tI = paramAttributeSet.getResourceId(R.styleable.CastMiniController_castButtonColor, 0);
    int i = paramAttributeSet.getResourceId(R.styleable.CastMiniController_castControlButtons, 0);
    if (i != 0)
    {
      paramContext = paramContext.getResources().obtainTypedArray(i);
      if (paramContext.length() == 3) {}
      for (;;)
      {
        zzaa.zzbt(bool);
        this.ty = new int[paramContext.length()];
        i = 0;
        while (i < paramContext.length())
        {
          this.ty[i] = paramContext.getResourceId(i, 0);
          i += 1;
        }
        bool = false;
      }
      paramContext.recycle();
      if (this.tD) {
        this.ty[0] = R.id.cast_button_type_empty;
      }
      this.tJ = 0;
      paramContext = this.ty;
      int k = paramContext.length;
      i = j;
      while (i < k)
      {
        if (paramContext[i] != R.id.cast_button_type_empty) {
          this.tJ += 1;
        }
        i += 1;
      }
    }
    oT.zzf("Unable to read attribute castControlButtons.", new Object[0]);
    this.ty = new int[] { R.id.cast_button_type_empty, R.id.cast_button_type_empty, R.id.cast_button_type_empty };
    paramAttributeSet.recycle();
  }
  
  private void zza(RelativeLayout paramRelativeLayout, int paramInt1, int paramInt2)
  {
    ImageView localImageView = (ImageView)paramRelativeLayout.findViewById(paramInt1);
    paramInt2 = this.ty[paramInt2];
    if (paramInt2 == R.id.cast_button_type_empty) {
      localImageView.setVisibility(4);
    }
    while (paramInt2 == R.id.cast_button_type_custom) {
      return;
    }
    int i;
    int j;
    if (paramInt2 == R.id.cast_button_type_play_pause_toggle)
    {
      i = R.drawable.cast_ic_mini_controller_play;
      j = R.drawable.cast_ic_mini_controller_pause;
      paramInt2 = R.drawable.cast_ic_mini_controller_stop;
      if (this.tJ != 1) {
        break label391;
      }
      paramInt2 = R.drawable.cast_ic_mini_controller_play_large;
      j = R.drawable.cast_ic_mini_controller_pause_large;
      int k = R.drawable.cast_ic_mini_controller_stop_large;
      i = paramInt2;
      paramInt2 = k;
    }
    label391:
    for (;;)
    {
      Drawable localDrawable1 = zzfb(i);
      Drawable localDrawable2 = zzfb(j);
      Drawable localDrawable3 = zzfb(paramInt2);
      localImageView.setImageDrawable(localDrawable2);
      ProgressBar localProgressBar = zzfa(paramInt1);
      paramRelativeLayout.addView(localProgressBar);
      this.tB.bindImageViewToPlayPauseToggle(localImageView, localDrawable1, localDrawable2, localDrawable3, localProgressBar, true);
      return;
      if (paramInt2 == R.id.cast_button_type_skip_previous)
      {
        localImageView.setImageDrawable(zzfb(R.drawable.cast_ic_mini_controller_skip_prev));
        localImageView.setContentDescription(getResources().getString(R.string.cast_skip_prev));
        this.tB.bindViewToSkipPrev(localImageView, 0);
        return;
      }
      if (paramInt2 == R.id.cast_button_type_skip_next)
      {
        localImageView.setImageDrawable(zzfb(R.drawable.cast_ic_mini_controller_skip_next));
        localImageView.setContentDescription(getResources().getString(R.string.cast_skip_next));
        this.tB.bindViewToSkipNext(localImageView, 0);
        return;
      }
      if (paramInt2 == R.id.cast_button_type_rewind_30_seconds)
      {
        localImageView.setImageDrawable(zzfb(R.drawable.cast_ic_mini_controller_rewind30));
        localImageView.setContentDescription(getResources().getString(R.string.cast_rewind_30));
        this.tB.bindViewToRewind(localImageView, 30000L);
        return;
      }
      if (paramInt2 == R.id.cast_button_type_forward_30_seconds)
      {
        localImageView.setImageDrawable(zzfb(R.drawable.cast_ic_mini_controller_forward30));
        localImageView.setContentDescription(getResources().getString(R.string.cast_forward_30));
        this.tB.bindViewToForward(localImageView, 30000L);
        return;
      }
      if (paramInt2 == R.id.cast_button_type_mute_toggle)
      {
        localImageView.setImageDrawable(zzfb(R.drawable.cast_ic_mini_controller_mute));
        this.tB.bindImageViewToMuteToggle(localImageView);
        return;
      }
      if (paramInt2 != R.id.cast_button_type_closed_caption) {
        break;
      }
      localImageView.setImageDrawable(zzfb(R.drawable.cast_ic_mini_controller_closed_caption));
      this.tB.bindViewToClosedCaption(localImageView);
      return;
    }
  }
  
  private ProgressBar zzfa(int paramInt)
  {
    ProgressBar localProgressBar = new ProgressBar(getContext());
    Object localObject = new RelativeLayout.LayoutParams(-2, -2);
    ((RelativeLayout.LayoutParams)localObject).addRule(8, paramInt);
    ((RelativeLayout.LayoutParams)localObject).addRule(6, paramInt);
    ((RelativeLayout.LayoutParams)localObject).addRule(5, paramInt);
    ((RelativeLayout.LayoutParams)localObject).addRule(7, paramInt);
    ((RelativeLayout.LayoutParams)localObject).addRule(15);
    localProgressBar.setLayoutParams((ViewGroup.LayoutParams)localObject);
    localProgressBar.setVisibility(8);
    localObject = localProgressBar.getIndeterminateDrawable();
    if ((this.tH != 0) && (localObject != null)) {
      ((Drawable)localObject).setColorFilter(this.tH, PorterDuff.Mode.SRC_IN);
    }
    return localProgressBar;
  }
  
  private Drawable zzfb(@DrawableRes int paramInt)
  {
    Drawable localDrawable = DrawableCompat.wrap(getResources().getDrawable(paramInt).mutate());
    DrawableCompat.setTintMode(localDrawable, PorterDuff.Mode.SRC_IN);
    if (this.tI != 0) {}
    int i;
    for (Object localObject = ContextCompat.getColorStateList(getContext(), this.tI);; localObject = new ColorStateList(new int[][] { { 16842910 }, { -16842910 } }, new int[] { paramInt, i }))
    {
      DrawableCompat.setTintList(localDrawable, (ColorStateList)localObject);
      return localDrawable;
      localObject = getContext().obtainStyledAttributes(new int[] { 16842800 });
      paramInt = ((TypedArray)localObject).getColor(0, 0);
      ((TypedArray)localObject).recycle();
      i = ColorUtils.setAlphaComponent(paramInt, 128);
    }
  }
  
  public final ImageView getButtonImageViewAt(int paramInt)
    throws IndexOutOfBoundsException
  {
    return this.tz[paramInt];
  }
  
  public final int getButtonSlotCount()
  {
    return 3;
  }
  
  public final int getButtonTypeAt(int paramInt)
    throws IndexOutOfBoundsException
  {
    return this.ty[paramInt];
  }
  
  public UIMediaController getUIMediaController()
  {
    return this.tB;
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    this.tB = new UIMediaController(getActivity());
    paramLayoutInflater = paramLayoutInflater.inflate(R.layout.cast_mini_controller, paramViewGroup);
    paramLayoutInflater.setVisibility(8);
    this.tB.bindViewVisibilityToMediaSession(paramLayoutInflater, 8);
    paramViewGroup = (RelativeLayout)paramLayoutInflater.findViewById(R.id.container_current);
    if (this.tG != 0) {
      paramViewGroup.setBackgroundResource(this.tG);
    }
    paramBundle = (ImageView)paramLayoutInflater.findViewById(R.id.icon_view);
    TextView localTextView1 = (TextView)paramLayoutInflater.findViewById(R.id.title_view);
    if (this.tE != 0) {
      localTextView1.setTextAppearance(getActivity(), this.tE);
    }
    TextView localTextView2 = (TextView)paramLayoutInflater.findViewById(R.id.subtitle_view);
    if (this.tF != 0) {
      localTextView2.setTextAppearance(getActivity(), this.tF);
    }
    ProgressBar localProgressBar = (ProgressBar)paramLayoutInflater.findViewById(R.id.progressBar);
    if (this.tH != 0) {
      ((LayerDrawable)localProgressBar.getProgressDrawable()).setColorFilter(this.tH, PorterDuff.Mode.SRC_IN);
    }
    this.tB.bindTextViewToMetadataOfCurrentItem(localTextView1, "com.google.android.gms.cast.metadata.TITLE");
    this.tB.bindTextViewToMetadataOfCurrentItem(localTextView2, "com.google.android.gms.cast.metadata.SUBTITLE");
    this.tB.bindProgressBar(localProgressBar);
    this.tB.bindViewToLaunchExpandedController(paramViewGroup);
    if (this.tD)
    {
      int i = getResources().getDimensionPixelSize(R.dimen.cast_mini_controller_icon_width);
      int j = getResources().getDimensionPixelSize(R.dimen.cast_mini_controller_icon_height);
      this.tB.bindImageViewToImageOfCurrentItem(paramBundle, new ImageHints(2, i, j), R.drawable.cast_album_art_placeholder);
    }
    for (;;)
    {
      this.tz[0] = ((ImageView)paramViewGroup.findViewById(R.id.button_0));
      this.tz[1] = ((ImageView)paramViewGroup.findViewById(R.id.button_1));
      this.tz[2] = ((ImageView)paramViewGroup.findViewById(R.id.button_2));
      zza(paramViewGroup, R.id.button_0, 0);
      zza(paramViewGroup, R.id.button_1, 1);
      zza(paramViewGroup, R.id.button_2, 2);
      return paramLayoutInflater;
      paramBundle.setVisibility(8);
    }
  }
  
  public void onDestroy()
  {
    if (this.tB != null)
    {
      this.tB.dispose();
      this.tB = null;
    }
    super.onDestroy();
  }
  
  public void onInflate(Context paramContext, AttributeSet paramAttributeSet, Bundle paramBundle)
  {
    super.onInflate(paramContext, paramAttributeSet, paramBundle);
    zza(paramContext, paramAttributeSet);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/framework/media/widget/MiniControllerFragment.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */