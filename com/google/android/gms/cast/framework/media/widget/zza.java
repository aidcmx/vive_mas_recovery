package com.google.android.gms.cast.framework.media.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.renderscript.Allocation;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import com.google.android.gms.cast.internal.zzm;

public class zza
{
  private static final zzm oT = new zzm("BlurUtil");
  
  @TargetApi(17)
  public static Bitmap zza(Context paramContext, Bitmap paramBitmap, float paramFloat1, float paramFloat2)
  {
    oT.zzb("Begin blurring bitmap %s, original width = %d, original height = %d.", new Object[] { paramBitmap, Integer.valueOf(paramBitmap.getWidth()), Integer.valueOf(paramBitmap.getHeight()) });
    int i = Math.round(paramBitmap.getWidth() * paramFloat1);
    int j = Math.round(paramBitmap.getHeight() * paramFloat1);
    paramBitmap = Bitmap.createScaledBitmap(paramBitmap, i, j, false);
    Bitmap localBitmap = Bitmap.createBitmap(i, j, paramBitmap.getConfig());
    paramContext = RenderScript.create(paramContext);
    Allocation localAllocation1 = Allocation.createFromBitmap(paramContext, paramBitmap);
    Allocation localAllocation2 = Allocation.createTyped(paramContext, localAllocation1.getType());
    ScriptIntrinsicBlur localScriptIntrinsicBlur = ScriptIntrinsicBlur.create(paramContext, localAllocation1.getElement());
    localScriptIntrinsicBlur.setInput(localAllocation1);
    localScriptIntrinsicBlur.setRadius(paramFloat2);
    localScriptIntrinsicBlur.forEach(localAllocation2);
    localAllocation2.copyTo(localBitmap);
    paramContext.destroy();
    oT.zzb("End blurring bitmap %s, original width = %d, original height = %d.", new Object[] { paramBitmap, Integer.valueOf(i), Integer.valueOf(j) });
    return localBitmap;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/framework/media/widget/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */