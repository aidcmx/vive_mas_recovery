package com.google.android.gms.cast.framework.media;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zza
  implements Parcelable.Creator<CastMediaOptions>
{
  static void zza(CastMediaOptions paramCastMediaOptions, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramCastMediaOptions.getVersionCode());
    zzb.zza(paramParcel, 2, paramCastMediaOptions.getMediaIntentReceiverClassName(), false);
    zzb.zza(paramParcel, 3, paramCastMediaOptions.getExpandedControllerActivityClassName(), false);
    zzb.zza(paramParcel, 4, paramCastMediaOptions.zzana(), false);
    zzb.zza(paramParcel, 5, paramCastMediaOptions.getNotificationOptions(), paramInt, false);
    zzb.zzaj(paramParcel, i);
  }
  
  public CastMediaOptions zzbx(Parcel paramParcel)
  {
    NotificationOptions localNotificationOptions = null;
    int j = com.google.android.gms.common.internal.safeparcel.zza.zzcr(paramParcel);
    int i = 0;
    IBinder localIBinder = null;
    String str1 = null;
    String str2 = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = com.google.android.gms.common.internal.safeparcel.zza.zzcq(paramParcel);
      switch (com.google.android.gms.common.internal.safeparcel.zza.zzgu(k))
      {
      default: 
        com.google.android.gms.common.internal.safeparcel.zza.zzb(paramParcel, k);
        break;
      case 1: 
        i = com.google.android.gms.common.internal.safeparcel.zza.zzg(paramParcel, k);
        break;
      case 2: 
        str2 = com.google.android.gms.common.internal.safeparcel.zza.zzq(paramParcel, k);
        break;
      case 3: 
        str1 = com.google.android.gms.common.internal.safeparcel.zza.zzq(paramParcel, k);
        break;
      case 4: 
        localIBinder = com.google.android.gms.common.internal.safeparcel.zza.zzr(paramParcel, k);
        break;
      case 5: 
        localNotificationOptions = (NotificationOptions)com.google.android.gms.common.internal.safeparcel.zza.zza(paramParcel, k, NotificationOptions.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new CastMediaOptions(i, str2, str1, localIBinder, localNotificationOptions);
  }
  
  public CastMediaOptions[] zzev(int paramInt)
  {
    return new CastMediaOptions[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/framework/media/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */