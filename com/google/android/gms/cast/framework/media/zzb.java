package com.google.android.gms.cast.framework.media;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import com.google.android.gms.cast.MediaMetadata;
import com.google.android.gms.common.images.WebImage;
import com.google.android.gms.dynamic.zzd;
import com.google.android.gms.dynamic.zzd.zza;

public abstract interface zzb
  extends IInterface
{
  public abstract WebImage onPickImage(MediaMetadata paramMediaMetadata, int paramInt)
    throws RemoteException;
  
  public abstract WebImage zza(MediaMetadata paramMediaMetadata, ImageHints paramImageHints)
    throws RemoteException;
  
  public abstract int zzalm()
    throws RemoteException;
  
  public abstract zzd zzanb()
    throws RemoteException;
  
  public static abstract class zza
    extends Binder
    implements zzb
  {
    public zza()
    {
      attachInterface(this, "com.google.android.gms.cast.framework.media.IImagePicker");
    }
    
    public static zzb zzdc(IBinder paramIBinder)
    {
      if (paramIBinder == null) {
        return null;
      }
      IInterface localIInterface = paramIBinder.queryLocalInterface("com.google.android.gms.cast.framework.media.IImagePicker");
      if ((localIInterface != null) && ((localIInterface instanceof zzb))) {
        return (zzb)localIInterface;
      }
      return new zza(paramIBinder);
    }
    
    public IBinder asBinder()
    {
      return this;
    }
    
    public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
      throws RemoteException
    {
      MediaMetadata localMediaMetadata = null;
      switch (paramInt1)
      {
      default: 
        return super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
      case 1598968902: 
        paramParcel2.writeString("com.google.android.gms.cast.framework.media.IImagePicker");
        return true;
      case 3: 
        paramParcel1.enforceInterface("com.google.android.gms.cast.framework.media.IImagePicker");
        paramInt1 = zzalm();
        paramParcel2.writeNoException();
        paramParcel2.writeInt(paramInt1);
        return true;
      case 1: 
        paramParcel1.enforceInterface("com.google.android.gms.cast.framework.media.IImagePicker");
        if (paramParcel1.readInt() != 0)
        {
          localMediaMetadata = (MediaMetadata)MediaMetadata.CREATOR.createFromParcel(paramParcel1);
          paramParcel1 = onPickImage(localMediaMetadata, paramParcel1.readInt());
          paramParcel2.writeNoException();
          if (paramParcel1 == null) {
            break label161;
          }
          paramParcel2.writeInt(1);
          paramParcel1.writeToParcel(paramParcel2, 1);
        }
        for (;;)
        {
          return true;
          localMediaMetadata = null;
          break;
          paramParcel2.writeInt(0);
        }
      case 2: 
        label161:
        paramParcel1.enforceInterface("com.google.android.gms.cast.framework.media.IImagePicker");
        zzd localzzd = zzanb();
        paramParcel2.writeNoException();
        paramParcel1 = localMediaMetadata;
        if (localzzd != null) {
          paramParcel1 = localzzd.asBinder();
        }
        paramParcel2.writeStrongBinder(paramParcel1);
        return true;
      }
      paramParcel1.enforceInterface("com.google.android.gms.cast.framework.media.IImagePicker");
      if (paramParcel1.readInt() != 0)
      {
        localMediaMetadata = (MediaMetadata)MediaMetadata.CREATOR.createFromParcel(paramParcel1);
        if (paramParcel1.readInt() == 0) {
          break label290;
        }
        paramParcel1 = (ImageHints)ImageHints.CREATOR.createFromParcel(paramParcel1);
        label255:
        paramParcel1 = zza(localMediaMetadata, paramParcel1);
        paramParcel2.writeNoException();
        if (paramParcel1 == null) {
          break label295;
        }
        paramParcel2.writeInt(1);
        paramParcel1.writeToParcel(paramParcel2, 1);
      }
      for (;;)
      {
        return true;
        localMediaMetadata = null;
        break;
        label290:
        paramParcel1 = null;
        break label255;
        label295:
        paramParcel2.writeInt(0);
      }
    }
    
    private static class zza
      implements zzb
    {
      private IBinder zzajq;
      
      zza(IBinder paramIBinder)
      {
        this.zzajq = paramIBinder;
      }
      
      public IBinder asBinder()
      {
        return this.zzajq;
      }
      
      public WebImage onPickImage(MediaMetadata paramMediaMetadata, int paramInt)
        throws RemoteException
      {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        for (;;)
        {
          try
          {
            localParcel1.writeInterfaceToken("com.google.android.gms.cast.framework.media.IImagePicker");
            if (paramMediaMetadata != null)
            {
              localParcel1.writeInt(1);
              paramMediaMetadata.writeToParcel(localParcel1, 0);
              localParcel1.writeInt(paramInt);
              this.zzajq.transact(1, localParcel1, localParcel2, 0);
              localParcel2.readException();
              if (localParcel2.readInt() != 0)
              {
                paramMediaMetadata = (WebImage)WebImage.CREATOR.createFromParcel(localParcel2);
                return paramMediaMetadata;
              }
            }
            else
            {
              localParcel1.writeInt(0);
              continue;
            }
            paramMediaMetadata = null;
          }
          finally
          {
            localParcel2.recycle();
            localParcel1.recycle();
          }
        }
      }
      
      public WebImage zza(MediaMetadata paramMediaMetadata, ImageHints paramImageHints)
        throws RemoteException
      {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        for (;;)
        {
          try
          {
            localParcel1.writeInterfaceToken("com.google.android.gms.cast.framework.media.IImagePicker");
            if (paramMediaMetadata != null)
            {
              localParcel1.writeInt(1);
              paramMediaMetadata.writeToParcel(localParcel1, 0);
              if (paramImageHints != null)
              {
                localParcel1.writeInt(1);
                paramImageHints.writeToParcel(localParcel1, 0);
                this.zzajq.transact(4, localParcel1, localParcel2, 0);
                localParcel2.readException();
                if (localParcel2.readInt() == 0) {
                  break label126;
                }
                paramMediaMetadata = (WebImage)WebImage.CREATOR.createFromParcel(localParcel2);
                return paramMediaMetadata;
              }
            }
            else
            {
              localParcel1.writeInt(0);
              continue;
            }
            localParcel1.writeInt(0);
          }
          finally
          {
            localParcel2.recycle();
            localParcel1.recycle();
          }
          continue;
          label126:
          paramMediaMetadata = null;
        }
      }
      
      public int zzalm()
        throws RemoteException
      {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        try
        {
          localParcel1.writeInterfaceToken("com.google.android.gms.cast.framework.media.IImagePicker");
          this.zzajq.transact(3, localParcel1, localParcel2, 0);
          localParcel2.readException();
          int i = localParcel2.readInt();
          return i;
        }
        finally
        {
          localParcel2.recycle();
          localParcel1.recycle();
        }
      }
      
      public zzd zzanb()
        throws RemoteException
      {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        try
        {
          localParcel1.writeInterfaceToken("com.google.android.gms.cast.framework.media.IImagePicker");
          this.zzajq.transact(2, localParcel1, localParcel2, 0);
          localParcel2.readException();
          zzd localzzd = zzd.zza.zzfd(localParcel2.readStrongBinder());
          return localzzd;
        }
        finally
        {
          localParcel2.recycle();
          localParcel1.recycle();
        }
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/framework/media/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */