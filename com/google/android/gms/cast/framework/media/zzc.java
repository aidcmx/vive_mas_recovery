package com.google.android.gms.cast.framework.media;

import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface zzc
  extends IInterface
{
  public abstract IBinder onBind(Intent paramIntent)
    throws RemoteException;
  
  public abstract void onCreate()
    throws RemoteException;
  
  public abstract void onDestroy()
    throws RemoteException;
  
  public abstract int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2)
    throws RemoteException;
  
  public static abstract class zza
    extends Binder
    implements zzc
  {
    public static zzc zzdd(IBinder paramIBinder)
    {
      if (paramIBinder == null) {
        return null;
      }
      IInterface localIInterface = paramIBinder.queryLocalInterface("com.google.android.gms.cast.framework.media.IMediaNotificationService");
      if ((localIInterface != null) && ((localIInterface instanceof zzc))) {
        return (zzc)localIInterface;
      }
      return new zza(paramIBinder);
    }
    
    public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
      throws RemoteException
    {
      Object localObject2 = null;
      Object localObject1 = null;
      switch (paramInt1)
      {
      default: 
        return super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
      case 1598968902: 
        paramParcel2.writeString("com.google.android.gms.cast.framework.media.IMediaNotificationService");
        return true;
      case 1: 
        paramParcel1.enforceInterface("com.google.android.gms.cast.framework.media.IMediaNotificationService");
        onCreate();
        paramParcel2.writeNoException();
        return true;
      case 2: 
        paramParcel1.enforceInterface("com.google.android.gms.cast.framework.media.IMediaNotificationService");
        if (paramParcel1.readInt() != 0) {
          localObject1 = (Intent)Intent.CREATOR.createFromParcel(paramParcel1);
        }
        paramInt1 = onStartCommand((Intent)localObject1, paramParcel1.readInt(), paramParcel1.readInt());
        paramParcel2.writeNoException();
        paramParcel2.writeInt(paramInt1);
        return true;
      case 3: 
        paramParcel1.enforceInterface("com.google.android.gms.cast.framework.media.IMediaNotificationService");
        localObject1 = localObject2;
        if (paramParcel1.readInt() != 0) {
          localObject1 = (Intent)Intent.CREATOR.createFromParcel(paramParcel1);
        }
        paramParcel1 = onBind((Intent)localObject1);
        paramParcel2.writeNoException();
        paramParcel2.writeStrongBinder(paramParcel1);
        return true;
      }
      paramParcel1.enforceInterface("com.google.android.gms.cast.framework.media.IMediaNotificationService");
      onDestroy();
      paramParcel2.writeNoException();
      return true;
    }
    
    private static class zza
      implements zzc
    {
      private IBinder zzajq;
      
      zza(IBinder paramIBinder)
      {
        this.zzajq = paramIBinder;
      }
      
      public IBinder asBinder()
      {
        return this.zzajq;
      }
      
      /* Error */
      public IBinder onBind(Intent paramIntent)
        throws RemoteException
      {
        // Byte code:
        //   0: invokestatic 31	android/os/Parcel:obtain	()Landroid/os/Parcel;
        //   3: astore_2
        //   4: invokestatic 31	android/os/Parcel:obtain	()Landroid/os/Parcel;
        //   7: astore_3
        //   8: aload_2
        //   9: ldc 33
        //   11: invokevirtual 37	android/os/Parcel:writeInterfaceToken	(Ljava/lang/String;)V
        //   14: aload_1
        //   15: ifnull +47 -> 62
        //   18: aload_2
        //   19: iconst_1
        //   20: invokevirtual 41	android/os/Parcel:writeInt	(I)V
        //   23: aload_1
        //   24: aload_2
        //   25: iconst_0
        //   26: invokevirtual 47	android/content/Intent:writeToParcel	(Landroid/os/Parcel;I)V
        //   29: aload_0
        //   30: getfield 18	com/google/android/gms/cast/framework/media/zzc$zza$zza:zzajq	Landroid/os/IBinder;
        //   33: iconst_3
        //   34: aload_2
        //   35: aload_3
        //   36: iconst_0
        //   37: invokeinterface 53 5 0
        //   42: pop
        //   43: aload_3
        //   44: invokevirtual 56	android/os/Parcel:readException	()V
        //   47: aload_3
        //   48: invokevirtual 59	android/os/Parcel:readStrongBinder	()Landroid/os/IBinder;
        //   51: astore_1
        //   52: aload_3
        //   53: invokevirtual 62	android/os/Parcel:recycle	()V
        //   56: aload_2
        //   57: invokevirtual 62	android/os/Parcel:recycle	()V
        //   60: aload_1
        //   61: areturn
        //   62: aload_2
        //   63: iconst_0
        //   64: invokevirtual 41	android/os/Parcel:writeInt	(I)V
        //   67: goto -38 -> 29
        //   70: astore_1
        //   71: aload_3
        //   72: invokevirtual 62	android/os/Parcel:recycle	()V
        //   75: aload_2
        //   76: invokevirtual 62	android/os/Parcel:recycle	()V
        //   79: aload_1
        //   80: athrow
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	81	0	this	zza
        //   0	81	1	paramIntent	Intent
        //   3	73	2	localParcel1	Parcel
        //   7	65	3	localParcel2	Parcel
        // Exception table:
        //   from	to	target	type
        //   8	14	70	finally
        //   18	29	70	finally
        //   29	52	70	finally
        //   62	67	70	finally
      }
      
      public void onCreate()
        throws RemoteException
      {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        try
        {
          localParcel1.writeInterfaceToken("com.google.android.gms.cast.framework.media.IMediaNotificationService");
          this.zzajq.transact(1, localParcel1, localParcel2, 0);
          localParcel2.readException();
          return;
        }
        finally
        {
          localParcel2.recycle();
          localParcel1.recycle();
        }
      }
      
      public void onDestroy()
        throws RemoteException
      {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        try
        {
          localParcel1.writeInterfaceToken("com.google.android.gms.cast.framework.media.IMediaNotificationService");
          this.zzajq.transact(4, localParcel1, localParcel2, 0);
          localParcel2.readException();
          return;
        }
        finally
        {
          localParcel2.recycle();
          localParcel1.recycle();
        }
      }
      
      /* Error */
      public int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2)
        throws RemoteException
      {
        // Byte code:
        //   0: invokestatic 31	android/os/Parcel:obtain	()Landroid/os/Parcel;
        //   3: astore 4
        //   5: invokestatic 31	android/os/Parcel:obtain	()Landroid/os/Parcel;
        //   8: astore 5
        //   10: aload 4
        //   12: ldc 33
        //   14: invokevirtual 37	android/os/Parcel:writeInterfaceToken	(Ljava/lang/String;)V
        //   17: aload_1
        //   18: ifnull +67 -> 85
        //   21: aload 4
        //   23: iconst_1
        //   24: invokevirtual 41	android/os/Parcel:writeInt	(I)V
        //   27: aload_1
        //   28: aload 4
        //   30: iconst_0
        //   31: invokevirtual 47	android/content/Intent:writeToParcel	(Landroid/os/Parcel;I)V
        //   34: aload 4
        //   36: iload_2
        //   37: invokevirtual 41	android/os/Parcel:writeInt	(I)V
        //   40: aload 4
        //   42: iload_3
        //   43: invokevirtual 41	android/os/Parcel:writeInt	(I)V
        //   46: aload_0
        //   47: getfield 18	com/google/android/gms/cast/framework/media/zzc$zza$zza:zzajq	Landroid/os/IBinder;
        //   50: iconst_2
        //   51: aload 4
        //   53: aload 5
        //   55: iconst_0
        //   56: invokeinterface 53 5 0
        //   61: pop
        //   62: aload 5
        //   64: invokevirtual 56	android/os/Parcel:readException	()V
        //   67: aload 5
        //   69: invokevirtual 71	android/os/Parcel:readInt	()I
        //   72: istore_2
        //   73: aload 5
        //   75: invokevirtual 62	android/os/Parcel:recycle	()V
        //   78: aload 4
        //   80: invokevirtual 62	android/os/Parcel:recycle	()V
        //   83: iload_2
        //   84: ireturn
        //   85: aload 4
        //   87: iconst_0
        //   88: invokevirtual 41	android/os/Parcel:writeInt	(I)V
        //   91: goto -57 -> 34
        //   94: astore_1
        //   95: aload 5
        //   97: invokevirtual 62	android/os/Parcel:recycle	()V
        //   100: aload 4
        //   102: invokevirtual 62	android/os/Parcel:recycle	()V
        //   105: aload_1
        //   106: athrow
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	107	0	this	zza
        //   0	107	1	paramIntent	Intent
        //   0	107	2	paramInt1	int
        //   0	107	3	paramInt2	int
        //   3	98	4	localParcel1	Parcel
        //   8	88	5	localParcel2	Parcel
        // Exception table:
        //   from	to	target	type
        //   10	17	94	finally
        //   21	34	94	finally
        //   34	73	94	finally
        //   85	91	94	finally
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/framework/media/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */