package com.google.android.gms.cast.framework.media;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzd
  implements Parcelable.Creator<ImageHints>
{
  static void zza(ImageHints paramImageHints, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramImageHints.getVersionCode());
    zzb.zzc(paramParcel, 2, paramImageHints.getType());
    zzb.zzc(paramParcel, 3, paramImageHints.getWidthInPixels());
    zzb.zzc(paramParcel, 4, paramImageHints.getHeightInPixels());
    zzb.zzaj(paramParcel, paramInt);
  }
  
  public ImageHints zzby(Parcel paramParcel)
  {
    int m = 0;
    int n = zza.zzcr(paramParcel);
    int k = 0;
    int j = 0;
    int i = 0;
    while (paramParcel.dataPosition() < n)
    {
      int i1 = zza.zzcq(paramParcel);
      switch (zza.zzgu(i1))
      {
      default: 
        zza.zzb(paramParcel, i1);
        break;
      case 1: 
        i = zza.zzg(paramParcel, i1);
        break;
      case 2: 
        j = zza.zzg(paramParcel, i1);
        break;
      case 3: 
        k = zza.zzg(paramParcel, i1);
        break;
      case 4: 
        m = zza.zzg(paramParcel, i1);
      }
    }
    if (paramParcel.dataPosition() != n) {
      throw new zza.zza(37 + "Overread allowed size end=" + n, paramParcel);
    }
    return new ImageHints(i, j, k, m);
  }
  
  public ImageHints[] zzew(int paramInt)
  {
    return new ImageHints[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/framework/media/zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */