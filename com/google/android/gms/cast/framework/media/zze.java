package com.google.android.gms.cast.framework.media;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.ArrayList;

public class zze
  implements Parcelable.Creator<NotificationOptions>
{
  static void zza(NotificationOptions paramNotificationOptions, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramNotificationOptions.getVersionCode());
    zzb.zzb(paramParcel, 2, paramNotificationOptions.getActions(), false);
    zzb.zza(paramParcel, 3, paramNotificationOptions.getCompatActionIndices(), false);
    zzb.zza(paramParcel, 4, paramNotificationOptions.getSkipStepMs());
    zzb.zza(paramParcel, 5, paramNotificationOptions.getTargetActivityClassName(), false);
    zzb.zzc(paramParcel, 6, paramNotificationOptions.getSmallIconDrawableResId());
    zzb.zzc(paramParcel, 7, paramNotificationOptions.getStopLiveStreamDrawableResId());
    zzb.zzc(paramParcel, 8, paramNotificationOptions.getPauseDrawableResId());
    zzb.zzc(paramParcel, 9, paramNotificationOptions.getPlayDrawableResId());
    zzb.zzc(paramParcel, 10, paramNotificationOptions.getSkipNextDrawableResId());
    zzb.zzc(paramParcel, 11, paramNotificationOptions.getSkipPrevDrawableResId());
    zzb.zzc(paramParcel, 12, paramNotificationOptions.getForwardDrawableResId());
    zzb.zzc(paramParcel, 13, paramNotificationOptions.getForward10DrawableResId());
    zzb.zzc(paramParcel, 14, paramNotificationOptions.getForward30DrawableResId());
    zzb.zzc(paramParcel, 15, paramNotificationOptions.getRewindDrawableResId());
    zzb.zzc(paramParcel, 16, paramNotificationOptions.getRewind10DrawableResId());
    zzb.zzc(paramParcel, 17, paramNotificationOptions.getRewind30DrawableResId());
    zzb.zzc(paramParcel, 18, paramNotificationOptions.getDisconnectDrawableResId());
    zzb.zzc(paramParcel, 19, paramNotificationOptions.zzand());
    zzb.zzc(paramParcel, 20, paramNotificationOptions.getCastingToDeviceStringResId());
    zzb.zzc(paramParcel, 21, paramNotificationOptions.getStopLiveStreamTitleResId());
    zzb.zzc(paramParcel, 22, paramNotificationOptions.zzane());
    zzb.zzc(paramParcel, 23, paramNotificationOptions.zzanf());
    zzb.zzc(paramParcel, 24, paramNotificationOptions.zzang());
    zzb.zzc(paramParcel, 25, paramNotificationOptions.zzanh());
    zzb.zzc(paramParcel, 26, paramNotificationOptions.zzani());
    zzb.zzc(paramParcel, 27, paramNotificationOptions.zzanj());
    zzb.zzc(paramParcel, 28, paramNotificationOptions.zzank());
    zzb.zzc(paramParcel, 29, paramNotificationOptions.zzanl());
    zzb.zzc(paramParcel, 30, paramNotificationOptions.zzanm());
    zzb.zzc(paramParcel, 31, paramNotificationOptions.zzann());
    zzb.zzc(paramParcel, 32, paramNotificationOptions.zzano());
    zzb.zzaj(paramParcel, paramInt);
  }
  
  public NotificationOptions zzbz(Parcel paramParcel)
  {
    int i24 = zza.zzcr(paramParcel);
    int i23 = 0;
    ArrayList localArrayList = null;
    int[] arrayOfInt = null;
    long l = 0L;
    String str = null;
    int i22 = 0;
    int i21 = 0;
    int i20 = 0;
    int i19 = 0;
    int i18 = 0;
    int i17 = 0;
    int i16 = 0;
    int i15 = 0;
    int i14 = 0;
    int i13 = 0;
    int i12 = 0;
    int i11 = 0;
    int i10 = 0;
    int i9 = 0;
    int i8 = 0;
    int i7 = 0;
    int i6 = 0;
    int i5 = 0;
    int i4 = 0;
    int i3 = 0;
    int i2 = 0;
    int i1 = 0;
    int n = 0;
    int m = 0;
    int k = 0;
    int j = 0;
    int i = 0;
    while (paramParcel.dataPosition() < i24)
    {
      int i25 = zza.zzcq(paramParcel);
      switch (zza.zzgu(i25))
      {
      default: 
        zza.zzb(paramParcel, i25);
        break;
      case 1: 
        i23 = zza.zzg(paramParcel, i25);
        break;
      case 2: 
        localArrayList = zza.zzae(paramParcel, i25);
        break;
      case 3: 
        arrayOfInt = zza.zzw(paramParcel, i25);
        break;
      case 4: 
        l = zza.zzi(paramParcel, i25);
        break;
      case 5: 
        str = zza.zzq(paramParcel, i25);
        break;
      case 6: 
        i22 = zza.zzg(paramParcel, i25);
        break;
      case 7: 
        i21 = zza.zzg(paramParcel, i25);
        break;
      case 8: 
        i20 = zza.zzg(paramParcel, i25);
        break;
      case 9: 
        i19 = zza.zzg(paramParcel, i25);
        break;
      case 10: 
        i18 = zza.zzg(paramParcel, i25);
        break;
      case 11: 
        i17 = zza.zzg(paramParcel, i25);
        break;
      case 12: 
        i16 = zza.zzg(paramParcel, i25);
        break;
      case 13: 
        i15 = zza.zzg(paramParcel, i25);
        break;
      case 14: 
        i14 = zza.zzg(paramParcel, i25);
        break;
      case 15: 
        i13 = zza.zzg(paramParcel, i25);
        break;
      case 16: 
        i12 = zza.zzg(paramParcel, i25);
        break;
      case 17: 
        i11 = zza.zzg(paramParcel, i25);
        break;
      case 18: 
        i10 = zza.zzg(paramParcel, i25);
        break;
      case 19: 
        i9 = zza.zzg(paramParcel, i25);
        break;
      case 20: 
        i8 = zza.zzg(paramParcel, i25);
        break;
      case 21: 
        i7 = zza.zzg(paramParcel, i25);
        break;
      case 22: 
        i6 = zza.zzg(paramParcel, i25);
        break;
      case 23: 
        i5 = zza.zzg(paramParcel, i25);
        break;
      case 24: 
        i4 = zza.zzg(paramParcel, i25);
        break;
      case 25: 
        i3 = zza.zzg(paramParcel, i25);
        break;
      case 26: 
        i2 = zza.zzg(paramParcel, i25);
        break;
      case 27: 
        i1 = zza.zzg(paramParcel, i25);
        break;
      case 28: 
        n = zza.zzg(paramParcel, i25);
        break;
      case 29: 
        m = zza.zzg(paramParcel, i25);
        break;
      case 30: 
        k = zza.zzg(paramParcel, i25);
        break;
      case 31: 
        j = zza.zzg(paramParcel, i25);
        break;
      case 32: 
        i = zza.zzg(paramParcel, i25);
      }
    }
    if (paramParcel.dataPosition() != i24) {
      throw new zza.zza(37 + "Overread allowed size end=" + i24, paramParcel);
    }
    return new NotificationOptions(i23, localArrayList, arrayOfInt, l, str, i22, i21, i20, i19, i18, i17, i16, i15, i14, i13, i12, i11, i10, i9, i8, i7, i6, i5, i4, i3, i2, i1, n, m, k, j, i);
  }
  
  public NotificationOptions[] zzex(int paramInt)
  {
    return new NotificationOptions[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/framework/media/zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */