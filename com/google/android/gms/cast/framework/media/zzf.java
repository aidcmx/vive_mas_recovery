package com.google.android.gms.cast.framework.media;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.TextView;
import com.google.android.gms.R.id;
import com.google.android.gms.R.layout;
import com.google.android.gms.R.string;
import com.google.android.gms.cast.MediaTrack;
import java.util.List;

public class zzf
  extends ArrayAdapter<MediaTrack>
  implements View.OnClickListener
{
  private final Context mContext;
  private int rV;
  
  public zzf(Context paramContext, List<MediaTrack> paramList, int paramInt) {}
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    if (paramView == null)
    {
      paramView = ((LayoutInflater)this.mContext.getSystemService("layout_inflater")).inflate(R.layout.cast_tracks_chooser_dialog_row_layout, paramViewGroup, false);
      paramViewGroup = new zza((TextView)paramView.findViewById(R.id.text), (RadioButton)paramView.findViewById(R.id.radio), null);
      paramView.setTag(paramViewGroup);
    }
    while (paramViewGroup == null)
    {
      return null;
      paramViewGroup = (zza)paramView.getTag();
    }
    paramViewGroup.rX.setTag(Integer.valueOf(paramInt));
    Object localObject = paramViewGroup.rX;
    if (this.rV == paramInt) {}
    for (boolean bool = true;; bool = false)
    {
      ((RadioButton)localObject).setChecked(bool);
      paramView.setOnClickListener(this);
      String str = ((MediaTrack)getItem(paramInt)).getName();
      localObject = str;
      if (TextUtils.isEmpty(str)) {
        localObject = this.mContext.getString(R.string.cast_tracks_chooser_dialog_default_track_name, new Object[] { Integer.valueOf(paramInt) });
      }
      paramViewGroup.rW.setText((CharSequence)localObject);
      return paramView;
    }
  }
  
  public void onClick(View paramView)
  {
    this.rV = ((Integer)((zza)paramView.getTag()).rX.getTag()).intValue();
    notifyDataSetChanged();
  }
  
  public MediaTrack zzanx()
  {
    if ((this.rV >= 0) && (this.rV < getCount())) {
      return (MediaTrack)getItem(this.rV);
    }
    return null;
  }
  
  private class zza
  {
    final TextView rW;
    final RadioButton rX;
    
    private zza(TextView paramTextView, RadioButton paramRadioButton)
    {
      this.rW = paramTextView;
      this.rX = paramRadioButton;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/framework/media/zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */