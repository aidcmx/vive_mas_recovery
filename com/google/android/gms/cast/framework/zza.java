package com.google.android.gms.cast.framework;

import com.google.android.gms.dynamic.zzd;
import com.google.android.gms.dynamic.zze;

public class zza
  extends zze.zza
{
  private final AppVisibilityListener oS;
  
  public zza(AppVisibilityListener paramAppVisibilityListener)
  {
    this.oS = paramAppVisibilityListener;
  }
  
  public void onAppEnteredBackground()
  {
    this.oS.onAppEnteredBackground();
  }
  
  public void onAppEnteredForeground()
  {
    this.oS.onAppEnteredForeground();
  }
  
  public int zzalm()
  {
    return 9877208;
  }
  
  public zzd zzaln()
  {
    return zze.zzac(this.oS);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/framework/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */