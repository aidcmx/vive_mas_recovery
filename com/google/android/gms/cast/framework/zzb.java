package com.google.android.gms.cast.framework;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.cast.LaunchOptions;
import com.google.android.gms.cast.framework.media.CastMediaOptions;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import java.util.ArrayList;

public class zzb
  implements Parcelable.Creator<CastOptions>
{
  static void zza(CastOptions paramCastOptions, Parcel paramParcel, int paramInt)
  {
    int i = com.google.android.gms.common.internal.safeparcel.zzb.zzcs(paramParcel);
    com.google.android.gms.common.internal.safeparcel.zzb.zzc(paramParcel, 1, paramCastOptions.getVersionCode());
    com.google.android.gms.common.internal.safeparcel.zzb.zza(paramParcel, 2, paramCastOptions.getReceiverApplicationId(), false);
    com.google.android.gms.common.internal.safeparcel.zzb.zzb(paramParcel, 3, paramCastOptions.getSupportedNamespaces(), false);
    com.google.android.gms.common.internal.safeparcel.zzb.zza(paramParcel, 4, paramCastOptions.getStopReceiverApplicationWhenEndingSession());
    com.google.android.gms.common.internal.safeparcel.zzb.zza(paramParcel, 5, paramCastOptions.getLaunchOptions(), paramInt, false);
    com.google.android.gms.common.internal.safeparcel.zzb.zza(paramParcel, 6, paramCastOptions.getResumeSavedSession());
    com.google.android.gms.common.internal.safeparcel.zzb.zza(paramParcel, 7, paramCastOptions.getCastMediaOptions(), paramInt, false);
    com.google.android.gms.common.internal.safeparcel.zzb.zza(paramParcel, 8, paramCastOptions.getEnableReconnectionService());
    com.google.android.gms.common.internal.safeparcel.zzb.zza(paramParcel, 9, paramCastOptions.getVolumeDeltaBeforeIceCreamSandwich());
    com.google.android.gms.common.internal.safeparcel.zzb.zzaj(paramParcel, i);
  }
  
  public CastOptions zzbw(Parcel paramParcel)
  {
    CastMediaOptions localCastMediaOptions = null;
    boolean bool1 = false;
    int j = zza.zzcr(paramParcel);
    double d = 0.0D;
    boolean bool2 = false;
    LaunchOptions localLaunchOptions = null;
    boolean bool3 = false;
    ArrayList localArrayList = null;
    String str = null;
    int i = 0;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.zzcq(paramParcel);
      switch (zza.zzgu(k))
      {
      default: 
        zza.zzb(paramParcel, k);
        break;
      case 1: 
        i = zza.zzg(paramParcel, k);
        break;
      case 2: 
        str = zza.zzq(paramParcel, k);
        break;
      case 3: 
        localArrayList = zza.zzae(paramParcel, k);
        break;
      case 4: 
        bool3 = zza.zzc(paramParcel, k);
        break;
      case 5: 
        localLaunchOptions = (LaunchOptions)zza.zza(paramParcel, k, LaunchOptions.CREATOR);
        break;
      case 6: 
        bool2 = zza.zzc(paramParcel, k);
        break;
      case 7: 
        localCastMediaOptions = (CastMediaOptions)zza.zza(paramParcel, k, CastMediaOptions.CREATOR);
        break;
      case 8: 
        bool1 = zza.zzc(paramParcel, k);
        break;
      case 9: 
        d = zza.zzn(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new CastOptions(i, str, localArrayList, bool3, localLaunchOptions, bool2, localCastMediaOptions, bool1, d);
  }
  
  public CastOptions[] zzer(int paramInt)
  {
    return new CastOptions[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/framework/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */