package com.google.android.gms.cast.framework;

import com.google.android.gms.dynamic.zzd;
import com.google.android.gms.dynamic.zze;

public class zzc
  extends zzi.zza
{
  private final CastStateListener ps;
  
  public zzc(CastStateListener paramCastStateListener)
  {
    this.ps = paramCastStateListener;
  }
  
  public void onCastStateChanged(int paramInt)
  {
    this.ps.onCastStateChanged(paramInt);
  }
  
  public int zzalm()
  {
    return 9877208;
  }
  
  public zzd zzaln()
  {
    return zze.zzac(this.ps);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/framework/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */