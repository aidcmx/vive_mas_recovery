package com.google.android.gms.cast.framework;

import android.os.RemoteException;
import com.google.android.gms.cast.internal.zzm;

public class zzd
{
  private static final zzm oT = new zzm("DiscoveryManager");
  private final zzj pt;
  
  zzd(zzj paramzzj)
  {
    this.pt = paramzzj;
  }
  
  public com.google.android.gms.dynamic.zzd zzalq()
  {
    try
    {
      com.google.android.gms.dynamic.zzd localzzd = this.pt.zzalv();
      return localzzd;
    }
    catch (RemoteException localRemoteException)
    {
      oT.zzb(localRemoteException, "Unable to call %s on %s.", new Object[] { "getWrappedThis", zzj.class.getSimpleName() });
    }
    return null;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/framework/zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */