package com.google.android.gms.cast.framework;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.zzd;

public abstract interface zzp
  extends IInterface
{
  public abstract String getCategory()
    throws RemoteException;
  
  public abstract boolean isSessionRecoverable()
    throws RemoteException;
  
  public abstract zzd zzgp(String paramString)
    throws RemoteException;
  
  public static abstract class zza
    extends Binder
    implements zzp
  {
    public zza()
    {
      attachInterface(this, "com.google.android.gms.cast.framework.ISessionProvider");
    }
    
    public IBinder asBinder()
    {
      return this;
    }
    
    public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
      throws RemoteException
    {
      switch (paramInt1)
      {
      default: 
        return super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
      case 1598968902: 
        paramParcel2.writeString("com.google.android.gms.cast.framework.ISessionProvider");
        return true;
      case 4: 
        paramParcel1.enforceInterface("com.google.android.gms.cast.framework.ISessionProvider");
        paramParcel2.writeNoException();
        paramParcel2.writeInt(9877208);
        return true;
      case 1: 
        paramParcel1.enforceInterface("com.google.android.gms.cast.framework.ISessionProvider");
        paramParcel1 = zzgp(paramParcel1.readString());
        paramParcel2.writeNoException();
        if (paramParcel1 != null) {}
        for (paramParcel1 = paramParcel1.asBinder();; paramParcel1 = null)
        {
          paramParcel2.writeStrongBinder(paramParcel1);
          return true;
        }
      case 2: 
        paramParcel1.enforceInterface("com.google.android.gms.cast.framework.ISessionProvider");
        boolean bool = isSessionRecoverable();
        paramParcel2.writeNoException();
        if (bool) {}
        for (paramInt1 = 1;; paramInt1 = 0)
        {
          paramParcel2.writeInt(paramInt1);
          return true;
        }
      }
      paramParcel1.enforceInterface("com.google.android.gms.cast.framework.ISessionProvider");
      paramParcel1 = getCategory();
      paramParcel2.writeNoException();
      paramParcel2.writeString(paramParcel1);
      return true;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/framework/zzp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */