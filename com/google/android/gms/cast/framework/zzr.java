package com.google.android.gms.cast.framework;

import android.os.RemoteException;
import android.support.annotation.NonNull;
import com.google.android.gms.dynamic.zzd;
import com.google.android.gms.dynamic.zze;

public class zzr<T extends Session>
  extends zzo.zza
{
  private final SessionManagerListener<T> pG;
  private final Class<T> pH;
  
  public zzr(@NonNull SessionManagerListener<T> paramSessionManagerListener, @NonNull Class<T> paramClass)
  {
    this.pG = paramSessionManagerListener;
    this.pH = paramClass;
  }
  
  public void zza(@NonNull zzd paramzzd, boolean paramBoolean)
    throws RemoteException
  {
    paramzzd = (Session)zze.zzae(paramzzd);
    if (this.pH.isInstance(paramzzd)) {
      this.pG.onSessionResumed((Session)this.pH.cast(paramzzd), paramBoolean);
    }
  }
  
  public void zzaa(@NonNull zzd paramzzd)
    throws RemoteException
  {
    paramzzd = (Session)zze.zzae(paramzzd);
    if (this.pH.isInstance(paramzzd)) {
      this.pG.onSessionEnding((Session)this.pH.cast(paramzzd));
    }
  }
  
  public int zzalm()
  {
    return 9877208;
  }
  
  public zzd zzaln()
  {
    return zze.zzac(this.pG);
  }
  
  public void zzd(@NonNull zzd paramzzd, int paramInt)
    throws RemoteException
  {
    paramzzd = (Session)zze.zzae(paramzzd);
    if (this.pH.isInstance(paramzzd)) {
      this.pG.onSessionStartFailed((Session)this.pH.cast(paramzzd), paramInt);
    }
  }
  
  public void zzd(@NonNull zzd paramzzd, String paramString)
    throws RemoteException
  {
    paramzzd = (Session)zze.zzae(paramzzd);
    if (this.pH.isInstance(paramzzd)) {
      this.pG.onSessionStarted((Session)this.pH.cast(paramzzd), paramString);
    }
  }
  
  public void zze(@NonNull zzd paramzzd, int paramInt)
    throws RemoteException
  {
    paramzzd = (Session)zze.zzae(paramzzd);
    if (this.pH.isInstance(paramzzd)) {
      this.pG.onSessionEnded((Session)this.pH.cast(paramzzd), paramInt);
    }
  }
  
  public void zze(@NonNull zzd paramzzd, String paramString)
    throws RemoteException
  {
    paramzzd = (Session)zze.zzae(paramzzd);
    if (this.pH.isInstance(paramzzd)) {
      this.pG.onSessionResuming((Session)this.pH.cast(paramzzd), paramString);
    }
  }
  
  public void zzf(@NonNull zzd paramzzd, int paramInt)
    throws RemoteException
  {
    paramzzd = (Session)zze.zzae(paramzzd);
    if (this.pH.isInstance(paramzzd)) {
      this.pG.onSessionResumeFailed((Session)this.pH.cast(paramzzd), paramInt);
    }
  }
  
  public void zzg(@NonNull zzd paramzzd, int paramInt)
    throws RemoteException
  {
    paramzzd = (Session)zze.zzae(paramzzd);
    if (this.pH.isInstance(paramzzd)) {
      this.pG.onSessionSuspended((Session)this.pH.cast(paramzzd), paramInt);
    }
  }
  
  public void zzz(@NonNull zzd paramzzd)
    throws RemoteException
  {
    paramzzd = (Session)zze.zzae(paramzzd);
    if (this.pH.isInstance(paramzzd)) {
      this.pG.onSessionStarting((Session)this.pH.cast(paramzzd));
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/framework/zzr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */