package com.google.android.gms.cast.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.cast.ApplicationMetadata;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;

public class DeviceStatus
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<DeviceStatus> CREATOR = new zzg();
  private final int mVersionCode;
  private double nN;
  private boolean nO;
  private int uN;
  private int uO;
  private ApplicationMetadata uZ;
  
  public DeviceStatus()
  {
    this(3, NaN.0D, false, -1, null, -1);
  }
  
  DeviceStatus(int paramInt1, double paramDouble, boolean paramBoolean, int paramInt2, ApplicationMetadata paramApplicationMetadata, int paramInt3)
  {
    this.mVersionCode = paramInt1;
    this.nN = paramDouble;
    this.nO = paramBoolean;
    this.uN = paramInt2;
    this.uZ = paramApplicationMetadata;
    this.uO = paramInt3;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    do
    {
      return true;
      if (!(paramObject instanceof DeviceStatus)) {
        return false;
      }
      paramObject = (DeviceStatus)paramObject;
    } while ((this.nN == ((DeviceStatus)paramObject).nN) && (this.nO == ((DeviceStatus)paramObject).nO) && (this.uN == ((DeviceStatus)paramObject).uN) && (zzf.zza(this.uZ, ((DeviceStatus)paramObject).uZ)) && (this.uO == ((DeviceStatus)paramObject).uO));
    return false;
  }
  
  public int getActiveInputState()
  {
    return this.uN;
  }
  
  public ApplicationMetadata getApplicationMetadata()
  {
    return this.uZ;
  }
  
  public int getStandbyState()
  {
    return this.uO;
  }
  
  public int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public double getVolume()
  {
    return this.nN;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { Double.valueOf(this.nN), Boolean.valueOf(this.nO), Integer.valueOf(this.uN), this.uZ, Integer.valueOf(this.uO) });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzg.zza(this, paramParcel, paramInt);
  }
  
  public boolean zzapv()
  {
    return this.nO;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/internal/DeviceStatus.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */