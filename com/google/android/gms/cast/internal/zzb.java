package com.google.android.gms.cast.internal;

import com.google.android.gms.cast.Cast;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.internal.zzqo.zza;

public abstract class zzb<R extends Result>
  extends zzqo.zza<R, zze>
{
  public zzb(GoogleApiClient paramGoogleApiClient)
  {
    super(Cast.API, paramGoogleApiClient);
  }
  
  public void zze(int paramInt, String paramString)
  {
    zzc(zzc(new Status(paramInt, paramString, null)));
  }
  
  public void zzff(int paramInt)
  {
    zzc(zzc(new Status(paramInt)));
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/internal/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */