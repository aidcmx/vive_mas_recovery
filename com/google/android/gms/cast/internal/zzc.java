package com.google.android.gms.cast.internal;

import android.os.Handler;
import android.os.Looper;
import com.google.android.gms.common.util.zze;
import com.google.android.gms.common.util.zzh;

public abstract class zzc
  extends zzd
{
  protected final Handler mHandler = new Handler(Looper.getMainLooper());
  protected final long uA;
  protected final Runnable uB;
  protected boolean uC;
  protected final zze zzaql;
  
  public zzc(String paramString1, zze paramzze, String paramString2, String paramString3)
  {
    this(paramString1, paramzze, paramString2, paramString3, 1000L);
  }
  
  public zzc(String paramString1, zze paramzze, String paramString2, String paramString3, long paramLong)
  {
    super(paramString1, paramString2, paramString3);
    this.zzaql = paramzze;
    this.uB = new zza(null);
    this.uA = paramLong;
    zzbp(false);
  }
  
  public zzc(String paramString1, String paramString2, String paramString3)
  {
    this(paramString1, zzh.zzayl(), paramString2, paramString3);
  }
  
  protected abstract boolean zzae(long paramLong);
  
  public void zzapk()
  {
    zzbp(false);
  }
  
  protected final void zzbp(boolean paramBoolean)
  {
    if (this.uC != paramBoolean)
    {
      this.uC = paramBoolean;
      if (paramBoolean) {
        this.mHandler.postDelayed(this.uB, this.uA);
      }
    }
    else
    {
      return;
    }
    this.mHandler.removeCallbacks(this.uB);
  }
  
  private class zza
    implements Runnable
  {
    private zza() {}
    
    public void run()
    {
      zzc.this.uC = false;
      long l = zzc.this.zzaql.elapsedRealtime();
      boolean bool = zzc.this.zzae(l);
      zzc.this.zzbp(bool);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/internal/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */