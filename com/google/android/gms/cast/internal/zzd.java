package com.google.android.gms.cast.internal;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import java.io.IOException;

public abstract class zzd
{
  private final String lf;
  protected final zzm oT;
  private zzo uE;
  
  protected zzd(String paramString1, String paramString2, String paramString3)
  {
    zzf.zzgu(paramString1);
    this.lf = paramString1;
    this.oT = new zzm(paramString2);
    setSessionLabel(paramString3);
  }
  
  public String getNamespace()
  {
    return this.lf;
  }
  
  public void setSessionLabel(String paramString)
  {
    if (!TextUtils.isEmpty(paramString)) {
      this.oT.zzgz(paramString);
    }
  }
  
  public final void zza(zzo paramzzo)
  {
    this.uE = paramzzo;
    if (this.uE == null) {
      zzapk();
    }
  }
  
  protected final void zza(String paramString1, long paramLong, String paramString2)
    throws IOException
  {
    this.oT.zza("Sending text message: %s to: %s", new Object[] { paramString1, paramString2 });
    this.uE.zza(this.lf, paramString1, paramLong, paramString2);
  }
  
  public void zzapk() {}
  
  protected final long zzapl()
  {
    return this.uE.zzall();
  }
  
  public void zzb(long paramLong, int paramInt) {}
  
  public void zzgt(@NonNull String paramString) {}
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/internal/zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */