package com.google.android.gms.cast.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.google.android.gms.cast.ApplicationMetadata;
import com.google.android.gms.cast.Cast.ApplicationConnectionResult;
import com.google.android.gms.cast.Cast.Listener;
import com.google.android.gms.cast.Cast.MessageReceivedCallback;
import com.google.android.gms.cast.CastDevice;
import com.google.android.gms.cast.JoinOptions;
import com.google.android.gms.cast.LaunchOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.BinderWrapper;
import com.google.android.gms.internal.zzqo.zzb;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

public class zze
  extends com.google.android.gms.common.internal.zzj<zzj>
{
  private static final zzm mz = new zzm("CastClientImpl");
  private static final Object uX = new Object();
  private static final Object uY = new Object();
  private final Cast.Listener mh;
  private double nN;
  private boolean nO;
  private final CastDevice po;
  private ApplicationMetadata uF;
  private final Map<String, Cast.MessageReceivedCallback> uG;
  private final long uH;
  private zzb uI;
  private String uJ;
  private boolean uK;
  private boolean uL;
  private boolean uM;
  private int uN;
  private int uO;
  private final AtomicLong uP;
  private String uQ;
  private String uR;
  private Bundle uS;
  private final Map<Long, zzqo.zzb<Status>> uT;
  private zzj uU;
  private zzqo.zzb<Cast.ApplicationConnectionResult> uV;
  private zzqo.zzb<Status> uW;
  
  public zze(Context paramContext, Looper paramLooper, com.google.android.gms.common.internal.zzf paramzzf, CastDevice paramCastDevice, long paramLong, Cast.Listener paramListener, GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
  {
    super(paramContext, paramLooper, 10, paramzzf, paramConnectionCallbacks, paramOnConnectionFailedListener);
    this.po = paramCastDevice;
    this.mh = paramListener;
    this.uH = paramLong;
    this.uG = new HashMap();
    this.uP = new AtomicLong(0L);
    this.uT = new HashMap();
    zzapm();
  }
  
  private void zza(ApplicationStatus paramApplicationStatus)
  {
    paramApplicationStatus = paramApplicationStatus.zzapj();
    if (!zzf.zza(paramApplicationStatus, this.uJ)) {
      this.uJ = paramApplicationStatus;
    }
    for (boolean bool = true;; bool = false)
    {
      mz.zzb("hasChanged=%b, mFirstApplicationStatusUpdate=%b", new Object[] { Boolean.valueOf(bool), Boolean.valueOf(this.uK) });
      if ((this.mh != null) && ((bool) || (this.uK))) {
        this.mh.onApplicationStatusChanged();
      }
      this.uK = false;
      return;
    }
  }
  
  private void zza(DeviceStatus paramDeviceStatus)
  {
    ApplicationMetadata localApplicationMetadata = paramDeviceStatus.getApplicationMetadata();
    if (!zzf.zza(localApplicationMetadata, this.uF))
    {
      this.uF = localApplicationMetadata;
      this.mh.onApplicationMetadataChanged(this.uF);
    }
    double d = paramDeviceStatus.getVolume();
    if ((!Double.isNaN(d)) && (Math.abs(d - this.nN) > 1.0E-7D)) {
      this.nN = d;
    }
    for (boolean bool1 = true;; bool1 = false)
    {
      boolean bool2 = paramDeviceStatus.zzapv();
      if (bool2 != this.nO)
      {
        this.nO = bool2;
        bool1 = true;
      }
      mz.zzb("hasVolumeChanged=%b, mFirstDeviceStatusUpdate=%b", new Object[] { Boolean.valueOf(bool1), Boolean.valueOf(this.uL) });
      if ((this.mh != null) && ((bool1) || (this.uL))) {
        this.mh.onVolumeChanged();
      }
      int i = paramDeviceStatus.getActiveInputState();
      if (i != this.uN) {
        this.uN = i;
      }
      for (bool1 = true;; bool1 = false)
      {
        mz.zzb("hasActiveInputChanged=%b, mFirstDeviceStatusUpdate=%b", new Object[] { Boolean.valueOf(bool1), Boolean.valueOf(this.uL) });
        if ((this.mh != null) && ((bool1) || (this.uL))) {
          this.mh.onActiveInputStateChanged(this.uN);
        }
        i = paramDeviceStatus.getStandbyState();
        if (i != this.uO) {
          this.uO = i;
        }
        for (bool1 = true;; bool1 = false)
        {
          mz.zzb("hasStandbyStateChanged=%b, mFirstDeviceStatusUpdate=%b", new Object[] { Boolean.valueOf(bool1), Boolean.valueOf(this.uL) });
          if ((this.mh != null) && ((bool1) || (this.uL))) {
            this.mh.onStandbyStateChanged(this.uO);
          }
          this.uL = false;
          return;
        }
      }
    }
  }
  
  private void zza(zzqo.zzb<Cast.ApplicationConnectionResult> paramzzb)
  {
    synchronized (uX)
    {
      if (this.uV != null) {
        this.uV.setResult(new zza(new Status(2002)));
      }
      this.uV = paramzzb;
      return;
    }
  }
  
  private void zzapm()
  {
    this.uM = false;
    this.uN = -1;
    this.uO = -1;
    this.uF = null;
    this.uJ = null;
    this.nN = 0.0D;
    this.nO = false;
  }
  
  private void zzapp()
  {
    mz.zzb("removing all MessageReceivedCallbacks", new Object[0]);
    synchronized (this.uG)
    {
      this.uG.clear();
      return;
    }
  }
  
  private void zzc(zzqo.zzb<Status> paramzzb)
  {
    synchronized (uY)
    {
      if (this.uW != null)
      {
        paramzzb.setResult(new Status(2001));
        return;
      }
      this.uW = paramzzb;
      return;
    }
  }
  
  /* Error */
  public void disconnect()
  {
    // Byte code:
    //   0: getstatic 76	com/google/android/gms/cast/internal/zze:mz	Lcom/google/android/gms/cast/internal/zzm;
    //   3: ldc_w 279
    //   6: iconst_2
    //   7: anewarray 78	java/lang/Object
    //   10: dup
    //   11: iconst_0
    //   12: aload_0
    //   13: getfield 281	com/google/android/gms/cast/internal/zze:uI	Lcom/google/android/gms/cast/internal/zze$zzb;
    //   16: aastore
    //   17: dup
    //   18: iconst_1
    //   19: aload_0
    //   20: invokevirtual 284	com/google/android/gms/cast/internal/zze:isConnected	()Z
    //   23: invokestatic 143	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   26: aastore
    //   27: invokevirtual 148	com/google/android/gms/cast/internal/zzm:zzb	(Ljava/lang/String;[Ljava/lang/Object;)V
    //   30: aload_0
    //   31: getfield 281	com/google/android/gms/cast/internal/zze:uI	Lcom/google/android/gms/cast/internal/zze$zzb;
    //   34: astore_1
    //   35: aload_0
    //   36: aconst_null
    //   37: putfield 281	com/google/android/gms/cast/internal/zze:uI	Lcom/google/android/gms/cast/internal/zze$zzb;
    //   40: aload_1
    //   41: ifnull +10 -> 51
    //   44: aload_1
    //   45: invokevirtual 288	com/google/android/gms/cast/internal/zze$zzb:zzapu	()Lcom/google/android/gms/cast/internal/zze;
    //   48: ifnonnull +17 -> 65
    //   51: getstatic 76	com/google/android/gms/cast/internal/zze:mz	Lcom/google/android/gms/cast/internal/zzm;
    //   54: ldc_w 290
    //   57: iconst_0
    //   58: anewarray 78	java/lang/Object
    //   61: invokevirtual 148	com/google/android/gms/cast/internal/zzm:zzb	(Ljava/lang/String;[Ljava/lang/Object;)V
    //   64: return
    //   65: aload_0
    //   66: invokespecial 292	com/google/android/gms/cast/internal/zze:zzapp	()V
    //   69: aload_0
    //   70: invokevirtual 296	com/google/android/gms/cast/internal/zze:zzapo	()Lcom/google/android/gms/cast/internal/zzj;
    //   73: invokeinterface 300 1 0
    //   78: aload_0
    //   79: invokespecial 301	com/google/android/gms/common/internal/zzj:disconnect	()V
    //   82: return
    //   83: astore_1
    //   84: getstatic 76	com/google/android/gms/cast/internal/zze:mz	Lcom/google/android/gms/cast/internal/zzm;
    //   87: aload_1
    //   88: ldc_w 303
    //   91: iconst_1
    //   92: anewarray 78	java/lang/Object
    //   95: dup
    //   96: iconst_0
    //   97: aload_1
    //   98: invokevirtual 308	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   101: aastore
    //   102: invokevirtual 311	com/google/android/gms/cast/internal/zzm:zzb	(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    //   105: aload_0
    //   106: invokespecial 301	com/google/android/gms/common/internal/zzj:disconnect	()V
    //   109: return
    //   110: astore_1
    //   111: aload_0
    //   112: invokespecial 301	com/google/android/gms/common/internal/zzj:disconnect	()V
    //   115: aload_1
    //   116: athrow
    //   117: astore_1
    //   118: goto -34 -> 84
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	121	0	this	zze
    //   34	11	1	localzzb	zzb
    //   83	15	1	localRemoteException	RemoteException
    //   110	6	1	localObject	Object
    //   117	1	1	localIllegalStateException	IllegalStateException
    // Exception table:
    //   from	to	target	type
    //   69	78	83	android/os/RemoteException
    //   69	78	110	finally
    //   84	105	110	finally
    //   69	78	117	java/lang/IllegalStateException
  }
  
  public int getActiveInputState()
    throws IllegalStateException
  {
    zzapq();
    return this.uN;
  }
  
  public ApplicationMetadata getApplicationMetadata()
    throws IllegalStateException
  {
    zzapq();
    return this.uF;
  }
  
  public String getApplicationStatus()
    throws IllegalStateException
  {
    zzapq();
    return this.uJ;
  }
  
  public int getStandbyState()
    throws IllegalStateException
  {
    zzapq();
    return this.uO;
  }
  
  public double getVolume()
    throws IllegalStateException
  {
    zzapq();
    return this.nN;
  }
  
  public boolean isMute()
    throws IllegalStateException
  {
    zzapq();
    return this.nO;
  }
  
  public void onConnectionFailed(ConnectionResult paramConnectionResult)
  {
    super.onConnectionFailed(paramConnectionResult);
    zzapp();
  }
  
  public void removeMessageReceivedCallbacks(String paramString)
    throws IllegalArgumentException, RemoteException
  {
    if (TextUtils.isEmpty(paramString)) {
      throw new IllegalArgumentException("Channel namespace cannot be null or empty");
    }
    synchronized (this.uG)
    {
      Cast.MessageReceivedCallback localMessageReceivedCallback = (Cast.MessageReceivedCallback)this.uG.remove(paramString);
      if (localMessageReceivedCallback == null) {}
    }
  }
  
  public void requestStatus()
    throws IllegalStateException, RemoteException
  {
    zzapo().requestStatus();
  }
  
  public void setMessageReceivedCallbacks(String paramString, Cast.MessageReceivedCallback paramMessageReceivedCallback)
    throws IllegalArgumentException, IllegalStateException, RemoteException
  {
    zzf.zzgu(paramString);
    removeMessageReceivedCallbacks(paramString);
    if (paramMessageReceivedCallback != null) {}
    synchronized (this.uG)
    {
      this.uG.put(paramString, paramMessageReceivedCallback);
      zzapo().zzgx(paramString);
      return;
    }
  }
  
  public void setMute(boolean paramBoolean)
    throws IllegalStateException, RemoteException
  {
    zzapo().zza(paramBoolean, this.nN, this.nO);
  }
  
  public void setVolume(double paramDouble)
    throws IllegalArgumentException, IllegalStateException, RemoteException
  {
    if ((Double.isInfinite(paramDouble)) || (Double.isNaN(paramDouble))) {
      throw new IllegalArgumentException(41 + "Volume cannot be " + paramDouble);
    }
    zzapo().zza(paramDouble, this.nN, this.nO);
  }
  
  protected void zza(int paramInt1, IBinder paramIBinder, Bundle paramBundle, int paramInt2)
  {
    mz.zzb("in onPostInitHandler; statusCode=%d", new Object[] { Integer.valueOf(paramInt1) });
    if ((paramInt1 == 0) || (paramInt1 == 1001))
    {
      this.uM = true;
      this.uK = true;
      this.uL = true;
    }
    for (;;)
    {
      int i = paramInt1;
      if (paramInt1 == 1001)
      {
        this.uS = new Bundle();
        this.uS.putBoolean("com.google.android.gms.cast.EXTRA_APP_NO_LONGER_RUNNING", true);
        i = 0;
      }
      super.zza(i, paramIBinder, paramBundle, paramInt2);
      return;
      this.uM = false;
    }
  }
  
  public void zza(String paramString, LaunchOptions paramLaunchOptions, zzqo.zzb<Cast.ApplicationConnectionResult> paramzzb)
    throws IllegalStateException, RemoteException
  {
    zza(paramzzb);
    zzapo().zzb(paramString, paramLaunchOptions);
  }
  
  public void zza(String paramString, zzqo.zzb<Status> paramzzb)
    throws IllegalStateException, RemoteException
  {
    zzc(paramzzb);
    zzapo().zzgo(paramString);
  }
  
  public void zza(String paramString1, String paramString2, JoinOptions paramJoinOptions, zzqo.zzb<Cast.ApplicationConnectionResult> paramzzb)
    throws IllegalStateException, RemoteException
  {
    zza(paramzzb);
    paramzzb = paramJoinOptions;
    if (paramJoinOptions == null) {
      paramzzb = new JoinOptions();
    }
    zzapo().zza(paramString1, paramString2, paramzzb);
  }
  
  public void zza(String paramString1, String paramString2, zzqo.zzb<Status> paramzzb)
    throws IllegalArgumentException, IllegalStateException, RemoteException
  {
    if (TextUtils.isEmpty(paramString2)) {
      throw new IllegalArgumentException("The message payload cannot be null or empty");
    }
    if (paramString2.length() > 65536) {
      throw new IllegalArgumentException("Message exceeds maximum size");
    }
    zzf.zzgu(paramString1);
    zzapq();
    long l = this.uP.incrementAndGet();
    try
    {
      this.uT.put(Long.valueOf(l), paramzzb);
      zzapo().zzb(paramString1, paramString2, l);
      return;
    }
    catch (Throwable paramString1)
    {
      this.uT.remove(Long.valueOf(l));
      throw paramString1;
    }
  }
  
  public void zza(String paramString, boolean paramBoolean, zzqo.zzb<Cast.ApplicationConnectionResult> paramzzb)
    throws IllegalStateException, RemoteException
  {
    LaunchOptions localLaunchOptions = new LaunchOptions();
    localLaunchOptions.setRelaunchIfRunning(paramBoolean);
    zza(paramString, localLaunchOptions, paramzzb);
  }
  
  protected Bundle zzahv()
  {
    Bundle localBundle = new Bundle();
    mz.zzb("getRemoteService(): mLastApplicationId=%s, mLastSessionId=%s", new Object[] { this.uQ, this.uR });
    this.po.putInBundle(localBundle);
    localBundle.putLong("com.google.android.gms.cast.EXTRA_CAST_FLAGS", this.uH);
    this.uI = new zzb(this);
    localBundle.putParcelable("listener", new BinderWrapper(this.uI.asBinder()));
    if (this.uQ != null)
    {
      localBundle.putString("last_application_id", this.uQ);
      if (this.uR != null) {
        localBundle.putString("last_session_id", this.uR);
      }
    }
    return localBundle;
  }
  
  public Bundle zzapn()
  {
    if (this.uS != null)
    {
      Bundle localBundle = this.uS;
      this.uS = null;
      return localBundle;
    }
    return super.zzapn();
  }
  
  zzj zzapo()
    throws DeadObjectException
  {
    if (0 == 0) {
      return (zzj)super.zzavg();
    }
    return this.uU;
  }
  
  void zzapq()
    throws IllegalStateException
  {
    if ((!this.uM) || (this.uI == null) || (this.uI.isDisposed())) {
      throw new IllegalStateException("Not connected to a device");
    }
  }
  
  public void zzb(zzqo.zzb<Status> paramzzb)
    throws IllegalStateException, RemoteException
  {
    zzc(paramzzb);
    zzapo().zzapw();
  }
  
  protected zzj zzdg(IBinder paramIBinder)
  {
    return zzj.zza.zzdh(paramIBinder);
  }
  
  @NonNull
  protected String zzjx()
  {
    return "com.google.android.gms.cast.service.BIND_CAST_DEVICE_CONTROLLER_SERVICE";
  }
  
  @NonNull
  protected String zzjy()
  {
    return "com.google.android.gms.cast.internal.ICastDeviceController";
  }
  
  static final class zza
    implements Cast.ApplicationConnectionResult
  {
    private final Status hv;
    private final ApplicationMetadata uZ;
    private final String va;
    private final boolean vb;
    private final String zzctq;
    
    public zza(Status paramStatus)
    {
      this(paramStatus, null, null, null, false);
    }
    
    public zza(Status paramStatus, ApplicationMetadata paramApplicationMetadata, String paramString1, String paramString2, boolean paramBoolean)
    {
      this.hv = paramStatus;
      this.uZ = paramApplicationMetadata;
      this.va = paramString1;
      this.zzctq = paramString2;
      this.vb = paramBoolean;
    }
    
    public ApplicationMetadata getApplicationMetadata()
    {
      return this.uZ;
    }
    
    public String getApplicationStatus()
    {
      return this.va;
    }
    
    public String getSessionId()
    {
      return this.zzctq;
    }
    
    public Status getStatus()
    {
      return this.hv;
    }
    
    public boolean getWasLaunched()
    {
      return this.vb;
    }
  }
  
  static class zzb
    extends zzk.zza
  {
    private final Handler mHandler;
    private final AtomicReference<zze> vc;
    
    public zzb(zze paramzze)
    {
      this.vc = new AtomicReference(paramzze);
      this.mHandler = new Handler(paramzze.getLooper());
    }
    
    private void zza(zze paramzze, long paramLong, int paramInt)
    {
      synchronized (zze.zzg(paramzze))
      {
        paramzze = (zzqo.zzb)zze.zzg(paramzze).remove(Long.valueOf(paramLong));
        if (paramzze != null) {
          paramzze.setResult(new Status(paramInt));
        }
        return;
      }
    }
    
    private boolean zza(zze paramzze, int paramInt)
    {
      synchronized ()
      {
        if (zze.zzh(paramzze) != null)
        {
          zze.zzh(paramzze).setResult(new Status(paramInt));
          zze.zzb(paramzze, null);
          return true;
        }
        return false;
      }
    }
    
    public boolean isDisposed()
    {
      return this.vc.get() == null;
    }
    
    public void onApplicationDisconnected(final int paramInt)
    {
      final zze localzze = (zze)this.vc.get();
      if (localzze == null) {}
      do
      {
        return;
        zze.zza(localzze, null);
        zze.zzb(localzze, null);
        zza(localzze, paramInt);
      } while (zze.zzd(localzze) == null);
      this.mHandler.post(new Runnable()
      {
        public void run()
        {
          zze.zzd(localzze).onApplicationDisconnected(paramInt);
        }
      });
    }
    
    public void zza(ApplicationMetadata paramApplicationMetadata, String paramString1, String paramString2, boolean paramBoolean)
    {
      zze localzze = (zze)this.vc.get();
      if (localzze == null) {
        return;
      }
      zze.zza(localzze, paramApplicationMetadata);
      zze.zza(localzze, paramApplicationMetadata.getApplicationId());
      zze.zzb(localzze, paramString2);
      zze.zzc(localzze, paramString1);
      synchronized (zze.zzaps())
      {
        if (zze.zzc(localzze) != null)
        {
          zze.zzc(localzze).setResult(new zze.zza(new Status(0), paramApplicationMetadata, paramString1, paramString2, paramBoolean));
          zze.zza(localzze, null);
        }
        return;
      }
    }
    
    public void zza(String paramString, double paramDouble, boolean paramBoolean)
    {
      zze.zzapr().zzb("Deprecated callback: \"onStatusreceived\"", new Object[0]);
    }
    
    public void zza(String paramString, long paramLong, int paramInt)
    {
      paramString = (zze)this.vc.get();
      if (paramString == null) {
        return;
      }
      zza(paramString, paramLong, paramInt);
    }
    
    public zze zzapu()
    {
      zze localzze = (zze)this.vc.getAndSet(null);
      if (localzze == null) {
        return null;
      }
      zze.zzb(localzze);
      return localzze;
    }
    
    public void zzb(final ApplicationStatus paramApplicationStatus)
    {
      final zze localzze = (zze)this.vc.get();
      if (localzze == null) {
        return;
      }
      zze.zzapr().zzb("onApplicationStatusChanged", new Object[0]);
      this.mHandler.post(new Runnable()
      {
        public void run()
        {
          zze.zza(localzze, paramApplicationStatus);
        }
      });
    }
    
    public void zzb(final DeviceStatus paramDeviceStatus)
    {
      final zze localzze = (zze)this.vc.get();
      if (localzze == null) {
        return;
      }
      zze.zzapr().zzb("onDeviceStatusChanged", new Object[0]);
      this.mHandler.post(new Runnable()
      {
        public void run()
        {
          zze.zza(localzze, paramDeviceStatus);
        }
      });
    }
    
    public void zzb(String paramString, byte[] paramArrayOfByte)
    {
      if ((zze)this.vc.get() == null) {
        return;
      }
      zze.zzapr().zzb("IGNORING: Receive (type=binary, ns=%s) <%d bytes>", new Object[] { paramString, Integer.valueOf(paramArrayOfByte.length) });
    }
    
    public void zzc(String paramString, long paramLong)
    {
      paramString = (zze)this.vc.get();
      if (paramString == null) {
        return;
      }
      zza(paramString, paramLong, 0);
    }
    
    public void zzet(int paramInt)
    {
      zze localzze = (zze)this.vc.get();
      if (localzze == null) {
        return;
      }
      synchronized (zze.zzaps())
      {
        if (zze.zzc(localzze) != null)
        {
          zze.zzc(localzze).setResult(new zze.zza(new Status(paramInt)));
          zze.zza(localzze, null);
        }
        return;
      }
    }
    
    public void zzfg(int paramInt)
    {
      zze localzze = zzapu();
      if (localzze == null) {}
      do
      {
        return;
        zze.zzapr().zzb("ICastDeviceControllerListener.onDisconnected: %d", new Object[] { Integer.valueOf(paramInt) });
      } while (paramInt == 0);
      localzze.zzgk(2);
    }
    
    public void zzfh(int paramInt)
    {
      zze localzze = (zze)this.vc.get();
      if (localzze == null) {
        return;
      }
      zza(localzze, paramInt);
    }
    
    public void zzfi(int paramInt)
    {
      zze localzze = (zze)this.vc.get();
      if (localzze == null) {
        return;
      }
      zza(localzze, paramInt);
    }
    
    public void zzz(final String paramString1, final String paramString2)
    {
      final zze localzze = (zze)this.vc.get();
      if (localzze == null) {
        return;
      }
      zze.zzapr().zzb("Receive (type=text, ns=%s) %s", new Object[] { paramString1, paramString2 });
      this.mHandler.post(new Runnable()
      {
        public void run()
        {
          synchronized (zze.zze(localzze))
          {
            Cast.MessageReceivedCallback localMessageReceivedCallback = (Cast.MessageReceivedCallback)zze.zze(localzze).get(paramString1);
            if (localMessageReceivedCallback != null)
            {
              localMessageReceivedCallback.onMessageReceived(zze.zzf(localzze), paramString1, paramString2);
              return;
            }
          }
          zze.zzapr().zzb("Discarded message for unknown namespace '%s'", new Object[] { paramString1 });
        }
      });
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/internal/zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */