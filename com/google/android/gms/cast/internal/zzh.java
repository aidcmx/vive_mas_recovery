package com.google.android.gms.cast.internal;

import android.content.Context;
import com.google.android.gms.internal.zzvq;
import com.google.android.gms.internal.zzvr;
import com.google.android.gms.internal.zzvu;

public final class zzh
{
  public static final zzvq<Boolean> vk = zzvq.zzb(0, "gms:cast:remote_display_enabled", Boolean.valueOf(false));
  
  public static final void initialize(Context paramContext)
  {
    zzvu.zzbhe();
    zzvr.initialize(paramContext);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/internal/zzh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */