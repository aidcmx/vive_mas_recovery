package com.google.android.gms.cast.internal;

import com.google.android.gms.common.api.Api.zzf;
import com.google.android.gms.internal.zzpy;
import java.nio.charset.Charset;
import java.nio.charset.IllegalCharsetNameException;
import java.nio.charset.UnsupportedCharsetException;

public final class zzl
{
  public static final Api.zzf<zze> vl = new Api.zzf();
  public static final Api.zzf<zzpy> vm = new Api.zzf();
  public static final Api.zzf<Object> vn = new Api.zzf();
  public static final Api.zzf<Object> vo = new Api.zzf();
  public static final Charset vp;
  public static final String vq = zzf.zzgv("com.google.cast.multizone");
  
  static
  {
    Object localObject = null;
    try
    {
      Charset localCharset = Charset.forName("UTF-8");
      localObject = localCharset;
    }
    catch (UnsupportedCharsetException localUnsupportedCharsetException)
    {
      for (;;) {}
    }
    catch (IllegalCharsetNameException localIllegalCharsetNameException)
    {
      for (;;) {}
    }
    vp = (Charset)localObject;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/internal/zzl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */