package com.google.android.gms.cast.internal;

import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.common.internal.zzaa;
import java.util.Locale;

public class zzm
{
  private static boolean vr = false;
  protected final String mTag;
  private final boolean vs;
  private boolean vt;
  private boolean vu;
  private String vv;
  
  public zzm(String paramString)
  {
    this(paramString, zzapz());
  }
  
  public zzm(String paramString, boolean paramBoolean)
  {
    zzaa.zzh(paramString, "The log tag cannot be null or empty.");
    this.mTag = paramString;
    if (paramString.length() <= 23) {}
    for (boolean bool = true;; bool = false)
    {
      this.vs = bool;
      this.vt = paramBoolean;
      this.vu = false;
      return;
    }
  }
  
  public static boolean zzapz()
  {
    return false;
  }
  
  public void zza(String paramString, Object... paramVarArgs)
  {
    if (zzapy()) {
      Log.v(this.mTag, zzg(paramString, paramVarArgs));
    }
  }
  
  public void zza(Throwable paramThrowable, String paramString, Object... paramVarArgs)
  {
    Log.e(this.mTag, zzg(paramString, paramVarArgs), paramThrowable);
  }
  
  public boolean zzapx()
  {
    return (this.vt) || ((this.vs) && (Log.isLoggable(this.mTag, 3)));
  }
  
  public boolean zzapy()
  {
    return false;
  }
  
  public void zzb(String paramString, Object... paramVarArgs)
  {
    if (!zzapx()) {
      return;
    }
    Log.d(this.mTag, zzg(paramString, paramVarArgs));
  }
  
  public void zzb(Throwable paramThrowable, String paramString, Object... paramVarArgs)
  {
    if (!zzapx()) {
      return;
    }
    Log.d(this.mTag, zzg(paramString, paramVarArgs), paramThrowable);
  }
  
  public void zzbq(boolean paramBoolean)
  {
    this.vt = paramBoolean;
  }
  
  public void zzc(String paramString, Object... paramVarArgs)
  {
    Log.e(this.mTag, zzg(paramString, paramVarArgs));
  }
  
  public void zzc(Throwable paramThrowable, String paramString, Object... paramVarArgs)
  {
    Log.w(this.mTag, zzg(paramString, paramVarArgs), paramThrowable);
  }
  
  public void zze(String paramString, Object... paramVarArgs)
  {
    Log.i(this.mTag, zzg(paramString, paramVarArgs));
  }
  
  public void zzf(String paramString, Object... paramVarArgs)
  {
    Log.w(this.mTag, zzg(paramString, paramVarArgs));
  }
  
  protected String zzg(String paramString, Object... paramVarArgs)
  {
    if (paramVarArgs.length == 0) {}
    while (!TextUtils.isEmpty(this.vv))
    {
      paramVarArgs = String.valueOf(this.vv);
      paramString = String.valueOf(paramString);
      if (paramString.length() != 0)
      {
        return paramVarArgs.concat(paramString);
        paramString = String.format(Locale.ROOT, paramString, paramVarArgs);
      }
      else
      {
        return new String(paramVarArgs);
      }
    }
    return paramString;
  }
  
  public void zzgz(String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {}
    for (paramString = null;; paramString = String.format("[%s] ", new Object[] { paramString }))
    {
      this.vv = paramString;
      return;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/internal/zzm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */