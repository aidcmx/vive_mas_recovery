package com.google.android.gms.cast.internal;

import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaQueueItem;
import com.google.android.gms.cast.MediaStatus;
import com.google.android.gms.cast.TextTrackStyle;
import com.google.android.gms.common.util.zze;
import com.google.android.gms.common.util.zzh;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class zzn
  extends zzc
{
  public static final String NAMESPACE = zzf.zzgv("com.google.cast.media");
  private final List<zzq> tM = new ArrayList();
  private final zzq vA = new zzq(this.zzaql, 86400000L);
  private final zzq vB = new zzq(this.zzaql, 86400000L);
  private final zzq vC = new zzq(this.zzaql, 86400000L);
  private final zzq vD = new zzq(this.zzaql, 86400000L);
  private final zzq vE = new zzq(this.zzaql, 86400000L);
  private final zzq vF = new zzq(this.zzaql, 86400000L);
  private final zzq vG = new zzq(this.zzaql, 86400000L);
  private final zzq vH = new zzq(this.zzaql, 86400000L);
  private final zzq vI = new zzq(this.zzaql, 86400000L);
  private final zzq vJ = new zzq(this.zzaql, 86400000L);
  private final zzq vK = new zzq(this.zzaql, 86400000L);
  private final zzq vL = new zzq(this.zzaql, 86400000L);
  private final zzq vM = new zzq(this.zzaql, 86400000L);
  private long vw;
  private MediaStatus vx;
  private zza vy;
  private final zzq vz = new zzq(this.zzaql, 86400000L);
  
  public zzn(String paramString)
  {
    super(NAMESPACE, zzh.zzayl(), "MediaControlChannel", paramString, 1000L);
    this.tM.add(this.vz);
    this.tM.add(this.vA);
    this.tM.add(this.vB);
    this.tM.add(this.vC);
    this.tM.add(this.vD);
    this.tM.add(this.vE);
    this.tM.add(this.vF);
    this.tM.add(this.vG);
    this.tM.add(this.vH);
    this.tM.add(this.vI);
    this.tM.add(this.vJ);
    this.tM.add(this.vK);
    this.tM.add(this.vL);
    this.tM.add(this.vM);
    zzaqa();
  }
  
  private void onMetadataUpdated()
  {
    if (this.vy != null) {
      this.vy.onMetadataUpdated();
    }
  }
  
  private void onPreloadStatusUpdated()
  {
    if (this.vy != null) {
      this.vy.onPreloadStatusUpdated();
    }
  }
  
  private void onQueueStatusUpdated()
  {
    if (this.vy != null) {
      this.vy.onQueueStatusUpdated();
    }
  }
  
  private void onStatusUpdated()
  {
    if (this.vy != null) {
      this.vy.onStatusUpdated();
    }
  }
  
  private void zza(long paramLong, JSONObject paramJSONObject)
    throws JSONException
  {
    int k = 1;
    boolean bool = this.vz.test(paramLong);
    int j;
    if ((this.vD.zzaqc()) && (!this.vD.test(paramLong)))
    {
      i = 1;
      if (this.vE.zzaqc())
      {
        j = k;
        if (!this.vE.test(paramLong)) {}
      }
      else
      {
        if ((!this.vF.zzaqc()) || (this.vF.test(paramLong))) {
          break label277;
        }
        j = k;
      }
      label87:
      if (i == 0) {
        break label299;
      }
    }
    label277:
    label299:
    for (int i = 2;; i = 0)
    {
      k = i;
      if (j != 0) {
        k = i | 0x1;
      }
      if ((bool) || (this.vx == null))
      {
        this.vx = new MediaStatus(paramJSONObject);
        this.vw = this.zzaql.elapsedRealtime();
      }
      for (i = 31;; i = this.vx.zza(paramJSONObject, k))
      {
        if ((i & 0x1) != 0)
        {
          this.vw = this.zzaql.elapsedRealtime();
          onStatusUpdated();
        }
        if ((i & 0x2) != 0)
        {
          this.vw = this.zzaql.elapsedRealtime();
          onStatusUpdated();
        }
        if ((i & 0x4) != 0) {
          onMetadataUpdated();
        }
        if ((i & 0x8) != 0) {
          onQueueStatusUpdated();
        }
        if ((i & 0x10) != 0) {
          onPreloadStatusUpdated();
        }
        paramJSONObject = this.tM.iterator();
        while (paramJSONObject.hasNext()) {
          ((zzq)paramJSONObject.next()).zzc(paramLong, 0);
        }
        i = 0;
        break;
        j = 0;
        break label87;
      }
      return;
    }
  }
  
  private void zzaqa()
  {
    this.vw = 0L;
    this.vx = null;
    Iterator localIterator = this.tM.iterator();
    while (localIterator.hasNext()) {
      ((zzq)localIterator.next()).clear();
    }
  }
  
  public long getApproximateStreamPosition()
  {
    MediaInfo localMediaInfo = getMediaInfo();
    if (localMediaInfo == null) {}
    while (this.vw == 0L) {
      return 0L;
    }
    double d = this.vx.getPlaybackRate();
    long l3 = this.vx.getStreamPosition();
    int i = this.vx.getPlayerState();
    if ((d == 0.0D) || (i != 2)) {
      return l3;
    }
    long l1 = this.zzaql.elapsedRealtime() - this.vw;
    if (l1 < 0L) {
      l1 = 0L;
    }
    for (;;)
    {
      if (l1 == 0L) {
        return l3;
      }
      long l2 = localMediaInfo.getStreamDuration();
      l1 = l3 + (l1 * d);
      if ((l2 > 0L) && (l1 > l2)) {
        l1 = l2;
      }
      for (;;)
      {
        return l1;
        if (l1 < 0L) {
          l1 = 0L;
        }
      }
    }
  }
  
  public MediaInfo getMediaInfo()
  {
    if (this.vx == null) {
      return null;
    }
    return this.vx.getMediaInfo();
  }
  
  public MediaStatus getMediaStatus()
  {
    return this.vx;
  }
  
  public long getStreamDuration()
  {
    MediaInfo localMediaInfo = getMediaInfo();
    if (localMediaInfo != null) {
      return localMediaInfo.getStreamDuration();
    }
    return 0L;
  }
  
  public long zza(zzp paramzzp)
    throws IOException
  {
    JSONObject localJSONObject = new JSONObject();
    long l = zzapl();
    this.vG.zza(l, paramzzp);
    zzbp(true);
    try
    {
      localJSONObject.put("requestId", l);
      localJSONObject.put("type", "GET_STATUS");
      if (this.vx != null) {
        localJSONObject.put("mediaSessionId", this.vx.zzalj());
      }
      zza(localJSONObject.toString(), l, null);
      return l;
    }
    catch (JSONException paramzzp)
    {
      for (;;) {}
    }
  }
  
  public long zza(zzp paramzzp, double paramDouble, JSONObject paramJSONObject)
    throws IOException, zzn.zzb, IllegalArgumentException
  {
    if ((Double.isInfinite(paramDouble)) || (Double.isNaN(paramDouble))) {
      throw new IllegalArgumentException(41 + "Volume cannot be " + paramDouble);
    }
    JSONObject localJSONObject = new JSONObject();
    long l = zzapl();
    this.vE.zza(l, paramzzp);
    zzbp(true);
    try
    {
      localJSONObject.put("requestId", l);
      localJSONObject.put("type", "SET_VOLUME");
      localJSONObject.put("mediaSessionId", zzalj());
      paramzzp = new JSONObject();
      paramzzp.put("level", paramDouble);
      localJSONObject.put("volume", paramzzp);
      if (paramJSONObject != null) {
        localJSONObject.put("customData", paramJSONObject);
      }
    }
    catch (JSONException paramzzp)
    {
      for (;;) {}
    }
    zza(localJSONObject.toString(), l, null);
    return l;
  }
  
  public long zza(zzp paramzzp, int paramInt1, long paramLong, MediaQueueItem[] paramArrayOfMediaQueueItem, int paramInt2, Integer paramInteger, JSONObject paramJSONObject)
    throws IllegalArgumentException, IOException, zzn.zzb
  {
    if ((paramLong != -1L) && (paramLong < 0L)) {
      throw new IllegalArgumentException(53 + "playPosition cannot be negative: " + paramLong);
    }
    JSONObject localJSONObject = new JSONObject();
    long l = zzapl();
    this.vK.zza(l, paramzzp);
    zzbp(true);
    for (;;)
    {
      try
      {
        localJSONObject.put("requestId", l);
        localJSONObject.put("type", "QUEUE_UPDATE");
        localJSONObject.put("mediaSessionId", zzalj());
        if (paramInt1 != 0) {
          localJSONObject.put("currentItemId", paramInt1);
        }
        if (paramInt2 != 0) {
          localJSONObject.put("jump", paramInt2);
        }
        if ((paramArrayOfMediaQueueItem != null) && (paramArrayOfMediaQueueItem.length > 0))
        {
          paramzzp = new JSONArray();
          paramInt1 = 0;
          if (paramInt1 < paramArrayOfMediaQueueItem.length)
          {
            paramzzp.put(paramInt1, paramArrayOfMediaQueueItem[paramInt1].toJson());
            paramInt1 += 1;
            continue;
          }
          localJSONObject.put("items", paramzzp);
        }
        if (paramInteger != null) {}
        switch (paramInteger.intValue())
        {
        case 0: 
          if (paramLong != -1L) {
            localJSONObject.put("currentTime", zzf.zzaf(paramLong));
          }
          if (paramJSONObject != null) {
            localJSONObject.put("customData", paramJSONObject);
          }
          break;
        }
      }
      catch (JSONException paramzzp)
      {
        continue;
      }
      zza(localJSONObject.toString(), l, null);
      return l;
      localJSONObject.put("repeatMode", "REPEAT_OFF");
      continue;
      localJSONObject.put("repeatMode", "REPEAT_ALL");
      continue;
      localJSONObject.put("repeatMode", "REPEAT_SINGLE");
      continue;
      localJSONObject.put("repeatMode", "REPEAT_ALL_AND_SHUFFLE");
    }
  }
  
  public long zza(zzp paramzzp, long paramLong, int paramInt, JSONObject paramJSONObject)
    throws IOException, zzn.zzb
  {
    JSONObject localJSONObject = new JSONObject();
    long l = zzapl();
    this.vD.zza(l, paramzzp);
    zzbp(true);
    for (;;)
    {
      try
      {
        localJSONObject.put("requestId", l);
        localJSONObject.put("type", "SEEK");
        localJSONObject.put("mediaSessionId", zzalj());
        localJSONObject.put("currentTime", zzf.zzaf(paramLong));
        if (paramInt != 1) {
          continue;
        }
        localJSONObject.put("resumeState", "PLAYBACK_START");
        if (paramJSONObject != null) {
          localJSONObject.put("customData", paramJSONObject);
        }
      }
      catch (JSONException paramzzp)
      {
        continue;
      }
      zza(localJSONObject.toString(), l, null);
      return l;
      if (paramInt == 2) {
        localJSONObject.put("resumeState", "PLAYBACK_PAUSE");
      }
    }
  }
  
  public long zza(zzp paramzzp, MediaInfo paramMediaInfo, boolean paramBoolean, long paramLong, long[] paramArrayOfLong, JSONObject paramJSONObject)
    throws IOException
  {
    JSONObject localJSONObject = new JSONObject();
    long l = zzapl();
    this.vz.zza(l, paramzzp);
    zzbp(true);
    try
    {
      localJSONObject.put("requestId", l);
      localJSONObject.put("type", "LOAD");
      localJSONObject.put("media", paramMediaInfo.toJson());
      localJSONObject.put("autoplay", paramBoolean);
      localJSONObject.put("currentTime", zzf.zzaf(paramLong));
      if (paramArrayOfLong != null)
      {
        paramzzp = new JSONArray();
        int i = 0;
        while (i < paramArrayOfLong.length)
        {
          paramzzp.put(i, paramArrayOfLong[i]);
          i += 1;
        }
        localJSONObject.put("activeTrackIds", paramzzp);
      }
      if (paramJSONObject != null) {
        localJSONObject.put("customData", paramJSONObject);
      }
    }
    catch (JSONException paramzzp)
    {
      for (;;) {}
    }
    zza(localJSONObject.toString(), l, null);
    return l;
  }
  
  public long zza(zzp paramzzp, TextTrackStyle paramTextTrackStyle)
    throws IOException, zzn.zzb
  {
    JSONObject localJSONObject = new JSONObject();
    long l = zzapl();
    this.vI.zza(l, paramzzp);
    zzbp(true);
    try
    {
      localJSONObject.put("requestId", l);
      localJSONObject.put("type", "EDIT_TRACKS_INFO");
      if (paramTextTrackStyle != null) {
        localJSONObject.put("textTrackStyle", paramTextTrackStyle.toJson());
      }
      localJSONObject.put("mediaSessionId", zzalj());
    }
    catch (JSONException paramzzp)
    {
      for (;;) {}
    }
    zza(localJSONObject.toString(), l, null);
    return l;
  }
  
  public long zza(zzp paramzzp, JSONObject paramJSONObject)
    throws IOException, zzn.zzb
  {
    JSONObject localJSONObject = new JSONObject();
    long l = zzapl();
    this.vA.zza(l, paramzzp);
    zzbp(true);
    try
    {
      localJSONObject.put("requestId", l);
      localJSONObject.put("type", "PAUSE");
      localJSONObject.put("mediaSessionId", zzalj());
      if (paramJSONObject != null) {
        localJSONObject.put("customData", paramJSONObject);
      }
    }
    catch (JSONException paramzzp)
    {
      for (;;) {}
    }
    zza(localJSONObject.toString(), l, null);
    return l;
  }
  
  public long zza(zzp paramzzp, boolean paramBoolean, JSONObject paramJSONObject)
    throws IOException, zzn.zzb
  {
    JSONObject localJSONObject = new JSONObject();
    long l = zzapl();
    this.vF.zza(l, paramzzp);
    zzbp(true);
    try
    {
      localJSONObject.put("requestId", l);
      localJSONObject.put("type", "SET_VOLUME");
      localJSONObject.put("mediaSessionId", zzalj());
      paramzzp = new JSONObject();
      paramzzp.put("muted", paramBoolean);
      localJSONObject.put("volume", paramzzp);
      if (paramJSONObject != null) {
        localJSONObject.put("customData", paramJSONObject);
      }
    }
    catch (JSONException paramzzp)
    {
      for (;;) {}
    }
    zza(localJSONObject.toString(), l, null);
    return l;
  }
  
  public long zza(zzp paramzzp, int[] paramArrayOfInt, int paramInt, JSONObject paramJSONObject)
    throws IOException, zzn.zzb, IllegalArgumentException
  {
    if ((paramArrayOfInt == null) || (paramArrayOfInt.length == 0)) {
      throw new IllegalArgumentException("itemIdsToReorder must not be null or empty.");
    }
    JSONObject localJSONObject = new JSONObject();
    long l = zzapl();
    this.vM.zza(l, paramzzp);
    zzbp(true);
    try
    {
      localJSONObject.put("requestId", l);
      localJSONObject.put("type", "QUEUE_REORDER");
      localJSONObject.put("mediaSessionId", zzalj());
      paramzzp = new JSONArray();
      int i = 0;
      while (i < paramArrayOfInt.length)
      {
        paramzzp.put(i, paramArrayOfInt[i]);
        i += 1;
      }
      localJSONObject.put("itemIds", paramzzp);
      if (paramInt != 0) {
        localJSONObject.put("insertBefore", paramInt);
      }
      if (paramJSONObject != null) {
        localJSONObject.put("customData", paramJSONObject);
      }
    }
    catch (JSONException paramzzp)
    {
      for (;;) {}
    }
    zza(localJSONObject.toString(), l, null);
    return l;
  }
  
  public long zza(zzp paramzzp, int[] paramArrayOfInt, JSONObject paramJSONObject)
    throws IOException, zzn.zzb, IllegalArgumentException
  {
    if ((paramArrayOfInt == null) || (paramArrayOfInt.length == 0)) {
      throw new IllegalArgumentException("itemIdsToRemove must not be null or empty.");
    }
    JSONObject localJSONObject = new JSONObject();
    long l = zzapl();
    this.vL.zza(l, paramzzp);
    zzbp(true);
    try
    {
      localJSONObject.put("requestId", l);
      localJSONObject.put("type", "QUEUE_REMOVE");
      localJSONObject.put("mediaSessionId", zzalj());
      paramzzp = new JSONArray();
      int i = 0;
      while (i < paramArrayOfInt.length)
      {
        paramzzp.put(i, paramArrayOfInt[i]);
        i += 1;
      }
      localJSONObject.put("itemIds", paramzzp);
      if (paramJSONObject != null) {
        localJSONObject.put("customData", paramJSONObject);
      }
    }
    catch (JSONException paramzzp)
    {
      for (;;) {}
    }
    zza(localJSONObject.toString(), l, null);
    return l;
  }
  
  public long zza(zzp paramzzp, long[] paramArrayOfLong)
    throws IOException, zzn.zzb
  {
    JSONObject localJSONObject = new JSONObject();
    long l = zzapl();
    this.vH.zza(l, paramzzp);
    zzbp(true);
    try
    {
      localJSONObject.put("requestId", l);
      localJSONObject.put("type", "EDIT_TRACKS_INFO");
      localJSONObject.put("mediaSessionId", zzalj());
      paramzzp = new JSONArray();
      int i = 0;
      while (i < paramArrayOfLong.length)
      {
        paramzzp.put(i, paramArrayOfLong[i]);
        i += 1;
      }
      localJSONObject.put("activeTrackIds", paramzzp);
    }
    catch (JSONException paramzzp)
    {
      for (;;) {}
    }
    zza(localJSONObject.toString(), l, null);
    return l;
  }
  
  public long zza(zzp paramzzp, MediaQueueItem[] paramArrayOfMediaQueueItem, int paramInt1, int paramInt2, int paramInt3, long paramLong, JSONObject paramJSONObject)
    throws IOException, zzn.zzb, IllegalArgumentException
  {
    if ((paramArrayOfMediaQueueItem == null) || (paramArrayOfMediaQueueItem.length == 0)) {
      throw new IllegalArgumentException("itemsToInsert must not be null or empty.");
    }
    if ((paramInt2 != 0) && (paramInt3 != -1)) {
      throw new IllegalArgumentException("can not set both currentItemId and currentItemIndexInItemsToInsert");
    }
    if ((paramInt3 != -1) && ((paramInt3 < 0) || (paramInt3 >= paramArrayOfMediaQueueItem.length))) {
      throw new IllegalArgumentException(String.format(Locale.ROOT, "currentItemIndexInItemsToInsert %d out of range [0, %d).", new Object[] { Integer.valueOf(paramInt3), Integer.valueOf(paramArrayOfMediaQueueItem.length) }));
    }
    if ((paramLong != -1L) && (paramLong < 0L)) {
      throw new IllegalArgumentException(54 + "playPosition can not be negative: " + paramLong);
    }
    JSONObject localJSONObject = new JSONObject();
    long l = zzapl();
    this.vJ.zza(l, paramzzp);
    zzbp(true);
    try
    {
      localJSONObject.put("requestId", l);
      localJSONObject.put("type", "QUEUE_INSERT");
      localJSONObject.put("mediaSessionId", zzalj());
      paramzzp = new JSONArray();
      int i = 0;
      while (i < paramArrayOfMediaQueueItem.length)
      {
        paramzzp.put(i, paramArrayOfMediaQueueItem[i].toJson());
        i += 1;
      }
      localJSONObject.put("items", paramzzp);
      if (paramInt1 != 0) {
        localJSONObject.put("insertBefore", paramInt1);
      }
      if (paramInt2 != 0) {
        localJSONObject.put("currentItemId", paramInt2);
      }
      if (paramInt3 != -1) {
        localJSONObject.put("currentItemIndex", paramInt3);
      }
      if (paramLong != -1L) {
        localJSONObject.put("currentTime", zzf.zzaf(paramLong));
      }
      if (paramJSONObject != null) {
        localJSONObject.put("customData", paramJSONObject);
      }
    }
    catch (JSONException paramzzp)
    {
      for (;;) {}
    }
    zza(localJSONObject.toString(), l, null);
    return l;
  }
  
  public long zza(zzp paramzzp, MediaQueueItem[] paramArrayOfMediaQueueItem, int paramInt1, int paramInt2, long paramLong, JSONObject paramJSONObject)
    throws IOException, IllegalArgumentException
  {
    if ((paramArrayOfMediaQueueItem == null) || (paramArrayOfMediaQueueItem.length == 0)) {
      throw new IllegalArgumentException("items must not be null or empty.");
    }
    if ((paramInt1 < 0) || (paramInt1 >= paramArrayOfMediaQueueItem.length)) {
      throw new IllegalArgumentException(31 + "Invalid startIndex: " + paramInt1);
    }
    if ((paramLong != -1L) && (paramLong < 0L)) {
      throw new IllegalArgumentException(54 + "playPosition can not be negative: " + paramLong);
    }
    JSONObject localJSONObject = new JSONObject();
    long l = zzapl();
    this.vz.zza(l, paramzzp);
    zzbp(true);
    for (;;)
    {
      try
      {
        localJSONObject.put("requestId", l);
        localJSONObject.put("type", "QUEUE_LOAD");
        paramzzp = new JSONArray();
        int i = 0;
        if (i < paramArrayOfMediaQueueItem.length)
        {
          paramzzp.put(i, paramArrayOfMediaQueueItem[i].toJson());
          i += 1;
          continue;
        }
        localJSONObject.put("items", paramzzp);
        switch (paramInt2)
        {
        case 0: 
          throw new IllegalArgumentException(32 + "Invalid repeat mode: " + paramInt2);
        }
      }
      catch (JSONException paramzzp)
      {
        zza(localJSONObject.toString(), l, null);
        return l;
      }
      localJSONObject.put("repeatMode", "REPEAT_OFF");
      for (;;)
      {
        localJSONObject.put("startIndex", paramInt1);
        if (paramLong != -1L) {
          localJSONObject.put("currentTime", zzf.zzaf(paramLong));
        }
        if (paramJSONObject == null) {
          break;
        }
        localJSONObject.put("customData", paramJSONObject);
        break;
        localJSONObject.put("repeatMode", "REPEAT_ALL");
        continue;
        localJSONObject.put("repeatMode", "REPEAT_SINGLE");
        continue;
        localJSONObject.put("repeatMode", "REPEAT_ALL_AND_SHUFFLE");
      }
    }
  }
  
  public void zza(zza paramzza)
  {
    this.vy = paramzza;
  }
  
  protected boolean zzae(long paramLong)
  {
    ??? = this.tM.iterator();
    while (((Iterator)???).hasNext()) {
      ((zzq)((Iterator)???).next()).zzd(paramLong, 2102);
    }
    for (;;)
    {
      synchronized (zzq.zzaix)
      {
        Iterator localIterator = this.tM.iterator();
        if (localIterator.hasNext())
        {
          if (!((zzq)localIterator.next()).zzaqc()) {
            continue;
          }
          bool = true;
          return bool;
        }
      }
      boolean bool = false;
    }
  }
  
  public long zzalj()
    throws zzn.zzb
  {
    if (this.vx == null) {
      throw new zzb();
    }
    return this.vx.zzalj();
  }
  
  public void zzapk()
  {
    super.zzapk();
    zzaqa();
  }
  
  public long zzb(zzp paramzzp, JSONObject paramJSONObject)
    throws IOException, zzn.zzb
  {
    JSONObject localJSONObject = new JSONObject();
    long l = zzapl();
    this.vC.zza(l, paramzzp);
    zzbp(true);
    try
    {
      localJSONObject.put("requestId", l);
      localJSONObject.put("type", "STOP");
      localJSONObject.put("mediaSessionId", zzalj());
      if (paramJSONObject != null) {
        localJSONObject.put("customData", paramJSONObject);
      }
    }
    catch (JSONException paramzzp)
    {
      for (;;) {}
    }
    zza(localJSONObject.toString(), l, null);
    return l;
  }
  
  public void zzb(long paramLong, int paramInt)
  {
    Iterator localIterator = this.tM.iterator();
    while (localIterator.hasNext()) {
      ((zzq)localIterator.next()).zzc(paramLong, paramInt);
    }
  }
  
  public long zzc(zzp paramzzp, JSONObject paramJSONObject)
    throws IOException, zzn.zzb
  {
    JSONObject localJSONObject = new JSONObject();
    long l = zzapl();
    this.vB.zza(l, paramzzp);
    zzbp(true);
    try
    {
      localJSONObject.put("requestId", l);
      localJSONObject.put("type", "PLAY");
      localJSONObject.put("mediaSessionId", zzalj());
      if (paramJSONObject != null) {
        localJSONObject.put("customData", paramJSONObject);
      }
    }
    catch (JSONException paramzzp)
    {
      for (;;) {}
    }
    zza(localJSONObject.toString(), l, null);
    return l;
  }
  
  public final void zzgt(String paramString)
  {
    this.oT.zzb("message received: %s", new Object[] { paramString });
    Object localObject2;
    long l;
    try
    {
      Object localObject1 = new JSONObject(paramString);
      localObject2 = ((JSONObject)localObject1).getString("type");
      l = ((JSONObject)localObject1).optLong("requestId", -1L);
      if (((String)localObject2).equals("MEDIA_STATUS"))
      {
        localObject1 = ((JSONObject)localObject1).getJSONArray("status");
        if (((JSONArray)localObject1).length() > 0)
        {
          zza(l, ((JSONArray)localObject1).getJSONObject(0));
          return;
        }
        this.vx = null;
        onStatusUpdated();
        onMetadataUpdated();
        onQueueStatusUpdated();
        onPreloadStatusUpdated();
        this.vG.zzc(l, 0);
        return;
      }
    }
    catch (JSONException localJSONException)
    {
      this.oT.zzf("Message is malformed (%s); ignoring: %s", new Object[] { localJSONException.getMessage(), paramString });
      return;
    }
    JSONObject localJSONObject;
    if (((String)localObject2).equals("INVALID_PLAYER_STATE"))
    {
      this.oT.zzf("received unexpected error: Invalid Player State.", new Object[0]);
      localJSONObject = localJSONException.optJSONObject("customData");
      localObject2 = this.tM.iterator();
      while (((Iterator)localObject2).hasNext()) {
        ((zzq)((Iterator)localObject2).next()).zzc(l, 2100, localJSONObject);
      }
    }
    if (((String)localObject2).equals("LOAD_FAILED"))
    {
      localJSONObject = localJSONObject.optJSONObject("customData");
      this.vz.zzc(l, 2100, localJSONObject);
      return;
    }
    if (((String)localObject2).equals("LOAD_CANCELLED"))
    {
      localJSONObject = localJSONObject.optJSONObject("customData");
      this.vz.zzc(l, 2101, localJSONObject);
      return;
    }
    if (((String)localObject2).equals("INVALID_REQUEST"))
    {
      this.oT.zzf("received unexpected error: Invalid Request.", new Object[0]);
      localJSONObject = localJSONObject.optJSONObject("customData");
      localObject2 = this.tM.iterator();
      while (((Iterator)localObject2).hasNext()) {
        ((zzq)((Iterator)localObject2).next()).zzc(l, 2100, localJSONObject);
      }
    }
  }
  
  public static abstract interface zza
  {
    public abstract void onMetadataUpdated();
    
    public abstract void onPreloadStatusUpdated();
    
    public abstract void onQueueStatusUpdated();
    
    public abstract void onStatusUpdated();
  }
  
  public static class zzb
    extends Exception
  {}
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/internal/zzn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */