package com.google.android.gms.cast.internal;

import java.io.IOException;

public abstract interface zzo
{
  public abstract void zza(String paramString1, String paramString2, long paramLong, String paramString3)
    throws IOException;
  
  public abstract long zzall();
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/internal/zzo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */