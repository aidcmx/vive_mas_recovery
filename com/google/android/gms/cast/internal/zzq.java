package com.google.android.gms.cast.internal;

import com.google.android.gms.common.util.zze;

public final class zzq
{
  private static final zzm mz = new zzm("RequestTracker");
  public static final Object zzaix = new Object();
  private long ui;
  private long vN;
  private long vO;
  private zzp vP;
  private final zze zzaql;
  
  public zzq(zze paramzze, long paramLong)
  {
    this.zzaql = paramzze;
    this.vN = paramLong;
    this.ui = -1L;
    this.vO = 0L;
  }
  
  private void zzaqb()
  {
    this.ui = -1L;
    this.vP = null;
    this.vO = 0L;
  }
  
  public void clear()
  {
    synchronized (zzaix)
    {
      if (this.ui != -1L) {
        zzaqb();
      }
      return;
    }
  }
  
  public boolean test(long paramLong)
  {
    for (;;)
    {
      synchronized (zzaix)
      {
        if ((this.ui != -1L) && (this.ui == paramLong))
        {
          bool = true;
          return bool;
        }
      }
      boolean bool = false;
    }
  }
  
  public void zza(long paramLong, zzp paramzzp)
  {
    synchronized (zzaix)
    {
      zzp localzzp = this.vP;
      long l = this.ui;
      this.ui = paramLong;
      this.vP = paramzzp;
      this.vO = this.zzaql.elapsedRealtime();
      if (localzzp != null) {
        localzzp.zzac(l);
      }
      return;
    }
  }
  
  public boolean zzaqc()
  {
    for (;;)
    {
      synchronized (zzaix)
      {
        if (this.ui != -1L)
        {
          bool = true;
          return bool;
        }
      }
      boolean bool = false;
    }
  }
  
  public boolean zzc(long paramLong, int paramInt)
  {
    return zzc(paramLong, paramInt, null);
  }
  
  public boolean zzc(long paramLong, int paramInt, Object paramObject)
  {
    boolean bool = true;
    zzp localzzp = null;
    for (;;)
    {
      synchronized (zzaix)
      {
        if ((this.ui != -1L) && (this.ui == paramLong))
        {
          mz.zzb("request %d completed", new Object[] { Long.valueOf(this.ui) });
          localzzp = this.vP;
          zzaqb();
          if (localzzp != null) {
            localzzp.zza(paramLong, paramInt, paramObject);
          }
          return bool;
        }
      }
      bool = false;
    }
  }
  
  public boolean zzd(long paramLong, int paramInt)
  {
    boolean bool = true;
    long l = 0L;
    for (;;)
    {
      synchronized (zzaix)
      {
        if ((this.ui != -1L) && (paramLong - this.vO >= this.vN))
        {
          mz.zzb("request %d timed out", new Object[] { Long.valueOf(this.ui) });
          paramLong = this.ui;
          zzp localzzp = this.vP;
          zzaqb();
          if (localzzp != null) {
            localzzp.zza(paramLong, paramInt, null);
          }
          return bool;
        }
      }
      bool = false;
      Object localObject2 = null;
      paramLong = l;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/internal/zzq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */