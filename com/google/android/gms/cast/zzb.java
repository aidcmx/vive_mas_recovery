package com.google.android.gms.cast;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.images.WebImage;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import java.util.ArrayList;

public class zzb
  implements Parcelable.Creator<ApplicationMetadata>
{
  static void zza(ApplicationMetadata paramApplicationMetadata, Parcel paramParcel, int paramInt)
  {
    int i = com.google.android.gms.common.internal.safeparcel.zzb.zzcs(paramParcel);
    com.google.android.gms.common.internal.safeparcel.zzb.zzc(paramParcel, 1, paramApplicationMetadata.getVersionCode());
    com.google.android.gms.common.internal.safeparcel.zzb.zza(paramParcel, 2, paramApplicationMetadata.getApplicationId(), false);
    com.google.android.gms.common.internal.safeparcel.zzb.zza(paramParcel, 3, paramApplicationMetadata.getName(), false);
    com.google.android.gms.common.internal.safeparcel.zzb.zzc(paramParcel, 4, paramApplicationMetadata.getImages(), false);
    com.google.android.gms.common.internal.safeparcel.zzb.zzb(paramParcel, 5, paramApplicationMetadata.getSupportedNamespaces(), false);
    com.google.android.gms.common.internal.safeparcel.zzb.zza(paramParcel, 6, paramApplicationMetadata.getSenderAppIdentifier(), false);
    com.google.android.gms.common.internal.safeparcel.zzb.zza(paramParcel, 7, paramApplicationMetadata.zzakr(), paramInt, false);
    com.google.android.gms.common.internal.safeparcel.zzb.zzaj(paramParcel, i);
  }
  
  public ApplicationMetadata zzbm(Parcel paramParcel)
  {
    Uri localUri = null;
    int j = zza.zzcr(paramParcel);
    int i = 0;
    String str1 = null;
    ArrayList localArrayList1 = null;
    ArrayList localArrayList2 = null;
    String str2 = null;
    String str3 = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.zzcq(paramParcel);
      switch (zza.zzgu(k))
      {
      default: 
        zza.zzb(paramParcel, k);
        break;
      case 1: 
        i = zza.zzg(paramParcel, k);
        break;
      case 2: 
        str3 = zza.zzq(paramParcel, k);
        break;
      case 3: 
        str2 = zza.zzq(paramParcel, k);
        break;
      case 4: 
        localArrayList2 = zza.zzc(paramParcel, k, WebImage.CREATOR);
        break;
      case 5: 
        localArrayList1 = zza.zzae(paramParcel, k);
        break;
      case 6: 
        str1 = zza.zzq(paramParcel, k);
        break;
      case 7: 
        localUri = (Uri)zza.zza(paramParcel, k, Uri.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new ApplicationMetadata(i, str3, str2, localArrayList2, localArrayList1, str1, localUri);
  }
  
  public ApplicationMetadata[] zzee(int paramInt)
  {
    return new ApplicationMetadata[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */