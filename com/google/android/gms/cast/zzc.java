package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.images.WebImage;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.ArrayList;

public class zzc
  implements Parcelable.Creator<CastDevice>
{
  static void zza(CastDevice paramCastDevice, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramCastDevice.getVersionCode());
    zzb.zza(paramParcel, 2, paramCastDevice.zzaks(), false);
    zzb.zza(paramParcel, 3, paramCastDevice.mm, false);
    zzb.zza(paramParcel, 4, paramCastDevice.getFriendlyName(), false);
    zzb.zza(paramParcel, 5, paramCastDevice.getModelName(), false);
    zzb.zza(paramParcel, 6, paramCastDevice.getDeviceVersion(), false);
    zzb.zzc(paramParcel, 7, paramCastDevice.getServicePort());
    zzb.zzc(paramParcel, 8, paramCastDevice.getIcons(), false);
    zzb.zzc(paramParcel, 9, paramCastDevice.getCapabilities());
    zzb.zzc(paramParcel, 10, paramCastDevice.getStatus());
    zzb.zza(paramParcel, 11, paramCastDevice.zzakt(), false);
    zzb.zzaj(paramParcel, paramInt);
  }
  
  public CastDevice zzbn(Parcel paramParcel)
  {
    int j = 0;
    String str1 = null;
    int n = zza.zzcr(paramParcel);
    int i = -1;
    ArrayList localArrayList = null;
    int k = 0;
    String str2 = null;
    String str3 = null;
    String str4 = null;
    String str5 = null;
    String str6 = null;
    int m = 0;
    while (paramParcel.dataPosition() < n)
    {
      int i1 = zza.zzcq(paramParcel);
      switch (zza.zzgu(i1))
      {
      default: 
        zza.zzb(paramParcel, i1);
        break;
      case 1: 
        m = zza.zzg(paramParcel, i1);
        break;
      case 2: 
        str6 = zza.zzq(paramParcel, i1);
        break;
      case 3: 
        str5 = zza.zzq(paramParcel, i1);
        break;
      case 4: 
        str4 = zza.zzq(paramParcel, i1);
        break;
      case 5: 
        str3 = zza.zzq(paramParcel, i1);
        break;
      case 6: 
        str2 = zza.zzq(paramParcel, i1);
        break;
      case 7: 
        k = zza.zzg(paramParcel, i1);
        break;
      case 8: 
        localArrayList = zza.zzc(paramParcel, i1, WebImage.CREATOR);
        break;
      case 9: 
        j = zza.zzg(paramParcel, i1);
        break;
      case 10: 
        i = zza.zzg(paramParcel, i1);
        break;
      case 11: 
        str1 = zza.zzq(paramParcel, i1);
      }
    }
    if (paramParcel.dataPosition() != n) {
      throw new zza.zza(37 + "Overread allowed size end=" + n, paramParcel);
    }
    return new CastDevice(m, str6, str5, str4, str3, str2, k, localArrayList, j, i, str1);
  }
  
  public CastDevice[] zzef(int paramInt)
  {
    return new CastDevice[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */