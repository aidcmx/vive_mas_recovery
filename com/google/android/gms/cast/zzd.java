package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzd
  implements Parcelable.Creator<JoinOptions>
{
  static void zza(JoinOptions paramJoinOptions, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramJoinOptions.getVersionCode());
    zzb.zzc(paramParcel, 2, paramJoinOptions.getConnectionType());
    zzb.zzaj(paramParcel, paramInt);
  }
  
  public JoinOptions zzbo(Parcel paramParcel)
  {
    int j = 0;
    int k = zza.zzcr(paramParcel);
    int i = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.zzcq(paramParcel);
      switch (zza.zzgu(m))
      {
      default: 
        zza.zzb(paramParcel, m);
        break;
      case 1: 
        i = zza.zzg(paramParcel, m);
        break;
      case 2: 
        j = zza.zzg(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza(37 + "Overread allowed size end=" + k, paramParcel);
    }
    return new JoinOptions(i, j);
  }
  
  public JoinOptions[] zzeg(int paramInt)
  {
    return new JoinOptions[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */