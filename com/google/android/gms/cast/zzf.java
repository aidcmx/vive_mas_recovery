package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.ArrayList;

public class zzf
  implements Parcelable.Creator<MediaInfo>
{
  static void zza(MediaInfo paramMediaInfo, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramMediaInfo.getVersionCode());
    zzb.zza(paramParcel, 2, paramMediaInfo.getContentId(), false);
    zzb.zzc(paramParcel, 3, paramMediaInfo.getStreamType());
    zzb.zza(paramParcel, 4, paramMediaInfo.getContentType(), false);
    zzb.zza(paramParcel, 5, paramMediaInfo.getMetadata(), paramInt, false);
    zzb.zza(paramParcel, 6, paramMediaInfo.getStreamDuration());
    zzb.zzc(paramParcel, 7, paramMediaInfo.getMediaTracks(), false);
    zzb.zza(paramParcel, 8, paramMediaInfo.getTextTrackStyle(), paramInt, false);
    zzb.zza(paramParcel, 9, paramMediaInfo.nn, false);
    zzb.zzc(paramParcel, 10, paramMediaInfo.getAdBreaks(), false);
    zzb.zzaj(paramParcel, i);
  }
  
  public MediaInfo zzbq(Parcel paramParcel)
  {
    int i = 0;
    ArrayList localArrayList1 = null;
    int k = zza.zzcr(paramParcel);
    long l = 0L;
    String str1 = null;
    TextTrackStyle localTextTrackStyle = null;
    ArrayList localArrayList2 = null;
    MediaMetadata localMediaMetadata = null;
    String str2 = null;
    String str3 = null;
    int j = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.zzcq(paramParcel);
      switch (zza.zzgu(m))
      {
      default: 
        zza.zzb(paramParcel, m);
        break;
      case 1: 
        j = zza.zzg(paramParcel, m);
        break;
      case 2: 
        str3 = zza.zzq(paramParcel, m);
        break;
      case 3: 
        i = zza.zzg(paramParcel, m);
        break;
      case 4: 
        str2 = zza.zzq(paramParcel, m);
        break;
      case 5: 
        localMediaMetadata = (MediaMetadata)zza.zza(paramParcel, m, MediaMetadata.CREATOR);
        break;
      case 6: 
        l = zza.zzi(paramParcel, m);
        break;
      case 7: 
        localArrayList2 = zza.zzc(paramParcel, m, MediaTrack.CREATOR);
        break;
      case 8: 
        localTextTrackStyle = (TextTrackStyle)zza.zza(paramParcel, m, TextTrackStyle.CREATOR);
        break;
      case 9: 
        str1 = zza.zzq(paramParcel, m);
        break;
      case 10: 
        localArrayList1 = zza.zzc(paramParcel, m, AdBreakInfo.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza(37 + "Overread allowed size end=" + k, paramParcel);
    }
    return new MediaInfo(j, str3, i, str2, localMediaMetadata, l, localArrayList2, localTextTrackStyle, str1, localArrayList1);
  }
  
  public MediaInfo[] zzei(int paramInt)
  {
    return new MediaInfo[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */