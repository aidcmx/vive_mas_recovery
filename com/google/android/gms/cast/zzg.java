package com.google.android.gms.cast;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.images.WebImage;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.ArrayList;

public class zzg
  implements Parcelable.Creator<MediaMetadata>
{
  static void zza(MediaMetadata paramMediaMetadata, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramMediaMetadata.getVersionCode());
    zzb.zzc(paramParcel, 2, paramMediaMetadata.getImages(), false);
    zzb.zza(paramParcel, 3, paramMediaMetadata.nt, false);
    zzb.zzc(paramParcel, 4, paramMediaMetadata.getMediaType());
    zzb.zzaj(paramParcel, paramInt);
  }
  
  public MediaMetadata zzbr(Parcel paramParcel)
  {
    Bundle localBundle = null;
    int j = 0;
    int k = zza.zzcr(paramParcel);
    ArrayList localArrayList = null;
    int i = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.zzcq(paramParcel);
      switch (zza.zzgu(m))
      {
      default: 
        zza.zzb(paramParcel, m);
        break;
      case 1: 
        i = zza.zzg(paramParcel, m);
        break;
      case 2: 
        localArrayList = zza.zzc(paramParcel, m, WebImage.CREATOR);
        break;
      case 3: 
        localBundle = zza.zzs(paramParcel, m);
        break;
      case 4: 
        j = zza.zzg(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza(37 + "Overread allowed size end=" + k, paramParcel);
    }
    return new MediaMetadata(i, localArrayList, localBundle, j);
  }
  
  public MediaMetadata[] zzej(int paramInt)
  {
    return new MediaMetadata[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/zzg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */