package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzh
  implements Parcelable.Creator<MediaQueueItem>
{
  static void zza(MediaQueueItem paramMediaQueueItem, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramMediaQueueItem.getVersionCode());
    zzb.zza(paramParcel, 2, paramMediaQueueItem.getMedia(), paramInt, false);
    zzb.zzc(paramParcel, 3, paramMediaQueueItem.getItemId());
    zzb.zza(paramParcel, 4, paramMediaQueueItem.getAutoplay());
    zzb.zza(paramParcel, 5, paramMediaQueueItem.getStartTime());
    zzb.zza(paramParcel, 6, paramMediaQueueItem.getPlaybackDuration());
    zzb.zza(paramParcel, 7, paramMediaQueueItem.getPreloadTime());
    zzb.zza(paramParcel, 8, paramMediaQueueItem.getActiveTrackIds(), false);
    zzb.zza(paramParcel, 9, paramMediaQueueItem.nn, false);
    zzb.zzaj(paramParcel, i);
  }
  
  public MediaQueueItem zzbs(Parcel paramParcel)
  {
    int k = zza.zzcr(paramParcel);
    int j = 0;
    MediaInfo localMediaInfo = null;
    int i = 0;
    boolean bool = false;
    double d3 = 0.0D;
    double d2 = 0.0D;
    double d1 = 0.0D;
    long[] arrayOfLong = null;
    String str = null;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.zzcq(paramParcel);
      switch (zza.zzgu(m))
      {
      default: 
        zza.zzb(paramParcel, m);
        break;
      case 1: 
        j = zza.zzg(paramParcel, m);
        break;
      case 2: 
        localMediaInfo = (MediaInfo)zza.zza(paramParcel, m, MediaInfo.CREATOR);
        break;
      case 3: 
        i = zza.zzg(paramParcel, m);
        break;
      case 4: 
        bool = zza.zzc(paramParcel, m);
        break;
      case 5: 
        d3 = zza.zzn(paramParcel, m);
        break;
      case 6: 
        d2 = zza.zzn(paramParcel, m);
        break;
      case 7: 
        d1 = zza.zzn(paramParcel, m);
        break;
      case 8: 
        arrayOfLong = zza.zzx(paramParcel, m);
        break;
      case 9: 
        str = zza.zzq(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza(37 + "Overread allowed size end=" + k, paramParcel);
    }
    return new MediaQueueItem(j, localMediaInfo, i, bool, d3, d2, d1, arrayOfLong, str);
  }
  
  public MediaQueueItem[] zzel(int paramInt)
  {
    return new MediaQueueItem[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/zzh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */