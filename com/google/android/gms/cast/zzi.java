package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.ArrayList;

public class zzi
  implements Parcelable.Creator<MediaStatus>
{
  static void zza(MediaStatus paramMediaStatus, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramMediaStatus.getVersionCode());
    zzb.zza(paramParcel, 2, paramMediaStatus.getMediaInfo(), paramInt, false);
    zzb.zza(paramParcel, 3, paramMediaStatus.zzalj());
    zzb.zzc(paramParcel, 4, paramMediaStatus.getCurrentItemId());
    zzb.zza(paramParcel, 5, paramMediaStatus.getPlaybackRate());
    zzb.zzc(paramParcel, 6, paramMediaStatus.getPlayerState());
    zzb.zzc(paramParcel, 7, paramMediaStatus.getIdleReason());
    zzb.zza(paramParcel, 8, paramMediaStatus.getStreamPosition());
    zzb.zza(paramParcel, 9, paramMediaStatus.nM);
    zzb.zza(paramParcel, 10, paramMediaStatus.getStreamVolume());
    zzb.zza(paramParcel, 11, paramMediaStatus.isMute());
    zzb.zza(paramParcel, 12, paramMediaStatus.getActiveTrackIds(), false);
    zzb.zzc(paramParcel, 13, paramMediaStatus.getLoadingItemId());
    zzb.zzc(paramParcel, 14, paramMediaStatus.getPreloadedItemId());
    zzb.zza(paramParcel, 15, paramMediaStatus.nn, false);
    zzb.zzc(paramParcel, 16, paramMediaStatus.nR);
    zzb.zzc(paramParcel, 17, paramMediaStatus.nS, false);
    zzb.zza(paramParcel, 18, paramMediaStatus.isPlayingAd());
    zzb.zzaj(paramParcel, i);
  }
  
  public MediaStatus zzbt(Parcel paramParcel)
  {
    int i3 = zza.zzcr(paramParcel);
    int i2 = 0;
    MediaInfo localMediaInfo = null;
    long l3 = 0L;
    int i1 = 0;
    double d2 = 0.0D;
    int n = 0;
    int m = 0;
    long l2 = 0L;
    long l1 = 0L;
    double d1 = 0.0D;
    boolean bool2 = false;
    long[] arrayOfLong = null;
    int k = 0;
    int j = 0;
    String str = null;
    int i = 0;
    ArrayList localArrayList = null;
    boolean bool1 = false;
    while (paramParcel.dataPosition() < i3)
    {
      int i4 = zza.zzcq(paramParcel);
      switch (zza.zzgu(i4))
      {
      default: 
        zza.zzb(paramParcel, i4);
        break;
      case 1: 
        i2 = zza.zzg(paramParcel, i4);
        break;
      case 2: 
        localMediaInfo = (MediaInfo)zza.zza(paramParcel, i4, MediaInfo.CREATOR);
        break;
      case 3: 
        l3 = zza.zzi(paramParcel, i4);
        break;
      case 4: 
        i1 = zza.zzg(paramParcel, i4);
        break;
      case 5: 
        d2 = zza.zzn(paramParcel, i4);
        break;
      case 6: 
        n = zza.zzg(paramParcel, i4);
        break;
      case 7: 
        m = zza.zzg(paramParcel, i4);
        break;
      case 8: 
        l2 = zza.zzi(paramParcel, i4);
        break;
      case 9: 
        l1 = zza.zzi(paramParcel, i4);
        break;
      case 10: 
        d1 = zza.zzn(paramParcel, i4);
        break;
      case 11: 
        bool2 = zza.zzc(paramParcel, i4);
        break;
      case 12: 
        arrayOfLong = zza.zzx(paramParcel, i4);
        break;
      case 13: 
        k = zza.zzg(paramParcel, i4);
        break;
      case 14: 
        j = zza.zzg(paramParcel, i4);
        break;
      case 15: 
        str = zza.zzq(paramParcel, i4);
        break;
      case 16: 
        i = zza.zzg(paramParcel, i4);
        break;
      case 17: 
        localArrayList = zza.zzc(paramParcel, i4, MediaQueueItem.CREATOR);
        break;
      case 18: 
        bool1 = zza.zzc(paramParcel, i4);
      }
    }
    if (paramParcel.dataPosition() != i3) {
      throw new zza.zza(37 + "Overread allowed size end=" + i3, paramParcel);
    }
    return new MediaStatus(i2, localMediaInfo, l3, i1, d2, n, m, l2, l1, d1, bool2, arrayOfLong, k, j, str, i, localArrayList, bool1);
  }
  
  public MediaStatus[] zzem(int paramInt)
  {
    return new MediaStatus[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/zzi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */