package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzj
  implements Parcelable.Creator<MediaTrack>
{
  static void zza(MediaTrack paramMediaTrack, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramMediaTrack.getVersionCode());
    zzb.zza(paramParcel, 2, paramMediaTrack.getId());
    zzb.zzc(paramParcel, 3, paramMediaTrack.getType());
    zzb.zza(paramParcel, 4, paramMediaTrack.getContentId(), false);
    zzb.zza(paramParcel, 5, paramMediaTrack.getContentType(), false);
    zzb.zza(paramParcel, 6, paramMediaTrack.getName(), false);
    zzb.zza(paramParcel, 7, paramMediaTrack.getLanguage(), false);
    zzb.zzc(paramParcel, 8, paramMediaTrack.getSubtype());
    zzb.zza(paramParcel, 9, paramMediaTrack.nn, false);
    zzb.zzaj(paramParcel, paramInt);
  }
  
  public MediaTrack zzbu(Parcel paramParcel)
  {
    int i = 0;
    String str1 = null;
    int m = zza.zzcr(paramParcel);
    long l = 0L;
    String str2 = null;
    String str3 = null;
    String str4 = null;
    String str5 = null;
    int j = 0;
    int k = 0;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.zzcq(paramParcel);
      switch (zza.zzgu(n))
      {
      default: 
        zza.zzb(paramParcel, n);
        break;
      case 1: 
        k = zza.zzg(paramParcel, n);
        break;
      case 2: 
        l = zza.zzi(paramParcel, n);
        break;
      case 3: 
        j = zza.zzg(paramParcel, n);
        break;
      case 4: 
        str5 = zza.zzq(paramParcel, n);
        break;
      case 5: 
        str4 = zza.zzq(paramParcel, n);
        break;
      case 6: 
        str3 = zza.zzq(paramParcel, n);
        break;
      case 7: 
        str2 = zza.zzq(paramParcel, n);
        break;
      case 8: 
        i = zza.zzg(paramParcel, n);
        break;
      case 9: 
        str1 = zza.zzq(paramParcel, n);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza(37 + "Overread allowed size end=" + m, paramParcel);
    }
    return new MediaTrack(k, l, j, str5, str4, str3, str2, i, str1);
  }
  
  public MediaTrack[] zzeo(int paramInt)
  {
    return new MediaTrack[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/zzj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */