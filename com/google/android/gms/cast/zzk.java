package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzk
  implements Parcelable.Creator<TextTrackStyle>
{
  static void zza(TextTrackStyle paramTextTrackStyle, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramTextTrackStyle.getVersionCode());
    zzb.zza(paramParcel, 2, paramTextTrackStyle.getFontScale());
    zzb.zzc(paramParcel, 3, paramTextTrackStyle.getForegroundColor());
    zzb.zzc(paramParcel, 4, paramTextTrackStyle.getBackgroundColor());
    zzb.zzc(paramParcel, 5, paramTextTrackStyle.getEdgeType());
    zzb.zzc(paramParcel, 6, paramTextTrackStyle.getEdgeColor());
    zzb.zzc(paramParcel, 7, paramTextTrackStyle.getWindowType());
    zzb.zzc(paramParcel, 8, paramTextTrackStyle.getWindowColor());
    zzb.zzc(paramParcel, 9, paramTextTrackStyle.getWindowCornerRadius());
    zzb.zza(paramParcel, 10, paramTextTrackStyle.getFontFamily(), false);
    zzb.zzc(paramParcel, 11, paramTextTrackStyle.getFontGenericFamily());
    zzb.zzc(paramParcel, 12, paramTextTrackStyle.getFontStyle());
    zzb.zza(paramParcel, 13, paramTextTrackStyle.nn, false);
    zzb.zzaj(paramParcel, paramInt);
  }
  
  public TextTrackStyle zzbv(Parcel paramParcel)
  {
    int i6 = zza.zzcr(paramParcel);
    int i5 = 0;
    float f = 0.0F;
    int i4 = 0;
    int i3 = 0;
    int i2 = 0;
    int i1 = 0;
    int n = 0;
    int m = 0;
    int k = 0;
    String str2 = null;
    int j = 0;
    int i = 0;
    String str1 = null;
    while (paramParcel.dataPosition() < i6)
    {
      int i7 = zza.zzcq(paramParcel);
      switch (zza.zzgu(i7))
      {
      default: 
        zza.zzb(paramParcel, i7);
        break;
      case 1: 
        i5 = zza.zzg(paramParcel, i7);
        break;
      case 2: 
        f = zza.zzl(paramParcel, i7);
        break;
      case 3: 
        i4 = zza.zzg(paramParcel, i7);
        break;
      case 4: 
        i3 = zza.zzg(paramParcel, i7);
        break;
      case 5: 
        i2 = zza.zzg(paramParcel, i7);
        break;
      case 6: 
        i1 = zza.zzg(paramParcel, i7);
        break;
      case 7: 
        n = zza.zzg(paramParcel, i7);
        break;
      case 8: 
        m = zza.zzg(paramParcel, i7);
        break;
      case 9: 
        k = zza.zzg(paramParcel, i7);
        break;
      case 10: 
        str2 = zza.zzq(paramParcel, i7);
        break;
      case 11: 
        j = zza.zzg(paramParcel, i7);
        break;
      case 12: 
        i = zza.zzg(paramParcel, i7);
        break;
      case 13: 
        str1 = zza.zzq(paramParcel, i7);
      }
    }
    if (paramParcel.dataPosition() != i6) {
      throw new zza.zza(37 + "Overread allowed size end=" + i6, paramParcel);
    }
    return new TextTrackStyle(i5, f, i4, i3, i2, i1, n, m, k, str2, j, i, str1);
  }
  
  public TextTrackStyle[] zzeq(int paramInt)
  {
    return new TextTrackStyle[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/cast/zzk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */