package com.google.android.gms.clearcut;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.internal.zzasf.zzc;
import com.google.android.gms.playlog.internal.PlayLoggerContext;
import java.util.Arrays;

public class LogEventParcelable
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<LogEventParcelable> CREATOR = new zzc();
  public final int versionCode;
  public byte[][] wA;
  public boolean wB;
  public final zzasf.zzc wC;
  public final zza.zzc wD;
  public final zza.zzc wE;
  public PlayLoggerContext wv;
  public byte[] ww;
  public int[] wx;
  public String[] wy;
  public int[] wz;
  
  LogEventParcelable(int paramInt, PlayLoggerContext paramPlayLoggerContext, byte[] paramArrayOfByte, int[] paramArrayOfInt1, String[] paramArrayOfString, int[] paramArrayOfInt2, byte[][] paramArrayOfByte1, boolean paramBoolean)
  {
    this.versionCode = paramInt;
    this.wv = paramPlayLoggerContext;
    this.ww = paramArrayOfByte;
    this.wx = paramArrayOfInt1;
    this.wy = paramArrayOfString;
    this.wC = null;
    this.wD = null;
    this.wE = null;
    this.wz = paramArrayOfInt2;
    this.wA = paramArrayOfByte1;
    this.wB = paramBoolean;
  }
  
  public LogEventParcelable(PlayLoggerContext paramPlayLoggerContext, zzasf.zzc paramzzc, zza.zzc paramzzc1, zza.zzc paramzzc2, int[] paramArrayOfInt1, String[] paramArrayOfString, int[] paramArrayOfInt2, byte[][] paramArrayOfByte, boolean paramBoolean)
  {
    this.versionCode = 1;
    this.wv = paramPlayLoggerContext;
    this.wC = paramzzc;
    this.wD = paramzzc1;
    this.wE = paramzzc2;
    this.wx = paramArrayOfInt1;
    this.wy = paramArrayOfString;
    this.wz = paramArrayOfInt2;
    this.wA = paramArrayOfByte;
    this.wB = paramBoolean;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    do
    {
      return true;
      if (!(paramObject instanceof LogEventParcelable)) {
        break;
      }
      paramObject = (LogEventParcelable)paramObject;
    } while ((this.versionCode == ((LogEventParcelable)paramObject).versionCode) && (zzz.equal(this.wv, ((LogEventParcelable)paramObject).wv)) && (Arrays.equals(this.ww, ((LogEventParcelable)paramObject).ww)) && (Arrays.equals(this.wx, ((LogEventParcelable)paramObject).wx)) && (Arrays.equals(this.wy, ((LogEventParcelable)paramObject).wy)) && (zzz.equal(this.wC, ((LogEventParcelable)paramObject).wC)) && (zzz.equal(this.wD, ((LogEventParcelable)paramObject).wD)) && (zzz.equal(this.wE, ((LogEventParcelable)paramObject).wE)) && (Arrays.equals(this.wz, ((LogEventParcelable)paramObject).wz)) && (Arrays.deepEquals(this.wA, ((LogEventParcelable)paramObject).wA)) && (this.wB == ((LogEventParcelable)paramObject).wB));
    return false;
    return false;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { Integer.valueOf(this.versionCode), this.wv, this.ww, this.wx, this.wy, this.wC, this.wD, this.wE, this.wz, this.wA, Boolean.valueOf(this.wB) });
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("LogEventParcelable[").append(this.versionCode).append(", ").append(this.wv).append(", ").append("LogEventBytes: ");
    if (this.ww == null) {}
    for (String str = null;; str = new String(this.ww)) {
      return str + ", " + "TestCodes: " + Arrays.toString(this.wx) + ", " + "MendelPackages: " + Arrays.toString(this.wy) + ", " + "LogEvent: " + this.wC + ", " + "ExtensionProducer: " + this.wD + ", " + "VeProducer: " + this.wE + ", " + "ExperimentIDs: " + Arrays.toString(this.wz) + ", " + "ExperimentTokens: " + Arrays.toString(this.wA) + ", " + "AddPhenotypeExperimentTokens: " + this.wB + "]";
    }
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzc.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/clearcut/LogEventParcelable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */