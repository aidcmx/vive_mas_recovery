package com.google.android.gms.clearcut;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Looper;
import android.util.Log;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.ApiOptions.NoOptions;
import com.google.android.gms.common.api.Api.zza;
import com.google.android.gms.common.api.Api.zzf;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.PendingResults;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.util.zze;
import com.google.android.gms.common.util.zzh;
import com.google.android.gms.internal.zzasf.zzc;
import com.google.android.gms.internal.zzqc;
import com.google.android.gms.internal.zzqd;
import com.google.android.gms.internal.zzqh;
import com.google.android.gms.playlog.internal.PlayLoggerContext;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.TimeZone;

public final class zza
{
  @Deprecated
  public static final Api<Api.ApiOptions.NoOptions> API = new Api("ClearcutLogger.API", hh, hg);
  public static final Api.zzf<zzqd> hg = new Api.zzf();
  public static final Api.zza<zzqd, Api.ApiOptions.NoOptions> hh = new Api.zza()
  {
    public zzqd zze(Context paramAnonymousContext, Looper paramAnonymousLooper, zzf paramAnonymouszzf, Api.ApiOptions.NoOptions paramAnonymousNoOptions, GoogleApiClient.ConnectionCallbacks paramAnonymousConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramAnonymousOnConnectionFailedListener)
    {
      return new zzqd(paramAnonymousContext, paramAnonymousLooper, paramAnonymouszzf, paramAnonymousConnectionCallbacks, paramAnonymousOnConnectionFailedListener);
    }
  };
  private final int wc;
  private String wd;
  private int we = -1;
  private String wf;
  private String wg;
  private final boolean wh;
  private int wi = 0;
  private final zzb wj;
  private zzd wk;
  private final zzb wl;
  private final zze zzaql;
  private final String zzcjc;
  
  public zza(Context paramContext, int paramInt, String paramString1, String paramString2, String paramString3, boolean paramBoolean, zzb paramzzb, zze paramzze, zzd paramzzd, zzb paramzzb1)
  {
    this.zzcjc = paramContext.getPackageName();
    this.wc = zzbh(paramContext);
    this.we = paramInt;
    this.wd = paramString1;
    this.wf = paramString2;
    this.wg = paramString3;
    this.wh = paramBoolean;
    this.wj = paramzzb;
    this.zzaql = paramzze;
    if (paramzzd != null) {}
    for (;;)
    {
      this.wk = paramzzd;
      this.wi = 0;
      this.wl = paramzzb1;
      if (this.wh)
      {
        paramBoolean = bool;
        if (this.wf == null) {
          paramBoolean = true;
        }
        zzaa.zzb(paramBoolean, "can't be anonymous with an upload account");
      }
      return;
      paramzzd = new zzd();
    }
  }
  
  public zza(Context paramContext, String paramString1, String paramString2)
  {
    this(paramContext, -1, paramString1, paramString2, null, false, zzqc.zzbi(paramContext), zzh.zzayl(), null, new zzqh(paramContext));
  }
  
  private static int[] zzb(ArrayList<Integer> paramArrayList)
  {
    if (paramArrayList == null) {
      return null;
    }
    int[] arrayOfInt = new int[paramArrayList.size()];
    paramArrayList = paramArrayList.iterator();
    int i = 0;
    while (paramArrayList.hasNext())
    {
      arrayOfInt[i] = ((Integer)paramArrayList.next()).intValue();
      i += 1;
    }
    return arrayOfInt;
  }
  
  private int zzbh(Context paramContext)
  {
    try
    {
      int i = paramContext.getPackageManager().getPackageInfo(paramContext.getPackageName(), 0).versionCode;
      return i;
    }
    catch (PackageManager.NameNotFoundException paramContext)
    {
      Log.wtf("ClearcutLogger", "This can't happen.");
    }
    return 0;
  }
  
  private static String[] zzc(ArrayList<String> paramArrayList)
  {
    if (paramArrayList == null) {
      return null;
    }
    return (String[])paramArrayList.toArray(new String[0]);
  }
  
  private static byte[][] zzd(ArrayList<byte[]> paramArrayList)
  {
    if (paramArrayList == null) {
      return null;
    }
    return (byte[][])paramArrayList.toArray(new byte[0][]);
  }
  
  public zza zzm(byte[] paramArrayOfByte)
  {
    return new zza(paramArrayOfByte, null);
  }
  
  public class zza
  {
    private String wd = zza.zzb(zza.this);
    private int we = zza.zza(zza.this);
    private String wf = zza.zzc(zza.this);
    private String wg = zza.zzd(zza.this);
    private int wi = zza.zze(zza.this);
    private final zza.zzc wm;
    private ArrayList<Integer> wn = null;
    private ArrayList<String> wo = null;
    private ArrayList<Integer> wp = null;
    private ArrayList<byte[]> wq = null;
    private boolean wr = true;
    private final zzasf.zzc ws = new zzasf.zzc();
    private boolean wt = false;
    
    private zza(byte[] paramArrayOfByte)
    {
      this(paramArrayOfByte, null);
    }
    
    private zza(byte[] paramArrayOfByte, zza.zzc paramzzc)
    {
      this.ws.buh = zza.zzf(zza.this).currentTimeMillis();
      this.ws.bui = zza.zzf(zza.this).elapsedRealtime();
      this.ws.bus = zza.zzg(zza.this).zzag(this.ws.buh);
      if (paramArrayOfByte != null) {
        this.ws.buo = paramArrayOfByte;
      }
      this.wm = paramzzc;
    }
    
    public LogEventParcelable zzaqg()
    {
      return new LogEventParcelable(new PlayLoggerContext(zza.zzi(zza.this), zza.zzj(zza.this), this.we, this.wd, this.wf, this.wg, zza.zzh(zza.this), this.wi), this.ws, this.wm, null, zza.zze(null), zza.zzf(null), zza.zze(null), zza.zzg(null), this.wr);
    }
    
    @Deprecated
    public PendingResult<Status> zzaqh()
    {
      if (this.wt) {
        throw new IllegalStateException("do not reuse LogEventBuilder");
      }
      this.wt = true;
      LogEventParcelable localLogEventParcelable = zzaqg();
      PlayLoggerContext localPlayLoggerContext = localLogEventParcelable.wv;
      if (zza.zzk(zza.this).zzh(localPlayLoggerContext.aAG, localPlayLoggerContext.aAC)) {
        return zza.zzl(zza.this).zza(localLogEventParcelable);
      }
      return PendingResults.immediatePendingResult(Status.xZ);
    }
    
    @Deprecated
    public PendingResult<Status> zze(GoogleApiClient paramGoogleApiClient)
    {
      return zzaqh();
    }
    
    public zza zzfl(int paramInt)
    {
      this.ws.buk = paramInt;
      return this;
    }
    
    public zza zzfm(int paramInt)
    {
      this.ws.zzajo = paramInt;
      return this;
    }
  }
  
  public static abstract interface zzb
  {
    public abstract boolean zzh(String paramString, int paramInt);
  }
  
  public static abstract interface zzc
  {
    public abstract byte[] zzaqi();
  }
  
  public static class zzd
  {
    public long zzag(long paramLong)
    {
      return TimeZone.getDefault().getOffset(paramLong) / 1000;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/clearcut/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */