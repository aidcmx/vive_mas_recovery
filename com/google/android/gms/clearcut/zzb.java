package com.google.android.gms.clearcut;

import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;

public abstract interface zzb
{
  public abstract PendingResult<Status> zza(LogEventParcelable paramLogEventParcelable);
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/clearcut/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */