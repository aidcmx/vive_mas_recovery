package com.google.android.gms.common.api;

public final class BatchResultToken<R extends Result>
{
  protected final int mId;
  
  BatchResultToken(int paramInt)
  {
    this.mId = paramInt;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/common/api/BatchResultToken.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */