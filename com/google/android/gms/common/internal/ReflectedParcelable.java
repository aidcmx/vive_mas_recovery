package com.google.android.gms.common.internal;

import android.os.Parcelable;

public abstract interface ReflectedParcelable
  extends Parcelable
{}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/common/internal/ReflectedParcelable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */