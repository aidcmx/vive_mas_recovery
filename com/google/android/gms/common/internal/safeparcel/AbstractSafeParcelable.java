package com.google.android.gms.common.internal.safeparcel;

public abstract class AbstractSafeParcelable
  implements SafeParcelable
{
  public final int describeContents()
  {
    return 0;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/common/internal/safeparcel/AbstractSafeParcelable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */