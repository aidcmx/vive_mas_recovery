package com.google.android.gms.common.stats;

import android.content.ComponentName;

public final class zzc
{
  public static final ComponentName FP = new ComponentName("com.google.android.gms", "com.google.android.gms.common.stats.GmsCoreStatsService");
  public static int FQ = 1;
  public static int FR = 2;
  public static int FS = 4;
  public static int FT = 8;
  public static int FU = 16;
  public static int FV = 32;
  public static int FW = 1;
  public static int LOG_LEVEL_OFF = 0;
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/common/stats/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */