package com.google.android.gms.common.util;

public abstract interface zze
{
  public abstract long currentTimeMillis();
  
  public abstract long elapsedRealtime();
  
  public abstract long nanoTime();
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/common/util/zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */