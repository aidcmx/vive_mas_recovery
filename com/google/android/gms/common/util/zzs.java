package com.google.android.gms.common.util;

import android.os.Build.VERSION;

public final class zzs
{
  public static boolean isAtLeastN()
  {
    return Build.VERSION.SDK_INT >= 24;
  }
  
  public static boolean zzayn()
  {
    return Build.VERSION.SDK_INT >= 11;
  }
  
  public static boolean zzayo()
  {
    return Build.VERSION.SDK_INT >= 12;
  }
  
  public static boolean zzayp()
  {
    return Build.VERSION.SDK_INT >= 13;
  }
  
  public static boolean zzayq()
  {
    return Build.VERSION.SDK_INT >= 14;
  }
  
  public static boolean zzayr()
  {
    return Build.VERSION.SDK_INT >= 16;
  }
  
  public static boolean zzays()
  {
    return Build.VERSION.SDK_INT >= 17;
  }
  
  public static boolean zzayt()
  {
    return Build.VERSION.SDK_INT >= 18;
  }
  
  public static boolean zzayu()
  {
    return Build.VERSION.SDK_INT >= 19;
  }
  
  public static boolean zzayv()
  {
    return Build.VERSION.SDK_INT >= 20;
  }
  
  @Deprecated
  public static boolean zzayw()
  {
    return zzayx();
  }
  
  public static boolean zzayx()
  {
    return Build.VERSION.SDK_INT >= 21;
  }
  
  public static boolean zzayy()
  {
    return Build.VERSION.SDK_INT >= 23;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/common/util/zzs.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */