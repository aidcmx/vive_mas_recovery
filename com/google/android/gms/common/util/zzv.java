package com.google.android.gms.common.util;

import java.util.regex.Pattern;

public class zzv
{
  private static final Pattern GG = Pattern.compile("\\$\\{(.*?)\\}");
  
  public static boolean zzij(String paramString)
  {
    return (paramString == null) || (paramString.trim().isEmpty());
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/common/util/zzv.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */