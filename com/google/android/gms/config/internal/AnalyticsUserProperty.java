package com.google.android.gms.config.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public class AnalyticsUserProperty
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<AnalyticsUserProperty> CREATOR = new zza();
  private final String mName;
  private final String mValue;
  private final int mVersionCode;
  
  AnalyticsUserProperty(int paramInt, String paramString1, String paramString2)
  {
    this.mVersionCode = paramInt;
    this.mName = paramString1;
    this.mValue = paramString2;
  }
  
  public AnalyticsUserProperty(String paramString1, String paramString2)
  {
    this(1, paramString1, paramString2);
  }
  
  public String getName()
  {
    return this.mName;
  }
  
  public String getValue()
  {
    return this.mValue;
  }
  
  public int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zza.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/config/internal/AnalyticsUserProperty.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */