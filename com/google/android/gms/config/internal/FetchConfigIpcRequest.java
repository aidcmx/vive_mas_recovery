package com.google.android.gms.config.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.util.List;

public class FetchConfigIpcRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<FetchConfigIpcRequest> CREATOR = new zzg();
  private final int GU;
  private final List<AnalyticsUserProperty> GV;
  private final long He;
  private final DataHolder Hf;
  private final String Hg;
  private final String Hh;
  private final String Hi;
  private final List<String> Hj;
  public final int mVersionCode;
  private final String zzcjc;
  
  FetchConfigIpcRequest(int paramInt1, String paramString1, long paramLong, DataHolder paramDataHolder, String paramString2, String paramString3, String paramString4, List<String> paramList, int paramInt2, List<AnalyticsUserProperty> paramList1)
  {
    this.mVersionCode = paramInt1;
    this.zzcjc = paramString1;
    this.He = paramLong;
    this.Hf = paramDataHolder;
    this.Hg = paramString2;
    this.Hh = paramString3;
    this.Hi = paramString4;
    this.Hj = paramList;
    this.GU = paramInt2;
    this.GV = paramList1;
  }
  
  public FetchConfigIpcRequest(String paramString1, long paramLong, DataHolder paramDataHolder, String paramString2, String paramString3, String paramString4, List<String> paramList, int paramInt, List<AnalyticsUserProperty> paramList1)
  {
    this(2, paramString1, paramLong, paramDataHolder, paramString2, paramString3, paramString4, paramList, paramInt, paramList1);
  }
  
  public String getPackageName()
  {
    return this.zzcjc;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzg.zza(this, paramParcel, paramInt);
  }
  
  public int zzazh()
  {
    return this.GU;
  }
  
  public long zzazk()
  {
    return this.He;
  }
  
  public DataHolder zzazl()
  {
    return this.Hf;
  }
  
  public String zzazm()
  {
    return this.Hg;
  }
  
  public String zzazn()
  {
    return this.Hh;
  }
  
  public String zzazo()
  {
    return this.Hi;
  }
  
  public List<String> zzazp()
  {
    return this.Hj;
  }
  
  public List<AnalyticsUserProperty> zzazq()
  {
    return this.GV;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/config/internal/FetchConfigIpcRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */