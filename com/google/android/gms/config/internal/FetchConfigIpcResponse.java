package com.google.android.gms.config.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public class FetchConfigIpcResponse
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<FetchConfigIpcResponse> CREATOR = new zzh();
  private final long Hd;
  private final DataHolder Hk;
  private final int mVersionCode;
  private final int uo;
  
  FetchConfigIpcResponse(int paramInt1, int paramInt2, DataHolder paramDataHolder, long paramLong)
  {
    this.mVersionCode = paramInt1;
    this.uo = paramInt2;
    this.Hk = paramDataHolder;
    this.Hd = paramLong;
  }
  
  public int getStatusCode()
  {
    return this.uo;
  }
  
  public long getThrottleEndTimeMillis()
  {
    return this.Hd;
  }
  
  public int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzh.zza(this, paramParcel, paramInt);
  }
  
  public DataHolder zzazr()
  {
    return this.Hk;
  }
  
  public void zzazs()
  {
    if ((this.Hk != null) && (!this.Hk.isClosed())) {
      this.Hk.close();
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/config/internal/FetchConfigIpcResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */