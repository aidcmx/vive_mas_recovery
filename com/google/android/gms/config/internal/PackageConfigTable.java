package com.google.android.gms.config.internal;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public class PackageConfigTable
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<PackageConfigTable> CREATOR = new zzk();
  private final Bundle Hl;
  private final int mVersionCode;
  
  PackageConfigTable(int paramInt, Bundle paramBundle)
  {
    this.mVersionCode = paramInt;
    this.Hl = paramBundle;
  }
  
  public int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzk.zza(this, paramParcel, paramInt);
  }
  
  public Bundle zzazt()
  {
    return this.Hl;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/config/internal/PackageConfigTable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */