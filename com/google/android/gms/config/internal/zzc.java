package com.google.android.gms.config.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataBuffer;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.DataHolder.zza;
import com.google.android.gms.common.data.zzd;
import com.google.android.gms.internal.zzqo.zza;
import com.google.android.gms.internal.zzrk;
import com.google.android.gms.internal.zzta;
import com.google.android.gms.internal.zztb;
import com.google.android.gms.internal.zztb.zza;
import com.google.android.gms.internal.zztb.zzb;
import com.google.android.gms.internal.zztc;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Pattern;

public class zzc
  implements zztb
{
  private static final Pattern GW = Pattern.compile("^(1|true|t|yes|y|on)$", 2);
  private static final Pattern GX = Pattern.compile("^(0|false|f|no|n|off|)$", 2);
  private static final Charset UTF_8 = Charset.forName("UTF-8");
  
  private static HashMap<String, TreeMap<String, byte[]>> zza(FetchConfigIpcResponse paramFetchConfigIpcResponse)
  {
    if (paramFetchConfigIpcResponse == null) {}
    do
    {
      return null;
      localObject1 = paramFetchConfigIpcResponse.zzazr();
    } while (localObject1 == null);
    Object localObject1 = (PackageConfigTable)new zzd((DataHolder)localObject1, PackageConfigTable.CREATOR).get(0);
    paramFetchConfigIpcResponse.zzazs();
    paramFetchConfigIpcResponse = new HashMap();
    Iterator localIterator1 = ((PackageConfigTable)localObject1).zzazt().keySet().iterator();
    while (localIterator1.hasNext())
    {
      Object localObject2 = (String)localIterator1.next();
      TreeMap localTreeMap = new TreeMap();
      paramFetchConfigIpcResponse.put(localObject2, localTreeMap);
      localObject2 = ((PackageConfigTable)localObject1).zzazt().getBundle((String)localObject2);
      Iterator localIterator2 = ((Bundle)localObject2).keySet().iterator();
      while (localIterator2.hasNext())
      {
        String str = (String)localIterator2.next();
        localTreeMap.put(str, ((Bundle)localObject2).getByteArray(str));
      }
    }
    return paramFetchConfigIpcResponse;
  }
  
  private static Status zzhl(int paramInt)
  {
    return new Status(paramInt, zztc.getStatusCodeString(paramInt));
  }
  
  public PendingResult<zztb.zzb> zza(GoogleApiClient paramGoogleApiClient, final zztb.zza paramzza)
  {
    if ((paramGoogleApiClient == null) || (paramzza == null)) {
      return null;
    }
    paramGoogleApiClient.zza(new zzc(paramGoogleApiClient)
    {
      protected void zza(Context paramAnonymousContext, zzj paramAnonymouszzj)
        throws RemoteException
      {
        Object localObject1 = zzd.zzaum();
        Object localObject2 = paramzza.zzazg().entrySet().iterator();
        Object localObject3;
        while (((Iterator)localObject2).hasNext())
        {
          localObject3 = (Map.Entry)((Iterator)localObject2).next();
          zzd.zza((DataHolder.zza)localObject1, new CustomVariable((String)((Map.Entry)localObject3).getKey(), (String)((Map.Entry)localObject3).getValue()));
        }
        DataHolder localDataHolder = ((DataHolder.zza)localObject1).zzgc(0);
        if (zzrk.zzby(paramAnonymousContext) == Status.xZ) {
          localObject2 = zzrk.zzatt();
        }
        for (;;)
        {
          try
          {
            localObject1 = com.google.firebase.iid.zzc.C().getId();
          }
          catch (IllegalStateException localIllegalStateException1)
          {
            List localList;
            localObject1 = null;
          }
          try
          {
            localObject3 = com.google.firebase.iid.zzc.C().getToken();
            localList = zzb.zzcq(paramAnonymousContext);
            paramAnonymousContext = new FetchConfigIpcRequest(paramAnonymousContext.getPackageName(), paramzza.zzazf(), localDataHolder, (String)localObject2, (String)localObject1, (String)localObject3, null, paramzza.zzazh(), localList);
            paramAnonymouszzj.zza(this.Ha, paramAnonymousContext);
            localDataHolder.close();
            return;
          }
          catch (IllegalStateException localIllegalStateException2)
          {
            Object localObject4;
            for (;;) {}
          }
          localObject2 = null;
          continue;
          if (Log.isLoggable("ConfigApiImpl", 3)) {
            Log.d("ConfigApiImpl", "Cannot retrieve instanceId or instanceIdToken.", localIllegalStateException1);
          }
          localObject4 = null;
        }
      }
      
      protected zztb.zzb zzag(Status paramAnonymousStatus)
      {
        return new zzc.zzd(paramAnonymousStatus, new HashMap());
      }
    });
  }
  
  static abstract class zza
    extends zzi.zza
  {
    public void zza(Status paramStatus, FetchConfigIpcResponse paramFetchConfigIpcResponse)
    {
      throw new UnsupportedOperationException();
    }
    
    public void zza(Status paramStatus, Map paramMap)
    {
      throw new UnsupportedOperationException();
    }
    
    public void zza(Status paramStatus, byte[] paramArrayOfByte)
    {
      throw new UnsupportedOperationException();
    }
    
    public void zzah(Status paramStatus)
    {
      throw new UnsupportedOperationException();
    }
  }
  
  static abstract class zzb<R extends Result>
    extends zzqo.zza<R, zze>
  {
    public zzb(GoogleApiClient paramGoogleApiClient)
    {
      super(paramGoogleApiClient);
    }
    
    protected abstract void zza(Context paramContext, zzj paramzzj)
      throws RemoteException;
    
    protected final void zza(zze paramzze)
      throws RemoteException
    {
      zza(paramzze.getContext(), (zzj)paramzze.zzavg());
    }
  }
  
  static abstract class zzc
    extends zzc.zzb<zztb.zzb>
  {
    protected zzi Ha = new zzc.zza()
    {
      public void zza(Status paramAnonymousStatus, FetchConfigIpcResponse paramAnonymousFetchConfigIpcResponse)
      {
        if ((paramAnonymousFetchConfigIpcResponse.getStatusCode() == 6502) || (paramAnonymousFetchConfigIpcResponse.getStatusCode() == 6507))
        {
          zzc.zzc.this.zzc(new zzc.zzd(zzc.zzhm(paramAnonymousFetchConfigIpcResponse.getStatusCode()), zzc.zzb(paramAnonymousFetchConfigIpcResponse), paramAnonymousFetchConfigIpcResponse.getThrottleEndTimeMillis()));
          return;
        }
        zzc.zzc.this.zzc(new zzc.zzd(zzc.zzhm(paramAnonymousFetchConfigIpcResponse.getStatusCode()), zzc.zzb(paramAnonymousFetchConfigIpcResponse)));
      }
    };
    
    public zzc(GoogleApiClient paramGoogleApiClient)
    {
      super();
    }
  }
  
  public static class zzd
    implements zztb.zzb
  {
    private final Map<String, TreeMap<String, byte[]>> Hc;
    private final long Hd;
    private final Status hv;
    
    public zzd(Status paramStatus, Map<String, TreeMap<String, byte[]>> paramMap)
    {
      this(paramStatus, paramMap, -1L);
    }
    
    public zzd(Status paramStatus, Map<String, TreeMap<String, byte[]>> paramMap, long paramLong)
    {
      this.hv = paramStatus;
      this.Hc = paramMap;
      this.Hd = paramLong;
    }
    
    public Status getStatus()
    {
      return this.hv;
    }
    
    public long getThrottleEndTimeMillis()
    {
      return this.Hd;
    }
    
    public byte[] zza(String paramString1, byte[] paramArrayOfByte, String paramString2)
    {
      if (zzah(paramString1, paramString2)) {
        return (byte[])((TreeMap)this.Hc.get(paramString2)).get(paramString1);
      }
      return paramArrayOfByte;
    }
    
    public boolean zzah(String paramString1, String paramString2)
    {
      if ((this.Hc == null) || (this.Hc.get(paramString2) == null)) {
        return false;
      }
      return ((TreeMap)this.Hc.get(paramString2)).get(paramString1) != null;
    }
    
    public Map<String, Set<String>> zzazj()
    {
      HashMap localHashMap = new HashMap();
      if (this.Hc != null)
      {
        Iterator localIterator = this.Hc.keySet().iterator();
        while (localIterator.hasNext())
        {
          String str = (String)localIterator.next();
          Map localMap = (Map)this.Hc.get(str);
          if (localMap != null) {
            localHashMap.put(str, localMap.keySet());
          }
        }
      }
      return localHashMap;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/config/internal/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */