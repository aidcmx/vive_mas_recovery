package com.google.android.gms.config.internal;

import android.content.Context;
import com.google.android.gms.common.api.Api.ApiOptions.NoOptions;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.zzc;
import com.google.android.gms.internal.zzqk;
import com.google.android.gms.internal.zzta;
import com.google.android.gms.internal.zztb;
import com.google.android.gms.internal.zztb.zza;
import com.google.android.gms.internal.zztb.zzb;

public class zzd
  extends zzc<Api.ApiOptions.NoOptions>
{
  public zzd(Context paramContext)
  {
    super(paramContext, zzta.API, null, new zzqk());
  }
  
  public PendingResult<zztb.zzb> zza(zztb.zza paramzza)
  {
    return zzta.GR.zza(asGoogleApiClient(), paramzza);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/config/internal/zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */