package com.google.android.gms.config.internal;

import android.content.Context;
import android.os.IBinder;
import android.os.Looper;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.internal.zzf;

public class zze
  extends com.google.android.gms.common.internal.zzj<zzj>
{
  public zze(Context paramContext, Looper paramLooper, zzf paramzzf, GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
  {
    super(paramContext, paramLooper, 64, paramzzf, paramConnectionCallbacks, paramOnConnectionFailedListener);
  }
  
  protected zzj zzec(IBinder paramIBinder)
  {
    return zzj.zza.zzee(paramIBinder);
  }
  
  protected String zzjx()
  {
    return "com.google.android.gms.config.START";
  }
  
  protected String zzjy()
  {
    return "com.google.android.gms.config.internal.IConfigService";
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/config/internal/zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */