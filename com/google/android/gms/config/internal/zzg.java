package com.google.android.gms.config.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.ArrayList;

public class zzg
  implements Parcelable.Creator<FetchConfigIpcRequest>
{
  static void zza(FetchConfigIpcRequest paramFetchConfigIpcRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramFetchConfigIpcRequest.mVersionCode);
    zzb.zza(paramParcel, 2, paramFetchConfigIpcRequest.getPackageName(), false);
    zzb.zza(paramParcel, 3, paramFetchConfigIpcRequest.zzazk());
    zzb.zza(paramParcel, 4, paramFetchConfigIpcRequest.zzazl(), paramInt, false);
    zzb.zza(paramParcel, 5, paramFetchConfigIpcRequest.zzazm(), false);
    zzb.zza(paramParcel, 6, paramFetchConfigIpcRequest.zzazn(), false);
    zzb.zza(paramParcel, 7, paramFetchConfigIpcRequest.zzazo(), false);
    zzb.zzb(paramParcel, 8, paramFetchConfigIpcRequest.zzazp(), false);
    zzb.zzc(paramParcel, 9, paramFetchConfigIpcRequest.zzazh());
    zzb.zzc(paramParcel, 10, paramFetchConfigIpcRequest.zzazq(), false);
    zzb.zzaj(paramParcel, i);
  }
  
  public FetchConfigIpcRequest zzdf(Parcel paramParcel)
  {
    int i = 0;
    ArrayList localArrayList1 = null;
    int k = zza.zzcr(paramParcel);
    long l = 0L;
    ArrayList localArrayList2 = null;
    String str1 = null;
    String str2 = null;
    String str3 = null;
    DataHolder localDataHolder = null;
    String str4 = null;
    int j = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.zzcq(paramParcel);
      switch (zza.zzgu(m))
      {
      default: 
        zza.zzb(paramParcel, m);
        break;
      case 1: 
        j = zza.zzg(paramParcel, m);
        break;
      case 2: 
        str4 = zza.zzq(paramParcel, m);
        break;
      case 3: 
        l = zza.zzi(paramParcel, m);
        break;
      case 4: 
        localDataHolder = (DataHolder)zza.zza(paramParcel, m, DataHolder.CREATOR);
        break;
      case 5: 
        str3 = zza.zzq(paramParcel, m);
        break;
      case 6: 
        str2 = zza.zzq(paramParcel, m);
        break;
      case 7: 
        str1 = zza.zzq(paramParcel, m);
        break;
      case 8: 
        localArrayList2 = zza.zzae(paramParcel, m);
        break;
      case 9: 
        i = zza.zzg(paramParcel, m);
        break;
      case 10: 
        localArrayList1 = zza.zzc(paramParcel, m, AnalyticsUserProperty.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza(37 + "Overread allowed size end=" + k, paramParcel);
    }
    return new FetchConfigIpcRequest(j, str4, l, localDataHolder, str3, str2, str1, localArrayList2, i, localArrayList1);
  }
  
  public FetchConfigIpcRequest[] zzho(int paramInt)
  {
    return new FetchConfigIpcRequest[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/config/internal/zzg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */