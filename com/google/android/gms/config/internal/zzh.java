package com.google.android.gms.config.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzh
  implements Parcelable.Creator<FetchConfigIpcResponse>
{
  static void zza(FetchConfigIpcResponse paramFetchConfigIpcResponse, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramFetchConfigIpcResponse.getVersionCode());
    zzb.zzc(paramParcel, 2, paramFetchConfigIpcResponse.getStatusCode());
    zzb.zza(paramParcel, 3, paramFetchConfigIpcResponse.zzazr(), paramInt, false);
    zzb.zza(paramParcel, 4, paramFetchConfigIpcResponse.getThrottleEndTimeMillis());
    zzb.zzaj(paramParcel, i);
  }
  
  public FetchConfigIpcResponse zzdg(Parcel paramParcel)
  {
    int i = 0;
    int k = zza.zzcr(paramParcel);
    DataHolder localDataHolder = null;
    long l = 0L;
    int j = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.zzcq(paramParcel);
      switch (zza.zzgu(m))
      {
      default: 
        zza.zzb(paramParcel, m);
        break;
      case 1: 
        j = zza.zzg(paramParcel, m);
        break;
      case 2: 
        i = zza.zzg(paramParcel, m);
        break;
      case 3: 
        localDataHolder = (DataHolder)zza.zza(paramParcel, m, DataHolder.CREATOR);
        break;
      case 4: 
        l = zza.zzi(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza(37 + "Overread allowed size end=" + k, paramParcel);
    }
    return new FetchConfigIpcResponse(j, i, localDataHolder, l);
  }
  
  public FetchConfigIpcResponse[] zzhp(int paramInt)
  {
    return new FetchConfigIpcResponse[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/config/internal/zzh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */