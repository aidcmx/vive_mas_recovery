package com.google.android.gms.contextmanager;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.util.Log;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.internal.zzaqx;
import com.google.android.gms.internal.zzara;
import com.google.android.gms.internal.zzarz;
import com.google.android.gms.internal.zzasa;

public class ContextData
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<ContextData> CREATOR = new zzb();
  public static final int[] Hm = { 0, 1 };
  private zzaqx Hn;
  private byte[] Ho;
  private final int mVersionCode;
  
  ContextData(int paramInt, byte[] paramArrayOfByte)
  {
    this.mVersionCode = paramInt;
    this.Hn = null;
    this.Ho = paramArrayOfByte;
    zzazw();
  }
  
  private void zzazu()
  {
    if (!zzazv()) {}
    try
    {
      this.Hn = zzaqx.zzba(this.Ho);
      this.Ho = null;
      zzazw();
      return;
    }
    catch (zzarz localzzarz)
    {
      Log.e("ContextData", "Could not deserialize context bytes.", localzzarz);
      throw new IllegalStateException(localzzarz);
    }
  }
  
  private void zzazw()
  {
    if ((this.Hn == null) && (this.Ho != null)) {}
    while ((this.Hn != null) && (this.Ho == null)) {
      return;
    }
    if ((this.Hn != null) && (this.Ho != null)) {
      throw new IllegalStateException("Invalid internal representation - full");
    }
    if ((this.Hn == null) && (this.Ho == null)) {
      throw new IllegalStateException("Invalid internal representation - empty");
    }
    throw new IllegalStateException("Impossible");
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    do
    {
      return true;
      if (!(paramObject instanceof ContextData)) {
        return false;
      }
      paramObject = (ContextData)paramObject;
      zzazu();
      ((ContextData)paramObject).zzazu();
    } while ((getId().equals(((ContextData)paramObject).getId())) && (this.Hn.bsh.version == ((ContextData)paramObject).Hn.bsh.version));
    return false;
  }
  
  public String getId()
  {
    zzazu();
    return this.Hn.bsg;
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    zzazu();
    return zzz.hashCode(new Object[] { getId(), Integer.valueOf(this.Hn.bsh.version) });
  }
  
  public String toString()
  {
    zzazu();
    return this.Hn.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzb.zza(this, paramParcel, paramInt);
  }
  
  boolean zzazv()
  {
    return this.Hn != null;
  }
  
  byte[] zzazx()
  {
    if (this.Ho != null) {
      return this.Ho;
    }
    return zzasa.zzf(this.Hn);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/contextmanager/ContextData.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */