package com.google.android.gms.contextmanager.fence.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zzz;

public class ContextFenceRegistrationStub
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<ContextFenceRegistrationStub> CREATOR = new zzf();
  public final String Hy;
  public ContextFenceStub Hz;
  public final int versionCode;
  
  ContextFenceRegistrationStub(int paramInt, String paramString, ContextFenceStub paramContextFenceStub)
  {
    this.versionCode = paramInt;
    this.Hy = paramString;
    this.Hz = paramContextFenceStub;
  }
  
  public ContextFenceRegistrationStub(String paramString, ContextFenceStub paramContextFenceStub)
  {
    this(1, zzaa.zzib(paramString), (ContextFenceStub)zzaa.zzy(paramContextFenceStub));
  }
  
  public static ContextFenceRegistrationStub zza(String paramString, ContextFenceStub paramContextFenceStub)
  {
    return new ContextFenceRegistrationStub(paramString, paramContextFenceStub);
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if (!(paramObject instanceof ContextFenceRegistrationStub)) {
      return false;
    }
    paramObject = (ContextFenceRegistrationStub)paramObject;
    return TextUtils.equals(this.Hy, ((ContextFenceRegistrationStub)paramObject).Hy);
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.Hy });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzf.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/contextmanager/fence/internal/ContextFenceRegistrationStub.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */