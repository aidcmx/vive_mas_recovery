package com.google.android.gms.contextmanager.fence.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.awareness.fence.AwarenessFence;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.internal.zzaqz;
import com.google.android.gms.internal.zzarz;
import com.google.android.gms.internal.zzasa;
import com.google.android.gms.internal.zzcd;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class ContextFenceStub
  extends AwarenessFence
{
  public static final Parcelable.Creator<ContextFenceStub> CREATOR = new zzg();
  private zzaqz HA;
  private byte[] HB;
  public final int versionCode;
  
  ContextFenceStub(int paramInt, byte[] paramArrayOfByte)
  {
    this.versionCode = paramInt;
    this.HA = null;
    this.HB = paramArrayOfByte;
    zzazw();
  }
  
  public ContextFenceStub(zzaqz paramzzaqz)
  {
    this.versionCode = 1;
    this.HA = ((zzaqz)zzaa.zzy(paramzzaqz));
    this.HB = null;
    zzazw();
  }
  
  public static ContextFenceStub zza(ContextFenceStub paramContextFenceStub)
  {
    zzaa.zzy(paramContextFenceStub);
    ArrayList localArrayList = new ArrayList();
    localArrayList.add(paramContextFenceStub);
    paramContextFenceStub = zzhv(3);
    paramContextFenceStub.bsq = zzi(localArrayList);
    return new ContextFenceStub(paramContextFenceStub);
  }
  
  public static ContextFenceStub zza(zza paramzza)
  {
    zzaa.zzy(paramzza);
    zzaqz localzzaqz = zzhv(7);
    localzzaqz.bsu = paramzza.zzazy();
    return new ContextFenceStub(localzzaqz);
  }
  
  public static ContextFenceStub zza(zzb paramzzb)
  {
    zzaa.zzy(paramzzb);
    zzaqz localzzaqz = zzhv(11);
    localzzaqz.bsy = paramzzb.zzbab();
    return new ContextFenceStub(localzzaqz);
  }
  
  public static ContextFenceStub zza(zzd paramzzd)
  {
    zzaa.zzy(paramzzd);
    zzaqz localzzaqz = zzhv(12);
    localzzaqz.bsz = paramzzd.zzbac();
    return new ContextFenceStub(localzzaqz);
  }
  
  public static ContextFenceStub zza(zzn paramzzn)
  {
    zzaa.zzy(paramzzn);
    zzaqz localzzaqz = zzhv(5);
    localzzaqz.bss = paramzzn.zzbag();
    return new ContextFenceStub(localzzaqz);
  }
  
  public static ContextFenceStub zza(zzp paramzzp)
  {
    zzaa.zzy(paramzzp);
    zzaqz localzzaqz = zzhv(4);
    localzzaqz.bsr = paramzzp.zzbah();
    return new ContextFenceStub(localzzaqz);
  }
  
  private void zzazu()
  {
    if (!zzazv()) {}
    try
    {
      this.HA = zzaqz.zzbb(this.HB);
      this.HB = null;
      zzazw();
      return;
    }
    catch (zzarz localzzarz)
    {
      zzcd.zza("ContextFenceStub", "Could not deserialize context fence bytes.", localzzarz);
      throw new IllegalStateException(localzzarz);
    }
  }
  
  private boolean zzazv()
  {
    return this.HA != null;
  }
  
  private void zzazw()
  {
    if ((this.HA == null) && (this.HB != null)) {}
    while ((this.HA != null) && (this.HB == null)) {
      return;
    }
    if ((this.HA != null) && (this.HB != null)) {
      throw new IllegalStateException("Invalid internal representation - full");
    }
    if ((this.HA == null) && (this.HB == null)) {
      throw new IllegalStateException("Invalid internal representation - empty");
    }
    throw new IllegalStateException("Impossible");
  }
  
  public static ContextFenceStub zzg(Collection<ContextFenceStub> paramCollection)
  {
    zzaa.zzy(paramCollection);
    if (!paramCollection.isEmpty()) {}
    for (boolean bool = true;; bool = false)
    {
      zzaa.zzbt(bool);
      zzaqz localzzaqz = zzhv(1);
      localzzaqz.bsq = zzi(paramCollection);
      return new ContextFenceStub(localzzaqz);
    }
  }
  
  public static ContextFenceStub zzh(Collection<ContextFenceStub> paramCollection)
  {
    zzaa.zzy(paramCollection);
    if (!paramCollection.isEmpty()) {}
    for (boolean bool = true;; bool = false)
    {
      zzaa.zzbt(bool);
      zzaqz localzzaqz = zzhv(2);
      localzzaqz.bsq = zzi(paramCollection);
      return new ContextFenceStub(localzzaqz);
    }
  }
  
  private static zzaqz zzhv(int paramInt)
  {
    zzaqz localzzaqz = new zzaqz();
    localzzaqz.type = paramInt;
    return localzzaqz;
  }
  
  private static zzaqz[] zzi(Collection<ContextFenceStub> paramCollection)
  {
    zzaqz[] arrayOfzzaqz = new zzaqz[paramCollection.size()];
    paramCollection = paramCollection.iterator();
    int i = 0;
    while (paramCollection.hasNext())
    {
      arrayOfzzaqz[i] = ((ContextFenceStub)paramCollection.next()).zzbae();
      i += 1;
    }
    return arrayOfzzaqz;
  }
  
  public String toString()
  {
    zzazu();
    return this.HA.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzg.zza(this, paramParcel, paramInt);
  }
  
  public byte[] zzbad()
  {
    if (this.HB != null) {
      return this.HB;
    }
    return zzasa.zzf(this.HA);
  }
  
  public zzaqz zzbae()
  {
    zzazu();
    return this.HA;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/contextmanager/fence/internal/ContextFenceStub.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */