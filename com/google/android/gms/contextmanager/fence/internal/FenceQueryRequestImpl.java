package com.google.android.gms.contextmanager.fence.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.awareness.fence.FenceQueryRequest;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class FenceQueryRequestImpl
  extends FenceQueryRequest
{
  public static final Parcelable.Creator<FenceQueryRequestImpl> CREATOR = new zzh();
  public final QueryFenceOperation HC;
  public final int versionCode;
  
  public FenceQueryRequestImpl()
  {
    this(1, QueryFenceOperation.zza(1, null));
  }
  
  FenceQueryRequestImpl(int paramInt, QueryFenceOperation paramQueryFenceOperation)
  {
    this.versionCode = paramInt;
    this.HC = paramQueryFenceOperation;
  }
  
  public FenceQueryRequestImpl(Collection<String> paramCollection)
  {
    this(1, QueryFenceOperation.zza(2, new ArrayList(paramCollection)));
  }
  
  public FenceQueryRequestImpl(String... paramVarArgs)
  {
    this(1, QueryFenceOperation.zza(2, Arrays.asList(paramVarArgs)));
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzh.zza(this, paramParcel, paramInt);
  }
  
  public static class QueryFenceOperation
    extends AbstractSafeParcelable
  {
    public static final Parcelable.Creator<QueryFenceOperation> CREATOR = new zzo();
    public final List<String> HD;
    public final int type;
    public final int versionCode;
    
    QueryFenceOperation(int paramInt1, int paramInt2, List<String> paramList)
    {
      this.versionCode = paramInt1;
      this.type = paramInt2;
      this.HD = paramList;
    }
    
    private QueryFenceOperation(int paramInt, List<String> paramList)
    {
      this(1, paramInt, paramList);
    }
    
    public static QueryFenceOperation zza(int paramInt, List<String> paramList)
    {
      return new QueryFenceOperation(paramInt, paramList);
    }
    
    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
      zzo.zza(this, paramParcel, paramInt);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/contextmanager/fence/internal/FenceQueryRequestImpl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */