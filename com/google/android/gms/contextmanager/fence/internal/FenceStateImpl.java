package com.google.android.gms.contextmanager.fence.internal;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.awareness.fence.FenceState;

public class FenceStateImpl
  extends FenceState
{
  public static final Parcelable.Creator<FenceStateImpl> CREATOR = new zzi();
  public final int HE;
  public final long HF;
  public final String HG;
  public final int HH;
  public final int versionCode;
  
  FenceStateImpl(int paramInt1, int paramInt2, long paramLong, String paramString, int paramInt3)
  {
    this.versionCode = paramInt1;
    this.HE = paramInt2;
    this.HF = paramLong;
    this.HG = paramString;
    this.HH = paramInt3;
  }
  
  public FenceStateImpl(int paramInt1, long paramLong, String paramString, int paramInt2)
  {
    this(1, paramInt1, paramLong, paramString, paramInt2);
  }
  
  public static FenceState extract(Intent paramIntent)
  {
    return new FenceStateImpl(paramIntent.getIntExtra("context_fence_current_state", 0), paramIntent.getLongExtra("context_fence_last_updated_time", 0L), paramIntent.getStringExtra("context_fence_key"), paramIntent.getIntExtra("context_fence_previous_state", 0));
  }
  
  public int getCurrentState()
  {
    return this.HE;
  }
  
  public String getFenceKey()
  {
    return this.HG;
  }
  
  public long getLastFenceUpdateTimeMillis()
  {
    return this.HF;
  }
  
  public int getPreviousState()
  {
    return this.HH;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzi.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/contextmanager/fence/internal/FenceStateImpl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */