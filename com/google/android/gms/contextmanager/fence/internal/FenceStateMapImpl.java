package com.google.android.gms.contextmanager.fence.internal;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.awareness.fence.FenceStateMap;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.zzc;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class FenceStateMapImpl
  extends AbstractSafeParcelable
  implements FenceStateMap
{
  public static final Parcelable.Creator<FenceStateMapImpl> CREATOR = new zzj();
  public final Map<String, FenceStateImpl> HI;
  public final int versionCode;
  
  FenceStateMapImpl(int paramInt, Bundle paramBundle)
  {
    this.versionCode = paramInt;
    this.HI = new HashMap();
    if (paramBundle != null)
    {
      Iterator localIterator = paramBundle.keySet().iterator();
      while (localIterator.hasNext())
      {
        String str = (String)localIterator.next();
        this.HI.put(str, (FenceStateImpl)zzc.zza(paramBundle.getByteArray(str), FenceStateImpl.CREATOR));
      }
    }
  }
  
  public Set<String> getFenceKeys()
  {
    return this.HI.keySet();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzj.zza(this, paramParcel, paramInt);
  }
  
  Bundle zzbaf()
  {
    if (this.HI == null) {
      return null;
    }
    Bundle localBundle = new Bundle();
    Iterator localIterator = this.HI.entrySet().iterator();
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      localBundle.putByteArray((String)localEntry.getKey(), zzc.zza((FenceStateImpl)localEntry.getValue()));
    }
    return localBundle;
  }
  
  public FenceStateImpl zzil(String paramString)
  {
    if (this.HI.containsKey(paramString)) {
      return (FenceStateImpl)this.HI.get(paramString);
    }
    return null;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/contextmanager/fence/internal/FenceStateMapImpl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */