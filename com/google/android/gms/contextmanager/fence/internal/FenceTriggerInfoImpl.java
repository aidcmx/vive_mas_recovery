package com.google.android.gms.contextmanager.fence.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Deprecated
public class FenceTriggerInfoImpl
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<FenceTriggerInfoImpl> CREATOR = new zzk();
  public final boolean HJ;
  public final int versionCode;
  public final String zzcb;
  
  FenceTriggerInfoImpl(int paramInt, boolean paramBoolean, String paramString)
  {
    this.versionCode = paramInt;
    this.HJ = paramBoolean;
    this.zzcb = paramString;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzk.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/contextmanager/fence/internal/FenceTriggerInfoImpl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */