package com.google.android.gms.contextmanager.fence.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.awareness.fence.FenceUpdateRequest;
import com.google.android.gms.awareness.fence.zza;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.internal.zzce;
import java.util.ArrayList;
import java.util.Iterator;

public class FenceUpdateRequestImpl
  extends AbstractSafeParcelable
  implements FenceUpdateRequest
{
  public static final Parcelable.Creator<FenceUpdateRequestImpl> CREATOR = new zzl();
  public final ArrayList<UpdateFenceOperation> HK;
  public final int versionCode;
  
  @Deprecated
  public FenceUpdateRequestImpl()
  {
    this(new ArrayList());
  }
  
  FenceUpdateRequestImpl(int paramInt, ArrayList<UpdateFenceOperation> paramArrayList)
  {
    this.versionCode = paramInt;
    this.HK = paramArrayList;
  }
  
  public FenceUpdateRequestImpl(ArrayList<UpdateFenceOperation> paramArrayList)
  {
    this(1, paramArrayList);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzl.zza(this, paramParcel, paramInt);
  }
  
  public void zza(zzce<zza, zze> paramzzce)
  {
    Iterator localIterator = this.HK.iterator();
    while (localIterator.hasNext()) {
      ((UpdateFenceOperation)localIterator.next()).zzb(paramzzce);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/contextmanager/fence/internal/FenceUpdateRequestImpl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */