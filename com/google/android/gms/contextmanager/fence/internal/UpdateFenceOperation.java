package com.google.android.gms.contextmanager.fence.internal;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.awareness.fence.zza;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.internal.zzce;

public class UpdateFenceOperation
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<UpdateFenceOperation> CREATOR = new zzq();
  public final String HG;
  public final ContextFenceRegistrationStub HN;
  public zzm HO;
  private final zza HP;
  public final PendingIntent HQ;
  public final long HR;
  public final long HS;
  public final int type;
  public final int versionCode;
  
  UpdateFenceOperation(int paramInt1, int paramInt2, ContextFenceRegistrationStub paramContextFenceRegistrationStub, IBinder paramIBinder, PendingIntent paramPendingIntent, String paramString, long paramLong1, long paramLong2)
  {
    this.versionCode = paramInt1;
    this.type = paramInt2;
    this.HN = paramContextFenceRegistrationStub;
    if (paramIBinder == null) {}
    for (paramContextFenceRegistrationStub = null;; paramContextFenceRegistrationStub = zzm.zza.zzef(paramIBinder))
    {
      this.HO = paramContextFenceRegistrationStub;
      this.HP = null;
      this.HQ = paramPendingIntent;
      this.HG = paramString;
      this.HR = paramLong1;
      this.HS = paramLong2;
      return;
    }
  }
  
  private UpdateFenceOperation(int paramInt, ContextFenceRegistrationStub paramContextFenceRegistrationStub, zza paramzza, PendingIntent paramPendingIntent, String paramString, long paramLong1, long paramLong2)
  {
    this.versionCode = 1;
    this.type = paramInt;
    this.HN = paramContextFenceRegistrationStub;
    this.HO = null;
    this.HP = paramzza;
    this.HQ = paramPendingIntent;
    this.HG = paramString;
    this.HR = paramLong1;
    this.HS = paramLong2;
  }
  
  public static final UpdateFenceOperation zza(PendingIntent paramPendingIntent)
  {
    return new UpdateFenceOperation(4, null, null, paramPendingIntent, null, -1L, -1L);
  }
  
  public static final UpdateFenceOperation zza(String paramString, ContextFenceStub paramContextFenceStub, PendingIntent paramPendingIntent)
  {
    return new UpdateFenceOperation(2, ContextFenceRegistrationStub.zza(paramString, paramContextFenceStub), null, paramPendingIntent, null, -1L, -1L);
  }
  
  public static final UpdateFenceOperation zzim(String paramString)
  {
    return new UpdateFenceOperation(5, null, null, null, paramString, -1L, -1L);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzq.zza(this, paramParcel, paramInt);
  }
  
  void zzb(zzce<zza, zze> paramzzce)
  {
    if ((this.HO == null) && (this.HP != null)) {
      this.HO = ((zzm)paramzzce.zzc(this.HP));
    }
  }
  
  IBinder zzbai()
  {
    if (this.HO == null) {
      return null;
    }
    return this.HO.asBinder();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/contextmanager/fence/internal/UpdateFenceOperation.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */