package com.google.android.gms.contextmanager.fence.internal;

import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.internal.zzaqu;

public class zza
{
  private final zzaqu Hp;
  
  public zza(zzaqu paramzzaqu)
  {
    this.Hp = ((zzaqu)zzaa.zzy(paramzzaqu));
  }
  
  public static zza zza(int paramInt, int[] paramArrayOfInt)
  {
    if ((paramArrayOfInt != null) && (paramArrayOfInt.length > 0)) {}
    for (boolean bool = true;; bool = false)
    {
      zzaa.zzbt(bool);
      return new zza(zza(paramInt, paramArrayOfInt, 3000L));
    }
  }
  
  private static zzaqu zza(int paramInt, int[] paramArrayOfInt, long paramLong)
  {
    zzaqu localzzaqu = new zzaqu();
    localzzaqu.brT = paramInt;
    localzzaqu.brU = paramLong;
    localzzaqu.brV = paramArrayOfInt;
    return localzzaqu;
  }
  
  public static zza zzb(int[] paramArrayOfInt)
  {
    return zza(1, paramArrayOfInt);
  }
  
  public zzaqu zzazy()
  {
    return this.Hp;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/contextmanager/fence/internal/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */