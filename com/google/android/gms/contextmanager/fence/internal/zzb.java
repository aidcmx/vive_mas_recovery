package com.google.android.gms.contextmanager.fence.internal;

import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.internal.zzaqv;
import com.google.android.gms.internal.zzcd;

public class zzb
{
  private final zzaqv Hq;
  
  public zzb(zzaqv paramzzaqv)
  {
    this.Hq = ((zzaqv)zzaa.zzy(paramzzaqv));
  }
  
  private static zzaqv zza(int paramInt1, long paramLong, int paramInt2)
  {
    zzaqv localzzaqv = new zzaqv();
    localzzaqv.brT = paramInt1;
    localzzaqv.brU = paramLong;
    switch (paramInt1)
    {
    default: 
      zzcd.zza("AudioStateFenceStub", "Unknown trigger type=%s", Integer.valueOf(paramInt1));
      return localzzaqv;
    case 1: 
    case 2: 
    case 3: 
      localzzaqv.brW = paramInt2;
      return localzzaqv;
    case 4: 
    case 5: 
    case 6: 
      localzzaqv.brX = paramInt2;
      return localzzaqv;
    case 7: 
    case 8: 
    case 9: 
      localzzaqv.brY = paramInt2;
      return localzzaqv;
    case 10: 
    case 11: 
    case 12: 
      localzzaqv.brZ = paramInt2;
      return localzzaqv;
    case 13: 
    case 14: 
    case 15: 
      localzzaqv.bsa = paramInt2;
      return localzzaqv;
    }
    localzzaqv.bsb = paramInt2;
    return localzzaqv;
  }
  
  public static zzb zzazz()
  {
    return new zzb(zza(2, 3000L, 0));
  }
  
  public static zzb zzbaa()
  {
    return new zzb(zza(3, 3000L, 0));
  }
  
  public static zzb zzht(int paramInt)
  {
    return new zzb(zza(1, 0L, paramInt));
  }
  
  public zzaqv zzbab()
  {
    return this.Hq;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/contextmanager/fence/internal/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */