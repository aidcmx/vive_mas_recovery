package com.google.android.gms.contextmanager.fence.internal;

import android.os.RemoteException;
import com.google.android.gms.awareness.FenceApi;
import com.google.android.gms.awareness.fence.FenceQueryRequest;
import com.google.android.gms.awareness.fence.FenceQueryResult;
import com.google.android.gms.awareness.fence.FenceUpdateRequest;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.contextmanager.internal.zzc.zza;
import com.google.android.gms.contextmanager.internal.zzc.zzc;
import com.google.android.gms.contextmanager.internal.zzd;

public class zzc
  implements FenceApi
{
  public PendingResult<FenceQueryResult> queryFences(GoogleApiClient paramGoogleApiClient, final FenceQueryRequest paramFenceQueryRequest)
  {
    paramGoogleApiClient.zza(new zzc.zza(paramGoogleApiClient)
    {
      protected void zza(zzd paramAnonymouszzd)
        throws RemoteException
      {
        paramAnonymouszzd.zza(this, (FenceQueryRequestImpl)paramFenceQueryRequest);
      }
    });
  }
  
  public PendingResult<Status> updateFences(GoogleApiClient paramGoogleApiClient, final FenceUpdateRequest paramFenceUpdateRequest)
  {
    paramGoogleApiClient.zza(new zzc.zzc(paramGoogleApiClient)
    {
      protected void zza(zzd paramAnonymouszzd)
        throws RemoteException
      {
        paramAnonymouszzd.zza(this, (FenceUpdateRequestImpl)paramFenceUpdateRequest);
      }
    });
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/contextmanager/fence/internal/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */