package com.google.android.gms.contextmanager.fence.internal;

import com.google.android.gms.awareness.snapshot.internal.BeaconStateImpl.TypeFilterImpl;
import com.google.android.gms.awareness.state.BeaconState.TypeFilter;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.internal.zzaqw;
import com.google.android.gms.internal.zzaqw.zza;

public class zzd
{
  private final zzaqw Hu;
  
  public zzd(zzaqw paramzzaqw)
  {
    this.Hu = ((zzaqw)zzaa.zzy(paramzzaqw));
  }
  
  private static zzaqw zza(int paramInt, BeaconState.TypeFilter[] paramArrayOfTypeFilter, long paramLong)
  {
    zzaqw localzzaqw = new zzaqw();
    localzzaqw.brT = paramInt;
    if ((paramArrayOfTypeFilter != null) && (paramArrayOfTypeFilter.length != 0))
    {
      localzzaqw.bsc = new zzaqw.zza[paramArrayOfTypeFilter.length];
      int j = paramArrayOfTypeFilter.length;
      paramInt = 0;
      int i = 0;
      while (paramInt < j)
      {
        BeaconStateImpl.TypeFilterImpl localTypeFilterImpl = (BeaconStateImpl.TypeFilterImpl)paramArrayOfTypeFilter[paramInt];
        localzzaqw.bsc[i] = localTypeFilterImpl.zzajv();
        i += 1;
        paramInt += 1;
      }
    }
    localzzaqw.brU = paramLong;
    return localzzaqw;
  }
  
  public static zzd zzb(BeaconState.TypeFilter[] paramArrayOfTypeFilter)
  {
    return new zzd(zza(1, paramArrayOfTypeFilter, 3000L));
  }
  
  public static zzd zzc(BeaconState.TypeFilter[] paramArrayOfTypeFilter)
  {
    return new zzd(zza(2, paramArrayOfTypeFilter, 3000L));
  }
  
  public static zzd zzd(BeaconState.TypeFilter[] paramArrayOfTypeFilter)
  {
    return new zzd(zza(3, paramArrayOfTypeFilter, 3000L));
  }
  
  public zzaqw zzbac()
  {
    return this.Hu;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/contextmanager/fence/internal/zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */