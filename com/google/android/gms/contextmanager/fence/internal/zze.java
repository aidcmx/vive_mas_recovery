package com.google.android.gms.contextmanager.fence.internal;

import android.os.Handler;
import android.os.Looper;
import com.google.android.gms.awareness.fence.zza;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.contextmanager.internal.zzd;
import com.google.android.gms.internal.zzce.zza;

public class zze
  extends zzm.zza
{
  public static final zzce.zza<zza, zze> Hv = new zzce.zza()
  {
    public zze zza(zza paramAnonymouszza, Looper paramAnonymousLooper)
    {
      return new zze(paramAnonymouszza, paramAnonymousLooper, null);
    }
  };
  private final zza Hw;
  private final Handler mHandler;
  
  private zze(zza paramzza, Looper paramLooper)
  {
    this.Hw = ((zza)zzaa.zzy(paramzza));
    this.mHandler = zzd.zza((Looper)zzaa.zzy(paramLooper));
  }
  
  public void zza(FenceTriggerInfoImpl paramFenceTriggerInfoImpl)
  {
    this.mHandler.post(new zza(this.Hw, paramFenceTriggerInfoImpl));
  }
  
  private static class zza
    implements Runnable
  {
    private final zza Hw;
    private final FenceTriggerInfoImpl Hx;
    
    public zza(zza paramzza, FenceTriggerInfoImpl paramFenceTriggerInfoImpl)
    {
      this.Hw = ((zza)zzaa.zzy(paramzza));
      this.Hx = ((FenceTriggerInfoImpl)zzaa.zzy(paramFenceTriggerInfoImpl));
    }
    
    public void run()
    {
      this.Hw.zzi(this.Hx.zzcb, this.Hx.HJ);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/contextmanager/fence/internal/zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */