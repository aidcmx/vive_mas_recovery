package com.google.android.gms.contextmanager.fence.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzf
  implements Parcelable.Creator<ContextFenceRegistrationStub>
{
  static void zza(ContextFenceRegistrationStub paramContextFenceRegistrationStub, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramContextFenceRegistrationStub.versionCode);
    zzb.zza(paramParcel, 2, paramContextFenceRegistrationStub.Hy, false);
    zzb.zza(paramParcel, 3, paramContextFenceRegistrationStub.Hz, paramInt, false);
    zzb.zzaj(paramParcel, i);
  }
  
  public ContextFenceRegistrationStub zzdj(Parcel paramParcel)
  {
    ContextFenceStub localContextFenceStub = null;
    int j = zza.zzcr(paramParcel);
    int i = 0;
    String str = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.zzcq(paramParcel);
      switch (zza.zzgu(k))
      {
      default: 
        zza.zzb(paramParcel, k);
        break;
      case 1: 
        i = zza.zzg(paramParcel, k);
        break;
      case 2: 
        str = zza.zzq(paramParcel, k);
        break;
      case 3: 
        localContextFenceStub = (ContextFenceStub)zza.zza(paramParcel, k, ContextFenceStub.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new ContextFenceRegistrationStub(i, str, localContextFenceStub);
  }
  
  public ContextFenceRegistrationStub[] zzhu(int paramInt)
  {
    return new ContextFenceRegistrationStub[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/contextmanager/fence/internal/zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */