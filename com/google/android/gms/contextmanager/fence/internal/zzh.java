package com.google.android.gms.contextmanager.fence.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzh
  implements Parcelable.Creator<FenceQueryRequestImpl>
{
  static void zza(FenceQueryRequestImpl paramFenceQueryRequestImpl, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramFenceQueryRequestImpl.versionCode);
    zzb.zza(paramParcel, 2, paramFenceQueryRequestImpl.HC, paramInt, false);
    zzb.zzaj(paramParcel, i);
  }
  
  public FenceQueryRequestImpl zzdl(Parcel paramParcel)
  {
    int j = zza.zzcr(paramParcel);
    int i = 0;
    FenceQueryRequestImpl.QueryFenceOperation localQueryFenceOperation = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.zzcq(paramParcel);
      switch (zza.zzgu(k))
      {
      default: 
        zza.zzb(paramParcel, k);
        break;
      case 1: 
        i = zza.zzg(paramParcel, k);
        break;
      case 2: 
        localQueryFenceOperation = (FenceQueryRequestImpl.QueryFenceOperation)zza.zza(paramParcel, k, FenceQueryRequestImpl.QueryFenceOperation.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new FenceQueryRequestImpl(i, localQueryFenceOperation);
  }
  
  public FenceQueryRequestImpl[] zzhx(int paramInt)
  {
    return new FenceQueryRequestImpl[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/contextmanager/fence/internal/zzh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */