package com.google.android.gms.contextmanager.fence.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzi
  implements Parcelable.Creator<FenceStateImpl>
{
  static void zza(FenceStateImpl paramFenceStateImpl, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramFenceStateImpl.versionCode);
    zzb.zzc(paramParcel, 2, paramFenceStateImpl.HE);
    zzb.zza(paramParcel, 3, paramFenceStateImpl.HF);
    zzb.zza(paramParcel, 4, paramFenceStateImpl.HG, false);
    zzb.zzc(paramParcel, 5, paramFenceStateImpl.HH);
    zzb.zzaj(paramParcel, paramInt);
  }
  
  public FenceStateImpl zzdm(Parcel paramParcel)
  {
    int i = 0;
    int m = zza.zzcr(paramParcel);
    long l = 0L;
    String str = null;
    int j = 0;
    int k = 0;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.zzcq(paramParcel);
      switch (zza.zzgu(n))
      {
      default: 
        zza.zzb(paramParcel, n);
        break;
      case 1: 
        k = zza.zzg(paramParcel, n);
        break;
      case 2: 
        j = zza.zzg(paramParcel, n);
        break;
      case 3: 
        l = zza.zzi(paramParcel, n);
        break;
      case 4: 
        str = zza.zzq(paramParcel, n);
        break;
      case 5: 
        i = zza.zzg(paramParcel, n);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza(37 + "Overread allowed size end=" + m, paramParcel);
    }
    return new FenceStateImpl(k, j, l, str, i);
  }
  
  public FenceStateImpl[] zzhy(int paramInt)
  {
    return new FenceStateImpl[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/contextmanager/fence/internal/zzi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */