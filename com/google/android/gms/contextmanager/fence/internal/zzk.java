package com.google.android.gms.contextmanager.fence.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzk
  implements Parcelable.Creator<FenceTriggerInfoImpl>
{
  static void zza(FenceTriggerInfoImpl paramFenceTriggerInfoImpl, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramFenceTriggerInfoImpl.versionCode);
    zzb.zza(paramParcel, 2, paramFenceTriggerInfoImpl.HJ);
    zzb.zza(paramParcel, 3, paramFenceTriggerInfoImpl.zzcb, false);
    zzb.zzaj(paramParcel, paramInt);
  }
  
  public FenceTriggerInfoImpl zzdo(Parcel paramParcel)
  {
    boolean bool = false;
    int j = zza.zzcr(paramParcel);
    String str = null;
    int i = 0;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.zzcq(paramParcel);
      switch (zza.zzgu(k))
      {
      default: 
        zza.zzb(paramParcel, k);
        break;
      case 1: 
        i = zza.zzg(paramParcel, k);
        break;
      case 2: 
        bool = zza.zzc(paramParcel, k);
        break;
      case 3: 
        str = zza.zzq(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new FenceTriggerInfoImpl(i, bool, str);
  }
  
  public FenceTriggerInfoImpl[] zzia(int paramInt)
  {
    return new FenceTriggerInfoImpl[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/contextmanager/fence/internal/zzk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */