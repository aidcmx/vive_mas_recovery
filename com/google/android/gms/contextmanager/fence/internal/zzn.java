package com.google.android.gms.contextmanager.fence.internal;

import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.internal.zzarg;

public class zzn
{
  private final zzarg HL;
  
  public zzn(zzarg paramzzarg)
  {
    this.HL = ((zzarg)zzaa.zzy(paramzzarg));
  }
  
  public static zzn zza(int paramInt1, int paramInt2, double paramDouble)
  {
    if (paramDouble >= 0.0D) {}
    for (boolean bool = true;; bool = false)
    {
      zzaa.zzbt(bool);
      return new zzn(zza(2, 1, paramInt1, paramInt2, paramDouble, paramDouble, 3000L, 0L));
    }
  }
  
  public static zzn zza(int paramInt1, int paramInt2, double paramDouble, long paramLong)
  {
    if (paramDouble >= 0.0D) {}
    for (boolean bool = true;; bool = false)
    {
      zzaa.zzbt(bool);
      return new zzn(zza(1, 1, paramInt1, paramInt2, paramDouble, paramDouble, 0L, paramLong));
    }
  }
  
  private static zzarg zza(int paramInt1, int paramInt2, int paramInt3, int paramInt4, double paramDouble1, double paramDouble2, long paramLong1, long paramLong2)
  {
    zzarg localzzarg = new zzarg();
    localzzarg.brT = paramInt1;
    localzzarg.bte = paramInt2;
    localzzarg.brU = paramLong1;
    localzzarg.btj = paramLong2;
    if (paramInt2 == 1)
    {
      localzzarg.btf = paramInt3;
      localzzarg.btg = paramInt4;
      localzzarg.bth = paramDouble1;
      localzzarg.bti = paramDouble2;
    }
    return localzzarg;
  }
  
  public static zzn zzb(int paramInt1, int paramInt2, double paramDouble)
  {
    if (paramDouble >= 0.0D) {}
    for (boolean bool = true;; bool = false)
    {
      zzaa.zzbt(bool);
      return new zzn(zza(3, 1, paramInt1, paramInt2, paramDouble, paramDouble, 3000L, 0L));
    }
  }
  
  public zzarg zzbag()
  {
    return this.HL;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/contextmanager/fence/internal/zzn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */