package com.google.android.gms.contextmanager.fence.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.ArrayList;

public class zzo
  implements Parcelable.Creator<FenceQueryRequestImpl.QueryFenceOperation>
{
  static void zza(FenceQueryRequestImpl.QueryFenceOperation paramQueryFenceOperation, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramQueryFenceOperation.versionCode);
    zzb.zzc(paramParcel, 2, paramQueryFenceOperation.type);
    zzb.zzb(paramParcel, 3, paramQueryFenceOperation.HD, false);
    zzb.zzaj(paramParcel, paramInt);
  }
  
  public FenceQueryRequestImpl.QueryFenceOperation zzdq(Parcel paramParcel)
  {
    int j = 0;
    int k = zza.zzcr(paramParcel);
    ArrayList localArrayList = null;
    int i = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.zzcq(paramParcel);
      switch (zza.zzgu(m))
      {
      default: 
        zza.zzb(paramParcel, m);
        break;
      case 1: 
        i = zza.zzg(paramParcel, m);
        break;
      case 2: 
        j = zza.zzg(paramParcel, m);
        break;
      case 3: 
        localArrayList = zza.zzae(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza(37 + "Overread allowed size end=" + k, paramParcel);
    }
    return new FenceQueryRequestImpl.QueryFenceOperation(i, j, localArrayList);
  }
  
  public FenceQueryRequestImpl.QueryFenceOperation[] zzic(int paramInt)
  {
    return new FenceQueryRequestImpl.QueryFenceOperation[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/contextmanager/fence/internal/zzo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */