package com.google.android.gms.contextmanager.fence.internal;

import android.text.TextUtils;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.internal.zzarq;
import java.util.TimeZone;

public class zzp
{
  private final zzarq HM;
  
  public zzp(zzarq paramzzarq)
  {
    this.HM = ((zzarq)zzaa.zzy(paramzzarq));
  }
  
  public static zzp zza(int paramInt, TimeZone paramTimeZone, long paramLong1, long paramLong2)
  {
    boolean bool2 = true;
    if (paramInt != 1)
    {
      bool1 = true;
      zzaa.zzbt(bool1);
      zzaa.zzy(paramTimeZone);
      if (paramLong1 < 0L) {
        break label121;
      }
      bool1 = true;
      label30:
      zzaa.zzbt(bool1);
      if (paramLong1 > 86400000L) {
        break label127;
      }
      bool1 = true;
      label46:
      zzaa.zzbt(bool1);
      if (paramLong2 < 0L) {
        break label133;
      }
      bool1 = true;
      label61:
      zzaa.zzbt(bool1);
      if (paramLong2 > 86400000L) {
        break label139;
      }
      bool1 = true;
      label78:
      zzaa.zzbt(bool1);
      if (paramLong1 > paramLong2) {
        break label145;
      }
    }
    label121:
    label127:
    label133:
    label139:
    label145:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      zzaa.zzbt(bool1);
      return new zzp(zzb(paramInt, paramTimeZone, paramLong1, paramLong2));
      bool1 = false;
      break;
      bool1 = false;
      break label30;
      bool1 = false;
      break label46;
      bool1 = false;
      break label61;
      bool1 = false;
      break label78;
    }
  }
  
  private static zzarq zzb(int paramInt, TimeZone paramTimeZone, long paramLong1, long paramLong2)
  {
    zzarq localzzarq = new zzarq();
    localzzarq.brT = paramInt;
    if ((paramTimeZone != null) && (!TextUtils.isEmpty(paramTimeZone.getID()))) {
      localzzarq.bts = paramTimeZone.getID();
    }
    localzzarq.IK = paramLong1;
    localzzarq.btt = paramLong2;
    return localzzarq;
  }
  
  public static zzp zze(long paramLong1, long paramLong2)
  {
    boolean bool2 = false;
    if (paramLong1 >= 0L)
    {
      bool1 = true;
      zzaa.zzbt(bool1);
      if (paramLong2 < 0L) {
        break label70;
      }
    }
    label70:
    for (boolean bool1 = true;; bool1 = false)
    {
      zzaa.zzbt(bool1);
      bool1 = bool2;
      if (paramLong1 <= paramLong2) {
        bool1 = true;
      }
      zzaa.zzbt(bool1);
      return new zzp(zzb(1, null, paramLong1, paramLong2));
      bool1 = false;
      break;
    }
  }
  
  public zzarq zzbah()
  {
    return this.HM;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/contextmanager/fence/internal/zzp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */