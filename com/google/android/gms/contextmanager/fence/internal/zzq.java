package com.google.android.gms.contextmanager.fence.internal;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzq
  implements Parcelable.Creator<UpdateFenceOperation>
{
  static void zza(UpdateFenceOperation paramUpdateFenceOperation, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramUpdateFenceOperation.versionCode);
    zzb.zzc(paramParcel, 2, paramUpdateFenceOperation.type);
    zzb.zza(paramParcel, 3, paramUpdateFenceOperation.HN, paramInt, false);
    zzb.zza(paramParcel, 4, paramUpdateFenceOperation.zzbai(), false);
    zzb.zza(paramParcel, 5, paramUpdateFenceOperation.HQ, paramInt, false);
    zzb.zza(paramParcel, 6, paramUpdateFenceOperation.HG, false);
    zzb.zza(paramParcel, 7, paramUpdateFenceOperation.HR);
    zzb.zza(paramParcel, 8, paramUpdateFenceOperation.HS);
    zzb.zzaj(paramParcel, i);
  }
  
  public UpdateFenceOperation zzdr(Parcel paramParcel)
  {
    long l1 = 0L;
    int i = 0;
    String str = null;
    int k = zza.zzcr(paramParcel);
    long l2 = 0L;
    PendingIntent localPendingIntent = null;
    IBinder localIBinder = null;
    ContextFenceRegistrationStub localContextFenceRegistrationStub = null;
    int j = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.zzcq(paramParcel);
      switch (zza.zzgu(m))
      {
      default: 
        zza.zzb(paramParcel, m);
        break;
      case 1: 
        j = zza.zzg(paramParcel, m);
        break;
      case 2: 
        i = zza.zzg(paramParcel, m);
        break;
      case 3: 
        localContextFenceRegistrationStub = (ContextFenceRegistrationStub)zza.zza(paramParcel, m, ContextFenceRegistrationStub.CREATOR);
        break;
      case 4: 
        localIBinder = zza.zzr(paramParcel, m);
        break;
      case 5: 
        localPendingIntent = (PendingIntent)zza.zza(paramParcel, m, PendingIntent.CREATOR);
        break;
      case 6: 
        str = zza.zzq(paramParcel, m);
        break;
      case 7: 
        l2 = zza.zzi(paramParcel, m);
        break;
      case 8: 
        l1 = zza.zzi(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza(37 + "Overread allowed size end=" + k, paramParcel);
    }
    return new UpdateFenceOperation(j, i, localContextFenceRegistrationStub, localIBinder, localPendingIntent, str, l2, l1);
  }
  
  public UpdateFenceOperation[] zzid(int paramInt)
  {
    return new UpdateFenceOperation[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/contextmanager/fence/internal/zzq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */