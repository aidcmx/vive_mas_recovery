package com.google.android.gms.contextmanager.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import java.util.ArrayList;
import java.util.Iterator;

public class ContextDataFilterImpl
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<ContextDataFilterImpl> CREATOR = new zza();
  public final ArrayList<Inclusion> HT;
  public final ArrayList<String> HU;
  public final QueryFilterParameters HV;
  public final int versionCode;
  
  public ContextDataFilterImpl(int paramInt, ArrayList<Inclusion> paramArrayList, ArrayList<String> paramArrayList1, QueryFilterParameters paramQueryFilterParameters)
  {
    this.versionCode = paramInt;
    this.HT = paramArrayList;
    this.HU = paramArrayList1;
    this.HV = paramQueryFilterParameters;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    do
    {
      return true;
      if (!(paramObject instanceof ContextDataFilterImpl)) {
        return false;
      }
      paramObject = (ContextDataFilterImpl)paramObject;
    } while ((zzz.equal(this.HT, ((ContextDataFilterImpl)paramObject).HT)) && (zzz.equal(this.HU, ((ContextDataFilterImpl)paramObject).HU)));
    return false;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.HT, this.HU });
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("contexts=[");
    if ((this.HT != null) && (this.HT.size() > 0))
    {
      Iterator localIterator = this.HT.iterator();
      while (localIterator.hasNext()) {
        localStringBuilder.append(((Inclusion)localIterator.next()).name).append(",");
      }
    }
    localStringBuilder.append("]");
    return localStringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zza.zza(this, paramParcel, paramInt);
  }
  
  public static class Inclusion
    extends AbstractSafeParcelable
  {
    public static final Parcelable.Creator<Inclusion> CREATOR = new zzb();
    public final int HW;
    public final TimeFilterImpl HX;
    public final KeyFilterImpl HY;
    public final int name;
    public final int versionCode;
    
    Inclusion(int paramInt1, int paramInt2, int paramInt3, TimeFilterImpl paramTimeFilterImpl, KeyFilterImpl paramKeyFilterImpl)
    {
      this.versionCode = paramInt1;
      this.HW = paramInt2;
      this.name = paramInt3;
      this.HX = paramTimeFilterImpl;
      this.HY = paramKeyFilterImpl;
    }
    
    public boolean equals(Object paramObject)
    {
      if (this == paramObject) {}
      do
      {
        return true;
        if (!(paramObject instanceof Inclusion)) {
          return false;
        }
        paramObject = (Inclusion)paramObject;
      } while ((this.HW == ((Inclusion)paramObject).HW) && (this.name == ((Inclusion)paramObject).name) && (this.HX.equals(((Inclusion)paramObject).HX)) && (zzz.equal(this.HY, ((Inclusion)paramObject).HY)));
      return false;
    }
    
    public int hashCode()
    {
      return zzz.hashCode(new Object[] { Integer.valueOf(this.HW), Integer.valueOf(this.name), this.HX, this.HY });
    }
    
    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
      zzb.zza(this, paramParcel, paramInt);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/contextmanager/internal/ContextDataFilterImpl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */