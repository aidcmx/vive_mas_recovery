package com.google.android.gms.contextmanager.internal;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.Process;
import com.google.android.gms.awareness.AwarenessOptions;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.zzc;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.util.zzd;
import com.google.android.gms.internal.zzca;

public class ContextManagerClientInfo
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<ContextManagerClientInfo> CREATOR = new zze();
  public final String Ie;
  public final int If;
  public final int Ig;
  public final String Ih;
  public final String Ii;
  public final int Ij;
  private zzca Ik;
  public final String moduleId;
  public final String packageName;
  public final int uid;
  public final int versionCode;
  
  ContextManagerClientInfo(int paramInt1, String paramString1, String paramString2, int paramInt2, String paramString3, int paramInt3, int paramInt4, String paramString4, String paramString5, int paramInt5)
  {
    this.versionCode = paramInt1;
    this.Ie = paramString1;
    this.packageName = paramString2;
    this.uid = paramInt2;
    this.moduleId = paramString3;
    this.If = paramInt3;
    this.Ig = paramInt4;
    this.Ih = paramString4;
    this.Ii = paramString5;
    this.Ij = paramInt5;
  }
  
  public ContextManagerClientInfo(String paramString1, String paramString2, int paramInt1, String paramString3, int paramInt2, int paramInt3, String paramString4, String paramString5, int paramInt4)
  {
    this(1, zzaa.zzib(paramString1), zzaa.zzib(paramString2), paramInt1, zzaa.zzib(paramString3), paramInt2, paramInt3, paramString4, paramString5, paramInt4);
  }
  
  public static ContextManagerClientInfo zza(Context paramContext, String paramString, AwarenessOptions paramAwarenessOptions)
  {
    return new ContextManagerClientInfo(paramString, paramContext.getPackageName(), Process.myUid(), paramAwarenessOptions.zzajp(), zzd.zzv(paramContext, paramContext.getPackageName()), paramAwarenessOptions.zzajq(), paramAwarenessOptions.zzajr(), paramAwarenessOptions.zzajs(), paramAwarenessOptions.zzajt());
  }
  
  public static ContextManagerClientInfo zzz(Context paramContext, String paramString)
  {
    return new ContextManagerClientInfo(paramString, paramContext.getPackageName(), Process.myUid(), paramContext.getPackageName(), zzd.zzv(paramContext, paramContext.getPackageName()), 3, null, null, -1);
  }
  
  public byte[] toByteArray()
  {
    return zzc.zza(this);
  }
  
  public String toString()
  {
    String str1 = String.valueOf(zzbak());
    String str2 = this.packageName;
    int i = this.uid;
    String str3 = this.moduleId;
    int j = this.If;
    String str4 = String.valueOf(zzbal());
    String str5 = this.Ih;
    String str6 = this.Ii;
    return String.valueOf(str1).length() + 68 + String.valueOf(str2).length() + String.valueOf(str3).length() + String.valueOf(str4).length() + String.valueOf(str5).length() + String.valueOf(str6).length() + "(accnt=" + str1 + ", " + str2 + "(" + i + "):" + str3 + ", vrsn=" + j + ", " + str4 + ", 3pPkg = " + str5 + " ,  3pMdlId = " + str6 + ")";
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zze.zza(this, paramParcel, paramInt);
  }
  
  public zzca zzbak()
  {
    if (this.Ie == null) {
      return null;
    }
    if (this.Ik == null) {
      this.Ik = new zzca(this.Ie);
    }
    return this.Ik;
  }
  
  public String zzbal()
  {
    return Integer.toString(this.Ig);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/contextmanager/internal/ContextManagerClientInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */