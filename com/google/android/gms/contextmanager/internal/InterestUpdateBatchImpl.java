package com.google.android.gms.contextmanager.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.contextmanager.interest.InterestRecordStub;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.util.ArrayList;

public class InterestUpdateBatchImpl
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<InterestUpdateBatchImpl> CREATOR = new zzj();
  public final ArrayList<Operation> HK;
  public final int versionCode;
  
  public InterestUpdateBatchImpl()
  {
    this.versionCode = 1;
    this.HK = new ArrayList();
  }
  
  InterestUpdateBatchImpl(int paramInt, ArrayList<Operation> paramArrayList)
  {
    this.versionCode = paramInt;
    this.HK = paramArrayList;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzj.zza(this, paramParcel, paramInt);
  }
  
  public static class Operation
    extends AbstractSafeParcelable
  {
    public static final Parcelable.Creator<Operation> CREATOR = new zzk();
    public final InterestRecordStub IA;
    public final String IB;
    public final int type;
    public final int versionCode;
    
    Operation(int paramInt1, int paramInt2, InterestRecordStub paramInterestRecordStub, String paramString)
    {
      this.versionCode = paramInt1;
      this.type = paramInt2;
      this.IA = paramInterestRecordStub;
      this.IB = paramString;
    }
    
    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
      zzk.zza(this, paramParcel, paramInt);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/contextmanager/internal/InterestUpdateBatchImpl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */