package com.google.android.gms.contextmanager.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import java.util.ArrayList;
import java.util.Arrays;

public class KeyFilterImpl
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<KeyFilterImpl> CREATOR = new zzl();
  public final ArrayList<Inclusion> HT;
  public final int versionCode;
  
  KeyFilterImpl(int paramInt, ArrayList<Inclusion> paramArrayList)
  {
    this.versionCode = paramInt;
    this.HT = paramArrayList;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if (!(paramObject instanceof KeyFilterImpl)) {
      return false;
    }
    paramObject = (KeyFilterImpl)paramObject;
    return this.HT.equals(((KeyFilterImpl)paramObject).HT);
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.HT });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzl.zza(this, paramParcel, paramInt);
  }
  
  public static class Inclusion
    extends AbstractSafeParcelable
  {
    public static final Parcelable.Creator<Inclusion> CREATOR = new zzm();
    public final String[] IC;
    public final String[] IE;
    public final String[] IF;
    public final int versionCode;
    
    Inclusion(int paramInt, String[] paramArrayOfString1, String[] paramArrayOfString2, String[] paramArrayOfString3)
    {
      this.versionCode = paramInt;
      this.IC = paramArrayOfString1;
      this.IE = paramArrayOfString2;
      this.IF = paramArrayOfString3;
    }
    
    public boolean equals(Object paramObject)
    {
      if (this == paramObject) {}
      do
      {
        return true;
        if (!(paramObject instanceof Inclusion)) {
          return false;
        }
        paramObject = (Inclusion)paramObject;
      } while ((Arrays.equals(this.IC, ((Inclusion)paramObject).IC)) && (Arrays.equals(this.IE, ((Inclusion)paramObject).IE)) && (Arrays.equals(this.IF, ((Inclusion)paramObject).IF)));
      return false;
    }
    
    public int hashCode()
    {
      return Arrays.hashCode(this.IC) + Arrays.hashCode(this.IE) + Arrays.hashCode(this.IF);
    }
    
    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
      zzm.zza(this, paramParcel, paramInt);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/contextmanager/internal/KeyFilterImpl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */