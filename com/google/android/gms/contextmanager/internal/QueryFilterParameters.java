package com.google.android.gms.contextmanager.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;

public class QueryFilterParameters
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<QueryFilterParameters> CREATOR = new zzn();
  public final int IG;
  public final int[] IH;
  public final int limit;
  public final int versionCode;
  
  QueryFilterParameters(int paramInt1, int paramInt2, int paramInt3, int[] paramArrayOfInt)
  {
    this.versionCode = paramInt1;
    this.IG = paramInt2;
    this.limit = paramInt3;
    this.IH = paramArrayOfInt;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    do
    {
      return true;
      if (!(paramObject instanceof QueryFilterParameters)) {
        return false;
      }
      paramObject = (QueryFilterParameters)paramObject;
      if (((QueryFilterParameters)paramObject).limit != this.limit) {
        return false;
      }
      if (((QueryFilterParameters)paramObject).IG != this.IG) {
        return false;
      }
      if (((QueryFilterParameters)paramObject).versionCode != this.versionCode) {
        return false;
      }
      if (zzbam() != ((QueryFilterParameters)paramObject).zzbam()) {
        return false;
      }
    } while (!zzbam());
    if (this.IH.length != ((QueryFilterParameters)paramObject).IH.length) {
      return false;
    }
    paramObject = ((QueryFilterParameters)paramObject).IH;
    int k = paramObject.length;
    int i = 0;
    label106:
    if (i < k)
    {
      int m = paramObject[i];
      int[] arrayOfInt = this.IH;
      int n = arrayOfInt.length;
      j = 0;
      label130:
      if (j >= n) {
        break label167;
      }
      if (arrayOfInt[j] != m) {}
    }
    label167:
    for (int j = 1;; j = 0)
    {
      if (j == 0)
      {
        return false;
        j += 1;
        break label130;
      }
      i += 1;
      break label106;
      break;
    }
  }
  
  public int hashCode()
  {
    if (this.IH != null)
    {
      int[] arrayOfInt = this.IH;
      int m = arrayOfInt.length;
      int j = 0;
      for (int i = 0;; i = k * 13 + i)
      {
        k = i;
        if (j >= m) {
          break;
        }
        k = arrayOfInt[j];
        j += 1;
      }
    }
    int k = 0;
    return zzz.hashCode(new Object[] { Integer.valueOf(k), Integer.valueOf(this.IG), Integer.valueOf(this.limit), Integer.valueOf(this.versionCode) });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzn.zza(this, paramParcel, paramInt);
  }
  
  public boolean zzbam()
  {
    return this.IH != null;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/contextmanager/internal/QueryFilterParameters.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */