package com.google.android.gms.contextmanager.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import java.util.ArrayList;

public class TimeFilterImpl
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<TimeFilterImpl> CREATOR = new zzp();
  public final ArrayList<Interval> II;
  public final int[] IJ;
  public final int versionCode;
  
  TimeFilterImpl(int paramInt, ArrayList<Interval> paramArrayList, int[] paramArrayOfInt)
  {
    this.versionCode = paramInt;
    this.II = paramArrayList;
    this.IJ = paramArrayOfInt;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    do
    {
      return true;
      if (!(paramObject instanceof TimeFilterImpl)) {
        return false;
      }
      paramObject = (TimeFilterImpl)paramObject;
    } while ((zzz.equal(this.II, ((TimeFilterImpl)paramObject).II)) && (zzz.equal(this.IJ, ((TimeFilterImpl)paramObject).IJ)));
    return false;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.II, this.IJ });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzp.zza(this, paramParcel, paramInt);
  }
  
  public static class Interval
    extends AbstractSafeParcelable
  {
    public static final Parcelable.Creator<Interval> CREATOR = new zzq();
    public final long IK;
    public final long IL;
    public final int versionCode;
    
    Interval(int paramInt, long paramLong1, long paramLong2)
    {
      this.versionCode = paramInt;
      this.IK = paramLong1;
      this.IL = paramLong2;
    }
    
    public boolean equals(Object paramObject)
    {
      if (this == paramObject) {}
      do
      {
        return true;
        if (!(paramObject instanceof Interval)) {
          return false;
        }
        paramObject = (Interval)paramObject;
      } while ((this.IK == ((Interval)paramObject).zzagx()) && (this.IL == ((Interval)paramObject).zzban()));
      return false;
    }
    
    public int hashCode()
    {
      return zzz.hashCode(new Object[] { Long.valueOf(this.IK), Long.valueOf(this.IL) });
    }
    
    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
      zzq.zza(this, paramParcel, paramInt);
    }
    
    public long zzagx()
    {
      return this.IK;
    }
    
    public long zzban()
    {
      return this.IL;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/contextmanager/internal/TimeFilterImpl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */