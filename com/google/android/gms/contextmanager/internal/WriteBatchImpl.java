package com.google.android.gms.contextmanager.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.contextmanager.ContextData;
import java.util.ArrayList;

public class WriteBatchImpl
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<WriteBatchImpl> CREATOR = new zzr();
  public final boolean IM;
  public ArrayList<ContextData> IN;
  public final int versionCode;
  
  public WriteBatchImpl()
  {
    this(false);
  }
  
  WriteBatchImpl(int paramInt, boolean paramBoolean, ArrayList<ContextData> paramArrayList)
  {
    this.versionCode = paramInt;
    this.IM = paramBoolean;
    this.IN = paramArrayList;
  }
  
  public WriteBatchImpl(boolean paramBoolean)
  {
    this.versionCode = 1;
    this.IM = paramBoolean;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzr.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/contextmanager/internal/WriteBatchImpl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */