package com.google.android.gms.contextmanager.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.ArrayList;

public class zza
  implements Parcelable.Creator<ContextDataFilterImpl>
{
  static void zza(ContextDataFilterImpl paramContextDataFilterImpl, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramContextDataFilterImpl.versionCode);
    zzb.zzc(paramParcel, 2, paramContextDataFilterImpl.HT, false);
    zzb.zzb(paramParcel, 3, paramContextDataFilterImpl.HU, false);
    zzb.zza(paramParcel, 4, paramContextDataFilterImpl.HV, paramInt, false);
    zzb.zzaj(paramParcel, i);
  }
  
  public ContextDataFilterImpl zzds(Parcel paramParcel)
  {
    QueryFilterParameters localQueryFilterParameters = null;
    int j = com.google.android.gms.common.internal.safeparcel.zza.zzcr(paramParcel);
    int i = 0;
    ArrayList localArrayList2 = null;
    ArrayList localArrayList1 = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = com.google.android.gms.common.internal.safeparcel.zza.zzcq(paramParcel);
      switch (com.google.android.gms.common.internal.safeparcel.zza.zzgu(k))
      {
      default: 
        com.google.android.gms.common.internal.safeparcel.zza.zzb(paramParcel, k);
        break;
      case 1: 
        i = com.google.android.gms.common.internal.safeparcel.zza.zzg(paramParcel, k);
        break;
      case 2: 
        localArrayList1 = com.google.android.gms.common.internal.safeparcel.zza.zzc(paramParcel, k, ContextDataFilterImpl.Inclusion.CREATOR);
        break;
      case 3: 
        localArrayList2 = com.google.android.gms.common.internal.safeparcel.zza.zzae(paramParcel, k);
        break;
      case 4: 
        localQueryFilterParameters = (QueryFilterParameters)com.google.android.gms.common.internal.safeparcel.zza.zza(paramParcel, k, QueryFilterParameters.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new ContextDataFilterImpl(i, localArrayList1, localArrayList2, localQueryFilterParameters);
  }
  
  public ContextDataFilterImpl[] zzie(int paramInt)
  {
    return new ContextDataFilterImpl[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/contextmanager/internal/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */