package com.google.android.gms.contextmanager.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;

public class zzb
  implements Parcelable.Creator<ContextDataFilterImpl.Inclusion>
{
  static void zza(ContextDataFilterImpl.Inclusion paramInclusion, Parcel paramParcel, int paramInt)
  {
    int i = com.google.android.gms.common.internal.safeparcel.zzb.zzcs(paramParcel);
    com.google.android.gms.common.internal.safeparcel.zzb.zzc(paramParcel, 1, paramInclusion.versionCode);
    com.google.android.gms.common.internal.safeparcel.zzb.zzc(paramParcel, 2, paramInclusion.HW);
    com.google.android.gms.common.internal.safeparcel.zzb.zzc(paramParcel, 3, paramInclusion.name);
    com.google.android.gms.common.internal.safeparcel.zzb.zza(paramParcel, 4, paramInclusion.HX, paramInt, false);
    com.google.android.gms.common.internal.safeparcel.zzb.zza(paramParcel, 5, paramInclusion.HY, paramInt, false);
    com.google.android.gms.common.internal.safeparcel.zzb.zzaj(paramParcel, i);
  }
  
  public ContextDataFilterImpl.Inclusion zzdt(Parcel paramParcel)
  {
    KeyFilterImpl localKeyFilterImpl = null;
    int i = 0;
    int m = zza.zzcr(paramParcel);
    TimeFilterImpl localTimeFilterImpl = null;
    int j = 0;
    int k = 0;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.zzcq(paramParcel);
      switch (zza.zzgu(n))
      {
      default: 
        zza.zzb(paramParcel, n);
        break;
      case 1: 
        k = zza.zzg(paramParcel, n);
        break;
      case 2: 
        j = zza.zzg(paramParcel, n);
        break;
      case 3: 
        i = zza.zzg(paramParcel, n);
        break;
      case 4: 
        localTimeFilterImpl = (TimeFilterImpl)zza.zza(paramParcel, n, TimeFilterImpl.CREATOR);
        break;
      case 5: 
        localKeyFilterImpl = (KeyFilterImpl)zza.zza(paramParcel, n, KeyFilterImpl.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza(37 + "Overread allowed size end=" + m, paramParcel);
    }
    return new ContextDataFilterImpl.Inclusion(k, j, i, localTimeFilterImpl, localKeyFilterImpl);
  }
  
  public ContextDataFilterImpl.Inclusion[] zzif(int paramInt)
  {
    return new ContextDataFilterImpl.Inclusion[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/contextmanager/internal/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */