package com.google.android.gms.contextmanager.internal;

import com.google.android.gms.awareness.fence.FenceQueryResult;
import com.google.android.gms.awareness.fence.FenceStateMap;
import com.google.android.gms.awareness.snapshot.internal.Snapshot;
import com.google.android.gms.awareness.snapshot.internal.zzm;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.internal.zzqo.zza;

public class zzc
{
  public static abstract class zza
    extends zzqo.zza<FenceQueryResult, zzd>
  {
    public zza(GoogleApiClient paramGoogleApiClient)
    {
      super(paramGoogleApiClient);
    }
    
    public FenceQueryResult zzai(final Status paramStatus)
    {
      new FenceQueryResult()
      {
        public FenceStateMap getFenceStateMap()
        {
          return null;
        }
        
        public Status getStatus()
        {
          return paramStatus;
        }
      };
    }
  }
  
  public static abstract class zzb
    extends zzqo.zza<zzm, zzd>
  {
    public zzb(GoogleApiClient paramGoogleApiClient)
    {
      super(paramGoogleApiClient);
    }
    
    public zzm zzaj(final Status paramStatus)
    {
      new zzm()
      {
        public Status getStatus()
        {
          return paramStatus;
        }
        
        public Snapshot zzakn()
        {
          return null;
        }
      };
    }
  }
  
  public static abstract class zzc
    extends zzqo.zza<Status, zzd>
  {
    public zzc(GoogleApiClient paramGoogleApiClient)
    {
      super(paramGoogleApiClient);
    }
    
    public Status zzb(Status paramStatus)
    {
      return paramStatus;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/contextmanager/internal/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */