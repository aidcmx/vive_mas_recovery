package com.google.android.gms.contextmanager.internal;

import android.accounts.Account;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import com.google.android.gms.awareness.AwarenessOptions;
import com.google.android.gms.awareness.fence.FenceQueryResult;
import com.google.android.gms.awareness.fence.zza;
import com.google.android.gms.awareness.snapshot.internal.SnapshotRequest;
import com.google.android.gms.awareness.snapshot.internal.zzm;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzj;
import com.google.android.gms.contextmanager.fence.internal.FenceQueryRequestImpl;
import com.google.android.gms.contextmanager.fence.internal.FenceUpdateRequestImpl;
import com.google.android.gms.contextmanager.fence.internal.zze;
import com.google.android.gms.internal.zzcb.zza;
import com.google.android.gms.internal.zzce;
import com.google.android.gms.internal.zzqo.zzb;

public class zzd
  extends zzj<zzi>
{
  private static zzcb.zza Ib = zzcb.zza.zzajt;
  private final ContextManagerClientInfo Ic;
  private zzce<zza, zze> Id;
  private final Looper zzajy;
  
  public zzd(Context paramContext, Looper paramLooper, com.google.android.gms.common.internal.zzf paramzzf, AwarenessOptions paramAwarenessOptions, GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
  {
    super(paramContext, paramLooper, 47, paramzzf, paramConnectionCallbacks, paramOnConnectionFailedListener);
    this.zzajy = paramLooper;
    if (paramzzf.getAccount() == null)
    {
      paramLooper = "@@ContextManagerNullAccount@@";
      if (paramAwarenessOptions != null) {
        break label56;
      }
    }
    label56:
    for (paramContext = ContextManagerClientInfo.zzz(paramContext, paramLooper);; paramContext = ContextManagerClientInfo.zza(paramContext, paramLooper, paramAwarenessOptions))
    {
      this.Ic = paramContext;
      return;
      paramLooper = paramzzf.getAccount().name;
      break;
    }
  }
  
  public static Handler zza(Looper paramLooper)
  {
    if (Ib == null) {
      return zzcb.zza.zzajt.zza(paramLooper);
    }
    return Ib.zza(paramLooper);
  }
  
  private zzce<zza, zze> zzbaj()
  {
    if (this.Id == null) {
      this.Id = new zzce(this.zzajy, zze.Hv);
    }
    return this.Id;
  }
  
  public void zza(zzqo.zzb<zzm> paramzzb, SnapshotRequest paramSnapshotRequest)
    throws RemoteException
  {
    zzavf();
    ((zzi)zzavg()).zza(zzf.zzd(paramzzb), this.Ic.packageName, this.Ic.Ie, this.Ic.moduleId, paramSnapshotRequest);
  }
  
  public void zza(zzqo.zzb<FenceQueryResult> paramzzb, FenceQueryRequestImpl paramFenceQueryRequestImpl)
    throws RemoteException
  {
    zzavf();
    ((zzi)zzavg()).zza(zzf.zze(paramzzb), this.Ic.packageName, this.Ic.Ie, this.Ic.moduleId, paramFenceQueryRequestImpl);
  }
  
  public void zza(zzqo.zzb<Status> paramzzb, FenceUpdateRequestImpl paramFenceUpdateRequestImpl)
    throws RemoteException
  {
    zzavf();
    paramFenceUpdateRequestImpl.zza(zzbaj());
    ((zzi)zzavg()).zza(zzf.zza(paramzzb, null), this.Ic.packageName, this.Ic.Ie, this.Ic.moduleId, paramFenceUpdateRequestImpl);
  }
  
  protected Bundle zzahv()
  {
    Bundle localBundle = new Bundle();
    localBundle.putByteArray("com.google.android.contextmanager.service.args", this.Ic.toByteArray());
    return localBundle;
  }
  
  public boolean zzavh()
  {
    return false;
  }
  
  protected zzi zzeg(IBinder paramIBinder)
  {
    return zzi.zza.zzej(paramIBinder);
  }
  
  protected String zzjx()
  {
    return "com.google.android.contextmanager.service.ContextManagerService.START";
  }
  
  protected String zzjy()
  {
    return "com.google.android.gms.contextmanager.internal.IContextManagerService";
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/contextmanager/internal/zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */