package com.google.android.gms.contextmanager.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zze
  implements Parcelable.Creator<ContextManagerClientInfo>
{
  static void zza(ContextManagerClientInfo paramContextManagerClientInfo, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramContextManagerClientInfo.versionCode);
    zzb.zza(paramParcel, 2, paramContextManagerClientInfo.Ie, false);
    zzb.zza(paramParcel, 3, paramContextManagerClientInfo.packageName, false);
    zzb.zzc(paramParcel, 4, paramContextManagerClientInfo.uid);
    zzb.zza(paramParcel, 5, paramContextManagerClientInfo.moduleId, false);
    zzb.zzc(paramParcel, 6, paramContextManagerClientInfo.If);
    zzb.zzc(paramParcel, 7, paramContextManagerClientInfo.Ig);
    zzb.zza(paramParcel, 8, paramContextManagerClientInfo.Ih, false);
    zzb.zza(paramParcel, 9, paramContextManagerClientInfo.Ii, false);
    zzb.zzc(paramParcel, 10, paramContextManagerClientInfo.Ij);
    zzb.zzaj(paramParcel, paramInt);
  }
  
  public ContextManagerClientInfo zzdu(Parcel paramParcel)
  {
    String str1 = null;
    int i = 0;
    int i1 = zza.zzcr(paramParcel);
    String str2 = null;
    int j = 0;
    int k = 0;
    String str3 = null;
    int m = 0;
    String str4 = null;
    String str5 = null;
    int n = 0;
    while (paramParcel.dataPosition() < i1)
    {
      int i2 = zza.zzcq(paramParcel);
      switch (zza.zzgu(i2))
      {
      default: 
        zza.zzb(paramParcel, i2);
        break;
      case 1: 
        n = zza.zzg(paramParcel, i2);
        break;
      case 2: 
        str5 = zza.zzq(paramParcel, i2);
        break;
      case 3: 
        str4 = zza.zzq(paramParcel, i2);
        break;
      case 4: 
        m = zza.zzg(paramParcel, i2);
        break;
      case 5: 
        str3 = zza.zzq(paramParcel, i2);
        break;
      case 6: 
        k = zza.zzg(paramParcel, i2);
        break;
      case 7: 
        j = zza.zzg(paramParcel, i2);
        break;
      case 8: 
        str2 = zza.zzq(paramParcel, i2);
        break;
      case 9: 
        str1 = zza.zzq(paramParcel, i2);
        break;
      case 10: 
        i = zza.zzg(paramParcel, i2);
      }
    }
    if (paramParcel.dataPosition() != i1) {
      throw new zza.zza(37 + "Overread allowed size end=" + i1, paramParcel);
    }
    return new ContextManagerClientInfo(n, str5, str4, m, str3, k, j, str2, str1, i);
  }
  
  public ContextManagerClientInfo[] zzig(int paramInt)
  {
    return new ContextManagerClientInfo[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/contextmanager/internal/zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */