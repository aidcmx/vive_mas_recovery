package com.google.android.gms.contextmanager.internal;

import android.os.RemoteException;
import com.google.android.gms.awareness.fence.FenceQueryResult;
import com.google.android.gms.awareness.fence.FenceStateMap;
import com.google.android.gms.awareness.fence.zzb;
import com.google.android.gms.awareness.snapshot.internal.Snapshot;
import com.google.android.gms.awareness.snapshot.internal.zzm;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.contextmanager.fence.internal.FenceStateImpl;
import com.google.android.gms.contextmanager.fence.internal.FenceStateMapImpl;
import com.google.android.gms.contextmanager.zza;
import com.google.android.gms.contextmanager.zzd;
import com.google.android.gms.contextmanager.zze;
import com.google.android.gms.internal.zzcd;
import com.google.android.gms.internal.zzqo.zzb;

public class zzf
  extends zzh.zza
{
  private final zza Il;
  private zzqo.zzb<Status> Im;
  private zzqo.zzb<zze> In;
  private zzqo.zzb<zzs> Io;
  private zzqo.zzb<zzd> Ip;
  private zzqo.zzb<zzm> Iq;
  private zzqo.zzb<FenceQueryResult> Ir;
  private zzqo.zzb<zzb> Is;
  
  private zzf(zzqo.zzb<Status> paramzzb, zzqo.zzb<zze> paramzzb1, zzqo.zzb<zzs> paramzzb2, zzqo.zzb<zzd> paramzzb3, zzqo.zzb<zzm> paramzzb4, zzqo.zzb<FenceQueryResult> paramzzb5, zzqo.zzb<zzb> paramzzb6, zza paramzza)
  {
    this.Im = paramzzb;
    this.In = paramzzb1;
    this.Io = paramzzb2;
    this.Ip = paramzzb3;
    this.Iq = paramzzb4;
    this.Ir = paramzzb5;
    this.Is = paramzzb6;
    this.Il = paramzza;
  }
  
  public static zzf zza(zzqo.zzb<Status> paramzzb, zza paramzza)
  {
    return new zzf(paramzzb, null, null, null, null, null, null, paramzza);
  }
  
  private void zzak(Status paramStatus)
  {
    if (this.Il != null) {
      this.Il.zzal(paramStatus);
    }
  }
  
  public static zzf zzd(zzqo.zzb<zzm> paramzzb)
  {
    return new zzf(null, null, null, null, paramzzb, null, null, null);
  }
  
  public static zzf zze(zzqo.zzb<FenceQueryResult> paramzzb)
  {
    return new zzf(null, null, null, null, null, paramzzb, null, null);
  }
  
  public void zza(final Status paramStatus, final Snapshot paramSnapshot)
    throws RemoteException
  {
    if (this.Iq == null)
    {
      zzcd.zzd("ContextManagerPendingResult", "Unexpected callback to onSnapshotResult");
      return;
    }
    this.Iq.setResult(new zzm()
    {
      public Status getStatus()
      {
        return paramStatus;
      }
      
      public Snapshot zzakn()
      {
        return paramSnapshot;
      }
    });
    this.Iq = null;
    zzak(paramStatus);
  }
  
  public void zza(final Status paramStatus, final DataHolder paramDataHolder)
    throws RemoteException
  {
    if (this.Ip == null)
    {
      zzcd.zzd("ContextManagerPendingResult", "Unexpected callback to onStateResult");
      return;
    }
    this.Ip.setResult(new zzd()
    {
      private final zza It;
      
      public Status getStatus()
      {
        return paramStatus;
      }
    });
    this.Ip = null;
    zzak(paramStatus);
  }
  
  public void zza(final Status paramStatus, final DataHolder paramDataHolder1, DataHolder paramDataHolder2)
    throws RemoteException
  {
    if (this.In == null)
    {
      zzcd.zzd("ContextManagerPendingResult", "Unexpected callback to onReadResult.");
      return;
    }
    this.In.setResult(new zze()
    {
      private final zza It;
      
      public Status getStatus()
      {
        return paramStatus;
      }
    });
    this.In = null;
    zzak(paramStatus);
  }
  
  public void zza(final Status paramStatus, final FenceStateImpl paramFenceStateImpl)
  {
    if (this.Is == null)
    {
      zzcd.zzd("ContextManagerPendingResult", "Unexpected callback to onFenceEvaluateResult");
      return;
    }
    this.Is.setResult(new zzb()
    {
      public Status getStatus()
      {
        return paramStatus;
      }
    });
    this.Is = null;
    zzak(paramStatus);
  }
  
  public void zza(final Status paramStatus, final FenceStateMapImpl paramFenceStateMapImpl)
  {
    if (this.Ir == null)
    {
      zzcd.zzd("ContextManagerPendingResult", "Unexpected callback to onFenceQueryResult");
      return;
    }
    this.Ir.setResult(new FenceQueryResult()
    {
      public FenceStateMap getFenceStateMap()
      {
        return paramFenceStateMapImpl;
      }
      
      public Status getStatus()
      {
        return paramStatus;
      }
    });
    this.Ir = null;
    zzak(paramStatus);
  }
  
  public void zza(final Status paramStatus, final WriteBatchImpl paramWriteBatchImpl)
    throws RemoteException
  {
    if (this.Io == null)
    {
      zzcd.zzd("ContextManagerPendingResult", "Unexpected callback to onWriteBatchResult");
      return;
    }
    this.Io.setResult(new zzs()
    {
      public Status getStatus()
      {
        return paramStatus;
      }
    });
    this.Io = null;
    zzak(paramStatus);
  }
  
  public void zzh(Status paramStatus)
    throws RemoteException
  {
    if (this.Im == null)
    {
      zzcd.zzd("ContextManagerPendingResult", "Unexpected callback to onStatusResult.");
      return;
    }
    this.Im.setResult(paramStatus);
    this.Im = null;
    zzak(paramStatus);
  }
  
  public static abstract interface zza
  {
    public abstract void zzal(Status paramStatus);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/contextmanager/internal/zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */