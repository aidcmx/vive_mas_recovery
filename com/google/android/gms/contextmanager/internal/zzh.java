package com.google.android.gms.contextmanager.internal;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import com.google.android.gms.awareness.snapshot.internal.Snapshot;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.contextmanager.fence.internal.FenceStateImpl;
import com.google.android.gms.contextmanager.fence.internal.FenceStateMapImpl;

public abstract interface zzh
  extends IInterface
{
  public abstract void zza(Status paramStatus, Snapshot paramSnapshot)
    throws RemoteException;
  
  public abstract void zza(Status paramStatus, DataHolder paramDataHolder)
    throws RemoteException;
  
  public abstract void zza(Status paramStatus, DataHolder paramDataHolder1, DataHolder paramDataHolder2)
    throws RemoteException;
  
  public abstract void zza(Status paramStatus, FenceStateImpl paramFenceStateImpl)
    throws RemoteException;
  
  public abstract void zza(Status paramStatus, FenceStateMapImpl paramFenceStateMapImpl)
    throws RemoteException;
  
  public abstract void zza(Status paramStatus, WriteBatchImpl paramWriteBatchImpl)
    throws RemoteException;
  
  public abstract void zzh(Status paramStatus)
    throws RemoteException;
  
  public static abstract class zza
    extends Binder
    implements zzh
  {
    public zza()
    {
      attachInterface(this, "com.google.android.gms.contextmanager.internal.IContextManagerPendingResult");
    }
    
    public static zzh zzei(IBinder paramIBinder)
    {
      if (paramIBinder == null) {
        return null;
      }
      IInterface localIInterface = paramIBinder.queryLocalInterface("com.google.android.gms.contextmanager.internal.IContextManagerPendingResult");
      if ((localIInterface != null) && ((localIInterface instanceof zzh))) {
        return (zzh)localIInterface;
      }
      return new zza(paramIBinder);
    }
    
    public IBinder asBinder()
    {
      return this;
    }
    
    public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
      throws RemoteException
    {
      Status localStatus;
      switch (paramInt1)
      {
      default: 
        return super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
      case 1598968902: 
        paramParcel2.writeString("com.google.android.gms.contextmanager.internal.IContextManagerPendingResult");
        return true;
      case 1: 
        paramParcel1.enforceInterface("com.google.android.gms.contextmanager.internal.IContextManagerPendingResult");
        if (paramParcel1.readInt() != 0) {}
        for (paramParcel1 = (Status)Status.CREATOR.createFromParcel(paramParcel1);; paramParcel1 = null)
        {
          zzh(paramParcel1);
          paramParcel2.writeNoException();
          return true;
        }
      case 2: 
        paramParcel1.enforceInterface("com.google.android.gms.contextmanager.internal.IContextManagerPendingResult");
        DataHolder localDataHolder;
        if (paramParcel1.readInt() != 0)
        {
          localStatus = (Status)Status.CREATOR.createFromParcel(paramParcel1);
          if (paramParcel1.readInt() == 0) {
            break label225;
          }
          localDataHolder = (DataHolder)DataHolder.CREATOR.createFromParcel(paramParcel1);
          if (paramParcel1.readInt() == 0) {
            break label231;
          }
        }
        for (paramParcel1 = (DataHolder)DataHolder.CREATOR.createFromParcel(paramParcel1);; paramParcel1 = null)
        {
          zza(localStatus, localDataHolder, paramParcel1);
          paramParcel2.writeNoException();
          return true;
          localStatus = null;
          break;
          localDataHolder = null;
          break label184;
        }
      case 3: 
        paramParcel1.enforceInterface("com.google.android.gms.contextmanager.internal.IContextManagerPendingResult");
        if (paramParcel1.readInt() != 0)
        {
          localStatus = (Status)Status.CREATOR.createFromParcel(paramParcel1);
          if (paramParcel1.readInt() == 0) {
            break label302;
          }
        }
        for (paramParcel1 = (WriteBatchImpl)WriteBatchImpl.CREATOR.createFromParcel(paramParcel1);; paramParcel1 = null)
        {
          zza(localStatus, paramParcel1);
          paramParcel2.writeNoException();
          return true;
          localStatus = null;
          break;
        }
      case 5: 
        paramParcel1.enforceInterface("com.google.android.gms.contextmanager.internal.IContextManagerPendingResult");
        if (paramParcel1.readInt() != 0)
        {
          localStatus = (Status)Status.CREATOR.createFromParcel(paramParcel1);
          if (paramParcel1.readInt() == 0) {
            break label373;
          }
        }
        for (paramParcel1 = (DataHolder)DataHolder.CREATOR.createFromParcel(paramParcel1);; paramParcel1 = null)
        {
          zza(localStatus, paramParcel1);
          paramParcel2.writeNoException();
          return true;
          localStatus = null;
          break;
        }
      case 6: 
        paramParcel1.enforceInterface("com.google.android.gms.contextmanager.internal.IContextManagerPendingResult");
        if (paramParcel1.readInt() != 0)
        {
          localStatus = (Status)Status.CREATOR.createFromParcel(paramParcel1);
          if (paramParcel1.readInt() == 0) {
            break label444;
          }
        }
        for (paramParcel1 = (Snapshot)Snapshot.CREATOR.createFromParcel(paramParcel1);; paramParcel1 = null)
        {
          zza(localStatus, paramParcel1);
          paramParcel2.writeNoException();
          return true;
          localStatus = null;
          break;
        }
      case 7: 
        label184:
        label225:
        label231:
        label302:
        label373:
        label444:
        paramParcel1.enforceInterface("com.google.android.gms.contextmanager.internal.IContextManagerPendingResult");
        if (paramParcel1.readInt() != 0)
        {
          localStatus = (Status)Status.CREATOR.createFromParcel(paramParcel1);
          if (paramParcel1.readInt() == 0) {
            break label515;
          }
        }
        label515:
        for (paramParcel1 = (FenceStateMapImpl)FenceStateMapImpl.CREATOR.createFromParcel(paramParcel1);; paramParcel1 = null)
        {
          zza(localStatus, paramParcel1);
          paramParcel2.writeNoException();
          return true;
          localStatus = null;
          break;
        }
      }
      paramParcel1.enforceInterface("com.google.android.gms.contextmanager.internal.IContextManagerPendingResult");
      if (paramParcel1.readInt() != 0)
      {
        localStatus = (Status)Status.CREATOR.createFromParcel(paramParcel1);
        if (paramParcel1.readInt() == 0) {
          break label586;
        }
      }
      label586:
      for (paramParcel1 = (FenceStateImpl)FenceStateImpl.CREATOR.createFromParcel(paramParcel1);; paramParcel1 = null)
      {
        zza(localStatus, paramParcel1);
        paramParcel2.writeNoException();
        return true;
        localStatus = null;
        break;
      }
    }
    
    private static class zza
      implements zzh
    {
      private IBinder zzajq;
      
      zza(IBinder paramIBinder)
      {
        this.zzajq = paramIBinder;
      }
      
      public IBinder asBinder()
      {
        return this.zzajq;
      }
      
      public void zza(Status paramStatus, Snapshot paramSnapshot)
        throws RemoteException
      {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        for (;;)
        {
          try
          {
            localParcel1.writeInterfaceToken("com.google.android.gms.contextmanager.internal.IContextManagerPendingResult");
            if (paramStatus != null)
            {
              localParcel1.writeInt(1);
              paramStatus.writeToParcel(localParcel1, 0);
              if (paramSnapshot != null)
              {
                localParcel1.writeInt(1);
                paramSnapshot.writeToParcel(localParcel1, 0);
                this.zzajq.transact(6, localParcel1, localParcel2, 0);
                localParcel2.readException();
              }
            }
            else
            {
              localParcel1.writeInt(0);
              continue;
            }
            localParcel1.writeInt(0);
          }
          finally
          {
            localParcel2.recycle();
            localParcel1.recycle();
          }
        }
      }
      
      public void zza(Status paramStatus, DataHolder paramDataHolder)
        throws RemoteException
      {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        for (;;)
        {
          try
          {
            localParcel1.writeInterfaceToken("com.google.android.gms.contextmanager.internal.IContextManagerPendingResult");
            if (paramStatus != null)
            {
              localParcel1.writeInt(1);
              paramStatus.writeToParcel(localParcel1, 0);
              if (paramDataHolder != null)
              {
                localParcel1.writeInt(1);
                paramDataHolder.writeToParcel(localParcel1, 0);
                this.zzajq.transact(5, localParcel1, localParcel2, 0);
                localParcel2.readException();
              }
            }
            else
            {
              localParcel1.writeInt(0);
              continue;
            }
            localParcel1.writeInt(0);
          }
          finally
          {
            localParcel2.recycle();
            localParcel1.recycle();
          }
        }
      }
      
      public void zza(Status paramStatus, DataHolder paramDataHolder1, DataHolder paramDataHolder2)
        throws RemoteException
      {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        for (;;)
        {
          try
          {
            localParcel1.writeInterfaceToken("com.google.android.gms.contextmanager.internal.IContextManagerPendingResult");
            if (paramStatus != null)
            {
              localParcel1.writeInt(1);
              paramStatus.writeToParcel(localParcel1, 0);
              if (paramDataHolder1 != null)
              {
                localParcel1.writeInt(1);
                paramDataHolder1.writeToParcel(localParcel1, 0);
                if (paramDataHolder2 == null) {
                  break label131;
                }
                localParcel1.writeInt(1);
                paramDataHolder2.writeToParcel(localParcel1, 0);
                this.zzajq.transact(2, localParcel1, localParcel2, 0);
                localParcel2.readException();
              }
            }
            else
            {
              localParcel1.writeInt(0);
              continue;
            }
            localParcel1.writeInt(0);
          }
          finally
          {
            localParcel2.recycle();
            localParcel1.recycle();
          }
          continue;
          label131:
          localParcel1.writeInt(0);
        }
      }
      
      public void zza(Status paramStatus, FenceStateImpl paramFenceStateImpl)
        throws RemoteException
      {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        for (;;)
        {
          try
          {
            localParcel1.writeInterfaceToken("com.google.android.gms.contextmanager.internal.IContextManagerPendingResult");
            if (paramStatus != null)
            {
              localParcel1.writeInt(1);
              paramStatus.writeToParcel(localParcel1, 0);
              if (paramFenceStateImpl != null)
              {
                localParcel1.writeInt(1);
                paramFenceStateImpl.writeToParcel(localParcel1, 0);
                this.zzajq.transact(8, localParcel1, localParcel2, 0);
                localParcel2.readException();
              }
            }
            else
            {
              localParcel1.writeInt(0);
              continue;
            }
            localParcel1.writeInt(0);
          }
          finally
          {
            localParcel2.recycle();
            localParcel1.recycle();
          }
        }
      }
      
      public void zza(Status paramStatus, FenceStateMapImpl paramFenceStateMapImpl)
        throws RemoteException
      {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        for (;;)
        {
          try
          {
            localParcel1.writeInterfaceToken("com.google.android.gms.contextmanager.internal.IContextManagerPendingResult");
            if (paramStatus != null)
            {
              localParcel1.writeInt(1);
              paramStatus.writeToParcel(localParcel1, 0);
              if (paramFenceStateMapImpl != null)
              {
                localParcel1.writeInt(1);
                paramFenceStateMapImpl.writeToParcel(localParcel1, 0);
                this.zzajq.transact(7, localParcel1, localParcel2, 0);
                localParcel2.readException();
              }
            }
            else
            {
              localParcel1.writeInt(0);
              continue;
            }
            localParcel1.writeInt(0);
          }
          finally
          {
            localParcel2.recycle();
            localParcel1.recycle();
          }
        }
      }
      
      public void zza(Status paramStatus, WriteBatchImpl paramWriteBatchImpl)
        throws RemoteException
      {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        for (;;)
        {
          try
          {
            localParcel1.writeInterfaceToken("com.google.android.gms.contextmanager.internal.IContextManagerPendingResult");
            if (paramStatus != null)
            {
              localParcel1.writeInt(1);
              paramStatus.writeToParcel(localParcel1, 0);
              if (paramWriteBatchImpl != null)
              {
                localParcel1.writeInt(1);
                paramWriteBatchImpl.writeToParcel(localParcel1, 0);
                this.zzajq.transact(3, localParcel1, localParcel2, 0);
                localParcel2.readException();
              }
            }
            else
            {
              localParcel1.writeInt(0);
              continue;
            }
            localParcel1.writeInt(0);
          }
          finally
          {
            localParcel2.recycle();
            localParcel1.recycle();
          }
        }
      }
      
      /* Error */
      public void zzh(Status paramStatus)
        throws RemoteException
      {
        // Byte code:
        //   0: invokestatic 30	android/os/Parcel:obtain	()Landroid/os/Parcel;
        //   3: astore_2
        //   4: invokestatic 30	android/os/Parcel:obtain	()Landroid/os/Parcel;
        //   7: astore_3
        //   8: aload_2
        //   9: ldc 32
        //   11: invokevirtual 36	android/os/Parcel:writeInterfaceToken	(Ljava/lang/String;)V
        //   14: aload_1
        //   15: ifnull +41 -> 56
        //   18: aload_2
        //   19: iconst_1
        //   20: invokevirtual 40	android/os/Parcel:writeInt	(I)V
        //   23: aload_1
        //   24: aload_2
        //   25: iconst_0
        //   26: invokevirtual 46	com/google/android/gms/common/api/Status:writeToParcel	(Landroid/os/Parcel;I)V
        //   29: aload_0
        //   30: getfield 18	com/google/android/gms/contextmanager/internal/zzh$zza$zza:zzajq	Landroid/os/IBinder;
        //   33: iconst_1
        //   34: aload_2
        //   35: aload_3
        //   36: iconst_0
        //   37: invokeinterface 55 5 0
        //   42: pop
        //   43: aload_3
        //   44: invokevirtual 58	android/os/Parcel:readException	()V
        //   47: aload_3
        //   48: invokevirtual 61	android/os/Parcel:recycle	()V
        //   51: aload_2
        //   52: invokevirtual 61	android/os/Parcel:recycle	()V
        //   55: return
        //   56: aload_2
        //   57: iconst_0
        //   58: invokevirtual 40	android/os/Parcel:writeInt	(I)V
        //   61: goto -32 -> 29
        //   64: astore_1
        //   65: aload_3
        //   66: invokevirtual 61	android/os/Parcel:recycle	()V
        //   69: aload_2
        //   70: invokevirtual 61	android/os/Parcel:recycle	()V
        //   73: aload_1
        //   74: athrow
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	75	0	this	zza
        //   0	75	1	paramStatus	Status
        //   3	67	2	localParcel1	Parcel
        //   7	59	3	localParcel2	Parcel
        // Exception table:
        //   from	to	target	type
        //   8	14	64	finally
        //   18	29	64	finally
        //   29	47	64	finally
        //   56	61	64	finally
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/contextmanager/internal/zzh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */