package com.google.android.gms.contextmanager.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.contextmanager.interest.InterestRecordStub;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzk
  implements Parcelable.Creator<InterestUpdateBatchImpl.Operation>
{
  static void zza(InterestUpdateBatchImpl.Operation paramOperation, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramOperation.versionCode);
    zzb.zzc(paramParcel, 2, paramOperation.type);
    zzb.zza(paramParcel, 3, paramOperation.IA, paramInt, false);
    zzb.zza(paramParcel, 4, paramOperation.IB, false);
    zzb.zzaj(paramParcel, i);
  }
  
  public InterestUpdateBatchImpl.Operation zzdw(Parcel paramParcel)
  {
    String str = null;
    int j = 0;
    int m = zza.zzcr(paramParcel);
    InterestRecordStub localInterestRecordStub = null;
    int i = 0;
    if (paramParcel.dataPosition() < m)
    {
      int k = zza.zzcq(paramParcel);
      switch (zza.zzgu(k))
      {
      default: 
        zza.zzb(paramParcel, k);
        k = j;
        j = i;
        i = k;
      }
      for (;;)
      {
        k = j;
        j = i;
        i = k;
        break;
        k = zza.zzg(paramParcel, k);
        i = j;
        j = k;
        continue;
        k = zza.zzg(paramParcel, k);
        j = i;
        i = k;
        continue;
        localInterestRecordStub = (InterestRecordStub)zza.zza(paramParcel, k, InterestRecordStub.CREATOR);
        k = i;
        i = j;
        j = k;
        continue;
        str = zza.zzq(paramParcel, k);
        k = i;
        i = j;
        j = k;
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza(37 + "Overread allowed size end=" + m, paramParcel);
    }
    return new InterestUpdateBatchImpl.Operation(i, j, localInterestRecordStub, str);
  }
  
  public InterestUpdateBatchImpl.Operation[] zzii(int paramInt)
  {
    return new InterestUpdateBatchImpl.Operation[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/contextmanager/internal/zzk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */