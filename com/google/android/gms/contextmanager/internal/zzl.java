package com.google.android.gms.contextmanager.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.ArrayList;

public class zzl
  implements Parcelable.Creator<KeyFilterImpl>
{
  static void zza(KeyFilterImpl paramKeyFilterImpl, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramKeyFilterImpl.versionCode);
    zzb.zzc(paramParcel, 2, paramKeyFilterImpl.HT, false);
    zzb.zzaj(paramParcel, paramInt);
  }
  
  public KeyFilterImpl zzdx(Parcel paramParcel)
  {
    int j = zza.zzcr(paramParcel);
    int i = 0;
    ArrayList localArrayList = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.zzcq(paramParcel);
      switch (zza.zzgu(k))
      {
      default: 
        zza.zzb(paramParcel, k);
        break;
      case 1: 
        i = zza.zzg(paramParcel, k);
        break;
      case 2: 
        localArrayList = zza.zzc(paramParcel, k, KeyFilterImpl.Inclusion.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new KeyFilterImpl(i, localArrayList);
  }
  
  public KeyFilterImpl[] zzij(int paramInt)
  {
    return new KeyFilterImpl[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/contextmanager/internal/zzl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */