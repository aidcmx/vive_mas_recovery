package com.google.android.gms.contextmanager.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzm
  implements Parcelable.Creator<KeyFilterImpl.Inclusion>
{
  static void zza(KeyFilterImpl.Inclusion paramInclusion, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramInclusion.versionCode);
    zzb.zza(paramParcel, 2, paramInclusion.IC, false);
    zzb.zza(paramParcel, 3, paramInclusion.IE, false);
    zzb.zza(paramParcel, 4, paramInclusion.IF, false);
    zzb.zzaj(paramParcel, paramInt);
  }
  
  public KeyFilterImpl.Inclusion zzdy(Parcel paramParcel)
  {
    String[] arrayOfString3 = null;
    int j = zza.zzcr(paramParcel);
    int i = 0;
    String[] arrayOfString2 = null;
    String[] arrayOfString1 = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.zzcq(paramParcel);
      switch (zza.zzgu(k))
      {
      default: 
        zza.zzb(paramParcel, k);
        break;
      case 1: 
        i = zza.zzg(paramParcel, k);
        break;
      case 2: 
        arrayOfString1 = zza.zzac(paramParcel, k);
        break;
      case 3: 
        arrayOfString2 = zza.zzac(paramParcel, k);
        break;
      case 4: 
        arrayOfString3 = zza.zzac(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new KeyFilterImpl.Inclusion(i, arrayOfString1, arrayOfString2, arrayOfString3);
  }
  
  public KeyFilterImpl.Inclusion[] zzik(int paramInt)
  {
    return new KeyFilterImpl.Inclusion[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/contextmanager/internal/zzm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */