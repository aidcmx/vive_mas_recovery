package com.google.android.gms.contextmanager.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzn
  implements Parcelable.Creator<QueryFilterParameters>
{
  static void zza(QueryFilterParameters paramQueryFilterParameters, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramQueryFilterParameters.versionCode);
    zzb.zzc(paramParcel, 2, paramQueryFilterParameters.IG);
    zzb.zzc(paramParcel, 3, paramQueryFilterParameters.limit);
    zzb.zza(paramParcel, 4, paramQueryFilterParameters.IH, false);
    zzb.zzaj(paramParcel, paramInt);
  }
  
  public QueryFilterParameters zzdz(Parcel paramParcel)
  {
    int k = 0;
    int m = zza.zzcr(paramParcel);
    int[] arrayOfInt = null;
    int j = 0;
    int i = 0;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.zzcq(paramParcel);
      switch (zza.zzgu(n))
      {
      default: 
        zza.zzb(paramParcel, n);
        break;
      case 1: 
        i = zza.zzg(paramParcel, n);
        break;
      case 2: 
        j = zza.zzg(paramParcel, n);
        break;
      case 3: 
        k = zza.zzg(paramParcel, n);
        break;
      case 4: 
        arrayOfInt = zza.zzw(paramParcel, n);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza(37 + "Overread allowed size end=" + m, paramParcel);
    }
    return new QueryFilterParameters(i, j, k, arrayOfInt);
  }
  
  public QueryFilterParameters[] zzil(int paramInt)
  {
    return new QueryFilterParameters[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/contextmanager/internal/zzn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */