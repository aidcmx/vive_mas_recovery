package com.google.android.gms.contextmanager.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.ArrayList;

public class zzp
  implements Parcelable.Creator<TimeFilterImpl>
{
  static void zza(TimeFilterImpl paramTimeFilterImpl, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramTimeFilterImpl.versionCode);
    zzb.zzc(paramParcel, 2, paramTimeFilterImpl.II, false);
    zzb.zza(paramParcel, 3, paramTimeFilterImpl.IJ, false);
    zzb.zzaj(paramParcel, paramInt);
  }
  
  public TimeFilterImpl zzeb(Parcel paramParcel)
  {
    int[] arrayOfInt = null;
    int j = zza.zzcr(paramParcel);
    int i = 0;
    ArrayList localArrayList = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.zzcq(paramParcel);
      switch (zza.zzgu(k))
      {
      default: 
        zza.zzb(paramParcel, k);
        break;
      case 1: 
        i = zza.zzg(paramParcel, k);
        break;
      case 2: 
        localArrayList = zza.zzc(paramParcel, k, TimeFilterImpl.Interval.CREATOR);
        break;
      case 3: 
        arrayOfInt = zza.zzw(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new TimeFilterImpl(i, localArrayList, arrayOfInt);
  }
  
  public TimeFilterImpl[] zzin(int paramInt)
  {
    return new TimeFilterImpl[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/contextmanager/internal/zzp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */