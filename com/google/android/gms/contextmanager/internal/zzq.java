package com.google.android.gms.contextmanager.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzq
  implements Parcelable.Creator<TimeFilterImpl.Interval>
{
  static void zza(TimeFilterImpl.Interval paramInterval, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramInterval.versionCode);
    zzb.zza(paramParcel, 2, paramInterval.IK);
    zzb.zza(paramParcel, 3, paramInterval.IL);
    zzb.zzaj(paramParcel, paramInt);
  }
  
  public TimeFilterImpl.Interval zzec(Parcel paramParcel)
  {
    long l1 = 0L;
    int j = zza.zzcr(paramParcel);
    int i = 0;
    long l2 = 0L;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.zzcq(paramParcel);
      switch (zza.zzgu(k))
      {
      default: 
        zza.zzb(paramParcel, k);
        break;
      case 1: 
        i = zza.zzg(paramParcel, k);
        break;
      case 2: 
        l2 = zza.zzi(paramParcel, k);
        break;
      case 3: 
        l1 = zza.zzi(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new TimeFilterImpl.Interval(i, l2, l1);
  }
  
  public TimeFilterImpl.Interval[] zzio(int paramInt)
  {
    return new TimeFilterImpl.Interval[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/contextmanager/internal/zzq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */