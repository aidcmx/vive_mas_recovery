package com.google.android.gms.contextmanager;

import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.zzd;

public class zza
  extends zzd<ContextData>
{
  public zza(DataHolder paramDataHolder)
  {
    super(paramDataHolder, ContextData.CREATOR);
  }
  
  public ContextData zzhr(int paramInt)
  {
    return (ContextData)super.zzfz(paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/contextmanager/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */