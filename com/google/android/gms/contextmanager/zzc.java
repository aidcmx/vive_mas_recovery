package com.google.android.gms.contextmanager;

import android.content.Context;
import android.os.Looper;
import com.google.android.gms.awareness.AwarenessOptions;
import com.google.android.gms.awareness.FenceApi;
import com.google.android.gms.awareness.SnapshotApi;
import com.google.android.gms.awareness.snapshot.internal.zzj;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.zza;
import com.google.android.gms.common.api.Api.zzf;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.contextmanager.internal.zzd;

public final class zzc
{
  public static final Api<AwarenessOptions> API = new Api("ContextManager.API", hh, hg);
  public static final FenceApi FenceApi;
  public static final SnapshotApi SnapshotApi;
  public static final Api.zzf<zzd> hg = new Api.zzf();
  private static final Api.zza<zzd, AwarenessOptions> hh;
  
  static
  {
    FenceApi = new com.google.android.gms.contextmanager.fence.internal.zzc();
    SnapshotApi = new zzj();
    hh = new Api.zza()
    {
      public zzd zza(Context paramAnonymousContext, Looper paramAnonymousLooper, zzf paramAnonymouszzf, AwarenessOptions paramAnonymousAwarenessOptions, GoogleApiClient.ConnectionCallbacks paramAnonymousConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramAnonymousOnConnectionFailedListener)
      {
        return new zzd(paramAnonymousContext, paramAnonymousLooper, paramAnonymouszzf, paramAnonymousAwarenessOptions, paramAnonymousConnectionCallbacks, paramAnonymousOnConnectionFailedListener);
      }
    };
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/contextmanager/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */