package com.google.android.gms.drive;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.util.Base64;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.internal.zzasa;
import com.google.android.gms.internal.zztd;

public class ChangeSequenceNumber
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<ChangeSequenceNumber> CREATOR = new zza();
  final long IO;
  final long IP;
  final long IQ;
  private volatile String IR = null;
  final int mVersionCode;
  
  ChangeSequenceNumber(int paramInt, long paramLong1, long paramLong2, long paramLong3)
  {
    if (paramLong1 != -1L)
    {
      bool1 = true;
      zzaa.zzbt(bool1);
      if (paramLong2 == -1L) {
        break label92;
      }
      bool1 = true;
      label40:
      zzaa.zzbt(bool1);
      if (paramLong3 == -1L) {
        break label98;
      }
    }
    label92:
    label98:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      zzaa.zzbt(bool1);
      this.mVersionCode = paramInt;
      this.IO = paramLong1;
      this.IP = paramLong2;
      this.IQ = paramLong3;
      return;
      bool1 = false;
      break;
      bool1 = false;
      break label40;
    }
  }
  
  public final String encodeToString()
  {
    String str2;
    if (this.IR == null)
    {
      str2 = Base64.encodeToString(zzbao(), 10);
      str1 = String.valueOf("ChangeSequenceNumber:");
      str2 = String.valueOf(str2);
      if (str2.length() == 0) {
        break label51;
      }
    }
    label51:
    for (String str1 = str1.concat(str2);; str1 = new String(str1))
    {
      this.IR = str1;
      return this.IR;
    }
  }
  
  public boolean equals(Object paramObject)
  {
    if (!(paramObject instanceof ChangeSequenceNumber)) {}
    do
    {
      return false;
      paramObject = (ChangeSequenceNumber)paramObject;
    } while ((((ChangeSequenceNumber)paramObject).IP != this.IP) || (((ChangeSequenceNumber)paramObject).IQ != this.IQ) || (((ChangeSequenceNumber)paramObject).IO != this.IO));
    return true;
  }
  
  public int hashCode()
  {
    String str1 = String.valueOf(String.valueOf(this.IO));
    String str2 = String.valueOf(String.valueOf(this.IP));
    String str3 = String.valueOf(String.valueOf(this.IQ));
    return (String.valueOf(str1).length() + 0 + String.valueOf(str2).length() + String.valueOf(str3).length() + str1 + str2 + str3).hashCode();
  }
  
  public String toString()
  {
    return encodeToString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zza.zza(this, paramParcel, paramInt);
  }
  
  final byte[] zzbao()
  {
    zztd localzztd = new zztd();
    localzztd.versionCode = this.mVersionCode;
    localzztd.MV = this.IO;
    localzztd.MW = this.IP;
    localzztd.MX = this.IQ;
    return zzasa.zzf(localzztd);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/ChangeSequenceNumber.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */