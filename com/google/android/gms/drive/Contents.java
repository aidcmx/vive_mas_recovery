package com.google.android.gms.drive;

import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

public class Contents
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<Contents> CREATOR = new zzb();
  final ParcelFileDescriptor CA;
  final int IS;
  final int IT;
  final DriveId IU;
  final boolean IV;
  final int mVersionCode;
  final String zzavu;
  
  Contents(int paramInt1, ParcelFileDescriptor paramParcelFileDescriptor, int paramInt2, int paramInt3, DriveId paramDriveId, boolean paramBoolean, String paramString)
  {
    this.mVersionCode = paramInt1;
    this.CA = paramParcelFileDescriptor;
    this.IS = paramInt2;
    this.IT = paramInt3;
    this.IU = paramDriveId;
    this.IV = paramBoolean;
    this.zzavu = paramString;
  }
  
  public DriveId getDriveId()
  {
    return this.IU;
  }
  
  public InputStream getInputStream()
  {
    return new FileInputStream(this.CA.getFileDescriptor());
  }
  
  public int getMode()
  {
    return this.IT;
  }
  
  public OutputStream getOutputStream()
  {
    return new FileOutputStream(this.CA.getFileDescriptor());
  }
  
  public ParcelFileDescriptor getParcelFileDescriptor()
  {
    return this.CA;
  }
  
  public int getRequestId()
  {
    return this.IS;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzb.zza(this, paramParcel, paramInt);
  }
  
  public boolean zzbap()
  {
    return this.IV;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/Contents.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */