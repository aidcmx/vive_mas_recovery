package com.google.android.gms.drive;

import android.content.IntentSender;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.drive.internal.zzl;
import com.google.android.gms.drive.internal.zzv;

public class CreateFileActivityBuilder
{
  public static final String EXTRA_RESPONSE_DRIVE_ID = "response_drive_id";
  private final zzl IW = new zzl(0);
  private DriveContents IX;
  private boolean IY;
  
  public IntentSender build(GoogleApiClient paramGoogleApiClient)
  {
    zzaa.zzb(Boolean.valueOf(this.IY), "Must call setInitialDriveContents to CreateFileActivityBuilder.");
    zzaa.zza(paramGoogleApiClient.isConnected(), "Client must be connected");
    if (this.IX != null) {
      this.IX.zzbas();
    }
    return this.IW.build(paramGoogleApiClient);
  }
  
  public CreateFileActivityBuilder setActivityStartFolder(DriveId paramDriveId)
  {
    this.IW.zza(paramDriveId);
    return this;
  }
  
  public CreateFileActivityBuilder setActivityTitle(String paramString)
  {
    this.IW.zzis(paramString);
    return this;
  }
  
  public CreateFileActivityBuilder setInitialDriveContents(DriveContents paramDriveContents)
  {
    if (paramDriveContents != null)
    {
      if (!(paramDriveContents instanceof zzv)) {
        throw new IllegalArgumentException("Only DriveContents obtained from the Drive API are accepted.");
      }
      if (paramDriveContents.getDriveId() != null) {
        throw new IllegalArgumentException("Only DriveContents obtained through DriveApi.newDriveContents are accepted for file creation.");
      }
      if (paramDriveContents.zzbat()) {
        throw new IllegalArgumentException("DriveContents are already closed.");
      }
      this.IW.zzjz(paramDriveContents.zzbar().getRequestId());
      this.IX = paramDriveContents;
    }
    for (;;)
    {
      this.IY = true;
      return this;
      this.IW.zzjz(1);
    }
  }
  
  public CreateFileActivityBuilder setInitialMetadata(MetadataChangeSet paramMetadataChangeSet)
  {
    this.IW.zza(paramMetadataChangeSet);
    return this;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/CreateFileActivityBuilder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */