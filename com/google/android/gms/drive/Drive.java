package com.google.android.gms.drive;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.ApiOptions;
import com.google.android.gms.common.api.Api.ApiOptions.NoOptions;
import com.google.android.gms.common.api.Api.ApiOptions.Optional;
import com.google.android.gms.common.api.Api.zza;
import com.google.android.gms.common.api.Api.zzf;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.drive.internal.zzaa;
import com.google.android.gms.drive.internal.zzac;
import com.google.android.gms.drive.internal.zzs;
import com.google.android.gms.drive.internal.zzu;
import com.google.android.gms.drive.internal.zzx;

public final class Drive
{
  public static final Api<Api.ApiOptions.NoOptions> API;
  public static final DriveApi DriveApi;
  public static final DrivePreferencesApi DrivePreferencesApi = new zzaa();
  public static final Scope IZ;
  public static final Scope Ja;
  public static final Api<zzb> Jb;
  public static final zzd Jc;
  public static final zzf Jd;
  public static final Scope SCOPE_APPFOLDER;
  public static final Scope SCOPE_FILE;
  public static final Api.zzf<zzu> hg = new Api.zzf();
  
  static
  {
    SCOPE_FILE = new Scope("https://www.googleapis.com/auth/drive.file");
    SCOPE_APPFOLDER = new Scope("https://www.googleapis.com/auth/drive.appdata");
    IZ = new Scope("https://www.googleapis.com/auth/drive");
    Ja = new Scope("https://www.googleapis.com/auth/drive.apps");
    API = new Api("Drive.API", new zza()
    {
      protected Bundle zza(Api.ApiOptions.NoOptions paramAnonymousNoOptions)
      {
        return new Bundle();
      }
    }, hg);
    Jb = new Api("Drive.INTERNAL_API", new zza()
    {
      protected Bundle zza(Drive.zzb paramAnonymouszzb)
      {
        if (paramAnonymouszzb == null) {
          return new Bundle();
        }
        return paramAnonymouszzb.zzbaq();
      }
    }, hg);
    DriveApi = new zzs();
    Jc = new zzx();
    Jd = new zzac();
  }
  
  public static abstract class zza<O extends Api.ApiOptions>
    extends Api.zza<zzu, O>
  {
    protected abstract Bundle zza(O paramO);
    
    public zzu zza(Context paramContext, Looper paramLooper, com.google.android.gms.common.internal.zzf paramzzf, O paramO, GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
    {
      return new zzu(paramContext, paramLooper, paramzzf, paramConnectionCallbacks, paramOnConnectionFailedListener, zza(paramO));
    }
  }
  
  public static class zzb
    implements Api.ApiOptions.Optional
  {
    private final Bundle hf;
    
    public Bundle zzbaq()
    {
      return this.hf;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/Drive.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */