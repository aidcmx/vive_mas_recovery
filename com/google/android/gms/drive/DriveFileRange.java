package com.google.android.gms.drive;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public class DriveFileRange
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<DriveFileRange> CREATOR = new zzc();
  final long Je;
  final long Jf;
  final int mVersionCode;
  
  DriveFileRange(int paramInt, long paramLong1, long paramLong2)
  {
    this.mVersionCode = paramInt;
    this.Je = paramLong1;
    this.Jf = paramLong2;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzc.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/DriveFileRange.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */