package com.google.android.gms.drive;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.util.Base64;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.drive.internal.zzab;
import com.google.android.gms.drive.internal.zzw;
import com.google.android.gms.drive.internal.zzy;
import com.google.android.gms.drive.internal.zzz;
import com.google.android.gms.internal.zzarz;
import com.google.android.gms.internal.zzasa;
import com.google.android.gms.internal.zzte;
import com.google.android.gms.internal.zztf;

public class DriveId
  extends AbstractSafeParcelable
  implements ReflectedParcelable
{
  public static final Parcelable.Creator<DriveId> CREATOR = new zze();
  public static final int RESOURCE_TYPE_FILE = 0;
  public static final int RESOURCE_TYPE_FOLDER = 1;
  public static final int RESOURCE_TYPE_UNKNOWN = -1;
  final long IP;
  private volatile String IR = null;
  final String Jg;
  final long Jh;
  final int Ji;
  private volatile String Jj = null;
  final int mVersionCode;
  
  DriveId(int paramInt1, String paramString, long paramLong1, long paramLong2, int paramInt2)
  {
    this.mVersionCode = paramInt1;
    this.Jg = paramString;
    if (!"".equals(paramString)) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      zzaa.zzbt(bool1);
      if (paramString == null)
      {
        bool1 = bool2;
        if (paramLong1 == -1L) {}
      }
      else
      {
        bool1 = true;
      }
      zzaa.zzbt(bool1);
      this.Jh = paramLong1;
      this.IP = paramLong2;
      this.Ji = paramInt2;
      return;
    }
  }
  
  public DriveId(String paramString, long paramLong1, long paramLong2, int paramInt)
  {
    this(1, paramString, paramLong1, paramLong2, paramInt);
  }
  
  public static DriveId decodeFromString(String paramString)
  {
    boolean bool = paramString.startsWith("DriveId:");
    String str = String.valueOf(paramString);
    if (str.length() != 0) {}
    for (str = "Invalid DriveId: ".concat(str);; str = new String("Invalid DriveId: "))
    {
      zzaa.zzb(bool, str);
      return zzt(Base64.decode(paramString.substring("DriveId:".length()), 10));
    }
  }
  
  private byte[] zzbau()
  {
    zztf localzztf = new zztf();
    localzztf.MZ = this.Jh;
    localzztf.MW = this.IP;
    return zzasa.zzf(localzztf);
  }
  
  public static DriveId zzin(String paramString)
  {
    zzaa.zzy(paramString);
    return new DriveId(paramString, -1L, -1L, -1);
  }
  
  static DriveId zzt(byte[] paramArrayOfByte)
  {
    for (;;)
    {
      zzte localzzte;
      try
      {
        localzzte = zzte.zzu(paramArrayOfByte);
        if ("".equals(localzzte.MY))
        {
          paramArrayOfByte = null;
          return new DriveId(localzzte.versionCode, paramArrayOfByte, localzzte.MZ, localzzte.MW, localzzte.Na);
        }
      }
      catch (zzarz paramArrayOfByte)
      {
        throw new IllegalArgumentException();
      }
      paramArrayOfByte = localzzte.MY;
    }
  }
  
  public DriveFile asDriveFile()
  {
    if (this.Ji == 1) {
      throw new IllegalStateException("This DriveId corresponds to a folder. Call asDriveFolder instead.");
    }
    return new zzw(this);
  }
  
  public DriveFolder asDriveFolder()
  {
    if (this.Ji == 0) {
      throw new IllegalStateException("This DriveId corresponds to a file. Call asDriveFile instead.");
    }
    return new zzy(this);
  }
  
  public DriveResource asDriveResource()
  {
    if (this.Ji == 1) {
      return asDriveFolder();
    }
    if (this.Ji == 0) {
      return asDriveFile();
    }
    return new zzab(this);
  }
  
  public final String encodeToString()
  {
    String str2;
    if (this.IR == null)
    {
      str2 = Base64.encodeToString(zzbao(), 10);
      str1 = String.valueOf("DriveId:");
      str2 = String.valueOf(str2);
      if (str2.length() == 0) {
        break label51;
      }
    }
    label51:
    for (String str1 = str1.concat(str2);; str1 = new String(str1))
    {
      this.IR = str1;
      return this.IR;
    }
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool = true;
    if (!(paramObject instanceof DriveId)) {}
    do
    {
      do
      {
        return false;
        paramObject = (DriveId)paramObject;
      } while (((DriveId)paramObject).IP != this.IP);
      if ((((DriveId)paramObject).Jh == -1L) && (this.Jh == -1L)) {
        return ((DriveId)paramObject).Jg.equals(this.Jg);
      }
      if ((this.Jg == null) || (((DriveId)paramObject).Jg == null))
      {
        if (((DriveId)paramObject).Jh == this.Jh) {}
        for (;;)
        {
          return bool;
          bool = false;
        }
      }
    } while (((DriveId)paramObject).Jh != this.Jh);
    if (((DriveId)paramObject).Jg.equals(this.Jg)) {
      return true;
    }
    zzz.zzae("DriveId", "Unexpected unequal resourceId for same DriveId object.");
    return false;
  }
  
  public String getResourceId()
  {
    return this.Jg;
  }
  
  public int getResourceType()
  {
    return this.Ji;
  }
  
  public int hashCode()
  {
    if (this.Jh == -1L) {
      return this.Jg.hashCode();
    }
    String str1 = String.valueOf(String.valueOf(this.IP));
    String str2 = String.valueOf(String.valueOf(this.Jh));
    if (str2.length() != 0) {}
    for (str1 = str1.concat(str2);; str1 = new String(str1)) {
      return str1.hashCode();
    }
  }
  
  public final String toInvariantString()
  {
    if (this.Jj == null) {
      this.Jj = Base64.encodeToString(zzbau(), 10);
    }
    return this.Jj;
  }
  
  public String toString()
  {
    return encodeToString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zze.zza(this, paramParcel, paramInt);
  }
  
  final byte[] zzbao()
  {
    zzte localzzte = new zzte();
    localzzte.versionCode = this.mVersionCode;
    if (this.Jg == null) {}
    for (String str = "";; str = this.Jg)
    {
      localzzte.MY = str;
      localzzte.MZ = this.Jh;
      localzzte.MW = this.IP;
      localzzte.Na = this.Ji;
      return zzasa.zzf(localzzte);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/DriveId.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */