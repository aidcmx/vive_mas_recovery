package com.google.android.gms.drive;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.util.zzf;
import java.util.Set;
import java.util.regex.Pattern;

public class DriveSpace
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<DriveSpace> CREATOR = new zzg();
  public static final DriveSpace Jk = new DriveSpace("DRIVE");
  public static final DriveSpace Jl = new DriveSpace("APP_DATA_FOLDER");
  public static final DriveSpace Jm = new DriveSpace("PHOTOS");
  public static final Set<DriveSpace> Jn = zzf.zza(Jk, Jl, Jm);
  public static final String Jo = TextUtils.join(",", Jn.toArray());
  private static final Pattern Jp = Pattern.compile("[A-Z0-9_]*");
  private final String mName;
  final int mVersionCode;
  
  DriveSpace(int paramInt, String paramString)
  {
    this.mVersionCode = paramInt;
    this.mName = ((String)zzaa.zzy(paramString));
  }
  
  private DriveSpace(String paramString)
  {
    this(1, paramString);
  }
  
  public boolean equals(Object paramObject)
  {
    if ((paramObject == null) || (paramObject.getClass() != DriveSpace.class)) {
      return false;
    }
    return this.mName.equals(((DriveSpace)paramObject).mName);
  }
  
  public String getName()
  {
    return this.mName;
  }
  
  public int hashCode()
  {
    return 0x4A54C0DE ^ this.mName.hashCode();
  }
  
  public String toString()
  {
    return this.mName;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzg.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/DriveSpace.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */