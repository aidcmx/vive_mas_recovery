package com.google.android.gms.drive;

import android.text.TextUtils;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.drive.internal.zzu;

public class ExecutionOptions
{
  public static final int CONFLICT_STRATEGY_KEEP_REMOTE = 1;
  public static final int CONFLICT_STRATEGY_OVERWRITE_REMOTE = 0;
  public static final int MAX_TRACKING_TAG_STRING_LENGTH = 65536;
  private final String Jq;
  private final boolean Jr;
  private final int Js;
  
  public ExecutionOptions(String paramString, boolean paramBoolean, int paramInt)
  {
    this.Jq = paramString;
    this.Jr = paramBoolean;
    this.Js = paramInt;
  }
  
  public static boolean zzio(String paramString)
  {
    return (!TextUtils.isEmpty(paramString)) && (paramString.length() <= 65536);
  }
  
  public static boolean zziv(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return false;
    }
    return true;
  }
  
  public static boolean zziw(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return false;
    }
    return true;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if ((paramObject == null) || (paramObject.getClass() != getClass())) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramObject == this);
      paramObject = (ExecutionOptions)paramObject;
      if ((!zzz.equal(this.Jq, ((ExecutionOptions)paramObject).Jq)) || (this.Js != ((ExecutionOptions)paramObject).Js)) {
        break;
      }
      bool1 = bool2;
    } while (this.Jr == ((ExecutionOptions)paramObject).Jr);
    return false;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.Jq, Integer.valueOf(this.Js), Boolean.valueOf(this.Jr) });
  }
  
  public String zzbav()
  {
    return this.Jq;
  }
  
  public boolean zzbaw()
  {
    return this.Jr;
  }
  
  public int zzbax()
  {
    return this.Js;
  }
  
  public void zzh(GoogleApiClient paramGoogleApiClient)
  {
    paramGoogleApiClient = (zzu)paramGoogleApiClient.zza(Drive.hg);
    if ((zzbaw()) && (!paramGoogleApiClient.zzbcc())) {
      throw new IllegalStateException("Application must define an exported DriveEventService subclass in AndroidManifest.xml to be notified on completion");
    }
  }
  
  public static class Builder
  {
    protected String Jq;
    protected boolean Jr;
    protected int Js = 0;
    
    public ExecutionOptions build()
    {
      zzbay();
      return new ExecutionOptions(this.Jq, this.Jr, this.Js);
    }
    
    public Builder setConflictStrategy(int paramInt)
    {
      if (!ExecutionOptions.zziw(paramInt)) {
        throw new IllegalArgumentException(53 + "Unrecognized value for conflict strategy: " + paramInt);
      }
      this.Js = paramInt;
      return this;
    }
    
    public Builder setNotifyOnCompletion(boolean paramBoolean)
    {
      this.Jr = paramBoolean;
      return this;
    }
    
    public Builder setTrackingTag(String paramString)
    {
      if (!ExecutionOptions.zzio(paramString)) {
        throw new IllegalArgumentException(String.format("trackingTag must not be null nor empty, and the length must be <= the maximum length (%s)", new Object[] { Integer.valueOf(65536) }));
      }
      this.Jq = paramString;
      return this;
    }
    
    protected void zzbay()
    {
      if ((this.Js == 1) && (!this.Jr)) {
        throw new IllegalStateException("Cannot use CONFLICT_STRATEGY_KEEP_REMOTE without requesting completion notifications");
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/ExecutionOptions.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */