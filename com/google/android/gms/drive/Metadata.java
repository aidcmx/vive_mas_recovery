package com.google.android.gms.drive;

import com.google.android.gms.common.data.Freezable;
import com.google.android.gms.drive.metadata.CustomPropertyKey;
import com.google.android.gms.drive.metadata.MetadataField;
import com.google.android.gms.drive.metadata.internal.AppVisibleCustomProperties;
import com.google.android.gms.internal.zztg;
import com.google.android.gms.internal.zzti;
import com.google.android.gms.internal.zztk;
import java.util.Collections;
import java.util.Date;
import java.util.Map;

public abstract class Metadata
  implements Freezable<Metadata>
{
  public static final int CONTENT_AVAILABLE_LOCALLY = 1;
  public static final int CONTENT_NOT_AVAILABLE_LOCALLY = 0;
  
  public String getAlternateLink()
  {
    return (String)zza(zztg.Nr);
  }
  
  public int getContentAvailability()
  {
    Integer localInteger = (Integer)zza(zztk.Op);
    if (localInteger == null) {
      return 0;
    }
    return localInteger.intValue();
  }
  
  public Date getCreatedDate()
  {
    return (Date)zza(zzti.Oi);
  }
  
  public Map<CustomPropertyKey, String> getCustomProperties()
  {
    AppVisibleCustomProperties localAppVisibleCustomProperties = (AppVisibleCustomProperties)zza(zztg.Ns);
    if (localAppVisibleCustomProperties == null) {
      return Collections.emptyMap();
    }
    return localAppVisibleCustomProperties.zzbct();
  }
  
  public String getDescription()
  {
    return (String)zza(zztg.Nt);
  }
  
  public DriveId getDriveId()
  {
    return (DriveId)zza(zztg.Nq);
  }
  
  public String getEmbedLink()
  {
    return (String)zza(zztg.Nu);
  }
  
  public String getFileExtension()
  {
    return (String)zza(zztg.Nv);
  }
  
  public long getFileSize()
  {
    return ((Long)zza(zztg.Nw)).longValue();
  }
  
  public Date getLastViewedByMeDate()
  {
    return (Date)zza(zzti.Oj);
  }
  
  public String getMimeType()
  {
    return (String)zza(zztg.NN);
  }
  
  public Date getModifiedByMeDate()
  {
    return (Date)zza(zzti.Ol);
  }
  
  public Date getModifiedDate()
  {
    return (Date)zza(zzti.Ok);
  }
  
  public String getOriginalFilename()
  {
    return (String)zza(zztg.NO);
  }
  
  public long getQuotaBytesUsed()
  {
    return ((Long)zza(zztg.NT)).longValue();
  }
  
  public Date getSharedWithMeDate()
  {
    return (Date)zza(zzti.Om);
  }
  
  public String getTitle()
  {
    return (String)zza(zztg.NW);
  }
  
  public String getWebContentLink()
  {
    return (String)zza(zztg.NY);
  }
  
  public String getWebViewLink()
  {
    return (String)zza(zztg.NZ);
  }
  
  public boolean isEditable()
  {
    Boolean localBoolean = (Boolean)zza(zztg.NC);
    if (localBoolean == null) {
      return false;
    }
    return localBoolean.booleanValue();
  }
  
  public boolean isExplicitlyTrashed()
  {
    Boolean localBoolean = (Boolean)zza(zztg.ND);
    if (localBoolean == null) {
      return false;
    }
    return localBoolean.booleanValue();
  }
  
  public boolean isFolder()
  {
    return "application/vnd.google-apps.folder".equals(getMimeType());
  }
  
  public boolean isInAppFolder()
  {
    Boolean localBoolean = (Boolean)zza(zztg.NA);
    if (localBoolean == null) {
      return false;
    }
    return localBoolean.booleanValue();
  }
  
  public boolean isPinnable()
  {
    Boolean localBoolean = (Boolean)zza(zztk.Oq);
    if (localBoolean == null) {
      return false;
    }
    return localBoolean.booleanValue();
  }
  
  public boolean isPinned()
  {
    Boolean localBoolean = (Boolean)zza(zztg.NF);
    if (localBoolean == null) {
      return false;
    }
    return localBoolean.booleanValue();
  }
  
  public boolean isRestricted()
  {
    Boolean localBoolean = (Boolean)zza(zztg.NH);
    if (localBoolean == null) {
      return false;
    }
    return localBoolean.booleanValue();
  }
  
  public boolean isShared()
  {
    Boolean localBoolean = (Boolean)zza(zztg.NI);
    if (localBoolean == null) {
      return false;
    }
    return localBoolean.booleanValue();
  }
  
  public boolean isStarred()
  {
    Boolean localBoolean = (Boolean)zza(zztg.NU);
    if (localBoolean == null) {
      return false;
    }
    return localBoolean.booleanValue();
  }
  
  public boolean isTrashable()
  {
    Boolean localBoolean = (Boolean)zza(zztg.NL);
    if (localBoolean == null) {
      return true;
    }
    return localBoolean.booleanValue();
  }
  
  public boolean isTrashed()
  {
    Boolean localBoolean = (Boolean)zza(zztg.NX);
    if (localBoolean == null) {
      return false;
    }
    return localBoolean.booleanValue();
  }
  
  public boolean isViewed()
  {
    Boolean localBoolean = (Boolean)zza(zztg.NM);
    if (localBoolean == null) {
      return false;
    }
    return localBoolean.booleanValue();
  }
  
  public abstract <T> T zza(MetadataField<T> paramMetadataField);
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/Metadata.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */