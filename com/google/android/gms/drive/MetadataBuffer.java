package com.google.android.gms.drive;

import android.os.Bundle;
import com.google.android.gms.common.data.AbstractDataBuffer;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.internal.zzp;
import com.google.android.gms.drive.metadata.MetadataField;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;
import com.google.android.gms.drive.metadata.internal.zze;
import com.google.android.gms.internal.zztg;
import java.util.Collection;
import java.util.Iterator;

public final class MetadataBuffer
  extends AbstractDataBuffer<Metadata>
{
  private zza Jw;
  
  public MetadataBuffer(DataHolder paramDataHolder)
  {
    super(paramDataHolder);
    paramDataHolder.zzaui().setClassLoader(MetadataBuffer.class.getClassLoader());
  }
  
  public Metadata get(int paramInt)
  {
    zza localzza2 = this.Jw;
    zza localzza1;
    if (localzza2 != null)
    {
      localzza1 = localzza2;
      if (zza.zza(localzza2) == paramInt) {}
    }
    else
    {
      localzza1 = new zza(this.zy, paramInt);
      this.Jw = localzza1;
    }
    return localzza1;
  }
  
  @Deprecated
  public String getNextPageToken()
  {
    return null;
  }
  
  public void release()
  {
    if (this.zy != null) {
      zze.zza(this.zy);
    }
    super.release();
  }
  
  private static class zza
    extends Metadata
  {
    private final int BV;
    private final int Jx;
    private final DataHolder zy;
    
    public zza(DataHolder paramDataHolder, int paramInt)
    {
      this.zy = paramDataHolder;
      this.Jx = paramInt;
      this.BV = paramDataHolder.zzga(paramInt);
    }
    
    public boolean isDataValid()
    {
      return !this.zy.isClosed();
    }
    
    public <T> T zza(MetadataField<T> paramMetadataField)
    {
      return (T)paramMetadataField.zza(this.zy, this.Jx, this.BV);
    }
    
    public Metadata zzbbe()
    {
      MetadataBundle localMetadataBundle = MetadataBundle.zzbcy();
      Iterator localIterator = zze.zzbcw().iterator();
      while (localIterator.hasNext())
      {
        MetadataField localMetadataField = (MetadataField)localIterator.next();
        if (localMetadataField != zztg.NV) {
          localMetadataField.zza(this.zy, localMetadataBundle, this.Jx, this.BV);
        }
      }
      return new zzp(localMetadataBundle);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/MetadataBuffer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */