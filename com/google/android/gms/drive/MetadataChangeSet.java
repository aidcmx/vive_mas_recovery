package com.google.android.gms.drive;

import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.drive.metadata.CustomPropertyKey;
import com.google.android.gms.drive.metadata.MetadataField;
import com.google.android.gms.drive.metadata.internal.AppVisibleCustomProperties;
import com.google.android.gms.drive.metadata.internal.AppVisibleCustomProperties.zza;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;
import com.google.android.gms.internal.zztg;
import com.google.android.gms.internal.zzti;
import java.util.Collections;
import java.util.Date;
import java.util.Map;

public final class MetadataChangeSet
{
  public static final int CUSTOM_PROPERTY_SIZE_LIMIT_BYTES = 124;
  public static final int INDEXABLE_TEXT_SIZE_LIMIT_BYTES = 131072;
  public static final MetadataChangeSet Jy = new MetadataChangeSet(MetadataBundle.zzbcy());
  public static final int MAX_PRIVATE_PROPERTIES_PER_RESOURCE_PER_APP = 30;
  public static final int MAX_PUBLIC_PROPERTIES_PER_RESOURCE = 30;
  public static final int MAX_TOTAL_PROPERTIES_PER_RESOURCE = 100;
  private final MetadataBundle Jz;
  
  public MetadataChangeSet(MetadataBundle paramMetadataBundle)
  {
    this.Jz = paramMetadataBundle.zzbcz();
  }
  
  public Map<CustomPropertyKey, String> getCustomPropertyChangeMap()
  {
    AppVisibleCustomProperties localAppVisibleCustomProperties = (AppVisibleCustomProperties)this.Jz.zza(zztg.Ns);
    if (localAppVisibleCustomProperties == null) {
      return Collections.emptyMap();
    }
    return localAppVisibleCustomProperties.zzbct();
  }
  
  public String getDescription()
  {
    return (String)this.Jz.zza(zztg.Nt);
  }
  
  public String getIndexableText()
  {
    return (String)this.Jz.zza(zztg.Nz);
  }
  
  public Date getLastViewedByMeDate()
  {
    return (Date)this.Jz.zza(zzti.Oj);
  }
  
  public String getMimeType()
  {
    return (String)this.Jz.zza(zztg.NN);
  }
  
  public String getTitle()
  {
    return (String)this.Jz.zza(zztg.NW);
  }
  
  public Boolean isPinned()
  {
    return (Boolean)this.Jz.zza(zztg.NF);
  }
  
  public Boolean isStarred()
  {
    return (Boolean)this.Jz.zza(zztg.NU);
  }
  
  public Boolean isViewed()
  {
    return (Boolean)this.Jz.zza(zztg.NM);
  }
  
  public <T> MetadataChangeSet zza(MetadataField<T> paramMetadataField, T paramT)
  {
    MetadataChangeSet localMetadataChangeSet = zzbbg();
    localMetadataChangeSet.zzbbf().zzc(paramMetadataField, paramT);
    return localMetadataChangeSet;
  }
  
  public MetadataBundle zzbbf()
  {
    return this.Jz;
  }
  
  public MetadataChangeSet zzbbg()
  {
    return new MetadataChangeSet(zzbbf());
  }
  
  public static class Builder
  {
    private AppVisibleCustomProperties.zza JA;
    private final MetadataBundle Jz = MetadataBundle.zzbcy();
    
    private AppVisibleCustomProperties.zza zzbbh()
    {
      if (this.JA == null) {
        this.JA = new AppVisibleCustomProperties.zza();
      }
      return this.JA;
    }
    
    private int zzir(String paramString)
    {
      if (paramString == null) {
        return 0;
      }
      return paramString.getBytes().length;
    }
    
    private String zzj(String paramString, int paramInt1, int paramInt2)
    {
      return String.format("%s must be no more than %d bytes, but is %d bytes.", new Object[] { paramString, Integer.valueOf(paramInt1), Integer.valueOf(paramInt2) });
    }
    
    private void zzk(String paramString, int paramInt1, int paramInt2)
    {
      if (paramInt2 <= paramInt1) {}
      for (boolean bool = true;; bool = false)
      {
        zzaa.zzb(bool, zzj(paramString, paramInt1, paramInt2));
        return;
      }
    }
    
    public MetadataChangeSet build()
    {
      if (this.JA != null) {
        this.Jz.zzc(zztg.Ns, this.JA.zzbcu());
      }
      return new MetadataChangeSet(this.Jz);
    }
    
    public Builder deleteCustomProperty(CustomPropertyKey paramCustomPropertyKey)
    {
      zzaa.zzb(paramCustomPropertyKey, "key");
      zzbbh().zza(paramCustomPropertyKey, null);
      return this;
    }
    
    public Builder setCustomProperty(CustomPropertyKey paramCustomPropertyKey, String paramString)
    {
      zzaa.zzb(paramCustomPropertyKey, "key");
      zzaa.zzb(paramString, "value");
      zzk("The total size of key string and value string of a custom property", 124, zzir(paramCustomPropertyKey.getKey()) + zzir(paramString));
      zzbbh().zza(paramCustomPropertyKey, paramString);
      return this;
    }
    
    public Builder setDescription(String paramString)
    {
      this.Jz.zzc(zztg.Nt, paramString);
      return this;
    }
    
    public Builder setIndexableText(String paramString)
    {
      zzk("Indexable text size", 131072, zzir(paramString));
      this.Jz.zzc(zztg.Nz, paramString);
      return this;
    }
    
    public Builder setLastViewedByMeDate(Date paramDate)
    {
      this.Jz.zzc(zzti.Oj, paramDate);
      return this;
    }
    
    public Builder setMimeType(String paramString)
    {
      this.Jz.zzc(zztg.NN, paramString);
      return this;
    }
    
    public Builder setPinned(boolean paramBoolean)
    {
      this.Jz.zzc(zztg.NF, Boolean.valueOf(paramBoolean));
      return this;
    }
    
    public Builder setStarred(boolean paramBoolean)
    {
      this.Jz.zzc(zztg.NU, Boolean.valueOf(paramBoolean));
      return this;
    }
    
    public Builder setTitle(String paramString)
    {
      this.Jz.zzc(zztg.NW, paramString);
      return this;
    }
    
    public Builder setViewed(boolean paramBoolean)
    {
      this.Jz.zzc(zztg.NM, Boolean.valueOf(paramBoolean));
      return this;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/MetadataChangeSet.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */