package com.google.android.gms.drive;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;

public class Permission
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<Permission> CREATOR = new zzj();
  private String JF;
  private int JG;
  private String JH;
  private String JI;
  private int JJ;
  private boolean JK;
  final int mVersionCode;
  
  Permission(int paramInt1, String paramString1, int paramInt2, String paramString2, String paramString3, int paramInt3, boolean paramBoolean)
  {
    this.mVersionCode = paramInt1;
    this.JF = paramString1;
    this.JG = paramInt2;
    this.JH = paramString2;
    this.JI = paramString3;
    this.JJ = paramInt3;
    this.JK = paramBoolean;
  }
  
  public static boolean zziz(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return false;
    }
    return true;
  }
  
  public static boolean zzja(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return false;
    }
    return true;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if ((paramObject == null) || (paramObject.getClass() != getClass())) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramObject == this);
      paramObject = (Permission)paramObject;
      if ((!zzz.equal(this.JF, ((Permission)paramObject).JF)) || (this.JG != ((Permission)paramObject).JG) || (this.JJ != ((Permission)paramObject).JJ)) {
        break;
      }
      bool1 = bool2;
    } while (this.JK == ((Permission)paramObject).JK);
    return false;
  }
  
  public int getRole()
  {
    if (!zzja(this.JJ)) {
      return -1;
    }
    return this.JJ;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.JF, Integer.valueOf(this.JG), Integer.valueOf(this.JJ), Boolean.valueOf(this.JK) });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzj.zza(this, paramParcel, paramInt);
  }
  
  public String zzbbi()
  {
    if (!zziz(this.JG)) {
      return null;
    }
    return this.JF;
  }
  
  public int zzbbj()
  {
    if (!zziz(this.JG)) {
      return -1;
    }
    return this.JG;
  }
  
  public String zzbbk()
  {
    return this.JH;
  }
  
  public String zzbbl()
  {
    return this.JI;
  }
  
  public boolean zzbbm()
  {
    return this.JK;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/Permission.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */