package com.google.android.gms.drive;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;

public abstract class WriteAwareParcelable
  extends AbstractSafeParcelable
{
  private volatile transient boolean JR = false;
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    if (!zzbbn()) {}
    for (boolean bool = true;; bool = false)
    {
      zzaa.zzbs(bool);
      this.JR = true;
      zzak(paramParcel, paramInt);
      return;
    }
  }
  
  protected abstract void zzak(Parcel paramParcel, int paramInt);
  
  public final boolean zzbbn()
  {
    return this.JR;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/WriteAwareParcelable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */