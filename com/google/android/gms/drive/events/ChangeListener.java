package com.google.android.gms.drive.events;

public abstract interface ChangeListener
  extends zzf
{
  public abstract void onChange(ChangeEvent paramChangeEvent);
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/events/ChangeListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */