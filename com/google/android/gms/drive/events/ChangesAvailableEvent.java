package com.google.android.gms.drive.events;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import java.util.Locale;

public final class ChangesAvailableEvent
  extends AbstractSafeParcelable
  implements DriveEvent
{
  public static final Parcelable.Creator<ChangesAvailableEvent> CREATOR = new zzb();
  final ChangesAvailableOptions JT;
  final String hy;
  final int mVersionCode;
  
  ChangesAvailableEvent(int paramInt, String paramString, ChangesAvailableOptions paramChangesAvailableOptions)
  {
    this.mVersionCode = paramInt;
    this.hy = paramString;
    this.JT = paramChangesAvailableOptions;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if ((paramObject == null) || (paramObject.getClass() != getClass())) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramObject == this);
      paramObject = (ChangesAvailableEvent)paramObject;
      if (!zzz.equal(this.JT, ((ChangesAvailableEvent)paramObject).JT)) {
        break;
      }
      bool1 = bool2;
    } while (zzz.equal(this.hy, ((ChangesAvailableEvent)paramObject).hy));
    return false;
  }
  
  public int getType()
  {
    return 4;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.JT, this.hy });
  }
  
  public String toString()
  {
    return String.format(Locale.US, "ChangesAvailableEvent [changesAvailableOptions=%s]", new Object[] { this.JT });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzb.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/events/ChangesAvailableEvent.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */