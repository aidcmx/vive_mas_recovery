package com.google.android.gms.drive.events;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.drive.DriveSpace;
import java.util.List;
import java.util.Locale;
import java.util.Set;

public final class ChangesAvailableOptions
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<ChangesAvailableOptions> CREATOR = new zzd();
  final int JU;
  final boolean JV;
  final List<DriveSpace> JW;
  private final Set<DriveSpace> JX;
  final int mVersionCode;
  
  ChangesAvailableOptions(int paramInt1, int paramInt2, boolean paramBoolean, List<DriveSpace> paramList) {}
  
  private ChangesAvailableOptions(int paramInt1, int paramInt2, boolean paramBoolean, List<DriveSpace> paramList, Set<DriveSpace> paramSet)
  {
    this.mVersionCode = paramInt1;
    this.JU = paramInt2;
    this.JV = paramBoolean;
    this.JW = paramList;
    this.JX = paramSet;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if ((paramObject == null) || (paramObject.getClass() != getClass())) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramObject == this);
      paramObject = (ChangesAvailableOptions)paramObject;
      if ((!zzz.equal(this.JX, ((ChangesAvailableOptions)paramObject).JX)) || (this.JU != ((ChangesAvailableOptions)paramObject).JU)) {
        break;
      }
      bool1 = bool2;
    } while (this.JV == ((ChangesAvailableOptions)paramObject).JV);
    return false;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.JX, Integer.valueOf(this.JU), Boolean.valueOf(this.JV) });
  }
  
  public String toString()
  {
    return String.format(Locale.US, "ChangesAvailableOptions[ChangesSizeLimit=%d, Repeats=%s, Spaces=%s]", new Object[] { Integer.valueOf(this.JU), Boolean.valueOf(this.JV), this.JW });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzd.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/events/ChangesAvailableOptions.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */