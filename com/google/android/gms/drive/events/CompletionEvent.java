package com.google.android.gms.drive.events;

import android.os.IBinder;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import android.text.TextUtils;
import com.google.android.gms.common.data.BitmapTeleporter;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.util.zzo;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.drive.internal.zzap;
import com.google.android.gms.drive.internal.zzap.zza;
import com.google.android.gms.drive.internal.zzz;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;
import com.google.android.gms.internal.zztg;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public final class CompletionEvent
  extends AbstractSafeParcelable
  implements ResourceEvent
{
  public static final Parcelable.Creator<CompletionEvent> CREATOR = new zze();
  public static final int STATUS_CANCELED = 3;
  public static final int STATUS_CONFLICT = 2;
  public static final int STATUS_FAILURE = 1;
  public static final int STATUS_SUCCESS = 0;
  final DriveId IU;
  final ParcelFileDescriptor JY;
  final ParcelFileDescriptor JZ;
  final MetadataBundle Ka;
  final List<String> Kb;
  final IBinder Kc;
  private boolean Kd = false;
  private boolean Ke = false;
  private boolean Kf = false;
  final String hy;
  final int mVersionCode;
  final int zzbtt;
  
  CompletionEvent(int paramInt1, DriveId paramDriveId, String paramString, ParcelFileDescriptor paramParcelFileDescriptor1, ParcelFileDescriptor paramParcelFileDescriptor2, MetadataBundle paramMetadataBundle, List<String> paramList, int paramInt2, IBinder paramIBinder)
  {
    this.mVersionCode = paramInt1;
    this.IU = paramDriveId;
    this.hy = paramString;
    this.JY = paramParcelFileDescriptor1;
    this.JZ = paramParcelFileDescriptor2;
    this.Ka = paramMetadataBundle;
    this.Kb = paramList;
    this.zzbtt = paramInt2;
    this.Kc = paramIBinder;
  }
  
  private void zzbbo()
  {
    if (this.Kf) {
      throw new IllegalStateException("Event has already been dismissed or snoozed.");
    }
  }
  
  private void zzz(boolean paramBoolean)
  {
    zzbbo();
    this.Kf = true;
    zzo.zza(this.JY);
    zzo.zza(this.JZ);
    if ((this.Ka != null) && (this.Ka.zzc(zztg.NV))) {
      ((BitmapTeleporter)this.Ka.zza(zztg.NV)).release();
    }
    if (this.Kc == null)
    {
      if (paramBoolean)
      {
        str1 = "snooze";
        str1 = String.valueOf(str1);
        if (str1.length() == 0) {
          break label105;
        }
      }
      label105:
      for (str1 = "No callback on ".concat(str1);; str1 = new String("No callback on "))
      {
        zzz.zzaf("CompletionEvent", str1);
        return;
        str1 = "dismiss";
        break;
      }
    }
    try
    {
      zzap.zza.zzeo(this.Kc).zzz(paramBoolean);
      return;
    }
    catch (RemoteException localRemoteException)
    {
      if (!paramBoolean) {}
    }
    for (String str1 = "snooze";; str1 = "dismiss")
    {
      String str2 = String.valueOf(localRemoteException);
      zzz.zzaf("CompletionEvent", String.valueOf(str1).length() + 21 + String.valueOf(str2).length() + "RemoteException on " + str1 + ": " + str2);
      return;
    }
  }
  
  public void dismiss()
  {
    zzz(false);
  }
  
  public String getAccountName()
  {
    zzbbo();
    return this.hy;
  }
  
  public InputStream getBaseContentsInputStream()
  {
    zzbbo();
    if (this.JY == null) {
      return null;
    }
    if (this.Kd) {
      throw new IllegalStateException("getBaseInputStream() can only be called once per CompletionEvent instance.");
    }
    this.Kd = true;
    return new FileInputStream(this.JY.getFileDescriptor());
  }
  
  public DriveId getDriveId()
  {
    zzbbo();
    return this.IU;
  }
  
  public InputStream getModifiedContentsInputStream()
  {
    zzbbo();
    if (this.JZ == null) {
      return null;
    }
    if (this.Ke) {
      throw new IllegalStateException("getModifiedInputStream() can only be called once per CompletionEvent instance.");
    }
    this.Ke = true;
    return new FileInputStream(this.JZ.getFileDescriptor());
  }
  
  public MetadataChangeSet getModifiedMetadataChangeSet()
  {
    zzbbo();
    if (this.Ka != null) {
      return new MetadataChangeSet(this.Ka);
    }
    return null;
  }
  
  public int getStatus()
  {
    zzbbo();
    return this.zzbtt;
  }
  
  public List<String> getTrackingTags()
  {
    zzbbo();
    return new ArrayList(this.Kb);
  }
  
  public int getType()
  {
    return 2;
  }
  
  public void snooze()
  {
    zzz(true);
  }
  
  public String toString()
  {
    if (this.Kb == null) {}
    for (String str = "<null>";; str = String.valueOf(str).length() + 2 + "'" + str + "'")
    {
      return String.format(Locale.US, "CompletionEvent [id=%s, status=%s, trackingTag=%s]", new Object[] { this.IU, Integer.valueOf(this.zzbtt), str });
      str = String.valueOf(TextUtils.join("','", this.Kb));
    }
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zze.zza(this, paramParcel, paramInt | 0x1);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/events/CompletionEvent.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */