package com.google.android.gms.drive.events;

public abstract interface CompletionListener
  extends zzf
{
  public abstract void onCompletion(CompletionEvent paramCompletionEvent);
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/events/CompletionListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */