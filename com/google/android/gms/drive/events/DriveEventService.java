package com.google.android.gms.drive.events;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import com.google.android.gms.common.util.zzx;
import com.google.android.gms.drive.internal.OnEventResponse;
import com.google.android.gms.drive.internal.zzao.zza;
import com.google.android.gms.drive.internal.zzz;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public abstract class DriveEventService
  extends Service
  implements ChangeListener, CompletionListener, zzc, zzq
{
  public static final String ACTION_HANDLE_EVENT = "com.google.android.gms.drive.events.HANDLE_EVENT";
  int De = -1;
  private CountDownLatch Kg;
  zza Kh;
  boolean Ki = false;
  private final String mName;
  
  protected DriveEventService()
  {
    this("DriveEventService");
  }
  
  protected DriveEventService(String paramString)
  {
    this.mName = paramString;
  }
  
  private void zza(OnEventResponse paramOnEventResponse)
  {
    paramOnEventResponse = paramOnEventResponse.zzbcj();
    String str1 = String.valueOf(paramOnEventResponse);
    zzz.zzad("DriveEventService", String.valueOf(str1).length() + 20 + "handleEventMessage: " + str1);
    for (;;)
    {
      try
      {
        switch (paramOnEventResponse.getType())
        {
        case 3: 
        case 5: 
        case 6: 
          str1 = this.mName;
          str2 = String.valueOf(paramOnEventResponse);
          zzz.zzae(str1, String.valueOf(str2).length() + 17 + "Unhandled event: " + str2);
          return;
        }
      }
      catch (Exception localException)
      {
        String str2 = this.mName;
        paramOnEventResponse = String.valueOf(paramOnEventResponse);
        zzz.zza(str2, localException, String.valueOf(paramOnEventResponse).length() + 22 + "Error handling event: " + paramOnEventResponse);
        return;
      }
      onChange((ChangeEvent)paramOnEventResponse);
      return;
      onCompletion((CompletionEvent)paramOnEventResponse);
      return;
      zza((ChangesAvailableEvent)paramOnEventResponse);
      return;
      zza((TransferStateEvent)paramOnEventResponse);
      return;
    }
  }
  
  private void zzbbp()
    throws SecurityException
  {
    int i = getCallingUid();
    if (i == this.De) {
      return;
    }
    if (zzx.zzf(this, i))
    {
      this.De = i;
      return;
    }
    throw new SecurityException("Caller is not GooglePlayServices");
  }
  
  protected int getCallingUid()
  {
    return Binder.getCallingUid();
  }
  
  public final IBinder onBind(final Intent paramIntent)
  {
    for (;;)
    {
      try
      {
        if ("com.google.android.gms.drive.events.HANDLE_EVENT".equals(paramIntent.getAction()))
        {
          if ((this.Kh == null) && (!this.Ki))
          {
            this.Ki = true;
            paramIntent = new CountDownLatch(1);
            this.Kg = new CountDownLatch(1);
            new Thread()
            {
              public void run()
              {
                try
                {
                  Looper.prepare();
                  DriveEventService.this.Kh = new DriveEventService.zza(DriveEventService.this);
                  DriveEventService.this.Ki = false;
                  paramIntent.countDown();
                  zzz.zzad("DriveEventService", "Bound and starting loop");
                  Looper.loop();
                  zzz.zzad("DriveEventService", "Finished loop");
                  return;
                }
                finally
                {
                  if (DriveEventService.zzb(DriveEventService.this) != null) {
                    DriveEventService.zzb(DriveEventService.this).countDown();
                  }
                }
              }
            }.start();
          }
          try
          {
            if (!paramIntent.await(5000L, TimeUnit.MILLISECONDS)) {
              zzz.zzaf("DriveEventService", "Failed to synchronously initialize event handler.");
            }
            paramIntent = new zzb().asBinder();
            return paramIntent;
          }
          catch (InterruptedException paramIntent)
          {
            throw new RuntimeException("Unable to start event handler", paramIntent);
          }
        }
        paramIntent = null;
      }
      finally {}
    }
  }
  
  public void onChange(ChangeEvent paramChangeEvent)
  {
    String str = this.mName;
    paramChangeEvent = String.valueOf(paramChangeEvent);
    zzz.zzae(str, String.valueOf(paramChangeEvent).length() + 24 + "Unhandled change event: " + paramChangeEvent);
  }
  
  public void onCompletion(CompletionEvent paramCompletionEvent)
  {
    String str = this.mName;
    paramCompletionEvent = String.valueOf(paramCompletionEvent);
    zzz.zzae(str, String.valueOf(paramCompletionEvent).length() + 28 + "Unhandled completion event: " + paramCompletionEvent);
  }
  
  /* Error */
  public void onDestroy()
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: ldc 37
    //   4: ldc -42
    //   6: invokestatic 99	com/google/android/gms/drive/internal/zzz:zzad	(Ljava/lang/String;Ljava/lang/String;)V
    //   9: aload_0
    //   10: getfield 169	com/google/android/gms/drive/events/DriveEventService:Kh	Lcom/google/android/gms/drive/events/DriveEventService$zza;
    //   13: ifnull +53 -> 66
    //   16: aload_0
    //   17: getfield 169	com/google/android/gms/drive/events/DriveEventService:Kh	Lcom/google/android/gms/drive/events/DriveEventService$zza;
    //   20: invokestatic 217	com/google/android/gms/drive/events/DriveEventService$zza:zza	(Lcom/google/android/gms/drive/events/DriveEventService$zza;)Landroid/os/Message;
    //   23: astore_1
    //   24: aload_0
    //   25: getfield 169	com/google/android/gms/drive/events/DriveEventService:Kh	Lcom/google/android/gms/drive/events/DriveEventService$zza;
    //   28: aload_1
    //   29: invokevirtual 221	com/google/android/gms/drive/events/DriveEventService$zza:sendMessage	(Landroid/os/Message;)Z
    //   32: pop
    //   33: aload_0
    //   34: aconst_null
    //   35: putfield 169	com/google/android/gms/drive/events/DriveEventService:Kh	Lcom/google/android/gms/drive/events/DriveEventService$zza;
    //   38: aload_0
    //   39: getfield 139	com/google/android/gms/drive/events/DriveEventService:Kg	Ljava/util/concurrent/CountDownLatch;
    //   42: ldc2_w 179
    //   45: getstatic 186	java/util/concurrent/TimeUnit:MILLISECONDS	Ljava/util/concurrent/TimeUnit;
    //   48: invokevirtual 190	java/util/concurrent/CountDownLatch:await	(JLjava/util/concurrent/TimeUnit;)Z
    //   51: ifne +10 -> 61
    //   54: ldc 37
    //   56: ldc -33
    //   58: invokestatic 109	com/google/android/gms/drive/internal/zzz:zzae	(Ljava/lang/String;Ljava/lang/String;)V
    //   61: aload_0
    //   62: aconst_null
    //   63: putfield 139	com/google/android/gms/drive/events/DriveEventService:Kg	Ljava/util/concurrent/CountDownLatch;
    //   66: aload_0
    //   67: invokespecial 225	android/app/Service:onDestroy	()V
    //   70: aload_0
    //   71: monitorexit
    //   72: return
    //   73: astore_1
    //   74: aload_0
    //   75: monitorexit
    //   76: aload_1
    //   77: athrow
    //   78: astore_1
    //   79: goto -18 -> 61
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	82	0	this	DriveEventService
    //   23	6	1	localMessage	Message
    //   73	4	1	localObject	Object
    //   78	1	1	localInterruptedException	InterruptedException
    // Exception table:
    //   from	to	target	type
    //   2	38	73	finally
    //   38	61	73	finally
    //   61	66	73	finally
    //   66	70	73	finally
    //   38	61	78	java/lang/InterruptedException
  }
  
  public boolean onUnbind(Intent paramIntent)
  {
    return true;
  }
  
  public void zza(ChangesAvailableEvent paramChangesAvailableEvent)
  {
    String str = this.mName;
    paramChangesAvailableEvent = String.valueOf(paramChangesAvailableEvent);
    zzz.zzae(str, String.valueOf(paramChangesAvailableEvent).length() + 35 + "Unhandled changes available event: " + paramChangesAvailableEvent);
  }
  
  public void zza(TransferStateEvent paramTransferStateEvent)
  {
    String str = this.mName;
    paramTransferStateEvent = String.valueOf(paramTransferStateEvent);
    zzz.zzae(str, String.valueOf(paramTransferStateEvent).length() + 32 + "Unhandled transfer state event: " + paramTransferStateEvent);
  }
  
  final class zza
    extends Handler
  {
    zza() {}
    
    private Message zzb(OnEventResponse paramOnEventResponse)
    {
      return obtainMessage(1, paramOnEventResponse);
    }
    
    private Message zzbbq()
    {
      return obtainMessage(2);
    }
    
    public void handleMessage(Message paramMessage)
    {
      int i = paramMessage.what;
      zzz.zzad("DriveEventService", 38 + "handleMessage message type:" + i);
      switch (paramMessage.what)
      {
      default: 
        i = paramMessage.what;
        zzz.zzae("DriveEventService", 35 + "Unexpected message type:" + i);
        return;
      case 1: 
        DriveEventService.zza(DriveEventService.this, (OnEventResponse)paramMessage.obj);
        return;
      }
      getLooper().quit();
    }
  }
  
  final class zzb
    extends zzao.zza
  {
    zzb() {}
    
    public void zzc(OnEventResponse paramOnEventResponse)
      throws RemoteException
    {
      synchronized (DriveEventService.this)
      {
        String str = String.valueOf(paramOnEventResponse);
        zzz.zzad("DriveEventService", String.valueOf(str).length() + 9 + "onEvent: " + str);
        DriveEventService.zza(DriveEventService.this);
        if (DriveEventService.this.Kh != null)
        {
          paramOnEventResponse = DriveEventService.zza.zza(DriveEventService.this.Kh, paramOnEventResponse);
          DriveEventService.this.Kh.sendMessage(paramOnEventResponse);
          return;
        }
        zzz.zzaf("DriveEventService", "Receiving event before initialize is completed.");
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/events/DriveEventService.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */