package com.google.android.gms.drive.events;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.WriteAwareParcelable;

public class QueryResultEventParcelable
  extends WriteAwareParcelable
  implements DriveEvent
{
  public static final Parcelable.Creator<QueryResultEventParcelable> CREATOR = new zzl();
  final boolean Kl;
  final int Km;
  final int mVersionCode;
  final DataHolder zy;
  
  QueryResultEventParcelable(int paramInt1, DataHolder paramDataHolder, boolean paramBoolean, int paramInt2)
  {
    this.mVersionCode = paramInt1;
    this.zy = paramDataHolder;
    this.Kl = paramBoolean;
    this.Km = paramInt2;
  }
  
  public int getType()
  {
    return 3;
  }
  
  public void zzak(Parcel paramParcel, int paramInt)
  {
    zzl.zza(this, paramParcel, paramInt);
  }
  
  public DataHolder zzbbr()
  {
    return this.zy;
  }
  
  public boolean zzbbs()
  {
    return this.Kl;
  }
  
  public int zzbbt()
  {
    return this.Km;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/events/QueryResultEventParcelable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */