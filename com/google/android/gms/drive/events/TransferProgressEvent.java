package com.google.android.gms.drive.events;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.drive.events.internal.TransferProgressData;

public final class TransferProgressEvent
  extends AbstractSafeParcelable
  implements DriveEvent
{
  public static final Parcelable.Creator<TransferProgressEvent> CREATOR = new zzn();
  final TransferProgressData Kn;
  final int mVersionCode;
  
  TransferProgressEvent(int paramInt, TransferProgressData paramTransferProgressData)
  {
    this.mVersionCode = paramInt;
    this.Kn = paramTransferProgressData;
  }
  
  public boolean equals(Object paramObject)
  {
    if ((paramObject == null) || (paramObject.getClass() != getClass())) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    paramObject = (TransferProgressEvent)paramObject;
    return zzz.equal(this.Kn, ((TransferProgressEvent)paramObject).Kn);
  }
  
  public int getType()
  {
    return 8;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.Kn });
  }
  
  public String toString()
  {
    return String.format("TransferProgressEvent[%s]", new Object[] { this.Kn.toString() });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzn.zza(this, paramParcel, paramInt);
  }
  
  public TransferProgressData zzbbu()
  {
    return this.Kn;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/events/TransferProgressEvent.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */