package com.google.android.gms.drive.events;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import java.util.Locale;

public final class TransferProgressOptions
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<TransferProgressOptions> CREATOR = new zzo();
  final int Ko;
  final int mVersionCode;
  
  TransferProgressOptions(int paramInt1, int paramInt2)
  {
    this.mVersionCode = paramInt1;
    this.Ko = paramInt2;
  }
  
  public boolean equals(Object paramObject)
  {
    if ((paramObject == null) || (paramObject.getClass() != getClass())) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    paramObject = (TransferProgressOptions)paramObject;
    return zzz.equal(Integer.valueOf(this.Ko), Integer.valueOf(((TransferProgressOptions)paramObject).Ko));
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { Integer.valueOf(this.Ko) });
  }
  
  public String toString()
  {
    return String.format(Locale.US, "TransferProgressOptions[type=%d]", new Object[] { Integer.valueOf(this.Ko) });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzo.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/events/TransferProgressOptions.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */