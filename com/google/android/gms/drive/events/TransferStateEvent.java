package com.google.android.gms.drive.events;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.drive.events.internal.TransferProgressData;
import java.util.List;

public final class TransferStateEvent
  extends AbstractSafeParcelable
  implements DriveEvent
{
  public static final Parcelable.Creator<TransferStateEvent> CREATOR = new zzp();
  final List<TransferProgressData> Kp;
  final String hy;
  final int mVersionCode;
  
  TransferStateEvent(int paramInt, String paramString, List<TransferProgressData> paramList)
  {
    this.mVersionCode = paramInt;
    this.hy = paramString;
    this.Kp = paramList;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if ((paramObject == null) || (paramObject.getClass() != getClass())) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramObject == this);
      paramObject = (TransferStateEvent)paramObject;
      if (!zzz.equal(this.hy, ((TransferStateEvent)paramObject).hy)) {
        break;
      }
      bool1 = bool2;
    } while (zzz.equal(this.Kp, ((TransferStateEvent)paramObject).Kp));
    return false;
  }
  
  public int getType()
  {
    return 7;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.hy, this.Kp });
  }
  
  public String toString()
  {
    return String.format("TransferStateEvent[%s]", new Object[] { TextUtils.join("','", this.Kp) });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzp.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/events/TransferStateEvent.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */