package com.google.android.gms.drive.events;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.drive.DriveSpace;
import java.util.List;
import java.util.Locale;
import java.util.Set;

public final class TransferStateOptions
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<TransferStateOptions> CREATOR = new zzr();
  final List<DriveSpace> JW;
  private final Set<DriveSpace> JX;
  final int mVersionCode;
  
  TransferStateOptions(int paramInt, List<DriveSpace> paramList) {}
  
  private TransferStateOptions(int paramInt, List<DriveSpace> paramList, Set<DriveSpace> paramSet)
  {
    this.mVersionCode = paramInt;
    this.JW = paramList;
    this.JX = paramSet;
  }
  
  public boolean equals(Object paramObject)
  {
    if ((paramObject == null) || (paramObject.getClass() != getClass())) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    paramObject = (TransferStateOptions)paramObject;
    return zzz.equal(this.JX, ((TransferStateOptions)paramObject).JX);
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.JX });
  }
  
  public String toString()
  {
    return String.format(Locale.US, "TransferStateOptions[Spaces=%s]", new Object[] { this.JW });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzr.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/events/TransferStateOptions.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */