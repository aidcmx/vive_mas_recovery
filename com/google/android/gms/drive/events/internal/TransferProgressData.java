package com.google.android.gms.drive.events.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.drive.DriveId;

public class TransferProgressData
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<TransferProgressData> CREATOR = new zzc();
  final DriveId IU;
  final int Ko;
  final long Kr;
  final long Ks;
  final int mVersionCode;
  final int zzbtt;
  
  TransferProgressData(int paramInt1, int paramInt2, DriveId paramDriveId, int paramInt3, long paramLong1, long paramLong2)
  {
    this.mVersionCode = paramInt1;
    this.Ko = paramInt2;
    this.IU = paramDriveId;
    this.zzbtt = paramInt3;
    this.Kr = paramLong1;
    this.Ks = paramLong2;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if ((paramObject == null) || (paramObject.getClass() != getClass())) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramObject == this);
      paramObject = (TransferProgressData)paramObject;
      if ((this.Ko != ((TransferProgressData)paramObject).Ko) || (!zzz.equal(this.IU, ((TransferProgressData)paramObject).IU)) || (this.zzbtt != ((TransferProgressData)paramObject).zzbtt) || (this.Kr != ((TransferProgressData)paramObject).Kr)) {
        break;
      }
      bool1 = bool2;
    } while (this.Ks == ((TransferProgressData)paramObject).Ks);
    return false;
  }
  
  public long getBytesTransferred()
  {
    return this.Kr;
  }
  
  public DriveId getDriveId()
  {
    return this.IU;
  }
  
  public int getStatus()
  {
    return this.zzbtt;
  }
  
  public long getTotalBytes()
  {
    return this.Ks;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { Integer.valueOf(this.Ko), this.IU, Integer.valueOf(this.zzbtt), Long.valueOf(this.Kr), Long.valueOf(this.Ks) });
  }
  
  public String toString()
  {
    return String.format("TransferProgressData[TransferType: %d, DriveId: %s, status: %d, bytes transferred: %d, total bytes: %d]", new Object[] { Integer.valueOf(this.Ko), this.IU, Integer.valueOf(this.zzbtt), Long.valueOf(this.Kr), Long.valueOf(this.Ks) });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzc.zza(this, paramParcel, paramInt);
  }
  
  public int zzbbv()
  {
    return this.Ko;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/events/internal/TransferProgressData.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */