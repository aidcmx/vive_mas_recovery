package com.google.android.gms.drive.events.internal;

import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.drive.events.zzh;
import com.google.android.gms.drive.events.zzj;

public class zza
  implements zzh
{
  private final zzj Kq;
  private final long Kr;
  private final long Ks;
  
  public zza(TransferProgressData paramTransferProgressData)
  {
    this.Kq = new zzb(paramTransferProgressData);
    this.Kr = paramTransferProgressData.getBytesTransferred();
    this.Ks = paramTransferProgressData.getTotalBytes();
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if ((paramObject == null) || (paramObject.getClass() != getClass())) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramObject == this);
      paramObject = (zza)paramObject;
      if ((!zzz.equal(this.Kq, ((zza)paramObject).Kq)) || (this.Kr != ((zza)paramObject).Kr)) {
        break;
      }
      bool1 = bool2;
    } while (this.Ks == ((zza)paramObject).Ks);
    return false;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { Long.valueOf(this.Ks), Long.valueOf(this.Kr), Long.valueOf(this.Ks) });
  }
  
  public String toString()
  {
    return String.format("FileTransferProgress[FileTransferState: %s, BytesTransferred: %d, TotalBytes: %d]", new Object[] { this.Kq.toString(), Long.valueOf(this.Kr), Long.valueOf(this.Ks) });
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/events/internal/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */