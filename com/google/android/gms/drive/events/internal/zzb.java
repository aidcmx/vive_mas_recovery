package com.google.android.gms.drive.events.internal;

import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.events.zzj;

public class zzb
  implements zzj
{
  private final DriveId IU;
  private final int Ko;
  private final int zzbtt;
  
  public zzb(TransferProgressData paramTransferProgressData)
  {
    this.IU = paramTransferProgressData.getDriveId();
    this.Ko = paramTransferProgressData.zzbbv();
    this.zzbtt = paramTransferProgressData.getStatus();
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if ((paramObject == null) || (paramObject.getClass() != getClass())) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramObject == this);
      paramObject = (zzb)paramObject;
      if ((!zzz.equal(this.IU, ((zzb)paramObject).IU)) || (this.Ko != ((zzb)paramObject).Ko)) {
        break;
      }
      bool1 = bool2;
    } while (this.zzbtt == ((zzb)paramObject).zzbtt);
    return false;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.IU, Integer.valueOf(this.Ko), Integer.valueOf(this.zzbtt) });
  }
  
  public String toString()
  {
    return String.format("FileTransferState[TransferType: %d, DriveId: %s, status: %d]", new Object[] { Integer.valueOf(this.Ko), this.IU, Integer.valueOf(this.zzbtt) });
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/events/internal/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */