package com.google.android.gms.drive.events.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.drive.DriveId;

public class zzc
  implements Parcelable.Creator<TransferProgressData>
{
  static void zza(TransferProgressData paramTransferProgressData, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramTransferProgressData.mVersionCode);
    zzb.zzc(paramParcel, 2, paramTransferProgressData.Ko);
    zzb.zza(paramParcel, 3, paramTransferProgressData.IU, paramInt, false);
    zzb.zzc(paramParcel, 4, paramTransferProgressData.zzbtt);
    zzb.zza(paramParcel, 5, paramTransferProgressData.Kr);
    zzb.zza(paramParcel, 6, paramTransferProgressData.Ks);
    zzb.zzaj(paramParcel, i);
  }
  
  public TransferProgressData zzev(Parcel paramParcel)
  {
    long l1 = 0L;
    int i = 0;
    int m = zza.zzcr(paramParcel);
    DriveId localDriveId = null;
    long l2 = 0L;
    int j = 0;
    int k = 0;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.zzcq(paramParcel);
      switch (zza.zzgu(n))
      {
      default: 
        zza.zzb(paramParcel, n);
        break;
      case 1: 
        k = zza.zzg(paramParcel, n);
        break;
      case 2: 
        j = zza.zzg(paramParcel, n);
        break;
      case 3: 
        localDriveId = (DriveId)zza.zza(paramParcel, n, DriveId.CREATOR);
        break;
      case 4: 
        i = zza.zzg(paramParcel, n);
        break;
      case 5: 
        l2 = zza.zzi(paramParcel, n);
        break;
      case 6: 
        l1 = zza.zzi(paramParcel, n);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza(37 + "Overread allowed size end=" + m, paramParcel);
    }
    return new TransferProgressData(k, j, localDriveId, i, l2, l1);
  }
  
  public TransferProgressData[] zzjo(int paramInt)
  {
    return new TransferProgressData[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/events/internal/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */