package com.google.android.gms.drive.events;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.drive.events.internal.TransferProgressData;

public class zzn
  implements Parcelable.Creator<TransferProgressEvent>
{
  static void zza(TransferProgressEvent paramTransferProgressEvent, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramTransferProgressEvent.mVersionCode);
    zzb.zza(paramParcel, 2, paramTransferProgressEvent.Kn, paramInt, false);
    zzb.zzaj(paramParcel, i);
  }
  
  public TransferProgressEvent zzer(Parcel paramParcel)
  {
    int j = zza.zzcr(paramParcel);
    int i = 0;
    TransferProgressData localTransferProgressData = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.zzcq(paramParcel);
      switch (zza.zzgu(k))
      {
      default: 
        zza.zzb(paramParcel, k);
        break;
      case 1: 
        i = zza.zzg(paramParcel, k);
        break;
      case 2: 
        localTransferProgressData = (TransferProgressData)zza.zza(paramParcel, k, TransferProgressData.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new TransferProgressEvent(i, localTransferProgressData);
  }
  
  public TransferProgressEvent[] zzjk(int paramInt)
  {
    return new TransferProgressEvent[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/events/zzn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */