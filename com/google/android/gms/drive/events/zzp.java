package com.google.android.gms.drive.events;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.drive.events.internal.TransferProgressData;
import java.util.ArrayList;

public class zzp
  implements Parcelable.Creator<TransferStateEvent>
{
  static void zza(TransferStateEvent paramTransferStateEvent, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramTransferStateEvent.mVersionCode);
    zzb.zza(paramParcel, 2, paramTransferStateEvent.hy, false);
    zzb.zzc(paramParcel, 3, paramTransferStateEvent.Kp, false);
    zzb.zzaj(paramParcel, paramInt);
  }
  
  public TransferStateEvent zzet(Parcel paramParcel)
  {
    ArrayList localArrayList = null;
    int j = zza.zzcr(paramParcel);
    int i = 0;
    String str = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.zzcq(paramParcel);
      switch (zza.zzgu(k))
      {
      default: 
        zza.zzb(paramParcel, k);
        break;
      case 1: 
        i = zza.zzg(paramParcel, k);
        break;
      case 2: 
        str = zza.zzq(paramParcel, k);
        break;
      case 3: 
        localArrayList = zza.zzc(paramParcel, k, TransferProgressData.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new TransferStateEvent(i, str, localArrayList);
  }
  
  public TransferStateEvent[] zzjm(int paramInt)
  {
    return new TransferStateEvent[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/events/zzp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */