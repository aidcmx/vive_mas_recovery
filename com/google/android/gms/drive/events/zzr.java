package com.google.android.gms.drive.events;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.drive.DriveSpace;
import java.util.ArrayList;

public class zzr
  implements Parcelable.Creator<TransferStateOptions>
{
  static void zza(TransferStateOptions paramTransferStateOptions, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramTransferStateOptions.mVersionCode);
    zzb.zzc(paramParcel, 2, paramTransferStateOptions.JW, false);
    zzb.zzaj(paramParcel, paramInt);
  }
  
  public TransferStateOptions zzeu(Parcel paramParcel)
  {
    int j = zza.zzcr(paramParcel);
    int i = 0;
    ArrayList localArrayList = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.zzcq(paramParcel);
      switch (zza.zzgu(k))
      {
      default: 
        zza.zzb(paramParcel, k);
        break;
      case 1: 
        i = zza.zzg(paramParcel, k);
        break;
      case 2: 
        localArrayList = zza.zzc(paramParcel, k, DriveSpace.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new TransferStateOptions(i, localArrayList);
  }
  
  public TransferStateOptions[] zzjn(int paramInt)
  {
    return new TransferStateOptions[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/events/zzr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */