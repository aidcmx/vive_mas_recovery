package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.events.ChangesAvailableOptions;
import com.google.android.gms.drive.events.TransferProgressOptions;
import com.google.android.gms.drive.events.TransferStateOptions;

public class AddEventListenerRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<AddEventListenerRequest> CREATOR = new zza();
  final int Gb;
  final DriveId IU;
  final ChangesAvailableOptions JT;
  final TransferStateOptions Kt;
  final TransferProgressOptions Ku;
  final int mVersionCode;
  
  public AddEventListenerRequest(int paramInt, DriveId paramDriveId)
  {
    this(1, (DriveId)zzaa.zzy(paramDriveId), paramInt, null, null, null);
  }
  
  AddEventListenerRequest(int paramInt1, DriveId paramDriveId, int paramInt2, ChangesAvailableOptions paramChangesAvailableOptions, TransferStateOptions paramTransferStateOptions, TransferProgressOptions paramTransferProgressOptions)
  {
    this.mVersionCode = paramInt1;
    this.IU = paramDriveId;
    this.Gb = paramInt2;
    this.JT = paramChangesAvailableOptions;
    this.Kt = paramTransferStateOptions;
    this.Ku = paramTransferProgressOptions;
  }
  
  public DriveId getDriveId()
  {
    return this.IU;
  }
  
  public int getEventType()
  {
    return this.Gb;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zza.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/internal/AddEventListenerRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */