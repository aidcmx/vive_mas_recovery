package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.util.List;

public class CancelPendingActionsRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<CancelPendingActionsRequest> CREATOR = new zze();
  final List<String> Kb;
  final int mVersionCode;
  
  CancelPendingActionsRequest(int paramInt, List<String> paramList)
  {
    this.mVersionCode = paramInt;
    this.Kb = paramList;
  }
  
  public CancelPendingActionsRequest(List<String> paramList)
  {
    this(1, paramList);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zze.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/internal/CancelPendingActionsRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */