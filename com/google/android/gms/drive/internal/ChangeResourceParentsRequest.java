package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.drive.DriveId;
import java.util.List;

public class ChangeResourceParentsRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<ChangeResourceParentsRequest> CREATOR = new zzf();
  final DriveId KA;
  final List<DriveId> KB;
  final List<DriveId> KC;
  final int mVersionCode;
  
  ChangeResourceParentsRequest(int paramInt, DriveId paramDriveId, List<DriveId> paramList1, List<DriveId> paramList2)
  {
    this.mVersionCode = paramInt;
    this.KA = paramDriveId;
    this.KB = paramList1;
    this.KC = paramList2;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzf.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/internal/ChangeResourceParentsRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */