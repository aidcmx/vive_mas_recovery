package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.drive.Contents;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;
import com.google.android.gms.drive.zzi;

public class CloseContentsAndUpdateMetadataRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<CloseContentsAndUpdateMetadataRequest> CREATOR = new zzh();
  final String Jq;
  final boolean Jr;
  final boolean Jv;
  final DriveId KE;
  final MetadataBundle KF;
  final Contents KG;
  final int KH;
  final int KI;
  final boolean KJ;
  final int mVersionCode;
  
  CloseContentsAndUpdateMetadataRequest(int paramInt1, DriveId paramDriveId, MetadataBundle paramMetadataBundle, Contents paramContents, boolean paramBoolean1, String paramString, int paramInt2, int paramInt3, boolean paramBoolean2, boolean paramBoolean3)
  {
    this.mVersionCode = paramInt1;
    this.KE = paramDriveId;
    this.KF = paramMetadataBundle;
    this.KG = paramContents;
    this.Jr = paramBoolean1;
    this.Jq = paramString;
    this.KH = paramInt2;
    this.KI = paramInt3;
    this.KJ = paramBoolean2;
    this.Jv = paramBoolean3;
  }
  
  public CloseContentsAndUpdateMetadataRequest(DriveId paramDriveId, MetadataBundle paramMetadataBundle, int paramInt, boolean paramBoolean, zzi paramzzi)
  {
    this(1, paramDriveId, paramMetadataBundle, null, paramzzi.zzbaw(), paramzzi.zzbav(), paramzzi.zzbax(), paramInt, paramBoolean, paramzzi.zzbbc());
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzh.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */