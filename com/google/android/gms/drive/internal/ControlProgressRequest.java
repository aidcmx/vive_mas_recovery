package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.drive.DriveId;

public class ControlProgressRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<ControlProgressRequest> CREATOR = new zzj();
  final DriveId IU;
  final int KL;
  final int KM;
  final ParcelableTransferPreferences KN;
  final int mVersionCode;
  
  ControlProgressRequest(int paramInt1, int paramInt2, int paramInt3, DriveId paramDriveId, ParcelableTransferPreferences paramParcelableTransferPreferences)
  {
    this.mVersionCode = paramInt1;
    this.KL = paramInt2;
    this.KM = paramInt3;
    this.IU = paramDriveId;
    this.KN = paramParcelableTransferPreferences;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzj.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/internal/ControlProgressRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */