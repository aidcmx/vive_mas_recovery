package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.drive.Contents;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;
import com.google.android.gms.drive.zzh;

public class CreateFileRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<CreateFileRequest> CREATOR = new zzn();
  final String Jq;
  final String Jt;
  final Contents KG;
  final MetadataBundle KR;
  final Integer KS;
  final DriveId KT;
  final int KU;
  final int KV;
  final boolean Ky;
  final int mVersionCode;
  
  CreateFileRequest(int paramInt1, DriveId paramDriveId, MetadataBundle paramMetadataBundle, Contents paramContents, Integer paramInteger, boolean paramBoolean, String paramString1, int paramInt2, int paramInt3, String paramString2)
  {
    if ((paramContents != null) && (paramInt3 != 0)) {
      if (paramContents.getRequestId() != paramInt3) {
        break label67;
      }
    }
    label67:
    for (boolean bool = true;; bool = false)
    {
      zzaa.zzb(bool, "inconsistent contents reference");
      if (((paramInteger != null) && (paramInteger.intValue() != 0)) || (paramContents != null) || (paramInt3 != 0)) {
        break;
      }
      throw new IllegalArgumentException("Need a valid contents");
    }
    this.mVersionCode = paramInt1;
    this.KT = ((DriveId)zzaa.zzy(paramDriveId));
    this.KR = ((MetadataBundle)zzaa.zzy(paramMetadataBundle));
    this.KG = paramContents;
    this.KS = paramInteger;
    this.Jq = paramString1;
    this.KU = paramInt2;
    this.Ky = paramBoolean;
    this.KV = paramInt3;
    this.Jt = paramString2;
  }
  
  public CreateFileRequest(DriveId paramDriveId, MetadataBundle paramMetadataBundle, int paramInt1, int paramInt2, zzh paramzzh)
  {
    this(2, paramDriveId, paramMetadataBundle, null, Integer.valueOf(paramInt2), paramzzh.zzbaw(), paramzzh.zzbav(), paramzzh.zzbax(), paramInt1, paramzzh.zzbaz());
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzn.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/internal/CreateFileRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */