package com.google.android.gms.drive.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzq;
import com.google.android.gms.common.internal.zzq.zza;

public class DriveServiceResponse
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<DriveServiceResponse> CREATOR = new zzad();
  final IBinder LX;
  final int mVersionCode;
  
  DriveServiceResponse(int paramInt, IBinder paramIBinder)
  {
    this.mVersionCode = paramInt;
    this.LX = paramIBinder;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzad.zza(this, paramParcel, paramInt);
  }
  
  public zzq zzbcd()
  {
    return zzq.zza.zzds(this.LX);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/internal/DriveServiceResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */