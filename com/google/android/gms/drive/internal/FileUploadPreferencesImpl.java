package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.drive.FileUploadPreferences;

public final class FileUploadPreferencesImpl
  extends AbstractSafeParcelable
  implements FileUploadPreferences
{
  public static final Parcelable.Creator<FileUploadPreferencesImpl> CREATOR = new zzag();
  int Mc;
  int Md;
  boolean Me;
  final int mVersionCode;
  
  FileUploadPreferencesImpl(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean)
  {
    this.mVersionCode = paramInt1;
    this.Mc = paramInt2;
    this.Md = paramInt3;
    this.Me = paramBoolean;
  }
  
  public static boolean zzkj(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return false;
    }
    return true;
  }
  
  public static boolean zzkk(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return false;
    }
    return true;
  }
  
  public int getBatteryUsagePreference()
  {
    if (!zzkk(this.Md)) {
      return 0;
    }
    return this.Md;
  }
  
  public int getNetworkTypePreference()
  {
    if (!zzkj(this.Mc)) {
      return 0;
    }
    return this.Mc;
  }
  
  public boolean isRoamingAllowed()
  {
    return this.Me;
  }
  
  public void setBatteryUsagePreference(int paramInt)
  {
    if (!zzkk(paramInt)) {
      throw new IllegalArgumentException("Invalid battery usage preference value.");
    }
    this.Md = paramInt;
  }
  
  public void setNetworkTypePreference(int paramInt)
  {
    if (!zzkj(paramInt)) {
      throw new IllegalArgumentException("Invalid data connection preference value.");
    }
    this.Mc = paramInt;
  }
  
  public void setRoamingAllowed(boolean paramBoolean)
  {
    this.Me = paramBoolean;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzag.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/internal/FileUploadPreferencesImpl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */