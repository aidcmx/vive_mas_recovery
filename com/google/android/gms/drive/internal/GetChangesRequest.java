package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.drive.ChangeSequenceNumber;
import com.google.android.gms.drive.DriveSpace;
import java.util.List;
import java.util.Set;

public class GetChangesRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<GetChangesRequest> CREATOR = new zzah();
  final List<DriveSpace> JW;
  private final Set<DriveSpace> JX;
  final ChangeSequenceNumber Mf;
  final int Mg;
  final boolean Mh;
  final int mVersionCode;
  
  private GetChangesRequest(int paramInt1, ChangeSequenceNumber paramChangeSequenceNumber, int paramInt2, List<DriveSpace> paramList, Set<DriveSpace> paramSet, boolean paramBoolean)
  {
    this.mVersionCode = paramInt1;
    this.Mf = paramChangeSequenceNumber;
    this.Mg = paramInt2;
    this.JW = paramList;
    this.JX = paramSet;
    this.Mh = paramBoolean;
  }
  
  GetChangesRequest(int paramInt1, ChangeSequenceNumber paramChangeSequenceNumber, int paramInt2, List<DriveSpace> paramList, boolean paramBoolean) {}
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzah.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/internal/GetChangesRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */