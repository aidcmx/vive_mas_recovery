package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.drive.Permission;
import java.util.List;

public class GetPermissionsResponse
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<GetPermissionsResponse> CREATOR = new zzal();
  final List<Permission> Mk;
  final int mVersionCode;
  final int zzbqp;
  
  GetPermissionsResponse(int paramInt1, List<Permission> paramList, int paramInt2)
  {
    this.mVersionCode = paramInt1;
    this.Mk = paramList;
    this.zzbqp = paramInt2;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzal.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/internal/GetPermissionsResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */