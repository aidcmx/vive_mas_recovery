package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.drive.DriveId;
import java.util.List;

public class LoadRealtimeRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<LoadRealtimeRequest> CREATOR = new zzar();
  final DriveId IU;
  final boolean Mm;
  final List<String> Mn;
  final boolean Mo;
  final DataHolder Mp;
  final String Mq;
  final int mVersionCode;
  
  LoadRealtimeRequest(int paramInt, DriveId paramDriveId, boolean paramBoolean1, List<String> paramList, boolean paramBoolean2, DataHolder paramDataHolder, String paramString)
  {
    this.mVersionCode = paramInt;
    this.IU = paramDriveId;
    this.Mm = paramBoolean1;
    this.Mn = paramList;
    this.Mo = paramBoolean2;
    this.Mp = paramDataHolder;
    this.Mq = paramString;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzar.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/internal/LoadRealtimeRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */