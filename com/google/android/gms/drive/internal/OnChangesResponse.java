package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.ChangeSequenceNumber;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.WriteAwareParcelable;
import java.util.List;

public class OnChangesResponse
  extends WriteAwareParcelable
{
  public static final Parcelable.Creator<OnChangesResponse> CREATOR = new zzas();
  final DataHolder Mr;
  final List<DriveId> Ms;
  final ChangeSequenceNumber Mt;
  final boolean Mu;
  final int mVersionCode;
  
  OnChangesResponse(int paramInt, DataHolder paramDataHolder, List<DriveId> paramList, ChangeSequenceNumber paramChangeSequenceNumber, boolean paramBoolean)
  {
    this.mVersionCode = paramInt;
    this.Mr = paramDataHolder;
    this.Ms = paramList;
    this.Mt = paramChangeSequenceNumber;
    this.Mu = paramBoolean;
  }
  
  protected void zzak(Parcel paramParcel, int paramInt)
  {
    zzas.zza(this, paramParcel, paramInt | 0x1);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/internal/OnChangesResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */