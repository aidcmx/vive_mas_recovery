package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.drive.Contents;

public class OnContentsResponse
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<OnContentsResponse> CREATOR = new zzat();
  final Contents Lw;
  final boolean Mv;
  final int mVersionCode;
  
  OnContentsResponse(int paramInt, Contents paramContents, boolean paramBoolean)
  {
    this.mVersionCode = paramInt;
    this.Lw = paramContents;
    this.Mv = paramBoolean;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzat.zza(this, paramParcel, paramInt);
  }
  
  public Contents zzbce()
  {
    return this.Lw;
  }
  
  public boolean zzbcf()
  {
    return this.Mv;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/internal/OnContentsResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */