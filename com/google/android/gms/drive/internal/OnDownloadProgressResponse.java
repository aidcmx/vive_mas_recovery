package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.drive.DriveFileRange;
import java.util.Collections;
import java.util.List;

public class OnDownloadProgressResponse
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<OnDownloadProgressResponse> CREATOR = new zzav();
  private static final List<DriveFileRange> Mx = ;
  final List<DriveFileRange> MA;
  final long My;
  final long Mz;
  final int mVersionCode;
  final int zzbtt;
  
  OnDownloadProgressResponse(int paramInt1, long paramLong1, long paramLong2, int paramInt2, List<DriveFileRange> paramList)
  {
    this.mVersionCode = paramInt1;
    this.My = paramLong1;
    this.Mz = paramLong2;
    this.zzbtt = paramInt2;
    this.MA = paramList;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzav.zza(this, paramParcel, paramInt);
  }
  
  public long zzbch()
  {
    return this.My;
  }
  
  public long zzbci()
  {
    return this.Mz;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/internal/OnDownloadProgressResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */