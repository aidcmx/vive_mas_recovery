package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.drive.DriveId;

public class OnDriveIdResponse
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<OnDriveIdResponse> CREATOR = new zzaw();
  DriveId KE;
  final int mVersionCode;
  
  OnDriveIdResponse(int paramInt, DriveId paramDriveId)
  {
    this.mVersionCode = paramInt;
    this.KE = paramDriveId;
  }
  
  public DriveId getDriveId()
  {
    return this.KE;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzaw.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/internal/OnDriveIdResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */