package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.drive.events.ChangeEvent;
import com.google.android.gms.drive.events.ChangesAvailableEvent;
import com.google.android.gms.drive.events.CompletionEvent;
import com.google.android.gms.drive.events.DriveEvent;
import com.google.android.gms.drive.events.QueryResultEventParcelable;
import com.google.android.gms.drive.events.TransferProgressEvent;
import com.google.android.gms.drive.events.TransferStateEvent;

public class OnEventResponse
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<OnEventResponse> CREATOR = new zzax();
  final int Gb;
  final ChangeEvent MB;
  final CompletionEvent MC;
  final QueryResultEventParcelable MD;
  final ChangesAvailableEvent ME;
  final TransferStateEvent MF;
  final TransferProgressEvent MG;
  final int mVersionCode;
  
  OnEventResponse(int paramInt1, int paramInt2, ChangeEvent paramChangeEvent, CompletionEvent paramCompletionEvent, QueryResultEventParcelable paramQueryResultEventParcelable, ChangesAvailableEvent paramChangesAvailableEvent, TransferStateEvent paramTransferStateEvent, TransferProgressEvent paramTransferProgressEvent)
  {
    this.mVersionCode = paramInt1;
    this.Gb = paramInt2;
    this.MB = paramChangeEvent;
    this.MC = paramCompletionEvent;
    this.MD = paramQueryResultEventParcelable;
    this.ME = paramChangesAvailableEvent;
    this.MF = paramTransferStateEvent;
    this.MG = paramTransferProgressEvent;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzax.zza(this, paramParcel, paramInt);
  }
  
  public DriveEvent zzbcj()
  {
    switch (this.Gb)
    {
    case 5: 
    case 6: 
    default: 
      int i = this.Gb;
      throw new IllegalStateException(33 + "Unexpected event type " + i);
    case 1: 
      return this.MB;
    case 2: 
      return this.MC;
    case 3: 
      return this.MD;
    case 4: 
      return this.ME;
    case 7: 
      return this.MF;
    }
    return this.MG;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/internal/OnEventResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */