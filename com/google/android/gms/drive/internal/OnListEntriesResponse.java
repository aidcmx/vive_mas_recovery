package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.WriteAwareParcelable;

public class OnListEntriesResponse
  extends WriteAwareParcelable
{
  public static final Parcelable.Creator<OnListEntriesResponse> CREATOR = new zzaz();
  final boolean Le;
  final DataHolder MI;
  final int mVersionCode;
  
  OnListEntriesResponse(int paramInt, DataHolder paramDataHolder, boolean paramBoolean)
  {
    this.mVersionCode = paramInt;
    this.MI = paramDataHolder;
    this.Le = paramBoolean;
  }
  
  protected void zzak(Parcel paramParcel, int paramInt)
  {
    zzaz.zza(this, paramParcel, paramInt);
  }
  
  public DataHolder zzbck()
  {
    return this.MI;
  }
  
  public boolean zzbcl()
  {
    return this.Le;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/internal/OnListEntriesResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */