package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.WriteAwareParcelable;

public class OnListParentsResponse
  extends WriteAwareParcelable
{
  public static final Parcelable.Creator<OnListParentsResponse> CREATOR = new zzba();
  final DataHolder MJ;
  final int mVersionCode;
  
  OnListParentsResponse(int paramInt, DataHolder paramDataHolder)
  {
    this.mVersionCode = paramInt;
    this.MJ = paramDataHolder;
  }
  
  protected void zzak(Parcel paramParcel, int paramInt)
  {
    zzba.zza(this, paramParcel, paramInt);
  }
  
  public DataHolder zzbcm()
  {
    return this.MJ;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/internal/OnListParentsResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */