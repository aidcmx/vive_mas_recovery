package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public class OnLoadRealtimeResponse
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<OnLoadRealtimeResponse> CREATOR = new zzbd();
  final int mVersionCode;
  final boolean zzaoz;
  
  OnLoadRealtimeResponse(int paramInt, boolean paramBoolean)
  {
    this.mVersionCode = paramInt;
    this.zzaoz = paramBoolean;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzbd.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/internal/OnLoadRealtimeResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */