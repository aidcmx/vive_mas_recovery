package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public class OnPinnedDownloadPreferencesResponse
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<OnPinnedDownloadPreferencesResponse> CREATOR = new zzbc();
  final ParcelableTransferPreferences MK;
  final int mVersionCode;
  
  OnPinnedDownloadPreferencesResponse(int paramInt, ParcelableTransferPreferences paramParcelableTransferPreferences)
  {
    this.mVersionCode = paramInt;
    this.MK = paramParcelableTransferPreferences;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzbc.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/internal/OnPinnedDownloadPreferencesResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */