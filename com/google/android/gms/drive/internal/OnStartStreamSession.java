package com.google.android.gms.drive.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public class OnStartStreamSession
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<OnStartStreamSession> CREATOR = new zzbf();
  final ParcelFileDescriptor ML;
  final IBinder MM;
  final int mVersionCode;
  final String zzavu;
  
  OnStartStreamSession(int paramInt, ParcelFileDescriptor paramParcelFileDescriptor, IBinder paramIBinder, String paramString)
  {
    this.mVersionCode = paramInt;
    this.ML = paramParcelFileDescriptor;
    this.MM = paramIBinder;
    this.zzavu = paramString;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzbf.zza(this, paramParcel, paramInt | 0x1);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/internal/OnStartStreamSession.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */