package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public class OnSyncMoreResponse
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<OnSyncMoreResponse> CREATOR = new zzbg();
  final boolean Le;
  final int mVersionCode;
  
  OnSyncMoreResponse(int paramInt, boolean paramBoolean)
  {
    this.mVersionCode = paramInt;
    this.Le = paramBoolean;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzbg.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/internal/OnSyncMoreResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */