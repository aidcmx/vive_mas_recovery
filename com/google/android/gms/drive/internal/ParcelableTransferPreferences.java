package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public class ParcelableTransferPreferences
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<ParcelableTransferPreferences> CREATOR = new zzbk();
  final boolean MQ;
  final int Mc;
  final int Md;
  final int mVersionCode;
  
  ParcelableTransferPreferences(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean)
  {
    this.mVersionCode = paramInt1;
    this.Mc = paramInt2;
    this.Md = paramInt3;
    this.MQ = paramBoolean;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzbk.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/internal/ParcelableTransferPreferences.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */