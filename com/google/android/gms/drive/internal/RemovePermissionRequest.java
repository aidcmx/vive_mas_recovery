package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.drive.DriveId;

public class RemovePermissionRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<RemovePermissionRequest> CREATOR = new zzbn();
  final DriveId IU;
  final String JF;
  final String Jq;
  final boolean Ky;
  final int mVersionCode;
  
  RemovePermissionRequest(int paramInt, DriveId paramDriveId, String paramString1, boolean paramBoolean, String paramString2)
  {
    this.mVersionCode = paramInt;
    this.IU = paramDriveId;
    this.JF = paramString1;
    this.Ky = paramBoolean;
    this.Jq = paramString2;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzbn.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/internal/RemovePermissionRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */