package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.util.List;

public class StringListResponse
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<StringListResponse> CREATOR = new zzbt();
  private final List<String> MT;
  private final int mVersionCode;
  
  StringListResponse(int paramInt, List<String> paramList)
  {
    this.mVersionCode = paramInt;
    this.MT = paramList;
  }
  
  public int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzbt.zza(this, paramParcel, paramInt);
  }
  
  public List<String> zzbco()
  {
    return this.MT;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/internal/StringListResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */