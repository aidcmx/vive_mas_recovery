package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.drive.DriveId;

public class TrashResourceRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<TrashResourceRequest> CREATOR = new zzbu();
  final DriveId KE;
  final int mVersionCode;
  
  TrashResourceRequest(int paramInt, DriveId paramDriveId)
  {
    this.mVersionCode = paramInt;
    this.KE = paramDriveId;
  }
  
  public TrashResourceRequest(DriveId paramDriveId)
  {
    this(1, paramDriveId);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzbu.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/internal/TrashResourceRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */