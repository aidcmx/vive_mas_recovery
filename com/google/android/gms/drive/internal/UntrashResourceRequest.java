package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.drive.DriveId;

public class UntrashResourceRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<UntrashResourceRequest> CREATOR = new zzbw();
  final DriveId KE;
  final int mVersionCode;
  
  UntrashResourceRequest(int paramInt, DriveId paramDriveId)
  {
    this.mVersionCode = paramInt;
    this.KE = paramDriveId;
  }
  
  public UntrashResourceRequest(DriveId paramDriveId)
  {
    this(1, paramDriveId);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzbw.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/internal/UntrashResourceRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */