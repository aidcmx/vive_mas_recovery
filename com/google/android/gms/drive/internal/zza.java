package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.events.ChangesAvailableOptions;
import com.google.android.gms.drive.events.TransferProgressOptions;
import com.google.android.gms.drive.events.TransferStateOptions;

public class zza
  implements Parcelable.Creator<AddEventListenerRequest>
{
  static void zza(AddEventListenerRequest paramAddEventListenerRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramAddEventListenerRequest.mVersionCode);
    zzb.zza(paramParcel, 2, paramAddEventListenerRequest.IU, paramInt, false);
    zzb.zzc(paramParcel, 3, paramAddEventListenerRequest.Gb);
    zzb.zza(paramParcel, 4, paramAddEventListenerRequest.JT, paramInt, false);
    zzb.zza(paramParcel, 5, paramAddEventListenerRequest.Kt, paramInt, false);
    zzb.zza(paramParcel, 6, paramAddEventListenerRequest.Ku, paramInt, false);
    zzb.zzaj(paramParcel, i);
  }
  
  public AddEventListenerRequest zzew(Parcel paramParcel)
  {
    int i = 0;
    TransferProgressOptions localTransferProgressOptions = null;
    int k = com.google.android.gms.common.internal.safeparcel.zza.zzcr(paramParcel);
    TransferStateOptions localTransferStateOptions = null;
    ChangesAvailableOptions localChangesAvailableOptions = null;
    DriveId localDriveId = null;
    int j = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = com.google.android.gms.common.internal.safeparcel.zza.zzcq(paramParcel);
      switch (com.google.android.gms.common.internal.safeparcel.zza.zzgu(m))
      {
      default: 
        com.google.android.gms.common.internal.safeparcel.zza.zzb(paramParcel, m);
        break;
      case 1: 
        j = com.google.android.gms.common.internal.safeparcel.zza.zzg(paramParcel, m);
        break;
      case 2: 
        localDriveId = (DriveId)com.google.android.gms.common.internal.safeparcel.zza.zza(paramParcel, m, DriveId.CREATOR);
        break;
      case 3: 
        i = com.google.android.gms.common.internal.safeparcel.zza.zzg(paramParcel, m);
        break;
      case 4: 
        localChangesAvailableOptions = (ChangesAvailableOptions)com.google.android.gms.common.internal.safeparcel.zza.zza(paramParcel, m, ChangesAvailableOptions.CREATOR);
        break;
      case 5: 
        localTransferStateOptions = (TransferStateOptions)com.google.android.gms.common.internal.safeparcel.zza.zza(paramParcel, m, TransferStateOptions.CREATOR);
        break;
      case 6: 
        localTransferProgressOptions = (TransferProgressOptions)com.google.android.gms.common.internal.safeparcel.zza.zza(paramParcel, m, TransferProgressOptions.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza(37 + "Overread allowed size end=" + k, paramParcel);
    }
    return new AddEventListenerRequest(j, localDriveId, i, localChangesAvailableOptions, localTransferStateOptions, localTransferProgressOptions);
  }
  
  public AddEventListenerRequest[] zzjp(int paramInt)
  {
    return new AddEventListenerRequest[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/internal/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */