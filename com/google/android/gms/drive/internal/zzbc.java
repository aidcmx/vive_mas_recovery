package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzbc
  implements Parcelable.Creator<OnPinnedDownloadPreferencesResponse>
{
  static void zza(OnPinnedDownloadPreferencesResponse paramOnPinnedDownloadPreferencesResponse, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramOnPinnedDownloadPreferencesResponse.mVersionCode);
    zzb.zza(paramParcel, 2, paramOnPinnedDownloadPreferencesResponse.MK, paramInt, false);
    zzb.zzaj(paramParcel, i);
  }
  
  public OnPinnedDownloadPreferencesResponse zzgf(Parcel paramParcel)
  {
    int j = zza.zzcr(paramParcel);
    int i = 0;
    ParcelableTransferPreferences localParcelableTransferPreferences = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.zzcq(paramParcel);
      switch (zza.zzgu(k))
      {
      default: 
        zza.zzb(paramParcel, k);
        break;
      case 1: 
        i = zza.zzg(paramParcel, k);
        break;
      case 2: 
        localParcelableTransferPreferences = (ParcelableTransferPreferences)zza.zza(paramParcel, k, ParcelableTransferPreferences.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new OnPinnedDownloadPreferencesResponse(i, localParcelableTransferPreferences);
  }
  
  public OnPinnedDownloadPreferencesResponse[] zzld(int paramInt)
  {
    return new OnPinnedDownloadPreferencesResponse[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/internal/zzbc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */