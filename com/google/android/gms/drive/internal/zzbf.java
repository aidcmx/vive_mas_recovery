package com.google.android.gms.drive.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzbf
  implements Parcelable.Creator<OnStartStreamSession>
{
  static void zza(OnStartStreamSession paramOnStartStreamSession, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramOnStartStreamSession.mVersionCode);
    zzb.zza(paramParcel, 2, paramOnStartStreamSession.ML, paramInt, false);
    zzb.zza(paramParcel, 3, paramOnStartStreamSession.MM, false);
    zzb.zza(paramParcel, 4, paramOnStartStreamSession.zzavu, false);
    zzb.zzaj(paramParcel, i);
  }
  
  public OnStartStreamSession zzgi(Parcel paramParcel)
  {
    String str = null;
    int j = zza.zzcr(paramParcel);
    int i = 0;
    Object localObject2 = null;
    Object localObject1 = null;
    if (paramParcel.dataPosition() < j)
    {
      int k = zza.zzcq(paramParcel);
      Object localObject3;
      switch (zza.zzgu(k))
      {
      default: 
        zza.zzb(paramParcel, k);
        localObject3 = localObject2;
        localObject2 = localObject1;
        localObject1 = localObject3;
      }
      for (;;)
      {
        localObject3 = localObject2;
        localObject2 = localObject1;
        localObject1 = localObject3;
        break;
        i = zza.zzg(paramParcel, k);
        localObject3 = localObject1;
        localObject1 = localObject2;
        localObject2 = localObject3;
        continue;
        localObject3 = (ParcelFileDescriptor)zza.zza(paramParcel, k, ParcelFileDescriptor.CREATOR);
        localObject1 = localObject2;
        localObject2 = localObject3;
        continue;
        localObject3 = zza.zzr(paramParcel, k);
        localObject2 = localObject1;
        localObject1 = localObject3;
        continue;
        str = zza.zzq(paramParcel, k);
        localObject3 = localObject1;
        localObject1 = localObject2;
        localObject2 = localObject3;
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new OnStartStreamSession(i, (ParcelFileDescriptor)localObject1, (IBinder)localObject2, str);
  }
  
  public OnStartStreamSession[] zzlg(int paramInt)
  {
    return new OnStartStreamSession[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/internal/zzbf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */