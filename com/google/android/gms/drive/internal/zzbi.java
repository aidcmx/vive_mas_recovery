package com.google.android.gms.drive.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.drive.DriveApi.DriveContentsResult;
import com.google.android.gms.drive.DriveFile.DownloadProgressListener;
import com.google.android.gms.internal.zzqo.zzb;

class zzbi
  extends zzd
{
  private final zzqo.zzb<DriveApi.DriveContentsResult> EW;
  private final DriveFile.DownloadProgressListener MO;
  
  zzbi(zzqo.zzb<DriveApi.DriveContentsResult> paramzzb, DriveFile.DownloadProgressListener paramDownloadProgressListener)
  {
    this.EW = paramzzb;
    this.MO = paramDownloadProgressListener;
  }
  
  public void onError(Status paramStatus)
    throws RemoteException
  {
    this.EW.setResult(new zzs.zzb(paramStatus, null));
  }
  
  public void zza(OnContentsResponse paramOnContentsResponse)
    throws RemoteException
  {
    if (paramOnContentsResponse.zzbcf()) {}
    for (Status localStatus = new Status(-1);; localStatus = Status.xZ)
    {
      this.EW.setResult(new zzs.zzb(localStatus, new zzv(paramOnContentsResponse.zzbce())));
      return;
    }
  }
  
  public void zza(OnDownloadProgressResponse paramOnDownloadProgressResponse)
    throws RemoteException
  {
    if (this.MO != null) {
      this.MO.onProgress(paramOnDownloadProgressResponse.zzbch(), paramOnDownloadProgressResponse.zzbci());
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/internal/zzbi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */