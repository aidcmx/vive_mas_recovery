package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzbk
  implements Parcelable.Creator<ParcelableTransferPreferences>
{
  static void zza(ParcelableTransferPreferences paramParcelableTransferPreferences, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramParcelableTransferPreferences.mVersionCode);
    zzb.zzc(paramParcel, 2, paramParcelableTransferPreferences.Mc);
    zzb.zzc(paramParcel, 3, paramParcelableTransferPreferences.Md);
    zzb.zza(paramParcel, 4, paramParcelableTransferPreferences.MQ);
    zzb.zzaj(paramParcel, paramInt);
  }
  
  public ParcelableTransferPreferences zzgm(Parcel paramParcel)
  {
    boolean bool = false;
    int m = zza.zzcr(paramParcel);
    int k = 0;
    int j = 0;
    int i = 0;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.zzcq(paramParcel);
      switch (zza.zzgu(n))
      {
      default: 
        zza.zzb(paramParcel, n);
        break;
      case 1: 
        i = zza.zzg(paramParcel, n);
        break;
      case 2: 
        j = zza.zzg(paramParcel, n);
        break;
      case 3: 
        k = zza.zzg(paramParcel, n);
        break;
      case 4: 
        bool = zza.zzc(paramParcel, n);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza(37 + "Overread allowed size end=" + m, paramParcel);
    }
    return new ParcelableTransferPreferences(i, j, k, bool);
  }
  
  public ParcelableTransferPreferences[] zzlk(int paramInt)
  {
    return new ParcelableTransferPreferences[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/internal/zzbk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */