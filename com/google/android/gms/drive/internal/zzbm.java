package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.events.TransferProgressOptions;

public class zzbm
  implements Parcelable.Creator<RemoveEventListenerRequest>
{
  static void zza(RemoveEventListenerRequest paramRemoveEventListenerRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramRemoveEventListenerRequest.mVersionCode);
    zzb.zza(paramParcel, 2, paramRemoveEventListenerRequest.IU, paramInt, false);
    zzb.zzc(paramParcel, 3, paramRemoveEventListenerRequest.Gb);
    zzb.zza(paramParcel, 4, paramRemoveEventListenerRequest.Ku, paramInt, false);
    zzb.zzaj(paramParcel, i);
  }
  
  public RemoveEventListenerRequest zzgo(Parcel paramParcel)
  {
    TransferProgressOptions localTransferProgressOptions = null;
    int j = 0;
    int m = zza.zzcr(paramParcel);
    DriveId localDriveId = null;
    int i = 0;
    if (paramParcel.dataPosition() < m)
    {
      int k = zza.zzcq(paramParcel);
      switch (zza.zzgu(k))
      {
      default: 
        zza.zzb(paramParcel, k);
        k = j;
        j = i;
        i = k;
      }
      for (;;)
      {
        k = j;
        j = i;
        i = k;
        break;
        k = zza.zzg(paramParcel, k);
        i = j;
        j = k;
        continue;
        localDriveId = (DriveId)zza.zza(paramParcel, k, DriveId.CREATOR);
        k = i;
        i = j;
        j = k;
        continue;
        k = zza.zzg(paramParcel, k);
        j = i;
        i = k;
        continue;
        localTransferProgressOptions = (TransferProgressOptions)zza.zza(paramParcel, k, TransferProgressOptions.CREATOR);
        k = i;
        i = j;
        j = k;
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza(37 + "Overread allowed size end=" + m, paramParcel);
    }
    return new RemoveEventListenerRequest(i, localDriveId, j, localTransferProgressOptions);
  }
  
  public RemoveEventListenerRequest[] zzlm(int paramInt)
  {
    return new RemoveEventListenerRequest[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/internal/zzbm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */