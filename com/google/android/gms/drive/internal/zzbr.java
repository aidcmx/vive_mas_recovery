package com.google.android.gms.drive.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.internal.zzqo.zzb;

public class zzbr
  extends zzd
{
  private final zzqo.zzb<Status> EW;
  
  public zzbr(zzqo.zzb<Status> paramzzb)
  {
    this.EW = paramzzb;
  }
  
  public void onError(Status paramStatus)
    throws RemoteException
  {
    this.EW.setResult(paramStatus);
  }
  
  public void onSuccess()
    throws RemoteException
  {
    this.EW.setResult(Status.xZ);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/internal/zzbr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */