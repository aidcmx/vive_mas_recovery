package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.drive.DriveId;

public class zzbu
  implements Parcelable.Creator<TrashResourceRequest>
{
  static void zza(TrashResourceRequest paramTrashResourceRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramTrashResourceRequest.mVersionCode);
    zzb.zza(paramParcel, 2, paramTrashResourceRequest.KE, paramInt, false);
    zzb.zzaj(paramParcel, i);
  }
  
  public TrashResourceRequest zzgv(Parcel paramParcel)
  {
    int j = zza.zzcr(paramParcel);
    int i = 0;
    DriveId localDriveId = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.zzcq(paramParcel);
      switch (zza.zzgu(k))
      {
      default: 
        zza.zzb(paramParcel, k);
        break;
      case 1: 
        i = zza.zzg(paramParcel, k);
        break;
      case 2: 
        localDriveId = (DriveId)zza.zza(paramParcel, k, DriveId.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new TrashResourceRequest(i, localDriveId);
  }
  
  public TrashResourceRequest[] zzlt(int paramInt)
  {
    return new TrashResourceRequest[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/internal/zzbu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */