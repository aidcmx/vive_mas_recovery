package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.drive.DriveId;

public class zzby
  implements Parcelable.Creator<UpdatePermissionRequest>
{
  static void zza(UpdatePermissionRequest paramUpdatePermissionRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramUpdatePermissionRequest.mVersionCode);
    zzb.zza(paramParcel, 2, paramUpdatePermissionRequest.IU, paramInt, false);
    zzb.zza(paramParcel, 3, paramUpdatePermissionRequest.JF, false);
    zzb.zzc(paramParcel, 4, paramUpdatePermissionRequest.MU);
    zzb.zza(paramParcel, 5, paramUpdatePermissionRequest.Ky);
    zzb.zza(paramParcel, 6, paramUpdatePermissionRequest.Jq, false);
    zzb.zzaj(paramParcel, i);
  }
  
  public UpdatePermissionRequest zzgz(Parcel paramParcel)
  {
    String str1 = null;
    boolean bool = false;
    int k = zza.zzcr(paramParcel);
    int i = 0;
    String str2 = null;
    DriveId localDriveId = null;
    int j = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.zzcq(paramParcel);
      switch (zza.zzgu(m))
      {
      default: 
        zza.zzb(paramParcel, m);
        break;
      case 1: 
        j = zza.zzg(paramParcel, m);
        break;
      case 2: 
        localDriveId = (DriveId)zza.zza(paramParcel, m, DriveId.CREATOR);
        break;
      case 3: 
        str2 = zza.zzq(paramParcel, m);
        break;
      case 4: 
        i = zza.zzg(paramParcel, m);
        break;
      case 5: 
        bool = zza.zzc(paramParcel, m);
        break;
      case 6: 
        str1 = zza.zzq(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza(37 + "Overread allowed size end=" + k, paramParcel);
    }
    return new UpdatePermissionRequest(j, localDriveId, str2, i, bool, str1);
  }
  
  public UpdatePermissionRequest[] zzlx(int paramInt)
  {
    return new UpdatePermissionRequest[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/internal/zzby.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */