package com.google.android.gms.drive.internal;

import android.content.IntentSender;
import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;

public class zzl
{
  private String JB;
  private DriveId JE;
  protected MetadataChangeSet KO;
  private Integer KP;
  private final int KQ;
  
  public zzl(int paramInt)
  {
    this.KQ = paramInt;
  }
  
  public IntentSender build(GoogleApiClient paramGoogleApiClient)
  {
    zzaa.zzb(this.KO, "Must provide initial metadata to CreateFileActivityBuilder.");
    zzaa.zza(paramGoogleApiClient.isConnected(), "Client must be connected");
    paramGoogleApiClient = (zzu)paramGoogleApiClient.zza(Drive.hg);
    this.KO.zzbbf().setContext(paramGoogleApiClient.getContext());
    if (this.KP == null) {}
    for (int i = 0;; i = this.KP.intValue()) {
      try
      {
        paramGoogleApiClient = paramGoogleApiClient.zzbby().zza(new CreateFileIntentSenderRequest(this.KO.zzbbf(), i, this.JB, this.JE, this.KQ));
        return paramGoogleApiClient;
      }
      catch (RemoteException paramGoogleApiClient)
      {
        throw new RuntimeException("Unable to connect Drive Play Service", paramGoogleApiClient);
      }
    }
  }
  
  public void zza(DriveId paramDriveId)
  {
    this.JE = ((DriveId)zzaa.zzy(paramDriveId));
  }
  
  public void zza(MetadataChangeSet paramMetadataChangeSet)
  {
    this.KO = ((MetadataChangeSet)zzaa.zzy(paramMetadataChangeSet));
  }
  
  public void zzis(String paramString)
  {
    this.JB = ((String)zzaa.zzy(paramString));
  }
  
  public void zzjz(int paramInt)
  {
    this.KP = Integer.valueOf(paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/internal/zzl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */