package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.drive.Contents;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;

public class zzn
  implements Parcelable.Creator<CreateFileRequest>
{
  static void zza(CreateFileRequest paramCreateFileRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramCreateFileRequest.mVersionCode);
    zzb.zza(paramParcel, 2, paramCreateFileRequest.KT, paramInt, false);
    zzb.zza(paramParcel, 3, paramCreateFileRequest.KR, paramInt, false);
    zzb.zza(paramParcel, 4, paramCreateFileRequest.KG, paramInt, false);
    zzb.zza(paramParcel, 5, paramCreateFileRequest.KS, false);
    zzb.zza(paramParcel, 6, paramCreateFileRequest.Ky);
    zzb.zza(paramParcel, 7, paramCreateFileRequest.Jq, false);
    zzb.zzc(paramParcel, 8, paramCreateFileRequest.KU);
    zzb.zzc(paramParcel, 9, paramCreateFileRequest.KV);
    zzb.zza(paramParcel, 10, paramCreateFileRequest.Jt, false);
    zzb.zzaj(paramParcel, i);
  }
  
  public CreateFileRequest zzfh(Parcel paramParcel)
  {
    int i = 0;
    String str1 = null;
    int m = zza.zzcr(paramParcel);
    int j = 0;
    String str2 = null;
    boolean bool = false;
    Integer localInteger = null;
    Contents localContents = null;
    MetadataBundle localMetadataBundle = null;
    DriveId localDriveId = null;
    int k = 0;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.zzcq(paramParcel);
      switch (zza.zzgu(n))
      {
      default: 
        zza.zzb(paramParcel, n);
        break;
      case 1: 
        k = zza.zzg(paramParcel, n);
        break;
      case 2: 
        localDriveId = (DriveId)zza.zza(paramParcel, n, DriveId.CREATOR);
        break;
      case 3: 
        localMetadataBundle = (MetadataBundle)zza.zza(paramParcel, n, MetadataBundle.CREATOR);
        break;
      case 4: 
        localContents = (Contents)zza.zza(paramParcel, n, Contents.CREATOR);
        break;
      case 5: 
        localInteger = zza.zzh(paramParcel, n);
        break;
      case 6: 
        bool = zza.zzc(paramParcel, n);
        break;
      case 7: 
        str2 = zza.zzq(paramParcel, n);
        break;
      case 8: 
        j = zza.zzg(paramParcel, n);
        break;
      case 9: 
        i = zza.zzg(paramParcel, n);
        break;
      case 10: 
        str1 = zza.zzq(paramParcel, n);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza(37 + "Overread allowed size end=" + m, paramParcel);
    }
    return new CreateFileRequest(k, localDriveId, localMetadataBundle, localContents, localInteger, bool, str2, j, i, str1);
  }
  
  public CreateFileRequest[] zzkb(int paramInt)
  {
    return new CreateFileRequest[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/internal/zzn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */