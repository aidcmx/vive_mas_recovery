package com.google.android.gms.drive.internal;

import com.google.android.gms.drive.Metadata;
import com.google.android.gms.drive.metadata.MetadataField;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;

public final class zzp
  extends Metadata
{
  private final MetadataBundle KW;
  
  public zzp(MetadataBundle paramMetadataBundle)
  {
    this.KW = paramMetadataBundle;
  }
  
  public boolean isDataValid()
  {
    return this.KW != null;
  }
  
  public String toString()
  {
    String str = String.valueOf(this.KW);
    return String.valueOf(str).length() + 17 + "Metadata [mImpl=" + str + "]";
  }
  
  public <T> T zza(MetadataField<T> paramMetadataField)
  {
    return (T)this.KW.zza(paramMetadataField);
  }
  
  public Metadata zzbbe()
  {
    return new zzp(this.KW.zzbcz());
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/internal/zzp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */