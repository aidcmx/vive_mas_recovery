package com.google.android.gms.drive.internal;

import android.annotation.SuppressLint;
import android.os.RemoteException;
import com.google.android.gms.common.api.BooleanResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Releasable;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.drive.CreateFileActivityBuilder;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.DriveApi.DriveContentsResult;
import com.google.android.gms.drive.DriveApi.DriveIdResult;
import com.google.android.gms.drive.DriveApi.MetadataBufferResult;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.MetadataBuffer;
import com.google.android.gms.drive.OpenFileActivityBuilder;
import com.google.android.gms.drive.query.Query;
import com.google.android.gms.internal.zzqo.zzb;
import java.util.List;

public class zzs
  implements DriveApi
{
  public PendingResult<Status> cancelPendingActions(GoogleApiClient paramGoogleApiClient, List<String> paramList)
  {
    return ((zzu)paramGoogleApiClient.zza(Drive.hg)).cancelPendingActions(paramGoogleApiClient, paramList);
  }
  
  public PendingResult<DriveApi.DriveIdResult> fetchDriveId(GoogleApiClient paramGoogleApiClient, final String paramString)
  {
    paramGoogleApiClient.zza(new zzf(paramGoogleApiClient)
    {
      protected void zza(zzu paramAnonymouszzu)
        throws RemoteException
      {
        paramAnonymouszzu.zzbby().zza(new GetMetadataRequest(DriveId.zzin(paramString), false), new zzs.zzd(this));
      }
    });
  }
  
  public DriveFolder getAppFolder(GoogleApiClient paramGoogleApiClient)
  {
    paramGoogleApiClient = (zzu)paramGoogleApiClient.zza(Drive.hg);
    if (!paramGoogleApiClient.zzbcb()) {
      throw new IllegalStateException("Client is not yet connected");
    }
    paramGoogleApiClient = paramGoogleApiClient.zzbca();
    if (paramGoogleApiClient != null) {
      return new zzy(paramGoogleApiClient);
    }
    return null;
  }
  
  public DriveFile getFile(GoogleApiClient paramGoogleApiClient, DriveId paramDriveId)
  {
    if (paramDriveId == null) {
      throw new IllegalArgumentException("Id must be provided.");
    }
    if (!paramGoogleApiClient.isConnected()) {
      throw new IllegalStateException("Client must be connected");
    }
    return new zzw(paramDriveId);
  }
  
  public DriveFolder getFolder(GoogleApiClient paramGoogleApiClient, DriveId paramDriveId)
  {
    if (paramDriveId == null) {
      throw new IllegalArgumentException("Id must be provided.");
    }
    if (!paramGoogleApiClient.isConnected()) {
      throw new IllegalStateException("Client must be connected");
    }
    return new zzy(paramDriveId);
  }
  
  public DriveFolder getRootFolder(GoogleApiClient paramGoogleApiClient)
  {
    paramGoogleApiClient = (zzu)paramGoogleApiClient.zza(Drive.hg);
    if (!paramGoogleApiClient.zzbcb()) {
      throw new IllegalStateException("Client is not yet connected");
    }
    paramGoogleApiClient = paramGoogleApiClient.zzbbz();
    if (paramGoogleApiClient != null) {
      return new zzy(paramGoogleApiClient);
    }
    return null;
  }
  
  public PendingResult<BooleanResult> isAutobackupEnabled(GoogleApiClient paramGoogleApiClient)
  {
    paramGoogleApiClient.zza(new zzt(paramGoogleApiClient)
    {
      protected void zza(zzu paramAnonymouszzu)
        throws RemoteException
      {
        paramAnonymouszzu.zzbby().zze(new zzd()
        {
          public void zzbw(boolean paramAnonymous2Boolean)
          {
            jdField_this.zzc(new BooleanResult(Status.xZ, paramAnonymous2Boolean));
          }
        });
      }
      
      protected BooleanResult zzam(Status paramAnonymousStatus)
      {
        return new BooleanResult(paramAnonymousStatus, false);
      }
    });
  }
  
  public CreateFileActivityBuilder newCreateFileActivityBuilder()
  {
    return new CreateFileActivityBuilder();
  }
  
  public PendingResult<DriveApi.DriveContentsResult> newDriveContents(GoogleApiClient paramGoogleApiClient)
  {
    return zzb(paramGoogleApiClient, 536870912);
  }
  
  public OpenFileActivityBuilder newOpenFileActivityBuilder()
  {
    return new OpenFileActivityBuilder();
  }
  
  public PendingResult<DriveApi.MetadataBufferResult> query(GoogleApiClient paramGoogleApiClient, final Query paramQuery)
  {
    if (paramQuery == null) {
      throw new IllegalArgumentException("Query must be provided.");
    }
    paramGoogleApiClient.zza(new zzh(paramGoogleApiClient)
    {
      protected void zza(zzu paramAnonymouszzu)
        throws RemoteException
      {
        paramAnonymouszzu.zzbby().zza(new QueryRequest(paramQuery), new zzs.zzi(this));
      }
    });
  }
  
  public PendingResult<Status> requestSync(GoogleApiClient paramGoogleApiClient)
  {
    paramGoogleApiClient.zzb(new zzt.zza(paramGoogleApiClient)
    {
      protected void zza(zzu paramAnonymouszzu)
        throws RemoteException
      {
        paramAnonymouszzu.zzbby().zza(new zzbr(this));
      }
    });
  }
  
  public PendingResult<DriveApi.DriveContentsResult> zzb(GoogleApiClient paramGoogleApiClient, final int paramInt)
  {
    paramGoogleApiClient.zza(new zzc(paramGoogleApiClient)
    {
      protected void zza(zzu paramAnonymouszzu)
        throws RemoteException
      {
        paramAnonymouszzu.zzbby().zza(new CreateContentsRequest(paramInt), new zzs.zza(this));
      }
    });
  }
  
  private static class zza
    extends zzd
  {
    private final zzqo.zzb<DriveApi.DriveContentsResult> EW;
    
    public zza(zzqo.zzb<DriveApi.DriveContentsResult> paramzzb)
    {
      this.EW = paramzzb;
    }
    
    public void onError(Status paramStatus)
      throws RemoteException
    {
      this.EW.setResult(new zzs.zzb(paramStatus, null));
    }
    
    public void zza(OnContentsResponse paramOnContentsResponse)
      throws RemoteException
    {
      this.EW.setResult(new zzs.zzb(Status.xZ, new zzv(paramOnContentsResponse.zzbce())));
    }
  }
  
  static class zzb
    implements Releasable, DriveApi.DriveContentsResult
  {
    private final DriveContents IX;
    private final Status hv;
    
    public zzb(Status paramStatus, DriveContents paramDriveContents)
    {
      this.hv = paramStatus;
      this.IX = paramDriveContents;
    }
    
    public DriveContents getDriveContents()
    {
      return this.IX;
    }
    
    public Status getStatus()
    {
      return this.hv;
    }
    
    public void release()
    {
      if (this.IX != null) {
        this.IX.zzbas();
      }
    }
  }
  
  static abstract class zzc
    extends zzt<DriveApi.DriveContentsResult>
  {
    zzc(GoogleApiClient paramGoogleApiClient)
    {
      super();
    }
    
    public DriveApi.DriveContentsResult zzan(Status paramStatus)
    {
      return new zzs.zzb(paramStatus, null);
    }
  }
  
  static class zzd
    extends zzd
  {
    private final zzqo.zzb<DriveApi.DriveIdResult> EW;
    
    public zzd(zzqo.zzb<DriveApi.DriveIdResult> paramzzb)
    {
      this.EW = paramzzb;
    }
    
    public void onError(Status paramStatus)
      throws RemoteException
    {
      this.EW.setResult(new zzs.zze(paramStatus, null));
    }
    
    public void zza(OnDriveIdResponse paramOnDriveIdResponse)
      throws RemoteException
    {
      this.EW.setResult(new zzs.zze(Status.xZ, paramOnDriveIdResponse.getDriveId()));
    }
    
    public void zza(OnMetadataResponse paramOnMetadataResponse)
      throws RemoteException
    {
      this.EW.setResult(new zzs.zze(Status.xZ, new zzp(paramOnMetadataResponse.zzbcn()).getDriveId()));
    }
  }
  
  private static class zze
    implements DriveApi.DriveIdResult
  {
    private final DriveId IU;
    private final Status hv;
    
    public zze(Status paramStatus, DriveId paramDriveId)
    {
      this.hv = paramStatus;
      this.IU = paramDriveId;
    }
    
    public DriveId getDriveId()
    {
      return this.IU;
    }
    
    public Status getStatus()
    {
      return this.hv;
    }
  }
  
  static abstract class zzf
    extends zzt<DriveApi.DriveIdResult>
  {
    zzf(GoogleApiClient paramGoogleApiClient)
    {
      super();
    }
    
    public DriveApi.DriveIdResult zzao(Status paramStatus)
    {
      return new zzs.zze(paramStatus, null);
    }
  }
  
  static class zzg
    implements DriveApi.MetadataBufferResult
  {
    private final MetadataBuffer Ld;
    private final boolean Le;
    private final Status hv;
    
    public zzg(Status paramStatus, MetadataBuffer paramMetadataBuffer, boolean paramBoolean)
    {
      this.hv = paramStatus;
      this.Ld = paramMetadataBuffer;
      this.Le = paramBoolean;
    }
    
    public MetadataBuffer getMetadataBuffer()
    {
      return this.Ld;
    }
    
    public Status getStatus()
    {
      return this.hv;
    }
    
    public void release()
    {
      if (this.Ld != null) {
        this.Ld.release();
      }
    }
  }
  
  static abstract class zzh
    extends zzt<DriveApi.MetadataBufferResult>
  {
    zzh(GoogleApiClient paramGoogleApiClient)
    {
      super();
    }
    
    public DriveApi.MetadataBufferResult zzap(Status paramStatus)
    {
      return new zzs.zzg(paramStatus, null, false);
    }
  }
  
  private static class zzi
    extends zzd
  {
    private final zzqo.zzb<DriveApi.MetadataBufferResult> EW;
    
    public zzi(zzqo.zzb<DriveApi.MetadataBufferResult> paramzzb)
    {
      this.EW = paramzzb;
    }
    
    public void onError(Status paramStatus)
      throws RemoteException
    {
      this.EW.setResult(new zzs.zzg(paramStatus, null, false));
    }
    
    public void zza(OnListEntriesResponse paramOnListEntriesResponse)
      throws RemoteException
    {
      MetadataBuffer localMetadataBuffer = new MetadataBuffer(paramOnListEntriesResponse.zzbck());
      this.EW.setResult(new zzs.zzg(Status.xZ, localMetadataBuffer, paramOnListEntriesResponse.zzbcl()));
    }
  }
  
  @SuppressLint({"MissingRemoteException"})
  static class zzj
    extends zzt.zza
  {
    zzj(GoogleApiClient paramGoogleApiClient, Status paramStatus)
    {
      super();
      zzc(paramStatus);
    }
    
    protected void zza(zzu paramzzu) {}
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/internal/zzs.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */