package com.google.android.gms.drive.internal;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.IBinder;
import android.os.Looper;
import android.os.Process;
import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzj;
import com.google.android.gms.common.util.zzx;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.events.ChangeListener;
import com.google.android.gms.drive.events.zzc;
import com.google.android.gms.drive.events.zzg;
import com.google.android.gms.drive.events.zzi;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class zzu
  extends zzj<zzam>
{
  private final Bundle Lf;
  private final boolean Lg;
  private volatile DriveId Lh;
  private volatile DriveId Li;
  private volatile boolean Lj = false;
  final GoogleApiClient.ConnectionCallbacks Lk;
  final Map<DriveId, Map<ChangeListener, zzae>> Ll = new HashMap();
  final Map<zzc, zzae> Lm = new HashMap();
  final Map<DriveId, Map<zzi, zzae>> Ln = new HashMap();
  final Map<DriveId, Map<zzi, zzae>> Lo = new HashMap();
  private final String hu;
  
  public zzu(Context paramContext, Looper paramLooper, zzf paramzzf, GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener, Bundle paramBundle)
  {
    super(paramContext, paramLooper, 11, paramzzf, paramConnectionCallbacks, paramOnConnectionFailedListener);
    this.hu = paramzzf.zzavs();
    this.Lk = paramConnectionCallbacks;
    this.Lf = paramBundle;
    paramLooper = new Intent("com.google.android.gms.drive.events.HANDLE_EVENT");
    paramLooper.setPackage(paramContext.getPackageName());
    paramContext = paramContext.getPackageManager().queryIntentServices(paramLooper, 0);
    switch (paramContext.size())
    {
    default: 
      paramContext = String.valueOf(paramLooper.getAction());
      throw new IllegalStateException(String.valueOf(paramContext).length() + 72 + "AndroidManifest.xml can only define one service that handles the " + paramContext + " action");
    case 0: 
      this.Lg = false;
      return;
    }
    paramContext = ((ResolveInfo)paramContext.get(0)).serviceInfo;
    if (!paramContext.exported)
    {
      paramContext = String.valueOf(paramContext.name);
      throw new IllegalStateException(String.valueOf(paramContext).length() + 60 + "Drive event service " + paramContext + " must be exported in AndroidManifest.xml");
    }
    this.Lg = true;
  }
  
  private PendingResult<Status> zza(GoogleApiClient paramGoogleApiClient, final int paramInt, final DriveId paramDriveId)
  {
    zzaa.zzbt(zzg.zza(paramInt, paramDriveId));
    zzaa.zza(isConnected(), "Client must be connected");
    paramGoogleApiClient.zzb(new zzt.zza(paramGoogleApiClient)
    {
      protected void zza(zzu paramAnonymouszzu)
        throws RemoteException
      {
        paramAnonymouszzu.zzbby().zza(new RemoveEventListenerRequest(paramDriveId, paramInt), null, null, new zzbr(this));
      }
    });
  }
  
  private PendingResult<Status> zza(GoogleApiClient paramGoogleApiClient, final AddEventListenerRequest paramAddEventListenerRequest)
  {
    zzaa.zzbt(zzg.zza(paramAddEventListenerRequest.getEventType(), paramAddEventListenerRequest.getDriveId()));
    zzaa.zza(isConnected(), "Client must be connected");
    if (!this.Lg) {
      throw new IllegalStateException("Application must define an exported DriveEventService subclass in AndroidManifest.xml to add event subscriptions");
    }
    paramGoogleApiClient.zzb(new zzt.zza(paramGoogleApiClient)
    {
      protected void zza(zzu paramAnonymouszzu)
        throws RemoteException
      {
        paramAnonymouszzu.zzbby().zza(paramAddEventListenerRequest, null, null, new zzbr(this));
      }
    });
  }
  
  private PendingResult<Status> zza(GoogleApiClient paramGoogleApiClient, final AddEventListenerRequest paramAddEventListenerRequest, final zzae paramzzae)
  {
    paramGoogleApiClient.zzb(new zzt.zza(paramGoogleApiClient)
    {
      protected void zza(zzu paramAnonymouszzu)
        throws RemoteException
      {
        paramAnonymouszzu.zzbby().zza(paramAddEventListenerRequest, paramzzae, null, new zzbr(this));
      }
    });
  }
  
  private PendingResult<Status> zza(GoogleApiClient paramGoogleApiClient, final RemoveEventListenerRequest paramRemoveEventListenerRequest, final zzae paramzzae)
  {
    paramGoogleApiClient.zzb(new zzt.zza(paramGoogleApiClient)
    {
      protected void zza(zzu paramAnonymouszzu)
        throws RemoteException
      {
        paramAnonymouszzu.zzbby().zza(paramRemoveEventListenerRequest, paramzzae, null, new zzbr(this));
      }
    });
  }
  
  PendingResult<Status> cancelPendingActions(GoogleApiClient paramGoogleApiClient, final List<String> paramList)
  {
    boolean bool2 = true;
    if (paramList != null)
    {
      bool1 = true;
      zzaa.zzbt(bool1);
      if (paramList.isEmpty()) {
        break label58;
      }
    }
    label58:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      zzaa.zzbt(bool1);
      zzaa.zza(isConnected(), "Client must be connected");
      paramGoogleApiClient.zzb(new zzt.zza(paramGoogleApiClient)
      {
        protected void zza(zzu paramAnonymouszzu)
          throws RemoteException
        {
          paramAnonymouszzu.zzbby().zza(new CancelPendingActionsRequest(paramList), new zzbr(this));
        }
      });
      bool1 = false;
      break;
    }
  }
  
  public void disconnect()
  {
    if (isConnected()) {}
    try
    {
      ((zzam)zzavg()).zza(new DisconnectRequest());
      super.disconnect();
      synchronized (this.Ll)
      {
        this.Ll.clear();
        synchronized (this.Lm)
        {
          this.Lm.clear();
          synchronized (this.Ln)
          {
            this.Ln.clear();
          }
        }
      }
      synchronized (this.Lo)
      {
        this.Lo.clear();
        return;
        localObject1 = finally;
        throw ((Throwable)localObject1);
        localObject2 = finally;
        throw ((Throwable)localObject2);
        localObject3 = finally;
        throw ((Throwable)localObject3);
      }
    }
    catch (RemoteException localRemoteException)
    {
      for (;;) {}
    }
  }
  
  PendingResult<Status> zza(GoogleApiClient paramGoogleApiClient, DriveId paramDriveId)
  {
    return zza(paramGoogleApiClient, new AddEventListenerRequest(1, paramDriveId));
  }
  
  PendingResult<Status> zza(GoogleApiClient paramGoogleApiClient, DriveId paramDriveId, ChangeListener paramChangeListener)
  {
    zzaa.zzbt(zzg.zza(1, paramDriveId));
    zzaa.zzb(paramChangeListener, "listener");
    zzaa.zza(isConnected(), "Client must be connected");
    for (;;)
    {
      synchronized (this.Ll)
      {
        Object localObject = (Map)this.Ll.get(paramDriveId);
        if (localObject == null)
        {
          localObject = new HashMap();
          this.Ll.put(paramDriveId, localObject);
          zzae localzzae = (zzae)((Map)localObject).get(paramChangeListener);
          if (localzzae == null)
          {
            localzzae = new zzae(getLooper(), getContext(), 1, paramChangeListener);
            ((Map)localObject).put(paramChangeListener, localzzae);
            paramChangeListener = localzzae;
            paramChangeListener.zzkg(1);
            paramGoogleApiClient = zza(paramGoogleApiClient, new AddEventListenerRequest(1, paramDriveId), paramChangeListener);
            return paramGoogleApiClient;
          }
          paramChangeListener = localzzae;
          if (localzzae.zzkh(1))
          {
            paramGoogleApiClient = new zzs.zzj(paramGoogleApiClient, Status.xZ);
            return paramGoogleApiClient;
          }
        }
      }
    }
  }
  
  protected void zza(int paramInt1, IBinder paramIBinder, Bundle paramBundle, int paramInt2)
  {
    if (paramBundle != null)
    {
      paramBundle.setClassLoader(getClass().getClassLoader());
      this.Lh = ((DriveId)paramBundle.getParcelable("com.google.android.gms.drive.root_id"));
      this.Li = ((DriveId)paramBundle.getParcelable("com.google.android.gms.drive.appdata_id"));
      this.Lj = true;
    }
    super.zza(paramInt1, paramIBinder, paramBundle, paramInt2);
  }
  
  protected Bundle zzahv()
  {
    String str = getContext().getPackageName();
    zzaa.zzy(str);
    if (!zzawb().zzavq().isEmpty()) {}
    for (boolean bool = true;; bool = false)
    {
      zzaa.zzbs(bool);
      Bundle localBundle = new Bundle();
      if (!str.equals(this.hu)) {
        localBundle.putString("proxy_package_name", this.hu);
      }
      localBundle.putAll(this.Lf);
      return localBundle;
    }
  }
  
  public boolean zzain()
  {
    return (!getContext().getPackageName().equals(this.hu)) || (!zzbbx());
  }
  
  public boolean zzavh()
  {
    return true;
  }
  
  PendingResult<Status> zzb(GoogleApiClient paramGoogleApiClient, DriveId paramDriveId)
  {
    return zza(paramGoogleApiClient, 1, paramDriveId);
  }
  
  PendingResult<Status> zzb(GoogleApiClient paramGoogleApiClient, DriveId paramDriveId, ChangeListener paramChangeListener)
  {
    zzaa.zzbt(zzg.zza(1, paramDriveId));
    zzaa.zza(isConnected(), "Client must be connected");
    zzaa.zzb(paramChangeListener, "listener");
    Map localMap2;
    synchronized (this.Ll)
    {
      localMap2 = (Map)this.Ll.get(paramDriveId);
      if (localMap2 == null)
      {
        paramGoogleApiClient = new zzs.zzj(paramGoogleApiClient, Status.xZ);
        return paramGoogleApiClient;
      }
      paramChangeListener = (zzae)localMap2.remove(paramChangeListener);
      if (paramChangeListener == null)
      {
        paramGoogleApiClient = new zzs.zzj(paramGoogleApiClient, Status.xZ);
        return paramGoogleApiClient;
      }
    }
    if (localMap2.isEmpty()) {
      this.Ll.remove(paramDriveId);
    }
    paramGoogleApiClient = zza(paramGoogleApiClient, new RemoveEventListenerRequest(paramDriveId, 1), paramChangeListener);
    return paramGoogleApiClient;
  }
  
  boolean zzbbx()
  {
    return zzx.zzf(getContext(), Process.myUid());
  }
  
  public zzam zzbby()
    throws DeadObjectException
  {
    return (zzam)zzavg();
  }
  
  public DriveId zzbbz()
  {
    return this.Lh;
  }
  
  public DriveId zzbca()
  {
    return this.Li;
  }
  
  public boolean zzbcb()
  {
    return this.Lj;
  }
  
  public boolean zzbcc()
  {
    return this.Lg;
  }
  
  protected zzam zzek(IBinder paramIBinder)
  {
    return zzam.zza.zzel(paramIBinder);
  }
  
  protected String zzjx()
  {
    return "com.google.android.gms.drive.ApiService.START";
  }
  
  protected String zzjy()
  {
    return "com.google.android.gms.drive.internal.IDriveService";
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/internal/zzu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */