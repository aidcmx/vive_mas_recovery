package com.google.android.gms.drive.internal;

import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.util.zzo;
import com.google.android.gms.drive.Contents;
import com.google.android.gms.drive.DriveApi.DriveContentsResult;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.ExecutionOptions;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;
import com.google.android.gms.drive.zzi;
import com.google.android.gms.drive.zzi.zza;
import java.io.InputStream;
import java.io.OutputStream;

public class zzv
  implements DriveContents
{
  private final Contents Lw;
  private boolean Lx = false;
  private boolean Ly = false;
  private boolean mClosed = false;
  
  public zzv(Contents paramContents)
  {
    this.Lw = ((Contents)zzaa.zzy(paramContents));
  }
  
  public PendingResult<Status> commit(GoogleApiClient paramGoogleApiClient, MetadataChangeSet paramMetadataChangeSet)
  {
    return zza(paramGoogleApiClient, paramMetadataChangeSet, null);
  }
  
  public PendingResult<Status> commit(GoogleApiClient paramGoogleApiClient, MetadataChangeSet paramMetadataChangeSet, ExecutionOptions paramExecutionOptions)
  {
    if (paramExecutionOptions == null) {}
    for (paramExecutionOptions = null;; paramExecutionOptions = zzi.zzb(paramExecutionOptions)) {
      return zza(paramGoogleApiClient, paramMetadataChangeSet, paramExecutionOptions);
    }
  }
  
  public void discard(GoogleApiClient paramGoogleApiClient)
  {
    if (zzbat()) {
      throw new IllegalStateException("DriveContents already closed.");
    }
    zzbas();
    ((4)paramGoogleApiClient.zzb(new zzt.zza(paramGoogleApiClient)
    {
      protected void zza(zzu paramAnonymouszzu)
        throws RemoteException
      {
        paramAnonymouszzu.zzbby().zza(new CloseContentsRequest(zzv.zza(zzv.this).getRequestId(), false), new zzbr(this));
      }
    })).setResultCallback(new ResultCallback()
    {
      public void zzp(Status paramAnonymousStatus)
      {
        if (!paramAnonymousStatus.isSuccess())
        {
          zzz.zzaf("DriveContentsImpl", "Error discarding contents");
          return;
        }
        zzz.zzad("DriveContentsImpl", "Contents discarded");
      }
    });
  }
  
  public DriveId getDriveId()
  {
    return this.Lw.getDriveId();
  }
  
  public InputStream getInputStream()
  {
    if (zzbat()) {
      throw new IllegalStateException("Contents have been closed, cannot access the input stream.");
    }
    if (this.Lw.getMode() != 268435456) {
      throw new IllegalStateException("getInputStream() can only be used with contents opened with MODE_READ_ONLY.");
    }
    if (this.Lx) {
      throw new IllegalStateException("getInputStream() can only be called once per Contents instance.");
    }
    this.Lx = true;
    return this.Lw.getInputStream();
  }
  
  public int getMode()
  {
    return this.Lw.getMode();
  }
  
  public OutputStream getOutputStream()
  {
    if (zzbat()) {
      throw new IllegalStateException("Contents have been closed, cannot access the output stream.");
    }
    if (this.Lw.getMode() != 536870912) {
      throw new IllegalStateException("getOutputStream() can only be used with contents opened with MODE_WRITE_ONLY.");
    }
    if (this.Ly) {
      throw new IllegalStateException("getOutputStream() can only be called once per Contents instance.");
    }
    this.Ly = true;
    return this.Lw.getOutputStream();
  }
  
  public ParcelFileDescriptor getParcelFileDescriptor()
  {
    if (zzbat()) {
      throw new IllegalStateException("Contents have been closed, cannot access the output stream.");
    }
    return this.Lw.getParcelFileDescriptor();
  }
  
  public PendingResult<DriveApi.DriveContentsResult> reopenForWrite(GoogleApiClient paramGoogleApiClient)
  {
    if (zzbat()) {
      throw new IllegalStateException("DriveContents already closed.");
    }
    if (this.Lw.getMode() != 268435456) {
      throw new IllegalStateException("reopenForWrite can only be used with DriveContents opened with MODE_READ_ONLY.");
    }
    zzbas();
    paramGoogleApiClient.zza(new zzs.zzc(paramGoogleApiClient)
    {
      protected void zza(zzu paramAnonymouszzu)
        throws RemoteException
      {
        paramAnonymouszzu.zzbby().zza(new OpenContentsRequest(zzv.this.getDriveId(), 536870912, zzv.zza(zzv.this).getRequestId()), new zzbi(this, null));
      }
    });
  }
  
  public PendingResult<Status> zza(GoogleApiClient paramGoogleApiClient, final MetadataChangeSet paramMetadataChangeSet, final zzi paramzzi)
  {
    if (paramzzi == null) {
      paramzzi = (zzi)new zzi.zza().build();
    }
    for (;;)
    {
      if (this.Lw.getMode() == 268435456) {
        throw new IllegalStateException("Cannot commit contents opened with MODE_READ_ONLY");
      }
      if ((ExecutionOptions.zziv(paramzzi.zzbax())) && (!this.Lw.zzbap())) {
        throw new IllegalStateException("DriveContents must be valid for conflict detection.");
      }
      paramzzi.zzh(paramGoogleApiClient);
      if (zzbat()) {
        throw new IllegalStateException("DriveContents already closed.");
      }
      if (getDriveId() == null) {
        throw new IllegalStateException("Only DriveContents obtained through DriveFile.open can be committed.");
      }
      if (paramMetadataChangeSet != null) {}
      for (;;)
      {
        zzbas();
        paramGoogleApiClient.zzb(new zzt.zza(paramGoogleApiClient)
        {
          protected void zza(zzu paramAnonymouszzu)
            throws RemoteException
          {
            paramMetadataChangeSet.zzbbf().setContext(paramAnonymouszzu.getContext());
            paramAnonymouszzu.zzbby().zza(new CloseContentsAndUpdateMetadataRequest(zzv.zza(zzv.this).getDriveId(), paramMetadataChangeSet.zzbbf(), zzv.zza(zzv.this).getRequestId(), zzv.zza(zzv.this).zzbap(), paramzzi), new zzbr(this));
          }
        });
        paramMetadataChangeSet = MetadataChangeSet.Jy;
      }
    }
  }
  
  public Contents zzbar()
  {
    return this.Lw;
  }
  
  public void zzbas()
  {
    zzo.zza(this.Lw.getParcelFileDescriptor());
    this.mClosed = true;
  }
  
  public boolean zzbat()
  {
    return this.mClosed;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/internal/zzv.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */