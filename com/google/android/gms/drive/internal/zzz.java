package com.google.android.gms.drive.internal;

import android.content.Context;
import com.google.android.gms.common.internal.zzo;

public final class zzz
{
  private static final zzo LP = new zzo("GmsDrive");
  
  public static void zza(Context paramContext, String paramString1, String paramString2, Throwable paramThrowable)
  {
    LP.zze(paramString1, paramString2, paramThrowable);
  }
  
  public static void zza(String paramString1, Throwable paramThrowable, String paramString2)
  {
    LP.zzd(paramString1, paramString2, paramThrowable);
  }
  
  public static void zzad(String paramString1, String paramString2)
  {
    LP.zzad(paramString1, paramString2);
  }
  
  public static void zzae(String paramString1, String paramString2)
  {
    LP.zzae(paramString1, paramString2);
  }
  
  public static void zzaf(String paramString1, String paramString2)
  {
    LP.zzaf(paramString1, paramString2);
  }
  
  public static void zzh(Context paramContext, String paramString1, String paramString2)
  {
    zza(paramContext, paramString1, paramString2, new Throwable());
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/internal/zzz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */