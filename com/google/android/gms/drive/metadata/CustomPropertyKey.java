package com.google.android.gms.drive.metadata;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONException;
import org.json.JSONObject;

public class CustomPropertyKey
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<CustomPropertyKey> CREATOR = new zzc();
  private static final Pattern Nf = Pattern.compile("[\\w.!@$%^&*()/-]+");
  public static final int PRIVATE = 1;
  public static final int PUBLIC = 0;
  final int mVersionCode;
  final int mVisibility;
  final String zzbcn;
  
  CustomPropertyKey(int paramInt1, String paramString, int paramInt2)
  {
    zzaa.zzb(paramString, "key");
    zzaa.zzb(Nf.matcher(paramString).matches(), "key name characters must be alphanumeric or one of .!@$%^&*()-_/");
    boolean bool1 = bool2;
    if (paramInt2 != 0) {
      if (paramInt2 != 1) {
        break label69;
      }
    }
    label69:
    for (bool1 = bool2;; bool1 = false)
    {
      zzaa.zzb(bool1, "visibility must be either PUBLIC or PRIVATE");
      this.mVersionCode = paramInt1;
      this.zzbcn = paramString;
      this.mVisibility = paramInt2;
      return;
    }
  }
  
  public CustomPropertyKey(String paramString, int paramInt)
  {
    this(1, paramString, paramInt);
  }
  
  public static CustomPropertyKey fromJson(JSONObject paramJSONObject)
    throws JSONException
  {
    return new CustomPropertyKey(paramJSONObject.getString("key"), paramJSONObject.getInt("visibility"));
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool = true;
    if (paramObject == null) {}
    do
    {
      return false;
      if (paramObject == this) {
        return true;
      }
    } while (!(paramObject instanceof CustomPropertyKey));
    paramObject = (CustomPropertyKey)paramObject;
    if ((((CustomPropertyKey)paramObject).getKey().equals(this.zzbcn)) && (((CustomPropertyKey)paramObject).getVisibility() == this.mVisibility)) {}
    for (;;)
    {
      return bool;
      bool = false;
    }
  }
  
  public String getKey()
  {
    return this.zzbcn;
  }
  
  public int getVisibility()
  {
    return this.mVisibility;
  }
  
  public int hashCode()
  {
    String str = this.zzbcn;
    int i = this.mVisibility;
    return (String.valueOf(str).length() + 11 + str + i).hashCode();
  }
  
  public JSONObject toJson()
    throws JSONException
  {
    JSONObject localJSONObject = new JSONObject();
    localJSONObject.put("key", getKey());
    localJSONObject.put("visibility", getVisibility());
    return localJSONObject;
  }
  
  public String toString()
  {
    String str = this.zzbcn;
    int i = this.mVisibility;
    return String.valueOf(str).length() + 31 + "CustomPropertyKey(" + str + "," + i + ")";
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzc.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/metadata/CustomPropertyKey.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */