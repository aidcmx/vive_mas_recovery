package com.google.android.gms.drive.metadata.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.drive.metadata.CustomPropertyKey;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public final class AppVisibleCustomProperties
  extends AbstractSafeParcelable
  implements ReflectedParcelable, Iterable<CustomProperty>
{
  public static final Parcelable.Creator<AppVisibleCustomProperties> CREATOR = new zza();
  public static final AppVisibleCustomProperties Ng = new zza().zzbcu();
  final List<CustomProperty> Nh;
  final int mVersionCode;
  
  AppVisibleCustomProperties(int paramInt, Collection<CustomProperty> paramCollection)
  {
    this.mVersionCode = paramInt;
    zzaa.zzy(paramCollection);
    this.Nh = new ArrayList(paramCollection);
  }
  
  private AppVisibleCustomProperties(Collection<CustomProperty> paramCollection)
  {
    this(1, paramCollection);
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if ((paramObject == null) || (paramObject.getClass() != getClass())) {
      return false;
    }
    return zzbct().equals(((AppVisibleCustomProperties)paramObject).zzbct());
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.Nh });
  }
  
  public Iterator<CustomProperty> iterator()
  {
    return this.Nh.iterator();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zza.zza(this, paramParcel, paramInt);
  }
  
  public Map<CustomPropertyKey, String> zzbct()
  {
    HashMap localHashMap = new HashMap(this.Nh.size());
    Iterator localIterator = this.Nh.iterator();
    while (localIterator.hasNext())
    {
      CustomProperty localCustomProperty = (CustomProperty)localIterator.next();
      localHashMap.put(localCustomProperty.zzbcv(), localCustomProperty.getValue());
    }
    return Collections.unmodifiableMap(localHashMap);
  }
  
  public static class zza
  {
    private final Map<CustomPropertyKey, CustomProperty> Ni = new HashMap();
    
    public zza zza(CustomPropertyKey paramCustomPropertyKey, String paramString)
    {
      zzaa.zzb(paramCustomPropertyKey, "key");
      this.Ni.put(paramCustomPropertyKey, new CustomProperty(paramCustomPropertyKey, paramString));
      return this;
    }
    
    public zza zza(CustomProperty paramCustomProperty)
    {
      zzaa.zzb(paramCustomProperty, "property");
      this.Ni.put(paramCustomProperty.zzbcv(), paramCustomProperty);
      return this;
    }
    
    public AppVisibleCustomProperties zzbcu()
    {
      return new AppVisibleCustomProperties(this.Ni.values(), null);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/metadata/internal/AppVisibleCustomProperties.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */