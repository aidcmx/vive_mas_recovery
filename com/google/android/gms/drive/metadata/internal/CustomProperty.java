package com.google.android.gms.drive.metadata.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.drive.metadata.CustomPropertyKey;

public class CustomProperty
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<CustomProperty> CREATOR = new zzc();
  final CustomPropertyKey Nj;
  final String mValue;
  final int mVersionCode;
  
  CustomProperty(int paramInt, CustomPropertyKey paramCustomPropertyKey, String paramString)
  {
    this.mVersionCode = paramInt;
    zzaa.zzb(paramCustomPropertyKey, "key");
    this.Nj = paramCustomPropertyKey;
    this.mValue = paramString;
  }
  
  public CustomProperty(CustomPropertyKey paramCustomPropertyKey, String paramString)
  {
    this(1, paramCustomPropertyKey, paramString);
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    do
    {
      return true;
      if ((paramObject == null) || (paramObject.getClass() != getClass())) {
        return false;
      }
      paramObject = (CustomProperty)paramObject;
    } while ((zzz.equal(this.Nj, ((CustomProperty)paramObject).Nj)) && (zzz.equal(this.mValue, ((CustomProperty)paramObject).mValue)));
    return false;
  }
  
  public String getValue()
  {
    return this.mValue;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.Nj, this.mValue });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzc.zza(this, paramParcel, paramInt);
  }
  
  public CustomPropertyKey zzbcv()
  {
    return this.Nj;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/metadata/internal/CustomProperty.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */