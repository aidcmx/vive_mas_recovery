package com.google.android.gms.drive.metadata.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.data.BitmapTeleporter;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.drive.metadata.MetadataField;
import com.google.android.gms.internal.zztg;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public final class MetadataBundle
  extends AbstractSafeParcelable
  implements ReflectedParcelable
{
  public static final Parcelable.Creator<MetadataBundle> CREATOR = new zzh();
  final Bundle Nm;
  final int mVersionCode;
  
  MetadataBundle(int paramInt, Bundle paramBundle)
  {
    this.mVersionCode = paramInt;
    this.Nm = ((Bundle)zzaa.zzy(paramBundle));
    this.Nm.setClassLoader(getClass().getClassLoader());
    Object localObject = new ArrayList();
    Iterator localIterator = this.Nm.keySet().iterator();
    while (localIterator.hasNext())
    {
      paramBundle = (String)localIterator.next();
      if (zze.zzit(paramBundle) == null)
      {
        ((List)localObject).add(paramBundle);
        paramBundle = String.valueOf(paramBundle);
        if (paramBundle.length() != 0) {}
        for (paramBundle = "Ignored unknown metadata field in bundle: ".concat(paramBundle);; paramBundle = new String("Ignored unknown metadata field in bundle: "))
        {
          com.google.android.gms.drive.internal.zzz.zzae("MetadataBundle", paramBundle);
          break;
        }
      }
    }
    paramBundle = ((List)localObject).iterator();
    while (paramBundle.hasNext())
    {
      localObject = (String)paramBundle.next();
      this.Nm.remove((String)localObject);
    }
  }
  
  private MetadataBundle(Bundle paramBundle)
  {
    this(1, paramBundle);
  }
  
  public static <T> MetadataBundle zzb(MetadataField<T> paramMetadataField, T paramT)
  {
    MetadataBundle localMetadataBundle = zzbcy();
    localMetadataBundle.zzc(paramMetadataField, paramT);
    return localMetadataBundle;
  }
  
  public static MetadataBundle zzbcy()
  {
    return new MetadataBundle(new Bundle());
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if (!(paramObject instanceof MetadataBundle)) {
      return false;
    }
    paramObject = (MetadataBundle)paramObject;
    Object localObject = this.Nm.keySet();
    if (!((Set)localObject).equals(((MetadataBundle)paramObject).Nm.keySet())) {
      return false;
    }
    localObject = ((Set)localObject).iterator();
    while (((Iterator)localObject).hasNext())
    {
      String str = (String)((Iterator)localObject).next();
      if (!com.google.android.gms.common.internal.zzz.equal(this.Nm.get(str), ((MetadataBundle)paramObject).Nm.get(str))) {
        return false;
      }
    }
    return true;
  }
  
  public int hashCode()
  {
    Iterator localIterator = this.Nm.keySet().iterator();
    String str;
    for (int i = 1; localIterator.hasNext(); i = this.Nm.get(str).hashCode() + i * 31) {
      str = (String)localIterator.next();
    }
    return i;
  }
  
  public void setContext(Context paramContext)
  {
    BitmapTeleporter localBitmapTeleporter = (BitmapTeleporter)zza(zztg.NV);
    if (localBitmapTeleporter != null) {
      localBitmapTeleporter.zzd(paramContext.getCacheDir());
    }
  }
  
  public String toString()
  {
    String str = String.valueOf(this.Nm);
    return String.valueOf(str).length() + 24 + "MetadataBundle [values=" + str + "]";
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzh.zza(this, paramParcel, paramInt);
  }
  
  public <T> T zza(MetadataField<T> paramMetadataField)
  {
    return (T)paramMetadataField.zzr(this.Nm);
  }
  
  public MetadataBundle zzbcz()
  {
    return new MetadataBundle(new Bundle(this.Nm));
  }
  
  public Set<MetadataField<?>> zzbda()
  {
    HashSet localHashSet = new HashSet();
    Iterator localIterator = this.Nm.keySet().iterator();
    while (localIterator.hasNext()) {
      localHashSet.add(zze.zzit((String)localIterator.next()));
    }
    return localHashSet;
  }
  
  public <T> void zzc(MetadataField<T> paramMetadataField, T paramT)
  {
    if (zze.zzit(paramMetadataField.getName()) == null)
    {
      paramMetadataField = String.valueOf(paramMetadataField.getName());
      if (paramMetadataField.length() != 0) {}
      for (paramMetadataField = "Unregistered field: ".concat(paramMetadataField);; paramMetadataField = new String("Unregistered field: ")) {
        throw new IllegalArgumentException(paramMetadataField);
      }
    }
    paramMetadataField.zza(paramT, this.Nm);
  }
  
  public boolean zzc(MetadataField<?> paramMetadataField)
  {
    return this.Nm.containsKey(paramMetadataField.getName());
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/metadata/internal/MetadataBundle.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */