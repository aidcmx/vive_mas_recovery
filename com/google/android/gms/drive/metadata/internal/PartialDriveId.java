package com.google.android.gms.drive.metadata.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.drive.DriveId;

public class PartialDriveId
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<PartialDriveId> CREATOR = new zzn();
  final String Jg;
  final long Jh;
  final int Ji;
  final int mVersionCode;
  
  PartialDriveId(int paramInt1, String paramString, long paramLong, int paramInt2)
  {
    this.mVersionCode = paramInt1;
    this.Jg = paramString;
    this.Jh = paramLong;
    this.Ji = paramInt2;
  }
  
  public PartialDriveId(String paramString, long paramLong, int paramInt)
  {
    this(1, paramString, paramLong, paramInt);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzn.zza(this, paramParcel, paramInt);
  }
  
  public DriveId zzal(long paramLong)
  {
    return new DriveId(this.Jg, this.Jh, paramLong, this.Ji);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/metadata/internal/PartialDriveId.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */