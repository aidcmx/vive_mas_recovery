package com.google.android.gms.drive.metadata.internal;

import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.metadata.MetadataField;
import com.google.android.gms.internal.zztg;
import com.google.android.gms.internal.zzth;
import com.google.android.gms.internal.zzti;
import com.google.android.gms.internal.zztk;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public final class zze
{
  private static final Map<String, MetadataField<?>> Nk = new HashMap();
  private static final Map<String, zza> Nl = new HashMap();
  
  static
  {
    zzb(zztg.Nq);
    zzb(zztg.NW);
    zzb(zztg.NN);
    zzb(zztg.NU);
    zzb(zztg.NX);
    zzb(zztg.ND);
    zzb(zztg.NC);
    zzb(zztg.NE);
    zzb(zztg.NF);
    zzb(zztg.NG);
    zzb(zztg.NA);
    zzb(zztg.NI);
    zzb(zztg.NJ);
    zzb(zztg.NK);
    zzb(zztg.NS);
    zzb(zztg.Nr);
    zzb(zztg.NP);
    zzb(zztg.Nt);
    zzb(zztg.NB);
    zzb(zztg.Nu);
    zzb(zztg.Nv);
    zzb(zztg.Nw);
    zzb(zztg.Nx);
    zzb(zztg.NM);
    zzb(zztg.NH);
    zzb(zztg.NO);
    zzb(zztg.NQ);
    zzb(zztg.NR);
    zzb(zztg.NT);
    zzb(zztg.NY);
    zzb(zztg.NZ);
    zzb(zztg.Nz);
    zzb(zztg.Ny);
    zzb(zztg.NV);
    zzb(zztg.NL);
    zzb(zztg.Ns);
    zzb(zztg.Oa);
    zzb(zztg.Ob);
    zzb(zztg.Oc);
    zzb(zztg.Od);
    zzb(zztg.Oe);
    zzb(zztg.Of);
    zzb(zztg.Og);
    zzb(zzti.Oi);
    zzb(zzti.Ok);
    zzb(zzti.Ol);
    zzb(zzti.Om);
    zzb(zzti.Oj);
    zzb(zzti.On);
    zzb(zztk.Op);
    zzb(zztk.Oq);
    zzm localzzm = zztg.NS;
    zza(zzm.Np);
    zza(zzth.Oh);
  }
  
  public static void zza(DataHolder paramDataHolder)
  {
    Iterator localIterator = Nl.values().iterator();
    while (localIterator.hasNext()) {
      ((zza)localIterator.next()).zzb(paramDataHolder);
    }
  }
  
  private static void zza(zza paramzza)
  {
    if (Nl.put(paramzza.zzbcx(), paramzza) != null)
    {
      paramzza = String.valueOf(paramzza.zzbcx());
      throw new IllegalStateException(String.valueOf(paramzza).length() + 46 + "A cleaner for key " + paramzza + " has already been registered");
    }
  }
  
  private static void zzb(MetadataField<?> paramMetadataField)
  {
    if (Nk.containsKey(paramMetadataField.getName()))
    {
      paramMetadataField = String.valueOf(paramMetadataField.getName());
      if (paramMetadataField.length() != 0) {}
      for (paramMetadataField = "Duplicate field name registered: ".concat(paramMetadataField);; paramMetadataField = new String("Duplicate field name registered: ")) {
        throw new IllegalArgumentException(paramMetadataField);
      }
    }
    Nk.put(paramMetadataField.getName(), paramMetadataField);
  }
  
  public static Collection<MetadataField<?>> zzbcw()
  {
    return Collections.unmodifiableCollection(Nk.values());
  }
  
  public static MetadataField<?> zzit(String paramString)
  {
    return (MetadataField)Nk.get(paramString);
  }
  
  public static abstract interface zza
  {
    public abstract void zzb(DataHolder paramDataHolder);
    
    public abstract String zzbcx();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/metadata/internal/zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */