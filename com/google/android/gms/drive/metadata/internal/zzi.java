package com.google.android.gms.drive.metadata.internal;

import com.google.android.gms.common.internal.zzaa;

public class zzi
{
  private String Nn;
  
  private zzi(String paramString)
  {
    this.Nn = paramString.toLowerCase();
  }
  
  public static zzi zziu(String paramString)
  {
    if ((paramString == null) || (!paramString.isEmpty())) {}
    for (boolean bool = true;; bool = false)
    {
      zzaa.zzbt(bool);
      if (paramString != null) {
        break;
      }
      return null;
    }
    return new zzi(paramString);
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {}
    do
    {
      return false;
      if (paramObject == this) {
        return true;
      }
    } while (paramObject.getClass() != getClass());
    paramObject = (zzi)paramObject;
    return this.Nn.equals(((zzi)paramObject).Nn);
  }
  
  public int hashCode()
  {
    return this.Nn.hashCode();
  }
  
  public boolean isFolder()
  {
    return this.Nn.equals("application/vnd.google-apps.folder");
  }
  
  public String toString()
  {
    return this.Nn;
  }
  
  public boolean zzbdb()
  {
    return (!zzbdc()) && (!isFolder());
  }
  
  public boolean zzbdc()
  {
    return this.Nn.startsWith("application/vnd.google-apps");
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/metadata/internal/zzi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */