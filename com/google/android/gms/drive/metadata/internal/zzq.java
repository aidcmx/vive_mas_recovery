package com.google.android.gms.drive.metadata.internal;

import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.UserMetadata;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public class zzq
  extends zzk<UserMetadata>
{
  public zzq(String paramString, int paramInt)
  {
    super(paramString, zzix(paramString), Collections.emptyList(), paramInt);
  }
  
  private static String zzai(String paramString1, String paramString2)
  {
    return String.valueOf(paramString1).length() + 1 + String.valueOf(paramString2).length() + paramString1 + "." + paramString2;
  }
  
  private String zziw(String paramString)
  {
    return zzai(getName(), paramString);
  }
  
  private static Collection<String> zzix(String paramString)
  {
    return Arrays.asList(new String[] { zzai(paramString, "permissionId"), zzai(paramString, "displayName"), zzai(paramString, "picture"), zzai(paramString, "isAuthenticatedUser"), zzai(paramString, "emailAddress") });
  }
  
  protected boolean zzb(DataHolder paramDataHolder, int paramInt1, int paramInt2)
  {
    return (paramDataHolder.zzho(zziw("permissionId"))) && (!paramDataHolder.zzi(zziw("permissionId"), paramInt1, paramInt2));
  }
  
  protected UserMetadata zzj(DataHolder paramDataHolder, int paramInt1, int paramInt2)
  {
    String str1 = paramDataHolder.zzd(zziw("permissionId"), paramInt1, paramInt2);
    if (str1 != null)
    {
      String str2 = paramDataHolder.zzd(zziw("displayName"), paramInt1, paramInt2);
      String str3 = paramDataHolder.zzd(zziw("picture"), paramInt1, paramInt2);
      boolean bool = paramDataHolder.zze(zziw("isAuthenticatedUser"), paramInt1, paramInt2);
      paramDataHolder = paramDataHolder.zzd(zziw("emailAddress"), paramInt1, paramInt2);
      return new UserMetadata(str1, str2, str3, Boolean.valueOf(bool).booleanValue(), paramDataHolder);
    }
    return null;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/metadata/internal/zzq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */