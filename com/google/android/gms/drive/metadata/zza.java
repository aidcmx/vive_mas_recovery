package com.google.android.gms.drive.metadata;

import android.os.Bundle;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public abstract class zza<T>
  implements MetadataField<T>
{
  private final String Nb;
  private final Set<String> Nc;
  private final Set<String> Nd;
  private final int Ne;
  
  protected zza(String paramString, int paramInt)
  {
    this.Nb = ((String)zzaa.zzb(paramString, "fieldName"));
    this.Nc = Collections.singleton(paramString);
    this.Nd = Collections.emptySet();
    this.Ne = paramInt;
  }
  
  protected zza(String paramString, Collection<String> paramCollection1, Collection<String> paramCollection2, int paramInt)
  {
    this.Nb = ((String)zzaa.zzb(paramString, "fieldName"));
    this.Nc = Collections.unmodifiableSet(new HashSet(paramCollection1));
    this.Nd = Collections.unmodifiableSet(new HashSet(paramCollection2));
    this.Ne = paramInt;
  }
  
  public final String getName()
  {
    return this.Nb;
  }
  
  public String toString()
  {
    return this.Nb;
  }
  
  public final T zza(DataHolder paramDataHolder, int paramInt1, int paramInt2)
  {
    if (zzb(paramDataHolder, paramInt1, paramInt2)) {
      return (T)zzc(paramDataHolder, paramInt1, paramInt2);
    }
    return null;
  }
  
  protected abstract void zza(Bundle paramBundle, T paramT);
  
  public final void zza(DataHolder paramDataHolder, MetadataBundle paramMetadataBundle, int paramInt1, int paramInt2)
  {
    zzaa.zzb(paramDataHolder, "dataHolder");
    zzaa.zzb(paramMetadataBundle, "bundle");
    if (zzb(paramDataHolder, paramInt1, paramInt2)) {
      paramMetadataBundle.zzc(this, zzc(paramDataHolder, paramInt1, paramInt2));
    }
  }
  
  public final void zza(T paramT, Bundle paramBundle)
  {
    zzaa.zzb(paramBundle, "bundle");
    if (paramT == null)
    {
      paramBundle.putString(getName(), null);
      return;
    }
    zza(paramBundle, paramT);
  }
  
  protected boolean zzb(DataHolder paramDataHolder, int paramInt1, int paramInt2)
  {
    Iterator localIterator = this.Nc.iterator();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      if ((!paramDataHolder.zzho(str)) || (paramDataHolder.zzi(str, paramInt1, paramInt2))) {
        return false;
      }
    }
    return true;
  }
  
  public final Collection<String> zzbcs()
  {
    return this.Nc;
  }
  
  protected abstract T zzc(DataHolder paramDataHolder, int paramInt1, int paramInt2);
  
  public final T zzr(Bundle paramBundle)
  {
    zzaa.zzb(paramBundle, "bundle");
    if (paramBundle.get(getName()) != null) {
      return (T)zzs(paramBundle);
    }
    return null;
  }
  
  protected abstract T zzs(Bundle paramBundle);
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/metadata/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */