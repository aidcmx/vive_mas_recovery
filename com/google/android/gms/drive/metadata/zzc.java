package com.google.android.gms.drive.metadata;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzc
  implements Parcelable.Creator<CustomPropertyKey>
{
  static void zza(CustomPropertyKey paramCustomPropertyKey, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramCustomPropertyKey.mVersionCode);
    zzb.zza(paramParcel, 2, paramCustomPropertyKey.zzbcn, false);
    zzb.zzc(paramParcel, 3, paramCustomPropertyKey.mVisibility);
    zzb.zzaj(paramParcel, paramInt);
  }
  
  public CustomPropertyKey zzha(Parcel paramParcel)
  {
    int j = 0;
    int k = zza.zzcr(paramParcel);
    String str = null;
    int i = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.zzcq(paramParcel);
      switch (zza.zzgu(m))
      {
      default: 
        zza.zzb(paramParcel, m);
        break;
      case 1: 
        i = zza.zzg(paramParcel, m);
        break;
      case 2: 
        str = zza.zzq(paramParcel, m);
        break;
      case 3: 
        j = zza.zzg(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza(37 + "Overread allowed size end=" + k, paramParcel);
    }
    return new CustomPropertyKey(i, str, j);
  }
  
  public CustomPropertyKey[] zzly(int paramInt)
  {
    return new CustomPropertyKey[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/metadata/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */