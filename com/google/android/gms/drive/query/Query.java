package com.google.android.gms.drive.query;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.drive.DriveSpace;
import com.google.android.gms.drive.query.internal.LogicalFilter;
import com.google.android.gms.drive.query.internal.MatchAllFilter;
import com.google.android.gms.drive.query.internal.Operator;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;

public class Query
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<Query> CREATOR = new zza();
  final List<DriveSpace> JW;
  private final Set<DriveSpace> JX;
  final boolean Mh;
  final LogicalFilter Or;
  final String Os;
  final SortOrder Ot;
  final List<String> Ou;
  final boolean Ov;
  final int mVersionCode;
  
  private Query(int paramInt, LogicalFilter paramLogicalFilter, String paramString, SortOrder paramSortOrder, List<String> paramList, boolean paramBoolean1, List<DriveSpace> paramList1, Set<DriveSpace> paramSet, boolean paramBoolean2)
  {
    this.mVersionCode = paramInt;
    this.Or = paramLogicalFilter;
    this.Os = paramString;
    this.Ot = paramSortOrder;
    this.Ou = paramList;
    this.Ov = paramBoolean1;
    this.JW = paramList1;
    this.JX = paramSet;
    this.Mh = paramBoolean2;
  }
  
  Query(int paramInt, LogicalFilter paramLogicalFilter, String paramString, SortOrder paramSortOrder, List<String> paramList, boolean paramBoolean1, List<DriveSpace> paramList1, boolean paramBoolean2) {}
  
  private Query(LogicalFilter paramLogicalFilter, String paramString, SortOrder paramSortOrder, List<String> paramList, boolean paramBoolean1, Set<DriveSpace> paramSet, boolean paramBoolean2) {}
  
  public Filter getFilter()
  {
    return this.Or;
  }
  
  @Deprecated
  public String getPageToken()
  {
    return this.Os;
  }
  
  public SortOrder getSortOrder()
  {
    return this.Ot;
  }
  
  public String toString()
  {
    return String.format(Locale.US, "Query[%s,%s,PageToken=%s,Spaces=%s]", new Object[] { this.Or, this.Ot, this.Os, this.JW });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zza.zza(this, paramParcel, paramInt);
  }
  
  public List<String> zzbdd()
  {
    return this.Ou;
  }
  
  public boolean zzbde()
  {
    return this.Ov;
  }
  
  public Set<DriveSpace> zzbdf()
  {
    return this.JX;
  }
  
  public boolean zzbdg()
  {
    return this.Mh;
  }
  
  public static class Builder
  {
    private Set<DriveSpace> JX;
    private boolean Mh;
    private String Os;
    private SortOrder Ot;
    private List<String> Ou;
    private boolean Ov;
    private final List<Filter> Ow = new ArrayList();
    
    public Builder() {}
    
    public Builder(Query paramQuery)
    {
      this.Ow.add(paramQuery.getFilter());
      this.Os = paramQuery.getPageToken();
      this.Ot = paramQuery.getSortOrder();
      this.Ou = paramQuery.zzbdd();
      this.Ov = paramQuery.zzbde();
      this.JX = paramQuery.zzbdf();
      this.Mh = paramQuery.zzbdg();
    }
    
    public Builder addFilter(Filter paramFilter)
    {
      if (!(paramFilter instanceof MatchAllFilter)) {
        this.Ow.add(paramFilter);
      }
      return this;
    }
    
    public Query build()
    {
      return new Query(new LogicalFilter(Operator.OY, this.Ow), this.Os, this.Ot, this.Ou, this.Ov, this.JX, this.Mh, null);
    }
    
    @Deprecated
    public Builder setPageToken(String paramString)
    {
      this.Os = paramString;
      return this;
    }
    
    public Builder setSortOrder(SortOrder paramSortOrder)
    {
      this.Ot = paramSortOrder;
      return this;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/query/Query.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */