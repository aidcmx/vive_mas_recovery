package com.google.android.gms.drive.query;

import com.google.android.gms.drive.metadata.SortableMetadataField;
import com.google.android.gms.internal.zztg;
import com.google.android.gms.internal.zzti;
import java.util.Date;

public class SortableField
{
  public static final SortableMetadataField<Date> CREATED_DATE;
  public static final SortableMetadataField<Date> LAST_VIEWED_BY_ME;
  public static final SortableMetadataField<Date> MODIFIED_BY_ME_DATE;
  public static final SortableMetadataField<Date> MODIFIED_DATE;
  public static final SortableMetadataField<Date> OB = zzti.On;
  public static final SortableMetadataField<Long> QUOTA_USED;
  public static final SortableMetadataField<Date> SHARED_WITH_ME_DATE;
  public static final SortableMetadataField<String> TITLE = zztg.NW;
  
  static
  {
    CREATED_DATE = zzti.Oi;
    MODIFIED_DATE = zzti.Ok;
    MODIFIED_BY_ME_DATE = zzti.Ol;
    LAST_VIEWED_BY_ME = zzti.Oj;
    SHARED_WITH_ME_DATE = zzti.Om;
    QUOTA_USED = zztg.NT;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/query/SortableField.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */