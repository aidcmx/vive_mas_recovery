package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.drive.query.Filter;

public class FilterHolder
  extends AbstractSafeParcelable
  implements ReflectedParcelable
{
  public static final Parcelable.Creator<FilterHolder> CREATOR = new zzd();
  private final Filter JD;
  final ComparisonFilter<?> OG;
  final FieldOnlyFilter OH;
  final LogicalFilter OI;
  final NotFilter OJ;
  final InFilter<?> OK;
  final MatchAllFilter OL;
  final HasFilter OM;
  final FullTextSearchFilter ON;
  final OwnedByMeFilter OO;
  final int mVersionCode;
  
  FilterHolder(int paramInt, ComparisonFilter<?> paramComparisonFilter, FieldOnlyFilter paramFieldOnlyFilter, LogicalFilter paramLogicalFilter, NotFilter paramNotFilter, InFilter<?> paramInFilter, MatchAllFilter paramMatchAllFilter, HasFilter<?> paramHasFilter, FullTextSearchFilter paramFullTextSearchFilter, OwnedByMeFilter paramOwnedByMeFilter)
  {
    this.mVersionCode = paramInt;
    this.OG = paramComparisonFilter;
    this.OH = paramFieldOnlyFilter;
    this.OI = paramLogicalFilter;
    this.OJ = paramNotFilter;
    this.OK = paramInFilter;
    this.OL = paramMatchAllFilter;
    this.OM = paramHasFilter;
    this.ON = paramFullTextSearchFilter;
    this.OO = paramOwnedByMeFilter;
    if (this.OG != null)
    {
      this.JD = this.OG;
      return;
    }
    if (this.OH != null)
    {
      this.JD = this.OH;
      return;
    }
    if (this.OI != null)
    {
      this.JD = this.OI;
      return;
    }
    if (this.OJ != null)
    {
      this.JD = this.OJ;
      return;
    }
    if (this.OK != null)
    {
      this.JD = this.OK;
      return;
    }
    if (this.OL != null)
    {
      this.JD = this.OL;
      return;
    }
    if (this.OM != null)
    {
      this.JD = this.OM;
      return;
    }
    if (this.ON != null)
    {
      this.JD = this.ON;
      return;
    }
    if (this.OO != null)
    {
      this.JD = this.OO;
      return;
    }
    throw new IllegalArgumentException("At least one filter must be set.");
  }
  
  public FilterHolder(Filter paramFilter)
  {
    zzaa.zzb(paramFilter, "Null filter.");
    this.mVersionCode = 2;
    if ((paramFilter instanceof ComparisonFilter))
    {
      localObject = (ComparisonFilter)paramFilter;
      this.OG = ((ComparisonFilter)localObject);
      if (!(paramFilter instanceof FieldOnlyFilter)) {
        break label247;
      }
      localObject = (FieldOnlyFilter)paramFilter;
      label45:
      this.OH = ((FieldOnlyFilter)localObject);
      if (!(paramFilter instanceof LogicalFilter)) {
        break label252;
      }
      localObject = (LogicalFilter)paramFilter;
      label62:
      this.OI = ((LogicalFilter)localObject);
      if (!(paramFilter instanceof NotFilter)) {
        break label257;
      }
      localObject = (NotFilter)paramFilter;
      label79:
      this.OJ = ((NotFilter)localObject);
      if (!(paramFilter instanceof InFilter)) {
        break label262;
      }
      localObject = (InFilter)paramFilter;
      label96:
      this.OK = ((InFilter)localObject);
      if (!(paramFilter instanceof MatchAllFilter)) {
        break label267;
      }
      localObject = (MatchAllFilter)paramFilter;
      label113:
      this.OL = ((MatchAllFilter)localObject);
      if (!(paramFilter instanceof HasFilter)) {
        break label272;
      }
      localObject = (HasFilter)paramFilter;
      label130:
      this.OM = ((HasFilter)localObject);
      if (!(paramFilter instanceof FullTextSearchFilter)) {
        break label277;
      }
      localObject = (FullTextSearchFilter)paramFilter;
      label147:
      this.ON = ((FullTextSearchFilter)localObject);
      if (!(paramFilter instanceof OwnedByMeFilter)) {
        break label282;
      }
    }
    label247:
    label252:
    label257:
    label262:
    label267:
    label272:
    label277:
    label282:
    for (Object localObject = (OwnedByMeFilter)paramFilter;; localObject = null)
    {
      this.OO = ((OwnedByMeFilter)localObject);
      if ((this.OG != null) || (this.OH != null) || (this.OI != null) || (this.OJ != null) || (this.OK != null) || (this.OL != null) || (this.OM != null) || (this.ON != null) || (this.OO != null)) {
        break label287;
      }
      throw new IllegalArgumentException("Invalid filter type.");
      localObject = null;
      break;
      localObject = null;
      break label45;
      localObject = null;
      break label62;
      localObject = null;
      break label79;
      localObject = null;
      break label96;
      localObject = null;
      break label113;
      localObject = null;
      break label130;
      localObject = null;
      break label147;
    }
    label287:
    this.JD = paramFilter;
  }
  
  public Filter getFilter()
  {
    return this.JD;
  }
  
  public String toString()
  {
    return String.format("FilterHolder[%s]", new Object[] { this.JD });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzd.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/query/internal/FilterHolder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */