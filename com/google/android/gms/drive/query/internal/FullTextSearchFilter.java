package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;

public class FullTextSearchFilter
  extends AbstractFilter
{
  public static final Parcelable.Creator<FullTextSearchFilter> CREATOR = new zzh();
  final String mValue;
  final int mVersionCode;
  
  FullTextSearchFilter(int paramInt, String paramString)
  {
    this.mVersionCode = paramInt;
    this.mValue = paramString;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzh.zza(this, paramParcel, paramInt);
  }
  
  public <F> F zza(zzf<F> paramzzf)
  {
    return (F)paramzzf.zzja(this.mValue);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/query/internal/FullTextSearchFilter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */