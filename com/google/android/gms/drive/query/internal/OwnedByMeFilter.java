package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;

public class OwnedByMeFilter
  extends AbstractFilter
{
  public static final Parcelable.Creator<OwnedByMeFilter> CREATOR = new zzo();
  final int mVersionCode;
  
  public OwnedByMeFilter()
  {
    this(1);
  }
  
  OwnedByMeFilter(int paramInt)
  {
    this.mVersionCode = paramInt;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzo.zza(this, paramParcel, paramInt);
  }
  
  public <F> F zza(zzf<F> paramzzf)
  {
    return (F)paramzzf.zzbdj();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/query/internal/OwnedByMeFilter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */