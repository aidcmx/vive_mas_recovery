package com.google.android.gms.drive.query.internal;

import com.google.android.gms.drive.metadata.MetadataField;
import com.google.android.gms.drive.metadata.zzb;
import java.util.List;

public abstract interface zzf<F>
{
  public abstract F zzab(F paramF);
  
  public abstract <T> F zzb(zzb<T> paramzzb, T paramT);
  
  public abstract <T> F zzb(Operator paramOperator, MetadataField<T> paramMetadataField, T paramT);
  
  public abstract F zzb(Operator paramOperator, List<F> paramList);
  
  public abstract F zzbdj();
  
  public abstract F zzbdk();
  
  public abstract F zze(MetadataField<?> paramMetadataField);
  
  public abstract <T> F zze(MetadataField<T> paramMetadataField, T paramT);
  
  public abstract F zzja(String paramString);
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/query/internal/zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */