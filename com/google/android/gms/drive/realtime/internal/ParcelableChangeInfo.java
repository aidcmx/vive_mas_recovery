package com.google.android.gms.drive.realtime.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.drive.realtime.internal.event.ParcelableEvent;
import java.util.List;

public class ParcelableChangeInfo
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<ParcelableChangeInfo> CREATOR = new zzp();
  final int mVersionCode;
  final long vO;
  final List<ParcelableEvent> zzani;
  
  ParcelableChangeInfo(int paramInt, long paramLong, List<ParcelableEvent> paramList)
  {
    this.mVersionCode = paramInt;
    this.vO = paramLong;
    this.zzani = paramList;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzp.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/realtime/internal/ParcelableChangeInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */