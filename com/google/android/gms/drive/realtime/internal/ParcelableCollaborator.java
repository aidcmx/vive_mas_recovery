package com.google.android.gms.drive.realtime.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public class ParcelableCollaborator
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<ParcelableCollaborator> CREATOR = new zzq();
  final boolean Pe;
  final String Pf;
  final String Pg;
  final String ck;
  final String jh;
  final int mVersionCode;
  final boolean wh;
  final String zzctq;
  
  ParcelableCollaborator(int paramInt, boolean paramBoolean1, boolean paramBoolean2, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
  {
    this.mVersionCode = paramInt;
    this.Pe = paramBoolean1;
    this.wh = paramBoolean2;
    this.zzctq = paramString1;
    this.ck = paramString2;
    this.jh = paramString3;
    this.Pf = paramString4;
    this.Pg = paramString5;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if (!(paramObject instanceof ParcelableCollaborator)) {
      return false;
    }
    paramObject = (ParcelableCollaborator)paramObject;
    return this.zzctq.equals(((ParcelableCollaborator)paramObject).zzctq);
  }
  
  public int hashCode()
  {
    return this.zzctq.hashCode();
  }
  
  public String toString()
  {
    boolean bool1 = this.Pe;
    boolean bool2 = this.wh;
    String str1 = this.zzctq;
    String str2 = this.ck;
    String str3 = this.jh;
    String str4 = this.Pf;
    String str5 = this.Pg;
    return String.valueOf(str1).length() + 98 + String.valueOf(str2).length() + String.valueOf(str3).length() + String.valueOf(str4).length() + String.valueOf(str5).length() + "Collaborator [isMe=" + bool1 + ", isAnonymous=" + bool2 + ", sessionId=" + str1 + ", userId=" + str2 + ", displayName=" + str3 + ", color=" + str4 + ", photoUrl=" + str5 + "]";
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzq.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/realtime/internal/ParcelableCollaborator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */