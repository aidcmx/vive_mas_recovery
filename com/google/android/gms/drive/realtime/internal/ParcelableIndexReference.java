package com.google.android.gms.drive.realtime.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public class ParcelableIndexReference
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<ParcelableIndexReference> CREATOR = new zzr();
  final String Ph;
  final boolean Pi;
  final int Pj;
  final int mIndex;
  final int mVersionCode;
  
  ParcelableIndexReference(int paramInt1, String paramString, int paramInt2, boolean paramBoolean, int paramInt3)
  {
    this.mVersionCode = paramInt1;
    this.Ph = paramString;
    this.mIndex = paramInt2;
    this.Pi = paramBoolean;
    this.Pj = paramInt3;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzr.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/realtime/internal/ParcelableIndexReference.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */