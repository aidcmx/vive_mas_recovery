package com.google.android.gms.drive.realtime.internal.event;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.util.List;

public class ParcelableEvent
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<ParcelableEvent> CREATOR = new zzc();
  final FieldChangedDetails PA;
  final String Ph;
  final List<String> Pn;
  final boolean Po;
  final boolean Pp;
  final boolean Pq;
  final String Pr;
  final TextInsertedDetails Ps;
  final TextDeletedDetails Pt;
  final ValuesAddedDetails Pu;
  final ValuesRemovedDetails Pv;
  final ValuesSetDetails Pw;
  final ValueChangedDetails Px;
  final ReferenceShiftedDetails Py;
  final ObjectChangedDetails Pz;
  final String ck;
  final int mVersionCode;
  final String zzctq;
  
  ParcelableEvent(int paramInt, String paramString1, String paramString2, List<String> paramList, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, String paramString3, String paramString4, TextInsertedDetails paramTextInsertedDetails, TextDeletedDetails paramTextDeletedDetails, ValuesAddedDetails paramValuesAddedDetails, ValuesRemovedDetails paramValuesRemovedDetails, ValuesSetDetails paramValuesSetDetails, ValueChangedDetails paramValueChangedDetails, ReferenceShiftedDetails paramReferenceShiftedDetails, ObjectChangedDetails paramObjectChangedDetails, FieldChangedDetails paramFieldChangedDetails)
  {
    this.mVersionCode = paramInt;
    this.zzctq = paramString1;
    this.ck = paramString2;
    this.Pn = paramList;
    this.Po = paramBoolean1;
    this.Pp = paramBoolean2;
    this.Pq = paramBoolean3;
    this.Ph = paramString3;
    this.Pr = paramString4;
    this.Ps = paramTextInsertedDetails;
    this.Pt = paramTextDeletedDetails;
    this.Pu = paramValuesAddedDetails;
    this.Pv = paramValuesRemovedDetails;
    this.Pw = paramValuesSetDetails;
    this.Px = paramValueChangedDetails;
    this.Py = paramReferenceShiftedDetails;
    this.Pz = paramObjectChangedDetails;
    this.PA = paramFieldChangedDetails;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzc.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/realtime/internal/event/ParcelableEvent.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */