package com.google.android.gms.drive.realtime.internal.event;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zze
  implements Parcelable.Creator<ReferenceShiftedDetails>
{
  static void zza(ReferenceShiftedDetails paramReferenceShiftedDetails, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramReferenceShiftedDetails.mVersionCode);
    zzb.zza(paramParcel, 2, paramReferenceShiftedDetails.PF, false);
    zzb.zza(paramParcel, 3, paramReferenceShiftedDetails.PG, false);
    zzb.zzc(paramParcel, 4, paramReferenceShiftedDetails.PH);
    zzb.zzc(paramParcel, 5, paramReferenceShiftedDetails.PI);
    zzb.zzaj(paramParcel, paramInt);
  }
  
  public ReferenceShiftedDetails zzid(Parcel paramParcel)
  {
    String str1 = null;
    int i = 0;
    int m = zza.zzcr(paramParcel);
    int j = 0;
    String str2 = null;
    int k = 0;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.zzcq(paramParcel);
      switch (zza.zzgu(n))
      {
      default: 
        zza.zzb(paramParcel, n);
        break;
      case 1: 
        k = zza.zzg(paramParcel, n);
        break;
      case 2: 
        str2 = zza.zzq(paramParcel, n);
        break;
      case 3: 
        str1 = zza.zzq(paramParcel, n);
        break;
      case 4: 
        j = zza.zzg(paramParcel, n);
        break;
      case 5: 
        i = zza.zzg(paramParcel, n);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza(37 + "Overread allowed size end=" + m, paramParcel);
    }
    return new ReferenceShiftedDetails(k, str2, str1, j, i);
  }
  
  public ReferenceShiftedDetails[] zznc(int paramInt)
  {
    return new ReferenceShiftedDetails[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/realtime/internal/event/zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */