package com.google.android.gms.drive.realtime.internal.event;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzf
  implements Parcelable.Creator<TextDeletedDetails>
{
  static void zza(TextDeletedDetails paramTextDeletedDetails, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramTextDeletedDetails.mVersionCode);
    zzb.zzc(paramParcel, 2, paramTextDeletedDetails.mIndex);
    zzb.zzc(paramParcel, 3, paramTextDeletedDetails.PJ);
    zzb.zzaj(paramParcel, paramInt);
  }
  
  public TextDeletedDetails zzie(Parcel paramParcel)
  {
    int k = 0;
    int m = zza.zzcr(paramParcel);
    int j = 0;
    int i = 0;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.zzcq(paramParcel);
      switch (zza.zzgu(n))
      {
      default: 
        zza.zzb(paramParcel, n);
        break;
      case 1: 
        i = zza.zzg(paramParcel, n);
        break;
      case 2: 
        j = zza.zzg(paramParcel, n);
        break;
      case 3: 
        k = zza.zzg(paramParcel, n);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza(37 + "Overread allowed size end=" + m, paramParcel);
    }
    return new TextDeletedDetails(i, j, k);
  }
  
  public TextDeletedDetails[] zznd(int paramInt)
  {
    return new TextDeletedDetails[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/realtime/internal/event/zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */