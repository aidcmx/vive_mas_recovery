package com.google.android.gms.drive;

public class zzh
  extends ExecutionOptions
{
  private String Jt;
  private String Ju;
  
  private zzh(String paramString1, boolean paramBoolean, String paramString2, String paramString3, int paramInt)
  {
    super(paramString1, paramBoolean, paramInt);
    this.Jt = paramString2;
    this.Ju = paramString3;
  }
  
  public static zzh zza(ExecutionOptions paramExecutionOptions)
  {
    zza localzza = new zza();
    if (paramExecutionOptions != null)
    {
      if (paramExecutionOptions.zzbax() != 0) {
        throw new IllegalStateException("May not set a conflict strategy for new file creation.");
      }
      Object localObject = paramExecutionOptions.zzbav();
      if (localObject != null) {
        localObject = (zza)localzza.setTrackingTag((String)localObject);
      }
      paramExecutionOptions = (zza)localzza.setNotifyOnCompletion(paramExecutionOptions.zzbaw());
    }
    return (zzh)localzza.build();
  }
  
  public String zzbaz()
  {
    return this.Jt;
  }
  
  public String zzbba()
  {
    return this.Ju;
  }
  
  public static class zza
    extends ExecutionOptions.Builder
  {
    public zzh zzbbb()
    {
      zzbay();
      return new zzh(this.Jq, this.Jr, null, null, this.Js, null);
    }
    
    public zza zzbu(boolean paramBoolean)
    {
      super.setNotifyOnCompletion(paramBoolean);
      return this;
    }
    
    public zza zzip(String paramString)
    {
      super.setTrackingTag(paramString);
      return this;
    }
    
    public zza zzix(int paramInt)
    {
      throw new UnsupportedOperationException();
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/zzh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */