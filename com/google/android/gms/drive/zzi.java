package com.google.android.gms.drive;

public class zzi
  extends ExecutionOptions
{
  private boolean Jv;
  
  private zzi(String paramString, boolean paramBoolean1, int paramInt, boolean paramBoolean2)
  {
    super(paramString, paramBoolean1, paramInt);
    this.Jv = paramBoolean2;
  }
  
  public static zzi zzb(ExecutionOptions paramExecutionOptions)
  {
    zza localzza1 = new zza();
    if (paramExecutionOptions != null)
    {
      zza localzza2 = (zza)localzza1.setConflictStrategy(paramExecutionOptions.zzbax());
      localzza2 = (zza)localzza1.setNotifyOnCompletion(paramExecutionOptions.zzbaw());
      paramExecutionOptions = paramExecutionOptions.zzbav();
      if (paramExecutionOptions != null) {
        paramExecutionOptions = (zza)localzza1.setTrackingTag(paramExecutionOptions);
      }
    }
    return (zzi)localzza1.build();
  }
  
  public boolean zzbbc()
  {
    return this.Jv;
  }
  
  public static class zza
    extends ExecutionOptions.Builder
  {
    private boolean Jv = true;
    
    public zzi zzbbd()
    {
      zzbay();
      return new zzi(this.Jq, this.Jr, this.Js, this.Jv, null);
    }
    
    public zza zzbv(boolean paramBoolean)
    {
      super.setNotifyOnCompletion(paramBoolean);
      return this;
    }
    
    public zza zziq(String paramString)
    {
      super.setTrackingTag(paramString);
      return this;
    }
    
    public zza zziy(int paramInt)
    {
      super.setConflictStrategy(paramInt);
      return this;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/zzi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */