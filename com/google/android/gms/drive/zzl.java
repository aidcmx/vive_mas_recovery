package com.google.android.gms.drive;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzl
  implements Parcelable.Creator<UserMetadata>
{
  static void zza(UserMetadata paramUserMetadata, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramUserMetadata.mVersionCode);
    zzb.zza(paramParcel, 2, paramUserMetadata.JN, false);
    zzb.zza(paramParcel, 3, paramUserMetadata.jh, false);
    zzb.zza(paramParcel, 4, paramUserMetadata.JO, false);
    zzb.zza(paramParcel, 5, paramUserMetadata.JP);
    zzb.zza(paramParcel, 6, paramUserMetadata.JQ, false);
    zzb.zzaj(paramParcel, paramInt);
  }
  
  public UserMetadata zzel(Parcel paramParcel)
  {
    boolean bool = false;
    String str1 = null;
    int j = zza.zzcr(paramParcel);
    String str2 = null;
    String str3 = null;
    String str4 = null;
    int i = 0;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.zzcq(paramParcel);
      switch (zza.zzgu(k))
      {
      default: 
        zza.zzb(paramParcel, k);
        break;
      case 1: 
        i = zza.zzg(paramParcel, k);
        break;
      case 2: 
        str4 = zza.zzq(paramParcel, k);
        break;
      case 3: 
        str3 = zza.zzq(paramParcel, k);
        break;
      case 4: 
        str2 = zza.zzq(paramParcel, k);
        break;
      case 5: 
        bool = zza.zzc(paramParcel, k);
        break;
      case 6: 
        str1 = zza.zzq(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new UserMetadata(i, str4, str3, str2, bool, str1);
  }
  
  public UserMetadata[] zzjd(int paramInt)
  {
    return new UserMetadata[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/drive/zzl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */