package com.google.android.gms.dynamite.descriptors.com.google.android.gms.firebase_database;

import com.google.android.gms.common.util.DynamiteApi;

@DynamiteApi
public class ModuleDescriptor
{
  public static final String MODULE_ID = "com.google.android.gms.firebase_database";
  public static final int MODULE_VERSION = 4;
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/dynamite/descriptors/com/google/android/gms/firebase_database/ModuleDescriptor.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */