package com.google.android.gms.fitness;

public class FitnessActivities
{
  public static final String AEROBICS = "aerobics";
  public static final String BADMINTON = "badminton";
  public static final String BASEBALL = "baseball";
  public static final String BASKETBALL = "basketball";
  public static final String BIATHLON = "biathlon";
  public static final String BIKING = "biking";
  public static final String BIKING_HAND = "biking.hand";
  public static final String BIKING_MOUNTAIN = "biking.mountain";
  public static final String BIKING_ROAD = "biking.road";
  public static final String BIKING_SPINNING = "biking.spinning";
  public static final String BIKING_STATIONARY = "biking.stationary";
  public static final String BIKING_UTILITY = "biking.utility";
  public static final String BOXING = "boxing";
  public static final String CALISTHENICS = "calisthenics";
  public static final String CIRCUIT_TRAINING = "circuit_training";
  public static final String CRICKET = "cricket";
  public static final String CROSSFIT = "crossfit";
  public static final String CURLING = "curling";
  public static final String DANCING = "dancing";
  public static final String DIVING = "diving";
  public static final String ELEVATOR = "elevator";
  public static final String ELLIPTICAL = "elliptical";
  public static final String ERGOMETER = "ergometer";
  public static final String ESCALATOR = "escalator";
  public static final String EXTRA_STATUS = "actionStatus";
  public static final String FENCING = "fencing";
  public static final String FOOTBALL_AMERICAN = "football.american";
  public static final String FOOTBALL_AUSTRALIAN = "football.australian";
  public static final String FOOTBALL_SOCCER = "football.soccer";
  public static final String FRISBEE_DISC = "frisbee_disc";
  public static final String GARDENING = "gardening";
  public static final String GOLF = "golf";
  public static final String GYMNASTICS = "gymnastics";
  public static final String HANDBALL = "handball";
  public static final String HIGH_INTENSITY_INTERVAL_TRAINING = "interval_training.high_intensity";
  public static final String HIKING = "hiking";
  public static final String HOCKEY = "hockey";
  public static final String HORSEBACK_RIDING = "horseback_riding";
  public static final String HOUSEWORK = "housework";
  public static final String ICE_SKATING = "ice_skating";
  public static final String INTERVAL_TRAINING = "interval_training";
  public static final String IN_VEHICLE = "in_vehicle";
  public static final String JUMP_ROPE = "jump_rope";
  public static final String KAYAKING = "kayaking";
  public static final String KETTLEBELL_TRAINING = "kettlebell_training";
  public static final String KICKBOXING = "kickboxing";
  public static final String KICK_SCOOTER = "kick_scooter";
  public static final String KITESURFING = "kitesurfing";
  public static final String MARTIAL_ARTS = "martial_arts";
  public static final String MEDITATION = "meditation";
  public static final String MIME_TYPE_PREFIX = "vnd.google.fitness.activity/";
  public static final String MIXED_MARTIAL_ARTS = "martial_arts.mixed";
  public static final String ON_FOOT = "on_foot";
  public static final String OTHER = "other";
  public static final String P90X = "p90x";
  public static final String PARAGLIDING = "paragliding";
  public static final String PILATES = "pilates";
  public static final String POLO = "polo";
  private static final String[] Qx = new String[119];
  public static final String RACQUETBALL = "racquetball";
  public static final String ROCK_CLIMBING = "rock_climbing";
  public static final String ROWING = "rowing";
  public static final String ROWING_MACHINE = "rowing.machine";
  public static final String RUGBY = "rugby";
  public static final String RUNNING = "running";
  public static final String RUNNING_JOGGING = "running.jogging";
  public static final String RUNNING_SAND = "running.sand";
  public static final String RUNNING_TREADMILL = "running.treadmill";
  public static final String SAILING = "sailing";
  public static final String SCUBA_DIVING = "scuba_diving";
  public static final String SKATEBOARDING = "skateboarding";
  public static final String SKATING = "skating";
  public static final String SKATING_CROSS = "skating.cross";
  public static final String SKATING_INDOOR = "skating.indoor";
  public static final String SKATING_INLINE = "skating.inline";
  public static final String SKIING = "skiing";
  public static final String SKIING_BACK_COUNTRY = "skiing.back_country";
  public static final String SKIING_CROSS_COUNTRY = "skiing.cross_country";
  public static final String SKIING_DOWNHILL = "skiing.downhill";
  public static final String SKIING_KITE = "skiing.kite";
  public static final String SKIING_ROLLER = "skiing.roller";
  public static final String SLEDDING = "sledding";
  public static final String SLEEP = "sleep";
  public static final String SLEEP_AWAKE = "sleep.awake";
  public static final String SLEEP_DEEP = "sleep.deep";
  public static final String SLEEP_LIGHT = "sleep.light";
  public static final String SLEEP_REM = "sleep.rem";
  public static final String SNOWBOARDING = "snowboarding";
  public static final String SNOWMOBILE = "snowmobile";
  public static final String SNOWSHOEING = "snowshoeing";
  public static final String SQUASH = "squash";
  public static final String STAIR_CLIMBING = "stair_climbing";
  public static final String STAIR_CLIMBING_MACHINE = "stair_climbing.machine";
  public static final String STANDUP_PADDLEBOARDING = "standup_paddleboarding";
  public static final String STATUS_ACTIVE = "ActiveActionStatus";
  public static final String STATUS_COMPLETED = "CompletedActionStatus";
  public static final String STILL = "still";
  public static final String STRENGTH_TRAINING = "strength_training";
  public static final String SURFING = "surfing";
  public static final String SWIMMING = "swimming";
  public static final String SWIMMING_OPEN_WATER = "swimming.open_water";
  public static final String SWIMMING_POOL = "swimming.pool";
  public static final String TABLE_TENNIS = "table_tennis";
  public static final String TEAM_SPORTS = "team_sports";
  public static final String TENNIS = "tennis";
  public static final String TILTING = "tilting";
  public static final String TREADMILL = "treadmill";
  public static final String UNKNOWN = "unknown";
  public static final String VOLLEYBALL = "volleyball";
  public static final String VOLLEYBALL_BEACH = "volleyball.beach";
  public static final String VOLLEYBALL_INDOOR = "volleyball.indoor";
  public static final String WAKEBOARDING = "wakeboarding";
  public static final String WALKING = "walking";
  public static final String WALKING_FITNESS = "walking.fitness";
  public static final String WALKING_NORDIC = "walking.nordic";
  public static final String WALKING_STROLLER = "walking.stroller";
  public static final String WALKING_TREADMILL = "walking.treadmill";
  public static final String WATER_POLO = "water_polo";
  public static final String WEIGHTLIFTING = "weightlifting";
  public static final String WHEELCHAIR = "wheelchair";
  public static final String WINDSURFING = "windsurfing";
  public static final String YOGA = "yoga";
  public static final String ZUMBA = "zumba";
  
  static
  {
    Qx[9] = "aerobics";
    Qx[10] = "badminton";
    Qx[11] = "baseball";
    Qx[12] = "basketball";
    Qx[13] = "biathlon";
    Qx[1] = "biking";
    Qx[14] = "biking.hand";
    Qx[15] = "biking.mountain";
    Qx[16] = "biking.road";
    Qx[17] = "biking.spinning";
    Qx[18] = "biking.stationary";
    Qx[19] = "biking.utility";
    Qx[20] = "boxing";
    Qx[21] = "calisthenics";
    Qx[22] = "circuit_training";
    Qx[23] = "cricket";
    Qx[113] = "crossfit";
    Qx[106] = "curling";
    Qx[24] = "dancing";
    Qx[102] = "diving";
    Qx[117] = "elevator";
    Qx[25] = "elliptical";
    Qx[103] = "ergometer";
    Qx[118] = "escalator";
    Qx[6] = "exiting_vehicle";
    Qx[26] = "fencing";
    Qx[27] = "football.american";
    Qx[28] = "football.australian";
    Qx[29] = "football.soccer";
    Qx[30] = "frisbee_disc";
    Qx[31] = "gardening";
    Qx[32] = "golf";
    Qx[33] = "gymnastics";
    Qx[34] = "handball";
    Qx[114] = "interval_training.high_intensity";
    Qx[35] = "hiking";
    Qx[36] = "hockey";
    Qx[37] = "horseback_riding";
    Qx[38] = "housework";
    Qx[104] = "ice_skating";
    Qx[0] = "in_vehicle";
    Qx[115] = "interval_training";
    Qx[39] = "jump_rope";
    Qx[40] = "kayaking";
    Qx[41] = "kettlebell_training";
    Qx[107] = "kick_scooter";
    Qx[42] = "kickboxing";
    Qx[43] = "kitesurfing";
    Qx[44] = "martial_arts";
    Qx[45] = "meditation";
    Qx[46] = "martial_arts.mixed";
    Qx[2] = "on_foot";
    Qx[108] = "other";
    Qx[47] = "p90x";
    Qx[48] = "paragliding";
    Qx[49] = "pilates";
    Qx[50] = "polo";
    Qx[51] = "racquetball";
    Qx[52] = "rock_climbing";
    Qx[53] = "rowing";
    Qx[54] = "rowing.machine";
    Qx[55] = "rugby";
    Qx[8] = "running";
    Qx[56] = "running.jogging";
    Qx[57] = "running.sand";
    Qx[58] = "running.treadmill";
    Qx[59] = "sailing";
    Qx[60] = "scuba_diving";
    Qx[61] = "skateboarding";
    Qx[62] = "skating";
    Qx[63] = "skating.cross";
    Qx[105] = "skating.indoor";
    Qx[64] = "skating.inline";
    Qx[65] = "skiing";
    Qx[66] = "skiing.back_country";
    Qx[67] = "skiing.cross_country";
    Qx[68] = "skiing.downhill";
    Qx[69] = "skiing.kite";
    Qx[70] = "skiing.roller";
    Qx[71] = "sledding";
    Qx[72] = "sleep";
    Qx[109] = "sleep.light";
    Qx[110] = "sleep.deep";
    Qx[111] = "sleep.rem";
    Qx[112] = "sleep.awake";
    Qx[73] = "snowboarding";
    Qx[74] = "snowmobile";
    Qx[75] = "snowshoeing";
    Qx[76] = "squash";
    Qx[77] = "stair_climbing";
    Qx[78] = "stair_climbing.machine";
    Qx[79] = "standup_paddleboarding";
    Qx[3] = "still";
    Qx[80] = "strength_training";
    Qx[81] = "surfing";
    Qx[82] = "swimming";
    Qx[83] = "swimming.pool";
    Qx[84] = "swimming.open_water";
    Qx[85] = "table_tennis";
    Qx[86] = "team_sports";
    Qx[87] = "tennis";
    Qx[5] = "tilting";
    Qx[88] = "treadmill";
    Qx[4] = "unknown";
    Qx[89] = "volleyball";
    Qx[90] = "volleyball.beach";
    Qx[91] = "volleyball.indoor";
    Qx[92] = "wakeboarding";
    Qx[7] = "walking";
    Qx[93] = "walking.fitness";
    Qx[94] = "walking.nordic";
    Qx[95] = "walking.treadmill";
    Qx[116] = "walking.stroller";
    Qx[96] = "water_polo";
    Qx[97] = "weightlifting";
    Qx[98] = "wheelchair";
    Qx[99] = "windsurfing";
    Qx[100] = "yoga";
    Qx[101] = "zumba";
  }
  
  public static String getMimeType(String paramString)
  {
    String str = String.valueOf("vnd.google.fitness.activity/");
    paramString = String.valueOf(paramString);
    if (paramString.length() != 0) {
      return str.concat(paramString);
    }
    return new String(str);
  }
  
  public static String getName(int paramInt)
  {
    Object localObject;
    if ((paramInt < 0) || (paramInt >= Qx.length)) {
      localObject = "unknown";
    }
    String str;
    do
    {
      return (String)localObject;
      str = Qx[paramInt];
      localObject = str;
    } while (str != null);
    return "unknown";
  }
  
  public static int zzje(String paramString)
  {
    int i = 0;
    while (i < Qx.length)
    {
      if (Qx[i].equals(paramString)) {
        return i;
      }
      i += 1;
    }
    return 4;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/FitnessActivities.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */