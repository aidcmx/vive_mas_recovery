package com.google.android.gms.fitness;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.fitness.request.GoalsReadRequest;
import com.google.android.gms.fitness.result.GoalsResult;

public abstract interface GoalsApi
{
  public abstract PendingResult<GoalsResult> readCurrentGoals(GoogleApiClient paramGoogleApiClient, GoalsReadRequest paramGoalsReadRequest);
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/GoalsApi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */