package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zzz;

public final class Application
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<Application> CREATOR = new zzb();
  public static final Application QG = new Application("com.google.android.gms", null, null);
  private final String QH;
  private final String packageName;
  private final String version;
  private final int versionCode;
  
  Application(int paramInt, String paramString1, String paramString2, String paramString3)
  {
    this.versionCode = paramInt;
    this.packageName = ((String)zzaa.zzy(paramString1));
    this.version = "";
    this.QH = paramString3;
  }
  
  public Application(String paramString1, String paramString2, String paramString3)
  {
    this(1, paramString1, "", paramString3);
  }
  
  private boolean zza(Application paramApplication)
  {
    return (this.packageName.equals(paramApplication.packageName)) && (zzz.equal(this.version, paramApplication.version)) && (zzz.equal(this.QH, paramApplication.QH));
  }
  
  public static Application zzf(String paramString1, String paramString2, String paramString3)
  {
    if ("com.google.android.gms".equals(paramString1)) {
      return QG;
    }
    return new Application(paramString1, paramString2, paramString3);
  }
  
  public static Application zzjf(String paramString)
  {
    return zzf(paramString, null, null);
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof Application)) && (zza((Application)paramObject)));
  }
  
  public String getPackageName()
  {
    return this.packageName;
  }
  
  public String getVersion()
  {
    return this.version;
  }
  
  int getVersionCode()
  {
    return this.versionCode;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.packageName, this.version, this.QH });
  }
  
  public String toString()
  {
    return String.format("Application{%s:%s:%s}", new Object[] { this.packageName, this.version, this.QH });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzb.zza(this, paramParcel, paramInt);
  }
  
  public String zzbdv()
  {
    return this.QH;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/data/Application.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */