package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;
import com.google.android.gms.internal.zzto;
import java.util.Collections;
import java.util.List;

public class BleDevice
  extends AbstractSafeParcelable
  implements ReflectedParcelable
{
  public static final Parcelable.Creator<BleDevice> CREATOR = new zzc();
  private final String QI;
  private final List<String> QJ;
  private final List<DataType> QK;
  private final String mName;
  private final int mVersionCode;
  
  BleDevice(int paramInt, String paramString1, String paramString2, List<String> paramList, List<DataType> paramList1)
  {
    this.mVersionCode = paramInt;
    this.QI = paramString1;
    this.mName = paramString2;
    this.QJ = Collections.unmodifiableList(paramList);
    this.QK = Collections.unmodifiableList(paramList1);
  }
  
  private boolean zza(BleDevice paramBleDevice)
  {
    return (this.mName.equals(paramBleDevice.mName)) && (this.QI.equals(paramBleDevice.QI)) && (zzto.zza(paramBleDevice.QJ, this.QJ)) && (zzto.zza(this.QK, paramBleDevice.QK));
  }
  
  public boolean equals(Object paramObject)
  {
    return (paramObject == this) || (((paramObject instanceof BleDevice)) && (zza((BleDevice)paramObject)));
  }
  
  public String getAddress()
  {
    return this.QI;
  }
  
  public List<DataType> getDataTypes()
  {
    return this.QK;
  }
  
  public String getName()
  {
    return this.mName;
  }
  
  public List<String> getSupportedProfiles()
  {
    return this.QJ;
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.mName, this.QI, this.QJ, this.QK });
  }
  
  public String toString()
  {
    return zzz.zzx(this).zzg("name", this.mName).zzg("address", this.QI).zzg("dataTypes", this.QK).zzg("supportedProfiles", this.QJ).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzc.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/data/BleDevice.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */