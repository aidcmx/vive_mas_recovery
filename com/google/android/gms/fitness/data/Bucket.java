package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;
import com.google.android.gms.fitness.FitnessActivities;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Bucket
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<Bucket> CREATOR = new zzd();
  public static final int TYPE_ACTIVITY_SEGMENT = 4;
  public static final int TYPE_ACTIVITY_TYPE = 3;
  public static final int TYPE_SESSION = 2;
  public static final int TYPE_TIME = 1;
  private final Session QB;
  private final long QL;
  private final int QM;
  private final List<DataSet> QN;
  private final int QO;
  private boolean QP = false;
  private final long eg;
  private final int mVersionCode;
  
  Bucket(int paramInt1, long paramLong1, long paramLong2, Session paramSession, int paramInt2, List<DataSet> paramList, int paramInt3, boolean paramBoolean)
  {
    this.mVersionCode = paramInt1;
    this.eg = paramLong1;
    this.QL = paramLong2;
    this.QB = paramSession;
    this.QM = paramInt2;
    this.QN = paramList;
    this.QO = paramInt3;
    this.QP = paramBoolean;
  }
  
  public Bucket(RawBucket paramRawBucket, List<DataSource> paramList)
  {
    this(2, paramRawBucket.eg, paramRawBucket.QL, paramRawBucket.QB, paramRawBucket.Tp, zza(paramRawBucket.QN, paramList), paramRawBucket.QO, paramRawBucket.QP);
  }
  
  private static List<DataSet> zza(Collection<RawDataSet> paramCollection, List<DataSource> paramList)
  {
    ArrayList localArrayList = new ArrayList(paramCollection.size());
    paramCollection = paramCollection.iterator();
    while (paramCollection.hasNext()) {
      localArrayList.add(new DataSet((RawDataSet)paramCollection.next(), paramList));
    }
    return localArrayList;
  }
  
  private boolean zza(Bucket paramBucket)
  {
    return (this.eg == paramBucket.eg) && (this.QL == paramBucket.QL) && (this.QM == paramBucket.QM) && (zzz.equal(this.QN, paramBucket.QN)) && (this.QO == paramBucket.QO) && (this.QP == paramBucket.QP);
  }
  
  public static String zznm(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return "bug";
    case 1: 
      return "time";
    case 3: 
      return "type";
    case 4: 
      return "segment";
    case 2: 
      return "session";
    }
    return "unknown";
  }
  
  public boolean equals(Object paramObject)
  {
    return (paramObject == this) || (((paramObject instanceof Bucket)) && (zza((Bucket)paramObject)));
  }
  
  public String getActivity()
  {
    return FitnessActivities.getName(this.QM);
  }
  
  public int getBucketType()
  {
    return this.QO;
  }
  
  public DataSet getDataSet(DataType paramDataType)
  {
    Iterator localIterator = this.QN.iterator();
    while (localIterator.hasNext())
    {
      DataSet localDataSet = (DataSet)localIterator.next();
      if (localDataSet.getDataType().equals(paramDataType)) {
        return localDataSet;
      }
    }
    return null;
  }
  
  public List<DataSet> getDataSets()
  {
    return this.QN;
  }
  
  public long getEndTime(TimeUnit paramTimeUnit)
  {
    return paramTimeUnit.convert(this.QL, TimeUnit.MILLISECONDS);
  }
  
  public Session getSession()
  {
    return this.QB;
  }
  
  public long getStartTime(TimeUnit paramTimeUnit)
  {
    return paramTimeUnit.convert(this.eg, TimeUnit.MILLISECONDS);
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { Long.valueOf(this.eg), Long.valueOf(this.QL), Integer.valueOf(this.QM), Integer.valueOf(this.QO) });
  }
  
  public String toString()
  {
    return zzz.zzx(this).zzg("startTime", Long.valueOf(this.eg)).zzg("endTime", Long.valueOf(this.QL)).zzg("activity", Integer.valueOf(this.QM)).zzg("dataSets", this.QN).zzg("bucketType", zznm(this.QO)).zzg("serverHasMoreData", Boolean.valueOf(this.QP)).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzd.zza(this, paramParcel, paramInt);
  }
  
  public long zzagx()
  {
    return this.eg;
  }
  
  public boolean zzb(Bucket paramBucket)
  {
    return (this.eg == paramBucket.eg) && (this.QL == paramBucket.QL) && (this.QM == paramBucket.QM) && (this.QO == paramBucket.QO);
  }
  
  public long zzban()
  {
    return this.QL;
  }
  
  public int zzbdw()
  {
    return this.QM;
  }
  
  public boolean zzbdx()
  {
    if (this.QP) {
      return true;
    }
    Iterator localIterator = this.QN.iterator();
    while (localIterator.hasNext()) {
      if (((DataSet)localIterator.next()).zzbdx()) {
        return true;
      }
    }
    return false;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/data/Bucket.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */