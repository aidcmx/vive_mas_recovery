package com.google.android.gms.fitness.data;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.util.Log;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.zzc;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.internal.zztp;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

public final class DataPoint
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<DataPoint> CREATOR = new zzg();
  private long QS;
  private long QT;
  private final Value[] QU;
  private DataSource QV;
  private long QW;
  private long QX;
  private final DataSource Qz;
  private final int versionCode;
  
  DataPoint(int paramInt, DataSource paramDataSource1, long paramLong1, long paramLong2, Value[] paramArrayOfValue, DataSource paramDataSource2, long paramLong3, long paramLong4)
  {
    this.versionCode = paramInt;
    this.Qz = paramDataSource1;
    this.QV = paramDataSource2;
    this.QS = paramLong1;
    this.QT = paramLong2;
    this.QU = paramArrayOfValue;
    this.QW = paramLong3;
    this.QX = paramLong4;
  }
  
  private DataPoint(DataSource paramDataSource)
  {
    this.versionCode = 4;
    this.Qz = ((DataSource)zzaa.zzb(paramDataSource, "Data source cannot be null"));
    paramDataSource = paramDataSource.getDataType().getFields();
    this.QU = new Value[paramDataSource.size()];
    paramDataSource = paramDataSource.iterator();
    int i = 0;
    while (paramDataSource.hasNext())
    {
      Field localField = (Field)paramDataSource.next();
      this.QU[i] = new Value(localField.getFormat());
      i += 1;
    }
  }
  
  public DataPoint(DataSource paramDataSource1, DataSource paramDataSource2, RawDataPoint paramRawDataPoint)
  {
    this(4, paramDataSource1, zza(Long.valueOf(paramRawDataPoint.Tq), 0L), zza(Long.valueOf(paramRawDataPoint.Tr), 0L), paramRawDataPoint.Ts, paramDataSource2, zza(Long.valueOf(paramRawDataPoint.Tv), 0L), zza(Long.valueOf(paramRawDataPoint.Tw), 0L));
  }
  
  DataPoint(List<DataSource> paramList, RawDataPoint paramRawDataPoint)
  {
    this(zzc(paramList, paramRawDataPoint.Tt), zzc(paramList, paramRawDataPoint.Tu), paramRawDataPoint);
  }
  
  public static DataPoint create(DataSource paramDataSource)
  {
    return new DataPoint(paramDataSource);
  }
  
  public static DataPoint extract(Intent paramIntent)
  {
    if (paramIntent == null) {
      return null;
    }
    return (DataPoint)zzc.zza(paramIntent, "com.google.android.gms.fitness.EXTRA_DATA_POINT", CREATOR);
  }
  
  private static long zza(Long paramLong, long paramLong1)
  {
    if (paramLong != null) {
      paramLong1 = paramLong.longValue();
    }
    return paramLong1;
  }
  
  private boolean zzbdz()
  {
    return getDataType() == DataType.TYPE_LOCATION_SAMPLE;
  }
  
  private static DataSource zzc(List<DataSource> paramList, int paramInt)
  {
    if ((paramInt >= 0) && (paramInt < paramList.size())) {
      return (DataSource)paramList.get(paramInt);
    }
    return null;
  }
  
  private boolean zzc(DataPoint paramDataPoint)
  {
    return (zzz.equal(this.Qz, paramDataPoint.Qz)) && (this.QS == paramDataPoint.QS) && (this.QT == paramDataPoint.QT) && (Arrays.equals(this.QU, paramDataPoint.QU)) && (zzz.equal(getOriginalDataSource(), paramDataPoint.getOriginalDataSource()));
  }
  
  private void zznp(int paramInt)
  {
    List localList = getDataType().getFields();
    int i = localList.size();
    if (paramInt == i) {}
    for (boolean bool = true;; bool = false)
    {
      zzaa.zzb(bool, "Attempting to insert %s values, but needed %s: %s", new Object[] { Integer.valueOf(paramInt), Integer.valueOf(i), localList });
      return;
    }
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof DataPoint)) && (zzc((DataPoint)paramObject)));
  }
  
  public DataSource getDataSource()
  {
    return this.Qz;
  }
  
  public DataType getDataType()
  {
    return this.Qz.getDataType();
  }
  
  public long getEndTime(TimeUnit paramTimeUnit)
  {
    return paramTimeUnit.convert(this.QS, TimeUnit.NANOSECONDS);
  }
  
  public DataSource getOriginalDataSource()
  {
    if (this.QV != null) {
      return this.QV;
    }
    return this.Qz;
  }
  
  public long getStartTime(TimeUnit paramTimeUnit)
  {
    return paramTimeUnit.convert(this.QT, TimeUnit.NANOSECONDS);
  }
  
  public long getTimestamp(TimeUnit paramTimeUnit)
  {
    return paramTimeUnit.convert(this.QS, TimeUnit.NANOSECONDS);
  }
  
  public long getTimestampNanos()
  {
    return this.QS;
  }
  
  public Value getValue(Field paramField)
  {
    int i = getDataType().indexOf(paramField);
    return this.QU[i];
  }
  
  public int getVersionCode()
  {
    return this.versionCode;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.Qz, Long.valueOf(this.QS), Long.valueOf(this.QT) });
  }
  
  public DataPoint setFloatValues(float... paramVarArgs)
  {
    zznp(paramVarArgs.length);
    int i = 0;
    while (i < paramVarArgs.length)
    {
      this.QU[i].setFloat(paramVarArgs[i]);
      i += 1;
    }
    return this;
  }
  
  public DataPoint setIntValues(int... paramVarArgs)
  {
    zznp(paramVarArgs.length);
    int i = 0;
    while (i < paramVarArgs.length)
    {
      this.QU[i].setInt(paramVarArgs[i]);
      i += 1;
    }
    return this;
  }
  
  public DataPoint setTimeInterval(long paramLong1, long paramLong2, TimeUnit paramTimeUnit)
  {
    this.QT = paramTimeUnit.toNanos(paramLong1);
    this.QS = paramTimeUnit.toNanos(paramLong2);
    return this;
  }
  
  public DataPoint setTimestamp(long paramLong, TimeUnit paramTimeUnit)
  {
    this.QS = paramTimeUnit.toNanos(paramLong);
    if ((zzbdz()) && (zztp.zza(paramTimeUnit)))
    {
      Log.w("Fitness", "Storing location at more than millisecond granularity is not supported. Extra precision is lost as the value is converted to milliseconds.");
      this.QS = zztp.zza(this.QS, TimeUnit.NANOSECONDS, TimeUnit.MILLISECONDS);
    }
    return this;
  }
  
  public String toString()
  {
    String str2 = Arrays.toString(this.QU);
    long l1 = this.QT;
    long l2 = this.QS;
    long l3 = this.QW;
    long l4 = this.QX;
    String str3 = this.Qz.toDebugString();
    if (this.QV != null) {}
    for (String str1 = this.QV.toDebugString();; str1 = "N/A") {
      return String.format("DataPoint{%s@[%s, %s,raw=%s,insert=%s](%s %s)}", new Object[] { str2, Long.valueOf(l1), Long.valueOf(l2), Long.valueOf(l3), Long.valueOf(l4), str3, str1 });
    }
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzg.zza(this, paramParcel, paramInt);
  }
  
  public Value[] zzbea()
  {
    return this.QU;
  }
  
  public DataSource zzbeb()
  {
    return this.QV;
  }
  
  public long zzbec()
  {
    return this.QW;
  }
  
  public long zzbed()
  {
    return this.QX;
  }
  
  public void zzbee()
  {
    DataSource localDataSource = getDataSource();
    zzaa.zzb(getDataType().getName().equals(localDataSource.getDataType().getName()), "Conflicting data types found %s vs %s", new Object[] { getDataType(), getDataType() });
    if (this.QS > 0L)
    {
      bool = true;
      zzaa.zzb(bool, "Data point does not have the timestamp set: %s", new Object[] { this });
      if (this.QT > this.QS) {
        break label107;
      }
    }
    label107:
    for (boolean bool = true;; bool = false)
    {
      zzaa.zzb(bool, "Data point with start time greater than end time found: %s", new Object[] { this });
      return;
      bool = false;
      break;
    }
  }
  
  public long zzbef()
  {
    return this.QT;
  }
  
  public Value zzno(int paramInt)
  {
    DataType localDataType = getDataType();
    if ((paramInt < 0) || (paramInt >= localDataType.getFields().size())) {
      throw new IllegalArgumentException(String.format("fieldIndex %s is out of range for %s", new Object[] { Integer.valueOf(paramInt), localDataType }));
    }
    return this.QU[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/data/DataPoint.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */