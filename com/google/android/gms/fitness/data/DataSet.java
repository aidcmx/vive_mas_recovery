package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.util.Log;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.internal.zztv;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public final class DataSet
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<DataSet> CREATOR = new zzh();
  private final List<DataPoint> QY;
  private final List<DataSource> QZ;
  private final DataType Qy;
  private final DataSource Qz;
  private boolean Ra = false;
  private final int versionCode;
  
  DataSet(int paramInt, DataSource paramDataSource, DataType paramDataType, List<RawDataPoint> paramList, List<DataSource> paramList1, boolean paramBoolean)
  {
    this.versionCode = paramInt;
    this.Qz = paramDataSource;
    this.Qy = paramDataSource.getDataType();
    this.Ra = paramBoolean;
    this.QY = new ArrayList(paramList.size());
    if (paramInt >= 2) {}
    for (;;)
    {
      this.QZ = paramList1;
      paramDataSource = paramList.iterator();
      while (paramDataSource.hasNext())
      {
        paramDataType = (RawDataPoint)paramDataSource.next();
        this.QY.add(new DataPoint(this.QZ, paramDataType));
      }
      paramList1 = Collections.singletonList(paramDataSource);
    }
  }
  
  public DataSet(DataSource paramDataSource)
  {
    this.versionCode = 3;
    this.Qz = ((DataSource)zzaa.zzy(paramDataSource));
    this.Qy = paramDataSource.getDataType();
    this.QY = new ArrayList();
    this.QZ = new ArrayList();
    this.QZ.add(this.Qz);
  }
  
  public DataSet(RawDataSet paramRawDataSet, List<DataSource> paramList)
  {
    this.versionCode = 3;
    this.Qz = ((DataSource)zzd(paramList, paramRawDataSet.Tt));
    this.Qy = this.Qz.getDataType();
    this.QZ = paramList;
    this.Ra = paramRawDataSet.QP;
    paramRawDataSet = paramRawDataSet.Ty;
    this.QY = new ArrayList(paramRawDataSet.size());
    paramRawDataSet = paramRawDataSet.iterator();
    while (paramRawDataSet.hasNext())
    {
      paramList = (RawDataPoint)paramRawDataSet.next();
      this.QY.add(new DataPoint(this.QZ, paramList));
    }
  }
  
  public static DataSet create(DataSource paramDataSource)
  {
    zzaa.zzb(paramDataSource, "DataSource should be specified");
    return new DataSet(paramDataSource);
  }
  
  private boolean zza(DataSet paramDataSet)
  {
    return (zzz.equal(getDataType(), paramDataSet.getDataType())) && (zzz.equal(this.Qz, paramDataSet.Qz)) && (zzz.equal(this.QY, paramDataSet.QY)) && (this.Ra == paramDataSet.Ra);
  }
  
  private static <T> T zzd(List<T> paramList, int paramInt)
  {
    if ((paramInt >= 0) && (paramInt < paramList.size())) {
      return (T)paramList.get(paramInt);
    }
    return null;
  }
  
  public static void zze(DataPoint paramDataPoint)
    throws IllegalArgumentException
  {
    String str = zztv.zza(paramDataPoint, zze.QQ);
    if (str != null)
    {
      paramDataPoint = String.valueOf(paramDataPoint);
      Log.w("Fitness", String.valueOf(paramDataPoint).length() + 20 + "Invalid data point: " + paramDataPoint);
      throw new IllegalArgumentException(str);
    }
  }
  
  public void add(DataPoint paramDataPoint)
  {
    DataSource localDataSource = paramDataPoint.getDataSource();
    zzaa.zzb(localDataSource.getStreamIdentifier().equals(this.Qz.getStreamIdentifier()), "Conflicting data sources found %s vs %s", new Object[] { localDataSource, this.Qz });
    paramDataPoint.zzbee();
    zze(paramDataPoint);
    zzd(paramDataPoint);
  }
  
  public void addAll(Iterable<DataPoint> paramIterable)
  {
    paramIterable = paramIterable.iterator();
    while (paramIterable.hasNext()) {
      add((DataPoint)paramIterable.next());
    }
  }
  
  public DataPoint createDataPoint()
  {
    return DataPoint.create(this.Qz);
  }
  
  public boolean equals(Object paramObject)
  {
    return (paramObject == this) || (((paramObject instanceof DataSet)) && (zza((DataSet)paramObject)));
  }
  
  public List<DataPoint> getDataPoints()
  {
    return Collections.unmodifiableList(this.QY);
  }
  
  public DataSource getDataSource()
  {
    return this.Qz;
  }
  
  public DataType getDataType()
  {
    return this.Qz.getDataType();
  }
  
  int getVersionCode()
  {
    return this.versionCode;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.Qz });
  }
  
  public boolean isEmpty()
  {
    return this.QY.isEmpty();
  }
  
  public String toString()
  {
    Object localObject = zzbeg();
    String str = this.Qz.toDebugString();
    if (this.QY.size() < 10) {}
    for (;;)
    {
      return String.format("DataSet{%s %s}", new Object[] { str, localObject });
      localObject = String.format("%d data points, first 5: %s", new Object[] { Integer.valueOf(this.QY.size()), ((List)localObject).subList(0, 5) });
    }
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzh.zza(this, paramParcel, paramInt);
  }
  
  List<RawDataPoint> zzaa(List<DataSource> paramList)
  {
    ArrayList localArrayList = new ArrayList(this.QY.size());
    Iterator localIterator = this.QY.iterator();
    while (localIterator.hasNext()) {
      localArrayList.add(new RawDataPoint((DataPoint)localIterator.next(), paramList));
    }
    return localArrayList;
  }
  
  public boolean zzbdx()
  {
    return this.Ra;
  }
  
  List<RawDataPoint> zzbeg()
  {
    return zzaa(this.QZ);
  }
  
  List<DataSource> zzbeh()
  {
    return this.QZ;
  }
  
  public void zzc(Iterable<DataPoint> paramIterable)
  {
    paramIterable = paramIterable.iterator();
    while (paramIterable.hasNext()) {
      zzd((DataPoint)paramIterable.next());
    }
  }
  
  public void zzd(DataPoint paramDataPoint)
  {
    this.QY.add(paramDataPoint);
    paramDataPoint = paramDataPoint.getOriginalDataSource();
    if ((paramDataPoint != null) && (!this.QZ.contains(paramDataPoint))) {
      this.QZ.add(paramDataPoint);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/data/DataSet.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */