package com.google.android.gms.fitness.data;

import android.content.Context;
import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.zzc;
import com.google.android.gms.common.internal.zzaa;

public class DataSource
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<DataSource> CREATOR = new zzi();
  public static final String EXTRA_DATA_SOURCE = "vnd.google.fitness.data_source";
  private static final int[] Rb = new int[0];
  public static final int TYPE_DERIVED = 1;
  public static final int TYPE_RAW = 0;
  private final DataType Qy;
  private final Device Rc;
  private final Application Rd;
  private final String Re;
  private final int[] Rf;
  private final String Rg;
  private final String name;
  private final int type;
  private final int versionCode;
  
  DataSource(int paramInt1, DataType paramDataType, String paramString1, int paramInt2, Device paramDevice, Application paramApplication, String paramString2, int[] paramArrayOfInt)
  {
    this.versionCode = paramInt1;
    this.Qy = paramDataType;
    this.type = paramInt2;
    this.name = paramString1;
    this.Rc = paramDevice;
    this.Rd = paramApplication;
    this.Re = paramString2;
    this.Rg = zzbek();
    if (paramArrayOfInt != null) {}
    for (;;)
    {
      this.Rf = paramArrayOfInt;
      return;
      paramArrayOfInt = Rb;
    }
  }
  
  private DataSource(Builder paramBuilder)
  {
    this.versionCode = 3;
    this.Qy = Builder.zza(paramBuilder);
    this.type = Builder.zzb(paramBuilder);
    this.name = Builder.zzc(paramBuilder);
    this.Rc = Builder.zzd(paramBuilder);
    this.Rd = Builder.zze(paramBuilder);
    this.Re = Builder.zzf(paramBuilder);
    this.Rg = zzbek();
    this.Rf = Builder.zzg(paramBuilder);
  }
  
  public static DataSource extract(Intent paramIntent)
  {
    if (paramIntent == null) {
      return null;
    }
    return (DataSource)zzc.zza(paramIntent, "vnd.google.fitness.data_source", CREATOR);
  }
  
  private String getTypeString()
  {
    switch (this.type)
    {
    default: 
      return "derived";
    case 0: 
      return "raw";
    case 1: 
      return "derived";
    case 2: 
      return "cleaned";
    }
    return "converted";
  }
  
  private boolean zza(DataSource paramDataSource)
  {
    return this.Rg.equals(paramDataSource.Rg);
  }
  
  private String zzbek()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(getTypeString());
    localStringBuilder.append(":").append(this.Qy.getName());
    if (this.Rd != null) {
      localStringBuilder.append(":").append(this.Rd.getPackageName());
    }
    if (this.Rc != null) {
      localStringBuilder.append(":").append(this.Rc.getStreamIdentifier());
    }
    if (this.Re != null) {
      localStringBuilder.append(":").append(this.Re);
    }
    return localStringBuilder.toString();
  }
  
  private static String zzns(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return "?";
    case 0: 
      return "r";
    case 1: 
      return "d";
    case 2: 
      return "c";
    }
    return "v";
  }
  
  public static String zznt(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return "unknown";
    case 1: 
      return "blood_pressure_esh2002";
    case 2: 
      return "blood_pressure_esh2010";
    case 3: 
      return "blood_pressure_aami";
    case 4: 
      return "blood_pressure_bhs_a_a";
    case 5: 
      return "blood_pressure_bhs_a_b";
    case 6: 
      return "blood_pressure_bhs_b_a";
    case 7: 
      return "blood_pressure_bhs_b_b";
    case 8: 
      return "blood_glucose_iso151972003";
    }
    return "blood_glucose_iso151972013";
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof DataSource)) && (zza((DataSource)paramObject)));
  }
  
  public String getAppPackageName()
  {
    if (this.Rd == null) {
      return null;
    }
    return this.Rd.getPackageName();
  }
  
  public DataType getDataType()
  {
    return this.Qy;
  }
  
  public Device getDevice()
  {
    return this.Rc;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public String getStreamIdentifier()
  {
    return this.Rg;
  }
  
  public String getStreamName()
  {
    return this.Re;
  }
  
  public int getType()
  {
    return this.type;
  }
  
  int getVersionCode()
  {
    return this.versionCode;
  }
  
  public int hashCode()
  {
    return this.Rg.hashCode();
  }
  
  public String toDebugString()
  {
    String str4 = String.valueOf(zzns(this.type));
    String str5 = String.valueOf(this.Qy.zzbel());
    String str1;
    String str2;
    String str3;
    if (this.Rd == null)
    {
      str1 = "";
      if (this.Rc == null) {
        break label279;
      }
      str2 = String.valueOf(this.Rc.getModel());
      str3 = String.valueOf(this.Rc.getUid());
      str3 = String.valueOf(str2).length() + 2 + String.valueOf(str3).length() + ":" + str2 + ":" + str3;
      label109:
      if (this.Re == null) {
        break label298;
      }
      str2 = String.valueOf(this.Re);
      if (str2.length() == 0) {
        break label285;
      }
      str2 = ":".concat(str2);
    }
    for (;;)
    {
      return String.valueOf(str4).length() + 1 + String.valueOf(str5).length() + String.valueOf(str1).length() + String.valueOf(str3).length() + String.valueOf(str2).length() + str4 + ":" + str5 + str1 + str3 + str2;
      if (this.Rd.equals(Application.QG))
      {
        str1 = ":gms";
        break;
      }
      str1 = String.valueOf(this.Rd.getPackageName());
      if (str1.length() != 0)
      {
        str1 = ":".concat(str1);
        break;
      }
      str1 = new String(":");
      break;
      label279:
      str3 = "";
      break label109;
      label285:
      str2 = new String(":");
      continue;
      label298:
      str2 = "";
    }
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("DataSource{");
    localStringBuilder.append(getTypeString());
    if (this.name != null) {
      localStringBuilder.append(":").append(this.name);
    }
    if (this.Rd != null) {
      localStringBuilder.append(":").append(this.Rd);
    }
    if (this.Rc != null) {
      localStringBuilder.append(":").append(this.Rc);
    }
    if (this.Re != null) {
      localStringBuilder.append(":").append(this.Re);
    }
    localStringBuilder.append(":").append(this.Qy);
    return "}";
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzi.zza(this, paramParcel, paramInt);
  }
  
  public Application zzbei()
  {
    return this.Rd;
  }
  
  public int[] zzbej()
  {
    return this.Rf;
  }
  
  public static final class Builder
  {
    private DataType Qy;
    private Device Rc;
    private Application Rd;
    private String Re = "";
    private String name;
    private int type = -1;
    
    public DataSource build()
    {
      boolean bool2 = true;
      if (this.Qy != null)
      {
        bool1 = true;
        zzaa.zza(bool1, "Must set data type");
        if (this.type < 0) {
          break label47;
        }
      }
      label47:
      for (boolean bool1 = bool2;; bool1 = false)
      {
        zzaa.zza(bool1, "Must set data source type");
        return new DataSource(this, null);
        bool1 = false;
        break;
      }
    }
    
    public Builder setAppPackageName(Context paramContext)
    {
      return setAppPackageName(paramContext.getPackageName());
    }
    
    public Builder setAppPackageName(String paramString)
    {
      this.Rd = Application.zzjf(paramString);
      return this;
    }
    
    public Builder setDataType(DataType paramDataType)
    {
      this.Qy = paramDataType;
      return this;
    }
    
    public Builder setDevice(Device paramDevice)
    {
      this.Rc = paramDevice;
      return this;
    }
    
    public Builder setName(String paramString)
    {
      this.name = paramString;
      return this;
    }
    
    public Builder setStreamName(String paramString)
    {
      if (paramString != null) {}
      for (boolean bool = true;; bool = false)
      {
        zzaa.zzb(bool, "Must specify a valid stream name");
        this.Re = paramString;
        return this;
      }
    }
    
    public Builder setType(int paramInt)
    {
      this.type = paramInt;
      return this;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/data/DataSource.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */