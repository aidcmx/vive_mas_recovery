package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.RequiresPermission;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.util.zzb;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

public final class DataType
  extends AbstractSafeParcelable
{
  public static final DataType AGGREGATE_ACTIVITY_SUMMARY;
  public static final DataType AGGREGATE_BASAL_METABOLIC_RATE_SUMMARY;
  public static final DataType AGGREGATE_BODY_FAT_PERCENTAGE_SUMMARY;
  @Deprecated
  public static final DataType AGGREGATE_CALORIES_CONSUMED;
  public static final DataType AGGREGATE_CALORIES_EXPENDED;
  public static final DataType AGGREGATE_DISTANCE_DELTA;
  @RequiresPermission(conditional=true, value="android.permission.BODY_SENSORS")
  public static final DataType AGGREGATE_HEART_RATE_SUMMARY;
  public static final DataType AGGREGATE_HYDRATION;
  @Deprecated
  public static final Set<DataType> AGGREGATE_INPUT_TYPES;
  @RequiresPermission(conditional=true, value="android.permission.ACCESS_FINE_LOCATION")
  public static final DataType AGGREGATE_LOCATION_BOUNDING_BOX;
  public static final DataType AGGREGATE_NUTRITION_SUMMARY;
  public static final DataType AGGREGATE_POWER_SUMMARY;
  public static final DataType AGGREGATE_SPEED_SUMMARY;
  public static final DataType AGGREGATE_STEP_COUNT_DELTA;
  public static final DataType AGGREGATE_WEIGHT_SUMMARY;
  public static final Parcelable.Creator<DataType> CREATOR = new zzj();
  public static final String MIME_TYPE_PREFIX = "vnd.google.fitness.data_type/";
  public static final DataType Rh;
  public static final DataType Ri;
  public static final DataType Rj;
  public static final DataType Rk;
  public static final DataType Rl;
  public static final DataType Rm;
  public static final DataType Rn;
  public static final DataType Ro;
  public static final DataType Rp;
  public static final DataType Rq;
  @Deprecated
  public static final DataType TYPE_ACTIVITY_SAMPLE;
  public static final DataType TYPE_ACTIVITY_SAMPLES;
  public static final DataType TYPE_ACTIVITY_SEGMENT;
  public static final DataType TYPE_BASAL_METABOLIC_RATE;
  public static final DataType TYPE_BODY_FAT_PERCENTAGE;
  @Deprecated
  public static final DataType TYPE_CALORIES_CONSUMED;
  public static final DataType TYPE_CALORIES_EXPENDED;
  public static final DataType TYPE_CYCLING_PEDALING_CADENCE;
  public static final DataType TYPE_CYCLING_PEDALING_CUMULATIVE;
  public static final DataType TYPE_CYCLING_WHEEL_REVOLUTION;
  public static final DataType TYPE_CYCLING_WHEEL_RPM;
  @RequiresPermission(conditional=true, value="android.permission.ACCESS_FINE_LOCATION")
  @KeepName
  public static final DataType TYPE_DISTANCE_CUMULATIVE;
  @RequiresPermission(conditional=true, value="android.permission.ACCESS_FINE_LOCATION")
  public static final DataType TYPE_DISTANCE_DELTA;
  @RequiresPermission(conditional=true, value="android.permission.BODY_SENSORS")
  public static final DataType TYPE_HEART_RATE_BPM;
  public static final DataType TYPE_HEIGHT;
  public static final DataType TYPE_HYDRATION;
  @RequiresPermission(conditional=true, value="android.permission.ACCESS_FINE_LOCATION")
  public static final DataType TYPE_LOCATION_SAMPLE;
  @RequiresPermission(conditional=true, value="android.permission.ACCESS_FINE_LOCATION")
  public static final DataType TYPE_LOCATION_TRACK;
  public static final DataType TYPE_NUTRITION;
  public static final DataType TYPE_POWER_SAMPLE;
  @RequiresPermission(conditional=true, value="android.permission.ACCESS_FINE_LOCATION")
  public static final DataType TYPE_SPEED;
  public static final DataType TYPE_STEP_COUNT_CADENCE;
  @KeepName
  public static final DataType TYPE_STEP_COUNT_CUMULATIVE;
  public static final DataType TYPE_STEP_COUNT_DELTA = new DataType("com.google.step_count.delta", new Field[] { Field.FIELD_STEPS });
  public static final DataType TYPE_WEIGHT;
  public static final DataType TYPE_WORKOUT_EXERCISE;
  private final List<Field> Rr;
  private final String name;
  private final int versionCode;
  
  static
  {
    TYPE_STEP_COUNT_CUMULATIVE = new DataType("com.google.step_count.cumulative", new Field[] { Field.FIELD_STEPS });
    TYPE_STEP_COUNT_CADENCE = new DataType("com.google.step_count.cadence", new Field[] { Field.FIELD_RPM });
    Rh = new DataType("com.google.stride_model", new Field[] { Field.RJ });
    TYPE_ACTIVITY_SEGMENT = new DataType("com.google.activity.segment", new Field[] { Field.FIELD_ACTIVITY });
    Ri = new DataType("com.google.floor_change", new Field[] { Field.FIELD_ACTIVITY, Field.FIELD_CONFIDENCE, Field.RK, Field.RN });
    TYPE_CALORIES_CONSUMED = new DataType("com.google.calories.consumed", new Field[] { Field.FIELD_CALORIES });
    TYPE_CALORIES_EXPENDED = new DataType("com.google.calories.expended", new Field[] { Field.FIELD_CALORIES });
    TYPE_BASAL_METABOLIC_RATE = new DataType("com.google.calories.bmr", new Field[] { Field.FIELD_CALORIES });
    TYPE_POWER_SAMPLE = new DataType("com.google.power.sample", new Field[] { Field.FIELD_WATTS });
    TYPE_ACTIVITY_SAMPLE = new DataType("com.google.activity.sample", new Field[] { Field.FIELD_ACTIVITY, Field.FIELD_CONFIDENCE });
    TYPE_ACTIVITY_SAMPLES = new DataType("com.google.activity.samples", new Field[] { Field.FIELD_ACTIVITY_CONFIDENCE });
    Rj = new DataType("com.google.accelerometer", new Field[] { Field.zza.RZ, Field.zza.Sa, Field.zza.Sb });
    Rk = new DataType("com.google.sensor.events", new Field[] { Field.RR, Field.RT, Field.RX });
    Rl = new DataType("com.google.sensor.const_rate_events", new Field[] { Field.RS, Field.RU, Field.RV, Field.RW, Field.RX });
    TYPE_HEART_RATE_BPM = new DataType("com.google.heart_rate.bpm", new Field[] { Field.FIELD_BPM });
    TYPE_LOCATION_SAMPLE = new DataType("com.google.location.sample", new Field[] { Field.FIELD_LATITUDE, Field.FIELD_LONGITUDE, Field.FIELD_ACCURACY, Field.FIELD_ALTITUDE });
    TYPE_LOCATION_TRACK = new DataType("com.google.location.track", new Field[] { Field.FIELD_LATITUDE, Field.FIELD_LONGITUDE, Field.FIELD_ACCURACY, Field.FIELD_ALTITUDE });
    TYPE_DISTANCE_DELTA = new DataType("com.google.distance.delta", new Field[] { Field.FIELD_DISTANCE });
    TYPE_DISTANCE_CUMULATIVE = new DataType("com.google.distance.cumulative", new Field[] { Field.FIELD_DISTANCE });
    TYPE_SPEED = new DataType("com.google.speed", new Field[] { Field.FIELD_SPEED });
    TYPE_CYCLING_WHEEL_REVOLUTION = new DataType("com.google.cycling.wheel_revolution.cumulative", new Field[] { Field.FIELD_REVOLUTIONS });
    TYPE_CYCLING_WHEEL_RPM = new DataType("com.google.cycling.wheel_revolution.rpm", new Field[] { Field.FIELD_RPM });
    TYPE_CYCLING_PEDALING_CUMULATIVE = new DataType("com.google.cycling.pedaling.cumulative", new Field[] { Field.FIELD_REVOLUTIONS });
    TYPE_CYCLING_PEDALING_CADENCE = new DataType("com.google.cycling.pedaling.cadence", new Field[] { Field.FIELD_RPM });
    TYPE_HEIGHT = new DataType("com.google.height", new Field[] { Field.FIELD_HEIGHT });
    TYPE_WEIGHT = new DataType("com.google.weight", new Field[] { Field.FIELD_WEIGHT });
    TYPE_BODY_FAT_PERCENTAGE = new DataType("com.google.body.fat.percentage", new Field[] { Field.FIELD_PERCENTAGE });
    Rm = new DataType("com.google.body.waist.circumference", new Field[] { Field.FIELD_CIRCUMFERENCE });
    Rn = new DataType("com.google.body.hip.circumference", new Field[] { Field.FIELD_CIRCUMFERENCE });
    TYPE_NUTRITION = new DataType("com.google.nutrition", new Field[] { Field.FIELD_NUTRIENTS, Field.FIELD_MEAL_TYPE, Field.FIELD_FOOD_ITEM });
    TYPE_HYDRATION = new DataType("com.google.hydration", new Field[] { Field.FIELD_VOLUME });
    TYPE_WORKOUT_EXERCISE = new DataType("com.google.activity.exercise", new Field[] { Field.FIELD_EXERCISE, Field.FIELD_REPETITIONS, Field.FIELD_DURATION, Field.FIELD_RESISTANCE_TYPE, Field.FIELD_RESISTANCE });
    AGGREGATE_ACTIVITY_SUMMARY = new DataType("com.google.activity.summary", new Field[] { Field.FIELD_ACTIVITY, Field.FIELD_DURATION, Field.FIELD_NUM_SEGMENTS });
    Ro = new DataType("com.google.floor_change.summary", new Field[] { Field.RH, Field.RI, Field.RL, Field.RM, Field.RO, Field.RP });
    AGGREGATE_BASAL_METABOLIC_RATE_SUMMARY = new DataType("com.google.calories.bmr.summary", new Field[] { Field.FIELD_AVERAGE, Field.FIELD_MAX, Field.FIELD_MIN });
    AGGREGATE_STEP_COUNT_DELTA = TYPE_STEP_COUNT_DELTA;
    AGGREGATE_DISTANCE_DELTA = TYPE_DISTANCE_DELTA;
    AGGREGATE_CALORIES_CONSUMED = TYPE_CALORIES_CONSUMED;
    AGGREGATE_CALORIES_EXPENDED = TYPE_CALORIES_EXPENDED;
    AGGREGATE_HEART_RATE_SUMMARY = new DataType("com.google.heart_rate.summary", new Field[] { Field.FIELD_AVERAGE, Field.FIELD_MAX, Field.FIELD_MIN });
    AGGREGATE_LOCATION_BOUNDING_BOX = new DataType("com.google.location.bounding_box", new Field[] { Field.FIELD_LOW_LATITUDE, Field.FIELD_LOW_LONGITUDE, Field.FIELD_HIGH_LATITUDE, Field.FIELD_HIGH_LONGITUDE });
    AGGREGATE_POWER_SUMMARY = new DataType("com.google.power.summary", new Field[] { Field.FIELD_AVERAGE, Field.FIELD_MAX, Field.FIELD_MIN });
    AGGREGATE_SPEED_SUMMARY = new DataType("com.google.speed.summary", new Field[] { Field.FIELD_AVERAGE, Field.FIELD_MAX, Field.FIELD_MIN });
    AGGREGATE_BODY_FAT_PERCENTAGE_SUMMARY = new DataType("com.google.body.fat.percentage.summary", new Field[] { Field.FIELD_AVERAGE, Field.FIELD_MAX, Field.FIELD_MIN });
    Rp = new DataType("com.google.body.hip.circumference.summary", new Field[] { Field.FIELD_AVERAGE, Field.FIELD_MAX, Field.FIELD_MIN });
    Rq = new DataType("com.google.body.waist.circumference.summary", new Field[] { Field.FIELD_AVERAGE, Field.FIELD_MAX, Field.FIELD_MIN });
    AGGREGATE_WEIGHT_SUMMARY = new DataType("com.google.weight.summary", new Field[] { Field.FIELD_AVERAGE, Field.FIELD_MAX, Field.FIELD_MIN });
    AGGREGATE_NUTRITION_SUMMARY = new DataType("com.google.nutrition.summary", new Field[] { Field.FIELD_NUTRIENTS, Field.FIELD_MEAL_TYPE });
    AGGREGATE_HYDRATION = TYPE_HYDRATION;
    AGGREGATE_INPUT_TYPES = new com.google.android.gms.common.util.zza();
    AGGREGATE_INPUT_TYPES.add(TYPE_ACTIVITY_SEGMENT);
    AGGREGATE_INPUT_TYPES.add(TYPE_BASAL_METABOLIC_RATE);
    AGGREGATE_INPUT_TYPES.add(TYPE_BODY_FAT_PERCENTAGE);
    AGGREGATE_INPUT_TYPES.add(Rn);
    AGGREGATE_INPUT_TYPES.add(Rm);
    AGGREGATE_INPUT_TYPES.add(TYPE_CALORIES_CONSUMED);
    AGGREGATE_INPUT_TYPES.add(TYPE_CALORIES_EXPENDED);
    AGGREGATE_INPUT_TYPES.add(TYPE_DISTANCE_DELTA);
    AGGREGATE_INPUT_TYPES.add(Ri);
    AGGREGATE_INPUT_TYPES.add(TYPE_LOCATION_SAMPLE);
    AGGREGATE_INPUT_TYPES.add(TYPE_NUTRITION);
    AGGREGATE_INPUT_TYPES.add(TYPE_HYDRATION);
    AGGREGATE_INPUT_TYPES.add(TYPE_HEART_RATE_BPM);
    AGGREGATE_INPUT_TYPES.add(TYPE_POWER_SAMPLE);
    AGGREGATE_INPUT_TYPES.add(TYPE_SPEED);
    AGGREGATE_INPUT_TYPES.add(TYPE_STEP_COUNT_DELTA);
    AGGREGATE_INPUT_TYPES.add(TYPE_WEIGHT);
  }
  
  DataType(int paramInt, String paramString, List<Field> paramList)
  {
    this.versionCode = paramInt;
    this.name = paramString;
    this.Rr = Collections.unmodifiableList(paramList);
  }
  
  public DataType(String paramString, Field... paramVarArgs)
  {
    this(1, paramString, zzb.zzb(paramVarArgs));
  }
  
  public static List<DataType> getAggregatesForInput(DataType paramDataType)
  {
    paramDataType = (List)zza.QF.get(paramDataType);
    if (paramDataType == null) {
      return Collections.emptyList();
    }
    return Collections.unmodifiableList(paramDataType);
  }
  
  public static String getMimeType(DataType paramDataType)
  {
    String str = String.valueOf("vnd.google.fitness.data_type/");
    paramDataType = String.valueOf(paramDataType.getName());
    if (paramDataType.length() != 0) {
      return str.concat(paramDataType);
    }
    return new String(str);
  }
  
  private boolean zzc(DataType paramDataType)
  {
    return (this.name.equals(paramDataType.name)) && (this.Rr.equals(paramDataType.Rr));
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof DataType)) && (zzc((DataType)paramObject)));
  }
  
  public List<Field> getFields()
  {
    return this.Rr;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  int getVersionCode()
  {
    return this.versionCode;
  }
  
  public int hashCode()
  {
    return this.name.hashCode();
  }
  
  public int indexOf(Field paramField)
  {
    int i = this.Rr.indexOf(paramField);
    if (i < 0) {
      throw new IllegalArgumentException(String.format("%s not a field of %s", new Object[] { paramField, this }));
    }
    return i;
  }
  
  public String toString()
  {
    return String.format("DataType{%s%s}", new Object[] { this.name, this.Rr });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzj.zza(this, paramParcel, paramInt);
  }
  
  public String zzbel()
  {
    if (this.name.startsWith("com.google.")) {
      return this.name.substring(11);
    }
    return this.name;
  }
  
  public static final class zza
  {
    public static final DataType Rs = new DataType("com.google.internal.session.debug", new Field[] { Field.zza.Sc });
    public static final DataType Rt = new DataType("com.google.internal.session.v2", new Field[] { Field.zza.Sd });
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/data/DataType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */