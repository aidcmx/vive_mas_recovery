package com.google.android.gms.fitness.data;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.zzc;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;
import java.util.concurrent.TimeUnit;

public class DataUpdateNotification
  extends AbstractSafeParcelable
{
  public static final String ACTION = "com.google.android.gms.fitness.DATA_UPDATE_NOTIFICATION";
  public static final Parcelable.Creator<DataUpdateNotification> CREATOR = new zzl();
  public static final String EXTRA_DATA_UPDATE_NOTIFICATION = "vnd.google.fitness.data_udpate_notification";
  public static final int OPERATION_DELETE = 2;
  public static final int OPERATION_INSERT = 1;
  public static final int OPERATION_UPDATE = 3;
  private final DataType RA;
  private final long Rw;
  private final long Rx;
  private final int Ry;
  private final DataSource Rz;
  final int mVersionCode;
  
  DataUpdateNotification(int paramInt1, long paramLong1, long paramLong2, int paramInt2, DataSource paramDataSource, DataType paramDataType)
  {
    this.mVersionCode = paramInt1;
    this.Rw = paramLong1;
    this.Rx = paramLong2;
    this.Ry = paramInt2;
    this.Rz = paramDataSource;
    this.RA = paramDataType;
  }
  
  public static DataUpdateNotification getDataUpdateNotification(Intent paramIntent)
  {
    return (DataUpdateNotification)zzc.zza(paramIntent, "vnd.google.fitness.data_udpate_notification", CREATOR);
  }
  
  private boolean zza(DataUpdateNotification paramDataUpdateNotification)
  {
    return (this.Rw == paramDataUpdateNotification.Rw) && (this.Rx == paramDataUpdateNotification.Rx) && (this.Ry == paramDataUpdateNotification.Ry) && (zzz.equal(this.Rz, paramDataUpdateNotification.Rz)) && (zzz.equal(this.RA, paramDataUpdateNotification.RA));
  }
  
  public boolean equals(Object paramObject)
  {
    return (paramObject == this) || (((paramObject instanceof DataUpdateNotification)) && (zza((DataUpdateNotification)paramObject)));
  }
  
  public DataSource getDataSource()
  {
    return this.Rz;
  }
  
  public DataType getDataType()
  {
    return this.RA;
  }
  
  public int getOperationType()
  {
    return this.Ry;
  }
  
  public long getUpdateEndTime(TimeUnit paramTimeUnit)
  {
    return paramTimeUnit.convert(this.Rx, TimeUnit.NANOSECONDS);
  }
  
  public long getUpdateStartTime(TimeUnit paramTimeUnit)
  {
    return paramTimeUnit.convert(this.Rw, TimeUnit.NANOSECONDS);
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { Long.valueOf(this.Rw), Long.valueOf(this.Rx), Integer.valueOf(this.Ry), this.Rz, this.RA });
  }
  
  public String toString()
  {
    return zzz.zzx(this).zzg("updateStartTimeNanos", Long.valueOf(this.Rw)).zzg("updateEndTimeNanos", Long.valueOf(this.Rx)).zzg("operationType", Integer.valueOf(this.Ry)).zzg("dataSource", this.Rz).zzg("dataType", this.RA).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzl.zza(this, paramParcel, paramInt);
  }
  
  public long zzbem()
  {
    return this.Rw;
  }
  
  public long zzben()
  {
    return this.Rx;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/data/DataUpdateNotification.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */