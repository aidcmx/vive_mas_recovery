package com.google.android.gms.fitness.data;

import android.content.Context;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.provider.Settings.Secure;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.internal.zztu;
import com.google.android.gms.internal.zzvp;

public final class Device
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<Device> CREATOR = new zzm();
  public static final int TYPE_CHEST_STRAP = 4;
  public static final int TYPE_HEAD_MOUNTED = 6;
  public static final int TYPE_PHONE = 1;
  public static final int TYPE_SCALE = 5;
  public static final int TYPE_TABLET = 2;
  public static final int TYPE_UNKNOWN = 0;
  public static final int TYPE_WATCH = 3;
  private final String RB;
  private final String RC;
  private final String RD;
  private final int RE;
  private final int type;
  private final String version;
  private final int versionCode;
  
  Device(int paramInt1, String paramString1, String paramString2, String paramString3, String paramString4, int paramInt2, int paramInt3)
  {
    this.versionCode = paramInt1;
    this.RB = ((String)zzaa.zzy(paramString1));
    this.RC = ((String)zzaa.zzy(paramString2));
    this.version = "";
    this.RD = zzji(paramString4);
    this.type = paramInt2;
    this.RE = paramInt3;
  }
  
  public Device(String paramString1, String paramString2, String paramString3, int paramInt)
  {
    this(paramString1, paramString2, "", paramString3, paramInt, 0);
  }
  
  public Device(String paramString1, String paramString2, String paramString3, String paramString4, int paramInt1, int paramInt2)
  {
    this(1, paramString1, paramString2, "", paramString4, paramInt1, paramInt2);
  }
  
  public static Device getLocalDevice(Context paramContext)
  {
    int i = zztu.zzcv(paramContext);
    paramContext = zzct(paramContext);
    return new Device(Build.MANUFACTURER, Build.MODEL, Build.VERSION.RELEASE, paramContext, i, 2);
  }
  
  private boolean zza(Device paramDevice)
  {
    return (zzz.equal(this.RB, paramDevice.RB)) && (zzz.equal(this.RC, paramDevice.RC)) && (zzz.equal(this.version, paramDevice.version)) && (zzz.equal(this.RD, paramDevice.RD)) && (this.type == paramDevice.type) && (this.RE == paramDevice.RE);
  }
  
  private boolean zzbep()
  {
    return zzbeo() == 1;
  }
  
  private static String zzct(Context paramContext)
  {
    return Settings.Secure.getString(paramContext.getContentResolver(), "android_id");
  }
  
  private String zzji(String paramString)
  {
    if (paramString != null) {
      return paramString;
    }
    throw new IllegalStateException("Device UID is null.");
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof Device)) && (zza((Device)paramObject)));
  }
  
  public String getManufacturer()
  {
    return this.RB;
  }
  
  public String getModel()
  {
    return this.RC;
  }
  
  String getStreamIdentifier()
  {
    return String.format("%s:%s:%s", new Object[] { this.RB, this.RC, this.RD });
  }
  
  public int getType()
  {
    return this.type;
  }
  
  public String getUid()
  {
    return this.RD;
  }
  
  public String getVersion()
  {
    return this.version;
  }
  
  int getVersionCode()
  {
    return this.versionCode;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.RB, this.RC, this.version, this.RD, Integer.valueOf(this.type) });
  }
  
  public String toString()
  {
    return String.format("Device{%s:%s:%s:%s}", new Object[] { getStreamIdentifier(), this.version, Integer.valueOf(this.type), Integer.valueOf(this.RE) });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzm.zza(this, paramParcel, paramInt);
  }
  
  public int zzbeo()
  {
    return this.RE;
  }
  
  public String zzbeq()
  {
    if (zzbep()) {
      return this.RD;
    }
    return zzvp.zzju(this.RD);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/data/Device.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */