package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;

public final class Field
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<Field> CREATOR = new zzo();
  public static final Field FIELD_ACCURACY;
  public static final Field FIELD_ACTIVITY = zzjj("activity");
  public static final Field FIELD_ACTIVITY_CONFIDENCE;
  public static final Field FIELD_ALTITUDE;
  public static final Field FIELD_AVERAGE;
  public static final Field FIELD_BPM;
  public static final Field FIELD_CALORIES;
  public static final Field FIELD_CIRCUMFERENCE;
  public static final Field FIELD_CONFIDENCE = zzjl("confidence");
  public static final Field FIELD_DISTANCE;
  public static final Field FIELD_DURATION;
  public static final Field FIELD_EXERCISE;
  public static final Field FIELD_FOOD_ITEM;
  public static final Field FIELD_HEIGHT;
  public static final Field FIELD_HIGH_LATITUDE;
  public static final Field FIELD_HIGH_LONGITUDE;
  public static final Field FIELD_LATITUDE;
  public static final Field FIELD_LONGITUDE;
  public static final Field FIELD_LOW_LATITUDE;
  public static final Field FIELD_LOW_LONGITUDE;
  public static final Field FIELD_MAX;
  public static final Field FIELD_MEAL_TYPE;
  public static final Field FIELD_MIN;
  public static final Field FIELD_NUM_SEGMENTS;
  public static final Field FIELD_NUTRIENTS;
  public static final Field FIELD_PERCENTAGE;
  public static final Field FIELD_REPETITIONS;
  public static final Field FIELD_RESISTANCE;
  public static final Field FIELD_RESISTANCE_TYPE;
  public static final Field FIELD_REVOLUTIONS;
  public static final Field FIELD_RPM;
  public static final Field FIELD_SPEED;
  public static final Field FIELD_STEPS;
  public static final Field FIELD_VOLUME;
  public static final Field FIELD_WATTS;
  public static final Field FIELD_WEIGHT;
  public static final int FORMAT_FLOAT = 2;
  public static final int FORMAT_INT32 = 1;
  public static final int FORMAT_MAP = 4;
  public static final int FORMAT_STRING = 3;
  public static final int MEAL_TYPE_BREAKFAST = 1;
  public static final int MEAL_TYPE_DINNER = 3;
  public static final int MEAL_TYPE_LUNCH = 2;
  public static final int MEAL_TYPE_SNACK = 4;
  public static final int MEAL_TYPE_UNKNOWN = 0;
  public static final String NUTRIENT_CALCIUM = "calcium";
  public static final String NUTRIENT_CALORIES = "calories";
  public static final String NUTRIENT_CHOLESTEROL = "cholesterol";
  public static final String NUTRIENT_DIETARY_FIBER = "dietary_fiber";
  public static final String NUTRIENT_IRON = "iron";
  public static final String NUTRIENT_MONOUNSATURATED_FAT = "fat.monounsaturated";
  public static final String NUTRIENT_POLYUNSATURATED_FAT = "fat.polyunsaturated";
  public static final String NUTRIENT_POTASSIUM = "potassium";
  public static final String NUTRIENT_PROTEIN = "protein";
  public static final String NUTRIENT_SATURATED_FAT = "fat.saturated";
  public static final String NUTRIENT_SODIUM = "sodium";
  public static final String NUTRIENT_SUGAR = "sugar";
  public static final String NUTRIENT_TOTAL_CARBS = "carbs.total";
  public static final String NUTRIENT_TOTAL_FAT = "fat.total";
  public static final String NUTRIENT_TRANS_FAT = "fat.trans";
  public static final String NUTRIENT_UNSATURATED_FAT = "fat.unsaturated";
  public static final String NUTRIENT_VITAMIN_A = "vitamin_a";
  public static final String NUTRIENT_VITAMIN_C = "vitamin_c";
  public static final int RESISTANCE_TYPE_BARBELL = 1;
  public static final int RESISTANCE_TYPE_BODY = 6;
  public static final int RESISTANCE_TYPE_CABLE = 2;
  public static final int RESISTANCE_TYPE_DUMBBELL = 3;
  public static final int RESISTANCE_TYPE_KETTLEBELL = 4;
  public static final int RESISTANCE_TYPE_MACHINE = 5;
  public static final int RESISTANCE_TYPE_UNKNOWN = 0;
  public static final Field RF;
  public static final Field RG;
  public static final Field RH;
  public static final Field RI;
  public static final Field RJ;
  public static final Field RK;
  public static final Field RL;
  public static final Field RM;
  public static final Field RN;
  public static final Field RO;
  public static final Field RP;
  public static final Field RQ;
  public static final Field RR;
  public static final Field RS;
  public static final Field RT;
  public static final Field RU;
  public static final Field RV;
  public static final Field RW;
  public static final Field RX;
  private final Boolean RY;
  private final int format;
  private final String name;
  private final int versionCode;
  
  static
  {
    FIELD_ACTIVITY_CONFIDENCE = zzjo("activity_confidence");
    FIELD_STEPS = zzjj("steps");
    FIELD_DURATION = zzjj("duration");
    RF = zzjk("duration");
    RG = zzjo("activity_duration");
    RH = zzjo("activity_duration.ascending");
    RI = zzjo("activity_duration.descending");
    FIELD_BPM = zzjl("bpm");
    FIELD_LATITUDE = zzjl("latitude");
    FIELD_LONGITUDE = zzjl("longitude");
    FIELD_ACCURACY = zzjl("accuracy");
    FIELD_ALTITUDE = zzjm("altitude");
    FIELD_DISTANCE = zzjl("distance");
    FIELD_HEIGHT = zzjl("height");
    FIELD_WEIGHT = zzjl("weight");
    FIELD_CIRCUMFERENCE = zzjl("circumference");
    FIELD_PERCENTAGE = zzjl("percentage");
    FIELD_SPEED = zzjl("speed");
    FIELD_RPM = zzjl("rpm");
    RJ = zzjr("google.android.fitness.StrideModel");
    FIELD_REVOLUTIONS = zzjj("revolutions");
    FIELD_CALORIES = zzjl("calories");
    FIELD_WATTS = zzjl("watts");
    FIELD_VOLUME = zzjl("volume");
    FIELD_MEAL_TYPE = zzjj("meal_type");
    FIELD_FOOD_ITEM = zzjn("food_item");
    FIELD_NUTRIENTS = zzjo("nutrients");
    RK = zzjl("elevation.change");
    RL = zzjo("elevation.gain");
    RM = zzjo("elevation.loss");
    RN = zzjl("floors");
    RO = zzjo("floor.gain");
    RP = zzjo("floor.loss");
    FIELD_EXERCISE = zzjn("exercise");
    FIELD_REPETITIONS = zzjj("repetitions");
    FIELD_RESISTANCE = zzjl("resistance");
    FIELD_RESISTANCE_TYPE = zzjj("resistance_type");
    FIELD_NUM_SEGMENTS = zzjj("num_segments");
    FIELD_AVERAGE = zzjl("average");
    FIELD_MAX = zzjl("max");
    FIELD_MIN = zzjl("min");
    FIELD_LOW_LATITUDE = zzjl("low_latitude");
    FIELD_LOW_LONGITUDE = zzjl("low_longitude");
    FIELD_HIGH_LATITUDE = zzjl("high_latitude");
    FIELD_HIGH_LONGITUDE = zzjl("high_longitude");
    RQ = zzjj("occurrences");
    RR = zzjj("sensor_type");
    RS = zzjj("sensor_types");
    RT = zzjp("timestamps");
    RU = zzjj("sample_period");
    RV = zzjj("num_samples");
    RW = zzjj("num_dimensions");
    RX = zzjq("sensor_values");
  }
  
  Field(int paramInt1, String paramString, int paramInt2, Boolean paramBoolean)
  {
    this.versionCode = paramInt1;
    this.name = ((String)zzaa.zzy(paramString));
    this.format = paramInt2;
    this.RY = paramBoolean;
  }
  
  private Field(String paramString, int paramInt)
  {
    this(2, paramString, paramInt, null);
  }
  
  private Field(String paramString, int paramInt, Boolean paramBoolean)
  {
    this(2, paramString, paramInt, paramBoolean);
  }
  
  public static Field zza(String paramString, int paramInt, Boolean paramBoolean)
  {
    int i = -1;
    switch (paramString.hashCode())
    {
    }
    for (;;)
    {
      switch (i)
      {
      default: 
        return new Field(paramString, paramInt, paramBoolean);
        if (paramString.equals("accuracy"))
        {
          i = 0;
          continue;
          if (paramString.equals("activity"))
          {
            i = 1;
            continue;
            if (paramString.equals("activity_confidence"))
            {
              i = 2;
              continue;
              if (paramString.equals("activity_duration"))
              {
                i = 3;
                continue;
                if (paramString.equals("activity_duration.ascending"))
                {
                  i = 4;
                  continue;
                  if (paramString.equals("activity_duration.descending"))
                  {
                    i = 5;
                    continue;
                    if (paramString.equals("altitude"))
                    {
                      i = 6;
                      continue;
                      if (paramString.equals("average"))
                      {
                        i = 7;
                        continue;
                        if (paramString.equals("blood_glucose_level"))
                        {
                          i = 8;
                          continue;
                          if (paramString.equals("blood_glucose_specimen_source"))
                          {
                            i = 9;
                            continue;
                            if (paramString.equals("blood_pressure_diastolic"))
                            {
                              i = 10;
                              continue;
                              if (paramString.equals("blood_pressure_diastolic_average"))
                              {
                                i = 11;
                                continue;
                                if (paramString.equals("blood_pressure_diastolic_max"))
                                {
                                  i = 12;
                                  continue;
                                  if (paramString.equals("blood_pressure_diastolic_min"))
                                  {
                                    i = 13;
                                    continue;
                                    if (paramString.equals("blood_pressure_measurement_location"))
                                    {
                                      i = 14;
                                      continue;
                                      if (paramString.equals("blood_pressure_systolic"))
                                      {
                                        i = 15;
                                        continue;
                                        if (paramString.equals("blood_pressure_systolic_average"))
                                        {
                                          i = 16;
                                          continue;
                                          if (paramString.equals("blood_pressure_systolic_max"))
                                          {
                                            i = 17;
                                            continue;
                                            if (paramString.equals("blood_pressure_systolic_min"))
                                            {
                                              i = 18;
                                              continue;
                                              if (paramString.equals("body_position"))
                                              {
                                                i = 19;
                                                continue;
                                                if (paramString.equals("body_temperature"))
                                                {
                                                  i = 20;
                                                  continue;
                                                  if (paramString.equals("body_temperature_measurement_location"))
                                                  {
                                                    i = 21;
                                                    continue;
                                                    if (paramString.equals("bpm"))
                                                    {
                                                      i = 22;
                                                      continue;
                                                      if (paramString.equals("calories"))
                                                      {
                                                        i = 23;
                                                        continue;
                                                        if (paramString.equals("cervical_dilation"))
                                                        {
                                                          i = 24;
                                                          continue;
                                                          if (paramString.equals("cervical_firmness"))
                                                          {
                                                            i = 25;
                                                            continue;
                                                            if (paramString.equals("cervical_mucus_amount"))
                                                            {
                                                              i = 26;
                                                              continue;
                                                              if (paramString.equals("cervical_mucus_texture"))
                                                              {
                                                                i = 27;
                                                                continue;
                                                                if (paramString.equals("cervical_position"))
                                                                {
                                                                  i = 28;
                                                                  continue;
                                                                  if (paramString.equals("circumference"))
                                                                  {
                                                                    i = 29;
                                                                    continue;
                                                                    if (paramString.equals("confidence"))
                                                                    {
                                                                      i = 30;
                                                                      continue;
                                                                      if (paramString.equals("distance"))
                                                                      {
                                                                        i = 31;
                                                                        continue;
                                                                        if (paramString.equals("duration"))
                                                                        {
                                                                          i = 32;
                                                                          continue;
                                                                          if (paramString.equals("elevation.change"))
                                                                          {
                                                                            i = 33;
                                                                            continue;
                                                                            if (paramString.equals("elevation.gain"))
                                                                            {
                                                                              i = 34;
                                                                              continue;
                                                                              if (paramString.equals("elevation.loss"))
                                                                              {
                                                                                i = 35;
                                                                                continue;
                                                                                if (paramString.equals("exercise"))
                                                                                {
                                                                                  i = 36;
                                                                                  continue;
                                                                                  if (paramString.equals("floor.gain"))
                                                                                  {
                                                                                    i = 37;
                                                                                    continue;
                                                                                    if (paramString.equals("floor.loss"))
                                                                                    {
                                                                                      i = 38;
                                                                                      continue;
                                                                                      if (paramString.equals("floors"))
                                                                                      {
                                                                                        i = 39;
                                                                                        continue;
                                                                                        if (paramString.equals("food_item"))
                                                                                        {
                                                                                          i = 40;
                                                                                          continue;
                                                                                          if (paramString.equals("height"))
                                                                                          {
                                                                                            i = 41;
                                                                                            continue;
                                                                                            if (paramString.equals("high_latitude"))
                                                                                            {
                                                                                              i = 42;
                                                                                              continue;
                                                                                              if (paramString.equals("high_longitude"))
                                                                                              {
                                                                                                i = 43;
                                                                                                continue;
                                                                                                if (paramString.equals("latitude"))
                                                                                                {
                                                                                                  i = 44;
                                                                                                  continue;
                                                                                                  if (paramString.equals("longitude"))
                                                                                                  {
                                                                                                    i = 45;
                                                                                                    continue;
                                                                                                    if (paramString.equals("low_latitude"))
                                                                                                    {
                                                                                                      i = 46;
                                                                                                      continue;
                                                                                                      if (paramString.equals("low_longitude"))
                                                                                                      {
                                                                                                        i = 47;
                                                                                                        continue;
                                                                                                        if (paramString.equals("max"))
                                                                                                        {
                                                                                                          i = 48;
                                                                                                          continue;
                                                                                                          if (paramString.equals("meal_type"))
                                                                                                          {
                                                                                                            i = 49;
                                                                                                            continue;
                                                                                                            if (paramString.equals("menstrual_flow"))
                                                                                                            {
                                                                                                              i = 50;
                                                                                                              continue;
                                                                                                              if (paramString.equals("min"))
                                                                                                              {
                                                                                                                i = 51;
                                                                                                                continue;
                                                                                                                if (paramString.equals("num_dimensions"))
                                                                                                                {
                                                                                                                  i = 52;
                                                                                                                  continue;
                                                                                                                  if (paramString.equals("num_samples"))
                                                                                                                  {
                                                                                                                    i = 53;
                                                                                                                    continue;
                                                                                                                    if (paramString.equals("num_segments"))
                                                                                                                    {
                                                                                                                      i = 54;
                                                                                                                      continue;
                                                                                                                      if (paramString.equals("nutrients"))
                                                                                                                      {
                                                                                                                        i = 55;
                                                                                                                        continue;
                                                                                                                        if (paramString.equals("occurrences"))
                                                                                                                        {
                                                                                                                          i = 56;
                                                                                                                          continue;
                                                                                                                          if (paramString.equals("ovulation_test_result"))
                                                                                                                          {
                                                                                                                            i = 57;
                                                                                                                            continue;
                                                                                                                            if (paramString.equals("oxygen_saturation"))
                                                                                                                            {
                                                                                                                              i = 58;
                                                                                                                              continue;
                                                                                                                              if (paramString.equals("oxygen_saturation_average"))
                                                                                                                              {
                                                                                                                                i = 59;
                                                                                                                                continue;
                                                                                                                                if (paramString.equals("oxygen_saturation_max"))
                                                                                                                                {
                                                                                                                                  i = 60;
                                                                                                                                  continue;
                                                                                                                                  if (paramString.equals("oxygen_saturation_measurement_method"))
                                                                                                                                  {
                                                                                                                                    i = 61;
                                                                                                                                    continue;
                                                                                                                                    if (paramString.equals("oxygen_saturation_min"))
                                                                                                                                    {
                                                                                                                                      i = 62;
                                                                                                                                      continue;
                                                                                                                                      if (paramString.equals("oxygen_saturation_system"))
                                                                                                                                      {
                                                                                                                                        i = 63;
                                                                                                                                        continue;
                                                                                                                                        if (paramString.equals("oxygen_therapy_administration_mode"))
                                                                                                                                        {
                                                                                                                                          i = 64;
                                                                                                                                          continue;
                                                                                                                                          if (paramString.equals("timestamps"))
                                                                                                                                          {
                                                                                                                                            i = 65;
                                                                                                                                            continue;
                                                                                                                                            if (paramString.equals("sample_period"))
                                                                                                                                            {
                                                                                                                                              i = 66;
                                                                                                                                              continue;
                                                                                                                                              if (paramString.equals("sensor_type"))
                                                                                                                                              {
                                                                                                                                                i = 67;
                                                                                                                                                continue;
                                                                                                                                                if (paramString.equals("sensor_types"))
                                                                                                                                                {
                                                                                                                                                  i = 68;
                                                                                                                                                  continue;
                                                                                                                                                  if (paramString.equals("sensor_values"))
                                                                                                                                                  {
                                                                                                                                                    i = 69;
                                                                                                                                                    continue;
                                                                                                                                                    if (paramString.equals("supplemental_oxygen_flow_rate"))
                                                                                                                                                    {
                                                                                                                                                      i = 70;
                                                                                                                                                      continue;
                                                                                                                                                      if (paramString.equals("supplemental_oxygen_flow_rate_average"))
                                                                                                                                                      {
                                                                                                                                                        i = 71;
                                                                                                                                                        continue;
                                                                                                                                                        if (paramString.equals("supplemental_oxygen_flow_rate_max"))
                                                                                                                                                        {
                                                                                                                                                          i = 72;
                                                                                                                                                          continue;
                                                                                                                                                          if (paramString.equals("supplemental_oxygen_flow_rate_min"))
                                                                                                                                                          {
                                                                                                                                                            i = 73;
                                                                                                                                                            continue;
                                                                                                                                                            if (paramString.equals("percentage"))
                                                                                                                                                            {
                                                                                                                                                              i = 74;
                                                                                                                                                              continue;
                                                                                                                                                              if (paramString.equals("repetitions"))
                                                                                                                                                              {
                                                                                                                                                                i = 75;
                                                                                                                                                                continue;
                                                                                                                                                                if (paramString.equals("resistance"))
                                                                                                                                                                {
                                                                                                                                                                  i = 76;
                                                                                                                                                                  continue;
                                                                                                                                                                  if (paramString.equals("resistance_type"))
                                                                                                                                                                  {
                                                                                                                                                                    i = 77;
                                                                                                                                                                    continue;
                                                                                                                                                                    if (paramString.equals("revolutions"))
                                                                                                                                                                    {
                                                                                                                                                                      i = 78;
                                                                                                                                                                      continue;
                                                                                                                                                                      if (paramString.equals("rpm"))
                                                                                                                                                                      {
                                                                                                                                                                        i = 79;
                                                                                                                                                                        continue;
                                                                                                                                                                        if (paramString.equals("speed"))
                                                                                                                                                                        {
                                                                                                                                                                          i = 80;
                                                                                                                                                                          continue;
                                                                                                                                                                          if (paramString.equals("steps"))
                                                                                                                                                                          {
                                                                                                                                                                            i = 81;
                                                                                                                                                                            continue;
                                                                                                                                                                            if (paramString.equals("google.android.fitness.StrideModel"))
                                                                                                                                                                            {
                                                                                                                                                                              i = 82;
                                                                                                                                                                              continue;
                                                                                                                                                                              if (paramString.equals("temporal_relation_to_meal"))
                                                                                                                                                                              {
                                                                                                                                                                                i = 83;
                                                                                                                                                                                continue;
                                                                                                                                                                                if (paramString.equals("temporal_relation_to_sleep"))
                                                                                                                                                                                {
                                                                                                                                                                                  i = 84;
                                                                                                                                                                                  continue;
                                                                                                                                                                                  if (paramString.equals("volume"))
                                                                                                                                                                                  {
                                                                                                                                                                                    i = 85;
                                                                                                                                                                                    continue;
                                                                                                                                                                                    if (paramString.equals("watts"))
                                                                                                                                                                                    {
                                                                                                                                                                                      i = 86;
                                                                                                                                                                                      continue;
                                                                                                                                                                                      if (paramString.equals("weight"))
                                                                                                                                                                                      {
                                                                                                                                                                                        i = 87;
                                                                                                                                                                                        continue;
                                                                                                                                                                                        if (paramString.equals("x"))
                                                                                                                                                                                        {
                                                                                                                                                                                          i = 88;
                                                                                                                                                                                          continue;
                                                                                                                                                                                          if (paramString.equals("y"))
                                                                                                                                                                                          {
                                                                                                                                                                                            i = 89;
                                                                                                                                                                                            continue;
                                                                                                                                                                                            if (paramString.equals("z"))
                                                                                                                                                                                            {
                                                                                                                                                                                              i = 90;
                                                                                                                                                                                              continue;
                                                                                                                                                                                              if (paramString.equals("debug_session"))
                                                                                                                                                                                              {
                                                                                                                                                                                                i = 91;
                                                                                                                                                                                                continue;
                                                                                                                                                                                                if (paramString.equals("google.android.fitness.SessionV2")) {
                                                                                                                                                                                                  i = 92;
                                                                                                                                                                                                }
                                                                                                                                                                                              }
                                                                                                                                                                                            }
                                                                                                                                                                                          }
                                                                                                                                                                                        }
                                                                                                                                                                                      }
                                                                                                                                                                                    }
                                                                                                                                                                                  }
                                                                                                                                                                                }
                                                                                                                                                                              }
                                                                                                                                                                            }
                                                                                                                                                                          }
                                                                                                                                                                        }
                                                                                                                                                                      }
                                                                                                                                                                    }
                                                                                                                                                                  }
                                                                                                                                                                }
                                                                                                                                                              }
                                                                                                                                                            }
                                                                                                                                                          }
                                                                                                                                                        }
                                                                                                                                                      }
                                                                                                                                                    }
                                                                                                                                                  }
                                                                                                                                                }
                                                                                                                                              }
                                                                                                                                            }
                                                                                                                                          }
                                                                                                                                        }
                                                                                                                                      }
                                                                                                                                    }
                                                                                                                                  }
                                                                                                                                }
                                                                                                                              }
                                                                                                                            }
                                                                                                                          }
                                                                                                                        }
                                                                                                                      }
                                                                                                                    }
                                                                                                                  }
                                                                                                                }
                                                                                                              }
                                                                                                            }
                                                                                                          }
                                                                                                        }
                                                                                                      }
                                                                                                    }
                                                                                                  }
                                                                                                }
                                                                                              }
                                                                                            }
                                                                                          }
                                                                                        }
                                                                                      }
                                                                                    }
                                                                                  }
                                                                                }
                                                                              }
                                                                            }
                                                                          }
                                                                        }
                                                                      }
                                                                    }
                                                                  }
                                                                }
                                                              }
                                                            }
                                                          }
                                                        }
                                                      }
                                                    }
                                                  }
                                                }
                                              }
                                            }
                                          }
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
        break;
      }
    }
    return FIELD_ACCURACY;
    return FIELD_ACTIVITY;
    return FIELD_ACTIVITY_CONFIDENCE;
    return RG;
    return RH;
    return RI;
    return FIELD_ALTITUDE;
    return FIELD_AVERAGE;
    return zzs.SP;
    return zzs.SS;
    return zzs.SJ;
    return zzs.SK;
    return zzs.SM;
    return zzs.SL;
    return zzs.SO;
    return zzs.SF;
    return zzs.SG;
    return zzs.SI;
    return zzs.SH;
    return zzs.SN;
    return zzs.Te;
    return zzs.Tf;
    return FIELD_BPM;
    return FIELD_CALORIES;
    return zzs.Tj;
    return zzs.Tk;
    return zzs.Th;
    return zzs.Tg;
    return zzs.Ti;
    return FIELD_CIRCUMFERENCE;
    return FIELD_CONFIDENCE;
    return FIELD_DISTANCE;
    if ((paramBoolean != null) && (paramBoolean.booleanValue())) {
      return RF;
    }
    return FIELD_DURATION;
    return RK;
    return RL;
    return RM;
    return FIELD_EXERCISE;
    return RO;
    return RP;
    return RN;
    return FIELD_FOOD_ITEM;
    return FIELD_HEIGHT;
    return FIELD_HIGH_LATITUDE;
    return FIELD_HIGH_LONGITUDE;
    return FIELD_LATITUDE;
    return FIELD_LONGITUDE;
    return FIELD_LOW_LATITUDE;
    return FIELD_LOW_LONGITUDE;
    return FIELD_MAX;
    return FIELD_MEAL_TYPE;
    return zzs.Tl;
    return FIELD_MIN;
    return RW;
    return RV;
    return FIELD_NUM_SEGMENTS;
    return FIELD_NUTRIENTS;
    return RQ;
    return zzs.Tm;
    return zzs.ST;
    return zzs.SU;
    return zzs.SW;
    return zzs.Td;
    return zzs.SV;
    return zzs.Tc;
    return zzs.Tb;
    return RT;
    return RU;
    return RR;
    return RS;
    return RX;
    return zzs.SX;
    return zzs.SY;
    return zzs.Ta;
    return zzs.SZ;
    return FIELD_PERCENTAGE;
    return FIELD_REPETITIONS;
    return FIELD_RESISTANCE;
    return FIELD_RESISTANCE_TYPE;
    return FIELD_REVOLUTIONS;
    return FIELD_RPM;
    return FIELD_SPEED;
    return FIELD_STEPS;
    return RJ;
    return zzs.SQ;
    return zzs.SR;
    return FIELD_VOLUME;
    return FIELD_WATTS;
    return FIELD_WEIGHT;
    return zza.RZ;
    return zza.Sa;
    return zza.Sb;
    return zza.Sc;
    return zza.Sd;
  }
  
  private boolean zza(Field paramField)
  {
    return (this.name.equals(paramField.name)) && (this.format == paramField.format);
  }
  
  static Field zzjj(String paramString)
  {
    return new Field(paramString, 1);
  }
  
  static Field zzjk(String paramString)
  {
    return new Field(paramString, 1, Boolean.valueOf(true));
  }
  
  static Field zzjl(String paramString)
  {
    return new Field(paramString, 2);
  }
  
  static Field zzjm(String paramString)
  {
    return new Field(paramString, 2, Boolean.valueOf(true));
  }
  
  static Field zzjn(String paramString)
  {
    return new Field(paramString, 3);
  }
  
  static Field zzjo(String paramString)
  {
    return new Field(paramString, 4);
  }
  
  static Field zzjp(String paramString)
  {
    return new Field(paramString, 5);
  }
  
  static Field zzjq(String paramString)
  {
    return new Field(paramString, 6);
  }
  
  static Field zzjr(String paramString)
  {
    return new Field(paramString, 7);
  }
  
  static Field zzjs(String paramString)
  {
    return new Field(paramString, 7, Boolean.valueOf(true));
  }
  
  public static Field zzn(String paramString, int paramInt)
  {
    return zza(paramString, paramInt, null);
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    while (((paramObject instanceof Field)) && (zza((Field)paramObject))) {
      return true;
    }
    return false;
  }
  
  public int getFormat()
  {
    return this.format;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  int getVersionCode()
  {
    return this.versionCode;
  }
  
  public int hashCode()
  {
    return this.name.hashCode();
  }
  
  public Boolean isOptional()
  {
    return this.RY;
  }
  
  public String toString()
  {
    String str2 = this.name;
    if (this.format == 1) {}
    for (String str1 = "i";; str1 = "f") {
      return String.format("%s(%s)", new Object[] { str2, str1 });
    }
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzo.zza(this, paramParcel, paramInt);
  }
  
  public static class zza
  {
    public static final Field RZ = Field.zzjl("x");
    public static final Field Sa = Field.zzjl("y");
    public static final Field Sb = Field.zzjl("z");
    public static final Field Sc = Field.zzjs("debug_session");
    public static final Field Sd = Field.zzjs("google.android.fitness.SessionV2");
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/data/Field.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */