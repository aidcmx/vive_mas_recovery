package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.Nullable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zzz.zza;
import com.google.android.gms.fitness.FitnessActivities;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Goal
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<Goal> CREATOR = new zzq();
  public static final int OBJECTIVE_TYPE_DURATION = 2;
  public static final int OBJECTIVE_TYPE_FREQUENCY = 3;
  public static final int OBJECTIVE_TYPE_METRIC = 1;
  private final long Se;
  private final long Sf;
  private final List<Integer> Sg;
  private final Recurrence Sh;
  private final int Si;
  private final MetricObjective Sj;
  private final DurationObjective Sk;
  private final FrequencyObjective Sl;
  private final int versionCode;
  
  Goal(int paramInt1, long paramLong1, long paramLong2, List<Integer> paramList, Recurrence paramRecurrence, int paramInt2, MetricObjective paramMetricObjective, DurationObjective paramDurationObjective, FrequencyObjective paramFrequencyObjective)
  {
    this.versionCode = paramInt1;
    this.Se = paramLong1;
    this.Sf = paramLong2;
    Object localObject = paramList;
    if (paramList == null) {
      localObject = Collections.emptyList();
    }
    this.Sg = ((List)localObject);
    this.Sh = paramRecurrence;
    this.Si = paramInt2;
    this.Sj = paramMetricObjective;
    this.Sk = paramDurationObjective;
    this.Sl = paramFrequencyObjective;
  }
  
  private boolean zza(Goal paramGoal)
  {
    return (this.Se == paramGoal.Se) && (this.Sf == paramGoal.Sf) && (com.google.android.gms.common.internal.zzz.equal(this.Sg, paramGoal.Sg)) && (com.google.android.gms.common.internal.zzz.equal(this.Sh, paramGoal.Sh)) && (this.Si == paramGoal.Si) && (com.google.android.gms.common.internal.zzz.equal(this.Sj, paramGoal.Sj)) && (com.google.android.gms.common.internal.zzz.equal(this.Sk, paramGoal.Sk)) && (com.google.android.gms.common.internal.zzz.equal(this.Sl, paramGoal.Sl));
  }
  
  private static String zzob(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      throw new IllegalArgumentException("invalid objective type value");
    case 1: 
      return "metric";
    case 2: 
      return "duration";
    case 3: 
      return "frequency";
    }
    return "unknown";
  }
  
  private void zzoc(int paramInt)
    throws Goal.MismatchedGoalException
  {
    if (paramInt != this.Si) {
      throw new MismatchedGoalException(String.format("%s goal does not have %s objective", new Object[] { zzob(this.Si), zzob(paramInt) }));
    }
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof Goal)) && (zza((Goal)paramObject)));
  }
  
  @Nullable
  public String getActivityName()
  {
    if ((this.Sg.isEmpty()) || (this.Sg.size() > 1)) {
      return null;
    }
    return FitnessActivities.getName(((Integer)this.Sg.get(0)).intValue());
  }
  
  public long getCreateTime(TimeUnit paramTimeUnit)
  {
    return paramTimeUnit.convert(this.Se, TimeUnit.NANOSECONDS);
  }
  
  public DurationObjective getDurationObjective()
  {
    zzoc(2);
    return this.Sk;
  }
  
  public long getEndTime(Calendar paramCalendar, TimeUnit paramTimeUnit)
  {
    if (this.Sh != null)
    {
      Calendar localCalendar = Calendar.getInstance();
      localCalendar.setTime(paramCalendar.getTime());
      switch (Recurrence.zzb(this.Sh))
      {
      default: 
        int i = Recurrence.zzb(this.Sh);
        throw new IllegalArgumentException(24 + "Invalid unit " + i);
      case 1: 
        localCalendar.add(5, 1);
        localCalendar.set(11, 0);
        return paramTimeUnit.convert(localCalendar.getTimeInMillis(), TimeUnit.MILLISECONDS);
      case 2: 
        localCalendar.add(4, 1);
        localCalendar.set(7, 2);
        localCalendar.set(11, 0);
        return paramTimeUnit.convert(localCalendar.getTimeInMillis(), TimeUnit.MILLISECONDS);
      }
      localCalendar.add(2, 1);
      localCalendar.set(5, 1);
      localCalendar.set(11, 0);
      return paramTimeUnit.convert(localCalendar.getTimeInMillis(), TimeUnit.MILLISECONDS);
    }
    return paramTimeUnit.convert(this.Sf, TimeUnit.NANOSECONDS);
  }
  
  public FrequencyObjective getFrequencyObjective()
  {
    zzoc(3);
    return this.Sl;
  }
  
  public MetricObjective getMetricObjective()
  {
    zzoc(1);
    return this.Sj;
  }
  
  public int getObjectiveType()
  {
    return this.Si;
  }
  
  public Recurrence getRecurrence()
  {
    return this.Sh;
  }
  
  public long getStartTime(Calendar paramCalendar, TimeUnit paramTimeUnit)
  {
    if (this.Sh != null)
    {
      Calendar localCalendar = Calendar.getInstance();
      localCalendar.setTime(paramCalendar.getTime());
      switch (Recurrence.zzb(this.Sh))
      {
      default: 
        int i = Recurrence.zzb(this.Sh);
        throw new IllegalArgumentException(24 + "Invalid unit " + i);
      case 1: 
        localCalendar.set(11, 0);
        return paramTimeUnit.convert(localCalendar.getTimeInMillis(), TimeUnit.MILLISECONDS);
      case 2: 
        localCalendar.set(7, 2);
        localCalendar.set(11, 0);
        return paramTimeUnit.convert(localCalendar.getTimeInMillis(), TimeUnit.MILLISECONDS);
      }
      localCalendar.set(5, 1);
      localCalendar.set(11, 0);
      return paramTimeUnit.convert(localCalendar.getTimeInMillis(), TimeUnit.MILLISECONDS);
    }
    return paramTimeUnit.convert(this.Se, TimeUnit.NANOSECONDS);
  }
  
  int getVersionCode()
  {
    return this.versionCode;
  }
  
  public int hashCode()
  {
    return this.Si;
  }
  
  public String toString()
  {
    return com.google.android.gms.common.internal.zzz.zzx(this).zzg("activity", getActivityName()).zzg("recurrence", this.Sh).zzg("metricObjective", this.Sj).zzg("durationObjective", this.Sk).zzg("frequencyObjective", this.Sl).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzq.zza(this, paramParcel, paramInt);
  }
  
  public long zzber()
  {
    return this.Se;
  }
  
  public long zzbes()
  {
    return this.Sf;
  }
  
  public List<Integer> zzbet()
  {
    return this.Sg;
  }
  
  public MetricObjective zzbeu()
  {
    return this.Sj;
  }
  
  public DurationObjective zzbev()
  {
    return this.Sk;
  }
  
  public FrequencyObjective zzbew()
  {
    return this.Sl;
  }
  
  public static class DurationObjective
    extends AbstractSafeParcelable
  {
    public static final Parcelable.Creator<DurationObjective> CREATOR = new zzn();
    private final long Sm;
    private final int versionCode;
    
    DurationObjective(int paramInt, long paramLong)
    {
      this.versionCode = paramInt;
      this.Sm = paramLong;
    }
    
    public DurationObjective(long paramLong, TimeUnit paramTimeUnit)
    {
      this(1, paramTimeUnit.toNanos(paramLong));
    }
    
    private boolean zza(DurationObjective paramDurationObjective)
    {
      return this.Sm == paramDurationObjective.Sm;
    }
    
    public boolean equals(Object paramObject)
    {
      return (this == paramObject) || (((paramObject instanceof DurationObjective)) && (zza((DurationObjective)paramObject)));
    }
    
    public long getDuration()
    {
      return this.Sm;
    }
    
    public long getDuration(TimeUnit paramTimeUnit)
    {
      return paramTimeUnit.convert(this.Sm, TimeUnit.NANOSECONDS);
    }
    
    int getVersionCode()
    {
      return this.versionCode;
    }
    
    public int hashCode()
    {
      return (int)this.Sm;
    }
    
    public String toString()
    {
      return com.google.android.gms.common.internal.zzz.zzx(this).zzg("duration", Long.valueOf(this.Sm)).toString();
    }
    
    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
      zzn.zza(this, paramParcel, paramInt);
    }
  }
  
  public static class FrequencyObjective
    extends AbstractSafeParcelable
  {
    public static final Parcelable.Creator<FrequencyObjective> CREATOR = new zzp();
    private final int frequency;
    private final int versionCode;
    
    public FrequencyObjective(int paramInt)
    {
      this(1, paramInt);
    }
    
    FrequencyObjective(int paramInt1, int paramInt2)
    {
      this.versionCode = paramInt1;
      this.frequency = paramInt2;
    }
    
    private boolean zza(FrequencyObjective paramFrequencyObjective)
    {
      return this.frequency == paramFrequencyObjective.frequency;
    }
    
    public boolean equals(Object paramObject)
    {
      return (this == paramObject) || (((paramObject instanceof FrequencyObjective)) && (zza((FrequencyObjective)paramObject)));
    }
    
    public int getFrequency()
    {
      return this.frequency;
    }
    
    int getVersionCode()
    {
      return this.versionCode;
    }
    
    public int hashCode()
    {
      return this.frequency;
    }
    
    public String toString()
    {
      return com.google.android.gms.common.internal.zzz.zzx(this).zzg("frequency", Integer.valueOf(this.frequency)).toString();
    }
    
    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
      zzp.zza(this, paramParcel, paramInt);
    }
  }
  
  public static class MetricObjective
    extends AbstractSafeParcelable
  {
    public static final Parcelable.Creator<MetricObjective> CREATOR = new zzv();
    private final String Sn;
    private final double So;
    private final double value;
    private final int versionCode;
    
    MetricObjective(int paramInt, String paramString, double paramDouble1, double paramDouble2)
    {
      this.versionCode = paramInt;
      this.Sn = paramString;
      this.value = paramDouble1;
      this.So = paramDouble2;
    }
    
    public MetricObjective(String paramString, double paramDouble)
    {
      this(1, paramString, paramDouble, 0.0D);
    }
    
    private boolean zza(MetricObjective paramMetricObjective)
    {
      return (com.google.android.gms.common.internal.zzz.equal(this.Sn, paramMetricObjective.Sn)) && (this.value == paramMetricObjective.value) && (this.So == paramMetricObjective.So);
    }
    
    public boolean equals(Object paramObject)
    {
      return (this == paramObject) || (((paramObject instanceof MetricObjective)) && (zza((MetricObjective)paramObject)));
    }
    
    public String getDataTypeName()
    {
      return this.Sn;
    }
    
    public double getValue()
    {
      return this.value;
    }
    
    int getVersionCode()
    {
      return this.versionCode;
    }
    
    public int hashCode()
    {
      return this.Sn.hashCode();
    }
    
    public String toString()
    {
      return com.google.android.gms.common.internal.zzz.zzx(this).zzg("dataTypeName", this.Sn).zzg("value", Double.valueOf(this.value)).zzg("initialValue", Double.valueOf(this.So)).toString();
    }
    
    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
      zzv.zza(this, paramParcel, paramInt);
    }
    
    public double zzbex()
    {
      return this.So;
    }
  }
  
  public static class MismatchedGoalException
    extends IllegalStateException
  {
    public MismatchedGoalException(String paramString)
    {
      super();
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface ObjectiveType {}
  
  public static class Recurrence
    extends AbstractSafeParcelable
  {
    public static final Parcelable.Creator<Recurrence> CREATOR = new zzz();
    public static final int UNIT_DAY = 1;
    public static final int UNIT_MONTH = 3;
    public static final int UNIT_WEEK = 2;
    private final int Sp;
    private final int count;
    private final int versionCode;
    
    public Recurrence(int paramInt1, int paramInt2)
    {
      this(1, paramInt1, paramInt2);
    }
    
    Recurrence(int paramInt1, int paramInt2, int paramInt3)
    {
      this.versionCode = paramInt1;
      this.count = paramInt2;
      if ((paramInt3 > 0) && (paramInt3 <= 3)) {}
      for (boolean bool = true;; bool = false)
      {
        zzaa.zzbs(bool);
        this.Sp = paramInt3;
        return;
      }
    }
    
    private boolean zza(Recurrence paramRecurrence)
    {
      return (this.count == paramRecurrence.count) && (this.Sp == paramRecurrence.Sp);
    }
    
    private static String zzod(int paramInt)
    {
      switch (paramInt)
      {
      default: 
        throw new IllegalArgumentException("invalid unit value");
      case 1: 
        return "day";
      case 2: 
        return "week";
      }
      return "month";
    }
    
    public boolean equals(Object paramObject)
    {
      return (this == paramObject) || (((paramObject instanceof Recurrence)) && (zza((Recurrence)paramObject)));
    }
    
    public int getCount()
    {
      return this.count;
    }
    
    public int getUnit()
    {
      return this.Sp;
    }
    
    int getVersionCode()
    {
      return this.versionCode;
    }
    
    public int hashCode()
    {
      return this.Sp;
    }
    
    public String toString()
    {
      return com.google.android.gms.common.internal.zzz.zzx(this).zzg("count", Integer.valueOf(this.count)).zzg("unit", zzod(this.Sp)).toString();
    }
    
    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
      zzz.zza(this, paramParcel, paramInt);
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public static @interface RecurrenceUnit {}
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/data/Goal.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */