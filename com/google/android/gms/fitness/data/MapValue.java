package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;

public class MapValue
  extends AbstractSafeParcelable
  implements ReflectedParcelable
{
  public static final Parcelable.Creator<MapValue> CREATOR = new zzu();
  private final int Tn;
  private final float To;
  private final int mVersionCode;
  
  public MapValue(int paramInt, float paramFloat)
  {
    this(1, paramInt, paramFloat);
  }
  
  MapValue(int paramInt1, int paramInt2, float paramFloat)
  {
    this.mVersionCode = paramInt1;
    this.Tn = paramInt2;
    this.To = paramFloat;
  }
  
  private boolean zza(MapValue paramMapValue)
  {
    if (this.Tn == paramMapValue.Tn)
    {
      switch (this.Tn)
      {
      default: 
        if (this.To != paramMapValue.To) {
          break;
        }
      case 2: 
        do
        {
          return true;
        } while (asFloat() == paramMapValue.asFloat());
        return false;
      }
      return false;
    }
    return false;
  }
  
  public static MapValue zzd(float paramFloat)
  {
    return new MapValue(2, paramFloat);
  }
  
  public float asFloat()
  {
    if (this.Tn == 2) {}
    for (boolean bool = true;; bool = false)
    {
      zzaa.zza(bool, "Value is not in float format");
      return this.To;
    }
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof MapValue)) && (zza((MapValue)paramObject)));
  }
  
  int getFormat()
  {
    return this.Tn;
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    return (int)this.To;
  }
  
  public String toString()
  {
    switch (this.Tn)
    {
    default: 
      return "unknown";
    }
    return Float.toString(asFloat());
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzu.zza(this, paramParcel, paramInt);
  }
  
  float zzbey()
  {
    return this.To;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/data/MapValue.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */