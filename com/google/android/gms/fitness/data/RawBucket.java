package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

@KeepName
public final class RawBucket
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<RawBucket> CREATOR = new zzw();
  public final Session QB;
  public final long QL;
  public final List<RawDataSet> QN;
  public final int QO;
  public final boolean QP;
  public final int Tp;
  public final long eg;
  final int mVersionCode;
  
  public RawBucket(int paramInt1, long paramLong1, long paramLong2, Session paramSession, int paramInt2, List<RawDataSet> paramList, int paramInt3, boolean paramBoolean)
  {
    this.mVersionCode = paramInt1;
    this.eg = paramLong1;
    this.QL = paramLong2;
    this.QB = paramSession;
    this.Tp = paramInt2;
    this.QN = paramList;
    this.QO = paramInt3;
    this.QP = paramBoolean;
  }
  
  public RawBucket(Bucket paramBucket, List<DataSource> paramList, List<DataType> paramList1)
  {
    this.mVersionCode = 2;
    this.eg = paramBucket.getStartTime(TimeUnit.MILLISECONDS);
    this.QL = paramBucket.getEndTime(TimeUnit.MILLISECONDS);
    this.QB = paramBucket.getSession();
    this.Tp = paramBucket.zzbdw();
    this.QO = paramBucket.getBucketType();
    this.QP = paramBucket.zzbdx();
    paramBucket = paramBucket.getDataSets();
    this.QN = new ArrayList(paramBucket.size());
    paramBucket = paramBucket.iterator();
    while (paramBucket.hasNext())
    {
      DataSet localDataSet = (DataSet)paramBucket.next();
      this.QN.add(new RawDataSet(localDataSet, paramList, paramList1));
    }
  }
  
  private boolean zza(RawBucket paramRawBucket)
  {
    return (this.eg == paramRawBucket.eg) && (this.QL == paramRawBucket.QL) && (this.Tp == paramRawBucket.Tp) && (zzz.equal(this.QN, paramRawBucket.QN)) && (this.QO == paramRawBucket.QO) && (this.QP == paramRawBucket.QP);
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof RawBucket)) && (zza((RawBucket)paramObject)));
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { Long.valueOf(this.eg), Long.valueOf(this.QL), Integer.valueOf(this.QO) });
  }
  
  public String toString()
  {
    return zzz.zzx(this).zzg("startTime", Long.valueOf(this.eg)).zzg("endTime", Long.valueOf(this.QL)).zzg("activity", Integer.valueOf(this.Tp)).zzg("dataSets", this.QN).zzg("bucketType", Integer.valueOf(this.QO)).zzg("serverHasMoreData", Boolean.valueOf(this.QP)).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzw.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/data/RawBucket.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */