package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.internal.zztq;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

@KeepName
public final class RawDataPoint
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<RawDataPoint> CREATOR = new zzx();
  public final long Tq;
  public final long Tr;
  public final Value[] Ts;
  public final int Tt;
  public final int Tu;
  public final long Tv;
  public final long Tw;
  final int versionCode;
  
  public RawDataPoint(int paramInt1, long paramLong1, long paramLong2, Value[] paramArrayOfValue, int paramInt2, int paramInt3, long paramLong3, long paramLong4)
  {
    this.versionCode = paramInt1;
    this.Tq = paramLong1;
    this.Tr = paramLong2;
    this.Tt = paramInt2;
    this.Tu = paramInt3;
    this.Tv = paramLong3;
    this.Tw = paramLong4;
    this.Ts = paramArrayOfValue;
  }
  
  RawDataPoint(DataPoint paramDataPoint, List<DataSource> paramList)
  {
    this.versionCode = 4;
    this.Tq = paramDataPoint.getTimestamp(TimeUnit.NANOSECONDS);
    this.Tr = paramDataPoint.getStartTime(TimeUnit.NANOSECONDS);
    this.Ts = paramDataPoint.zzbea();
    this.Tt = zztq.zza(paramDataPoint.getDataSource(), paramList);
    this.Tu = zztq.zza(paramDataPoint.zzbeb(), paramList);
    this.Tv = paramDataPoint.zzbec();
    this.Tw = paramDataPoint.zzbed();
  }
  
  private boolean zza(RawDataPoint paramRawDataPoint)
  {
    return (this.Tq == paramRawDataPoint.Tq) && (this.Tr == paramRawDataPoint.Tr) && (Arrays.equals(this.Ts, paramRawDataPoint.Ts)) && (this.Tt == paramRawDataPoint.Tt) && (this.Tu == paramRawDataPoint.Tu) && (this.Tv == paramRawDataPoint.Tv);
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof RawDataPoint)) && (zza((RawDataPoint)paramObject)));
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { Long.valueOf(this.Tq), Long.valueOf(this.Tr) });
  }
  
  public String toString()
  {
    return String.format("RawDataPoint{%s@[%s, %s](%d,%d)}", new Object[] { Arrays.toString(this.Ts), Long.valueOf(this.Tr), Long.valueOf(this.Tq), Integer.valueOf(this.Tt), Integer.valueOf(this.Tu) });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzx.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/data/RawDataPoint.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */