package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.internal.zztq;
import java.util.List;

@KeepName
public final class RawDataSet
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<RawDataSet> CREATOR = new zzy();
  public final boolean QP;
  public final int Tt;
  public final int Tx;
  public final List<RawDataPoint> Ty;
  final int mVersionCode;
  
  public RawDataSet(int paramInt1, int paramInt2, int paramInt3, List<RawDataPoint> paramList, boolean paramBoolean)
  {
    this.mVersionCode = paramInt1;
    this.Tt = paramInt2;
    this.Tx = paramInt3;
    this.Ty = paramList;
    this.QP = paramBoolean;
  }
  
  public RawDataSet(DataSet paramDataSet, List<DataSource> paramList, List<DataType> paramList1)
  {
    this.mVersionCode = 3;
    this.Ty = paramDataSet.zzaa(paramList);
    this.QP = paramDataSet.zzbdx();
    this.Tt = zztq.zza(paramDataSet.getDataSource(), paramList);
    this.Tx = zztq.zza(paramDataSet.getDataType(), paramList1);
  }
  
  private boolean zza(RawDataSet paramRawDataSet)
  {
    return (this.Tt == paramRawDataSet.Tt) && (this.QP == paramRawDataSet.QP) && (zzz.equal(this.Ty, paramRawDataSet.Ty));
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof RawDataSet)) && (zza((RawDataSet)paramObject)));
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { Integer.valueOf(this.Tt) });
  }
  
  public String toString()
  {
    return String.format("RawDataSet{%s@[%s]}", new Object[] { Integer.valueOf(this.Tt), this.Ty });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzy.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/data/RawDataSet.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */