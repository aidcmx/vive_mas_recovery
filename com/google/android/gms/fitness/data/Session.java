package com.google.android.gms.fitness.data;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.zzc;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;
import com.google.android.gms.fitness.FitnessActivities;
import java.util.concurrent.TimeUnit;

public class Session
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<Session> CREATOR = new zzaa();
  public static final String EXTRA_SESSION = "vnd.google.fitness.session";
  public static final String MIME_TYPE_PREFIX = "vnd.google.fitness.session/";
  private final long QL;
  private final int QM;
  private final Application TA;
  private final Long TB;
  private final String Tz;
  private final String cg;
  private final long eg;
  private final String mName;
  private final int mVersionCode;
  
  Session(int paramInt1, long paramLong1, long paramLong2, String paramString1, String paramString2, String paramString3, int paramInt2, Application paramApplication, Long paramLong)
  {
    this.mVersionCode = paramInt1;
    this.eg = paramLong1;
    this.QL = paramLong2;
    this.mName = paramString1;
    this.Tz = paramString2;
    this.cg = paramString3;
    this.QM = paramInt2;
    this.TA = paramApplication;
    this.TB = paramLong;
  }
  
  public Session(long paramLong1, long paramLong2, String paramString1, String paramString2, String paramString3, int paramInt, Application paramApplication, Long paramLong)
  {
    this(3, paramLong1, paramLong2, paramString1, paramString2, paramString3, paramInt, paramApplication, paramLong);
  }
  
  private Session(Builder paramBuilder)
  {
    this(Builder.zza(paramBuilder), Builder.zzb(paramBuilder), Builder.zzc(paramBuilder), Builder.zzd(paramBuilder), Builder.zze(paramBuilder), Builder.zzf(paramBuilder), Builder.zzg(paramBuilder), Builder.zzh(paramBuilder));
  }
  
  public static Session extract(Intent paramIntent)
  {
    if (paramIntent == null) {
      return null;
    }
    return (Session)zzc.zza(paramIntent, "vnd.google.fitness.session", CREATOR);
  }
  
  public static String getMimeType(String paramString)
  {
    String str = String.valueOf("vnd.google.fitness.session/");
    paramString = String.valueOf(paramString);
    if (paramString.length() != 0) {
      return str.concat(paramString);
    }
    return new String(str);
  }
  
  private boolean zza(Session paramSession)
  {
    return (this.eg == paramSession.eg) && (this.QL == paramSession.QL) && (zzz.equal(this.mName, paramSession.mName)) && (zzz.equal(this.Tz, paramSession.Tz)) && (zzz.equal(this.cg, paramSession.cg)) && (zzz.equal(this.TA, paramSession.TA)) && (this.QM == paramSession.QM);
  }
  
  public boolean equals(Object paramObject)
  {
    return (paramObject == this) || (((paramObject instanceof Session)) && (zza((Session)paramObject)));
  }
  
  public long getActiveTime(TimeUnit paramTimeUnit)
  {
    if (this.TB != null) {}
    for (boolean bool = true;; bool = false)
    {
      com.google.android.gms.common.internal.zzaa.zza(bool, "Active time is not set");
      return paramTimeUnit.convert(this.TB.longValue(), TimeUnit.MILLISECONDS);
    }
  }
  
  public String getActivity()
  {
    return FitnessActivities.getName(this.QM);
  }
  
  public String getAppPackageName()
  {
    if (this.TA == null) {
      return null;
    }
    return this.TA.getPackageName();
  }
  
  public String getDescription()
  {
    return this.cg;
  }
  
  public long getEndTime(TimeUnit paramTimeUnit)
  {
    return paramTimeUnit.convert(this.QL, TimeUnit.MILLISECONDS);
  }
  
  public String getIdentifier()
  {
    return this.Tz;
  }
  
  public String getName()
  {
    return this.mName;
  }
  
  public long getStartTime(TimeUnit paramTimeUnit)
  {
    return paramTimeUnit.convert(this.eg, TimeUnit.MILLISECONDS);
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public boolean hasActiveTime()
  {
    return this.TB != null;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { Long.valueOf(this.eg), Long.valueOf(this.QL), this.Tz });
  }
  
  public boolean isOngoing()
  {
    return this.QL == 0L;
  }
  
  public String toString()
  {
    return zzz.zzx(this).zzg("startTime", Long.valueOf(this.eg)).zzg("endTime", Long.valueOf(this.QL)).zzg("name", this.mName).zzg("identifier", this.Tz).zzg("description", this.cg).zzg("activity", Integer.valueOf(this.QM)).zzg("application", this.TA).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzaa.zza(this, paramParcel, paramInt);
  }
  
  public long zzagx()
  {
    return this.eg;
  }
  
  public long zzban()
  {
    return this.QL;
  }
  
  public int zzbdw()
  {
    return this.QM;
  }
  
  public Application zzbei()
  {
    return this.TA;
  }
  
  public Long zzbez()
  {
    return this.TB;
  }
  
  public static class Builder
  {
    private long QL = 0L;
    private int QM = 4;
    private Long TB;
    private String Tz;
    private String cg;
    private long eg = 0L;
    private String mName = null;
    
    public Session build()
    {
      boolean bool2 = false;
      boolean bool1;
      if (this.eg > 0L)
      {
        bool1 = true;
        com.google.android.gms.common.internal.zzaa.zza(bool1, "Start time should be specified.");
        if (this.QL != 0L)
        {
          bool1 = bool2;
          if (this.QL <= this.eg) {}
        }
        else
        {
          bool1 = true;
        }
        com.google.android.gms.common.internal.zzaa.zza(bool1, "End time should be later than start time.");
        if (this.Tz == null) {
          if (this.mName != null) {
            break label135;
          }
        }
      }
      label135:
      for (String str = "";; str = this.mName)
      {
        long l = this.eg;
        this.Tz = (String.valueOf(str).length() + 20 + str + l);
        if (this.cg == null) {
          this.cg = "";
        }
        return new Session(this, null);
        bool1 = false;
        break;
      }
    }
    
    public Builder setActiveTime(long paramLong, TimeUnit paramTimeUnit)
    {
      this.TB = Long.valueOf(paramTimeUnit.toMillis(paramLong));
      return this;
    }
    
    public Builder setActivity(String paramString)
    {
      return zzol(FitnessActivities.zzje(paramString));
    }
    
    public Builder setDescription(String paramString)
    {
      if (paramString.length() <= 1000) {}
      for (boolean bool = true;; bool = false)
      {
        com.google.android.gms.common.internal.zzaa.zzb(bool, "Session description cannot exceed %d characters", new Object[] { Integer.valueOf(1000) });
        this.cg = paramString;
        return this;
      }
    }
    
    public Builder setEndTime(long paramLong, TimeUnit paramTimeUnit)
    {
      if (paramLong >= 0L) {}
      for (boolean bool = true;; bool = false)
      {
        com.google.android.gms.common.internal.zzaa.zza(bool, "End time should be positive.");
        this.QL = paramTimeUnit.toMillis(paramLong);
        return this;
      }
    }
    
    public Builder setIdentifier(String paramString)
    {
      if ((paramString != null) && (TextUtils.getTrimmedLength(paramString) > 0)) {}
      for (boolean bool = true;; bool = false)
      {
        com.google.android.gms.common.internal.zzaa.zzbt(bool);
        this.Tz = paramString;
        return this;
      }
    }
    
    public Builder setName(String paramString)
    {
      if (paramString.length() <= 100) {}
      for (boolean bool = true;; bool = false)
      {
        com.google.android.gms.common.internal.zzaa.zzb(bool, "Session name cannot exceed %d characters", new Object[] { Integer.valueOf(100) });
        this.mName = paramString;
        return this;
      }
    }
    
    public Builder setStartTime(long paramLong, TimeUnit paramTimeUnit)
    {
      if (paramLong > 0L) {}
      for (boolean bool = true;; bool = false)
      {
        com.google.android.gms.common.internal.zzaa.zza(bool, "Start time should be positive.");
        this.eg = paramTimeUnit.toMillis(paramLong);
        return this;
      }
    }
    
    public Builder zzol(int paramInt)
    {
      this.QM = paramInt;
      return this;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/data/Session.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */