package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;

public class SessionDataSet
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<SessionDataSet> CREATOR = new zzab();
  private final Session QB;
  private final DataSet TC;
  final int mVersionCode;
  
  SessionDataSet(int paramInt, Session paramSession, DataSet paramDataSet)
  {
    this.mVersionCode = paramInt;
    this.QB = paramSession;
    this.TC = paramDataSet;
  }
  
  private boolean zza(SessionDataSet paramSessionDataSet)
  {
    return (zzz.equal(this.QB, paramSessionDataSet.QB)) && (zzz.equal(this.TC, paramSessionDataSet.TC));
  }
  
  public boolean equals(Object paramObject)
  {
    return (paramObject == this) || (((paramObject instanceof SessionDataSet)) && (zza((SessionDataSet)paramObject)));
  }
  
  public DataSet getDataSet()
  {
    return this.TC;
  }
  
  public Session getSession()
  {
    return this.QB;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.QB, this.TC });
  }
  
  public String toString()
  {
    return zzz.zzx(this).zzg("session", this.QB).zzg("dataSet", this.TC).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzab.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/data/SessionDataSet.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */