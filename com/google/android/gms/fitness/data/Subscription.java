package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;

public class Subscription
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<Subscription> CREATOR = new zzac();
  private final DataType RA;
  private final DataSource Rz;
  private final long TD;
  private final int TE;
  private final int mVersionCode;
  
  Subscription(int paramInt1, DataSource paramDataSource, DataType paramDataType, long paramLong, int paramInt2)
  {
    this.mVersionCode = paramInt1;
    this.Rz = paramDataSource;
    this.RA = paramDataType;
    this.TD = paramLong;
    this.TE = paramInt2;
  }
  
  private Subscription(zza paramzza)
  {
    this.mVersionCode = 1;
    this.RA = zza.zza(paramzza);
    this.Rz = zza.zzb(paramzza);
    this.TD = zza.zzc(paramzza);
    this.TE = zza.zzd(paramzza);
  }
  
  private boolean zza(Subscription paramSubscription)
  {
    return (zzz.equal(this.Rz, paramSubscription.Rz)) && (zzz.equal(this.RA, paramSubscription.RA)) && (this.TD == paramSubscription.TD) && (this.TE == paramSubscription.TE);
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof Subscription)) && (zza((Subscription)paramObject)));
  }
  
  public int getAccuracyMode()
  {
    return this.TE;
  }
  
  public DataSource getDataSource()
  {
    return this.Rz;
  }
  
  public DataType getDataType()
  {
    return this.RA;
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.Rz, this.Rz, Long.valueOf(this.TD), Integer.valueOf(this.TE) });
  }
  
  public String toDebugString()
  {
    if (this.Rz == null) {}
    for (String str = this.RA.getName();; str = this.Rz.toDebugString()) {
      return String.format("Subscription{%s}", new Object[] { str });
    }
  }
  
  public String toString()
  {
    return zzz.zzx(this).zzg("dataSource", this.Rz).zzg("dataType", this.RA).zzg("samplingIntervalMicros", Long.valueOf(this.TD)).zzg("accuracyMode", Integer.valueOf(this.TE)).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzac.zza(this, paramParcel, paramInt);
  }
  
  public long zzbfa()
  {
    return this.TD;
  }
  
  public DataType zzbfb()
  {
    if (this.RA == null) {
      return this.Rz.getDataType();
    }
    return this.RA;
  }
  
  public static class zza
  {
    private DataType RA;
    private DataSource Rz;
    private long TD = -1L;
    private int TE = 2;
    
    public zza zzb(DataSource paramDataSource)
    {
      this.Rz = paramDataSource;
      return this;
    }
    
    public Subscription zzbfc()
    {
      boolean bool2 = false;
      if ((this.Rz != null) || (this.RA != null)) {}
      for (boolean bool1 = true;; bool1 = false)
      {
        zzaa.zza(bool1, "Must call setDataSource() or setDataType()");
        if ((this.RA != null) && (this.Rz != null))
        {
          bool1 = bool2;
          if (!this.RA.equals(this.Rz.getDataType())) {}
        }
        else
        {
          bool1 = true;
        }
        zzaa.zza(bool1, "Specified data type is incompatible with specified data source");
        return new Subscription(this, null);
      }
    }
    
    public zza zzd(DataType paramDataType)
    {
      this.RA = paramDataType;
      return this;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/data/Subscription.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */