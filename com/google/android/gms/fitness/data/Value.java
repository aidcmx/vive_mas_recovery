package com.google.android.gms.fitness.data;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.util.zzm;
import com.google.android.gms.fitness.FitnessActivities;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

public final class Value
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<Value> CREATOR = new zzad();
  private String Fe;
  private boolean TF;
  private Map<String, MapValue> TG;
  private int[] TH;
  private float[] TI;
  private byte[] TJ;
  private final int format;
  private float value;
  private final int versionCode;
  
  public Value(int paramInt)
  {
    this(3, paramInt, false, 0.0F, null, null, null, null, null);
  }
  
  Value(int paramInt1, int paramInt2, boolean paramBoolean, float paramFloat, String paramString, Bundle paramBundle, int[] paramArrayOfInt, float[] paramArrayOfFloat, byte[] paramArrayOfByte)
  {
    this.versionCode = paramInt1;
    this.format = paramInt2;
    this.TF = paramBoolean;
    this.value = paramFloat;
    this.Fe = paramString;
    this.TG = zzaa(paramBundle);
    this.TH = paramArrayOfInt;
    this.TI = paramArrayOfFloat;
    this.TJ = paramArrayOfByte;
  }
  
  private boolean zza(Value paramValue)
  {
    if ((this.format == paramValue.format) && (this.TF == paramValue.TF))
    {
      switch (this.format)
      {
      default: 
        if (this.value != paramValue.value) {
          break;
        }
      case 1: 
      case 2: 
        do
        {
          do
          {
            return true;
          } while (asInt() == paramValue.asInt());
          return false;
        } while (this.value == paramValue.value);
        return false;
      case 3: 
        return zzz.equal(this.Fe, paramValue.Fe);
      case 4: 
        return zzz.equal(this.TG, paramValue.TG);
      case 5: 
        return Arrays.equals(this.TH, paramValue.TH);
      case 6: 
        return Arrays.equals(this.TI, paramValue.TI);
      case 7: 
        return Arrays.equals(this.TJ, paramValue.TJ);
      }
      return false;
    }
    return false;
  }
  
  private static Map<String, MapValue> zzaa(Bundle paramBundle)
  {
    if (paramBundle == null) {
      return null;
    }
    paramBundle.setClassLoader(MapValue.class.getClassLoader());
    ArrayMap localArrayMap = new ArrayMap(paramBundle.size());
    Iterator localIterator = paramBundle.keySet().iterator();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      localArrayMap.put(str, (MapValue)paramBundle.getParcelable(str));
    }
    return localArrayMap;
  }
  
  public String asActivity()
  {
    return FitnessActivities.getName(asInt());
  }
  
  public float asFloat()
  {
    if (this.format == 2) {}
    for (boolean bool = true;; bool = false)
    {
      zzaa.zza(bool, "Value is not in float format");
      return this.value;
    }
  }
  
  public int asInt()
  {
    boolean bool = true;
    if (this.format == 1) {}
    for (;;)
    {
      zzaa.zza(bool, "Value is not in int format");
      return Float.floatToRawIntBits(this.value);
      bool = false;
    }
  }
  
  public String asString()
  {
    if (this.format == 3) {}
    for (boolean bool = true;; bool = false)
    {
      zzaa.zza(bool, "Value is not in string format");
      return this.Fe;
    }
  }
  
  public void clearKey(String paramString)
  {
    if (this.format == 4) {}
    for (boolean bool = true;; bool = false)
    {
      zzaa.zza(bool, "Attempting to set a key's value to a field that is not in FLOAT_MAP format.  Please check the data type definition and use the right format.");
      if (this.TG != null) {
        this.TG.remove(paramString);
      }
      return;
    }
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof Value)) && (zza((Value)paramObject)));
  }
  
  public int getFormat()
  {
    return this.format;
  }
  
  @Nullable
  public Float getKeyValue(String paramString)
  {
    if (this.format == 4) {}
    for (boolean bool = true;; bool = false)
    {
      zzaa.zza(bool, "Value is not in float map format");
      if ((this.TG == null) || (!this.TG.containsKey(paramString))) {
        break;
      }
      return Float.valueOf(((MapValue)this.TG.get(paramString)).asFloat());
    }
    return null;
  }
  
  int getVersionCode()
  {
    return this.versionCode;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { Float.valueOf(this.value), this.Fe, this.TG, this.TH, this.TI, this.TJ });
  }
  
  public boolean isSet()
  {
    return this.TF;
  }
  
  public void setActivity(String paramString)
  {
    setInt(FitnessActivities.zzje(paramString));
  }
  
  public void setFloat(float paramFloat)
  {
    if (this.format == 2) {}
    for (boolean bool = true;; bool = false)
    {
      zzaa.zza(bool, "Attempting to set an float value to a field that is not in FLOAT format.  Please check the data type definition and use the right format.");
      this.TF = true;
      this.value = paramFloat;
      return;
    }
  }
  
  public void setInt(int paramInt)
  {
    if (this.format == 1) {}
    for (boolean bool = true;; bool = false)
    {
      zzaa.zza(bool, "Attempting to set an int value to a field that is not in INT32 format.  Please check the data type definition and use the right format.");
      this.TF = true;
      this.value = Float.intBitsToFloat(paramInt);
      return;
    }
  }
  
  public void setKeyValue(String paramString, float paramFloat)
  {
    if (this.format == 4) {}
    for (boolean bool = true;; bool = false)
    {
      zzaa.zza(bool, "Attempting to set a key's value to a field that is not in FLOAT_MAP format.  Please check the data type definition and use the right format.");
      this.TF = true;
      if (this.TG == null) {
        this.TG = new HashMap();
      }
      this.TG.put(paramString, MapValue.zzd(paramFloat));
      return;
    }
  }
  
  public void setString(String paramString)
  {
    if (this.format == 3) {}
    for (boolean bool = true;; bool = false)
    {
      zzaa.zza(bool, "Attempting to set a string value to a field that is not in STRING format.  Please check the data type definition and use the right format.");
      this.TF = true;
      this.Fe = paramString;
      return;
    }
  }
  
  public String toString()
  {
    if (!this.TF) {
      return "unset";
    }
    switch (this.format)
    {
    default: 
      return "unknown";
    case 1: 
      return Integer.toString(asInt());
    case 2: 
      return Float.toString(this.value);
    case 3: 
      return this.Fe;
    case 4: 
      return new TreeMap(this.TG).toString();
    case 5: 
      return Arrays.toString(this.TH);
    case 6: 
      return Arrays.toString(this.TI);
    }
    return zzm.zza(this.TJ, 0, this.TJ.length, false);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzad.zza(this, paramParcel, paramInt);
  }
  
  float zzbey()
  {
    return this.value;
  }
  
  String zzbfd()
  {
    return this.Fe;
  }
  
  Bundle zzbfe()
  {
    if (this.TG == null) {
      return null;
    }
    Bundle localBundle = new Bundle(this.TG.size());
    Iterator localIterator = this.TG.entrySet().iterator();
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      localBundle.putParcelable((String)localEntry.getKey(), (Parcelable)localEntry.getValue());
    }
    return localBundle;
  }
  
  int[] zzbff()
  {
    return this.TH;
  }
  
  float[] zzbfg()
  {
    return this.TI;
  }
  
  byte[] zzbfh()
  {
    return this.TJ;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/data/Value.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */