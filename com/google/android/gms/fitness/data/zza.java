package com.google.android.gms.fitness.data;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class zza
{
  static final Map<DataType, List<DataType>> QF = new HashMap();
  
  static
  {
    QF.put(DataType.TYPE_ACTIVITY_SEGMENT, Collections.singletonList(DataType.AGGREGATE_ACTIVITY_SUMMARY));
    QF.put(DataType.TYPE_BASAL_METABOLIC_RATE, Collections.singletonList(DataType.AGGREGATE_BASAL_METABOLIC_RATE_SUMMARY));
    QF.put(DataType.TYPE_BODY_FAT_PERCENTAGE, Collections.singletonList(DataType.AGGREGATE_BODY_FAT_PERCENTAGE_SUMMARY));
    QF.put(DataType.Rn, Collections.singletonList(DataType.Rp));
    QF.put(DataType.Rm, Collections.singletonList(DataType.Rq));
    QF.put(DataType.TYPE_CALORIES_CONSUMED, Collections.singletonList(DataType.AGGREGATE_CALORIES_CONSUMED));
    QF.put(DataType.TYPE_CALORIES_EXPENDED, Collections.singletonList(DataType.AGGREGATE_CALORIES_EXPENDED));
    QF.put(DataType.TYPE_DISTANCE_DELTA, Collections.singletonList(DataType.AGGREGATE_DISTANCE_DELTA));
    QF.put(DataType.Ri, Collections.singletonList(DataType.Ro));
    QF.put(DataType.TYPE_LOCATION_SAMPLE, Collections.singletonList(DataType.AGGREGATE_LOCATION_BOUNDING_BOX));
    QF.put(DataType.TYPE_NUTRITION, Collections.singletonList(DataType.AGGREGATE_NUTRITION_SUMMARY));
    QF.put(DataType.TYPE_HYDRATION, Collections.singletonList(DataType.AGGREGATE_HYDRATION));
    QF.put(DataType.TYPE_HEART_RATE_BPM, Collections.singletonList(DataType.AGGREGATE_HEART_RATE_SUMMARY));
    QF.put(DataType.TYPE_POWER_SAMPLE, Collections.singletonList(DataType.AGGREGATE_POWER_SUMMARY));
    QF.put(DataType.TYPE_SPEED, Collections.singletonList(DataType.AGGREGATE_SPEED_SUMMARY));
    QF.put(DataType.TYPE_STEP_COUNT_DELTA, Collections.singletonList(DataType.AGGREGATE_STEP_COUNT_DELTA));
    QF.put(DataType.TYPE_WEIGHT, Collections.singletonList(DataType.AGGREGATE_WEIGHT_SUMMARY));
    QF.put(zzr.Sq, Collections.singletonList(zzr.SA));
    QF.put(zzr.Sr, Collections.singletonList(zzr.SB));
    QF.put(zzr.Ss, Collections.singletonList(zzr.SC));
    QF.put(zzr.St, Collections.singletonList(zzr.SD));
    QF.put(zzr.Su, Collections.singletonList(zzr.SE));
    QF.put(zzr.Sv, Collections.singletonList(zzr.Sv));
    QF.put(zzr.Sw, Collections.singletonList(zzr.Sw));
    QF.put(zzr.Sx, Collections.singletonList(zzr.Sx));
    QF.put(zzr.Sy, Collections.singletonList(zzr.Sy));
    QF.put(zzr.Sz, Collections.singletonList(zzr.Sz));
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/data/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */