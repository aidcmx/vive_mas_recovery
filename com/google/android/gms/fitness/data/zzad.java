package com.google.android.gms.fitness.data;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzad
  implements Parcelable.Creator<Value>
{
  static void zza(Value paramValue, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramValue.getFormat());
    zzb.zza(paramParcel, 2, paramValue.isSet());
    zzb.zza(paramParcel, 3, paramValue.zzbey());
    zzb.zza(paramParcel, 4, paramValue.zzbfd(), false);
    zzb.zza(paramParcel, 5, paramValue.zzbfe(), false);
    zzb.zza(paramParcel, 6, paramValue.zzbff(), false);
    zzb.zza(paramParcel, 7, paramValue.zzbfg(), false);
    zzb.zzc(paramParcel, 1000, paramValue.getVersionCode());
    zzb.zza(paramParcel, 8, paramValue.zzbfh(), false);
    zzb.zzaj(paramParcel, paramInt);
  }
  
  public Value zzjg(Parcel paramParcel)
  {
    boolean bool = false;
    byte[] arrayOfByte = null;
    int k = zza.zzcr(paramParcel);
    float f = 0.0F;
    float[] arrayOfFloat = null;
    int[] arrayOfInt = null;
    Bundle localBundle = null;
    String str = null;
    int i = 0;
    int j = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.zzcq(paramParcel);
      switch (zza.zzgu(m))
      {
      default: 
        zza.zzb(paramParcel, m);
        break;
      case 1: 
        i = zza.zzg(paramParcel, m);
        break;
      case 2: 
        bool = zza.zzc(paramParcel, m);
        break;
      case 3: 
        f = zza.zzl(paramParcel, m);
        break;
      case 4: 
        str = zza.zzq(paramParcel, m);
        break;
      case 5: 
        localBundle = zza.zzs(paramParcel, m);
        break;
      case 6: 
        arrayOfInt = zza.zzw(paramParcel, m);
        break;
      case 7: 
        arrayOfFloat = zza.zzz(paramParcel, m);
        break;
      case 1000: 
        j = zza.zzg(paramParcel, m);
        break;
      case 8: 
        arrayOfByte = zza.zzt(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza(37 + "Overread allowed size end=" + k, paramParcel);
    }
    return new Value(j, i, bool, f, str, localBundle, arrayOfInt, arrayOfFloat, arrayOfByte);
  }
  
  public Value[] zzop(int paramInt)
  {
    return new Value[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/data/zzad.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */