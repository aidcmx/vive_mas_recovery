package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.ArrayList;

public class zzc
  implements Parcelable.Creator<BleDevice>
{
  static void zza(BleDevice paramBleDevice, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.zzcs(paramParcel);
    zzb.zza(paramParcel, 1, paramBleDevice.getAddress(), false);
    zzb.zza(paramParcel, 2, paramBleDevice.getName(), false);
    zzb.zzb(paramParcel, 3, paramBleDevice.getSupportedProfiles(), false);
    zzb.zzc(paramParcel, 4, paramBleDevice.getDataTypes(), false);
    zzb.zzc(paramParcel, 1000, paramBleDevice.getVersionCode());
    zzb.zzaj(paramParcel, paramInt);
  }
  
  public BleDevice zzil(Parcel paramParcel)
  {
    ArrayList localArrayList1 = null;
    int j = zza.zzcr(paramParcel);
    int i = 0;
    ArrayList localArrayList2 = null;
    String str1 = null;
    String str2 = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.zzcq(paramParcel);
      switch (zza.zzgu(k))
      {
      default: 
        zza.zzb(paramParcel, k);
        break;
      case 1: 
        str2 = zza.zzq(paramParcel, k);
        break;
      case 2: 
        str1 = zza.zzq(paramParcel, k);
        break;
      case 3: 
        localArrayList2 = zza.zzae(paramParcel, k);
        break;
      case 4: 
        localArrayList1 = zza.zzc(paramParcel, k, DataType.CREATOR);
        break;
      case 1000: 
        i = zza.zzg(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new BleDevice(i, str2, str1, localArrayList2, localArrayList1);
  }
  
  public BleDevice[] zznl(int paramInt)
  {
    return new BleDevice[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/data/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */