package com.google.android.gms.fitness.data;

import com.google.android.gms.internal.zztr;
import com.google.android.gms.internal.zzts;
import java.util.concurrent.TimeUnit;

class zze
  implements zztr<DataPoint, DataType>
{
  public static final zze QQ = new zze();
  
  public long zza(DataPoint paramDataPoint, TimeUnit paramTimeUnit)
  {
    return paramDataPoint.getEndTime(paramTimeUnit) - paramDataPoint.getStartTime(paramTimeUnit);
  }
  
  public String zza(DataPoint paramDataPoint)
  {
    return paramDataPoint.getDataType().getName();
  }
  
  public boolean zza(DataPoint paramDataPoint, int paramInt)
  {
    return paramDataPoint.zzno(paramInt).isSet();
  }
  
  public int zzb(DataPoint paramDataPoint, int paramInt)
  {
    return paramDataPoint.zzno(paramInt).asInt();
  }
  
  public DataType zzb(DataPoint paramDataPoint)
  {
    return paramDataPoint.getDataType();
  }
  
  public zzts<DataType> zzbdy()
  {
    return zzf.QR;
  }
  
  public double zzc(DataPoint paramDataPoint, int paramInt)
  {
    return paramDataPoint.zzno(paramInt).asFloat();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/data/zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */