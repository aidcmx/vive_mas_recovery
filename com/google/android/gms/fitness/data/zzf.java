package com.google.android.gms.fitness.data;

import com.google.android.gms.internal.zzts;
import java.util.List;

class zzf
  implements zzts<DataType>
{
  public static final zzf QR = new zzf();
  
  private Field zzd(DataType paramDataType, int paramInt)
  {
    return (Field)paramDataType.getFields().get(paramInt);
  }
  
  public String zza(DataType paramDataType)
  {
    return paramDataType.getName();
  }
  
  public String zza(DataType paramDataType, int paramInt)
  {
    return zzd(paramDataType, paramInt).getName();
  }
  
  public int zzb(DataType paramDataType)
  {
    return paramDataType.getFields().size();
  }
  
  public boolean zzb(DataType paramDataType, int paramInt)
  {
    return Boolean.TRUE.equals(zzd(paramDataType, paramInt).isOptional());
  }
  
  public int zzc(DataType paramDataType, int paramInt)
  {
    return zzd(paramDataType, paramInt).getFormat();
  }
  
  public boolean zzjg(String paramString)
  {
    return zzk.zzjh(paramString) != null;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/data/zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */