package com.google.android.gms.fitness.data;

public class zzk
{
  public static final DataType[] Ru = { DataType.Rj, DataType.TYPE_WORKOUT_EXERCISE, DataType.TYPE_ACTIVITY_SAMPLE, DataType.TYPE_ACTIVITY_SAMPLES, DataType.TYPE_ACTIVITY_SEGMENT, DataType.AGGREGATE_ACTIVITY_SUMMARY, zzr.Su, zzr.SE, zzr.Sr, zzr.SB, zzr.Sq, zzr.SA, DataType.TYPE_BODY_FAT_PERCENTAGE, DataType.AGGREGATE_BODY_FAT_PERCENTAGE_SUMMARY, DataType.Rn, DataType.Rp, zzr.St, zzr.SD, DataType.Rm, DataType.Rq, DataType.TYPE_BASAL_METABOLIC_RATE, DataType.AGGREGATE_BASAL_METABOLIC_RATE_SUMMARY, DataType.TYPE_CALORIES_CONSUMED, DataType.TYPE_CALORIES_EXPENDED, zzr.Sv, zzr.Sw, DataType.TYPE_CYCLING_PEDALING_CADENCE, DataType.TYPE_CYCLING_PEDALING_CUMULATIVE, DataType.TYPE_CYCLING_WHEEL_REVOLUTION, DataType.TYPE_CYCLING_WHEEL_RPM, DataType.TYPE_DISTANCE_CUMULATIVE, DataType.TYPE_DISTANCE_DELTA, DataType.Ri, DataType.Ro, DataType.TYPE_HEART_RATE_BPM, DataType.AGGREGATE_HEART_RATE_SUMMARY, DataType.TYPE_HEIGHT, DataType.AGGREGATE_LOCATION_BOUNDING_BOX, DataType.TYPE_LOCATION_SAMPLE, DataType.TYPE_LOCATION_TRACK, zzr.Sx, DataType.TYPE_NUTRITION, DataType.TYPE_HYDRATION, DataType.AGGREGATE_NUTRITION_SUMMARY, zzr.Sy, zzr.Ss, zzr.SC, DataType.TYPE_POWER_SAMPLE, DataType.AGGREGATE_POWER_SUMMARY, DataType.Rl, DataType.Rk, DataType.TYPE_SPEED, DataType.AGGREGATE_SPEED_SUMMARY, DataType.TYPE_STEP_COUNT_CADENCE, DataType.Rh, DataType.TYPE_STEP_COUNT_CUMULATIVE, DataType.TYPE_STEP_COUNT_DELTA, zzr.Sz, DataType.TYPE_WEIGHT, DataType.AGGREGATE_WEIGHT_SUMMARY };
  public static final DataType[] Rv = { zzr.Su, zzr.SE, zzr.Sr, zzr.SB, zzr.Sq, zzr.SA, zzr.St, zzr.SD, zzr.Sv, zzr.Sw, zzr.Sx, zzr.Sy, zzr.Ss, zzr.SC, zzr.Sz };
  
  public static DataType zzjh(String paramString)
  {
    int i = -1;
    switch (paramString.hashCode())
    {
    }
    for (;;)
    {
      switch (i)
      {
      default: 
        return null;
        if (paramString.equals("com.google.accelerometer"))
        {
          i = 0;
          continue;
          if (paramString.equals("com.google.activity.exercise"))
          {
            i = 1;
            continue;
            if (paramString.equals("com.google.activity.sample"))
            {
              i = 2;
              continue;
              if (paramString.equals("com.google.activity.samples"))
              {
                i = 3;
                continue;
                if (paramString.equals("com.google.activity.segment"))
                {
                  i = 4;
                  continue;
                  if (paramString.equals("com.google.activity.summary"))
                  {
                    i = 5;
                    continue;
                    if (paramString.equals("com.google.body.temperature.basal"))
                    {
                      i = 6;
                      continue;
                      if (paramString.equals("com.google.body.temperature.basal.summary"))
                      {
                        i = 7;
                        continue;
                        if (paramString.equals("com.google.body.fat.percentage"))
                        {
                          i = 8;
                          continue;
                          if (paramString.equals("com.google.blood_glucose"))
                          {
                            i = 9;
                            continue;
                            if (paramString.equals("com.google.blood_glucose.summary"))
                            {
                              i = 10;
                              continue;
                              if (paramString.equals("com.google.blood_pressure"))
                              {
                                i = 11;
                                continue;
                                if (paramString.equals("com.google.blood_pressure.summary"))
                                {
                                  i = 12;
                                  continue;
                                  if (paramString.equals("com.google.body.fat.percentage.summary"))
                                  {
                                    i = 13;
                                    continue;
                                    if (paramString.equals("com.google.body.hip.circumference"))
                                    {
                                      i = 14;
                                      continue;
                                      if (paramString.equals("com.google.body.hip.circumference.summary"))
                                      {
                                        i = 15;
                                        continue;
                                        if (paramString.equals("com.google.body.temperature"))
                                        {
                                          i = 16;
                                          continue;
                                          if (paramString.equals("com.google.body.temperature.summary"))
                                          {
                                            i = 17;
                                            continue;
                                            if (paramString.equals("com.google.body.waist.circumference"))
                                            {
                                              i = 18;
                                              continue;
                                              if (paramString.equals("com.google.body.waist.circumference.summary"))
                                              {
                                                i = 19;
                                                continue;
                                                if (paramString.equals("com.google.calories.bmr"))
                                                {
                                                  i = 20;
                                                  continue;
                                                  if (paramString.equals("com.google.calories.bmr.summary"))
                                                  {
                                                    i = 21;
                                                    continue;
                                                    if (paramString.equals("com.google.calories.consumed"))
                                                    {
                                                      i = 22;
                                                      continue;
                                                      if (paramString.equals("com.google.calories.expended"))
                                                      {
                                                        i = 23;
                                                        continue;
                                                        if (paramString.equals("com.google.cervical_mucus"))
                                                        {
                                                          i = 24;
                                                          continue;
                                                          if (paramString.equals("com.google.cervical_position"))
                                                          {
                                                            i = 25;
                                                            continue;
                                                            if (paramString.equals("com.google.cycling.pedaling.cadence"))
                                                            {
                                                              i = 26;
                                                              continue;
                                                              if (paramString.equals("com.google.cycling.pedaling.cumulative"))
                                                              {
                                                                i = 27;
                                                                continue;
                                                                if (paramString.equals("com.google.cycling.wheel_revolution.cumulative"))
                                                                {
                                                                  i = 28;
                                                                  continue;
                                                                  if (paramString.equals("com.google.cycling.wheel_revolution.rpm"))
                                                                  {
                                                                    i = 29;
                                                                    continue;
                                                                    if (paramString.equals("com.google.distance.cumulative"))
                                                                    {
                                                                      i = 30;
                                                                      continue;
                                                                      if (paramString.equals("com.google.distance.delta"))
                                                                      {
                                                                        i = 31;
                                                                        continue;
                                                                        if (paramString.equals("com.google.floor_change"))
                                                                        {
                                                                          i = 32;
                                                                          continue;
                                                                          if (paramString.equals("com.google.floor_change.summary"))
                                                                          {
                                                                            i = 33;
                                                                            continue;
                                                                            if (paramString.equals("com.google.heart_rate.bpm"))
                                                                            {
                                                                              i = 34;
                                                                              continue;
                                                                              if (paramString.equals("com.google.heart_rate.summary"))
                                                                              {
                                                                                i = 35;
                                                                                continue;
                                                                                if (paramString.equals("com.google.height"))
                                                                                {
                                                                                  i = 36;
                                                                                  continue;
                                                                                  if (paramString.equals("com.google.location.bounding_box"))
                                                                                  {
                                                                                    i = 37;
                                                                                    continue;
                                                                                    if (paramString.equals("com.google.location.sample"))
                                                                                    {
                                                                                      i = 38;
                                                                                      continue;
                                                                                      if (paramString.equals("com.google.location.track"))
                                                                                      {
                                                                                        i = 39;
                                                                                        continue;
                                                                                        if (paramString.equals("com.google.menstruation"))
                                                                                        {
                                                                                          i = 40;
                                                                                          continue;
                                                                                          if (paramString.equals("com.google.nutrition"))
                                                                                          {
                                                                                            i = 41;
                                                                                            continue;
                                                                                            if (paramString.equals("com.google.hydration"))
                                                                                            {
                                                                                              i = 42;
                                                                                              continue;
                                                                                              if (paramString.equals("com.google.nutrition.summary"))
                                                                                              {
                                                                                                i = 43;
                                                                                                continue;
                                                                                                if (paramString.equals("com.google.ovulation_test"))
                                                                                                {
                                                                                                  i = 44;
                                                                                                  continue;
                                                                                                  if (paramString.equals("com.google.oxygen_saturation"))
                                                                                                  {
                                                                                                    i = 45;
                                                                                                    continue;
                                                                                                    if (paramString.equals("com.google.oxygen_saturation.summary"))
                                                                                                    {
                                                                                                      i = 46;
                                                                                                      continue;
                                                                                                      if (paramString.equals("com.google.power.sample"))
                                                                                                      {
                                                                                                        i = 47;
                                                                                                        continue;
                                                                                                        if (paramString.equals("com.google.power.summary"))
                                                                                                        {
                                                                                                          i = 48;
                                                                                                          continue;
                                                                                                          if (paramString.equals("com.google.sensor.const_rate_events"))
                                                                                                          {
                                                                                                            i = 49;
                                                                                                            continue;
                                                                                                            if (paramString.equals("com.google.sensor.events"))
                                                                                                            {
                                                                                                              i = 50;
                                                                                                              continue;
                                                                                                              if (paramString.equals("com.google.speed"))
                                                                                                              {
                                                                                                                i = 51;
                                                                                                                continue;
                                                                                                                if (paramString.equals("com.google.internal.session.debug"))
                                                                                                                {
                                                                                                                  i = 52;
                                                                                                                  continue;
                                                                                                                  if (paramString.equals("com.google.internal.session.v2"))
                                                                                                                  {
                                                                                                                    i = 53;
                                                                                                                    continue;
                                                                                                                    if (paramString.equals("com.google.speed.summary"))
                                                                                                                    {
                                                                                                                      i = 54;
                                                                                                                      continue;
                                                                                                                      if (paramString.equals("com.google.step_count.cadence"))
                                                                                                                      {
                                                                                                                        i = 55;
                                                                                                                        continue;
                                                                                                                        if (paramString.equals("com.google.step_count.cumulative"))
                                                                                                                        {
                                                                                                                          i = 56;
                                                                                                                          continue;
                                                                                                                          if (paramString.equals("com.google.step_count.delta"))
                                                                                                                          {
                                                                                                                            i = 57;
                                                                                                                            continue;
                                                                                                                            if (paramString.equals("com.google.stride_model"))
                                                                                                                            {
                                                                                                                              i = 58;
                                                                                                                              continue;
                                                                                                                              if (paramString.equals("com.google.vaginal_spotting"))
                                                                                                                              {
                                                                                                                                i = 59;
                                                                                                                                continue;
                                                                                                                                if (paramString.equals("com.google.weight"))
                                                                                                                                {
                                                                                                                                  i = 60;
                                                                                                                                  continue;
                                                                                                                                  if (paramString.equals("com.google.weight.summary")) {
                                                                                                                                    i = 61;
                                                                                                                                  }
                                                                                                                                }
                                                                                                                              }
                                                                                                                            }
                                                                                                                          }
                                                                                                                        }
                                                                                                                      }
                                                                                                                    }
                                                                                                                  }
                                                                                                                }
                                                                                                              }
                                                                                                            }
                                                                                                          }
                                                                                                        }
                                                                                                      }
                                                                                                    }
                                                                                                  }
                                                                                                }
                                                                                              }
                                                                                            }
                                                                                          }
                                                                                        }
                                                                                      }
                                                                                    }
                                                                                  }
                                                                                }
                                                                              }
                                                                            }
                                                                          }
                                                                        }
                                                                      }
                                                                    }
                                                                  }
                                                                }
                                                              }
                                                            }
                                                          }
                                                        }
                                                      }
                                                    }
                                                  }
                                                }
                                              }
                                            }
                                          }
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
        break;
      }
    }
    return DataType.Rj;
    return DataType.TYPE_WORKOUT_EXERCISE;
    return DataType.TYPE_ACTIVITY_SAMPLE;
    return DataType.TYPE_ACTIVITY_SAMPLES;
    return DataType.TYPE_ACTIVITY_SEGMENT;
    return DataType.AGGREGATE_ACTIVITY_SUMMARY;
    return zzr.Su;
    return zzr.SE;
    return DataType.TYPE_BODY_FAT_PERCENTAGE;
    return zzr.Sr;
    return zzr.SB;
    return zzr.Sq;
    return zzr.SA;
    return DataType.AGGREGATE_BODY_FAT_PERCENTAGE_SUMMARY;
    return DataType.Rn;
    return DataType.Rp;
    return zzr.St;
    return zzr.SD;
    return DataType.Rm;
    return DataType.Rq;
    return DataType.TYPE_BASAL_METABOLIC_RATE;
    return DataType.AGGREGATE_BASAL_METABOLIC_RATE_SUMMARY;
    return DataType.TYPE_CALORIES_CONSUMED;
    return DataType.TYPE_CALORIES_EXPENDED;
    return zzr.Sv;
    return zzr.Sw;
    return DataType.TYPE_CYCLING_PEDALING_CADENCE;
    return DataType.TYPE_CYCLING_PEDALING_CUMULATIVE;
    return DataType.TYPE_CYCLING_WHEEL_REVOLUTION;
    return DataType.TYPE_CYCLING_WHEEL_RPM;
    return DataType.TYPE_DISTANCE_CUMULATIVE;
    return DataType.TYPE_DISTANCE_DELTA;
    return DataType.Ri;
    return DataType.Ro;
    return DataType.TYPE_HEART_RATE_BPM;
    return DataType.AGGREGATE_HEART_RATE_SUMMARY;
    return DataType.TYPE_HEIGHT;
    return DataType.AGGREGATE_LOCATION_BOUNDING_BOX;
    return DataType.TYPE_LOCATION_SAMPLE;
    return DataType.TYPE_LOCATION_TRACK;
    return zzr.Sx;
    return DataType.TYPE_NUTRITION;
    return DataType.TYPE_HYDRATION;
    return DataType.AGGREGATE_NUTRITION_SUMMARY;
    return zzr.Sy;
    return zzr.Ss;
    return zzr.SC;
    return DataType.TYPE_POWER_SAMPLE;
    return DataType.AGGREGATE_POWER_SUMMARY;
    return DataType.Rl;
    return DataType.Rk;
    return DataType.TYPE_SPEED;
    return DataType.zza.Rs;
    return DataType.zza.Rt;
    return DataType.AGGREGATE_SPEED_SUMMARY;
    return DataType.TYPE_STEP_COUNT_CADENCE;
    return DataType.TYPE_STEP_COUNT_CUMULATIVE;
    return DataType.TYPE_STEP_COUNT_DELTA;
    return DataType.Rh;
    return zzr.Sz;
    return DataType.TYPE_WEIGHT;
    return DataType.AGGREGATE_WEIGHT_SUMMARY;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/data/zzk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */