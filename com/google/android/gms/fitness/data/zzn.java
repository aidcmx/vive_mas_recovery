package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzn
  implements Parcelable.Creator<Goal.DurationObjective>
{
  static void zza(Goal.DurationObjective paramDurationObjective, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.zzcs(paramParcel);
    zzb.zza(paramParcel, 1, paramDurationObjective.getDuration());
    zzb.zzc(paramParcel, 1000, paramDurationObjective.getVersionCode());
    zzb.zzaj(paramParcel, paramInt);
  }
  
  public Goal.DurationObjective zzit(Parcel paramParcel)
  {
    int j = zza.zzcr(paramParcel);
    int i = 0;
    long l = 0L;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.zzcq(paramParcel);
      switch (zza.zzgu(k))
      {
      default: 
        zza.zzb(paramParcel, k);
        break;
      case 1: 
        l = zza.zzi(paramParcel, k);
        break;
      case 1000: 
        i = zza.zzg(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new Goal.DurationObjective(i, l);
  }
  
  public Goal.DurationObjective[] zzny(int paramInt)
  {
    return new Goal.DurationObjective[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/data/zzn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */