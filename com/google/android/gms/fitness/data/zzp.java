package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzp
  implements Parcelable.Creator<Goal.FrequencyObjective>
{
  static void zza(Goal.FrequencyObjective paramFrequencyObjective, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramFrequencyObjective.getFrequency());
    zzb.zzc(paramParcel, 1000, paramFrequencyObjective.getVersionCode());
    zzb.zzaj(paramParcel, paramInt);
  }
  
  public Goal.FrequencyObjective zziv(Parcel paramParcel)
  {
    int j = 0;
    int k = zza.zzcr(paramParcel);
    int i = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.zzcq(paramParcel);
      switch (zza.zzgu(m))
      {
      default: 
        zza.zzb(paramParcel, m);
        break;
      case 1: 
        j = zza.zzg(paramParcel, m);
        break;
      case 1000: 
        i = zza.zzg(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza(37 + "Overread allowed size end=" + k, paramParcel);
    }
    return new Goal.FrequencyObjective(i, j);
  }
  
  public Goal.FrequencyObjective[] zzoa(int paramInt)
  {
    return new Goal.FrequencyObjective[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/data/zzp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */