package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.ArrayList;

public class zzq
  implements Parcelable.Creator<Goal>
{
  static void zza(Goal paramGoal, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zza(paramParcel, 1, paramGoal.zzber());
    zzb.zza(paramParcel, 2, paramGoal.zzbes());
    zzb.zzd(paramParcel, 3, paramGoal.zzbet(), false);
    zzb.zza(paramParcel, 4, paramGoal.getRecurrence(), paramInt, false);
    zzb.zzc(paramParcel, 5, paramGoal.getObjectiveType());
    zzb.zza(paramParcel, 6, paramGoal.zzbeu(), paramInt, false);
    zzb.zza(paramParcel, 7, paramGoal.zzbev(), paramInt, false);
    zzb.zzc(paramParcel, 1000, paramGoal.getVersionCode());
    zzb.zza(paramParcel, 8, paramGoal.zzbew(), paramInt, false);
    zzb.zzaj(paramParcel, i);
  }
  
  public Goal zziw(Parcel paramParcel)
  {
    long l1 = 0L;
    int i = 0;
    Goal.FrequencyObjective localFrequencyObjective = null;
    int k = zza.zzcr(paramParcel);
    ArrayList localArrayList = new ArrayList();
    Goal.DurationObjective localDurationObjective = null;
    Goal.MetricObjective localMetricObjective = null;
    Goal.Recurrence localRecurrence = null;
    long l2 = 0L;
    int j = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.zzcq(paramParcel);
      switch (zza.zzgu(m))
      {
      default: 
        zza.zzb(paramParcel, m);
        break;
      case 1: 
        l2 = zza.zzi(paramParcel, m);
        break;
      case 2: 
        l1 = zza.zzi(paramParcel, m);
        break;
      case 3: 
        zza.zza(paramParcel, m, localArrayList, getClass().getClassLoader());
        break;
      case 4: 
        localRecurrence = (Goal.Recurrence)zza.zza(paramParcel, m, Goal.Recurrence.CREATOR);
        break;
      case 5: 
        i = zza.zzg(paramParcel, m);
        break;
      case 6: 
        localMetricObjective = (Goal.MetricObjective)zza.zza(paramParcel, m, Goal.MetricObjective.CREATOR);
        break;
      case 7: 
        localDurationObjective = (Goal.DurationObjective)zza.zza(paramParcel, m, Goal.DurationObjective.CREATOR);
        break;
      case 1000: 
        j = zza.zzg(paramParcel, m);
        break;
      case 8: 
        localFrequencyObjective = (Goal.FrequencyObjective)zza.zza(paramParcel, m, Goal.FrequencyObjective.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza(37 + "Overread allowed size end=" + k, paramParcel);
    }
    return new Goal(j, l2, l1, localArrayList, localRecurrence, i, localMetricObjective, localDurationObjective, localFrequencyObjective);
  }
  
  public Goal[] zzoe(int paramInt)
  {
    return new Goal[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/data/zzq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */