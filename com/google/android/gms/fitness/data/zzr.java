package com.google.android.gms.fitness.data;

public final class zzr
{
  public static final DataType SA = new DataType("com.google.blood_pressure.summary", new Field[] { zzs.SG, zzs.SI, zzs.SH, zzs.SK, zzs.SM, zzs.SL, zzs.SN, zzs.SO });
  public static final DataType SB = new DataType("com.google.blood_glucose.summary", new Field[] { Field.FIELD_AVERAGE, Field.FIELD_MAX, Field.FIELD_MIN, zzs.SQ, Field.FIELD_MEAL_TYPE, zzs.SR, zzs.SS });
  public static final DataType SC = new DataType("com.google.oxygen_saturation.summary", new Field[] { zzs.SU, zzs.SW, zzs.SV, zzs.SY, zzs.Ta, zzs.SZ, zzs.Tb, zzs.Tc, zzs.Td });
  public static final DataType SD = new DataType("com.google.body.temperature.summary", new Field[] { Field.FIELD_AVERAGE, Field.FIELD_MAX, Field.FIELD_MIN, zzs.Tf });
  public static final DataType SE = new DataType("com.google.body.temperature.basal.summary", new Field[] { Field.FIELD_AVERAGE, Field.FIELD_MAX, Field.FIELD_MIN, zzs.Tf });
  public static final DataType Sq = new DataType("com.google.blood_pressure", new Field[] { zzs.SF, zzs.SJ, zzs.SN, zzs.SO });
  public static final DataType Sr = new DataType("com.google.blood_glucose", new Field[] { zzs.SP, zzs.SQ, Field.FIELD_MEAL_TYPE, zzs.SR, zzs.SS });
  public static final DataType Ss = new DataType("com.google.oxygen_saturation", new Field[] { zzs.ST, zzs.SX, zzs.Tb, zzs.Tc, zzs.Td });
  public static final DataType St = new DataType("com.google.body.temperature", new Field[] { zzs.Te, zzs.Tf });
  public static final DataType Su = new DataType("com.google.body.temperature.basal", new Field[] { zzs.Te, zzs.Tf });
  public static final DataType Sv = new DataType("com.google.cervical_mucus", new Field[] { zzs.Tg, zzs.Th });
  public static final DataType Sw = new DataType("com.google.cervical_position", new Field[] { zzs.Ti, zzs.Tj, zzs.Tk });
  public static final DataType Sx = new DataType("com.google.menstruation", new Field[] { zzs.Tl });
  public static final DataType Sy = new DataType("com.google.ovulation_test", new Field[] { zzs.Tm });
  public static final DataType Sz = new DataType("com.google.vaginal_spotting", new Field[] { Field.RQ });
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/data/zzr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */