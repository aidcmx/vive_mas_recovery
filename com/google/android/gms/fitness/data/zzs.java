package com.google.android.gms.fitness.data;

public final class zzs
{
  public static final Field SF = Field.zzjl("blood_pressure_systolic");
  public static final Field SG = Field.zzjl("blood_pressure_systolic_average");
  public static final Field SH = Field.zzjl("blood_pressure_systolic_min");
  public static final Field SI = Field.zzjl("blood_pressure_systolic_max");
  public static final Field SJ = Field.zzjl("blood_pressure_diastolic");
  public static final Field SK = Field.zzjl("blood_pressure_diastolic_average");
  public static final Field SL = Field.zzjl("blood_pressure_diastolic_min");
  public static final Field SM = Field.zzjl("blood_pressure_diastolic_max");
  public static final Field SN = Field.zzjk("body_position");
  public static final Field SO = Field.zzjk("blood_pressure_measurement_location");
  public static final Field SP = Field.zzjl("blood_glucose_level");
  public static final Field SQ = Field.zzjk("temporal_relation_to_meal");
  public static final Field SR = Field.zzjk("temporal_relation_to_sleep");
  public static final Field SS = Field.zzjk("blood_glucose_specimen_source");
  public static final Field ST = Field.zzjl("oxygen_saturation");
  public static final Field SU = Field.zzjl("oxygen_saturation_average");
  public static final Field SV = Field.zzjl("oxygen_saturation_min");
  public static final Field SW = Field.zzjl("oxygen_saturation_max");
  public static final Field SX = Field.zzjl("supplemental_oxygen_flow_rate");
  public static final Field SY = Field.zzjl("supplemental_oxygen_flow_rate_average");
  public static final Field SZ = Field.zzjl("supplemental_oxygen_flow_rate_min");
  public static final Field Ta = Field.zzjl("supplemental_oxygen_flow_rate_max");
  public static final Field Tb = Field.zzjk("oxygen_therapy_administration_mode");
  public static final Field Tc = Field.zzjk("oxygen_saturation_system");
  public static final Field Td = Field.zzjk("oxygen_saturation_measurement_method");
  public static final Field Te = Field.zzjl("body_temperature");
  public static final Field Tf = Field.zzjk("body_temperature_measurement_location");
  public static final Field Tg = Field.zzjk("cervical_mucus_texture");
  public static final Field Th = Field.zzjk("cervical_mucus_amount");
  public static final Field Ti = Field.zzjk("cervical_position");
  public static final Field Tj = Field.zzjk("cervical_dilation");
  public static final Field Tk = Field.zzjk("cervical_firmness");
  public static final Field Tl = Field.zzjk("menstrual_flow");
  public static final Field Tm = Field.zzjk("ovulation_test_result");
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/data/zzs.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */