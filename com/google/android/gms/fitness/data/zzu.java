package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzu
  implements Parcelable.Creator<MapValue>
{
  static void zza(MapValue paramMapValue, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramMapValue.getFormat());
    zzb.zza(paramParcel, 2, paramMapValue.zzbey());
    zzb.zzc(paramParcel, 1000, paramMapValue.getVersionCode());
    zzb.zzaj(paramParcel, paramInt);
  }
  
  public MapValue zzix(Parcel paramParcel)
  {
    int j = 0;
    int k = zza.zzcr(paramParcel);
    float f = 0.0F;
    int i = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.zzcq(paramParcel);
      switch (zza.zzgu(m))
      {
      default: 
        zza.zzb(paramParcel, m);
        break;
      case 1: 
        j = zza.zzg(paramParcel, m);
        break;
      case 2: 
        f = zza.zzl(paramParcel, m);
        break;
      case 1000: 
        i = zza.zzg(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza(37 + "Overread allowed size end=" + k, paramParcel);
    }
    return new MapValue(i, j, f);
  }
  
  public MapValue[] zzof(int paramInt)
  {
    return new MapValue[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/data/zzu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */