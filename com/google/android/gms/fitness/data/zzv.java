package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzv
  implements Parcelable.Creator<Goal.MetricObjective>
{
  static void zza(Goal.MetricObjective paramMetricObjective, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.zzcs(paramParcel);
    zzb.zza(paramParcel, 1, paramMetricObjective.getDataTypeName(), false);
    zzb.zza(paramParcel, 2, paramMetricObjective.getValue());
    zzb.zza(paramParcel, 3, paramMetricObjective.zzbex());
    zzb.zzc(paramParcel, 1000, paramMetricObjective.getVersionCode());
    zzb.zzaj(paramParcel, paramInt);
  }
  
  public Goal.MetricObjective zziy(Parcel paramParcel)
  {
    double d1 = 0.0D;
    int j = zza.zzcr(paramParcel);
    int i = 0;
    String str = null;
    double d2 = 0.0D;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.zzcq(paramParcel);
      switch (zza.zzgu(k))
      {
      default: 
        zza.zzb(paramParcel, k);
        break;
      case 1: 
        str = zza.zzq(paramParcel, k);
        break;
      case 2: 
        d2 = zza.zzn(paramParcel, k);
        break;
      case 3: 
        d1 = zza.zzn(paramParcel, k);
        break;
      case 1000: 
        i = zza.zzg(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new Goal.MetricObjective(i, str, d2, d1);
  }
  
  public Goal.MetricObjective[] zzog(int paramInt)
  {
    return new Goal.MetricObjective[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/data/zzv.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */