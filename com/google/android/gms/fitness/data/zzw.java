package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.ArrayList;

public class zzw
  implements Parcelable.Creator<RawBucket>
{
  static void zza(RawBucket paramRawBucket, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zza(paramParcel, 1, paramRawBucket.eg);
    zzb.zza(paramParcel, 2, paramRawBucket.QL);
    zzb.zza(paramParcel, 3, paramRawBucket.QB, paramInt, false);
    zzb.zzc(paramParcel, 4, paramRawBucket.Tp);
    zzb.zzc(paramParcel, 5, paramRawBucket.QN, false);
    zzb.zzc(paramParcel, 6, paramRawBucket.QO);
    zzb.zza(paramParcel, 7, paramRawBucket.QP);
    zzb.zzc(paramParcel, 1000, paramRawBucket.mVersionCode);
    zzb.zzaj(paramParcel, i);
  }
  
  public RawBucket zziz(Parcel paramParcel)
  {
    long l1 = 0L;
    ArrayList localArrayList = null;
    boolean bool = false;
    int m = zza.zzcr(paramParcel);
    int i = 0;
    int j = 0;
    Session localSession = null;
    long l2 = 0L;
    int k = 0;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.zzcq(paramParcel);
      switch (zza.zzgu(n))
      {
      default: 
        zza.zzb(paramParcel, n);
        break;
      case 1: 
        l2 = zza.zzi(paramParcel, n);
        break;
      case 2: 
        l1 = zza.zzi(paramParcel, n);
        break;
      case 3: 
        localSession = (Session)zza.zza(paramParcel, n, Session.CREATOR);
        break;
      case 4: 
        j = zza.zzg(paramParcel, n);
        break;
      case 5: 
        localArrayList = zza.zzc(paramParcel, n, RawDataSet.CREATOR);
        break;
      case 6: 
        i = zza.zzg(paramParcel, n);
        break;
      case 7: 
        bool = zza.zzc(paramParcel, n);
        break;
      case 1000: 
        k = zza.zzg(paramParcel, n);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza(37 + "Overread allowed size end=" + m, paramParcel);
    }
    return new RawBucket(k, l2, l1, localSession, j, localArrayList, i, bool);
  }
  
  public RawBucket[] zzoh(int paramInt)
  {
    return new RawBucket[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/data/zzw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */