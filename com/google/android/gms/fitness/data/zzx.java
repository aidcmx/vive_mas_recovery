package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzx
  implements Parcelable.Creator<RawDataPoint>
{
  static void zza(RawDataPoint paramRawDataPoint, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zza(paramParcel, 1, paramRawDataPoint.Tq);
    zzb.zza(paramParcel, 2, paramRawDataPoint.Tr);
    zzb.zza(paramParcel, 3, paramRawDataPoint.Ts, paramInt, false);
    zzb.zzc(paramParcel, 4, paramRawDataPoint.Tt);
    zzb.zzc(paramParcel, 5, paramRawDataPoint.Tu);
    zzb.zza(paramParcel, 6, paramRawDataPoint.Tv);
    zzb.zza(paramParcel, 7, paramRawDataPoint.Tw);
    zzb.zzc(paramParcel, 1000, paramRawDataPoint.versionCode);
    zzb.zzaj(paramParcel, i);
  }
  
  public RawDataPoint zzja(Parcel paramParcel)
  {
    int m = zza.zzcr(paramParcel);
    int k = 0;
    long l4 = 0L;
    long l3 = 0L;
    Value[] arrayOfValue = null;
    int j = 0;
    int i = 0;
    long l2 = 0L;
    long l1 = 0L;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.zzcq(paramParcel);
      switch (zza.zzgu(n))
      {
      default: 
        zza.zzb(paramParcel, n);
        break;
      case 1: 
        l4 = zza.zzi(paramParcel, n);
        break;
      case 2: 
        l3 = zza.zzi(paramParcel, n);
        break;
      case 3: 
        arrayOfValue = (Value[])zza.zzb(paramParcel, n, Value.CREATOR);
        break;
      case 4: 
        j = zza.zzg(paramParcel, n);
        break;
      case 5: 
        i = zza.zzg(paramParcel, n);
        break;
      case 6: 
        l2 = zza.zzi(paramParcel, n);
        break;
      case 7: 
        l1 = zza.zzi(paramParcel, n);
        break;
      case 1000: 
        k = zza.zzg(paramParcel, n);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza(37 + "Overread allowed size end=" + m, paramParcel);
    }
    return new RawDataPoint(k, l4, l3, arrayOfValue, j, i, l2, l1);
  }
  
  public RawDataPoint[] zzoi(int paramInt)
  {
    return new RawDataPoint[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/data/zzx.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */