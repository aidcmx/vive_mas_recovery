package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzz
  implements Parcelable.Creator<Goal.Recurrence>
{
  static void zza(Goal.Recurrence paramRecurrence, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramRecurrence.getCount());
    zzb.zzc(paramParcel, 2, paramRecurrence.getUnit());
    zzb.zzc(paramParcel, 1000, paramRecurrence.getVersionCode());
    zzb.zzaj(paramParcel, paramInt);
  }
  
  public Goal.Recurrence zzjc(Parcel paramParcel)
  {
    int k = 0;
    int m = zza.zzcr(paramParcel);
    int j = 0;
    int i = 0;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.zzcq(paramParcel);
      switch (zza.zzgu(n))
      {
      default: 
        zza.zzb(paramParcel, n);
        break;
      case 1: 
        j = zza.zzg(paramParcel, n);
        break;
      case 2: 
        k = zza.zzg(paramParcel, n);
        break;
      case 1000: 
        i = zza.zzg(paramParcel, n);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza(37 + "Overread allowed size end=" + m, paramParcel);
    }
    return new Goal.Recurrence(i, j, k);
  }
  
  public Goal.Recurrence[] zzok(int paramInt)
  {
    return new Goal.Recurrence[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/data/zzz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */