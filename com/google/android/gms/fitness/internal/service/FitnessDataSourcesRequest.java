package com.google.android.gms.fitness.internal.service;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;
import com.google.android.gms.fitness.data.DataType;
import java.util.Collections;
import java.util.List;

public class FitnessDataSourcesRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<FitnessDataSourcesRequest> CREATOR = new zza();
  private final List<DataType> QK;
  private final int mVersionCode;
  
  FitnessDataSourcesRequest(int paramInt, List<DataType> paramList)
  {
    this.mVersionCode = paramInt;
    this.QK = paramList;
  }
  
  public List<DataType> getDataTypes()
  {
    return Collections.unmodifiableList(this.QK);
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public String toString()
  {
    return zzz.zzx(this).zzg("dataTypes", this.QK).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zza.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/internal/service/FitnessDataSourcesRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */