package com.google.android.gms.fitness.internal.service;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.fitness.data.DataSource;

public class FitnessUnregistrationRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<FitnessUnregistrationRequest> CREATOR = new zzb();
  private final DataSource Rz;
  private final int mVersionCode;
  
  FitnessUnregistrationRequest(int paramInt, DataSource paramDataSource)
  {
    this.mVersionCode = paramInt;
    this.Rz = paramDataSource;
  }
  
  public DataSource getDataSource()
  {
    return this.Rz;
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public String toString()
  {
    return String.format("ApplicationUnregistrationRequest{%s}", new Object[] { this.Rz });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzb.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/internal/service/FitnessUnregistrationRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */