package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.fitness.data.BleDevice;
import com.google.android.gms.internal.zzvb;
import com.google.android.gms.internal.zzvb.zza;

public class ClaimBleDeviceRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<ClaimBleDeviceRequest> CREATOR = new zzb();
  private final String UN;
  private final BleDevice UO;
  private final zzvb UP;
  private final int mVersionCode;
  
  ClaimBleDeviceRequest(int paramInt, String paramString, BleDevice paramBleDevice, IBinder paramIBinder)
  {
    this.mVersionCode = paramInt;
    this.UN = paramString;
    this.UO = paramBleDevice;
    this.UP = zzvb.zza.zzgj(paramIBinder);
  }
  
  public ClaimBleDeviceRequest(String paramString, BleDevice paramBleDevice, zzvb paramzzvb)
  {
    this.mVersionCode = 4;
    this.UN = paramString;
    this.UO = paramBleDevice;
    this.UP = paramzzvb;
  }
  
  public IBinder getCallbackBinder()
  {
    if (this.UP == null) {
      return null;
    }
    return this.UP.asBinder();
  }
  
  public String getDeviceAddress()
  {
    return this.UN;
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public String toString()
  {
    return String.format("ClaimBleDeviceRequest{%s %s}", new Object[] { this.UN, this.UO });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzb.zza(this, paramParcel, paramInt);
  }
  
  public BleDevice zzbfn()
  {
    return this.UO;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/request/ClaimBleDeviceRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */