package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.internal.zzuh;
import com.google.android.gms.internal.zzuh.zza;

public class DailyTotalRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<DailyTotalRequest> CREATOR = new zzc();
  private DataType Qy;
  private final zzuh UQ;
  private final boolean UR;
  private final int versionCode;
  
  DailyTotalRequest(int paramInt, IBinder paramIBinder, DataType paramDataType, boolean paramBoolean)
  {
    this.versionCode = paramInt;
    this.UQ = zzuh.zza.zzfp(paramIBinder);
    this.Qy = paramDataType;
    this.UR = paramBoolean;
  }
  
  public DailyTotalRequest(zzuh paramzzuh, DataType paramDataType, boolean paramBoolean)
  {
    this.versionCode = 3;
    this.UQ = paramzzuh;
    this.Qy = paramDataType;
    this.UR = paramBoolean;
  }
  
  public IBinder getCallbackBinder()
  {
    return this.UQ.asBinder();
  }
  
  public DataType getDataType()
  {
    return this.Qy;
  }
  
  int getVersionCode()
  {
    return this.versionCode;
  }
  
  public String toString()
  {
    if (this.Qy == null) {}
    for (String str = "null";; str = this.Qy.zzbel()) {
      return String.format("DailyTotalRequest{%s}", new Object[] { str });
    }
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzc.zza(this, paramParcel, paramInt);
  }
  
  public boolean zzbfo()
  {
    return this.UR;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/request/DailyTotalRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */