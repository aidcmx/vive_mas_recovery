package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Session;
import com.google.android.gms.internal.zzvb;
import com.google.android.gms.internal.zzvb.zza;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class DataDeleteRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<DataDeleteRequest> CREATOR = new zzd();
  private final List<DataType> QK;
  private final long QL;
  private final zzvb UP;
  private final List<DataSource> US;
  private final List<Session> UT;
  private final boolean UU;
  private final boolean UV;
  private final long eg;
  private final int mVersionCode;
  
  DataDeleteRequest(int paramInt, long paramLong1, long paramLong2, List<DataSource> paramList, List<DataType> paramList1, List<Session> paramList2, boolean paramBoolean1, boolean paramBoolean2, IBinder paramIBinder)
  {
    this.mVersionCode = paramInt;
    this.eg = paramLong1;
    this.QL = paramLong2;
    this.US = Collections.unmodifiableList(paramList);
    this.QK = Collections.unmodifiableList(paramList1);
    this.UT = paramList2;
    this.UU = paramBoolean1;
    this.UV = paramBoolean2;
    this.UP = zzvb.zza.zzgj(paramIBinder);
  }
  
  public DataDeleteRequest(long paramLong1, long paramLong2, List<DataSource> paramList, List<DataType> paramList1, List<Session> paramList2, boolean paramBoolean1, boolean paramBoolean2, zzvb paramzzvb)
  {
    this.mVersionCode = 3;
    this.eg = paramLong1;
    this.QL = paramLong2;
    this.US = Collections.unmodifiableList(paramList);
    this.QK = Collections.unmodifiableList(paramList1);
    this.UT = paramList2;
    this.UU = paramBoolean1;
    this.UV = paramBoolean2;
    this.UP = paramzzvb;
  }
  
  private DataDeleteRequest(Builder paramBuilder)
  {
    this(Builder.zza(paramBuilder), Builder.zzb(paramBuilder), Builder.zzc(paramBuilder), Builder.zzd(paramBuilder), Builder.zze(paramBuilder), Builder.zzf(paramBuilder), Builder.zzg(paramBuilder), null);
  }
  
  public DataDeleteRequest(DataDeleteRequest paramDataDeleteRequest, zzvb paramzzvb)
  {
    this(paramDataDeleteRequest.eg, paramDataDeleteRequest.QL, paramDataDeleteRequest.US, paramDataDeleteRequest.QK, paramDataDeleteRequest.UT, paramDataDeleteRequest.UU, paramDataDeleteRequest.UV, paramzzvb);
  }
  
  private boolean zzb(DataDeleteRequest paramDataDeleteRequest)
  {
    return (this.eg == paramDataDeleteRequest.eg) && (this.QL == paramDataDeleteRequest.QL) && (zzz.equal(this.US, paramDataDeleteRequest.US)) && (zzz.equal(this.QK, paramDataDeleteRequest.QK)) && (zzz.equal(this.UT, paramDataDeleteRequest.UT)) && (this.UU == paramDataDeleteRequest.UU) && (this.UV == paramDataDeleteRequest.UV);
  }
  
  public boolean deleteAllData()
  {
    return this.UU;
  }
  
  public boolean deleteAllSessions()
  {
    return this.UV;
  }
  
  public boolean equals(Object paramObject)
  {
    return (paramObject == this) || (((paramObject instanceof DataDeleteRequest)) && (zzb((DataDeleteRequest)paramObject)));
  }
  
  public IBinder getCallbackBinder()
  {
    if (this.UP == null) {
      return null;
    }
    return this.UP.asBinder();
  }
  
  public List<DataSource> getDataSources()
  {
    return this.US;
  }
  
  public List<DataType> getDataTypes()
  {
    return this.QK;
  }
  
  public long getEndTime(TimeUnit paramTimeUnit)
  {
    return paramTimeUnit.convert(this.QL, TimeUnit.MILLISECONDS);
  }
  
  public List<Session> getSessions()
  {
    return this.UT;
  }
  
  public long getStartTime(TimeUnit paramTimeUnit)
  {
    return paramTimeUnit.convert(this.eg, TimeUnit.MILLISECONDS);
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { Long.valueOf(this.eg), Long.valueOf(this.QL) });
  }
  
  public String toString()
  {
    return zzz.zzx(this).zzg("startTimeMillis", Long.valueOf(this.eg)).zzg("endTimeMillis", Long.valueOf(this.QL)).zzg("dataSources", this.US).zzg("dateTypes", this.QK).zzg("sessions", this.UT).zzg("deleteAllData", Boolean.valueOf(this.UU)).zzg("deleteAllSessions", Boolean.valueOf(this.UV)).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzd.zza(this, paramParcel, paramInt);
  }
  
  public long zzagx()
  {
    return this.eg;
  }
  
  public long zzban()
  {
    return this.QL;
  }
  
  public boolean zzbfp()
  {
    return this.UU;
  }
  
  public boolean zzbfq()
  {
    return this.UV;
  }
  
  public static class Builder
  {
    private List<DataType> QK = new ArrayList();
    private long QL;
    private List<DataSource> US = new ArrayList();
    private List<Session> UT = new ArrayList();
    private boolean UU = false;
    private boolean UV = false;
    private long eg;
    
    private void zzbfr()
    {
      if (this.UT.isEmpty()) {
        return;
      }
      Iterator localIterator = this.UT.iterator();
      label23:
      Session localSession;
      if (localIterator.hasNext())
      {
        localSession = (Session)localIterator.next();
        if ((localSession.getStartTime(TimeUnit.MILLISECONDS) < this.eg) || (localSession.getEndTime(TimeUnit.MILLISECONDS) > this.QL)) {
          break label111;
        }
      }
      label111:
      for (boolean bool = true;; bool = false)
      {
        zzaa.zza(bool, "Session %s is outside the time interval [%d, %d]", new Object[] { localSession, Long.valueOf(this.eg), Long.valueOf(this.QL) });
        break label23;
        break;
      }
    }
    
    public Builder addDataSource(DataSource paramDataSource)
    {
      boolean bool2 = true;
      if (!this.UU)
      {
        bool1 = true;
        zzaa.zzb(bool1, "All data is already marked for deletion.  addDataSource() cannot be combined with deleteAllData()");
        if (paramDataSource == null) {
          break label60;
        }
      }
      label60:
      for (boolean bool1 = bool2;; bool1 = false)
      {
        zzaa.zzb(bool1, "Must specify a valid data source");
        if (!this.US.contains(paramDataSource)) {
          this.US.add(paramDataSource);
        }
        return this;
        bool1 = false;
        break;
      }
    }
    
    public Builder addDataType(DataType paramDataType)
    {
      boolean bool2 = true;
      if (!this.UU)
      {
        bool1 = true;
        zzaa.zzb(bool1, "All data is already marked for deletion.  addDataType() cannot be combined with deleteAllData()");
        if (paramDataType == null) {
          break label60;
        }
      }
      label60:
      for (boolean bool1 = bool2;; bool1 = false)
      {
        zzaa.zzb(bool1, "Must specify a valid data type");
        if (!this.QK.contains(paramDataType)) {
          this.QK.add(paramDataType);
        }
        return this;
        bool1 = false;
        break;
      }
    }
    
    public Builder addSession(Session paramSession)
    {
      boolean bool2 = true;
      if (!this.UV)
      {
        bool1 = true;
        zzaa.zzb(bool1, "All sessions already marked for deletion.  addSession() cannot be combined with deleteAllSessions()");
        if (paramSession == null) {
          break label67;
        }
        bool1 = true;
        label23:
        zzaa.zzb(bool1, "Must specify a valid session");
        if (paramSession.getEndTime(TimeUnit.MILLISECONDS) <= 0L) {
          break label72;
        }
      }
      label67:
      label72:
      for (boolean bool1 = bool2;; bool1 = false)
      {
        zzaa.zzb(bool1, "Cannot delete an ongoing session. Please stop the session prior to deleting it");
        this.UT.add(paramSession);
        return this;
        bool1 = false;
        break;
        bool1 = false;
        break label23;
      }
    }
    
    public DataDeleteRequest build()
    {
      boolean bool2 = false;
      boolean bool1;
      int i;
      if ((this.eg > 0L) && (this.QL > this.eg))
      {
        bool1 = true;
        zzaa.zza(bool1, "Must specify a valid time interval");
        if ((!this.UU) && (this.US.isEmpty()) && (this.QK.isEmpty())) {
          break label124;
        }
        i = 1;
        label65:
        if ((!this.UV) && (this.UT.isEmpty())) {
          break label129;
        }
      }
      label124:
      label129:
      for (int j = 1;; j = 0)
      {
        if (i == 0)
        {
          bool1 = bool2;
          if (j == 0) {}
        }
        else
        {
          bool1 = true;
        }
        zzaa.zza(bool1, "No data or session marked for deletion");
        zzbfr();
        return new DataDeleteRequest(this, null);
        bool1 = false;
        break;
        i = 0;
        break label65;
      }
    }
    
    public Builder deleteAllData()
    {
      zzaa.zzb(this.QK.isEmpty(), "Specific data type already added for deletion. deleteAllData() will delete all data types and cannot be combined with addDataType()");
      zzaa.zzb(this.US.isEmpty(), "Specific data source already added for deletion. deleteAllData() will delete all data sources and cannot be combined with addDataSource()");
      this.UU = true;
      return this;
    }
    
    public Builder deleteAllSessions()
    {
      zzaa.zzb(this.UT.isEmpty(), "Specific session already added for deletion. deleteAllData() will delete all sessions and cannot be combined with addSession()");
      this.UV = true;
      return this;
    }
    
    public Builder setTimeInterval(long paramLong1, long paramLong2, TimeUnit paramTimeUnit)
    {
      if (paramLong1 > 0L)
      {
        bool = true;
        zzaa.zzb(bool, "Invalid start time :%d", new Object[] { Long.valueOf(paramLong1) });
        if (paramLong2 <= paramLong1) {
          break label82;
        }
      }
      label82:
      for (boolean bool = true;; bool = false)
      {
        zzaa.zzb(bool, "Invalid end time :%d", new Object[] { Long.valueOf(paramLong2) });
        this.eg = paramTimeUnit.toMillis(paramLong1);
        this.QL = paramTimeUnit.toMillis(paramLong2);
        return this;
        bool = false;
        break;
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/request/DataDeleteRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */