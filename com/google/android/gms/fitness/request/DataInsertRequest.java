package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.internal.zzvb;
import com.google.android.gms.internal.zzvb.zza;

public class DataInsertRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<DataInsertRequest> CREATOR = new zze();
  private final DataSet TC;
  private final zzvb UP;
  private final boolean UW;
  private final int mVersionCode;
  
  DataInsertRequest(int paramInt, DataSet paramDataSet, IBinder paramIBinder, boolean paramBoolean)
  {
    this.mVersionCode = paramInt;
    this.TC = paramDataSet;
    this.UP = zzvb.zza.zzgj(paramIBinder);
    this.UW = paramBoolean;
  }
  
  public DataInsertRequest(DataSet paramDataSet, zzvb paramzzvb, boolean paramBoolean)
  {
    this.mVersionCode = 4;
    this.TC = paramDataSet;
    this.UP = paramzzvb;
    this.UW = paramBoolean;
  }
  
  private boolean zzc(DataInsertRequest paramDataInsertRequest)
  {
    return zzz.equal(this.TC, paramDataInsertRequest.TC);
  }
  
  public boolean equals(Object paramObject)
  {
    return (paramObject == this) || (((paramObject instanceof DataInsertRequest)) && (zzc((DataInsertRequest)paramObject)));
  }
  
  public IBinder getCallbackBinder()
  {
    if (this.UP == null) {
      return null;
    }
    return this.UP.asBinder();
  }
  
  public DataSet getDataSet()
  {
    return this.TC;
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.TC });
  }
  
  public String toString()
  {
    return zzz.zzx(this).zzg("dataSet", this.TC).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zze.zza(this, paramParcel, paramInt);
  }
  
  public boolean zzbfs()
  {
    return this.UW;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/request/DataInsertRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */