package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.fitness.data.Bucket;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Device;
import com.google.android.gms.internal.zzui;
import com.google.android.gms.internal.zzui.zza;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class DataReadRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<DataReadRequest> CREATOR = new zzf();
  public static final int NO_LIMIT = 0;
  private final List<DataType> QK;
  private final long QL;
  private final int QO;
  private final List<DataSource> US;
  private final List<DataType> UX;
  private final List<DataSource> UY;
  private final long UZ;
  private final DataSource Va;
  private final int Vb;
  private final boolean Vc;
  private final boolean Vd;
  private final zzui Ve;
  private final List<Device> Vf;
  private final List<Integer> Vg;
  private final long eg;
  private final int mVersionCode;
  
  DataReadRequest(int paramInt1, List<DataType> paramList1, List<DataSource> paramList2, long paramLong1, long paramLong2, List<DataType> paramList3, List<DataSource> paramList4, int paramInt2, long paramLong3, DataSource paramDataSource, int paramInt3, boolean paramBoolean1, boolean paramBoolean2, IBinder paramIBinder, List<Device> paramList, List<Integer> paramList5)
  {
    this.mVersionCode = paramInt1;
    this.QK = paramList1;
    this.US = paramList2;
    this.eg = paramLong1;
    this.QL = paramLong2;
    this.UX = paramList3;
    this.UY = paramList4;
    this.QO = paramInt2;
    this.UZ = paramLong3;
    this.Va = paramDataSource;
    this.Vb = paramInt3;
    this.Vc = paramBoolean1;
    this.Vd = paramBoolean2;
    if (paramIBinder == null) {}
    for (paramList1 = null;; paramList1 = zzui.zza.zzfq(paramIBinder))
    {
      this.Ve = paramList1;
      paramList1 = paramList;
      if (paramList == null) {
        paramList1 = Collections.emptyList();
      }
      this.Vf = paramList1;
      paramList1 = paramList5;
      if (paramList5 == null) {
        paramList1 = Collections.emptyList();
      }
      this.Vg = paramList1;
      return;
    }
  }
  
  private DataReadRequest(Builder paramBuilder)
  {
    this(Builder.zza(paramBuilder), Builder.zzb(paramBuilder), Builder.zzc(paramBuilder), Builder.zzd(paramBuilder), Builder.zze(paramBuilder), Builder.zzf(paramBuilder), Builder.zzg(paramBuilder), Builder.zzh(paramBuilder), Builder.zzi(paramBuilder), Builder.zzj(paramBuilder), Builder.zzk(paramBuilder), Builder.zzl(paramBuilder), null, Builder.zzm(paramBuilder), Builder.zzn(paramBuilder));
  }
  
  public DataReadRequest(DataReadRequest paramDataReadRequest, zzui paramzzui)
  {
    this(paramDataReadRequest.QK, paramDataReadRequest.US, paramDataReadRequest.eg, paramDataReadRequest.QL, paramDataReadRequest.UX, paramDataReadRequest.UY, paramDataReadRequest.QO, paramDataReadRequest.UZ, paramDataReadRequest.Va, paramDataReadRequest.Vb, paramDataReadRequest.Vc, paramDataReadRequest.Vd, paramzzui, paramDataReadRequest.Vf, paramDataReadRequest.Vg);
  }
  
  public DataReadRequest(List<DataType> paramList1, List<DataSource> paramList2, long paramLong1, long paramLong2, List<DataType> paramList3, List<DataSource> paramList4, int paramInt1, long paramLong3, DataSource paramDataSource, int paramInt2, boolean paramBoolean1, boolean paramBoolean2, zzui paramzzui, List<Device> paramList, List<Integer> paramList5) {}
  
  private boolean zzb(DataReadRequest paramDataReadRequest)
  {
    return (this.QK.equals(paramDataReadRequest.QK)) && (this.US.equals(paramDataReadRequest.US)) && (this.eg == paramDataReadRequest.eg) && (this.QL == paramDataReadRequest.QL) && (this.QO == paramDataReadRequest.QO) && (this.UY.equals(paramDataReadRequest.UY)) && (this.UX.equals(paramDataReadRequest.UX)) && (zzz.equal(this.Va, paramDataReadRequest.Va)) && (this.UZ == paramDataReadRequest.UZ) && (this.Vd == paramDataReadRequest.Vd) && (this.Vb == paramDataReadRequest.Vb) && (this.Vc == paramDataReadRequest.Vc) && (zzz.equal(this.Ve, paramDataReadRequest.Ve)) && (zzz.equal(this.Vf, paramDataReadRequest.Vf)) && (zzz.equal(this.Vg, paramDataReadRequest.Vg));
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof DataReadRequest)) && (zzb((DataReadRequest)paramObject)));
  }
  
  public DataSource getActivityDataSource()
  {
    return this.Va;
  }
  
  public List<DataSource> getAggregatedDataSources()
  {
    return this.UY;
  }
  
  public List<DataType> getAggregatedDataTypes()
  {
    return this.UX;
  }
  
  public long getBucketDuration(TimeUnit paramTimeUnit)
  {
    return paramTimeUnit.convert(this.UZ, TimeUnit.MILLISECONDS);
  }
  
  public int getBucketType()
  {
    return this.QO;
  }
  
  public IBinder getCallbackBinder()
  {
    if (this.Ve == null) {
      return null;
    }
    return this.Ve.asBinder();
  }
  
  public List<DataSource> getDataSources()
  {
    return this.US;
  }
  
  public List<DataType> getDataTypes()
  {
    return this.QK;
  }
  
  public long getEndTime(TimeUnit paramTimeUnit)
  {
    return paramTimeUnit.convert(this.QL, TimeUnit.MILLISECONDS);
  }
  
  public int getLimit()
  {
    return this.Vb;
  }
  
  public long getStartTime(TimeUnit paramTimeUnit)
  {
    return paramTimeUnit.convert(this.eg, TimeUnit.MILLISECONDS);
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { Integer.valueOf(this.QO), Long.valueOf(this.eg), Long.valueOf(this.QL) });
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("DataReadRequest{");
    Iterator localIterator;
    if (!this.QK.isEmpty())
    {
      localIterator = this.QK.iterator();
      while (localIterator.hasNext()) {
        localStringBuilder.append(((DataType)localIterator.next()).zzbel()).append(" ");
      }
    }
    if (!this.US.isEmpty())
    {
      localIterator = this.US.iterator();
      while (localIterator.hasNext()) {
        localStringBuilder.append(((DataSource)localIterator.next()).toDebugString()).append(" ");
      }
    }
    if (this.QO != 0)
    {
      localStringBuilder.append("bucket by ").append(Bucket.zznm(this.QO));
      if (this.UZ > 0L) {
        localStringBuilder.append(" >").append(this.UZ).append("ms");
      }
      localStringBuilder.append(": ");
    }
    if (!this.UX.isEmpty())
    {
      localIterator = this.UX.iterator();
      while (localIterator.hasNext()) {
        localStringBuilder.append(((DataType)localIterator.next()).zzbel()).append(" ");
      }
    }
    if (!this.UY.isEmpty())
    {
      localIterator = this.UY.iterator();
      while (localIterator.hasNext()) {
        localStringBuilder.append(((DataSource)localIterator.next()).toDebugString()).append(" ");
      }
    }
    localStringBuilder.append(String.format("(%tF %tT - %tF %tT)", new Object[] { Long.valueOf(this.eg), Long.valueOf(this.eg), Long.valueOf(this.QL), Long.valueOf(this.QL) }));
    if (this.Va != null) {
      localStringBuilder.append("activities: ").append(this.Va.toDebugString());
    }
    if (!this.Vg.isEmpty())
    {
      localStringBuilder.append("quality: ");
      localIterator = this.Vg.iterator();
      while (localIterator.hasNext()) {
        localStringBuilder.append(DataSource.zznt(((Integer)localIterator.next()).intValue())).append(" ");
      }
    }
    if (this.Vd) {
      localStringBuilder.append(" +server");
    }
    localStringBuilder.append("}");
    return localStringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzf.zza(this, paramParcel, paramInt);
  }
  
  public long zzagx()
  {
    return this.eg;
  }
  
  public long zzban()
  {
    return this.QL;
  }
  
  public boolean zzbft()
  {
    return this.Vd;
  }
  
  public boolean zzbfu()
  {
    return this.Vc;
  }
  
  public long zzbfv()
  {
    return this.UZ;
  }
  
  public List<Device> zzbfw()
  {
    return this.Vf;
  }
  
  public List<Integer> zzbfx()
  {
    return this.Vg;
  }
  
  public static class Builder
  {
    private List<DataType> QK = new ArrayList();
    private long QL;
    private int QO = 0;
    private List<DataSource> US = new ArrayList();
    private List<DataType> UX = new ArrayList();
    private List<DataSource> UY = new ArrayList();
    private long UZ = 0L;
    private DataSource Va;
    private int Vb = 0;
    private boolean Vc = false;
    private boolean Vd = false;
    private final List<Device> Vf = new ArrayList();
    private final List<Integer> Vg = new ArrayList();
    private long eg;
    
    public Builder aggregate(DataSource paramDataSource, DataType paramDataType)
    {
      zzaa.zzb(paramDataSource, "Attempting to add a null data source");
      if (!this.US.contains(paramDataSource)) {}
      for (boolean bool = true;; bool = false)
      {
        zzaa.zza(bool, "Cannot add the same data source for aggregated and detailed");
        DataType localDataType = paramDataSource.getDataType();
        zzaa.zzb(DataType.AGGREGATE_INPUT_TYPES.contains(localDataType), "Unsupported input data type specified for aggregation: %s", new Object[] { localDataType });
        zzaa.zzb(DataType.getAggregatesForInput(localDataType).contains(paramDataType), "Invalid output aggregate data type specified: %s -> %s", new Object[] { localDataType, paramDataType });
        if (!this.UY.contains(paramDataSource)) {
          this.UY.add(paramDataSource);
        }
        return this;
      }
    }
    
    public Builder aggregate(DataType paramDataType1, DataType paramDataType2)
    {
      zzaa.zzb(paramDataType1, "Attempting to use a null data type");
      if (!this.QK.contains(paramDataType1)) {}
      for (boolean bool = true;; bool = false)
      {
        zzaa.zza(bool, "Cannot add the same data type as aggregated and detailed");
        zzaa.zzb(DataType.AGGREGATE_INPUT_TYPES.contains(paramDataType1), "Unsupported input data type specified for aggregation: %s", new Object[] { paramDataType1 });
        zzaa.zzb(DataType.getAggregatesForInput(paramDataType1).contains(paramDataType2), "Invalid output aggregate data type specified: %s -> %s", new Object[] { paramDataType1, paramDataType2 });
        if (!this.UX.contains(paramDataType1)) {
          this.UX.add(paramDataType1);
        }
        return this;
      }
    }
    
    public Builder bucketByActivitySegment(int paramInt, TimeUnit paramTimeUnit)
    {
      if (this.QO == 0)
      {
        bool = true;
        zzaa.zzb(bool, "Bucketing strategy already set to %s", new Object[] { Integer.valueOf(this.QO) });
        if (paramInt <= 0) {
          break label74;
        }
      }
      label74:
      for (boolean bool = true;; bool = false)
      {
        zzaa.zzb(bool, "Must specify a valid minimum duration for an activity segment: %d", new Object[] { Integer.valueOf(paramInt) });
        this.QO = 4;
        this.UZ = paramTimeUnit.toMillis(paramInt);
        return this;
        bool = false;
        break;
      }
    }
    
    public Builder bucketByActivitySegment(int paramInt, TimeUnit paramTimeUnit, DataSource paramDataSource)
    {
      if (this.QO == 0)
      {
        bool = true;
        zzaa.zzb(bool, "Bucketing strategy already set to %s", new Object[] { Integer.valueOf(this.QO) });
        if (paramInt <= 0) {
          break label121;
        }
        bool = true;
        label38:
        zzaa.zzb(bool, "Must specify a valid minimum duration for an activity segment: %d", new Object[] { Integer.valueOf(paramInt) });
        if (paramDataSource == null) {
          break label127;
        }
      }
      label121:
      label127:
      for (boolean bool = true;; bool = false)
      {
        zzaa.zzb(bool, "Invalid activity data source specified");
        zzaa.zzb(paramDataSource.getDataType().equals(DataType.TYPE_ACTIVITY_SEGMENT), "Invalid activity data source specified: %s", new Object[] { paramDataSource });
        this.Va = paramDataSource;
        this.QO = 4;
        this.UZ = paramTimeUnit.toMillis(paramInt);
        return this;
        bool = false;
        break;
        bool = false;
        break label38;
      }
    }
    
    public Builder bucketByActivityType(int paramInt, TimeUnit paramTimeUnit)
    {
      if (this.QO == 0)
      {
        bool = true;
        zzaa.zzb(bool, "Bucketing strategy already set to %s", new Object[] { Integer.valueOf(this.QO) });
        if (paramInt <= 0) {
          break label74;
        }
      }
      label74:
      for (boolean bool = true;; bool = false)
      {
        zzaa.zzb(bool, "Must specify a valid minimum duration for an activity segment: %d", new Object[] { Integer.valueOf(paramInt) });
        this.QO = 3;
        this.UZ = paramTimeUnit.toMillis(paramInt);
        return this;
        bool = false;
        break;
      }
    }
    
    public Builder bucketByActivityType(int paramInt, TimeUnit paramTimeUnit, DataSource paramDataSource)
    {
      if (this.QO == 0)
      {
        bool = true;
        zzaa.zzb(bool, "Bucketing strategy already set to %s", new Object[] { Integer.valueOf(this.QO) });
        if (paramInt <= 0) {
          break label121;
        }
        bool = true;
        label38:
        zzaa.zzb(bool, "Must specify a valid minimum duration for an activity segment: %d", new Object[] { Integer.valueOf(paramInt) });
        if (paramDataSource == null) {
          break label127;
        }
      }
      label121:
      label127:
      for (boolean bool = true;; bool = false)
      {
        zzaa.zzb(bool, "Invalid activity data source specified");
        zzaa.zzb(paramDataSource.getDataType().equals(DataType.TYPE_ACTIVITY_SEGMENT), "Invalid activity data source specified: %s", new Object[] { paramDataSource });
        this.Va = paramDataSource;
        this.QO = 3;
        this.UZ = paramTimeUnit.toMillis(paramInt);
        return this;
        bool = false;
        break;
        bool = false;
        break label38;
      }
    }
    
    public Builder bucketBySession(int paramInt, TimeUnit paramTimeUnit)
    {
      if (this.QO == 0)
      {
        bool = true;
        zzaa.zzb(bool, "Bucketing strategy already set to %s", new Object[] { Integer.valueOf(this.QO) });
        if (paramInt <= 0) {
          break label74;
        }
      }
      label74:
      for (boolean bool = true;; bool = false)
      {
        zzaa.zzb(bool, "Must specify a valid minimum duration for an activity segment: %d", new Object[] { Integer.valueOf(paramInt) });
        this.QO = 2;
        this.UZ = paramTimeUnit.toMillis(paramInt);
        return this;
        bool = false;
        break;
      }
    }
    
    public Builder bucketByTime(int paramInt, TimeUnit paramTimeUnit)
    {
      if (this.QO == 0)
      {
        bool = true;
        zzaa.zzb(bool, "Bucketing strategy already set to %s", new Object[] { Integer.valueOf(this.QO) });
        if (paramInt <= 0) {
          break label74;
        }
      }
      label74:
      for (boolean bool = true;; bool = false)
      {
        zzaa.zzb(bool, "Must specify a valid minimum duration for an activity segment: %d", new Object[] { Integer.valueOf(paramInt) });
        this.QO = 1;
        this.UZ = paramTimeUnit.toMillis(paramInt);
        return this;
        bool = false;
        break;
      }
    }
    
    public DataReadRequest build()
    {
      boolean bool2 = true;
      label69:
      label112:
      int i;
      if ((!this.US.isEmpty()) || (!this.QK.isEmpty()) || (!this.UY.isEmpty()) || (!this.UX.isEmpty()))
      {
        bool1 = true;
        zzaa.zza(bool1, "Must add at least one data source (aggregated or detailed)");
        if (this.eg <= 0L) {
          break label205;
        }
        bool1 = true;
        zzaa.zza(bool1, "Invalid start time: %s", new Object[] { Long.valueOf(this.eg) });
        if ((this.QL <= 0L) || (this.QL <= this.eg)) {
          break label210;
        }
        bool1 = true;
        zzaa.zza(bool1, "Invalid end time: %s", new Object[] { Long.valueOf(this.QL) });
        if ((!this.UY.isEmpty()) || (!this.UX.isEmpty())) {
          break label215;
        }
        i = 1;
        label158:
        if (i != 0)
        {
          bool1 = bool2;
          if (this.QO == 0) {}
        }
        else
        {
          if ((i != 0) || (this.QO == 0)) {
            break label220;
          }
        }
      }
      label205:
      label210:
      label215:
      label220:
      for (boolean bool1 = bool2;; bool1 = false)
      {
        zzaa.zza(bool1, "Must specify a valid bucketing strategy while requesting aggregation");
        return new DataReadRequest(this, null);
        bool1 = false;
        break;
        bool1 = false;
        break label69;
        bool1 = false;
        break label112;
        i = 0;
        break label158;
      }
    }
    
    public Builder enableServerQueries()
    {
      this.Vd = true;
      return this;
    }
    
    public Builder read(DataSource paramDataSource)
    {
      zzaa.zzb(paramDataSource, "Attempting to add a null data source");
      if (!this.UY.contains(paramDataSource)) {}
      for (boolean bool = true;; bool = false)
      {
        zzaa.zzb(bool, "Cannot add the same data source as aggregated and detailed");
        if (!this.US.contains(paramDataSource)) {
          this.US.add(paramDataSource);
        }
        return this;
      }
    }
    
    public Builder read(DataType paramDataType)
    {
      zzaa.zzb(paramDataType, "Attempting to use a null data type");
      if (!this.UX.contains(paramDataType)) {}
      for (boolean bool = true;; bool = false)
      {
        zzaa.zza(bool, "Cannot add the same data type as aggregated and detailed");
        if (!this.QK.contains(paramDataType)) {
          this.QK.add(paramDataType);
        }
        return this;
      }
    }
    
    public Builder setLimit(int paramInt)
    {
      if (paramInt > 0) {}
      for (boolean bool = true;; bool = false)
      {
        zzaa.zzb(bool, "Invalid limit %d is specified", new Object[] { Integer.valueOf(paramInt) });
        this.Vb = paramInt;
        return this;
      }
    }
    
    public Builder setTimeRange(long paramLong1, long paramLong2, TimeUnit paramTimeUnit)
    {
      this.eg = paramTimeUnit.toMillis(paramLong1);
      this.QL = paramTimeUnit.toMillis(paramLong2);
      return this;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/request/DataReadRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */