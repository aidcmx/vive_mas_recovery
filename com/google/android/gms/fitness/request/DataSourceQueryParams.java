package com.google.android.gms.fitness.request;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.fitness.data.DataSource;

public class DataSourceQueryParams
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<DataSourceQueryParams> CREATOR = new zzg();
  public final DataSource Rz;
  public final long Tr;
  public final int Vb;
  public final long Vh;
  public final int Vi;
  public final long hx;
  final int versionCode;
  
  DataSourceQueryParams(int paramInt1, DataSource paramDataSource, long paramLong1, long paramLong2, long paramLong3, int paramInt2, int paramInt3)
  {
    this.versionCode = paramInt1;
    this.Rz = paramDataSource;
    this.hx = paramLong1;
    this.Tr = paramLong2;
    this.Vh = paramLong3;
    this.Vb = paramInt2;
    this.Vi = paramInt3;
  }
  
  public String toString()
  {
    return String.format("{%1$s, %2$tF %2$tT - %3$tF %3$tT (%4$s %5$s)}", new Object[] { this.Rz.toDebugString(), Long.valueOf(this.Tr / 1000000L), Long.valueOf(this.Vh / 1000000L), Integer.valueOf(this.Vb), Integer.valueOf(this.Vi) });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzg.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/request/DataSourceQueryParams.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */