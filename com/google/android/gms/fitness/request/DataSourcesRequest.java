package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;
import com.google.android.gms.common.util.zzb;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.internal.zzuj;
import com.google.android.gms.internal.zzuj.zza;
import java.util.Arrays;
import java.util.List;

public class DataSourcesRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<DataSourcesRequest> CREATOR = new zzh();
  private final List<DataType> QK;
  private final List<Integer> Vj;
  private final boolean Vk;
  private final zzuj Vl;
  private final int mVersionCode;
  
  DataSourcesRequest(int paramInt, List<DataType> paramList, List<Integer> paramList1, boolean paramBoolean, IBinder paramIBinder)
  {
    this.mVersionCode = paramInt;
    this.QK = paramList;
    this.Vj = paramList1;
    this.Vk = paramBoolean;
    this.Vl = zzuj.zza.zzfr(paramIBinder);
  }
  
  private DataSourcesRequest(Builder paramBuilder)
  {
    this(zzb.zzb(Builder.zza(paramBuilder)), Arrays.asList(zzb.zza(Builder.zzb(paramBuilder))), Builder.zzc(paramBuilder), null);
  }
  
  public DataSourcesRequest(DataSourcesRequest paramDataSourcesRequest, zzuj paramzzuj)
  {
    this(paramDataSourcesRequest.QK, paramDataSourcesRequest.Vj, paramDataSourcesRequest.Vk, paramzzuj);
  }
  
  public DataSourcesRequest(List<DataType> paramList, List<Integer> paramList1, boolean paramBoolean, zzuj paramzzuj)
  {
    this.mVersionCode = 4;
    this.QK = paramList;
    this.Vj = paramList1;
    this.Vk = paramBoolean;
    this.Vl = paramzzuj;
  }
  
  public IBinder getCallbackBinder()
  {
    if (this.Vl == null) {
      return null;
    }
    return this.Vl.asBinder();
  }
  
  public List<DataType> getDataTypes()
  {
    return this.QK;
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public String toString()
  {
    zzz.zza localzza = zzz.zzx(this).zzg("dataTypes", this.QK).zzg("sourceTypes", this.Vj);
    if (this.Vk) {
      localzza.zzg("includeDbOnlySources", "true");
    }
    return localzza.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzh.zza(this, paramParcel, paramInt);
  }
  
  public List<Integer> zzbfy()
  {
    return this.Vj;
  }
  
  public boolean zzbfz()
  {
    return this.Vk;
  }
  
  public static class Builder
  {
    private boolean Vk = false;
    private DataType[] Vm = new DataType[0];
    private int[] Vn = { 0, 1 };
    
    public DataSourcesRequest build()
    {
      boolean bool2 = true;
      if (this.Vm.length > 0)
      {
        bool1 = true;
        zzaa.zza(bool1, "Must add at least one data type");
        if (this.Vn.length <= 0) {
          break label49;
        }
      }
      label49:
      for (boolean bool1 = bool2;; bool1 = false)
      {
        zzaa.zza(bool1, "Must add at least one data source type");
        return new DataSourcesRequest(this, null);
        bool1 = false;
        break;
      }
    }
    
    public Builder setDataSourceTypes(int... paramVarArgs)
    {
      this.Vn = paramVarArgs;
      return this;
    }
    
    public Builder setDataTypes(DataType... paramVarArgs)
    {
      this.Vm = paramVarArgs;
      return this;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/request/DataSourcesRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */