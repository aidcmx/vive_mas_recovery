package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.internal.zzuk;
import com.google.android.gms.internal.zzuk.zza;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DataTypeCreateRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<DataTypeCreateRequest> CREATOR = new zzi();
  private final List<Field> Vo;
  private final zzuk Vp;
  private final String mName;
  private final int mVersionCode;
  
  DataTypeCreateRequest(int paramInt, String paramString, List<Field> paramList, IBinder paramIBinder)
  {
    this.mVersionCode = paramInt;
    this.mName = paramString;
    this.Vo = Collections.unmodifiableList(paramList);
    this.Vp = zzuk.zza.zzfs(paramIBinder);
  }
  
  private DataTypeCreateRequest(Builder paramBuilder)
  {
    this(Builder.zza(paramBuilder), Builder.zzb(paramBuilder), null);
  }
  
  public DataTypeCreateRequest(DataTypeCreateRequest paramDataTypeCreateRequest, zzuk paramzzuk)
  {
    this(paramDataTypeCreateRequest.mName, paramDataTypeCreateRequest.Vo, paramzzuk);
  }
  
  public DataTypeCreateRequest(String paramString, List<Field> paramList, zzuk paramzzuk)
  {
    this.mVersionCode = 3;
    this.mName = paramString;
    this.Vo = Collections.unmodifiableList(paramList);
    this.Vp = paramzzuk;
  }
  
  private boolean zzb(DataTypeCreateRequest paramDataTypeCreateRequest)
  {
    return (zzz.equal(this.mName, paramDataTypeCreateRequest.mName)) && (zzz.equal(this.Vo, paramDataTypeCreateRequest.Vo));
  }
  
  public boolean equals(Object paramObject)
  {
    return (paramObject == this) || (((paramObject instanceof DataTypeCreateRequest)) && (zzb((DataTypeCreateRequest)paramObject)));
  }
  
  public IBinder getCallbackBinder()
  {
    if (this.Vp == null) {
      return null;
    }
    return this.Vp.asBinder();
  }
  
  public List<Field> getFields()
  {
    return this.Vo;
  }
  
  public String getName()
  {
    return this.mName;
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.mName, this.Vo });
  }
  
  public String toString()
  {
    return zzz.zzx(this).zzg("name", this.mName).zzg("fields", this.Vo).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzi.zza(this, paramParcel, paramInt);
  }
  
  public static class Builder
  {
    private List<Field> Vo = new ArrayList();
    private String mName;
    
    public Builder addField(Field paramField)
    {
      if (!this.Vo.contains(paramField)) {
        this.Vo.add(paramField);
      }
      return this;
    }
    
    public Builder addField(String paramString, int paramInt)
    {
      if ((paramString != null) && (!paramString.isEmpty())) {}
      for (boolean bool = true;; bool = false)
      {
        zzaa.zzb(bool, "Invalid name specified");
        return addField(Field.zzn(paramString, paramInt));
      }
    }
    
    public DataTypeCreateRequest build()
    {
      boolean bool2 = true;
      if (this.mName != null)
      {
        bool1 = true;
        zzaa.zza(bool1, "Must set the name");
        if (this.Vo.isEmpty()) {
          break label52;
        }
      }
      label52:
      for (boolean bool1 = bool2;; bool1 = false)
      {
        zzaa.zza(bool1, "Must specify the data fields");
        return new DataTypeCreateRequest(this, null);
        bool1 = false;
        break;
      }
    }
    
    public Builder setName(String paramString)
    {
      this.mName = paramString;
      return this;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/request/DataTypeCreateRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */