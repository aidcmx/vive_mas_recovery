package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;
import com.google.android.gms.internal.zzuk;
import com.google.android.gms.internal.zzuk.zza;

public class DataTypeReadRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<DataTypeReadRequest> CREATOR = new zzj();
  private final zzuk Vp;
  private final String mName;
  private final int mVersionCode;
  
  DataTypeReadRequest(int paramInt, String paramString, IBinder paramIBinder)
  {
    this.mVersionCode = paramInt;
    this.mName = paramString;
    this.Vp = zzuk.zza.zzfs(paramIBinder);
  }
  
  public DataTypeReadRequest(String paramString, zzuk paramzzuk)
  {
    this.mVersionCode = 3;
    this.mName = paramString;
    this.Vp = paramzzuk;
  }
  
  private boolean zzb(DataTypeReadRequest paramDataTypeReadRequest)
  {
    return zzz.equal(this.mName, paramDataTypeReadRequest.mName);
  }
  
  public boolean equals(Object paramObject)
  {
    return (paramObject == this) || (((paramObject instanceof DataTypeReadRequest)) && (zzb((DataTypeReadRequest)paramObject)));
  }
  
  public IBinder getCallbackBinder()
  {
    return this.Vp.asBinder();
  }
  
  public String getName()
  {
    return this.mName;
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.mName });
  }
  
  public String toString()
  {
    return zzz.zzx(this).zzg("name", this.mName).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzj.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/request/DataTypeReadRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */