package com.google.android.gms.fitness.request;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.internal.zzvb;
import com.google.android.gms.internal.zzvb.zza;

public class DataUpdateListenerRegistrationRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<DataUpdateListenerRegistrationRequest> CREATOR = new zzk();
  private DataType RA;
  private DataSource Rz;
  private final zzvb UP;
  private final PendingIntent mPendingIntent;
  private final int mVersionCode;
  
  DataUpdateListenerRegistrationRequest(int paramInt, DataSource paramDataSource, DataType paramDataType, PendingIntent paramPendingIntent, IBinder paramIBinder)
  {
    this.mVersionCode = paramInt;
    this.Rz = paramDataSource;
    this.RA = paramDataType;
    this.mPendingIntent = paramPendingIntent;
    this.UP = zzvb.zza.zzgj(paramIBinder);
  }
  
  public DataUpdateListenerRegistrationRequest(DataSource paramDataSource, DataType paramDataType, PendingIntent paramPendingIntent, IBinder paramIBinder)
  {
    this.mVersionCode = 1;
    this.Rz = paramDataSource;
    this.RA = paramDataType;
    this.mPendingIntent = paramPendingIntent;
    this.UP = zzvb.zza.zzgj(paramIBinder);
  }
  
  private DataUpdateListenerRegistrationRequest(Builder paramBuilder)
  {
    this(Builder.zza(paramBuilder), Builder.zzb(paramBuilder), Builder.zzc(paramBuilder), null);
  }
  
  public DataUpdateListenerRegistrationRequest(DataUpdateListenerRegistrationRequest paramDataUpdateListenerRegistrationRequest, IBinder paramIBinder)
  {
    this(paramDataUpdateListenerRegistrationRequest.Rz, paramDataUpdateListenerRegistrationRequest.RA, paramDataUpdateListenerRegistrationRequest.mPendingIntent, paramIBinder);
  }
  
  private boolean zzb(DataUpdateListenerRegistrationRequest paramDataUpdateListenerRegistrationRequest)
  {
    return (zzz.equal(this.Rz, paramDataUpdateListenerRegistrationRequest.Rz)) && (zzz.equal(this.RA, paramDataUpdateListenerRegistrationRequest.RA)) && (zzz.equal(this.mPendingIntent, paramDataUpdateListenerRegistrationRequest.mPendingIntent));
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof DataUpdateListenerRegistrationRequest)) && (zzb((DataUpdateListenerRegistrationRequest)paramObject)));
  }
  
  public IBinder getCallbackBinder()
  {
    if (this.UP == null) {
      return null;
    }
    return this.UP.asBinder();
  }
  
  public DataSource getDataSource()
  {
    return this.Rz;
  }
  
  public DataType getDataType()
  {
    return this.RA;
  }
  
  public PendingIntent getIntent()
  {
    return this.mPendingIntent;
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.Rz, this.RA, this.mPendingIntent });
  }
  
  public String toString()
  {
    return zzz.zzx(this).zzg("dataSource", this.Rz).zzg("dataType", this.RA).zzg("pendingIntent", this.mPendingIntent).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzk.zza(this, paramParcel, paramInt);
  }
  
  public static class Builder
  {
    private DataType RA;
    private DataSource Rz;
    private PendingIntent mPendingIntent;
    
    public DataUpdateListenerRegistrationRequest build()
    {
      if ((this.Rz != null) || (this.RA != null)) {}
      for (boolean bool = true;; bool = false)
      {
        zzaa.zza(bool, "Set either dataSource or dataTYpe");
        zzaa.zzb(this.mPendingIntent, "pendingIntent must be set");
        return new DataUpdateListenerRegistrationRequest(this, null);
      }
    }
    
    public Builder setDataSource(DataSource paramDataSource)
      throws NullPointerException
    {
      zzaa.zzy(paramDataSource);
      this.Rz = paramDataSource;
      return this;
    }
    
    public Builder setDataType(DataType paramDataType)
    {
      zzaa.zzy(paramDataType);
      this.RA = paramDataType;
      return this;
    }
    
    public Builder setPendingIntent(PendingIntent paramPendingIntent)
    {
      zzaa.zzy(paramPendingIntent);
      this.mPendingIntent = paramPendingIntent;
      return this;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/request/DataUpdateListenerRegistrationRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */