package com.google.android.gms.fitness.request;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.internal.zzvb;
import com.google.android.gms.internal.zzvb.zza;

public class DataUpdateListenerUnregistrationRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<DataUpdateListenerUnregistrationRequest> CREATOR = new zzl();
  private final zzvb UP;
  private final PendingIntent mPendingIntent;
  private final int mVersionCode;
  
  DataUpdateListenerUnregistrationRequest(int paramInt, PendingIntent paramPendingIntent, IBinder paramIBinder)
  {
    this.mVersionCode = paramInt;
    this.mPendingIntent = paramPendingIntent;
    this.UP = zzvb.zza.zzgj(paramIBinder);
  }
  
  public DataUpdateListenerUnregistrationRequest(PendingIntent paramPendingIntent, IBinder paramIBinder)
  {
    this.mVersionCode = 1;
    this.mPendingIntent = paramPendingIntent;
    this.UP = zzvb.zza.zzgj(paramIBinder);
  }
  
  private boolean zzb(DataUpdateListenerUnregistrationRequest paramDataUpdateListenerUnregistrationRequest)
  {
    return zzz.equal(this.mPendingIntent, paramDataUpdateListenerUnregistrationRequest.mPendingIntent);
  }
  
  public boolean equals(Object paramObject)
  {
    return (paramObject == this) || (((paramObject instanceof DataUpdateListenerUnregistrationRequest)) && (zzb((DataUpdateListenerUnregistrationRequest)paramObject)));
  }
  
  public IBinder getCallbackBinder()
  {
    if (this.UP == null) {
      return null;
    }
    return this.UP.asBinder();
  }
  
  public PendingIntent getIntent()
  {
    return this.mPendingIntent;
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.mPendingIntent });
  }
  
  public String toString()
  {
    return "DataUpdateListenerUnregistrationRequest";
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzl.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/request/DataUpdateListenerUnregistrationRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */