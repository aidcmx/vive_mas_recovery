package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.internal.zzvb;
import com.google.android.gms.internal.zzvb.zza;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class DataUpdateRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<DataUpdateRequest> CREATOR = new zzm();
  private final long QL;
  private final DataSet TC;
  private final zzvb UP;
  private final long eg;
  private final int mVersionCode;
  
  DataUpdateRequest(int paramInt, long paramLong1, long paramLong2, DataSet paramDataSet, IBinder paramIBinder)
  {
    this.mVersionCode = paramInt;
    this.eg = paramLong1;
    this.QL = paramLong2;
    this.TC = paramDataSet;
    this.UP = zzvb.zza.zzgj(paramIBinder);
  }
  
  public DataUpdateRequest(long paramLong1, long paramLong2, DataSet paramDataSet, IBinder paramIBinder)
  {
    this.mVersionCode = 1;
    this.eg = paramLong1;
    this.QL = paramLong2;
    this.TC = paramDataSet;
    this.UP = zzvb.zza.zzgj(paramIBinder);
  }
  
  private DataUpdateRequest(Builder paramBuilder)
  {
    this(Builder.zza(paramBuilder), Builder.zzb(paramBuilder), Builder.zzc(paramBuilder), null);
  }
  
  public DataUpdateRequest(DataUpdateRequest paramDataUpdateRequest, IBinder paramIBinder)
  {
    this(paramDataUpdateRequest.zzagx(), paramDataUpdateRequest.zzban(), paramDataUpdateRequest.getDataSet(), paramIBinder);
  }
  
  private boolean zzb(DataUpdateRequest paramDataUpdateRequest)
  {
    return (this.eg == paramDataUpdateRequest.eg) && (this.QL == paramDataUpdateRequest.QL) && (zzz.equal(this.TC, paramDataUpdateRequest.TC));
  }
  
  public boolean equals(Object paramObject)
  {
    return (paramObject == this) || (((paramObject instanceof DataUpdateRequest)) && (zzb((DataUpdateRequest)paramObject)));
  }
  
  public IBinder getCallbackBinder()
  {
    if (this.UP == null) {
      return null;
    }
    return this.UP.asBinder();
  }
  
  public DataSet getDataSet()
  {
    return this.TC;
  }
  
  public long getEndTime(TimeUnit paramTimeUnit)
  {
    return paramTimeUnit.convert(this.QL, TimeUnit.MILLISECONDS);
  }
  
  public long getStartTime(TimeUnit paramTimeUnit)
  {
    return paramTimeUnit.convert(this.eg, TimeUnit.MILLISECONDS);
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { Long.valueOf(this.eg), Long.valueOf(this.QL), this.TC });
  }
  
  public String toString()
  {
    return zzz.zzx(this).zzg("startTimeMillis", Long.valueOf(this.eg)).zzg("endTimeMillis", Long.valueOf(this.QL)).zzg("dataSet", this.TC).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzm.zza(this, paramParcel, paramInt);
  }
  
  public long zzagx()
  {
    return this.eg;
  }
  
  public long zzban()
  {
    return this.QL;
  }
  
  public static class Builder
  {
    private long QL;
    private DataSet TC;
    private long eg;
    
    private void zzbga()
    {
      zzaa.zza(this.eg, "Must set a non-zero value for startTimeMillis/startTime");
      zzaa.zza(this.QL, "Must set a non-zero value for endTimeMillis/endTime");
      zzaa.zzb(this.TC, "Must set the data set");
      Iterator localIterator = this.TC.getDataPoints().iterator();
      if (localIterator.hasNext())
      {
        DataPoint localDataPoint = (DataPoint)localIterator.next();
        long l1 = localDataPoint.getStartTime(TimeUnit.MILLISECONDS);
        long l2 = localDataPoint.getEndTime(TimeUnit.MILLISECONDS);
        int i;
        if ((l1 > l2) || ((l1 != 0L) && (l1 < this.eg)) || ((l1 != 0L) && (l1 > this.QL)) || (l2 > this.QL) || (l2 < this.eg))
        {
          i = 1;
          label144:
          if (i != 0) {
            break label205;
          }
        }
        label205:
        for (boolean bool = true;; bool = false)
        {
          zzaa.zza(bool, "Data Point's startTimeMillis %d, endTimeMillis %d should lie between timeRange provided in the request. StartTimeMillis %d, EndTimeMillis: %d", new Object[] { Long.valueOf(l1), Long.valueOf(l2), Long.valueOf(this.eg), Long.valueOf(this.QL) });
          break;
          i = 0;
          break label144;
        }
      }
    }
    
    public DataUpdateRequest build()
    {
      zzbga();
      return new DataUpdateRequest(this, null);
    }
    
    public Builder setDataSet(DataSet paramDataSet)
    {
      zzaa.zzb(paramDataSet, "Must set the data set");
      this.TC = paramDataSet;
      return this;
    }
    
    public Builder setTimeInterval(long paramLong1, long paramLong2, TimeUnit paramTimeUnit)
    {
      if (paramLong1 > 0L)
      {
        bool = true;
        zzaa.zzb(bool, "Invalid start time :%d", new Object[] { Long.valueOf(paramLong1) });
        if (paramLong2 < paramLong1) {
          break label82;
        }
      }
      label82:
      for (boolean bool = true;; bool = false)
      {
        zzaa.zzb(bool, "Invalid end time :%d", new Object[] { Long.valueOf(paramLong2) });
        this.eg = paramTimeUnit.toMillis(paramLong1);
        this.QL = paramTimeUnit.toMillis(paramLong2);
        return this;
        bool = false;
        break;
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/request/DataUpdateRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */