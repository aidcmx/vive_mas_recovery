package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.internal.zzul;
import com.google.android.gms.internal.zzul.zza;

public class DebugInfoRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<DebugInfoRequest> CREATOR = new zzp();
  private final zzul Vq;
  private final int versionCode;
  
  DebugInfoRequest(int paramInt, IBinder paramIBinder)
  {
    this.versionCode = paramInt;
    this.Vq = zzul.zza.zzft(paramIBinder);
  }
  
  public IBinder getCallbackBinder()
  {
    return this.Vq.asBinder();
  }
  
  int getVersionCode()
  {
    return this.versionCode;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzp.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/request/DebugInfoRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */