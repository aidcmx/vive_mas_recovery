package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.internal.zzvb;
import com.google.android.gms.internal.zzvb.zza;

public class DisableFitRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<DisableFitRequest> CREATOR = new zzo();
  private final zzvb UP;
  private final int mVersionCode;
  
  DisableFitRequest(int paramInt, IBinder paramIBinder)
  {
    this.mVersionCode = paramInt;
    this.UP = zzvb.zza.zzgj(paramIBinder);
  }
  
  public DisableFitRequest(zzvb paramzzvb)
  {
    this.mVersionCode = 2;
    this.UP = paramzzvb;
  }
  
  public IBinder getCallbackBinder()
  {
    return this.UP.asBinder();
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public String toString()
  {
    return String.format("DisableFitRequest", new Object[0]);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzo.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/request/DisableFitRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */