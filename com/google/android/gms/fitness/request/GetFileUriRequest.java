package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.internal.zzum;
import com.google.android.gms.internal.zzum.zza;
import java.util.Locale;

public class GetFileUriRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<GetFileUriRequest> CREATOR = new zzq();
  private final zzum Vr;
  private final int versionCode;
  
  GetFileUriRequest(int paramInt, IBinder paramIBinder)
  {
    this.versionCode = paramInt;
    this.Vr = zzum.zza.zzfu(paramIBinder);
  }
  
  public IBinder getCallbackBinder()
  {
    return this.Vr.asBinder();
  }
  
  int getVersionCode()
  {
    return this.versionCode;
  }
  
  public String toString()
  {
    return String.format(Locale.ENGLISH, "GetFileUriRequest {%d, %s}", new Object[] { Integer.valueOf(this.versionCode), this.Vr });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzq.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/request/GetFileUriRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */