package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.internal.zzuj;
import com.google.android.gms.internal.zzuj.zza;

public class GetStoredDataSourcesRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<GetStoredDataSourcesRequest> CREATOR = new zzr();
  private final zzuj Vs;
  private final int versionCode;
  
  GetStoredDataSourcesRequest(int paramInt, IBinder paramIBinder)
  {
    this.versionCode = paramInt;
    this.Vs = zzuj.zza.zzfr(paramIBinder);
  }
  
  public IBinder getCallbackBinder()
  {
    return this.Vs.asBinder();
  }
  
  int getVersionCode()
  {
    return this.versionCode;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzr.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/request/GetStoredDataSourcesRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */