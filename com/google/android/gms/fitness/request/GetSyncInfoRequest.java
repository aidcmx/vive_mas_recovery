package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.internal.zzvc;
import com.google.android.gms.internal.zzvc.zza;

public class GetSyncInfoRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<GetSyncInfoRequest> CREATOR = new zzs();
  private final zzvc Vt;
  private final int mVersionCode;
  
  GetSyncInfoRequest(int paramInt, IBinder paramIBinder)
  {
    this.mVersionCode = paramInt;
    this.Vt = zzvc.zza.zzgk(paramIBinder);
  }
  
  public IBinder getCallbackBinder()
  {
    return this.Vt.asBinder();
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public String toString()
  {
    return String.format("GetSyncInfoRequest {%d, %s}", new Object[] { Integer.valueOf(this.mVersionCode), this.Vt });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzs.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/request/GetSyncInfoRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */