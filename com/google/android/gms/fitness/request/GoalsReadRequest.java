package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;
import com.google.android.gms.fitness.FitnessActivities;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.internal.zztq;
import com.google.android.gms.internal.zzun;
import com.google.android.gms.internal.zzun.zza;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class GoalsReadRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<GoalsReadRequest> CREATOR = new zzt();
  private final List<Integer> Sg;
  private final zzun Vu;
  private final List<DataType> Vv;
  private final List<Integer> Vw;
  private final int versionCode;
  
  GoalsReadRequest(int paramInt, IBinder paramIBinder, List<DataType> paramList, List<Integer> paramList1, List<Integer> paramList2)
  {
    this.versionCode = paramInt;
    if (paramIBinder == null) {}
    for (paramIBinder = null;; paramIBinder = zzun.zza.zzfv(paramIBinder))
    {
      this.Vu = paramIBinder;
      this.Vv = paramList;
      this.Vw = paramList1;
      this.Sg = paramList2;
      return;
    }
  }
  
  private GoalsReadRequest(Builder paramBuilder)
  {
    this(null, Builder.zza(paramBuilder), Builder.zzb(paramBuilder), Builder.zzc(paramBuilder));
  }
  
  public GoalsReadRequest(GoalsReadRequest paramGoalsReadRequest, zzun paramzzun)
  {
    this(paramzzun, paramGoalsReadRequest.getDataTypes(), paramGoalsReadRequest.zzbgb(), paramGoalsReadRequest.zzbet());
  }
  
  private GoalsReadRequest(zzun paramzzun, List<DataType> paramList, List<Integer> paramList1, List<Integer> paramList2) {}
  
  private boolean zzb(GoalsReadRequest paramGoalsReadRequest)
  {
    return (zzz.equal(this.Vv, paramGoalsReadRequest.Vv)) && (zzz.equal(this.Vw, paramGoalsReadRequest.Vw)) && (zzz.equal(this.Sg, paramGoalsReadRequest.Sg));
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof GoalsReadRequest)) && (zzb((GoalsReadRequest)paramObject)));
  }
  
  public List<String> getActivityNames()
  {
    if (this.Sg.isEmpty()) {
      return null;
    }
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = this.Sg.iterator();
    while (localIterator.hasNext()) {
      localArrayList.add(FitnessActivities.getName(((Integer)localIterator.next()).intValue()));
    }
    return localArrayList;
  }
  
  public IBinder getCallbackBinder()
  {
    return this.Vu.asBinder();
  }
  
  public List<DataType> getDataTypes()
  {
    return this.Vv;
  }
  
  public List<Integer> getObjectiveTypes()
  {
    if (this.Vw.isEmpty()) {
      return null;
    }
    return this.Vw;
  }
  
  int getVersionCode()
  {
    return this.versionCode;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.Vv, this.Vw, getActivityNames() });
  }
  
  public String toString()
  {
    return zzz.zzx(this).zzg("dataTypes", this.Vv).zzg("objectiveTypes", this.Vw).zzg("activities", getActivityNames()).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzt.zza(this, paramParcel, paramInt);
  }
  
  public List<Integer> zzbet()
  {
    return this.Sg;
  }
  
  public List<Integer> zzbgb()
  {
    return this.Vw;
  }
  
  public static class Builder
  {
    private final List<Integer> Sg = new ArrayList();
    private final List<DataType> Vv = new ArrayList();
    private final List<Integer> Vw = new ArrayList();
    
    public Builder addActivity(String paramString)
    {
      int i = FitnessActivities.zzje(paramString);
      if (i != 4) {}
      for (boolean bool = true;; bool = false)
      {
        zzaa.zza(bool, "Attempting to add an unknown activity");
        zztq.zza(Integer.valueOf(i), this.Sg);
        return this;
      }
    }
    
    public Builder addDataType(DataType paramDataType)
    {
      zzaa.zzb(paramDataType, "Attempting to use a null data type");
      if (!this.Vv.contains(paramDataType)) {
        this.Vv.add(paramDataType);
      }
      return this;
    }
    
    public Builder addObjectiveType(int paramInt)
    {
      boolean bool2 = true;
      boolean bool1 = bool2;
      if (paramInt != 1)
      {
        bool1 = bool2;
        if (paramInt != 2) {
          if (paramInt != 3) {
            break label61;
          }
        }
      }
      label61:
      for (bool1 = bool2;; bool1 = false)
      {
        zzaa.zza(bool1, "Attempting to add an invalid objective type");
        if (!this.Vw.contains(Integer.valueOf(paramInt))) {
          this.Vw.add(Integer.valueOf(paramInt));
        }
        return this;
      }
    }
    
    public GoalsReadRequest build()
    {
      if (!this.Vv.isEmpty()) {}
      for (boolean bool = true;; bool = false)
      {
        zzaa.zza(bool, "At least one data type should be specified.");
        return new GoalsReadRequest(this, null);
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/request/GoalsReadRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */