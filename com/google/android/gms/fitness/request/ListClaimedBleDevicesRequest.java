package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.internal.zzvo;
import com.google.android.gms.internal.zzvo.zza;

public class ListClaimedBleDevicesRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<ListClaimedBleDevicesRequest> CREATOR = new zzv();
  private final zzvo Vx;
  private final int mVersionCode;
  
  ListClaimedBleDevicesRequest(int paramInt, IBinder paramIBinder)
  {
    this.mVersionCode = paramInt;
    this.Vx = zzvo.zza.zzgl(paramIBinder);
  }
  
  public ListClaimedBleDevicesRequest(zzvo paramzzvo)
  {
    this.mVersionCode = 2;
    this.Vx = paramzzvo;
  }
  
  public IBinder getCallbackBinder()
  {
    return this.Vx.asBinder();
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzv.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/request/ListClaimedBleDevicesRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */