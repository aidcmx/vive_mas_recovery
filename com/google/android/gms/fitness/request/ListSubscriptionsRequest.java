package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.internal.zzuw;
import com.google.android.gms.internal.zzuw.zza;

public class ListSubscriptionsRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<ListSubscriptionsRequest> CREATOR = new zzw();
  private final DataType RA;
  private final zzuw Vy;
  private final int mVersionCode;
  
  ListSubscriptionsRequest(int paramInt, DataType paramDataType, IBinder paramIBinder)
  {
    this.mVersionCode = paramInt;
    this.RA = paramDataType;
    this.Vy = zzuw.zza.zzge(paramIBinder);
  }
  
  public ListSubscriptionsRequest(DataType paramDataType, zzuw paramzzuw)
  {
    this.mVersionCode = 3;
    this.RA = paramDataType;
    this.Vy = paramzzuw;
  }
  
  public IBinder getCallbackBinder()
  {
    if (this.Vy == null) {
      return null;
    }
    return this.Vy.asBinder();
  }
  
  public DataType getDataType()
  {
    return this.RA;
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzw.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/request/ListSubscriptionsRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */