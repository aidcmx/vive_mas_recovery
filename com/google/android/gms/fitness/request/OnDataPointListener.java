package com.google.android.gms.fitness.request;

import com.google.android.gms.fitness.data.DataPoint;

public abstract interface OnDataPointListener
{
  public abstract void onDataPoint(DataPoint paramDataPoint);
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/request/OnDataPointListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */