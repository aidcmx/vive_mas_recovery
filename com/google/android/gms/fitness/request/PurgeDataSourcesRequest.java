package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.internal.zzvb;
import com.google.android.gms.internal.zzvb.zza;
import java.util.Collections;
import java.util.List;

public class PurgeDataSourcesRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<PurgeDataSourcesRequest> CREATOR = new zzy();
  private final zzvb VC;
  private final List<String> VD;
  private final int versionCode;
  
  PurgeDataSourcesRequest(int paramInt, IBinder paramIBinder, List<String> paramList)
  {
    this.versionCode = paramInt;
    this.VC = zzvb.zza.zzgj(paramIBinder);
    if (paramList != null) {}
    for (;;)
    {
      this.VD = paramList;
      return;
      paramList = Collections.emptyList();
    }
  }
  
  public IBinder getCallbackBinder()
  {
    return this.VC.asBinder();
  }
  
  int getVersionCode()
  {
    return this.versionCode;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzy.zza(this, paramParcel, paramInt);
  }
  
  public List<String> zzbgd()
  {
    return this.VD;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/request/PurgeDataSourcesRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */