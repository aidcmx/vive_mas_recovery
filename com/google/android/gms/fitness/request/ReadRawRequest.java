package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz.zza;
import com.google.android.gms.internal.zzux;
import com.google.android.gms.internal.zzux.zza;
import java.util.List;

public class ReadRawRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<ReadRawRequest> CREATOR = new zzz();
  private final zzux VE;
  private final List<DataSourceQueryParams> VF;
  private final boolean VG;
  private final boolean VH;
  private final int versionCode;
  
  ReadRawRequest(int paramInt, IBinder paramIBinder, List<DataSourceQueryParams> paramList, boolean paramBoolean1, boolean paramBoolean2)
  {
    this.versionCode = paramInt;
    this.VE = zzux.zza.zzgf(paramIBinder);
    this.VF = paramList;
    this.VG = paramBoolean1;
    this.VH = paramBoolean2;
  }
  
  public IBinder getCallbackBinder()
  {
    if (this.VE != null) {
      return this.VE.asBinder();
    }
    return null;
  }
  
  int getVersionCode()
  {
    return this.versionCode;
  }
  
  public String toString()
  {
    return com.google.android.gms.common.internal.zzz.zzx(this).zzg("params", this.VF).zzg("server", Boolean.valueOf(this.VH)).zzg("flush", Boolean.valueOf(this.VG)).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzz.zza(this, paramParcel, paramInt);
  }
  
  public boolean zzbft()
  {
    return this.VH;
  }
  
  public boolean zzbfu()
  {
    return this.VG;
  }
  
  public List<DataSourceQueryParams> zzbge()
  {
    return this.VF;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/request/ReadRawRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */