package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.internal.zzuy;
import com.google.android.gms.internal.zzuy.zza;
import java.util.List;

public class ReadStatsRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<ReadStatsRequest> CREATOR = new zzaa();
  private final List<DataSource> US;
  private final zzuy VI;
  private final int mVersionCode;
  
  ReadStatsRequest(int paramInt, IBinder paramIBinder, List<DataSource> paramList)
  {
    this.mVersionCode = paramInt;
    this.VI = zzuy.zza.zzgg(paramIBinder);
    this.US = paramList;
  }
  
  public IBinder getCallbackBinder()
  {
    return this.VI.asBinder();
  }
  
  public List<DataSource> getDataSources()
  {
    return this.US;
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public String toString()
  {
    return String.format("ReadStatsRequest", new Object[0]);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzaa.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/request/ReadStatsRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */