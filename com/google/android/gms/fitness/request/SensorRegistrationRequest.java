package com.google.android.gms.fitness.request;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.zzt;
import com.google.android.gms.fitness.data.zzt.zza;
import com.google.android.gms.internal.zzvb;
import com.google.android.gms.internal.zzvb.zza;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.internal.ClientIdentity;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class SensorRegistrationRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<SensorRegistrationRequest> CREATOR = new zzab();
  private DataType RA;
  private DataSource Rz;
  private final long TD;
  private final int TE;
  private final zzvb UP;
  private zzt VJ;
  int VK;
  int VL;
  private final long VM;
  private final long VN;
  private final List<LocationRequest> VO;
  private final long VP;
  private final List<ClientIdentity> VQ;
  private final PendingIntent mPendingIntent;
  private final int mVersionCode;
  
  SensorRegistrationRequest(int paramInt1, DataSource paramDataSource, DataType paramDataType, IBinder paramIBinder1, int paramInt2, int paramInt3, long paramLong1, long paramLong2, PendingIntent paramPendingIntent, long paramLong3, int paramInt4, List<LocationRequest> paramList, long paramLong4, IBinder paramIBinder2)
  {
    this.mVersionCode = paramInt1;
    this.Rz = paramDataSource;
    this.RA = paramDataType;
    if (paramIBinder1 == null) {}
    for (paramDataSource = null;; paramDataSource = zzt.zza.zzfg(paramIBinder1))
    {
      this.VJ = paramDataSource;
      long l = paramLong1;
      if (paramLong1 == 0L) {
        l = paramInt2;
      }
      this.TD = l;
      this.VN = paramLong3;
      paramLong1 = paramLong2;
      if (paramLong2 == 0L) {
        paramLong1 = paramInt3;
      }
      this.VM = paramLong1;
      this.VO = paramList;
      this.mPendingIntent = paramPendingIntent;
      this.TE = paramInt4;
      this.VQ = Collections.emptyList();
      this.VP = paramLong4;
      this.UP = zzvb.zza.zzgj(paramIBinder2);
      return;
    }
  }
  
  public SensorRegistrationRequest(DataSource paramDataSource, DataType paramDataType, zzt paramzzt, PendingIntent paramPendingIntent, long paramLong1, long paramLong2, long paramLong3, int paramInt, List<LocationRequest> paramList, List<ClientIdentity> paramList1, long paramLong4, zzvb paramzzvb)
  {
    this.mVersionCode = 6;
    this.Rz = paramDataSource;
    this.RA = paramDataType;
    this.VJ = paramzzt;
    this.mPendingIntent = paramPendingIntent;
    this.TD = paramLong1;
    this.VN = paramLong2;
    this.VM = paramLong3;
    this.TE = paramInt;
    this.VO = paramList;
    this.VQ = paramList1;
    this.VP = paramLong4;
    this.UP = paramzzvb;
  }
  
  public SensorRegistrationRequest(SensorRequest paramSensorRequest, zzt paramzzt, PendingIntent paramPendingIntent, zzvb paramzzvb)
  {
    this(paramSensorRequest.getDataSource(), paramSensorRequest.getDataType(), paramzzt, paramPendingIntent, paramSensorRequest.getSamplingRate(TimeUnit.MICROSECONDS), paramSensorRequest.getFastestRate(TimeUnit.MICROSECONDS), paramSensorRequest.getMaxDeliveryLatency(TimeUnit.MICROSECONDS), paramSensorRequest.getAccuracyMode(), null, Collections.emptyList(), paramSensorRequest.zzbgk(), paramzzvb);
  }
  
  private boolean zzb(SensorRegistrationRequest paramSensorRegistrationRequest)
  {
    return (zzz.equal(this.Rz, paramSensorRegistrationRequest.Rz)) && (zzz.equal(this.RA, paramSensorRegistrationRequest.RA)) && (this.TD == paramSensorRegistrationRequest.TD) && (this.VN == paramSensorRegistrationRequest.VN) && (this.VM == paramSensorRegistrationRequest.VM) && (this.TE == paramSensorRegistrationRequest.TE) && (zzz.equal(this.VO, paramSensorRegistrationRequest.VO));
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof SensorRegistrationRequest)) && (zzb((SensorRegistrationRequest)paramObject)));
  }
  
  public int getAccuracyMode()
  {
    return this.TE;
  }
  
  public IBinder getCallbackBinder()
  {
    if (this.UP == null) {
      return null;
    }
    return this.UP.asBinder();
  }
  
  public DataSource getDataSource()
  {
    return this.Rz;
  }
  
  public DataType getDataType()
  {
    return this.RA;
  }
  
  public PendingIntent getIntent()
  {
    return this.mPendingIntent;
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.Rz, this.RA, this.VJ, Long.valueOf(this.TD), Long.valueOf(this.VN), Long.valueOf(this.VM), Integer.valueOf(this.TE), this.VO });
  }
  
  public String toString()
  {
    return String.format("SensorRegistrationRequest{type %s source %s interval %s fastest %s latency %s}", new Object[] { this.RA, this.Rz, Long.valueOf(this.TD), Long.valueOf(this.VN), Long.valueOf(this.VM) });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzab.zza(this, paramParcel, paramInt);
  }
  
  public long zzbfa()
  {
    return this.TD;
  }
  
  public long zzbgf()
  {
    return this.VN;
  }
  
  public long zzbgg()
  {
    return this.VM;
  }
  
  public List<LocationRequest> zzbgh()
  {
    return this.VO;
  }
  
  public long zzbgi()
  {
    return this.VP;
  }
  
  IBinder zzbgj()
  {
    if (this.VJ == null) {
      return null;
    }
    return this.VJ.asBinder();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/request/SensorRegistrationRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */