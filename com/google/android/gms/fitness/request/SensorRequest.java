package com.google.android.gms.fitness.request;

import android.os.SystemClock;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.internal.zztt;
import com.google.android.gms.location.LocationRequest;
import java.util.concurrent.TimeUnit;

public class SensorRequest
{
  public static final int ACCURACY_MODE_DEFAULT = 2;
  public static final int ACCURACY_MODE_HIGH = 3;
  public static final int ACCURACY_MODE_LOW = 1;
  private final DataType RA;
  private final DataSource Rz;
  private final long TD;
  private final int TE;
  private final long VM;
  private final long VN;
  private final LocationRequest VR;
  private final long VS;
  
  private SensorRequest(DataSource paramDataSource, LocationRequest paramLocationRequest)
  {
    this.VR = paramLocationRequest;
    this.TD = TimeUnit.MILLISECONDS.toMicros(paramLocationRequest.getInterval());
    this.VN = TimeUnit.MILLISECONDS.toMicros(paramLocationRequest.getFastestInterval());
    this.VM = this.TD;
    this.RA = paramDataSource.getDataType();
    this.TE = zza(paramLocationRequest);
    this.Rz = paramDataSource;
    long l = paramLocationRequest.getExpirationTime();
    if (l == Long.MAX_VALUE)
    {
      this.VS = Long.MAX_VALUE;
      return;
    }
    this.VS = TimeUnit.MILLISECONDS.toMicros(l - SystemClock.elapsedRealtime());
  }
  
  private SensorRequest(Builder paramBuilder)
  {
    this.Rz = Builder.zza(paramBuilder);
    this.RA = Builder.zzb(paramBuilder);
    this.TD = Builder.zzc(paramBuilder);
    this.VN = Builder.zzd(paramBuilder);
    this.VM = Builder.zze(paramBuilder);
    this.TE = Builder.zzf(paramBuilder);
    this.VR = null;
    this.VS = Builder.zzg(paramBuilder);
  }
  
  public static SensorRequest fromLocationRequest(DataSource paramDataSource, LocationRequest paramLocationRequest)
  {
    return new SensorRequest(paramDataSource, paramLocationRequest);
  }
  
  private static int zza(LocationRequest paramLocationRequest)
  {
    switch (paramLocationRequest.getPriority())
    {
    default: 
      return 2;
    case 100: 
      return 3;
    }
    return 1;
  }
  
  private boolean zza(SensorRequest paramSensorRequest)
  {
    return (zzz.equal(this.Rz, paramSensorRequest.Rz)) && (zzz.equal(this.RA, paramSensorRequest.RA)) && (this.TD == paramSensorRequest.TD) && (this.VN == paramSensorRequest.VN) && (this.VM == paramSensorRequest.VM) && (this.TE == paramSensorRequest.TE) && (zzz.equal(this.VR, paramSensorRequest.VR)) && (this.VS == paramSensorRequest.VS);
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof SensorRequest)) && (zza((SensorRequest)paramObject)));
  }
  
  public int getAccuracyMode()
  {
    return this.TE;
  }
  
  public DataSource getDataSource()
  {
    return this.Rz;
  }
  
  public DataType getDataType()
  {
    return this.RA;
  }
  
  public long getFastestRate(TimeUnit paramTimeUnit)
  {
    return paramTimeUnit.convert(this.VN, TimeUnit.MICROSECONDS);
  }
  
  public long getMaxDeliveryLatency(TimeUnit paramTimeUnit)
  {
    return paramTimeUnit.convert(this.VM, TimeUnit.MICROSECONDS);
  }
  
  public long getSamplingRate(TimeUnit paramTimeUnit)
  {
    return paramTimeUnit.convert(this.TD, TimeUnit.MICROSECONDS);
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.Rz, this.RA, Long.valueOf(this.TD), Long.valueOf(this.VN), Long.valueOf(this.VM), Integer.valueOf(this.TE), this.VR, Long.valueOf(this.VS) });
  }
  
  public String toString()
  {
    return zzz.zzx(this).zzg("dataSource", this.Rz).zzg("dataType", this.RA).zzg("samplingRateMicros", Long.valueOf(this.TD)).zzg("deliveryLatencyMicros", Long.valueOf(this.VM)).zzg("timeOutMicros", Long.valueOf(this.VS)).toString();
  }
  
  public long zzbgk()
  {
    return this.VS;
  }
  
  public static class Builder
  {
    private DataType RA;
    private DataSource Rz;
    private long TD = -1L;
    private int TE = 2;
    private long VM = 0L;
    private long VN = 0L;
    private long VS = Long.MAX_VALUE;
    private boolean VT = false;
    
    public SensorRequest build()
    {
      boolean bool2 = false;
      if ((this.Rz != null) || (this.RA != null)) {}
      for (boolean bool1 = true;; bool1 = false)
      {
        zzaa.zza(bool1, "Must call setDataSource() or setDataType()");
        if ((this.RA != null) && (this.Rz != null))
        {
          bool1 = bool2;
          if (!this.RA.equals(this.Rz.getDataType())) {}
        }
        else
        {
          bool1 = true;
        }
        zzaa.zza(bool1, "Specified data type is incompatible with specified data source");
        return new SensorRequest(this, null);
      }
    }
    
    public Builder setAccuracyMode(int paramInt)
    {
      this.TE = zztt.zzoq(paramInt);
      return this;
    }
    
    public Builder setDataSource(DataSource paramDataSource)
    {
      this.Rz = paramDataSource;
      return this;
    }
    
    public Builder setDataType(DataType paramDataType)
    {
      this.RA = paramDataType;
      return this;
    }
    
    public Builder setFastestRate(int paramInt, TimeUnit paramTimeUnit)
    {
      if (paramInt >= 0) {}
      for (boolean bool = true;; bool = false)
      {
        zzaa.zzb(bool, "Cannot use a negative interval");
        this.VT = true;
        this.VN = paramTimeUnit.toMicros(paramInt);
        return this;
      }
    }
    
    public Builder setMaxDeliveryLatency(int paramInt, TimeUnit paramTimeUnit)
    {
      if (paramInt >= 0) {}
      for (boolean bool = true;; bool = false)
      {
        zzaa.zzb(bool, "Cannot use a negative delivery interval");
        this.VM = paramTimeUnit.toMicros(paramInt);
        return this;
      }
    }
    
    public Builder setSamplingRate(long paramLong, TimeUnit paramTimeUnit)
    {
      if (paramLong >= 0L) {}
      for (boolean bool = true;; bool = false)
      {
        zzaa.zzb(bool, "Cannot use a negative sampling interval");
        this.TD = paramTimeUnit.toMicros(paramLong);
        if (!this.VT) {
          this.VN = (this.TD / 2L);
        }
        return this;
      }
    }
    
    public Builder setTimeout(long paramLong, TimeUnit paramTimeUnit)
    {
      boolean bool2 = true;
      if (paramLong > 0L)
      {
        bool1 = true;
        zzaa.zzb(bool1, "Invalid time out value specified: %d", new Object[] { Long.valueOf(paramLong) });
        if (paramTimeUnit == null) {
          break label62;
        }
      }
      label62:
      for (boolean bool1 = bool2;; bool1 = false)
      {
        zzaa.zzb(bool1, "Invalid time unit specified");
        this.VS = paramTimeUnit.toMicros(paramLong);
        return this;
        bool1 = false;
        break;
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/request/SensorRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */