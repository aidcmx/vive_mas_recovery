package com.google.android.gms.fitness.request;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.fitness.data.zzt;
import com.google.android.gms.fitness.data.zzt.zza;
import com.google.android.gms.internal.zzvb;
import com.google.android.gms.internal.zzvb.zza;

public class SensorUnregistrationRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<SensorUnregistrationRequest> CREATOR = new zzac();
  private final zzvb UP;
  private final zzt VJ;
  private final PendingIntent mPendingIntent;
  private final int mVersionCode;
  
  SensorUnregistrationRequest(int paramInt, IBinder paramIBinder1, PendingIntent paramPendingIntent, IBinder paramIBinder2)
  {
    this.mVersionCode = paramInt;
    if (paramIBinder1 == null) {}
    for (paramIBinder1 = null;; paramIBinder1 = zzt.zza.zzfg(paramIBinder1))
    {
      this.VJ = paramIBinder1;
      this.mPendingIntent = paramPendingIntent;
      this.UP = zzvb.zza.zzgj(paramIBinder2);
      return;
    }
  }
  
  public SensorUnregistrationRequest(zzt paramzzt, PendingIntent paramPendingIntent, zzvb paramzzvb)
  {
    this.mVersionCode = 4;
    this.VJ = paramzzt;
    this.mPendingIntent = paramPendingIntent;
    this.UP = paramzzvb;
  }
  
  public IBinder getCallbackBinder()
  {
    if (this.UP == null) {
      return null;
    }
    return this.UP.asBinder();
  }
  
  public PendingIntent getIntent()
  {
    return this.mPendingIntent;
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public String toString()
  {
    return String.format("SensorUnregistrationRequest{%s}", new Object[] { this.VJ });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzac.zza(this, paramParcel, paramInt);
  }
  
  IBinder zzbgj()
  {
    if (this.VJ == null) {
      return null;
    }
    return this.VJ.asBinder();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/request/SensorUnregistrationRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */