package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.util.Log;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.Session;
import com.google.android.gms.internal.zztp;
import com.google.android.gms.internal.zzvb;
import com.google.android.gms.internal.zzvb.zza;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class SessionInsertRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<SessionInsertRequest> CREATOR = new zzad();
  private final zzvb VC;
  private final Session VU;
  private final List<DataSet> VV;
  private final List<DataPoint> VW;
  private final int versionCode;
  
  SessionInsertRequest(int paramInt, Session paramSession, List<DataSet> paramList, List<DataPoint> paramList1, IBinder paramIBinder)
  {
    this.versionCode = paramInt;
    this.VU = paramSession;
    this.VV = Collections.unmodifiableList(paramList);
    this.VW = Collections.unmodifiableList(paramList1);
    this.VC = zzvb.zza.zzgj(paramIBinder);
  }
  
  public SessionInsertRequest(Session paramSession, List<DataSet> paramList, List<DataPoint> paramList1, zzvb paramzzvb)
  {
    this.versionCode = 3;
    this.VU = paramSession;
    this.VV = Collections.unmodifiableList(paramList);
    this.VW = Collections.unmodifiableList(paramList1);
    this.VC = paramzzvb;
  }
  
  private SessionInsertRequest(Builder paramBuilder)
  {
    this(Builder.zza(paramBuilder), Builder.zzb(paramBuilder), Builder.zzc(paramBuilder), null);
  }
  
  public SessionInsertRequest(SessionInsertRequest paramSessionInsertRequest, zzvb paramzzvb)
  {
    this(paramSessionInsertRequest.VU, paramSessionInsertRequest.VV, paramSessionInsertRequest.VW, paramzzvb);
  }
  
  private boolean zzb(SessionInsertRequest paramSessionInsertRequest)
  {
    return (zzz.equal(this.VU, paramSessionInsertRequest.VU)) && (zzz.equal(this.VV, paramSessionInsertRequest.VV)) && (zzz.equal(this.VW, paramSessionInsertRequest.VW));
  }
  
  public boolean equals(Object paramObject)
  {
    return (paramObject == this) || (((paramObject instanceof SessionInsertRequest)) && (zzb((SessionInsertRequest)paramObject)));
  }
  
  public List<DataPoint> getAggregateDataPoints()
  {
    return this.VW;
  }
  
  public IBinder getCallbackBinder()
  {
    if (this.VC == null) {
      return null;
    }
    return this.VC.asBinder();
  }
  
  public List<DataSet> getDataSets()
  {
    return this.VV;
  }
  
  public Session getSession()
  {
    return this.VU;
  }
  
  int getVersionCode()
  {
    return this.versionCode;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.VU, this.VV, this.VW });
  }
  
  public String toString()
  {
    return zzz.zzx(this).zzg("session", this.VU).zzg("dataSets", this.VV).zzg("aggregateDataPoints", this.VW).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzad.zza(this, paramParcel, paramInt);
  }
  
  public static class Builder
  {
    private Session VU;
    private List<DataSet> VV = new ArrayList();
    private List<DataPoint> VW = new ArrayList();
    private List<DataSource> VX = new ArrayList();
    
    private void zzbgl()
    {
      Iterator localIterator1 = this.VV.iterator();
      while (localIterator1.hasNext())
      {
        Iterator localIterator2 = ((DataSet)localIterator1.next()).getDataPoints().iterator();
        while (localIterator2.hasNext()) {
          zzg((DataPoint)localIterator2.next());
        }
      }
      localIterator1 = this.VW.iterator();
      while (localIterator1.hasNext()) {
        zzg((DataPoint)localIterator1.next());
      }
    }
    
    private void zzg(DataPoint paramDataPoint)
    {
      zzi(paramDataPoint);
      zzh(paramDataPoint);
    }
    
    private void zzh(DataPoint paramDataPoint)
    {
      long l3 = this.VU.getStartTime(TimeUnit.NANOSECONDS);
      long l4 = this.VU.getEndTime(TimeUnit.NANOSECONDS);
      long l5 = paramDataPoint.getStartTime(TimeUnit.NANOSECONDS);
      long l2 = paramDataPoint.getEndTime(TimeUnit.NANOSECONDS);
      TimeUnit localTimeUnit;
      long l1;
      if ((l5 != 0L) && (l2 != 0L))
      {
        localTimeUnit = TimeUnit.MILLISECONDS;
        l1 = l2;
        if (l2 > l4) {
          l1 = zztp.zza(l2, TimeUnit.NANOSECONDS, localTimeUnit);
        }
        if ((l5 < l3) || (l1 > l4)) {
          break label196;
        }
      }
      label196:
      for (boolean bool = true;; bool = false)
      {
        zzaa.zza(bool, "Data point %s has start and end times outside session interval [%d, %d]", new Object[] { paramDataPoint, Long.valueOf(l3), Long.valueOf(l4) });
        if (l1 != paramDataPoint.getEndTime(TimeUnit.NANOSECONDS))
        {
          Log.w("Fitness", String.format("Data point end time [%d] is truncated to [%d] to match the precision [%s] of the session start and end time", new Object[] { Long.valueOf(paramDataPoint.getEndTime(TimeUnit.NANOSECONDS)), Long.valueOf(l1), localTimeUnit }));
          paramDataPoint.setTimeInterval(l5, l1, TimeUnit.NANOSECONDS);
        }
        return;
      }
    }
    
    private void zzi(DataPoint paramDataPoint)
    {
      long l3 = this.VU.getStartTime(TimeUnit.NANOSECONDS);
      long l4 = this.VU.getEndTime(TimeUnit.NANOSECONDS);
      long l2 = paramDataPoint.getTimestamp(TimeUnit.NANOSECONDS);
      TimeUnit localTimeUnit;
      long l1;
      if (l2 != 0L)
      {
        localTimeUnit = TimeUnit.MILLISECONDS;
        if (l2 >= l3)
        {
          l1 = l2;
          if (l2 <= l4) {}
        }
        else
        {
          l1 = zztp.zza(l2, TimeUnit.NANOSECONDS, localTimeUnit);
        }
        if ((l1 < l3) || (l1 > l4)) {
          break label185;
        }
      }
      label185:
      for (boolean bool = true;; bool = false)
      {
        zzaa.zza(bool, "Data point %s has time stamp outside session interval [%d, %d]", new Object[] { paramDataPoint, Long.valueOf(l3), Long.valueOf(l4) });
        if (paramDataPoint.getTimestamp(TimeUnit.NANOSECONDS) != l1)
        {
          Log.w("Fitness", String.format("Data point timestamp [%d] is truncated to [%d] to match the precision [%s] of the session start and end time", new Object[] { Long.valueOf(paramDataPoint.getTimestamp(TimeUnit.NANOSECONDS)), Long.valueOf(l1), localTimeUnit }));
          paramDataPoint.setTimestamp(l1, TimeUnit.NANOSECONDS);
        }
        return;
      }
    }
    
    public Builder addAggregateDataPoint(DataPoint paramDataPoint)
    {
      DataSource localDataSource;
      if (paramDataPoint != null)
      {
        bool = true;
        zzaa.zzb(bool, "Must specify a valid aggregate data point.");
        localDataSource = paramDataPoint.getDataSource();
        if (this.VX.contains(localDataSource)) {
          break label79;
        }
      }
      label79:
      for (boolean bool = true;; bool = false)
      {
        zzaa.zza(bool, "Data set/Aggregate data point for this data source %s is already added.", new Object[] { localDataSource });
        DataSet.zze(paramDataPoint);
        this.VX.add(localDataSource);
        this.VW.add(paramDataPoint);
        return this;
        bool = false;
        break;
      }
    }
    
    public Builder addDataSet(DataSet paramDataSet)
    {
      boolean bool2 = true;
      DataSource localDataSource;
      if (paramDataSet != null)
      {
        bool1 = true;
        zzaa.zzb(bool1, "Must specify a valid data set.");
        localDataSource = paramDataSet.getDataSource();
        if (this.VX.contains(localDataSource)) {
          break label101;
        }
        bool1 = true;
        label36:
        zzaa.zza(bool1, "Data set for this data source %s is already added.", new Object[] { localDataSource });
        if (paramDataSet.getDataPoints().isEmpty()) {
          break label106;
        }
      }
      label101:
      label106:
      for (boolean bool1 = bool2;; bool1 = false)
      {
        zzaa.zzb(bool1, "No data points specified in the input data set.");
        this.VX.add(localDataSource);
        this.VV.add(paramDataSet);
        return this;
        bool1 = false;
        break;
        bool1 = false;
        break label36;
      }
    }
    
    public SessionInsertRequest build()
    {
      boolean bool2 = true;
      if (this.VU != null)
      {
        bool1 = true;
        zzaa.zza(bool1, "Must specify a valid session.");
        if (this.VU.getEndTime(TimeUnit.MILLISECONDS) == 0L) {
          break label59;
        }
      }
      label59:
      for (boolean bool1 = bool2;; bool1 = false)
      {
        zzaa.zza(bool1, "Must specify a valid end time, cannot insert a continuing session.");
        zzbgl();
        return new SessionInsertRequest(this, null);
        bool1 = false;
        break;
      }
    }
    
    public Builder setSession(Session paramSession)
    {
      this.VU = paramSession;
      return this;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/request/SessionInsertRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */