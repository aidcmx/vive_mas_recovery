package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.internal.zzuz;
import com.google.android.gms.internal.zzuz.zza;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class SessionReadRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<SessionReadRequest> CREATOR = new zzae();
  private final List<DataType> QK;
  private final long QL;
  private final List<DataSource> US;
  private final String VY;
  private boolean VZ;
  private final boolean Vd;
  private final List<String> Wa;
  private final zzuz Wb;
  private final long eg;
  private final int mVersionCode;
  private final String zzctq;
  
  SessionReadRequest(int paramInt, String paramString1, String paramString2, long paramLong1, long paramLong2, List<DataType> paramList, List<DataSource> paramList1, boolean paramBoolean1, boolean paramBoolean2, List<String> paramList2, IBinder paramIBinder)
  {
    this.mVersionCode = paramInt;
    this.VY = paramString1;
    this.zzctq = paramString2;
    this.eg = paramLong1;
    this.QL = paramLong2;
    this.QK = paramList;
    this.US = paramList1;
    this.VZ = paramBoolean1;
    this.Vd = paramBoolean2;
    this.Wa = paramList2;
    this.Wb = zzuz.zza.zzgh(paramIBinder);
  }
  
  private SessionReadRequest(Builder paramBuilder)
  {
    this(Builder.zza(paramBuilder), Builder.zzb(paramBuilder), Builder.zzc(paramBuilder), Builder.zzd(paramBuilder), Builder.zze(paramBuilder), Builder.zzf(paramBuilder), Builder.zzg(paramBuilder), Builder.zzh(paramBuilder), Builder.zzi(paramBuilder), null);
  }
  
  public SessionReadRequest(SessionReadRequest paramSessionReadRequest, zzuz paramzzuz)
  {
    this(paramSessionReadRequest.VY, paramSessionReadRequest.zzctq, paramSessionReadRequest.eg, paramSessionReadRequest.QL, paramSessionReadRequest.QK, paramSessionReadRequest.US, paramSessionReadRequest.VZ, paramSessionReadRequest.Vd, paramSessionReadRequest.Wa, paramzzuz);
  }
  
  public SessionReadRequest(String paramString1, String paramString2, long paramLong1, long paramLong2, List<DataType> paramList, List<DataSource> paramList1, boolean paramBoolean1, boolean paramBoolean2, List<String> paramList2, zzuz paramzzuz) {}
  
  private boolean zzb(SessionReadRequest paramSessionReadRequest)
  {
    return (zzz.equal(this.VY, paramSessionReadRequest.VY)) && (this.zzctq.equals(paramSessionReadRequest.zzctq)) && (this.eg == paramSessionReadRequest.eg) && (this.QL == paramSessionReadRequest.QL) && (zzz.equal(this.QK, paramSessionReadRequest.QK)) && (zzz.equal(this.US, paramSessionReadRequest.US)) && (this.VZ == paramSessionReadRequest.VZ) && (this.Wa.equals(paramSessionReadRequest.Wa)) && (this.Vd == paramSessionReadRequest.Vd);
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof SessionReadRequest)) && (zzb((SessionReadRequest)paramObject)));
  }
  
  public IBinder getCallbackBinder()
  {
    if (this.Wb == null) {
      return null;
    }
    return this.Wb.asBinder();
  }
  
  public List<DataSource> getDataSources()
  {
    return this.US;
  }
  
  public List<DataType> getDataTypes()
  {
    return this.QK;
  }
  
  public long getEndTime(TimeUnit paramTimeUnit)
  {
    return paramTimeUnit.convert(this.QL, TimeUnit.MILLISECONDS);
  }
  
  public List<String> getExcludedPackages()
  {
    return this.Wa;
  }
  
  public String getSessionId()
  {
    return this.zzctq;
  }
  
  public String getSessionName()
  {
    return this.VY;
  }
  
  public long getStartTime(TimeUnit paramTimeUnit)
  {
    return paramTimeUnit.convert(this.eg, TimeUnit.MILLISECONDS);
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.VY, this.zzctq, Long.valueOf(this.eg), Long.valueOf(this.QL) });
  }
  
  public boolean includeSessionsFromAllApps()
  {
    return this.VZ;
  }
  
  public String toString()
  {
    return zzz.zzx(this).zzg("sessionName", this.VY).zzg("sessionId", this.zzctq).zzg("startTimeMillis", Long.valueOf(this.eg)).zzg("endTimeMillis", Long.valueOf(this.QL)).zzg("dataTypes", this.QK).zzg("dataSources", this.US).zzg("sessionsFromAllApps", Boolean.valueOf(this.VZ)).zzg("excludedPackages", this.Wa).zzg("useServer", Boolean.valueOf(this.Vd)).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzae.zza(this, paramParcel, paramInt);
  }
  
  public long zzagx()
  {
    return this.eg;
  }
  
  public long zzban()
  {
    return this.QL;
  }
  
  public boolean zzbft()
  {
    return this.Vd;
  }
  
  public boolean zzbgm()
  {
    return this.VZ;
  }
  
  public static class Builder
  {
    private List<DataType> QK = new ArrayList();
    private long QL = 0L;
    private List<DataSource> US = new ArrayList();
    private String VY;
    private boolean VZ = false;
    private boolean Vd = false;
    private List<String> Wa = new ArrayList();
    private long eg = 0L;
    private String zzctq;
    
    public SessionReadRequest build()
    {
      if (this.eg > 0L)
      {
        bool = true;
        zzaa.zzb(bool, "Invalid start time: %s", new Object[] { Long.valueOf(this.eg) });
        if ((this.QL <= 0L) || (this.QL <= this.eg)) {
          break label89;
        }
      }
      label89:
      for (boolean bool = true;; bool = false)
      {
        zzaa.zzb(bool, "Invalid end time: %s", new Object[] { Long.valueOf(this.QL) });
        return new SessionReadRequest(this, null);
        bool = false;
        break;
      }
    }
    
    public Builder enableServerQueries()
    {
      this.Vd = true;
      return this;
    }
    
    public Builder excludePackage(String paramString)
    {
      zzaa.zzb(paramString, "Attempting to use a null package name");
      if (!this.Wa.contains(paramString)) {
        this.Wa.add(paramString);
      }
      return this;
    }
    
    public Builder read(DataSource paramDataSource)
    {
      zzaa.zzb(paramDataSource, "Attempting to add a null data source");
      if (!this.US.contains(paramDataSource)) {
        this.US.add(paramDataSource);
      }
      return this;
    }
    
    public Builder read(DataType paramDataType)
    {
      zzaa.zzb(paramDataType, "Attempting to use a null data type");
      if (!this.QK.contains(paramDataType)) {
        this.QK.add(paramDataType);
      }
      return this;
    }
    
    public Builder readSessionsFromAllApps()
    {
      this.VZ = true;
      return this;
    }
    
    public Builder setSessionId(String paramString)
    {
      this.zzctq = paramString;
      return this;
    }
    
    public Builder setSessionName(String paramString)
    {
      this.VY = paramString;
      return this;
    }
    
    public Builder setTimeInterval(long paramLong1, long paramLong2, TimeUnit paramTimeUnit)
    {
      this.eg = paramTimeUnit.toMillis(paramLong1);
      this.QL = paramTimeUnit.toMillis(paramLong2);
      return this;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/request/SessionReadRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */