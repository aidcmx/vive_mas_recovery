package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;
import com.google.android.gms.fitness.data.Session;
import com.google.android.gms.internal.zzvb;
import com.google.android.gms.internal.zzvb.zza;
import java.util.concurrent.TimeUnit;

public class SessionStartRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<SessionStartRequest> CREATOR = new zzag();
  private final Session QB;
  private final zzvb UP;
  private final int mVersionCode;
  
  SessionStartRequest(int paramInt, Session paramSession, IBinder paramIBinder)
  {
    this.mVersionCode = paramInt;
    this.QB = paramSession;
    this.UP = zzvb.zza.zzgj(paramIBinder);
  }
  
  public SessionStartRequest(Session paramSession, zzvb paramzzvb)
  {
    if (paramSession.getStartTime(TimeUnit.MILLISECONDS) < System.currentTimeMillis()) {}
    for (boolean bool = true;; bool = false)
    {
      zzaa.zzb(bool, "Cannot start a session in the future");
      zzaa.zzb(paramSession.isOngoing(), "Cannot start a session which has already ended");
      this.mVersionCode = 3;
      this.QB = paramSession;
      this.UP = paramzzvb;
      return;
    }
  }
  
  private boolean zzb(SessionStartRequest paramSessionStartRequest)
  {
    return zzz.equal(this.QB, paramSessionStartRequest.QB);
  }
  
  public boolean equals(Object paramObject)
  {
    return (paramObject == this) || (((paramObject instanceof SessionStartRequest)) && (zzb((SessionStartRequest)paramObject)));
  }
  
  public IBinder getCallbackBinder()
  {
    if (this.UP == null) {
      return null;
    }
    return this.UP.asBinder();
  }
  
  public Session getSession()
  {
    return this.QB;
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.QB });
  }
  
  public String toString()
  {
    return zzz.zzx(this).zzg("session", this.QB).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzag.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/request/SessionStartRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */