package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;
import com.google.android.gms.internal.zzva;
import com.google.android.gms.internal.zzva.zza;

public class SessionStopRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<SessionStopRequest> CREATOR = new zzah();
  private final String Tz;
  private final zzva Wd;
  private final String mName;
  private final int mVersionCode;
  
  SessionStopRequest(int paramInt, String paramString1, String paramString2, IBinder paramIBinder)
  {
    this.mVersionCode = paramInt;
    this.mName = paramString1;
    this.Tz = paramString2;
    this.Wd = zzva.zza.zzgi(paramIBinder);
  }
  
  public SessionStopRequest(String paramString1, String paramString2, zzva paramzzva)
  {
    this.mVersionCode = 3;
    this.mName = paramString1;
    this.Tz = paramString2;
    this.Wd = paramzzva;
  }
  
  private boolean zzb(SessionStopRequest paramSessionStopRequest)
  {
    return (zzz.equal(this.mName, paramSessionStopRequest.mName)) && (zzz.equal(this.Tz, paramSessionStopRequest.Tz));
  }
  
  public boolean equals(Object paramObject)
  {
    return (paramObject == this) || (((paramObject instanceof SessionStopRequest)) && (zzb((SessionStopRequest)paramObject)));
  }
  
  public IBinder getCallbackBinder()
  {
    if (this.Wd == null) {
      return null;
    }
    return this.Wd.asBinder();
  }
  
  public String getIdentifier()
  {
    return this.Tz;
  }
  
  public String getName()
  {
    return this.mName;
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.mName, this.Tz });
  }
  
  public String toString()
  {
    return zzz.zzx(this).zzg("name", this.mName).zzg("identifier", this.Tz).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzah.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/request/SessionStopRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */