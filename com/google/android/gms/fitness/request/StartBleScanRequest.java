package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;
import com.google.android.gms.common.util.zzb;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.internal.zzvb;
import com.google.android.gms.internal.zzvb.zza;
import java.util.Collections;
import java.util.List;

public class StartBleScanRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<StartBleScanRequest> CREATOR = new zzaj();
  private final List<DataType> QK;
  private final zzvb UP;
  private final zzu We;
  private final int Wf;
  private final int mVersionCode;
  
  StartBleScanRequest(int paramInt1, List<DataType> paramList, IBinder paramIBinder1, int paramInt2, IBinder paramIBinder2)
  {
    this.mVersionCode = paramInt1;
    this.QK = paramList;
    this.We = zzu.zza.zzgm(paramIBinder1);
    this.Wf = paramInt2;
    this.UP = zzvb.zza.zzgj(paramIBinder2);
  }
  
  private StartBleScanRequest(Builder paramBuilder)
  {
    this(zzb.zzb(Builder.zza(paramBuilder)), Builder.zzb(paramBuilder), Builder.zzc(paramBuilder), null);
  }
  
  public StartBleScanRequest(StartBleScanRequest paramStartBleScanRequest, zzvb paramzzvb)
  {
    this(paramStartBleScanRequest.QK, paramStartBleScanRequest.We, paramStartBleScanRequest.Wf, paramzzvb);
  }
  
  public StartBleScanRequest(List<DataType> paramList, zzu paramzzu, int paramInt, zzvb paramzzvb)
  {
    this.mVersionCode = 4;
    this.QK = paramList;
    this.We = paramzzu;
    this.Wf = paramInt;
    this.UP = paramzzvb;
  }
  
  public IBinder getCallbackBinder()
  {
    if (this.UP == null) {
      return null;
    }
    return this.UP.asBinder();
  }
  
  public List<DataType> getDataTypes()
  {
    return Collections.unmodifiableList(this.QK);
  }
  
  public int getTimeoutSecs()
  {
    return this.Wf;
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public String toString()
  {
    return zzz.zzx(this).zzg("dataTypes", this.QK).zzg("timeoutSecs", Integer.valueOf(this.Wf)).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzaj.zza(this, paramParcel, paramInt);
  }
  
  public IBinder zzbgo()
  {
    return this.We.asBinder();
  }
  
  public static class Builder
  {
    private DataType[] Vm = new DataType[0];
    private zzu We;
    private int Wf = 10;
    
    public StartBleScanRequest build()
    {
      if (this.We != null) {}
      for (boolean bool = true;; bool = false)
      {
        zzaa.zza(bool, "Must set BleScanCallback");
        return new StartBleScanRequest(this, null);
      }
    }
    
    public Builder setBleScanCallback(BleScanCallback paramBleScanCallback)
    {
      zza(zza.zza.zzbfm().zza(paramBleScanCallback));
      return this;
    }
    
    public Builder setDataTypes(DataType... paramVarArgs)
    {
      this.Vm = paramVarArgs;
      return this;
    }
    
    public Builder setTimeoutSecs(int paramInt)
    {
      boolean bool2 = true;
      if (paramInt > 0)
      {
        bool1 = true;
        zzaa.zzb(bool1, "Stop time must be greater than zero");
        if (paramInt > 60) {
          break label40;
        }
      }
      label40:
      for (boolean bool1 = bool2;; bool1 = false)
      {
        zzaa.zzb(bool1, "Stop time must be less than 1 minute");
        this.Wf = paramInt;
        return this;
        bool1 = false;
        break;
      }
    }
    
    public Builder zza(zzu paramzzu)
    {
      this.We = paramzzu;
      return this;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/request/StartBleScanRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */