package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.internal.zzvb;
import com.google.android.gms.internal.zzvb.zza;

public class StopBleScanRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<StopBleScanRequest> CREATOR = new zzak();
  private final zzvb UP;
  private final zzu We;
  private final int mVersionCode;
  
  StopBleScanRequest(int paramInt, IBinder paramIBinder1, IBinder paramIBinder2)
  {
    this.mVersionCode = paramInt;
    this.We = zzu.zza.zzgm(paramIBinder1);
    this.UP = zzvb.zza.zzgj(paramIBinder2);
  }
  
  public StopBleScanRequest(BleScanCallback paramBleScanCallback, zzvb paramzzvb)
  {
    this(zza.zza.zzbfm().zzb(paramBleScanCallback), paramzzvb);
  }
  
  public StopBleScanRequest(zzu paramzzu, zzvb paramzzvb)
  {
    this.mVersionCode = 3;
    this.We = paramzzu;
    this.UP = paramzzvb;
  }
  
  public IBinder getCallbackBinder()
  {
    if (this.UP == null) {
      return null;
    }
    return this.UP.asBinder();
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzak.zza(this, paramParcel, paramInt);
  }
  
  public IBinder zzbgo()
  {
    return this.We.asBinder();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/request/StopBleScanRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */