package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;
import com.google.android.gms.fitness.data.Subscription;
import com.google.android.gms.internal.zzvb;
import com.google.android.gms.internal.zzvb.zza;

public class SubscribeRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<SubscribeRequest> CREATOR = new zzal();
  private final zzvb UP;
  private Subscription Wg;
  private final boolean Wh;
  private final int mVersionCode;
  
  SubscribeRequest(int paramInt, Subscription paramSubscription, boolean paramBoolean, IBinder paramIBinder)
  {
    this.mVersionCode = paramInt;
    this.Wg = paramSubscription;
    this.Wh = paramBoolean;
    this.UP = zzvb.zza.zzgj(paramIBinder);
  }
  
  public SubscribeRequest(Subscription paramSubscription, boolean paramBoolean, zzvb paramzzvb)
  {
    this.mVersionCode = 3;
    this.Wg = paramSubscription;
    this.Wh = paramBoolean;
    this.UP = paramzzvb;
  }
  
  public IBinder getCallbackBinder()
  {
    if (this.UP == null) {
      return null;
    }
    return this.UP.asBinder();
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public String toString()
  {
    return zzz.zzx(this).zzg("subscription", this.Wg).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzal.zza(this, paramParcel, paramInt);
  }
  
  public Subscription zzbgp()
  {
    return this.Wg;
  }
  
  public boolean zzbgq()
  {
    return this.Wh;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/request/SubscribeRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */