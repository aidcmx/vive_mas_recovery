package com.google.android.gms.fitness.request;

import android.os.RemoteException;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.fitness.data.BleDevice;
import java.util.HashMap;
import java.util.Map;

public class zza
  extends zzu.zza
{
  private final BleScanCallback UK;
  
  private zza(BleScanCallback paramBleScanCallback)
  {
    this.UK = ((BleScanCallback)zzaa.zzy(paramBleScanCallback));
  }
  
  public void onDeviceFound(BleDevice paramBleDevice)
    throws RemoteException
  {
    this.UK.onDeviceFound(paramBleDevice);
  }
  
  public void onScanStopped()
    throws RemoteException
  {
    this.UK.onScanStopped();
  }
  
  public static class zza
  {
    private static final zza UL = new zza();
    private final Map<BleScanCallback, zza> UM = new HashMap();
    
    public static zza zzbfm()
    {
      return UL;
    }
    
    public zza zza(BleScanCallback paramBleScanCallback)
    {
      synchronized (this.UM)
      {
        zza localzza2 = (zza)this.UM.get(paramBleScanCallback);
        zza localzza1 = localzza2;
        if (localzza2 == null)
        {
          localzza1 = new zza(paramBleScanCallback, null);
          this.UM.put(paramBleScanCallback, localzza1);
        }
        return localzza1;
      }
    }
    
    public zza zzb(BleScanCallback paramBleScanCallback)
    {
      synchronized (this.UM)
      {
        zza localzza = (zza)this.UM.get(paramBleScanCallback);
        if (localzza != null) {
          return localzza;
        }
        paramBleScanCallback = new zza(paramBleScanCallback, null);
        return paramBleScanCallback;
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/request/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */