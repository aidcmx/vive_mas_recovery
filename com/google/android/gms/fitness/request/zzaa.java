package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.fitness.data.DataSource;
import java.util.ArrayList;

public class zzaa
  implements Parcelable.Creator<ReadStatsRequest>
{
  static void zza(ReadStatsRequest paramReadStatsRequest, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.zzcs(paramParcel);
    zzb.zza(paramParcel, 1, paramReadStatsRequest.getCallbackBinder(), false);
    zzb.zzc(paramParcel, 3, paramReadStatsRequest.getDataSources(), false);
    zzb.zzc(paramParcel, 1000, paramReadStatsRequest.getVersionCode());
    zzb.zzaj(paramParcel, paramInt);
  }
  
  public ReadStatsRequest zzkg(Parcel paramParcel)
  {
    ArrayList localArrayList = null;
    int j = zza.zzcr(paramParcel);
    int i = 0;
    IBinder localIBinder = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.zzcq(paramParcel);
      switch (zza.zzgu(k))
      {
      default: 
        zza.zzb(paramParcel, k);
        break;
      case 1: 
        localIBinder = zza.zzr(paramParcel, k);
        break;
      case 3: 
        localArrayList = zza.zzc(paramParcel, k, DataSource.CREATOR);
        break;
      case 1000: 
        i = zza.zzg(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new ReadStatsRequest(i, localIBinder, localArrayList);
  }
  
  public ReadStatsRequest[] zzpq(int paramInt)
  {
    return new ReadStatsRequest[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/request/zzaa.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */