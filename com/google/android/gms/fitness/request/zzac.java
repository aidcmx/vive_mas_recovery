package com.google.android.gms.fitness.request;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzac
  implements Parcelable.Creator<SensorUnregistrationRequest>
{
  static void zza(SensorUnregistrationRequest paramSensorUnregistrationRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zza(paramParcel, 1, paramSensorUnregistrationRequest.zzbgj(), false);
    zzb.zza(paramParcel, 2, paramSensorUnregistrationRequest.getIntent(), paramInt, false);
    zzb.zza(paramParcel, 3, paramSensorUnregistrationRequest.getCallbackBinder(), false);
    zzb.zzc(paramParcel, 1000, paramSensorUnregistrationRequest.getVersionCode());
    zzb.zzaj(paramParcel, i);
  }
  
  public SensorUnregistrationRequest zzki(Parcel paramParcel)
  {
    IBinder localIBinder = null;
    int j = zza.zzcr(paramParcel);
    int i = 0;
    Object localObject2 = null;
    Object localObject1 = null;
    if (paramParcel.dataPosition() < j)
    {
      int k = zza.zzcq(paramParcel);
      Object localObject3;
      switch (zza.zzgu(k))
      {
      default: 
        zza.zzb(paramParcel, k);
        localObject3 = localObject2;
        localObject2 = localObject1;
        localObject1 = localObject3;
      }
      for (;;)
      {
        localObject3 = localObject2;
        localObject2 = localObject1;
        localObject1 = localObject3;
        break;
        localObject3 = zza.zzr(paramParcel, k);
        localObject1 = localObject2;
        localObject2 = localObject3;
        continue;
        localObject3 = (PendingIntent)zza.zza(paramParcel, k, PendingIntent.CREATOR);
        localObject2 = localObject1;
        localObject1 = localObject3;
        continue;
        localIBinder = zza.zzr(paramParcel, k);
        localObject3 = localObject1;
        localObject1 = localObject2;
        localObject2 = localObject3;
        continue;
        i = zza.zzg(paramParcel, k);
        localObject3 = localObject1;
        localObject1 = localObject2;
        localObject2 = localObject3;
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new SensorUnregistrationRequest(i, (IBinder)localObject1, (PendingIntent)localObject2, localIBinder);
  }
  
  public SensorUnregistrationRequest[] zzps(int paramInt)
  {
    return new SensorUnregistrationRequest[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/request/zzac.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */