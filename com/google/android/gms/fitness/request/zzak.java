package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzak
  implements Parcelable.Creator<StopBleScanRequest>
{
  static void zza(StopBleScanRequest paramStopBleScanRequest, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.zzcs(paramParcel);
    zzb.zza(paramParcel, 1, paramStopBleScanRequest.zzbgo(), false);
    zzb.zza(paramParcel, 2, paramStopBleScanRequest.getCallbackBinder(), false);
    zzb.zzc(paramParcel, 1000, paramStopBleScanRequest.getVersionCode());
    zzb.zzaj(paramParcel, paramInt);
  }
  
  public StopBleScanRequest zzkq(Parcel paramParcel)
  {
    IBinder localIBinder2 = null;
    int j = zza.zzcr(paramParcel);
    int i = 0;
    IBinder localIBinder1 = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.zzcq(paramParcel);
      switch (zza.zzgu(k))
      {
      default: 
        zza.zzb(paramParcel, k);
        break;
      case 1: 
        localIBinder1 = zza.zzr(paramParcel, k);
        break;
      case 2: 
        localIBinder2 = zza.zzr(paramParcel, k);
        break;
      case 1000: 
        i = zza.zzg(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new StopBleScanRequest(i, localIBinder1, localIBinder2);
  }
  
  public StopBleScanRequest[] zzqa(int paramInt)
  {
    return new StopBleScanRequest[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/request/zzak.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */