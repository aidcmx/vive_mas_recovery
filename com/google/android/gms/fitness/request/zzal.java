package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.fitness.data.Subscription;

public class zzal
  implements Parcelable.Creator<SubscribeRequest>
{
  static void zza(SubscribeRequest paramSubscribeRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zza(paramParcel, 1, paramSubscribeRequest.zzbgp(), paramInt, false);
    zzb.zza(paramParcel, 2, paramSubscribeRequest.zzbgq());
    zzb.zza(paramParcel, 3, paramSubscribeRequest.getCallbackBinder(), false);
    zzb.zzc(paramParcel, 1000, paramSubscribeRequest.getVersionCode());
    zzb.zzaj(paramParcel, i);
  }
  
  public SubscribeRequest zzkr(Parcel paramParcel)
  {
    IBinder localIBinder = null;
    boolean bool = false;
    int j = zza.zzcr(paramParcel);
    Subscription localSubscription = null;
    int i = 0;
    if (paramParcel.dataPosition() < j)
    {
      int k = zza.zzcq(paramParcel);
      switch (zza.zzgu(k))
      {
      default: 
        zza.zzb(paramParcel, k);
      }
      for (;;)
      {
        break;
        localSubscription = (Subscription)zza.zza(paramParcel, k, Subscription.CREATOR);
        continue;
        bool = zza.zzc(paramParcel, k);
        continue;
        localIBinder = zza.zzr(paramParcel, k);
        continue;
        i = zza.zzg(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new SubscribeRequest(i, localSubscription, bool, localIBinder);
  }
  
  public SubscribeRequest[] zzqb(int paramInt)
  {
    return new SubscribeRequest[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/request/zzal.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */