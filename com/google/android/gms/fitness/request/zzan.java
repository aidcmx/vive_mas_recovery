package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;

public class zzan
  implements Parcelable.Creator<UnsubscribeRequest>
{
  static void zza(UnsubscribeRequest paramUnsubscribeRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zza(paramParcel, 1, paramUnsubscribeRequest.getDataType(), paramInt, false);
    zzb.zza(paramParcel, 2, paramUnsubscribeRequest.getDataSource(), paramInt, false);
    zzb.zza(paramParcel, 3, paramUnsubscribeRequest.getCallbackBinder(), false);
    zzb.zzc(paramParcel, 1000, paramUnsubscribeRequest.getVersionCode());
    zzb.zzaj(paramParcel, i);
  }
  
  public UnsubscribeRequest zzkt(Parcel paramParcel)
  {
    IBinder localIBinder = null;
    int j = zza.zzcr(paramParcel);
    int i = 0;
    Object localObject2 = null;
    Object localObject1 = null;
    if (paramParcel.dataPosition() < j)
    {
      int k = zza.zzcq(paramParcel);
      Object localObject3;
      switch (zza.zzgu(k))
      {
      default: 
        zza.zzb(paramParcel, k);
        localObject3 = localObject2;
        localObject2 = localObject1;
        localObject1 = localObject3;
      }
      for (;;)
      {
        localObject3 = localObject2;
        localObject2 = localObject1;
        localObject1 = localObject3;
        break;
        localObject3 = (DataType)zza.zza(paramParcel, k, DataType.CREATOR);
        localObject1 = localObject2;
        localObject2 = localObject3;
        continue;
        localObject3 = (DataSource)zza.zza(paramParcel, k, DataSource.CREATOR);
        localObject2 = localObject1;
        localObject1 = localObject3;
        continue;
        localIBinder = zza.zzr(paramParcel, k);
        localObject3 = localObject1;
        localObject1 = localObject2;
        localObject2 = localObject3;
        continue;
        i = zza.zzg(paramParcel, k);
        localObject3 = localObject1;
        localObject1 = localObject2;
        localObject2 = localObject3;
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new UnsubscribeRequest(i, (DataType)localObject1, (DataSource)localObject2, localIBinder);
  }
  
  public UnsubscribeRequest[] zzqd(int paramInt)
  {
    return new UnsubscribeRequest[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/request/zzan.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */