package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Device;
import java.util.ArrayList;

public class zzf
  implements Parcelable.Creator<DataReadRequest>
{
  static void zza(DataReadRequest paramDataReadRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramDataReadRequest.getDataTypes(), false);
    zzb.zzc(paramParcel, 2, paramDataReadRequest.getDataSources(), false);
    zzb.zza(paramParcel, 3, paramDataReadRequest.zzagx());
    zzb.zza(paramParcel, 4, paramDataReadRequest.zzban());
    zzb.zzc(paramParcel, 5, paramDataReadRequest.getAggregatedDataTypes(), false);
    zzb.zzc(paramParcel, 6, paramDataReadRequest.getAggregatedDataSources(), false);
    zzb.zzc(paramParcel, 7, paramDataReadRequest.getBucketType());
    zzb.zzc(paramParcel, 1000, paramDataReadRequest.getVersionCode());
    zzb.zza(paramParcel, 8, paramDataReadRequest.zzbfv());
    zzb.zza(paramParcel, 9, paramDataReadRequest.getActivityDataSource(), paramInt, false);
    zzb.zzc(paramParcel, 10, paramDataReadRequest.getLimit());
    zzb.zza(paramParcel, 12, paramDataReadRequest.zzbfu());
    zzb.zza(paramParcel, 13, paramDataReadRequest.zzbft());
    zzb.zza(paramParcel, 14, paramDataReadRequest.getCallbackBinder(), false);
    zzb.zzc(paramParcel, 16, paramDataReadRequest.zzbfw(), false);
    zzb.zza(paramParcel, 17, paramDataReadRequest.zzbfx(), false);
    zzb.zzaj(paramParcel, i);
  }
  
  public DataReadRequest zzjn(Parcel paramParcel)
  {
    int m = zza.zzcr(paramParcel);
    int k = 0;
    ArrayList localArrayList6 = null;
    ArrayList localArrayList5 = null;
    long l3 = 0L;
    long l2 = 0L;
    ArrayList localArrayList4 = null;
    ArrayList localArrayList3 = null;
    int j = 0;
    long l1 = 0L;
    DataSource localDataSource = null;
    int i = 0;
    boolean bool2 = false;
    boolean bool1 = false;
    IBinder localIBinder = null;
    ArrayList localArrayList2 = null;
    ArrayList localArrayList1 = null;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.zzcq(paramParcel);
      switch (zza.zzgu(n))
      {
      default: 
        zza.zzb(paramParcel, n);
        break;
      case 1: 
        localArrayList6 = zza.zzc(paramParcel, n, DataType.CREATOR);
        break;
      case 2: 
        localArrayList5 = zza.zzc(paramParcel, n, DataSource.CREATOR);
        break;
      case 3: 
        l3 = zza.zzi(paramParcel, n);
        break;
      case 4: 
        l2 = zza.zzi(paramParcel, n);
        break;
      case 5: 
        localArrayList4 = zza.zzc(paramParcel, n, DataType.CREATOR);
        break;
      case 6: 
        localArrayList3 = zza.zzc(paramParcel, n, DataSource.CREATOR);
        break;
      case 7: 
        j = zza.zzg(paramParcel, n);
        break;
      case 1000: 
        k = zza.zzg(paramParcel, n);
        break;
      case 8: 
        l1 = zza.zzi(paramParcel, n);
        break;
      case 9: 
        localDataSource = (DataSource)zza.zza(paramParcel, n, DataSource.CREATOR);
        break;
      case 10: 
        i = zza.zzg(paramParcel, n);
        break;
      case 12: 
        bool2 = zza.zzc(paramParcel, n);
        break;
      case 13: 
        bool1 = zza.zzc(paramParcel, n);
        break;
      case 14: 
        localIBinder = zza.zzr(paramParcel, n);
        break;
      case 16: 
        localArrayList2 = zza.zzc(paramParcel, n, Device.CREATOR);
        break;
      case 17: 
        localArrayList1 = zza.zzad(paramParcel, n);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza(37 + "Overread allowed size end=" + m, paramParcel);
    }
    return new DataReadRequest(k, localArrayList6, localArrayList5, l3, l2, localArrayList4, localArrayList3, j, l1, localDataSource, i, bool2, bool1, localIBinder, localArrayList2, localArrayList1);
  }
  
  public DataReadRequest[] zzox(int paramInt)
  {
    return new DataReadRequest[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/request/zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */