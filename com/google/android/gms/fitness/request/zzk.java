package com.google.android.gms.fitness.request;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;

public class zzk
  implements Parcelable.Creator<DataUpdateListenerRegistrationRequest>
{
  static void zza(DataUpdateListenerRegistrationRequest paramDataUpdateListenerRegistrationRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zza(paramParcel, 1, paramDataUpdateListenerRegistrationRequest.getDataSource(), paramInt, false);
    zzb.zza(paramParcel, 2, paramDataUpdateListenerRegistrationRequest.getDataType(), paramInt, false);
    zzb.zza(paramParcel, 3, paramDataUpdateListenerRegistrationRequest.getIntent(), paramInt, false);
    zzb.zza(paramParcel, 4, paramDataUpdateListenerRegistrationRequest.getCallbackBinder(), false);
    zzb.zzc(paramParcel, 1000, paramDataUpdateListenerRegistrationRequest.getVersionCode());
    zzb.zzaj(paramParcel, i);
  }
  
  public DataUpdateListenerRegistrationRequest zzjs(Parcel paramParcel)
  {
    IBinder localIBinder = null;
    int j = zza.zzcr(paramParcel);
    int i = 0;
    PendingIntent localPendingIntent = null;
    DataType localDataType = null;
    DataSource localDataSource = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.zzcq(paramParcel);
      switch (zza.zzgu(k))
      {
      default: 
        zza.zzb(paramParcel, k);
        break;
      case 1: 
        localDataSource = (DataSource)zza.zza(paramParcel, k, DataSource.CREATOR);
        break;
      case 2: 
        localDataType = (DataType)zza.zza(paramParcel, k, DataType.CREATOR);
        break;
      case 3: 
        localPendingIntent = (PendingIntent)zza.zza(paramParcel, k, PendingIntent.CREATOR);
        break;
      case 4: 
        localIBinder = zza.zzr(paramParcel, k);
        break;
      case 1000: 
        i = zza.zzg(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new DataUpdateListenerRegistrationRequest(i, localDataSource, localDataType, localPendingIntent, localIBinder);
  }
  
  public DataUpdateListenerRegistrationRequest[] zzpc(int paramInt)
  {
    return new DataUpdateListenerRegistrationRequest[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/request/zzk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */