package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.fitness.data.DataSet;

public class zzm
  implements Parcelable.Creator<DataUpdateRequest>
{
  static void zza(DataUpdateRequest paramDataUpdateRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zza(paramParcel, 1, paramDataUpdateRequest.zzagx());
    zzb.zza(paramParcel, 2, paramDataUpdateRequest.zzban());
    zzb.zza(paramParcel, 3, paramDataUpdateRequest.getDataSet(), paramInt, false);
    zzb.zza(paramParcel, 4, paramDataUpdateRequest.getCallbackBinder(), false);
    zzb.zzc(paramParcel, 1000, paramDataUpdateRequest.getVersionCode());
    zzb.zzaj(paramParcel, i);
  }
  
  public DataUpdateRequest zzju(Parcel paramParcel)
  {
    long l1 = 0L;
    IBinder localIBinder = null;
    int j = zza.zzcr(paramParcel);
    int i = 0;
    DataSet localDataSet = null;
    long l2 = 0L;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.zzcq(paramParcel);
      switch (zza.zzgu(k))
      {
      default: 
        zza.zzb(paramParcel, k);
        break;
      case 1: 
        l2 = zza.zzi(paramParcel, k);
        break;
      case 2: 
        l1 = zza.zzi(paramParcel, k);
        break;
      case 3: 
        localDataSet = (DataSet)zza.zza(paramParcel, k, DataSet.CREATOR);
        break;
      case 4: 
        localIBinder = zza.zzr(paramParcel, k);
        break;
      case 1000: 
        i = zza.zzg(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new DataUpdateRequest(i, l2, l1, localDataSet, localIBinder);
  }
  
  public DataUpdateRequest[] zzpe(int paramInt)
  {
    return new DataUpdateRequest[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/request/zzm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */