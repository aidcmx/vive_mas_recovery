package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzp
  implements Parcelable.Creator<DebugInfoRequest>
{
  static void zza(DebugInfoRequest paramDebugInfoRequest, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.zzcs(paramParcel);
    zzb.zza(paramParcel, 1, paramDebugInfoRequest.getCallbackBinder(), false);
    zzb.zzc(paramParcel, 1000, paramDebugInfoRequest.getVersionCode());
    zzb.zzaj(paramParcel, paramInt);
  }
  
  public DebugInfoRequest zzjx(Parcel paramParcel)
  {
    int j = zza.zzcr(paramParcel);
    int i = 0;
    IBinder localIBinder = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.zzcq(paramParcel);
      switch (zza.zzgu(k))
      {
      default: 
        zza.zzb(paramParcel, k);
        break;
      case 1: 
        localIBinder = zza.zzr(paramParcel, k);
        break;
      case 1000: 
        i = zza.zzg(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new DebugInfoRequest(i, localIBinder);
  }
  
  public DebugInfoRequest[] zzph(int paramInt)
  {
    return new DebugInfoRequest[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/request/zzp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */