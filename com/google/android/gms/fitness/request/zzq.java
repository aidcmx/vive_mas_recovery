package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzq
  implements Parcelable.Creator<GetFileUriRequest>
{
  static void zza(GetFileUriRequest paramGetFileUriRequest, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.zzcs(paramParcel);
    zzb.zza(paramParcel, 1, paramGetFileUriRequest.getCallbackBinder(), false);
    zzb.zzc(paramParcel, 1000, paramGetFileUriRequest.getVersionCode());
    zzb.zzaj(paramParcel, paramInt);
  }
  
  public GetFileUriRequest zzjy(Parcel paramParcel)
  {
    int j = zza.zzcr(paramParcel);
    int i = 0;
    IBinder localIBinder = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.zzcq(paramParcel);
      switch (zza.zzgu(k))
      {
      default: 
        zza.zzb(paramParcel, k);
        break;
      case 1: 
        localIBinder = zza.zzr(paramParcel, k);
        break;
      case 1000: 
        i = zza.zzg(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new GetFileUriRequest(i, localIBinder);
  }
  
  public GetFileUriRequest[] zzpi(int paramInt)
  {
    return new GetFileUriRequest[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/request/zzq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */