package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.ArrayList;

public class zzt
  implements Parcelable.Creator<GoalsReadRequest>
{
  static void zza(GoalsReadRequest paramGoalsReadRequest, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.zzcs(paramParcel);
    zzb.zza(paramParcel, 1, paramGoalsReadRequest.getCallbackBinder(), false);
    zzb.zzd(paramParcel, 2, paramGoalsReadRequest.getDataTypes(), false);
    zzb.zzd(paramParcel, 3, paramGoalsReadRequest.zzbgb(), false);
    zzb.zzd(paramParcel, 4, paramGoalsReadRequest.zzbet(), false);
    zzb.zzc(paramParcel, 1000, paramGoalsReadRequest.getVersionCode());
    zzb.zzaj(paramParcel, paramInt);
  }
  
  public GoalsReadRequest zzkb(Parcel paramParcel)
  {
    int j = zza.zzcr(paramParcel);
    int i = 0;
    IBinder localIBinder = null;
    ArrayList localArrayList1 = new ArrayList();
    ArrayList localArrayList2 = new ArrayList();
    ArrayList localArrayList3 = new ArrayList();
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.zzcq(paramParcel);
      switch (zza.zzgu(k))
      {
      default: 
        zza.zzb(paramParcel, k);
        break;
      case 1: 
        localIBinder = zza.zzr(paramParcel, k);
        break;
      case 2: 
        zza.zza(paramParcel, k, localArrayList1, getClass().getClassLoader());
        break;
      case 3: 
        zza.zza(paramParcel, k, localArrayList2, getClass().getClassLoader());
        break;
      case 4: 
        zza.zza(paramParcel, k, localArrayList3, getClass().getClassLoader());
        break;
      case 1000: 
        i = zza.zzg(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new GoalsReadRequest(i, localIBinder, localArrayList1, localArrayList2, localArrayList3);
  }
  
  public GoalsReadRequest[] zzpl(int paramInt)
  {
    return new GoalsReadRequest[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/request/zzt.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */