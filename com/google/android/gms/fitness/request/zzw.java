package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.fitness.data.DataType;

public class zzw
  implements Parcelable.Creator<ListSubscriptionsRequest>
{
  static void zza(ListSubscriptionsRequest paramListSubscriptionsRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zza(paramParcel, 1, paramListSubscriptionsRequest.getDataType(), paramInt, false);
    zzb.zza(paramParcel, 2, paramListSubscriptionsRequest.getCallbackBinder(), false);
    zzb.zzc(paramParcel, 1000, paramListSubscriptionsRequest.getVersionCode());
    zzb.zzaj(paramParcel, i);
  }
  
  public ListSubscriptionsRequest zzkd(Parcel paramParcel)
  {
    IBinder localIBinder = null;
    int j = zza.zzcr(paramParcel);
    int i = 0;
    DataType localDataType = null;
    if (paramParcel.dataPosition() < j)
    {
      int k = zza.zzcq(paramParcel);
      switch (zza.zzgu(k))
      {
      default: 
        zza.zzb(paramParcel, k);
      }
      for (;;)
      {
        break;
        localDataType = (DataType)zza.zza(paramParcel, k, DataType.CREATOR);
        continue;
        localIBinder = zza.zzr(paramParcel, k);
        continue;
        i = zza.zzg(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new ListSubscriptionsRequest(i, localDataType, localIBinder);
  }
  
  public ListSubscriptionsRequest[] zzpn(int paramInt)
  {
    return new ListSubscriptionsRequest[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/request/zzw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */