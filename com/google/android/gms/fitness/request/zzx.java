package com.google.android.gms.fitness.request;

import android.os.RemoteException;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.zzt.zza;
import java.util.HashMap;
import java.util.Map;

public class zzx
  extends zzt.zza
{
  private final OnDataPointListener Vz;
  
  private zzx(OnDataPointListener paramOnDataPointListener)
  {
    this.Vz = ((OnDataPointListener)zzaa.zzy(paramOnDataPointListener));
  }
  
  public void zzf(DataPoint paramDataPoint)
    throws RemoteException
  {
    this.Vz.onDataPoint(paramDataPoint);
  }
  
  public static class zza
  {
    private static final zza VA = new zza();
    private final Map<OnDataPointListener, zzx> VB = new HashMap();
    
    public static zza zzbgc()
    {
      return VA;
    }
    
    public zzx zza(OnDataPointListener paramOnDataPointListener)
    {
      synchronized (this.VB)
      {
        zzx localzzx2 = (zzx)this.VB.get(paramOnDataPointListener);
        zzx localzzx1 = localzzx2;
        if (localzzx2 == null)
        {
          localzzx1 = new zzx(paramOnDataPointListener, null);
          this.VB.put(paramOnDataPointListener, localzzx1);
        }
        return localzzx1;
      }
    }
    
    public zzx zzb(OnDataPointListener paramOnDataPointListener)
    {
      synchronized (this.VB)
      {
        paramOnDataPointListener = (zzx)this.VB.get(paramOnDataPointListener);
        return paramOnDataPointListener;
      }
    }
    
    public zzx zzc(OnDataPointListener paramOnDataPointListener)
    {
      synchronized (this.VB)
      {
        zzx localzzx = (zzx)this.VB.remove(paramOnDataPointListener);
        if (localzzx != null) {
          return localzzx;
        }
        paramOnDataPointListener = new zzx(paramOnDataPointListener, null);
        return paramOnDataPointListener;
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/request/zzx.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */