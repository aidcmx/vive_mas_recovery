package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.ArrayList;

public class zzy
  implements Parcelable.Creator<PurgeDataSourcesRequest>
{
  static void zza(PurgeDataSourcesRequest paramPurgeDataSourcesRequest, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.zzcs(paramParcel);
    zzb.zza(paramParcel, 1, paramPurgeDataSourcesRequest.getCallbackBinder(), false);
    zzb.zzb(paramParcel, 2, paramPurgeDataSourcesRequest.zzbgd(), false);
    zzb.zzc(paramParcel, 1000, paramPurgeDataSourcesRequest.getVersionCode());
    zzb.zzaj(paramParcel, paramInt);
  }
  
  public PurgeDataSourcesRequest zzke(Parcel paramParcel)
  {
    ArrayList localArrayList = null;
    int j = zza.zzcr(paramParcel);
    int i = 0;
    IBinder localIBinder = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.zzcq(paramParcel);
      switch (zza.zzgu(k))
      {
      default: 
        zza.zzb(paramParcel, k);
        break;
      case 1: 
        localIBinder = zza.zzr(paramParcel, k);
        break;
      case 2: 
        localArrayList = zza.zzae(paramParcel, k);
        break;
      case 1000: 
        i = zza.zzg(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new PurgeDataSourcesRequest(i, localIBinder, localArrayList);
  }
  
  public PurgeDataSourcesRequest[] zzpo(int paramInt)
  {
    return new PurgeDataSourcesRequest[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/request/zzy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */