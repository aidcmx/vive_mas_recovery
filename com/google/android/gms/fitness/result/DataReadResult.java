package com.google.android.gms.fitness.result;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;
import com.google.android.gms.fitness.data.Bucket;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataSource.Builder;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.RawBucket;
import com.google.android.gms.fitness.data.RawDataSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class DataReadResult
  extends AbstractSafeParcelable
  implements Result
{
  public static final Parcelable.Creator<DataReadResult> CREATOR = new zzc();
  private final List<DataSet> QN;
  private final List<Bucket> Wj;
  private int Wk;
  private final List<DataSource> Wl;
  private final List<DataType> Wm;
  private final Status hv;
  private final int mVersionCode;
  
  DataReadResult(int paramInt1, List<RawDataSet> paramList, Status paramStatus, List<RawBucket> paramList1, int paramInt2, List<DataSource> paramList2, List<DataType> paramList3)
  {
    this.mVersionCode = paramInt1;
    this.hv = paramStatus;
    this.Wk = paramInt2;
    this.Wl = paramList2;
    this.Wm = paramList3;
    this.QN = new ArrayList(paramList.size());
    paramList = paramList.iterator();
    while (paramList.hasNext())
    {
      paramStatus = (RawDataSet)paramList.next();
      this.QN.add(new DataSet(paramStatus, paramList2));
    }
    this.Wj = new ArrayList(paramList1.size());
    paramList = paramList1.iterator();
    while (paramList.hasNext())
    {
      paramStatus = (RawBucket)paramList.next();
      this.Wj.add(new Bucket(paramStatus, paramList2));
    }
  }
  
  public DataReadResult(List<DataSet> paramList, List<Bucket> paramList1, Status paramStatus)
  {
    this.mVersionCode = 5;
    this.QN = paramList;
    this.hv = paramStatus;
    this.Wj = paramList1;
    this.Wk = 1;
    this.Wl = new ArrayList();
    this.Wm = new ArrayList();
  }
  
  public static DataReadResult zza(Status paramStatus, List<DataType> paramList, List<DataSource> paramList1)
  {
    ArrayList localArrayList = new ArrayList();
    paramList1 = paramList1.iterator();
    while (paramList1.hasNext()) {
      localArrayList.add(DataSet.create((DataSource)paramList1.next()));
    }
    paramList = paramList.iterator();
    while (paramList.hasNext())
    {
      paramList1 = (DataType)paramList.next();
      localArrayList.add(DataSet.create(new DataSource.Builder().setDataType(paramList1).setType(1).setName("Default").build()));
    }
    return new DataReadResult(localArrayList, Collections.emptyList(), paramStatus);
  }
  
  private void zza(Bucket paramBucket, List<Bucket> paramList)
  {
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
    {
      paramList = (Bucket)localIterator.next();
      if (paramList.zzb(paramBucket))
      {
        paramBucket = paramBucket.getDataSets().iterator();
        while (paramBucket.hasNext()) {
          zza((DataSet)paramBucket.next(), paramList.getDataSets());
        }
      }
    }
    this.Wj.add(paramBucket);
  }
  
  private void zza(DataSet paramDataSet, List<DataSet> paramList)
  {
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
    {
      DataSet localDataSet = (DataSet)localIterator.next();
      if (localDataSet.getDataSource().equals(paramDataSet.getDataSource()))
      {
        localDataSet.zzc(paramDataSet.getDataPoints());
        return;
      }
    }
    paramList.add(paramDataSet);
  }
  
  private boolean zzc(DataReadResult paramDataReadResult)
  {
    return (this.hv.equals(paramDataReadResult.hv)) && (zzz.equal(this.QN, paramDataReadResult.QN)) && (zzz.equal(this.Wj, paramDataReadResult.Wj));
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof DataReadResult)) && (zzc((DataReadResult)paramObject)));
  }
  
  public List<Bucket> getBuckets()
  {
    return this.Wj;
  }
  
  public DataSet getDataSet(DataSource paramDataSource)
  {
    Iterator localIterator = this.QN.iterator();
    while (localIterator.hasNext())
    {
      DataSet localDataSet = (DataSet)localIterator.next();
      if (paramDataSource.equals(localDataSet.getDataSource())) {
        return localDataSet;
      }
    }
    return DataSet.create(paramDataSource);
  }
  
  public DataSet getDataSet(DataType paramDataType)
  {
    Iterator localIterator = this.QN.iterator();
    while (localIterator.hasNext())
    {
      DataSet localDataSet = (DataSet)localIterator.next();
      if (paramDataType.equals(localDataSet.getDataType())) {
        return localDataSet;
      }
    }
    return DataSet.create(new DataSource.Builder().setDataType(paramDataType).setType(1).build());
  }
  
  public List<DataSet> getDataSets()
  {
    return this.QN;
  }
  
  public Status getStatus()
  {
    return this.hv;
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.hv, this.QN, this.Wj });
  }
  
  public String toString()
  {
    zzz.zza localzza = zzz.zzx(this).zzg("status", this.hv);
    int i;
    if (this.QN.size() > 5)
    {
      i = this.QN.size();
      localObject = 21 + i + " data sets";
      localzza = localzza.zzg("dataSets", localObject);
      if (this.Wj.size() <= 5) {
        break label131;
      }
      i = this.Wj.size();
    }
    label131:
    for (Object localObject = 19 + i + " buckets";; localObject = this.Wj)
    {
      return localzza.zzg("buckets", localObject).toString();
      localObject = this.QN;
      break;
    }
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzc.zza(this, paramParcel, paramInt);
  }
  
  public void zzb(DataReadResult paramDataReadResult)
  {
    Iterator localIterator = paramDataReadResult.getDataSets().iterator();
    while (localIterator.hasNext()) {
      zza((DataSet)localIterator.next(), this.QN);
    }
    paramDataReadResult = paramDataReadResult.getBuckets().iterator();
    while (paramDataReadResult.hasNext()) {
      zza((Bucket)paramDataReadResult.next(), this.Wj);
    }
  }
  
  List<DataSource> zzbeh()
  {
    return this.Wl;
  }
  
  public int zzbgr()
  {
    return this.Wk;
  }
  
  List<RawBucket> zzbgs()
  {
    ArrayList localArrayList = new ArrayList(this.Wj.size());
    Iterator localIterator = this.Wj.iterator();
    while (localIterator.hasNext()) {
      localArrayList.add(new RawBucket((Bucket)localIterator.next(), this.Wl, this.Wm));
    }
    return localArrayList;
  }
  
  List<RawDataSet> zzbgt()
  {
    ArrayList localArrayList = new ArrayList(this.QN.size());
    Iterator localIterator = this.QN.iterator();
    while (localIterator.hasNext()) {
      localArrayList.add(new RawDataSet((DataSet)localIterator.next(), this.Wl, this.Wm));
    }
    return localArrayList;
  }
  
  List<DataType> zzbgu()
  {
    return this.Wm;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/result/DataReadResult.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */