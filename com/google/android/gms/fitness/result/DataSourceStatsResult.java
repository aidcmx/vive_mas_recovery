package com.google.android.gms.fitness.result;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.fitness.data.DataSource;

public class DataSourceStatsResult
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<DataSourceStatsResult> CREATOR = new zzd();
  public final DataSource Rz;
  public final boolean Wn;
  public final long Wo;
  public final long Wp;
  public final long hx;
  final int mVersionCode;
  
  DataSourceStatsResult(int paramInt, DataSource paramDataSource, long paramLong1, boolean paramBoolean, long paramLong2, long paramLong3)
  {
    this.mVersionCode = paramInt;
    this.Rz = paramDataSource;
    this.hx = paramLong1;
    this.Wn = paramBoolean;
    this.Wo = paramLong2;
    this.Wp = paramLong3;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzd.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/result/DataSourceStatsResult.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */