package com.google.android.gms.fitness.result;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.util.List;
import java.util.Locale;

public class DataStatsResult
  extends AbstractSafeParcelable
  implements Result
{
  public static final Parcelable.Creator<DataStatsResult> CREATOR = new zzf();
  private final List<DataSourceStatsResult> Wr;
  private final Status gy;
  private final int versionCode;
  
  DataStatsResult(int paramInt, Status paramStatus, List<DataSourceStatsResult> paramList)
  {
    this.versionCode = paramInt;
    this.gy = paramStatus;
    this.Wr = paramList;
  }
  
  public Status getStatus()
  {
    return this.gy;
  }
  
  int getVersionCode()
  {
    return this.versionCode;
  }
  
  public String toString()
  {
    return String.format(Locale.getDefault(), "DataStatsResult{%s %d sources}", new Object[] { this.gy, Integer.valueOf(this.Wr.size()) });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzf.zza(this, paramParcel, paramInt);
  }
  
  List<DataSourceStatsResult> zzbgv()
  {
    return this.Wr;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/result/DataStatsResult.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */