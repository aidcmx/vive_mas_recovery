package com.google.android.gms.fitness.result;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;
import com.google.android.gms.fitness.data.DataType;

public class DataTypeResult
  extends AbstractSafeParcelable
  implements Result
{
  public static final Parcelable.Creator<DataTypeResult> CREATOR = new zzg();
  private final DataType RA;
  private final Status hv;
  private final int mVersionCode;
  
  DataTypeResult(int paramInt, Status paramStatus, DataType paramDataType)
  {
    this.mVersionCode = paramInt;
    this.hv = paramStatus;
    this.RA = paramDataType;
  }
  
  public DataTypeResult(Status paramStatus, DataType paramDataType)
  {
    this.mVersionCode = 2;
    this.hv = paramStatus;
    this.RA = paramDataType;
  }
  
  private boolean zzb(DataTypeResult paramDataTypeResult)
  {
    return (this.hv.equals(paramDataTypeResult.hv)) && (zzz.equal(this.RA, paramDataTypeResult.RA));
  }
  
  public static DataTypeResult zzbf(Status paramStatus)
  {
    return new DataTypeResult(paramStatus, null);
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof DataTypeResult)) && (zzb((DataTypeResult)paramObject)));
  }
  
  public DataType getDataType()
  {
    return this.RA;
  }
  
  public Status getStatus()
  {
    return this.hv;
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.hv, this.RA });
  }
  
  public String toString()
  {
    return zzz.zzx(this).zzg("status", this.hv).zzg("dataType", this.RA).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzg.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/result/DataTypeResult.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */