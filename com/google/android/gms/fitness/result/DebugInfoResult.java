package com.google.android.gms.fitness.result;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public class DebugInfoResult
  extends AbstractSafeParcelable
  implements Result
{
  public static final Parcelable.Creator<DebugInfoResult> CREATOR = new zzh();
  private final Bundle Ws;
  private final Status gy;
  private final int versionCode;
  
  DebugInfoResult(int paramInt, Status paramStatus, Bundle paramBundle)
  {
    this.versionCode = paramInt;
    this.gy = paramStatus;
    this.Ws = paramBundle;
  }
  
  public Status getStatus()
  {
    return this.gy;
  }
  
  int getVersionCode()
  {
    return this.versionCode;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzh.zza(this, paramParcel, paramInt);
  }
  
  Bundle zzbgw()
  {
    return this.Ws;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/result/DebugInfoResult.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */