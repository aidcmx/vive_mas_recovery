package com.google.android.gms.fitness.result;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;

public class FileUriResult
  extends AbstractSafeParcelable
  implements Result
{
  public static final Parcelable.Creator<FileUriResult> CREATOR = new zzi();
  private final Status gy;
  private final Uri uri;
  private final int versionCode;
  
  FileUriResult(int paramInt, Uri paramUri, Status paramStatus)
  {
    this.versionCode = paramInt;
    this.uri = paramUri;
    this.gy = paramStatus;
  }
  
  private boolean zzb(FileUriResult paramFileUriResult)
  {
    return (this.gy.equals(paramFileUriResult.gy)) && (zzz.equal(this.uri, paramFileUriResult.uri));
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof FileUriResult)) && (zzb((FileUriResult)paramObject)));
  }
  
  public Status getStatus()
  {
    return this.gy;
  }
  
  public Uri getUri()
  {
    return this.uri;
  }
  
  int getVersionCode()
  {
    return this.versionCode;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.gy, this.uri });
  }
  
  public String toString()
  {
    return zzz.zzx(this).zzg("status", this.gy).zzg("uri", this.uri).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzi.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/result/FileUriResult.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */