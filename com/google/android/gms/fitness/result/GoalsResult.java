package com.google.android.gms.fitness.result;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.fitness.data.Goal;
import java.util.Collections;
import java.util.List;

public class GoalsResult
  extends AbstractSafeParcelable
  implements Result
{
  public static final Parcelable.Creator<GoalsResult> CREATOR = new zzj();
  private final List<Goal> Wt;
  private final Status gy;
  private final int versionCode;
  
  GoalsResult(int paramInt, Status paramStatus, List<Goal> paramList)
  {
    this.versionCode = paramInt;
    this.gy = paramStatus;
    this.Wt = paramList;
  }
  
  public GoalsResult(Status paramStatus, List<Goal> paramList)
  {
    this(1, paramStatus, paramList);
  }
  
  public static GoalsResult zzbg(Status paramStatus)
  {
    return new GoalsResult(paramStatus, Collections.emptyList());
  }
  
  public List<Goal> getGoals()
  {
    return this.Wt;
  }
  
  public Status getStatus()
  {
    return this.gy;
  }
  
  int getVersionCode()
  {
    return this.versionCode;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzj.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/result/GoalsResult.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */