package com.google.android.gms.fitness.result;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.io.Closeable;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class ReadRawResult
  extends AbstractSafeParcelable
  implements Result, Closeable
{
  public static final Parcelable.Creator<ReadRawResult> CREATOR = new zzl();
  private final List<DataHolder> Wv;
  private final int mVersionCode;
  private final DataHolder zy;
  
  ReadRawResult(int paramInt, DataHolder paramDataHolder, List<DataHolder> paramList)
  {
    this.mVersionCode = paramInt;
    this.zy = paramDataHolder;
    Object localObject = paramList;
    if (paramList == null) {
      localObject = Collections.singletonList(paramDataHolder);
    }
    this.Wv = ((List)localObject);
  }
  
  public void close()
  {
    if (this.zy != null) {
      this.zy.close();
    }
    Iterator localIterator = this.Wv.iterator();
    while (localIterator.hasNext()) {
      ((DataHolder)localIterator.next()).close();
    }
  }
  
  public Status getStatus()
  {
    return new Status(this.zy.getStatusCode());
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzl.zza(this, paramParcel, paramInt);
  }
  
  DataHolder zzbbr()
  {
    return this.zy;
  }
  
  public List<DataHolder> zzbgx()
  {
    return this.Wv;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/result/ReadRawResult.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */