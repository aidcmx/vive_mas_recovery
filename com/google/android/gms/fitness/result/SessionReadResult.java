package com.google.android.gms.fitness.result;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Session;
import com.google.android.gms.fitness.data.SessionDataSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class SessionReadResult
  extends AbstractSafeParcelable
  implements Result
{
  public static final Parcelable.Creator<SessionReadResult> CREATOR = new zzm();
  private final List<Session> UT;
  private final List<SessionDataSet> Ww;
  private final Status hv;
  private final int mVersionCode;
  
  SessionReadResult(int paramInt, List<Session> paramList, List<SessionDataSet> paramList1, Status paramStatus)
  {
    this.mVersionCode = paramInt;
    this.UT = paramList;
    this.Ww = Collections.unmodifiableList(paramList1);
    this.hv = paramStatus;
  }
  
  public SessionReadResult(List<Session> paramList, List<SessionDataSet> paramList1, Status paramStatus)
  {
    this.mVersionCode = 3;
    this.UT = paramList;
    this.Ww = Collections.unmodifiableList(paramList1);
    this.hv = paramStatus;
  }
  
  private boolean zzb(SessionReadResult paramSessionReadResult)
  {
    return (this.hv.equals(paramSessionReadResult.hv)) && (zzz.equal(this.UT, paramSessionReadResult.UT)) && (zzz.equal(this.Ww, paramSessionReadResult.Ww));
  }
  
  public static SessionReadResult zzbi(Status paramStatus)
  {
    return new SessionReadResult(new ArrayList(), new ArrayList(), paramStatus);
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof SessionReadResult)) && (zzb((SessionReadResult)paramObject)));
  }
  
  public List<DataSet> getDataSet(Session paramSession)
  {
    zzaa.zzb(this.UT.contains(paramSession), "Attempting to read data for session %s which was not returned", new Object[] { paramSession });
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = this.Ww.iterator();
    while (localIterator.hasNext())
    {
      SessionDataSet localSessionDataSet = (SessionDataSet)localIterator.next();
      if (zzz.equal(paramSession, localSessionDataSet.getSession())) {
        localArrayList.add(localSessionDataSet.getDataSet());
      }
    }
    return localArrayList;
  }
  
  public List<DataSet> getDataSet(Session paramSession, DataType paramDataType)
  {
    zzaa.zzb(this.UT.contains(paramSession), "Attempting to read data for session %s which was not returned", new Object[] { paramSession });
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = this.Ww.iterator();
    while (localIterator.hasNext())
    {
      SessionDataSet localSessionDataSet = (SessionDataSet)localIterator.next();
      if ((zzz.equal(paramSession, localSessionDataSet.getSession())) && (paramDataType.equals(localSessionDataSet.getDataSet().getDataType()))) {
        localArrayList.add(localSessionDataSet.getDataSet());
      }
    }
    return localArrayList;
  }
  
  public List<Session> getSessions()
  {
    return this.UT;
  }
  
  public Status getStatus()
  {
    return this.hv;
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.hv, this.UT, this.Ww });
  }
  
  public String toString()
  {
    return zzz.zzx(this).zzg("status", this.hv).zzg("sessions", this.UT).zzg("sessionDataSets", this.Ww).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzm.zza(this, paramParcel, paramInt);
  }
  
  public List<SessionDataSet> zzbgy()
  {
    return this.Ww;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/result/SessionReadResult.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */