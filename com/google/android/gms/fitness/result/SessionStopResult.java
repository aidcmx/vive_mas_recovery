package com.google.android.gms.fitness.result;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;
import com.google.android.gms.fitness.data.Session;
import java.util.Collections;
import java.util.List;

public class SessionStopResult
  extends AbstractSafeParcelable
  implements Result
{
  public static final Parcelable.Creator<SessionStopResult> CREATOR = new zzn();
  private final List<Session> UT;
  private final Status hv;
  private final int mVersionCode;
  
  SessionStopResult(int paramInt, Status paramStatus, List<Session> paramList)
  {
    this.mVersionCode = paramInt;
    this.hv = paramStatus;
    this.UT = Collections.unmodifiableList(paramList);
  }
  
  public SessionStopResult(Status paramStatus, List<Session> paramList)
  {
    this.mVersionCode = 3;
    this.hv = paramStatus;
    this.UT = Collections.unmodifiableList(paramList);
  }
  
  private boolean zzb(SessionStopResult paramSessionStopResult)
  {
    return (this.hv.equals(paramSessionStopResult.hv)) && (zzz.equal(this.UT, paramSessionStopResult.UT));
  }
  
  public static SessionStopResult zzbj(Status paramStatus)
  {
    return new SessionStopResult(paramStatus, Collections.emptyList());
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof SessionStopResult)) && (zzb((SessionStopResult)paramObject)));
  }
  
  public List<Session> getSessions()
  {
    return this.UT;
  }
  
  public Status getStatus()
  {
    return this.hv;
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.hv, this.UT });
  }
  
  public String toString()
  {
    return zzz.zzx(this).zzg("status", this.hv).zzg("sessions", this.UT).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzn.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/result/SessionStopResult.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */