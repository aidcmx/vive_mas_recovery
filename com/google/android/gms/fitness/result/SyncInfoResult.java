package com.google.android.gms.fitness.result;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;

public class SyncInfoResult
  extends AbstractSafeParcelable
  implements Result
{
  public static final Parcelable.Creator<SyncInfoResult> CREATOR = new zzo();
  private final Boolean Wx;
  private final Status gy;
  private final long timestamp;
  private final int versionCode;
  
  SyncInfoResult(int paramInt, Status paramStatus, long paramLong, Boolean paramBoolean)
  {
    this.versionCode = paramInt;
    this.gy = paramStatus;
    this.timestamp = paramLong;
    this.Wx = paramBoolean;
  }
  
  private boolean zzb(SyncInfoResult paramSyncInfoResult)
  {
    return (this.gy.equals(paramSyncInfoResult.gy)) && (zzz.equal(Long.valueOf(this.timestamp), Long.valueOf(paramSyncInfoResult.timestamp)));
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof SyncInfoResult)) && (zzb((SyncInfoResult)paramObject)));
  }
  
  public Status getStatus()
  {
    return this.gy;
  }
  
  int getVersionCode()
  {
    return this.versionCode;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.gy, Long.valueOf(this.timestamp) });
  }
  
  public String toString()
  {
    return zzz.zzx(this).zzg("status", this.gy).zzg("timestamp", Long.valueOf(this.timestamp)).zzg("syncEnabled", this.Wx).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzo.zza(this, paramParcel, paramInt);
  }
  
  public long zzbgz()
  {
    return this.timestamp;
  }
  
  public Boolean zzbha()
  {
    return this.Wx;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/result/SyncInfoResult.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */