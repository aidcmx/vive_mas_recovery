package com.google.android.gms.fitness.result;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.fitness.data.DataSource;
import java.util.ArrayList;

public class zze
  implements Parcelable.Creator<DataSourcesResult>
{
  static void zza(DataSourcesResult paramDataSourcesResult, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramDataSourcesResult.getDataSources(), false);
    zzb.zza(paramParcel, 2, paramDataSourcesResult.getStatus(), paramInt, false);
    zzb.zzc(paramParcel, 1000, paramDataSourcesResult.getVersionCode());
    zzb.zzaj(paramParcel, i);
  }
  
  public DataSourcesResult zzky(Parcel paramParcel)
  {
    Status localStatus = null;
    int j = zza.zzcr(paramParcel);
    int i = 0;
    ArrayList localArrayList = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.zzcq(paramParcel);
      switch (zza.zzgu(k))
      {
      default: 
        zza.zzb(paramParcel, k);
        break;
      case 1: 
        localArrayList = zza.zzc(paramParcel, k, DataSource.CREATOR);
        break;
      case 2: 
        localStatus = (Status)zza.zza(paramParcel, k, Status.CREATOR);
        break;
      case 1000: 
        i = zza.zzg(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new DataSourcesResult(i, localArrayList, localStatus);
  }
  
  public DataSourcesResult[] zzqi(int paramInt)
  {
    return new DataSourcesResult[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/result/zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */