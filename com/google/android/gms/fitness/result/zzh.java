package com.google.android.gms.fitness.result;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzh
  implements Parcelable.Creator<DebugInfoResult>
{
  static void zza(DebugInfoResult paramDebugInfoResult, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zza(paramParcel, 1, paramDebugInfoResult.getStatus(), paramInt, false);
    zzb.zza(paramParcel, 2, paramDebugInfoResult.zzbgw(), false);
    zzb.zzc(paramParcel, 1000, paramDebugInfoResult.getVersionCode());
    zzb.zzaj(paramParcel, i);
  }
  
  public DebugInfoResult zzlb(Parcel paramParcel)
  {
    Bundle localBundle = null;
    int j = zza.zzcr(paramParcel);
    int i = 0;
    Status localStatus = null;
    if (paramParcel.dataPosition() < j)
    {
      int k = zza.zzcq(paramParcel);
      switch (zza.zzgu(k))
      {
      default: 
        zza.zzb(paramParcel, k);
      }
      for (;;)
      {
        break;
        localStatus = (Status)zza.zza(paramParcel, k, Status.CREATOR);
        continue;
        localBundle = zza.zzs(paramParcel, k);
        continue;
        i = zza.zzg(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new DebugInfoResult(i, localStatus, localBundle);
  }
  
  public DebugInfoResult[] zzql(int paramInt)
  {
    return new DebugInfoResult[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/result/zzh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */