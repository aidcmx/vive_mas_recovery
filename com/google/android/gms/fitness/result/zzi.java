package com.google.android.gms.fitness.result;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzi
  implements Parcelable.Creator<FileUriResult>
{
  static void zza(FileUriResult paramFileUriResult, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zza(paramParcel, 1, paramFileUriResult.getUri(), paramInt, false);
    zzb.zza(paramParcel, 2, paramFileUriResult.getStatus(), paramInt, false);
    zzb.zzc(paramParcel, 1000, paramFileUriResult.getVersionCode());
    zzb.zzaj(paramParcel, i);
  }
  
  public FileUriResult zzlc(Parcel paramParcel)
  {
    Status localStatus = null;
    int j = zza.zzcr(paramParcel);
    int i = 0;
    Uri localUri = null;
    if (paramParcel.dataPosition() < j)
    {
      int k = zza.zzcq(paramParcel);
      switch (zza.zzgu(k))
      {
      default: 
        zza.zzb(paramParcel, k);
      }
      for (;;)
      {
        break;
        localUri = (Uri)zza.zza(paramParcel, k, Uri.CREATOR);
        continue;
        localStatus = (Status)zza.zza(paramParcel, k, Status.CREATOR);
        continue;
        i = zza.zzg(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new FileUriResult(i, localUri, localStatus);
  }
  
  public FileUriResult[] zzqm(int paramInt)
  {
    return new FileUriResult[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/result/zzi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */