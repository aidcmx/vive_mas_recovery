package com.google.android.gms.fitness.result;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.fitness.data.Goal;
import java.util.ArrayList;

public class zzj
  implements Parcelable.Creator<GoalsResult>
{
  static void zza(GoalsResult paramGoalsResult, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zza(paramParcel, 1, paramGoalsResult.getStatus(), paramInt, false);
    zzb.zzc(paramParcel, 2, paramGoalsResult.getGoals(), false);
    zzb.zzc(paramParcel, 1000, paramGoalsResult.getVersionCode());
    zzb.zzaj(paramParcel, i);
  }
  
  public GoalsResult zzld(Parcel paramParcel)
  {
    ArrayList localArrayList = null;
    int j = zza.zzcr(paramParcel);
    int i = 0;
    Status localStatus = null;
    if (paramParcel.dataPosition() < j)
    {
      int k = zza.zzcq(paramParcel);
      switch (zza.zzgu(k))
      {
      default: 
        zza.zzb(paramParcel, k);
      }
      for (;;)
      {
        break;
        localStatus = (Status)zza.zza(paramParcel, k, Status.CREATOR);
        continue;
        localArrayList = zza.zzc(paramParcel, k, Goal.CREATOR);
        continue;
        i = zza.zzg(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new GoalsResult(i, localStatus, localArrayList);
  }
  
  public GoalsResult[] zzqn(int paramInt)
  {
    return new GoalsResult[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/result/zzj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */