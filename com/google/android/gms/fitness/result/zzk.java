package com.google.android.gms.fitness.result;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.fitness.data.Subscription;
import java.util.ArrayList;

public class zzk
  implements Parcelable.Creator<ListSubscriptionsResult>
{
  static void zza(ListSubscriptionsResult paramListSubscriptionsResult, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramListSubscriptionsResult.getSubscriptions(), false);
    zzb.zza(paramParcel, 2, paramListSubscriptionsResult.getStatus(), paramInt, false);
    zzb.zzc(paramParcel, 1000, paramListSubscriptionsResult.getVersionCode());
    zzb.zzaj(paramParcel, i);
  }
  
  public ListSubscriptionsResult zzle(Parcel paramParcel)
  {
    Status localStatus = null;
    int j = zza.zzcr(paramParcel);
    int i = 0;
    ArrayList localArrayList = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.zzcq(paramParcel);
      switch (zza.zzgu(k))
      {
      default: 
        zza.zzb(paramParcel, k);
        break;
      case 1: 
        localArrayList = zza.zzc(paramParcel, k, Subscription.CREATOR);
        break;
      case 2: 
        localStatus = (Status)zza.zza(paramParcel, k, Status.CREATOR);
        break;
      case 1000: 
        i = zza.zzg(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new ListSubscriptionsResult(i, localArrayList, localStatus);
  }
  
  public ListSubscriptionsResult[] zzqo(int paramInt)
  {
    return new ListSubscriptionsResult[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/result/zzk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */