package com.google.android.gms.fitness.service;

import android.annotation.TargetApi;
import android.app.AppOpsManager;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Binder;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.CallSuper;
import android.util.Log;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.util.zzs;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.internal.service.FitnessDataSourcesRequest;
import com.google.android.gms.fitness.internal.service.FitnessUnregistrationRequest;
import com.google.android.gms.fitness.internal.service.zzc.zza;
import com.google.android.gms.fitness.result.DataSourcesResult;
import com.google.android.gms.internal.zzuj;
import com.google.android.gms.internal.zzvb;
import java.util.List;

public abstract class FitnessSensorService
  extends Service
{
  public static final String SERVICE_INTERFACE = "com.google.android.gms.fitness.service.FitnessSensorService";
  private zza Wy;
  
  @CallSuper
  public IBinder onBind(Intent paramIntent)
  {
    if ("com.google.android.gms.fitness.service.FitnessSensorService".equals(paramIntent.getAction()))
    {
      if (Log.isLoggable("FitnessSensorService", 3))
      {
        paramIntent = String.valueOf(paramIntent);
        String str = String.valueOf(getClass().getName());
        Log.d("FitnessSensorService", String.valueOf(paramIntent).length() + 20 + String.valueOf(str).length() + "Intent " + paramIntent + " received by " + str);
      }
      return this.Wy.asBinder();
    }
    return null;
  }
  
  @CallSuper
  public void onCreate()
  {
    super.onCreate();
    this.Wy = new zza(this, null);
  }
  
  public abstract List<DataSource> onFindDataSources(List<DataType> paramList);
  
  public abstract boolean onRegister(FitnessSensorServiceRequest paramFitnessSensorServiceRequest);
  
  public abstract boolean onUnregister(DataSource paramDataSource);
  
  @TargetApi(19)
  protected void zzbhb()
    throws SecurityException
  {
    int i = Binder.getCallingUid();
    if (zzs.zzayu())
    {
      ((AppOpsManager)getSystemService("appops")).checkPackage(i, "com.google.android.gms");
      return;
    }
    String[] arrayOfString = getPackageManager().getPackagesForUid(i);
    if (arrayOfString != null)
    {
      int j = arrayOfString.length;
      i = 0;
      for (;;)
      {
        if (i >= j) {
          break label67;
        }
        if (arrayOfString[i].equals("com.google.android.gms")) {
          break;
        }
        i += 1;
      }
    }
    label67:
    throw new SecurityException("Unauthorized caller");
  }
  
  private static class zza
    extends zzc.zza
  {
    private final FitnessSensorService Wz;
    
    private zza(FitnessSensorService paramFitnessSensorService)
    {
      this.Wz = paramFitnessSensorService;
    }
    
    public void zza(FitnessDataSourcesRequest paramFitnessDataSourcesRequest, zzuj paramzzuj)
      throws RemoteException
    {
      this.Wz.zzbhb();
      paramzzuj.zza(new DataSourcesResult(this.Wz.onFindDataSources(paramFitnessDataSourcesRequest.getDataTypes()), Status.xZ));
    }
    
    public void zza(FitnessUnregistrationRequest paramFitnessUnregistrationRequest, zzvb paramzzvb)
      throws RemoteException
    {
      this.Wz.zzbhb();
      if (this.Wz.onUnregister(paramFitnessUnregistrationRequest.getDataSource()))
      {
        paramzzvb.zzp(Status.xZ);
        return;
      }
      paramzzvb.zzp(new Status(13));
    }
    
    public void zza(FitnessSensorServiceRequest paramFitnessSensorServiceRequest, zzvb paramzzvb)
      throws RemoteException
    {
      this.Wz.zzbhb();
      if (this.Wz.onRegister(paramFitnessSensorServiceRequest))
      {
        paramzzvb.zzp(Status.xZ);
        return;
      }
      paramzzvb.zzp(new Status(13));
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/service/FitnessSensorService.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */