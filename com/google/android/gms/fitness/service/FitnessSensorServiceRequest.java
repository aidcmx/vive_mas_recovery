package com.google.android.gms.fitness.service;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.zzt;
import com.google.android.gms.fitness.data.zzt.zza;
import java.util.concurrent.TimeUnit;

public class FitnessSensorServiceRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<FitnessSensorServiceRequest> CREATOR = new zza();
  public static final int UNSPECIFIED = -1;
  private final DataSource Rz;
  private final zzt VJ;
  private final long WA;
  private final long WB;
  private final int mVersionCode;
  
  FitnessSensorServiceRequest(int paramInt, DataSource paramDataSource, IBinder paramIBinder, long paramLong1, long paramLong2)
  {
    this.mVersionCode = paramInt;
    this.Rz = paramDataSource;
    this.VJ = zzt.zza.zzfg(paramIBinder);
    this.WA = paramLong1;
    this.WB = paramLong2;
  }
  
  private boolean zza(FitnessSensorServiceRequest paramFitnessSensorServiceRequest)
  {
    return (zzz.equal(this.Rz, paramFitnessSensorServiceRequest.Rz)) && (this.WA == paramFitnessSensorServiceRequest.WA) && (this.WB == paramFitnessSensorServiceRequest.WB);
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof FitnessSensorServiceRequest)) && (zza((FitnessSensorServiceRequest)paramObject)));
  }
  
  public long getBatchInterval(TimeUnit paramTimeUnit)
  {
    return paramTimeUnit.convert(this.WB, TimeUnit.MICROSECONDS);
  }
  
  public DataSource getDataSource()
  {
    return this.Rz;
  }
  
  public SensorEventDispatcher getDispatcher()
  {
    return new zzb(this.VJ);
  }
  
  public long getSamplingRate(TimeUnit paramTimeUnit)
  {
    if (this.WA == -1L) {
      return -1L;
    }
    return paramTimeUnit.convert(this.WA, TimeUnit.MICROSECONDS);
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.Rz, Long.valueOf(this.WA), Long.valueOf(this.WB) });
  }
  
  public String toString()
  {
    return String.format("FitnessSensorServiceRequest{%s}", new Object[] { this.Rz });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zza.zza(this, paramParcel, paramInt);
  }
  
  public long zzbfa()
  {
    return this.WA;
  }
  
  IBinder zzbgj()
  {
    return this.VJ.asBinder();
  }
  
  public long zzbhc()
  {
    return this.WB;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/service/FitnessSensorServiceRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */