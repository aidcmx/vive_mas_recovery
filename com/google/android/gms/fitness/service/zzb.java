package com.google.android.gms.fitness.service;

import android.os.RemoteException;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.zzt;
import java.util.Iterator;
import java.util.List;

class zzb
  implements SensorEventDispatcher
{
  private final zzt VJ;
  
  zzb(zzt paramzzt)
  {
    this.VJ = ((zzt)zzaa.zzy(paramzzt));
  }
  
  public void publish(DataPoint paramDataPoint)
    throws RemoteException
  {
    paramDataPoint.zzbee();
    this.VJ.zzf(paramDataPoint);
  }
  
  public void publish(List<DataPoint> paramList)
    throws RemoteException
  {
    paramList = paramList.iterator();
    while (paramList.hasNext()) {
      publish((DataPoint)paramList.next());
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/fitness/service/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */