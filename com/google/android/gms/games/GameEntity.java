package com.google.android.gms.games;

import android.database.CharArrayBuffer;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;
import com.google.android.gms.common.util.zzg;
import com.google.android.gms.games.internal.GamesDowngradeableSafeParcel;

public final class GameEntity
  extends GamesDowngradeableSafeParcel
  implements Game
{
  public static final Parcelable.Creator<GameEntity> CREATOR = new GameEntityCreatorCompat();
  private final String WN;
  private final String WO;
  private final String WP;
  private final Uri WQ;
  private final Uri WR;
  private final Uri WS;
  private final boolean WT;
  private final boolean WU;
  private final String WV;
  private final int WW;
  private final int WX;
  private final int WY;
  private final boolean WZ;
  private final boolean Xa;
  private final String Xb;
  private final String Xc;
  private final String Xd;
  private final boolean Xe;
  private final boolean Xf;
  private final String Xg;
  private final boolean Xh;
  private final String cg;
  private final String jh;
  private final String lU;
  private final int mVersionCode;
  private final boolean zzces;
  
  GameEntity(int paramInt1, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, Uri paramUri1, Uri paramUri2, Uri paramUri3, boolean paramBoolean1, boolean paramBoolean2, String paramString7, int paramInt2, int paramInt3, int paramInt4, boolean paramBoolean3, boolean paramBoolean4, String paramString8, String paramString9, String paramString10, boolean paramBoolean5, boolean paramBoolean6, boolean paramBoolean7, String paramString11, boolean paramBoolean8)
  {
    this.mVersionCode = paramInt1;
    this.lU = paramString1;
    this.jh = paramString2;
    this.WN = paramString3;
    this.WO = paramString4;
    this.cg = paramString5;
    this.WP = paramString6;
    this.WQ = paramUri1;
    this.Xb = paramString8;
    this.WR = paramUri2;
    this.Xc = paramString9;
    this.WS = paramUri3;
    this.Xd = paramString10;
    this.WT = paramBoolean1;
    this.WU = paramBoolean2;
    this.WV = paramString7;
    this.WW = paramInt2;
    this.WX = paramInt3;
    this.WY = paramInt4;
    this.WZ = paramBoolean3;
    this.Xa = paramBoolean4;
    this.zzces = paramBoolean5;
    this.Xe = paramBoolean6;
    this.Xf = paramBoolean7;
    this.Xg = paramString11;
    this.Xh = paramBoolean8;
  }
  
  public GameEntity(Game paramGame)
  {
    this.mVersionCode = 7;
    this.lU = paramGame.getApplicationId();
    this.WN = paramGame.getPrimaryCategory();
    this.WO = paramGame.getSecondaryCategory();
    this.cg = paramGame.getDescription();
    this.WP = paramGame.getDeveloperName();
    this.jh = paramGame.getDisplayName();
    this.WQ = paramGame.getIconImageUri();
    this.Xb = paramGame.getIconImageUrl();
    this.WR = paramGame.getHiResImageUri();
    this.Xc = paramGame.getHiResImageUrl();
    this.WS = paramGame.getFeaturedImageUri();
    this.Xd = paramGame.getFeaturedImageUrl();
    this.WT = paramGame.zzbhj();
    this.WU = paramGame.zzbhl();
    this.WV = paramGame.zzbhm();
    this.WW = 1;
    this.WX = paramGame.getAchievementTotalCount();
    this.WY = paramGame.getLeaderboardCount();
    this.WZ = paramGame.isRealTimeMultiplayerEnabled();
    this.Xa = paramGame.isTurnBasedMultiplayerEnabled();
    this.zzces = paramGame.isMuted();
    this.Xe = paramGame.zzbhk();
    this.Xf = paramGame.areSnapshotsEnabled();
    this.Xg = paramGame.getThemeColor();
    this.Xh = paramGame.hasGamepadSupport();
  }
  
  static int zza(Game paramGame)
  {
    return zzz.hashCode(new Object[] { paramGame.getApplicationId(), paramGame.getDisplayName(), paramGame.getPrimaryCategory(), paramGame.getSecondaryCategory(), paramGame.getDescription(), paramGame.getDeveloperName(), paramGame.getIconImageUri(), paramGame.getHiResImageUri(), paramGame.getFeaturedImageUri(), Boolean.valueOf(paramGame.zzbhj()), Boolean.valueOf(paramGame.zzbhl()), paramGame.zzbhm(), Integer.valueOf(paramGame.getAchievementTotalCount()), Integer.valueOf(paramGame.getLeaderboardCount()), Boolean.valueOf(paramGame.isRealTimeMultiplayerEnabled()), Boolean.valueOf(paramGame.isTurnBasedMultiplayerEnabled()), Boolean.valueOf(paramGame.isMuted()), Boolean.valueOf(paramGame.zzbhk()), Boolean.valueOf(paramGame.areSnapshotsEnabled()), paramGame.getThemeColor(), Boolean.valueOf(paramGame.hasGamepadSupport()) });
  }
  
  static boolean zza(Game paramGame, Object paramObject)
  {
    boolean bool2 = true;
    if (!(paramObject instanceof Game)) {
      bool1 = false;
    }
    do
    {
      return bool1;
      bool1 = bool2;
    } while (paramGame == paramObject);
    paramObject = (Game)paramObject;
    boolean bool3;
    if ((zzz.equal(((Game)paramObject).getApplicationId(), paramGame.getApplicationId())) && (zzz.equal(((Game)paramObject).getDisplayName(), paramGame.getDisplayName())) && (zzz.equal(((Game)paramObject).getPrimaryCategory(), paramGame.getPrimaryCategory())) && (zzz.equal(((Game)paramObject).getSecondaryCategory(), paramGame.getSecondaryCategory())) && (zzz.equal(((Game)paramObject).getDescription(), paramGame.getDescription())) && (zzz.equal(((Game)paramObject).getDeveloperName(), paramGame.getDeveloperName())) && (zzz.equal(((Game)paramObject).getIconImageUri(), paramGame.getIconImageUri())) && (zzz.equal(((Game)paramObject).getHiResImageUri(), paramGame.getHiResImageUri())) && (zzz.equal(((Game)paramObject).getFeaturedImageUri(), paramGame.getFeaturedImageUri())) && (zzz.equal(Boolean.valueOf(((Game)paramObject).zzbhj()), Boolean.valueOf(paramGame.zzbhj()))) && (zzz.equal(Boolean.valueOf(((Game)paramObject).zzbhl()), Boolean.valueOf(paramGame.zzbhl()))) && (zzz.equal(((Game)paramObject).zzbhm(), paramGame.zzbhm())) && (zzz.equal(Integer.valueOf(((Game)paramObject).getAchievementTotalCount()), Integer.valueOf(paramGame.getAchievementTotalCount()))) && (zzz.equal(Integer.valueOf(((Game)paramObject).getLeaderboardCount()), Integer.valueOf(paramGame.getLeaderboardCount()))) && (zzz.equal(Boolean.valueOf(((Game)paramObject).isRealTimeMultiplayerEnabled()), Boolean.valueOf(paramGame.isRealTimeMultiplayerEnabled()))))
    {
      bool3 = ((Game)paramObject).isTurnBasedMultiplayerEnabled();
      if ((!paramGame.isTurnBasedMultiplayerEnabled()) || (!zzz.equal(Boolean.valueOf(((Game)paramObject).isMuted()), Boolean.valueOf(paramGame.isMuted()))) || (!zzz.equal(Boolean.valueOf(((Game)paramObject).zzbhk()), Boolean.valueOf(paramGame.zzbhk())))) {
        break label477;
      }
    }
    label477:
    for (boolean bool1 = true;; bool1 = false)
    {
      if ((zzz.equal(Boolean.valueOf(bool3), Boolean.valueOf(bool1))) && (zzz.equal(Boolean.valueOf(((Game)paramObject).areSnapshotsEnabled()), Boolean.valueOf(paramGame.areSnapshotsEnabled()))) && (zzz.equal(((Game)paramObject).getThemeColor(), paramGame.getThemeColor())))
      {
        bool1 = bool2;
        if (zzz.equal(Boolean.valueOf(((Game)paramObject).hasGamepadSupport()), Boolean.valueOf(paramGame.hasGamepadSupport()))) {
          break;
        }
      }
      return false;
    }
  }
  
  static String zzb(Game paramGame)
  {
    return zzz.zzx(paramGame).zzg("ApplicationId", paramGame.getApplicationId()).zzg("DisplayName", paramGame.getDisplayName()).zzg("PrimaryCategory", paramGame.getPrimaryCategory()).zzg("SecondaryCategory", paramGame.getSecondaryCategory()).zzg("Description", paramGame.getDescription()).zzg("DeveloperName", paramGame.getDeveloperName()).zzg("IconImageUri", paramGame.getIconImageUri()).zzg("IconImageUrl", paramGame.getIconImageUrl()).zzg("HiResImageUri", paramGame.getHiResImageUri()).zzg("HiResImageUrl", paramGame.getHiResImageUrl()).zzg("FeaturedImageUri", paramGame.getFeaturedImageUri()).zzg("FeaturedImageUrl", paramGame.getFeaturedImageUrl()).zzg("PlayEnabledGame", Boolean.valueOf(paramGame.zzbhj())).zzg("InstanceInstalled", Boolean.valueOf(paramGame.zzbhl())).zzg("InstancePackageName", paramGame.zzbhm()).zzg("AchievementTotalCount", Integer.valueOf(paramGame.getAchievementTotalCount())).zzg("LeaderboardCount", Integer.valueOf(paramGame.getLeaderboardCount())).zzg("RealTimeMultiplayerEnabled", Boolean.valueOf(paramGame.isRealTimeMultiplayerEnabled())).zzg("TurnBasedMultiplayerEnabled", Boolean.valueOf(paramGame.isTurnBasedMultiplayerEnabled())).zzg("AreSnapshotsEnabled", Boolean.valueOf(paramGame.areSnapshotsEnabled())).zzg("ThemeColor", paramGame.getThemeColor()).zzg("HasGamepadSupport", Boolean.valueOf(paramGame.hasGamepadSupport())).toString();
  }
  
  public boolean areSnapshotsEnabled()
  {
    return this.Xf;
  }
  
  public boolean equals(Object paramObject)
  {
    return zza(this, paramObject);
  }
  
  public Game freeze()
  {
    return this;
  }
  
  public int getAchievementTotalCount()
  {
    return this.WX;
  }
  
  public String getApplicationId()
  {
    return this.lU;
  }
  
  public String getDescription()
  {
    return this.cg;
  }
  
  public void getDescription(CharArrayBuffer paramCharArrayBuffer)
  {
    zzg.zzb(this.cg, paramCharArrayBuffer);
  }
  
  public String getDeveloperName()
  {
    return this.WP;
  }
  
  public void getDeveloperName(CharArrayBuffer paramCharArrayBuffer)
  {
    zzg.zzb(this.WP, paramCharArrayBuffer);
  }
  
  public String getDisplayName()
  {
    return this.jh;
  }
  
  public void getDisplayName(CharArrayBuffer paramCharArrayBuffer)
  {
    zzg.zzb(this.jh, paramCharArrayBuffer);
  }
  
  public Uri getFeaturedImageUri()
  {
    return this.WS;
  }
  
  public String getFeaturedImageUrl()
  {
    return this.Xd;
  }
  
  public Uri getHiResImageUri()
  {
    return this.WR;
  }
  
  public String getHiResImageUrl()
  {
    return this.Xc;
  }
  
  public Uri getIconImageUri()
  {
    return this.WQ;
  }
  
  public String getIconImageUrl()
  {
    return this.Xb;
  }
  
  public int getLeaderboardCount()
  {
    return this.WY;
  }
  
  public String getPrimaryCategory()
  {
    return this.WN;
  }
  
  public String getSecondaryCategory()
  {
    return this.WO;
  }
  
  public String getThemeColor()
  {
    return this.Xg;
  }
  
  public int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public boolean hasGamepadSupport()
  {
    return this.Xh;
  }
  
  public int hashCode()
  {
    return zza(this);
  }
  
  public boolean isDataValid()
  {
    return true;
  }
  
  public boolean isMuted()
  {
    return this.zzces;
  }
  
  public boolean isRealTimeMultiplayerEnabled()
  {
    return this.WZ;
  }
  
  public boolean isTurnBasedMultiplayerEnabled()
  {
    return this.Xa;
  }
  
  public String toString()
  {
    return zzb(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    int i = 1;
    Object localObject2 = null;
    if (!zzawa())
    {
      GameEntityCreator.zza(this, paramParcel, paramInt);
      return;
    }
    paramParcel.writeString(this.lU);
    paramParcel.writeString(this.jh);
    paramParcel.writeString(this.WN);
    paramParcel.writeString(this.WO);
    paramParcel.writeString(this.cg);
    paramParcel.writeString(this.WP);
    Object localObject1;
    if (this.WQ == null)
    {
      localObject1 = null;
      paramParcel.writeString((String)localObject1);
      if (this.WR != null) {
        break label189;
      }
      localObject1 = null;
      label93:
      paramParcel.writeString((String)localObject1);
      if (this.WS != null) {
        break label201;
      }
      localObject1 = localObject2;
      label110:
      paramParcel.writeString((String)localObject1);
      if (!this.WT) {
        break label213;
      }
      paramInt = 1;
      label125:
      paramParcel.writeInt(paramInt);
      if (!this.WU) {
        break label218;
      }
    }
    label189:
    label201:
    label213:
    label218:
    for (paramInt = i;; paramInt = 0)
    {
      paramParcel.writeInt(paramInt);
      paramParcel.writeString(this.WV);
      paramParcel.writeInt(this.WW);
      paramParcel.writeInt(this.WX);
      paramParcel.writeInt(this.WY);
      return;
      localObject1 = this.WQ.toString();
      break;
      localObject1 = this.WR.toString();
      break label93;
      localObject1 = this.WS.toString();
      break label110;
      paramInt = 0;
      break label125;
    }
  }
  
  public boolean zzbhj()
  {
    return this.WT;
  }
  
  public boolean zzbhk()
  {
    return this.Xe;
  }
  
  public boolean zzbhl()
  {
    return this.WU;
  }
  
  public String zzbhm()
  {
    return this.WV;
  }
  
  public int zzbhn()
  {
    return this.WW;
  }
  
  static final class GameEntityCreatorCompat
    extends GameEntityCreator
  {
    public GameEntity zzlk(Parcel paramParcel)
    {
      if ((GameEntity.zze(GameEntity.zzbho())) || (GameEntity.zzjv(GameEntity.class.getCanonicalName()))) {
        return super.zzlk(paramParcel);
      }
      String str1 = paramParcel.readString();
      String str2 = paramParcel.readString();
      String str3 = paramParcel.readString();
      String str4 = paramParcel.readString();
      String str5 = paramParcel.readString();
      String str6 = paramParcel.readString();
      Object localObject1 = paramParcel.readString();
      Object localObject2;
      label90:
      Object localObject3;
      label104:
      boolean bool1;
      if (localObject1 == null)
      {
        localObject1 = null;
        localObject2 = paramParcel.readString();
        if (localObject2 != null) {
          break label188;
        }
        localObject2 = null;
        localObject3 = paramParcel.readString();
        if (localObject3 != null) {
          break label198;
        }
        localObject3 = null;
        if (paramParcel.readInt() <= 0) {
          break label208;
        }
        bool1 = true;
        label113:
        if (paramParcel.readInt() <= 0) {
          break label213;
        }
      }
      label188:
      label198:
      label208:
      label213:
      for (boolean bool2 = true;; bool2 = false)
      {
        return new GameEntity(7, str1, str2, str3, str4, str5, str6, (Uri)localObject1, (Uri)localObject2, (Uri)localObject3, bool1, bool2, paramParcel.readString(), paramParcel.readInt(), paramParcel.readInt(), paramParcel.readInt(), false, false, null, null, null, false, false, false, null, false);
        localObject1 = Uri.parse((String)localObject1);
        break;
        localObject2 = Uri.parse((String)localObject2);
        break label90;
        localObject3 = Uri.parse((String)localObject3);
        break label104;
        bool1 = false;
        break label113;
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/GameEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */