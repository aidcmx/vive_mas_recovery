package com.google.android.gms.games;

import android.database.CharArrayBuffer;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.zzc;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;
import com.google.android.gms.common.util.zzg;
import com.google.android.gms.games.internal.GamesDowngradeableSafeParcel;
import com.google.android.gms.games.internal.player.MostRecentGameInfo;
import com.google.android.gms.games.internal.player.MostRecentGameInfoEntity;

public final class PlayerEntity
  extends GamesDowngradeableSafeParcel
  implements Player
{
  public static final Parcelable.Creator<PlayerEntity> CREATOR = new PlayerEntityCreatorCompat();
  private final boolean C;
  private final String JB;
  private final Uri WQ;
  private final Uri WR;
  private final long XA;
  private final int XB;
  private final long XC;
  private final MostRecentGameInfoEntity XD;
  private final PlayerLevelInfo XE;
  private final boolean XF;
  private final boolean XG;
  private final String XH;
  private final Uri XI;
  private final String XJ;
  private final Uri XK;
  private final String XL;
  private final int XM;
  private final long XN;
  private final String Xb;
  private final String Xc;
  private String jh;
  private final String mName;
  private final int mVersionCode;
  private String uh;
  
  PlayerEntity(int paramInt1, String paramString1, String paramString2, Uri paramUri1, Uri paramUri2, long paramLong1, int paramInt2, long paramLong2, String paramString3, String paramString4, String paramString5, MostRecentGameInfoEntity paramMostRecentGameInfoEntity, PlayerLevelInfo paramPlayerLevelInfo, boolean paramBoolean1, boolean paramBoolean2, String paramString6, String paramString7, Uri paramUri3, String paramString8, Uri paramUri4, String paramString9, int paramInt3, long paramLong3, boolean paramBoolean3)
  {
    this.mVersionCode = paramInt1;
    this.uh = paramString1;
    this.jh = paramString2;
    this.WQ = paramUri1;
    this.Xb = paramString3;
    this.WR = paramUri2;
    this.Xc = paramString4;
    this.XA = paramLong1;
    this.XB = paramInt2;
    this.XC = paramLong2;
    this.JB = paramString5;
    this.XF = paramBoolean1;
    this.XD = paramMostRecentGameInfoEntity;
    this.XE = paramPlayerLevelInfo;
    this.XG = paramBoolean2;
    this.XH = paramString6;
    this.mName = paramString7;
    this.XI = paramUri3;
    this.XJ = paramString8;
    this.XK = paramUri4;
    this.XL = paramString9;
    this.XM = paramInt3;
    this.XN = paramLong3;
    this.C = paramBoolean3;
  }
  
  public PlayerEntity(Player paramPlayer)
  {
    this(paramPlayer, true);
  }
  
  public PlayerEntity(Player paramPlayer, boolean paramBoolean)
  {
    this.mVersionCode = 14;
    Object localObject1;
    if (paramBoolean)
    {
      localObject1 = paramPlayer.getPlayerId();
      this.uh = ((String)localObject1);
      this.jh = paramPlayer.getDisplayName();
      this.WQ = paramPlayer.getIconImageUri();
      this.Xb = paramPlayer.getIconImageUrl();
      this.WR = paramPlayer.getHiResImageUri();
      this.Xc = paramPlayer.getHiResImageUrl();
      this.XA = paramPlayer.getRetrievedTimestamp();
      this.XB = paramPlayer.zzbhs();
      this.XC = paramPlayer.getLastPlayedWithTimestamp();
      this.JB = paramPlayer.getTitle();
      this.XF = paramPlayer.zzbht();
      localObject1 = paramPlayer.zzbhu();
      if (localObject1 != null) {
        break label297;
      }
      localObject1 = localObject2;
      label143:
      this.XD = ((MostRecentGameInfoEntity)localObject1);
      this.XE = paramPlayer.getLevelInfo();
      this.XG = paramPlayer.zzbhr();
      this.XH = paramPlayer.zzbhq();
      this.mName = paramPlayer.getName();
      this.XI = paramPlayer.getBannerImageLandscapeUri();
      this.XJ = paramPlayer.getBannerImageLandscapeUrl();
      this.XK = paramPlayer.getBannerImagePortraitUri();
      this.XL = paramPlayer.getBannerImagePortraitUrl();
      this.XM = paramPlayer.zzbhv();
      this.XN = paramPlayer.zzbhw();
      this.C = paramPlayer.isMuted();
      if (paramBoolean) {
        zzc.zzu(this.uh);
      }
      zzc.zzu(this.jh);
      if (this.XA <= 0L) {
        break label309;
      }
    }
    label297:
    label309:
    for (paramBoolean = true;; paramBoolean = false)
    {
      zzc.zzbs(paramBoolean);
      return;
      localObject1 = null;
      break;
      localObject1 = new MostRecentGameInfoEntity((MostRecentGameInfo)localObject1);
      break label143;
    }
  }
  
  static boolean zza(Player paramPlayer, Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if (!(paramObject instanceof Player)) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramPlayer == paramObject);
      paramObject = (Player)paramObject;
      if ((!zzz.equal(((Player)paramObject).getPlayerId(), paramPlayer.getPlayerId())) || (!zzz.equal(((Player)paramObject).getDisplayName(), paramPlayer.getDisplayName())) || (!zzz.equal(Boolean.valueOf(((Player)paramObject).zzbhr()), Boolean.valueOf(paramPlayer.zzbhr()))) || (!zzz.equal(((Player)paramObject).getIconImageUri(), paramPlayer.getIconImageUri())) || (!zzz.equal(((Player)paramObject).getHiResImageUri(), paramPlayer.getHiResImageUri())) || (!zzz.equal(Long.valueOf(((Player)paramObject).getRetrievedTimestamp()), Long.valueOf(paramPlayer.getRetrievedTimestamp()))) || (!zzz.equal(((Player)paramObject).getTitle(), paramPlayer.getTitle())) || (!zzz.equal(((Player)paramObject).getLevelInfo(), paramPlayer.getLevelInfo())) || (!zzz.equal(((Player)paramObject).zzbhq(), paramPlayer.zzbhq())) || (!zzz.equal(((Player)paramObject).getName(), paramPlayer.getName())) || (!zzz.equal(((Player)paramObject).getBannerImageLandscapeUri(), paramPlayer.getBannerImageLandscapeUri())) || (!zzz.equal(((Player)paramObject).getBannerImagePortraitUri(), paramPlayer.getBannerImagePortraitUri())) || (!zzz.equal(Integer.valueOf(((Player)paramObject).zzbhv()), Integer.valueOf(paramPlayer.zzbhv()))) || (!zzz.equal(Long.valueOf(((Player)paramObject).zzbhw()), Long.valueOf(paramPlayer.zzbhw())))) {
        break;
      }
      bool1 = bool2;
    } while (zzz.equal(Boolean.valueOf(((Player)paramObject).isMuted()), Boolean.valueOf(paramPlayer.isMuted())));
    return false;
  }
  
  static int zzb(Player paramPlayer)
  {
    return zzz.hashCode(new Object[] { paramPlayer.getPlayerId(), paramPlayer.getDisplayName(), Boolean.valueOf(paramPlayer.zzbhr()), paramPlayer.getIconImageUri(), paramPlayer.getHiResImageUri(), Long.valueOf(paramPlayer.getRetrievedTimestamp()), paramPlayer.getTitle(), paramPlayer.getLevelInfo(), paramPlayer.zzbhq(), paramPlayer.getName(), paramPlayer.getBannerImageLandscapeUri(), paramPlayer.getBannerImagePortraitUri(), Integer.valueOf(paramPlayer.zzbhv()), Long.valueOf(paramPlayer.zzbhw()), Boolean.valueOf(paramPlayer.isMuted()) });
  }
  
  static String zzc(Player paramPlayer)
  {
    return zzz.zzx(paramPlayer).zzg("PlayerId", paramPlayer.getPlayerId()).zzg("DisplayName", paramPlayer.getDisplayName()).zzg("HasDebugAccess", Boolean.valueOf(paramPlayer.zzbhr())).zzg("IconImageUri", paramPlayer.getIconImageUri()).zzg("IconImageUrl", paramPlayer.getIconImageUrl()).zzg("HiResImageUri", paramPlayer.getHiResImageUri()).zzg("HiResImageUrl", paramPlayer.getHiResImageUrl()).zzg("RetrievedTimestamp", Long.valueOf(paramPlayer.getRetrievedTimestamp())).zzg("Title", paramPlayer.getTitle()).zzg("LevelInfo", paramPlayer.getLevelInfo()).zzg("GamerTag", paramPlayer.zzbhq()).zzg("Name", paramPlayer.getName()).zzg("BannerImageLandscapeUri", paramPlayer.getBannerImageLandscapeUri()).zzg("BannerImageLandscapeUrl", paramPlayer.getBannerImageLandscapeUrl()).zzg("BannerImagePortraitUri", paramPlayer.getBannerImagePortraitUri()).zzg("BannerImagePortraitUrl", paramPlayer.getBannerImagePortraitUrl()).zzg("GamerFriendStatus", Integer.valueOf(paramPlayer.zzbhv())).zzg("GamerFriendUpdateTimestamp", Long.valueOf(paramPlayer.zzbhw())).zzg("IsMuted", Boolean.valueOf(paramPlayer.isMuted())).toString();
  }
  
  public boolean equals(Object paramObject)
  {
    return zza(this, paramObject);
  }
  
  public Player freeze()
  {
    return this;
  }
  
  public Uri getBannerImageLandscapeUri()
  {
    return this.XI;
  }
  
  public String getBannerImageLandscapeUrl()
  {
    return this.XJ;
  }
  
  public Uri getBannerImagePortraitUri()
  {
    return this.XK;
  }
  
  public String getBannerImagePortraitUrl()
  {
    return this.XL;
  }
  
  public String getDisplayName()
  {
    return this.jh;
  }
  
  public void getDisplayName(CharArrayBuffer paramCharArrayBuffer)
  {
    zzg.zzb(this.jh, paramCharArrayBuffer);
  }
  
  public Uri getHiResImageUri()
  {
    return this.WR;
  }
  
  public String getHiResImageUrl()
  {
    return this.Xc;
  }
  
  public Uri getIconImageUri()
  {
    return this.WQ;
  }
  
  public String getIconImageUrl()
  {
    return this.Xb;
  }
  
  public long getLastPlayedWithTimestamp()
  {
    return this.XC;
  }
  
  public PlayerLevelInfo getLevelInfo()
  {
    return this.XE;
  }
  
  public String getName()
  {
    return this.mName;
  }
  
  public String getPlayerId()
  {
    return this.uh;
  }
  
  public long getRetrievedTimestamp()
  {
    return this.XA;
  }
  
  public String getTitle()
  {
    return this.JB;
  }
  
  public void getTitle(CharArrayBuffer paramCharArrayBuffer)
  {
    zzg.zzb(this.JB, paramCharArrayBuffer);
  }
  
  public int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public boolean hasHiResImage()
  {
    return getHiResImageUri() != null;
  }
  
  public boolean hasIconImage()
  {
    return getIconImageUri() != null;
  }
  
  public int hashCode()
  {
    return zzb(this);
  }
  
  public boolean isDataValid()
  {
    return true;
  }
  
  public boolean isMuted()
  {
    return this.C;
  }
  
  public String toString()
  {
    return zzc(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    Object localObject2 = null;
    if (!zzawa())
    {
      PlayerEntityCreator.zza(this, paramParcel, paramInt);
      return;
    }
    paramParcel.writeString(this.uh);
    paramParcel.writeString(this.jh);
    if (this.WQ == null)
    {
      localObject1 = null;
      paramParcel.writeString((String)localObject1);
      if (this.WR != null) {
        break label82;
      }
    }
    label82:
    for (Object localObject1 = localObject2;; localObject1 = this.WR.toString())
    {
      paramParcel.writeString((String)localObject1);
      paramParcel.writeLong(this.XA);
      return;
      localObject1 = this.WQ.toString();
      break;
    }
  }
  
  public String zzbhq()
  {
    return this.XH;
  }
  
  public boolean zzbhr()
  {
    return this.XG;
  }
  
  public int zzbhs()
  {
    return this.XB;
  }
  
  public boolean zzbht()
  {
    return this.XF;
  }
  
  public MostRecentGameInfo zzbhu()
  {
    return this.XD;
  }
  
  public int zzbhv()
  {
    return this.XM;
  }
  
  public long zzbhw()
  {
    return this.XN;
  }
  
  static final class PlayerEntityCreatorCompat
    extends PlayerEntityCreator
  {
    public PlayerEntity zzll(Parcel paramParcel)
    {
      if ((PlayerEntity.zze(PlayerEntity.zzbho())) || (PlayerEntity.zzjv(PlayerEntity.class.getCanonicalName()))) {
        return super.zzll(paramParcel);
      }
      String str1 = paramParcel.readString();
      String str2 = paramParcel.readString();
      Object localObject1 = paramParcel.readString();
      Object localObject2 = paramParcel.readString();
      if (localObject1 == null)
      {
        localObject1 = null;
        if (localObject2 != null) {
          break label116;
        }
      }
      label116:
      for (localObject2 = null;; localObject2 = Uri.parse((String)localObject2))
      {
        return new PlayerEntity(14, str1, str2, (Uri)localObject1, (Uri)localObject2, paramParcel.readLong(), -1, -1L, null, null, null, null, null, true, false, paramParcel.readString(), paramParcel.readString(), null, null, null, null, -1, -1L, false);
        localObject1 = Uri.parse((String)localObject1);
        break;
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/PlayerEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */