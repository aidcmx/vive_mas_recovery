package com.google.android.gms.games;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.games.internal.player.MostRecentGameInfoEntity;

public class PlayerEntityCreator
  implements Parcelable.Creator<PlayerEntity>
{
  static void zza(PlayerEntity paramPlayerEntity, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zza(paramParcel, 1, paramPlayerEntity.getPlayerId(), false);
    zzb.zza(paramParcel, 2, paramPlayerEntity.getDisplayName(), false);
    zzb.zza(paramParcel, 3, paramPlayerEntity.getIconImageUri(), paramInt, false);
    zzb.zza(paramParcel, 4, paramPlayerEntity.getHiResImageUri(), paramInt, false);
    zzb.zza(paramParcel, 5, paramPlayerEntity.getRetrievedTimestamp());
    zzb.zzc(paramParcel, 6, paramPlayerEntity.zzbhs());
    zzb.zza(paramParcel, 7, paramPlayerEntity.getLastPlayedWithTimestamp());
    zzb.zzc(paramParcel, 1000, paramPlayerEntity.getVersionCode());
    zzb.zza(paramParcel, 8, paramPlayerEntity.getIconImageUrl(), false);
    zzb.zza(paramParcel, 9, paramPlayerEntity.getHiResImageUrl(), false);
    zzb.zza(paramParcel, 14, paramPlayerEntity.getTitle(), false);
    zzb.zza(paramParcel, 15, paramPlayerEntity.zzbhu(), paramInt, false);
    zzb.zza(paramParcel, 16, paramPlayerEntity.getLevelInfo(), paramInt, false);
    zzb.zza(paramParcel, 18, paramPlayerEntity.zzbht());
    zzb.zza(paramParcel, 19, paramPlayerEntity.zzbhr());
    zzb.zza(paramParcel, 20, paramPlayerEntity.zzbhq(), false);
    zzb.zza(paramParcel, 21, paramPlayerEntity.getName(), false);
    zzb.zza(paramParcel, 22, paramPlayerEntity.getBannerImageLandscapeUri(), paramInt, false);
    zzb.zza(paramParcel, 23, paramPlayerEntity.getBannerImageLandscapeUrl(), false);
    zzb.zza(paramParcel, 24, paramPlayerEntity.getBannerImagePortraitUri(), paramInt, false);
    zzb.zza(paramParcel, 25, paramPlayerEntity.getBannerImagePortraitUrl(), false);
    zzb.zzc(paramParcel, 26, paramPlayerEntity.zzbhv());
    zzb.zza(paramParcel, 27, paramPlayerEntity.zzbhw());
    zzb.zza(paramParcel, 28, paramPlayerEntity.isMuted());
    zzb.zzaj(paramParcel, i);
  }
  
  public PlayerEntity zzll(Parcel paramParcel)
  {
    int m = zza.zzcr(paramParcel);
    int k = 0;
    String str9 = null;
    String str8 = null;
    Uri localUri4 = null;
    Uri localUri3 = null;
    long l3 = 0L;
    int j = 0;
    long l2 = 0L;
    String str7 = null;
    String str6 = null;
    String str5 = null;
    MostRecentGameInfoEntity localMostRecentGameInfoEntity = null;
    PlayerLevelInfo localPlayerLevelInfo = null;
    boolean bool3 = false;
    boolean bool2 = false;
    String str4 = null;
    String str3 = null;
    Uri localUri2 = null;
    String str2 = null;
    Uri localUri1 = null;
    String str1 = null;
    int i = 0;
    long l1 = 0L;
    boolean bool1 = false;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.zzcq(paramParcel);
      switch (zza.zzgu(n))
      {
      default: 
        zza.zzb(paramParcel, n);
        break;
      case 1: 
        str9 = zza.zzq(paramParcel, n);
        break;
      case 2: 
        str8 = zza.zzq(paramParcel, n);
        break;
      case 3: 
        localUri4 = (Uri)zza.zza(paramParcel, n, Uri.CREATOR);
        break;
      case 4: 
        localUri3 = (Uri)zza.zza(paramParcel, n, Uri.CREATOR);
        break;
      case 5: 
        l3 = zza.zzi(paramParcel, n);
        break;
      case 6: 
        j = zza.zzg(paramParcel, n);
        break;
      case 7: 
        l2 = zza.zzi(paramParcel, n);
        break;
      case 1000: 
        k = zza.zzg(paramParcel, n);
        break;
      case 8: 
        str7 = zza.zzq(paramParcel, n);
        break;
      case 9: 
        str6 = zza.zzq(paramParcel, n);
        break;
      case 14: 
        str5 = zza.zzq(paramParcel, n);
        break;
      case 15: 
        localMostRecentGameInfoEntity = (MostRecentGameInfoEntity)zza.zza(paramParcel, n, MostRecentGameInfoEntity.CREATOR);
        break;
      case 16: 
        localPlayerLevelInfo = (PlayerLevelInfo)zza.zza(paramParcel, n, PlayerLevelInfo.CREATOR);
        break;
      case 18: 
        bool3 = zza.zzc(paramParcel, n);
        break;
      case 19: 
        bool2 = zza.zzc(paramParcel, n);
        break;
      case 20: 
        str4 = zza.zzq(paramParcel, n);
        break;
      case 21: 
        str3 = zza.zzq(paramParcel, n);
        break;
      case 22: 
        localUri2 = (Uri)zza.zza(paramParcel, n, Uri.CREATOR);
        break;
      case 23: 
        str2 = zza.zzq(paramParcel, n);
        break;
      case 24: 
        localUri1 = (Uri)zza.zza(paramParcel, n, Uri.CREATOR);
        break;
      case 25: 
        str1 = zza.zzq(paramParcel, n);
        break;
      case 26: 
        i = zza.zzg(paramParcel, n);
        break;
      case 27: 
        l1 = zza.zzi(paramParcel, n);
        break;
      case 28: 
        bool1 = zza.zzc(paramParcel, n);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza(37 + "Overread allowed size end=" + m, paramParcel);
    }
    return new PlayerEntity(k, str9, str8, localUri4, localUri3, l3, j, l2, str7, str6, str5, localMostRecentGameInfoEntity, localPlayerLevelInfo, bool3, bool2, str4, str3, localUri2, str2, localUri1, str1, i, l1, bool1);
  }
  
  public PlayerEntity[] zzqw(int paramInt)
  {
    return new PlayerEntity[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/PlayerEntityCreator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */