package com.google.android.gms.games;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zzz;

public final class PlayerLevelInfo
  implements SafeParcelable
{
  public static final Parcelable.Creator<PlayerLevelInfo> CREATOR = new PlayerLevelInfoCreator();
  private final long XR;
  private final long XS;
  private final PlayerLevel XT;
  private final PlayerLevel XU;
  private final int mVersionCode;
  
  PlayerLevelInfo(int paramInt, long paramLong1, long paramLong2, PlayerLevel paramPlayerLevel1, PlayerLevel paramPlayerLevel2)
  {
    if (paramLong1 != -1L) {}
    for (boolean bool = true;; bool = false)
    {
      zzaa.zzbs(bool);
      zzaa.zzy(paramPlayerLevel1);
      zzaa.zzy(paramPlayerLevel2);
      this.mVersionCode = paramInt;
      this.XR = paramLong1;
      this.XS = paramLong2;
      this.XT = paramPlayerLevel1;
      this.XU = paramPlayerLevel2;
      return;
    }
  }
  
  public PlayerLevelInfo(long paramLong1, long paramLong2, PlayerLevel paramPlayerLevel1, PlayerLevel paramPlayerLevel2)
  {
    this(1, paramLong1, paramLong2, paramPlayerLevel1, paramPlayerLevel2);
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if (!(paramObject instanceof PlayerLevelInfo)) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramObject == this);
      paramObject = (PlayerLevelInfo)paramObject;
      if ((!zzz.equal(Long.valueOf(this.XR), Long.valueOf(((PlayerLevelInfo)paramObject).XR))) || (!zzz.equal(Long.valueOf(this.XS), Long.valueOf(((PlayerLevelInfo)paramObject).XS))) || (!zzz.equal(this.XT, ((PlayerLevelInfo)paramObject).XT))) {
        break;
      }
      bool1 = bool2;
    } while (zzz.equal(this.XU, ((PlayerLevelInfo)paramObject).XU));
    return false;
  }
  
  public PlayerLevel getCurrentLevel()
  {
    return this.XT;
  }
  
  public long getCurrentXpTotal()
  {
    return this.XR;
  }
  
  public long getLastLevelUpTimestamp()
  {
    return this.XS;
  }
  
  public PlayerLevel getNextLevel()
  {
    return this.XU;
  }
  
  public int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { Long.valueOf(this.XR), Long.valueOf(this.XS), this.XT, this.XU });
  }
  
  public boolean isMaxLevel()
  {
    return this.XT.equals(this.XU);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    PlayerLevelInfoCreator.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/PlayerLevelInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */