package com.google.android.gms.games;

import android.database.CharArrayBuffer;
import android.net.Uri;
import android.os.Parcel;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.zzc;
import com.google.android.gms.games.internal.player.MostRecentGameInfo;
import com.google.android.gms.games.internal.player.MostRecentGameInfoRef;
import com.google.android.gms.games.internal.player.PlayerColumnNames;

public final class PlayerRef
  extends zzc
  implements Player
{
  private final PlayerLevelInfo XE;
  private final PlayerColumnNames XV;
  private final MostRecentGameInfoRef XW;
  
  public PlayerRef(DataHolder paramDataHolder, int paramInt)
  {
    this(paramDataHolder, paramInt, null);
  }
  
  public PlayerRef(DataHolder paramDataHolder, int paramInt, String paramString)
  {
    super(paramDataHolder, paramInt);
    this.XV = new PlayerColumnNames(paramString);
    this.XW = new MostRecentGameInfoRef(paramDataHolder, paramInt, this.XV);
    int i;
    if (zzbhx())
    {
      paramInt = getInteger(this.XV.ads);
      i = getInteger(this.XV.adv);
      paramString = new PlayerLevel(paramInt, getLong(this.XV.adt), getLong(this.XV.adu));
      if (paramInt == i) {
        break label178;
      }
    }
    label178:
    for (paramDataHolder = new PlayerLevel(i, getLong(this.XV.adu), getLong(this.XV.adw));; paramDataHolder = paramString)
    {
      this.XE = new PlayerLevelInfo(getLong(this.XV.adr), getLong(this.XV.adx), paramString, paramDataHolder);
      return;
      this.XE = null;
      return;
    }
  }
  
  private boolean zzbhx()
  {
    if (zzhq(this.XV.adr)) {}
    while (getLong(this.XV.adr) == -1L) {
      return false;
    }
    return true;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    return PlayerEntity.zza(this, paramObject);
  }
  
  public Player freeze()
  {
    return new PlayerEntity(this);
  }
  
  public Uri getBannerImageLandscapeUri()
  {
    return zzhp(this.XV.adI);
  }
  
  public String getBannerImageLandscapeUrl()
  {
    return getString(this.XV.adJ);
  }
  
  public Uri getBannerImagePortraitUri()
  {
    return zzhp(this.XV.adK);
  }
  
  public String getBannerImagePortraitUrl()
  {
    return getString(this.XV.adL);
  }
  
  public String getDisplayName()
  {
    return getString(this.XV.adj);
  }
  
  public void getDisplayName(CharArrayBuffer paramCharArrayBuffer)
  {
    zza(this.XV.adj, paramCharArrayBuffer);
  }
  
  public Uri getHiResImageUri()
  {
    return zzhp(this.XV.adm);
  }
  
  public String getHiResImageUrl()
  {
    return getString(this.XV.adn);
  }
  
  public Uri getIconImageUri()
  {
    return zzhp(this.XV.adk);
  }
  
  public String getIconImageUrl()
  {
    return getString(this.XV.adl);
  }
  
  public long getLastPlayedWithTimestamp()
  {
    if ((!zzho(this.XV.adq)) || (zzhq(this.XV.adq))) {
      return -1L;
    }
    return getLong(this.XV.adq);
  }
  
  public PlayerLevelInfo getLevelInfo()
  {
    return this.XE;
  }
  
  public String getName()
  {
    return getString(this.XV.name);
  }
  
  public String getPlayerId()
  {
    return getString(this.XV.adi);
  }
  
  public long getRetrievedTimestamp()
  {
    return getLong(this.XV.ado);
  }
  
  public String getTitle()
  {
    return getString(this.XV.title);
  }
  
  public void getTitle(CharArrayBuffer paramCharArrayBuffer)
  {
    zza(this.XV.title, paramCharArrayBuffer);
  }
  
  public boolean hasHiResImage()
  {
    return getHiResImageUri() != null;
  }
  
  public boolean hasIconImage()
  {
    return getIconImageUri() != null;
  }
  
  public int hashCode()
  {
    return PlayerEntity.zzb(this);
  }
  
  public boolean isMuted()
  {
    return getBoolean(this.XV.adO);
  }
  
  public String toString()
  {
    return PlayerEntity.zzc(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    ((PlayerEntity)freeze()).writeToParcel(paramParcel, paramInt);
  }
  
  public String zzbhq()
  {
    return getString(this.XV.adH);
  }
  
  public boolean zzbhr()
  {
    return getBoolean(this.XV.adG);
  }
  
  public int zzbhs()
  {
    return getInteger(this.XV.adp);
  }
  
  public boolean zzbht()
  {
    return getBoolean(this.XV.adz);
  }
  
  public MostRecentGameInfo zzbhu()
  {
    if (zzhq(this.XV.adA)) {
      return null;
    }
    return this.XW;
  }
  
  public int zzbhv()
  {
    return getInteger(this.XV.adM);
  }
  
  public long zzbhw()
  {
    return getLong(this.XV.adN);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/PlayerRef.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */