package com.google.android.gms.games;

import android.content.Intent;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Releasable;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.games.internal.player.StockProfileImage;

public abstract interface Players
{
  public static final String EXTRA_PLAYER_SEARCH_RESULTS = "player_search_results";
  
  public abstract Intent getCompareProfileIntent(GoogleApiClient paramGoogleApiClient, Player paramPlayer);
  
  public abstract Player getCurrentPlayer(GoogleApiClient paramGoogleApiClient);
  
  public abstract String getCurrentPlayerId(GoogleApiClient paramGoogleApiClient);
  
  public abstract Intent getPlayerSearchIntent(GoogleApiClient paramGoogleApiClient);
  
  public abstract PendingResult<LoadPlayersResult> loadConnectedPlayers(GoogleApiClient paramGoogleApiClient, boolean paramBoolean);
  
  public abstract PendingResult<LoadPlayersResult> loadInvitablePlayers(GoogleApiClient paramGoogleApiClient, int paramInt, boolean paramBoolean);
  
  public abstract PendingResult<LoadPlayersResult> loadMoreInvitablePlayers(GoogleApiClient paramGoogleApiClient, int paramInt);
  
  public abstract PendingResult<LoadPlayersResult> loadMoreRecentlyPlayedWithPlayers(GoogleApiClient paramGoogleApiClient, int paramInt);
  
  public abstract PendingResult<LoadPlayersResult> loadPlayer(GoogleApiClient paramGoogleApiClient, String paramString);
  
  public abstract PendingResult<LoadPlayersResult> loadPlayer(GoogleApiClient paramGoogleApiClient, String paramString, boolean paramBoolean);
  
  public abstract PendingResult<LoadPlayersResult> loadRecentlyPlayedWithPlayers(GoogleApiClient paramGoogleApiClient, int paramInt, boolean paramBoolean);
  
  public static abstract interface LoadPlayersResult
    extends Releasable, Result
  {
    public abstract PlayerBuffer getPlayers();
  }
  
  public static abstract interface LoadProfileSettingsResult
    extends Result
  {
    public abstract String zzbhq();
    
    public abstract boolean zzbht();
    
    public abstract boolean zzbhy();
    
    public abstract StockProfileImage zzbhz();
    
    public abstract boolean zzbia();
    
    public abstract boolean zzbib();
    
    public abstract boolean zzbic();
  }
  
  public static abstract interface LoadStockProfileImagesResult
    extends Releasable, Result
  {}
  
  public static abstract interface LoadXpForGameCategoriesResult
    extends Result
  {}
  
  public static abstract interface LoadXpForGamesResult
    extends Result
  {}
  
  public static abstract interface LoadXpStreamResult
    extends Result
  {}
  
  public static abstract interface UpdateGamerProfileResult
    extends Result
  {}
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/Players.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */