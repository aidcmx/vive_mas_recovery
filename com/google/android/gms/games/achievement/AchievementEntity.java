package com.google.android.gms.games.achievement;

import android.database.CharArrayBuffer;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzc;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;
import com.google.android.gms.common.util.zzg;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerEntity;

public final class AchievementEntity
  extends AbstractSafeParcelable
  implements Achievement
{
  public static final Parcelable.Creator<AchievementEntity> CREATOR = new AchievementEntityCreator();
  private final String XX;
  private final Uri XY;
  private final String XZ;
  private final Uri Ya;
  private final String Yb;
  private final int Yc;
  private final String Yd;
  private final PlayerEntity Ye;
  private final int Yf;
  private final String Yg;
  private final long Yh;
  private final long Yi;
  private final String cg;
  private final String mName;
  private final int mState;
  private final int mVersionCode;
  private final int nV;
  
  AchievementEntity(int paramInt1, String paramString1, int paramInt2, String paramString2, String paramString3, Uri paramUri1, String paramString4, Uri paramUri2, String paramString5, int paramInt3, String paramString6, PlayerEntity paramPlayerEntity, int paramInt4, int paramInt5, String paramString7, long paramLong1, long paramLong2)
  {
    this.mVersionCode = paramInt1;
    this.XX = paramString1;
    this.nV = paramInt2;
    this.mName = paramString2;
    this.cg = paramString3;
    this.XY = paramUri1;
    this.XZ = paramString4;
    this.Ya = paramUri2;
    this.Yb = paramString5;
    this.Yc = paramInt3;
    this.Yd = paramString6;
    this.Ye = paramPlayerEntity;
    this.mState = paramInt4;
    this.Yf = paramInt5;
    this.Yg = paramString7;
    this.Yh = paramLong1;
    this.Yi = paramLong2;
  }
  
  public AchievementEntity(Achievement paramAchievement)
  {
    this.mVersionCode = 1;
    this.XX = paramAchievement.getAchievementId();
    this.nV = paramAchievement.getType();
    this.mName = paramAchievement.getName();
    this.cg = paramAchievement.getDescription();
    this.XY = paramAchievement.getUnlockedImageUri();
    this.XZ = paramAchievement.getUnlockedImageUrl();
    this.Ya = paramAchievement.getRevealedImageUri();
    this.Yb = paramAchievement.getRevealedImageUrl();
    this.Ye = ((PlayerEntity)paramAchievement.getPlayer().freeze());
    this.mState = paramAchievement.getState();
    this.Yh = paramAchievement.getLastUpdatedTimestamp();
    this.Yi = paramAchievement.getXpValue();
    if (paramAchievement.getType() == 1)
    {
      this.Yc = paramAchievement.getTotalSteps();
      this.Yd = paramAchievement.getFormattedTotalSteps();
      this.Yf = paramAchievement.getCurrentSteps();
    }
    for (this.Yg = paramAchievement.getFormattedCurrentSteps();; this.Yg = null)
    {
      zzc.zzu(this.XX);
      zzc.zzu(this.cg);
      return;
      this.Yc = 0;
      this.Yd = null;
      this.Yf = 0;
    }
  }
  
  static int zza(Achievement paramAchievement)
  {
    int j;
    int i;
    if (paramAchievement.getType() == 1)
    {
      j = paramAchievement.getCurrentSteps();
      i = paramAchievement.getTotalSteps();
    }
    for (;;)
    {
      return zzz.hashCode(new Object[] { paramAchievement.getAchievementId(), paramAchievement.getName(), Integer.valueOf(paramAchievement.getType()), paramAchievement.getDescription(), Long.valueOf(paramAchievement.getXpValue()), Integer.valueOf(paramAchievement.getState()), Long.valueOf(paramAchievement.getLastUpdatedTimestamp()), paramAchievement.getPlayer(), Integer.valueOf(j), Integer.valueOf(i) });
      i = 0;
      j = 0;
    }
  }
  
  static boolean zza(Achievement paramAchievement, Object paramObject)
  {
    boolean bool3 = true;
    boolean bool2;
    if (!(paramObject instanceof Achievement)) {
      bool2 = false;
    }
    do
    {
      return bool2;
      bool2 = bool3;
    } while (paramAchievement == paramObject);
    paramObject = (Achievement)paramObject;
    boolean bool1;
    if (paramAchievement.getType() == 1)
    {
      bool2 = zzz.equal(Integer.valueOf(((Achievement)paramObject).getCurrentSteps()), Integer.valueOf(paramAchievement.getCurrentSteps()));
      bool1 = zzz.equal(Integer.valueOf(((Achievement)paramObject).getTotalSteps()), Integer.valueOf(paramAchievement.getTotalSteps()));
    }
    for (;;)
    {
      if ((zzz.equal(((Achievement)paramObject).getAchievementId(), paramAchievement.getAchievementId())) && (zzz.equal(((Achievement)paramObject).getName(), paramAchievement.getName())) && (zzz.equal(Integer.valueOf(((Achievement)paramObject).getType()), Integer.valueOf(paramAchievement.getType()))) && (zzz.equal(((Achievement)paramObject).getDescription(), paramAchievement.getDescription())) && (zzz.equal(Long.valueOf(((Achievement)paramObject).getXpValue()), Long.valueOf(paramAchievement.getXpValue()))) && (zzz.equal(Integer.valueOf(((Achievement)paramObject).getState()), Integer.valueOf(paramAchievement.getState()))) && (zzz.equal(Long.valueOf(((Achievement)paramObject).getLastUpdatedTimestamp()), Long.valueOf(paramAchievement.getLastUpdatedTimestamp()))) && (zzz.equal(((Achievement)paramObject).getPlayer(), paramAchievement.getPlayer())) && (bool2))
      {
        bool2 = bool3;
        if (bool1) {
          break;
        }
      }
      return false;
      bool1 = true;
      bool2 = true;
    }
  }
  
  static String zzb(Achievement paramAchievement)
  {
    zzz.zza localzza = zzz.zzx(paramAchievement).zzg("Id", paramAchievement.getAchievementId()).zzg("Type", Integer.valueOf(paramAchievement.getType())).zzg("Name", paramAchievement.getName()).zzg("Description", paramAchievement.getDescription()).zzg("Player", paramAchievement.getPlayer()).zzg("State", Integer.valueOf(paramAchievement.getState()));
    if (paramAchievement.getType() == 1)
    {
      localzza.zzg("CurrentSteps", Integer.valueOf(paramAchievement.getCurrentSteps()));
      localzza.zzg("TotalSteps", Integer.valueOf(paramAchievement.getTotalSteps()));
    }
    return localzza.toString();
  }
  
  public boolean equals(Object paramObject)
  {
    return zza(this, paramObject);
  }
  
  public Achievement freeze()
  {
    return this;
  }
  
  public String getAchievementId()
  {
    return this.XX;
  }
  
  public int getCurrentSteps()
  {
    boolean bool = true;
    if (getType() == 1) {}
    for (;;)
    {
      zzc.zzbs(bool);
      return zzbif();
      bool = false;
    }
  }
  
  public String getDescription()
  {
    return this.cg;
  }
  
  public void getDescription(CharArrayBuffer paramCharArrayBuffer)
  {
    zzg.zzb(this.cg, paramCharArrayBuffer);
  }
  
  public String getFormattedCurrentSteps()
  {
    boolean bool = true;
    if (getType() == 1) {}
    for (;;)
    {
      zzc.zzbs(bool);
      return zzbig();
      bool = false;
    }
  }
  
  public void getFormattedCurrentSteps(CharArrayBuffer paramCharArrayBuffer)
  {
    boolean bool = true;
    if (getType() == 1) {}
    for (;;)
    {
      zzc.zzbs(bool);
      zzg.zzb(this.Yg, paramCharArrayBuffer);
      return;
      bool = false;
    }
  }
  
  public String getFormattedTotalSteps()
  {
    boolean bool = true;
    if (getType() == 1) {}
    for (;;)
    {
      zzc.zzbs(bool);
      return zzbie();
      bool = false;
    }
  }
  
  public void getFormattedTotalSteps(CharArrayBuffer paramCharArrayBuffer)
  {
    boolean bool = true;
    if (getType() == 1) {}
    for (;;)
    {
      zzc.zzbs(bool);
      zzg.zzb(this.Yd, paramCharArrayBuffer);
      return;
      bool = false;
    }
  }
  
  public long getLastUpdatedTimestamp()
  {
    return this.Yh;
  }
  
  public String getName()
  {
    return this.mName;
  }
  
  public void getName(CharArrayBuffer paramCharArrayBuffer)
  {
    zzg.zzb(this.mName, paramCharArrayBuffer);
  }
  
  public Player getPlayer()
  {
    return this.Ye;
  }
  
  public Uri getRevealedImageUri()
  {
    return this.Ya;
  }
  
  public String getRevealedImageUrl()
  {
    return this.Yb;
  }
  
  public int getState()
  {
    return this.mState;
  }
  
  public int getTotalSteps()
  {
    boolean bool = true;
    if (getType() == 1) {}
    for (;;)
    {
      zzc.zzbs(bool);
      return zzbid();
      bool = false;
    }
  }
  
  public int getType()
  {
    return this.nV;
  }
  
  public Uri getUnlockedImageUri()
  {
    return this.XY;
  }
  
  public String getUnlockedImageUrl()
  {
    return this.XZ;
  }
  
  public int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public long getXpValue()
  {
    return this.Yi;
  }
  
  public int hashCode()
  {
    return zza(this);
  }
  
  public boolean isDataValid()
  {
    return true;
  }
  
  public String toString()
  {
    return zzb(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    AchievementEntityCreator.zza(this, paramParcel, paramInt);
  }
  
  public int zzbid()
  {
    return this.Yc;
  }
  
  public String zzbie()
  {
    return this.Yd;
  }
  
  public int zzbif()
  {
    return this.Yf;
  }
  
  public String zzbig()
  {
    return this.Yg;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/achievement/AchievementEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */