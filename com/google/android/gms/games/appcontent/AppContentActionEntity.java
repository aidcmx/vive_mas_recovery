package com.google.android.gms.games.appcontent;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;
import java.util.ArrayList;
import java.util.List;

public final class AppContentActionEntity
  extends AbstractSafeParcelable
  implements AppContentAction
{
  public static final Parcelable.Creator<AppContentActionEntity> CREATOR = new AppContentActionEntityCreator();
  private final ArrayList<AppContentConditionEntity> Yj;
  private final AppContentAnnotationEntity Yk;
  private final String Yl;
  private final Bundle mExtras;
  private final int mVersionCode;
  private final String sy;
  private final String zzboa;
  private final String zzcpo;
  
  AppContentActionEntity(int paramInt, ArrayList<AppContentConditionEntity> paramArrayList, String paramString1, Bundle paramBundle, String paramString2, String paramString3, AppContentAnnotationEntity paramAppContentAnnotationEntity, String paramString4)
  {
    this.mVersionCode = paramInt;
    this.Yk = paramAppContentAnnotationEntity;
    this.Yj = paramArrayList;
    this.sy = paramString1;
    this.mExtras = paramBundle;
    this.zzboa = paramString3;
    this.Yl = paramString4;
    this.zzcpo = paramString2;
  }
  
  public AppContentActionEntity(AppContentAction paramAppContentAction)
  {
    this.mVersionCode = 5;
    this.Yk = ((AppContentAnnotationEntity)paramAppContentAction.zzbih().freeze());
    this.sy = paramAppContentAction.zzbij();
    this.mExtras = paramAppContentAction.getExtras();
    this.zzboa = paramAppContentAction.getId();
    this.Yl = paramAppContentAction.zzbik();
    this.zzcpo = paramAppContentAction.getType();
    paramAppContentAction = paramAppContentAction.zzbii();
    int j = paramAppContentAction.size();
    this.Yj = new ArrayList(j);
    int i = 0;
    while (i < j)
    {
      this.Yj.add((AppContentConditionEntity)((AppContentCondition)paramAppContentAction.get(i)).freeze());
      i += 1;
    }
  }
  
  static int zza(AppContentAction paramAppContentAction)
  {
    return zzz.hashCode(new Object[] { paramAppContentAction.zzbih(), paramAppContentAction.zzbii(), paramAppContentAction.zzbij(), paramAppContentAction.getExtras(), paramAppContentAction.getId(), paramAppContentAction.zzbik(), paramAppContentAction.getType() });
  }
  
  static boolean zza(AppContentAction paramAppContentAction, Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if (!(paramObject instanceof AppContentAction)) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramAppContentAction == paramObject);
      paramObject = (AppContentAction)paramObject;
      if ((!zzz.equal(((AppContentAction)paramObject).zzbih(), paramAppContentAction.zzbih())) || (!zzz.equal(((AppContentAction)paramObject).zzbii(), paramAppContentAction.zzbii())) || (!zzz.equal(((AppContentAction)paramObject).zzbij(), paramAppContentAction.zzbij())) || (!zzz.equal(((AppContentAction)paramObject).getExtras(), paramAppContentAction.getExtras())) || (!zzz.equal(((AppContentAction)paramObject).getId(), paramAppContentAction.getId())) || (!zzz.equal(((AppContentAction)paramObject).zzbik(), paramAppContentAction.zzbik()))) {
        break;
      }
      bool1 = bool2;
    } while (zzz.equal(((AppContentAction)paramObject).getType(), paramAppContentAction.getType()));
    return false;
  }
  
  static String zzb(AppContentAction paramAppContentAction)
  {
    return zzz.zzx(paramAppContentAction).zzg("Annotation", paramAppContentAction.zzbih()).zzg("Conditions", paramAppContentAction.zzbii()).zzg("ContentDescription", paramAppContentAction.zzbij()).zzg("Extras", paramAppContentAction.getExtras()).zzg("Id", paramAppContentAction.getId()).zzg("OverflowText", paramAppContentAction.zzbik()).zzg("Type", paramAppContentAction.getType()).toString();
  }
  
  public boolean equals(Object paramObject)
  {
    return zza(this, paramObject);
  }
  
  public Bundle getExtras()
  {
    return this.mExtras;
  }
  
  public String getId()
  {
    return this.zzboa;
  }
  
  public String getType()
  {
    return this.zzcpo;
  }
  
  public int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    return zza(this);
  }
  
  public boolean isDataValid()
  {
    return true;
  }
  
  public String toString()
  {
    return zzb(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    AppContentActionEntityCreator.zza(this, paramParcel, paramInt);
  }
  
  public AppContentAnnotation zzbih()
  {
    return this.Yk;
  }
  
  public List<AppContentCondition> zzbii()
  {
    return new ArrayList(this.Yj);
  }
  
  public String zzbij()
  {
    return this.sy;
  }
  
  public String zzbik()
  {
    return this.Yl;
  }
  
  public AppContentAction zzbil()
  {
    return this;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/appcontent/AppContentActionEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */