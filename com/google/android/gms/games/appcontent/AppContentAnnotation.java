package com.google.android.gms.games.appcontent;

import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import com.google.android.gms.common.data.Freezable;

public abstract interface AppContentAnnotation
  extends Parcelable, Freezable<AppContentAnnotation>
{
  public abstract String getDescription();
  
  public abstract String getId();
  
  public abstract String getTitle();
  
  public abstract String zzbim();
  
  public abstract int zzbin();
  
  public abstract Uri zzbio();
  
  public abstract Bundle zzbip();
  
  public abstract int zzbiq();
  
  public abstract String zzbir();
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/appcontent/AppContentAnnotation.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */