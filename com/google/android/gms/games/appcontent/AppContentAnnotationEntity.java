package com.google.android.gms.games.appcontent;

import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;

public final class AppContentAnnotationEntity
  extends AbstractSafeParcelable
  implements AppContentAnnotation
{
  public static final Parcelable.Creator<AppContentAnnotationEntity> CREATOR = new AppContentAnnotationEntityCreator();
  private final String JB;
  private final Uri Ym;
  private final String Yn;
  private final String Yo;
  private final int Yp;
  private final int Yq;
  private final Bundle Yr;
  private final String cg;
  private final int mVersionCode;
  private final String zzboa;
  
  AppContentAnnotationEntity(int paramInt1, String paramString1, Uri paramUri, String paramString2, String paramString3, String paramString4, String paramString5, int paramInt2, int paramInt3, Bundle paramBundle)
  {
    this.mVersionCode = paramInt1;
    this.cg = paramString1;
    this.zzboa = paramString3;
    this.Yo = paramString5;
    this.Yp = paramInt2;
    this.Ym = paramUri;
    this.Yq = paramInt3;
    this.Yn = paramString4;
    this.Yr = paramBundle;
    this.JB = paramString2;
  }
  
  public AppContentAnnotationEntity(AppContentAnnotation paramAppContentAnnotation)
  {
    this.mVersionCode = 4;
    this.cg = paramAppContentAnnotation.getDescription();
    this.zzboa = paramAppContentAnnotation.getId();
    this.Yo = paramAppContentAnnotation.zzbim();
    this.Yp = paramAppContentAnnotation.zzbin();
    this.Ym = paramAppContentAnnotation.zzbio();
    this.Yq = paramAppContentAnnotation.zzbiq();
    this.Yn = paramAppContentAnnotation.zzbir();
    this.Yr = paramAppContentAnnotation.zzbip();
    this.JB = paramAppContentAnnotation.getTitle();
  }
  
  static int zza(AppContentAnnotation paramAppContentAnnotation)
  {
    return zzz.hashCode(new Object[] { paramAppContentAnnotation.getDescription(), paramAppContentAnnotation.getId(), paramAppContentAnnotation.zzbim(), Integer.valueOf(paramAppContentAnnotation.zzbin()), paramAppContentAnnotation.zzbio(), Integer.valueOf(paramAppContentAnnotation.zzbiq()), paramAppContentAnnotation.zzbir(), paramAppContentAnnotation.zzbip(), paramAppContentAnnotation.getTitle() });
  }
  
  static boolean zza(AppContentAnnotation paramAppContentAnnotation, Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if (!(paramObject instanceof AppContentAnnotation)) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramAppContentAnnotation == paramObject);
      paramObject = (AppContentAnnotation)paramObject;
      if ((!zzz.equal(((AppContentAnnotation)paramObject).getDescription(), paramAppContentAnnotation.getDescription())) || (!zzz.equal(((AppContentAnnotation)paramObject).getId(), paramAppContentAnnotation.getId())) || (!zzz.equal(((AppContentAnnotation)paramObject).zzbim(), paramAppContentAnnotation.zzbim())) || (!zzz.equal(Integer.valueOf(((AppContentAnnotation)paramObject).zzbin()), Integer.valueOf(paramAppContentAnnotation.zzbin()))) || (!zzz.equal(((AppContentAnnotation)paramObject).zzbio(), paramAppContentAnnotation.zzbio())) || (!zzz.equal(Integer.valueOf(((AppContentAnnotation)paramObject).zzbiq()), Integer.valueOf(paramAppContentAnnotation.zzbiq()))) || (!zzz.equal(((AppContentAnnotation)paramObject).zzbir(), paramAppContentAnnotation.zzbir())) || (!zzz.equal(((AppContentAnnotation)paramObject).zzbip(), paramAppContentAnnotation.zzbip()))) {
        break;
      }
      bool1 = bool2;
    } while (zzz.equal(((AppContentAnnotation)paramObject).getTitle(), paramAppContentAnnotation.getTitle()));
    return false;
  }
  
  static String zzb(AppContentAnnotation paramAppContentAnnotation)
  {
    return zzz.zzx(paramAppContentAnnotation).zzg("Description", paramAppContentAnnotation.getDescription()).zzg("Id", paramAppContentAnnotation.getId()).zzg("ImageDefaultId", paramAppContentAnnotation.zzbim()).zzg("ImageHeight", Integer.valueOf(paramAppContentAnnotation.zzbin())).zzg("ImageUri", paramAppContentAnnotation.zzbio()).zzg("ImageWidth", Integer.valueOf(paramAppContentAnnotation.zzbiq())).zzg("LayoutSlot", paramAppContentAnnotation.zzbir()).zzg("Modifiers", paramAppContentAnnotation.zzbip()).zzg("Title", paramAppContentAnnotation.getTitle()).toString();
  }
  
  public boolean equals(Object paramObject)
  {
    return zza(this, paramObject);
  }
  
  public String getDescription()
  {
    return this.cg;
  }
  
  public String getId()
  {
    return this.zzboa;
  }
  
  public String getTitle()
  {
    return this.JB;
  }
  
  public int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    return zza(this);
  }
  
  public boolean isDataValid()
  {
    return true;
  }
  
  public String toString()
  {
    return zzb(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    AppContentAnnotationEntityCreator.zza(this, paramParcel, paramInt);
  }
  
  public String zzbim()
  {
    return this.Yo;
  }
  
  public int zzbin()
  {
    return this.Yp;
  }
  
  public Uri zzbio()
  {
    return this.Ym;
  }
  
  public Bundle zzbip()
  {
    return this.Yr;
  }
  
  public int zzbiq()
  {
    return this.Yq;
  }
  
  public String zzbir()
  {
    return this.Yn;
  }
  
  public AppContentAnnotation zzbis()
  {
    return this;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/appcontent/AppContentAnnotationEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */