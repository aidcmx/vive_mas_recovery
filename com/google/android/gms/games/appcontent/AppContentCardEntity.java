package com.google.android.gms.games.appcontent;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;
import java.util.ArrayList;
import java.util.List;

public final class AppContentCardEntity
  extends AbstractSafeParcelable
  implements AppContentCard
{
  public static final Parcelable.Creator<AppContentCardEntity> CREATOR = new AppContentCardEntityCreator();
  private final String JB;
  private final ArrayList<AppContentConditionEntity> Yj;
  private final ArrayList<AppContentAnnotationEntity> Ys;
  private final int Yt;
  private final String Yu;
  private final int Yv;
  private final String cg;
  private final ArrayList<AppContentActionEntity> mActions;
  private final Bundle mExtras;
  private final int mVersionCode;
  private final String sy;
  private final String zzboa;
  private final String zzcpo;
  
  AppContentCardEntity(int paramInt1, ArrayList<AppContentActionEntity> paramArrayList, ArrayList<AppContentAnnotationEntity> paramArrayList1, ArrayList<AppContentConditionEntity> paramArrayList2, String paramString1, int paramInt2, String paramString2, Bundle paramBundle, String paramString3, String paramString4, int paramInt3, String paramString5, String paramString6)
  {
    this.mVersionCode = paramInt1;
    this.mActions = paramArrayList;
    this.Ys = paramArrayList1;
    this.Yj = paramArrayList2;
    this.sy = paramString1;
    this.Yt = paramInt2;
    this.cg = paramString2;
    this.mExtras = paramBundle;
    this.zzboa = paramString6;
    this.Yu = paramString3;
    this.JB = paramString4;
    this.Yv = paramInt3;
    this.zzcpo = paramString5;
  }
  
  public AppContentCardEntity(AppContentCard paramAppContentCard)
  {
    this.mVersionCode = 4;
    this.sy = paramAppContentCard.zzbij();
    this.Yt = paramAppContentCard.zzbiu();
    this.cg = paramAppContentCard.getDescription();
    this.mExtras = paramAppContentCard.getExtras();
    this.zzboa = paramAppContentCard.getId();
    this.JB = paramAppContentCard.getTitle();
    this.Yu = paramAppContentCard.zzbiv();
    this.Yv = paramAppContentCard.zzbiw();
    this.zzcpo = paramAppContentCard.getType();
    List localList = paramAppContentCard.getActions();
    int k = localList.size();
    this.mActions = new ArrayList(k);
    int i = 0;
    while (i < k)
    {
      this.mActions.add((AppContentActionEntity)((AppContentAction)localList.get(i)).freeze());
      i += 1;
    }
    localList = paramAppContentCard.zzbit();
    k = localList.size();
    this.Ys = new ArrayList(k);
    i = 0;
    while (i < k)
    {
      this.Ys.add((AppContentAnnotationEntity)((AppContentAnnotation)localList.get(i)).freeze());
      i += 1;
    }
    paramAppContentCard = paramAppContentCard.zzbii();
    k = paramAppContentCard.size();
    this.Yj = new ArrayList(k);
    i = j;
    while (i < k)
    {
      this.Yj.add((AppContentConditionEntity)((AppContentCondition)paramAppContentCard.get(i)).freeze());
      i += 1;
    }
  }
  
  static int zza(AppContentCard paramAppContentCard)
  {
    return zzz.hashCode(new Object[] { paramAppContentCard.getActions(), paramAppContentCard.zzbit(), paramAppContentCard.zzbii(), paramAppContentCard.zzbij(), Integer.valueOf(paramAppContentCard.zzbiu()), paramAppContentCard.getDescription(), paramAppContentCard.getExtras(), paramAppContentCard.getId(), paramAppContentCard.zzbiv(), paramAppContentCard.getTitle(), Integer.valueOf(paramAppContentCard.zzbiw()), paramAppContentCard.getType() });
  }
  
  static boolean zza(AppContentCard paramAppContentCard, Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if (!(paramObject instanceof AppContentCard)) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramAppContentCard == paramObject);
      paramObject = (AppContentCard)paramObject;
      if ((!zzz.equal(((AppContentCard)paramObject).getActions(), paramAppContentCard.getActions())) || (!zzz.equal(((AppContentCard)paramObject).zzbit(), paramAppContentCard.zzbit())) || (!zzz.equal(((AppContentCard)paramObject).zzbii(), paramAppContentCard.zzbii())) || (!zzz.equal(((AppContentCard)paramObject).zzbij(), paramAppContentCard.zzbij())) || (!zzz.equal(Integer.valueOf(((AppContentCard)paramObject).zzbiu()), Integer.valueOf(paramAppContentCard.zzbiu()))) || (!zzz.equal(((AppContentCard)paramObject).getDescription(), paramAppContentCard.getDescription())) || (!zzz.equal(((AppContentCard)paramObject).getExtras(), paramAppContentCard.getExtras())) || (!zzz.equal(((AppContentCard)paramObject).getId(), paramAppContentCard.getId())) || (!zzz.equal(((AppContentCard)paramObject).zzbiv(), paramAppContentCard.zzbiv())) || (!zzz.equal(((AppContentCard)paramObject).getTitle(), paramAppContentCard.getTitle())) || (!zzz.equal(Integer.valueOf(((AppContentCard)paramObject).zzbiw()), Integer.valueOf(paramAppContentCard.zzbiw())))) {
        break;
      }
      bool1 = bool2;
    } while (zzz.equal(((AppContentCard)paramObject).getType(), paramAppContentCard.getType()));
    return false;
  }
  
  static String zzb(AppContentCard paramAppContentCard)
  {
    return zzz.zzx(paramAppContentCard).zzg("Actions", paramAppContentCard.getActions()).zzg("Annotations", paramAppContentCard.zzbit()).zzg("Conditions", paramAppContentCard.zzbii()).zzg("ContentDescription", paramAppContentCard.zzbij()).zzg("CurrentSteps", Integer.valueOf(paramAppContentCard.zzbiu())).zzg("Description", paramAppContentCard.getDescription()).zzg("Extras", paramAppContentCard.getExtras()).zzg("Id", paramAppContentCard.getId()).zzg("Subtitle", paramAppContentCard.zzbiv()).zzg("Title", paramAppContentCard.getTitle()).zzg("TotalSteps", Integer.valueOf(paramAppContentCard.zzbiw())).zzg("Type", paramAppContentCard.getType()).toString();
  }
  
  public boolean equals(Object paramObject)
  {
    return zza(this, paramObject);
  }
  
  public List<AppContentAction> getActions()
  {
    return new ArrayList(this.mActions);
  }
  
  public String getDescription()
  {
    return this.cg;
  }
  
  public Bundle getExtras()
  {
    return this.mExtras;
  }
  
  public String getId()
  {
    return this.zzboa;
  }
  
  public String getTitle()
  {
    return this.JB;
  }
  
  public String getType()
  {
    return this.zzcpo;
  }
  
  public int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    return zza(this);
  }
  
  public boolean isDataValid()
  {
    return true;
  }
  
  public String toString()
  {
    return zzb(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    AppContentCardEntityCreator.zza(this, paramParcel, paramInt);
  }
  
  public List<AppContentCondition> zzbii()
  {
    return new ArrayList(this.Yj);
  }
  
  public String zzbij()
  {
    return this.sy;
  }
  
  public List<AppContentAnnotation> zzbit()
  {
    return new ArrayList(this.Ys);
  }
  
  public int zzbiu()
  {
    return this.Yt;
  }
  
  public String zzbiv()
  {
    return this.Yu;
  }
  
  public int zzbiw()
  {
    return this.Yv;
  }
  
  public AppContentCard zzbix()
  {
    return this;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/appcontent/AppContentCardEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */