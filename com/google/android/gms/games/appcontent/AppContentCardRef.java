package com.google.android.gms.games.appcontent;

import android.os.Bundle;
import android.os.Parcel;
import com.google.android.gms.common.data.DataHolder;
import java.util.ArrayList;
import java.util.List;

public final class AppContentCardRef
  extends MultiDataBufferRef
  implements AppContentCard
{
  AppContentCardRef(ArrayList<DataHolder> paramArrayList, int paramInt)
  {
    super(paramArrayList, 0, paramInt);
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    return AppContentCardEntity.zza(this, paramObject);
  }
  
  public List<AppContentAction> getActions()
  {
    return AppContentUtils.zza(this.zy, this.YG, "card_actions", this.BU);
  }
  
  public String getDescription()
  {
    return getString("card_description");
  }
  
  public Bundle getExtras()
  {
    return AppContentUtils.zzd(this.zy, this.YG, "card_data", this.BU);
  }
  
  public String getId()
  {
    return getString("card_id");
  }
  
  public String getTitle()
  {
    return getString("card_title");
  }
  
  public String getType()
  {
    return getString("card_type");
  }
  
  public int hashCode()
  {
    return AppContentCardEntity.zza(this);
  }
  
  public String toString()
  {
    return AppContentCardEntity.zzb(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    ((AppContentCardEntity)freeze()).writeToParcel(paramParcel, paramInt);
  }
  
  public List<AppContentCondition> zzbii()
  {
    return AppContentUtils.zzc(this.zy, this.YG, "card_conditions", this.BU);
  }
  
  public String zzbij()
  {
    return getString("card_content_description");
  }
  
  public List<AppContentAnnotation> zzbit()
  {
    return AppContentUtils.zzb(this.zy, this.YG, "card_annotations", this.BU);
  }
  
  public int zzbiu()
  {
    return getInteger("card_current_steps");
  }
  
  public String zzbiv()
  {
    return getString("card_subtitle");
  }
  
  public int zzbiw()
  {
    return getInteger("card_total_steps");
  }
  
  public AppContentCard zzbix()
  {
    return new AppContentCardEntity(this);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/appcontent/AppContentCardRef.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */