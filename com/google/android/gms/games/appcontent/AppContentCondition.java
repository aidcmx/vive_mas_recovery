package com.google.android.gms.games.appcontent;

import android.os.Bundle;
import android.os.Parcelable;
import com.google.android.gms.common.data.Freezable;

public abstract interface AppContentCondition
  extends Parcelable, Freezable<AppContentCondition>
{
  public abstract String zzbiy();
  
  public abstract String zzbiz();
  
  public abstract String zzbja();
  
  public abstract Bundle zzbjb();
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/appcontent/AppContentCondition.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */