package com.google.android.gms.games.appcontent;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;

public final class AppContentConditionEntity
  extends AbstractSafeParcelable
  implements AppContentCondition
{
  public static final Parcelable.Creator<AppContentConditionEntity> CREATOR = new AppContentConditionEntityCreator();
  private final String Yw;
  private final String Yx;
  private final String Yy;
  private final Bundle Yz;
  private final int mVersionCode;
  
  AppContentConditionEntity(int paramInt, String paramString1, String paramString2, String paramString3, Bundle paramBundle)
  {
    this.mVersionCode = paramInt;
    this.Yw = paramString1;
    this.Yx = paramString2;
    this.Yy = paramString3;
    this.Yz = paramBundle;
  }
  
  public AppContentConditionEntity(AppContentCondition paramAppContentCondition)
  {
    this.mVersionCode = 1;
    this.Yw = paramAppContentCondition.zzbiy();
    this.Yx = paramAppContentCondition.zzbiz();
    this.Yy = paramAppContentCondition.zzbja();
    this.Yz = paramAppContentCondition.zzbjb();
  }
  
  static int zza(AppContentCondition paramAppContentCondition)
  {
    return zzz.hashCode(new Object[] { paramAppContentCondition.zzbiy(), paramAppContentCondition.zzbiz(), paramAppContentCondition.zzbja(), paramAppContentCondition.zzbjb() });
  }
  
  static boolean zza(AppContentCondition paramAppContentCondition, Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if (!(paramObject instanceof AppContentCondition)) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramAppContentCondition == paramObject);
      paramObject = (AppContentCondition)paramObject;
      if ((!zzz.equal(((AppContentCondition)paramObject).zzbiy(), paramAppContentCondition.zzbiy())) || (!zzz.equal(((AppContentCondition)paramObject).zzbiz(), paramAppContentCondition.zzbiz())) || (!zzz.equal(((AppContentCondition)paramObject).zzbja(), paramAppContentCondition.zzbja()))) {
        break;
      }
      bool1 = bool2;
    } while (zzz.equal(((AppContentCondition)paramObject).zzbjb(), paramAppContentCondition.zzbjb()));
    return false;
  }
  
  static String zzb(AppContentCondition paramAppContentCondition)
  {
    return zzz.zzx(paramAppContentCondition).zzg("DefaultValue", paramAppContentCondition.zzbiy()).zzg("ExpectedValue", paramAppContentCondition.zzbiz()).zzg("Predicate", paramAppContentCondition.zzbja()).zzg("PredicateParameters", paramAppContentCondition.zzbjb()).toString();
  }
  
  public boolean equals(Object paramObject)
  {
    return zza(this, paramObject);
  }
  
  public int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    return zza(this);
  }
  
  public boolean isDataValid()
  {
    return true;
  }
  
  public String toString()
  {
    return zzb(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    AppContentConditionEntityCreator.zza(this, paramParcel, paramInt);
  }
  
  public String zzbiy()
  {
    return this.Yw;
  }
  
  public String zzbiz()
  {
    return this.Yx;
  }
  
  public String zzbja()
  {
    return this.Yy;
  }
  
  public Bundle zzbjb()
  {
    return this.Yz;
  }
  
  public AppContentCondition zzbjc()
  {
    return this;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/appcontent/AppContentConditionEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */