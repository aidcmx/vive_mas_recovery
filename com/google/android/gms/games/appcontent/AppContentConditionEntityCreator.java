package com.google.android.gms.games.appcontent;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class AppContentConditionEntityCreator
  implements Parcelable.Creator<AppContentConditionEntity>
{
  static void zza(AppContentConditionEntity paramAppContentConditionEntity, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.zzcs(paramParcel);
    zzb.zza(paramParcel, 1, paramAppContentConditionEntity.zzbiy(), false);
    zzb.zza(paramParcel, 2, paramAppContentConditionEntity.zzbiz(), false);
    zzb.zza(paramParcel, 3, paramAppContentConditionEntity.zzbja(), false);
    zzb.zza(paramParcel, 4, paramAppContentConditionEntity.zzbjb(), false);
    zzb.zzc(paramParcel, 1000, paramAppContentConditionEntity.getVersionCode());
    zzb.zzaj(paramParcel, paramInt);
  }
  
  public AppContentConditionEntity zzls(Parcel paramParcel)
  {
    Bundle localBundle = null;
    int j = zza.zzcr(paramParcel);
    int i = 0;
    String str1 = null;
    String str2 = null;
    String str3 = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.zzcq(paramParcel);
      switch (zza.zzgu(k))
      {
      default: 
        zza.zzb(paramParcel, k);
        break;
      case 1: 
        str3 = zza.zzq(paramParcel, k);
        break;
      case 2: 
        str2 = zza.zzq(paramParcel, k);
        break;
      case 3: 
        str1 = zza.zzq(paramParcel, k);
        break;
      case 4: 
        localBundle = zza.zzs(paramParcel, k);
        break;
      case 1000: 
        i = zza.zzg(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new AppContentConditionEntity(i, str3, str2, str1, localBundle);
  }
  
  public AppContentConditionEntity[] zzrd(int paramInt)
  {
    return new AppContentConditionEntity[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/appcontent/AppContentConditionEntityCreator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */