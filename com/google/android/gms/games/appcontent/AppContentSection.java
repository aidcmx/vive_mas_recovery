package com.google.android.gms.games.appcontent;

import android.os.Bundle;
import android.os.Parcelable;
import com.google.android.gms.common.data.Freezable;
import java.util.List;

public abstract interface AppContentSection
  extends Parcelable, Freezable<AppContentSection>
{
  public abstract List<AppContentAction> getActions();
  
  public abstract Bundle getExtras();
  
  public abstract String getId();
  
  public abstract String getTitle();
  
  public abstract String getType();
  
  public abstract String zzbij();
  
  public abstract List<AppContentAnnotation> zzbit();
  
  public abstract String zzbiv();
  
  public abstract List<AppContentCard> zzbjd();
  
  public abstract String zzbje();
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/appcontent/AppContentSection.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */