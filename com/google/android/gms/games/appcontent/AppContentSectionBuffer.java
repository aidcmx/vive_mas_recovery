package com.google.android.gms.games.appcontent;

import com.google.android.gms.common.data.zzf;

public final class AppContentSectionBuffer
  extends zzf<AppContentSection>
{
  public void release()
  {
    super.release();
    throw new NullPointerException();
  }
  
  protected String zzauq()
  {
    return "section_id";
  }
  
  protected String zzaus()
  {
    return "card_id";
  }
  
  protected AppContentSection zzo(int paramInt1, int paramInt2)
  {
    return new AppContentSectionRef(null, paramInt1, paramInt2);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/appcontent/AppContentSectionBuffer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */