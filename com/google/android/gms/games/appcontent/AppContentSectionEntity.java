package com.google.android.gms.games.appcontent;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;
import java.util.ArrayList;
import java.util.List;

public final class AppContentSectionEntity
  extends AbstractSafeParcelable
  implements AppContentSection
{
  public static final Parcelable.Creator<AppContentSectionEntity> CREATOR = new AppContentSectionEntityCreator();
  private final String JB;
  private final ArrayList<AppContentCardEntity> YA;
  private final String YB;
  private final ArrayList<AppContentAnnotationEntity> Ys;
  private final String Yu;
  private final ArrayList<AppContentActionEntity> mActions;
  private final Bundle mExtras;
  private final int mVersionCode;
  private final String sy;
  private final String zzboa;
  private final String zzcpo;
  
  AppContentSectionEntity(int paramInt, ArrayList<AppContentActionEntity> paramArrayList, ArrayList<AppContentCardEntity> paramArrayList1, String paramString1, Bundle paramBundle, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, ArrayList<AppContentAnnotationEntity> paramArrayList2)
  {
    this.mVersionCode = paramInt;
    this.mActions = paramArrayList;
    this.Ys = paramArrayList2;
    this.YA = paramArrayList1;
    this.YB = paramString6;
    this.sy = paramString1;
    this.mExtras = paramBundle;
    this.zzboa = paramString5;
    this.Yu = paramString2;
    this.JB = paramString3;
    this.zzcpo = paramString4;
  }
  
  public AppContentSectionEntity(AppContentSection paramAppContentSection)
  {
    this.mVersionCode = 5;
    this.YB = paramAppContentSection.zzbje();
    this.sy = paramAppContentSection.zzbij();
    this.mExtras = paramAppContentSection.getExtras();
    this.zzboa = paramAppContentSection.getId();
    this.Yu = paramAppContentSection.zzbiv();
    this.JB = paramAppContentSection.getTitle();
    this.zzcpo = paramAppContentSection.getType();
    List localList = paramAppContentSection.getActions();
    int k = localList.size();
    this.mActions = new ArrayList(k);
    int i = 0;
    while (i < k)
    {
      this.mActions.add((AppContentActionEntity)((AppContentAction)localList.get(i)).freeze());
      i += 1;
    }
    localList = paramAppContentSection.zzbjd();
    k = localList.size();
    this.YA = new ArrayList(k);
    i = 0;
    while (i < k)
    {
      this.YA.add((AppContentCardEntity)((AppContentCard)localList.get(i)).freeze());
      i += 1;
    }
    paramAppContentSection = paramAppContentSection.zzbit();
    k = paramAppContentSection.size();
    this.Ys = new ArrayList(k);
    i = j;
    while (i < k)
    {
      this.Ys.add((AppContentAnnotationEntity)((AppContentAnnotation)paramAppContentSection.get(i)).freeze());
      i += 1;
    }
  }
  
  static int zza(AppContentSection paramAppContentSection)
  {
    return zzz.hashCode(new Object[] { paramAppContentSection.getActions(), paramAppContentSection.zzbit(), paramAppContentSection.zzbjd(), paramAppContentSection.zzbje(), paramAppContentSection.zzbij(), paramAppContentSection.getExtras(), paramAppContentSection.getId(), paramAppContentSection.zzbiv(), paramAppContentSection.getTitle(), paramAppContentSection.getType() });
  }
  
  static boolean zza(AppContentSection paramAppContentSection, Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if (!(paramObject instanceof AppContentSection)) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramAppContentSection == paramObject);
      paramObject = (AppContentSection)paramObject;
      if ((!zzz.equal(((AppContentSection)paramObject).getActions(), paramAppContentSection.getActions())) || (!zzz.equal(((AppContentSection)paramObject).zzbit(), paramAppContentSection.zzbit())) || (!zzz.equal(((AppContentSection)paramObject).zzbjd(), paramAppContentSection.zzbjd())) || (!zzz.equal(((AppContentSection)paramObject).zzbje(), paramAppContentSection.zzbje())) || (!zzz.equal(((AppContentSection)paramObject).zzbij(), paramAppContentSection.zzbij())) || (!zzz.equal(((AppContentSection)paramObject).getExtras(), paramAppContentSection.getExtras())) || (!zzz.equal(((AppContentSection)paramObject).getId(), paramAppContentSection.getId())) || (!zzz.equal(((AppContentSection)paramObject).zzbiv(), paramAppContentSection.zzbiv())) || (!zzz.equal(((AppContentSection)paramObject).getTitle(), paramAppContentSection.getTitle()))) {
        break;
      }
      bool1 = bool2;
    } while (zzz.equal(((AppContentSection)paramObject).getType(), paramAppContentSection.getType()));
    return false;
  }
  
  static String zzb(AppContentSection paramAppContentSection)
  {
    return zzz.zzx(paramAppContentSection).zzg("Actions", paramAppContentSection.getActions()).zzg("Annotations", paramAppContentSection.zzbit()).zzg("Cards", paramAppContentSection.zzbjd()).zzg("CardType", paramAppContentSection.zzbje()).zzg("ContentDescription", paramAppContentSection.zzbij()).zzg("Extras", paramAppContentSection.getExtras()).zzg("Id", paramAppContentSection.getId()).zzg("Subtitle", paramAppContentSection.zzbiv()).zzg("Title", paramAppContentSection.getTitle()).zzg("Type", paramAppContentSection.getType()).toString();
  }
  
  public boolean equals(Object paramObject)
  {
    return zza(this, paramObject);
  }
  
  public List<AppContentAction> getActions()
  {
    return new ArrayList(this.mActions);
  }
  
  public Bundle getExtras()
  {
    return this.mExtras;
  }
  
  public String getId()
  {
    return this.zzboa;
  }
  
  public String getTitle()
  {
    return this.JB;
  }
  
  public String getType()
  {
    return this.zzcpo;
  }
  
  public int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    return zza(this);
  }
  
  public boolean isDataValid()
  {
    return true;
  }
  
  public String toString()
  {
    return zzb(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    AppContentSectionEntityCreator.zza(this, paramParcel, paramInt);
  }
  
  public String zzbij()
  {
    return this.sy;
  }
  
  public List<AppContentAnnotation> zzbit()
  {
    return new ArrayList(this.Ys);
  }
  
  public String zzbiv()
  {
    return this.Yu;
  }
  
  public List<AppContentCard> zzbjd()
  {
    return new ArrayList(this.YA);
  }
  
  public String zzbje()
  {
    return this.YB;
  }
  
  public AppContentSection zzbjf()
  {
    return this;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/appcontent/AppContentSectionEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */