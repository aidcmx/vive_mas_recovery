package com.google.android.gms.games.appcontent;

import android.os.Bundle;
import android.os.Parcel;
import com.google.android.gms.common.data.DataHolder;
import java.util.ArrayList;

public final class AppContentSectionRef
  extends MultiDataBufferRef
  implements AppContentSection
{
  private final int YC;
  
  AppContentSectionRef(ArrayList<DataHolder> paramArrayList, int paramInt1, int paramInt2)
  {
    super(paramArrayList, 0, paramInt1);
    this.YC = paramInt2;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    return AppContentSectionEntity.zza(this, paramObject);
  }
  
  public Bundle getExtras()
  {
    return AppContentUtils.zzd(this.zy, this.YG, "section_data", this.BU);
  }
  
  public String getId()
  {
    return getString("section_id");
  }
  
  public String getTitle()
  {
    return getString("section_title");
  }
  
  public String getType()
  {
    return getString("section_type");
  }
  
  public int hashCode()
  {
    return AppContentSectionEntity.zza(this);
  }
  
  public String toString()
  {
    return AppContentSectionEntity.zzb(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    ((AppContentSectionEntity)freeze()).writeToParcel(paramParcel, paramInt);
  }
  
  public String zzbij()
  {
    return getString("section_content_description");
  }
  
  public String zzbiv()
  {
    return getString("section_subtitle");
  }
  
  public String zzbje()
  {
    return getString("section_card_type");
  }
  
  public AppContentSection zzbjf()
  {
    return new AppContentSectionEntity(this);
  }
  
  public ArrayList<AppContentAction> zzbjg()
  {
    return AppContentUtils.zza(this.zy, this.YG, "section_actions", this.BU);
  }
  
  public ArrayList<AppContentAnnotation> zzbjh()
  {
    return AppContentUtils.zzb(this.zy, this.YG, "section_annotations", this.BU);
  }
  
  public ArrayList<AppContentCard> zzbji()
  {
    ArrayList localArrayList = new ArrayList(this.YC);
    int i = 0;
    while (i < this.YC)
    {
      localArrayList.add(new AppContentCardRef(this.YG, this.BU + i));
      i += 1;
    }
    return localArrayList;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/appcontent/AppContentSectionRef.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */