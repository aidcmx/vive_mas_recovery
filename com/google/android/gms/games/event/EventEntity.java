package com.google.android.gms.games.event;

import android.database.CharArrayBuffer;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;
import com.google.android.gms.common.util.zzg;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerEntity;

public final class EventEntity
  extends AbstractSafeParcelable
  implements Event
{
  public static final Parcelable.Creator<EventEntity> CREATOR = new EventEntityCreator();
  private final Uri WQ;
  private final String Xb;
  private final String YH;
  private final String YI;
  private final PlayerEntity Ye;
  private final long cf;
  private final String cg;
  private final String mName;
  private final int mVersionCode;
  private final boolean zzatq;
  
  EventEntity(int paramInt, String paramString1, String paramString2, String paramString3, Uri paramUri, String paramString4, Player paramPlayer, long paramLong, String paramString5, boolean paramBoolean)
  {
    this.mVersionCode = paramInt;
    this.YH = paramString1;
    this.mName = paramString2;
    this.cg = paramString3;
    this.WQ = paramUri;
    this.Xb = paramString4;
    this.Ye = new PlayerEntity(paramPlayer);
    this.cf = paramLong;
    this.YI = paramString5;
    this.zzatq = paramBoolean;
  }
  
  public EventEntity(Event paramEvent)
  {
    this.mVersionCode = 1;
    this.YH = paramEvent.getEventId();
    this.mName = paramEvent.getName();
    this.cg = paramEvent.getDescription();
    this.WQ = paramEvent.getIconImageUri();
    this.Xb = paramEvent.getIconImageUrl();
    this.Ye = ((PlayerEntity)paramEvent.getPlayer().freeze());
    this.cf = paramEvent.getValue();
    this.YI = paramEvent.getFormattedValue();
    this.zzatq = paramEvent.isVisible();
  }
  
  static int zza(Event paramEvent)
  {
    return zzz.hashCode(new Object[] { paramEvent.getEventId(), paramEvent.getName(), paramEvent.getDescription(), paramEvent.getIconImageUri(), paramEvent.getIconImageUrl(), paramEvent.getPlayer(), Long.valueOf(paramEvent.getValue()), paramEvent.getFormattedValue(), Boolean.valueOf(paramEvent.isVisible()) });
  }
  
  static boolean zza(Event paramEvent, Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if (!(paramObject instanceof Event)) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramEvent == paramObject);
      paramObject = (Event)paramObject;
      if ((!zzz.equal(((Event)paramObject).getEventId(), paramEvent.getEventId())) || (!zzz.equal(((Event)paramObject).getName(), paramEvent.getName())) || (!zzz.equal(((Event)paramObject).getDescription(), paramEvent.getDescription())) || (!zzz.equal(((Event)paramObject).getIconImageUri(), paramEvent.getIconImageUri())) || (!zzz.equal(((Event)paramObject).getIconImageUrl(), paramEvent.getIconImageUrl())) || (!zzz.equal(((Event)paramObject).getPlayer(), paramEvent.getPlayer())) || (!zzz.equal(Long.valueOf(((Event)paramObject).getValue()), Long.valueOf(paramEvent.getValue()))) || (!zzz.equal(((Event)paramObject).getFormattedValue(), paramEvent.getFormattedValue()))) {
        break;
      }
      bool1 = bool2;
    } while (zzz.equal(Boolean.valueOf(((Event)paramObject).isVisible()), Boolean.valueOf(paramEvent.isVisible())));
    return false;
  }
  
  static String zzb(Event paramEvent)
  {
    return zzz.zzx(paramEvent).zzg("Id", paramEvent.getEventId()).zzg("Name", paramEvent.getName()).zzg("Description", paramEvent.getDescription()).zzg("IconImageUri", paramEvent.getIconImageUri()).zzg("IconImageUrl", paramEvent.getIconImageUrl()).zzg("Player", paramEvent.getPlayer()).zzg("Value", Long.valueOf(paramEvent.getValue())).zzg("FormattedValue", paramEvent.getFormattedValue()).zzg("isVisible", Boolean.valueOf(paramEvent.isVisible())).toString();
  }
  
  public boolean equals(Object paramObject)
  {
    return zza(this, paramObject);
  }
  
  public Event freeze()
  {
    return this;
  }
  
  public String getDescription()
  {
    return this.cg;
  }
  
  public void getDescription(CharArrayBuffer paramCharArrayBuffer)
  {
    zzg.zzb(this.cg, paramCharArrayBuffer);
  }
  
  public String getEventId()
  {
    return this.YH;
  }
  
  public String getFormattedValue()
  {
    return this.YI;
  }
  
  public void getFormattedValue(CharArrayBuffer paramCharArrayBuffer)
  {
    zzg.zzb(this.YI, paramCharArrayBuffer);
  }
  
  public Uri getIconImageUri()
  {
    return this.WQ;
  }
  
  public String getIconImageUrl()
  {
    return this.Xb;
  }
  
  public String getName()
  {
    return this.mName;
  }
  
  public void getName(CharArrayBuffer paramCharArrayBuffer)
  {
    zzg.zzb(this.mName, paramCharArrayBuffer);
  }
  
  public Player getPlayer()
  {
    return this.Ye;
  }
  
  public long getValue()
  {
    return this.cf;
  }
  
  public int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    return zza(this);
  }
  
  public boolean isDataValid()
  {
    return true;
  }
  
  public boolean isVisible()
  {
    return this.zzatq;
  }
  
  public String toString()
  {
    return zzb(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    EventEntityCreator.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/event/EventEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */