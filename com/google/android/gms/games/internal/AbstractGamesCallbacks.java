package com.google.android.gms.games.internal;

import android.net.Uri;
import android.os.Bundle;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.Contents;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMessage;
import com.google.android.gms.games.video.VideoCapabilities;

public abstract class AbstractGamesCallbacks
  extends IGamesCallbacks.Stub
{
  public void onCaptureOverlayStateChanged(int paramInt) {}
  
  public void onInvitationRemoved(String paramString) {}
  
  public void onLeftRoom(int paramInt, String paramString) {}
  
  public void onP2PConnected(String paramString) {}
  
  public void onP2PDisconnected(String paramString) {}
  
  public void onRealTimeMessageReceived(RealTimeMessage paramRealTimeMessage) {}
  
  public void onRequestRemoved(String paramString) {}
  
  public void onTurnBasedMatchRemoved(String paramString) {}
  
  public void zza(int paramInt, Uri paramUri) {}
  
  public void zza(int paramInt, VideoCapabilities paramVideoCapabilities) {}
  
  public void zza(int paramInt, String paramString, boolean paramBoolean) {}
  
  public void zza(int paramInt, boolean paramBoolean1, boolean paramBoolean2) {}
  
  public void zza(DataHolder paramDataHolder1, DataHolder paramDataHolder2) {}
  
  public void zza(DataHolder paramDataHolder, Contents paramContents) {}
  
  public void zza(DataHolder paramDataHolder, String paramString, Contents paramContents1, Contents paramContents2, Contents paramContents3) {}
  
  public void zza(DataHolder paramDataHolder, String[] paramArrayOfString) {}
  
  public void zza(DataHolder[] paramArrayOfDataHolder) {}
  
  public void zzaa(DataHolder paramDataHolder) {}
  
  public void zzab(DataHolder paramDataHolder) {}
  
  public void zzac(DataHolder paramDataHolder) {}
  
  public void zzad(DataHolder paramDataHolder) {}
  
  public void zzae(DataHolder paramDataHolder) {}
  
  public void zzaf(DataHolder paramDataHolder) {}
  
  public void zzag(DataHolder paramDataHolder) {}
  
  public void zzah(DataHolder paramDataHolder) {}
  
  public void zzai(DataHolder paramDataHolder) {}
  
  public void zzaj(DataHolder paramDataHolder) {}
  
  public void zzak(DataHolder paramDataHolder) {}
  
  public void zzal(DataHolder paramDataHolder) {}
  
  public void zzam(DataHolder paramDataHolder) {}
  
  public void zzan(DataHolder paramDataHolder) {}
  
  public void zzao(DataHolder paramDataHolder) {}
  
  public void zzap(DataHolder paramDataHolder) {}
  
  public void zzaq(DataHolder paramDataHolder) {}
  
  public void zzar(DataHolder paramDataHolder) {}
  
  public void zzas(DataHolder paramDataHolder) {}
  
  public void zzat(DataHolder paramDataHolder) {}
  
  public void zzau(DataHolder paramDataHolder) {}
  
  public void zzav(DataHolder paramDataHolder) {}
  
  public void zzaw(DataHolder paramDataHolder) {}
  
  public void zzax(DataHolder paramDataHolder) {}
  
  public void zzay(DataHolder paramDataHolder) {}
  
  public void zzaz(DataHolder paramDataHolder) {}
  
  public void zzb(int paramInt1, int paramInt2, String paramString) {}
  
  public void zzb(DataHolder paramDataHolder, String[] paramArrayOfString) {}
  
  public void zzba(DataHolder paramDataHolder) {}
  
  public void zzbb(DataHolder paramDataHolder) {}
  
  public void zzbc(DataHolder paramDataHolder) {}
  
  public void zzbd(DataHolder paramDataHolder) {}
  
  public void zzbe(DataHolder paramDataHolder) {}
  
  public void zzbf(DataHolder paramDataHolder) {}
  
  public void zzbg(DataHolder paramDataHolder) {}
  
  public void zzbh(DataHolder paramDataHolder) {}
  
  public void zzbjk() {}
  
  public void zzbm(Status paramStatus) {}
  
  public void zzbn(Status paramStatus) {}
  
  public void zzbo(Status paramStatus) {}
  
  public void zzbp(Status paramStatus) {}
  
  public void zzc(int paramInt, Bundle paramBundle) {}
  
  public void zzc(DataHolder paramDataHolder, String[] paramArrayOfString) {}
  
  public void zzd(int paramInt, Bundle paramBundle) {}
  
  public void zzd(int paramInt, String paramString1, String paramString2) {}
  
  public void zzd(int paramInt, boolean paramBoolean) {}
  
  public void zzd(DataHolder paramDataHolder, String[] paramArrayOfString) {}
  
  public void zze(int paramInt, Bundle paramBundle) {}
  
  public void zze(DataHolder paramDataHolder, String[] paramArrayOfString) {}
  
  public void zzf(int paramInt, Bundle paramBundle) {}
  
  public void zzf(DataHolder paramDataHolder, String[] paramArrayOfString) {}
  
  public void zzg(int paramInt, Bundle paramBundle) {}
  
  public void zzh(int paramInt, Bundle paramBundle) {}
  
  public void zzh(int paramInt, String paramString) {}
  
  public void zzh(DataHolder paramDataHolder) {}
  
  public void zzi(int paramInt, Bundle paramBundle) {}
  
  public void zzi(int paramInt, String paramString) {}
  
  public void zzi(DataHolder paramDataHolder) {}
  
  public void zzj(int paramInt, String paramString) {}
  
  public void zzj(DataHolder paramDataHolder) {}
  
  public void zzk(int paramInt, String paramString) {}
  
  public void zzk(DataHolder paramDataHolder) {}
  
  public void zzl(int paramInt, String paramString) {}
  
  public void zzl(DataHolder paramDataHolder) {}
  
  public void zzm(DataHolder paramDataHolder) {}
  
  public void zzn(DataHolder paramDataHolder) {}
  
  public void zzo(DataHolder paramDataHolder) {}
  
  public void zzp(DataHolder paramDataHolder) {}
  
  public void zzq(DataHolder paramDataHolder) {}
  
  public void zzr(DataHolder paramDataHolder) {}
  
  public void zzrh(int paramInt) {}
  
  public void zzri(int paramInt) {}
  
  public void zzrj(int paramInt) {}
  
  public void zzrk(int paramInt) {}
  
  public void zzrl(int paramInt) {}
  
  public void zzrm(int paramInt) {}
  
  public void zzrn(int paramInt) {}
  
  public void zzro(int paramInt) {}
  
  public void zzrp(int paramInt) {}
  
  public void zzrq(int paramInt) {}
  
  public void zzs(DataHolder paramDataHolder) {}
  
  public void zzt(DataHolder paramDataHolder) {}
  
  public void zzu(DataHolder paramDataHolder) {}
  
  public void zzv(DataHolder paramDataHolder) {}
  
  public void zzw(DataHolder paramDataHolder) {}
  
  public void zzx(DataHolder paramDataHolder) {}
  
  public void zzy(DataHolder paramDataHolder) {}
  
  public void zzz(DataHolder paramDataHolder) {}
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/internal/AbstractGamesCallbacks.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */