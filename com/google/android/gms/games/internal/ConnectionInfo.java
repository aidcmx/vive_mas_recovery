package com.google.android.gms.games.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public class ConnectionInfo
  implements SafeParcelable
{
  public static final Parcelable.Creator<ConnectionInfo> CREATOR = new ConnectionInfoCreator();
  private final String YJ;
  private final int YK;
  private final int mVersionCode;
  
  public ConnectionInfo(int paramInt1, String paramString, int paramInt2)
  {
    this.mVersionCode = paramInt1;
    this.YJ = paramString;
    this.YK = paramInt2;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    ConnectionInfoCreator.zza(this, paramParcel, paramInt);
  }
  
  public String zzbjm()
  {
    return this.YJ;
  }
  
  public int zzbjn()
  {
    return this.YK;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/internal/ConnectionInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */