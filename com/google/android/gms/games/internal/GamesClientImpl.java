package com.google.android.gms.games.internal;

import android.accounts.Account;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.view.View;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.BitmapTeleporter;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.BinderWrapper;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zzc;
import com.google.android.gms.common.internal.zze.zzf;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzj;
import com.google.android.gms.drive.Contents;
import com.google.android.gms.games.GameBuffer;
import com.google.android.gms.games.GameEntity;
import com.google.android.gms.games.Games.BaseGamesApiMethodImpl;
import com.google.android.gms.games.Games.GamesOptions;
import com.google.android.gms.games.Games.GetServerAuthCodeResult;
import com.google.android.gms.games.Games.GetTokenResult;
import com.google.android.gms.games.GamesMetadata.LoadGameInstancesResult;
import com.google.android.gms.games.GamesMetadata.LoadGameSearchSuggestionsResult;
import com.google.android.gms.games.GamesMetadata.LoadGamesResult;
import com.google.android.gms.games.GamesStatusCodes;
import com.google.android.gms.games.Notifications.ContactSettingLoadResult;
import com.google.android.gms.games.Notifications.GameMuteStatusChangeResult;
import com.google.android.gms.games.Notifications.GameMuteStatusLoadResult;
import com.google.android.gms.games.Notifications.InboxCountResult;
import com.google.android.gms.games.OnNearbyPlayerDetectedListener;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerBuffer;
import com.google.android.gms.games.PlayerEntity;
import com.google.android.gms.games.Players.LoadPlayersResult;
import com.google.android.gms.games.Players.LoadProfileSettingsResult;
import com.google.android.gms.games.Players.LoadStockProfileImagesResult;
import com.google.android.gms.games.Players.LoadXpForGameCategoriesResult;
import com.google.android.gms.games.Players.LoadXpStreamResult;
import com.google.android.gms.games.Players.UpdateGamerProfileResult;
import com.google.android.gms.games.achievement.AchievementBuffer;
import com.google.android.gms.games.achievement.Achievements.LoadAchievementsResult;
import com.google.android.gms.games.achievement.Achievements.UpdateAchievementResult;
import com.google.android.gms.games.appcontent.AppContents.LoadAppContentResult;
import com.google.android.gms.games.event.EventBuffer;
import com.google.android.gms.games.event.Events.LoadEventsResult;
import com.google.android.gms.games.internal.constants.RequestType;
import com.google.android.gms.games.internal.events.EventIncrementCache;
import com.google.android.gms.games.internal.events.EventIncrementManager;
import com.google.android.gms.games.internal.experience.ExperienceEventBuffer;
import com.google.android.gms.games.internal.game.GameInstanceBuffer;
import com.google.android.gms.games.internal.game.GameSearchSuggestionBuffer;
import com.google.android.gms.games.internal.player.ProfileSettingsEntity;
import com.google.android.gms.games.internal.player.StockProfileImageBuffer;
import com.google.android.gms.games.internal.request.RequestUpdateOutcomes;
import com.google.android.gms.games.leaderboard.Leaderboard;
import com.google.android.gms.games.leaderboard.LeaderboardBuffer;
import com.google.android.gms.games.leaderboard.LeaderboardEntity;
import com.google.android.gms.games.leaderboard.LeaderboardScore;
import com.google.android.gms.games.leaderboard.LeaderboardScoreBuffer;
import com.google.android.gms.games.leaderboard.LeaderboardScoreBufferHeader;
import com.google.android.gms.games.leaderboard.LeaderboardScoreEntity;
import com.google.android.gms.games.leaderboard.Leaderboards.LeaderboardMetadataResult;
import com.google.android.gms.games.leaderboard.Leaderboards.LoadPlayerScoreResult;
import com.google.android.gms.games.leaderboard.Leaderboards.LoadScoresResult;
import com.google.android.gms.games.leaderboard.Leaderboards.SubmitScoreResult;
import com.google.android.gms.games.leaderboard.ScoreSubmissionData;
import com.google.android.gms.games.multiplayer.Invitation;
import com.google.android.gms.games.multiplayer.InvitationBuffer;
import com.google.android.gms.games.multiplayer.Invitations.LoadInvitationsResult;
import com.google.android.gms.games.multiplayer.OnInvitationReceivedListener;
import com.google.android.gms.games.multiplayer.ParticipantResult;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMessage;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMessageReceivedListener;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMultiplayer.ReliableMessageSentCallback;
import com.google.android.gms.games.multiplayer.realtime.Room;
import com.google.android.gms.games.multiplayer.realtime.RoomBuffer;
import com.google.android.gms.games.multiplayer.realtime.RoomConfig;
import com.google.android.gms.games.multiplayer.realtime.RoomEntity;
import com.google.android.gms.games.multiplayer.realtime.RoomStatusUpdateListener;
import com.google.android.gms.games.multiplayer.realtime.RoomUpdateListener;
import com.google.android.gms.games.multiplayer.turnbased.LoadMatchesResponse;
import com.google.android.gms.games.multiplayer.turnbased.OnTurnBasedMatchUpdateReceivedListener;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatchBuffer;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatchConfig;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer.CancelMatchResult;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer.InitiateMatchResult;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer.LeaveMatchResult;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer.LoadMatchResult;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer.LoadMatchesResult;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer.UpdateMatchResult;
import com.google.android.gms.games.quest.Milestone;
import com.google.android.gms.games.quest.Quest;
import com.google.android.gms.games.quest.QuestBuffer;
import com.google.android.gms.games.quest.QuestUpdateListener;
import com.google.android.gms.games.quest.Quests.AcceptQuestResult;
import com.google.android.gms.games.quest.Quests.ClaimMilestoneResult;
import com.google.android.gms.games.quest.Quests.LoadQuestsResult;
import com.google.android.gms.games.request.GameRequest;
import com.google.android.gms.games.request.GameRequestBuffer;
import com.google.android.gms.games.request.OnRequestReceivedListener;
import com.google.android.gms.games.request.Requests.LoadRequestSummariesResult;
import com.google.android.gms.games.request.Requests.LoadRequestsResult;
import com.google.android.gms.games.request.Requests.SendRequestResult;
import com.google.android.gms.games.request.Requests.UpdateRequestsResult;
import com.google.android.gms.games.snapshot.Snapshot;
import com.google.android.gms.games.snapshot.SnapshotContents;
import com.google.android.gms.games.snapshot.SnapshotContentsEntity;
import com.google.android.gms.games.snapshot.SnapshotEntity;
import com.google.android.gms.games.snapshot.SnapshotMetadata;
import com.google.android.gms.games.snapshot.SnapshotMetadataBuffer;
import com.google.android.gms.games.snapshot.SnapshotMetadataChange;
import com.google.android.gms.games.snapshot.SnapshotMetadataChangeEntity;
import com.google.android.gms.games.snapshot.SnapshotMetadataEntity;
import com.google.android.gms.games.snapshot.Snapshots.CommitSnapshotResult;
import com.google.android.gms.games.snapshot.Snapshots.DeleteSnapshotResult;
import com.google.android.gms.games.snapshot.Snapshots.LoadSnapshotsResult;
import com.google.android.gms.games.snapshot.Snapshots.OpenSnapshotResult;
import com.google.android.gms.games.social.OnSocialInviteUpdateReceivedListener;
import com.google.android.gms.games.social.Social.InviteUpdateResult;
import com.google.android.gms.games.social.Social.LoadInvitesResult;
import com.google.android.gms.games.social.SocialInvite;
import com.google.android.gms.games.social.SocialInviteBuffer;
import com.google.android.gms.games.stats.PlayerStats;
import com.google.android.gms.games.stats.Stats.LoadPlayerStatsResult;
import com.google.android.gms.games.video.CaptureState;
import com.google.android.gms.games.video.VideoBuffer;
import com.google.android.gms.games.video.VideoCapabilities;
import com.google.android.gms.games.video.VideoConfiguration;
import com.google.android.gms.games.video.Videos.CaptureAvailableResult;
import com.google.android.gms.games.video.Videos.CaptureCapabilitiesResult;
import com.google.android.gms.games.video.Videos.CaptureOverlayStateListener;
import com.google.android.gms.games.video.Videos.CaptureRuntimeErrorCallback;
import com.google.android.gms.games.video.Videos.CaptureStateResult;
import com.google.android.gms.games.video.Videos.CaptureStoppedResult;
import com.google.android.gms.games.video.Videos.CaptureStreamingAvailabilityResult;
import com.google.android.gms.games.video.Videos.CaptureStreamingMetadataResult;
import com.google.android.gms.games.video.Videos.CaptureStreamingUrlResult;
import com.google.android.gms.games.video.Videos.ListVideosResult;
import com.google.android.gms.internal.zzqo.zzb;
import com.google.android.gms.internal.zzqx;
import com.google.android.gms.internal.zzqy;
import com.google.android.gms.internal.zzrr;
import com.google.android.gms.internal.zzrr.zzc;
import com.google.android.gms.signin.internal.zzg;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;

public final class GamesClientImpl
  extends zzj<IGamesService>
{
  EventIncrementManager YL = new EventIncrementManager()
  {
    public EventIncrementCache zzbkn()
    {
      return new GamesClientImpl.GameClientEventIncrementCache(GamesClientImpl.this);
    }
  };
  private final String YM;
  private PlayerEntity YN;
  private GameEntity YO;
  private final PopupManager YP;
  private boolean YQ = false;
  private final Binder YR;
  private final long YS;
  private final Games.GamesOptions YT;
  
  public GamesClientImpl(Context paramContext, Looper paramLooper, zzf paramzzf, Games.GamesOptions paramGamesOptions, GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
  {
    super(paramContext, paramLooper, 1, paramzzf, paramConnectionCallbacks, paramOnConnectionFailedListener);
    this.YM = paramzzf.zzavs();
    this.YR = new Binder();
    this.YP = PopupManager.zza(this, paramzzf.zzavo());
    zzw(paramzzf.zzavu());
    this.YS = hashCode();
    this.YT = paramGamesOptions;
  }
  
  private void zzb(RemoteException paramRemoteException)
  {
    GamesLog.zzc("GamesClientImpl", "service died", paramRemoteException);
  }
  
  private static Room zzbi(DataHolder paramDataHolder)
  {
    RoomBuffer localRoomBuffer = new RoomBuffer(paramDataHolder);
    paramDataHolder = null;
    try
    {
      if (localRoomBuffer.getCount() > 0) {
        paramDataHolder = (Room)((Room)localRoomBuffer.get(0)).freeze();
      }
      return paramDataHolder;
    }
    finally
    {
      localRoomBuffer.release();
    }
  }
  
  private void zzbjo()
  {
    this.YN = null;
    this.YO = null;
  }
  
  public void disconnect()
  {
    this.YQ = false;
    if (isConnected()) {}
    try
    {
      IGamesService localIGamesService = (IGamesService)zzavg();
      localIGamesService.zzbkm();
      this.YL.flush();
      localIGamesService.zzam(this.YS);
      super.disconnect();
      return;
    }
    catch (RemoteException localRemoteException)
    {
      for (;;)
      {
        GamesLog.zzae("GamesClientImpl", "Failed to notify client disconnect.");
      }
    }
  }
  
  public void onConnectionFailed(ConnectionResult paramConnectionResult)
  {
    super.onConnectionFailed(paramConnectionResult);
    this.YQ = false;
  }
  
  public int zza(zzrr<RealTimeMultiplayer.ReliableMessageSentCallback> paramzzrr, byte[] paramArrayOfByte, String paramString1, String paramString2)
  {
    try
    {
      int i = ((IGamesService)zzavg()).zza(new RealTimeReliableMessageBinderCallbacks(paramzzrr), paramArrayOfByte, paramString1, paramString2);
      return i;
    }
    catch (RemoteException paramzzrr)
    {
      zzb(paramzzrr);
    }
    return -1;
  }
  
  public int zza(byte[] paramArrayOfByte, String paramString, String[] paramArrayOfString)
  {
    zzaa.zzb(paramArrayOfString, "Participant IDs must not be null");
    try
    {
      int i = ((IGamesService)zzavg()).zzb(paramArrayOfByte, paramString, paramArrayOfString);
      return i;
    }
    catch (RemoteException paramArrayOfByte)
    {
      zzb(paramArrayOfByte);
    }
    return -1;
  }
  
  public Intent zza(int paramInt1, byte[] paramArrayOfByte, int paramInt2, Bitmap paramBitmap, String paramString)
  {
    try
    {
      paramArrayOfByte = ((IGamesService)zzavg()).zza(paramInt1, paramArrayOfByte, paramInt2, paramString);
      zzaa.zzb(paramBitmap, "Must provide a non null icon");
      paramArrayOfByte.putExtra("com.google.android.gms.games.REQUEST_ITEM_ICON", paramBitmap);
      return paramArrayOfByte;
    }
    catch (RemoteException paramArrayOfByte)
    {
      zzb(paramArrayOfByte);
    }
    return null;
  }
  
  public Intent zza(PlayerEntity paramPlayerEntity)
  {
    try
    {
      paramPlayerEntity = ((IGamesService)zzavg()).zza(paramPlayerEntity);
      return paramPlayerEntity;
    }
    catch (RemoteException paramPlayerEntity)
    {
      zzb(paramPlayerEntity);
    }
    return null;
  }
  
  public Intent zza(Room paramRoom, int paramInt)
  {
    try
    {
      paramRoom = ((IGamesService)zzavg()).zza((RoomEntity)paramRoom.freeze(), paramInt);
      return paramRoom;
    }
    catch (RemoteException paramRoom)
    {
      zzb(paramRoom);
    }
    return null;
  }
  
  public Intent zza(String paramString, boolean paramBoolean1, boolean paramBoolean2, int paramInt)
  {
    try
    {
      paramString = ((IGamesService)zzavg()).zza(paramString, paramBoolean1, paramBoolean2, paramInt);
      return paramString;
    }
    catch (RemoteException paramString)
    {
      zzb(paramString);
    }
    return null;
  }
  
  protected void zza(int paramInt1, IBinder paramIBinder, Bundle paramBundle, int paramInt2)
  {
    if ((paramInt1 == 0) && (paramBundle != null))
    {
      paramBundle.setClassLoader(GamesClientImpl.class.getClassLoader());
      this.YQ = paramBundle.getBoolean("show_welcome_popup");
      this.YN = ((PlayerEntity)paramBundle.getParcelable("com.google.android.gms.games.current_player"));
      this.YO = ((GameEntity)paramBundle.getParcelable("com.google.android.gms.games.current_game"));
    }
    super.zza(paramInt1, paramIBinder, paramBundle, paramInt2);
  }
  
  public void zza(Account paramAccount, byte[] paramArrayOfByte)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zza(paramAccount, paramArrayOfByte);
  }
  
  public void zza(IBinder paramIBinder, Bundle paramBundle)
  {
    if (isConnected()) {}
    try
    {
      ((IGamesService)zzavg()).zza(paramIBinder, paramBundle);
      return;
    }
    catch (RemoteException paramIBinder)
    {
      zzb(paramIBinder);
    }
  }
  
  public void zza(zze.zzf paramzzf)
  {
    zzbjo();
    super.zza(paramzzf);
  }
  
  public void zza(Games.BaseGamesApiMethodImpl<Status> paramBaseGamesApiMethodImpl, String paramString, VideoConfiguration paramVideoConfiguration)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zza(new CaptureLaunchGameAndOverlayBinderCallback(paramBaseGamesApiMethodImpl), paramString, paramVideoConfiguration);
  }
  
  public void zza(@NonNull IGamesService paramIGamesService)
  {
    super.zza(paramIGamesService);
    if (this.YQ)
    {
      this.YP.zzblb();
      this.YQ = false;
    }
    if ((!this.YT.Xp) && (!this.YT.Xx)) {
      zzb(paramIGamesService);
    }
  }
  
  public void zza(Snapshot paramSnapshot)
  {
    paramSnapshot = paramSnapshot.getSnapshotContents();
    if (!paramSnapshot.isClosed()) {}
    for (boolean bool = true;; bool = false)
    {
      zzaa.zza(bool, "Snapshot already closed");
      Contents localContents = paramSnapshot.zzbar();
      paramSnapshot.close();
      try
      {
        ((IGamesService)zzavg()).zza(localContents);
        return;
      }
      catch (RemoteException paramSnapshot)
      {
        zzb(paramSnapshot);
      }
    }
  }
  
  public void zza(zzqo.zzb<Invitations.LoadInvitationsResult> paramzzb, int paramInt)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zza(new InvitationsLoadedBinderCallback(paramzzb), paramInt);
  }
  
  public void zza(zzqo.zzb<Requests.LoadRequestsResult> paramzzb, int paramInt1, int paramInt2, int paramInt3)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zza(new RequestsLoadedBinderCallbacks(paramzzb), paramInt1, paramInt2, paramInt3);
  }
  
  public void zza(zzqo.zzb<AppContents.LoadAppContentResult> paramzzb, int paramInt, String paramString, String[] paramArrayOfString, boolean paramBoolean)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zza(new AppContentLoadedBinderCallbacks(paramzzb), paramInt, paramString, paramArrayOfString, paramBoolean);
  }
  
  public void zza(zzqo.zzb<Players.LoadPlayersResult> paramzzb, int paramInt, boolean paramBoolean1, boolean paramBoolean2)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zza(new PlayersLoadedBinderCallback(paramzzb), paramInt, paramBoolean1, paramBoolean2);
  }
  
  public void zza(zzqo.zzb<TurnBasedMultiplayer.LoadMatchesResult> paramzzb, int paramInt, int[] paramArrayOfInt)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zza(new TurnBasedMatchesLoadedBinderCallbacks(paramzzb), paramInt, paramArrayOfInt);
  }
  
  public void zza(zzqo.zzb<Players.LoadPlayersResult> paramzzb, Account paramAccount)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zza(new PlayersLoadedBinderCallback(paramzzb), paramAccount);
  }
  
  public void zza(zzqo.zzb<Leaderboards.LoadScoresResult> paramzzb, LeaderboardScoreBuffer paramLeaderboardScoreBuffer, int paramInt1, int paramInt2)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zza(new LeaderboardScoresLoadedBinderCallback(paramzzb), paramLeaderboardScoreBuffer.zzbmn().asBundle(), paramInt1, paramInt2);
  }
  
  public void zza(zzqo.zzb<TurnBasedMultiplayer.InitiateMatchResult> paramzzb, TurnBasedMatchConfig paramTurnBasedMatchConfig)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zza(new TurnBasedMatchInitiatedBinderCallbacks(paramzzb), paramTurnBasedMatchConfig.getVariant(), paramTurnBasedMatchConfig.zzbmt(), paramTurnBasedMatchConfig.getInvitedPlayerIds(), paramTurnBasedMatchConfig.getAutoMatchCriteria());
  }
  
  public void zza(zzqo.zzb<Snapshots.CommitSnapshotResult> paramzzb, Snapshot paramSnapshot, SnapshotMetadataChange paramSnapshotMetadataChange)
    throws RemoteException
  {
    SnapshotContents localSnapshotContents = paramSnapshot.getSnapshotContents();
    if (!localSnapshotContents.isClosed()) {}
    for (boolean bool = true;; bool = false)
    {
      zzaa.zza(bool, "Snapshot already closed");
      Object localObject = paramSnapshotMetadataChange.zzbmy();
      if (localObject != null) {
        ((BitmapTeleporter)localObject).zzd(getContext().getCacheDir());
      }
      localObject = localSnapshotContents.zzbar();
      localSnapshotContents.close();
      ((IGamesService)zzavg()).zza(new SnapshotCommittedBinderCallbacks(paramzzb), paramSnapshot.getMetadata().getSnapshotId(), (SnapshotMetadataChangeEntity)paramSnapshotMetadataChange, (Contents)localObject);
      return;
    }
  }
  
  public void zza(zzqo.zzb<Status> paramzzb, VideoConfiguration paramVideoConfiguration, Videos.CaptureRuntimeErrorCallback paramCaptureRuntimeErrorCallback)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zza(new CaptureStartedBinderCallbacks(paramzzb, paramCaptureRuntimeErrorCallback), paramVideoConfiguration);
  }
  
  public void zza(zzqo.zzb<Achievements.UpdateAchievementResult> paramzzb, String paramString)
    throws RemoteException
  {
    if (paramzzb == null) {}
    for (paramzzb = null;; paramzzb = new AchievementUpdatedBinderCallback(paramzzb))
    {
      ((IGamesService)zzavg()).zza(paramzzb, paramString, this.YP.zzbld(), this.YP.zzblc());
      return;
    }
  }
  
  public void zza(zzqo.zzb<Achievements.UpdateAchievementResult> paramzzb, String paramString, int paramInt)
    throws RemoteException
  {
    if (paramzzb == null) {}
    for (paramzzb = null;; paramzzb = new AchievementUpdatedBinderCallback(paramzzb))
    {
      ((IGamesService)zzavg()).zza(paramzzb, paramString, paramInt, this.YP.zzbld(), this.YP.zzblc());
      return;
    }
  }
  
  public void zza(zzqo.zzb<Leaderboards.LoadScoresResult> paramzzb, String paramString, int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zza(new LeaderboardScoresLoadedBinderCallback(paramzzb), paramString, paramInt1, paramInt2, paramInt3, paramBoolean);
  }
  
  public void zza(zzqo.zzb<Players.LoadPlayersResult> paramzzb, String paramString, int paramInt, boolean paramBoolean1, boolean paramBoolean2)
    throws RemoteException
  {
    int i = -1;
    switch (paramString.hashCode())
    {
    default: 
      switch (i)
      {
      default: 
        paramzzb = String.valueOf(paramString);
        if (paramzzb.length() == 0) {}
        break;
      }
      break;
    }
    for (paramzzb = "Invalid player collection: ".concat(paramzzb);; paramzzb = new String("Invalid player collection: "))
    {
      throw new IllegalArgumentException(paramzzb);
      if (!paramString.equals("played_with")) {
        break;
      }
      i = 0;
      break;
    }
    ((IGamesService)zzavg()).zzd(new PlayersLoadedBinderCallback(paramzzb), paramString, paramInt, paramBoolean1, paramBoolean2);
  }
  
  public void zza(zzqo.zzb<TurnBasedMultiplayer.LoadMatchesResult> paramzzb, String paramString, int paramInt, int[] paramArrayOfInt)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zza(new TurnBasedMatchesLoadedBinderCallbacks(paramzzb), paramString, paramInt, paramArrayOfInt);
  }
  
  public void zza(zzqo.zzb<Leaderboards.SubmitScoreResult> paramzzb, String paramString1, long paramLong, String paramString2)
    throws RemoteException
  {
    if (paramzzb == null) {}
    for (paramzzb = null;; paramzzb = new SubmitScoreBinderCallbacks(paramzzb))
    {
      ((IGamesService)zzavg()).zza(paramzzb, paramString1, paramLong, paramString2);
      return;
    }
  }
  
  public void zza(zzqo.zzb<TurnBasedMultiplayer.LeaveMatchResult> paramzzb, String paramString1, String paramString2)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzc(new TurnBasedMatchLeftBinderCallbacks(paramzzb), paramString1, paramString2);
  }
  
  public void zza(zzqo.zzb<Leaderboards.LoadPlayerScoreResult> paramzzb, String paramString1, String paramString2, int paramInt1, int paramInt2)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zza(new PlayerLeaderboardScoreLoadedBinderCallback(paramzzb), paramString1, paramString2, paramInt1, paramInt2);
  }
  
  public void zza(zzqo.zzb<Requests.LoadRequestsResult> paramzzb, String paramString1, String paramString2, int paramInt1, int paramInt2, int paramInt3)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zza(new RequestsLoadedBinderCallbacks(paramzzb), paramString1, paramString2, paramInt1, paramInt2, paramInt3);
  }
  
  public void zza(zzqo.zzb<Leaderboards.LoadScoresResult> paramzzb, String paramString1, String paramString2, int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zza(new LeaderboardScoresLoadedBinderCallback(paramzzb), paramString1, paramString2, paramInt1, paramInt2, paramInt3, paramBoolean);
  }
  
  public void zza(zzqo.zzb<Players.LoadPlayersResult> paramzzb, String paramString1, String paramString2, int paramInt, boolean paramBoolean1, boolean paramBoolean2)
    throws RemoteException
  {
    int i = -1;
    switch (paramString1.hashCode())
    {
    default: 
      switch (i)
      {
      default: 
        paramzzb = String.valueOf(paramString1);
        if (paramzzb.length() == 0) {}
        break;
      }
      break;
    }
    for (paramzzb = "Invalid player collection: ".concat(paramzzb);; paramzzb = new String("Invalid player collection: "))
    {
      throw new IllegalArgumentException(paramzzb);
      if (!paramString1.equals("circled")) {
        break;
      }
      i = 0;
      break;
      if (!paramString1.equals("played_with")) {
        break;
      }
      i = 1;
      break;
      if (!paramString1.equals("nearby")) {
        break;
      }
      i = 2;
      break;
    }
    ((IGamesService)zzavg()).zza(new PlayersLoadedBinderCallback(paramzzb), paramString1, paramString2, paramInt, paramBoolean1, paramBoolean2);
  }
  
  public void zza(zzqo.zzb<Snapshots.OpenSnapshotResult> paramzzb, String paramString1, String paramString2, SnapshotMetadataChange paramSnapshotMetadataChange, SnapshotContents paramSnapshotContents)
    throws RemoteException
  {
    if (!paramSnapshotContents.isClosed()) {}
    for (boolean bool = true;; bool = false)
    {
      zzaa.zza(bool, "SnapshotContents already closed");
      Object localObject = paramSnapshotMetadataChange.zzbmy();
      if (localObject != null) {
        ((BitmapTeleporter)localObject).zzd(getContext().getCacheDir());
      }
      localObject = paramSnapshotContents.zzbar();
      paramSnapshotContents.close();
      ((IGamesService)zzavg()).zza(new SnapshotOpenedBinderCallbacks(paramzzb), paramString1, paramString2, (SnapshotMetadataChangeEntity)paramSnapshotMetadataChange, (Contents)localObject);
      return;
    }
  }
  
  public void zza(zzqo.zzb<Leaderboards.LeaderboardMetadataResult> paramzzb, String paramString1, String paramString2, boolean paramBoolean)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzb(new LeaderboardsLoadedBinderCallback(paramzzb), paramString1, paramString2, paramBoolean);
  }
  
  public void zza(zzqo.zzb<Quests.LoadQuestsResult> paramzzb, String paramString1, String paramString2, boolean paramBoolean, String[] paramArrayOfString)
    throws RemoteException
  {
    this.YL.flush();
    ((IGamesService)zzavg()).zza(new QuestsLoadedBinderCallbacks(paramzzb), paramString1, paramString2, paramArrayOfString, paramBoolean);
  }
  
  public void zza(zzqo.zzb<Quests.LoadQuestsResult> paramzzb, String paramString1, String paramString2, int[] paramArrayOfInt, int paramInt, boolean paramBoolean)
    throws RemoteException
  {
    this.YL.flush();
    ((IGamesService)zzavg()).zza(new QuestsLoadedBinderCallbacks(paramzzb), paramString1, paramString2, paramArrayOfInt, paramInt, paramBoolean);
  }
  
  public void zza(zzqo.zzb<Requests.UpdateRequestsResult> paramzzb, String paramString1, String paramString2, String[] paramArrayOfString)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zza(new RequestsUpdatedBinderCallbacks(paramzzb), paramString1, paramString2, paramArrayOfString);
  }
  
  public void zza(zzqo.zzb<Players.LoadPlayersResult> paramzzb, String paramString, boolean paramBoolean)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzf(new PlayersLoadedBinderCallback(paramzzb), paramString, paramBoolean);
  }
  
  public void zza(zzqo.zzb<Snapshots.OpenSnapshotResult> paramzzb, String paramString, boolean paramBoolean, int paramInt)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zza(new SnapshotOpenedBinderCallbacks(paramzzb), paramString, paramBoolean, paramInt);
  }
  
  public void zza(zzqo.zzb<Players.UpdateGamerProfileResult> paramzzb, String paramString1, boolean paramBoolean1, String paramString2, boolean paramBoolean2, boolean paramBoolean3)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zza(new GamerProfileUpdatedBinderCallback(paramzzb), paramString1, paramBoolean1, paramString2, paramBoolean2, paramBoolean3);
  }
  
  public void zza(zzqo.zzb<TurnBasedMultiplayer.UpdateMatchResult> paramzzb, String paramString1, byte[] paramArrayOfByte, String paramString2, ParticipantResult[] paramArrayOfParticipantResult)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zza(new TurnBasedMatchUpdatedBinderCallbacks(paramzzb), paramString1, paramArrayOfByte, paramString2, paramArrayOfParticipantResult);
  }
  
  public void zza(zzqo.zzb<TurnBasedMultiplayer.UpdateMatchResult> paramzzb, String paramString, byte[] paramArrayOfByte, ParticipantResult[] paramArrayOfParticipantResult)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zza(new TurnBasedMatchUpdatedBinderCallbacks(paramzzb), paramString, paramArrayOfByte, paramArrayOfParticipantResult);
  }
  
  public void zza(zzqo.zzb<Requests.SendRequestResult> paramzzb, String paramString, String[] paramArrayOfString, int paramInt1, byte[] paramArrayOfByte, int paramInt2)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zza(new RequestSentBinderCallbacks(paramzzb), paramString, paramArrayOfString, paramInt1, paramArrayOfByte, paramInt2);
  }
  
  public void zza(zzqo.zzb<Players.LoadPlayersResult> paramzzb, boolean paramBoolean)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzc(new PlayersLoadedBinderCallback(paramzzb), paramBoolean);
  }
  
  public void zza(zzqo.zzb<Players.LoadProfileSettingsResult> paramzzb, boolean paramBoolean1, boolean paramBoolean2)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zza(new ProfileSettingsLoadedBinderCallback(paramzzb), paramBoolean1, paramBoolean2);
  }
  
  public void zza(zzqo.zzb<Status> paramzzb, boolean paramBoolean1, boolean paramBoolean2, Bundle paramBundle)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zza(new ContactSettingsUpdatedBinderCallback(paramzzb), paramBoolean1, paramBoolean2, paramBundle);
  }
  
  public void zza(zzqo.zzb<Events.LoadEventsResult> paramzzb, boolean paramBoolean, String... paramVarArgs)
    throws RemoteException
  {
    this.YL.flush();
    ((IGamesService)zzavg()).zza(new EventsLoadedBinderCallback(paramzzb), paramBoolean, paramVarArgs);
  }
  
  public void zza(zzqo.zzb<Quests.LoadQuestsResult> paramzzb, int[] paramArrayOfInt, int paramInt, boolean paramBoolean)
    throws RemoteException
  {
    this.YL.flush();
    ((IGamesService)zzavg()).zza(new QuestsLoadedBinderCallbacks(paramzzb), paramArrayOfInt, paramInt, paramBoolean);
  }
  
  public void zza(zzqo.zzb<Players.LoadPlayersResult> paramzzb, String[] paramArrayOfString)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzc(new PlayersLoadedBinderCallback(paramzzb), paramArrayOfString);
  }
  
  public void zza(zzrr<OnInvitationReceivedListener> paramzzrr)
  {
    try
    {
      paramzzrr = new InvitationReceivedBinderCallback(paramzzrr);
      ((IGamesService)zzavg()).zza(paramzzrr, this.YS);
      return;
    }
    catch (RemoteException paramzzrr)
    {
      zzb(paramzzrr);
    }
  }
  
  public void zza(zzrr<RoomUpdateListener> paramzzrr, zzrr<RoomStatusUpdateListener> paramzzrr1, zzrr<RealTimeMessageReceivedListener> paramzzrr2, RoomConfig paramRoomConfig)
  {
    try
    {
      paramzzrr = new RoomBinderCallbacks(paramzzrr, paramzzrr1, paramzzrr2);
      ((IGamesService)zzavg()).zza(paramzzrr, this.YR, paramRoomConfig.getVariant(), paramRoomConfig.getInvitedPlayerIds(), paramRoomConfig.getAutoMatchCriteria(), false, this.YS);
      return;
    }
    catch (RemoteException paramzzrr)
    {
      zzb(paramzzrr);
    }
  }
  
  public void zza(zzrr<RoomUpdateListener> paramzzrr, String paramString)
  {
    try
    {
      ((IGamesService)zzavg()).zzc(new RoomBinderCallbacks(paramzzrr), paramString);
      return;
    }
    catch (RemoteException paramzzrr)
    {
      zzb(paramzzrr);
    }
  }
  
  protected Bundle zzahv()
  {
    String str = getContext().getResources().getConfiguration().locale.toString();
    Bundle localBundle = this.YT.zzbhp();
    localBundle.putString("com.google.android.gms.games.key.gamePackageName", this.YM);
    localBundle.putString("com.google.android.gms.games.key.desiredLocale", str);
    localBundle.putParcelable("com.google.android.gms.games.key.popupWindowToken", new BinderWrapper(this.YP.zzbld()));
    localBundle.putInt("com.google.android.gms.games.key.API_VERSION", 4);
    localBundle.putBundle("com.google.android.gms.games.key.signInOptions", zzg.zza(zzawb()));
    return localBundle;
  }
  
  public boolean zzain()
  {
    return true;
  }
  
  public Bundle zzapn()
  {
    try
    {
      Bundle localBundle = ((IGamesService)zzavg()).zzapn();
      if (localBundle != null) {
        localBundle.setClassLoader(GamesClientImpl.class.getClassLoader());
      }
      return localBundle;
    }
    catch (RemoteException localRemoteException)
    {
      zzb(localRemoteException);
    }
    return null;
  }
  
  public Intent zzb(int paramInt1, int paramInt2, boolean paramBoolean)
  {
    try
    {
      Intent localIntent = ((IGamesService)zzavg()).zzb(paramInt1, paramInt2, paramBoolean);
      return localIntent;
    }
    catch (RemoteException localRemoteException)
    {
      zzb(localRemoteException);
    }
    return null;
  }
  
  public void zzb(IGamesService paramIGamesService)
  {
    try
    {
      paramIGamesService.zza(new PopupLocationInfoBinderCallbacks(this.YP), this.YS);
      return;
    }
    catch (RemoteException paramIGamesService)
    {
      zzb(paramIGamesService);
    }
  }
  
  public void zzb(zzqo.zzb<Videos.CaptureAvailableResult> paramzzb, int paramInt)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzc(new CaptureAvailableBinderCallback(paramzzb), paramInt);
  }
  
  public void zzb(zzqo.zzb<Players.LoadPlayersResult> paramzzb, int paramInt, boolean paramBoolean1, boolean paramBoolean2)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzb(new PlayersLoadedBinderCallback(paramzzb), paramInt, paramBoolean1, paramBoolean2);
  }
  
  public void zzb(zzqo.zzb<Achievements.UpdateAchievementResult> paramzzb, String paramString)
    throws RemoteException
  {
    if (paramzzb == null) {}
    for (paramzzb = null;; paramzzb = new AchievementUpdatedBinderCallback(paramzzb))
    {
      ((IGamesService)zzavg()).zzb(paramzzb, paramString, this.YP.zzbld(), this.YP.zzblc());
      return;
    }
  }
  
  public void zzb(zzqo.zzb<Achievements.UpdateAchievementResult> paramzzb, String paramString, int paramInt)
    throws RemoteException
  {
    if (paramzzb == null) {}
    for (paramzzb = null;; paramzzb = new AchievementUpdatedBinderCallback(paramzzb))
    {
      ((IGamesService)zzavg()).zzb(paramzzb, paramString, paramInt, this.YP.zzbld(), this.YP.zzblc());
      return;
    }
  }
  
  public void zzb(zzqo.zzb<Leaderboards.LoadScoresResult> paramzzb, String paramString, int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzb(new LeaderboardScoresLoadedBinderCallback(paramzzb), paramString, paramInt1, paramInt2, paramInt3, paramBoolean);
  }
  
  public void zzb(zzqo.zzb<Players.LoadPlayersResult> paramzzb, String paramString, int paramInt, boolean paramBoolean1, boolean paramBoolean2)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzb(new PlayersLoadedBinderCallback(paramzzb), paramString, paramInt, paramBoolean1, paramBoolean2);
  }
  
  public void zzb(zzqo.zzb<Quests.ClaimMilestoneResult> paramzzb, String paramString1, String paramString2)
    throws RemoteException
  {
    this.YL.flush();
    ((IGamesService)zzavg()).zzf(new QuestMilestoneClaimBinderCallbacks(paramzzb, paramString2), paramString1, paramString2);
  }
  
  public void zzb(zzqo.zzb<Leaderboards.LoadScoresResult> paramzzb, String paramString1, String paramString2, int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzb(new LeaderboardScoresLoadedBinderCallback(paramzzb), paramString1, paramString2, paramInt1, paramInt2, paramInt3, paramBoolean);
  }
  
  public void zzb(zzqo.zzb<Players.LoadPlayersResult> paramzzb, String paramString1, String paramString2, int paramInt, boolean paramBoolean1, boolean paramBoolean2)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzb(new PlayersLoadedBinderCallback(paramzzb), paramString1, paramString2, paramInt, paramBoolean1, paramBoolean2);
  }
  
  public void zzb(zzqo.zzb<Achievements.LoadAchievementsResult> paramzzb, String paramString1, String paramString2, boolean paramBoolean)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zza(new AchievementsLoadedBinderCallback(paramzzb), paramString1, paramString2, paramBoolean);
  }
  
  public void zzb(zzqo.zzb<Leaderboards.LeaderboardMetadataResult> paramzzb, String paramString, boolean paramBoolean)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzc(new LeaderboardsLoadedBinderCallback(paramzzb), paramString, paramBoolean);
  }
  
  public void zzb(zzqo.zzb<Leaderboards.LeaderboardMetadataResult> paramzzb, boolean paramBoolean)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzb(new LeaderboardsLoadedBinderCallback(paramzzb), paramBoolean);
  }
  
  public void zzb(zzqo.zzb<Quests.LoadQuestsResult> paramzzb, boolean paramBoolean, String[] paramArrayOfString)
    throws RemoteException
  {
    this.YL.flush();
    ((IGamesService)zzavg()).zza(new QuestsLoadedBinderCallbacks(paramzzb), paramArrayOfString, paramBoolean);
  }
  
  public void zzb(zzqo.zzb<Requests.UpdateRequestsResult> paramzzb, String[] paramArrayOfString)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zza(new RequestsUpdatedBinderCallbacks(paramzzb), paramArrayOfString);
  }
  
  public void zzb(zzrr<OnTurnBasedMatchUpdateReceivedListener> paramzzrr)
  {
    try
    {
      paramzzrr = new MatchUpdateReceivedBinderCallback(paramzzrr);
      ((IGamesService)zzavg()).zzb(paramzzrr, this.YS);
      return;
    }
    catch (RemoteException paramzzrr)
    {
      zzb(paramzzrr);
    }
  }
  
  public void zzb(zzrr<RoomUpdateListener> paramzzrr, zzrr<RoomStatusUpdateListener> paramzzrr1, zzrr<RealTimeMessageReceivedListener> paramzzrr2, RoomConfig paramRoomConfig)
  {
    try
    {
      paramzzrr = new RoomBinderCallbacks(paramzzrr, paramzzrr1, paramzzrr2);
      ((IGamesService)zzavg()).zza(paramzzrr, this.YR, paramRoomConfig.getInvitationId(), false, this.YS);
      return;
    }
    catch (RemoteException paramzzrr)
    {
      zzb(paramzzrr);
    }
  }
  
  public void zzb(String paramString, zzqo.zzb<Games.GetServerAuthCodeResult> paramzzb)
    throws RemoteException
  {
    zzaa.zzh(paramString, "Please provide a valid serverClientId");
    ((IGamesService)zzavg()).zza(paramString, new GetServerAuthCodeBinderCallbacks(paramzzb));
  }
  
  public String zzbjp()
  {
    try
    {
      String str = ((IGamesService)zzavg()).zzbjp();
      return str;
    }
    catch (RemoteException localRemoteException)
    {
      zzb(localRemoteException);
    }
    return null;
  }
  
  /* Error */
  public Player zzbjq()
  {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual 1296	com/google/android/gms/games/internal/GamesClientImpl:zzavf	()V
    //   4: aload_0
    //   5: monitorenter
    //   6: aload_0
    //   7: getfield 621	com/google/android/gms/games/internal/GamesClientImpl:YN	Lcom/google/android/gms/games/PlayerEntity;
    //   10: astore_1
    //   11: aload_1
    //   12: ifnonnull +54 -> 66
    //   15: new 1298	com/google/android/gms/games/PlayerBuffer
    //   18: dup
    //   19: aload_0
    //   20: invokevirtual 634	com/google/android/gms/games/internal/GamesClientImpl:zzavg	()Landroid/os/IInterface;
    //   23: checkcast 636	com/google/android/gms/games/internal/IGamesService
    //   26: invokeinterface 1302 1 0
    //   31: invokespecial 1303	com/google/android/gms/games/PlayerBuffer:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
    //   34: astore_1
    //   35: aload_1
    //   36: invokevirtual 1304	com/google/android/gms/games/PlayerBuffer:getCount	()I
    //   39: ifle +23 -> 62
    //   42: aload_0
    //   43: aload_1
    //   44: iconst_0
    //   45: invokevirtual 1305	com/google/android/gms/games/PlayerBuffer:get	(I)Ljava/lang/Object;
    //   48: checkcast 1307	com/google/android/gms/games/Player
    //   51: invokeinterface 1308 1 0
    //   56: checkcast 732	com/google/android/gms/games/PlayerEntity
    //   59: putfield 621	com/google/android/gms/games/internal/GamesClientImpl:YN	Lcom/google/android/gms/games/PlayerEntity;
    //   62: aload_1
    //   63: invokevirtual 1309	com/google/android/gms/games/PlayerBuffer:release	()V
    //   66: aload_0
    //   67: monitorexit
    //   68: aload_0
    //   69: getfield 621	com/google/android/gms/games/internal/GamesClientImpl:YN	Lcom/google/android/gms/games/PlayerEntity;
    //   72: areturn
    //   73: astore_2
    //   74: aload_1
    //   75: invokevirtual 1309	com/google/android/gms/games/PlayerBuffer:release	()V
    //   78: aload_2
    //   79: athrow
    //   80: astore_1
    //   81: aload_0
    //   82: aload_1
    //   83: invokespecial 582	com/google/android/gms/games/internal/GamesClientImpl:zzb	(Landroid/os/RemoteException;)V
    //   86: goto -20 -> 66
    //   89: astore_1
    //   90: aload_0
    //   91: monitorexit
    //   92: aload_1
    //   93: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	94	0	this	GamesClientImpl
    //   10	65	1	localObject1	Object
    //   80	3	1	localRemoteException	RemoteException
    //   89	4	1	localObject2	Object
    //   73	6	2	localObject3	Object
    // Exception table:
    //   from	to	target	type
    //   35	62	73	finally
    //   15	35	80	android/os/RemoteException
    //   62	66	80	android/os/RemoteException
    //   74	80	80	android/os/RemoteException
    //   6	11	89	finally
    //   15	35	89	finally
    //   62	66	89	finally
    //   66	68	89	finally
    //   74	80	89	finally
    //   81	86	89	finally
    //   90	92	89	finally
  }
  
  /* Error */
  public com.google.android.gms.games.Game zzbjr()
  {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual 1296	com/google/android/gms/games/internal/GamesClientImpl:zzavf	()V
    //   4: aload_0
    //   5: monitorenter
    //   6: aload_0
    //   7: getfield 623	com/google/android/gms/games/internal/GamesClientImpl:YO	Lcom/google/android/gms/games/GameEntity;
    //   10: astore_1
    //   11: aload_1
    //   12: ifnonnull +54 -> 66
    //   15: new 1313	com/google/android/gms/games/GameBuffer
    //   18: dup
    //   19: aload_0
    //   20: invokevirtual 634	com/google/android/gms/games/internal/GamesClientImpl:zzavg	()Landroid/os/IInterface;
    //   23: checkcast 636	com/google/android/gms/games/internal/IGamesService
    //   26: invokeinterface 1316 1 0
    //   31: invokespecial 1317	com/google/android/gms/games/GameBuffer:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
    //   34: astore_1
    //   35: aload_1
    //   36: invokevirtual 1318	com/google/android/gms/games/GameBuffer:getCount	()I
    //   39: ifle +23 -> 62
    //   42: aload_0
    //   43: aload_1
    //   44: iconst_0
    //   45: invokevirtual 1319	com/google/android/gms/games/GameBuffer:get	(I)Ljava/lang/Object;
    //   48: checkcast 1321	com/google/android/gms/games/Game
    //   51: invokeinterface 1322 1 0
    //   56: checkcast 736	com/google/android/gms/games/GameEntity
    //   59: putfield 623	com/google/android/gms/games/internal/GamesClientImpl:YO	Lcom/google/android/gms/games/GameEntity;
    //   62: aload_1
    //   63: invokevirtual 1323	com/google/android/gms/games/GameBuffer:release	()V
    //   66: aload_0
    //   67: monitorexit
    //   68: aload_0
    //   69: getfield 623	com/google/android/gms/games/internal/GamesClientImpl:YO	Lcom/google/android/gms/games/GameEntity;
    //   72: areturn
    //   73: astore_2
    //   74: aload_1
    //   75: invokevirtual 1323	com/google/android/gms/games/GameBuffer:release	()V
    //   78: aload_2
    //   79: athrow
    //   80: astore_1
    //   81: aload_0
    //   82: aload_1
    //   83: invokespecial 582	com/google/android/gms/games/internal/GamesClientImpl:zzb	(Landroid/os/RemoteException;)V
    //   86: goto -20 -> 66
    //   89: astore_1
    //   90: aload_0
    //   91: monitorexit
    //   92: aload_1
    //   93: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	94	0	this	GamesClientImpl
    //   10	65	1	localObject1	Object
    //   80	3	1	localRemoteException	RemoteException
    //   89	4	1	localObject2	Object
    //   73	6	2	localObject3	Object
    // Exception table:
    //   from	to	target	type
    //   35	62	73	finally
    //   15	35	80	android/os/RemoteException
    //   62	66	80	android/os/RemoteException
    //   74	80	80	android/os/RemoteException
    //   6	11	89	finally
    //   15	35	89	finally
    //   62	66	89	finally
    //   66	68	89	finally
    //   74	80	89	finally
    //   81	86	89	finally
    //   90	92	89	finally
  }
  
  public Intent zzbjs()
  {
    try
    {
      Intent localIntent = ((IGamesService)zzavg()).zzbjs();
      return localIntent;
    }
    catch (RemoteException localRemoteException)
    {
      zzb(localRemoteException);
    }
    return null;
  }
  
  public Intent zzbjt()
  {
    try
    {
      Intent localIntent = ((IGamesService)zzavg()).zzbjt();
      return localIntent;
    }
    catch (RemoteException localRemoteException)
    {
      zzb(localRemoteException);
    }
    return null;
  }
  
  public Intent zzbju()
  {
    try
    {
      Intent localIntent = ((IGamesService)zzavg()).zzbju();
      return localIntent;
    }
    catch (RemoteException localRemoteException)
    {
      zzb(localRemoteException);
    }
    return null;
  }
  
  public Intent zzbjv()
  {
    try
    {
      Intent localIntent = ((IGamesService)zzavg()).zzbjv();
      return localIntent;
    }
    catch (RemoteException localRemoteException)
    {
      zzb(localRemoteException);
    }
    return null;
  }
  
  public void zzbjw()
  {
    try
    {
      ((IGamesService)zzavg()).zzan(this.YS);
      return;
    }
    catch (RemoteException localRemoteException)
    {
      zzb(localRemoteException);
    }
  }
  
  public void zzbjx()
  {
    try
    {
      ((IGamesService)zzavg()).zzao(this.YS);
      return;
    }
    catch (RemoteException localRemoteException)
    {
      zzb(localRemoteException);
    }
  }
  
  public void zzbjy()
  {
    try
    {
      ((IGamesService)zzavg()).zzaq(this.YS);
      return;
    }
    catch (RemoteException localRemoteException)
    {
      zzb(localRemoteException);
    }
  }
  
  public void zzbjz()
  {
    try
    {
      ((IGamesService)zzavg()).zzap(this.YS);
      return;
    }
    catch (RemoteException localRemoteException)
    {
      zzb(localRemoteException);
    }
  }
  
  public Intent zzbka()
  {
    try
    {
      Intent localIntent = ((IGamesService)zzavg()).zzbka();
      return localIntent;
    }
    catch (RemoteException localRemoteException)
    {
      zzb(localRemoteException);
    }
    return null;
  }
  
  public Intent zzbkb()
  {
    try
    {
      Intent localIntent = ((IGamesService)zzavg()).zzbkb();
      return localIntent;
    }
    catch (RemoteException localRemoteException)
    {
      zzb(localRemoteException);
    }
    return null;
  }
  
  public int zzbkc()
  {
    try
    {
      int i = ((IGamesService)zzavg()).zzbkc();
      return i;
    }
    catch (RemoteException localRemoteException)
    {
      zzb(localRemoteException);
    }
    return 4368;
  }
  
  public int zzbkd()
  {
    try
    {
      int i = ((IGamesService)zzavg()).zzbkd();
      return i;
    }
    catch (RemoteException localRemoteException)
    {
      zzb(localRemoteException);
    }
    return -1;
  }
  
  public Intent zzbke()
  {
    try
    {
      Intent localIntent = ((IGamesService)zzavg()).zzbke();
      return localIntent;
    }
    catch (RemoteException localRemoteException)
    {
      zzb(localRemoteException);
    }
    return null;
  }
  
  public int zzbkf()
  {
    try
    {
      int i = ((IGamesService)zzavg()).zzbkf();
      return i;
    }
    catch (RemoteException localRemoteException)
    {
      zzb(localRemoteException);
    }
    return -1;
  }
  
  public int zzbkg()
  {
    try
    {
      int i = ((IGamesService)zzavg()).zzbkg();
      return i;
    }
    catch (RemoteException localRemoteException)
    {
      zzb(localRemoteException);
    }
    return -1;
  }
  
  public int zzbkh()
  {
    try
    {
      int i = ((IGamesService)zzavg()).zzbkh();
      return i;
    }
    catch (RemoteException localRemoteException)
    {
      zzb(localRemoteException);
    }
    return -1;
  }
  
  public int zzbki()
  {
    try
    {
      int i = ((IGamesService)zzavg()).zzbki();
      return i;
    }
    catch (RemoteException localRemoteException)
    {
      zzb(localRemoteException);
    }
    return -1;
  }
  
  public Intent zzbkj()
  {
    try
    {
      Intent localIntent = ((IGamesService)zzavg()).zzbkx();
      return localIntent;
    }
    catch (RemoteException localRemoteException)
    {
      zzb(localRemoteException);
    }
    return null;
  }
  
  public boolean zzbkk()
  {
    try
    {
      boolean bool = ((IGamesService)zzavg()).zzbkk();
      return bool;
    }
    catch (RemoteException localRemoteException)
    {
      zzb(localRemoteException);
    }
    return false;
  }
  
  public void zzbkl()
  {
    try
    {
      ((IGamesService)zzavg()).zzat(this.YS);
      return;
    }
    catch (RemoteException localRemoteException)
    {
      zzb(localRemoteException);
    }
  }
  
  public void zzbkm()
  {
    if (isConnected()) {}
    try
    {
      ((IGamesService)zzavg()).zzbkm();
      return;
    }
    catch (RemoteException localRemoteException)
    {
      zzb(localRemoteException);
    }
  }
  
  public String zzby(boolean paramBoolean)
  {
    if ((paramBoolean) && (this.YN != null)) {
      return this.YN.getPlayerId();
    }
    try
    {
      String str = ((IGamesService)zzavg()).zzbko();
      return str;
    }
    catch (RemoteException localRemoteException)
    {
      zzb(localRemoteException);
    }
    return null;
  }
  
  public Intent zzc(int paramInt1, int paramInt2, boolean paramBoolean)
  {
    try
    {
      Intent localIntent = ((IGamesService)zzavg()).zzc(paramInt1, paramInt2, paramBoolean);
      return localIntent;
    }
    catch (RemoteException localRemoteException)
    {
      zzb(localRemoteException);
    }
    return null;
  }
  
  public Intent zzc(int[] paramArrayOfInt)
  {
    try
    {
      paramArrayOfInt = ((IGamesService)zzavg()).zzc(paramArrayOfInt);
      return paramArrayOfInt;
    }
    catch (RemoteException paramArrayOfInt)
    {
      zzb(paramArrayOfInt);
    }
    return null;
  }
  
  protected Set<Scope> zzc(Set<Scope> paramSet)
  {
    Scope localScope1 = new Scope("https://www.googleapis.com/auth/games");
    Scope localScope2 = new Scope("https://www.googleapis.com/auth/games.firstparty");
    Iterator localIterator = paramSet.iterator();
    int i = 0;
    boolean bool = false;
    Scope localScope3;
    if (localIterator.hasNext())
    {
      localScope3 = (Scope)localIterator.next();
      if (localScope3.equals(localScope1)) {
        bool = true;
      }
    }
    for (;;)
    {
      break;
      if (localScope3.equals(localScope2))
      {
        i = 1;
        continue;
        if (i != 0)
        {
          if (!bool) {}
          for (bool = true;; bool = false)
          {
            zzaa.zza(bool, "Cannot have both %s and %s!", new Object[] { "https://www.googleapis.com/auth/games", "https://www.googleapis.com/auth/games.firstparty" });
            return paramSet;
          }
        }
        zzaa.zza(bool, "Games APIs requires %s to function.", new Object[] { "https://www.googleapis.com/auth/games" });
        return paramSet;
      }
    }
  }
  
  public void zzc(zzqo.zzb<Social.LoadInvitesResult> paramzzb, int paramInt)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzb(new InvitesLoadedBinderCallback(paramzzb), paramInt);
  }
  
  public void zzc(zzqo.zzb<Players.LoadPlayersResult> paramzzb, int paramInt, boolean paramBoolean1, boolean paramBoolean2)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzc(new PlayersLoadedBinderCallback(paramzzb), paramInt, paramBoolean1, paramBoolean2);
  }
  
  public void zzc(zzqo.zzb<TurnBasedMultiplayer.InitiateMatchResult> paramzzb, String paramString)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzk(new TurnBasedMatchInitiatedBinderCallbacks(paramzzb), paramString);
  }
  
  public void zzc(zzqo.zzb<Videos.ListVideosResult> paramzzb, String paramString, int paramInt)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzd(new ListVideosBinderCallback(paramzzb), paramString, paramInt);
  }
  
  public void zzc(zzqo.zzb<TurnBasedMultiplayer.InitiateMatchResult> paramzzb, String paramString1, String paramString2)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzd(new TurnBasedMatchInitiatedBinderCallbacks(paramzzb), paramString1, paramString2);
  }
  
  public void zzc(zzqo.zzb<Snapshots.LoadSnapshotsResult> paramzzb, String paramString1, String paramString2, boolean paramBoolean)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzc(new SnapshotsLoadedBinderCallbacks(paramzzb), paramString1, paramString2, paramBoolean);
  }
  
  public void zzc(zzqo.zzb<Status> paramzzb, String paramString, boolean paramBoolean)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzh(new UpdateHeadlessCapturePermissionBinderCallback(paramzzb), paramString, paramBoolean);
  }
  
  public void zzc(zzqo.zzb<Achievements.LoadAchievementsResult> paramzzb, boolean paramBoolean)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zza(new AchievementsLoadedBinderCallback(paramzzb), paramBoolean);
  }
  
  public void zzc(zzqo.zzb<Requests.UpdateRequestsResult> paramzzb, String[] paramArrayOfString)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzb(new RequestsUpdatedBinderCallbacks(paramzzb), paramArrayOfString);
  }
  
  public void zzc(zzrr<QuestUpdateListener> paramzzrr)
  {
    try
    {
      paramzzrr = new QuestUpdateBinderCallback(paramzzrr);
      ((IGamesService)zzavg()).zzd(paramzzrr, this.YS);
      return;
    }
    catch (RemoteException paramzzrr)
    {
      zzb(paramzzrr);
    }
  }
  
  public void zzd(zzqo.zzb<Players.LoadPlayersResult> paramzzb, int paramInt, boolean paramBoolean1, boolean paramBoolean2)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zze(new PlayersLoadedBinderCallback(paramzzb), paramInt, paramBoolean1, paramBoolean2);
  }
  
  public void zzd(zzqo.zzb<TurnBasedMultiplayer.InitiateMatchResult> paramzzb, String paramString)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzl(new TurnBasedMatchInitiatedBinderCallbacks(paramzzb), paramString);
  }
  
  public void zzd(zzqo.zzb<Players.LoadXpStreamResult> paramzzb, String paramString, int paramInt)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzb(new PlayerXpStreamLoadedBinderCallback(paramzzb), paramString, paramInt);
  }
  
  public void zzd(zzqo.zzb<TurnBasedMultiplayer.InitiateMatchResult> paramzzb, String paramString1, String paramString2)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zze(new TurnBasedMatchInitiatedBinderCallbacks(paramzzb), paramString1, paramString2);
  }
  
  public void zzd(zzqo.zzb<Leaderboards.LeaderboardMetadataResult> paramzzb, String paramString, boolean paramBoolean)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzd(new LeaderboardsLoadedBinderCallback(paramzzb), paramString, paramBoolean);
  }
  
  public void zzd(zzqo.zzb<Events.LoadEventsResult> paramzzb, boolean paramBoolean)
    throws RemoteException
  {
    this.YL.flush();
    ((IGamesService)zzavg()).zzf(new EventsLoadedBinderCallback(paramzzb), paramBoolean);
  }
  
  public void zzd(zzrr<OnRequestReceivedListener> paramzzrr)
  {
    try
    {
      paramzzrr = new RequestReceivedBinderCallback(paramzzrr);
      ((IGamesService)zzavg()).zzc(paramzzrr, this.YS);
      return;
    }
    catch (RemoteException paramzzrr)
    {
      zzb(paramzzrr);
    }
  }
  
  public int zze(byte[] paramArrayOfByte, String paramString)
  {
    try
    {
      int i = ((IGamesService)zzavg()).zzb(paramArrayOfByte, paramString, null);
      return i;
    }
    catch (RemoteException paramArrayOfByte)
    {
      zzb(paramArrayOfByte);
    }
    return -1;
  }
  
  public void zze(zzqo.zzb<Players.LoadPlayersResult> paramzzb, int paramInt, boolean paramBoolean1, boolean paramBoolean2)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzf(new PlayersLoadedBinderCallback(paramzzb), paramInt, paramBoolean1, paramBoolean2);
  }
  
  public void zze(zzqo.zzb<TurnBasedMultiplayer.LeaveMatchResult> paramzzb, String paramString)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzn(new TurnBasedMatchLeftBinderCallbacks(paramzzb), paramString);
  }
  
  public void zze(zzqo.zzb<Players.LoadXpStreamResult> paramzzb, String paramString, int paramInt)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzc(new PlayerXpStreamLoadedBinderCallback(paramzzb), paramString, paramInt);
  }
  
  public void zze(zzqo.zzb<Notifications.GameMuteStatusChangeResult> paramzzb, String paramString, boolean paramBoolean)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zza(new GameMuteStatusChangedBinderCallback(paramzzb), paramString, paramBoolean);
  }
  
  public void zze(zzqo.zzb<Stats.LoadPlayerStatsResult> paramzzb, boolean paramBoolean)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzi(new PlayerStatsLoadedBinderCallbacks(paramzzb), paramBoolean);
  }
  
  public void zze(zzrr<Videos.CaptureOverlayStateListener> paramzzrr)
  {
    try
    {
      paramzzrr = new CaptureOverlayStateBinderCallback(paramzzrr);
      ((IGamesService)zzavg()).zzf(paramzzrr, this.YS);
      return;
    }
    catch (RemoteException paramzzrr)
    {
      zzb(paramzzrr);
    }
  }
  
  public void zzf(Account paramAccount)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzf(paramAccount);
  }
  
  public void zzf(zzqo.zzb<Games.GetTokenResult> paramzzb)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzj(new GetAuthTokenBinderCallbacks(paramzzb));
  }
  
  public void zzf(zzqo.zzb<TurnBasedMultiplayer.CancelMatchResult> paramzzb, String paramString)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzm(new TurnBasedMatchCanceledBinderCallbacks(paramzzb), paramString);
  }
  
  public void zzf(zzqo.zzb<Invitations.LoadInvitationsResult> paramzzb, String paramString, int paramInt)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzb(new InvitationsLoadedBinderCallback(paramzzb), paramString, paramInt, false);
  }
  
  public void zzf(zzqo.zzb<Players.LoadPlayersResult> paramzzb, String paramString, boolean paramBoolean)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzg(new SetPlayerMutedBinderCallback(paramzzb), paramString, paramBoolean);
  }
  
  public void zzf(zzqo.zzb<Snapshots.LoadSnapshotsResult> paramzzb, boolean paramBoolean)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzd(new SnapshotsLoadedBinderCallbacks(paramzzb), paramBoolean);
  }
  
  public void zzg(zzqo.zzb<GamesMetadata.LoadGamesResult> paramzzb)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzd(new GamesLoadedBinderCallback(paramzzb));
  }
  
  public void zzg(zzqo.zzb<TurnBasedMultiplayer.LoadMatchResult> paramzzb, String paramString)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzo(new TurnBasedMatchLoadedBinderCallbacks(paramzzb), paramString);
  }
  
  public void zzg(zzqo.zzb<Requests.LoadRequestSummariesResult> paramzzb, String paramString, int paramInt)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zza(new RequestSummariesLoadedBinderCallbacks(paramzzb), paramString, paramInt);
  }
  
  public void zzg(zzqo.zzb<Status> paramzzb, boolean paramBoolean)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzm(new CapturePausedBinderCallback(paramzzb), paramBoolean);
  }
  
  protected IGamesService zzgn(IBinder paramIBinder)
  {
    return IGamesService.Stub.zzgq(paramIBinder);
  }
  
  public void zzh(zzqo.zzb<Status> paramzzb)
    throws RemoteException
  {
    this.YL.flush();
    ((IGamesService)zzavg()).zza(new SignOutCompleteBinderCallbacks(paramzzb));
  }
  
  public void zzh(zzqo.zzb<Quests.AcceptQuestResult> paramzzb, String paramString)
    throws RemoteException
  {
    this.YL.flush();
    ((IGamesService)zzavg()).zzt(new QuestAcceptedBinderCallbacks(paramzzb), paramString);
  }
  
  public void zzh(zzqo.zzb<Status> paramzzb, boolean paramBoolean)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzh(new ProfileSettingsUpdatedBinderCallback(paramzzb), paramBoolean);
  }
  
  public void zzi(zzqo.zzb<Videos.CaptureCapabilitiesResult> paramzzb)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzl(new CaptureCapabilitiesBinderCallback(paramzzb));
  }
  
  public void zzi(zzqo.zzb<Snapshots.DeleteSnapshotResult> paramzzb, String paramString)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzq(new SnapshotDeletedBinderCallbacks(paramzzb), paramString);
  }
  
  public void zzi(zzqo.zzb<Status> paramzzb, boolean paramBoolean)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzj(new DeletePlayerBinderCallback(paramzzb), paramBoolean);
  }
  
  public void zzj(zzqo.zzb<Videos.CaptureStateResult> paramzzb)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzv(new CaptureStateBinderCallbacks(paramzzb));
  }
  
  public void zzj(zzqo.zzb<GamesMetadata.LoadGameInstancesResult> paramzzb, String paramString)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzf(new GameInstancesLoadedBinderCallback(paramzzb), paramString);
  }
  
  public void zzj(zzqo.zzb<Status> paramzzb, boolean paramBoolean)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzk(new UpdateAutoSignInBinderCallback(paramzzb), paramBoolean);
  }
  
  public void zzjw(String paramString)
  {
    try
    {
      ((IGamesService)zzavg()).zzke(paramString);
      return;
    }
    catch (RemoteException paramString)
    {
      zzb(paramString);
    }
  }
  
  public Intent zzjx(String paramString)
  {
    try
    {
      paramString = ((IGamesService)zzavg()).zzjx(paramString);
      return paramString;
    }
    catch (RemoteException paramString)
    {
      zzb(paramString);
    }
    return null;
  }
  
  protected String zzjx()
  {
    return "com.google.android.gms.games.service.START";
  }
  
  protected String zzjy()
  {
    return "com.google.android.gms.games.internal.IGamesService";
  }
  
  public void zzjy(String paramString)
  {
    try
    {
      ((IGamesService)zzavg()).zza(paramString, this.YP.zzbld(), this.YP.zzblc());
      return;
    }
    catch (RemoteException paramString)
    {
      zzb(paramString);
    }
  }
  
  public void zzk(zzqo.zzb<Videos.CaptureStreamingAvailabilityResult> paramzzb)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzs(new CaptureStreamingAvailabilityBinderCallback(paramzzb));
  }
  
  public void zzk(zzqo.zzb<GamesMetadata.LoadGameSearchSuggestionsResult> paramzzb, String paramString)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzp(new GameSearchSuggestionsLoadedBinderCallback(paramzzb), paramString);
  }
  
  public void zzk(zzqo.zzb<Status> paramzzb, boolean paramBoolean)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzl(new UpdateProfileDiscoverabilityBinderCallback(paramzzb), paramBoolean);
  }
  
  public Intent zzl(String paramString, int paramInt1, int paramInt2)
  {
    try
    {
      paramString = ((IGamesService)zzavg()).zzm(paramString, paramInt1, paramInt2);
      return paramString;
    }
    catch (RemoteException paramString)
    {
      zzb(paramString);
    }
    return null;
  }
  
  public void zzl(zzqo.zzb<Videos.CaptureStreamingMetadataResult> paramzzb)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzq(new CaptureStreamingMetadataBinderCallback(paramzzb));
  }
  
  public void zzl(zzqo.zzb<Players.LoadXpForGameCategoriesResult> paramzzb, String paramString)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzr(new PlayerXpForGameCategoriesLoadedBinderCallback(paramzzb), paramString);
  }
  
  public void zzl(zzqo.zzb<Notifications.ContactSettingLoadResult> paramzzb, boolean paramBoolean)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zze(new ContactSettingsLoadedBinderCallback(paramzzb), paramBoolean);
  }
  
  public void zzm(zzqo.zzb<Videos.CaptureStreamingUrlResult> paramzzb)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzr(new CaptureStreamingUrlBinderCallback(paramzzb));
  }
  
  public void zzm(zzqo.zzb<Invitations.LoadInvitationsResult> paramzzb, String paramString)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzj(new InvitationsLoadedBinderCallback(paramzzb), paramString);
  }
  
  public void zzn(zzqo.zzb<Status> paramzzb)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzt(new HeadlessCaptureEnabledBinderCallback(paramzzb));
  }
  
  public void zzn(zzqo.zzb<Notifications.GameMuteStatusLoadResult> paramzzb, String paramString)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzi(new GameMuteStatusLoadedBinderCallback(paramzzb), paramString);
  }
  
  public void zzo(zzqo.zzb<Status> paramzzb)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzo(new CaptureStreamingEnabledBinderCallback(paramzzb));
  }
  
  public void zzo(zzqo.zzb<Social.InviteUpdateResult> paramzzb, String paramString)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzu(new SendFriendInviteFirstPartyBinderCallback(paramzzb), paramString);
  }
  
  public void zzo(String paramString, int paramInt)
  {
    this.YL.zzo(paramString, paramInt);
  }
  
  public void zzp(zzqo.zzb<Videos.ListVideosResult> paramzzb)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzk(new ListVideosBinderCallback(paramzzb));
  }
  
  public void zzp(zzqo.zzb<Social.InviteUpdateResult> paramzzb, String paramString)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzv(new AcceptFriendInviteFirstPartyBinderCallback(paramzzb), paramString);
  }
  
  public void zzp(String paramString, int paramInt)
  {
    try
    {
      ((IGamesService)zzavg()).zzp(paramString, paramInt);
      return;
    }
    catch (RemoteException paramString)
    {
      zzb(paramString);
    }
  }
  
  public void zzq(zzqo.zzb<Videos.CaptureStoppedResult> paramzzb)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzu(new CaptureStoppedBinderCallbacks(paramzzb));
  }
  
  public void zzq(zzqo.zzb<Social.InviteUpdateResult> paramzzb, String paramString)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzw(new IgnoreFriendInviteFirstPartyBinderCallback(paramzzb), paramString);
  }
  
  public void zzq(String paramString, int paramInt)
  {
    try
    {
      ((IGamesService)zzavg()).zzq(paramString, paramInt);
      return;
    }
    catch (RemoteException paramString)
    {
      zzb(paramString);
    }
  }
  
  public void zzr(zzqo.zzb<Players.LoadStockProfileImagesResult> paramzzb)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzp(new StockProfileImagesLoadedBinderCallback(paramzzb));
  }
  
  public void zzr(zzqo.zzb<Social.InviteUpdateResult> paramzzb, String paramString)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzx(new CancelFriendInviteFirstPartyBinderCallback(paramzzb), paramString);
  }
  
  public void zzrs(int paramInt)
  {
    this.YP.setGravity(paramInt);
  }
  
  public void zzrt(int paramInt)
  {
    try
    {
      ((IGamesService)zzavg()).zzrt(paramInt);
      return;
    }
    catch (RemoteException localRemoteException)
    {
      zzb(localRemoteException);
    }
  }
  
  public void zzs(zzqo.zzb<Notifications.InboxCountResult> paramzzb)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzs(new InboxCountsLoadedBinderCallback(paramzzb), null);
  }
  
  public void zzs(zzqo.zzb<Players.LoadPlayersResult> paramzzb, String paramString)
    throws RemoteException
  {
    ((IGamesService)zzavg()).zzy(new PlayerUnfriendedBinderCallback(paramzzb), paramString);
  }
  
  public String zzup()
  {
    try
    {
      String str = ((IGamesService)zzavg()).zzup();
      return str;
    }
    catch (RemoteException localRemoteException)
    {
      zzb(localRemoteException);
    }
    return null;
  }
  
  public void zzw(View paramView)
  {
    this.YP.zzx(paramView);
  }
  
  private static abstract class AbstractPeerStatusNotifier
    extends GamesClientImpl.AbstractRoomStatusNotifier
  {
    private final ArrayList<String> YV = new ArrayList();
    
    AbstractPeerStatusNotifier(DataHolder paramDataHolder, String[] paramArrayOfString)
    {
      super();
      int i = 0;
      int j = paramArrayOfString.length;
      while (i < j)
      {
        this.YV.add(paramArrayOfString[i]);
        i += 1;
      }
    }
    
    protected void zza(RoomStatusUpdateListener paramRoomStatusUpdateListener, Room paramRoom)
    {
      zza(paramRoomStatusUpdateListener, paramRoom, this.YV);
    }
    
    protected abstract void zza(RoomStatusUpdateListener paramRoomStatusUpdateListener, Room paramRoom, ArrayList<String> paramArrayList);
  }
  
  private static abstract class AbstractRoomNotifier
    extends zzqx<RoomUpdateListener>
  {
    AbstractRoomNotifier(DataHolder paramDataHolder)
    {
      super();
    }
    
    protected void zza(RoomUpdateListener paramRoomUpdateListener, DataHolder paramDataHolder)
    {
      zza(paramRoomUpdateListener, GamesClientImpl.zzbj(paramDataHolder), paramDataHolder.getStatusCode());
    }
    
    protected abstract void zza(RoomUpdateListener paramRoomUpdateListener, Room paramRoom, int paramInt);
  }
  
  private static abstract class AbstractRoomStatusNotifier
    extends zzqx<RoomStatusUpdateListener>
  {
    AbstractRoomStatusNotifier(DataHolder paramDataHolder)
    {
      super();
    }
    
    protected void zza(RoomStatusUpdateListener paramRoomStatusUpdateListener, DataHolder paramDataHolder)
    {
      zza(paramRoomStatusUpdateListener, GamesClientImpl.zzbj(paramDataHolder));
    }
    
    protected abstract void zza(RoomStatusUpdateListener paramRoomStatusUpdateListener, Room paramRoom);
  }
  
  private static final class AcceptFriendInviteFirstPartyBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Social.InviteUpdateResult> EW;
    
    AcceptFriendInviteFirstPartyBinderCallback(zzqo.zzb<Social.InviteUpdateResult> paramzzb)
    {
      this.EW = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzba(DataHolder paramDataHolder)
    {
      this.EW.setResult(new GamesClientImpl.InviteUpdateResultImpl(paramDataHolder));
    }
  }
  
  private static final class AcceptQuestResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements Quests.AcceptQuestResult
  {
    private final Quest YW;
    
    /* Error */
    AcceptQuestResultImpl(DataHolder paramDataHolder)
    {
      // Byte code:
      //   0: aload_0
      //   1: aload_1
      //   2: invokespecial 15	com/google/android/gms/games/internal/GamesClientImpl$GamesDataHolderResult:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   5: new 17	com/google/android/gms/games/quest/QuestBuffer
      //   8: dup
      //   9: aload_1
      //   10: invokespecial 18	com/google/android/gms/games/quest/QuestBuffer:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   13: astore_1
      //   14: aload_1
      //   15: invokevirtual 22	com/google/android/gms/games/quest/QuestBuffer:getCount	()I
      //   18: ifle +27 -> 45
      //   21: aload_0
      //   22: new 24	com/google/android/gms/games/quest/QuestEntity
      //   25: dup
      //   26: aload_1
      //   27: iconst_0
      //   28: invokevirtual 28	com/google/android/gms/games/quest/QuestBuffer:get	(I)Ljava/lang/Object;
      //   31: checkcast 30	com/google/android/gms/games/quest/Quest
      //   34: invokespecial 33	com/google/android/gms/games/quest/QuestEntity:<init>	(Lcom/google/android/gms/games/quest/Quest;)V
      //   37: putfield 35	com/google/android/gms/games/internal/GamesClientImpl$AcceptQuestResultImpl:YW	Lcom/google/android/gms/games/quest/Quest;
      //   40: aload_1
      //   41: invokevirtual 39	com/google/android/gms/games/quest/QuestBuffer:release	()V
      //   44: return
      //   45: aload_0
      //   46: aconst_null
      //   47: putfield 35	com/google/android/gms/games/internal/GamesClientImpl$AcceptQuestResultImpl:YW	Lcom/google/android/gms/games/quest/Quest;
      //   50: goto -10 -> 40
      //   53: astore_2
      //   54: aload_1
      //   55: invokevirtual 39	com/google/android/gms/games/quest/QuestBuffer:release	()V
      //   58: aload_2
      //   59: athrow
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	60	0	this	AcceptQuestResultImpl
      //   0	60	1	paramDataHolder	DataHolder
      //   53	6	2	localObject	Object
      // Exception table:
      //   from	to	target	type
      //   14	40	53	finally
      //   45	50	53	finally
    }
    
    public Quest getQuest()
    {
      return this.YW;
    }
  }
  
  private static final class AchievementUpdatedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Achievements.UpdateAchievementResult> EW;
    
    AchievementUpdatedBinderCallback(zzqo.zzb<Achievements.UpdateAchievementResult> paramzzb)
    {
      this.EW = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzi(int paramInt, String paramString)
    {
      this.EW.setResult(new GamesClientImpl.UpdateAchievementResultImpl(paramInt, paramString));
    }
  }
  
  private static final class AchievementsLoadedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Achievements.LoadAchievementsResult> EW;
    
    AchievementsLoadedBinderCallback(zzqo.zzb<Achievements.LoadAchievementsResult> paramzzb)
    {
      this.EW = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzh(DataHolder paramDataHolder)
    {
      this.EW.setResult(new GamesClientImpl.LoadAchievementsResultImpl(paramDataHolder));
    }
  }
  
  private static final class AppContentLoadedBinderCallbacks
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<AppContents.LoadAppContentResult> YX;
    
    public AppContentLoadedBinderCallbacks(zzqo.zzb<AppContents.LoadAppContentResult> paramzzb)
    {
      this.YX = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zza(DataHolder[] paramArrayOfDataHolder)
    {
      this.YX.setResult(new GamesClientImpl.LoadAppContentsResultImpl(paramArrayOfDataHolder));
    }
  }
  
  private static final class CancelFriendInviteFirstPartyBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Social.InviteUpdateResult> EW;
    
    CancelFriendInviteFirstPartyBinderCallback(zzqo.zzb<Social.InviteUpdateResult> paramzzb)
    {
      this.EW = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzbe(DataHolder paramDataHolder)
    {
      this.EW.setResult(new GamesClientImpl.InviteUpdateResultImpl(paramDataHolder));
    }
  }
  
  private static final class CancelMatchResultImpl
    implements TurnBasedMultiplayer.CancelMatchResult
  {
    private final String YY;
    private final Status hv;
    
    CancelMatchResultImpl(Status paramStatus, String paramString)
    {
      this.hv = paramStatus;
      this.YY = paramString;
    }
    
    public String getMatchId()
    {
      return this.YY;
    }
    
    public Status getStatus()
    {
      return this.hv;
    }
  }
  
  private static final class CaptureAvailableBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Videos.CaptureAvailableResult> EW;
    
    CaptureAvailableBinderCallback(zzqo.zzb<Videos.CaptureAvailableResult> paramzzb)
    {
      this.EW = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzd(int paramInt, boolean paramBoolean)
    {
      this.EW.setResult(new GamesClientImpl.CaptureAvailableResultImpl(new Status(paramInt), paramBoolean));
    }
  }
  
  private static final class CaptureAvailableResultImpl
    implements Videos.CaptureAvailableResult
  {
    private final boolean YZ;
    private final Status hv;
    
    CaptureAvailableResultImpl(Status paramStatus, boolean paramBoolean)
    {
      this.hv = paramStatus;
      this.YZ = paramBoolean;
    }
    
    public Status getStatus()
    {
      return this.hv;
    }
    
    public boolean isAvailable()
    {
      return this.YZ;
    }
  }
  
  private static final class CaptureCapabilitiesBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Videos.CaptureCapabilitiesResult> EW;
    
    CaptureCapabilitiesBinderCallback(zzqo.zzb<Videos.CaptureCapabilitiesResult> paramzzb)
    {
      this.EW = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zza(int paramInt, VideoCapabilities paramVideoCapabilities)
    {
      this.EW.setResult(new GamesClientImpl.CaptureCapabilitiesResultImpl(new Status(paramInt), paramVideoCapabilities));
    }
  }
  
  private static final class CaptureCapabilitiesResultImpl
    implements Videos.CaptureCapabilitiesResult
  {
    private final VideoCapabilities Za;
    private final Status hv;
    
    CaptureCapabilitiesResultImpl(Status paramStatus, VideoCapabilities paramVideoCapabilities)
    {
      this.hv = paramStatus;
      this.Za = paramVideoCapabilities;
    }
    
    public VideoCapabilities getCapabilities()
    {
      return this.Za;
    }
    
    public Status getStatus()
    {
      return this.hv;
    }
  }
  
  private static final class CaptureLaunchGameAndOverlayBinderCallback
    extends AbstractGamesCallbacks
  {
    private final Games.BaseGamesApiMethodImpl<Status> Zb;
    
    CaptureLaunchGameAndOverlayBinderCallback(Games.BaseGamesApiMethodImpl<Status> paramBaseGamesApiMethodImpl)
    {
      this.Zb = ((Games.BaseGamesApiMethodImpl)zzaa.zzb(paramBaseGamesApiMethodImpl, "Holder must not be null"));
    }
    
    public void zzbm(Status paramStatus)
    {
      this.Zb.zzc(paramStatus);
    }
    
    public void zzrk(int paramInt)
    {
      this.Zb.zzc(new Status(paramInt));
    }
  }
  
  private static final class CaptureOverlayStateBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zzrr<Videos.CaptureOverlayStateListener> LE;
    
    CaptureOverlayStateBinderCallback(zzrr<Videos.CaptureOverlayStateListener> paramzzrr)
    {
      this.LE = ((zzrr)zzaa.zzb(paramzzrr, "Callback must not be null"));
    }
    
    public void onCaptureOverlayStateChanged(int paramInt)
    {
      this.LE.zza(new GamesClientImpl.CaptureOverlayStateChangedNotifier(paramInt));
    }
  }
  
  private static final class CaptureOverlayStateChangedNotifier
    implements zzrr.zzc<Videos.CaptureOverlayStateListener>
  {
    private final int Zc;
    
    CaptureOverlayStateChangedNotifier(int paramInt)
    {
      this.Zc = paramInt;
    }
    
    public void zza(Videos.CaptureOverlayStateListener paramCaptureOverlayStateListener)
    {
      paramCaptureOverlayStateListener.onCaptureOverlayStateChanged(this.Zc);
    }
    
    public void zzasm() {}
  }
  
  private static final class CapturePausedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Status> EW;
    
    public CapturePausedBinderCallback(zzqo.zzb<Status> paramzzb)
    {
      this.EW = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzrq(int paramInt)
    {
      this.EW.setResult(new Status(paramInt));
    }
  }
  
  private static final class CaptureStartedBinderCallbacks
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Status> EW;
    private final Videos.CaptureRuntimeErrorCallback Zd;
    
    public CaptureStartedBinderCallbacks(zzqo.zzb<Status> paramzzb, Videos.CaptureRuntimeErrorCallback paramCaptureRuntimeErrorCallback)
    {
      this.EW = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
      this.Zd = ((Videos.CaptureRuntimeErrorCallback)zzaa.zzb(paramCaptureRuntimeErrorCallback, "Callback must not be null"));
    }
    
    public void zzbp(Status paramStatus)
    {
      this.EW.setResult(paramStatus);
    }
    
    public void zzrp(int paramInt)
    {
      this.Zd.zzth(paramInt);
    }
  }
  
  private static final class CaptureStateBinderCallbacks
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Videos.CaptureStateResult> EW;
    
    public CaptureStateBinderCallbacks(zzqo.zzb<Videos.CaptureStateResult> paramzzb)
    {
      this.EW = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzi(int paramInt, Bundle paramBundle)
    {
      this.EW.setResult(new GamesClientImpl.CaptureStateResultImpl(new Status(paramInt), CaptureState.zzab(paramBundle)));
    }
  }
  
  private static final class CaptureStateResultImpl
    implements Videos.CaptureStateResult
  {
    private final CaptureState Ze;
    private final Status hv;
    
    CaptureStateResultImpl(Status paramStatus, CaptureState paramCaptureState)
    {
      this.hv = paramStatus;
      this.Ze = paramCaptureState;
    }
    
    public CaptureState getCaptureState()
    {
      return this.Ze;
    }
    
    public Status getStatus()
    {
      return this.hv;
    }
  }
  
  private static final class CaptureStoppedBinderCallbacks
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Videos.CaptureStoppedResult> EW;
    
    public CaptureStoppedBinderCallbacks(zzqo.zzb<Videos.CaptureStoppedResult> paramzzb)
    {
      this.EW = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zza(int paramInt, Uri paramUri)
    {
      this.EW.setResult(new GamesClientImpl.CaptureStoppedResultImpl(new Status(paramInt), paramUri));
    }
  }
  
  private static final class CaptureStoppedResultImpl
    implements Videos.CaptureStoppedResult
  {
    private final Uri Zf;
    private final Status hv;
    
    CaptureStoppedResultImpl(Status paramStatus, Uri paramUri)
    {
      this.hv = paramStatus;
      this.Zf = paramUri;
    }
    
    public Status getStatus()
    {
      return this.hv;
    }
  }
  
  private static final class CaptureStreamingAvailabilityBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Videos.CaptureStreamingAvailabilityResult> EW;
    
    CaptureStreamingAvailabilityBinderCallback(zzqo.zzb<Videos.CaptureStreamingAvailabilityResult> paramzzb)
    {
      this.EW = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zza(int paramInt, boolean paramBoolean1, boolean paramBoolean2)
    {
      this.EW.setResult(new GamesClientImpl.CaptureStreamingAvailabilityResultImpl(new Status(paramInt), paramBoolean1, paramBoolean2));
    }
  }
  
  private static final class CaptureStreamingAvailabilityResultImpl
    implements Videos.CaptureStreamingAvailabilityResult
  {
    private final boolean YZ;
    private final boolean Zg;
    private final Status hv;
    
    CaptureStreamingAvailabilityResultImpl(Status paramStatus, boolean paramBoolean1, boolean paramBoolean2)
    {
      this.hv = paramStatus;
      this.YZ = paramBoolean1;
      this.Zg = paramBoolean2;
    }
    
    public Status getStatus()
    {
      return this.hv;
    }
  }
  
  private static final class CaptureStreamingEnabledBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Status> EW;
    
    CaptureStreamingEnabledBinderCallback(zzqo.zzb<Status> paramzzb)
    {
      this.EW = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzrl(int paramInt)
    {
      this.EW.setResult(new Status(paramInt));
    }
  }
  
  private static final class CaptureStreamingEnabledResultImpl
    implements Result
  {
    private final Status hv;
    
    public Status getStatus()
    {
      return this.hv;
    }
  }
  
  private static final class CaptureStreamingMetadataBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Videos.CaptureStreamingMetadataResult> EW;
    
    CaptureStreamingMetadataBinderCallback(zzqo.zzb<Videos.CaptureStreamingMetadataResult> paramzzb)
    {
      this.EW = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzd(int paramInt, String paramString1, String paramString2)
    {
      this.EW.setResult(new GamesClientImpl.CaptureStreamingMetadataResultImpl(new Status(paramInt), paramString1, paramString2));
    }
  }
  
  private static final class CaptureStreamingMetadataResultImpl
    implements Videos.CaptureStreamingMetadataResult
  {
    private final String JB;
    private final String cg;
    private final Status hv;
    
    CaptureStreamingMetadataResultImpl(Status paramStatus, String paramString1, String paramString2)
    {
      this.hv = paramStatus;
      this.JB = paramString1;
      this.cg = paramString2;
    }
    
    public Status getStatus()
    {
      return this.hv;
    }
  }
  
  private static final class CaptureStreamingUrlBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Videos.CaptureStreamingUrlResult> EW;
    
    CaptureStreamingUrlBinderCallback(zzqo.zzb<Videos.CaptureStreamingUrlResult> paramzzb)
    {
      this.EW = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzl(int paramInt, String paramString)
    {
      this.EW.setResult(new GamesClientImpl.CaptureStreamingUrlResultImpl(new Status(paramInt), paramString));
    }
  }
  
  private static final class CaptureStreamingUrlResultImpl
    implements Videos.CaptureStreamingUrlResult
  {
    private final Status hv;
    private final String zzae;
    
    CaptureStreamingUrlResultImpl(Status paramStatus, String paramString)
    {
      this.hv = paramStatus;
      this.zzae = paramString;
    }
    
    public Status getStatus()
    {
      return this.hv;
    }
    
    public String getUrl()
    {
      return this.zzae;
    }
  }
  
  private static final class ClaimMilestoneResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements Quests.ClaimMilestoneResult
  {
    private final Quest YW;
    private final Milestone Zh;
    
    /* Error */
    ClaimMilestoneResultImpl(DataHolder paramDataHolder, String paramString)
    {
      // Byte code:
      //   0: iconst_0
      //   1: istore_3
      //   2: aload_0
      //   3: aload_1
      //   4: invokespecial 18	com/google/android/gms/games/internal/GamesClientImpl$GamesDataHolderResult:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   7: new 20	com/google/android/gms/games/quest/QuestBuffer
      //   10: dup
      //   11: aload_1
      //   12: invokespecial 21	com/google/android/gms/games/quest/QuestBuffer:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   15: astore_1
      //   16: aload_1
      //   17: invokevirtual 25	com/google/android/gms/games/quest/QuestBuffer:getCount	()I
      //   20: ifle +108 -> 128
      //   23: aload_0
      //   24: new 27	com/google/android/gms/games/quest/QuestEntity
      //   27: dup
      //   28: aload_1
      //   29: iconst_0
      //   30: invokevirtual 31	com/google/android/gms/games/quest/QuestBuffer:get	(I)Ljava/lang/Object;
      //   33: checkcast 33	com/google/android/gms/games/quest/Quest
      //   36: invokespecial 36	com/google/android/gms/games/quest/QuestEntity:<init>	(Lcom/google/android/gms/games/quest/Quest;)V
      //   39: putfield 38	com/google/android/gms/games/internal/GamesClientImpl$ClaimMilestoneResultImpl:YW	Lcom/google/android/gms/games/quest/Quest;
      //   42: aload_0
      //   43: getfield 38	com/google/android/gms/games/internal/GamesClientImpl$ClaimMilestoneResultImpl:YW	Lcom/google/android/gms/games/quest/Quest;
      //   46: invokeinterface 42 1 0
      //   51: astore 5
      //   53: aload 5
      //   55: invokeinterface 47 1 0
      //   60: istore 4
      //   62: iload_3
      //   63: iload 4
      //   65: if_icmpge +53 -> 118
      //   68: aload 5
      //   70: iload_3
      //   71: invokeinterface 48 2 0
      //   76: checkcast 50	com/google/android/gms/games/quest/Milestone
      //   79: invokeinterface 54 1 0
      //   84: aload_2
      //   85: invokevirtual 60	java/lang/String:equals	(Ljava/lang/Object;)Z
      //   88: ifeq +23 -> 111
      //   91: aload_0
      //   92: aload 5
      //   94: iload_3
      //   95: invokeinterface 48 2 0
      //   100: checkcast 50	com/google/android/gms/games/quest/Milestone
      //   103: putfield 62	com/google/android/gms/games/internal/GamesClientImpl$ClaimMilestoneResultImpl:Zh	Lcom/google/android/gms/games/quest/Milestone;
      //   106: aload_1
      //   107: invokevirtual 66	com/google/android/gms/games/quest/QuestBuffer:release	()V
      //   110: return
      //   111: iload_3
      //   112: iconst_1
      //   113: iadd
      //   114: istore_3
      //   115: goto -53 -> 62
      //   118: aload_0
      //   119: aconst_null
      //   120: putfield 62	com/google/android/gms/games/internal/GamesClientImpl$ClaimMilestoneResultImpl:Zh	Lcom/google/android/gms/games/quest/Milestone;
      //   123: aload_1
      //   124: invokevirtual 66	com/google/android/gms/games/quest/QuestBuffer:release	()V
      //   127: return
      //   128: aload_0
      //   129: aconst_null
      //   130: putfield 62	com/google/android/gms/games/internal/GamesClientImpl$ClaimMilestoneResultImpl:Zh	Lcom/google/android/gms/games/quest/Milestone;
      //   133: aload_0
      //   134: aconst_null
      //   135: putfield 38	com/google/android/gms/games/internal/GamesClientImpl$ClaimMilestoneResultImpl:YW	Lcom/google/android/gms/games/quest/Quest;
      //   138: goto -15 -> 123
      //   141: astore_2
      //   142: aload_1
      //   143: invokevirtual 66	com/google/android/gms/games/quest/QuestBuffer:release	()V
      //   146: aload_2
      //   147: athrow
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	148	0	this	ClaimMilestoneResultImpl
      //   0	148	1	paramDataHolder	DataHolder
      //   0	148	2	paramString	String
      //   1	114	3	i	int
      //   60	6	4	j	int
      //   51	42	5	localList	List
      // Exception table:
      //   from	to	target	type
      //   16	62	141	finally
      //   68	106	141	finally
      //   118	123	141	finally
      //   128	138	141	finally
    }
    
    public Milestone getMilestone()
    {
      return this.Zh;
    }
    
    public Quest getQuest()
    {
      return this.YW;
    }
  }
  
  private static final class CommitSnapshotResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements Snapshots.CommitSnapshotResult
  {
    private final SnapshotMetadata Zi;
    
    /* Error */
    CommitSnapshotResultImpl(DataHolder paramDataHolder)
    {
      // Byte code:
      //   0: aload_0
      //   1: aload_1
      //   2: invokespecial 15	com/google/android/gms/games/internal/GamesClientImpl$GamesDataHolderResult:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   5: new 17	com/google/android/gms/games/snapshot/SnapshotMetadataBuffer
      //   8: dup
      //   9: aload_1
      //   10: invokespecial 18	com/google/android/gms/games/snapshot/SnapshotMetadataBuffer:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   13: astore_1
      //   14: aload_1
      //   15: invokevirtual 22	com/google/android/gms/games/snapshot/SnapshotMetadataBuffer:getCount	()I
      //   18: ifle +27 -> 45
      //   21: aload_0
      //   22: new 24	com/google/android/gms/games/snapshot/SnapshotMetadataEntity
      //   25: dup
      //   26: aload_1
      //   27: iconst_0
      //   28: invokevirtual 28	com/google/android/gms/games/snapshot/SnapshotMetadataBuffer:get	(I)Ljava/lang/Object;
      //   31: checkcast 30	com/google/android/gms/games/snapshot/SnapshotMetadata
      //   34: invokespecial 33	com/google/android/gms/games/snapshot/SnapshotMetadataEntity:<init>	(Lcom/google/android/gms/games/snapshot/SnapshotMetadata;)V
      //   37: putfield 35	com/google/android/gms/games/internal/GamesClientImpl$CommitSnapshotResultImpl:Zi	Lcom/google/android/gms/games/snapshot/SnapshotMetadata;
      //   40: aload_1
      //   41: invokevirtual 39	com/google/android/gms/games/snapshot/SnapshotMetadataBuffer:release	()V
      //   44: return
      //   45: aload_0
      //   46: aconst_null
      //   47: putfield 35	com/google/android/gms/games/internal/GamesClientImpl$CommitSnapshotResultImpl:Zi	Lcom/google/android/gms/games/snapshot/SnapshotMetadata;
      //   50: goto -10 -> 40
      //   53: astore_2
      //   54: aload_1
      //   55: invokevirtual 39	com/google/android/gms/games/snapshot/SnapshotMetadataBuffer:release	()V
      //   58: aload_2
      //   59: athrow
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	60	0	this	CommitSnapshotResultImpl
      //   0	60	1	paramDataHolder	DataHolder
      //   53	6	2	localObject	Object
      // Exception table:
      //   from	to	target	type
      //   14	40	53	finally
      //   45	50	53	finally
    }
    
    public SnapshotMetadata getSnapshotMetadata()
    {
      return this.Zi;
    }
  }
  
  private static final class ConnectedToRoomNotifier
    extends GamesClientImpl.AbstractRoomStatusNotifier
  {
    ConnectedToRoomNotifier(DataHolder paramDataHolder)
    {
      super();
    }
    
    public void zza(RoomStatusUpdateListener paramRoomStatusUpdateListener, Room paramRoom)
    {
      paramRoomStatusUpdateListener.onConnectedToRoom(paramRoom);
    }
  }
  
  private static final class ContactSettingLoadResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements Notifications.ContactSettingLoadResult
  {
    ContactSettingLoadResultImpl(DataHolder paramDataHolder)
    {
      super();
    }
  }
  
  private static final class ContactSettingsLoadedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Notifications.ContactSettingLoadResult> EW;
    
    ContactSettingsLoadedBinderCallback(zzqo.zzb<Notifications.ContactSettingLoadResult> paramzzb)
    {
      this.EW = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzai(DataHolder paramDataHolder)
    {
      this.EW.setResult(new GamesClientImpl.ContactSettingLoadResultImpl(paramDataHolder));
    }
  }
  
  private static final class ContactSettingsUpdatedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Status> EW;
    
    ContactSettingsUpdatedBinderCallback(zzqo.zzb<Status> paramzzb)
    {
      this.EW = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzri(int paramInt)
    {
      this.EW.setResult(GamesStatusCodes.zzqv(paramInt));
    }
  }
  
  private static final class DeletePlayerBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Status> EW;
    
    DeletePlayerBinderCallback(zzqo.zzb<Status> paramzzb)
    {
      this.EW = paramzzb;
    }
    
    public void zzrm(int paramInt)
    {
      this.EW.setResult(new Status(paramInt));
    }
  }
  
  private static final class DeleteSnapshotResultImpl
    implements Snapshots.DeleteSnapshotResult
  {
    private final String Zj;
    private final Status hv;
    
    DeleteSnapshotResultImpl(int paramInt, String paramString)
    {
      this.hv = GamesStatusCodes.zzqv(paramInt);
      this.Zj = paramString;
    }
    
    public String getSnapshotId()
    {
      return this.Zj;
    }
    
    public Status getStatus()
    {
      return this.hv;
    }
  }
  
  private static final class DisconnectedFromRoomNotifier
    extends GamesClientImpl.AbstractRoomStatusNotifier
  {
    DisconnectedFromRoomNotifier(DataHolder paramDataHolder)
    {
      super();
    }
    
    public void zza(RoomStatusUpdateListener paramRoomStatusUpdateListener, Room paramRoom)
    {
      paramRoomStatusUpdateListener.onDisconnectedFromRoom(paramRoom);
    }
  }
  
  private static final class EventsLoadedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Events.LoadEventsResult> EW;
    
    EventsLoadedBinderCallback(zzqo.zzb<Events.LoadEventsResult> paramzzb)
    {
      this.EW = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzi(DataHolder paramDataHolder)
    {
      this.EW.setResult(new GamesClientImpl.LoadEventResultImpl(paramDataHolder));
    }
  }
  
  private class GameClientEventIncrementCache
    extends EventIncrementCache
  {
    public GameClientEventIncrementCache()
    {
      super(1000);
    }
    
    protected void zzr(String paramString, int paramInt)
    {
      try
      {
        if (GamesClientImpl.this.isConnected())
        {
          ((IGamesService)GamesClientImpl.this.zzavg()).zzo(paramString, paramInt);
          return;
        }
        GamesLog.zzaf("GamesClientImpl", String.valueOf(paramString).length() + 89 + "Unable to increment event " + paramString + " by " + paramInt + " because the games client is no longer connected");
        return;
      }
      catch (RemoteException paramString)
      {
        GamesClientImpl.zza(GamesClientImpl.this, paramString);
      }
    }
  }
  
  private static final class GameInstancesLoadedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<GamesMetadata.LoadGameInstancesResult> EW;
    
    GameInstancesLoadedBinderCallback(zzqo.zzb<GamesMetadata.LoadGameInstancesResult> paramzzb)
    {
      this.EW = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzp(DataHolder paramDataHolder)
    {
      this.EW.setResult(new GamesClientImpl.LoadGameInstancesResultImpl(paramDataHolder));
    }
  }
  
  private static final class GameMuteStatusChangeResultImpl
    implements Notifications.GameMuteStatusChangeResult
  {
    private final boolean C;
    private final String Zk;
    private final Status hv;
    
    public GameMuteStatusChangeResultImpl(int paramInt, String paramString, boolean paramBoolean)
    {
      this.hv = GamesStatusCodes.zzqv(paramInt);
      this.Zk = paramString;
      this.C = paramBoolean;
    }
    
    public Status getStatus()
    {
      return this.hv;
    }
  }
  
  private static final class GameMuteStatusChangedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Notifications.GameMuteStatusChangeResult> EW;
    
    GameMuteStatusChangedBinderCallback(zzqo.zzb<Notifications.GameMuteStatusChangeResult> paramzzb)
    {
      this.EW = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zza(int paramInt, String paramString, boolean paramBoolean)
    {
      this.EW.setResult(new GamesClientImpl.GameMuteStatusChangeResultImpl(paramInt, paramString, paramBoolean));
    }
  }
  
  private static final class GameMuteStatusLoadResultImpl
    implements Notifications.GameMuteStatusLoadResult
  {
    private final boolean C;
    private final String Zk;
    private final Status hv;
    
    /* Error */
    public GameMuteStatusLoadResultImpl(DataHolder paramDataHolder)
    {
      // Byte code:
      //   0: aload_0
      //   1: invokespecial 20	java/lang/Object:<init>	()V
      //   4: aload_0
      //   5: aload_1
      //   6: invokevirtual 26	com/google/android/gms/common/data/DataHolder:getStatusCode	()I
      //   9: invokestatic 32	com/google/android/gms/games/GamesStatusCodes:zzqv	(I)Lcom/google/android/gms/common/api/Status;
      //   12: putfield 34	com/google/android/gms/games/internal/GamesClientImpl$GameMuteStatusLoadResultImpl:hv	Lcom/google/android/gms/common/api/Status;
      //   15: aload_1
      //   16: invokevirtual 37	com/google/android/gms/common/data/DataHolder:getCount	()I
      //   19: ifle +32 -> 51
      //   22: aload_0
      //   23: aload_1
      //   24: ldc 39
      //   26: iconst_0
      //   27: iconst_0
      //   28: invokevirtual 43	com/google/android/gms/common/data/DataHolder:zzd	(Ljava/lang/String;II)Ljava/lang/String;
      //   31: putfield 45	com/google/android/gms/games/internal/GamesClientImpl$GameMuteStatusLoadResultImpl:Zk	Ljava/lang/String;
      //   34: aload_0
      //   35: aload_1
      //   36: ldc 47
      //   38: iconst_0
      //   39: iconst_0
      //   40: invokevirtual 51	com/google/android/gms/common/data/DataHolder:zze	(Ljava/lang/String;II)Z
      //   43: putfield 53	com/google/android/gms/games/internal/GamesClientImpl$GameMuteStatusLoadResultImpl:C	Z
      //   46: aload_1
      //   47: invokevirtual 56	com/google/android/gms/common/data/DataHolder:close	()V
      //   50: return
      //   51: aload_0
      //   52: aconst_null
      //   53: putfield 45	com/google/android/gms/games/internal/GamesClientImpl$GameMuteStatusLoadResultImpl:Zk	Ljava/lang/String;
      //   56: aload_0
      //   57: iconst_0
      //   58: putfield 53	com/google/android/gms/games/internal/GamesClientImpl$GameMuteStatusLoadResultImpl:C	Z
      //   61: goto -15 -> 46
      //   64: astore_2
      //   65: aload_1
      //   66: invokevirtual 56	com/google/android/gms/common/data/DataHolder:close	()V
      //   69: aload_2
      //   70: athrow
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	71	0	this	GameMuteStatusLoadResultImpl
      //   0	71	1	paramDataHolder	DataHolder
      //   64	6	2	localObject	Object
      // Exception table:
      //   from	to	target	type
      //   4	46	64	finally
      //   51	61	64	finally
    }
    
    public Status getStatus()
    {
      return this.hv;
    }
  }
  
  private static final class GameMuteStatusLoadedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Notifications.GameMuteStatusLoadResult> EW;
    
    GameMuteStatusLoadedBinderCallback(zzqo.zzb<Notifications.GameMuteStatusLoadResult> paramzzb)
    {
      this.EW = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzag(DataHolder paramDataHolder)
    {
      this.EW.setResult(new GamesClientImpl.GameMuteStatusLoadResultImpl(paramDataHolder));
    }
  }
  
  private static final class GameSearchSuggestionsLoadedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<GamesMetadata.LoadGameSearchSuggestionsResult> EW;
    
    GameSearchSuggestionsLoadedBinderCallback(zzqo.zzb<GamesMetadata.LoadGameSearchSuggestionsResult> paramzzb)
    {
      this.EW = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzq(DataHolder paramDataHolder)
    {
      this.EW.setResult(new GamesClientImpl.LoadGameSearchSuggestionsResultImpl(paramDataHolder));
    }
  }
  
  private static final class GamerProfileUpdatedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Players.UpdateGamerProfileResult> EW;
    
    GamerProfileUpdatedBinderCallback(zzqo.zzb<Players.UpdateGamerProfileResult> paramzzb)
    {
      this.EW = paramzzb;
    }
    
    public void zzh(int paramInt, Bundle paramBundle)
    {
      this.EW.setResult(new GamesClientImpl.UpdateGamerProfileResultImpl(paramInt, paramBundle));
    }
  }
  
  private static abstract class GamesDataHolderResult
    extends zzqy
  {
    protected GamesDataHolderResult(DataHolder paramDataHolder)
    {
      super(GamesStatusCodes.zzqv(paramDataHolder.getStatusCode()));
    }
  }
  
  private static final class GamesLoadedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<GamesMetadata.LoadGamesResult> EW;
    
    GamesLoadedBinderCallback(zzqo.zzb<GamesMetadata.LoadGamesResult> paramzzb)
    {
      this.EW = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzn(DataHolder paramDataHolder)
    {
      this.EW.setResult(new GamesClientImpl.LoadGamesResultImpl(paramDataHolder));
    }
  }
  
  private static final class GetAuthTokenBinderCallbacks
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Games.GetTokenResult> EW;
    
    public GetAuthTokenBinderCallbacks(zzqo.zzb<Games.GetTokenResult> paramzzb)
    {
      this.EW = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzh(int paramInt, String paramString)
    {
      Status localStatus = GamesStatusCodes.zzqv(paramInt);
      this.EW.setResult(new GamesClientImpl.GetTokenResultImpl(localStatus, paramString));
    }
  }
  
  private static final class GetServerAuthCodeBinderCallbacks
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Games.GetServerAuthCodeResult> EW;
    
    public GetServerAuthCodeBinderCallbacks(zzqo.zzb<Games.GetServerAuthCodeResult> paramzzb)
    {
      this.EW = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzh(int paramInt, String paramString)
    {
      Status localStatus = GamesStatusCodes.zzqv(paramInt);
      this.EW.setResult(new GamesClientImpl.GetServerAuthCodeResultImpl(localStatus, paramString));
    }
  }
  
  private static final class GetServerAuthCodeResultImpl
    implements Games.GetServerAuthCodeResult
  {
    private final String Zl;
    private final Status hv;
    
    GetServerAuthCodeResultImpl(Status paramStatus, String paramString)
    {
      this.hv = paramStatus;
      this.Zl = paramString;
    }
    
    public String getCode()
    {
      return this.Zl;
    }
    
    public Status getStatus()
    {
      return this.hv;
    }
  }
  
  private static final class GetTokenResultImpl
    implements Games.GetTokenResult
  {
    private final String hN;
    private final Status hv;
    
    GetTokenResultImpl(Status paramStatus, String paramString)
    {
      this.hv = paramStatus;
      this.hN = paramString;
    }
    
    public Status getStatus()
    {
      return this.hv;
    }
  }
  
  private static final class HeadlessCaptureEnabledBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Status> EW;
    
    HeadlessCaptureEnabledBinderCallback(zzqo.zzb<Status> paramzzb)
    {
      this.EW = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzbn(Status paramStatus)
    {
      this.EW.setResult(paramStatus);
    }
  }
  
  private static final class IgnoreFriendInviteFirstPartyBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Social.InviteUpdateResult> EW;
    
    IgnoreFriendInviteFirstPartyBinderCallback(zzqo.zzb<Social.InviteUpdateResult> paramzzb)
    {
      this.EW = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzbd(DataHolder paramDataHolder)
    {
      this.EW.setResult(new GamesClientImpl.InviteUpdateResultImpl(paramDataHolder));
    }
  }
  
  private static final class InboxCountResultImpl
    implements Notifications.InboxCountResult
  {
    private final Bundle Zm;
    private final Status hv;
    
    InboxCountResultImpl(Status paramStatus, Bundle paramBundle)
    {
      this.hv = paramStatus;
      this.Zm = paramBundle;
    }
    
    public Status getStatus()
    {
      return this.hv;
    }
  }
  
  private static final class InboxCountsLoadedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Notifications.InboxCountResult> EW;
    
    InboxCountsLoadedBinderCallback(zzqo.zzb<Notifications.InboxCountResult> paramzzb)
    {
      this.EW = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzg(int paramInt, Bundle paramBundle)
    {
      paramBundle.setClassLoader(getClass().getClassLoader());
      Status localStatus = GamesStatusCodes.zzqv(paramInt);
      this.EW.setResult(new GamesClientImpl.InboxCountResultImpl(localStatus, paramBundle));
    }
  }
  
  private static final class InitiateMatchResultImpl
    extends GamesClientImpl.TurnBasedMatchResult
    implements TurnBasedMultiplayer.InitiateMatchResult
  {
    InitiateMatchResultImpl(DataHolder paramDataHolder)
    {
      super();
    }
  }
  
  private static final class InvitationReceivedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zzrr<OnInvitationReceivedListener> LE;
    
    InvitationReceivedBinderCallback(zzrr<OnInvitationReceivedListener> paramzzrr)
    {
      this.LE = paramzzrr;
    }
    
    public void onInvitationRemoved(String paramString)
    {
      this.LE.zza(new GamesClientImpl.InvitationRemovedNotifier(paramString));
    }
    
    public void zzs(DataHolder paramDataHolder)
    {
      InvitationBuffer localInvitationBuffer = new InvitationBuffer(paramDataHolder);
      paramDataHolder = null;
      try
      {
        if (localInvitationBuffer.getCount() > 0) {
          paramDataHolder = (Invitation)((Invitation)localInvitationBuffer.get(0)).freeze();
        }
        localInvitationBuffer.release();
        if (paramDataHolder != null) {
          this.LE.zza(new GamesClientImpl.InvitationReceivedNotifier(paramDataHolder));
        }
        return;
      }
      finally
      {
        localInvitationBuffer.release();
      }
    }
  }
  
  private static final class InvitationReceivedNotifier
    implements zzrr.zzc<OnInvitationReceivedListener>
  {
    private final Invitation Zn;
    
    InvitationReceivedNotifier(Invitation paramInvitation)
    {
      this.Zn = paramInvitation;
    }
    
    public void zza(OnInvitationReceivedListener paramOnInvitationReceivedListener)
    {
      paramOnInvitationReceivedListener.onInvitationReceived(this.Zn);
    }
    
    public void zzasm() {}
  }
  
  private static final class InvitationRemovedNotifier
    implements zzrr.zzc<OnInvitationReceivedListener>
  {
    private final String hm;
    
    InvitationRemovedNotifier(String paramString)
    {
      this.hm = paramString;
    }
    
    public void zza(OnInvitationReceivedListener paramOnInvitationReceivedListener)
    {
      paramOnInvitationReceivedListener.onInvitationRemoved(this.hm);
    }
    
    public void zzasm() {}
  }
  
  private static final class InvitationsLoadedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Invitations.LoadInvitationsResult> EW;
    
    InvitationsLoadedBinderCallback(zzqo.zzb<Invitations.LoadInvitationsResult> paramzzb)
    {
      this.EW = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzr(DataHolder paramDataHolder)
    {
      this.EW.setResult(new GamesClientImpl.LoadInvitationsResultImpl(paramDataHolder));
    }
  }
  
  private static final class InviteUpdateResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements Social.InviteUpdateResult
  {
    private final SocialInvite Zo;
    
    /* Error */
    InviteUpdateResultImpl(DataHolder paramDataHolder)
    {
      // Byte code:
      //   0: aload_0
      //   1: aload_1
      //   2: invokespecial 15	com/google/android/gms/games/internal/GamesClientImpl$GamesDataHolderResult:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   5: new 17	com/google/android/gms/games/social/SocialInviteBuffer
      //   8: dup
      //   9: aload_1
      //   10: invokespecial 18	com/google/android/gms/games/social/SocialInviteBuffer:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   13: astore_1
      //   14: aload_1
      //   15: invokevirtual 22	com/google/android/gms/games/social/SocialInviteBuffer:getCount	()I
      //   18: ifle +27 -> 45
      //   21: aload_0
      //   22: new 24	com/google/android/gms/games/social/SocialInviteEntity
      //   25: dup
      //   26: aload_1
      //   27: iconst_0
      //   28: invokevirtual 28	com/google/android/gms/games/social/SocialInviteBuffer:get	(I)Ljava/lang/Object;
      //   31: checkcast 30	com/google/android/gms/games/social/SocialInvite
      //   34: invokespecial 33	com/google/android/gms/games/social/SocialInviteEntity:<init>	(Lcom/google/android/gms/games/social/SocialInvite;)V
      //   37: putfield 35	com/google/android/gms/games/internal/GamesClientImpl$InviteUpdateResultImpl:Zo	Lcom/google/android/gms/games/social/SocialInvite;
      //   40: aload_1
      //   41: invokevirtual 39	com/google/android/gms/games/social/SocialInviteBuffer:release	()V
      //   44: return
      //   45: aload_0
      //   46: aconst_null
      //   47: putfield 35	com/google/android/gms/games/internal/GamesClientImpl$InviteUpdateResultImpl:Zo	Lcom/google/android/gms/games/social/SocialInvite;
      //   50: goto -10 -> 40
      //   53: astore_2
      //   54: aload_1
      //   55: invokevirtual 39	com/google/android/gms/games/social/SocialInviteBuffer:release	()V
      //   58: aload_2
      //   59: athrow
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	60	0	this	InviteUpdateResultImpl
      //   0	60	1	paramDataHolder	DataHolder
      //   53	6	2	localObject	Object
      // Exception table:
      //   from	to	target	type
      //   14	40	53	finally
      //   45	50	53	finally
    }
  }
  
  private static final class InvitesLoadedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Social.LoadInvitesResult> EW;
    
    InvitesLoadedBinderCallback(zzqo.zzb<Social.LoadInvitesResult> paramzzb)
    {
      this.EW = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzbh(DataHolder paramDataHolder)
    {
      this.EW.setResult(new GamesClientImpl.LoadInvitesResultImpl(paramDataHolder));
    }
  }
  
  private static final class JoinedRoomNotifier
    extends GamesClientImpl.AbstractRoomNotifier
  {
    public JoinedRoomNotifier(DataHolder paramDataHolder)
    {
      super();
    }
    
    public void zza(RoomUpdateListener paramRoomUpdateListener, Room paramRoom, int paramInt)
    {
      paramRoomUpdateListener.onJoinedRoom(paramInt, paramRoom);
    }
  }
  
  private static final class LeaderboardMetadataResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements Leaderboards.LeaderboardMetadataResult
  {
    private final LeaderboardBuffer Zp;
    
    LeaderboardMetadataResultImpl(DataHolder paramDataHolder)
    {
      super();
      this.Zp = new LeaderboardBuffer(paramDataHolder);
    }
    
    public LeaderboardBuffer getLeaderboards()
    {
      return this.Zp;
    }
  }
  
  private static final class LeaderboardScoresLoadedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Leaderboards.LoadScoresResult> EW;
    
    LeaderboardScoresLoadedBinderCallback(zzqo.zzb<Leaderboards.LoadScoresResult> paramzzb)
    {
      this.EW = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zza(DataHolder paramDataHolder1, DataHolder paramDataHolder2)
    {
      this.EW.setResult(new GamesClientImpl.LoadScoresResultImpl(paramDataHolder1, paramDataHolder2));
    }
  }
  
  private static final class LeaderboardsLoadedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Leaderboards.LeaderboardMetadataResult> EW;
    
    LeaderboardsLoadedBinderCallback(zzqo.zzb<Leaderboards.LeaderboardMetadataResult> paramzzb)
    {
      this.EW = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzj(DataHolder paramDataHolder)
    {
      this.EW.setResult(new GamesClientImpl.LeaderboardMetadataResultImpl(paramDataHolder));
    }
  }
  
  private static final class LeaveMatchResultImpl
    extends GamesClientImpl.TurnBasedMatchResult
    implements TurnBasedMultiplayer.LeaveMatchResult
  {
    LeaveMatchResultImpl(DataHolder paramDataHolder)
    {
      super();
    }
  }
  
  private static final class LeftRoomNotifier
    implements zzrr.zzc<RoomUpdateListener>
  {
    private final String Zq;
    private final int uo;
    
    LeftRoomNotifier(int paramInt, String paramString)
    {
      this.uo = paramInt;
      this.Zq = paramString;
    }
    
    public void zza(RoomUpdateListener paramRoomUpdateListener)
    {
      paramRoomUpdateListener.onLeftRoom(this.uo, this.Zq);
    }
    
    public void zzasm() {}
  }
  
  private static final class ListVideosBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Videos.ListVideosResult> EW;
    
    ListVideosBinderCallback(zzqo.zzb<Videos.ListVideosResult> paramzzb)
    {
      this.EW = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzax(DataHolder paramDataHolder)
    {
      this.EW.setResult(new GamesClientImpl.ListVideosResultImpl(paramDataHolder));
    }
  }
  
  private static final class ListVideosResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements Videos.ListVideosResult
  {
    private final VideoBuffer Zr;
    
    public ListVideosResultImpl(DataHolder paramDataHolder)
    {
      super();
      this.Zr = new VideoBuffer(paramDataHolder);
    }
  }
  
  private static final class LoadAchievementsResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements Achievements.LoadAchievementsResult
  {
    private final AchievementBuffer Zs;
    
    LoadAchievementsResultImpl(DataHolder paramDataHolder)
    {
      super();
      this.Zs = new AchievementBuffer(paramDataHolder);
    }
    
    public AchievementBuffer getAchievements()
    {
      return this.Zs;
    }
  }
  
  private static final class LoadAppContentsResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements AppContents.LoadAppContentResult
  {
    private final ArrayList<DataHolder> Zt;
    
    LoadAppContentsResultImpl(DataHolder[] paramArrayOfDataHolder)
    {
      super();
      this.Zt = new ArrayList(Arrays.asList(paramArrayOfDataHolder));
    }
  }
  
  private static final class LoadEventResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements Events.LoadEventsResult
  {
    private final EventBuffer Zu;
    
    LoadEventResultImpl(DataHolder paramDataHolder)
    {
      super();
      this.Zu = new EventBuffer(paramDataHolder);
    }
    
    public EventBuffer getEvents()
    {
      return this.Zu;
    }
  }
  
  private static final class LoadGameInstancesResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements GamesMetadata.LoadGameInstancesResult
  {
    private final GameInstanceBuffer Zv;
    
    LoadGameInstancesResultImpl(DataHolder paramDataHolder)
    {
      super();
      this.Zv = new GameInstanceBuffer(paramDataHolder);
    }
  }
  
  private static final class LoadGameSearchSuggestionsResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements GamesMetadata.LoadGameSearchSuggestionsResult
  {
    private final GameSearchSuggestionBuffer Zw;
    
    LoadGameSearchSuggestionsResultImpl(DataHolder paramDataHolder)
    {
      super();
      this.Zw = new GameSearchSuggestionBuffer(paramDataHolder);
    }
  }
  
  private static final class LoadGamesResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements GamesMetadata.LoadGamesResult
  {
    private final GameBuffer Zx;
    
    LoadGamesResultImpl(DataHolder paramDataHolder)
    {
      super();
      this.Zx = new GameBuffer(paramDataHolder);
    }
    
    public GameBuffer getGames()
    {
      return this.Zx;
    }
  }
  
  private static final class LoadInvitationsResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements Invitations.LoadInvitationsResult
  {
    private final InvitationBuffer Zy;
    
    LoadInvitationsResultImpl(DataHolder paramDataHolder)
    {
      super();
      this.Zy = new InvitationBuffer(paramDataHolder);
    }
    
    public InvitationBuffer getInvitations()
    {
      return this.Zy;
    }
  }
  
  private static final class LoadInvitesResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements Social.LoadInvitesResult
  {
    private final SocialInviteBuffer Zz;
    
    LoadInvitesResultImpl(DataHolder paramDataHolder)
    {
      super();
      this.Zz = new SocialInviteBuffer(paramDataHolder);
    }
  }
  
  private static final class LoadMatchResultImpl
    extends GamesClientImpl.TurnBasedMatchResult
    implements TurnBasedMultiplayer.LoadMatchResult
  {
    LoadMatchResultImpl(DataHolder paramDataHolder)
    {
      super();
    }
  }
  
  private static final class LoadMatchesResultImpl
    implements TurnBasedMultiplayer.LoadMatchesResult
  {
    private final LoadMatchesResponse ZA;
    private final Status hv;
    
    LoadMatchesResultImpl(Status paramStatus, Bundle paramBundle)
    {
      this.hv = paramStatus;
      this.ZA = new LoadMatchesResponse(paramBundle);
    }
    
    public LoadMatchesResponse getMatches()
    {
      return this.ZA;
    }
    
    public Status getStatus()
    {
      return this.hv;
    }
    
    public void release()
    {
      this.ZA.release();
    }
  }
  
  private static final class LoadPlayerScoreResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements Leaderboards.LoadPlayerScoreResult
  {
    private final LeaderboardScoreEntity ZB;
    
    /* Error */
    LoadPlayerScoreResultImpl(DataHolder paramDataHolder)
    {
      // Byte code:
      //   0: aload_0
      //   1: aload_1
      //   2: invokespecial 15	com/google/android/gms/games/internal/GamesClientImpl$GamesDataHolderResult:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   5: new 17	com/google/android/gms/games/leaderboard/LeaderboardScoreBuffer
      //   8: dup
      //   9: aload_1
      //   10: invokespecial 18	com/google/android/gms/games/leaderboard/LeaderboardScoreBuffer:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   13: astore_1
      //   14: aload_1
      //   15: invokevirtual 22	com/google/android/gms/games/leaderboard/LeaderboardScoreBuffer:getCount	()I
      //   18: ifle +28 -> 46
      //   21: aload_0
      //   22: aload_1
      //   23: iconst_0
      //   24: invokevirtual 26	com/google/android/gms/games/leaderboard/LeaderboardScoreBuffer:get	(I)Ljava/lang/Object;
      //   27: checkcast 28	com/google/android/gms/games/leaderboard/LeaderboardScore
      //   30: invokeinterface 32 1 0
      //   35: checkcast 34	com/google/android/gms/games/leaderboard/LeaderboardScoreEntity
      //   38: putfield 36	com/google/android/gms/games/internal/GamesClientImpl$LoadPlayerScoreResultImpl:ZB	Lcom/google/android/gms/games/leaderboard/LeaderboardScoreEntity;
      //   41: aload_1
      //   42: invokevirtual 40	com/google/android/gms/games/leaderboard/LeaderboardScoreBuffer:release	()V
      //   45: return
      //   46: aload_0
      //   47: aconst_null
      //   48: putfield 36	com/google/android/gms/games/internal/GamesClientImpl$LoadPlayerScoreResultImpl:ZB	Lcom/google/android/gms/games/leaderboard/LeaderboardScoreEntity;
      //   51: goto -10 -> 41
      //   54: astore_2
      //   55: aload_1
      //   56: invokevirtual 40	com/google/android/gms/games/leaderboard/LeaderboardScoreBuffer:release	()V
      //   59: aload_2
      //   60: athrow
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	61	0	this	LoadPlayerScoreResultImpl
      //   0	61	1	paramDataHolder	DataHolder
      //   54	6	2	localObject	Object
      // Exception table:
      //   from	to	target	type
      //   14	41	54	finally
      //   46	51	54	finally
    }
    
    public LeaderboardScore getScore()
    {
      return this.ZB;
    }
  }
  
  private static final class LoadPlayerStatsResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements Stats.LoadPlayerStatsResult
  {
    private final PlayerStats ZC;
    
    /* Error */
    LoadPlayerStatsResultImpl(DataHolder paramDataHolder)
    {
      // Byte code:
      //   0: aload_0
      //   1: aload_1
      //   2: invokespecial 15	com/google/android/gms/games/internal/GamesClientImpl$GamesDataHolderResult:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   5: new 17	com/google/android/gms/games/stats/PlayerStatsBuffer
      //   8: dup
      //   9: aload_1
      //   10: invokespecial 18	com/google/android/gms/games/stats/PlayerStatsBuffer:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   13: astore_1
      //   14: aload_1
      //   15: invokevirtual 22	com/google/android/gms/games/stats/PlayerStatsBuffer:getCount	()I
      //   18: ifle +27 -> 45
      //   21: aload_0
      //   22: new 24	com/google/android/gms/games/stats/PlayerStatsEntity
      //   25: dup
      //   26: aload_1
      //   27: iconst_0
      //   28: invokevirtual 28	com/google/android/gms/games/stats/PlayerStatsBuffer:get	(I)Ljava/lang/Object;
      //   31: checkcast 30	com/google/android/gms/games/stats/PlayerStats
      //   34: invokespecial 33	com/google/android/gms/games/stats/PlayerStatsEntity:<init>	(Lcom/google/android/gms/games/stats/PlayerStats;)V
      //   37: putfield 35	com/google/android/gms/games/internal/GamesClientImpl$LoadPlayerStatsResultImpl:ZC	Lcom/google/android/gms/games/stats/PlayerStats;
      //   40: aload_1
      //   41: invokevirtual 39	com/google/android/gms/games/stats/PlayerStatsBuffer:release	()V
      //   44: return
      //   45: aload_0
      //   46: aconst_null
      //   47: putfield 35	com/google/android/gms/games/internal/GamesClientImpl$LoadPlayerStatsResultImpl:ZC	Lcom/google/android/gms/games/stats/PlayerStats;
      //   50: goto -10 -> 40
      //   53: astore_2
      //   54: aload_1
      //   55: invokevirtual 39	com/google/android/gms/games/stats/PlayerStatsBuffer:release	()V
      //   58: aload_2
      //   59: athrow
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	60	0	this	LoadPlayerStatsResultImpl
      //   0	60	1	paramDataHolder	DataHolder
      //   53	6	2	localObject	Object
      // Exception table:
      //   from	to	target	type
      //   14	40	53	finally
      //   45	50	53	finally
    }
    
    public PlayerStats getPlayerStats()
    {
      return this.ZC;
    }
  }
  
  private static final class LoadPlayersResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements Players.LoadPlayersResult
  {
    private final PlayerBuffer ZD;
    
    LoadPlayersResultImpl(DataHolder paramDataHolder)
    {
      super();
      this.ZD = new PlayerBuffer(paramDataHolder);
    }
    
    public PlayerBuffer getPlayers()
    {
      return this.ZD;
    }
  }
  
  private static final class LoadQuestsResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements Quests.LoadQuestsResult
  {
    private final DataHolder zy;
    
    LoadQuestsResultImpl(DataHolder paramDataHolder)
    {
      super();
      this.zy = paramDataHolder;
    }
    
    public QuestBuffer getQuests()
    {
      return new QuestBuffer(this.zy);
    }
  }
  
  private static final class LoadRequestSummariesResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements Requests.LoadRequestSummariesResult
  {
    LoadRequestSummariesResultImpl(DataHolder paramDataHolder)
    {
      super();
    }
  }
  
  private static final class LoadRequestsResultImpl
    implements Requests.LoadRequestsResult
  {
    private final Bundle ZE;
    private final Status hv;
    
    LoadRequestsResultImpl(Status paramStatus, Bundle paramBundle)
    {
      this.hv = paramStatus;
      this.ZE = paramBundle;
    }
    
    public GameRequestBuffer getRequests(int paramInt)
    {
      String str = RequestType.zzrw(paramInt);
      if (!this.ZE.containsKey(str)) {
        return null;
      }
      return new GameRequestBuffer((DataHolder)this.ZE.get(str));
    }
    
    public Status getStatus()
    {
      return this.hv;
    }
    
    public void release()
    {
      Iterator localIterator = this.ZE.keySet().iterator();
      while (localIterator.hasNext())
      {
        Object localObject = (String)localIterator.next();
        localObject = (DataHolder)this.ZE.getParcelable((String)localObject);
        if (localObject != null) {
          ((DataHolder)localObject).close();
        }
      }
    }
  }
  
  private static final class LoadScoresResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements Leaderboards.LoadScoresResult
  {
    private final LeaderboardEntity ZF;
    private final LeaderboardScoreBuffer ZG;
    
    /* Error */
    LoadScoresResultImpl(DataHolder paramDataHolder1, DataHolder paramDataHolder2)
    {
      // Byte code:
      //   0: aload_0
      //   1: aload_2
      //   2: invokespecial 18	com/google/android/gms/games/internal/GamesClientImpl$GamesDataHolderResult:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   5: new 20	com/google/android/gms/games/leaderboard/LeaderboardBuffer
      //   8: dup
      //   9: aload_1
      //   10: invokespecial 21	com/google/android/gms/games/leaderboard/LeaderboardBuffer:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   13: astore_1
      //   14: aload_1
      //   15: invokevirtual 25	com/google/android/gms/games/leaderboard/LeaderboardBuffer:getCount	()I
      //   18: ifle +40 -> 58
      //   21: aload_0
      //   22: aload_1
      //   23: iconst_0
      //   24: invokevirtual 29	com/google/android/gms/games/leaderboard/LeaderboardBuffer:get	(I)Ljava/lang/Object;
      //   27: checkcast 31	com/google/android/gms/games/leaderboard/Leaderboard
      //   30: invokeinterface 35 1 0
      //   35: checkcast 37	com/google/android/gms/games/leaderboard/LeaderboardEntity
      //   38: putfield 39	com/google/android/gms/games/internal/GamesClientImpl$LoadScoresResultImpl:ZF	Lcom/google/android/gms/games/leaderboard/LeaderboardEntity;
      //   41: aload_1
      //   42: invokevirtual 43	com/google/android/gms/games/leaderboard/LeaderboardBuffer:release	()V
      //   45: aload_0
      //   46: new 45	com/google/android/gms/games/leaderboard/LeaderboardScoreBuffer
      //   49: dup
      //   50: aload_2
      //   51: invokespecial 46	com/google/android/gms/games/leaderboard/LeaderboardScoreBuffer:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   54: putfield 48	com/google/android/gms/games/internal/GamesClientImpl$LoadScoresResultImpl:ZG	Lcom/google/android/gms/games/leaderboard/LeaderboardScoreBuffer;
      //   57: return
      //   58: aload_0
      //   59: aconst_null
      //   60: putfield 39	com/google/android/gms/games/internal/GamesClientImpl$LoadScoresResultImpl:ZF	Lcom/google/android/gms/games/leaderboard/LeaderboardEntity;
      //   63: goto -22 -> 41
      //   66: astore_2
      //   67: aload_1
      //   68: invokevirtual 43	com/google/android/gms/games/leaderboard/LeaderboardBuffer:release	()V
      //   71: aload_2
      //   72: athrow
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	73	0	this	LoadScoresResultImpl
      //   0	73	1	paramDataHolder1	DataHolder
      //   0	73	2	paramDataHolder2	DataHolder
      // Exception table:
      //   from	to	target	type
      //   14	41	66	finally
      //   58	63	66	finally
    }
    
    public Leaderboard getLeaderboard()
    {
      return this.ZF;
    }
    
    public LeaderboardScoreBuffer getScores()
    {
      return this.ZG;
    }
  }
  
  private static final class LoadSnapshotsResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements Snapshots.LoadSnapshotsResult
  {
    LoadSnapshotsResultImpl(DataHolder paramDataHolder)
    {
      super();
    }
    
    public SnapshotMetadataBuffer getSnapshots()
    {
      return new SnapshotMetadataBuffer(this.zy);
    }
  }
  
  private static final class LoadStockProfileImagesResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements Players.LoadStockProfileImagesResult
  {
    private final StockProfileImageBuffer ZH;
    
    LoadStockProfileImagesResultImpl(DataHolder paramDataHolder)
    {
      super();
      this.ZH = new StockProfileImageBuffer(paramDataHolder);
    }
    
    public Status getStatus()
    {
      return this.hv;
    }
  }
  
  private static final class LoadXpForGameCategoriesResultImpl
    implements Players.LoadXpForGameCategoriesResult
  {
    private final List<String> ZI;
    private final Bundle ZJ;
    private final Status hv;
    
    LoadXpForGameCategoriesResultImpl(Status paramStatus, Bundle paramBundle)
    {
      this.hv = paramStatus;
      this.ZI = paramBundle.getStringArrayList("game_category_list");
      this.ZJ = paramBundle;
    }
    
    public Status getStatus()
    {
      return this.hv;
    }
  }
  
  private static final class LoadXpStreamResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements Players.LoadXpStreamResult
  {
    private final ExperienceEventBuffer ZK;
    
    LoadXpStreamResultImpl(DataHolder paramDataHolder)
    {
      super();
      this.ZK = new ExperienceEventBuffer(paramDataHolder);
    }
  }
  
  private static final class MatchRemovedNotifier
    implements zzrr.zzc<OnTurnBasedMatchUpdateReceivedListener>
  {
    private final String ZL;
    
    MatchRemovedNotifier(String paramString)
    {
      this.ZL = paramString;
    }
    
    public void zza(OnTurnBasedMatchUpdateReceivedListener paramOnTurnBasedMatchUpdateReceivedListener)
    {
      paramOnTurnBasedMatchUpdateReceivedListener.onTurnBasedMatchRemoved(this.ZL);
    }
    
    public void zzasm() {}
  }
  
  private static final class MatchUpdateReceivedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zzrr<OnTurnBasedMatchUpdateReceivedListener> LE;
    
    MatchUpdateReceivedBinderCallback(zzrr<OnTurnBasedMatchUpdateReceivedListener> paramzzrr)
    {
      this.LE = paramzzrr;
    }
    
    public void onTurnBasedMatchRemoved(String paramString)
    {
      this.LE.zza(new GamesClientImpl.MatchRemovedNotifier(paramString));
    }
    
    public void zzy(DataHolder paramDataHolder)
    {
      TurnBasedMatchBuffer localTurnBasedMatchBuffer = new TurnBasedMatchBuffer(paramDataHolder);
      paramDataHolder = null;
      try
      {
        if (localTurnBasedMatchBuffer.getCount() > 0) {
          paramDataHolder = (TurnBasedMatch)((TurnBasedMatch)localTurnBasedMatchBuffer.get(0)).freeze();
        }
        localTurnBasedMatchBuffer.release();
        if (paramDataHolder != null) {
          this.LE.zza(new GamesClientImpl.MatchUpdateReceivedNotifier(paramDataHolder));
        }
        return;
      }
      finally
      {
        localTurnBasedMatchBuffer.release();
      }
    }
  }
  
  private static final class MatchUpdateReceivedNotifier
    implements zzrr.zzc<OnTurnBasedMatchUpdateReceivedListener>
  {
    private final TurnBasedMatch ZM;
    
    MatchUpdateReceivedNotifier(TurnBasedMatch paramTurnBasedMatch)
    {
      this.ZM = paramTurnBasedMatch;
    }
    
    public void zza(OnTurnBasedMatchUpdateReceivedListener paramOnTurnBasedMatchUpdateReceivedListener)
    {
      paramOnTurnBasedMatchUpdateReceivedListener.onTurnBasedMatchReceived(this.ZM);
    }
    
    public void zzasm() {}
  }
  
  private static final class MessageReceivedNotifier
    implements zzrr.zzc<RealTimeMessageReceivedListener>
  {
    private final RealTimeMessage ZN;
    
    MessageReceivedNotifier(RealTimeMessage paramRealTimeMessage)
    {
      this.ZN = paramRealTimeMessage;
    }
    
    public void zza(RealTimeMessageReceivedListener paramRealTimeMessageReceivedListener)
    {
      paramRealTimeMessageReceivedListener.onRealTimeMessageReceived(this.ZN);
    }
    
    public void zzasm() {}
  }
  
  private static final class NearbyPlayerDetectedNotifier
    implements zzrr.zzc<OnNearbyPlayerDetectedListener>
  {
    private final Player ZO;
    
    public void zza(OnNearbyPlayerDetectedListener paramOnNearbyPlayerDetectedListener)
    {
      paramOnNearbyPlayerDetectedListener.zza(this.ZO);
    }
    
    public void zzasm() {}
  }
  
  private static final class OpenSnapshotResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements Snapshots.OpenSnapshotResult
  {
    private final Snapshot ZP;
    private final String ZQ;
    private final Snapshot ZR;
    private final Contents ZS;
    private final SnapshotContents ZT;
    
    OpenSnapshotResultImpl(DataHolder paramDataHolder, Contents paramContents)
    {
      this(paramDataHolder, null, paramContents, null, null);
    }
    
    OpenSnapshotResultImpl(DataHolder paramDataHolder, String paramString, Contents paramContents1, Contents paramContents2, Contents paramContents3)
    {
      super();
      SnapshotMetadataBuffer localSnapshotMetadataBuffer = new SnapshotMetadataBuffer(paramDataHolder);
      for (;;)
      {
        try
        {
          if (localSnapshotMetadataBuffer.getCount() == 0)
          {
            this.ZP = null;
            this.ZR = null;
            localSnapshotMetadataBuffer.release();
            this.ZQ = paramString;
            this.ZS = paramContents3;
            this.ZT = new SnapshotContentsEntity(paramContents3);
            return;
          }
          if (localSnapshotMetadataBuffer.getCount() != 1) {
            break label147;
          }
          if (paramDataHolder.getStatusCode() != 4004)
          {
            zzc.zzbs(bool);
            this.ZP = new SnapshotEntity(new SnapshotMetadataEntity((SnapshotMetadata)localSnapshotMetadataBuffer.get(0)), new SnapshotContentsEntity(paramContents1));
            this.ZR = null;
            continue;
          }
          bool = false;
        }
        finally
        {
          localSnapshotMetadataBuffer.release();
        }
        continue;
        label147:
        this.ZP = new SnapshotEntity(new SnapshotMetadataEntity((SnapshotMetadata)localSnapshotMetadataBuffer.get(0)), new SnapshotContentsEntity(paramContents1));
        this.ZR = new SnapshotEntity(new SnapshotMetadataEntity((SnapshotMetadata)localSnapshotMetadataBuffer.get(1)), new SnapshotContentsEntity(paramContents2));
      }
    }
    
    public String getConflictId()
    {
      return this.ZQ;
    }
    
    public Snapshot getConflictingSnapshot()
    {
      return this.ZR;
    }
    
    public SnapshotContents getResolutionSnapshotContents()
    {
      return this.ZT;
    }
    
    public Snapshot getSnapshot()
    {
      return this.ZP;
    }
  }
  
  private static final class P2PConnectedNotifier
    implements zzrr.zzc<RoomStatusUpdateListener>
  {
    private final String ZU;
    
    P2PConnectedNotifier(String paramString)
    {
      this.ZU = paramString;
    }
    
    public void zza(RoomStatusUpdateListener paramRoomStatusUpdateListener)
    {
      paramRoomStatusUpdateListener.onP2PConnected(this.ZU);
    }
    
    public void zzasm() {}
  }
  
  private static final class P2PDisconnectedNotifier
    implements zzrr.zzc<RoomStatusUpdateListener>
  {
    private final String ZU;
    
    P2PDisconnectedNotifier(String paramString)
    {
      this.ZU = paramString;
    }
    
    public void zza(RoomStatusUpdateListener paramRoomStatusUpdateListener)
    {
      paramRoomStatusUpdateListener.onP2PDisconnected(this.ZU);
    }
    
    public void zzasm() {}
  }
  
  private static final class PeerConnectedNotifier
    extends GamesClientImpl.AbstractPeerStatusNotifier
  {
    PeerConnectedNotifier(DataHolder paramDataHolder, String[] paramArrayOfString)
    {
      super(paramArrayOfString);
    }
    
    protected void zza(RoomStatusUpdateListener paramRoomStatusUpdateListener, Room paramRoom, ArrayList<String> paramArrayList)
    {
      paramRoomStatusUpdateListener.onPeersConnected(paramRoom, paramArrayList);
    }
  }
  
  private static final class PeerDeclinedNotifier
    extends GamesClientImpl.AbstractPeerStatusNotifier
  {
    PeerDeclinedNotifier(DataHolder paramDataHolder, String[] paramArrayOfString)
    {
      super(paramArrayOfString);
    }
    
    protected void zza(RoomStatusUpdateListener paramRoomStatusUpdateListener, Room paramRoom, ArrayList<String> paramArrayList)
    {
      paramRoomStatusUpdateListener.onPeerDeclined(paramRoom, paramArrayList);
    }
  }
  
  private static final class PeerDisconnectedNotifier
    extends GamesClientImpl.AbstractPeerStatusNotifier
  {
    PeerDisconnectedNotifier(DataHolder paramDataHolder, String[] paramArrayOfString)
    {
      super(paramArrayOfString);
    }
    
    protected void zza(RoomStatusUpdateListener paramRoomStatusUpdateListener, Room paramRoom, ArrayList<String> paramArrayList)
    {
      paramRoomStatusUpdateListener.onPeersDisconnected(paramRoom, paramArrayList);
    }
  }
  
  private static final class PeerInvitedToRoomNotifier
    extends GamesClientImpl.AbstractPeerStatusNotifier
  {
    PeerInvitedToRoomNotifier(DataHolder paramDataHolder, String[] paramArrayOfString)
    {
      super(paramArrayOfString);
    }
    
    protected void zza(RoomStatusUpdateListener paramRoomStatusUpdateListener, Room paramRoom, ArrayList<String> paramArrayList)
    {
      paramRoomStatusUpdateListener.onPeerInvitedToRoom(paramRoom, paramArrayList);
    }
  }
  
  private static final class PeerJoinedRoomNotifier
    extends GamesClientImpl.AbstractPeerStatusNotifier
  {
    PeerJoinedRoomNotifier(DataHolder paramDataHolder, String[] paramArrayOfString)
    {
      super(paramArrayOfString);
    }
    
    protected void zza(RoomStatusUpdateListener paramRoomStatusUpdateListener, Room paramRoom, ArrayList<String> paramArrayList)
    {
      paramRoomStatusUpdateListener.onPeerJoined(paramRoom, paramArrayList);
    }
  }
  
  private static final class PeerLeftRoomNotifier
    extends GamesClientImpl.AbstractPeerStatusNotifier
  {
    PeerLeftRoomNotifier(DataHolder paramDataHolder, String[] paramArrayOfString)
    {
      super(paramArrayOfString);
    }
    
    protected void zza(RoomStatusUpdateListener paramRoomStatusUpdateListener, Room paramRoom, ArrayList<String> paramArrayList)
    {
      paramRoomStatusUpdateListener.onPeerLeft(paramRoom, paramArrayList);
    }
  }
  
  private static final class PlayerLeaderboardScoreLoadedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Leaderboards.LoadPlayerScoreResult> EW;
    
    PlayerLeaderboardScoreLoadedBinderCallback(zzqo.zzb<Leaderboards.LoadPlayerScoreResult> paramzzb)
    {
      this.EW = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzaj(DataHolder paramDataHolder)
    {
      this.EW.setResult(new GamesClientImpl.LoadPlayerScoreResultImpl(paramDataHolder));
    }
  }
  
  private static final class PlayerStatsLoadedBinderCallbacks
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Stats.LoadPlayerStatsResult> EW;
    
    public PlayerStatsLoadedBinderCallbacks(zzqo.zzb<Stats.LoadPlayerStatsResult> paramzzb)
    {
      this.EW = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzaw(DataHolder paramDataHolder)
    {
      this.EW.setResult(new GamesClientImpl.LoadPlayerStatsResultImpl(paramDataHolder));
    }
  }
  
  private static final class PlayerUnfriendedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Players.LoadPlayersResult> EW;
    
    PlayerUnfriendedBinderCallback(zzqo.zzb<Players.LoadPlayersResult> paramzzb)
    {
      this.EW = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzbf(DataHolder paramDataHolder)
    {
      this.EW.setResult(new GamesClientImpl.LoadPlayersResultImpl(paramDataHolder));
    }
  }
  
  private static final class PlayerXpForGameCategoriesLoadedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Players.LoadXpForGameCategoriesResult> EW;
    
    PlayerXpForGameCategoriesLoadedBinderCallback(zzqo.zzb<Players.LoadXpForGameCategoriesResult> paramzzb)
    {
      this.EW = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzf(int paramInt, Bundle paramBundle)
    {
      paramBundle.setClassLoader(getClass().getClassLoader());
      Status localStatus = GamesStatusCodes.zzqv(paramInt);
      this.EW.setResult(new GamesClientImpl.LoadXpForGameCategoriesResultImpl(localStatus, paramBundle));
    }
  }
  
  static final class PlayerXpStreamLoadedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Players.LoadXpStreamResult> EW;
    
    PlayerXpStreamLoadedBinderCallback(zzqo.zzb<Players.LoadXpStreamResult> paramzzb)
    {
      this.EW = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzau(DataHolder paramDataHolder)
    {
      this.EW.setResult(new GamesClientImpl.LoadXpStreamResultImpl(paramDataHolder));
    }
  }
  
  private static final class PlayersLoadedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Players.LoadPlayersResult> EW;
    
    PlayersLoadedBinderCallback(zzqo.zzb<Players.LoadPlayersResult> paramzzb)
    {
      this.EW = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzl(DataHolder paramDataHolder)
    {
      this.EW.setResult(new GamesClientImpl.LoadPlayersResultImpl(paramDataHolder));
    }
    
    public void zzm(DataHolder paramDataHolder)
    {
      this.EW.setResult(new GamesClientImpl.LoadPlayersResultImpl(paramDataHolder));
    }
  }
  
  private static final class PopupLocationInfoBinderCallbacks
    extends AbstractGamesClient
  {
    private final PopupManager YP;
    
    public PopupLocationInfoBinderCallbacks(PopupManager paramPopupManager)
    {
      this.YP = paramPopupManager;
    }
    
    public PopupLocationInfoParcelable zzbjl()
    {
      return new PopupLocationInfoParcelable(this.YP.zzble());
    }
  }
  
  private static final class ProfileSettingsLoadedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Players.LoadProfileSettingsResult> EW;
    
    ProfileSettingsLoadedBinderCallback(zzqo.zzb<Players.LoadProfileSettingsResult> paramzzb)
    {
      this.EW = paramzzb;
    }
    
    public void zzav(DataHolder paramDataHolder)
    {
      this.EW.setResult(new ProfileSettingsEntity(paramDataHolder));
      paramDataHolder.close();
    }
  }
  
  private static final class ProfileSettingsUpdatedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Status> EW;
    
    ProfileSettingsUpdatedBinderCallback(zzqo.zzb<Status> paramzzb)
    {
      this.EW = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzrj(int paramInt)
    {
      this.EW.setResult(GamesStatusCodes.zzqv(paramInt));
    }
  }
  
  private static final class QuestAcceptedBinderCallbacks
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Quests.AcceptQuestResult> ZV;
    
    public QuestAcceptedBinderCallbacks(zzqo.zzb<Quests.AcceptQuestResult> paramzzb)
    {
      this.ZV = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzaq(DataHolder paramDataHolder)
    {
      this.ZV.setResult(new GamesClientImpl.AcceptQuestResultImpl(paramDataHolder));
    }
  }
  
  private static final class QuestCompletedNotifier
    implements zzrr.zzc<QuestUpdateListener>
  {
    private final Quest YW;
    
    QuestCompletedNotifier(Quest paramQuest)
    {
      this.YW = paramQuest;
    }
    
    public void zza(QuestUpdateListener paramQuestUpdateListener)
    {
      paramQuestUpdateListener.onQuestCompleted(this.YW);
    }
    
    public void zzasm() {}
  }
  
  private static final class QuestMilestoneClaimBinderCallbacks
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Quests.ClaimMilestoneResult> ZW;
    private final String ZX;
    
    public QuestMilestoneClaimBinderCallbacks(zzqo.zzb<Quests.ClaimMilestoneResult> paramzzb, String paramString)
    {
      this.ZW = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
      this.ZX = ((String)zzaa.zzb(paramString, "MilestoneId must not be null"));
    }
    
    public void zzap(DataHolder paramDataHolder)
    {
      this.ZW.setResult(new GamesClientImpl.ClaimMilestoneResultImpl(paramDataHolder, this.ZX));
    }
  }
  
  private static final class QuestUpdateBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zzrr<QuestUpdateListener> LE;
    
    QuestUpdateBinderCallback(zzrr<QuestUpdateListener> paramzzrr)
    {
      this.LE = paramzzrr;
    }
    
    private Quest zzbk(DataHolder paramDataHolder)
    {
      QuestBuffer localQuestBuffer = new QuestBuffer(paramDataHolder);
      paramDataHolder = null;
      try
      {
        if (localQuestBuffer.getCount() > 0) {
          paramDataHolder = (Quest)((Quest)localQuestBuffer.get(0)).freeze();
        }
        return paramDataHolder;
      }
      finally
      {
        localQuestBuffer.release();
      }
    }
    
    public void zzar(DataHolder paramDataHolder)
    {
      paramDataHolder = zzbk(paramDataHolder);
      if (paramDataHolder != null) {
        this.LE.zza(new GamesClientImpl.QuestCompletedNotifier(paramDataHolder));
      }
    }
  }
  
  private static final class QuestsLoadedBinderCallbacks
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Quests.LoadQuestsResult> ZY;
    
    public QuestsLoadedBinderCallbacks(zzqo.zzb<Quests.LoadQuestsResult> paramzzb)
    {
      this.ZY = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzat(DataHolder paramDataHolder)
    {
      this.ZY.setResult(new GamesClientImpl.LoadQuestsResultImpl(paramDataHolder));
    }
  }
  
  private static final class RealTimeMessageSentNotifier
    implements zzrr.zzc<RealTimeMultiplayer.ReliableMessageSentCallback>
  {
    private final String ZZ;
    private final int aaa;
    private final int uo;
    
    RealTimeMessageSentNotifier(int paramInt1, int paramInt2, String paramString)
    {
      this.uo = paramInt1;
      this.aaa = paramInt2;
      this.ZZ = paramString;
    }
    
    public void zza(RealTimeMultiplayer.ReliableMessageSentCallback paramReliableMessageSentCallback)
    {
      if (paramReliableMessageSentCallback != null) {
        paramReliableMessageSentCallback.onRealTimeMessageSent(this.uo, this.aaa, this.ZZ);
      }
    }
    
    public void zzasm() {}
  }
  
  private static final class RealTimeReliableMessageBinderCallbacks
    extends AbstractGamesCallbacks
  {
    final zzrr<RealTimeMultiplayer.ReliableMessageSentCallback> aab;
    
    public RealTimeReliableMessageBinderCallbacks(zzrr<RealTimeMultiplayer.ReliableMessageSentCallback> paramzzrr)
    {
      this.aab = paramzzrr;
    }
    
    public void zzb(int paramInt1, int paramInt2, String paramString)
    {
      if (this.aab != null) {
        this.aab.zza(new GamesClientImpl.RealTimeMessageSentNotifier(paramInt1, paramInt2, paramString));
      }
    }
  }
  
  private static final class RequestReceivedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zzrr<OnRequestReceivedListener> LE;
    
    RequestReceivedBinderCallback(zzrr<OnRequestReceivedListener> paramzzrr)
    {
      this.LE = paramzzrr;
    }
    
    public void onRequestRemoved(String paramString)
    {
      this.LE.zza(new GamesClientImpl.RequestRemovedNotifier(paramString));
    }
    
    public void zzt(DataHolder paramDataHolder)
    {
      GameRequestBuffer localGameRequestBuffer = new GameRequestBuffer(paramDataHolder);
      paramDataHolder = null;
      try
      {
        if (localGameRequestBuffer.getCount() > 0) {
          paramDataHolder = (GameRequest)((GameRequest)localGameRequestBuffer.get(0)).freeze();
        }
        localGameRequestBuffer.release();
        if (paramDataHolder != null) {
          this.LE.zza(new GamesClientImpl.RequestReceivedNotifier(paramDataHolder));
        }
        return;
      }
      finally
      {
        localGameRequestBuffer.release();
      }
    }
  }
  
  private static final class RequestReceivedNotifier
    implements zzrr.zzc<OnRequestReceivedListener>
  {
    private final GameRequest aac;
    
    RequestReceivedNotifier(GameRequest paramGameRequest)
    {
      this.aac = paramGameRequest;
    }
    
    public void zza(OnRequestReceivedListener paramOnRequestReceivedListener)
    {
      paramOnRequestReceivedListener.onRequestReceived(this.aac);
    }
    
    public void zzasm() {}
  }
  
  private static final class RequestRemovedNotifier
    implements zzrr.zzc<OnRequestReceivedListener>
  {
    private final String zzcec;
    
    RequestRemovedNotifier(String paramString)
    {
      this.zzcec = paramString;
    }
    
    public void zza(OnRequestReceivedListener paramOnRequestReceivedListener)
    {
      paramOnRequestReceivedListener.onRequestRemoved(this.zzcec);
    }
    
    public void zzasm() {}
  }
  
  private static final class RequestSentBinderCallbacks
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Requests.SendRequestResult> aad;
    
    public RequestSentBinderCallbacks(zzqo.zzb<Requests.SendRequestResult> paramzzb)
    {
      this.aad = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzal(DataHolder paramDataHolder)
    {
      this.aad.setResult(new GamesClientImpl.SendRequestResultImpl(paramDataHolder));
    }
  }
  
  private static final class RequestSummariesLoadedBinderCallbacks
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Requests.LoadRequestSummariesResult> aae;
    
    public RequestSummariesLoadedBinderCallbacks(zzqo.zzb<Requests.LoadRequestSummariesResult> paramzzb)
    {
      this.aae = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzam(DataHolder paramDataHolder)
    {
      this.aae.setResult(new GamesClientImpl.LoadRequestSummariesResultImpl(paramDataHolder));
    }
  }
  
  private static final class RequestsLoadedBinderCallbacks
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Requests.LoadRequestsResult> aaf;
    
    public RequestsLoadedBinderCallbacks(zzqo.zzb<Requests.LoadRequestsResult> paramzzb)
    {
      this.aaf = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzd(int paramInt, Bundle paramBundle)
    {
      paramBundle.setClassLoader(getClass().getClassLoader());
      Status localStatus = GamesStatusCodes.zzqv(paramInt);
      this.aaf.setResult(new GamesClientImpl.LoadRequestsResultImpl(localStatus, paramBundle));
    }
  }
  
  private static final class RequestsUpdatedBinderCallbacks
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Requests.UpdateRequestsResult> aag;
    
    public RequestsUpdatedBinderCallbacks(zzqo.zzb<Requests.UpdateRequestsResult> paramzzb)
    {
      this.aag = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzak(DataHolder paramDataHolder)
    {
      this.aag.setResult(new GamesClientImpl.UpdateRequestsResultImpl(paramDataHolder));
    }
  }
  
  private static final class RoomAutoMatchingNotifier
    extends GamesClientImpl.AbstractRoomStatusNotifier
  {
    RoomAutoMatchingNotifier(DataHolder paramDataHolder)
    {
      super();
    }
    
    public void zza(RoomStatusUpdateListener paramRoomStatusUpdateListener, Room paramRoom)
    {
      paramRoomStatusUpdateListener.onRoomAutoMatching(paramRoom);
    }
  }
  
  private static final class RoomBinderCallbacks
    extends AbstractGamesCallbacks
  {
    private final zzrr<? extends RoomUpdateListener> aah;
    private final zzrr<? extends RoomStatusUpdateListener> aai;
    private final zzrr<RealTimeMessageReceivedListener> aaj;
    
    public RoomBinderCallbacks(zzrr<RoomUpdateListener> paramzzrr)
    {
      this.aah = ((zzrr)zzaa.zzb(paramzzrr, "Callbacks must not be null"));
      this.aai = null;
      this.aaj = null;
    }
    
    public RoomBinderCallbacks(zzrr<? extends RoomUpdateListener> paramzzrr, zzrr<? extends RoomStatusUpdateListener> paramzzrr1, zzrr<RealTimeMessageReceivedListener> paramzzrr2)
    {
      this.aah = ((zzrr)zzaa.zzb(paramzzrr, "Callbacks must not be null"));
      this.aai = paramzzrr1;
      this.aaj = paramzzrr2;
    }
    
    public void onLeftRoom(int paramInt, String paramString)
    {
      this.aah.zza(new GamesClientImpl.LeftRoomNotifier(paramInt, paramString));
    }
    
    public void onP2PConnected(String paramString)
    {
      if (this.aai != null) {
        this.aai.zza(new GamesClientImpl.P2PConnectedNotifier(paramString));
      }
    }
    
    public void onP2PDisconnected(String paramString)
    {
      if (this.aai != null) {
        this.aai.zza(new GamesClientImpl.P2PDisconnectedNotifier(paramString));
      }
    }
    
    public void onRealTimeMessageReceived(RealTimeMessage paramRealTimeMessage)
    {
      if (this.aaj != null) {
        this.aaj.zza(new GamesClientImpl.MessageReceivedNotifier(paramRealTimeMessage));
      }
    }
    
    public void zza(DataHolder paramDataHolder, String[] paramArrayOfString)
    {
      if (this.aai != null) {
        this.aai.zza(new GamesClientImpl.PeerInvitedToRoomNotifier(paramDataHolder, paramArrayOfString));
      }
    }
    
    public void zzaa(DataHolder paramDataHolder)
    {
      this.aah.zza(new GamesClientImpl.JoinedRoomNotifier(paramDataHolder));
    }
    
    public void zzab(DataHolder paramDataHolder)
    {
      if (this.aai != null) {
        this.aai.zza(new GamesClientImpl.RoomConnectingNotifier(paramDataHolder));
      }
    }
    
    public void zzac(DataHolder paramDataHolder)
    {
      if (this.aai != null) {
        this.aai.zza(new GamesClientImpl.RoomAutoMatchingNotifier(paramDataHolder));
      }
    }
    
    public void zzad(DataHolder paramDataHolder)
    {
      this.aah.zza(new GamesClientImpl.RoomConnectedNotifier(paramDataHolder));
    }
    
    public void zzae(DataHolder paramDataHolder)
    {
      if (this.aai != null) {
        this.aai.zza(new GamesClientImpl.ConnectedToRoomNotifier(paramDataHolder));
      }
    }
    
    public void zzaf(DataHolder paramDataHolder)
    {
      if (this.aai != null) {
        this.aai.zza(new GamesClientImpl.DisconnectedFromRoomNotifier(paramDataHolder));
      }
    }
    
    public void zzb(DataHolder paramDataHolder, String[] paramArrayOfString)
    {
      if (this.aai != null) {
        this.aai.zza(new GamesClientImpl.PeerJoinedRoomNotifier(paramDataHolder, paramArrayOfString));
      }
    }
    
    public void zzc(DataHolder paramDataHolder, String[] paramArrayOfString)
    {
      if (this.aai != null) {
        this.aai.zza(new GamesClientImpl.PeerLeftRoomNotifier(paramDataHolder, paramArrayOfString));
      }
    }
    
    public void zzd(DataHolder paramDataHolder, String[] paramArrayOfString)
    {
      if (this.aai != null) {
        this.aai.zza(new GamesClientImpl.PeerDeclinedNotifier(paramDataHolder, paramArrayOfString));
      }
    }
    
    public void zze(DataHolder paramDataHolder, String[] paramArrayOfString)
    {
      if (this.aai != null) {
        this.aai.zza(new GamesClientImpl.PeerConnectedNotifier(paramDataHolder, paramArrayOfString));
      }
    }
    
    public void zzf(DataHolder paramDataHolder, String[] paramArrayOfString)
    {
      if (this.aai != null) {
        this.aai.zza(new GamesClientImpl.PeerDisconnectedNotifier(paramDataHolder, paramArrayOfString));
      }
    }
    
    public void zzz(DataHolder paramDataHolder)
    {
      this.aah.zza(new GamesClientImpl.RoomCreatedNotifier(paramDataHolder));
    }
  }
  
  private static final class RoomConnectedNotifier
    extends GamesClientImpl.AbstractRoomNotifier
  {
    RoomConnectedNotifier(DataHolder paramDataHolder)
    {
      super();
    }
    
    public void zza(RoomUpdateListener paramRoomUpdateListener, Room paramRoom, int paramInt)
    {
      paramRoomUpdateListener.onRoomConnected(paramInt, paramRoom);
    }
  }
  
  private static final class RoomConnectingNotifier
    extends GamesClientImpl.AbstractRoomStatusNotifier
  {
    RoomConnectingNotifier(DataHolder paramDataHolder)
    {
      super();
    }
    
    public void zza(RoomStatusUpdateListener paramRoomStatusUpdateListener, Room paramRoom)
    {
      paramRoomStatusUpdateListener.onRoomConnecting(paramRoom);
    }
  }
  
  private static final class RoomCreatedNotifier
    extends GamesClientImpl.AbstractRoomNotifier
  {
    public RoomCreatedNotifier(DataHolder paramDataHolder)
    {
      super();
    }
    
    public void zza(RoomUpdateListener paramRoomUpdateListener, Room paramRoom, int paramInt)
    {
      paramRoomUpdateListener.onRoomCreated(paramInt, paramRoom);
    }
  }
  
  private static final class SendFriendInviteFirstPartyBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Social.InviteUpdateResult> EW;
    
    SendFriendInviteFirstPartyBinderCallback(zzqo.zzb<Social.InviteUpdateResult> paramzzb)
    {
      this.EW = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzaz(DataHolder paramDataHolder)
    {
      this.EW.setResult(new GamesClientImpl.InviteUpdateResultImpl(paramDataHolder));
    }
  }
  
  private static final class SendRequestResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements Requests.SendRequestResult
  {
    private final GameRequest aac;
    
    /* Error */
    SendRequestResultImpl(DataHolder paramDataHolder)
    {
      // Byte code:
      //   0: aload_0
      //   1: aload_1
      //   2: invokespecial 15	com/google/android/gms/games/internal/GamesClientImpl$GamesDataHolderResult:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   5: new 17	com/google/android/gms/games/request/GameRequestBuffer
      //   8: dup
      //   9: aload_1
      //   10: invokespecial 18	com/google/android/gms/games/request/GameRequestBuffer:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   13: astore_1
      //   14: aload_1
      //   15: invokevirtual 22	com/google/android/gms/games/request/GameRequestBuffer:getCount	()I
      //   18: ifle +28 -> 46
      //   21: aload_0
      //   22: aload_1
      //   23: iconst_0
      //   24: invokevirtual 26	com/google/android/gms/games/request/GameRequestBuffer:get	(I)Ljava/lang/Object;
      //   27: checkcast 28	com/google/android/gms/games/request/GameRequest
      //   30: invokeinterface 32 1 0
      //   35: checkcast 28	com/google/android/gms/games/request/GameRequest
      //   38: putfield 34	com/google/android/gms/games/internal/GamesClientImpl$SendRequestResultImpl:aac	Lcom/google/android/gms/games/request/GameRequest;
      //   41: aload_1
      //   42: invokevirtual 38	com/google/android/gms/games/request/GameRequestBuffer:release	()V
      //   45: return
      //   46: aload_0
      //   47: aconst_null
      //   48: putfield 34	com/google/android/gms/games/internal/GamesClientImpl$SendRequestResultImpl:aac	Lcom/google/android/gms/games/request/GameRequest;
      //   51: goto -10 -> 41
      //   54: astore_2
      //   55: aload_1
      //   56: invokevirtual 38	com/google/android/gms/games/request/GameRequestBuffer:release	()V
      //   59: aload_2
      //   60: athrow
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	61	0	this	SendRequestResultImpl
      //   0	61	1	paramDataHolder	DataHolder
      //   54	6	2	localObject	Object
      // Exception table:
      //   from	to	target	type
      //   14	41	54	finally
      //   46	51	54	finally
    }
  }
  
  private static final class SetPlayerMutedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Players.LoadPlayersResult> EW;
    
    SetPlayerMutedBinderCallback(zzqo.zzb<Players.LoadPlayersResult> paramzzb)
    {
      this.EW = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzbg(DataHolder paramDataHolder)
    {
      this.EW.setResult(new GamesClientImpl.LoadPlayersResultImpl(paramDataHolder));
    }
  }
  
  private static final class SignOutCompleteBinderCallbacks
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Status> EW;
    
    public SignOutCompleteBinderCallbacks(zzqo.zzb<Status> paramzzb)
    {
      this.EW = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzbjk()
    {
      Status localStatus = GamesStatusCodes.zzqv(0);
      this.EW.setResult(localStatus);
    }
  }
  
  private static final class SnapshotCommittedBinderCallbacks
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Snapshots.CommitSnapshotResult> aak;
    
    public SnapshotCommittedBinderCallbacks(zzqo.zzb<Snapshots.CommitSnapshotResult> paramzzb)
    {
      this.aak = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzao(DataHolder paramDataHolder)
    {
      this.aak.setResult(new GamesClientImpl.CommitSnapshotResultImpl(paramDataHolder));
    }
  }
  
  static final class SnapshotDeletedBinderCallbacks
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Snapshots.DeleteSnapshotResult> EW;
    
    public SnapshotDeletedBinderCallbacks(zzqo.zzb<Snapshots.DeleteSnapshotResult> paramzzb)
    {
      this.EW = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzk(int paramInt, String paramString)
    {
      this.EW.setResult(new GamesClientImpl.DeleteSnapshotResultImpl(paramInt, paramString));
    }
  }
  
  private static final class SnapshotOpenedBinderCallbacks
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Snapshots.OpenSnapshotResult> aal;
    
    public SnapshotOpenedBinderCallbacks(zzqo.zzb<Snapshots.OpenSnapshotResult> paramzzb)
    {
      this.aal = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zza(DataHolder paramDataHolder, Contents paramContents)
    {
      this.aal.setResult(new GamesClientImpl.OpenSnapshotResultImpl(paramDataHolder, paramContents));
    }
    
    public void zza(DataHolder paramDataHolder, String paramString, Contents paramContents1, Contents paramContents2, Contents paramContents3)
    {
      this.aal.setResult(new GamesClientImpl.OpenSnapshotResultImpl(paramDataHolder, paramString, paramContents1, paramContents2, paramContents3));
    }
  }
  
  private static final class SnapshotsLoadedBinderCallbacks
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Snapshots.LoadSnapshotsResult> aam;
    
    public SnapshotsLoadedBinderCallbacks(zzqo.zzb<Snapshots.LoadSnapshotsResult> paramzzb)
    {
      this.aam = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzan(DataHolder paramDataHolder)
    {
      this.aam.setResult(new GamesClientImpl.LoadSnapshotsResultImpl(paramDataHolder));
    }
  }
  
  private static final class SocialInviteRemovedNotifier
    implements zzrr.zzc<OnSocialInviteUpdateReceivedListener>
  {
    private final SocialInvite aan;
    
    SocialInviteRemovedNotifier(SocialInvite paramSocialInvite)
    {
      this.aan = paramSocialInvite;
    }
    
    public void zza(OnSocialInviteUpdateReceivedListener paramOnSocialInviteUpdateReceivedListener)
    {
      paramOnSocialInviteUpdateReceivedListener.zzb(this.aan);
    }
    
    public void zzasm() {}
  }
  
  private static final class SocialInviteUpdateReceivedBinderCallback
    extends AbstractGamesCallbacks
  {
    public void zzbb(DataHolder paramDataHolder)
    {
      SocialInviteBuffer localSocialInviteBuffer = new SocialInviteBuffer(paramDataHolder);
      for (;;)
      {
        try
        {
          if (localSocialInviteBuffer.getCount() > 0)
          {
            paramDataHolder = (SocialInvite)((SocialInvite)localSocialInviteBuffer.get(0)).freeze();
            localSocialInviteBuffer.release();
            if (paramDataHolder != null)
            {
              new GamesClientImpl.SocialInviteUpdateReceivedNotifier(paramDataHolder);
              throw new NullPointerException();
            }
            return;
          }
        }
        finally
        {
          localSocialInviteBuffer.release();
        }
        paramDataHolder = null;
      }
    }
    
    public void zzbc(DataHolder paramDataHolder)
    {
      SocialInviteBuffer localSocialInviteBuffer = new SocialInviteBuffer(paramDataHolder);
      for (;;)
      {
        try
        {
          if (localSocialInviteBuffer.getCount() > 0)
          {
            paramDataHolder = (SocialInvite)((SocialInvite)localSocialInviteBuffer.get(0)).freeze();
            localSocialInviteBuffer.release();
            if (paramDataHolder != null)
            {
              new GamesClientImpl.SocialInviteRemovedNotifier(paramDataHolder);
              throw new NullPointerException();
            }
            return;
          }
        }
        finally
        {
          localSocialInviteBuffer.release();
        }
        paramDataHolder = null;
      }
    }
  }
  
  private static final class SocialInviteUpdateReceivedNotifier
    implements zzrr.zzc<OnSocialInviteUpdateReceivedListener>
  {
    private final SocialInvite aan;
    
    SocialInviteUpdateReceivedNotifier(SocialInvite paramSocialInvite)
    {
      this.aan = paramSocialInvite;
    }
    
    public void zza(OnSocialInviteUpdateReceivedListener paramOnSocialInviteUpdateReceivedListener)
    {
      paramOnSocialInviteUpdateReceivedListener.zza(this.aan);
    }
    
    public void zzasm() {}
  }
  
  private static final class StockProfileImagesLoadedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Players.LoadStockProfileImagesResult> EW;
    
    StockProfileImagesLoadedBinderCallback(zzqo.zzb<Players.LoadStockProfileImagesResult> paramzzb)
    {
      this.EW = paramzzb;
    }
    
    public void zzay(DataHolder paramDataHolder)
    {
      this.EW.setResult(new GamesClientImpl.LoadStockProfileImagesResultImpl(paramDataHolder));
    }
  }
  
  private static final class SubmitScoreBinderCallbacks
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Leaderboards.SubmitScoreResult> EW;
    
    public SubmitScoreBinderCallbacks(zzqo.zzb<Leaderboards.SubmitScoreResult> paramzzb)
    {
      this.EW = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzk(DataHolder paramDataHolder)
    {
      this.EW.setResult(new GamesClientImpl.SubmitScoreResultImpl(paramDataHolder));
    }
  }
  
  private static final class SubmitScoreResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements Leaderboards.SubmitScoreResult
  {
    private final ScoreSubmissionData aao;
    
    public SubmitScoreResultImpl(DataHolder paramDataHolder)
    {
      super();
      try
      {
        this.aao = new ScoreSubmissionData(paramDataHolder);
        return;
      }
      finally
      {
        paramDataHolder.close();
      }
    }
    
    public ScoreSubmissionData getScoreData()
    {
      return this.aao;
    }
  }
  
  private static final class TurnBasedMatchCanceledBinderCallbacks
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<TurnBasedMultiplayer.CancelMatchResult> aap;
    
    public TurnBasedMatchCanceledBinderCallbacks(zzqo.zzb<TurnBasedMultiplayer.CancelMatchResult> paramzzb)
    {
      this.aap = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzj(int paramInt, String paramString)
    {
      Status localStatus = GamesStatusCodes.zzqv(paramInt);
      this.aap.setResult(new GamesClientImpl.CancelMatchResultImpl(localStatus, paramString));
    }
  }
  
  private static final class TurnBasedMatchInitiatedBinderCallbacks
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<TurnBasedMultiplayer.InitiateMatchResult> aaq;
    
    public TurnBasedMatchInitiatedBinderCallbacks(zzqo.zzb<TurnBasedMultiplayer.InitiateMatchResult> paramzzb)
    {
      this.aaq = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzv(DataHolder paramDataHolder)
    {
      this.aaq.setResult(new GamesClientImpl.InitiateMatchResultImpl(paramDataHolder));
    }
  }
  
  private static final class TurnBasedMatchLeftBinderCallbacks
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<TurnBasedMultiplayer.LeaveMatchResult> aar;
    
    public TurnBasedMatchLeftBinderCallbacks(zzqo.zzb<TurnBasedMultiplayer.LeaveMatchResult> paramzzb)
    {
      this.aar = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzx(DataHolder paramDataHolder)
    {
      this.aar.setResult(new GamesClientImpl.LeaveMatchResultImpl(paramDataHolder));
    }
  }
  
  private static final class TurnBasedMatchLoadedBinderCallbacks
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<TurnBasedMultiplayer.LoadMatchResult> aas;
    
    public TurnBasedMatchLoadedBinderCallbacks(zzqo.zzb<TurnBasedMultiplayer.LoadMatchResult> paramzzb)
    {
      this.aas = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzu(DataHolder paramDataHolder)
    {
      this.aas.setResult(new GamesClientImpl.LoadMatchResultImpl(paramDataHolder));
    }
  }
  
  private static abstract class TurnBasedMatchResult
    extends GamesClientImpl.GamesDataHolderResult
  {
    final TurnBasedMatch ZM;
    
    /* Error */
    TurnBasedMatchResult(DataHolder paramDataHolder)
    {
      // Byte code:
      //   0: aload_0
      //   1: aload_1
      //   2: invokespecial 13	com/google/android/gms/games/internal/GamesClientImpl$GamesDataHolderResult:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   5: new 15	com/google/android/gms/games/multiplayer/turnbased/TurnBasedMatchBuffer
      //   8: dup
      //   9: aload_1
      //   10: invokespecial 16	com/google/android/gms/games/multiplayer/turnbased/TurnBasedMatchBuffer:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   13: astore_1
      //   14: aload_1
      //   15: invokevirtual 20	com/google/android/gms/games/multiplayer/turnbased/TurnBasedMatchBuffer:getCount	()I
      //   18: ifle +28 -> 46
      //   21: aload_0
      //   22: aload_1
      //   23: iconst_0
      //   24: invokevirtual 24	com/google/android/gms/games/multiplayer/turnbased/TurnBasedMatchBuffer:get	(I)Ljava/lang/Object;
      //   27: checkcast 26	com/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch
      //   30: invokeinterface 30 1 0
      //   35: checkcast 26	com/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch
      //   38: putfield 32	com/google/android/gms/games/internal/GamesClientImpl$TurnBasedMatchResult:ZM	Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;
      //   41: aload_1
      //   42: invokevirtual 36	com/google/android/gms/games/multiplayer/turnbased/TurnBasedMatchBuffer:release	()V
      //   45: return
      //   46: aload_0
      //   47: aconst_null
      //   48: putfield 32	com/google/android/gms/games/internal/GamesClientImpl$TurnBasedMatchResult:ZM	Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;
      //   51: goto -10 -> 41
      //   54: astore_2
      //   55: aload_1
      //   56: invokevirtual 36	com/google/android/gms/games/multiplayer/turnbased/TurnBasedMatchBuffer:release	()V
      //   59: aload_2
      //   60: athrow
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	61	0	this	TurnBasedMatchResult
      //   0	61	1	paramDataHolder	DataHolder
      //   54	6	2	localObject	Object
      // Exception table:
      //   from	to	target	type
      //   14	41	54	finally
      //   46	51	54	finally
    }
    
    public TurnBasedMatch getMatch()
    {
      return this.ZM;
    }
  }
  
  private static final class TurnBasedMatchUpdatedBinderCallbacks
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<TurnBasedMultiplayer.UpdateMatchResult> aat;
    
    public TurnBasedMatchUpdatedBinderCallbacks(zzqo.zzb<TurnBasedMultiplayer.UpdateMatchResult> paramzzb)
    {
      this.aat = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzw(DataHolder paramDataHolder)
    {
      this.aat.setResult(new GamesClientImpl.UpdateMatchResultImpl(paramDataHolder));
    }
  }
  
  private static final class TurnBasedMatchesLoadedBinderCallbacks
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<TurnBasedMultiplayer.LoadMatchesResult> aau;
    
    public TurnBasedMatchesLoadedBinderCallbacks(zzqo.zzb<TurnBasedMultiplayer.LoadMatchesResult> paramzzb)
    {
      this.aau = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzc(int paramInt, Bundle paramBundle)
    {
      paramBundle.setClassLoader(getClass().getClassLoader());
      Status localStatus = GamesStatusCodes.zzqv(paramInt);
      this.aau.setResult(new GamesClientImpl.LoadMatchesResultImpl(localStatus, paramBundle));
    }
  }
  
  private static final class UpdateAchievementResultImpl
    implements Achievements.UpdateAchievementResult
  {
    private final String XX;
    private final Status hv;
    
    UpdateAchievementResultImpl(int paramInt, String paramString)
    {
      this.hv = GamesStatusCodes.zzqv(paramInt);
      this.XX = paramString;
    }
    
    public String getAchievementId()
    {
      return this.XX;
    }
    
    public Status getStatus()
    {
      return this.hv;
    }
  }
  
  private static final class UpdateAutoSignInBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Status> EW;
    
    UpdateAutoSignInBinderCallback(zzqo.zzb<Status> paramzzb)
    {
      this.EW = paramzzb;
    }
    
    public void zzrn(int paramInt)
    {
      this.EW.setResult(new Status(paramInt));
    }
  }
  
  private static final class UpdateGamerProfileResultImpl
    implements Players.UpdateGamerProfileResult
  {
    private final Bundle aav;
    private final Status hv;
    
    UpdateGamerProfileResultImpl(int paramInt, Bundle paramBundle)
    {
      this.hv = new Status(paramInt);
      this.aav = paramBundle;
    }
    
    public Status getStatus()
    {
      return this.hv;
    }
  }
  
  private static final class UpdateHeadlessCapturePermissionBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Status> EW;
    
    UpdateHeadlessCapturePermissionBinderCallback(zzqo.zzb<Status> paramzzb)
    {
      this.EW = ((zzqo.zzb)zzaa.zzb(paramzzb, "Holder must not be null"));
    }
    
    public void zzbo(Status paramStatus)
    {
      this.EW.setResult(paramStatus);
    }
  }
  
  private static final class UpdateMatchResultImpl
    extends GamesClientImpl.TurnBasedMatchResult
    implements TurnBasedMultiplayer.UpdateMatchResult
  {
    UpdateMatchResultImpl(DataHolder paramDataHolder)
    {
      super();
    }
  }
  
  private static final class UpdateProfileDiscoverabilityBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zzqo.zzb<Status> EW;
    
    UpdateProfileDiscoverabilityBinderCallback(zzqo.zzb<Status> paramzzb)
    {
      this.EW = paramzzb;
    }
    
    public void zzro(int paramInt)
    {
      this.EW.setResult(new Status(paramInt));
    }
  }
  
  private static final class UpdateRequestsResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements Requests.UpdateRequestsResult
  {
    private final RequestUpdateOutcomes aaw;
    
    UpdateRequestsResultImpl(DataHolder paramDataHolder)
    {
      super();
      this.aaw = RequestUpdateOutcomes.zzbl(paramDataHolder);
    }
    
    public Set<String> getRequestIds()
    {
      return this.aaw.getRequestIds();
    }
    
    public int getRequestOutcome(String paramString)
    {
      return this.aaw.getRequestOutcome(paramString);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/internal/GamesClientImpl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */