package com.google.android.gms.games.internal;

import com.google.android.gms.common.internal.zzo;
import com.google.android.gms.internal.zzsi;

public final class GamesLog
{
  private static final zzsi<Boolean> aaA = zzsi.zzk("games.play_games_dogfood", false);
  private static final zzo aaz = new zzo("Games");
  
  public static void zzae(String paramString1, String paramString2)
  {
    aaz.zzae(paramString1, paramString2);
  }
  
  public static void zzaf(String paramString1, String paramString2)
  {
    aaz.zzaf(paramString1, paramString2);
  }
  
  public static void zzb(String paramString1, String paramString2, Throwable paramThrowable)
  {
    aaz.zzb(paramString1, paramString2, paramThrowable);
  }
  
  public static void zzc(String paramString1, String paramString2, Throwable paramThrowable)
  {
    aaz.zzc(paramString1, paramString2, paramThrowable);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/internal/GamesLog.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */