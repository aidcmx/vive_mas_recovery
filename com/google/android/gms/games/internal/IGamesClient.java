package com.google.android.gms.games.internal;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract interface IGamesClient
  extends IInterface
{
  public abstract PopupLocationInfoParcelable zzbjl()
    throws RemoteException;
  
  public static abstract class Stub
    extends Binder
    implements IGamesClient
  {
    public Stub()
    {
      attachInterface(this, "com.google.android.gms.games.internal.IGamesClient");
    }
    
    public static IGamesClient zzgp(IBinder paramIBinder)
    {
      if (paramIBinder == null) {
        return null;
      }
      IInterface localIInterface = paramIBinder.queryLocalInterface("com.google.android.gms.games.internal.IGamesClient");
      if ((localIInterface != null) && ((localIInterface instanceof IGamesClient))) {
        return (IGamesClient)localIInterface;
      }
      return new Proxy(paramIBinder);
    }
    
    public IBinder asBinder()
    {
      return this;
    }
    
    public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
      throws RemoteException
    {
      switch (paramInt1)
      {
      default: 
        return super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
      case 1598968902: 
        paramParcel2.writeString("com.google.android.gms.games.internal.IGamesClient");
        return true;
      }
      paramParcel1.enforceInterface("com.google.android.gms.games.internal.IGamesClient");
      paramParcel1 = zzbjl();
      paramParcel2.writeNoException();
      if (paramParcel1 != null)
      {
        paramParcel2.writeInt(1);
        paramParcel1.writeToParcel(paramParcel2, 1);
        return true;
      }
      paramParcel2.writeInt(0);
      return true;
    }
    
    private static class Proxy
      implements IGamesClient
    {
      private IBinder zzajq;
      
      Proxy(IBinder paramIBinder)
      {
        this.zzajq = paramIBinder;
      }
      
      public IBinder asBinder()
      {
        return this.zzajq;
      }
      
      /* Error */
      public PopupLocationInfoParcelable zzbjl()
        throws RemoteException
      {
        // Byte code:
        //   0: invokestatic 32	android/os/Parcel:obtain	()Landroid/os/Parcel;
        //   3: astore_2
        //   4: invokestatic 32	android/os/Parcel:obtain	()Landroid/os/Parcel;
        //   7: astore_3
        //   8: aload_2
        //   9: ldc 34
        //   11: invokevirtual 38	android/os/Parcel:writeInterfaceToken	(Ljava/lang/String;)V
        //   14: aload_0
        //   15: getfield 19	com/google/android/gms/games/internal/IGamesClient$Stub$Proxy:zzajq	Landroid/os/IBinder;
        //   18: sipush 1001
        //   21: aload_2
        //   22: aload_3
        //   23: iconst_0
        //   24: invokeinterface 44 5 0
        //   29: pop
        //   30: aload_3
        //   31: invokevirtual 47	android/os/Parcel:readException	()V
        //   34: aload_3
        //   35: invokevirtual 51	android/os/Parcel:readInt	()I
        //   38: ifeq +26 -> 64
        //   41: getstatic 57	com/google/android/gms/games/internal/PopupLocationInfoParcelable:CREATOR	Landroid/os/Parcelable$Creator;
        //   44: aload_3
        //   45: invokeinterface 63 2 0
        //   50: checkcast 53	com/google/android/gms/games/internal/PopupLocationInfoParcelable
        //   53: astore_1
        //   54: aload_3
        //   55: invokevirtual 66	android/os/Parcel:recycle	()V
        //   58: aload_2
        //   59: invokevirtual 66	android/os/Parcel:recycle	()V
        //   62: aload_1
        //   63: areturn
        //   64: aconst_null
        //   65: astore_1
        //   66: goto -12 -> 54
        //   69: astore_1
        //   70: aload_3
        //   71: invokevirtual 66	android/os/Parcel:recycle	()V
        //   74: aload_2
        //   75: invokevirtual 66	android/os/Parcel:recycle	()V
        //   78: aload_1
        //   79: athrow
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	80	0	this	Proxy
        //   53	13	1	localPopupLocationInfoParcelable	PopupLocationInfoParcelable
        //   69	10	1	localObject	Object
        //   3	72	2	localParcel1	Parcel
        //   7	64	3	localParcel2	Parcel
        // Exception table:
        //   from	to	target	type
        //   8	54	69	finally
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/internal/IGamesClient.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */