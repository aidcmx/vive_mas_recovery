package com.google.android.gms.games.internal;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public final class PopupLocationInfoParcelable
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<PopupLocationInfoParcelable> CREATOR = new PopupLocationInfoParcelableCreator();
  private final Bundle aaB;
  private final IBinder aaC;
  private final int mVersionCode;
  
  PopupLocationInfoParcelable(int paramInt, Bundle paramBundle, IBinder paramIBinder)
  {
    this.mVersionCode = paramInt;
    this.aaB = paramBundle;
    this.aaC = paramIBinder;
  }
  
  public PopupLocationInfoParcelable(PopupManager.PopupLocationInfo paramPopupLocationInfo)
  {
    this.mVersionCode = 1;
    this.aaB = paramPopupLocationInfo.zzbla();
    this.aaC = paramPopupLocationInfo.aaF;
  }
  
  public int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public IBinder getWindowToken()
  {
    return this.aaC;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    PopupLocationInfoParcelableCreator.zza(this, paramParcel, paramInt);
  }
  
  public Bundle zzbla()
  {
    return this.aaB;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/internal/PopupLocationInfoParcelable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */