package com.google.android.gms.games.internal;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.view.Display;
import android.view.View;
import android.view.View.OnAttachStateChangeListener;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.Window;
import com.google.android.gms.common.util.zzs;
import java.lang.ref.WeakReference;

public class PopupManager
{
  protected GamesClientImpl aaD;
  protected PopupLocationInfo aaE;
  
  private PopupManager(GamesClientImpl paramGamesClientImpl, int paramInt)
  {
    this.aaD = paramGamesClientImpl;
    zzrv(paramInt);
  }
  
  public static PopupManager zza(GamesClientImpl paramGamesClientImpl, int paramInt)
  {
    if (zzs.zzayo()) {
      return new PopupManagerHCMR1(paramGamesClientImpl, paramInt);
    }
    return new PopupManager(paramGamesClientImpl, paramInt);
  }
  
  public void setGravity(int paramInt)
  {
    this.aaE.gravity = paramInt;
  }
  
  public void zzblb()
  {
    this.aaD.zza(this.aaE.aaF, this.aaE.zzbla());
  }
  
  public Bundle zzblc()
  {
    return this.aaE.zzbla();
  }
  
  public IBinder zzbld()
  {
    return this.aaE.aaF;
  }
  
  public PopupLocationInfo zzble()
  {
    return this.aaE;
  }
  
  protected void zzrv(int paramInt)
  {
    this.aaE = new PopupLocationInfo(paramInt, new Binder(), null);
  }
  
  public void zzx(View paramView) {}
  
  public static final class PopupLocationInfo
  {
    public IBinder aaF;
    public int aaG = -1;
    public int bottom = 0;
    public int gravity;
    public int left = 0;
    public int right = 0;
    public int top = 0;
    
    private PopupLocationInfo(int paramInt, IBinder paramIBinder)
    {
      this.gravity = paramInt;
      this.aaF = paramIBinder;
    }
    
    public Bundle zzbla()
    {
      Bundle localBundle = new Bundle();
      localBundle.putInt("popupLocationInfo.gravity", this.gravity);
      localBundle.putInt("popupLocationInfo.displayId", this.aaG);
      localBundle.putInt("popupLocationInfo.left", this.left);
      localBundle.putInt("popupLocationInfo.top", this.top);
      localBundle.putInt("popupLocationInfo.right", this.right);
      localBundle.putInt("popupLocationInfo.bottom", this.bottom);
      return localBundle;
    }
  }
  
  @TargetApi(12)
  private static final class PopupManagerHCMR1
    extends PopupManager
    implements View.OnAttachStateChangeListener, ViewTreeObserver.OnGlobalLayoutListener
  {
    private boolean YQ = false;
    private WeakReference<View> aaH;
    
    protected PopupManagerHCMR1(GamesClientImpl paramGamesClientImpl, int paramInt)
    {
      super(paramInt, null);
    }
    
    @TargetApi(17)
    private void zzy(View paramView)
    {
      int j = -1;
      int i = j;
      if (zzs.zzays())
      {
        localObject = paramView.getDisplay();
        i = j;
        if (localObject != null) {
          i = ((Display)localObject).getDisplayId();
        }
      }
      Object localObject = paramView.getWindowToken();
      int[] arrayOfInt = new int[2];
      paramView.getLocationInWindow(arrayOfInt);
      j = paramView.getWidth();
      int k = paramView.getHeight();
      this.aaE.aaG = i;
      this.aaE.aaF = ((IBinder)localObject);
      this.aaE.left = arrayOfInt[0];
      this.aaE.top = arrayOfInt[1];
      this.aaE.right = (arrayOfInt[0] + j);
      this.aaE.bottom = (arrayOfInt[1] + k);
      if (this.YQ)
      {
        zzblb();
        this.YQ = false;
      }
    }
    
    public void onGlobalLayout()
    {
      if (this.aaH == null) {}
      View localView;
      do
      {
        return;
        localView = (View)this.aaH.get();
      } while (localView == null);
      zzy(localView);
    }
    
    public void onViewAttachedToWindow(View paramView)
    {
      zzy(paramView);
    }
    
    public void onViewDetachedFromWindow(View paramView)
    {
      this.aaD.zzbkm();
      paramView.removeOnAttachStateChangeListener(this);
    }
    
    public void zzblb()
    {
      if (this.aaE.aaF != null)
      {
        super.zzblb();
        return;
      }
      if (this.aaH != null) {}
      for (boolean bool = true;; bool = false)
      {
        this.YQ = bool;
        return;
      }
    }
    
    protected void zzrv(int paramInt)
    {
      this.aaE = new PopupManager.PopupLocationInfo(paramInt, null, null);
    }
    
    @TargetApi(16)
    public void zzx(View paramView)
    {
      this.aaD.zzbkm();
      Object localObject2;
      Object localObject1;
      if (this.aaH != null)
      {
        localObject2 = (View)this.aaH.get();
        Context localContext = this.aaD.getContext();
        localObject1 = localObject2;
        if (localObject2 == null)
        {
          localObject1 = localObject2;
          if ((localContext instanceof Activity)) {
            localObject1 = ((Activity)localContext).getWindow().getDecorView();
          }
        }
        if (localObject1 != null)
        {
          ((View)localObject1).removeOnAttachStateChangeListener(this);
          localObject1 = ((View)localObject1).getViewTreeObserver();
          if (!zzs.zzayr()) {
            break label186;
          }
          ((ViewTreeObserver)localObject1).removeOnGlobalLayoutListener(this);
        }
      }
      for (;;)
      {
        this.aaH = null;
        localObject2 = this.aaD.getContext();
        localObject1 = paramView;
        if (paramView == null)
        {
          localObject1 = paramView;
          if ((localObject2 instanceof Activity))
          {
            localObject1 = ((Activity)localObject2).findViewById(16908290);
            paramView = (View)localObject1;
            if (localObject1 == null) {
              paramView = ((Activity)localObject2).getWindow().getDecorView();
            }
            GamesLog.zzae("PopupManager", "You have not specified a View to use as content view for popups. Falling back to the Activity content view. Note that this may not work as expected in multi-screen environments");
            localObject1 = paramView;
          }
        }
        if (localObject1 == null) {
          break;
        }
        zzy((View)localObject1);
        this.aaH = new WeakReference(localObject1);
        ((View)localObject1).addOnAttachStateChangeListener(this);
        ((View)localObject1).getViewTreeObserver().addOnGlobalLayoutListener(this);
        return;
        label186:
        ((ViewTreeObserver)localObject1).removeGlobalOnLayoutListener(this);
      }
      GamesLog.zzaf("PopupManager", "No content view usable to display popups. Popups will not be displayed in response to this client's calls. Use setViewForPopups() to set your content view.");
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/internal/PopupManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */