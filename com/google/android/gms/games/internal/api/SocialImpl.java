package com.google.android.gms.games.internal.api;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.Games.BaseGamesApiMethodImpl;
import com.google.android.gms.games.internal.GamesClientImpl;
import com.google.android.gms.games.social.Social;
import com.google.android.gms.games.social.Social.InviteUpdateResult;
import com.google.android.gms.games.social.Social.LoadInvitesResult;

public class SocialImpl
  implements Social
{
  private static abstract class LoadSocialInvitesImpl
    extends Games.BaseGamesApiMethodImpl<Social.LoadInvitesResult>
  {
    public Social.LoadInvitesResult zzcx(final Status paramStatus)
    {
      new Social.LoadInvitesResult()
      {
        public Status getStatus()
        {
          return paramStatus;
        }
        
        public void release() {}
      };
    }
  }
  
  private static abstract class SocialInviteUpdateImpl
    extends Games.BaseGamesApiMethodImpl<Social.InviteUpdateResult>
  {
    public Social.InviteUpdateResult zzcy(final Status paramStatus)
    {
      new Social.InviteUpdateResult()
      {
        public Status getStatus()
        {
          return paramStatus;
        }
      };
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/internal/api/SocialImpl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */