package com.google.android.gms.games.internal.constants;

public final class LeaderboardCollection
{
  public static String zzrw(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      throw new IllegalArgumentException(43 + "Unknown leaderboard collection: " + paramInt);
    case 0: 
      return "PUBLIC";
    case 1: 
      return "SOCIAL";
    }
    return "SOCIAL_1P";
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/internal/constants/LeaderboardCollection.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */