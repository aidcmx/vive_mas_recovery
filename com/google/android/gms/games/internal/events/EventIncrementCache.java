package com.google.android.gms.games.internal.events;

import android.os.Handler;
import android.os.Looper;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class EventIncrementCache
{
  final Object acL = new Object();
  private Handler acM;
  private boolean acN;
  private HashMap<String, AtomicInteger> acO;
  private int acP;
  
  public EventIncrementCache(Looper paramLooper, int paramInt)
  {
    this.acM = new Handler(paramLooper);
    this.acO = new HashMap();
    this.acP = paramInt;
  }
  
  private void zzblf()
  {
    synchronized (this.acL)
    {
      this.acN = false;
      flush();
      return;
    }
  }
  
  public void flush()
  {
    synchronized (this.acL)
    {
      Iterator localIterator = this.acO.entrySet().iterator();
      if (localIterator.hasNext())
      {
        Map.Entry localEntry = (Map.Entry)localIterator.next();
        zzr((String)localEntry.getKey(), ((AtomicInteger)localEntry.getValue()).get());
      }
    }
    this.acO.clear();
  }
  
  protected abstract void zzr(String paramString, int paramInt);
  
  public void zzv(String paramString, int paramInt)
  {
    synchronized (this.acL)
    {
      if (!this.acN)
      {
        this.acN = true;
        this.acM.postDelayed(new Runnable()
        {
          public void run()
          {
            EventIncrementCache.zza(EventIncrementCache.this);
          }
        }, this.acP);
      }
      AtomicInteger localAtomicInteger2 = (AtomicInteger)this.acO.get(paramString);
      AtomicInteger localAtomicInteger1 = localAtomicInteger2;
      if (localAtomicInteger2 == null)
      {
        localAtomicInteger1 = new AtomicInteger();
        this.acO.put(paramString, localAtomicInteger1);
      }
      localAtomicInteger1.addAndGet(paramInt);
      return;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/internal/events/EventIncrementCache.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */