package com.google.android.gms.games.internal.experience;

import android.net.Uri;
import android.os.Parcelable;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.data.Freezable;
import com.google.android.gms.games.Game;

public abstract interface ExperienceEvent
  extends Parcelable, Freezable<ExperienceEvent>
{
  public abstract Game getGame();
  
  public abstract Uri getIconImageUri();
  
  @Deprecated
  @KeepName
  public abstract String getIconImageUrl();
  
  public abstract int getType();
  
  public abstract String zzblg();
  
  public abstract String zzblh();
  
  public abstract String zzbli();
  
  public abstract long zzblj();
  
  public abstract long zzblk();
  
  public abstract long zzbll();
  
  public abstract int zzblm();
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/internal/experience/ExperienceEvent.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */