package com.google.android.gms.games.internal.experience;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.GameEntity;

public final class ExperienceEventEntity
  extends AbstractSafeParcelable
  implements ExperienceEvent
{
  public static final Parcelable.Creator<ExperienceEventEntity> CREATOR = new ExperienceEventEntityCreator();
  private final Uri WQ;
  private final String Xb;
  private final String acS;
  private final GameEntity acT;
  private final String acU;
  private final String acV;
  private final long acW;
  private final long acX;
  private final long acY;
  private final int acZ;
  private final int mVersionCode;
  private final int nV;
  
  ExperienceEventEntity(int paramInt1, String paramString1, GameEntity paramGameEntity, String paramString2, String paramString3, String paramString4, Uri paramUri, long paramLong1, long paramLong2, long paramLong3, int paramInt2, int paramInt3)
  {
    this.mVersionCode = paramInt1;
    this.acS = paramString1;
    this.acT = paramGameEntity;
    this.acU = paramString2;
    this.acV = paramString3;
    this.Xb = paramString4;
    this.WQ = paramUri;
    this.acW = paramLong1;
    this.acX = paramLong2;
    this.acY = paramLong3;
    this.nV = paramInt2;
    this.acZ = paramInt3;
  }
  
  public ExperienceEventEntity(ExperienceEvent paramExperienceEvent)
  {
    this.mVersionCode = 1;
    this.acS = paramExperienceEvent.zzblg();
    this.acT = new GameEntity(paramExperienceEvent.getGame());
    this.acU = paramExperienceEvent.zzblh();
    this.acV = paramExperienceEvent.zzbli();
    this.Xb = paramExperienceEvent.getIconImageUrl();
    this.WQ = paramExperienceEvent.getIconImageUri();
    this.acW = paramExperienceEvent.zzblj();
    this.acX = paramExperienceEvent.zzblk();
    this.acY = paramExperienceEvent.zzbll();
    this.nV = paramExperienceEvent.getType();
    this.acZ = paramExperienceEvent.zzblm();
  }
  
  static int zza(ExperienceEvent paramExperienceEvent)
  {
    return zzz.hashCode(new Object[] { paramExperienceEvent.zzblg(), paramExperienceEvent.getGame(), paramExperienceEvent.zzblh(), paramExperienceEvent.zzbli(), paramExperienceEvent.getIconImageUrl(), paramExperienceEvent.getIconImageUri(), Long.valueOf(paramExperienceEvent.zzblj()), Long.valueOf(paramExperienceEvent.zzblk()), Long.valueOf(paramExperienceEvent.zzbll()), Integer.valueOf(paramExperienceEvent.getType()), Integer.valueOf(paramExperienceEvent.zzblm()) });
  }
  
  static boolean zza(ExperienceEvent paramExperienceEvent, Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if (!(paramObject instanceof ExperienceEvent)) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramExperienceEvent == paramObject);
      paramObject = (ExperienceEvent)paramObject;
      if ((!zzz.equal(((ExperienceEvent)paramObject).zzblg(), paramExperienceEvent.zzblg())) || (!zzz.equal(((ExperienceEvent)paramObject).getGame(), paramExperienceEvent.getGame())) || (!zzz.equal(((ExperienceEvent)paramObject).zzblh(), paramExperienceEvent.zzblh())) || (!zzz.equal(((ExperienceEvent)paramObject).zzbli(), paramExperienceEvent.zzbli())) || (!zzz.equal(((ExperienceEvent)paramObject).getIconImageUrl(), paramExperienceEvent.getIconImageUrl())) || (!zzz.equal(((ExperienceEvent)paramObject).getIconImageUri(), paramExperienceEvent.getIconImageUri())) || (!zzz.equal(Long.valueOf(((ExperienceEvent)paramObject).zzblj()), Long.valueOf(paramExperienceEvent.zzblj()))) || (!zzz.equal(Long.valueOf(((ExperienceEvent)paramObject).zzblk()), Long.valueOf(paramExperienceEvent.zzblk()))) || (!zzz.equal(Long.valueOf(((ExperienceEvent)paramObject).zzbll()), Long.valueOf(paramExperienceEvent.zzbll()))) || (!zzz.equal(Integer.valueOf(((ExperienceEvent)paramObject).getType()), Integer.valueOf(paramExperienceEvent.getType())))) {
        break;
      }
      bool1 = bool2;
    } while (zzz.equal(Integer.valueOf(((ExperienceEvent)paramObject).zzblm()), Integer.valueOf(paramExperienceEvent.zzblm())));
    return false;
  }
  
  static String zzb(ExperienceEvent paramExperienceEvent)
  {
    return zzz.zzx(paramExperienceEvent).zzg("ExperienceId", paramExperienceEvent.zzblg()).zzg("Game", paramExperienceEvent.getGame()).zzg("DisplayTitle", paramExperienceEvent.zzblh()).zzg("DisplayDescription", paramExperienceEvent.zzbli()).zzg("IconImageUrl", paramExperienceEvent.getIconImageUrl()).zzg("IconImageUri", paramExperienceEvent.getIconImageUri()).zzg("CreatedTimestamp", Long.valueOf(paramExperienceEvent.zzblj())).zzg("XpEarned", Long.valueOf(paramExperienceEvent.zzblk())).zzg("CurrentXp", Long.valueOf(paramExperienceEvent.zzbll())).zzg("Type", Integer.valueOf(paramExperienceEvent.getType())).zzg("NewLevel", Integer.valueOf(paramExperienceEvent.zzblm())).toString();
  }
  
  public boolean equals(Object paramObject)
  {
    return zza(this, paramObject);
  }
  
  public Game getGame()
  {
    return this.acT;
  }
  
  public Uri getIconImageUri()
  {
    return this.WQ;
  }
  
  public String getIconImageUrl()
  {
    return this.Xb;
  }
  
  public int getType()
  {
    return this.nV;
  }
  
  public int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    return zza(this);
  }
  
  public boolean isDataValid()
  {
    return true;
  }
  
  public String toString()
  {
    return zzb(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    ExperienceEventEntityCreator.zza(this, paramParcel, paramInt);
  }
  
  public String zzblg()
  {
    return this.acS;
  }
  
  public String zzblh()
  {
    return this.acU;
  }
  
  public String zzbli()
  {
    return this.acV;
  }
  
  public long zzblj()
  {
    return this.acW;
  }
  
  public long zzblk()
  {
    return this.acX;
  }
  
  public long zzbll()
  {
    return this.acY;
  }
  
  public int zzblm()
  {
    return this.acZ;
  }
  
  public ExperienceEvent zzbln()
  {
    return this;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/internal/experience/ExperienceEventEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */