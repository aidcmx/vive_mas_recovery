package com.google.android.gms.games.internal.experience;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.games.GameEntity;

public class ExperienceEventEntityCreator
  implements Parcelable.Creator<ExperienceEventEntity>
{
  static void zza(ExperienceEventEntity paramExperienceEventEntity, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zza(paramParcel, 1, paramExperienceEventEntity.zzblg(), false);
    zzb.zza(paramParcel, 2, paramExperienceEventEntity.getGame(), paramInt, false);
    zzb.zza(paramParcel, 3, paramExperienceEventEntity.zzblh(), false);
    zzb.zza(paramParcel, 4, paramExperienceEventEntity.zzbli(), false);
    zzb.zza(paramParcel, 5, paramExperienceEventEntity.getIconImageUrl(), false);
    zzb.zza(paramParcel, 6, paramExperienceEventEntity.getIconImageUri(), paramInt, false);
    zzb.zza(paramParcel, 7, paramExperienceEventEntity.zzblj());
    zzb.zzc(paramParcel, 1000, paramExperienceEventEntity.getVersionCode());
    zzb.zza(paramParcel, 8, paramExperienceEventEntity.zzblk());
    zzb.zza(paramParcel, 9, paramExperienceEventEntity.zzbll());
    zzb.zzc(paramParcel, 10, paramExperienceEventEntity.getType());
    zzb.zzc(paramParcel, 11, paramExperienceEventEntity.zzblm());
    zzb.zzaj(paramParcel, i);
  }
  
  public ExperienceEventEntity zzly(Parcel paramParcel)
  {
    int m = zza.zzcr(paramParcel);
    int k = 0;
    String str4 = null;
    GameEntity localGameEntity = null;
    String str3 = null;
    String str2 = null;
    String str1 = null;
    Uri localUri = null;
    long l3 = 0L;
    long l2 = 0L;
    long l1 = 0L;
    int j = 0;
    int i = 0;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.zzcq(paramParcel);
      switch (zza.zzgu(n))
      {
      default: 
        zza.zzb(paramParcel, n);
        break;
      case 1: 
        str4 = zza.zzq(paramParcel, n);
        break;
      case 2: 
        localGameEntity = (GameEntity)zza.zza(paramParcel, n, GameEntity.CREATOR);
        break;
      case 3: 
        str3 = zza.zzq(paramParcel, n);
        break;
      case 4: 
        str2 = zza.zzq(paramParcel, n);
        break;
      case 5: 
        str1 = zza.zzq(paramParcel, n);
        break;
      case 6: 
        localUri = (Uri)zza.zza(paramParcel, n, Uri.CREATOR);
        break;
      case 7: 
        l3 = zza.zzi(paramParcel, n);
        break;
      case 1000: 
        k = zza.zzg(paramParcel, n);
        break;
      case 8: 
        l2 = zza.zzi(paramParcel, n);
        break;
      case 9: 
        l1 = zza.zzi(paramParcel, n);
        break;
      case 10: 
        j = zza.zzg(paramParcel, n);
        break;
      case 11: 
        i = zza.zzg(paramParcel, n);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza(37 + "Overread allowed size end=" + m, paramParcel);
    }
    return new ExperienceEventEntity(k, str4, localGameEntity, str3, str2, str1, localUri, l3, l2, l1, j, i);
  }
  
  public ExperienceEventEntity[] zzry(int paramInt)
  {
    return new ExperienceEventEntity[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/internal/experience/ExperienceEventEntityCreator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */