package com.google.android.gms.games.internal.experience;

import android.net.Uri;
import android.os.Parcel;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.zzc;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.GameRef;

public final class ExperienceEventRef
  extends zzc
  implements ExperienceEvent
{
  private final GameRef ada;
  
  public ExperienceEventRef(DataHolder paramDataHolder, int paramInt)
  {
    super(paramDataHolder, paramInt);
    if (zzhq("external_game_id"))
    {
      this.ada = null;
      return;
    }
    this.ada = new GameRef(this.zy, this.BU);
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    return ExperienceEventEntity.zza(this, paramObject);
  }
  
  public Game getGame()
  {
    return this.ada;
  }
  
  public Uri getIconImageUri()
  {
    return zzhp("icon_uri");
  }
  
  public String getIconImageUrl()
  {
    return getString("icon_url");
  }
  
  public int getType()
  {
    return getInteger("type");
  }
  
  public int hashCode()
  {
    return ExperienceEventEntity.zza(this);
  }
  
  public String toString()
  {
    return ExperienceEventEntity.zzb(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    ((ExperienceEventEntity)freeze()).writeToParcel(paramParcel, paramInt);
  }
  
  public String zzblg()
  {
    return getString("external_experience_id");
  }
  
  public String zzblh()
  {
    return getString("display_title");
  }
  
  public String zzbli()
  {
    return getString("display_description");
  }
  
  public long zzblj()
  {
    return getLong("created_timestamp");
  }
  
  public long zzblk()
  {
    return getLong("xp_earned");
  }
  
  public long zzbll()
  {
    return getLong("current_xp");
  }
  
  public int zzblm()
  {
    return getInteger("newLevel");
  }
  
  public ExperienceEvent zzbln()
  {
    return new ExperienceEventEntity(this);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/internal/experience/ExperienceEventRef.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */