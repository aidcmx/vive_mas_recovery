package com.google.android.gms.games.internal.game;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;
import com.google.android.gms.games.internal.GamesDowngradeableSafeParcel;

public final class GameBadgeEntity
  extends GamesDowngradeableSafeParcel
  implements GameBadge
{
  public static final Parcelable.Creator<GameBadgeEntity> CREATOR = new GameBadgeEntityCreatorCompat();
  private String JB;
  private Uri WQ;
  private String cg;
  private final int mVersionCode;
  private int nV;
  
  GameBadgeEntity(int paramInt1, int paramInt2, String paramString1, String paramString2, Uri paramUri)
  {
    this.mVersionCode = paramInt1;
    this.nV = paramInt2;
    this.JB = paramString1;
    this.cg = paramString2;
    this.WQ = paramUri;
  }
  
  public GameBadgeEntity(GameBadge paramGameBadge)
  {
    this.mVersionCode = 1;
    this.nV = paramGameBadge.getType();
    this.JB = paramGameBadge.getTitle();
    this.cg = paramGameBadge.getDescription();
    this.WQ = paramGameBadge.getIconImageUri();
  }
  
  static int zza(GameBadge paramGameBadge)
  {
    return zzz.hashCode(new Object[] { Integer.valueOf(paramGameBadge.getType()), paramGameBadge.getTitle(), paramGameBadge.getDescription(), paramGameBadge.getIconImageUri() });
  }
  
  static boolean zza(GameBadge paramGameBadge, Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if (!(paramObject instanceof GameBadge)) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramGameBadge == paramObject);
      paramObject = (GameBadge)paramObject;
      if (!zzz.equal(Integer.valueOf(((GameBadge)paramObject).getType()), paramGameBadge.getTitle())) {
        break;
      }
      bool1 = bool2;
    } while (zzz.equal(((GameBadge)paramObject).getDescription(), paramGameBadge.getIconImageUri()));
    return false;
  }
  
  static String zzb(GameBadge paramGameBadge)
  {
    return zzz.zzx(paramGameBadge).zzg("Type", Integer.valueOf(paramGameBadge.getType())).zzg("Title", paramGameBadge.getTitle()).zzg("Description", paramGameBadge.getDescription()).zzg("IconImageUri", paramGameBadge.getIconImageUri()).toString();
  }
  
  public boolean equals(Object paramObject)
  {
    return zza(this, paramObject);
  }
  
  public String getDescription()
  {
    return this.cg;
  }
  
  public Uri getIconImageUri()
  {
    return this.WQ;
  }
  
  public String getTitle()
  {
    return this.JB;
  }
  
  public int getType()
  {
    return this.nV;
  }
  
  public int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    return zza(this);
  }
  
  public boolean isDataValid()
  {
    return true;
  }
  
  public String toString()
  {
    return zzb(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    if (!zzawa())
    {
      GameBadgeEntityCreator.zza(this, paramParcel, paramInt);
      return;
    }
    paramParcel.writeInt(this.nV);
    paramParcel.writeString(this.JB);
    paramParcel.writeString(this.cg);
    if (this.WQ == null) {}
    for (String str = null;; str = this.WQ.toString())
    {
      paramParcel.writeString(str);
      return;
    }
  }
  
  public GameBadge zzblo()
  {
    return this;
  }
  
  static final class GameBadgeEntityCreatorCompat
    extends GameBadgeEntityCreator
  {
    public GameBadgeEntity zzlz(Parcel paramParcel)
    {
      if ((GameBadgeEntity.zze(GameBadgeEntity.zzbho())) || (GameBadgeEntity.zzjv(GameBadgeEntity.class.getCanonicalName()))) {
        return super.zzlz(paramParcel);
      }
      int i = paramParcel.readInt();
      String str1 = paramParcel.readString();
      String str2 = paramParcel.readString();
      paramParcel = paramParcel.readString();
      if (paramParcel == null) {}
      for (paramParcel = null;; paramParcel = Uri.parse(paramParcel)) {
        return new GameBadgeEntity(1, i, str1, str2, paramParcel);
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/internal/game/GameBadgeEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */