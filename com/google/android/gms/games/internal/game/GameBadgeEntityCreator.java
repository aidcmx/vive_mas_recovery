package com.google.android.gms.games.internal.game;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class GameBadgeEntityCreator
  implements Parcelable.Creator<GameBadgeEntity>
{
  static void zza(GameBadgeEntity paramGameBadgeEntity, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramGameBadgeEntity.getType());
    zzb.zza(paramParcel, 2, paramGameBadgeEntity.getTitle(), false);
    zzb.zza(paramParcel, 3, paramGameBadgeEntity.getDescription(), false);
    zzb.zza(paramParcel, 4, paramGameBadgeEntity.getIconImageUri(), paramInt, false);
    zzb.zzc(paramParcel, 1000, paramGameBadgeEntity.getVersionCode());
    zzb.zzaj(paramParcel, i);
  }
  
  public GameBadgeEntity zzlz(Parcel paramParcel)
  {
    int i = 0;
    Uri localUri = null;
    int k = zza.zzcr(paramParcel);
    String str1 = null;
    String str2 = null;
    int j = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.zzcq(paramParcel);
      switch (zza.zzgu(m))
      {
      default: 
        zza.zzb(paramParcel, m);
        break;
      case 1: 
        i = zza.zzg(paramParcel, m);
        break;
      case 2: 
        str2 = zza.zzq(paramParcel, m);
        break;
      case 3: 
        str1 = zza.zzq(paramParcel, m);
        break;
      case 4: 
        localUri = (Uri)zza.zza(paramParcel, m, Uri.CREATOR);
        break;
      case 1000: 
        j = zza.zzg(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza(37 + "Overread allowed size end=" + k, paramParcel);
    }
    return new GameBadgeEntity(j, i, str2, str1, localUri);
  }
  
  public GameBadgeEntity[] zzsa(int paramInt)
  {
    return new GameBadgeEntity[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/internal/game/GameBadgeEntityCreator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */