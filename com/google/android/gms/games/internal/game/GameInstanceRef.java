package com.google.android.gms.games.internal.game;

import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.zzc;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;
import com.google.android.gms.games.internal.constants.PlatformType;

public final class GameInstanceRef
  extends zzc
  implements GameInstance
{
  GameInstanceRef(DataHolder paramDataHolder, int paramInt)
  {
    super(paramDataHolder, paramInt);
  }
  
  public String getApplicationId()
  {
    return getString("external_game_id");
  }
  
  public String getDisplayName()
  {
    return getString("instance_display_name");
  }
  
  public String getPackageName()
  {
    return getString("package_name");
  }
  
  public String toString()
  {
    return zzz.zzx(this).zzg("ApplicationId", getApplicationId()).zzg("DisplayName", getDisplayName()).zzg("SupportsRealTime", Boolean.valueOf(zzblp())).zzg("SupportsTurnBased", Boolean.valueOf(zzblq())).zzg("PlatformType", PlatformType.zzrw(zzbeo())).zzg("PackageName", getPackageName()).zzg("PiracyCheckEnabled", Boolean.valueOf(zzblr())).zzg("Installed", Boolean.valueOf(zzbls())).toString();
  }
  
  public int zzbeo()
  {
    return getInteger("platform_type");
  }
  
  public boolean zzblp()
  {
    return getInteger("real_time_support") > 0;
  }
  
  public boolean zzblq()
  {
    return getInteger("turn_based_support") > 0;
  }
  
  public boolean zzblr()
  {
    return getInteger("piracy_check") > 0;
  }
  
  public boolean zzbls()
  {
    return getInteger("installed") > 0;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/internal/game/GameInstanceRef.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */