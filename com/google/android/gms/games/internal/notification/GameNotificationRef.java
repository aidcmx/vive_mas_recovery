package com.google.android.gms.games.internal.notification;

import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.zzc;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;

public final class GameNotificationRef
  extends zzc
  implements GameNotification
{
  GameNotificationRef(DataHolder paramDataHolder, int paramInt)
  {
    super(paramDataHolder, paramInt);
  }
  
  public long getId()
  {
    return getLong("_id");
  }
  
  public String getText()
  {
    return getString("text");
  }
  
  public String getTitle()
  {
    return getString("title");
  }
  
  public int getType()
  {
    return getInteger("type");
  }
  
  public String toString()
  {
    return zzz.zzx(this).zzg("Id", Long.valueOf(getId())).zzg("NotificationId", zzblv()).zzg("Type", Integer.valueOf(getType())).zzg("Title", getTitle()).zzg("Ticker", zzblw()).zzg("Text", getText()).zzg("CoalescedText", zzblx()).zzg("isAcknowledged", Boolean.valueOf(zzbly())).zzg("isSilent", Boolean.valueOf(zzblz())).zzg("isQuiet", Boolean.valueOf(zzbma())).toString();
  }
  
  public String zzblv()
  {
    return getString("notification_id");
  }
  
  public String zzblw()
  {
    return getString("ticker");
  }
  
  public String zzblx()
  {
    return getString("coalesced_text");
  }
  
  public boolean zzbly()
  {
    return getInteger("acknowledged") > 0;
  }
  
  public boolean zzblz()
  {
    return getInteger("alert_level") == 0;
  }
  
  public boolean zzbma()
  {
    return getInteger("alert_level") == 2;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/internal/notification/GameNotificationRef.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */