package com.google.android.gms.games.internal.player;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;

public final class MostRecentGameInfoEntity
  extends AbstractSafeParcelable
  implements MostRecentGameInfo
{
  public static final Parcelable.Creator<MostRecentGameInfoEntity> CREATOR = new MostRecentGameInfoEntityCreator();
  private final String adc;
  private final String add;
  private final long ade;
  private final Uri adf;
  private final Uri adg;
  private final Uri adh;
  private final int mVersionCode;
  
  MostRecentGameInfoEntity(int paramInt, String paramString1, String paramString2, long paramLong, Uri paramUri1, Uri paramUri2, Uri paramUri3)
  {
    this.mVersionCode = paramInt;
    this.adc = paramString1;
    this.add = paramString2;
    this.ade = paramLong;
    this.adf = paramUri1;
    this.adg = paramUri2;
    this.adh = paramUri3;
  }
  
  public MostRecentGameInfoEntity(MostRecentGameInfo paramMostRecentGameInfo)
  {
    this.mVersionCode = 2;
    this.adc = paramMostRecentGameInfo.zzbmb();
    this.add = paramMostRecentGameInfo.zzbmc();
    this.ade = paramMostRecentGameInfo.zzbmd();
    this.adf = paramMostRecentGameInfo.zzbme();
    this.adg = paramMostRecentGameInfo.zzbmf();
    this.adh = paramMostRecentGameInfo.zzbmg();
  }
  
  static int zza(MostRecentGameInfo paramMostRecentGameInfo)
  {
    return zzz.hashCode(new Object[] { paramMostRecentGameInfo.zzbmb(), paramMostRecentGameInfo.zzbmc(), Long.valueOf(paramMostRecentGameInfo.zzbmd()), paramMostRecentGameInfo.zzbme(), paramMostRecentGameInfo.zzbmf(), paramMostRecentGameInfo.zzbmg() });
  }
  
  static boolean zza(MostRecentGameInfo paramMostRecentGameInfo, Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if (!(paramObject instanceof MostRecentGameInfo)) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramMostRecentGameInfo == paramObject);
      paramObject = (MostRecentGameInfo)paramObject;
      if ((!zzz.equal(((MostRecentGameInfo)paramObject).zzbmb(), paramMostRecentGameInfo.zzbmb())) || (!zzz.equal(((MostRecentGameInfo)paramObject).zzbmc(), paramMostRecentGameInfo.zzbmc())) || (!zzz.equal(Long.valueOf(((MostRecentGameInfo)paramObject).zzbmd()), Long.valueOf(paramMostRecentGameInfo.zzbmd()))) || (!zzz.equal(((MostRecentGameInfo)paramObject).zzbme(), paramMostRecentGameInfo.zzbme())) || (!zzz.equal(((MostRecentGameInfo)paramObject).zzbmf(), paramMostRecentGameInfo.zzbmf()))) {
        break;
      }
      bool1 = bool2;
    } while (zzz.equal(((MostRecentGameInfo)paramObject).zzbmg(), paramMostRecentGameInfo.zzbmg()));
    return false;
  }
  
  static String zzb(MostRecentGameInfo paramMostRecentGameInfo)
  {
    return zzz.zzx(paramMostRecentGameInfo).zzg("GameId", paramMostRecentGameInfo.zzbmb()).zzg("GameName", paramMostRecentGameInfo.zzbmc()).zzg("ActivityTimestampMillis", Long.valueOf(paramMostRecentGameInfo.zzbmd())).zzg("GameIconUri", paramMostRecentGameInfo.zzbme()).zzg("GameHiResUri", paramMostRecentGameInfo.zzbmf()).zzg("GameFeaturedUri", paramMostRecentGameInfo.zzbmg()).toString();
  }
  
  public boolean equals(Object paramObject)
  {
    return zza(this, paramObject);
  }
  
  public int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    return zza(this);
  }
  
  public boolean isDataValid()
  {
    return true;
  }
  
  public String toString()
  {
    return zzb(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    MostRecentGameInfoEntityCreator.zza(this, paramParcel, paramInt);
  }
  
  public String zzbmb()
  {
    return this.adc;
  }
  
  public String zzbmc()
  {
    return this.add;
  }
  
  public long zzbmd()
  {
    return this.ade;
  }
  
  public Uri zzbme()
  {
    return this.adf;
  }
  
  public Uri zzbmf()
  {
    return this.adg;
  }
  
  public Uri zzbmg()
  {
    return this.adh;
  }
  
  public MostRecentGameInfo zzbmh()
  {
    return this;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/internal/player/MostRecentGameInfoEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */