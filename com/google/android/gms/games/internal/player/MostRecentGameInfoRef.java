package com.google.android.gms.games.internal.player;

import android.net.Uri;
import android.os.Parcel;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.zzc;

public final class MostRecentGameInfoRef
  extends zzc
  implements MostRecentGameInfo
{
  private final PlayerColumnNames XV;
  
  public MostRecentGameInfoRef(DataHolder paramDataHolder, int paramInt, PlayerColumnNames paramPlayerColumnNames)
  {
    super(paramDataHolder, paramInt);
    this.XV = paramPlayerColumnNames;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    return MostRecentGameInfoEntity.zza(this, paramObject);
  }
  
  public int hashCode()
  {
    return MostRecentGameInfoEntity.zza(this);
  }
  
  public String toString()
  {
    return MostRecentGameInfoEntity.zzb(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    ((MostRecentGameInfoEntity)freeze()).writeToParcel(paramParcel, paramInt);
  }
  
  public String zzbmb()
  {
    return getString(this.XV.adA);
  }
  
  public String zzbmc()
  {
    return getString(this.XV.adB);
  }
  
  public long zzbmd()
  {
    return getLong(this.XV.adC);
  }
  
  public Uri zzbme()
  {
    return zzhp(this.XV.adD);
  }
  
  public Uri zzbmf()
  {
    return zzhp(this.XV.adE);
  }
  
  public Uri zzbmg()
  {
    return zzhp(this.XV.adF);
  }
  
  public MostRecentGameInfo zzbmh()
  {
    return new MostRecentGameInfoEntity(this);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/internal/player/MostRecentGameInfoRef.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */