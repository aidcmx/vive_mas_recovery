package com.google.android.gms.games.internal.player;

import android.text.TextUtils;

public final class PlayerColumnNames
{
  public final String adA;
  public final String adB;
  public final String adC;
  public final String adD;
  public final String adE;
  public final String adF;
  public final String adG;
  public final String adH;
  public final String adI;
  public final String adJ;
  public final String adK;
  public final String adL;
  public final String adM;
  public final String adN;
  public final String adO;
  public final String adi;
  public final String adj;
  public final String adk;
  public final String adl;
  public final String adm;
  public final String adn;
  public final String ado;
  public final String adp;
  public final String adq;
  public final String adr;
  public final String ads;
  public final String adt;
  public final String adu;
  public final String adv;
  public final String adw;
  public final String adx;
  public final String ady;
  public final String adz;
  public final String name;
  public final String title;
  
  public PlayerColumnNames(String paramString)
  {
    if (TextUtils.isEmpty(paramString))
    {
      this.adi = "external_player_id";
      this.adj = "profile_name";
      this.adk = "profile_icon_image_uri";
      this.adl = "profile_icon_image_url";
      this.adm = "profile_hi_res_image_uri";
      this.adn = "profile_hi_res_image_url";
      this.ado = "last_updated";
      this.adp = "is_in_circles";
      this.adq = "played_with_timestamp";
      this.adr = "current_xp_total";
      this.ads = "current_level";
      this.adt = "current_level_min_xp";
      this.adu = "current_level_max_xp";
      this.adv = "next_level";
      this.adw = "next_level_max_xp";
      this.adx = "last_level_up_timestamp";
      this.title = "player_title";
      this.ady = "has_all_public_acls";
      this.adz = "is_profile_visible";
      this.adA = "most_recent_external_game_id";
      this.adB = "most_recent_game_name";
      this.adC = "most_recent_activity_timestamp";
      this.adD = "most_recent_game_icon_uri";
      this.adE = "most_recent_game_hi_res_uri";
      this.adF = "most_recent_game_featured_uri";
      this.adG = "has_debug_access";
      this.adH = "gamer_tag";
      this.name = "real_name";
      this.adI = "banner_image_landscape_uri";
      this.adJ = "banner_image_landscape_url";
      this.adK = "banner_image_portrait_uri";
      this.adL = "banner_image_portrait_url";
      this.adM = "gamer_friend_status";
      this.adN = "gamer_friend_update_timestamp";
      this.adO = "is_muted";
      return;
    }
    String str1 = String.valueOf(paramString);
    String str2 = String.valueOf("external_player_id");
    if (str2.length() != 0)
    {
      str1 = str1.concat(str2);
      this.adi = str1;
      str1 = String.valueOf(paramString);
      str2 = String.valueOf("profile_name");
      if (str2.length() == 0) {
        break label1250;
      }
      str1 = str1.concat(str2);
      label275:
      this.adj = str1;
      str1 = String.valueOf(paramString);
      str2 = String.valueOf("profile_icon_image_uri");
      if (str2.length() == 0) {
        break label1262;
      }
      str1 = str1.concat(str2);
      label304:
      this.adk = str1;
      str1 = String.valueOf(paramString);
      str2 = String.valueOf("profile_icon_image_url");
      if (str2.length() == 0) {
        break label1274;
      }
      str1 = str1.concat(str2);
      label333:
      this.adl = str1;
      str1 = String.valueOf(paramString);
      str2 = String.valueOf("profile_hi_res_image_uri");
      if (str2.length() == 0) {
        break label1286;
      }
      str1 = str1.concat(str2);
      label362:
      this.adm = str1;
      str1 = String.valueOf(paramString);
      str2 = String.valueOf("profile_hi_res_image_url");
      if (str2.length() == 0) {
        break label1298;
      }
      str1 = str1.concat(str2);
      label391:
      this.adn = str1;
      str1 = String.valueOf(paramString);
      str2 = String.valueOf("last_updated");
      if (str2.length() == 0) {
        break label1310;
      }
      str1 = str1.concat(str2);
      label420:
      this.ado = str1;
      str1 = String.valueOf(paramString);
      str2 = String.valueOf("is_in_circles");
      if (str2.length() == 0) {
        break label1322;
      }
      str1 = str1.concat(str2);
      label449:
      this.adp = str1;
      str1 = String.valueOf(paramString);
      str2 = String.valueOf("played_with_timestamp");
      if (str2.length() == 0) {
        break label1334;
      }
      str1 = str1.concat(str2);
      label478:
      this.adq = str1;
      str1 = String.valueOf(paramString);
      str2 = String.valueOf("current_xp_total");
      if (str2.length() == 0) {
        break label1346;
      }
      str1 = str1.concat(str2);
      label507:
      this.adr = str1;
      str1 = String.valueOf(paramString);
      str2 = String.valueOf("current_level");
      if (str2.length() == 0) {
        break label1358;
      }
      str1 = str1.concat(str2);
      label536:
      this.ads = str1;
      str1 = String.valueOf(paramString);
      str2 = String.valueOf("current_level_min_xp");
      if (str2.length() == 0) {
        break label1370;
      }
      str1 = str1.concat(str2);
      label565:
      this.adt = str1;
      str1 = String.valueOf(paramString);
      str2 = String.valueOf("current_level_max_xp");
      if (str2.length() == 0) {
        break label1382;
      }
      str1 = str1.concat(str2);
      label594:
      this.adu = str1;
      str1 = String.valueOf(paramString);
      str2 = String.valueOf("next_level");
      if (str2.length() == 0) {
        break label1394;
      }
      str1 = str1.concat(str2);
      label623:
      this.adv = str1;
      str1 = String.valueOf(paramString);
      str2 = String.valueOf("next_level_max_xp");
      if (str2.length() == 0) {
        break label1406;
      }
      str1 = str1.concat(str2);
      label652:
      this.adw = str1;
      str1 = String.valueOf(paramString);
      str2 = String.valueOf("last_level_up_timestamp");
      if (str2.length() == 0) {
        break label1418;
      }
      str1 = str1.concat(str2);
      label681:
      this.adx = str1;
      str1 = String.valueOf(paramString);
      str2 = String.valueOf("player_title");
      if (str2.length() == 0) {
        break label1430;
      }
      str1 = str1.concat(str2);
      label710:
      this.title = str1;
      str1 = String.valueOf(paramString);
      str2 = String.valueOf("has_all_public_acls");
      if (str2.length() == 0) {
        break label1442;
      }
      str1 = str1.concat(str2);
      label739:
      this.ady = str1;
      str1 = String.valueOf(paramString);
      str2 = String.valueOf("is_profile_visible");
      if (str2.length() == 0) {
        break label1454;
      }
      str1 = str1.concat(str2);
      label768:
      this.adz = str1;
      str1 = String.valueOf(paramString);
      str2 = String.valueOf("most_recent_external_game_id");
      if (str2.length() == 0) {
        break label1466;
      }
      str1 = str1.concat(str2);
      label797:
      this.adA = str1;
      str1 = String.valueOf(paramString);
      str2 = String.valueOf("most_recent_game_name");
      if (str2.length() == 0) {
        break label1478;
      }
      str1 = str1.concat(str2);
      label826:
      this.adB = str1;
      str1 = String.valueOf(paramString);
      str2 = String.valueOf("most_recent_activity_timestamp");
      if (str2.length() == 0) {
        break label1490;
      }
      str1 = str1.concat(str2);
      label855:
      this.adC = str1;
      str1 = String.valueOf(paramString);
      str2 = String.valueOf("most_recent_game_icon_uri");
      if (str2.length() == 0) {
        break label1502;
      }
      str1 = str1.concat(str2);
      label884:
      this.adD = str1;
      str1 = String.valueOf(paramString);
      str2 = String.valueOf("most_recent_game_hi_res_uri");
      if (str2.length() == 0) {
        break label1514;
      }
      str1 = str1.concat(str2);
      label913:
      this.adE = str1;
      str1 = String.valueOf(paramString);
      str2 = String.valueOf("most_recent_game_featured_uri");
      if (str2.length() == 0) {
        break label1526;
      }
      str1 = str1.concat(str2);
      label942:
      this.adF = str1;
      str1 = String.valueOf(paramString);
      str2 = String.valueOf("has_debug_access");
      if (str2.length() == 0) {
        break label1538;
      }
      str1 = str1.concat(str2);
      label971:
      this.adG = str1;
      str1 = String.valueOf(paramString);
      str2 = String.valueOf("gamer_tag");
      if (str2.length() == 0) {
        break label1550;
      }
      str1 = str1.concat(str2);
      label1000:
      this.adH = str1;
      str1 = String.valueOf(paramString);
      str2 = String.valueOf("real_name");
      if (str2.length() == 0) {
        break label1562;
      }
      str1 = str1.concat(str2);
      label1029:
      this.name = str1;
      str1 = String.valueOf(paramString);
      str2 = String.valueOf("banner_image_landscape_uri");
      if (str2.length() == 0) {
        break label1574;
      }
      str1 = str1.concat(str2);
      label1058:
      this.adI = str1;
      str1 = String.valueOf(paramString);
      str2 = String.valueOf("banner_image_landscape_url");
      if (str2.length() == 0) {
        break label1586;
      }
      str1 = str1.concat(str2);
      label1087:
      this.adJ = str1;
      str1 = String.valueOf(paramString);
      str2 = String.valueOf("banner_image_portrait_uri");
      if (str2.length() == 0) {
        break label1598;
      }
      str1 = str1.concat(str2);
      label1116:
      this.adK = str1;
      str1 = String.valueOf(paramString);
      str2 = String.valueOf("banner_image_portrait_url");
      if (str2.length() == 0) {
        break label1610;
      }
      str1 = str1.concat(str2);
      label1145:
      this.adL = str1;
      str1 = String.valueOf(paramString);
      str2 = String.valueOf("gamer_friend_status");
      if (str2.length() == 0) {
        break label1622;
      }
      str1 = str1.concat(str2);
      label1174:
      this.adM = str1;
      str1 = String.valueOf(paramString);
      str2 = String.valueOf("gamer_friend_update_timestamp");
      if (str2.length() == 0) {
        break label1634;
      }
      str1 = str1.concat(str2);
      label1203:
      this.adN = str1;
      paramString = String.valueOf(paramString);
      str1 = String.valueOf("is_muted");
      if (str1.length() == 0) {
        break label1646;
      }
    }
    label1250:
    label1262:
    label1274:
    label1286:
    label1298:
    label1310:
    label1322:
    label1334:
    label1346:
    label1358:
    label1370:
    label1382:
    label1394:
    label1406:
    label1418:
    label1430:
    label1442:
    label1454:
    label1466:
    label1478:
    label1490:
    label1502:
    label1514:
    label1526:
    label1538:
    label1550:
    label1562:
    label1574:
    label1586:
    label1598:
    label1610:
    label1622:
    label1634:
    label1646:
    for (paramString = paramString.concat(str1);; paramString = new String(paramString))
    {
      this.adO = paramString;
      return;
      str1 = new String(str1);
      break;
      str1 = new String(str1);
      break label275;
      str1 = new String(str1);
      break label304;
      str1 = new String(str1);
      break label333;
      str1 = new String(str1);
      break label362;
      str1 = new String(str1);
      break label391;
      str1 = new String(str1);
      break label420;
      str1 = new String(str1);
      break label449;
      str1 = new String(str1);
      break label478;
      str1 = new String(str1);
      break label507;
      str1 = new String(str1);
      break label536;
      str1 = new String(str1);
      break label565;
      str1 = new String(str1);
      break label594;
      str1 = new String(str1);
      break label623;
      str1 = new String(str1);
      break label652;
      str1 = new String(str1);
      break label681;
      str1 = new String(str1);
      break label710;
      str1 = new String(str1);
      break label739;
      str1 = new String(str1);
      break label768;
      str1 = new String(str1);
      break label797;
      str1 = new String(str1);
      break label826;
      str1 = new String(str1);
      break label855;
      str1 = new String(str1);
      break label884;
      str1 = new String(str1);
      break label913;
      str1 = new String(str1);
      break label942;
      str1 = new String(str1);
      break label971;
      str1 = new String(str1);
      break label1000;
      str1 = new String(str1);
      break label1029;
      str1 = new String(str1);
      break label1058;
      str1 = new String(str1);
      break label1087;
      str1 = new String(str1);
      break label1116;
      str1 = new String(str1);
      break label1145;
      str1 = new String(str1);
      break label1174;
      str1 = new String(str1);
      break label1203;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/internal/player/PlayerColumnNames.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */