package com.google.android.gms.games.internal.player;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;
import com.google.android.gms.games.Players.LoadProfileSettingsResult;

public class ProfileSettingsEntity
  extends AbstractSafeParcelable
  implements Players.LoadProfileSettingsResult
{
  public static final Parcelable.Creator<ProfileSettingsEntity> CREATOR = new ProfileSettingsEntityCreator();
  private final boolean XF;
  private final String XH;
  private final boolean adP;
  private final boolean adQ;
  private final StockProfileImageEntity adR;
  private final boolean adS;
  private final boolean adT;
  private final Status hv;
  private final int mVersionCode;
  
  ProfileSettingsEntity(int paramInt, Status paramStatus, String paramString, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, StockProfileImageEntity paramStockProfileImageEntity, boolean paramBoolean4, boolean paramBoolean5)
  {
    this.mVersionCode = paramInt;
    this.hv = paramStatus;
    this.XH = paramString;
    this.adP = paramBoolean1;
    this.XF = paramBoolean2;
    this.adQ = paramBoolean3;
    this.adR = paramStockProfileImageEntity;
    this.adS = paramBoolean4;
    this.adT = paramBoolean5;
  }
  
  public ProfileSettingsEntity(DataHolder paramDataHolder)
  {
    this.mVersionCode = 3;
    this.hv = new Status(paramDataHolder.getStatusCode());
    if ((this.hv.isSuccess()) && (paramDataHolder.getCount() > 0))
    {
      int i = paramDataHolder.zzga(0);
      this.XH = paramDataHolder.zzd("gamer_tag", 0, i);
      this.adP = paramDataHolder.zze("gamer_tag_explicitly_set", 0, i);
      this.XF = paramDataHolder.zze("profile_visible", 0, i);
      this.adQ = paramDataHolder.zze("profile_visibility_explicitly_set", 0, i);
      String str1 = paramDataHolder.zzd("stock_avatar_url", 0, i);
      String str2 = paramDataHolder.zzd("stock_avatar_uri", 0, i);
      if ((!TextUtils.isEmpty(str1)) && (!TextUtils.isEmpty(str2))) {}
      for (this.adR = new StockProfileImageEntity(str1, Uri.parse(str2));; this.adR = null)
      {
        this.adS = paramDataHolder.zze("profile_discoverable", 0, i);
        this.adT = paramDataHolder.zze("auto_sign_in", 0, i);
        return;
      }
    }
    this.XH = null;
    this.adP = false;
    this.XF = false;
    this.adQ = false;
    this.adR = null;
    this.adS = false;
    this.adT = false;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if (!(paramObject instanceof Players.LoadProfileSettingsResult)) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (this == paramObject);
      paramObject = (Players.LoadProfileSettingsResult)paramObject;
      if ((!zzz.equal(this.XH, ((Players.LoadProfileSettingsResult)paramObject).zzbhq())) || (!zzz.equal(Boolean.valueOf(this.adP), Boolean.valueOf(((Players.LoadProfileSettingsResult)paramObject).zzbia()))) || (!zzz.equal(Boolean.valueOf(this.XF), Boolean.valueOf(((Players.LoadProfileSettingsResult)paramObject).zzbht()))) || (!zzz.equal(Boolean.valueOf(this.adQ), Boolean.valueOf(((Players.LoadProfileSettingsResult)paramObject).zzbhy()))) || (!zzz.equal(this.hv, ((Players.LoadProfileSettingsResult)paramObject).getStatus())) || (!zzz.equal(this.adR, ((Players.LoadProfileSettingsResult)paramObject).zzbhz())) || (!zzz.equal(Boolean.valueOf(this.adS), Boolean.valueOf(((Players.LoadProfileSettingsResult)paramObject).zzbib())))) {
        break;
      }
      bool1 = bool2;
    } while (zzz.equal(Boolean.valueOf(this.adT), Boolean.valueOf(((Players.LoadProfileSettingsResult)paramObject).zzbic())));
    return false;
  }
  
  public Status getStatus()
  {
    return this.hv;
  }
  
  public int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.XH, Boolean.valueOf(this.adP), Boolean.valueOf(this.XF), Boolean.valueOf(this.adQ), this.hv, this.adR, Boolean.valueOf(this.adS), Boolean.valueOf(this.adT) });
  }
  
  public String toString()
  {
    return zzz.zzx(this).zzg("GamerTag", this.XH).zzg("IsGamerTagExplicitlySet", Boolean.valueOf(this.adP)).zzg("IsProfileVisible", Boolean.valueOf(this.XF)).zzg("IsVisibilityExplicitlySet", Boolean.valueOf(this.adQ)).zzg("Status", this.hv).zzg("StockProfileImage", this.adR).zzg("IsProfileDiscoverable", Boolean.valueOf(this.adS)).zzg("AutoSignIn", Boolean.valueOf(this.adT)).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    ProfileSettingsEntityCreator.zza(this, paramParcel, paramInt);
  }
  
  public String zzbhq()
  {
    return this.XH;
  }
  
  public boolean zzbht()
  {
    return this.XF;
  }
  
  public boolean zzbhy()
  {
    return this.adQ;
  }
  
  public StockProfileImage zzbhz()
  {
    return this.adR;
  }
  
  public boolean zzbia()
  {
    return this.adP;
  }
  
  public boolean zzbib()
  {
    return this.adS;
  }
  
  public boolean zzbic()
  {
    return this.adT;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/internal/player/ProfileSettingsEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */