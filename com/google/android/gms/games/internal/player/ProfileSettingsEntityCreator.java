package com.google.android.gms.games.internal.player;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class ProfileSettingsEntityCreator
  implements Parcelable.Creator<ProfileSettingsEntity>
{
  static void zza(ProfileSettingsEntity paramProfileSettingsEntity, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zza(paramParcel, 1, paramProfileSettingsEntity.getStatus(), paramInt, false);
    zzb.zza(paramParcel, 2, paramProfileSettingsEntity.zzbhq(), false);
    zzb.zza(paramParcel, 3, paramProfileSettingsEntity.zzbia());
    zzb.zza(paramParcel, 4, paramProfileSettingsEntity.zzbht());
    zzb.zza(paramParcel, 5, paramProfileSettingsEntity.zzbhy());
    zzb.zza(paramParcel, 6, paramProfileSettingsEntity.zzbhz(), paramInt, false);
    zzb.zza(paramParcel, 7, paramProfileSettingsEntity.zzbib());
    zzb.zzc(paramParcel, 1000, paramProfileSettingsEntity.getVersionCode());
    zzb.zza(paramParcel, 8, paramProfileSettingsEntity.zzbic());
    zzb.zzaj(paramParcel, i);
  }
  
  public ProfileSettingsEntity zzmc(Parcel paramParcel)
  {
    StockProfileImageEntity localStockProfileImageEntity = null;
    boolean bool1 = false;
    int j = zza.zzcr(paramParcel);
    boolean bool2 = false;
    boolean bool3 = false;
    boolean bool4 = false;
    boolean bool5 = false;
    String str = null;
    Status localStatus = null;
    int i = 0;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.zzcq(paramParcel);
      switch (zza.zzgu(k))
      {
      default: 
        zza.zzb(paramParcel, k);
        break;
      case 1: 
        localStatus = (Status)zza.zza(paramParcel, k, Status.CREATOR);
        break;
      case 2: 
        str = zza.zzq(paramParcel, k);
        break;
      case 3: 
        bool5 = zza.zzc(paramParcel, k);
        break;
      case 4: 
        bool4 = zza.zzc(paramParcel, k);
        break;
      case 5: 
        bool3 = zza.zzc(paramParcel, k);
        break;
      case 6: 
        localStockProfileImageEntity = (StockProfileImageEntity)zza.zza(paramParcel, k, StockProfileImageEntity.CREATOR);
        break;
      case 7: 
        bool2 = zza.zzc(paramParcel, k);
        break;
      case 1000: 
        i = zza.zzg(paramParcel, k);
        break;
      case 8: 
        bool1 = zza.zzc(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new ProfileSettingsEntity(i, localStatus, str, bool5, bool4, bool3, localStockProfileImageEntity, bool2, bool1);
  }
  
  public ProfileSettingsEntity[] zzsg(int paramInt)
  {
    return new ProfileSettingsEntity[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/internal/player/ProfileSettingsEntityCreator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */