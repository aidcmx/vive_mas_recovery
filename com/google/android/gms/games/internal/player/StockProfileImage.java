package com.google.android.gms.games.internal.player;

import android.net.Uri;
import android.os.Parcelable;
import com.google.android.gms.common.data.Freezable;

public abstract interface StockProfileImage
  extends Parcelable, Freezable<StockProfileImage>
{
  public abstract String getImageUrl();
  
  public abstract Uri zzbio();
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/internal/player/StockProfileImage.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */