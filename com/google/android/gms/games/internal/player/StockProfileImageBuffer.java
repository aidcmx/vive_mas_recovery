package com.google.android.gms.games.internal.player;

import com.google.android.gms.common.data.AbstractDataBuffer;
import com.google.android.gms.common.data.DataHolder;

public class StockProfileImageBuffer
  extends AbstractDataBuffer<StockProfileImage>
{
  public StockProfileImageBuffer(DataHolder paramDataHolder)
  {
    super(paramDataHolder);
  }
  
  public StockProfileImage zzsh(int paramInt)
  {
    return new StockProfileImageRef(this.zy, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/internal/player/StockProfileImageBuffer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */