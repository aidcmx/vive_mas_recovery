package com.google.android.gms.games.internal.player;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;

public final class StockProfileImageEntity
  extends AbstractSafeParcelable
  implements StockProfileImage
{
  public static final Parcelable.Creator<StockProfileImageEntity> CREATOR = new StockProfileImageEntityCreator();
  private final Uri Ym;
  private final String adU;
  private final int mVersionCode;
  
  StockProfileImageEntity(int paramInt, String paramString, Uri paramUri)
  {
    this.mVersionCode = paramInt;
    this.adU = paramString;
    this.Ym = paramUri;
  }
  
  public StockProfileImageEntity(StockProfileImage paramStockProfileImage)
  {
    this(1, paramStockProfileImage.getImageUrl(), paramStockProfileImage.zzbio());
  }
  
  public StockProfileImageEntity(String paramString, Uri paramUri)
  {
    this(1, paramString, paramUri);
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if (!(paramObject instanceof StockProfileImage)) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramObject == this);
      paramObject = (StockProfileImage)paramObject;
      if (!zzz.equal(this.adU, ((StockProfileImage)paramObject).getImageUrl())) {
        break;
      }
      bool1 = bool2;
    } while (zzz.equal(this.Ym, ((StockProfileImage)paramObject).zzbio()));
    return false;
  }
  
  public String getImageUrl()
  {
    return this.adU;
  }
  
  public int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.adU, this.Ym });
  }
  
  public boolean isDataValid()
  {
    return true;
  }
  
  public String toString()
  {
    return zzz.zzx(this).zzg("ImageId", this.adU).zzg("ImageUri", this.Ym).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    StockProfileImageEntityCreator.zza(this, paramParcel, paramInt);
  }
  
  public Uri zzbio()
  {
    return this.Ym;
  }
  
  public StockProfileImage zzbmi()
  {
    return this;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/internal/player/StockProfileImageEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */