package com.google.android.gms.games.internal.player;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class StockProfileImageEntityCreator
  implements Parcelable.Creator<StockProfileImageEntity>
{
  static void zza(StockProfileImageEntity paramStockProfileImageEntity, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zza(paramParcel, 1, paramStockProfileImageEntity.getImageUrl(), false);
    zzb.zza(paramParcel, 2, paramStockProfileImageEntity.zzbio(), paramInt, false);
    zzb.zzc(paramParcel, 1000, paramStockProfileImageEntity.getVersionCode());
    zzb.zzaj(paramParcel, i);
  }
  
  public StockProfileImageEntity zzmd(Parcel paramParcel)
  {
    Uri localUri = null;
    int j = zza.zzcr(paramParcel);
    int i = 0;
    String str = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.zzcq(paramParcel);
      switch (zza.zzgu(k))
      {
      default: 
        zza.zzb(paramParcel, k);
        break;
      case 1: 
        str = zza.zzq(paramParcel, k);
        break;
      case 2: 
        localUri = (Uri)zza.zza(paramParcel, k, Uri.CREATOR);
        break;
      case 1000: 
        i = zza.zzg(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new StockProfileImageEntity(i, str, localUri);
  }
  
  public StockProfileImageEntity[] zzsi(int paramInt)
  {
    return new StockProfileImageEntity[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/internal/player/StockProfileImageEntityCreator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */