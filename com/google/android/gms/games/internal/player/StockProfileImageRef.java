package com.google.android.gms.games.internal.player;

import android.net.Uri;
import android.os.Parcel;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.zzc;

public class StockProfileImageRef
  extends zzc
  implements StockProfileImage
{
  public StockProfileImageRef(DataHolder paramDataHolder, int paramInt)
  {
    super(paramDataHolder, paramInt);
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public String getImageUrl()
  {
    return getString("image_url");
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    ((StockProfileImageEntity)freeze()).writeToParcel(paramParcel, paramInt);
  }
  
  public Uri zzbio()
  {
    return Uri.parse(getString("image_uri"));
  }
  
  public StockProfileImage zzbmi()
  {
    return new StockProfileImageEntity(this);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/internal/player/StockProfileImageRef.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */