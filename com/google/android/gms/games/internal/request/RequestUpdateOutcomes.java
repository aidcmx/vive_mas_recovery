package com.google.android.gms.games.internal.request;

import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.games.internal.constants.RequestUpdateResultOutcome;
import java.util.HashMap;
import java.util.Set;

public final class RequestUpdateOutcomes
{
  private static final String[] adW = { "requestId", "outcome" };
  private final HashMap<String, Integer> adX;
  private final int uo;
  
  private RequestUpdateOutcomes(int paramInt, HashMap<String, Integer> paramHashMap)
  {
    this.uo = paramInt;
    this.adX = paramHashMap;
  }
  
  public static RequestUpdateOutcomes zzbl(DataHolder paramDataHolder)
  {
    Builder localBuilder = new Builder();
    localBuilder.zzsk(paramDataHolder.getStatusCode());
    int j = paramDataHolder.getCount();
    int i = 0;
    while (i < j)
    {
      int k = paramDataHolder.zzga(i);
      localBuilder.zzw(paramDataHolder.zzd("requestId", i, k), paramDataHolder.zzc("outcome", i, k));
      i += 1;
    }
    return localBuilder.zzbml();
  }
  
  public Set<String> getRequestIds()
  {
    return this.adX.keySet();
  }
  
  public int getRequestOutcome(String paramString)
  {
    zzaa.zzb(this.adX.containsKey(paramString), String.valueOf(paramString).length() + 46 + "Request " + paramString + " was not part of the update operation!");
    return ((Integer)this.adX.get(paramString)).intValue();
  }
  
  public static final class Builder
  {
    private HashMap<String, Integer> adX = new HashMap();
    private int uo = 0;
    
    public RequestUpdateOutcomes zzbml()
    {
      return new RequestUpdateOutcomes(this.uo, this.adX, null);
    }
    
    public Builder zzsk(int paramInt)
    {
      this.uo = paramInt;
      return this;
    }
    
    public Builder zzw(String paramString, int paramInt)
    {
      if (RequestUpdateResultOutcome.isValid(paramInt)) {
        this.adX.put(paramString, Integer.valueOf(paramInt));
      }
      return this;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/internal/request/RequestUpdateOutcomes.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */