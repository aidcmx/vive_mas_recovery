package com.google.android.gms.games.leaderboard;

import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;
import com.google.android.gms.games.internal.constants.LeaderboardCollection;
import com.google.android.gms.games.internal.constants.TimeSpan;

public final class LeaderboardVariantEntity
  implements LeaderboardVariant
{
  private final String aeA;
  private final String aeB;
  private final int aeq;
  private final int aer;
  private final boolean aes;
  private final long aet;
  private final String aeu;
  private final long aev;
  private final String aew;
  private final String aex;
  private final long aey;
  private final String aez;
  
  public LeaderboardVariantEntity(LeaderboardVariant paramLeaderboardVariant)
  {
    this.aeq = paramLeaderboardVariant.getTimeSpan();
    this.aer = paramLeaderboardVariant.getCollection();
    this.aes = paramLeaderboardVariant.hasPlayerInfo();
    this.aet = paramLeaderboardVariant.getRawPlayerScore();
    this.aeu = paramLeaderboardVariant.getDisplayPlayerScore();
    this.aev = paramLeaderboardVariant.getPlayerRank();
    this.aew = paramLeaderboardVariant.getDisplayPlayerRank();
    this.aex = paramLeaderboardVariant.getPlayerScoreTag();
    this.aey = paramLeaderboardVariant.getNumScores();
    this.aez = paramLeaderboardVariant.zzbmp();
    this.aeA = paramLeaderboardVariant.zzbmq();
    this.aeB = paramLeaderboardVariant.zzbmr();
  }
  
  static int zza(LeaderboardVariant paramLeaderboardVariant)
  {
    return zzz.hashCode(new Object[] { Integer.valueOf(paramLeaderboardVariant.getTimeSpan()), Integer.valueOf(paramLeaderboardVariant.getCollection()), Boolean.valueOf(paramLeaderboardVariant.hasPlayerInfo()), Long.valueOf(paramLeaderboardVariant.getRawPlayerScore()), paramLeaderboardVariant.getDisplayPlayerScore(), Long.valueOf(paramLeaderboardVariant.getPlayerRank()), paramLeaderboardVariant.getDisplayPlayerRank(), Long.valueOf(paramLeaderboardVariant.getNumScores()), paramLeaderboardVariant.zzbmp(), paramLeaderboardVariant.zzbmr(), paramLeaderboardVariant.zzbmq() });
  }
  
  static boolean zza(LeaderboardVariant paramLeaderboardVariant, Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if (!(paramObject instanceof LeaderboardVariant)) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramLeaderboardVariant == paramObject);
      paramObject = (LeaderboardVariant)paramObject;
      if ((!zzz.equal(Integer.valueOf(((LeaderboardVariant)paramObject).getTimeSpan()), Integer.valueOf(paramLeaderboardVariant.getTimeSpan()))) || (!zzz.equal(Integer.valueOf(((LeaderboardVariant)paramObject).getCollection()), Integer.valueOf(paramLeaderboardVariant.getCollection()))) || (!zzz.equal(Boolean.valueOf(((LeaderboardVariant)paramObject).hasPlayerInfo()), Boolean.valueOf(paramLeaderboardVariant.hasPlayerInfo()))) || (!zzz.equal(Long.valueOf(((LeaderboardVariant)paramObject).getRawPlayerScore()), Long.valueOf(paramLeaderboardVariant.getRawPlayerScore()))) || (!zzz.equal(((LeaderboardVariant)paramObject).getDisplayPlayerScore(), paramLeaderboardVariant.getDisplayPlayerScore())) || (!zzz.equal(Long.valueOf(((LeaderboardVariant)paramObject).getPlayerRank()), Long.valueOf(paramLeaderboardVariant.getPlayerRank()))) || (!zzz.equal(((LeaderboardVariant)paramObject).getDisplayPlayerRank(), paramLeaderboardVariant.getDisplayPlayerRank())) || (!zzz.equal(Long.valueOf(((LeaderboardVariant)paramObject).getNumScores()), Long.valueOf(paramLeaderboardVariant.getNumScores()))) || (!zzz.equal(((LeaderboardVariant)paramObject).zzbmp(), paramLeaderboardVariant.zzbmp())) || (!zzz.equal(((LeaderboardVariant)paramObject).zzbmr(), paramLeaderboardVariant.zzbmr()))) {
        break;
      }
      bool1 = bool2;
    } while (zzz.equal(((LeaderboardVariant)paramObject).zzbmq(), paramLeaderboardVariant.zzbmq()));
    return false;
  }
  
  static String zzb(LeaderboardVariant paramLeaderboardVariant)
  {
    zzz.zza localzza = zzz.zzx(paramLeaderboardVariant).zzg("TimeSpan", TimeSpan.zzrw(paramLeaderboardVariant.getTimeSpan())).zzg("Collection", LeaderboardCollection.zzrw(paramLeaderboardVariant.getCollection()));
    if (paramLeaderboardVariant.hasPlayerInfo())
    {
      localObject = Long.valueOf(paramLeaderboardVariant.getRawPlayerScore());
      localzza = localzza.zzg("RawPlayerScore", localObject);
      if (!paramLeaderboardVariant.hasPlayerInfo()) {
        break label191;
      }
      localObject = paramLeaderboardVariant.getDisplayPlayerScore();
      label76:
      localzza = localzza.zzg("DisplayPlayerScore", localObject);
      if (!paramLeaderboardVariant.hasPlayerInfo()) {
        break label197;
      }
      localObject = Long.valueOf(paramLeaderboardVariant.getPlayerRank());
      label103:
      localzza = localzza.zzg("PlayerRank", localObject);
      if (!paramLeaderboardVariant.hasPlayerInfo()) {
        break label203;
      }
    }
    label191:
    label197:
    label203:
    for (Object localObject = paramLeaderboardVariant.getDisplayPlayerRank();; localObject = "none")
    {
      return localzza.zzg("DisplayPlayerRank", localObject).zzg("NumScores", Long.valueOf(paramLeaderboardVariant.getNumScores())).zzg("TopPageNextToken", paramLeaderboardVariant.zzbmp()).zzg("WindowPageNextToken", paramLeaderboardVariant.zzbmr()).zzg("WindowPagePrevToken", paramLeaderboardVariant.zzbmq()).toString();
      localObject = "none";
      break;
      localObject = "none";
      break label76;
      localObject = "none";
      break label103;
    }
  }
  
  public boolean equals(Object paramObject)
  {
    return zza(this, paramObject);
  }
  
  public int getCollection()
  {
    return this.aer;
  }
  
  public String getDisplayPlayerRank()
  {
    return this.aew;
  }
  
  public String getDisplayPlayerScore()
  {
    return this.aeu;
  }
  
  public long getNumScores()
  {
    return this.aey;
  }
  
  public long getPlayerRank()
  {
    return this.aev;
  }
  
  public String getPlayerScoreTag()
  {
    return this.aex;
  }
  
  public long getRawPlayerScore()
  {
    return this.aet;
  }
  
  public int getTimeSpan()
  {
    return this.aeq;
  }
  
  public boolean hasPlayerInfo()
  {
    return this.aes;
  }
  
  public int hashCode()
  {
    return zza(this);
  }
  
  public boolean isDataValid()
  {
    return true;
  }
  
  public String toString()
  {
    return zzb(this);
  }
  
  public String zzbmp()
  {
    return this.aez;
  }
  
  public String zzbmq()
  {
    return this.aeA;
  }
  
  public String zzbmr()
  {
    return this.aeB;
  }
  
  public LeaderboardVariant zzbms()
  {
    return this;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/leaderboard/LeaderboardVariantEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */