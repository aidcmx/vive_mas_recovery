package com.google.android.gms.games.multiplayer;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.GameEntity;
import com.google.android.gms.games.internal.GamesDowngradeableSafeParcel;
import java.util.ArrayList;

public final class InvitationEntity
  extends GamesDowngradeableSafeParcel
  implements Invitation
{
  public static final Parcelable.Creator<InvitationEntity> CREATOR = new InvitationEntityCreatorCompat();
  private final GameEntity acT;
  private final long aeD;
  private final int aeE;
  private final ParticipantEntity aeF;
  private final ArrayList<ParticipantEntity> aeG;
  private final int aeH;
  private final int aeI;
  private final String hm;
  private final int mVersionCode;
  
  InvitationEntity(int paramInt1, GameEntity paramGameEntity, String paramString, long paramLong, int paramInt2, ParticipantEntity paramParticipantEntity, ArrayList<ParticipantEntity> paramArrayList, int paramInt3, int paramInt4)
  {
    this.mVersionCode = paramInt1;
    this.acT = paramGameEntity;
    this.hm = paramString;
    this.aeD = paramLong;
    this.aeE = paramInt2;
    this.aeF = paramParticipantEntity;
    this.aeG = paramArrayList;
    this.aeH = paramInt3;
    this.aeI = paramInt4;
  }
  
  InvitationEntity(Invitation paramInvitation)
  {
    this.mVersionCode = 2;
    this.acT = new GameEntity(paramInvitation.getGame());
    this.hm = paramInvitation.getInvitationId();
    this.aeD = paramInvitation.getCreationTimestamp();
    this.aeE = paramInvitation.getInvitationType();
    this.aeH = paramInvitation.getVariant();
    this.aeI = paramInvitation.getAvailableAutoMatchSlots();
    String str = paramInvitation.getInviter().getParticipantId();
    Participant localParticipant = null;
    ArrayList localArrayList = paramInvitation.getParticipants();
    int j = localArrayList.size();
    this.aeG = new ArrayList(j);
    int i = 0;
    paramInvitation = localParticipant;
    while (i < j)
    {
      localParticipant = (Participant)localArrayList.get(i);
      if (localParticipant.getParticipantId().equals(str)) {
        paramInvitation = localParticipant;
      }
      this.aeG.add((ParticipantEntity)localParticipant.freeze());
      i += 1;
    }
    zzaa.zzb(paramInvitation, "Must have a valid inviter!");
    this.aeF = ((ParticipantEntity)paramInvitation.freeze());
  }
  
  static int zza(Invitation paramInvitation)
  {
    return zzz.hashCode(new Object[] { paramInvitation.getGame(), paramInvitation.getInvitationId(), Long.valueOf(paramInvitation.getCreationTimestamp()), Integer.valueOf(paramInvitation.getInvitationType()), paramInvitation.getInviter(), paramInvitation.getParticipants(), Integer.valueOf(paramInvitation.getVariant()), Integer.valueOf(paramInvitation.getAvailableAutoMatchSlots()) });
  }
  
  static boolean zza(Invitation paramInvitation, Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if (!(paramObject instanceof Invitation)) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramInvitation == paramObject);
      paramObject = (Invitation)paramObject;
      if ((!zzz.equal(((Invitation)paramObject).getGame(), paramInvitation.getGame())) || (!zzz.equal(((Invitation)paramObject).getInvitationId(), paramInvitation.getInvitationId())) || (!zzz.equal(Long.valueOf(((Invitation)paramObject).getCreationTimestamp()), Long.valueOf(paramInvitation.getCreationTimestamp()))) || (!zzz.equal(Integer.valueOf(((Invitation)paramObject).getInvitationType()), Integer.valueOf(paramInvitation.getInvitationType()))) || (!zzz.equal(((Invitation)paramObject).getInviter(), paramInvitation.getInviter())) || (!zzz.equal(((Invitation)paramObject).getParticipants(), paramInvitation.getParticipants())) || (!zzz.equal(Integer.valueOf(((Invitation)paramObject).getVariant()), Integer.valueOf(paramInvitation.getVariant())))) {
        break;
      }
      bool1 = bool2;
    } while (zzz.equal(Integer.valueOf(((Invitation)paramObject).getAvailableAutoMatchSlots()), Integer.valueOf(paramInvitation.getAvailableAutoMatchSlots())));
    return false;
  }
  
  static String zzb(Invitation paramInvitation)
  {
    return zzz.zzx(paramInvitation).zzg("Game", paramInvitation.getGame()).zzg("InvitationId", paramInvitation.getInvitationId()).zzg("CreationTimestamp", Long.valueOf(paramInvitation.getCreationTimestamp())).zzg("InvitationType", Integer.valueOf(paramInvitation.getInvitationType())).zzg("Inviter", paramInvitation.getInviter()).zzg("Participants", paramInvitation.getParticipants()).zzg("Variant", Integer.valueOf(paramInvitation.getVariant())).zzg("AvailableAutoMatchSlots", Integer.valueOf(paramInvitation.getAvailableAutoMatchSlots())).toString();
  }
  
  public boolean equals(Object paramObject)
  {
    return zza(this, paramObject);
  }
  
  public Invitation freeze()
  {
    return this;
  }
  
  public int getAvailableAutoMatchSlots()
  {
    return this.aeI;
  }
  
  public long getCreationTimestamp()
  {
    return this.aeD;
  }
  
  public Game getGame()
  {
    return this.acT;
  }
  
  public String getInvitationId()
  {
    return this.hm;
  }
  
  public int getInvitationType()
  {
    return this.aeE;
  }
  
  public Participant getInviter()
  {
    return this.aeF;
  }
  
  public ArrayList<Participant> getParticipants()
  {
    return new ArrayList(this.aeG);
  }
  
  public int getVariant()
  {
    return this.aeH;
  }
  
  public int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    return zza(this);
  }
  
  public boolean isDataValid()
  {
    return true;
  }
  
  public String toString()
  {
    return zzb(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    if (!zzawa()) {
      InvitationEntityCreator.zza(this, paramParcel, paramInt);
    }
    for (;;)
    {
      return;
      this.acT.writeToParcel(paramParcel, paramInt);
      paramParcel.writeString(this.hm);
      paramParcel.writeLong(this.aeD);
      paramParcel.writeInt(this.aeE);
      this.aeF.writeToParcel(paramParcel, paramInt);
      int j = this.aeG.size();
      paramParcel.writeInt(j);
      int i = 0;
      while (i < j)
      {
        ((ParticipantEntity)this.aeG.get(i)).writeToParcel(paramParcel, paramInt);
        i += 1;
      }
    }
  }
  
  static final class InvitationEntityCreatorCompat
    extends InvitationEntityCreator
  {
    public InvitationEntity zzmf(Parcel paramParcel)
    {
      if ((InvitationEntity.zze(InvitationEntity.zzbho())) || (InvitationEntity.zzjv(InvitationEntity.class.getCanonicalName()))) {
        return super.zzmf(paramParcel);
      }
      GameEntity localGameEntity = (GameEntity)GameEntity.CREATOR.createFromParcel(paramParcel);
      String str = paramParcel.readString();
      long l = paramParcel.readLong();
      int j = paramParcel.readInt();
      ParticipantEntity localParticipantEntity = (ParticipantEntity)ParticipantEntity.CREATOR.createFromParcel(paramParcel);
      int k = paramParcel.readInt();
      ArrayList localArrayList = new ArrayList(k);
      int i = 0;
      while (i < k)
      {
        localArrayList.add((ParticipantEntity)ParticipantEntity.CREATOR.createFromParcel(paramParcel));
        i += 1;
      }
      return new InvitationEntity(2, localGameEntity, str, l, j, localParticipantEntity, localArrayList, -1, 0);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/multiplayer/InvitationEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */