package com.google.android.gms.games.multiplayer;

import android.database.CharArrayBuffer;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;
import com.google.android.gms.common.util.zzg;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerEntity;
import com.google.android.gms.games.internal.GamesDowngradeableSafeParcel;

public final class ParticipantEntity
  extends GamesDowngradeableSafeParcel
  implements Participant
{
  public static final Parcelable.Creator<ParticipantEntity> CREATOR = new ParticipantEntityCreatorCompat();
  private final Uri WQ;
  private final Uri WR;
  private final String Xb;
  private final String Xc;
  private final String YJ;
  private final PlayerEntity Ye;
  private final String ZU;
  private final boolean aeK;
  private final ParticipantResult aeL;
  private final String jh;
  private final int mVersionCode;
  private final int mt;
  private final int zzbtt;
  
  ParticipantEntity(int paramInt1, String paramString1, String paramString2, Uri paramUri1, Uri paramUri2, int paramInt2, String paramString3, boolean paramBoolean, PlayerEntity paramPlayerEntity, int paramInt3, ParticipantResult paramParticipantResult, String paramString4, String paramString5)
  {
    this.mVersionCode = paramInt1;
    this.ZU = paramString1;
    this.jh = paramString2;
    this.WQ = paramUri1;
    this.WR = paramUri2;
    this.zzbtt = paramInt2;
    this.YJ = paramString3;
    this.aeK = paramBoolean;
    this.Ye = paramPlayerEntity;
    this.mt = paramInt3;
    this.aeL = paramParticipantResult;
    this.Xb = paramString4;
    this.Xc = paramString5;
  }
  
  public ParticipantEntity(Participant paramParticipant)
  {
    this.mVersionCode = 3;
    this.ZU = paramParticipant.getParticipantId();
    this.jh = paramParticipant.getDisplayName();
    this.WQ = paramParticipant.getIconImageUri();
    this.WR = paramParticipant.getHiResImageUri();
    this.zzbtt = paramParticipant.getStatus();
    this.YJ = paramParticipant.zzbjm();
    this.aeK = paramParticipant.isConnectedToRoom();
    Object localObject = paramParticipant.getPlayer();
    if (localObject == null) {}
    for (localObject = null;; localObject = new PlayerEntity((Player)localObject))
    {
      this.Ye = ((PlayerEntity)localObject);
      this.mt = paramParticipant.getCapabilities();
      this.aeL = paramParticipant.getResult();
      this.Xb = paramParticipant.getIconImageUrl();
      this.Xc = paramParticipant.getHiResImageUrl();
      return;
    }
  }
  
  static int zza(Participant paramParticipant)
  {
    return zzz.hashCode(new Object[] { paramParticipant.getPlayer(), Integer.valueOf(paramParticipant.getStatus()), paramParticipant.zzbjm(), Boolean.valueOf(paramParticipant.isConnectedToRoom()), paramParticipant.getDisplayName(), paramParticipant.getIconImageUri(), paramParticipant.getHiResImageUri(), Integer.valueOf(paramParticipant.getCapabilities()), paramParticipant.getResult(), paramParticipant.getParticipantId() });
  }
  
  static boolean zza(Participant paramParticipant, Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if (!(paramObject instanceof Participant)) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramParticipant == paramObject);
      paramObject = (Participant)paramObject;
      if ((!zzz.equal(((Participant)paramObject).getPlayer(), paramParticipant.getPlayer())) || (!zzz.equal(Integer.valueOf(((Participant)paramObject).getStatus()), Integer.valueOf(paramParticipant.getStatus()))) || (!zzz.equal(((Participant)paramObject).zzbjm(), paramParticipant.zzbjm())) || (!zzz.equal(Boolean.valueOf(((Participant)paramObject).isConnectedToRoom()), Boolean.valueOf(paramParticipant.isConnectedToRoom()))) || (!zzz.equal(((Participant)paramObject).getDisplayName(), paramParticipant.getDisplayName())) || (!zzz.equal(((Participant)paramObject).getIconImageUri(), paramParticipant.getIconImageUri())) || (!zzz.equal(((Participant)paramObject).getHiResImageUri(), paramParticipant.getHiResImageUri())) || (!zzz.equal(Integer.valueOf(((Participant)paramObject).getCapabilities()), Integer.valueOf(paramParticipant.getCapabilities()))) || (!zzz.equal(((Participant)paramObject).getResult(), paramParticipant.getResult()))) {
        break;
      }
      bool1 = bool2;
    } while (zzz.equal(((Participant)paramObject).getParticipantId(), paramParticipant.getParticipantId()));
    return false;
  }
  
  static String zzb(Participant paramParticipant)
  {
    return zzz.zzx(paramParticipant).zzg("ParticipantId", paramParticipant.getParticipantId()).zzg("Player", paramParticipant.getPlayer()).zzg("Status", Integer.valueOf(paramParticipant.getStatus())).zzg("ClientAddress", paramParticipant.zzbjm()).zzg("ConnectedToRoom", Boolean.valueOf(paramParticipant.isConnectedToRoom())).zzg("DisplayName", paramParticipant.getDisplayName()).zzg("IconImage", paramParticipant.getIconImageUri()).zzg("IconImageUrl", paramParticipant.getIconImageUrl()).zzg("HiResImage", paramParticipant.getHiResImageUri()).zzg("HiResImageUrl", paramParticipant.getHiResImageUrl()).zzg("Capabilities", Integer.valueOf(paramParticipant.getCapabilities())).zzg("Result", paramParticipant.getResult()).toString();
  }
  
  public boolean equals(Object paramObject)
  {
    return zza(this, paramObject);
  }
  
  public Participant freeze()
  {
    return this;
  }
  
  public int getCapabilities()
  {
    return this.mt;
  }
  
  public String getDisplayName()
  {
    if (this.Ye == null) {
      return this.jh;
    }
    return this.Ye.getDisplayName();
  }
  
  public void getDisplayName(CharArrayBuffer paramCharArrayBuffer)
  {
    if (this.Ye == null)
    {
      zzg.zzb(this.jh, paramCharArrayBuffer);
      return;
    }
    this.Ye.getDisplayName(paramCharArrayBuffer);
  }
  
  public Uri getHiResImageUri()
  {
    if (this.Ye == null) {
      return this.WR;
    }
    return this.Ye.getHiResImageUri();
  }
  
  public String getHiResImageUrl()
  {
    if (this.Ye == null) {
      return this.Xc;
    }
    return this.Ye.getHiResImageUrl();
  }
  
  public Uri getIconImageUri()
  {
    if (this.Ye == null) {
      return this.WQ;
    }
    return this.Ye.getIconImageUri();
  }
  
  public String getIconImageUrl()
  {
    if (this.Ye == null) {
      return this.Xb;
    }
    return this.Ye.getIconImageUrl();
  }
  
  public String getParticipantId()
  {
    return this.ZU;
  }
  
  public Player getPlayer()
  {
    return this.Ye;
  }
  
  public ParticipantResult getResult()
  {
    return this.aeL;
  }
  
  public int getStatus()
  {
    return this.zzbtt;
  }
  
  public int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    return zza(this);
  }
  
  public boolean isConnectedToRoom()
  {
    return this.aeK;
  }
  
  public boolean isDataValid()
  {
    return true;
  }
  
  public String toString()
  {
    return zzb(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    Object localObject2 = null;
    int j = 0;
    if (!zzawa())
    {
      ParticipantEntityCreator.zza(this, paramParcel, paramInt);
      return;
    }
    paramParcel.writeString(this.ZU);
    paramParcel.writeString(this.jh);
    Object localObject1;
    if (this.WQ == null)
    {
      localObject1 = null;
      label46:
      paramParcel.writeString((String)localObject1);
      if (this.WR != null) {
        break label143;
      }
      localObject1 = localObject2;
      label63:
      paramParcel.writeString((String)localObject1);
      paramParcel.writeInt(this.zzbtt);
      paramParcel.writeString(this.YJ);
      if (!this.aeK) {
        break label155;
      }
      i = 1;
      label94:
      paramParcel.writeInt(i);
      if (this.Ye != null) {
        break label160;
      }
    }
    label143:
    label155:
    label160:
    for (int i = j;; i = 1)
    {
      paramParcel.writeInt(i);
      if (this.Ye == null) {
        break;
      }
      this.Ye.writeToParcel(paramParcel, paramInt);
      return;
      localObject1 = this.WQ.toString();
      break label46;
      localObject1 = this.WR.toString();
      break label63;
      i = 0;
      break label94;
    }
  }
  
  public String zzbjm()
  {
    return this.YJ;
  }
  
  static final class ParticipantEntityCreatorCompat
    extends ParticipantEntityCreator
  {
    public ParticipantEntity zzmg(Parcel paramParcel)
    {
      int i = 1;
      if ((ParticipantEntity.zze(ParticipantEntity.zzbho())) || (ParticipantEntity.zzjv(ParticipantEntity.class.getCanonicalName()))) {
        return super.zzmg(paramParcel);
      }
      String str1 = paramParcel.readString();
      String str2 = paramParcel.readString();
      Object localObject1 = paramParcel.readString();
      Object localObject2;
      label68:
      int j;
      String str3;
      boolean bool;
      if (localObject1 == null)
      {
        localObject1 = null;
        localObject2 = paramParcel.readString();
        if (localObject2 != null) {
          break label151;
        }
        localObject2 = null;
        j = paramParcel.readInt();
        str3 = paramParcel.readString();
        if (paramParcel.readInt() <= 0) {
          break label161;
        }
        bool = true;
        label89:
        if (paramParcel.readInt() <= 0) {
          break label167;
        }
        label96:
        if (i == 0) {
          break label172;
        }
      }
      label151:
      label161:
      label167:
      label172:
      for (paramParcel = (PlayerEntity)PlayerEntity.CREATOR.createFromParcel(paramParcel);; paramParcel = null)
      {
        return new ParticipantEntity(3, str1, str2, (Uri)localObject1, (Uri)localObject2, j, str3, bool, paramParcel, 7, null, null, null);
        localObject1 = Uri.parse((String)localObject1);
        break;
        localObject2 = Uri.parse((String)localObject2);
        break label68;
        bool = false;
        break label89;
        i = 0;
        break label96;
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/multiplayer/ParticipantEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */