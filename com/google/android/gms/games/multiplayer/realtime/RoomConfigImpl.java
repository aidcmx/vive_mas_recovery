package com.google.android.gms.games.multiplayer.realtime;

import android.os.Bundle;
import com.google.android.gms.common.internal.zzaa;
import java.util.ArrayList;

public final class RoomConfigImpl
  extends RoomConfig
{
  private final int aeH;
  private final RoomUpdateListener aeS;
  private final RoomStatusUpdateListener aeT;
  private final RealTimeMessageReceivedListener aeU;
  private final Bundle aeX;
  private final String[] aeY;
  private final String hm;
  
  RoomConfigImpl(RoomConfig.Builder paramBuilder)
  {
    this.aeS = paramBuilder.aeS;
    this.aeT = paramBuilder.aeT;
    this.aeU = paramBuilder.aeU;
    this.hm = paramBuilder.aeV;
    this.aeH = paramBuilder.aeH;
    this.aeX = paramBuilder.aeX;
    int i = paramBuilder.aeW.size();
    this.aeY = ((String[])paramBuilder.aeW.toArray(new String[i]));
    zzaa.zzb(this.aeU, "Must specify a message listener");
  }
  
  public Bundle getAutoMatchCriteria()
  {
    return this.aeX;
  }
  
  public String getInvitationId()
  {
    return this.hm;
  }
  
  public String[] getInvitedPlayerIds()
  {
    return this.aeY;
  }
  
  public RealTimeMessageReceivedListener getMessageReceivedListener()
  {
    return this.aeU;
  }
  
  public RoomStatusUpdateListener getRoomStatusUpdateListener()
  {
    return this.aeT;
  }
  
  public RoomUpdateListener getRoomUpdateListener()
  {
    return this.aeS;
  }
  
  public int getVariant()
  {
    return this.aeH;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/multiplayer/realtime/RoomConfigImpl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */