package com.google.android.gms.games.multiplayer.realtime;

public abstract interface WaitingRoomListener
  extends RoomStatusUpdateListener, RoomUpdateListener
{}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/multiplayer/realtime/WaitingRoomListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */