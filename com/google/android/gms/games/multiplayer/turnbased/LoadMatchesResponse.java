package com.google.android.gms.games.multiplayer.turnbased;

import android.os.Bundle;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.games.internal.constants.TurnBasedMatchTurnStatus;
import com.google.android.gms.games.multiplayer.InvitationBuffer;

public final class LoadMatchesResponse
{
  private final InvitationBuffer afc;
  private final TurnBasedMatchBuffer afd;
  private final TurnBasedMatchBuffer afe;
  private final TurnBasedMatchBuffer aff;
  
  public LoadMatchesResponse(Bundle paramBundle)
  {
    DataHolder localDataHolder = zzc(paramBundle, 0);
    if (localDataHolder != null)
    {
      this.afc = new InvitationBuffer(localDataHolder);
      localDataHolder = zzc(paramBundle, 1);
      if (localDataHolder == null) {
        break label101;
      }
      this.afd = new TurnBasedMatchBuffer(localDataHolder);
      label48:
      localDataHolder = zzc(paramBundle, 2);
      if (localDataHolder == null) {
        break label109;
      }
    }
    label101:
    label109:
    for (this.afe = new TurnBasedMatchBuffer(localDataHolder);; this.afe = null)
    {
      paramBundle = zzc(paramBundle, 3);
      if (paramBundle == null) {
        break label117;
      }
      this.aff = new TurnBasedMatchBuffer(paramBundle);
      return;
      this.afc = null;
      break;
      this.afd = null;
      break label48;
    }
    label117:
    this.aff = null;
  }
  
  private static DataHolder zzc(Bundle paramBundle, int paramInt)
  {
    String str = TurnBasedMatchTurnStatus.zzrw(paramInt);
    if (!paramBundle.containsKey(str)) {
      return null;
    }
    return (DataHolder)paramBundle.getParcelable(str);
  }
  
  @Deprecated
  public void close()
  {
    release();
  }
  
  public TurnBasedMatchBuffer getCompletedMatches()
  {
    return this.aff;
  }
  
  public InvitationBuffer getInvitations()
  {
    return this.afc;
  }
  
  public TurnBasedMatchBuffer getMyTurnMatches()
  {
    return this.afd;
  }
  
  public TurnBasedMatchBuffer getTheirTurnMatches()
  {
    return this.afe;
  }
  
  public boolean hasData()
  {
    if ((this.afc != null) && (this.afc.getCount() > 0)) {}
    while (((this.afd != null) && (this.afd.getCount() > 0)) || ((this.afe != null) && (this.afe.getCount() > 0)) || ((this.aff != null) && (this.aff.getCount() > 0))) {
      return true;
    }
    return false;
  }
  
  public void release()
  {
    if (this.afc != null) {
      this.afc.release();
    }
    if (this.afd != null) {
      this.afd.release();
    }
    if (this.afe != null) {
      this.afe.release();
    }
    if (this.aff != null) {
      this.aff.release();
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/multiplayer/turnbased/LoadMatchesResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */