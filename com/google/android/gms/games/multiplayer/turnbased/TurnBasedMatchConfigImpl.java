package com.google.android.gms.games.multiplayer.turnbased;

import android.os.Bundle;
import java.util.ArrayList;

public final class TurnBasedMatchConfigImpl
  extends TurnBasedMatchConfig
{
  private final int aeH;
  private final Bundle aeX;
  private final String[] aeY;
  private final int afg;
  
  TurnBasedMatchConfigImpl(TurnBasedMatchConfig.Builder paramBuilder)
  {
    this.aeH = paramBuilder.aeH;
    this.afg = paramBuilder.afg;
    this.aeX = paramBuilder.aeX;
    int i = paramBuilder.aeW.size();
    this.aeY = ((String[])paramBuilder.aeW.toArray(new String[i]));
  }
  
  public Bundle getAutoMatchCriteria()
  {
    return this.aeX;
  }
  
  public String[] getInvitedPlayerIds()
  {
    return this.aeY;
  }
  
  public int getVariant()
  {
    return this.aeH;
  }
  
  public int zzbmt()
  {
    return this.afg;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/multiplayer/turnbased/TurnBasedMatchConfigImpl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */