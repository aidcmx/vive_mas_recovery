package com.google.android.gms.games.quest;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;

public final class MilestoneEntity
  extends AbstractSafeParcelable
  implements Milestone
{
  public static final Parcelable.Creator<MilestoneEntity> CREATOR = new MilestoneEntityCreator();
  private final String YH;
  private final String ZX;
  private final long afr;
  private final long afs;
  private final byte[] aft;
  private final int mState;
  private final int mVersionCode;
  
  MilestoneEntity(int paramInt1, String paramString1, long paramLong1, long paramLong2, byte[] paramArrayOfByte, int paramInt2, String paramString2)
  {
    this.mVersionCode = paramInt1;
    this.ZX = paramString1;
    this.afr = paramLong1;
    this.afs = paramLong2;
    this.aft = paramArrayOfByte;
    this.mState = paramInt2;
    this.YH = paramString2;
  }
  
  public MilestoneEntity(Milestone paramMilestone)
  {
    this.mVersionCode = 4;
    this.ZX = paramMilestone.getMilestoneId();
    this.afr = paramMilestone.getCurrentProgress();
    this.afs = paramMilestone.getTargetProgress();
    this.mState = paramMilestone.getState();
    this.YH = paramMilestone.getEventId();
    paramMilestone = paramMilestone.getCompletionRewardData();
    if (paramMilestone == null)
    {
      this.aft = null;
      return;
    }
    this.aft = new byte[paramMilestone.length];
    System.arraycopy(paramMilestone, 0, this.aft, 0, paramMilestone.length);
  }
  
  static int zza(Milestone paramMilestone)
  {
    return zzz.hashCode(new Object[] { paramMilestone.getMilestoneId(), Long.valueOf(paramMilestone.getCurrentProgress()), Long.valueOf(paramMilestone.getTargetProgress()), Integer.valueOf(paramMilestone.getState()), paramMilestone.getEventId() });
  }
  
  static boolean zza(Milestone paramMilestone, Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if (!(paramObject instanceof Milestone)) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramMilestone == paramObject);
      paramObject = (Milestone)paramObject;
      if ((!zzz.equal(((Milestone)paramObject).getMilestoneId(), paramMilestone.getMilestoneId())) || (!zzz.equal(Long.valueOf(((Milestone)paramObject).getCurrentProgress()), Long.valueOf(paramMilestone.getCurrentProgress()))) || (!zzz.equal(Long.valueOf(((Milestone)paramObject).getTargetProgress()), Long.valueOf(paramMilestone.getTargetProgress()))) || (!zzz.equal(Integer.valueOf(((Milestone)paramObject).getState()), Integer.valueOf(paramMilestone.getState())))) {
        break;
      }
      bool1 = bool2;
    } while (zzz.equal(((Milestone)paramObject).getEventId(), paramMilestone.getEventId()));
    return false;
  }
  
  static String zzb(Milestone paramMilestone)
  {
    return zzz.zzx(paramMilestone).zzg("MilestoneId", paramMilestone.getMilestoneId()).zzg("CurrentProgress", Long.valueOf(paramMilestone.getCurrentProgress())).zzg("TargetProgress", Long.valueOf(paramMilestone.getTargetProgress())).zzg("State", Integer.valueOf(paramMilestone.getState())).zzg("CompletionRewardData", paramMilestone.getCompletionRewardData()).zzg("EventId", paramMilestone.getEventId()).toString();
  }
  
  public boolean equals(Object paramObject)
  {
    return zza(this, paramObject);
  }
  
  public Milestone freeze()
  {
    return this;
  }
  
  public byte[] getCompletionRewardData()
  {
    return this.aft;
  }
  
  public long getCurrentProgress()
  {
    return this.afr;
  }
  
  public String getEventId()
  {
    return this.YH;
  }
  
  public String getMilestoneId()
  {
    return this.ZX;
  }
  
  public int getState()
  {
    return this.mState;
  }
  
  public long getTargetProgress()
  {
    return this.afs;
  }
  
  public int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    return zza(this);
  }
  
  public boolean isDataValid()
  {
    return true;
  }
  
  public String toString()
  {
    return zzb(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    MilestoneEntityCreator.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/quest/MilestoneEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */