package com.google.android.gms.games.quest;

import android.database.CharArrayBuffer;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;
import com.google.android.gms.common.util.zzg;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.GameEntity;
import java.util.ArrayList;
import java.util.List;

public final class QuestEntity
  extends AbstractSafeParcelable
  implements Quest
{
  public static final Parcelable.Creator<QuestEntity> CREATOR = new QuestEntityCreator();
  private final long Yh;
  private final GameEntity acT;
  private final long afA;
  private final long afB;
  private final ArrayList<MilestoneEntity> afC;
  private final String afu;
  private final long afv;
  private final Uri afw;
  private final String afx;
  private final long afy;
  private final String afz;
  private final String cg;
  private final Uri mIconUri;
  private final String mName;
  private final int mState;
  private final int mVersionCode;
  private final int nV;
  
  QuestEntity(int paramInt1, GameEntity paramGameEntity, String paramString1, long paramLong1, Uri paramUri1, String paramString2, String paramString3, long paramLong2, long paramLong3, Uri paramUri2, String paramString4, String paramString5, long paramLong4, long paramLong5, int paramInt2, int paramInt3, ArrayList<MilestoneEntity> paramArrayList)
  {
    this.mVersionCode = paramInt1;
    this.acT = paramGameEntity;
    this.afu = paramString1;
    this.afv = paramLong1;
    this.afw = paramUri1;
    this.afx = paramString2;
    this.cg = paramString3;
    this.afy = paramLong2;
    this.Yh = paramLong3;
    this.mIconUri = paramUri2;
    this.afz = paramString4;
    this.mName = paramString5;
    this.afA = paramLong4;
    this.afB = paramLong5;
    this.mState = paramInt2;
    this.nV = paramInt3;
    this.afC = paramArrayList;
  }
  
  public QuestEntity(Quest paramQuest)
  {
    this.mVersionCode = 2;
    this.acT = new GameEntity(paramQuest.getGame());
    this.afu = paramQuest.getQuestId();
    this.afv = paramQuest.getAcceptedTimestamp();
    this.cg = paramQuest.getDescription();
    this.afw = paramQuest.getBannerImageUri();
    this.afx = paramQuest.getBannerImageUrl();
    this.afy = paramQuest.getEndTimestamp();
    this.mIconUri = paramQuest.getIconImageUri();
    this.afz = paramQuest.getIconImageUrl();
    this.Yh = paramQuest.getLastUpdatedTimestamp();
    this.mName = paramQuest.getName();
    this.afA = paramQuest.zzbmw();
    this.afB = paramQuest.getStartTimestamp();
    this.mState = paramQuest.getState();
    this.nV = paramQuest.getType();
    paramQuest = paramQuest.zzbmv();
    int j = paramQuest.size();
    this.afC = new ArrayList(j);
    int i = 0;
    while (i < j)
    {
      this.afC.add((MilestoneEntity)((Milestone)paramQuest.get(i)).freeze());
      i += 1;
    }
  }
  
  static int zza(Quest paramQuest)
  {
    return zzz.hashCode(new Object[] { paramQuest.getGame(), paramQuest.getQuestId(), Long.valueOf(paramQuest.getAcceptedTimestamp()), paramQuest.getBannerImageUri(), paramQuest.getDescription(), Long.valueOf(paramQuest.getEndTimestamp()), paramQuest.getIconImageUri(), Long.valueOf(paramQuest.getLastUpdatedTimestamp()), paramQuest.zzbmv(), paramQuest.getName(), Long.valueOf(paramQuest.zzbmw()), Long.valueOf(paramQuest.getStartTimestamp()), Integer.valueOf(paramQuest.getState()) });
  }
  
  static boolean zza(Quest paramQuest, Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if (!(paramObject instanceof Quest)) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramQuest == paramObject);
      paramObject = (Quest)paramObject;
      if ((!zzz.equal(((Quest)paramObject).getGame(), paramQuest.getGame())) || (!zzz.equal(((Quest)paramObject).getQuestId(), paramQuest.getQuestId())) || (!zzz.equal(Long.valueOf(((Quest)paramObject).getAcceptedTimestamp()), Long.valueOf(paramQuest.getAcceptedTimestamp()))) || (!zzz.equal(((Quest)paramObject).getBannerImageUri(), paramQuest.getBannerImageUri())) || (!zzz.equal(((Quest)paramObject).getDescription(), paramQuest.getDescription())) || (!zzz.equal(Long.valueOf(((Quest)paramObject).getEndTimestamp()), Long.valueOf(paramQuest.getEndTimestamp()))) || (!zzz.equal(((Quest)paramObject).getIconImageUri(), paramQuest.getIconImageUri())) || (!zzz.equal(Long.valueOf(((Quest)paramObject).getLastUpdatedTimestamp()), Long.valueOf(paramQuest.getLastUpdatedTimestamp()))) || (!zzz.equal(((Quest)paramObject).zzbmv(), paramQuest.zzbmv())) || (!zzz.equal(((Quest)paramObject).getName(), paramQuest.getName())) || (!zzz.equal(Long.valueOf(((Quest)paramObject).zzbmw()), Long.valueOf(paramQuest.zzbmw()))) || (!zzz.equal(Long.valueOf(((Quest)paramObject).getStartTimestamp()), Long.valueOf(paramQuest.getStartTimestamp())))) {
        break;
      }
      bool1 = bool2;
    } while (zzz.equal(Integer.valueOf(((Quest)paramObject).getState()), Integer.valueOf(paramQuest.getState())));
    return false;
  }
  
  static String zzb(Quest paramQuest)
  {
    return zzz.zzx(paramQuest).zzg("Game", paramQuest.getGame()).zzg("QuestId", paramQuest.getQuestId()).zzg("AcceptedTimestamp", Long.valueOf(paramQuest.getAcceptedTimestamp())).zzg("BannerImageUri", paramQuest.getBannerImageUri()).zzg("BannerImageUrl", paramQuest.getBannerImageUrl()).zzg("Description", paramQuest.getDescription()).zzg("EndTimestamp", Long.valueOf(paramQuest.getEndTimestamp())).zzg("IconImageUri", paramQuest.getIconImageUri()).zzg("IconImageUrl", paramQuest.getIconImageUrl()).zzg("LastUpdatedTimestamp", Long.valueOf(paramQuest.getLastUpdatedTimestamp())).zzg("Milestones", paramQuest.zzbmv()).zzg("Name", paramQuest.getName()).zzg("NotifyTimestamp", Long.valueOf(paramQuest.zzbmw())).zzg("StartTimestamp", Long.valueOf(paramQuest.getStartTimestamp())).zzg("State", Integer.valueOf(paramQuest.getState())).toString();
  }
  
  public boolean equals(Object paramObject)
  {
    return zza(this, paramObject);
  }
  
  public Quest freeze()
  {
    return this;
  }
  
  public long getAcceptedTimestamp()
  {
    return this.afv;
  }
  
  public Uri getBannerImageUri()
  {
    return this.afw;
  }
  
  public String getBannerImageUrl()
  {
    return this.afx;
  }
  
  public Milestone getCurrentMilestone()
  {
    return (Milestone)zzbmv().get(0);
  }
  
  public String getDescription()
  {
    return this.cg;
  }
  
  public void getDescription(CharArrayBuffer paramCharArrayBuffer)
  {
    zzg.zzb(this.cg, paramCharArrayBuffer);
  }
  
  public long getEndTimestamp()
  {
    return this.afy;
  }
  
  public Game getGame()
  {
    return this.acT;
  }
  
  public Uri getIconImageUri()
  {
    return this.mIconUri;
  }
  
  public String getIconImageUrl()
  {
    return this.afz;
  }
  
  public long getLastUpdatedTimestamp()
  {
    return this.Yh;
  }
  
  public String getName()
  {
    return this.mName;
  }
  
  public void getName(CharArrayBuffer paramCharArrayBuffer)
  {
    zzg.zzb(this.mName, paramCharArrayBuffer);
  }
  
  public String getQuestId()
  {
    return this.afu;
  }
  
  public long getStartTimestamp()
  {
    return this.afB;
  }
  
  public int getState()
  {
    return this.mState;
  }
  
  public int getType()
  {
    return this.nV;
  }
  
  public int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    return zza(this);
  }
  
  public boolean isDataValid()
  {
    return true;
  }
  
  public boolean isEndingSoon()
  {
    return this.afA <= System.currentTimeMillis() + 1800000L;
  }
  
  public String toString()
  {
    return zzb(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    QuestEntityCreator.zza(this, paramParcel, paramInt);
  }
  
  public List<Milestone> zzbmv()
  {
    return new ArrayList(this.afC);
  }
  
  public long zzbmw()
  {
    return this.afA;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/quest/QuestEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */