package com.google.android.gms.games.quest;

public abstract interface QuestUpdateListener
{
  public abstract void onQuestCompleted(Quest paramQuest);
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/quest/QuestUpdateListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */