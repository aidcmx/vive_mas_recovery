package com.google.android.gms.games.request;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.GameEntity;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerEntity;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class GameRequestEntity
  extends AbstractSafeParcelable
  implements GameRequest
{
  public static final Parcelable.Creator<GameRequestEntity> CREATOR = new GameRequestEntityCreator();
  private final GameEntity acT;
  private final long aeD;
  private final PlayerEntity afD;
  private final ArrayList<PlayerEntity> afE;
  private final long afF;
  private final Bundle afG;
  private final byte[] afk;
  private final int mVersionCode;
  private final int nV;
  private final int zzbtt;
  private final String zzcec;
  
  GameRequestEntity(int paramInt1, GameEntity paramGameEntity, PlayerEntity paramPlayerEntity, byte[] paramArrayOfByte, String paramString, ArrayList<PlayerEntity> paramArrayList, int paramInt2, long paramLong1, long paramLong2, Bundle paramBundle, int paramInt3)
  {
    this.mVersionCode = paramInt1;
    this.acT = paramGameEntity;
    this.afD = paramPlayerEntity;
    this.afk = paramArrayOfByte;
    this.zzcec = paramString;
    this.afE = paramArrayList;
    this.nV = paramInt2;
    this.aeD = paramLong1;
    this.afF = paramLong2;
    this.afG = paramBundle;
    this.zzbtt = paramInt3;
  }
  
  public GameRequestEntity(GameRequest paramGameRequest)
  {
    this.mVersionCode = 2;
    this.acT = new GameEntity(paramGameRequest.getGame());
    this.afD = new PlayerEntity(paramGameRequest.getSender());
    this.zzcec = paramGameRequest.getRequestId();
    this.nV = paramGameRequest.getType();
    this.aeD = paramGameRequest.getCreationTimestamp();
    this.afF = paramGameRequest.getExpirationTimestamp();
    this.zzbtt = paramGameRequest.getStatus();
    Object localObject = paramGameRequest.getData();
    if (localObject == null) {
      this.afk = null;
    }
    for (;;)
    {
      localObject = paramGameRequest.getRecipients();
      int j = ((List)localObject).size();
      this.afE = new ArrayList(j);
      this.afG = new Bundle();
      int i = 0;
      while (i < j)
      {
        Player localPlayer = (Player)((Player)((List)localObject).get(i)).freeze();
        String str = localPlayer.getPlayerId();
        this.afE.add((PlayerEntity)localPlayer);
        this.afG.putInt(str, paramGameRequest.getRecipientStatus(str));
        i += 1;
      }
      this.afk = new byte[localObject.length];
      System.arraycopy(localObject, 0, this.afk, 0, localObject.length);
    }
  }
  
  static int zza(GameRequest paramGameRequest)
  {
    return zzz.hashCode(new Object[] { paramGameRequest.getGame(), paramGameRequest.getRecipients(), paramGameRequest.getRequestId(), paramGameRequest.getSender(), zzb(paramGameRequest), Integer.valueOf(paramGameRequest.getType()), Long.valueOf(paramGameRequest.getCreationTimestamp()), Long.valueOf(paramGameRequest.getExpirationTimestamp()) });
  }
  
  static boolean zza(GameRequest paramGameRequest, Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if (!(paramObject instanceof GameRequest)) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramGameRequest == paramObject);
      paramObject = (GameRequest)paramObject;
      if ((!zzz.equal(((GameRequest)paramObject).getGame(), paramGameRequest.getGame())) || (!zzz.equal(((GameRequest)paramObject).getRecipients(), paramGameRequest.getRecipients())) || (!zzz.equal(((GameRequest)paramObject).getRequestId(), paramGameRequest.getRequestId())) || (!zzz.equal(((GameRequest)paramObject).getSender(), paramGameRequest.getSender())) || (!Arrays.equals(zzb((GameRequest)paramObject), zzb(paramGameRequest))) || (!zzz.equal(Integer.valueOf(((GameRequest)paramObject).getType()), Integer.valueOf(paramGameRequest.getType()))) || (!zzz.equal(Long.valueOf(((GameRequest)paramObject).getCreationTimestamp()), Long.valueOf(paramGameRequest.getCreationTimestamp())))) {
        break;
      }
      bool1 = bool2;
    } while (zzz.equal(Long.valueOf(((GameRequest)paramObject).getExpirationTimestamp()), Long.valueOf(paramGameRequest.getExpirationTimestamp())));
    return false;
  }
  
  private static int[] zzb(GameRequest paramGameRequest)
  {
    List localList = paramGameRequest.getRecipients();
    int j = localList.size();
    int[] arrayOfInt = new int[j];
    int i = 0;
    while (i < j)
    {
      arrayOfInt[i] = paramGameRequest.getRecipientStatus(((Player)localList.get(i)).getPlayerId());
      i += 1;
    }
    return arrayOfInt;
  }
  
  static String zzc(GameRequest paramGameRequest)
  {
    return zzz.zzx(paramGameRequest).zzg("Game", paramGameRequest.getGame()).zzg("Sender", paramGameRequest.getSender()).zzg("Recipients", paramGameRequest.getRecipients()).zzg("Data", paramGameRequest.getData()).zzg("RequestId", paramGameRequest.getRequestId()).zzg("Type", Integer.valueOf(paramGameRequest.getType())).zzg("CreationTimestamp", Long.valueOf(paramGameRequest.getCreationTimestamp())).zzg("ExpirationTimestamp", Long.valueOf(paramGameRequest.getExpirationTimestamp())).toString();
  }
  
  public boolean equals(Object paramObject)
  {
    return zza(this, paramObject);
  }
  
  public GameRequest freeze()
  {
    return this;
  }
  
  public long getCreationTimestamp()
  {
    return this.aeD;
  }
  
  public byte[] getData()
  {
    return this.afk;
  }
  
  public long getExpirationTimestamp()
  {
    return this.afF;
  }
  
  public Game getGame()
  {
    return this.acT;
  }
  
  public int getRecipientStatus(String paramString)
  {
    return this.afG.getInt(paramString, 0);
  }
  
  public List<Player> getRecipients()
  {
    return new ArrayList(this.afE);
  }
  
  public String getRequestId()
  {
    return this.zzcec;
  }
  
  public Player getSender()
  {
    return this.afD;
  }
  
  public int getStatus()
  {
    return this.zzbtt;
  }
  
  public int getType()
  {
    return this.nV;
  }
  
  public int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    return zza(this);
  }
  
  public boolean isConsumed(String paramString)
  {
    return getRecipientStatus(paramString) == 1;
  }
  
  public boolean isDataValid()
  {
    return true;
  }
  
  public String toString()
  {
    return zzc(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    GameRequestEntityCreator.zza(this, paramParcel, paramInt);
  }
  
  public Bundle zzbmx()
  {
    return this.afG;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/request/GameRequestEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */