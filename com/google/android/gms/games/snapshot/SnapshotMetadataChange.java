package com.google.android.gms.games.snapshot;

import android.graphics.Bitmap;
import android.net.Uri;
import com.google.android.gms.common.data.BitmapTeleporter;

public abstract interface SnapshotMetadataChange
{
  public static final SnapshotMetadataChange EMPTY_CHANGE = new SnapshotMetadataChangeEntity();
  
  public abstract Bitmap getCoverImage();
  
  public abstract String getDescription();
  
  public abstract Long getPlayedTimeMillis();
  
  public abstract Long getProgressValue();
  
  public abstract BitmapTeleporter zzbmy();
  
  public static final class Builder
  {
    private Long afK;
    private Long afL;
    private BitmapTeleporter afM;
    private Uri afN;
    private String cg;
    
    public SnapshotMetadataChange build()
    {
      return new SnapshotMetadataChangeEntity(this.cg, this.afK, this.afM, this.afN, this.afL);
    }
    
    public Builder fromMetadata(SnapshotMetadata paramSnapshotMetadata)
    {
      this.cg = paramSnapshotMetadata.getDescription();
      this.afK = Long.valueOf(paramSnapshotMetadata.getPlayedTime());
      this.afL = Long.valueOf(paramSnapshotMetadata.getProgressValue());
      if (this.afK.longValue() == -1L) {
        this.afK = null;
      }
      this.afN = paramSnapshotMetadata.getCoverImageUri();
      if (this.afN != null) {
        this.afM = null;
      }
      return this;
    }
    
    public Builder setCoverImage(Bitmap paramBitmap)
    {
      this.afM = new BitmapTeleporter(paramBitmap);
      this.afN = null;
      return this;
    }
    
    public Builder setDescription(String paramString)
    {
      this.cg = paramString;
      return this;
    }
    
    public Builder setPlayedTimeMillis(long paramLong)
    {
      this.afK = Long.valueOf(paramLong);
      return this;
    }
    
    public Builder setProgressValue(long paramLong)
    {
      this.afL = Long.valueOf(paramLong);
      return this;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/snapshot/SnapshotMetadataChange.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */