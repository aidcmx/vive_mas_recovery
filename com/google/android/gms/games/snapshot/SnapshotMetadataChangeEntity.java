package com.google.android.gms.games.snapshot;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.data.BitmapTeleporter;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;

public final class SnapshotMetadataChangeEntity
  extends AbstractSafeParcelable
  implements SnapshotMetadataChange
{
  public static final Parcelable.Creator<SnapshotMetadataChangeEntity> CREATOR = new SnapshotMetadataChangeCreator();
  private final Long afL;
  private final Uri afN;
  private final Long afO;
  private BitmapTeleporter afP;
  private final String cg;
  private final int mVersionCode;
  
  SnapshotMetadataChangeEntity()
  {
    this(5, null, null, null, null, null);
  }
  
  SnapshotMetadataChangeEntity(int paramInt, String paramString, Long paramLong1, BitmapTeleporter paramBitmapTeleporter, Uri paramUri, Long paramLong2)
  {
    this.mVersionCode = paramInt;
    this.cg = paramString;
    this.afO = paramLong1;
    this.afP = paramBitmapTeleporter;
    this.afN = paramUri;
    this.afL = paramLong2;
    if (this.afP != null) {
      if (this.afN == null) {
        zzaa.zza(bool1, "Cannot set both a URI and an image");
      }
    }
    while (this.afN == null) {
      for (;;)
      {
        return;
        bool1 = false;
      }
    }
    if (this.afP == null) {}
    for (bool1 = bool2;; bool1 = false)
    {
      zzaa.zza(bool1, "Cannot set both a URI and an image");
      return;
    }
  }
  
  SnapshotMetadataChangeEntity(String paramString, Long paramLong1, BitmapTeleporter paramBitmapTeleporter, Uri paramUri, Long paramLong2)
  {
    this(5, paramString, paramLong1, paramBitmapTeleporter, paramUri, paramLong2);
  }
  
  public Bitmap getCoverImage()
  {
    if (this.afP == null) {
      return null;
    }
    return this.afP.zzauj();
  }
  
  public Uri getCoverImageUri()
  {
    return this.afN;
  }
  
  public String getDescription()
  {
    return this.cg;
  }
  
  public Long getPlayedTimeMillis()
  {
    return this.afO;
  }
  
  public Long getProgressValue()
  {
    return this.afL;
  }
  
  public int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    SnapshotMetadataChangeCreator.zza(this, paramParcel, paramInt);
  }
  
  public BitmapTeleporter zzbmy()
  {
    return this.afP;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/snapshot/SnapshotMetadataChangeEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */