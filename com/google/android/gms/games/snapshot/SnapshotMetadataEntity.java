package com.google.android.gms.games.snapshot;

import android.database.CharArrayBuffer;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;
import com.google.android.gms.common.util.zzg;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.GameEntity;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerEntity;

public final class SnapshotMetadataEntity
  extends AbstractSafeParcelable
  implements SnapshotMetadata
{
  public static final Parcelable.Creator<SnapshotMetadataEntity> CREATOR = new SnapshotMetadataEntityCreator();
  private final String JB;
  private final String Zj;
  private final GameEntity acT;
  private final Uri afN;
  private final PlayerEntity afQ;
  private final String afR;
  private final long afS;
  private final long afT;
  private final float afU;
  private final String afV;
  private final boolean afW;
  private final long afX;
  private final String afY;
  private final String cg;
  private final int mVersionCode;
  
  SnapshotMetadataEntity(int paramInt, GameEntity paramGameEntity, PlayerEntity paramPlayerEntity, String paramString1, Uri paramUri, String paramString2, String paramString3, String paramString4, long paramLong1, long paramLong2, float paramFloat, String paramString5, boolean paramBoolean, long paramLong3, String paramString6)
  {
    this.mVersionCode = paramInt;
    this.acT = paramGameEntity;
    this.afQ = paramPlayerEntity;
    this.Zj = paramString1;
    this.afN = paramUri;
    this.afR = paramString2;
    this.afU = paramFloat;
    this.JB = paramString3;
    this.cg = paramString4;
    this.afS = paramLong1;
    this.afT = paramLong2;
    this.afV = paramString5;
    this.afW = paramBoolean;
    this.afX = paramLong3;
    this.afY = paramString6;
  }
  
  public SnapshotMetadataEntity(SnapshotMetadata paramSnapshotMetadata)
  {
    this.mVersionCode = 6;
    this.acT = new GameEntity(paramSnapshotMetadata.getGame());
    this.afQ = new PlayerEntity(paramSnapshotMetadata.getOwner());
    this.Zj = paramSnapshotMetadata.getSnapshotId();
    this.afN = paramSnapshotMetadata.getCoverImageUri();
    this.afR = paramSnapshotMetadata.getCoverImageUrl();
    this.afU = paramSnapshotMetadata.getCoverImageAspectRatio();
    this.JB = paramSnapshotMetadata.getTitle();
    this.cg = paramSnapshotMetadata.getDescription();
    this.afS = paramSnapshotMetadata.getLastModifiedTimestamp();
    this.afT = paramSnapshotMetadata.getPlayedTime();
    this.afV = paramSnapshotMetadata.getUniqueName();
    this.afW = paramSnapshotMetadata.hasChangePending();
    this.afX = paramSnapshotMetadata.getProgressValue();
    this.afY = paramSnapshotMetadata.getDeviceName();
  }
  
  static int zza(SnapshotMetadata paramSnapshotMetadata)
  {
    return zzz.hashCode(new Object[] { paramSnapshotMetadata.getGame(), paramSnapshotMetadata.getOwner(), paramSnapshotMetadata.getSnapshotId(), paramSnapshotMetadata.getCoverImageUri(), Float.valueOf(paramSnapshotMetadata.getCoverImageAspectRatio()), paramSnapshotMetadata.getTitle(), paramSnapshotMetadata.getDescription(), Long.valueOf(paramSnapshotMetadata.getLastModifiedTimestamp()), Long.valueOf(paramSnapshotMetadata.getPlayedTime()), paramSnapshotMetadata.getUniqueName(), Boolean.valueOf(paramSnapshotMetadata.hasChangePending()), Long.valueOf(paramSnapshotMetadata.getProgressValue()), paramSnapshotMetadata.getDeviceName() });
  }
  
  static boolean zza(SnapshotMetadata paramSnapshotMetadata, Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if (!(paramObject instanceof SnapshotMetadata)) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramSnapshotMetadata == paramObject);
      paramObject = (SnapshotMetadata)paramObject;
      if ((!zzz.equal(((SnapshotMetadata)paramObject).getGame(), paramSnapshotMetadata.getGame())) || (!zzz.equal(((SnapshotMetadata)paramObject).getOwner(), paramSnapshotMetadata.getOwner())) || (!zzz.equal(((SnapshotMetadata)paramObject).getSnapshotId(), paramSnapshotMetadata.getSnapshotId())) || (!zzz.equal(((SnapshotMetadata)paramObject).getCoverImageUri(), paramSnapshotMetadata.getCoverImageUri())) || (!zzz.equal(Float.valueOf(((SnapshotMetadata)paramObject).getCoverImageAspectRatio()), Float.valueOf(paramSnapshotMetadata.getCoverImageAspectRatio()))) || (!zzz.equal(((SnapshotMetadata)paramObject).getTitle(), paramSnapshotMetadata.getTitle())) || (!zzz.equal(((SnapshotMetadata)paramObject).getDescription(), paramSnapshotMetadata.getDescription())) || (!zzz.equal(Long.valueOf(((SnapshotMetadata)paramObject).getLastModifiedTimestamp()), Long.valueOf(paramSnapshotMetadata.getLastModifiedTimestamp()))) || (!zzz.equal(Long.valueOf(((SnapshotMetadata)paramObject).getPlayedTime()), Long.valueOf(paramSnapshotMetadata.getPlayedTime()))) || (!zzz.equal(((SnapshotMetadata)paramObject).getUniqueName(), paramSnapshotMetadata.getUniqueName())) || (!zzz.equal(Boolean.valueOf(((SnapshotMetadata)paramObject).hasChangePending()), Boolean.valueOf(paramSnapshotMetadata.hasChangePending()))) || (!zzz.equal(Long.valueOf(((SnapshotMetadata)paramObject).getProgressValue()), Long.valueOf(paramSnapshotMetadata.getProgressValue())))) {
        break;
      }
      bool1 = bool2;
    } while (zzz.equal(((SnapshotMetadata)paramObject).getDeviceName(), paramSnapshotMetadata.getDeviceName()));
    return false;
  }
  
  static String zzb(SnapshotMetadata paramSnapshotMetadata)
  {
    return zzz.zzx(paramSnapshotMetadata).zzg("Game", paramSnapshotMetadata.getGame()).zzg("Owner", paramSnapshotMetadata.getOwner()).zzg("SnapshotId", paramSnapshotMetadata.getSnapshotId()).zzg("CoverImageUri", paramSnapshotMetadata.getCoverImageUri()).zzg("CoverImageUrl", paramSnapshotMetadata.getCoverImageUrl()).zzg("CoverImageAspectRatio", Float.valueOf(paramSnapshotMetadata.getCoverImageAspectRatio())).zzg("Description", paramSnapshotMetadata.getDescription()).zzg("LastModifiedTimestamp", Long.valueOf(paramSnapshotMetadata.getLastModifiedTimestamp())).zzg("PlayedTime", Long.valueOf(paramSnapshotMetadata.getPlayedTime())).zzg("UniqueName", paramSnapshotMetadata.getUniqueName()).zzg("ChangePending", Boolean.valueOf(paramSnapshotMetadata.hasChangePending())).zzg("ProgressValue", Long.valueOf(paramSnapshotMetadata.getProgressValue())).zzg("DeviceName", paramSnapshotMetadata.getDeviceName()).toString();
  }
  
  public boolean equals(Object paramObject)
  {
    return zza(this, paramObject);
  }
  
  public SnapshotMetadata freeze()
  {
    return this;
  }
  
  public float getCoverImageAspectRatio()
  {
    return this.afU;
  }
  
  public Uri getCoverImageUri()
  {
    return this.afN;
  }
  
  public String getCoverImageUrl()
  {
    return this.afR;
  }
  
  public String getDescription()
  {
    return this.cg;
  }
  
  public void getDescription(CharArrayBuffer paramCharArrayBuffer)
  {
    zzg.zzb(this.cg, paramCharArrayBuffer);
  }
  
  public String getDeviceName()
  {
    return this.afY;
  }
  
  public Game getGame()
  {
    return this.acT;
  }
  
  public long getLastModifiedTimestamp()
  {
    return this.afS;
  }
  
  public Player getOwner()
  {
    return this.afQ;
  }
  
  public long getPlayedTime()
  {
    return this.afT;
  }
  
  public long getProgressValue()
  {
    return this.afX;
  }
  
  public String getSnapshotId()
  {
    return this.Zj;
  }
  
  public String getTitle()
  {
    return this.JB;
  }
  
  public String getUniqueName()
  {
    return this.afV;
  }
  
  public int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public boolean hasChangePending()
  {
    return this.afW;
  }
  
  public int hashCode()
  {
    return zza(this);
  }
  
  public boolean isDataValid()
  {
    return true;
  }
  
  public String toString()
  {
    return zzb(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    SnapshotMetadataEntityCreator.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/snapshot/SnapshotMetadataEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */