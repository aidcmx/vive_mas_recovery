package com.google.android.gms.games.social;

public abstract interface OnSocialInviteUpdateReceivedListener
{
  public abstract void zza(SocialInvite paramSocialInvite);
  
  public abstract void zzb(SocialInvite paramSocialInvite);
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/social/OnSocialInviteUpdateReceivedListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */