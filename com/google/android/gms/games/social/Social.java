package com.google.android.gms.games.social;

import com.google.android.gms.common.api.Releasable;
import com.google.android.gms.common.api.Result;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public abstract interface Social
{
  @Retention(RetentionPolicy.SOURCE)
  public static @interface InviteDirection {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface InviteType {}
  
  public static abstract interface InviteUpdateResult
    extends Result
  {}
  
  public static abstract interface LoadInvitesResult
    extends Releasable, Result
  {}
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/social/Social.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */