package com.google.android.gms.games.social;

import android.os.Parcelable;
import com.google.android.gms.common.data.Freezable;
import com.google.android.gms.games.Player;

public abstract interface SocialInvite
  extends Parcelable, Freezable<SocialInvite>
{
  public abstract int getDirection();
  
  public abstract long getLastModifiedTimestamp();
  
  public abstract Player getPlayer();
  
  public abstract int getType();
  
  public abstract String zzbmz();
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/social/SocialInvite.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */