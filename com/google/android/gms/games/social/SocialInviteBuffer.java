package com.google.android.gms.games.social;

import com.google.android.gms.common.data.AbstractDataBuffer;
import com.google.android.gms.common.data.DataHolder;

public class SocialInviteBuffer
  extends AbstractDataBuffer<SocialInvite>
{
  public SocialInviteBuffer(DataHolder paramDataHolder)
  {
    super(paramDataHolder);
  }
  
  public SocialInvite zzsz(int paramInt)
  {
    return new SocialInviteRef(this.zy, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/social/SocialInviteBuffer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */