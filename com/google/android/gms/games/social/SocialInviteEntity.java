package com.google.android.gms.games.social;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerEntity;

public class SocialInviteEntity
  extends AbstractSafeParcelable
  implements SocialInvite
{
  public static final Parcelable.Creator<SocialInviteEntity> CREATOR = new SocialInviteEntityCreator();
  private final PlayerEntity Ye;
  private final long afS;
  private final String aga;
  private final int agb;
  private final int mVersionCode;
  private final int nV;
  
  SocialInviteEntity(int paramInt1, String paramString, PlayerEntity paramPlayerEntity, int paramInt2, int paramInt3, long paramLong)
  {
    this.mVersionCode = paramInt1;
    this.aga = paramString;
    this.Ye = paramPlayerEntity;
    this.nV = paramInt2;
    this.agb = paramInt3;
    this.afS = paramLong;
  }
  
  public SocialInviteEntity(SocialInvite paramSocialInvite)
  {
    this.mVersionCode = 1;
    this.aga = paramSocialInvite.zzbmz();
    Object localObject = paramSocialInvite.getPlayer();
    if (localObject == null) {}
    for (localObject = null;; localObject = (PlayerEntity)((Player)localObject).freeze())
    {
      this.Ye = ((PlayerEntity)localObject);
      this.nV = paramSocialInvite.getType();
      this.agb = paramSocialInvite.getDirection();
      this.afS = paramSocialInvite.getLastModifiedTimestamp();
      return;
    }
  }
  
  static boolean zza(SocialInvite paramSocialInvite, Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if (!(paramObject instanceof SocialInvite)) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramSocialInvite == paramObject);
      paramObject = (SocialInvite)paramObject;
      if ((!zzz.equal(((SocialInvite)paramObject).zzbmz(), paramSocialInvite.zzbmz())) || (!zzz.equal(((SocialInvite)paramObject).getPlayer(), paramSocialInvite.getPlayer())) || (!zzz.equal(Integer.valueOf(((SocialInvite)paramObject).getType()), Integer.valueOf(paramSocialInvite.getType()))) || (!zzz.equal(Integer.valueOf(((SocialInvite)paramObject).getDirection()), Integer.valueOf(paramSocialInvite.getDirection())))) {
        break;
      }
      bool1 = bool2;
    } while (zzz.equal(Long.valueOf(((SocialInvite)paramObject).getLastModifiedTimestamp()), Long.valueOf(paramSocialInvite.getLastModifiedTimestamp())));
    return false;
  }
  
  static int zzc(SocialInvite paramSocialInvite)
  {
    return zzz.hashCode(new Object[] { paramSocialInvite.zzbmz(), paramSocialInvite.getPlayer(), Integer.valueOf(paramSocialInvite.getType()), Integer.valueOf(paramSocialInvite.getDirection()), Long.valueOf(paramSocialInvite.getLastModifiedTimestamp()) });
  }
  
  static String zzd(SocialInvite paramSocialInvite)
  {
    return zzz.zzx(paramSocialInvite).zzg("Social Invite ID", paramSocialInvite.zzbmz()).zzg("Player", paramSocialInvite.getPlayer()).zzg("Type", Integer.valueOf(paramSocialInvite.getType())).zzg("Direction", Integer.valueOf(paramSocialInvite.getDirection())).zzg("Last Modified Timestamp", Long.valueOf(paramSocialInvite.getLastModifiedTimestamp())).toString();
  }
  
  public boolean equals(Object paramObject)
  {
    return zza(this, paramObject);
  }
  
  public int getDirection()
  {
    return this.agb;
  }
  
  public long getLastModifiedTimestamp()
  {
    return this.afS;
  }
  
  public Player getPlayer()
  {
    return this.Ye;
  }
  
  public int getType()
  {
    return this.nV;
  }
  
  public int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    return zzc(this);
  }
  
  public boolean isDataValid()
  {
    return true;
  }
  
  public String toString()
  {
    return zzd(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    SocialInviteEntityCreator.zza(this, paramParcel, paramInt);
  }
  
  public String zzbmz()
  {
    return this.aga;
  }
  
  public SocialInvite zzbna()
  {
    return this;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/social/SocialInviteEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */