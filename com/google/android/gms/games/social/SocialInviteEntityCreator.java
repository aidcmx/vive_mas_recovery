package com.google.android.gms.games.social;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.games.PlayerEntity;

public class SocialInviteEntityCreator
  implements Parcelable.Creator<SocialInviteEntity>
{
  static void zza(SocialInviteEntity paramSocialInviteEntity, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zza(paramParcel, 1, paramSocialInviteEntity.zzbmz(), false);
    zzb.zza(paramParcel, 2, paramSocialInviteEntity.getPlayer(), paramInt, false);
    zzb.zzc(paramParcel, 3, paramSocialInviteEntity.getType());
    zzb.zzc(paramParcel, 4, paramSocialInviteEntity.getDirection());
    zzb.zza(paramParcel, 5, paramSocialInviteEntity.getLastModifiedTimestamp());
    zzb.zzc(paramParcel, 1000, paramSocialInviteEntity.getVersionCode());
    zzb.zzaj(paramParcel, i);
  }
  
  public SocialInviteEntity zzms(Parcel paramParcel)
  {
    PlayerEntity localPlayerEntity = null;
    int i = 0;
    int m = zza.zzcr(paramParcel);
    long l = 0L;
    int j = 0;
    String str = null;
    int k = 0;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.zzcq(paramParcel);
      switch (zza.zzgu(n))
      {
      default: 
        zza.zzb(paramParcel, n);
        break;
      case 1: 
        str = zza.zzq(paramParcel, n);
        break;
      case 2: 
        localPlayerEntity = (PlayerEntity)zza.zza(paramParcel, n, PlayerEntity.CREATOR);
        break;
      case 3: 
        j = zza.zzg(paramParcel, n);
        break;
      case 4: 
        i = zza.zzg(paramParcel, n);
        break;
      case 5: 
        l = zza.zzi(paramParcel, n);
        break;
      case 1000: 
        k = zza.zzg(paramParcel, n);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza(37 + "Overread allowed size end=" + m, paramParcel);
    }
    return new SocialInviteEntity(k, str, localPlayerEntity, j, i, l);
  }
  
  public SocialInviteEntity[] zzta(int paramInt)
  {
    return new SocialInviteEntity[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/social/SocialInviteEntityCreator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */