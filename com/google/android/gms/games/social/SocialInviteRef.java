package com.google.android.gms.games.social;

import android.os.Parcel;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.zzc;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerRef;

public final class SocialInviteRef
  extends zzc
  implements SocialInvite
{
  private final Player ZO;
  
  SocialInviteRef(DataHolder paramDataHolder, int paramInt)
  {
    super(paramDataHolder, paramInt);
    this.ZO = new PlayerRef(paramDataHolder, paramInt);
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    return SocialInviteEntity.zza(this, paramObject);
  }
  
  public int getDirection()
  {
    return getInteger("direction");
  }
  
  public long getLastModifiedTimestamp()
  {
    return getLong("last_modified_timestamp");
  }
  
  public Player getPlayer()
  {
    return this.ZO;
  }
  
  public int getType()
  {
    return getInteger("type");
  }
  
  public int hashCode()
  {
    return SocialInviteEntity.zzc(this);
  }
  
  public String toString()
  {
    return SocialInviteEntity.zzd(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    ((SocialInviteEntity)freeze()).writeToParcel(paramParcel, paramInt);
  }
  
  public String zzbmz()
  {
    return getString("external_social_invite_id");
  }
  
  public SocialInvite zzbna()
  {
    return new SocialInviteEntity(this);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/social/SocialInviteRef.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */