package com.google.android.gms.games.stats;

import com.google.android.gms.common.data.AbstractDataBuffer;
import com.google.android.gms.common.data.DataHolder;

public final class PlayerStatsBuffer
  extends AbstractDataBuffer<PlayerStats>
{
  public PlayerStatsBuffer(DataHolder paramDataHolder)
  {
    super(paramDataHolder);
  }
  
  public PlayerStats zztb(int paramInt)
  {
    return new PlayerStatsRef(this.zy, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/stats/PlayerStatsBuffer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */