package com.google.android.gms.games.stats;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;

public class PlayerStatsEntity
  extends AbstractSafeParcelable
  implements PlayerStats
{
  public static final Parcelable.Creator<PlayerStatsEntity> CREATOR = new PlayerStatsEntityCreator();
  private final float agc;
  private final float agd;
  private final int age;
  private final int agf;
  private final int agg;
  private final float agh;
  private final float agi;
  private final Bundle agj;
  private final float agk;
  private final float agl;
  private final float agm;
  private final int mVersionCode;
  
  PlayerStatsEntity(int paramInt1, float paramFloat1, float paramFloat2, int paramInt2, int paramInt3, int paramInt4, float paramFloat3, float paramFloat4, Bundle paramBundle, float paramFloat5, float paramFloat6, float paramFloat7)
  {
    this.mVersionCode = paramInt1;
    this.agc = paramFloat1;
    this.agd = paramFloat2;
    this.age = paramInt2;
    this.agf = paramInt3;
    this.agg = paramInt4;
    this.agh = paramFloat3;
    this.agi = paramFloat4;
    this.agj = paramBundle;
    this.agk = paramFloat5;
    this.agl = paramFloat6;
    this.agm = paramFloat7;
  }
  
  public PlayerStatsEntity(PlayerStats paramPlayerStats)
  {
    this.mVersionCode = 4;
    this.agc = paramPlayerStats.getAverageSessionLength();
    this.agd = paramPlayerStats.getChurnProbability();
    this.age = paramPlayerStats.getDaysSinceLastPlayed();
    this.agf = paramPlayerStats.getNumberOfPurchases();
    this.agg = paramPlayerStats.getNumberOfSessions();
    this.agh = paramPlayerStats.getSessionPercentile();
    this.agi = paramPlayerStats.getSpendPercentile();
    this.agk = paramPlayerStats.getSpendProbability();
    this.agl = paramPlayerStats.getHighSpenderProbability();
    this.agm = paramPlayerStats.getTotalSpendNext28Days();
    this.agj = paramPlayerStats.zzbnb();
  }
  
  static int zza(PlayerStats paramPlayerStats)
  {
    return zzz.hashCode(new Object[] { Float.valueOf(paramPlayerStats.getAverageSessionLength()), Float.valueOf(paramPlayerStats.getChurnProbability()), Integer.valueOf(paramPlayerStats.getDaysSinceLastPlayed()), Integer.valueOf(paramPlayerStats.getNumberOfPurchases()), Integer.valueOf(paramPlayerStats.getNumberOfSessions()), Float.valueOf(paramPlayerStats.getSessionPercentile()), Float.valueOf(paramPlayerStats.getSpendPercentile()), Float.valueOf(paramPlayerStats.getSpendProbability()), Float.valueOf(paramPlayerStats.getHighSpenderProbability()), Float.valueOf(paramPlayerStats.getTotalSpendNext28Days()) });
  }
  
  static boolean zza(PlayerStats paramPlayerStats, Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if (!(paramObject instanceof PlayerStats)) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramPlayerStats == paramObject);
      paramObject = (PlayerStats)paramObject;
      if ((!zzz.equal(Float.valueOf(((PlayerStats)paramObject).getAverageSessionLength()), Float.valueOf(paramPlayerStats.getAverageSessionLength()))) || (!zzz.equal(Float.valueOf(((PlayerStats)paramObject).getChurnProbability()), Float.valueOf(paramPlayerStats.getChurnProbability()))) || (!zzz.equal(Integer.valueOf(((PlayerStats)paramObject).getDaysSinceLastPlayed()), Integer.valueOf(paramPlayerStats.getDaysSinceLastPlayed()))) || (!zzz.equal(Integer.valueOf(((PlayerStats)paramObject).getNumberOfPurchases()), Integer.valueOf(paramPlayerStats.getNumberOfPurchases()))) || (!zzz.equal(Integer.valueOf(((PlayerStats)paramObject).getNumberOfSessions()), Integer.valueOf(paramPlayerStats.getNumberOfSessions()))) || (!zzz.equal(Float.valueOf(((PlayerStats)paramObject).getSessionPercentile()), Float.valueOf(paramPlayerStats.getSessionPercentile()))) || (!zzz.equal(Float.valueOf(((PlayerStats)paramObject).getSpendPercentile()), Float.valueOf(paramPlayerStats.getSpendPercentile()))) || (!zzz.equal(Float.valueOf(((PlayerStats)paramObject).getSpendProbability()), Float.valueOf(paramPlayerStats.getSpendProbability()))) || (!zzz.equal(Float.valueOf(((PlayerStats)paramObject).getHighSpenderProbability()), Float.valueOf(paramPlayerStats.getHighSpenderProbability())))) {
        break;
      }
      bool1 = bool2;
    } while (zzz.equal(Float.valueOf(((PlayerStats)paramObject).getTotalSpendNext28Days()), Float.valueOf(paramPlayerStats.getTotalSpendNext28Days())));
    return false;
  }
  
  static String zzb(PlayerStats paramPlayerStats)
  {
    return zzz.zzx(paramPlayerStats).zzg("AverageSessionLength", Float.valueOf(paramPlayerStats.getAverageSessionLength())).zzg("ChurnProbability", Float.valueOf(paramPlayerStats.getChurnProbability())).zzg("DaysSinceLastPlayed", Integer.valueOf(paramPlayerStats.getDaysSinceLastPlayed())).zzg("NumberOfPurchases", Integer.valueOf(paramPlayerStats.getNumberOfPurchases())).zzg("NumberOfSessions", Integer.valueOf(paramPlayerStats.getNumberOfSessions())).zzg("SessionPercentile", Float.valueOf(paramPlayerStats.getSessionPercentile())).zzg("SpendPercentile", Float.valueOf(paramPlayerStats.getSpendPercentile())).zzg("SpendProbability", Float.valueOf(paramPlayerStats.getSpendProbability())).zzg("HighSpenderProbability", Float.valueOf(paramPlayerStats.getHighSpenderProbability())).zzg("TotalSpendNext28Days", Float.valueOf(paramPlayerStats.getTotalSpendNext28Days())).toString();
  }
  
  public boolean equals(Object paramObject)
  {
    return zza(this, paramObject);
  }
  
  public float getAverageSessionLength()
  {
    return this.agc;
  }
  
  public float getChurnProbability()
  {
    return this.agd;
  }
  
  public int getDaysSinceLastPlayed()
  {
    return this.age;
  }
  
  public float getHighSpenderProbability()
  {
    return this.agl;
  }
  
  public int getNumberOfPurchases()
  {
    return this.agf;
  }
  
  public int getNumberOfSessions()
  {
    return this.agg;
  }
  
  public float getSessionPercentile()
  {
    return this.agh;
  }
  
  public float getSpendPercentile()
  {
    return this.agi;
  }
  
  public float getSpendProbability()
  {
    return this.agk;
  }
  
  public float getTotalSpendNext28Days()
  {
    return this.agm;
  }
  
  public int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    return zza(this);
  }
  
  public boolean isDataValid()
  {
    return true;
  }
  
  public String toString()
  {
    return zzb(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    PlayerStatsEntityCreator.zza(this, paramParcel, paramInt);
  }
  
  public Bundle zzbnb()
  {
    return this.agj;
  }
  
  public PlayerStats zzbnc()
  {
    return this;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/stats/PlayerStatsEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */