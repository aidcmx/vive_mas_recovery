package com.google.android.gms.games.stats;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class PlayerStatsEntityCreator
  implements Parcelable.Creator<PlayerStatsEntity>
{
  static void zza(PlayerStatsEntity paramPlayerStatsEntity, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.zzcs(paramParcel);
    zzb.zza(paramParcel, 1, paramPlayerStatsEntity.getAverageSessionLength());
    zzb.zza(paramParcel, 2, paramPlayerStatsEntity.getChurnProbability());
    zzb.zzc(paramParcel, 3, paramPlayerStatsEntity.getDaysSinceLastPlayed());
    zzb.zzc(paramParcel, 4, paramPlayerStatsEntity.getNumberOfPurchases());
    zzb.zzc(paramParcel, 5, paramPlayerStatsEntity.getNumberOfSessions());
    zzb.zza(paramParcel, 6, paramPlayerStatsEntity.getSessionPercentile());
    zzb.zza(paramParcel, 7, paramPlayerStatsEntity.getSpendPercentile());
    zzb.zzc(paramParcel, 1000, paramPlayerStatsEntity.getVersionCode());
    zzb.zza(paramParcel, 8, paramPlayerStatsEntity.zzbnb(), false);
    zzb.zza(paramParcel, 9, paramPlayerStatsEntity.getSpendProbability());
    zzb.zza(paramParcel, 10, paramPlayerStatsEntity.getHighSpenderProbability());
    zzb.zza(paramParcel, 11, paramPlayerStatsEntity.getTotalSpendNext28Days());
    zzb.zzaj(paramParcel, paramInt);
  }
  
  public PlayerStatsEntity zzmt(Parcel paramParcel)
  {
    int n = zza.zzcr(paramParcel);
    int m = 0;
    float f7 = 0.0F;
    float f6 = 0.0F;
    int k = 0;
    int j = 0;
    int i = 0;
    float f5 = 0.0F;
    float f4 = 0.0F;
    Bundle localBundle = null;
    float f3 = 0.0F;
    float f2 = 0.0F;
    float f1 = 0.0F;
    while (paramParcel.dataPosition() < n)
    {
      int i1 = zza.zzcq(paramParcel);
      switch (zza.zzgu(i1))
      {
      default: 
        zza.zzb(paramParcel, i1);
        break;
      case 1: 
        f7 = zza.zzl(paramParcel, i1);
        break;
      case 2: 
        f6 = zza.zzl(paramParcel, i1);
        break;
      case 3: 
        k = zza.zzg(paramParcel, i1);
        break;
      case 4: 
        j = zza.zzg(paramParcel, i1);
        break;
      case 5: 
        i = zza.zzg(paramParcel, i1);
        break;
      case 6: 
        f5 = zza.zzl(paramParcel, i1);
        break;
      case 7: 
        f4 = zza.zzl(paramParcel, i1);
        break;
      case 1000: 
        m = zza.zzg(paramParcel, i1);
        break;
      case 8: 
        localBundle = zza.zzs(paramParcel, i1);
        break;
      case 9: 
        f3 = zza.zzl(paramParcel, i1);
        break;
      case 10: 
        f2 = zza.zzl(paramParcel, i1);
        break;
      case 11: 
        f1 = zza.zzl(paramParcel, i1);
      }
    }
    if (paramParcel.dataPosition() != n) {
      throw new zza.zza(37 + "Overread allowed size end=" + n, paramParcel);
    }
    return new PlayerStatsEntity(m, f7, f6, k, j, i, f5, f4, localBundle, f3, f2, f1);
  }
  
  public PlayerStatsEntity[] zztc(int paramInt)
  {
    return new PlayerStatsEntity[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/stats/PlayerStatsEntityCreator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */