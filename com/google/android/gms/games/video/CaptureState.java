package com.google.android.gms.games.video;

import android.os.Bundle;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;

public final class CaptureState
{
  private final boolean agn;
  private final int ago;
  private final int agp;
  private final boolean pN;
  private final boolean zzapl;
  
  private CaptureState(boolean paramBoolean1, int paramInt1, int paramInt2, boolean paramBoolean2, boolean paramBoolean3)
  {
    zzaa.zzbt(VideoConfiguration.isValidCaptureMode(paramInt1, true));
    zzaa.zzbt(VideoConfiguration.isValidQualityLevel(paramInt2, true));
    this.agn = paramBoolean1;
    this.ago = paramInt1;
    this.agp = paramInt2;
    this.pN = paramBoolean2;
    this.zzapl = paramBoolean3;
  }
  
  public static CaptureState zzab(Bundle paramBundle)
  {
    if ((paramBundle == null) || (paramBundle.get("IsCapturing") == null)) {
      return null;
    }
    return new CaptureState(paramBundle.getBoolean("IsCapturing", false), paramBundle.getInt("CaptureMode", -1), paramBundle.getInt("CaptureQuality", -1), paramBundle.getBoolean("IsOverlayVisible", false), paramBundle.getBoolean("IsPaused", false));
  }
  
  public int getCaptureMode()
  {
    return this.ago;
  }
  
  public int getCaptureQuality()
  {
    return this.agp;
  }
  
  public boolean isCapturing()
  {
    return this.agn;
  }
  
  public boolean isOverlayVisible()
  {
    return this.pN;
  }
  
  public boolean isPaused()
  {
    return this.zzapl;
  }
  
  public String toString()
  {
    return zzz.zzx(this).zzg("IsCapturing", Boolean.valueOf(this.agn)).zzg("CaptureMode", Integer.valueOf(this.ago)).zzg("CaptureQuality", Integer.valueOf(this.agp)).zzg("IsOverlayVisible", Boolean.valueOf(this.pN)).zzg("IsPaused", Boolean.valueOf(this.zzapl)).toString();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/video/CaptureState.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */