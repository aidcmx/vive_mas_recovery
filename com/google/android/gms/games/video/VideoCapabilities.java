package com.google.android.gms.games.video;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;

public final class VideoCapabilities
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<VideoCapabilities> CREATOR = new VideoCapabilitiesCreator();
  private final boolean agq;
  private final boolean agr;
  private final boolean ags;
  private final boolean[] agt;
  private final boolean[] agu;
  private final int mVersionCode;
  
  public VideoCapabilities(int paramInt, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean[] paramArrayOfBoolean1, boolean[] paramArrayOfBoolean2)
  {
    this.mVersionCode = paramInt;
    this.agq = paramBoolean1;
    this.agr = paramBoolean2;
    this.ags = paramBoolean3;
    this.agt = paramArrayOfBoolean1;
    this.agu = paramArrayOfBoolean2;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if (!(paramObject instanceof VideoCapabilities)) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (this == paramObject);
      paramObject = (VideoCapabilities)paramObject;
      if ((!zzz.equal(((VideoCapabilities)paramObject).zzbne(), zzbne())) || (!zzz.equal(((VideoCapabilities)paramObject).zzbnf(), zzbnf())) || (!zzz.equal(Boolean.valueOf(((VideoCapabilities)paramObject).isCameraSupported()), Boolean.valueOf(isCameraSupported()))) || (!zzz.equal(Boolean.valueOf(((VideoCapabilities)paramObject).isMicSupported()), Boolean.valueOf(isMicSupported())))) {
        break;
      }
      bool1 = bool2;
    } while (zzz.equal(Boolean.valueOf(((VideoCapabilities)paramObject).isWriteStorageSupported()), Boolean.valueOf(isWriteStorageSupported())));
    return false;
  }
  
  public int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { zzbne(), zzbnf(), Boolean.valueOf(isCameraSupported()), Boolean.valueOf(isMicSupported()), Boolean.valueOf(isWriteStorageSupported()) });
  }
  
  public boolean isCameraSupported()
  {
    return this.agq;
  }
  
  public boolean isFullySupported(int paramInt1, int paramInt2)
  {
    return (this.agq) && (this.agr) && (this.ags) && (supportsCaptureMode(paramInt1)) && (supportsQualityLevel(paramInt2));
  }
  
  public boolean isMicSupported()
  {
    return this.agr;
  }
  
  public boolean isWriteStorageSupported()
  {
    return this.ags;
  }
  
  public boolean supportsCaptureMode(int paramInt)
  {
    zzaa.zzbs(VideoConfiguration.isValidCaptureMode(paramInt, false));
    return this.agt[paramInt];
  }
  
  public boolean supportsQualityLevel(int paramInt)
  {
    zzaa.zzbs(VideoConfiguration.isValidQualityLevel(paramInt, false));
    return this.agu[paramInt];
  }
  
  public String toString()
  {
    return zzz.zzx(this).zzg("SupportedCaptureModes", zzbne()).zzg("SupportedQualityLevels", zzbnf()).zzg("CameraSupported", Boolean.valueOf(isCameraSupported())).zzg("MicSupported", Boolean.valueOf(isMicSupported())).zzg("StorageWriteSupported", Boolean.valueOf(isWriteStorageSupported())).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    VideoCapabilitiesCreator.zza(this, paramParcel, paramInt);
  }
  
  public boolean[] zzbne()
  {
    return this.agt;
  }
  
  public boolean[] zzbnf()
  {
    return this.agu;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/video/VideoCapabilities.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */