package com.google.android.gms.games.video;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;

public final class VideoConfiguration
  extends AbstractSafeParcelable
{
  public static final int CAPTURE_MODE_FILE = 0;
  public static final int CAPTURE_MODE_STREAM = 1;
  public static final int CAPTURE_MODE_UNKNOWN = -1;
  public static final Parcelable.Creator<VideoConfiguration> CREATOR = new VideoConfigurationCreator();
  public static final int NUM_CAPTURE_MODE = 2;
  public static final int NUM_QUALITY_LEVEL = 4;
  public static final int QUALITY_LEVEL_FULLHD = 3;
  public static final int QUALITY_LEVEL_HD = 1;
  public static final int QUALITY_LEVEL_SD = 0;
  public static final int QUALITY_LEVEL_UNKNOWN = -1;
  public static final int QUALITY_LEVEL_XHD = 2;
  private final boolean agA;
  private final int ago;
  private final int agv;
  private final String agw;
  private final String agx;
  private final String agy;
  private final String agz;
  private final int mVersionCode;
  
  public VideoConfiguration(int paramInt1, int paramInt2, int paramInt3, String paramString1, String paramString2, String paramString3, String paramString4, boolean paramBoolean)
  {
    this.mVersionCode = paramInt1;
    zzaa.zzbt(isValidQualityLevel(paramInt2, false));
    zzaa.zzbt(isValidCaptureMode(paramInt3, false));
    this.agv = paramInt2;
    this.ago = paramInt3;
    this.agA = paramBoolean;
    if (paramInt3 == 1)
    {
      this.agx = paramString2;
      this.agw = paramString1;
      this.agy = paramString3;
      this.agz = paramString4;
      return;
    }
    if (paramString2 == null)
    {
      paramBoolean = true;
      zzaa.zzb(paramBoolean, "Stream key should be null when not streaming");
      if (paramString1 != null) {
        break label162;
      }
      paramBoolean = true;
      label97:
      zzaa.zzb(paramBoolean, "Stream url should be null when not streaming");
      if (paramString3 != null) {
        break label168;
      }
      paramBoolean = true;
      label112:
      zzaa.zzb(paramBoolean, "Stream title should be null when not streaming");
      if (paramString4 != null) {
        break label174;
      }
    }
    label162:
    label168:
    label174:
    for (paramBoolean = bool;; paramBoolean = false)
    {
      zzaa.zzb(paramBoolean, "Stream description should be null when not streaming");
      this.agx = null;
      this.agw = null;
      this.agy = null;
      this.agz = null;
      return;
      paramBoolean = false;
      break;
      paramBoolean = false;
      break label97;
      paramBoolean = false;
      break label112;
    }
  }
  
  private VideoConfiguration(int paramInt1, int paramInt2, String paramString1, String paramString2, String paramString3, String paramString4, boolean paramBoolean)
  {
    this(2, paramInt1, paramInt2, paramString1, paramString2, paramString3, paramString4, paramBoolean);
  }
  
  public static boolean isValidCaptureMode(int paramInt, boolean paramBoolean)
  {
    switch (paramInt)
    {
    default: 
      paramBoolean = false;
    case -1: 
      return paramBoolean;
    }
    return true;
  }
  
  public static boolean isValidQualityLevel(int paramInt, boolean paramBoolean)
  {
    switch (paramInt)
    {
    default: 
      paramBoolean = false;
    case -1: 
      return paramBoolean;
    }
    return true;
  }
  
  public int getCaptureMode()
  {
    return this.ago;
  }
  
  public int getQualityLevel()
  {
    return this.agv;
  }
  
  public String getStreamUrl()
  {
    return this.agw;
  }
  
  public int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    VideoConfigurationCreator.zza(this, paramParcel, paramInt);
  }
  
  public String zzbng()
  {
    return this.agx;
  }
  
  public String zzbnh()
  {
    return this.agy;
  }
  
  public String zzbni()
  {
    return this.agz;
  }
  
  public boolean zzbnj()
  {
    return this.agA;
  }
  
  public static final class Builder
  {
    private boolean agA;
    private int ago;
    private int agv;
    private String agw;
    private String agx;
    private String agy;
    private String agz;
    
    public Builder(int paramInt1, int paramInt2)
    {
      this.agv = paramInt1;
      this.ago = paramInt2;
      this.agA = true;
      this.agw = null;
      this.agx = null;
      this.agy = null;
      this.agz = null;
    }
    
    public VideoConfiguration build()
    {
      return new VideoConfiguration(this.agv, this.ago, null, null, null, null, this.agA, null);
    }
    
    public Builder setCaptureMode(int paramInt)
    {
      this.ago = paramInt;
      return this;
    }
    
    public Builder setQualityLevel(int paramInt)
    {
      this.agv = paramInt;
      return this;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/video/VideoConfiguration.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */