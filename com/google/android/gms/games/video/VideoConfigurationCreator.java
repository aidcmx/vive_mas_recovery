package com.google.android.gms.games.video;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class VideoConfigurationCreator
  implements Parcelable.Creator<VideoConfiguration>
{
  static void zza(VideoConfiguration paramVideoConfiguration, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramVideoConfiguration.getQualityLevel());
    zzb.zzc(paramParcel, 2, paramVideoConfiguration.getCaptureMode());
    zzb.zza(paramParcel, 3, paramVideoConfiguration.getStreamUrl(), false);
    zzb.zza(paramParcel, 4, paramVideoConfiguration.zzbng(), false);
    zzb.zza(paramParcel, 5, paramVideoConfiguration.zzbnh(), false);
    zzb.zza(paramParcel, 6, paramVideoConfiguration.zzbni(), false);
    zzb.zza(paramParcel, 7, paramVideoConfiguration.zzbnj());
    zzb.zzc(paramParcel, 1000, paramVideoConfiguration.getVersionCode());
    zzb.zzaj(paramParcel, paramInt);
  }
  
  public VideoConfiguration zzmv(Parcel paramParcel)
  {
    String str1 = null;
    boolean bool = false;
    int m = zza.zzcr(paramParcel);
    String str2 = null;
    String str3 = null;
    String str4 = null;
    int i = 0;
    int j = 0;
    int k = 0;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.zzcq(paramParcel);
      switch (zza.zzgu(n))
      {
      default: 
        zza.zzb(paramParcel, n);
        break;
      case 1: 
        j = zza.zzg(paramParcel, n);
        break;
      case 2: 
        i = zza.zzg(paramParcel, n);
        break;
      case 3: 
        str4 = zza.zzq(paramParcel, n);
        break;
      case 4: 
        str3 = zza.zzq(paramParcel, n);
        break;
      case 5: 
        str2 = zza.zzq(paramParcel, n);
        break;
      case 6: 
        str1 = zza.zzq(paramParcel, n);
        break;
      case 7: 
        bool = zza.zzc(paramParcel, n);
        break;
      case 1000: 
        k = zza.zzg(paramParcel, n);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza(37 + "Overread allowed size end=" + m, paramParcel);
    }
    return new VideoConfiguration(k, j, i, str4, str3, str2, str1, bool);
  }
  
  public VideoConfiguration[] zztf(int paramInt)
  {
    return new VideoConfiguration[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/video/VideoConfigurationCreator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */