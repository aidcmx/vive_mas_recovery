package com.google.android.gms.games.video;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzc;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;

public final class VideoEntity
  extends AbstractSafeParcelable
  implements Video
{
  public static final Parcelable.Creator<VideoEntity> CREATOR = new VideoEntityCreator();
  private final int CP;
  private final String agB;
  private final long agC;
  private final int mVersionCode;
  private final long zzbwt;
  private final String zzcjc;
  
  VideoEntity(int paramInt1, int paramInt2, String paramString1, long paramLong1, long paramLong2, String paramString2)
  {
    this.mVersionCode = paramInt1;
    this.CP = paramInt2;
    this.agB = paramString1;
    this.agC = paramLong1;
    this.zzbwt = paramLong2;
    this.zzcjc = paramString2;
  }
  
  public VideoEntity(Video paramVideo)
  {
    this.mVersionCode = 1;
    this.CP = paramVideo.getDuration();
    this.agB = paramVideo.zzbnd();
    this.agC = paramVideo.getFileSize();
    this.zzbwt = paramVideo.getStartTime();
    this.zzcjc = paramVideo.getPackageName();
    zzc.zzu(this.agB);
    zzc.zzu(this.zzcjc);
  }
  
  static int zza(Video paramVideo)
  {
    return zzz.hashCode(new Object[] { Integer.valueOf(paramVideo.getDuration()), paramVideo.zzbnd(), Long.valueOf(paramVideo.getFileSize()), Long.valueOf(paramVideo.getStartTime()), paramVideo.getPackageName() });
  }
  
  static boolean zza(Video paramVideo, Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if (!(paramObject instanceof Video)) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramVideo == paramObject);
      paramObject = (Video)paramObject;
      if ((!zzz.equal(Integer.valueOf(((Video)paramObject).getDuration()), Integer.valueOf(paramVideo.getDuration()))) || (!zzz.equal(((Video)paramObject).zzbnd(), paramVideo.zzbnd())) || (!zzz.equal(Long.valueOf(((Video)paramObject).getFileSize()), Long.valueOf(paramVideo.getFileSize()))) || (!zzz.equal(Long.valueOf(((Video)paramObject).getStartTime()), Long.valueOf(paramVideo.getStartTime())))) {
        break;
      }
      bool1 = bool2;
    } while (zzz.equal(((Video)paramObject).getPackageName(), paramVideo.getPackageName()));
    return false;
  }
  
  static String zzb(Video paramVideo)
  {
    return zzz.zzx(paramVideo).zzg("Duration", Integer.valueOf(paramVideo.getDuration())).zzg("File path", paramVideo.zzbnd()).zzg("File size", Long.valueOf(paramVideo.getFileSize())).zzg("Start time", Long.valueOf(paramVideo.getStartTime())).zzg("Package name", paramVideo.getPackageName()).toString();
  }
  
  public boolean equals(Object paramObject)
  {
    return zza(this, paramObject);
  }
  
  public int getDuration()
  {
    return this.CP;
  }
  
  public long getFileSize()
  {
    return this.agC;
  }
  
  public String getPackageName()
  {
    return this.zzcjc;
  }
  
  public long getStartTime()
  {
    return this.zzbwt;
  }
  
  public int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    return zza(this);
  }
  
  public boolean isDataValid()
  {
    return true;
  }
  
  public String toString()
  {
    return zzb(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    VideoEntityCreator.zza(this, paramParcel, paramInt);
  }
  
  public String zzbnd()
  {
    return this.agB;
  }
  
  public Video zzbnk()
  {
    return this;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/video/VideoEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */