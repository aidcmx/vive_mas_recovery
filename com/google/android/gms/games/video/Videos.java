package com.google.android.gms.games.video;

import android.content.Intent;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;

public abstract interface Videos
{
  public static final int CAPTURE_OVERLAY_STATE_CAPTURE_STARTED = 2;
  public static final int CAPTURE_OVERLAY_STATE_CAPTURE_STOPPED = 3;
  public static final int CAPTURE_OVERLAY_STATE_DISMISSED = 4;
  public static final int CAPTURE_OVERLAY_STATE_SHOWN = 1;
  
  public abstract PendingResult<CaptureCapabilitiesResult> getCaptureCapabilities(GoogleApiClient paramGoogleApiClient);
  
  public abstract Intent getCaptureOverlayIntent(GoogleApiClient paramGoogleApiClient);
  
  public abstract PendingResult<CaptureStateResult> getCaptureState(GoogleApiClient paramGoogleApiClient);
  
  public abstract PendingResult<CaptureAvailableResult> isCaptureAvailable(GoogleApiClient paramGoogleApiClient, int paramInt);
  
  public abstract boolean isCaptureSupported(GoogleApiClient paramGoogleApiClient);
  
  public abstract void registerCaptureOverlayStateChangedListener(GoogleApiClient paramGoogleApiClient, CaptureOverlayStateListener paramCaptureOverlayStateListener);
  
  public abstract void unregisterCaptureOverlayStateChangedListener(GoogleApiClient paramGoogleApiClient);
  
  public static abstract interface CaptureAvailableResult
    extends Result
  {
    public abstract boolean isAvailable();
  }
  
  public static abstract interface CaptureCapabilitiesResult
    extends Result
  {
    public abstract VideoCapabilities getCapabilities();
  }
  
  public static abstract interface CaptureOverlayStateListener
  {
    public abstract void onCaptureOverlayStateChanged(int paramInt);
  }
  
  public static abstract interface CaptureRuntimeErrorCallback
  {
    public abstract void zzth(int paramInt);
  }
  
  public static abstract interface CaptureStateResult
    extends Result
  {
    public abstract CaptureState getCaptureState();
  }
  
  public static abstract interface CaptureStoppedResult
    extends Result
  {}
  
  public static abstract interface CaptureStreamingAvailabilityResult
    extends Result
  {}
  
  public static abstract interface CaptureStreamingMetadataResult
    extends Result
  {}
  
  public static abstract interface CaptureStreamingUrlResult
    extends Result
  {
    public abstract String getUrl();
  }
  
  public static abstract interface ListVideosResult
    extends Result
  {}
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/games/video/Videos.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */