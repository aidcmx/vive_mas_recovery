package com.google.android.gms.gass.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.internal.zzaf.zza;
import com.google.android.gms.internal.zzarz;
import com.google.android.gms.internal.zzasa;

public final class GassResponseParcel
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<GassResponseParcel> CREATOR = new zzd();
  private zzaf.zza agI;
  private byte[] agJ;
  public final int versionCode;
  
  GassResponseParcel(int paramInt, byte[] paramArrayOfByte)
  {
    this.versionCode = paramInt;
    this.agI = null;
    this.agJ = paramArrayOfByte;
    zzazw();
  }
  
  private void zzazu()
  {
    if (!zzazv()) {}
    try
    {
      this.agI = zzaf.zza.zzd(this.agJ);
      this.agJ = null;
      zzazw();
      return;
    }
    catch (zzarz localzzarz)
    {
      throw new IllegalStateException(localzzarz);
    }
  }
  
  private boolean zzazv()
  {
    return this.agI != null;
  }
  
  private void zzazw()
  {
    if ((this.agI == null) && (this.agJ != null)) {}
    while ((this.agI != null) && (this.agJ == null)) {
      return;
    }
    if ((this.agI != null) && (this.agJ != null)) {
      throw new IllegalStateException("Invalid internal representation - full");
    }
    if ((this.agI == null) && (this.agJ == null)) {
      throw new IllegalStateException("Invalid internal representation - empty");
    }
    throw new IllegalStateException("Impossible");
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzd.zza(this, paramParcel, paramInt);
  }
  
  public byte[] zzbnn()
  {
    if (this.agJ != null) {
      return this.agJ;
    }
    return zzasa.zzf(this.agI);
  }
  
  public zzaf.zza zzbno()
  {
    zzazu();
    return this.agI;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/gass/internal/GassResponseParcel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */