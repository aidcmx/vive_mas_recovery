package com.google.android.gms.gass.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.HandlerThread;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.zze.zzb;
import com.google.android.gms.common.internal.zze.zzc;
import com.google.android.gms.internal.zzaf.zza;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class zza
{
  public static zzaf.zza zzi(Context paramContext, String paramString1, String paramString2)
  {
    return new zza(paramContext, paramString1, paramString2).zzcv();
  }
  
  static class zza
    implements zze.zzb, zze.zzc
  {
    protected zzb agD;
    private final String agE;
    private final LinkedBlockingQueue<zzaf.zza> agF;
    private final HandlerThread agG;
    private final String packageName;
    
    public zza(Context paramContext, String paramString1, String paramString2)
    {
      this.packageName = paramString1;
      this.agE = paramString2;
      this.agG = new HandlerThread("GassClient");
      this.agG.start();
      this.agD = new zzb(paramContext, this.agG.getLooper(), this, this);
      this.agF = new LinkedBlockingQueue();
      connect();
    }
    
    protected void connect()
    {
      this.agD.zzavd();
    }
    
    public void onConnected(Bundle paramBundle)
    {
      paramBundle = zzbnl();
      if (paramBundle != null) {}
      try
      {
        paramBundle = paramBundle.zza(new GassRequestParcel(this.packageName, this.agE)).zzbno();
        this.agF.put(paramBundle);
        zztb();
        this.agG.quit();
        return;
      }
      catch (Throwable paramBundle)
      {
        paramBundle = paramBundle;
        zztb();
        this.agG.quit();
        return;
      }
      finally
      {
        paramBundle = finally;
        zztb();
        this.agG.quit();
        throw paramBundle;
      }
    }
    
    public void onConnectionFailed(ConnectionResult paramConnectionResult)
    {
      try
      {
        this.agF.put(new zzaf.zza());
        return;
      }
      catch (InterruptedException paramConnectionResult) {}
    }
    
    public void onConnectionSuspended(int paramInt)
    {
      try
      {
        this.agF.put(new zzaf.zza());
        return;
      }
      catch (InterruptedException localInterruptedException) {}
    }
    
    protected zze zzbnl()
    {
      try
      {
        zze localzze = this.agD.zzbnm();
        return localzze;
      }
      catch (IllegalStateException localIllegalStateException)
      {
        return null;
      }
      catch (DeadObjectException localDeadObjectException)
      {
        for (;;) {}
      }
    }
    
    public zzaf.zza zzcv()
    {
      return zzti(2000);
    }
    
    public void zztb()
    {
      if ((this.agD != null) && ((this.agD.isConnected()) || (this.agD.isConnecting()))) {
        this.agD.disconnect();
      }
    }
    
    public zzaf.zza zzti(int paramInt)
    {
      try
      {
        zzaf.zza localzza1 = (zzaf.zza)this.agF.poll(paramInt, TimeUnit.MILLISECONDS);
        zzaf.zza localzza2 = localzza1;
        if (localzza1 == null) {
          localzza2 = new zzaf.zza();
        }
        return localzza2;
      }
      catch (InterruptedException localInterruptedException)
      {
        for (;;)
        {
          Object localObject = null;
        }
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/gass/internal/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */