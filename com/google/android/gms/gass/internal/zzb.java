package com.google.android.gms.gass.internal;

import android.content.Context;
import android.os.DeadObjectException;
import android.os.IBinder;
import android.os.Looper;
import com.google.android.gms.common.internal.zze.zzb;
import com.google.android.gms.common.internal.zze.zzc;

public class zzb
  extends com.google.android.gms.common.internal.zze<zze>
{
  public zzb(Context paramContext, Looper paramLooper, zze.zzb paramzzb, zze.zzc paramzzc)
  {
    super(paramContext, paramLooper, 116, paramzzb, paramzzc, null);
  }
  
  public zze zzbnm()
    throws DeadObjectException
  {
    return (zze)super.zzavg();
  }
  
  protected zze zzgr(IBinder paramIBinder)
  {
    return zze.zza.zzgs(paramIBinder);
  }
  
  protected String zzjx()
  {
    return "com.google.android.gms.gass.START";
  }
  
  protected String zzjy()
  {
    return "com.google.android.gms.gass.internal.IGassService";
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/gass/internal/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */