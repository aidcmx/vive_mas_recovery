package com.google.android.gms.gcm;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.util.Log;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class GcmTaskService
  extends Service
{
  public static final String SERVICE_ACTION_EXECUTE_TASK = "com.google.android.gms.gcm.ACTION_TASK_READY";
  public static final String SERVICE_ACTION_INITIALIZE = "com.google.android.gms.gcm.SERVICE_ACTION_INITIALIZE";
  public static final String SERVICE_PERMISSION = "com.google.android.gms.permission.BIND_NETWORK_TASK_SERVICE";
  private final Set<String> agT = new HashSet();
  private int agU;
  private ExecutorService zzahu;
  
  private void zzkn(String paramString)
  {
    synchronized (this.agT)
    {
      this.agT.remove(paramString);
      if (this.agT.size() == 0) {
        stopSelf(this.agU);
      }
      return;
    }
  }
  
  private void zztm(int paramInt)
  {
    synchronized (this.agT)
    {
      this.agU = paramInt;
      if (this.agT.size() == 0) {
        stopSelf(this.agU);
      }
      return;
    }
  }
  
  public IBinder onBind(Intent paramIntent)
  {
    return null;
  }
  
  @CallSuper
  public void onCreate()
  {
    super.onCreate();
    this.zzahu = zzbns();
  }
  
  @CallSuper
  public void onDestroy()
  {
    super.onDestroy();
    List localList = this.zzahu.shutdownNow();
    if (!localList.isEmpty())
    {
      int i = localList.size();
      Log.e("GcmTaskService", 79 + "Shutting down, but not all tasks are finished executing. Remaining: " + i);
    }
  }
  
  public void onInitializeTasks() {}
  
  public abstract int onRunTask(TaskParams paramTaskParams);
  
  @CallSuper
  public int onStartCommand(Intent arg1, int paramInt1, int paramInt2)
  {
    if (??? == null)
    {
      zztm(paramInt2);
      return 2;
    }
    for (;;)
    {
      try
      {
        ???.setExtrasClassLoader(PendingCallback.class.getClassLoader());
        String str = ???.getAction();
        if ("com.google.android.gms.gcm.ACTION_TASK_READY".equals(str))
        {
          str = ???.getStringExtra("tag");
          Object localObject2 = ???.getParcelableExtra("callback");
          Bundle localBundle = (Bundle)???.getParcelableExtra("extras");
          if ((localObject2 == null) || (!(localObject2 instanceof PendingCallback)))
          {
            ??? = String.valueOf(getPackageName());
            Log.e("GcmTaskService", String.valueOf(???).length() + 47 + String.valueOf(str).length() + ??? + " " + str + ": Could not process request, invalid callback.");
            return 2;
          }
          synchronized (this.agT)
          {
            if (!this.agT.add(str))
            {
              localObject2 = String.valueOf(getPackageName());
              Log.w("GcmTaskService", String.valueOf(localObject2).length() + 44 + String.valueOf(str).length() + (String)localObject2 + " " + str + ": Task already running, won't start another");
              return 2;
            }
            this.zzahu.execute(new zza(str, ((PendingCallback)localObject2).getIBinder(), localBundle));
            return 2;
          }
        }
        if (!"com.google.android.gms.gcm.SERVICE_ACTION_INITIALIZE".equals(localObject1)) {
          break label310;
        }
      }
      finally
      {
        zztm(paramInt2);
      }
      onInitializeTasks();
      continue;
      label310:
      Log.e("GcmTaskService", String.valueOf(localObject1).length() + 37 + "Unknown action received " + (String)localObject1 + ", terminating");
    }
  }
  
  protected ExecutorService zzbns()
  {
    Executors.newFixedThreadPool(2, new ThreadFactory()
    {
      private final AtomicInteger agV = new AtomicInteger(1);
      
      public Thread newThread(@NonNull Runnable paramAnonymousRunnable)
      {
        int i = this.agV.getAndIncrement();
        paramAnonymousRunnable = new Thread(paramAnonymousRunnable, 20 + "gcm-task#" + i);
        paramAnonymousRunnable.setPriority(4);
        return paramAnonymousRunnable;
      }
    });
  }
  
  private class zza
    implements Runnable
  {
    private final zzb agX;
    private final Bundle mExtras;
    private final String mTag;
    
    zza(String paramString, IBinder paramIBinder, Bundle paramBundle)
    {
      this.mTag = paramString;
      this.agX = zzb.zza.zzgt(paramIBinder);
      this.mExtras = paramBundle;
    }
    
    public void run()
    {
      int i = GcmTaskService.this.onRunTask(new TaskParams(this.mTag, this.mExtras));
      try
      {
        this.agX.zztn(i);
        return;
      }
      catch (RemoteException localRemoteException)
      {
        String str = String.valueOf(this.mTag);
        if (str.length() != 0) {}
        for (str = "Error reporting result of operation to scheduler for ".concat(str);; str = new String("Error reporting result of operation to scheduler for "))
        {
          Log.e("GcmTaskService", str);
          return;
        }
      }
      finally
      {
        GcmTaskService.zza(GcmTaskService.this, this.mTag);
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/gcm/GcmTaskService.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */