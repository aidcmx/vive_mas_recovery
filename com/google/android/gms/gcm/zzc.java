package com.google.android.gms.gcm;

import android.os.Bundle;

public class zzc
{
  public static final zzc aho = new zzc(0, 30, 3600);
  public static final zzc ahp = new zzc(1, 30, 3600);
  private final int ahq;
  private final int ahr;
  private final int ahs;
  
  private zzc(int paramInt1, int paramInt2, int paramInt3)
  {
    this.ahq = paramInt1;
    this.ahr = paramInt2;
    this.ahs = paramInt3;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    do
    {
      return true;
      if (!(paramObject instanceof zzc)) {
        return false;
      }
      paramObject = (zzc)paramObject;
    } while ((((zzc)paramObject).ahq == this.ahq) && (((zzc)paramObject).ahr == this.ahr) && (((zzc)paramObject).ahs == this.ahs));
    return false;
  }
  
  public int hashCode()
  {
    return ((this.ahq + 1 ^ 0xF4243) * 1000003 ^ this.ahr) * 1000003 ^ this.ahs;
  }
  
  public String toString()
  {
    int i = this.ahq;
    int j = this.ahr;
    int k = this.ahs;
    return 74 + "policy=" + i + " initial_backoff=" + j + " maximum_backoff=" + k;
  }
  
  public Bundle zzaj(Bundle paramBundle)
  {
    paramBundle.putInt("retry_policy", this.ahq);
    paramBundle.putInt("initial_backoff_seconds", this.ahr);
    paramBundle.putInt("maximum_backoff_seconds", this.ahs);
    return paramBundle;
  }
  
  public int zzbnv()
  {
    return this.ahq;
  }
  
  public int zzbnw()
  {
    return this.ahr;
  }
  
  public int zzbnx()
  {
    return this.ahs;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/gcm/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */