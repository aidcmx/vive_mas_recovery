package com.google.android.gms.identity.intents.model;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public final class UserAddress
  extends AbstractSafeParcelable
  implements ReflectedParcelable
{
  public static final Parcelable.Creator<UserAddress> CREATOR = new zzb();
  String ahQ;
  String ahR;
  String ahS;
  String ahT;
  String ahU;
  String ahV;
  String ahW;
  String ahX;
  String ahY;
  boolean ahZ;
  String aia;
  String aib;
  private final int mVersionCode;
  String name;
  String phoneNumber;
  String zzcpw;
  
  UserAddress()
  {
    this.mVersionCode = 1;
  }
  
  UserAddress(int paramInt, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, String paramString9, String paramString10, String paramString11, String paramString12, boolean paramBoolean, String paramString13, String paramString14)
  {
    this.mVersionCode = paramInt;
    this.name = paramString1;
    this.ahQ = paramString2;
    this.ahR = paramString3;
    this.ahS = paramString4;
    this.ahT = paramString5;
    this.ahU = paramString6;
    this.ahV = paramString7;
    this.ahW = paramString8;
    this.zzcpw = paramString9;
    this.ahX = paramString10;
    this.ahY = paramString11;
    this.phoneNumber = paramString12;
    this.ahZ = paramBoolean;
    this.aia = paramString13;
    this.aib = paramString14;
  }
  
  public static UserAddress fromIntent(Intent paramIntent)
  {
    if ((paramIntent == null) || (!paramIntent.hasExtra("com.google.android.gms.identity.intents.EXTRA_ADDRESS"))) {
      return null;
    }
    return (UserAddress)paramIntent.getParcelableExtra("com.google.android.gms.identity.intents.EXTRA_ADDRESS");
  }
  
  public String getAddress1()
  {
    return this.ahQ;
  }
  
  public String getAddress2()
  {
    return this.ahR;
  }
  
  public String getAddress3()
  {
    return this.ahS;
  }
  
  public String getAddress4()
  {
    return this.ahT;
  }
  
  public String getAddress5()
  {
    return this.ahU;
  }
  
  public String getAdministrativeArea()
  {
    return this.ahV;
  }
  
  public String getCompanyName()
  {
    return this.aia;
  }
  
  public String getCountryCode()
  {
    return this.zzcpw;
  }
  
  public String getEmailAddress()
  {
    return this.aib;
  }
  
  public String getLocality()
  {
    return this.ahW;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public String getPhoneNumber()
  {
    return this.phoneNumber;
  }
  
  public String getPostalCode()
  {
    return this.ahX;
  }
  
  public String getSortingCode()
  {
    return this.ahY;
  }
  
  public int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public boolean isPostBox()
  {
    return this.ahZ;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzb.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/identity/intents/model/UserAddress.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */