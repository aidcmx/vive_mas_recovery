package com.google.android.gms.instantapps;

import android.content.Context;
import android.content.Intent;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;

public abstract interface InstantAppsApi
{
  public static final String EXTRA_DO_NOT_LAUNCH_INSTANT_APP = "com.google.android.gms.instantapps.DO_NOT_LAUNCH_INSTANT_APP";
  
  public abstract Intent getInstantAppIntent(Context paramContext, String paramString, Intent paramIntent);
  
  public abstract PendingResult<LaunchDataResult> getInstantAppLaunchData(GoogleApiClient paramGoogleApiClient, String paramString, LaunchSettings paramLaunchSettings);
  
  public abstract boolean initializeIntentClient(Context paramContext);
  
  public static abstract interface LaunchDataResult
    extends Result
  {
    public abstract LaunchData getLaunchData();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/instantapps/InstantAppsApi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */