package com.google.android.gms.instantapps;

import com.google.android.gms.common.api.CommonStatusCodes;

public final class InstantAppsStatusCodes
  extends CommonStatusCodes
{
  public static final int INSTANT_APP_NOT_FOUND = 19500;
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/instantapps/InstantAppsStatusCodes.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */