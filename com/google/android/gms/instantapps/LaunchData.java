package com.google.android.gms.instantapps;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.Nullable;
import com.google.android.gms.common.data.BitmapTeleporter;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public class LaunchData
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<LaunchData> CREATOR = new zza();
  private final String aiH;
  final BitmapTeleporter aiI;
  private final Bitmap aiJ;
  private final Intent intent;
  private final String packageName;
  private final int versionCode;
  
  LaunchData(int paramInt, Intent paramIntent, String paramString1, String paramString2, BitmapTeleporter paramBitmapTeleporter)
  {
    this.versionCode = paramInt;
    this.intent = paramIntent;
    this.packageName = paramString1;
    this.aiH = paramString2;
    this.aiI = paramBitmapTeleporter;
    if (paramBitmapTeleporter != null) {}
    for (paramIntent = paramBitmapTeleporter.zzauj();; paramIntent = null)
    {
      this.aiJ = paramIntent;
      return;
    }
  }
  
  @Nullable
  public Bitmap getApplicationIcon()
  {
    return this.aiJ;
  }
  
  @Nullable
  public String getApplicationLabel()
  {
    return this.aiH;
  }
  
  @Nullable
  public Intent getIntent()
  {
    return this.intent;
  }
  
  @Nullable
  public String getPackageName()
  {
    return this.packageName;
  }
  
  public int getVersionCode()
  {
    return this.versionCode;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zza.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/instantapps/LaunchData.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */