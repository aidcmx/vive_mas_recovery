package com.google.android.gms.instantapps;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public class LaunchSettings
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<LaunchSettings> CREATOR = new zzb();
  public static final int SOURCE_TYPE_CALLER = 1;
  public static final int SOURCE_TYPE_NFC = 2;
  public static final int SOURCE_TYPE_UNSET = 0;
  private Uri aiK;
  private int aiL;
  private boolean aiM;
  private boolean aiN;
  private final int versionCode;
  
  public LaunchSettings()
  {
    this.versionCode = 1;
  }
  
  LaunchSettings(int paramInt1, Uri paramUri, int paramInt2, boolean paramBoolean1, boolean paramBoolean2)
  {
    this.versionCode = paramInt1;
    this.aiK = paramUri;
    this.aiL = paramInt2;
    this.aiM = paramBoolean1;
    this.aiN = paramBoolean2;
  }
  
  public int getVersionCode()
  {
    return this.versionCode;
  }
  
  public LaunchSettings setIsReferrerTrusted(boolean paramBoolean)
  {
    this.aiM = paramBoolean;
    return this;
  }
  
  public LaunchSettings setIsUserConfirmedLaunch(boolean paramBoolean)
  {
    this.aiN = paramBoolean;
    return this;
  }
  
  public LaunchSettings setReferrerUri(Uri paramUri)
  {
    this.aiK = paramUri;
    return this;
  }
  
  public LaunchSettings setSourceType(int paramInt)
  {
    this.aiL = paramInt;
    return this;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzb.zza(this, paramParcel, paramInt);
  }
  
  public Uri zzboq()
  {
    return this.aiK;
  }
  
  public int zzbor()
  {
    return this.aiL;
  }
  
  public boolean zzbos()
  {
    return this.aiM;
  }
  
  public boolean zzbot()
  {
    return this.aiN;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/instantapps/LaunchSettings.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */