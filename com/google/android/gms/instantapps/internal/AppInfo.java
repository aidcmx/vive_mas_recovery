package com.google.android.gms.instantapps.internal;

import android.content.pm.PackageInfo;
import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.data.BitmapTeleporter;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.util.List;

public class AppInfo
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<AppInfo> CREATOR = new zza();
  final BitmapTeleporter aiO;
  private final List<Route> aiP;
  private final List<AtomInfo> aiQ;
  private final int aiR;
  private final byte[] aiS;
  private final Bitmap aiT;
  private final String packageName;
  private final String title;
  public final int version;
  private final PackageInfo zzcjv;
  
  AppInfo(int paramInt1, String paramString1, String paramString2, BitmapTeleporter paramBitmapTeleporter, List<Route> paramList, List<AtomInfo> paramList1, int paramInt2, byte[] paramArrayOfByte, PackageInfo paramPackageInfo)
  {
    this.version = paramInt1;
    this.packageName = paramString1;
    this.title = paramString2;
    this.aiO = paramBitmapTeleporter;
    this.aiP = paramList;
    this.aiQ = paramList1;
    this.aiR = paramInt2;
    this.aiS = paramArrayOfByte;
    this.zzcjv = paramPackageInfo;
    if (paramBitmapTeleporter == null)
    {
      this.aiT = null;
      return;
    }
    this.aiT = paramBitmapTeleporter.zzauj();
  }
  
  public String getPackageName()
  {
    return this.packageName;
  }
  
  public List<Route> getRoutes()
  {
    return this.aiP;
  }
  
  public String getTitle()
  {
    return this.title;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zza.zza(this, paramParcel, paramInt);
  }
  
  public List<AtomInfo> zzbou()
  {
    return this.aiQ;
  }
  
  public int zzbov()
  {
    return this.aiR;
  }
  
  public byte[] zzbow()
  {
    return this.aiS;
  }
  
  public PackageInfo zzbox()
  {
    return this.zzcjv;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/instantapps/internal/AppInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */