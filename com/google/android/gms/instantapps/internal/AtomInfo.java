package com.google.android.gms.instantapps.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public class AtomInfo
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<AtomInfo> CREATOR = new zzb();
  private final String aiU;
  private final String aiV;
  private final String aiW;
  private final String[] aiX;
  public final int version;
  
  AtomInfo(int paramInt, String paramString1, String paramString2, String paramString3, String[] paramArrayOfString)
  {
    this.version = paramInt;
    this.aiU = paramString1;
    this.aiV = paramString2;
    this.aiW = paramString3;
    this.aiX = paramArrayOfString;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzb.zza(this, paramParcel, paramInt);
  }
  
  public String zzboy()
  {
    return this.aiU;
  }
  
  public String zzboz()
  {
    return this.aiV;
  }
  
  public String zzbpa()
  {
    return this.aiW;
  }
  
  public String[] zzbpb()
  {
    return this.aiX;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/instantapps/internal/AtomInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */