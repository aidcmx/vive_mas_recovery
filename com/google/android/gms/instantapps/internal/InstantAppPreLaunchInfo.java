package com.google.android.gms.instantapps.internal;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public class InstantAppPreLaunchInfo
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<InstantAppPreLaunchInfo> CREATOR = new zzf();
  private final String Ie;
  private final int aiZ;
  private final boolean aja;
  private final Intent ajb;
  private final Intent ajc;
  private final byte[] ajd;
  private final AppInfo aje;
  private final Route ajf;
  public final int version;
  
  InstantAppPreLaunchInfo(int paramInt1, int paramInt2, String paramString, boolean paramBoolean, Intent paramIntent1, Intent paramIntent2, byte[] paramArrayOfByte, AppInfo paramAppInfo, Route paramRoute)
  {
    this.version = paramInt1;
    this.aiZ = paramInt2;
    this.Ie = paramString;
    this.aja = paramBoolean;
    this.ajb = paramIntent1;
    this.ajc = paramIntent2;
    this.ajd = paramArrayOfByte;
    this.aje = paramAppInfo;
    this.ajf = paramRoute;
  }
  
  public String getAccountName()
  {
    return this.Ie;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzf.zza(this, paramParcel, paramInt);
  }
  
  public int zzbpc()
  {
    return this.aiZ;
  }
  
  public boolean zzbpd()
  {
    return this.aja;
  }
  
  public Intent zzbpe()
  {
    return this.ajb;
  }
  
  public Intent zzbpf()
  {
    return this.ajc;
  }
  
  public byte[] zzbpg()
  {
    return this.ajd;
  }
  
  public AppInfo zzbph()
  {
    return this.aje;
  }
  
  public Route zzbpi()
  {
    return this.ajf;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/instantapps/internal/InstantAppPreLaunchInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */