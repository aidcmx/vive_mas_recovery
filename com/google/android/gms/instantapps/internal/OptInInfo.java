package com.google.android.gms.instantapps.internal;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public class OptInInfo
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<OptInInfo> CREATOR = new zzj();
  private final String Ie;
  private final int ajm;
  private final Account[] ajn;
  public final int version;
  
  OptInInfo(int paramInt1, int paramInt2, String paramString, Account[] paramArrayOfAccount)
  {
    this.version = paramInt1;
    this.ajm = paramInt2;
    this.Ie = paramString;
    this.ajn = paramArrayOfAccount;
  }
  
  public String getAccountName()
  {
    return this.Ie;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzj.zza(this, paramParcel, paramInt);
  }
  
  public int zzbpj()
  {
    return this.ajm;
  }
  
  public Account[] zzbpk()
  {
    return this.ajn;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/instantapps/internal/OptInInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */