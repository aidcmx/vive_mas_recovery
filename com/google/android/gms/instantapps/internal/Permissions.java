package com.google.android.gms.instantapps.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public final class Permissions
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<Permissions> CREATOR = new zzk();
  private final String[] ajo;
  private final String[] ajp;
  private final String[] ajq;
  final int versionCode;
  
  Permissions(int paramInt, String[] paramArrayOfString1, String[] paramArrayOfString2, String[] paramArrayOfString3)
  {
    this.versionCode = paramInt;
    this.ajo = paramArrayOfString1;
    this.ajp = paramArrayOfString2;
    this.ajq = paramArrayOfString3;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzk.zza(this, paramParcel, paramInt);
  }
  
  public String[] zzbpl()
  {
    return this.ajo;
  }
  
  public String[] zzbpm()
  {
    return this.ajp;
  }
  
  public String[] zzbpn()
  {
    return this.ajq;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/instantapps/internal/Permissions.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */