package com.google.android.gms.instantapps.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.PatternMatcher;
import android.text.TextUtils;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;

public class Route
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<Route> CREATOR = new zzl();
  private final String aiU;
  private final String ajr;
  private final String ajs;
  private final String ajt;
  private final String aju;
  private final String ajv;
  private final PatternMatcher ajw;
  private final int port;
  public final int version;
  
  Route(int paramInt1, String paramString1, int paramInt2, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6)
  {
    this.version = paramInt1;
    this.ajr = zzaa.zzib(paramString1);
    this.port = paramInt2;
    this.ajs = paramString2;
    this.ajt = paramString3;
    this.aju = paramString4;
    this.aiU = paramString5;
    this.ajv = paramString6;
    if (!TextUtils.isEmpty(paramString2))
    {
      this.ajw = new PatternMatcher(paramString2, 0);
      return;
    }
    if (!TextUtils.isEmpty(paramString3))
    {
      this.ajw = new PatternMatcher(paramString3, 1);
      return;
    }
    if (!TextUtils.isEmpty(paramString4))
    {
      this.ajw = new PatternMatcher(paramString4, 2);
      return;
    }
    this.ajw = null;
  }
  
  public String getPath()
  {
    return this.ajs;
  }
  
  public int getPort()
  {
    return this.port;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzl.zza(this, paramParcel, paramInt);
  }
  
  public String zzboy()
  {
    return this.aiU;
  }
  
  public String zzbpo()
  {
    return this.ajr;
  }
  
  public String zzbpp()
  {
    return this.ajt;
  }
  
  public String zzbpq()
  {
    return this.aju;
  }
  
  public String zzbpr()
  {
    return this.ajv;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/instantapps/internal/Route.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */