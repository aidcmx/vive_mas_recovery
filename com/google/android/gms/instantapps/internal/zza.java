package com.google.android.gms.instantapps.internal;

import android.content.pm.PackageInfo;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.data.BitmapTeleporter;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.ArrayList;

public class zza
  implements Parcelable.Creator<AppInfo>
{
  static void zza(AppInfo paramAppInfo, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramAppInfo.version);
    zzb.zza(paramParcel, 2, paramAppInfo.getPackageName(), false);
    zzb.zza(paramParcel, 3, paramAppInfo.getTitle(), false);
    zzb.zza(paramParcel, 4, paramAppInfo.aiO, paramInt, false);
    zzb.zzc(paramParcel, 5, paramAppInfo.getRoutes(), false);
    zzb.zzc(paramParcel, 6, paramAppInfo.zzbou(), false);
    zzb.zzc(paramParcel, 7, paramAppInfo.zzbov());
    zzb.zza(paramParcel, 8, paramAppInfo.zzbow(), false);
    zzb.zza(paramParcel, 9, paramAppInfo.zzbox(), paramInt, false);
    zzb.zzaj(paramParcel, i);
  }
  
  public AppInfo zzni(Parcel paramParcel)
  {
    int i = 0;
    PackageInfo localPackageInfo = null;
    int k = com.google.android.gms.common.internal.safeparcel.zza.zzcr(paramParcel);
    byte[] arrayOfByte = null;
    ArrayList localArrayList1 = null;
    ArrayList localArrayList2 = null;
    BitmapTeleporter localBitmapTeleporter = null;
    String str1 = null;
    String str2 = null;
    int j = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = com.google.android.gms.common.internal.safeparcel.zza.zzcq(paramParcel);
      switch (com.google.android.gms.common.internal.safeparcel.zza.zzgu(m))
      {
      default: 
        com.google.android.gms.common.internal.safeparcel.zza.zzb(paramParcel, m);
        break;
      case 1: 
        j = com.google.android.gms.common.internal.safeparcel.zza.zzg(paramParcel, m);
        break;
      case 2: 
        str2 = com.google.android.gms.common.internal.safeparcel.zza.zzq(paramParcel, m);
        break;
      case 3: 
        str1 = com.google.android.gms.common.internal.safeparcel.zza.zzq(paramParcel, m);
        break;
      case 4: 
        localBitmapTeleporter = (BitmapTeleporter)com.google.android.gms.common.internal.safeparcel.zza.zza(paramParcel, m, BitmapTeleporter.CREATOR);
        break;
      case 5: 
        localArrayList2 = com.google.android.gms.common.internal.safeparcel.zza.zzc(paramParcel, m, Route.CREATOR);
        break;
      case 6: 
        localArrayList1 = com.google.android.gms.common.internal.safeparcel.zza.zzc(paramParcel, m, AtomInfo.CREATOR);
        break;
      case 7: 
        i = com.google.android.gms.common.internal.safeparcel.zza.zzg(paramParcel, m);
        break;
      case 8: 
        arrayOfByte = com.google.android.gms.common.internal.safeparcel.zza.zzt(paramParcel, m);
        break;
      case 9: 
        localPackageInfo = (PackageInfo)com.google.android.gms.common.internal.safeparcel.zza.zza(paramParcel, m, PackageInfo.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza(37 + "Overread allowed size end=" + k, paramParcel);
    }
    return new AppInfo(j, str2, str1, localBitmapTeleporter, localArrayList2, localArrayList1, i, arrayOfByte, localPackageInfo);
  }
  
  public AppInfo[] zzty(int paramInt)
  {
    return new AppInfo[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/instantapps/internal/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */