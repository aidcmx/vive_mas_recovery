package com.google.android.gms.instantapps.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;

public class zzb
  implements Parcelable.Creator<AtomInfo>
{
  static void zza(AtomInfo paramAtomInfo, Parcel paramParcel, int paramInt)
  {
    paramInt = com.google.android.gms.common.internal.safeparcel.zzb.zzcs(paramParcel);
    com.google.android.gms.common.internal.safeparcel.zzb.zzc(paramParcel, 1, paramAtomInfo.version);
    com.google.android.gms.common.internal.safeparcel.zzb.zza(paramParcel, 2, paramAtomInfo.zzboy(), false);
    com.google.android.gms.common.internal.safeparcel.zzb.zza(paramParcel, 3, paramAtomInfo.zzboz(), false);
    com.google.android.gms.common.internal.safeparcel.zzb.zza(paramParcel, 4, paramAtomInfo.zzbpa(), false);
    com.google.android.gms.common.internal.safeparcel.zzb.zza(paramParcel, 5, paramAtomInfo.zzbpb(), false);
    com.google.android.gms.common.internal.safeparcel.zzb.zzaj(paramParcel, paramInt);
  }
  
  public AtomInfo zznj(Parcel paramParcel)
  {
    String[] arrayOfString = null;
    int j = zza.zzcr(paramParcel);
    int i = 0;
    String str1 = null;
    String str2 = null;
    String str3 = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.zzcq(paramParcel);
      switch (zza.zzgu(k))
      {
      default: 
        zza.zzb(paramParcel, k);
        break;
      case 1: 
        i = zza.zzg(paramParcel, k);
        break;
      case 2: 
        str3 = zza.zzq(paramParcel, k);
        break;
      case 3: 
        str2 = zza.zzq(paramParcel, k);
        break;
      case 4: 
        str1 = zza.zzq(paramParcel, k);
        break;
      case 5: 
        arrayOfString = zza.zzac(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new AtomInfo(i, str3, str2, str1, arrayOfString);
  }
  
  public AtomInfo[] zztz(int paramInt)
  {
    return new AtomInfo[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/instantapps/internal/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */