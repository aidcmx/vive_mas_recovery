package com.google.android.gms.instantapps.internal;

import android.content.pm.PackageInfo;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.instantapps.LaunchData;

public abstract interface zzd
  extends IInterface
{
  public abstract void zza(Status paramStatus, int paramInt)
    throws RemoteException;
  
  public abstract void zza(Status paramStatus, PackageInfo paramPackageInfo)
    throws RemoteException;
  
  public abstract void zza(Status paramStatus, LaunchData paramLaunchData)
    throws RemoteException;
  
  public abstract void zza(Status paramStatus, InstantAppPreLaunchInfo paramInstantAppPreLaunchInfo)
    throws RemoteException;
  
  public abstract void zza(Status paramStatus, OptInInfo paramOptInInfo)
    throws RemoteException;
  
  public abstract void zza(Status paramStatus, Permissions paramPermissions)
    throws RemoteException;
  
  public abstract void zzua(int paramInt)
    throws RemoteException;
  
  public static abstract class zza
    extends Binder
    implements zzd
  {
    public zza()
    {
      attachInterface(this, "com.google.android.gms.instantapps.internal.IInstantAppsCallbacks");
    }
    
    public static zzd zzgy(IBinder paramIBinder)
    {
      if (paramIBinder == null) {
        return null;
      }
      IInterface localIInterface = paramIBinder.queryLocalInterface("com.google.android.gms.instantapps.internal.IInstantAppsCallbacks");
      if ((localIInterface != null) && ((localIInterface instanceof zzd))) {
        return (zzd)localIInterface;
      }
      return new zza(paramIBinder);
    }
    
    public IBinder asBinder()
    {
      return this;
    }
    
    public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
      throws RemoteException
    {
      switch (paramInt1)
      {
      default: 
        return super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
      case 1598968902: 
        paramParcel2.writeString("com.google.android.gms.instantapps.internal.IInstantAppsCallbacks");
        return true;
      case 2: 
        paramParcel1.enforceInterface("com.google.android.gms.instantapps.internal.IInstantAppsCallbacks");
        if (paramParcel1.readInt() != 0)
        {
          paramParcel2 = (Status)Status.CREATOR.createFromParcel(paramParcel1);
          if (paramParcel1.readInt() == 0) {
            break label153;
          }
        }
        for (paramParcel1 = (InstantAppPreLaunchInfo)InstantAppPreLaunchInfo.CREATOR.createFromParcel(paramParcel1);; paramParcel1 = null)
        {
          zza(paramParcel2, paramParcel1);
          return true;
          paramParcel2 = null;
          break;
        }
      case 9: 
        paramParcel1.enforceInterface("com.google.android.gms.instantapps.internal.IInstantAppsCallbacks");
        if (paramParcel1.readInt() != 0)
        {
          paramParcel2 = (Status)Status.CREATOR.createFromParcel(paramParcel1);
          if (paramParcel1.readInt() == 0) {
            break label217;
          }
        }
        for (paramParcel1 = (Permissions)Permissions.CREATOR.createFromParcel(paramParcel1);; paramParcel1 = null)
        {
          zza(paramParcel2, paramParcel1);
          return true;
          paramParcel2 = null;
          break;
        }
      case 10: 
        paramParcel1.enforceInterface("com.google.android.gms.instantapps.internal.IInstantAppsCallbacks");
        zzua(paramParcel1.readInt());
        return true;
      case 12: 
        paramParcel1.enforceInterface("com.google.android.gms.instantapps.internal.IInstantAppsCallbacks");
        if (paramParcel1.readInt() != 0) {}
        for (paramParcel2 = (Status)Status.CREATOR.createFromParcel(paramParcel1);; paramParcel2 = null)
        {
          zza(paramParcel2, paramParcel1.readInt());
          return true;
        }
      case 13: 
        paramParcel1.enforceInterface("com.google.android.gms.instantapps.internal.IInstantAppsCallbacks");
        if (paramParcel1.readInt() != 0)
        {
          paramParcel2 = (Status)Status.CREATOR.createFromParcel(paramParcel1);
          if (paramParcel1.readInt() == 0) {
            break label339;
          }
        }
        for (paramParcel1 = (OptInInfo)OptInInfo.CREATOR.createFromParcel(paramParcel1);; paramParcel1 = null)
        {
          zza(paramParcel2, paramParcel1);
          return true;
          paramParcel2 = null;
          break;
        }
      case 18: 
        label153:
        label217:
        label339:
        paramParcel1.enforceInterface("com.google.android.gms.instantapps.internal.IInstantAppsCallbacks");
        if (paramParcel1.readInt() != 0)
        {
          paramParcel2 = (Status)Status.CREATOR.createFromParcel(paramParcel1);
          if (paramParcel1.readInt() == 0) {
            break label403;
          }
        }
        label403:
        for (paramParcel1 = (PackageInfo)PackageInfo.CREATOR.createFromParcel(paramParcel1);; paramParcel1 = null)
        {
          zza(paramParcel2, paramParcel1);
          return true;
          paramParcel2 = null;
          break;
        }
      }
      paramParcel1.enforceInterface("com.google.android.gms.instantapps.internal.IInstantAppsCallbacks");
      if (paramParcel1.readInt() != 0)
      {
        paramParcel2 = (Status)Status.CREATOR.createFromParcel(paramParcel1);
        if (paramParcel1.readInt() == 0) {
          break label467;
        }
      }
      label467:
      for (paramParcel1 = (LaunchData)LaunchData.CREATOR.createFromParcel(paramParcel1);; paramParcel1 = null)
      {
        zza(paramParcel2, paramParcel1);
        return true;
        paramParcel2 = null;
        break;
      }
    }
    
    private static class zza
      implements zzd
    {
      private IBinder zzajq;
      
      zza(IBinder paramIBinder)
      {
        this.zzajq = paramIBinder;
      }
      
      public IBinder asBinder()
      {
        return this.zzajq;
      }
      
      /* Error */
      public void zza(Status paramStatus, int paramInt)
        throws RemoteException
      {
        // Byte code:
        //   0: invokestatic 30	android/os/Parcel:obtain	()Landroid/os/Parcel;
        //   3: astore_3
        //   4: aload_3
        //   5: ldc 32
        //   7: invokevirtual 36	android/os/Parcel:writeInterfaceToken	(Ljava/lang/String;)V
        //   10: aload_1
        //   11: ifnull +39 -> 50
        //   14: aload_3
        //   15: iconst_1
        //   16: invokevirtual 40	android/os/Parcel:writeInt	(I)V
        //   19: aload_1
        //   20: aload_3
        //   21: iconst_0
        //   22: invokevirtual 46	com/google/android/gms/common/api/Status:writeToParcel	(Landroid/os/Parcel;I)V
        //   25: aload_3
        //   26: iload_2
        //   27: invokevirtual 40	android/os/Parcel:writeInt	(I)V
        //   30: aload_0
        //   31: getfield 18	com/google/android/gms/instantapps/internal/zzd$zza$zza:zzajq	Landroid/os/IBinder;
        //   34: bipush 12
        //   36: aload_3
        //   37: aconst_null
        //   38: iconst_1
        //   39: invokeinterface 52 5 0
        //   44: pop
        //   45: aload_3
        //   46: invokevirtual 55	android/os/Parcel:recycle	()V
        //   49: return
        //   50: aload_3
        //   51: iconst_0
        //   52: invokevirtual 40	android/os/Parcel:writeInt	(I)V
        //   55: goto -30 -> 25
        //   58: astore_1
        //   59: aload_3
        //   60: invokevirtual 55	android/os/Parcel:recycle	()V
        //   63: aload_1
        //   64: athrow
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	65	0	this	zza
        //   0	65	1	paramStatus	Status
        //   0	65	2	paramInt	int
        //   3	57	3	localParcel	Parcel
        // Exception table:
        //   from	to	target	type
        //   4	10	58	finally
        //   14	25	58	finally
        //   25	45	58	finally
        //   50	55	58	finally
      }
      
      public void zza(Status paramStatus, PackageInfo paramPackageInfo)
        throws RemoteException
      {
        Parcel localParcel = Parcel.obtain();
        for (;;)
        {
          try
          {
            localParcel.writeInterfaceToken("com.google.android.gms.instantapps.internal.IInstantAppsCallbacks");
            if (paramStatus != null)
            {
              localParcel.writeInt(1);
              paramStatus.writeToParcel(localParcel, 0);
              if (paramPackageInfo != null)
              {
                localParcel.writeInt(1);
                paramPackageInfo.writeToParcel(localParcel, 0);
                this.zzajq.transact(18, localParcel, null, 1);
              }
            }
            else
            {
              localParcel.writeInt(0);
              continue;
            }
            localParcel.writeInt(0);
          }
          finally
          {
            localParcel.recycle();
          }
        }
      }
      
      public void zza(Status paramStatus, LaunchData paramLaunchData)
        throws RemoteException
      {
        Parcel localParcel = Parcel.obtain();
        for (;;)
        {
          try
          {
            localParcel.writeInterfaceToken("com.google.android.gms.instantapps.internal.IInstantAppsCallbacks");
            if (paramStatus != null)
            {
              localParcel.writeInt(1);
              paramStatus.writeToParcel(localParcel, 0);
              if (paramLaunchData != null)
              {
                localParcel.writeInt(1);
                paramLaunchData.writeToParcel(localParcel, 0);
                this.zzajq.transact(19, localParcel, null, 1);
              }
            }
            else
            {
              localParcel.writeInt(0);
              continue;
            }
            localParcel.writeInt(0);
          }
          finally
          {
            localParcel.recycle();
          }
        }
      }
      
      public void zza(Status paramStatus, InstantAppPreLaunchInfo paramInstantAppPreLaunchInfo)
        throws RemoteException
      {
        Parcel localParcel = Parcel.obtain();
        for (;;)
        {
          try
          {
            localParcel.writeInterfaceToken("com.google.android.gms.instantapps.internal.IInstantAppsCallbacks");
            if (paramStatus != null)
            {
              localParcel.writeInt(1);
              paramStatus.writeToParcel(localParcel, 0);
              if (paramInstantAppPreLaunchInfo != null)
              {
                localParcel.writeInt(1);
                paramInstantAppPreLaunchInfo.writeToParcel(localParcel, 0);
                this.zzajq.transact(2, localParcel, null, 1);
              }
            }
            else
            {
              localParcel.writeInt(0);
              continue;
            }
            localParcel.writeInt(0);
          }
          finally
          {
            localParcel.recycle();
          }
        }
      }
      
      public void zza(Status paramStatus, OptInInfo paramOptInInfo)
        throws RemoteException
      {
        Parcel localParcel = Parcel.obtain();
        for (;;)
        {
          try
          {
            localParcel.writeInterfaceToken("com.google.android.gms.instantapps.internal.IInstantAppsCallbacks");
            if (paramStatus != null)
            {
              localParcel.writeInt(1);
              paramStatus.writeToParcel(localParcel, 0);
              if (paramOptInInfo != null)
              {
                localParcel.writeInt(1);
                paramOptInInfo.writeToParcel(localParcel, 0);
                this.zzajq.transact(13, localParcel, null, 1);
              }
            }
            else
            {
              localParcel.writeInt(0);
              continue;
            }
            localParcel.writeInt(0);
          }
          finally
          {
            localParcel.recycle();
          }
        }
      }
      
      public void zza(Status paramStatus, Permissions paramPermissions)
        throws RemoteException
      {
        Parcel localParcel = Parcel.obtain();
        for (;;)
        {
          try
          {
            localParcel.writeInterfaceToken("com.google.android.gms.instantapps.internal.IInstantAppsCallbacks");
            if (paramStatus != null)
            {
              localParcel.writeInt(1);
              paramStatus.writeToParcel(localParcel, 0);
              if (paramPermissions != null)
              {
                localParcel.writeInt(1);
                paramPermissions.writeToParcel(localParcel, 0);
                this.zzajq.transact(9, localParcel, null, 1);
              }
            }
            else
            {
              localParcel.writeInt(0);
              continue;
            }
            localParcel.writeInt(0);
          }
          finally
          {
            localParcel.recycle();
          }
        }
      }
      
      public void zzua(int paramInt)
        throws RemoteException
      {
        Parcel localParcel = Parcel.obtain();
        try
        {
          localParcel.writeInterfaceToken("com.google.android.gms.instantapps.internal.IInstantAppsCallbacks");
          localParcel.writeInt(paramInt);
          this.zzajq.transact(10, localParcel, null, 1);
          return;
        }
        finally
        {
          localParcel.recycle();
        }
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/instantapps/internal/zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */