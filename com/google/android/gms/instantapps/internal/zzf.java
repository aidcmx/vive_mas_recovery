package com.google.android.gms.instantapps.internal;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzf
  implements Parcelable.Creator<InstantAppPreLaunchInfo>
{
  static void zza(InstantAppPreLaunchInfo paramInstantAppPreLaunchInfo, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramInstantAppPreLaunchInfo.version);
    zzb.zzc(paramParcel, 2, paramInstantAppPreLaunchInfo.zzbpc());
    zzb.zza(paramParcel, 3, paramInstantAppPreLaunchInfo.getAccountName(), false);
    zzb.zza(paramParcel, 4, paramInstantAppPreLaunchInfo.zzbpd());
    zzb.zza(paramParcel, 5, paramInstantAppPreLaunchInfo.zzbpe(), paramInt, false);
    zzb.zza(paramParcel, 6, paramInstantAppPreLaunchInfo.zzbpf(), paramInt, false);
    zzb.zza(paramParcel, 7, paramInstantAppPreLaunchInfo.zzbpg(), false);
    zzb.zza(paramParcel, 8, paramInstantAppPreLaunchInfo.zzbph(), paramInt, false);
    zzb.zza(paramParcel, 9, paramInstantAppPreLaunchInfo.zzbpi(), paramInt, false);
    zzb.zzaj(paramParcel, i);
  }
  
  public InstantAppPreLaunchInfo zznk(Parcel paramParcel)
  {
    boolean bool = false;
    Route localRoute = null;
    int k = zza.zzcr(paramParcel);
    AppInfo localAppInfo = null;
    byte[] arrayOfByte = null;
    Intent localIntent1 = null;
    Intent localIntent2 = null;
    String str = null;
    int i = 0;
    int j = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.zzcq(paramParcel);
      switch (zza.zzgu(m))
      {
      default: 
        zza.zzb(paramParcel, m);
        break;
      case 1: 
        j = zza.zzg(paramParcel, m);
        break;
      case 2: 
        i = zza.zzg(paramParcel, m);
        break;
      case 3: 
        str = zza.zzq(paramParcel, m);
        break;
      case 4: 
        bool = zza.zzc(paramParcel, m);
        break;
      case 5: 
        localIntent2 = (Intent)zza.zza(paramParcel, m, Intent.CREATOR);
        break;
      case 6: 
        localIntent1 = (Intent)zza.zza(paramParcel, m, Intent.CREATOR);
        break;
      case 7: 
        arrayOfByte = zza.zzt(paramParcel, m);
        break;
      case 8: 
        localAppInfo = (AppInfo)zza.zza(paramParcel, m, AppInfo.CREATOR);
        break;
      case 9: 
        localRoute = (Route)zza.zza(paramParcel, m, Route.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza(37 + "Overread allowed size end=" + k, paramParcel);
    }
    return new InstantAppPreLaunchInfo(j, i, str, bool, localIntent2, localIntent1, arrayOfByte, localAppInfo, localRoute);
  }
  
  public InstantAppPreLaunchInfo[] zzub(int paramInt)
  {
    return new InstantAppPreLaunchInfo[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/instantapps/internal/zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */