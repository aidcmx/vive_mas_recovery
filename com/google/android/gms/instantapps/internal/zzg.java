package com.google.android.gms.instantapps.internal;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.instantapps.InstantApps;
import com.google.android.gms.instantapps.InstantAppsApi;
import com.google.android.gms.instantapps.InstantAppsApi.LaunchDataResult;
import com.google.android.gms.instantapps.LaunchData;
import com.google.android.gms.instantapps.LaunchSettings;
import com.google.android.gms.internal.zzqo.zza;

public class zzg
  implements InstantAppsApi
{
  public Intent getInstantAppIntent(Context paramContext, String paramString, Intent paramIntent)
  {
    return zzi.getInstantAppIntent(paramContext, paramString, paramIntent);
  }
  
  public PendingResult<InstantAppsApi.LaunchDataResult> getInstantAppLaunchData(GoogleApiClient paramGoogleApiClient, final String paramString, final LaunchSettings paramLaunchSettings)
  {
    zzaa.zzy(paramGoogleApiClient);
    zzaa.zzy(paramString);
    paramGoogleApiClient.zza(new zzb(paramGoogleApiClient)
    {
      protected void zza(Context paramAnonymousContext, zze paramAnonymouszze)
        throws RemoteException
      {
        paramAnonymouszze.zza(new zzg.zza()
        {
          public void zza(Status paramAnonymous2Status, LaunchData paramAnonymous2LaunchData)
          {
            zzg.1.this.zzc(new zzg.zzc(paramAnonymous2Status, paramAnonymous2LaunchData));
          }
        }, paramString, paramLaunchSettings);
      }
      
      protected InstantAppsApi.LaunchDataResult zzdo(Status paramAnonymousStatus)
      {
        return new zzg.zzc(paramAnonymousStatus, null);
      }
    });
  }
  
  public boolean initializeIntentClient(Context paramContext)
  {
    return zzi.zzdj(paramContext);
  }
  
  static class zza
    extends zzd.zza
  {
    public void zza(Status paramStatus, int paramInt)
    {
      throw new UnsupportedOperationException();
    }
    
    public void zza(Status paramStatus, PackageInfo paramPackageInfo)
    {
      throw new UnsupportedOperationException();
    }
    
    public void zza(Status paramStatus, LaunchData paramLaunchData)
    {
      throw new UnsupportedOperationException();
    }
    
    public void zza(Status paramStatus, InstantAppPreLaunchInfo paramInstantAppPreLaunchInfo)
    {
      throw new UnsupportedOperationException();
    }
    
    public void zza(Status paramStatus, OptInInfo paramOptInInfo)
    {
      throw new UnsupportedOperationException();
    }
    
    public void zza(Status paramStatus, Permissions paramPermissions)
    {
      throw new UnsupportedOperationException();
    }
    
    public void zzua(int paramInt)
    {
      throw new UnsupportedOperationException();
    }
  }
  
  static abstract class zzb<R extends Result>
    extends zzqo.zza<R, zzh>
  {
    zzb(GoogleApiClient paramGoogleApiClient)
    {
      super(paramGoogleApiClient);
    }
    
    protected abstract void zza(Context paramContext, zze paramzze)
      throws RemoteException;
    
    protected final void zza(zzh paramzzh)
      throws RemoteException
    {
      zza(paramzzh.getContext(), (zze)paramzzh.zzavg());
    }
  }
  
  static class zzc
    implements InstantAppsApi.LaunchDataResult
  {
    private final LaunchData ajj;
    private final Status gy;
    
    zzc(Status paramStatus, LaunchData paramLaunchData)
    {
      this.gy = paramStatus;
      this.ajj = paramLaunchData;
    }
    
    public LaunchData getLaunchData()
    {
      return this.ajj;
    }
    
    public Status getStatus()
    {
      return this.gy;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/instantapps/internal/zzg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */