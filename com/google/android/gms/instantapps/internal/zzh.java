package com.google.android.gms.instantapps.internal;

import android.content.Context;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.NonNull;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzj;

public class zzh
  extends zzj<zze>
{
  public zzh(Context paramContext, Looper paramLooper, GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
  {
    super(paramContext, paramLooper, 121, zzf.zzca(paramContext), paramConnectionCallbacks, paramOnConnectionFailedListener);
  }
  
  protected zze zzha(IBinder paramIBinder)
  {
    return zze.zza.zzgz(paramIBinder);
  }
  
  @NonNull
  protected String zzjx()
  {
    return "com.google.android.gms.instantapps.START";
  }
  
  @NonNull
  protected String zzjy()
  {
    return "com.google.android.gms.instantapps.internal.IInstantAppsService";
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/instantapps/internal/zzh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */