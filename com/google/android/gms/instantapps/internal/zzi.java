package com.google.android.gms.instantapps.internal;

import android.annotation.TargetApi;
import android.content.ContentProviderClient;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Parcelable;
import android.os.RemoteException;
import android.util.Log;

public class zzi
{
  private static Boolean ajk = null;
  private static ContentProviderClient ajl;
  
  static Intent getInstantAppIntent(Context paramContext, String paramString, Intent paramIntent)
  {
    return zza(paramContext, paramString, paramIntent, true);
  }
  
  static void reset()
  {
    try
    {
      if (ajl != null)
      {
        ajl.release();
        ajl = null;
      }
      ajk = null;
      return;
    }
    finally {}
  }
  
  @TargetApi(11)
  private static Intent zza(Context paramContext, String paramString, Parcelable paramParcelable, boolean paramBoolean)
  {
    if ((paramContext == null) || (paramString == null)) {
      throw new IllegalArgumentException("Parameter is null");
    }
    if (!zzdj(paramContext)) {
      return null;
    }
    Bundle localBundle;
    if (paramParcelable != null)
    {
      localBundle = new Bundle(1);
      localBundle.putParcelable("key_fallbackIntent", paramParcelable);
    }
    for (;;)
    {
      try
      {
        localBundle = zza(paramContext, "method_getInstantAppIntent", paramString, localBundle);
        if (localBundle == null) {
          return null;
        }
      }
      catch (DeadObjectException localDeadObjectException)
      {
        Log.e("InstantAppsApi", String.format("While calling %s %s:\n", new Object[] { zzc.aiY, "method_getInstantAppIntent" }), localDeadObjectException);
        reset();
        if (paramBoolean) {
          return zza(paramContext, paramString, paramParcelable, false);
        }
        return null;
      }
      catch (RemoteException paramContext)
      {
        Log.e("InstantAppsApi", String.format("While calling %s %s:\n", new Object[] { zzc.aiY, "method_getInstantAppIntent" }), paramContext);
        return null;
        return (Intent)localDeadObjectException.getParcelable("key_instantAppIntent");
      }
      catch (IllegalArgumentException paramContext)
      {
        continue;
      }
      Object localObject = null;
    }
  }
  
  /* Error */
  @TargetApi(11)
  private static Bundle zza(Context paramContext, String paramString1, String paramString2, Bundle paramBundle)
    throws RemoteException
  {
    // Byte code:
    //   0: ldc 2
    //   2: monitorenter
    //   3: getstatic 22	com/google/android/gms/instantapps/internal/zzi:ajl	Landroid/content/ContentProviderClient;
    //   6: ifnonnull +22 -> 28
    //   9: aload_0
    //   10: invokevirtual 102	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   13: getstatic 73	com/google/android/gms/instantapps/internal/zzc:aiY	Landroid/net/Uri;
    //   16: aload_1
    //   17: aload_2
    //   18: aload_3
    //   19: invokevirtual 108	android/content/ContentResolver:call	(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    //   22: astore_0
    //   23: ldc 2
    //   25: monitorexit
    //   26: aload_0
    //   27: areturn
    //   28: aload_1
    //   29: aload_2
    //   30: aload_3
    //   31: invokestatic 112	com/google/android/gms/instantapps/internal/zzi:zzd	(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    //   34: astore_0
    //   35: goto -12 -> 23
    //   38: astore_0
    //   39: ldc 2
    //   41: monitorexit
    //   42: aload_0
    //   43: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	44	0	paramContext	Context
    //   0	44	1	paramString1	String
    //   0	44	2	paramString2	String
    //   0	44	3	paramBundle	Bundle
    // Exception table:
    //   from	to	target	type
    //   3	23	38	finally
    //   28	35	38	finally
  }
  
  @TargetApi(17)
  private static Bundle zzd(String paramString1, String paramString2, Bundle paramBundle)
    throws RemoteException
  {
    try
    {
      paramString1 = ajl.call(paramString1, paramString2, paramBundle);
      return paramString1;
    }
    finally
    {
      paramString1 = finally;
      throw paramString1;
    }
  }
  
  /* Error */
  @TargetApi(17)
  private static boolean zzdi(Context paramContext)
  {
    // Byte code:
    //   0: ldc 2
    //   2: monitorenter
    //   3: getstatic 22	com/google/android/gms/instantapps/internal/zzi:ajl	Landroid/content/ContentProviderClient;
    //   6: ifnonnull +16 -> 22
    //   9: aload_0
    //   10: invokevirtual 102	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   13: getstatic 73	com/google/android/gms/instantapps/internal/zzc:aiY	Landroid/net/Uri;
    //   16: invokevirtual 121	android/content/ContentResolver:acquireUnstableContentProviderClient	(Landroid/net/Uri;)Landroid/content/ContentProviderClient;
    //   19: putstatic 22	com/google/android/gms/instantapps/internal/zzi:ajl	Landroid/content/ContentProviderClient;
    //   22: getstatic 22	com/google/android/gms/instantapps/internal/zzi:ajl	Landroid/content/ContentProviderClient;
    //   25: astore_0
    //   26: aload_0
    //   27: ifnull +10 -> 37
    //   30: iconst_1
    //   31: istore_1
    //   32: ldc 2
    //   34: monitorexit
    //   35: iload_1
    //   36: ireturn
    //   37: iconst_0
    //   38: istore_1
    //   39: goto -7 -> 32
    //   42: astore_0
    //   43: ldc 2
    //   45: monitorexit
    //   46: aload_0
    //   47: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	48	0	paramContext	Context
    //   31	8	1	bool	boolean
    // Exception table:
    //   from	to	target	type
    //   3	22	42	finally
    //   22	26	42	finally
  }
  
  @TargetApi(11)
  static boolean zzdj(Context paramContext)
  {
    if (paramContext == null) {
      try
      {
        throw new IllegalArgumentException("Parameter is null");
      }
      finally {}
    }
    if (ajk != null) {}
    for (boolean bool = ajk.booleanValue();; bool = ajk.booleanValue())
    {
      return bool;
      ajk = Boolean.valueOf(zzdk(paramContext));
    }
  }
  
  private static boolean zzdk(Context paramContext)
  {
    boolean bool2 = false;
    for (;;)
    {
      boolean bool1;
      try
      {
        int i = Build.VERSION.SDK_INT;
        if (i < 11)
        {
          bool1 = bool2;
          return bool1;
        }
        bool1 = bool2;
        if (!zzm.zzdl(paramContext)) {
          continue;
        }
        ProviderInfo localProviderInfo = paramContext.getPackageManager().resolveContentProvider(zzc.aiY.getAuthority(), 0);
        bool1 = bool2;
        if (localProviderInfo == null) {
          continue;
        }
        if (localProviderInfo.packageName.equals("com.google.android.gms")) {
          break label121;
        }
        paramContext = String.valueOf(localProviderInfo.packageName);
        if (paramContext.length() != 0)
        {
          paramContext = "Incorrect package name for instant apps content provider: ".concat(paramContext);
          Log.e("InstantAppsApi", paramContext);
          bool1 = bool2;
          continue;
        }
        paramContext = new String("Incorrect package name for instant apps content provider: ");
      }
      finally {}
      continue;
      label121:
      if (Build.VERSION.SDK_INT >= 17)
      {
        boolean bool3 = zzdi(paramContext);
        bool1 = bool2;
        if (!bool3) {}
      }
      else
      {
        bool1 = true;
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/instantapps/internal/zzi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */