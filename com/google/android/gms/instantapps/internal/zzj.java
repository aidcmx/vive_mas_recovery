package com.google.android.gms.instantapps.internal;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzj
  implements Parcelable.Creator<OptInInfo>
{
  static void zza(OptInInfo paramOptInInfo, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramOptInInfo.version);
    zzb.zzc(paramParcel, 2, paramOptInInfo.zzbpj());
    zzb.zza(paramParcel, 3, paramOptInInfo.getAccountName(), false);
    zzb.zza(paramParcel, 4, paramOptInInfo.zzbpk(), paramInt, false);
    zzb.zzaj(paramParcel, i);
  }
  
  public OptInInfo zznl(Parcel paramParcel)
  {
    Account[] arrayOfAccount = null;
    int j = 0;
    int k = zza.zzcr(paramParcel);
    String str = null;
    int i = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.zzcq(paramParcel);
      switch (zza.zzgu(m))
      {
      default: 
        zza.zzb(paramParcel, m);
        break;
      case 1: 
        i = zza.zzg(paramParcel, m);
        break;
      case 2: 
        j = zza.zzg(paramParcel, m);
        break;
      case 3: 
        str = zza.zzq(paramParcel, m);
        break;
      case 4: 
        arrayOfAccount = (Account[])zza.zzb(paramParcel, m, Account.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza(37 + "Overread allowed size end=" + k, paramParcel);
    }
    return new OptInInfo(i, j, str, arrayOfAccount);
  }
  
  public OptInInfo[] zzuc(int paramInt)
  {
    return new OptInInfo[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/instantapps/internal/zzj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */