package com.google.android.gms.instantapps.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzk
  implements Parcelable.Creator<Permissions>
{
  static void zza(Permissions paramPermissions, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.zzcs(paramParcel);
    zzb.zza(paramParcel, 1, paramPermissions.zzbpl(), false);
    zzb.zza(paramParcel, 2, paramPermissions.zzbpm(), false);
    zzb.zza(paramParcel, 3, paramPermissions.zzbpn(), false);
    zzb.zzc(paramParcel, 1000, paramPermissions.versionCode);
    zzb.zzaj(paramParcel, paramInt);
  }
  
  public Permissions zznm(Parcel paramParcel)
  {
    String[] arrayOfString3 = null;
    int j = zza.zzcr(paramParcel);
    int i = 0;
    String[] arrayOfString2 = null;
    String[] arrayOfString1 = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.zzcq(paramParcel);
      switch (zza.zzgu(k))
      {
      default: 
        zza.zzb(paramParcel, k);
        break;
      case 1: 
        arrayOfString1 = zza.zzac(paramParcel, k);
        break;
      case 2: 
        arrayOfString2 = zza.zzac(paramParcel, k);
        break;
      case 3: 
        arrayOfString3 = zza.zzac(paramParcel, k);
        break;
      case 1000: 
        i = zza.zzg(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new Permissions(i, arrayOfString1, arrayOfString2, arrayOfString3);
  }
  
  public Permissions[] zzud(int paramInt)
  {
    return new Permissions[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/instantapps/internal/zzk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */