package com.google.android.gms.instantapps.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzl
  implements Parcelable.Creator<Route>
{
  static void zza(Route paramRoute, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramRoute.version);
    zzb.zza(paramParcel, 2, paramRoute.zzbpo(), false);
    zzb.zzc(paramParcel, 3, paramRoute.getPort());
    zzb.zza(paramParcel, 4, paramRoute.getPath(), false);
    zzb.zza(paramParcel, 5, paramRoute.zzbpp(), false);
    zzb.zza(paramParcel, 6, paramRoute.zzbpq(), false);
    zzb.zza(paramParcel, 7, paramRoute.zzboy(), false);
    zzb.zza(paramParcel, 8, paramRoute.zzbpr(), false);
    zzb.zzaj(paramParcel, paramInt);
  }
  
  public Route zznn(Parcel paramParcel)
  {
    int i = 0;
    String str1 = null;
    int k = zza.zzcr(paramParcel);
    String str2 = null;
    String str3 = null;
    String str4 = null;
    String str5 = null;
    String str6 = null;
    int j = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.zzcq(paramParcel);
      switch (zza.zzgu(m))
      {
      default: 
        zza.zzb(paramParcel, m);
        break;
      case 1: 
        j = zza.zzg(paramParcel, m);
        break;
      case 2: 
        str6 = zza.zzq(paramParcel, m);
        break;
      case 3: 
        i = zza.zzg(paramParcel, m);
        break;
      case 4: 
        str5 = zza.zzq(paramParcel, m);
        break;
      case 5: 
        str4 = zza.zzq(paramParcel, m);
        break;
      case 6: 
        str3 = zza.zzq(paramParcel, m);
        break;
      case 7: 
        str2 = zza.zzq(paramParcel, m);
        break;
      case 8: 
        str1 = zza.zzq(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza(37 + "Overread allowed size end=" + k, paramParcel);
    }
    return new Route(j, str6, i, str5, str4, str3, str2, str1);
  }
  
  public Route[] zzue(int paramInt)
  {
    return new Route[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/instantapps/internal/zzl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */