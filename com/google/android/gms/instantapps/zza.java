package com.google.android.gms.instantapps;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.data.BitmapTeleporter;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zza
  implements Parcelable.Creator<LaunchData>
{
  static void zza(LaunchData paramLaunchData, Parcel paramParcel, int paramInt)
  {
    int i = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramLaunchData.getVersionCode());
    zzb.zza(paramParcel, 2, paramLaunchData.getIntent(), paramInt, false);
    zzb.zza(paramParcel, 3, paramLaunchData.getPackageName(), false);
    zzb.zza(paramParcel, 4, paramLaunchData.getApplicationLabel(), false);
    zzb.zza(paramParcel, 5, paramLaunchData.aiI, paramInt, false);
    zzb.zzaj(paramParcel, i);
  }
  
  public LaunchData zzng(Parcel paramParcel)
  {
    BitmapTeleporter localBitmapTeleporter = null;
    int j = com.google.android.gms.common.internal.safeparcel.zza.zzcr(paramParcel);
    int i = 0;
    String str1 = null;
    String str2 = null;
    Intent localIntent = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = com.google.android.gms.common.internal.safeparcel.zza.zzcq(paramParcel);
      switch (com.google.android.gms.common.internal.safeparcel.zza.zzgu(k))
      {
      default: 
        com.google.android.gms.common.internal.safeparcel.zza.zzb(paramParcel, k);
        break;
      case 1: 
        i = com.google.android.gms.common.internal.safeparcel.zza.zzg(paramParcel, k);
        break;
      case 2: 
        localIntent = (Intent)com.google.android.gms.common.internal.safeparcel.zza.zza(paramParcel, k, Intent.CREATOR);
        break;
      case 3: 
        str2 = com.google.android.gms.common.internal.safeparcel.zza.zzq(paramParcel, k);
        break;
      case 4: 
        str1 = com.google.android.gms.common.internal.safeparcel.zza.zzq(paramParcel, k);
        break;
      case 5: 
        localBitmapTeleporter = (BitmapTeleporter)com.google.android.gms.common.internal.safeparcel.zza.zza(paramParcel, k, BitmapTeleporter.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza(37 + "Overread allowed size end=" + j, paramParcel);
    }
    return new LaunchData(i, localIntent, str2, str1, localBitmapTeleporter);
  }
  
  public LaunchData[] zztw(int paramInt)
  {
    return new LaunchData[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/instantapps/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */