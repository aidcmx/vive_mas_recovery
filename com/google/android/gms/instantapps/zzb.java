package com.google.android.gms.instantapps;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;

public class zzb
  implements Parcelable.Creator<LaunchSettings>
{
  static void zza(LaunchSettings paramLaunchSettings, Parcel paramParcel, int paramInt)
  {
    int i = com.google.android.gms.common.internal.safeparcel.zzb.zzcs(paramParcel);
    com.google.android.gms.common.internal.safeparcel.zzb.zzc(paramParcel, 1, paramLaunchSettings.getVersionCode());
    com.google.android.gms.common.internal.safeparcel.zzb.zza(paramParcel, 2, paramLaunchSettings.zzboq(), paramInt, false);
    com.google.android.gms.common.internal.safeparcel.zzb.zzc(paramParcel, 3, paramLaunchSettings.zzbor());
    com.google.android.gms.common.internal.safeparcel.zzb.zza(paramParcel, 4, paramLaunchSettings.zzbos());
    com.google.android.gms.common.internal.safeparcel.zzb.zza(paramParcel, 5, paramLaunchSettings.zzbot());
    com.google.android.gms.common.internal.safeparcel.zzb.zzaj(paramParcel, i);
  }
  
  public LaunchSettings zznh(Parcel paramParcel)
  {
    boolean bool1 = false;
    int k = zza.zzcr(paramParcel);
    Uri localUri = null;
    boolean bool2 = false;
    int i = 0;
    int j = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.zzcq(paramParcel);
      switch (zza.zzgu(m))
      {
      default: 
        zza.zzb(paramParcel, m);
        break;
      case 1: 
        j = zza.zzg(paramParcel, m);
        break;
      case 2: 
        localUri = (Uri)zza.zza(paramParcel, m, Uri.CREATOR);
        break;
      case 3: 
        i = zza.zzg(paramParcel, m);
        break;
      case 4: 
        bool2 = zza.zzc(paramParcel, m);
        break;
      case 5: 
        bool1 = zza.zzc(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza(37 + "Overread allowed size end=" + k, paramParcel);
    }
    return new LaunchSettings(j, localUri, i, bool2, bool1);
  }
  
  public LaunchSettings[] zztx(int paramInt)
  {
    return new LaunchSettings[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/instantapps/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */