package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class zzaaa
  extends zzzj
{
  protected zzafk<?> zza(final zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    zzaa.zzy(paramVarArgs);
    boolean bool;
    zzafp localzzafp;
    if ((paramVarArgs.length == 1) || (paramVarArgs.length == 2))
    {
      bool = true;
      zzaa.zzbt(bool);
      zzaa.zzbt(paramVarArgs[0] instanceof zzafp);
      localzzafp = (zzafp)paramVarArgs[0];
      if (paramVarArgs.length != 2) {
        break label94;
      }
      zzaa.zzbt(paramVarArgs[1] instanceof zzafn);
    }
    label94:
    for (final zzafn localzzafn = (zzafn)paramVarArgs[1];; localzzafn = new zzafn(new zzb(null)))
    {
      Collections.sort((List)localzzafp.zzckf(), new zza(localzzafn, paramzzyu));
      return paramVarArgs[0];
      bool = false;
      break;
    }
  }
  
  class zza
    implements Comparator<zzafk<?>>
  {
    public int zze(zzafk<?> paramzzafk1, zzafk<?> paramzzafk2)
    {
      if (paramzzafk1 == null)
      {
        if (paramzzafk2 != null) {
          return 1;
        }
        return 0;
      }
      if (paramzzafk2 == null)
      {
        if (paramzzafk1 != null) {
          return -1;
        }
        return 0;
      }
      paramzzafk1 = ((zzzh)localzzafn.zzckf()).zzb(paramzzyu, new zzafk[] { paramzzafk1, paramzzafk2 });
      zzaa.zzbs(paramzzafk1 instanceof zzafm);
      return (int)((Double)((zzafm)paramzzafk1).zzckf()).doubleValue();
    }
  }
  
  private static class zzb
    implements zzzh
  {
    public zzafk<?> zzb(zzyu paramzzyu, zzafk<?>... paramVarArgs)
    {
      return new zzafm(Double.valueOf(zzzi.zzd(paramVarArgs[0]).compareTo(zzzi.zzd(paramVarArgs[1]))));
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaaa.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */