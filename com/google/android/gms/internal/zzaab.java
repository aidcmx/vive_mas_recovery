package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;
import java.util.ArrayList;
import java.util.List;

public class zzaab
  extends zzzj
{
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    zzaa.zzy(paramVarArgs);
    boolean bool;
    if (paramVarArgs.length >= 3)
    {
      bool = true;
      zzaa.zzbt(bool);
      zzaa.zzbt(paramVarArgs[0] instanceof zzafp);
      paramzzyu = (zzafp)paramVarArgs[0];
      i = (int)zzzi.zzc(paramVarArgs[1]);
      if (i >= 0) {
        break label188;
      }
    }
    ArrayList localArrayList1;
    ArrayList localArrayList2;
    label188:
    for (int i = Math.max(((List)paramzzyu.zzckf()).size() + i, 0);; i = Math.min(i, ((List)paramzzyu.zzckf()).size()))
    {
      int j = i + Math.min(Math.max((int)zzzi.zzc(paramVarArgs[2]), 0), ((List)paramzzyu.zzckf()).size() - i);
      localArrayList1 = new ArrayList(((List)paramzzyu.zzckf()).subList(i, j));
      ((List)paramzzyu.zzckf()).subList(i, j).clear();
      localArrayList2 = new ArrayList();
      j = 3;
      while (j < paramVarArgs.length)
      {
        localArrayList2.add(paramVarArgs[j]);
        j += 1;
      }
      bool = false;
      break;
    }
    ((List)paramzzyu.zzckf()).addAll(i, localArrayList2);
    return new zzafp(localArrayList1);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaab.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */