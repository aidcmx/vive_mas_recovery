package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;
import java.util.ArrayList;
import java.util.List;

public class zzaac
  extends zzzj
{
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    int i = 1;
    zzaa.zzy(paramVarArgs);
    if (paramVarArgs.length >= 1) {}
    ArrayList localArrayList;
    for (boolean bool = true;; bool = false)
    {
      zzaa.zzbt(bool);
      zzaa.zzbt(paramVarArgs[0] instanceof zzafp);
      paramzzyu = (zzafp)paramVarArgs[0];
      localArrayList = new ArrayList();
      while (i < paramVarArgs.length)
      {
        localArrayList.add(paramVarArgs[i]);
        i += 1;
      }
    }
    ((List)paramzzyu.zzckf()).addAll(0, localArrayList);
    return new zzafm(Double.valueOf(((List)paramzzyu.zzckf()).size()));
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaac.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */