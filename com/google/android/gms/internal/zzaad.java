package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;

public class zzaad
  implements zzzh
{
  public zzafk<?> zzb(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    if (paramVarArgs != null)
    {
      bool = true;
      zzaa.zzbt(bool);
      if (paramVarArgs.length != 2) {
        break label47;
      }
    }
    label47:
    for (boolean bool = true;; bool = false)
    {
      zzaa.zzbt(bool);
      zzafk localzzafk = zzaft.zza(paramzzyu, paramVarArgs[0]);
      if (zzzi.zza(localzzafk)) {
        break label52;
      }
      return localzzafk;
      bool = false;
      break;
    }
    label52:
    return zzaft.zza(paramzzyu, paramVarArgs[1]);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaad.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */