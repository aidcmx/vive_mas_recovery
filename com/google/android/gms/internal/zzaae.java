package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;

public class zzaae
  extends zzzj
{
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    if (paramVarArgs != null)
    {
      bool = true;
      zzaa.zzbt(bool);
      if (paramVarArgs.length != 2) {
        break label47;
      }
    }
    label47:
    for (boolean bool = true;; bool = false)
    {
      zzaa.zzbt(bool);
      return new zzafl(Boolean.valueOf(zzzi.zzc(paramVarArgs[0], paramVarArgs[1])));
      bool = false;
      break;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaae.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */