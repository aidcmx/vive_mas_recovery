package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;

public class zzaak
  extends zzzj
{
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    boolean bool2 = true;
    boolean bool3 = false;
    boolean bool1;
    if (paramVarArgs != null)
    {
      bool1 = true;
      zzaa.zzbt(bool1);
      if (paramVarArgs.length != 2) {
        break label167;
      }
      bool1 = true;
      label24:
      zzaa.zzbt(bool1);
      zzafk<?> localzzafk = paramVarArgs[0];
      paramVarArgs = paramVarArgs[1];
      if ((!(localzzafk instanceof zzafq)) && (!(localzzafk instanceof zzafp)))
      {
        paramzzyu = localzzafk;
        if (!(localzzafk instanceof zzafn)) {}
      }
      else
      {
        paramzzyu = new zzafs(zzzi.zzd(localzzafk));
      }
      if ((!(paramVarArgs instanceof zzafq)) && (!(paramVarArgs instanceof zzafp)) && (!(paramVarArgs instanceof zzafn))) {
        break label191;
      }
      paramVarArgs = new zzafs(zzzi.zzd(paramVarArgs));
    }
    label167:
    label191:
    for (;;)
    {
      if ((!(paramzzyu instanceof zzafs)) || (!(paramVarArgs instanceof zzafs)))
      {
        bool1 = bool3;
        if (!Double.isNaN(zzzi.zzb(paramzzyu)))
        {
          if (Double.isNaN(zzzi.zzb(paramVarArgs))) {
            bool1 = bool3;
          }
        }
        else
        {
          return new zzafl(Boolean.valueOf(bool1));
          bool1 = false;
          break;
          bool1 = false;
          break label24;
        }
      }
      if (!zzzi.zzb(paramVarArgs, paramzzyu)) {}
      for (bool1 = bool2;; bool1 = false) {
        break;
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaak.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */