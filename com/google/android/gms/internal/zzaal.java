package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;

public class zzaal
  extends zzzj
{
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    boolean bool2 = true;
    if (paramVarArgs != null)
    {
      bool1 = true;
      zzaa.zzbt(bool1);
      if (paramVarArgs.length != 1) {
        break label52;
      }
    }
    label52:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      zzaa.zzbt(bool1);
      return new zzafm(Double.valueOf(zzzi.zzb(paramVarArgs[0]) * -1.0D));
      bool1 = false;
      break;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaal.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */