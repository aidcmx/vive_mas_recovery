package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;

public class zzaam
  extends zzzj
{
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    boolean bool2 = true;
    if (paramVarArgs != null)
    {
      bool1 = true;
      zzaa.zzbt(bool1);
      if (paramVarArgs.length != 1) {
        break label54;
      }
      bool1 = true;
      label21:
      zzaa.zzbt(bool1);
      if (zzzi.zza(paramVarArgs[0])) {
        break label59;
      }
    }
    label54:
    label59:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      return new zzafl(Boolean.valueOf(bool1));
      bool1 = false;
      break;
      bool1 = false;
      break label21;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaam.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */