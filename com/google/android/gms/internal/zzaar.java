package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;

public class zzaar
  implements zzzh
{
  public zzafk<?> zzb(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    boolean bool;
    if (paramVarArgs != null)
    {
      bool = true;
      zzaa.zzbt(bool);
      if (paramVarArgs.length != 3) {
        break label79;
      }
      bool = true;
      label18:
      zzaa.zzbt(bool);
      if (!zzzi.zza(zzaft.zza(paramzzyu, paramVarArgs[0]))) {
        break label84;
      }
    }
    label79:
    label84:
    for (paramzzyu = zzaft.zza(paramzzyu, paramVarArgs[1]);; paramzzyu = zzaft.zza(paramzzyu, paramVarArgs[2]))
    {
      if ((!(paramzzyu instanceof zzafo)) || (paramzzyu == zzafo.aMi) || (paramzzyu == zzafo.aMh)) {
        return paramzzyu;
      }
      throw new IllegalArgumentException("Illegal InternalType passed to Ternary.");
      bool = false;
      break;
      bool = false;
      break label18;
    }
    return paramzzyu;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaar.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */