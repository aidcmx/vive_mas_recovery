package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;

public class zzaas
  extends zzzj
{
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    boolean bool2 = true;
    boolean bool1;
    if (paramVarArgs != null)
    {
      bool1 = true;
      zzaa.zzbt(bool1);
      if (paramVarArgs.length != 1) {
        break label87;
      }
      bool1 = true;
      label21:
      zzaa.zzbt(bool1);
      if ((paramVarArgs[0] instanceof zzafr)) {
        break label92;
      }
      bool1 = true;
      label36:
      zzaa.zzbt(bool1);
      if (zzaft.zzo(paramVarArgs[0])) {
        break label97;
      }
      bool1 = bool2;
      label52:
      zzaa.zzbt(bool1);
      paramVarArgs = paramVarArgs[0];
      paramzzyu = "object";
      if (paramVarArgs != zzafo.aMi) {
        break label102;
      }
      paramzzyu = "undefined";
    }
    for (;;)
    {
      return new zzafs(paramzzyu);
      bool1 = false;
      break;
      label87:
      bool1 = false;
      break label21;
      label92:
      bool1 = false;
      break label36;
      label97:
      bool1 = false;
      break label52;
      label102:
      if ((paramVarArgs instanceof zzafl)) {
        paramzzyu = "boolean";
      } else if ((paramVarArgs instanceof zzafm)) {
        paramzzyu = "number";
      } else if ((paramVarArgs instanceof zzafs)) {
        paramzzyu = "string";
      } else if ((paramVarArgs instanceof zzafn)) {
        paramzzyu = "function";
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaas.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */