package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;
import java.util.List;

public class zzaat
  extends zzzj
{
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    if (paramVarArgs != null)
    {
      bool = true;
      zzaa.zzbt(bool);
      if (paramVarArgs.length != 3) {
        break label140;
      }
    }
    Object localObject1;
    Object localObject2;
    label140:
    for (boolean bool = true;; bool = false)
    {
      zzaa.zzbt(bool);
      zzaa.zzbt(paramVarArgs[1] instanceof zzafs);
      zzaa.zzbt(paramVarArgs[2] instanceof zzafp);
      localObject1 = paramVarArgs[0];
      localObject2 = (String)((zzafs)paramVarArgs[1]).zzckf();
      paramVarArgs = (List)((zzafp)paramVarArgs[2]).zzckf();
      if (!((zzafk)localObject1).zzre((String)localObject2)) {
        break label189;
      }
      localObject1 = ((zzafk)localObject1).zzrf((String)localObject2);
      if (!(localObject1 instanceof zzafn)) {
        break label145;
      }
      return ((zzzh)((zzafn)localObject1).zzckf()).zzb(paramzzyu, (zzafk[])paramVarArgs.toArray(new zzafk[paramVarArgs.size()]));
      bool = false;
      break;
    }
    label145:
    throw new IllegalArgumentException(String.valueOf(localObject2).length() + 35 + "Apply TypeError: " + (String)localObject2 + " is not a function");
    label189:
    if (((zzafk)localObject1).zzrg((String)localObject2))
    {
      localObject2 = ((zzafk)localObject1).zzrh((String)localObject2);
      paramVarArgs.add(0, localObject1);
      return ((zzzh)localObject2).zzb(paramzzyu, (zzafk[])paramVarArgs.toArray(new zzafk[paramVarArgs.size()]));
    }
    throw new IllegalArgumentException(String.valueOf(localObject2).length() + 40 + "Apply TypeError: object has no " + (String)localObject2 + " property");
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */