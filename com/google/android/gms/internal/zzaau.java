package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;

public class zzaau
  extends zzzj
{
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    if (paramVarArgs != null)
    {
      bool = true;
      zzaa.zzbt(bool);
      if (paramVarArgs.length != 2) {
        break label72;
      }
    }
    String str;
    label72:
    for (boolean bool = true;; bool = false)
    {
      zzaa.zzbt(bool);
      zzaa.zzbt(paramVarArgs[0] instanceof zzafs);
      str = (String)((zzafs)paramVarArgs[0]).zzckf();
      if (!paramzzyu.has(str)) {
        break label77;
      }
      paramzzyu.zzb(str, paramVarArgs[1]);
      return paramVarArgs[1];
      bool = false;
      break;
    }
    label77:
    paramzzyu = String.valueOf(str);
    if (paramzzyu.length() != 0) {}
    for (paramzzyu = "Attempting to assign to undefined variable ".concat(paramzzyu);; paramzzyu = new String("Attempting to assign to undefined variable ")) {
      throw new IllegalStateException(paramzzyu);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaau.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */