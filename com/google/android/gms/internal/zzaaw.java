package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;
import java.util.Iterator;
import java.util.List;

public class zzaaw
  extends zzzj
{
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    boolean bool2 = true;
    if (paramVarArgs != null)
    {
      bool1 = true;
      zzaa.zzbt(bool1);
      if (paramVarArgs.length != 1) {
        break label120;
      }
    }
    label120:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      zzaa.zzbt(bool1);
      zzaa.zzbt(paramVarArgs[0] instanceof zzafp);
      paramVarArgs = ((List)((zzafp)paramVarArgs[0]).zzckf()).iterator();
      zzafk localzzafk;
      do
      {
        if (!paramVarArgs.hasNext()) {
          break;
        }
        localzzafk = zzaft.zza(paramzzyu, (zzafk)paramVarArgs.next());
      } while ((!(localzzafk instanceof zzafo)) || ((localzzafk != zzafo.aMf) && (localzzafk != zzafo.aMg) && (!((zzafo)localzzafk).zzckm())));
      return localzzafk;
      bool1 = false;
      break;
    }
    return zzafo.aMi;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaaw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */