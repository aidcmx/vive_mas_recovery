package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;
import java.util.ArrayList;
import java.util.List;

public class zzaay
  extends zzzj
{
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    int i;
    label26:
    zzafk<?> localzzafk;
    if (paramVarArgs != null)
    {
      bool = true;
      zzaa.zzbt(bool);
      paramzzyu = new ArrayList();
      int j = paramVarArgs.length;
      i = 0;
      if (i >= j) {
        break label97;
      }
      localzzafk = paramVarArgs[i];
      if (((localzzafk instanceof zzafo)) && (localzzafk != zzafo.aMi) && (localzzafk != zzafo.aMh)) {
        break label91;
      }
    }
    label91:
    for (boolean bool = true;; bool = false)
    {
      zzaa.zzbt(bool);
      paramzzyu.add(localzzafk);
      i += 1;
      break label26;
      bool = false;
      break;
    }
    label97:
    return new zzafp(paramzzyu);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaay.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */