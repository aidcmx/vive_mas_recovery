package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;
import java.util.HashMap;
import java.util.Map;

public class zzaaz
  extends zzzj
{
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    int i = 0;
    boolean bool;
    if (paramVarArgs != null)
    {
      bool = true;
      zzaa.zzbt(bool);
      paramzzyu = new HashMap();
    }
    for (;;)
    {
      if (i >= paramVarArgs.length - 1) {
        break label103;
      }
      String str = zzzi.zzd(paramVarArgs[i]);
      zzafk<?> localzzafk = paramVarArgs[(i + 1)];
      if (((localzzafk instanceof zzafo)) && (localzzafk != zzafo.aMh) && (localzzafk != zzafo.aMi))
      {
        throw new IllegalStateException("Illegal InternalType found in CreateObject.");
        bool = false;
        break;
      }
      paramzzyu.put(str, localzzafk);
      i += 2;
    }
    label103:
    return new zzafq(paramzzyu);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaaz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */