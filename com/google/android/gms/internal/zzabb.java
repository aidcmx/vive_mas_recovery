package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;
import java.util.Iterator;
import java.util.List;

public class zzabb
  extends zzzj
{
  public zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    zzaa.zzy(paramVarArgs);
    boolean bool;
    String str;
    Object localObject;
    if (paramVarArgs.length == 3)
    {
      bool = true;
      zzaa.zzbt(bool);
      zzaa.zzbt(paramVarArgs[0] instanceof zzafs);
      str = (String)((zzafs)paramVarArgs[0]).zzckf();
      zzaa.zzbt(paramzzyu.has(str));
      localObject = paramVarArgs[1];
      zzaa.zzy(localObject);
      paramVarArgs = paramVarArgs[2];
      zzaa.zzbt(paramVarArgs instanceof zzafp);
      paramVarArgs = (List)((zzafp)paramVarArgs).zzckf();
      localObject = ((zzafk)localObject).zzcke();
    }
    for (;;)
    {
      if (((Iterator)localObject).hasNext())
      {
        paramzzyu.zzb(str, (zzafk)((Iterator)localObject).next());
        zzafo localzzafo = zzaft.zza(paramzzyu, paramVarArgs);
        if (localzzafo == zzafo.aMf)
        {
          return zzafo.aMi;
          bool = false;
          break;
        }
        if (localzzafo.zzckm()) {
          return localzzafo;
        }
      }
    }
    return zzafo.aMi;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzabb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */