package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;

public class zzabc
  implements zzzh
{
  public zzafk<?> zzb(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    boolean bool2 = true;
    if (paramVarArgs != null)
    {
      bool1 = true;
      zzaa.zzbt(bool1);
      if (paramVarArgs.length != 1) {
        break label74;
      }
    }
    label74:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      zzaa.zzbt(bool1);
      zzaa.zzbt(paramVarArgs[0] instanceof zzafs);
      paramzzyu = paramzzyu.zzqn((String)((zzafs)paramVarArgs[0]).zzckf());
      if (!(paramzzyu instanceof zzafr)) {
        break label79;
      }
      throw new IllegalStateException("Illegal Statement type encountered in Get.");
      bool1 = false;
      break;
    }
    label79:
    if (((paramzzyu instanceof zzafo)) && (paramzzyu != zzafo.aMi) && (paramzzyu != zzafo.aMh)) {
      throw new IllegalStateException("Illegal InternalType encountered in Get.");
    }
    return paramzzyu;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzabc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */