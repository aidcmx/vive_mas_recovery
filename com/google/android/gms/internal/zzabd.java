package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;

public class zzabd
  implements zzzh
{
  private static zzys aIC;
  
  public zzabd(zzys paramzzys)
  {
    aIC = paramzzys;
  }
  
  public zzafk<?> zzb(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    boolean bool2 = true;
    if (paramVarArgs != null)
    {
      bool1 = true;
      zzaa.zzbt(bool1);
      if (paramVarArgs.length != 1) {
        break label59;
      }
    }
    label59:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      zzaa.zzbt(bool1);
      zzaa.zzbt(paramVarArgs[0] instanceof zzafs);
      return aIC.zzqj((String)((zzafs)paramVarArgs[0]).zzckf());
      bool1 = false;
      break;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzabd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */