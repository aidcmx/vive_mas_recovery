package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;
import java.util.List;

public class zzabe
  extends zzzj
{
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    boolean bool2 = true;
    boolean bool1;
    label24:
    zzafk<?> localzzafk;
    label57:
    label73:
    label89:
    String str;
    if (paramVarArgs != null)
    {
      bool1 = true;
      zzaa.zzbt(bool1);
      if (paramVarArgs.length != 2) {
        break label156;
      }
      bool1 = true;
      zzaa.zzbt(bool1);
      localzzafk = paramVarArgs[0];
      paramzzyu = paramVarArgs[1];
      if ((!(localzzafk instanceof zzafs)) && (zzaft.zzn(localzzafk))) {
        break label162;
      }
      bool1 = true;
      zzaa.zzbt(bool1);
      if (zzaft.zzo(localzzafk)) {
        break label168;
      }
      bool1 = true;
      zzaa.zzbt(bool1);
      if (zzaft.zzo(paramzzyu)) {
        break label174;
      }
      bool1 = bool2;
      zzaa.zzbt(bool1);
      str = zzzi.zzd(paramzzyu);
      if (!(localzzafk instanceof zzafp)) {
        break label231;
      }
      paramVarArgs = (zzafp)localzzafk;
      if (!"length".equals(str)) {
        break label180;
      }
      paramzzyu = new zzafm(Double.valueOf(((List)paramVarArgs.zzckf()).size()));
    }
    label156:
    label162:
    label168:
    label174:
    label180:
    do
    {
      return paramzzyu;
      bool1 = false;
      break;
      bool1 = false;
      break label24;
      bool1 = false;
      break label57;
      bool1 = false;
      break label73;
      bool1 = false;
      break label89;
      d = zzzi.zzb(paramzzyu);
      if ((d != Math.floor(d)) || (!str.equals(Integer.toString((int)d)))) {
        break label223;
      }
      paramVarArgs = paramVarArgs.zzaal((int)d);
      paramzzyu = paramVarArgs;
    } while (paramVarArgs != zzafo.aMi);
    label223:
    label231:
    while (!(localzzafk instanceof zzafs)) {
      return localzzafk.zzrf(str);
    }
    paramVarArgs = (zzafs)localzzafk;
    if ("length".equals(str)) {
      return new zzafm(Double.valueOf(((String)paramVarArgs.zzckf()).length()));
    }
    double d = zzzi.zzb(paramzzyu);
    if ((d == Math.floor(d)) && (str.equals(Integer.toString((int)d)))) {
      return paramVarArgs.zzaan((int)d);
    }
    return zzafo.aMi;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzabe.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */