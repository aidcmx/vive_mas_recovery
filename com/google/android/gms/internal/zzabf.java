package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;
import java.util.List;

public class zzabf
  extends zzzj
{
  public static final zzabf aKK = new zzabf();
  
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    if (paramVarArgs != null)
    {
      bool = true;
      zzaa.zzbt(bool);
      if (paramVarArgs.length != 2) {
        break label105;
      }
      bool = true;
      label21:
      zzaa.zzbt(bool);
      paramzzyu = paramVarArgs[0];
      paramVarArgs = paramVarArgs[1];
      if (zzaft.zzo(paramzzyu)) {
        break label111;
      }
      bool = true;
      label44:
      zzaa.zzbt(bool);
      if (zzaft.zzo(paramVarArgs)) {
        break label117;
      }
    }
    String str;
    label105:
    label111:
    label117:
    for (boolean bool = true;; bool = false)
    {
      zzaa.zzbt(bool);
      str = zzzi.zzd(paramVarArgs);
      if (!(paramzzyu instanceof zzafp)) {
        break label191;
      }
      if (!"length".equals(str)) {
        break label123;
      }
      return new zzafl(Boolean.valueOf(true));
      bool = false;
      break;
      bool = false;
      break label21;
      bool = false;
      break label44;
    }
    label123:
    double d = zzzi.zzb(paramVarArgs);
    if ((d == Math.floor(d)) && (str.equals(Integer.toString((int)d))))
    {
      int i = (int)d;
      if ((i >= 0) && (i < ((List)((zzafp)paramzzyu).zzckf()).size()))
      {
        return new zzafl(Boolean.valueOf(true));
        label191:
        if ((paramzzyu instanceof zzafs))
        {
          if ("length".equals(str)) {
            return new zzafl(Boolean.valueOf(true));
          }
          d = zzzi.zzb(paramVarArgs);
          if ((d == Math.floor(d)) && (str.equals(Integer.toString((int)d))))
          {
            i = (int)d;
            if ((i >= 0) && (i < ((String)((zzafs)paramzzyu).zzckf()).length())) {
              return new zzafl(Boolean.valueOf(true));
            }
          }
          return new zzafl(Boolean.valueOf(false));
        }
      }
    }
    return new zzafl(Boolean.valueOf(paramzzyu.zzre(str)));
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzabf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */