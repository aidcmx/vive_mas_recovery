package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;
import java.util.ArrayList;
import java.util.List;

public class zzabg
  extends zzzj
{
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    boolean bool;
    label24:
    Object localObject;
    if (paramVarArgs != null)
    {
      bool = true;
      zzaa.zzbt(bool);
      if ((paramVarArgs.length != 2) && (paramVarArgs.length != 3)) {
        break label112;
      }
      bool = true;
      zzaa.zzbt(bool);
      zzaa.zzbt(paramVarArgs[1] instanceof zzafp);
      if (paramVarArgs.length == 3) {
        zzaa.zzbt(paramVarArgs[2] instanceof zzafp);
      }
      localObject = new ArrayList();
      if (!zzzi.zza(paramVarArgs[0])) {
        break label117;
      }
      localObject = (List)((zzafp)paramVarArgs[1]).zzckf();
    }
    for (;;)
    {
      paramzzyu = zzaft.zza(paramzzyu, (List)localObject);
      if ((!(paramzzyu instanceof zzafo)) || (!zzaft.zzo(paramzzyu))) {
        break label140;
      }
      return paramzzyu;
      bool = false;
      break;
      label112:
      bool = false;
      break label24;
      label117:
      if (paramVarArgs.length > 2) {
        localObject = (List)((zzafp)paramVarArgs[2]).zzckf();
      }
    }
    label140:
    return zzafo.aMi;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzabg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */