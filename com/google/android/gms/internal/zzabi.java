package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;
import java.util.ArrayList;
import java.util.List;

public class zzabi
  implements zzzh
{
  public zzafk<?> zzb(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    int i = 0;
    if (paramVarArgs != null) {}
    for (boolean bool = true;; bool = false)
    {
      zzaa.zzbt(bool);
      paramzzyu = new ArrayList(paramVarArgs.length);
      int j = paramVarArgs.length;
      while (i < j)
      {
        paramzzyu.add(paramVarArgs[i]);
        i += 1;
      }
    }
    return new zzafp(paramzzyu);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzabi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */