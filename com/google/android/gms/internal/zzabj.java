package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;

public class zzabj
  extends zzzj
{
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    boolean bool;
    if (paramVarArgs != null)
    {
      bool = true;
      zzaa.zzbt(bool);
      if (paramVarArgs.length > 1) {
        break label46;
      }
      bool = true;
      label18:
      zzaa.zzbt(bool);
      if (paramVarArgs.length >= 1) {
        break label51;
      }
    }
    label46:
    label51:
    for (paramzzyu = zzafo.aMi;; paramzzyu = paramVarArgs[0])
    {
      return new zzafo(paramzzyu);
      bool = false;
      break;
      bool = false;
      break label18;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzabj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */