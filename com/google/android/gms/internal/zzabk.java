package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;

public class zzabk
  extends zzzj
{
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    boolean bool2 = true;
    label24:
    zzafk<?> localzzafk;
    if (paramVarArgs != null)
    {
      bool1 = true;
      zzaa.zzbt(bool1);
      if (paramVarArgs.length != 3) {
        break label133;
      }
      bool1 = true;
      zzaa.zzbt(bool1);
      paramzzyu = paramVarArgs[0];
      localzzafk = paramVarArgs[1];
      paramVarArgs = paramVarArgs[2];
      if (paramzzyu == zzafo.aMh) {
        break label139;
      }
      bool1 = true;
      label52:
      zzaa.zzbt(bool1);
      if (paramzzyu == zzafo.aMi) {
        break label145;
      }
      bool1 = true;
      label67:
      zzaa.zzbt(bool1);
      if (zzaft.zzo(paramzzyu)) {
        break label151;
      }
      bool1 = true;
      label82:
      zzaa.zzbt(bool1);
      if (zzaft.zzo(localzzafk)) {
        break label157;
      }
      bool1 = true;
      label98:
      zzaa.zzbt(bool1);
      if (zzaft.zzo(paramVarArgs)) {
        break label163;
      }
    }
    label133:
    label139:
    label145:
    label151:
    label157:
    label163:
    for (boolean bool1 = true;; bool1 = false)
    {
      zzaa.zzbt(bool1);
      if (!zzaft.zzn(paramzzyu)) {
        break label169;
      }
      return paramVarArgs;
      bool1 = false;
      break;
      bool1 = false;
      break label24;
      bool1 = false;
      break label52;
      bool1 = false;
      break label67;
      bool1 = false;
      break label82;
      bool1 = false;
      break label98;
    }
    label169:
    String str = zzzi.zzd(localzzafk);
    if ((paramzzyu instanceof zzafq))
    {
      paramzzyu = (zzafq)paramzzyu;
      if (!paramzzyu.zzckq()) {
        paramzzyu.zzc(str, paramVarArgs);
      }
      return paramVarArgs;
    }
    if ((paramzzyu instanceof zzafp))
    {
      zzafp localzzafp = (zzafp)paramzzyu;
      if ("length".equals(str))
      {
        d = zzzi.zzb(paramVarArgs);
        if ((!Double.isInfinite(d)) && (d == Math.floor(d))) {}
        for (bool1 = bool2;; bool1 = false)
        {
          zzaa.zzbt(bool1);
          localzzafp.setSize((int)d);
          return paramVarArgs;
        }
      }
      double d = zzzi.zzb(localzzafk);
      if ((!Double.isInfinite(d)) && (d >= 0.0D) && (str.equals(Integer.toString((int)d))))
      {
        localzzafp.zza((int)d, paramVarArgs);
        return paramVarArgs;
      }
    }
    paramzzyu.zzc(str, paramVarArgs);
    return paramVarArgs;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzabk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */