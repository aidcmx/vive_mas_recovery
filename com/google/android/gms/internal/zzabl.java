package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;
import java.util.List;

public class zzabl
  extends zzzj
{
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    int k = 1;
    label24:
    zzafk<?> localzzafk;
    List localList;
    if (paramVarArgs != null)
    {
      bool = true;
      zzaa.zzbt(bool);
      if (paramVarArgs.length != 3) {
        break label203;
      }
      bool = true;
      zzaa.zzbt(bool);
      zzaa.zzbt(paramVarArgs[1] instanceof zzafp);
      zzaa.zzbt(paramVarArgs[2] instanceof zzafp);
      localzzafk = paramVarArgs[0];
      localList = (List)((zzafp)paramVarArgs[1]).zzckf();
      paramVarArgs = (List)((zzafp)paramVarArgs[2]).zzckf();
      if (paramVarArgs.size() > localList.size() + 1) {
        break label209;
      }
    }
    int j;
    int i;
    zzafk localzzafk1;
    label203:
    label209:
    for (boolean bool = true;; bool = false)
    {
      zzaa.zzbt(bool);
      j = 0;
      i = 0;
      if (j >= localList.size()) {
        break label238;
      }
      if ((i == 0) && (!zzzi.zzd(localzzafk, zzaft.zza(paramzzyu, (zzafk)localList.get(j))))) {
        break label318;
      }
      localzzafk1 = zzaft.zza(paramzzyu, (zzafk)paramVarArgs.get(j));
      if (!(localzzafk1 instanceof zzafo)) {
        break label227;
      }
      if ((localzzafk1 != zzafo.aMg) && (!((zzafo)localzzafk1).zzckm())) {
        break label215;
      }
      return localzzafk1;
      bool = false;
      break;
      bool = false;
      break label24;
    }
    label215:
    if (localzzafk1 == zzafo.aMf)
    {
      return zzafo.aMi;
      label227:
      i = 1;
    }
    label238:
    label318:
    for (;;)
    {
      j += 1;
      break;
      if (localList.size() < paramVarArgs.size()) {}
      for (i = k; i != 0; i = 0)
      {
        paramzzyu = zzaft.zza(paramzzyu, (zzafk)paramVarArgs.get(paramVarArgs.size() - 1));
        if ((!(paramzzyu instanceof zzafo)) || ((paramzzyu != zzafo.aMg) && (!((zzafo)paramzzyu).zzckm()))) {
          break;
        }
        return paramzzyu;
      }
      return zzafo.aMi;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzabl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */