package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;

public class zzabn
  implements zzzh
{
  public zzafk<?> zzb(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    boolean bool2 = true;
    int i = 0;
    if (paramVarArgs != null)
    {
      bool1 = true;
      zzaa.zzbt(bool1);
      if (paramVarArgs.length <= 0) {
        break label91;
      }
    }
    label91:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      zzaa.zzbt(bool1);
      int j = paramVarArgs.length;
      while (i < j)
      {
        zzafk<?> localzzafk = paramVarArgs[i];
        zzaa.zzy(localzzafk);
        zzaa.zzbt(localzzafk instanceof zzafs);
        paramzzyu.zza((String)((zzafs)localzzafk).zzckf(), zzafo.aMi);
        i += 1;
      }
      bool1 = false;
      break;
    }
    return zzafo.aMi;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzabn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */