package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;
import java.util.List;

public class zzabo
  implements zzzh
{
  public zzafk<?> zzb(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    if (paramVarArgs != null)
    {
      bool = true;
      zzaa.zzbt(bool);
      if (paramVarArgs.length != 4) {
        break label107;
      }
    }
    Object localObject1;
    Object localObject2;
    label107:
    for (boolean bool = true;; bool = false)
    {
      zzaa.zzbt(bool);
      localObject1 = zzaft.zza(paramzzyu, paramVarArgs[3]);
      zzaa.zzbt(localObject1 instanceof zzafp);
      localObject1 = (List)((zzafp)localObject1).zzckf();
      localObject2 = paramVarArgs[2];
      zzaa.zzbt(localObject2 instanceof zzafl);
      if (!((Boolean)((zzafl)localObject2).zzckf()).booleanValue()) {
        break label131;
      }
      localObject2 = zzaft.zza(paramzzyu, (List)localObject1);
      if (localObject2 != zzafo.aMf) {
        break label112;
      }
      return zzafo.aMi;
      bool = false;
      break;
    }
    label112:
    if (((zzafo)localObject2).zzckm()) {
      return (zzafk<?>)localObject2;
    }
    label131:
    do
    {
      zzaft.zza(paramzzyu, paramVarArgs[1]);
      if (!zzzi.zza(zzaft.zza(paramzzyu, paramVarArgs[0]))) {
        break;
      }
      localObject2 = zzaft.zza(paramzzyu, (List)localObject1);
      if (localObject2 == zzafo.aMf) {
        return zzafo.aMi;
      }
    } while (!((zzafo)localObject2).zzckm());
    return (zzafk<?>)localObject2;
    return zzafo.aMi;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzabo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */