package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;

public class zzabp
  extends zzzj
{
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    if (paramVarArgs != null)
    {
      bool = true;
      zzaa.zzbt(bool);
      if (paramVarArgs.length != 2) {
        break label91;
      }
    }
    zzafk<?> localzzafk;
    label91:
    for (boolean bool = true;; bool = false)
    {
      zzaa.zzbt(bool);
      localzzafk = paramVarArgs[0];
      paramVarArgs = paramVarArgs[1];
      if (((!(localzzafk instanceof zzafo)) || (localzzafk == zzafo.aMi) || (localzzafk == zzafo.aMh)) && ((!(paramVarArgs instanceof zzafo)) || (paramVarArgs == zzafo.aMi) || (paramVarArgs == zzafo.aMh))) {
        break label96;
      }
      throw new IllegalArgumentException("Illegal InternalType passed to Add.");
      bool = false;
      break;
    }
    label96:
    if ((!(localzzafk instanceof zzafq)) && (!(localzzafk instanceof zzafp)))
    {
      paramzzyu = localzzafk;
      if (!(localzzafk instanceof zzafn)) {}
    }
    else
    {
      paramzzyu = new zzafs(zzzi.zzd(localzzafk));
    }
    if (((paramVarArgs instanceof zzafq)) || ((paramVarArgs instanceof zzafp)) || ((paramVarArgs instanceof zzafn))) {
      paramVarArgs = new zzafs(zzzi.zzd(paramVarArgs));
    }
    for (;;)
    {
      if (((paramzzyu instanceof zzafs)) || ((paramVarArgs instanceof zzafs)))
      {
        paramzzyu = String.valueOf(zzzi.zzd(paramzzyu));
        paramVarArgs = String.valueOf(zzzi.zzd(paramVarArgs));
        if (paramVarArgs.length() != 0) {}
        for (paramzzyu = paramzzyu.concat(paramVarArgs);; paramzzyu = new String(paramzzyu)) {
          return new zzafs(paramzzyu);
        }
      }
      return new zzafm(Double.valueOf(zzzi.zza(paramzzyu, paramVarArgs)));
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzabp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */