package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;

public class zzabr
  extends zzzj
{
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    if (paramVarArgs != null)
    {
      bool = true;
      zzaa.zzbt(bool);
      if (paramVarArgs.length != 2) {
        break label76;
      }
    }
    double d1;
    double d2;
    label76:
    for (boolean bool = true;; bool = false)
    {
      zzaa.zzbt(bool);
      d1 = zzzi.zzb(paramVarArgs[0]);
      d2 = zzzi.zzb(paramVarArgs[1]);
      if ((!Double.isNaN(d1)) && (!Double.isNaN(d2))) {
        break label82;
      }
      return new zzafm(Double.valueOf(NaN.0D));
      bool = false;
      break;
    }
    label82:
    if ((Double.isInfinite(d1)) || (d2 == 0.0D)) {
      return new zzafm(Double.valueOf(NaN.0D));
    }
    if ((!Double.isInfinite(d1)) && (Double.isInfinite(d2))) {
      return new zzafm(Double.valueOf(d1));
    }
    if ((d1 == 0.0D) && (d2 != 0.0D) && (!Double.isInfinite(d2))) {
      return new zzafm(Double.valueOf(d1));
    }
    return new zzafm(Double.valueOf(d1 % d2));
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzabr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */