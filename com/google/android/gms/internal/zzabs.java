package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;

public class zzabs
  extends zzzj
{
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    int j = 1;
    if (paramVarArgs != null)
    {
      bool = true;
      zzaa.zzbt(bool);
      if (paramVarArgs.length != 2) {
        break label79;
      }
    }
    double d1;
    double d2;
    label79:
    for (boolean bool = true;; bool = false)
    {
      zzaa.zzbt(bool);
      d1 = zzzi.zzb(paramVarArgs[0]);
      d2 = zzzi.zzb(paramVarArgs[1]);
      if ((!Double.isNaN(d1)) && (!Double.isNaN(d2))) {
        break label85;
      }
      return new zzafm(Double.valueOf(NaN.0D));
      bool = false;
      break;
    }
    label85:
    if (((Double.isInfinite(d1)) && (d2 == 0.0D)) || ((d1 == 0.0D) && (Double.isInfinite(d2)))) {
      return new zzafm(Double.valueOf(NaN.0D));
    }
    if ((Double.isInfinite(d1)) || (Double.isInfinite(d2)))
    {
      int i;
      if (Double.compare(d1, 0.0D) < 0.0D)
      {
        i = 1;
        if (Double.compare(d2, 0.0D) >= 0.0D) {
          break label198;
        }
        label168:
        if ((i ^ j) == 0) {
          break label204;
        }
      }
      label198:
      label204:
      for (d1 = Double.NEGATIVE_INFINITY;; d1 = Double.POSITIVE_INFINITY)
      {
        return new zzafm(Double.valueOf(d1));
        i = 0;
        break;
        j = 0;
        break label168;
      }
    }
    return new zzafm(Double.valueOf(d1 * d2));
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzabs.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */