package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;

public class zzabt
  extends zzzj
{
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    if (paramVarArgs != null)
    {
      bool = true;
      zzaa.zzbt(bool);
      if (paramVarArgs.length != 2) {
        break label66;
      }
    }
    label66:
    for (boolean bool = true;; bool = false)
    {
      zzaa.zzbt(bool);
      paramzzyu = new zzafm(Double.valueOf(-1.0D * zzzi.zzb(paramVarArgs[1])));
      return new zzafm(Double.valueOf(zzzi.zza(paramVarArgs[0], paramzzyu)));
      bool = false;
      break;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzabt.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */