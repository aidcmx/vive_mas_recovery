package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;

public class zzabu
  extends zzzj
{
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    int i = 0;
    if (paramVarArgs != null)
    {
      bool = true;
      zzaa.zzbt(bool);
      if ((paramVarArgs.length != 1) && (paramVarArgs.length != 2)) {
        break label98;
      }
    }
    label98:
    for (boolean bool = true;; bool = false)
    {
      zzaa.zzbt(bool);
      zzaa.zzbt(paramVarArgs[0] instanceof zzafs);
      paramzzyu = (String)((zzafs)paramVarArgs[0]).zzckf();
      if (paramVarArgs.length == 2) {
        i = (int)zzzi.zzc(paramVarArgs[1]);
      }
      if ((i >= 0) && (i < paramzzyu.length())) {
        break label104;
      }
      return new zzafs("");
      bool = false;
      break;
    }
    label104:
    return new zzafs(String.valueOf(paramzzyu.charAt(i)));
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzabu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */