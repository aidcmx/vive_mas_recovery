package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;

public class zzabv
  extends zzzj
{
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    int i = 1;
    if (paramVarArgs != null)
    {
      bool = true;
      zzaa.zzbt(bool);
      if (paramVarArgs.length <= 0) {
        break label86;
      }
    }
    label86:
    for (boolean bool = true;; bool = false)
    {
      zzaa.zzbt(bool);
      zzaa.zzbt(paramVarArgs[0] instanceof zzafs);
      paramzzyu = new StringBuilder((String)((zzafs)paramVarArgs[0]).zzckf());
      while (i < paramVarArgs.length)
      {
        paramzzyu.append(zzzi.zzd(paramVarArgs[i]));
        i += 1;
      }
      bool = false;
      break;
    }
    return new zzafs(paramzzyu.toString());
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzabv.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */