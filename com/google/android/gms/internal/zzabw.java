package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;

public class zzabw
  extends zzzj
{
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    boolean bool;
    label27:
    String str;
    if (paramVarArgs != null)
    {
      bool = true;
      zzaa.zzbt(bool);
      if ((paramVarArgs.length != 2) && (paramVarArgs.length != 3)) {
        break label108;
      }
      bool = true;
      zzaa.zzbt(bool);
      zzaa.zzbt(paramVarArgs[0] instanceof zzafs);
      paramzzyu = (String)((zzafs)paramVarArgs[0]).zzckf();
      str = zzzi.zzd(paramVarArgs[1]);
      if (paramVarArgs.length >= 3) {
        break label114;
      }
    }
    label108:
    label114:
    for (double d = 0.0D;; d = zzzi.zzc(paramVarArgs[2]))
    {
      return new zzafm(Double.valueOf(paramzzyu.indexOf(str, (int)Math.min(Math.max(d, 0.0D), paramzzyu.length()))));
      bool = false;
      break;
      bool = false;
      break label27;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzabw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */