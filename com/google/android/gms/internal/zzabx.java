package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;

public class zzabx
  extends zzzj
{
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    if (paramVarArgs != null)
    {
      bool = true;
      zzaa.zzbt(bool);
      if ((paramVarArgs.length != 2) && (paramVarArgs.length != 3)) {
        break label136;
      }
    }
    label136:
    for (boolean bool = true;; bool = false)
    {
      zzaa.zzbt(bool);
      zzaa.zzbt(paramVarArgs[0] instanceof zzafs);
      paramzzyu = (String)((zzafs)paramVarArgs[0]).zzckf();
      String str = zzzi.zzd(paramVarArgs[1]);
      double d2 = Double.POSITIVE_INFINITY;
      double d1 = d2;
      if (paramVarArgs.length == 3)
      {
        d1 = d2;
        if (!Double.isNaN(zzzi.zzb(paramVarArgs[2]))) {
          d1 = zzzi.zzc(paramVarArgs[2]);
        }
      }
      return new zzafm(Double.valueOf(paramzzyu.lastIndexOf(str, (int)Math.min(Math.max(d1, 0.0D), paramzzyu.length()))));
      bool = false;
      break;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzabx.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */