package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class zzaby
  extends zzzj
{
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    boolean bool;
    label24:
    String str;
    if (paramVarArgs != null)
    {
      bool = true;
      zzaa.zzbt(bool);
      if ((paramVarArgs.length != 1) && (paramVarArgs.length != 2)) {
        break label117;
      }
      bool = true;
      zzaa.zzbt(bool);
      zzaa.zzbt(paramVarArgs[0] instanceof zzafs);
      str = (String)((zzafs)paramVarArgs[0]).zzckf();
      if (paramVarArgs.length >= 2) {
        break label122;
      }
    }
    label117:
    label122:
    for (paramzzyu = "";; paramzzyu = zzzi.zzd(paramVarArgs[1]))
    {
      paramzzyu = Pattern.compile(paramzzyu).matcher(str);
      if (!paramzzyu.find()) {
        break label132;
      }
      paramVarArgs = new ArrayList();
      paramVarArgs.add(new zzafs(paramzzyu.group()));
      return new zzafp(paramVarArgs);
      bool = false;
      break;
      bool = false;
      break label24;
    }
    label132:
    return zzafo.aMh;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaby.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */