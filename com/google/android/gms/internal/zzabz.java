package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;

public class zzabz
  extends zzzj
{
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    if (paramVarArgs != null)
    {
      bool = true;
      zzaa.zzbt(bool);
      if ((paramVarArgs.length <= 0) || (paramVarArgs.length > 3)) {
        break label76;
      }
    }
    String str1;
    label76:
    for (boolean bool = true;; bool = false)
    {
      zzaa.zzbt(bool);
      zzaa.zzbt(paramVarArgs[0] instanceof zzafs);
      str1 = (String)((zzafs)paramVarArgs[0]).zzckf();
      if (paramVarArgs.length != 1) {
        break label82;
      }
      return new zzafs(str1);
      bool = false;
      break;
    }
    label82:
    String str2 = zzzi.zzd(paramVarArgs[1]);
    if (paramVarArgs.length < 3) {}
    int i;
    for (paramVarArgs = zzafo.aMi;; paramVarArgs = paramVarArgs[2])
    {
      i = str1.indexOf(str2);
      if (i != -1) {
        break;
      }
      return new zzafs(str1);
    }
    Object localObject = paramVarArgs;
    if ((paramVarArgs instanceof zzafn)) {
      localObject = ((zzzh)((zzafn)paramVarArgs).zzckf()).zzb(paramzzyu, new zzafk[] { new zzafs(str2), new zzafm(Double.valueOf(i)), new zzafs(str1) });
    }
    paramzzyu = zzzi.zzd((zzafk)localObject);
    paramVarArgs = String.valueOf(str1.substring(0, i));
    localObject = String.valueOf(str1.substring(str2.length() + i));
    return new zzafs(String.valueOf(paramVarArgs).length() + 0 + String.valueOf(paramzzyu).length() + String.valueOf(localObject).length() + paramVarArgs + paramzzyu + (String)localObject);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzabz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */