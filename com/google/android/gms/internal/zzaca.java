package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class zzaca
  extends zzzj
{
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    boolean bool;
    label24:
    String str;
    if (paramVarArgs != null)
    {
      bool = true;
      zzaa.zzbt(bool);
      if ((paramVarArgs.length != 1) && (paramVarArgs.length != 2)) {
        break label98;
      }
      bool = true;
      zzaa.zzbt(bool);
      zzaa.zzbt(paramVarArgs[0] instanceof zzafs);
      str = (String)((zzafs)paramVarArgs[0]).zzckf();
      if (paramVarArgs.length >= 2) {
        break label103;
      }
    }
    label98:
    label103:
    for (paramzzyu = "";; paramzzyu = zzzi.zzd(paramVarArgs[1]))
    {
      paramzzyu = Pattern.compile(paramzzyu).matcher(str);
      if (!paramzzyu.find()) {
        break label113;
      }
      return new zzafm(Double.valueOf(paramzzyu.start()));
      bool = false;
      break;
      bool = false;
      break label24;
    }
    label113:
    return new zzafm(Double.valueOf(-1.0D));
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaca.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */