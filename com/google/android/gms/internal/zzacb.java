package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;

public class zzacb
  extends zzzj
{
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    boolean bool;
    label26:
    double d1;
    label61:
    double d2;
    int i;
    if (paramVarArgs != null)
    {
      bool = true;
      zzaa.zzbt(bool);
      if ((paramVarArgs.length <= 0) || (paramVarArgs.length > 3)) {
        break label173;
      }
      bool = true;
      zzaa.zzbt(bool);
      zzaa.zzbt(paramVarArgs[0] instanceof zzafs);
      paramzzyu = (String)((zzafs)paramVarArgs[0]).zzckf();
      if (paramVarArgs.length >= 2) {
        break label179;
      }
      d1 = 0.0D;
      double d3 = paramzzyu.length();
      d2 = d3;
      if (paramVarArgs.length == 3)
      {
        d2 = d3;
        if (paramVarArgs[2] != zzafo.aMi) {
          d2 = zzzi.zzc(paramVarArgs[2]);
        }
      }
      if (d1 >= 0.0D) {
        break label189;
      }
      i = (int)Math.max(d1 + paramzzyu.length(), 0.0D);
      label119:
      if (d2 >= 0.0D) {
        break label204;
      }
    }
    label173:
    label179:
    label189:
    label204:
    for (int j = (int)Math.max(paramzzyu.length() + d2, 0.0D);; j = (int)Math.min(d2, paramzzyu.length()))
    {
      return new zzafs(paramzzyu.substring(i, Math.max(0, j - i) + i));
      bool = false;
      break;
      bool = false;
      break label26;
      d1 = zzzi.zzc(paramVarArgs[1]);
      break label61;
      i = (int)Math.min(d1, paramzzyu.length());
      break label119;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzacb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */