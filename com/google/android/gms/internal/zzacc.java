package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;
import java.util.ArrayList;
import java.util.List;

public class zzacc
  extends zzzj
{
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    int j = 1;
    boolean bool;
    if (paramVarArgs != null)
    {
      bool = true;
      zzaa.zzbt(bool);
      if ((paramVarArgs.length != 1) && (paramVarArgs.length != 2)) {
        break label83;
      }
      bool = true;
      label30:
      zzaa.zzbt(bool);
      zzaa.zzbt(paramVarArgs[0] instanceof zzafs);
      paramzzyu = new ArrayList();
      if (paramVarArgs.length != 1) {
        break label89;
      }
      paramzzyu.add(paramVarArgs[0]);
    }
    label83:
    label89:
    label156:
    label198:
    label203:
    label206:
    for (;;)
    {
      return new zzafp(paramzzyu);
      bool = false;
      break;
      bool = false;
      break label30;
      String str = (String)((zzafs)paramVarArgs[0]).zzckf();
      paramVarArgs = zzzi.zzd(paramVarArgs[1]);
      bool = paramVarArgs.equals("");
      int i;
      if (bool)
      {
        i = 0;
        paramVarArgs = str.split(paramVarArgs, i);
        if ((!bool) || (paramVarArgs.length <= 0) || (!paramVarArgs[0].equals(""))) {
          break label198;
        }
        i = 1;
        if (i == 0) {
          break label203;
        }
        i = j;
      }
      for (;;)
      {
        if (i >= paramVarArgs.length) {
          break label206;
        }
        paramzzyu.add(new zzafs(paramVarArgs[i]));
        i += 1;
        continue;
        i = -1;
        break;
        i = 0;
        break label156;
        i = 0;
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzacc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */