package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;

public class zzacd
  extends zzzj
{
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    boolean bool;
    label26:
    String str;
    if (paramVarArgs != null)
    {
      bool = true;
      zzaa.zzbt(bool);
      if ((paramVarArgs.length <= 0) || (paramVarArgs.length > 3)) {
        break label174;
      }
      bool = true;
      zzaa.zzbt(bool);
      zzaa.zzbt(paramVarArgs[0] instanceof zzafs);
      str = (String)((zzafs)paramVarArgs[0]).zzckf();
      if (paramVarArgs.length >= 2) {
        break label180;
      }
    }
    label174:
    label180:
    for (Object localObject = zzafo.aMi;; localObject = paramVarArgs[1])
    {
      int k = (int)zzzi.zzc((zzafk)localObject);
      int j = str.length();
      int i = j;
      if (paramVarArgs.length == 3)
      {
        i = j;
        if (paramVarArgs[2] != zzafo.aMi) {
          i = (int)zzzi.zzc(zzaft.zza(paramzzyu, paramVarArgs[2]));
        }
      }
      j = Math.min(Math.max(k, 0), str.length());
      i = Math.min(Math.max(i, 0), str.length());
      return new zzafs(str.substring(Math.min(j, i), Math.max(j, i)));
      bool = false;
      break;
      bool = false;
      break label26;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzacd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */