package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;

public class zzacj
  extends zzzj
{
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    boolean bool2 = true;
    if (paramVarArgs != null)
    {
      bool1 = true;
      zzaa.zzbt(bool1);
      if (paramVarArgs.length != 1) {
        break label63;
      }
    }
    label63:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      zzaa.zzbt(bool1);
      zzaa.zzbt(paramVarArgs[0] instanceof zzafs);
      return new zzafs(((String)((zzafs)paramVarArgs[0]).zzckf()).trim());
      bool1 = false;
      break;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzacj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */