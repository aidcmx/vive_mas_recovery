package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;

public class zzack
  extends zzzj
{
  static String decode(String paramString1, String paramString2)
    throws UnsupportedEncodingException
  {
    Charset localCharset = Charset.forName("UTF-8");
    StringBuilder localStringBuilder = new StringBuilder();
    int j = 0;
    while (j < paramString1.length())
    {
      int k = paramString1.charAt(j);
      if (k != 37)
      {
        localStringBuilder.append((char)k);
        j += 1;
      }
      else
      {
        int i = zzad(paramString1, j);
        k = j + 3;
        if ((i & 0x80) == 0)
        {
          if (paramString2.indexOf(i) == -1)
          {
            localStringBuilder.append((char)i);
            j = k;
          }
          else
          {
            localStringBuilder.append(paramString1.substring(k - 3, k));
            j = k;
          }
        }
        else
        {
          int m = 0;
          while ((i << m & 0x80) != 0) {
            m += 1;
          }
          if ((m < 2) || (m > 4)) {
            throw new UnsupportedEncodingException();
          }
          Object localObject = new byte[m];
          localObject[0] = i;
          int n = 1;
          while (n < m)
          {
            i = zzad(paramString1, k);
            if ((i & 0xC0) != 128) {
              throw new UnsupportedEncodingException();
            }
            localObject[n] = i;
            n += 1;
            k += 3;
          }
          localObject = localCharset.decode(ByteBuffer.wrap((byte[])localObject));
          if ((((CharBuffer)localObject).length() == 1) && (paramString2.indexOf(((CharBuffer)localObject).charAt(0)) != -1))
          {
            localStringBuilder.append(paramString1.substring(j, k));
            j = k;
          }
          else
          {
            localStringBuilder.append((CharSequence)localObject);
            j = k;
          }
        }
      }
    }
    return localStringBuilder.toString();
  }
  
  private static byte zzad(String paramString, int paramInt)
    throws UnsupportedEncodingException
  {
    if ((paramInt + 3 > paramString.length()) || (paramString.charAt(paramInt) != '%')) {
      throw new UnsupportedEncodingException();
    }
    paramString = paramString.substring(paramInt + 1, paramInt + 3);
    if ((paramString.charAt(0) == '+') || (paramString.charAt(0) == '-')) {
      throw new UnsupportedEncodingException();
    }
    try
    {
      paramInt = Integer.parseInt(paramString, 16);
      return (byte)paramInt;
    }
    catch (NumberFormatException paramString)
    {
      throw new UnsupportedEncodingException();
    }
  }
  
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    boolean bool;
    if (paramVarArgs != null)
    {
      bool = true;
      zzaa.zzbt(bool);
      if (paramVarArgs.length <= 0) {
        break label51;
      }
    }
    label51:
    for (paramzzyu = (zzafk)zzaa.zzy(paramVarArgs[0]);; paramzzyu = zzafo.aMi)
    {
      paramzzyu = zzzi.zzd(paramzzyu);
      try
      {
        paramzzyu = new zzafs(decode(paramzzyu, "#;/?:@&=+$,"));
        return paramzzyu;
      }
      catch (UnsupportedEncodingException paramzzyu) {}
      bool = false;
      break;
    }
    return zzafo.aMi;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzack.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */