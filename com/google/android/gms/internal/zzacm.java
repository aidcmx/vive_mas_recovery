package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

public class zzacm
  extends zzzj
{
  static String encode(String paramString1, String paramString2)
    throws UnsupportedEncodingException
  {
    StringBuilder localStringBuilder = new StringBuilder();
    Charset localCharset = Charset.forName("UTF-8");
    int i = 0;
    while (i < paramString1.length())
    {
      int k = paramString1.charAt(i);
      if (paramString2.indexOf(k) != -1)
      {
        localStringBuilder.append((char)k);
        i += 1;
      }
      else
      {
        int j = 1;
        if (Character.isHighSurrogate((char)k))
        {
          if (i + 1 >= paramString1.length()) {
            break label192;
          }
          if (Character.isLowSurrogate(paramString1.charAt(i + 1))) {
            j = 2;
          }
        }
        else
        {
          byte[] arrayOfByte = paramString1.substring(i, i + j).getBytes(localCharset);
          k = 0;
          while (k < arrayOfByte.length)
          {
            localStringBuilder.append("%");
            localStringBuilder.append(Character.toUpperCase(Character.forDigit(arrayOfByte[k] >> 4 & 0xF, 16)));
            localStringBuilder.append(Character.toUpperCase(Character.forDigit(arrayOfByte[k] & 0xF, 16)));
            k += 1;
          }
        }
        throw new UnsupportedEncodingException();
        label192:
        throw new UnsupportedEncodingException();
        i += j;
      }
    }
    return localStringBuilder.toString().replaceAll(" ", "%20");
  }
  
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    boolean bool;
    if (paramVarArgs != null)
    {
      bool = true;
      zzaa.zzbt(bool);
      if (paramVarArgs.length <= 0) {
        break label51;
      }
    }
    label51:
    for (paramzzyu = (zzafk)zzaa.zzy(paramVarArgs[0]);; paramzzyu = zzafo.aMi)
    {
      paramzzyu = zzzi.zzd(paramzzyu);
      try
      {
        paramzzyu = new zzafs(encode(paramzzyu, "#;/?:@&=+$,abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_.!~*'()0123456789"));
        return paramzzyu;
      }
      catch (UnsupportedEncodingException paramzzyu) {}
      bool = false;
      break;
    }
    return zzafo.aMi;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzacm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */