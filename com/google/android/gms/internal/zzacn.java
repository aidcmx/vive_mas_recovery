package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;
import java.io.UnsupportedEncodingException;

public class zzacn
  extends zzzj
{
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    boolean bool;
    if (paramVarArgs != null)
    {
      bool = true;
      zzaa.zzbt(bool);
      if (paramVarArgs.length <= 0) {
        break label51;
      }
    }
    label51:
    for (paramzzyu = (zzafk)zzaa.zzy(paramVarArgs[0]);; paramzzyu = zzafo.aMi)
    {
      paramzzyu = zzzi.zzd(paramzzyu);
      try
      {
        paramzzyu = new zzafs(zzacm.encode(paramzzyu, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_.!~*'()"));
        return paramzzyu;
      }
      catch (UnsupportedEncodingException paramzzyu) {}
      bool = false;
      break;
    }
    return zzafo.aMi;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzacn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */