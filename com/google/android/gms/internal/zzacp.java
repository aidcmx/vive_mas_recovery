package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class zzacp
  extends zzzj
{
  private final zza aKL;
  
  public zzacp(zza paramzza)
  {
    this.aKL = paramzza;
  }
  
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    zzaa.zzy(paramVarArgs);
    HashMap localHashMap;
    label95:
    Map.Entry localEntry;
    if (paramVarArgs.length >= 1)
    {
      bool = true;
      zzaa.zzbt(bool);
      zzaa.zzbt(paramVarArgs[0] instanceof zzafs);
      paramzzyu = (String)((zzafs)paramVarArgs[0]).zzckf();
      localHashMap = new HashMap();
      if ((paramVarArgs.length < 2) || (paramVarArgs[1] == zzafo.aMi)) {
        break label205;
      }
      zzaa.zzbt(paramVarArgs[1] instanceof zzafq);
      paramVarArgs = ((Map)((zzafq)paramVarArgs[1]).zzckf()).entrySet().iterator();
      if (!paramVarArgs.hasNext()) {
        break label205;
      }
      localEntry = (Map.Entry)paramVarArgs.next();
      if ((localEntry.getValue() instanceof zzafr)) {
        break label195;
      }
      bool = true;
      label130:
      zzaa.zzbs(bool);
      if (zzaft.zzo((zzafk)localEntry.getValue())) {
        break label200;
      }
    }
    label195:
    label200:
    for (boolean bool = true;; bool = false)
    {
      zzaa.zzbs(bool);
      localHashMap.put((String)localEntry.getKey(), ((zzafk)localEntry.getValue()).zzckf());
      break label95;
      bool = false;
      break;
      bool = false;
      break label130;
    }
    label205:
    return zzaft.zzbb(this.aKL.zzf(paramzzyu, localHashMap));
  }
  
  public static abstract interface zza
  {
    public abstract Object zzf(String paramString, Map<String, Object> paramMap);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzacp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */