package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;
import org.json.JSONArray;
import org.json.JSONException;

public class zzacq
  extends zzzj
{
  private final zzyu aJw;
  private final int nV;
  
  public zzacq(int paramInt, zzyu paramzzyu)
  {
    this.nV = paramInt;
    this.aJw = paramzzyu;
  }
  
  public zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    boolean bool2 = true;
    if (paramVarArgs != null)
    {
      bool1 = true;
      zzaa.zzbt(bool1);
      if (paramVarArgs.length != 1) {
        break label98;
      }
    }
    label98:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      zzaa.zzbt(bool1);
      zzaa.zzbt(paramVarArgs[0] instanceof zzafs);
      try
      {
        paramVarArgs = zzaeq.zzaz(new JSONArray((String)((zzafs)paramVarArgs[0]).zzckf()).getJSONArray(0));
        paramVarArgs.zza(this.aJw);
        paramzzyu = paramVarArgs.zzb(paramzzyu, new zzafk[0]);
        if (this.nV == 0) {
          paramzzyu = zzafo.aMi;
        }
        return paramzzyu;
      }
      catch (JSONException paramzzyu)
      {
        zzyl.zzb("Unable to convert Custom Pixie to instruction", paramzzyu);
      }
      bool1 = false;
      break;
    }
    return zzafo.aMi;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzacq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */