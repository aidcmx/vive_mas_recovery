package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;

public class zzacr
  extends zzzj
{
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    int i = 1;
    boolean bool;
    label23:
    String str;
    if (paramVarArgs != null)
    {
      bool = true;
      zzaa.zzbt(bool);
      if (paramVarArgs.length != 2) {
        break label181;
      }
      bool = true;
      zzaa.zzbt(bool);
      zzaa.zzbt(paramVarArgs[0] instanceof zzafs);
      paramzzyu = zzzi.zzd(paramVarArgs[1]);
      str = (String)((zzafs)paramVarArgs[0]).zzckf();
      switch (str.hashCode())
      {
      default: 
        label104:
        i = -1;
        switch (i)
        {
        default: 
          label106:
          paramzzyu = String.valueOf((String)((zzafs)paramVarArgs[0]).zzckf());
          if (paramzzyu.length() == 0) {}
          break;
        }
        break;
      }
    }
    for (paramzzyu = "Invalid logging level: ".concat(paramzzyu);; paramzzyu = new String("Invalid logging level: "))
    {
      throw new IllegalArgumentException(paramzzyu);
      bool = false;
      break;
      label181:
      bool = false;
      break label23;
      if (!str.equals("e")) {
        break label104;
      }
      i = 0;
      break label106;
      if (!str.equals("i")) {
        break label104;
      }
      break label106;
      if (!str.equals("v")) {
        break label104;
      }
      i = 2;
      break label106;
      if (!str.equals("w")) {
        break label104;
      }
      i = 3;
      break label106;
      zzyl.e(paramzzyu);
      for (;;)
      {
        return zzafo.aMi;
        zzyl.zzdh(paramzzyu);
        continue;
        zzyl.v(paramzzyu);
        continue;
        zzyl.zzdi(paramzzyu);
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzacr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */