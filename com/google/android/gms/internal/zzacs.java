package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.common.internal.zzaa;

public class zzacs
  extends zzzj
{
  private final zzys.zzc aJC;
  
  public zzacs(Context paramContext, zzys.zzc paramzzc)
  {
    this.aJC = paramzzc;
  }
  
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    boolean bool2 = true;
    if (paramVarArgs != null)
    {
      bool1 = true;
      zzaa.zzbt(bool1);
      if (paramVarArgs.length != 0) {
        break label47;
      }
    }
    label47:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      zzaa.zzbt(bool1);
      this.aJC.zzcik().zzcp(false);
      return zzafo.aMi;
      bool1 = false;
      break;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzacs.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */