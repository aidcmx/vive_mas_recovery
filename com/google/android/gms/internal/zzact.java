package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.common.internal.zzaa;

public class zzact
  implements zzzh
{
  private final zzxs aKM;
  
  public zzact(Context paramContext)
  {
    this(zzxs.zzeh(paramContext));
  }
  
  zzact(zzxs paramzzxs)
  {
    this.aKM = paramzzxs;
  }
  
  public zzafk<?> zzb(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    boolean bool2 = true;
    if (paramVarArgs != null)
    {
      bool1 = true;
      zzaa.zzbt(bool1);
      if (paramVarArgs.length != 0) {
        break label46;
      }
    }
    label46:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      zzaa.zzbt(bool1);
      paramzzyu = this.aKM.zzcdo();
      if (paramzzyu != null) {
        break label51;
      }
      return zzafo.aMi;
      bool1 = false;
      break;
    }
    label51:
    return new zzafs(paramzzyu);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzact.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */