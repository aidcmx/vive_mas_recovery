package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.common.internal.zzaa;

public class zzacu
  implements zzzh
{
  private final zzxs aKM;
  
  public zzacu(Context paramContext)
  {
    this(zzxs.zzeh(paramContext));
  }
  
  zzacu(zzxs paramzzxs)
  {
    this.aKM = paramzzxs;
  }
  
  public zzafk<?> zzb(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    boolean bool2 = true;
    if (paramVarArgs != null)
    {
      bool1 = true;
      zzaa.zzbt(bool1);
      if (paramVarArgs.length != 0) {
        break label54;
      }
      bool1 = true;
      label20:
      zzaa.zzbt(bool1);
      if (this.aKM.isLimitAdTrackingEnabled()) {
        break label59;
      }
    }
    label54:
    label59:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      return new zzafl(Boolean.valueOf(bool1));
      bool1 = false;
      break;
      bool1 = false;
      break label20;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzacu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */