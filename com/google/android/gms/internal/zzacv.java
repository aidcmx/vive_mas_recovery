package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.common.internal.zzaa;
import java.util.Map;

public class zzacv
  extends zzzj
{
  private final zzys.zzc aKN;
  private final Context mContext;
  
  public zzacv(Context paramContext, zzys.zzc paramzzc)
  {
    this.mContext = ((Context)zzaa.zzy(paramContext));
    this.aKN = paramzzc;
  }
  
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    if (paramVarArgs != null) {}
    for (boolean bool = true;; bool = false)
    {
      zzaa.zzbt(bool);
      if ((paramVarArgs.length != 0) && (paramVarArgs[0] != zzafo.aMi)) {
        break;
      }
      return new zzafs("");
    }
    paramzzyu = this.aKN.zzcik().zzcfc().get("_ldl");
    if (paramzzyu == null) {
      return new zzafs("");
    }
    paramzzyu = zzaft.zzbb(paramzzyu);
    if (!(paramzzyu instanceof zzafs)) {
      return new zzafs("");
    }
    String str = (String)((zzafs)paramzzyu).zzckf();
    if (!zzyk.zzbg(str, "conv").equals(zzzi.zzd(paramVarArgs[0]))) {
      return new zzafs("");
    }
    if (paramVarArgs.length > 1) {
      if (paramVarArgs[1] == zzafo.aMi) {
        paramzzyu = null;
      }
    }
    for (;;)
    {
      paramzzyu = zzyk.zzbg(str, paramzzyu);
      if (paramzzyu != null)
      {
        return new zzafs(paramzzyu);
        paramzzyu = zzzi.zzd(paramVarArgs[1]);
      }
      else
      {
        return new zzafs("");
        paramzzyu = null;
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzacv.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */