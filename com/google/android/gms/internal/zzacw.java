package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.common.internal.zzaa;

public class zzacw
  implements zzzh
{
  private final Context mContext;
  
  public zzacw(Context paramContext)
  {
    this.mContext = paramContext;
  }
  
  public zzafk<?> zzb(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    boolean bool2 = true;
    if (paramVarArgs != null)
    {
      bool1 = true;
      zzaa.zzbt(bool1);
      if (paramVarArgs.length != 0) {
        break label45;
      }
    }
    label45:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      zzaa.zzbt(bool1);
      return new zzafs(this.mContext.getPackageName());
      bool1 = false;
      break;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzacw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */