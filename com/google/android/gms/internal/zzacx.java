package com.google.android.gms.internal;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import com.google.android.gms.common.internal.zzaa;

public class zzacx
  implements zzzh
{
  private Context mContext;
  
  public zzacx(Context paramContext)
  {
    this.mContext = paramContext;
  }
  
  public zzafk<?> zzb(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    boolean bool2 = true;
    if (paramVarArgs != null)
    {
      bool1 = true;
      zzaa.zzbt(bool1);
      if (paramVarArgs.length != 0) {
        break label69;
      }
    }
    label69:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      zzaa.zzbt(bool1);
      try
      {
        paramzzyu = this.mContext.getPackageManager();
        paramzzyu = new zzafs(paramzzyu.getApplicationLabel(paramzzyu.getApplicationInfo(this.mContext.getPackageName(), 0)).toString());
        return paramzzyu;
      }
      catch (PackageManager.NameNotFoundException paramzzyu) {}
      bool1 = false;
      break;
    }
    return new zzafs("");
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzacx.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */