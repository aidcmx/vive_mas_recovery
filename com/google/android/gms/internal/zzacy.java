package com.google.android.gms.internal;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import com.google.android.gms.common.internal.zzaa;

public class zzacy
  implements zzzh
{
  private final Context mContext;
  
  public zzacy(Context paramContext)
  {
    this.mContext = ((Context)zzaa.zzy(paramContext));
  }
  
  public zzafk<?> zzb(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    boolean bool2 = true;
    if (paramVarArgs != null)
    {
      bool1 = true;
      zzaa.zzbt(bool1);
      if (paramVarArgs.length != 0) {
        break label65;
      }
    }
    label65:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      zzaa.zzbt(bool1);
      try
      {
        paramzzyu = new zzafm(Double.valueOf(this.mContext.getPackageManager().getPackageInfo(this.mContext.getPackageName(), 0).versionCode));
        return paramzzyu;
      }
      catch (PackageManager.NameNotFoundException paramVarArgs)
      {
        paramzzyu = String.valueOf(this.mContext.getPackageName());
        paramVarArgs = String.valueOf(paramVarArgs.getMessage());
        zzyl.e(String.valueOf(paramzzyu).length() + 25 + String.valueOf(paramVarArgs).length() + "Package name " + paramzzyu + " not found. " + paramVarArgs);
      }
      bool1 = false;
      break;
    }
    return zzafo.aMi;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzacy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */