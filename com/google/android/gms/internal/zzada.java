package com.google.android.gms.internal;

import android.content.Context;
import android.telephony.TelephonyManager;
import com.google.android.gms.common.internal.zzaa;

public class zzada
  implements zzzh
{
  private final Context mContext;
  
  public zzada(Context paramContext)
  {
    this.mContext = ((Context)zzaa.zzy(paramContext));
  }
  
  public zzafk<?> zzb(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    boolean bool2 = true;
    if (paramVarArgs != null)
    {
      bool1 = true;
      zzaa.zzbt(bool1);
      if (paramVarArgs.length != 0) {
        break label69;
      }
    }
    label69:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      zzaa.zzbt(bool1);
      paramVarArgs = (TelephonyManager)this.mContext.getSystemService("phone");
      paramzzyu = zzafo.aMi;
      if (paramVarArgs == null) {
        return paramzzyu;
      }
      paramVarArgs = paramVarArgs.getNetworkOperatorName();
      if (paramVarArgs == null) {
        return paramzzyu;
      }
      return new zzafs(paramVarArgs);
      bool1 = false;
      break;
    }
    return paramzzyu;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzada.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */