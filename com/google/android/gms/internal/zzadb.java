package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;

public class zzadb
  implements zzzh
{
  private final zzafk<?> aKO;
  
  public zzadb(zzafk<?> paramzzafk)
  {
    this.aKO = ((zzafk)zzaa.zzy(paramzzafk));
  }
  
  public zzafk<?> zzb(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    boolean bool2 = true;
    if (paramVarArgs != null)
    {
      bool1 = true;
      zzaa.zzbt(bool1);
      if (paramVarArgs.length != 0) {
        break label35;
      }
    }
    label35:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      zzaa.zzbt(bool1);
      return this.aKO;
      bool1 = false;
      break;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzadb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */