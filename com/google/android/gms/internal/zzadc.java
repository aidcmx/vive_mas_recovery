package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;

public class zzadc
  implements zzzh
{
  public zzafk<?> zzb(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    boolean bool2 = true;
    if (paramVarArgs != null)
    {
      bool1 = true;
      zzaa.zzbt(bool1);
      if (paramVarArgs.length != 0) {
        break label46;
      }
    }
    label46:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      zzaa.zzbt(bool1);
      if (!paramzzyu.has("gtm.globals.eventName")) {
        break label51;
      }
      return paramzzyu.zzqn("gtm.globals.eventName");
      bool1 = false;
      break;
    }
    label51:
    return zzafo.aMh;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzadc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */