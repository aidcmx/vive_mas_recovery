package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.util.zze;
import com.google.android.gms.common.util.zzh;

public class zzadd
  implements zzzh
{
  private zze zzaql = zzh.zzayl();
  
  public void zza(zze paramzze)
  {
    this.zzaql = ((zze)zzaa.zzy(paramzze));
  }
  
  public zzafk<?> zzb(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    boolean bool2 = true;
    if (paramVarArgs != null)
    {
      bool1 = true;
      zzaa.zzbt(bool1);
      if (paramVarArgs.length != 0) {
        break label51;
      }
    }
    label51:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      zzaa.zzbt(bool1);
      return new zzafm(Double.valueOf(this.zzaql.currentTimeMillis()));
      bool1 = false;
      break;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzadd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */