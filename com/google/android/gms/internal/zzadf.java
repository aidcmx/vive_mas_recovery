package com.google.android.gms.internal;

import android.content.Context;
import android.provider.Settings.Secure;
import com.google.android.gms.common.internal.zzaa;

public class zzadf
  implements zzzh
{
  private final Context mContext;
  
  public zzadf(Context paramContext)
  {
    this.mContext = paramContext;
  }
  
  public zzafk<?> zzb(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    boolean bool2 = true;
    if (paramVarArgs != null)
    {
      bool1 = true;
      zzaa.zzbt(bool1);
      if (paramVarArgs.length != 0) {
        break label57;
      }
    }
    label57:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      zzaa.zzbt(bool1);
      paramVarArgs = zzdy(this.mContext);
      paramzzyu = paramVarArgs;
      if (paramVarArgs == null) {
        paramzzyu = "";
      }
      return new zzafs(paramzzyu);
      bool1 = false;
      break;
    }
  }
  
  protected String zzdy(Context paramContext)
  {
    return Settings.Secure.getString(this.mContext.getContentResolver(), "android_id");
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzadf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */