package com.google.android.gms.internal;

import android.os.Build;
import com.google.android.gms.common.internal.zzaa;

public class zzadg
  implements zzzh
{
  private final String RC = Build.MODEL;
  
  public zzafk<?> zzb(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    boolean bool2 = true;
    if (paramVarArgs != null)
    {
      bool1 = true;
      zzaa.zzbt(bool1);
      if (paramVarArgs.length != 0) {
        break label42;
      }
    }
    label42:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      zzaa.zzbt(bool1);
      return new zzafs(this.RC);
      bool1 = false;
      break;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzadg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */