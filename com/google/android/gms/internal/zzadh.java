package com.google.android.gms.internal;

import android.os.Build;
import com.google.android.gms.common.internal.zzaa;

public class zzadh
  implements zzzh
{
  private final String aKQ = Build.MANUFACTURER;
  private final String aKR = Build.MODEL;
  
  public zzafk<?> zzb(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    boolean bool2 = true;
    if (paramVarArgs != null)
    {
      bool1 = true;
      zzaa.zzbt(bool1);
      if (paramVarArgs.length != 0) {
        break label116;
      }
    }
    label116:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      zzaa.zzbt(bool1);
      String str = this.aKQ;
      paramVarArgs = this.aKR;
      paramzzyu = paramVarArgs;
      if (!paramVarArgs.startsWith(str))
      {
        paramzzyu = paramVarArgs;
        if (!str.equals("unknown")) {
          paramzzyu = String.valueOf(str).length() + 1 + String.valueOf(paramVarArgs).length() + str + " " + paramVarArgs;
        }
      }
      return new zzafs(paramzzyu);
      bool1 = false;
      break;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzadh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */