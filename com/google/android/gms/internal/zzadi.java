package com.google.android.gms.internal;

import android.util.Base64;
import com.google.android.gms.common.internal.zzaa;

public class zzadi
  extends zzzj
{
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    int i = 1;
    boolean bool;
    label23:
    String str2;
    String str1;
    if (paramVarArgs != null)
    {
      bool = true;
      zzaa.zzbt(bool);
      if (paramVarArgs.length < 1) {
        break label138;
      }
      bool = true;
      zzaa.zzbt(bool);
      str2 = zzzi.zzd(paramVarArgs[0]);
      paramzzyu = "text";
      if (paramVarArgs.length > 1) {
        paramzzyu = zzzi.zzd(paramVarArgs[1]);
      }
      str1 = "base16";
      if (paramVarArgs.length > 2) {
        str1 = zzzi.zzd(paramVarArgs[2]);
      }
      if ((paramVarArgs.length <= 3) || (!zzzi.zza(paramVarArgs[3]))) {
        break label144;
      }
      label85:
      if (i == 0) {
        break label380;
      }
    }
    label138:
    label144:
    label367:
    label380:
    for (i = 3;; i = 2)
    {
      for (;;)
      {
        try
        {
          if (!"text".equals(paramzzyu)) {
            continue;
          }
          paramVarArgs = str2.getBytes();
          paramzzyu = paramVarArgs;
          if (!"base16".equals(str1)) {
            continue;
          }
          paramzzyu = zzxt.zzq(paramzzyu);
        }
        catch (IllegalArgumentException paramVarArgs)
        {
          paramzzyu = String.valueOf(paramzzyu);
          if (paramzzyu.length() == 0) {
            continue;
          }
          paramzzyu = "Encode: invalid input:".concat(paramzzyu);
          throw new RuntimeException(paramzzyu);
          paramVarArgs = new String("Encode: unknown input format: ");
          continue;
          paramzzyu = new String("Encode: invalid input:");
          continue;
          if (!"base64".equals(str1)) {
            continue;
          }
          paramzzyu = Base64.encodeToString(paramzzyu, i);
          continue;
          if (!"base64url".equals(str1)) {
            continue;
          }
          paramzzyu = Base64.encodeToString(paramzzyu, i | 0x8);
          continue;
          paramzzyu = String.valueOf(str1);
          if (paramzzyu.length() == 0) {
            break label367;
          }
        }
        return new zzafs(paramzzyu);
        bool = false;
        break;
        bool = false;
        break label23;
        i = 0;
        break label85;
        if ("base16".equals(paramzzyu))
        {
          paramVarArgs = zzxt.zzos(str2);
          paramzzyu = paramVarArgs;
        }
        else if ("base64".equals(paramzzyu))
        {
          paramVarArgs = Base64.decode(str2, i);
          paramzzyu = paramVarArgs;
        }
        else
        {
          if (!"base64url".equals(paramzzyu)) {
            continue;
          }
          paramVarArgs = Base64.decode(str2, i | 0x8);
          paramzzyu = paramVarArgs;
        }
      }
      paramVarArgs = String.valueOf(paramzzyu);
      if (paramVarArgs.length() != 0)
      {
        paramVarArgs = "Encode: unknown input format: ".concat(paramVarArgs);
        throw new UnsupportedOperationException(paramVarArgs);
      }
      for (paramzzyu = "Encode: unknown output format: ".concat(paramzzyu);; paramzzyu = new String("Encode: unknown output format: ")) {
        throw new RuntimeException(paramzzyu);
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzadi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */