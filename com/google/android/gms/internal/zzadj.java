package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class zzadj
  extends zzzj
{
  private static final Pattern aKS = Pattern.compile("(.+)/(.+)/(.+)");
  
  private static String zza(Cipher paramCipher, String paramString, SecretKeySpec paramSecretKeySpec, IvParameterSpec paramIvParameterSpec)
  {
    if ((paramString == null) || (paramString.length() == 0)) {
      throw new RuntimeException("Encrypt: empty input string");
    }
    try
    {
      paramCipher.init(1, paramSecretKeySpec, paramIvParameterSpec);
      paramCipher = zzxt.zzq(paramCipher.doFinal(paramString.getBytes()));
      return paramCipher;
    }
    catch (Exception paramCipher)
    {
      paramCipher = String.valueOf(paramCipher.getMessage());
      if (paramCipher.length() == 0) {}
    }
    for (paramCipher = "Encrypt: ".concat(paramCipher);; paramCipher = new String("Encrypt: ")) {
      throw new RuntimeException(paramCipher);
    }
  }
  
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    boolean bool;
    label18:
    String str;
    Object localObject2;
    if (paramVarArgs != null)
    {
      bool = true;
      zzaa.zzbt(bool);
      if (paramVarArgs.length < 3) {
        break label103;
      }
      bool = true;
      zzaa.zzbt(bool);
      str = zzzi.zzd(paramVarArgs[0]);
      localObject2 = zzzi.zzd(paramVarArgs[1]);
      localObject1 = zzzi.zzd(paramVarArgs[2]);
      if (paramVarArgs.length >= 4) {
        break label108;
      }
      paramzzyu = "AES/CBC/NoPadding";
      label55:
      paramVarArgs = aKS.matcher(paramzzyu);
      if (paramVarArgs.matches()) {
        break label131;
      }
      paramzzyu = String.valueOf(paramzzyu);
      if (paramzzyu.length() == 0) {
        break label118;
      }
    }
    label103:
    label108:
    label118:
    for (paramzzyu = "Encrypt: invalid transformation:".concat(paramzzyu);; paramzzyu = new String("Encrypt: invalid transformation:"))
    {
      throw new RuntimeException(paramzzyu);
      bool = false;
      break;
      bool = false;
      break label18;
      paramzzyu = zzzi.zzd(paramVarArgs[3]);
      break label55;
    }
    label131:
    paramVarArgs = paramVarArgs.group(1);
    paramVarArgs = new SecretKeySpec(((String)localObject2).getBytes(), paramVarArgs);
    Object localObject1 = new IvParameterSpec(((String)localObject1).getBytes());
    try
    {
      localObject2 = Cipher.getInstance(paramzzyu);
      return new zzafs(zza((Cipher)localObject2, str, paramVarArgs, (IvParameterSpec)localObject1));
    }
    catch (NoSuchAlgorithmException paramVarArgs)
    {
      paramzzyu = String.valueOf(paramzzyu);
      if (paramzzyu.length() != 0) {}
      for (paramzzyu = "Encrypt: invalid transformation:".concat(paramzzyu);; paramzzyu = new String("Encrypt: invalid transformation:")) {
        throw new RuntimeException(paramzzyu);
      }
    }
    catch (NoSuchPaddingException paramVarArgs)
    {
      for (;;) {}
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzadj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */