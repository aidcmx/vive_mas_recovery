package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;

public class zzadk
  extends zzzj
{
  private final zzys.zzc aKT;
  
  public zzadk(zzys.zzc paramzzc)
  {
    this.aKT = paramzzc;
  }
  
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    boolean bool2 = true;
    if (paramVarArgs != null)
    {
      bool1 = true;
      zzaa.zzbt(bool1);
      if (paramVarArgs.length != 0) {
        break label46;
      }
    }
    label46:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      zzaa.zzbt(bool1);
      return zzaft.zzbb(this.aKT.zzcik().zzchq());
      bool1 = false;
      break;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzadk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */