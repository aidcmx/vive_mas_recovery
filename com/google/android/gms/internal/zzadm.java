package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class zzadm
  extends zzzj
{
  private byte[] zzf(String paramString, byte[] paramArrayOfByte)
    throws NoSuchAlgorithmException
  {
    paramString = MessageDigest.getInstance(paramString);
    paramString.update(paramArrayOfByte);
    return paramString.digest();
  }
  
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    if (paramVarArgs != null)
    {
      bool = true;
      zzaa.zzbt(bool);
      if (paramVarArgs.length < 1) {
        break label40;
      }
    }
    label40:
    for (boolean bool = true;; bool = false)
    {
      zzaa.zzbt(bool);
      if (paramVarArgs[0] != zzafo.aMi) {
        break label45;
      }
      return zzafo.aMi;
      bool = false;
      break;
    }
    label45:
    String str2 = zzzi.zzd(paramVarArgs[0]);
    paramzzyu = "MD5";
    String str1;
    if (paramVarArgs.length > 1)
    {
      if (paramVarArgs[1] == zzafo.aMi) {
        paramzzyu = "MD5";
      }
    }
    else
    {
      str1 = "text";
      if (paramVarArgs.length > 2)
      {
        if (paramVarArgs[2] != zzafo.aMi) {
          break label142;
        }
        str1 = "text";
      }
      label97:
      if (!"text".equals(str1)) {
        break label153;
      }
      paramVarArgs = str2.getBytes();
    }
    for (;;)
    {
      try
      {
        paramVarArgs = new zzafs(zzxt.zzq(zzf(paramzzyu, paramVarArgs)));
        return paramVarArgs;
      }
      catch (NoSuchAlgorithmException paramVarArgs)
      {
        label142:
        label153:
        paramzzyu = String.valueOf(paramzzyu);
        if (paramzzyu.length() == 0) {
          break label244;
        }
      }
      paramzzyu = zzzi.zzd(paramVarArgs[1]);
      break;
      str1 = zzzi.zzd(paramVarArgs[2]);
      break label97;
      if ("base16".equals(str1))
      {
        paramVarArgs = zzxt.zzos(str2);
      }
      else
      {
        paramzzyu = String.valueOf(str1);
        if (paramzzyu.length() != 0) {}
        for (paramzzyu = "Hash: Unknown input format: ".concat(paramzzyu);; paramzzyu = new String("Hash: Unknown input format: ")) {
          throw new RuntimeException(paramzzyu);
        }
      }
    }
    label244:
    for (paramzzyu = "Hash: Unknown algorithm: ".concat(paramzzyu);; paramzzyu = new String("Hash: Unknown algorithm: ")) {
      throw new RuntimeException(paramzzyu, paramVarArgs);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzadm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */