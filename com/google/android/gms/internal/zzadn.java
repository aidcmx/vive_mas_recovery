package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.common.internal.zzaa;

public class zzadn
  implements zzzh
{
  private Context mContext;
  
  public zzadn(Context paramContext)
  {
    this.mContext = ((Context)zzaa.zzy(paramContext));
  }
  
  public zzafk<?> zzb(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    if (paramVarArgs != null) {}
    for (boolean bool = true;; bool = false)
    {
      zzaa.zzbt(bool);
      Object localObject2 = null;
      Object localObject1 = localObject2;
      if (paramVarArgs.length > 0)
      {
        localObject1 = localObject2;
        if (paramVarArgs[0] != zzafo.aMi) {
          localObject1 = zzzi.zzd(zzaft.zza(paramzzyu, paramVarArgs[0]));
        }
      }
      paramzzyu = zzyk.zzag(this.mContext, (String)localObject1);
      if (paramzzyu == null) {
        break;
      }
      return new zzafs(paramzzyu);
    }
    return zzafo.aMi;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzadn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */