package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class zzado
  extends zzzj
{
  private String zza(String paramString, int paramInt, Set<Character> paramSet)
  {
    switch (paramInt)
    {
    default: 
      return paramString;
    }
    try
    {
      paramSet = URLEncoder.encode(paramString, "UTF-8").replaceAll("\\+", "%20");
      return paramSet;
    }
    catch (UnsupportedEncodingException paramSet) {}
    paramString = paramString.replace("\\", "\\\\");
    Iterator localIterator = paramSet.iterator();
    if (localIterator.hasNext())
    {
      String str = ((Character)localIterator.next()).toString();
      paramSet = String.valueOf(str);
      if (paramSet.length() != 0) {}
      for (paramSet = "\\".concat(paramSet);; paramSet = new String("\\"))
      {
        paramString = paramString.replace(str, paramSet);
        break;
      }
    }
    return paramString;
    return paramString;
  }
  
  private void zza(StringBuilder paramStringBuilder, String paramString, int paramInt, Set<Character> paramSet)
  {
    paramStringBuilder.append(zza(paramString, paramInt, paramSet));
  }
  
  private void zza(Set<Character> paramSet, String paramString)
  {
    int i = 0;
    while (i < paramString.length())
    {
      paramSet.add(Character.valueOf(paramString.charAt(i)));
      i += 1;
    }
  }
  
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    int i = 2;
    int j = 1;
    boolean bool;
    label26:
    Object localObject3;
    Object localObject2;
    label47:
    label68:
    Object localObject1;
    if (paramVarArgs != null)
    {
      bool = true;
      zzaa.zzbt(bool);
      if (paramVarArgs.length < 1) {
        break label218;
      }
      bool = true;
      zzaa.zzbt(bool);
      localObject3 = paramVarArgs[0];
      if (paramVarArgs.length <= 1) {
        break label224;
      }
      localObject2 = paramVarArgs[1];
      paramzzyu = "";
      if (paramVarArgs.length > 2)
      {
        if (paramVarArgs[2] != zzafo.aMi) {
          break label232;
        }
        paramzzyu = "";
      }
      localObject1 = "=";
      if (paramVarArgs.length > 3)
      {
        if (paramVarArgs[3] != zzafo.aMi) {
          break label242;
        }
        localObject1 = "=";
      }
      label91:
      paramVarArgs = null;
      if (localObject2 == zzafo.aMi) {
        break label461;
      }
      zzaa.zzbt(localObject2 instanceof zzafs);
      if (!"url".equals(((zzafk)localObject2).zzckf())) {
        break label253;
      }
      i = 1;
    }
    for (;;)
    {
      localObject2 = new StringBuilder();
      if ((localObject3 instanceof zzafp))
      {
        localObject1 = ((List)((zzafp)localObject3).zzckf()).iterator();
        while (((Iterator)localObject1).hasNext())
        {
          localObject3 = (zzafk)((Iterator)localObject1).next();
          if (j == 0) {
            ((StringBuilder)localObject2).append(paramzzyu);
          }
          zza((StringBuilder)localObject2, zzzi.zzd((zzafk)localObject3), i, paramVarArgs);
          j = 0;
        }
        bool = false;
        break;
        label218:
        bool = false;
        break label26;
        label224:
        localObject2 = zzafo.aMi;
        break label47;
        label232:
        paramzzyu = zzzi.zzd(paramVarArgs[2]);
        break label68;
        label242:
        localObject1 = zzzi.zzd(paramVarArgs[3]);
        break label91;
        label253:
        if ("backslash".equals(((zzafk)localObject2).zzckf()))
        {
          paramVarArgs = new HashSet();
          zza(paramVarArgs, paramzzyu);
          zza(paramVarArgs, (String)localObject1);
          paramVarArgs.remove(Character.valueOf('\\'));
          continue;
        }
        return new zzafs("");
      }
      if ((localObject3 instanceof zzafq))
      {
        localObject3 = (Map)((zzafq)localObject3).zzckf();
        Iterator localIterator = ((Map)localObject3).keySet().iterator();
        for (j = 1; localIterator.hasNext(); j = 0)
        {
          String str1 = (String)localIterator.next();
          if (j == 0) {
            ((StringBuilder)localObject2).append(paramzzyu);
          }
          String str2 = zzzi.zzd((zzafk)((Map)localObject3).get(str1));
          zza((StringBuilder)localObject2, str1, i, paramVarArgs);
          ((StringBuilder)localObject2).append((String)localObject1);
          zza((StringBuilder)localObject2, str2, i, paramVarArgs);
        }
      }
      zza((StringBuilder)localObject2, zzzi.zzd((zzafk)localObject3), i, paramVarArgs);
      return new zzafs(((StringBuilder)localObject2).toString());
      label461:
      i = 0;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzado.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */