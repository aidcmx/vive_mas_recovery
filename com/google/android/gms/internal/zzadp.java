package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;
import java.util.Locale;

public class zzadp
  implements zzzh
{
  public zzafk<?> zzb(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    boolean bool2 = true;
    if (paramVarArgs != null)
    {
      bool1 = true;
      zzaa.zzbt(bool1);
      if (paramVarArgs.length != 0) {
        break label48;
      }
    }
    label48:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      zzaa.zzbt(bool1);
      paramzzyu = Locale.getDefault();
      if (paramzzyu != null) {
        break label53;
      }
      return new zzafs("");
      bool1 = false;
      break;
    }
    label53:
    paramzzyu = paramzzyu.getLanguage();
    if (paramzzyu == null) {
      return new zzafs("");
    }
    return new zzafs(paramzzyu.toLowerCase());
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzadp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */