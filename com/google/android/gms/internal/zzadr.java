package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.common.internal.zzaa;
import java.util.Map;

public class zzadr
  extends zzzj
{
  private final zzys.zzc aKN;
  private final Context mContext;
  
  public zzadr(Context paramContext, zzys.zzc paramzzc)
  {
    this.mContext = paramContext;
    this.aKN = paramzzc;
  }
  
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    if (paramVarArgs != null)
    {
      bool = true;
      zzaa.zzbt(bool);
      if (paramVarArgs.length < 1) {
        break label80;
      }
    }
    label80:
    for (boolean bool = true;; bool = false)
    {
      zzaa.zzbt(bool);
      paramzzyu = zzzi.zzd(paramVarArgs[0]);
      Object localObject = this.aKN.zzcik().zzcfc().get(paramzzyu);
      paramzzyu = (zzyu)localObject;
      if (localObject == null)
      {
        paramzzyu = (zzyu)localObject;
        if (paramVarArgs.length > 1) {
          paramzzyu = paramVarArgs[1];
        }
      }
      return zzaft.zzbb(paramzzyu);
      bool = false;
      break;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzadr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */