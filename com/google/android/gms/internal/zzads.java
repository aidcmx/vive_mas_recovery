package com.google.android.gms.internal;

import android.content.Context;
import android.provider.Settings.Secure;
import com.google.android.gms.common.internal.zzaa;

public class zzads
  implements zzzh
{
  private final Context mContext;
  
  public zzads(Context paramContext)
  {
    this.mContext = ((Context)zzaa.zzy(paramContext));
  }
  
  public zzafk<?> zzb(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    boolean bool2 = true;
    if (paramVarArgs != null)
    {
      bool1 = true;
      zzaa.zzbt(bool1);
      if (paramVarArgs.length != 0) {
        break label48;
      }
    }
    label48:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      zzaa.zzbt(bool1);
      paramzzyu = zzciz();
      if (paramzzyu == null) {
        break label53;
      }
      return new zzafs(paramzzyu);
      bool1 = false;
      break;
    }
    label53:
    return zzafo.aMi;
  }
  
  public String zzciz()
  {
    return Settings.Secure.getString(this.mContext.getContentResolver(), "android_id");
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzads.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */