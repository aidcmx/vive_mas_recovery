package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;

public class zzadv
  extends zzzj
{
  private static final zzafm aKU = new zzafm(Double.valueOf(0.0D));
  private static final zzafm aKV = new zzafm(Double.valueOf(2.147483647E9D));
  
  private boolean zzg(zzafk<?> paramzzafk)
  {
    return ((paramzzafk instanceof zzafm)) && (!Double.isNaN(((Double)((zzafm)paramzzafk).zzckf()).doubleValue()));
  }
  
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    boolean bool;
    label21:
    label31:
    double d2;
    double d1;
    if (paramVarArgs != null)
    {
      bool = true;
      zzaa.zzbt(bool);
      if (paramVarArgs.length <= 0) {
        break label116;
      }
      paramzzyu = paramVarArgs[0];
      if (paramVarArgs.length <= 1) {
        break label123;
      }
      paramVarArgs = paramVarArgs[1];
      if ((!zzg(paramzzyu)) || (!zzg(paramVarArgs)) || (!zzzi.zzb(paramzzyu, paramVarArgs))) {
        break label130;
      }
      d2 = ((Double)((zzafm)paramzzyu).zzckf()).doubleValue();
      d1 = ((Double)((zzafm)paramVarArgs).zzckf()).doubleValue();
    }
    for (;;)
    {
      return new zzafm(Double.valueOf(Math.round((d1 - d2) * Math.random() + d2)));
      bool = false;
      break;
      label116:
      paramzzyu = aKU;
      break label21;
      label123:
      paramVarArgs = aKV;
      break label31;
      label130:
      d1 = 2.147483647E9D;
      d2 = 0.0D;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzadv.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */