package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class zzadw
  extends zzzj
{
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    if (paramVarArgs != null)
    {
      bool = true;
      zzaa.zzbt(bool);
      if (paramVarArgs.length < 2) {
        break label54;
      }
    }
    label54:
    for (boolean bool = true;; bool = false)
    {
      zzaa.zzbt(bool);
      if ((paramVarArgs[0] != zzafo.aMi) && (paramVarArgs[1] != zzafo.aMi)) {
        break label60;
      }
      return zzafo.aMi;
      bool = false;
      break;
    }
    label60:
    paramzzyu = zzzi.zzd(paramVarArgs[0]);
    Object localObject = zzzi.zzd(paramVarArgs[1]);
    int j = 64;
    int i = j;
    if (paramVarArgs.length > 2)
    {
      i = j;
      if (paramVarArgs[2] != zzafo.aMi)
      {
        i = j;
        if (zzzi.zza(paramVarArgs[2])) {
          i = 66;
        }
      }
    }
    double d;
    if ((paramVarArgs.length > 3) && (paramVarArgs[3] != zzafo.aMi))
    {
      if (!(paramVarArgs[3] instanceof zzafm)) {
        return zzafo.aMi;
      }
      d = zzzi.zzc(paramVarArgs[3]);
      if ((Double.isInfinite(d)) || (d < 0.0D)) {
        return zzafo.aMi;
      }
    }
    for (j = (int)d;; j = 1)
    {
      paramVarArgs = null;
      try
      {
        localObject = Pattern.compile((String)localObject, i).matcher(paramzzyu);
        paramzzyu = paramVarArgs;
        if (((Matcher)localObject).find())
        {
          paramzzyu = paramVarArgs;
          if (((Matcher)localObject).groupCount() >= j) {
            paramzzyu = ((Matcher)localObject).group(j);
          }
        }
        if (paramzzyu == null) {
          return zzafo.aMi;
        }
        paramzzyu = new zzafs(paramzzyu);
        return paramzzyu;
      }
      catch (PatternSyntaxException paramzzyu)
      {
        return zzafo.aMi;
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzadw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */