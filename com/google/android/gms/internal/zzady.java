package com.google.android.gms.internal;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import com.google.android.gms.common.internal.zzaa;

public class zzady
  implements zzzh
{
  private Context mContext;
  private DisplayMetrics zzaur;
  
  public zzady(Context paramContext)
  {
    this.mContext = paramContext;
    this.zzaur = new DisplayMetrics();
  }
  
  public zzafk<?> zzb(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    boolean bool2 = true;
    if (paramVarArgs != null)
    {
      bool1 = true;
      zzaa.zzbt(bool1);
      if (paramVarArgs.length != 0) {
        break label105;
      }
    }
    label105:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      zzaa.zzbt(bool1);
      ((WindowManager)this.mContext.getSystemService("window")).getDefaultDisplay().getMetrics(this.zzaur);
      paramzzyu = new StringBuilder();
      paramzzyu.append(this.zzaur.widthPixels);
      paramzzyu.append("x");
      paramzzyu.append(this.zzaur.heightPixels);
      return new zzafs(paramzzyu.toString());
      bool1 = false;
      break;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzady.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */