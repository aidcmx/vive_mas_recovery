package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;

public abstract class zzaeh
  extends zzzj
{
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    boolean bool2 = true;
    if (paramVarArgs != null)
    {
      bool1 = true;
      zzaa.zzbt(bool1);
      if (paramVarArgs.length != 2) {
        break label78;
      }
    }
    double d1;
    double d2;
    label78:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      zzaa.zzbt(bool1);
      try
      {
        d1 = zzzi.zzb(paramVarArgs[0]);
        d2 = zzzi.zzb(paramVarArgs[1]);
        if ((!Double.isNaN(d1)) && (!Double.isNaN(d2))) {
          break label97;
        }
        return new zzafl(Boolean.valueOf(false));
      }
      catch (IllegalArgumentException paramzzyu)
      {
        return new zzafl(Boolean.valueOf(false));
      }
      bool1 = false;
      break;
    }
    label97:
    return new zzafl(Boolean.valueOf(zzf(d1, d2)));
  }
  
  protected abstract boolean zzf(double paramDouble1, double paramDouble2);
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaeh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */