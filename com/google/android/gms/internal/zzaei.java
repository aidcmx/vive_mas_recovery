package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class zzaei
  extends zzzj
{
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    boolean bool;
    label27:
    String str;
    if (paramVarArgs != null)
    {
      bool = true;
      zzaa.zzbt(bool);
      if ((paramVarArgs.length != 2) && (paramVarArgs.length != 3)) {
        break label96;
      }
      bool = true;
      zzaa.zzbt(bool);
      paramzzyu = zzzi.zzd(paramVarArgs[0]);
      str = zzzi.zzd(paramVarArgs[1]);
      if (paramVarArgs.length >= 3) {
        break label102;
      }
      bool = false;
      label56:
      if (!bool) {
        break label131;
      }
    }
    label96:
    label102:
    label131:
    for (int i = 66;; i = 64)
    {
      try
      {
        paramzzyu = new zzafl(Boolean.valueOf(Pattern.compile(str, i).matcher(paramzzyu).find()));
        return paramzzyu;
      }
      catch (PatternSyntaxException paramzzyu)
      {
        return new zzafl(Boolean.valueOf(false));
      }
      bool = false;
      break;
      bool = false;
      break label27;
      bool = "true".equalsIgnoreCase(zzzi.zzd(paramVarArgs[2]));
      break label56;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaei.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */