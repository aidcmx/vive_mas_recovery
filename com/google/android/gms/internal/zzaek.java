package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class zzaek
  extends zzzj
{
  private static final Set<String> aKX = new HashSet(Arrays.asList(new String[] { "GET", "HEAD", "POST", "PUT" }));
  private final zzyd aKW;
  
  public zzaek(zzyd paramzzyd)
  {
    this.aKW = paramzzyd;
  }
  
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    Object localObject2 = null;
    boolean bool;
    label21:
    String str1;
    Object localObject1;
    String str2;
    label157:
    label177:
    Object localObject3;
    if (paramVarArgs != null)
    {
      bool = true;
      zzaa.zzbt(bool);
      if (paramVarArgs.length != 1) {
        break label375;
      }
      bool = true;
      zzaa.zzbt(bool);
      zzaa.zzbt(paramVarArgs[0] instanceof zzafq);
      paramzzyu = paramVarArgs[0].zzrf("url");
      zzaa.zzbt(paramzzyu instanceof zzafs);
      str1 = (String)((zzafs)paramzzyu).zzckf();
      localObject1 = paramVarArgs[0].zzrf("method");
      paramzzyu = (zzyu)localObject1;
      if (localObject1 == zzafo.aMi) {
        paramzzyu = new zzafs("GET");
      }
      zzaa.zzbt(paramzzyu instanceof zzafs);
      str2 = (String)((zzafs)paramzzyu).zzckf();
      zzaa.zzbt(aKX.contains(str2));
      paramzzyu = paramVarArgs[0].zzrf("uniqueId");
      if ((paramzzyu != zzafo.aMi) && (paramzzyu != zzafo.aMh) && (!(paramzzyu instanceof zzafs))) {
        break label380;
      }
      bool = true;
      zzaa.zzbt(bool);
      if ((paramzzyu != zzafo.aMi) && (paramzzyu != zzafo.aMh)) {
        break label385;
      }
      paramzzyu = null;
      localObject3 = paramVarArgs[0].zzrf("headers");
      if ((localObject3 != zzafo.aMi) && (!(localObject3 instanceof zzafq))) {
        break label399;
      }
      bool = true;
      label205:
      zzaa.zzbt(bool);
      localObject1 = new HashMap();
      if (localObject3 != zzafo.aMi) {
        break label404;
      }
      localObject1 = null;
    }
    for (;;)
    {
      paramVarArgs = paramVarArgs[0].zzrf("body");
      if ((paramVarArgs == zzafo.aMi) || ((paramVarArgs instanceof zzafs)))
      {
        bool = true;
        label254:
        zzaa.zzbt(bool);
        if (paramVarArgs != zzafo.aMi) {
          break label530;
        }
      }
      label375:
      label380:
      label385:
      label399:
      label404:
      label530:
      for (paramVarArgs = (zzafk<?>[])localObject2;; paramVarArgs = (String)((zzafs)paramVarArgs).zzckf())
      {
        if (((str2.equals("GET")) || (str2.equals("HEAD"))) && (paramVarArgs != null)) {
          zzyl.zzdi(String.format("Body of %s hit will be ignored: %s.", new Object[] { str2, paramVarArgs }));
        }
        this.aKW.zza(str1, str2, paramzzyu, (Map)localObject1, paramVarArgs);
        zzyl.v(String.format("QueueRequest:\n  url = %s,\n  method = %s,\n  uniqueId = %s,\n  headers = %s,\n  body = %s", new Object[] { str1, str2, paramzzyu, localObject1, paramVarArgs }));
        return zzafo.aMi;
        bool = false;
        break;
        bool = false;
        break label21;
        bool = false;
        break label157;
        paramzzyu = (String)((zzafs)paramzzyu).zzckf();
        break label177;
        bool = false;
        break label205;
        localObject3 = ((Map)((zzafq)localObject3).zzckf()).entrySet().iterator();
        while (((Iterator)localObject3).hasNext())
        {
          Object localObject4 = (Map.Entry)((Iterator)localObject3).next();
          String str3 = (String)((Map.Entry)localObject4).getKey();
          localObject4 = (zzafk)((Map.Entry)localObject4).getValue();
          if (!(localObject4 instanceof zzafs)) {
            zzyl.zzdi(String.format("Ignore the non-string value of header key %s.", new Object[] { str3 }));
          } else {
            ((Map)localObject1).put(str3, (String)((zzafs)localObject4).zzckf());
          }
        }
        bool = false;
        break label254;
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaek.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */