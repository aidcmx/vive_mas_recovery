package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.tagmanager.zzbb;
import java.util.Map;

public class zzael
  extends zzzj
{
  private final zzys.zzc aJC;
  private final zzbb aKY;
  
  public zzael(zzbb paramzzbb, zzys.zzc paramzzc)
  {
    this.aKY = paramzzbb;
    this.aJC = paramzzc;
  }
  
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    boolean bool2 = true;
    if (paramVarArgs != null)
    {
      bool1 = true;
      zzaa.zzbt(bool1);
      if ((paramVarArgs.length != 1) && (paramVarArgs.length != 2)) {
        break label153;
      }
      bool1 = true;
      label27:
      zzaa.zzbt(bool1);
      zzaa.zzbt(paramVarArgs[0] instanceof zzafs);
      if (paramVarArgs.length <= 1) {
        break label158;
      }
      paramzzyu = paramVarArgs[1];
      label50:
      bool1 = bool2;
      if (paramzzyu != zzafo.aMi) {
        if (!(paramzzyu instanceof zzafq)) {
          break label165;
        }
      }
    }
    label153:
    label158:
    label165:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      zzaa.zzbt(bool1);
      zzya localzzya = this.aJC.zzcik();
      String str = (String)((zzafs)paramVarArgs[0]).zzckf();
      paramVarArgs = null;
      if (paramzzyu != zzafo.aMi) {
        paramVarArgs = zzaft.zzbr((Map)((zzafq)paramzzyu).zzckf());
      }
      try
      {
        this.aKY.zza(localzzya.zzchr(), str, paramVarArgs, localzzya.currentTimeMillis());
        return zzafo.aMi;
      }
      catch (RemoteException paramzzyu)
      {
        paramzzyu = String.valueOf(paramzzyu.getMessage());
        if (paramzzyu.length() == 0) {
          break label200;
        }
      }
      bool1 = false;
      break;
      bool1 = false;
      break label27;
      paramzzyu = zzafo.aMi;
      break label50;
    }
    label200:
    for (paramzzyu = "Error calling measurement proxy:".concat(paramzzyu);; paramzzyu = new String("Error calling measurement proxy:"))
    {
      zzyl.e(paramzzyu);
      break;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzael.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */