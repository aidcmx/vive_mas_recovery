package com.google.android.gms.internal;

import android.net.Uri;
import android.net.Uri.Builder;
import com.google.android.gms.common.internal.zzaa;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class zzaem
  extends zzzj
{
  private final zzyd aKW;
  
  public zzaem(zzyd paramzzyd)
  {
    this.aKW = paramzzyd;
  }
  
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    boolean bool2 = false;
    boolean bool1;
    label21:
    Object localObject2;
    label40:
    Object localObject1;
    if (paramVarArgs != null)
    {
      bool1 = true;
      zzaa.zzbt(bool1);
      if (paramVarArgs.length < 1) {
        break label268;
      }
      bool1 = true;
      zzaa.zzbt(bool1);
      localObject2 = paramVarArgs[0];
      if ((localObject2 instanceof zzafo)) {
        break label273;
      }
      bool1 = true;
      zzaa.zzbt(bool1);
      if (paramVarArgs.length <= 1) {
        break label278;
      }
      localObject1 = paramVarArgs[1];
      label55:
      if ((localObject1 != zzafo.aMi) && (!(localObject1 instanceof zzafp))) {
        break label286;
      }
      bool1 = true;
      label73:
      zzaa.zzbt(bool1);
      if (paramVarArgs.length <= 2) {
        break label291;
      }
    }
    label268:
    label273:
    label278:
    label286:
    label291:
    for (paramVarArgs = paramVarArgs[2];; paramVarArgs = zzafo.aMi)
    {
      if (paramVarArgs != zzafo.aMi)
      {
        bool1 = bool2;
        if ((paramVarArgs instanceof zzafo)) {}
      }
      else
      {
        bool1 = true;
      }
      zzaa.zzbt(bool1);
      localObject2 = Uri.parse(zzzi.zzd((zzafk)localObject2)).buildUpon();
      if (localObject1 == zzafo.aMi) {
        break label298;
      }
      localObject1 = ((List)((zzafp)localObject1).zzckf()).iterator();
      while (((Iterator)localObject1).hasNext())
      {
        Object localObject3 = (zzafk)((Iterator)localObject1).next();
        zzaa.zzbt(localObject3 instanceof zzafq);
        localObject3 = ((Map)((zzafq)localObject3).zzckf()).entrySet().iterator();
        while (((Iterator)localObject3).hasNext())
        {
          Map.Entry localEntry = (Map.Entry)((Iterator)localObject3).next();
          ((Uri.Builder)localObject2).appendQueryParameter(((String)localEntry.getKey()).toString(), zzzi.zzd(zzaft.zza(paramzzyu, (zzafk)localEntry.getValue())));
        }
      }
      bool1 = false;
      break;
      bool1 = false;
      break label21;
      bool1 = false;
      break label40;
      localObject1 = zzafo.aMi;
      break label55;
      bool1 = false;
      break label73;
    }
    label298:
    paramzzyu = ((Uri.Builder)localObject2).build().toString();
    if (paramVarArgs == zzafo.aMi)
    {
      this.aKW.zzpg(paramzzyu);
      paramzzyu = String.valueOf(paramzzyu);
      if (paramzzyu.length() != 0)
      {
        paramzzyu = "SendPixel: url = ".concat(paramzzyu);
        zzyl.v(paramzzyu);
      }
    }
    for (;;)
    {
      return zzafo.aMi;
      paramzzyu = new String("SendPixel: url = ");
      break;
      paramVarArgs = zzzi.zzd(paramVarArgs);
      this.aKW.zzbh(paramzzyu, paramVarArgs);
      zzyl.v(String.valueOf(paramzzyu).length() + 30 + String.valueOf(paramVarArgs).length() + "SendPixel: url = " + paramzzyu + ", uniqueId = " + paramVarArgs);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */