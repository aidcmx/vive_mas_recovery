package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.analytics.HitBuilders.ScreenViewBuilder;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.analytics.ecommerce.Product;
import com.google.android.gms.analytics.ecommerce.ProductAction;
import com.google.android.gms.analytics.ecommerce.Promotion;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.util.zzf;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class zzaen
  extends zzzj
{
  private static final String ID = zzag.zzkn.toString();
  private static final List<String> aIj = Arrays.asList(new String[] { "detail", "checkout", "checkout_option", "click", "add", "remove", "purchase", "refund" });
  private static final Pattern aIk = Pattern.compile("dimension(\\d+)");
  private static final Pattern aIl = Pattern.compile("metric(\\d+)");
  private static final Set<String> aKZ = zzf.zza("", "0", "false");
  private static final Map<String, String> aLa = zzf.zza("transactionId", "&ti", "transactionAffiliation", "&ta", "transactionTax", "&tt", "transactionShipping", "&ts", "transactionTotal", "&tr", "transactionCurrency", "&cu");
  private static final Map<String, String> aLb = zzf.zza("name", "&in", "sku", "&ic", "category", "&iv", "price", "&ip", "quantity", "&iq", "currency", "&cu");
  private final zzys.zzc aJC;
  private final zzze aLc;
  private Map<String, Object> aLd;
  
  public zzaen(Context paramContext, zzys.zzc paramzzc)
  {
    this(new zzze(paramContext), paramzzc);
  }
  
  public zzaen(zzze paramzzze, zzys.zzc paramzzc)
  {
    this.aJC = paramzzc;
    this.aLc = paramzzze;
  }
  
  private void zza(Tracker paramTracker, zzafk<?> paramzzafk)
  {
    paramTracker.send(zzi(paramzzafk));
  }
  
  private void zza(Tracker paramTracker, zzafk<?> paramzzafk1, zzafk<?> paramzzafk2, zzafk<?> paramzzafk3)
  {
    String str = (String)this.aLd.get("transactionId");
    if (str == null) {
      zzyl.e("Cannot find transactionId in data layer.");
    }
    for (;;)
    {
      return;
      LinkedList localLinkedList = new LinkedList();
      Map localMap;
      Object localObject1;
      Object localObject2;
      try
      {
        localMap = zzi(paramzzafk1);
        localMap.put("&t", "transaction");
        paramzzafk2 = zzj(paramzzafk2).entrySet().iterator();
        while (paramzzafk2.hasNext())
        {
          localObject1 = (Map.Entry)paramzzafk2.next();
          localObject2 = (String)this.aLd.get(((Map.Entry)localObject1).getKey());
          if (localObject2 != null) {
            localMap.put((String)((Map.Entry)localObject1).getValue(), localObject2);
          }
        }
        localLinkedList.add(localMap);
      }
      catch (IllegalArgumentException paramTracker)
      {
        zzyl.zzb("Unable to send transaction", paramTracker);
        return;
      }
      paramzzafk2 = zzqq("transactionProducts");
      if (paramzzafk2 != null)
      {
        paramzzafk2 = paramzzafk2.iterator();
        while (paramzzafk2.hasNext())
        {
          localMap = (Map)paramzzafk2.next();
          if (localMap.get("name") == null)
          {
            zzyl.e("Unable to send transaction item hit due to missing 'name' field.");
            return;
          }
          localObject1 = zzi(paramzzafk1);
          ((Map)localObject1).put("&t", "item");
          ((Map)localObject1).put("&ti", str);
          localObject2 = zzk(paramzzafk3).entrySet().iterator();
          while (((Iterator)localObject2).hasNext())
          {
            Map.Entry localEntry = (Map.Entry)((Iterator)localObject2).next();
            Object localObject3 = localMap.get(localEntry.getKey());
            if (localObject3 != null) {
              ((Map)localObject1).put((String)localEntry.getValue(), localObject3.toString());
            }
          }
          localLinkedList.add(localObject1);
        }
      }
      paramzzafk1 = localLinkedList.iterator();
      while (paramzzafk1.hasNext()) {
        paramTracker.send((Map)paramzzafk1.next());
      }
    }
  }
  
  private Double zzax(Object paramObject)
  {
    if ((paramObject instanceof String))
    {
      try
      {
        paramObject = Double.valueOf((String)paramObject);
        return (Double)paramObject;
      }
      catch (NumberFormatException paramObject)
      {
        paramObject = String.valueOf(((NumberFormatException)paramObject).getMessage());
        if (((String)paramObject).length() == 0) {}
      }
      for (paramObject = "Cannot convert the object to Double: ".concat((String)paramObject);; paramObject = new String("Cannot convert the object to Double: ")) {
        throw new RuntimeException((String)paramObject);
      }
    }
    if ((paramObject instanceof Integer)) {
      return Double.valueOf(((Integer)paramObject).doubleValue());
    }
    if ((paramObject instanceof Double)) {
      return (Double)paramObject;
    }
    paramObject = String.valueOf(paramObject.toString());
    if (((String)paramObject).length() != 0) {}
    for (paramObject = "Cannot convert the object to Double: ".concat((String)paramObject);; paramObject = new String("Cannot convert the object to Double: ")) {
      throw new RuntimeException((String)paramObject);
    }
  }
  
  private Integer zzay(Object paramObject)
  {
    if ((paramObject instanceof String))
    {
      try
      {
        paramObject = Integer.valueOf((String)paramObject);
        return (Integer)paramObject;
      }
      catch (NumberFormatException paramObject)
      {
        paramObject = String.valueOf(((NumberFormatException)paramObject).getMessage());
        if (((String)paramObject).length() == 0) {}
      }
      for (paramObject = "Cannot convert the object to Integer: ".concat((String)paramObject);; paramObject = new String("Cannot convert the object to Integer: ")) {
        throw new RuntimeException((String)paramObject);
      }
    }
    if ((paramObject instanceof Double)) {
      return Integer.valueOf(((Double)paramObject).intValue());
    }
    if ((paramObject instanceof Integer)) {
      return (Integer)paramObject;
    }
    paramObject = String.valueOf(paramObject.toString());
    if (((String)paramObject).length() != 0) {}
    for (paramObject = "Cannot convert the object to Integer: ".concat((String)paramObject);; paramObject = new String("Cannot convert the object to Integer: ")) {
      throw new RuntimeException((String)paramObject);
    }
  }
  
  private void zzb(Tracker paramTracker, zzafk<?> paramzzafk1, zzafk<?> paramzzafk2, zzafk<?> paramzzafk3)
  {
    HitBuilders.ScreenViewBuilder localScreenViewBuilder = new HitBuilders.ScreenViewBuilder();
    Object localObject = zzi(paramzzafk1);
    localScreenViewBuilder.setAll((Map)localObject);
    if (zzzi.zza(paramzzafk2))
    {
      paramzzafk1 = this.aLd.get("ecommerce");
      if (!(paramzzafk1 instanceof Map)) {
        break label650;
      }
      paramzzafk3 = (Map)paramzzafk1;
      paramzzafk2 = (String)((Map)localObject).get("&cu");
      paramzzafk1 = paramzzafk2;
      if (paramzzafk2 == null) {
        paramzzafk1 = (String)paramzzafk3.get("currencyCode");
      }
      if (paramzzafk1 != null) {
        localScreenViewBuilder.set("&cu", paramzzafk1);
      }
      paramzzafk1 = paramzzafk3.get("impressions");
      if ((paramzzafk1 instanceof List))
      {
        paramzzafk2 = ((List)paramzzafk1).iterator();
        for (;;)
        {
          label131:
          if (!paramzzafk2.hasNext()) {
            break label233;
          }
          paramzzafk1 = (Map)paramzzafk2.next();
          try
          {
            localScreenViewBuilder.addImpression(zzbq(paramzzafk1), (String)paramzzafk1.get("list"));
          }
          catch (RuntimeException paramzzafk1)
          {
            paramzzafk1 = String.valueOf(paramzzafk1.getMessage());
            if (paramzzafk1.length() == 0) {}
          }
        }
      }
    }
    else
    {
      for (paramzzafk1 = "Failed to extract a product from event data. ".concat(paramzzafk1);; paramzzafk1 = new String("Failed to extract a product from event data. "))
      {
        zzyl.e(paramzzafk1);
        break label131;
        paramzzafk1 = zzaft.zzl(zzaft.zzm(paramzzafk3));
        break;
      }
    }
    label233:
    paramzzafk1 = null;
    label284:
    label403:
    int i;
    if (paramzzafk3.containsKey("promoClick"))
    {
      paramzzafk1 = (List)((Map)paramzzafk3.get("promoClick")).get("promotions");
      if (paramzzafk1 != null)
      {
        paramzzafk2 = paramzzafk1.iterator();
        for (;;)
        {
          if (!paramzzafk2.hasNext()) {
            break label403;
          }
          paramzzafk1 = (Map)paramzzafk2.next();
          try
          {
            localScreenViewBuilder.addPromotion(zzbp(paramzzafk1));
          }
          catch (RuntimeException paramzzafk1)
          {
            paramzzafk1 = String.valueOf(paramzzafk1.getMessage());
            if (paramzzafk1.length() == 0) {}
          }
        }
      }
    }
    else
    {
      for (paramzzafk1 = "Failed to extract a promotion from event data. ".concat(paramzzafk1);; paramzzafk1 = new String("Failed to extract a promotion from event data. "))
      {
        zzyl.e(paramzzafk1);
        break label284;
        if (!paramzzafk3.containsKey("promoView")) {
          break;
        }
        paramzzafk1 = (List)((Map)paramzzafk3.get("promoView")).get("promotions");
        break;
      }
      if (paramzzafk3.containsKey("promoClick"))
      {
        localScreenViewBuilder.set("&promoa", "click");
        i = 0;
        if (i == 0) {
          break label650;
        }
        paramzzafk1 = aIj.iterator();
        do
        {
          if (!paramzzafk1.hasNext()) {
            break;
          }
          paramzzafk2 = (String)paramzzafk1.next();
        } while (!paramzzafk3.containsKey(paramzzafk2));
        paramzzafk3 = (Map)paramzzafk3.get(paramzzafk2);
        paramzzafk1 = (List)paramzzafk3.get("products");
        if (paramzzafk1 == null) {
          break label611;
        }
        localObject = paramzzafk1.iterator();
        for (;;)
        {
          label513:
          if (!((Iterator)localObject).hasNext()) {
            break label611;
          }
          paramzzafk1 = (Map)((Iterator)localObject).next();
          try
          {
            localScreenViewBuilder.addProduct(zzbq(paramzzafk1));
          }
          catch (RuntimeException paramzzafk1)
          {
            paramzzafk1 = String.valueOf(paramzzafk1.getMessage());
            if (paramzzafk1.length() == 0) {}
          }
        }
      }
    }
    for (paramzzafk1 = "Failed to extract a product from event data. ".concat(paramzzafk1);; paramzzafk1 = new String("Failed to extract a product from event data. "))
    {
      zzyl.e(paramzzafk1);
      break label513;
      localScreenViewBuilder.set("&promoa", "view");
      i = 1;
      break;
    }
    try
    {
      label611:
      if (paramzzafk3.containsKey("actionField")) {}
      for (paramzzafk1 = zzk(paramzzafk2, (Map)paramzzafk3.get("actionField"));; paramzzafk1 = new ProductAction(paramzzafk2))
      {
        localScreenViewBuilder.setProductAction(paramzzafk1);
        label650:
        paramTracker.send(localScreenViewBuilder.build());
        return;
      }
      paramzzafk1 = "Failed to extract a product action from event data. ".concat(paramzzafk1);
    }
    catch (RuntimeException paramzzafk1)
    {
      paramzzafk1 = String.valueOf(paramzzafk1.getMessage());
      if (paramzzafk1.length() == 0) {}
    }
    for (;;)
    {
      zzyl.e(paramzzafk1);
      break;
      paramzzafk1 = new String("Failed to extract a product action from event data. ");
    }
  }
  
  private Promotion zzbp(Map<String, String> paramMap)
  {
    Promotion localPromotion = new Promotion();
    String str = (String)paramMap.get("id");
    if (str != null) {
      localPromotion.setId(String.valueOf(str));
    }
    str = (String)paramMap.get("name");
    if (str != null) {
      localPromotion.setName(String.valueOf(str));
    }
    str = (String)paramMap.get("creative");
    if (str != null) {
      localPromotion.setCreative(String.valueOf(str));
    }
    paramMap = (String)paramMap.get("position");
    if (paramMap != null) {
      localPromotion.setPosition(String.valueOf(paramMap));
    }
    return localPromotion;
  }
  
  private Product zzbq(Map<String, Object> paramMap)
  {
    Product localProduct = new Product();
    Object localObject = paramMap.get("id");
    if (localObject != null) {
      localProduct.setId(String.valueOf(localObject));
    }
    localObject = paramMap.get("name");
    if (localObject != null) {
      localProduct.setName(String.valueOf(localObject));
    }
    localObject = paramMap.get("brand");
    if (localObject != null) {
      localProduct.setBrand(String.valueOf(localObject));
    }
    localObject = paramMap.get("category");
    if (localObject != null) {
      localProduct.setCategory(String.valueOf(localObject));
    }
    localObject = paramMap.get("variant");
    if (localObject != null) {
      localProduct.setVariant(String.valueOf(localObject));
    }
    localObject = paramMap.get("coupon");
    if (localObject != null) {
      localProduct.setCouponCode(String.valueOf(localObject));
    }
    localObject = paramMap.get("position");
    if (localObject != null) {
      localProduct.setPosition(zzay(localObject).intValue());
    }
    localObject = paramMap.get("price");
    if (localObject != null) {
      localProduct.setPrice(zzax(localObject).doubleValue());
    }
    localObject = paramMap.get("quantity");
    if (localObject != null) {
      localProduct.setQuantity(zzay(localObject).intValue());
    }
    Iterator localIterator = paramMap.keySet().iterator();
    for (;;)
    {
      if (!localIterator.hasNext()) {
        break label451;
      }
      localObject = (String)localIterator.next();
      Matcher localMatcher1 = aIk.matcher((CharSequence)localObject);
      int i;
      if (localMatcher1.matches())
      {
        try
        {
          i = Integer.parseInt(localMatcher1.group(1));
          localProduct.setCustomDimension(i, String.valueOf(paramMap.get(localObject)));
        }
        catch (NumberFormatException localNumberFormatException1)
        {
          localObject = String.valueOf(localObject);
          if (((String)localObject).length() == 0) {}
        }
        for (localObject = "illegal number in custom dimension value: ".concat((String)localObject);; localObject = new String("illegal number in custom dimension value: "))
        {
          zzyl.zzdi((String)localObject);
          break;
        }
      }
      else
      {
        Matcher localMatcher2 = aIl.matcher((CharSequence)localObject);
        if (localMatcher2.matches()) {
          try
          {
            i = Integer.parseInt(localMatcher2.group(1));
            localProduct.setCustomMetric(i, zzay(paramMap.get(localObject)).intValue());
          }
          catch (NumberFormatException localNumberFormatException2)
          {
            localObject = String.valueOf(localObject);
            if (((String)localObject).length() == 0) {}
          }
        }
      }
    }
    for (localObject = "illegal number in custom metric value: ".concat((String)localObject);; localObject = new String("illegal number in custom metric value: "))
    {
      zzyl.zzdi((String)localObject);
      break;
    }
    label451:
    return localProduct;
  }
  
  private Map<String, String> zzh(zzafk<?> paramzzafk)
  {
    zzaa.zzy(paramzzafk);
    zzaa.zzbt(paramzzafk instanceof zzafq);
    LinkedHashMap localLinkedHashMap = new LinkedHashMap();
    paramzzafk = zzaft.zzl(zzaft.zzm(paramzzafk));
    zzaa.zzbs(paramzzafk instanceof Map);
    paramzzafk = ((Map)paramzzafk).entrySet().iterator();
    while (paramzzafk.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)paramzzafk.next();
      localLinkedHashMap.put(localEntry.getKey().toString(), localEntry.getValue().toString());
    }
    return localLinkedHashMap;
  }
  
  private Map<String, String> zzi(zzafk<?> paramzzafk)
  {
    paramzzafk = zzh(paramzzafk);
    String str = (String)paramzzafk.get("&aip");
    if ((str != null) && (aKZ.contains(str.toLowerCase()))) {
      paramzzafk.remove("&aip");
    }
    return paramzzafk;
  }
  
  private Map<String, String> zzj(zzafk<?> paramzzafk)
  {
    if (paramzzafk == zzafo.aMi) {
      return aLa;
    }
    return zzh(paramzzafk);
  }
  
  private ProductAction zzk(String paramString, Map<String, Object> paramMap)
  {
    paramString = new ProductAction(paramString);
    Object localObject = paramMap.get("id");
    if (localObject != null) {
      paramString.setTransactionId(String.valueOf(localObject));
    }
    localObject = paramMap.get("affiliation");
    if (localObject != null) {
      paramString.setTransactionAffiliation(String.valueOf(localObject));
    }
    localObject = paramMap.get("coupon");
    if (localObject != null) {
      paramString.setTransactionCouponCode(String.valueOf(localObject));
    }
    localObject = paramMap.get("list");
    if (localObject != null) {
      paramString.setProductActionList(String.valueOf(localObject));
    }
    localObject = paramMap.get("option");
    if (localObject != null) {
      paramString.setCheckoutOptions(String.valueOf(localObject));
    }
    localObject = paramMap.get("revenue");
    if (localObject != null) {
      paramString.setTransactionRevenue(zzax(localObject).doubleValue());
    }
    localObject = paramMap.get("tax");
    if (localObject != null) {
      paramString.setTransactionTax(zzax(localObject).doubleValue());
    }
    localObject = paramMap.get("shipping");
    if (localObject != null) {
      paramString.setTransactionShipping(zzax(localObject).doubleValue());
    }
    paramMap = paramMap.get("step");
    if (paramMap != null) {
      paramString.setCheckoutStep(zzay(paramMap).intValue());
    }
    return paramString;
  }
  
  private Map<String, String> zzk(zzafk<?> paramzzafk)
  {
    if (paramzzafk == zzafo.aMi) {
      return aLb;
    }
    return zzh(paramzzafk);
  }
  
  private List<Map<String, Object>> zzqq(String paramString)
  {
    paramString = this.aLd.get(paramString);
    if (paramString == null)
    {
      paramString = null;
      return paramString;
    }
    if (!(paramString instanceof List)) {
      throw new IllegalArgumentException("transactionProducts should be of type List.");
    }
    List localList = (List)paramString;
    Iterator localIterator = localList.iterator();
    do
    {
      paramString = localList;
      if (!localIterator.hasNext()) {
        break;
      }
    } while ((localIterator.next() instanceof Map));
    throw new IllegalArgumentException("Each element of transactionProducts should be of type Map.");
  }
  
  protected zzafk<?> zza(zzyu paramzzyu, zzafk<?>... paramVarArgs)
  {
    boolean bool2 = false;
    boolean bool1;
    if (paramVarArgs != null)
    {
      bool1 = true;
      zzaa.zzbt(bool1);
      if (paramVarArgs.length < 1) {
        break label276;
      }
      bool1 = true;
      label21:
      zzaa.zzbt(bool1);
    }
    for (;;)
    {
      zzafk<?> localzzafk;
      Object localObject2;
      Object localObject3;
      Tracker localTracker;
      try
      {
        this.aLd = zzaft.zzao(this.aJC.zzcik().zzchq());
        localzzafk = paramVarArgs[0];
        if (paramVarArgs.length > 1)
        {
          paramzzyu = paramVarArgs[1];
          if (paramVarArgs.length <= 2) {
            continue;
          }
          localObject1 = paramVarArgs[2];
          if (paramVarArgs.length <= 3) {
            continue;
          }
          localObject2 = paramVarArgs[3];
          if (paramVarArgs.length <= 4) {
            continue;
          }
          localObject3 = paramVarArgs[4];
          if (paramVarArgs.length <= 5) {
            continue;
          }
          localObject4 = paramVarArgs[5];
          if (paramVarArgs.length <= 6) {
            continue;
          }
          localObject5 = paramVarArgs[6];
          if (paramVarArgs.length <= 7) {
            continue;
          }
          localObject6 = paramVarArgs[7];
          if (paramVarArgs.length <= 8) {
            continue;
          }
          paramVarArgs = paramVarArgs[8];
          zzaa.zzbt(localzzafk instanceof zzafq);
          if (localObject2 == zzafo.aMi) {
            break label441;
          }
          if (!(localObject2 instanceof zzafq)) {
            break label456;
          }
          break label441;
          zzaa.zzbt(bool1);
          if (localObject3 == zzafo.aMi) {
            break label446;
          }
          if (!(localObject3 instanceof zzafq)) {
            break label461;
          }
          break label446;
          zzaa.zzbt(bool1);
          if (localObject6 == zzafo.aMi) {
            break label451;
          }
          bool1 = bool2;
          if ((localObject6 instanceof zzafq)) {
            break label451;
          }
          zzaa.zzbt(bool1);
          localTracker = this.aLc.zzpu("_GTM_DEFAULT_TRACKER_");
          localTracker.enableAdvertisingIdCollection(zzzi.zza(paramVarArgs));
          if (!zzzi.zza((zzafk)localObject4)) {
            continue;
          }
          zzb(localTracker, localzzafk, (zzafk)localObject5, (zzafk)localObject6);
          return zzafo.aMi;
          bool1 = false;
          break;
          label276:
          bool1 = false;
          break label21;
        }
        paramzzyu = new zzafl(Boolean.valueOf(true));
        continue;
        Object localObject1 = new zzafl(Boolean.valueOf(false));
        continue;
        localObject2 = zzafo.aMi;
        continue;
        localObject3 = zzafo.aMi;
        continue;
        Object localObject4 = new zzafl(Boolean.valueOf(false));
        continue;
        Object localObject5 = new zzafl(Boolean.valueOf(false));
        continue;
        Object localObject6 = zzafo.aMi;
        continue;
        paramVarArgs = new zzafl(Boolean.valueOf(false));
        continue;
        if (zzzi.zza(paramzzyu))
        {
          zza(localTracker, localzzafk);
          continue;
        }
        if (!zzzi.zza((zzafk)localObject1)) {
          break label432;
        }
      }
      finally
      {
        this.aLd = null;
      }
      zza(localTracker, localzzafk, (zzafk)localObject2, (zzafk)localObject3);
      continue;
      label432:
      zzyl.zzdi("Ignoring unknown tag.");
      continue;
      label441:
      bool1 = true;
      continue;
      label446:
      bool1 = true;
      continue;
      label451:
      bool1 = true;
      continue;
      label456:
      bool1 = false;
      continue;
      label461:
      bool1 = false;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaen.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */