package com.google.android.gms.internal;

import android.support.annotation.Nullable;
import com.google.android.gms.common.internal.zzaa;

public class zzaeo
{
  private final String aDY;
  @Nullable
  private final String aGD;
  @Nullable
  private final String aIv;
  @Nullable
  private final String aLe;
  private final boolean aLf;
  @Nullable
  private final String aLg;
  
  public zzaeo(String paramString1, String paramString2, @Nullable String paramString3, boolean paramBoolean, @Nullable String paramString4)
  {
    this(paramString1, paramString2, paramString3, paramBoolean, paramString4, "");
  }
  
  public zzaeo(String paramString1, String paramString2, @Nullable String paramString3, boolean paramBoolean, @Nullable String paramString4, String paramString5)
  {
    zzaa.zzy(paramString1);
    zzaa.zzy(paramString5);
    this.aDY = paramString1;
    this.aIv = paramString2;
    this.aLe = paramString3;
    this.aLf = paramBoolean;
    this.aLg = paramString4;
    this.aGD = paramString5;
  }
  
  public String getContainerId()
  {
    return this.aDY;
  }
  
  public String zzcja()
  {
    return this.aIv;
  }
  
  public String zzcjb()
  {
    return this.aLe;
  }
  
  public String zzcjc()
  {
    if (this.aLe != null)
    {
      String str1 = this.aLe;
      String str2 = this.aDY;
      return String.valueOf(str1).length() + 1 + String.valueOf(str2).length() + str1 + "_" + str2;
    }
    return this.aDY;
  }
  
  public boolean zzcjd()
  {
    return this.aLf;
  }
  
  public String zzcje()
  {
    return this.aLg;
  }
  
  public String zzcjf()
  {
    return this.aGD;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaeo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */