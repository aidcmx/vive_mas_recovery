package com.google.android.gms.internal;

import android.support.annotation.Nullable;
import com.google.android.gms.common.internal.zzaa;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class zzaeq
{
  static zzafh zza(JSONArray paramJSONArray, List<zzafg> paramList1, List<zzafg> paramList2)
    throws zzaep, JSONException
  {
    zzafh.zza localzza = new zzafh.zza();
    int i = 0;
    if (i < paramJSONArray.length())
    {
      Object localObject = paramJSONArray.getJSONArray(i);
      int j;
      if (((JSONArray)localObject).getString(0).equals("if"))
      {
        j = 1;
        while (j < ((JSONArray)localObject).length())
        {
          localzza.zzd((zzafg)paramList2.get(((JSONArray)localObject).getInt(j)));
          j += 1;
        }
      }
      if (((JSONArray)localObject).getString(0).equals("unless"))
      {
        j = 1;
        while (j < ((JSONArray)localObject).length())
        {
          localzza.zze((zzafg)paramList2.get(((JSONArray)localObject).getInt(j)));
          j += 1;
        }
      }
      if (((JSONArray)localObject).getString(0).equals("add"))
      {
        j = 1;
        while (j < ((JSONArray)localObject).length())
        {
          localzza.zzf((zzafg)paramList1.get(((JSONArray)localObject).getInt(j)));
          j += 1;
        }
      }
      if (((JSONArray)localObject).getString(0).equals("block"))
      {
        j = 1;
        while (j < ((JSONArray)localObject).length())
        {
          localzza.zzg((zzafg)paramList1.get(((JSONArray)localObject).getInt(j)));
          j += 1;
        }
      }
      localObject = String.valueOf(((JSONArray)localObject).getString(0));
      if (((String)localObject).length() != 0) {}
      for (localObject = "Unknown Rule property: ".concat((String)localObject);; localObject = new String("Unknown Rule property: "))
      {
        zzqt((String)localObject);
        i += 1;
        break;
      }
    }
    return localzza.zzcjz();
  }
  
  public static zzzg zzaz(Object paramObject)
    throws JSONException
  {
    int i = 1;
    int j = 0;
    String str;
    if ((paramObject instanceof JSONObject))
    {
      paramObject = (JSONObject)paramObject;
      str = ((JSONObject)paramObject).getString("name");
      localObject1 = ((JSONObject)paramObject).getJSONArray("params");
      paramObject = ((JSONObject)paramObject).getJSONArray("instructions");
    }
    Object localObject2;
    for (;;)
    {
      localObject2 = new ArrayList();
      i = 0;
      while (i < ((JSONArray)localObject1).length())
      {
        ((List)localObject2).add(((JSONArray)localObject1).getString(i));
        i += 1;
      }
      if (!(paramObject instanceof JSONArray)) {
        break;
      }
      localObject2 = (JSONArray)paramObject;
      if (((JSONArray)localObject2).length() >= 3) {}
      for (boolean bool = true;; bool = false)
      {
        zzaa.zzbt(bool);
        str = ((JSONArray)localObject2).getString(1);
        paramObject = ((JSONArray)localObject2).getJSONArray(2);
        localObject1 = new JSONArray();
        while (i < ((JSONArray)paramObject).length())
        {
          zzaa.zzbt(((JSONArray)paramObject).get(i) instanceof String);
          ((JSONArray)localObject1).put(((JSONArray)paramObject).get(i));
          i += 1;
        }
      }
      paramObject = new JSONArray();
      i = 3;
      while (i < ((JSONArray)localObject2).length())
      {
        ((JSONArray)paramObject).put(((JSONArray)localObject2).get(i));
        i += 1;
      }
    }
    throw new IllegalArgumentException("invalid JSON in runtime section");
    Object localObject1 = new ArrayList();
    i = j;
    if (i < ((JSONArray)paramObject).length())
    {
      JSONArray localJSONArray = ((JSONArray)paramObject).getJSONArray(i);
      if (localJSONArray.length() == 0) {}
      for (;;)
      {
        i += 1;
        break;
        ((List)localObject1).add(zzd(localJSONArray));
      }
    }
    return new zzzg(null, str, (List)localObject2, (List)localObject1);
  }
  
  static zzafg zzb(JSONObject paramJSONObject, List<String> paramList)
    throws zzaep, JSONException
  {
    zzafg.zza localzza = new zzafg.zza();
    Iterator localIterator = paramJSONObject.keys();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      zzafj localzzafj = zzb(paramJSONObject.get(str), paramList).zzckd();
      if ("push_after_evaluate".equals(str)) {
        localzza.zzb(localzzafj);
      } else {
        localzza.zza(str, localzzafj);
      }
    }
    return localzza.zzcju();
  }
  
  static zzafj.zza zzb(Object paramObject, List<String> paramList)
    throws zzaep, JSONException
  {
    int i = 2;
    if ((paramObject instanceof JSONArray))
    {
      JSONArray localJSONArray = (JSONArray)paramObject;
      String str = localJSONArray.getString(0);
      if (str.equals("escape"))
      {
        paramObject = zzb(localJSONArray.get(1), paramList);
        while (i < localJSONArray.length())
        {
          ((zzafj.zza)paramObject).zzaak(localJSONArray.getInt(i));
          i += 1;
        }
        return (zzafj.zza)paramObject;
      }
      if (str.equals("list"))
      {
        paramObject = new ArrayList();
        i = 1;
        while (i < localJSONArray.length())
        {
          ((List)paramObject).add(zzb(localJSONArray.get(i), paramList).zzckd());
          i += 1;
        }
        paramObject = new zzafj.zza(2, paramObject);
        ((zzafj.zza)paramObject).zzcq(true);
        return (zzafj.zza)paramObject;
      }
      if (str.equals("map"))
      {
        paramObject = new HashMap();
        i = 1;
        while (i < localJSONArray.length())
        {
          ((Map)paramObject).put(zzb(localJSONArray.get(i), paramList).zzckd(), zzb(localJSONArray.get(i + 1), paramList).zzckd());
          i += 2;
        }
        paramObject = new zzafj.zza(3, paramObject);
        ((zzafj.zza)paramObject).zzcq(true);
        return (zzafj.zza)paramObject;
      }
      if (str.equals("macro"))
      {
        paramObject = new zzafj.zza(4, paramList.get(localJSONArray.getInt(1)));
        ((zzafj.zza)paramObject).zzcq(true);
        return (zzafj.zza)paramObject;
      }
      if (str.equals("template"))
      {
        paramObject = new ArrayList();
        i = 1;
        while (i < localJSONArray.length())
        {
          ((List)paramObject).add(zzb(localJSONArray.get(i), paramList).zzckd());
          i += 1;
        }
        paramObject = new zzafj.zza(7, paramObject);
        ((zzafj.zza)paramObject).zzcq(true);
        return (zzafj.zza)paramObject;
      }
      paramObject = String.valueOf(paramObject);
      zzqt(String.valueOf(paramObject).length() + 20 + "Invalid value type: " + (String)paramObject);
      return null;
    }
    if ((paramObject instanceof Boolean)) {
      return new zzafj.zza(8, paramObject);
    }
    if ((paramObject instanceof Integer)) {
      return new zzafj.zza(6, paramObject);
    }
    if ((paramObject instanceof String)) {
      return new zzafj.zza(1, paramObject);
    }
    paramObject = String.valueOf(paramObject);
    zzqt(String.valueOf(paramObject).length() + 20 + "Invalid value type: " + (String)paramObject);
    return null;
  }
  
  static List<zzafg> zzb(JSONArray paramJSONArray, List<String> paramList)
    throws JSONException, zzaep
  {
    ArrayList localArrayList = new ArrayList();
    int i = 0;
    while (i < paramJSONArray.length())
    {
      localArrayList.add(zzb(paramJSONArray.getJSONObject(i), paramList));
      i += 1;
    }
    return localArrayList;
  }
  
  static List<String> zzc(JSONArray paramJSONArray)
    throws JSONException
  {
    ArrayList localArrayList = new ArrayList();
    int i = 0;
    while (i < paramJSONArray.length())
    {
      localArrayList.add(paramJSONArray.getJSONObject(i).getString("instance_name"));
      i += 1;
    }
    return localArrayList;
  }
  
  private static zzafr zzd(JSONArray paramJSONArray)
    throws JSONException
  {
    int i = 1;
    boolean bool;
    String str;
    ArrayList localArrayList;
    label30:
    Object localObject;
    if (paramJSONArray.length() > 0)
    {
      bool = true;
      zzaa.zzbt(bool);
      str = paramJSONArray.getString(0);
      localArrayList = new ArrayList();
      if (i >= paramJSONArray.length()) {
        break label134;
      }
      localObject = paramJSONArray.get(i);
      if (!(localObject instanceof JSONArray)) {
        break label96;
      }
      localObject = (JSONArray)localObject;
      if (((JSONArray)localObject).length() != 0) {
        break label80;
      }
    }
    for (;;)
    {
      i += 1;
      break label30;
      bool = false;
      break;
      label80:
      localArrayList.add(zzd((JSONArray)localObject));
      continue;
      label96:
      if (localObject == JSONObject.NULL) {
        localArrayList.add(zzafo.aMh);
      } else {
        localArrayList.add(zzaft.zzbb(localObject));
      }
    }
    label134:
    return new zzafr(str, localArrayList);
  }
  
  static zzaff zzqr(String paramString)
    throws JSONException, zzaep
  {
    paramString = new JSONObject(paramString).get("resource");
    List localList1;
    List localList2;
    if ((paramString instanceof JSONObject))
    {
      localObject1 = (JSONObject)paramString;
      paramString = new zzaff.zza();
      paramString.zzrc(((JSONObject)localObject1).optString("version"));
      Object localObject2 = zzc(((JSONObject)localObject1).getJSONArray("macros"));
      localList1 = zzb(((JSONObject)localObject1).getJSONArray("tags"), (List)localObject2);
      localList2 = zzb(((JSONObject)localObject1).getJSONArray("predicates"), (List)localObject2);
      localObject2 = zzb(((JSONObject)localObject1).getJSONArray("macros"), (List)localObject2).iterator();
      while (((Iterator)localObject2).hasNext()) {
        paramString.zzc((zzafg)((Iterator)localObject2).next());
      }
    }
    throw new zzaep("Resource map not found");
    Object localObject1 = ((JSONObject)localObject1).getJSONArray("rules");
    int i = 0;
    while (i < ((JSONArray)localObject1).length())
    {
      paramString.zza(zza(((JSONArray)localObject1).getJSONArray(i), localList1, localList2));
      i += 1;
    }
    return paramString.zzcjs();
  }
  
  @Nullable
  static zzafi zzqs(String paramString)
    throws JSONException, zzaep
  {
    Object localObject = new JSONObject(paramString);
    paramString = ((JSONObject)localObject).optJSONArray("runtime");
    if (paramString == null) {
      return null;
    }
    zzafi.zza localzza = new zzafi.zza();
    localObject = ((JSONObject)localObject).get("resource");
    int i;
    if ((localObject instanceof JSONObject))
    {
      localzza.zzrd(((JSONObject)localObject).optString("version"));
      i = 0;
      if (i >= paramString.length()) {
        break label124;
      }
      localObject = paramString.get(i);
      if ((!(localObject instanceof JSONArray)) || (((JSONArray)localObject).length() != 0)) {
        break label112;
      }
    }
    for (;;)
    {
      i += 1;
      break;
      throw new zzaep("Resource map not found");
      label112:
      localzza.zza(zzaz(localObject));
    }
    label124:
    return localzza.zzckb();
  }
  
  private static void zzqt(String paramString)
    throws zzaep
  {
    zzyl.e(paramString);
    throw new zzaep(paramString);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaeq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */