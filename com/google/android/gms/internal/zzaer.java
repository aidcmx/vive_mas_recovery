package com.google.android.gms.internal;

import android.support.annotation.Nullable;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.util.zze;
import com.google.android.gms.common.util.zzh;

public abstract class zzaer
{
  @Nullable
  protected final zzxw aIB;
  protected final zzaev aLh;
  protected final zzaet aLi;
  protected final zze zzaql;
  private int zzbcm;
  
  public zzaer(int paramInt, zzaev paramzzaev, zzaet paramzzaet, @Nullable zzxw paramzzxw)
  {
    this(paramInt, paramzzaev, paramzzaet, paramzzxw, zzh.zzayl());
  }
  
  public zzaer(int paramInt, zzaev paramzzaev, zzaet paramzzaet, @Nullable zzxw paramzzxw, zze paramzze)
  {
    this.aLh = ((zzaev)zzaa.zzy(paramzzaev));
    zzaa.zzy(paramzzaev.zzcjh());
    this.zzbcm = paramInt;
    this.aLi = ((zzaet)zzaa.zzy(paramzzaet));
    this.zzaql = ((zze)zzaa.zzy(paramzze));
    this.aIB = paramzzxw;
  }
  
  protected abstract void zza(zzaew paramzzaew);
  
  public String zzaaj(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return "Unknown reason";
    case 0: 
      return "Resource not available";
    case 1: 
      return "IOError";
    }
    return "Server error";
  }
  
  public void zzam(byte[] paramArrayOfByte)
  {
    Object localObject = zzan(paramArrayOfByte);
    if ((this.aIB != null) && (this.zzbcm == 0)) {
      this.aIB.zzcen();
    }
    if ((localObject != null) && (((zzaew)localObject).getStatus() == Status.xZ))
    {
      zzaff localzzaff = ((zzaew)localObject).zzcji().zzcjn();
      paramArrayOfByte = new zzaew.zza(this.aLh.zzcjh(), paramArrayOfByte, localzzaff, this.zzaql.currentTimeMillis());
      localObject = ((zzaew)localObject).zzcjj();
    }
    for (paramArrayOfByte = new zzaew(Status.xZ, this.zzbcm, paramArrayOfByte, (zzafi)localObject);; paramArrayOfByte = new zzaew(Status.yb, this.zzbcm))
    {
      zza(paramArrayOfByte);
      return;
    }
  }
  
  protected zzaew zzan(byte[] paramArrayOfByte)
  {
    try
    {
      paramArrayOfByte = this.aLi.zzao(paramArrayOfByte);
      return paramArrayOfByte;
    }
    catch (zzaep paramArrayOfByte)
    {
      zzyl.zzdh("Resource data is corrupted");
    }
    return null;
  }
  
  public void zzw(int paramInt1, int paramInt2)
  {
    if ((this.aIB != null) && (paramInt2 == 0) && (paramInt1 == 3)) {
      this.aIB.zzcem();
    }
    String str1 = String.valueOf(this.aLh.zzcjh().getContainerId());
    String str2 = String.valueOf(zzaaj(paramInt1));
    zzyl.v(String.valueOf(str1).length() + 61 + String.valueOf(str2).length() + "Failed to fetch the container resource for the container \"" + str1 + "\": " + str2);
    zza(new zzaew(Status.yb, paramInt2));
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */