package com.google.android.gms.internal;

import android.content.Context;
import android.support.annotation.Nullable;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.util.zze;
import com.google.android.gms.common.util.zzh;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class zzaes
{
  private String aEC = null;
  private final zzaex aLj;
  final Map<String, zzc<zzaff>> aLk = new HashMap();
  private final Map<String, zzafd> aLl;
  private final Context mContext;
  private final zze zzaql;
  
  public zzaes(Context paramContext)
  {
    this(paramContext, new HashMap(), new zzaex(paramContext), zzh.zzayl());
  }
  
  zzaes(Context paramContext, Map<String, zzafd> paramMap, zzaex paramzzaex, zze paramzze)
  {
    this.mContext = paramContext.getApplicationContext();
    this.zzaql = paramzze;
    this.aLj = paramzzaex;
    this.aLl = paramMap;
  }
  
  private void zza(zzaev paramzzaev, List<Integer> paramList, int paramInt, zza paramzza)
  {
    zzaeo localzzaeo = paramzzaev.zzcjh();
    String str = String.valueOf(localzzaeo.getContainerId());
    zzyl.v(String.valueOf(str).length() + 52 + "Attempting to fetch container " + str + " from a saved resource");
    this.aLj.zza(localzzaeo.zzcjc(), new zzb(1, paramzzaev, zzaeu.aLr, paramList, paramInt, paramzza, null));
  }
  
  private void zzb(zzaev paramzzaev, List<Integer> paramList, int paramInt, zza paramzza)
  {
    zzaeo localzzaeo = paramzzaev.zzcjh();
    String str = String.valueOf(localzzaeo.getContainerId());
    zzyl.v(String.valueOf(str).length() + 56 + "Attempting to fetch container " + str + " from the default resource");
    this.aLj.zza(localzzaeo.zzcjc(), localzzaeo.zzcja(), new zzb(2, paramzzaev, zzaeu.aLr, paramList, paramInt, paramzza, null));
  }
  
  private void zzb(zzaev paramzzaev, List<Integer> paramList, int paramInt, zza paramzza, @Nullable zzxw paramzzxw)
  {
    Object localObject2 = paramzzaev.zzcjh();
    Object localObject1 = (zzc)this.aLk.get(((zzaeo)localObject2).getContainerId());
    int i;
    if (paramzzaev.zzcjh().zzcjd()) {
      i = 1;
    }
    for (;;)
    {
      if (i != 0)
      {
        localObject1 = (zzafd)this.aLl.get(paramzzaev.getId());
        if (localObject1 != null) {
          break label259;
        }
        if (0 == 0)
        {
          localObject1 = new zzafd();
          label79:
          this.aLl.put(paramzzaev.getId(), localObject1);
        }
      }
      label259:
      for (;;)
      {
        localObject2 = String.valueOf(((zzaeo)localObject2).getContainerId());
        zzyl.v(String.valueOf(localObject2).length() + 43 + "Attempting to fetch container " + (String)localObject2 + " from network");
        ((zzafd)localObject1).zza(this.mContext, paramzzaev, 0L, new zzb(0, paramzzaev, zzaeu.aLr, paramList, paramInt, paramzza, paramzzxw));
        return;
        if (localObject1 != null) {}
        for (long l = ((zzc)localObject1).zzcjg();; l = this.aLj.zzqv(((zzaeo)localObject2).getContainerId()))
        {
          if (l + 900000L >= this.zzaql.currentTimeMillis()) {
            break label262;
          }
          i = 1;
          break;
        }
        localObject1 = new zzafd(this.aEC);
        break label79;
        zza(paramzzaev, paramList, paramInt + 1, paramzza, paramzzxw);
        return;
      }
      label262:
      i = 0;
    }
  }
  
  void zza(Status paramStatus, zzaew.zza paramzza)
  {
    Object localObject = paramzza.zzcjm().getContainerId();
    paramzza = paramzza.zzcjn();
    if (this.aLk.containsKey(localObject))
    {
      localObject = (zzc)this.aLk.get(localObject);
      ((zzc)localObject).zzbx(this.zzaql.currentTimeMillis());
      if (paramStatus == Status.xZ)
      {
        ((zzc)localObject).zzem(paramStatus);
        ((zzc)localObject).zzba(paramzza);
      }
      return;
    }
    this.aLk.put(localObject, new zzc(paramStatus, paramzza, this.zzaql.currentTimeMillis()));
  }
  
  void zza(zzaev paramzzaev, List<Integer> paramList, int paramInt, zza paramzza, @Nullable zzxw paramzzxw)
  {
    if (paramInt == 0) {
      zzyl.v("Starting to fetch a new resource");
    }
    if (paramInt >= paramList.size())
    {
      paramzzaev = String.valueOf(paramzzaev.zzcjh().getContainerId());
      if (paramzzaev.length() != 0) {}
      for (paramzzaev = "There is no valid resource for the container: ".concat(paramzzaev);; paramzzaev = new String("There is no valid resource for the container: "))
      {
        zzyl.v(paramzzaev);
        paramzza.zza(new zzaew(new Status(16, paramzzaev), ((Integer)paramList.get(paramInt - 1)).intValue()));
        return;
      }
    }
    switch (((Integer)paramList.get(paramInt)).intValue())
    {
    default: 
      throw new UnsupportedOperationException(36 + "Unknown fetching source: " + paramInt);
    case 0: 
      zzb(paramzzaev, paramList, paramInt, paramzza, paramzzxw);
      return;
    case 1: 
      zza(paramzzaev, paramList, paramInt, paramzza);
      return;
    }
    zzb(paramzzaev, paramList, paramInt, paramzza);
  }
  
  public void zza(String paramString1, @Nullable String paramString2, @Nullable String paramString3, List<Integer> paramList, zza paramzza, zzxw paramzzxw)
  {
    if (!paramList.isEmpty()) {}
    for (boolean bool = true;; bool = false)
    {
      zzaa.zzbt(bool);
      zza(new zzaev().zza(new zzaeo(paramString1, paramString2, paramString3, zzqu(paramString1), zzyq.zzcib().zzcic())), Collections.unmodifiableList(paramList), 0, paramzza, paramzzxw);
      return;
    }
  }
  
  boolean zzqu(String paramString)
  {
    zzyq localzzyq = zzyq.zzcib();
    return (localzzyq.isPreview()) && (paramString.equals(localzzyq.getContainerId()));
  }
  
  public static abstract interface zza
  {
    public abstract void zza(zzaew paramzzaew);
  }
  
  class zzb
    extends zzaer
  {
    private final zzaes.zza aLm;
    private final List<Integer> aLn;
    private final int aLo;
    
    zzb(zzaev paramzzaev, zzaet paramzzaet, List<Integer> paramList, int paramInt, zzaes.zza paramzza, zzxw paramzzxw)
    {
      super(paramzzaet, paramList, localzzxw);
      this.aLm = paramzzxw;
      this.aLn = paramInt;
      this.aLo = paramzza;
    }
    
    protected void zza(zzaew paramzzaew)
    {
      Object localObject;
      int i;
      if (paramzzaew.getStatus() == Status.xZ)
      {
        localObject = String.valueOf(paramzzaew.zzcjk());
        if (((String)localObject).length() != 0)
        {
          localObject = "Container resource successfully loaded from ".concat((String)localObject);
          zzyl.v((String)localObject);
          if (paramzzaew.getSource() != 0) {
            break label141;
          }
          localObject = paramzzaew.zzcji();
          if (!((zzaew.zza)localObject).zzcjm().zzcjd()) {
            break label88;
          }
          i = 1;
        }
      }
      for (;;)
      {
        if (i != 0)
        {
          this.aLm.zza(paramzzaew);
          return;
          localObject = new String("Container resource successfully loaded from ");
          break;
          label88:
          zzaes.this.zza(paramzzaew.getStatus(), (zzaew.zza)localObject);
          if ((((zzaew.zza)localObject).zzcjl() == null) || (((zzaew.zza)localObject).zzcjl().length <= 0)) {
            break label337;
          }
          zzaes.zza(zzaes.this).zzg(((zzaew.zza)localObject).zzcjm().zzcjc(), ((zzaew.zza)localObject).zzcjl());
          i = 1;
          continue;
          label141:
          i = 1;
          continue;
        }
        String str = String.valueOf(paramzzaew.zzcjk());
        if (paramzzaew.getStatus().isSuccess())
        {
          localObject = "SUCCESS";
          zzyl.v(String.valueOf(str).length() + 54 + String.valueOf(localObject).length() + "Cannot fetch a valid resource from " + str + ". Response status: " + (String)localObject);
          if (paramzzaew.getStatus().isSuccess())
          {
            localObject = String.valueOf(paramzzaew.zzcjk());
            if (((String)localObject).length() == 0) {
              break label324;
            }
          }
        }
        label324:
        for (localObject = "Response source: ".concat((String)localObject);; localObject = new String("Response source: "))
        {
          zzyl.v((String)localObject);
          i = paramzzaew.zzcji().zzcjl().length;
          zzyl.v(26 + "Response size: " + i);
          zzaes.this.zza(this.aLh, this.aLn, this.aLo + 1, this.aLm, this.aIB);
          return;
          localObject = "FAILURE";
          break;
        }
        label337:
        i = 0;
      }
    }
  }
  
  static class zzc<T>
  {
    private long aLq;
    private Status hv;
    private T mData;
    
    public zzc(Status paramStatus, T paramT, long paramLong)
    {
      this.hv = paramStatus;
      this.mData = paramT;
      this.aLq = paramLong;
    }
    
    public void zzba(T paramT)
    {
      this.mData = paramT;
    }
    
    public void zzbx(long paramLong)
    {
      this.aLq = paramLong;
    }
    
    public long zzcjg()
    {
      return this.aLq;
    }
    
    public void zzem(Status paramStatus)
    {
      this.hv = paramStatus;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaes.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */