package com.google.android.gms.internal;

import com.google.android.gms.common.api.Status;
import org.json.JSONException;

public final class zzaeu
{
  public static final zzaet aLr = new zzaet()
  {
    public zzaew zzao(byte[] paramAnonymousArrayOfByte)
      throws zzaep
    {
      if (paramAnonymousArrayOfByte == null) {
        throw new zzaep("Cannot parse a null byte[]");
      }
      if (paramAnonymousArrayOfByte.length == 0) {
        throw new zzaep("Cannot parse a 0 length byte[]");
      }
      try
      {
        zzaff localzzaff = zzaeq.zzqr(new String(paramAnonymousArrayOfByte));
        if (localzzaff != null) {
          zzyl.v("The container was successfully parsed from the resource");
        }
        paramAnonymousArrayOfByte = zzaeu.aLs.zzao(paramAnonymousArrayOfByte);
        return new zzaew(Status.xZ, 0, new zzaew.zza(localzzaff), paramAnonymousArrayOfByte.zzcjj());
      }
      catch (JSONException paramAnonymousArrayOfByte)
      {
        throw new zzaep("The resource data is corrupted. The container cannot be extracted from the JSON data");
      }
      catch (zzaep paramAnonymousArrayOfByte)
      {
        throw new zzaep("The resource data is invalid. The container cannot be extracted from the JSON data");
      }
    }
  };
  public static final zzaet aLs = new zzaet()
  {
    public zzaew zzao(byte[] paramAnonymousArrayOfByte)
      throws zzaep
    {
      if (paramAnonymousArrayOfByte == null) {
        throw new zzaep("Cannot parse a null byte[]");
      }
      if (paramAnonymousArrayOfByte.length == 0) {
        throw new zzaep("Cannot parse a 0 length byte[]");
      }
      try
      {
        paramAnonymousArrayOfByte = zzaeq.zzqs(new String(paramAnonymousArrayOfByte));
        if (paramAnonymousArrayOfByte != null) {
          zzyl.v("The runtime configuration was successfully parsed from the resource");
        }
        return new zzaew(Status.xZ, 0, null, paramAnonymousArrayOfByte);
      }
      catch (JSONException paramAnonymousArrayOfByte)
      {
        throw new zzaep("The resource data is corrupted. The runtime configuration cannot be extracted from the JSON data");
      }
      catch (zzaep paramAnonymousArrayOfByte)
      {
        throw new zzaep("The resource data is invalid. The runtime  configuration cannot be extracted from the JSON data");
      }
    }
  };
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaeu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */