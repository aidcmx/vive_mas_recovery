package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;

public class zzaev
{
  private zzaeo aLt;
  
  public String getId()
  {
    if (this.aLt == null) {
      return "";
    }
    return this.aLt.getContainerId();
  }
  
  public zzaev zza(zzaeo paramzzaeo)
    throws IllegalArgumentException
  {
    zzaa.zzy(paramzzaeo);
    this.aLt = paramzzaeo;
    return this;
  }
  
  public zzaeo zzcjh()
  {
    return this.aLt;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaev.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */