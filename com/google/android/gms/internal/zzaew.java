package com.google.android.gms.internal;

import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;

public class zzaew
  implements Result
{
  private final zza aLu;
  private final zzafi aLv;
  private final Status hv;
  private final int zzbcm;
  
  public zzaew(Status paramStatus, int paramInt)
  {
    this(paramStatus, paramInt, null, null);
  }
  
  public zzaew(Status paramStatus, int paramInt, zza paramzza, zzafi paramzzafi)
  {
    this.hv = paramStatus;
    this.zzbcm = paramInt;
    this.aLu = paramzza;
    this.aLv = paramzzafi;
  }
  
  public int getSource()
  {
    return this.zzbcm;
  }
  
  public Status getStatus()
  {
    return this.hv;
  }
  
  public zza zzcji()
  {
    return this.aLu;
  }
  
  public zzafi zzcjj()
  {
    return this.aLv;
  }
  
  public String zzcjk()
  {
    if (this.zzbcm == 0) {
      return "Network";
    }
    if (this.zzbcm == 1) {
      return "Saved file on disk";
    }
    if (this.zzbcm == 2) {
      return "Default resource";
    }
    throw new IllegalStateException("Resource source is unknown.");
  }
  
  public static class zza
  {
    private final zzaff aJv;
    private final byte[] aLw;
    private final long aLx;
    private final zzaeo aLy;
    
    public zza(zzaeo paramzzaeo, byte[] paramArrayOfByte, zzaff paramzzaff, long paramLong)
    {
      this.aLy = paramzzaeo;
      this.aLw = paramArrayOfByte;
      this.aJv = paramzzaff;
      this.aLx = paramLong;
    }
    
    public zza(zzaff paramzzaff)
    {
      this(null, null, paramzzaff, 0L);
    }
    
    public byte[] zzcjl()
    {
      return this.aLw;
    }
    
    public zzaeo zzcjm()
    {
      return this.aLy;
    }
    
    public zzaff zzcjn()
    {
      return this.aJv;
    }
    
    public long zzcjo()
    {
      return this.aLx;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaew.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */