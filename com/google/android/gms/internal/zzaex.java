package com.google.android.gms.internal;

import android.content.Context;
import android.content.res.AssetManager;
import com.google.android.gms.common.util.zzo;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class zzaex
{
  private final ExecutorService aGI;
  private final zza aLz;
  private final Context mContext;
  
  public zzaex(Context paramContext)
  {
    this(paramContext, Executors.newSingleThreadExecutor(), new zza()
    {
      public InputStream open(String paramAnonymousString)
        throws IOException
      {
        return zzaex.this.getAssets().open(paramAnonymousString);
      }
    });
  }
  
  zzaex(Context paramContext, ExecutorService paramExecutorService, zza paramzza)
  {
    this.mContext = paramContext;
    this.aGI = paramExecutorService;
    this.aLz = paramzza;
  }
  
  private byte[] zzm(InputStream paramInputStream)
  {
    ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
    for (;;)
    {
      try
      {
        zzo.zza(paramInputStream, localByteArrayOutputStream);
      }
      catch (IOException localIOException)
      {
        zzyl.zzdi("Failed to read the resource from disk");
        try
        {
          paramInputStream.close();
        }
        catch (IOException paramInputStream)
        {
          zzyl.zzdi("Error closing stream for reading resource from disk");
          return null;
        }
      }
      finally
      {
        try
        {
          paramInputStream.close();
          throw ((Throwable)localObject);
        }
        catch (IOException paramInputStream)
        {
          zzyl.zzdi("Error closing stream for reading resource from disk");
        }
      }
      try
      {
        paramInputStream.close();
        return localByteArrayOutputStream.toByteArray();
      }
      catch (IOException paramInputStream)
      {
        zzyl.zzdi("Error closing stream for reading resource from disk");
        return null;
      }
    }
    return null;
  }
  
  private String zzqx(String paramString)
  {
    String str = String.valueOf("resource_");
    paramString = String.valueOf(paramString);
    if (paramString.length() != 0) {
      return str.concat(paramString);
    }
    return new String(str);
  }
  
  public void zza(final String paramString, final zzaer paramzzaer)
  {
    this.aGI.execute(new Runnable()
    {
      public void run()
      {
        zzaex.this.zzb(paramString, paramzzaer);
      }
    });
  }
  
  public void zza(final String paramString1, final String paramString2, final zzaer paramzzaer)
  {
    this.aGI.execute(new Runnable()
    {
      public void run()
      {
        zzaex.this.zzb(paramString1, paramString2, paramzzaer);
      }
    });
  }
  
  void zzb(String paramString, zzaer paramzzaer)
  {
    zzyl.v("Starting to load a saved resource file from Disk.");
    try
    {
      FileInputStream localFileInputStream = new FileInputStream(zzqw(paramString));
      if (localFileInputStream != null)
      {
        paramzzaer.zzam(zzm(localFileInputStream));
        return;
      }
      paramzzaer.zzw(0, 1);
      return;
    }
    catch (FileNotFoundException localFileNotFoundException)
    {
      paramString = String.valueOf(zzqx(paramString));
      if (paramString.length() == 0) {}
    }
    for (paramString = "Saved resource not found: ".concat(paramString);; paramString = new String("Saved resource not found: "))
    {
      zzyl.e(paramString);
      paramzzaer.zzw(0, 1);
      return;
    }
  }
  
  void zzb(String paramString1, String paramString2, zzaer paramzzaer)
  {
    zzyl.v("Starting to load a default asset file from Disk.");
    if (paramString2 == null)
    {
      zzyl.v("Default asset file is not specified. Not proceeding with the loading");
      paramzzaer.zzw(0, 2);
      return;
    }
    try
    {
      InputStream localInputStream = this.aLz.open(paramString2);
      if (localInputStream != null)
      {
        paramzzaer.zzam(zzm(localInputStream));
        return;
      }
    }
    catch (IOException localIOException)
    {
      zzyl.e(String.valueOf(paramString1).length() + 42 + String.valueOf(paramString2).length() + "Default asset file not found. " + paramString1 + ". Filename: " + paramString2);
      paramzzaer.zzw(0, 2);
      return;
    }
    paramzzaer.zzw(0, 2);
  }
  
  public void zzg(final String paramString, final byte[] paramArrayOfByte)
  {
    this.aGI.execute(new Runnable()
    {
      public void run()
      {
        zzaex.this.zzh(paramString, paramArrayOfByte);
      }
    });
  }
  
  /* Error */
  void zzh(String paramString, byte[] paramArrayOfByte)
  {
    // Byte code:
    //   0: aload_0
    //   1: aload_1
    //   2: invokevirtual 122	com/google/android/gms/internal/zzaex:zzqw	(Ljava/lang/String;)Ljava/io/File;
    //   5: astore 4
    //   7: new 177	java/io/FileOutputStream
    //   10: dup
    //   11: aload 4
    //   13: invokespecial 178	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   16: astore_3
    //   17: aload_3
    //   18: aload_2
    //   19: invokevirtual 181	java/io/FileOutputStream:write	([B)V
    //   22: aload_3
    //   23: invokevirtual 182	java/io/FileOutputStream:close	()V
    //   26: new 154	java/lang/StringBuilder
    //   29: dup
    //   30: aload_1
    //   31: invokestatic 85	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   34: invokevirtual 89	java/lang/String:length	()I
    //   37: bipush 24
    //   39: iadd
    //   40: invokespecial 157	java/lang/StringBuilder:<init>	(I)V
    //   43: ldc -72
    //   45: invokevirtual 163	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   48: aload_1
    //   49: invokevirtual 163	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   52: ldc -70
    //   54: invokevirtual 163	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   57: invokevirtual 169	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   60: invokestatic 116	com/google/android/gms/internal/zzyl:v	(Ljava/lang/String;)V
    //   63: return
    //   64: astore_1
    //   65: ldc -68
    //   67: invokestatic 144	com/google/android/gms/internal/zzyl:e	(Ljava/lang/String;)V
    //   70: return
    //   71: astore_1
    //   72: ldc -66
    //   74: invokestatic 144	com/google/android/gms/internal/zzyl:e	(Ljava/lang/String;)V
    //   77: return
    //   78: astore_2
    //   79: ldc -64
    //   81: invokestatic 144	com/google/android/gms/internal/zzyl:e	(Ljava/lang/String;)V
    //   84: aload 4
    //   86: invokevirtual 198	java/io/File:delete	()Z
    //   89: pop
    //   90: aload_3
    //   91: invokevirtual 182	java/io/FileOutputStream:close	()V
    //   94: new 154	java/lang/StringBuilder
    //   97: dup
    //   98: aload_1
    //   99: invokestatic 85	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   102: invokevirtual 89	java/lang/String:length	()I
    //   105: bipush 24
    //   107: iadd
    //   108: invokespecial 157	java/lang/StringBuilder:<init>	(I)V
    //   111: ldc -72
    //   113: invokevirtual 163	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   116: aload_1
    //   117: invokevirtual 163	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   120: ldc -70
    //   122: invokevirtual 163	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   125: invokevirtual 169	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   128: invokestatic 116	com/google/android/gms/internal/zzyl:v	(Ljava/lang/String;)V
    //   131: return
    //   132: astore_1
    //   133: ldc -66
    //   135: invokestatic 144	com/google/android/gms/internal/zzyl:e	(Ljava/lang/String;)V
    //   138: return
    //   139: astore_2
    //   140: aload_3
    //   141: invokevirtual 182	java/io/FileOutputStream:close	()V
    //   144: new 154	java/lang/StringBuilder
    //   147: dup
    //   148: aload_1
    //   149: invokestatic 85	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   152: invokevirtual 89	java/lang/String:length	()I
    //   155: bipush 24
    //   157: iadd
    //   158: invokespecial 157	java/lang/StringBuilder:<init>	(I)V
    //   161: ldc -72
    //   163: invokevirtual 163	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   166: aload_1
    //   167: invokevirtual 163	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   170: ldc -70
    //   172: invokevirtual 163	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   175: invokevirtual 169	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   178: invokestatic 116	com/google/android/gms/internal/zzyl:v	(Ljava/lang/String;)V
    //   181: aload_2
    //   182: athrow
    //   183: astore_1
    //   184: ldc -66
    //   186: invokestatic 144	com/google/android/gms/internal/zzyl:e	(Ljava/lang/String;)V
    //   189: goto -8 -> 181
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	192	0	this	zzaex
    //   0	192	1	paramString	String
    //   0	192	2	paramArrayOfByte	byte[]
    //   16	125	3	localFileOutputStream	java.io.FileOutputStream
    //   5	80	4	localFile	File
    // Exception table:
    //   from	to	target	type
    //   7	17	64	java/io/FileNotFoundException
    //   22	63	71	java/io/IOException
    //   17	22	78	java/io/IOException
    //   90	131	132	java/io/IOException
    //   17	22	139	finally
    //   79	90	139	finally
    //   140	181	183	java/io/IOException
  }
  
  public long zzqv(String paramString)
  {
    paramString = zzqw(paramString);
    if (paramString.exists()) {
      return paramString.lastModified();
    }
    return 0L;
  }
  
  File zzqw(String paramString)
  {
    return new File(this.mContext.getDir("google_tagmanager", 0), zzqx(paramString));
  }
  
  static abstract interface zza
  {
    public abstract InputStream open(String paramString)
      throws IOException;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaex.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */