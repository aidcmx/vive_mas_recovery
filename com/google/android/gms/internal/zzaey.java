package com.google.android.gms.internal;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class zzaey
{
  private String aEC = "https://www.google-analytics.com";
  
  private String zzqe(String paramString)
  {
    try
    {
      String str = URLEncoder.encode(paramString, "UTF-8").replaceAll("\\+", "%20");
      return str;
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      paramString = String.valueOf(paramString);
      if (paramString.length() == 0) {}
    }
    for (paramString = "Cannot encode the string: ".concat(paramString);; paramString = new String("Cannot encode the string: "))
    {
      zzyl.e(paramString);
      return "";
    }
  }
  
  public String zzb(zzaeo paramzzaeo)
  {
    String str1 = this.aEC;
    String str2 = String.valueOf("/gtm/android?");
    if (paramzzaeo.zzcjd()) {}
    for (paramzzaeo = paramzzaeo.zzcje();; paramzzaeo = zzc(paramzzaeo)) {
      return String.valueOf(str1).length() + 0 + String.valueOf(str2).length() + String.valueOf(paramzzaeo).length() + str1 + str2 + paramzzaeo;
    }
  }
  
  String zzc(zzaeo paramzzaeo)
  {
    if (paramzzaeo == null) {
      return "";
    }
    String str;
    StringBuilder localStringBuilder;
    if (!paramzzaeo.zzcjf().trim().equals(""))
    {
      str = paramzzaeo.zzcjf().trim();
      localStringBuilder = new StringBuilder();
      if (paramzzaeo.zzcjb() == null) {
        break label130;
      }
      localStringBuilder.append(paramzzaeo.zzcjb());
    }
    for (;;)
    {
      localStringBuilder.append("=").append(zzqe(paramzzaeo.getContainerId())).append("&").append("pv").append("=").append(zzqe(str)).append("&").append("rv=5.0");
      if (paramzzaeo.zzcjd()) {
        localStringBuilder.append("&gtm_debug=x");
      }
      return localStringBuilder.toString();
      str = "-1";
      break;
      label130:
      localStringBuilder.append("id");
    }
  }
  
  public void zzqy(String paramString)
  {
    this.aEC = paramString;
    paramString = String.valueOf(paramString);
    if (paramString.length() != 0) {}
    for (paramString = "The Ctfe server endpoint was changed to: ".concat(paramString);; paramString = new String("The Ctfe server endpoint was changed to: "))
    {
      zzyl.zzdh(paramString);
      return;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaey.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */