package com.google.android.gms.internal;

import java.io.IOException;
import java.io.InputStream;

public abstract interface zzafa
{
  public abstract void close();
  
  public abstract InputStream zzqz(String paramString)
    throws IOException;
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzafa.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */