package com.google.android.gms.internal;

import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.google.android.gms.common.internal.zzaa;

public class zzafc
  implements Runnable
{
  private final zzaer aLG;
  private final zzafb aLH;
  private final zzaey aLI;
  private final zzaev aLh;
  private final Context mContext;
  
  public zzafc(Context paramContext, zzaev paramzzaev, zzaer paramzzaer)
  {
    this(paramContext, paramzzaev, paramzzaer, new zzafb(), new zzaey());
  }
  
  zzafc(Context paramContext, zzaev paramzzaev, zzaer paramzzaer, zzafb paramzzafb, zzaey paramzzaey)
  {
    this.mContext = ((Context)zzaa.zzy(paramContext));
    this.aLG = ((zzaer)zzaa.zzy(paramzzaer));
    this.aLh = paramzzaev;
    this.aLH = paramzzafb;
    this.aLI = paramzzaey;
  }
  
  public zzafc(Context paramContext, zzaev paramzzaev, zzaer paramzzaer, String paramString)
  {
    this(paramContext, paramzzaev, paramzzaer, new zzafb(), new zzaey());
    this.aLI.zzqy(paramString);
  }
  
  public void run()
  {
    zznt();
  }
  
  boolean zzcjq()
  {
    if (!zzez("android.permission.INTERNET"))
    {
      zzyl.e("Missing android.permission.INTERNET. Please add the following declaration to your AndroidManifest.xml: <uses-permission android:name=\"android.permission.INTERNET\" />");
      return false;
    }
    if (!zzez("android.permission.ACCESS_NETWORK_STATE"))
    {
      zzyl.e("Missing android.permission.ACCESS_NETWORK_STATE. Please add the following declaration to your AndroidManifest.xml: <uses-permission android:name=\"android.permission.ACCESS_NETWORK_STATE\" />");
      return false;
    }
    NetworkInfo localNetworkInfo = ((ConnectivityManager)this.mContext.getSystemService("connectivity")).getActiveNetworkInfo();
    if ((localNetworkInfo == null) || (!localNetworkInfo.isConnected()))
    {
      zzyl.zzdi("No network connectivity - Offline");
      return false;
    }
    return true;
  }
  
  boolean zzez(String paramString)
  {
    return this.mContext.getPackageManager().checkPermission(paramString, this.mContext.getPackageName()) == 0;
  }
  
  /* Error */
  void zznt()
  {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual 123	com/google/android/gms/internal/zzafc:zzcjq	()Z
    //   4: ifne +13 -> 17
    //   7: aload_0
    //   8: getfield 45	com/google/android/gms/internal/zzafc:aLG	Lcom/google/android/gms/internal/zzaer;
    //   11: iconst_0
    //   12: iconst_0
    //   13: invokevirtual 127	com/google/android/gms/internal/zzaer:zzw	(II)V
    //   16: return
    //   17: ldc -127
    //   19: invokestatic 132	com/google/android/gms/internal/zzyl:v	(Ljava/lang/String;)V
    //   22: aload_0
    //   23: getfield 49	com/google/android/gms/internal/zzafc:aLH	Lcom/google/android/gms/internal/zzafb;
    //   26: invokevirtual 136	com/google/android/gms/internal/zzafb:zzcjp	()Lcom/google/android/gms/internal/zzafa;
    //   29: astore_3
    //   30: aconst_null
    //   31: astore_2
    //   32: aload_0
    //   33: getfield 51	com/google/android/gms/internal/zzafc:aLI	Lcom/google/android/gms/internal/zzaey;
    //   36: aload_0
    //   37: getfield 47	com/google/android/gms/internal/zzafc:aLh	Lcom/google/android/gms/internal/zzaev;
    //   40: invokevirtual 142	com/google/android/gms/internal/zzaev:zzcjh	()Lcom/google/android/gms/internal/zzaeo;
    //   43: invokevirtual 146	com/google/android/gms/internal/zzaey:zzb	(Lcom/google/android/gms/internal/zzaeo;)Ljava/lang/String;
    //   46: astore 4
    //   48: aload 4
    //   50: invokestatic 152	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   53: astore_1
    //   54: aload_1
    //   55: invokevirtual 156	java/lang/String:length	()I
    //   58: ifeq +55 -> 113
    //   61: ldc -98
    //   63: aload_1
    //   64: invokevirtual 162	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   67: astore_1
    //   68: aload_1
    //   69: invokestatic 132	com/google/android/gms/internal/zzyl:v	(Ljava/lang/String;)V
    //   72: aload_3
    //   73: aload 4
    //   75: invokeinterface 168 2 0
    //   80: astore_1
    //   81: new 170	java/io/ByteArrayOutputStream
    //   84: dup
    //   85: invokespecial 171	java/io/ByteArrayOutputStream:<init>	()V
    //   88: astore_2
    //   89: aload_1
    //   90: aload_2
    //   91: invokestatic 177	com/google/android/gms/common/util/zzo:zza	(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    //   94: pop2
    //   95: aload_0
    //   96: getfield 45	com/google/android/gms/internal/zzafc:aLG	Lcom/google/android/gms/internal/zzaer;
    //   99: aload_2
    //   100: invokevirtual 181	java/io/ByteArrayOutputStream:toByteArray	()[B
    //   103: invokevirtual 185	com/google/android/gms/internal/zzaer:zzam	([B)V
    //   106: aload_3
    //   107: invokeinterface 188 1 0
    //   112: return
    //   113: new 148	java/lang/String
    //   116: dup
    //   117: ldc -98
    //   119: invokespecial 190	java/lang/String:<init>	(Ljava/lang/String;)V
    //   122: astore_1
    //   123: goto -55 -> 68
    //   126: astore_1
    //   127: aload_3
    //   128: invokeinterface 188 1 0
    //   133: aload_1
    //   134: athrow
    //   135: astore_1
    //   136: aload 4
    //   138: invokestatic 152	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   141: astore_1
    //   142: aload_1
    //   143: invokevirtual 156	java/lang/String:length	()I
    //   146: ifeq +30 -> 176
    //   149: ldc -64
    //   151: aload_1
    //   152: invokevirtual 162	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   155: astore_1
    //   156: aload_1
    //   157: invokestatic 75	com/google/android/gms/internal/zzyl:e	(Ljava/lang/String;)V
    //   160: aload_0
    //   161: getfield 45	com/google/android/gms/internal/zzafc:aLG	Lcom/google/android/gms/internal/zzaer;
    //   164: iconst_2
    //   165: iconst_0
    //   166: invokevirtual 127	com/google/android/gms/internal/zzaer:zzw	(II)V
    //   169: aload_3
    //   170: invokeinterface 188 1 0
    //   175: return
    //   176: new 148	java/lang/String
    //   179: dup
    //   180: ldc -64
    //   182: invokespecial 190	java/lang/String:<init>	(Ljava/lang/String;)V
    //   185: astore_1
    //   186: goto -30 -> 156
    //   189: astore_1
    //   190: aload 4
    //   192: invokestatic 152	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   195: astore_1
    //   196: aload_1
    //   197: invokevirtual 156	java/lang/String:length	()I
    //   200: ifeq +28 -> 228
    //   203: ldc -62
    //   205: aload_1
    //   206: invokevirtual 162	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   209: astore_1
    //   210: aload_1
    //   211: invokestatic 75	com/google/android/gms/internal/zzyl:e	(Ljava/lang/String;)V
    //   214: aload_0
    //   215: getfield 45	com/google/android/gms/internal/zzafc:aLG	Lcom/google/android/gms/internal/zzaer;
    //   218: iconst_3
    //   219: iconst_0
    //   220: invokevirtual 127	com/google/android/gms/internal/zzaer:zzw	(II)V
    //   223: aload_2
    //   224: astore_1
    //   225: goto -144 -> 81
    //   228: new 148	java/lang/String
    //   231: dup
    //   232: ldc -62
    //   234: invokespecial 190	java/lang/String:<init>	(Ljava/lang/String;)V
    //   237: astore_1
    //   238: goto -28 -> 210
    //   241: astore_1
    //   242: aload_1
    //   243: invokevirtual 197	java/io/IOException:getMessage	()Ljava/lang/String;
    //   246: invokestatic 152	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   249: astore_2
    //   250: new 199	java/lang/StringBuilder
    //   253: dup
    //   254: aload 4
    //   256: invokestatic 152	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   259: invokevirtual 156	java/lang/String:length	()I
    //   262: bipush 54
    //   264: iadd
    //   265: aload_2
    //   266: invokestatic 152	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   269: invokevirtual 156	java/lang/String:length	()I
    //   272: iadd
    //   273: invokespecial 202	java/lang/StringBuilder:<init>	(I)V
    //   276: ldc -52
    //   278: invokevirtual 208	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   281: aload 4
    //   283: invokevirtual 208	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   286: ldc -46
    //   288: invokevirtual 208	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   291: aload_2
    //   292: invokevirtual 208	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   295: invokevirtual 213	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   298: aload_1
    //   299: invokestatic 216	com/google/android/gms/internal/zzyl:zzb	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   302: aload_0
    //   303: getfield 45	com/google/android/gms/internal/zzafc:aLG	Lcom/google/android/gms/internal/zzaer;
    //   306: iconst_1
    //   307: iconst_0
    //   308: invokevirtual 127	com/google/android/gms/internal/zzaer:zzw	(II)V
    //   311: aload_3
    //   312: invokeinterface 188 1 0
    //   317: return
    //   318: astore_1
    //   319: aload_1
    //   320: invokevirtual 197	java/io/IOException:getMessage	()Ljava/lang/String;
    //   323: invokestatic 152	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   326: astore_2
    //   327: new 199	java/lang/StringBuilder
    //   330: dup
    //   331: aload 4
    //   333: invokestatic 152	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   336: invokevirtual 156	java/lang/String:length	()I
    //   339: bipush 66
    //   341: iadd
    //   342: aload_2
    //   343: invokestatic 152	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   346: invokevirtual 156	java/lang/String:length	()I
    //   349: iadd
    //   350: invokespecial 202	java/lang/StringBuilder:<init>	(I)V
    //   353: ldc -38
    //   355: invokevirtual 208	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   358: aload 4
    //   360: invokevirtual 208	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   363: ldc -46
    //   365: invokevirtual 208	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   368: aload_2
    //   369: invokevirtual 208	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   372: invokevirtual 213	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   375: aload_1
    //   376: invokestatic 216	com/google/android/gms/internal/zzyl:zzb	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   379: aload_0
    //   380: getfield 45	com/google/android/gms/internal/zzafc:aLG	Lcom/google/android/gms/internal/zzaer;
    //   383: iconst_2
    //   384: iconst_0
    //   385: invokevirtual 127	com/google/android/gms/internal/zzaer:zzw	(II)V
    //   388: aload_3
    //   389: invokeinterface 188 1 0
    //   394: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	395	0	this	zzafc
    //   53	70	1	localObject1	Object
    //   126	8	1	localObject2	Object
    //   135	1	1	localFileNotFoundException	java.io.FileNotFoundException
    //   141	45	1	str1	String
    //   189	1	1	localzzafe	zzafe
    //   195	43	1	localObject3	Object
    //   241	58	1	localIOException1	java.io.IOException
    //   318	58	1	localIOException2	java.io.IOException
    //   31	338	2	localObject4	Object
    //   29	360	3	localzzafa	zzafa
    //   46	313	4	str2	String
    // Exception table:
    //   from	to	target	type
    //   32	68	126	finally
    //   68	72	126	finally
    //   72	81	126	finally
    //   81	106	126	finally
    //   113	123	126	finally
    //   136	156	126	finally
    //   156	169	126	finally
    //   176	186	126	finally
    //   190	210	126	finally
    //   210	223	126	finally
    //   228	238	126	finally
    //   242	311	126	finally
    //   319	388	126	finally
    //   72	81	135	java/io/FileNotFoundException
    //   72	81	189	com/google/android/gms/internal/zzafe
    //   72	81	241	java/io/IOException
    //   81	106	318	java/io/IOException
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzafc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */