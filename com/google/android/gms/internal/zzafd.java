package com.google.android.gms.internal;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

public class zzafd
{
  private String aEC = null;
  private final ScheduledExecutorService aGE;
  private ScheduledFuture<?> aGG = null;
  private boolean mClosed;
  
  public zzafd()
  {
    this(Executors.newSingleThreadScheduledExecutor());
  }
  
  public zzafd(String paramString)
  {
    this(Executors.newSingleThreadScheduledExecutor());
    this.aEC = paramString;
  }
  
  zzafd(ScheduledExecutorService paramScheduledExecutorService)
  {
    this.aGE = paramScheduledExecutorService;
    this.mClosed = false;
  }
  
  /* Error */
  public void zza(android.content.Context paramContext, zzaev paramzzaev, long paramLong, zzaer paramzzaer)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 32	com/google/android/gms/internal/zzafd:aGG	Ljava/util/concurrent/ScheduledFuture;
    //   6: ifnull +14 -> 20
    //   9: aload_0
    //   10: getfield 32	com/google/android/gms/internal/zzafd:aGG	Ljava/util/concurrent/ScheduledFuture;
    //   13: iconst_0
    //   14: invokeinterface 44 2 0
    //   19: pop
    //   20: aload_0
    //   21: getfield 28	com/google/android/gms/internal/zzafd:aEC	Ljava/lang/String;
    //   24: ifnull +40 -> 64
    //   27: new 46	com/google/android/gms/internal/zzafc
    //   30: dup
    //   31: aload_1
    //   32: aload_2
    //   33: aload 5
    //   35: aload_0
    //   36: getfield 28	com/google/android/gms/internal/zzafd:aEC	Ljava/lang/String;
    //   39: invokespecial 49	com/google/android/gms/internal/zzafc:<init>	(Landroid/content/Context;Lcom/google/android/gms/internal/zzaev;Lcom/google/android/gms/internal/zzaer;Ljava/lang/String;)V
    //   42: astore_1
    //   43: aload_0
    //   44: aload_0
    //   45: getfield 34	com/google/android/gms/internal/zzafd:aGE	Ljava/util/concurrent/ScheduledExecutorService;
    //   48: aload_1
    //   49: lload_3
    //   50: getstatic 55	java/util/concurrent/TimeUnit:MILLISECONDS	Ljava/util/concurrent/TimeUnit;
    //   53: invokeinterface 61 5 0
    //   58: putfield 32	com/google/android/gms/internal/zzafd:aGG	Ljava/util/concurrent/ScheduledFuture;
    //   61: aload_0
    //   62: monitorexit
    //   63: return
    //   64: new 46	com/google/android/gms/internal/zzafc
    //   67: dup
    //   68: aload_1
    //   69: aload_2
    //   70: aload 5
    //   72: invokespecial 64	com/google/android/gms/internal/zzafc:<init>	(Landroid/content/Context;Lcom/google/android/gms/internal/zzaev;Lcom/google/android/gms/internal/zzaer;)V
    //   75: astore_1
    //   76: goto -33 -> 43
    //   79: astore_1
    //   80: aload_0
    //   81: monitorexit
    //   82: aload_1
    //   83: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	84	0	this	zzafd
    //   0	84	1	paramContext	android.content.Context
    //   0	84	2	paramzzaev	zzaev
    //   0	84	3	paramLong	long
    //   0	84	5	paramzzaer	zzaer
    // Exception table:
    //   from	to	target	type
    //   2	20	79	finally
    //   20	43	79	finally
    //   43	63	79	finally
    //   64	76	79	finally
    //   80	82	79	finally
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzafd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */