package com.google.android.gms.internal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class zzaff
{
  private final List<zzafh> aLJ;
  private final Map<String, zzafg> aLK;
  private int aLL;
  private String um;
  
  public zzaff(List<zzafh> paramList, Map<String, zzafg> paramMap, String paramString, int paramInt)
  {
    this.aLJ = Collections.unmodifiableList(paramList);
    this.aLK = Collections.unmodifiableMap(paramMap);
    this.um = paramString;
    this.aLL = paramInt;
  }
  
  public String getVersion()
  {
    return this.um;
  }
  
  public String toString()
  {
    String str1 = String.valueOf(zzcjr());
    String str2 = String.valueOf(this.aLK);
    return String.valueOf(str1).length() + 18 + String.valueOf(str2).length() + "Rules: " + str1 + "\n  Macros: " + str2;
  }
  
  public List<zzafh> zzcjr()
  {
    return this.aLJ;
  }
  
  public zzafg zzrb(String paramString)
  {
    return (zzafg)this.aLK.get(paramString);
  }
  
  public static class zza
  {
    private final List<zzafh> aLJ = new ArrayList();
    private final Map<String, zzafg> aLK = new HashMap();
    private int aLL = 0;
    private String um = "";
    
    public zza zza(zzafh paramzzafh)
    {
      this.aLJ.add(paramzzafh);
      return this;
    }
    
    public zza zzc(zzafg paramzzafg)
    {
      String str = ((zzafj)paramzzafg.zzcjt().get("instance_name")).toString();
      this.aLK.put(str, paramzzafg);
      return this;
    }
    
    public zzaff zzcjs()
    {
      return new zzaff(this.aLJ, this.aLK, this.um, 0);
    }
    
    public zza zzrc(String paramString)
    {
      this.um = paramString;
      return this;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaff.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */