package com.google.android.gms.internal;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public final class zzafg
{
  private final Map<String, zzafj> aLM;
  private final zzafj aLN;
  
  private zzafg(Map<String, zzafj> paramMap, zzafj paramzzafj)
  {
    this.aLM = Collections.unmodifiableMap(paramMap);
    this.aLN = paramzzafj;
  }
  
  public String toString()
  {
    String str1 = String.valueOf(zzcjt());
    String str2 = String.valueOf(this.aLN);
    return String.valueOf(str1).length() + 32 + String.valueOf(str2).length() + "Properties: " + str1 + " pushAfterEvaluate: " + str2;
  }
  
  public Map<String, zzafj> zzcjt()
  {
    return this.aLM;
  }
  
  public static class zza
  {
    private final Map<String, zzafj> aLM = new HashMap();
    private zzafj aLN;
    
    public zza zza(String paramString, zzafj paramzzafj)
    {
      this.aLM.put(paramString, paramzzafj);
      return this;
    }
    
    public zza zzb(zzafj paramzzafj)
    {
      this.aLN = paramzzafj;
      return this;
    }
    
    public zzafg zzcju()
    {
      return new zzafg(this.aLM, this.aLN, null);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzafg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */