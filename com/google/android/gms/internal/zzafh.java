package com.google.android.gms.internal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class zzafh
{
  private final List<zzafg> aLO;
  private final List<zzafg> aLP;
  private final List<zzafg> aLQ;
  private final List<zzafg> aLR;
  
  private zzafh(List<zzafg> paramList1, List<zzafg> paramList2, List<zzafg> paramList3, List<zzafg> paramList4)
  {
    this.aLO = Collections.unmodifiableList(paramList1);
    this.aLP = Collections.unmodifiableList(paramList2);
    this.aLQ = Collections.unmodifiableList(paramList3);
    this.aLR = Collections.unmodifiableList(paramList4);
  }
  
  public String toString()
  {
    String str1 = String.valueOf(zzcjv());
    String str2 = String.valueOf(zzcjw());
    String str3 = String.valueOf(zzcjx());
    String str4 = String.valueOf(zzcjy());
    return String.valueOf(str1).length() + 71 + String.valueOf(str2).length() + String.valueOf(str3).length() + String.valueOf(str4).length() + "Positive predicates: " + str1 + "  Negative predicates: " + str2 + "  Add tags: " + str3 + "  Remove tags: " + str4;
  }
  
  public List<zzafg> zzcjv()
  {
    return this.aLO;
  }
  
  public List<zzafg> zzcjw()
  {
    return this.aLP;
  }
  
  public List<zzafg> zzcjx()
  {
    return this.aLQ;
  }
  
  public List<zzafg> zzcjy()
  {
    return this.aLR;
  }
  
  public static class zza
  {
    private final List<zzafg> aLO = new ArrayList();
    private final List<zzafg> aLP = new ArrayList();
    private final List<zzafg> aLQ = new ArrayList();
    private final List<zzafg> aLR = new ArrayList();
    
    public zzafh zzcjz()
    {
      return new zzafh(this.aLO, this.aLP, this.aLQ, this.aLR, null);
    }
    
    public zza zzd(zzafg paramzzafg)
    {
      this.aLO.add(paramzzafg);
      return this;
    }
    
    public zza zze(zzafg paramzzafg)
    {
      this.aLP.add(paramzzafg);
      return this;
    }
    
    public zza zzf(zzafg paramzzafg)
    {
      this.aLQ.add(paramzzafg);
      return this;
    }
    
    public zza zzg(zzafg paramzzafg)
    {
      this.aLR.add(paramzzafg);
      return this;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzafh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */