package com.google.android.gms.internal;

import java.util.ArrayList;
import java.util.List;

public class zzafi
{
  private final List<zzzg> aLS;
  private final String um;
  
  private zzafi(String paramString, List<zzzg> paramList)
  {
    this.um = paramString;
    this.aLS = paramList;
  }
  
  public List<zzzg> zzcka()
  {
    return this.aLS;
  }
  
  public static class zza
  {
    private List<zzzg> aLS = new ArrayList();
    private String um;
    
    public zza zza(zzzg paramzzzg)
    {
      this.aLS.add(paramzzzg);
      return this;
    }
    
    public zzafi zzckb()
    {
      return new zzafi(this.um, this.aLS, null);
    }
    
    public zza zzrd(String paramString)
    {
      this.um = paramString;
      return this;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzafi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */