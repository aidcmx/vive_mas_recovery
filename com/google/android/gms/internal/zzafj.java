package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class zzafj
{
  private static final String aHV = new String("");
  private static final Integer aLT = Integer.valueOf(0);
  private final List<Integer> aLU;
  private final boolean aLV;
  private final int nV;
  private final Object zzcyd;
  
  private zzafj(Integer paramInteger, Object paramObject, List<Integer> paramList, boolean paramBoolean)
  {
    this.nV = paramInteger.intValue();
    this.zzcyd = paramObject;
    this.aLU = Collections.unmodifiableList(paramList);
    this.aLV = paramBoolean;
  }
  
  public boolean equals(Object paramObject)
  {
    return ((paramObject instanceof zzafj)) && (((zzafj)paramObject).getValue().equals(this.zzcyd));
  }
  
  public int getType()
  {
    return this.nV;
  }
  
  public Object getValue()
  {
    return this.zzcyd;
  }
  
  public int hashCode()
  {
    return this.zzcyd.hashCode();
  }
  
  public String toString()
  {
    if (this.zzcyd == null)
    {
      zzyl.e("Fail to convert a null object to string");
      return aHV;
    }
    return this.zzcyd.toString();
  }
  
  public List<Integer> zzckc()
  {
    return this.aLU;
  }
  
  public static class zza
  {
    private final List<Integer> aLU = new ArrayList();
    private final Integer aLW;
    private boolean aLX = false;
    private final Object zzcyd;
    
    public zza(int paramInt, Object paramObject)
    {
      this.aLW = Integer.valueOf(paramInt);
      this.zzcyd = paramObject;
    }
    
    public zza zzaak(int paramInt)
    {
      this.aLU.add(Integer.valueOf(paramInt));
      return this;
    }
    
    public zzafj zzckd()
    {
      zzaa.zzy(this.aLW);
      zzaa.zzy(this.zzcyd);
      return new zzafj(this.aLW, this.zzcyd, this.aLU, this.aLX, null);
    }
    
    public zza zzcq(boolean paramBoolean)
    {
      this.aLX = paramBoolean;
      return this;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzafj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */