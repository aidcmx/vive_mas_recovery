package com.google.android.gms.internal;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

public abstract class zzafk<T>
{
  protected Map<String, zzafk<?>> aLY;
  
  public abstract String toString();
  
  public void zzc(String paramString, zzafk<?> paramzzafk)
  {
    if (this.aLY == null) {
      this.aLY = new HashMap();
    }
    this.aLY.put(paramString, paramzzafk);
  }
  
  public Iterator<zzafk<?>> zzcke()
  {
    return new zza(null);
  }
  
  public abstract T zzckf();
  
  protected Iterator<zzafk<?>> zzckg()
  {
    if (this.aLY == null) {
      return new zza(null);
    }
    new Iterator()
    {
      public boolean hasNext()
      {
        return this.aLZ.hasNext();
      }
      
      public void remove()
      {
        this.aLZ.remove();
      }
      
      public zzafk<?> zzckh()
      {
        return new zzafs((String)this.aLZ.next());
      }
    };
  }
  
  public boolean zzre(String paramString)
  {
    return (this.aLY != null) && (this.aLY.containsKey(paramString));
  }
  
  public zzafk<?> zzrf(String paramString)
  {
    if (this.aLY != null) {
      return (zzafk)this.aLY.get(paramString);
    }
    return zzafo.aMi;
  }
  
  public boolean zzrg(String paramString)
  {
    return false;
  }
  
  public zzzh zzrh(String paramString)
  {
    throw new IllegalStateException(String.valueOf(paramString).length() + 56 + "Attempting to access Native Method " + paramString + " on unsupported type.");
  }
  
  private static class zza
    implements Iterator<zzafk<?>>
  {
    public boolean hasNext()
    {
      return false;
    }
    
    public void remove()
    {
      throw new UnsupportedOperationException();
    }
    
    public zzafk<?> zzckh()
    {
      throw new NoSuchElementException();
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzafk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */