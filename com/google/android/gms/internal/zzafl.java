package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public final class zzafl
  extends zzafk<Boolean>
{
  private static final Map<String, zzzh> aMc;
  private final Boolean aMb;
  
  static
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put("hasOwnProperty", zzabf.aKK);
    localHashMap.put("toString", new zzach());
    aMc = Collections.unmodifiableMap(localHashMap);
  }
  
  public zzafl(Boolean paramBoolean)
  {
    zzaa.zzy(paramBoolean);
    this.aMb = paramBoolean;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if ((paramObject instanceof zzafl))
    {
      if ((Boolean)((zzafl)paramObject).zzckf() == this.aMb) {}
      for (boolean bool = true;; bool = false) {
        return bool;
      }
    }
    return false;
  }
  
  public String toString()
  {
    return this.aMb.toString();
  }
  
  public Boolean zzcki()
  {
    return this.aMb;
  }
  
  public boolean zzrg(String paramString)
  {
    return aMc.containsKey(paramString);
  }
  
  public zzzh zzrh(String paramString)
  {
    if (zzrg(paramString)) {
      return (zzzh)aMc.get(paramString);
    }
    throw new IllegalStateException(String.valueOf(paramString).length() + 54 + "Native Method " + paramString + " is not defined for type BooleanWrapper.");
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzafl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */