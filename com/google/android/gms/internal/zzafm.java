package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public final class zzafm
  extends zzafk<Double>
{
  private static final Map<String, zzzh> aMc;
  private Double aMd;
  
  static
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put("hasOwnProperty", zzabf.aKK);
    localHashMap.put("toString", new zzach());
    aMc = Collections.unmodifiableMap(localHashMap);
  }
  
  public zzafm(Double paramDouble)
  {
    zzaa.zzy(paramDouble);
    this.aMd = paramDouble;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if ((paramObject instanceof zzafm)) {
      return this.aMd.equals((Double)((zzafm)paramObject).zzckf());
    }
    return false;
  }
  
  public String toString()
  {
    return this.aMd.toString();
  }
  
  public Double zzckj()
  {
    return this.aMd;
  }
  
  public boolean zzrg(String paramString)
  {
    return aMc.containsKey(paramString);
  }
  
  public zzzh zzrh(String paramString)
  {
    if (zzrg(paramString)) {
      return (zzzh)aMc.get(paramString);
    }
    throw new IllegalStateException(String.valueOf(paramString).length() + 53 + "Native Method " + paramString + " is not defined for type DoubleWrapper.");
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzafm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */