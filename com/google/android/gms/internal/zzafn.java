package com.google.android.gms.internal;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class zzafn
  extends zzafk<zzzh>
{
  private static final Map<String, zzzh> aMc;
  private zzzh aMe;
  
  static
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put("hasOwnProperty", zzabf.aKK);
    aMc = Collections.unmodifiableMap(localHashMap);
  }
  
  public zzafn(zzzh paramzzzh)
  {
    this.aMe = paramzzzh;
  }
  
  public String toString()
  {
    return this.aMe.toString();
  }
  
  public Iterator<zzafk<?>> zzcke()
  {
    return zzckg();
  }
  
  public zzzh zzckk()
  {
    return this.aMe;
  }
  
  public boolean zzrg(String paramString)
  {
    return aMc.containsKey(paramString);
  }
  
  public zzzh zzrh(String paramString)
  {
    if (zzrg(paramString)) {
      return (zzzh)aMc.get(paramString);
    }
    throw new IllegalStateException(String.valueOf(paramString).length() + 60 + "Native Method " + paramString + " is not defined for type InstructionReference.");
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzafn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */