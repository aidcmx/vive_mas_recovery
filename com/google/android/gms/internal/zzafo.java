package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;

public final class zzafo
  extends zzafk<zzafk<?>>
{
  public static final zzafo aMf = new zzafo("BREAK");
  public static final zzafo aMg = new zzafo("CONTINUE");
  public static final zzafo aMh = new zzafo("NULL");
  public static final zzafo aMi = new zzafo("UNDEFINED");
  private final boolean aMj;
  private final zzafk<?> aMk;
  private final String mName;
  
  public zzafo(zzafk<?> paramzzafk)
  {
    zzaa.zzy(paramzzafk);
    this.mName = "RETURN";
    this.aMj = true;
    this.aMk = paramzzafk;
  }
  
  private zzafo(String paramString)
  {
    this.mName = paramString;
    this.aMj = false;
    this.aMk = null;
  }
  
  public String toString()
  {
    return this.mName;
  }
  
  public zzafk zzckl()
  {
    return this.aMk;
  }
  
  public boolean zzckm()
  {
    return this.aMj;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzafo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */