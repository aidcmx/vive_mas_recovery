package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

public final class zzafp
  extends zzafk<List<zzafk<?>>>
{
  private static final Map<String, zzzh> aMc;
  private final ArrayList<zzafk<?>> aMl;
  
  static
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put("concat", new zzzk());
    localHashMap.put("every", new zzzl());
    localHashMap.put("filter", new zzzm());
    localHashMap.put("forEach", new zzzn());
    localHashMap.put("indexOf", new zzzo());
    localHashMap.put("hasOwnProperty", zzabf.aKK);
    localHashMap.put("join", new zzzp());
    localHashMap.put("lastIndexOf", new zzzq());
    localHashMap.put("map", new zzzr());
    localHashMap.put("pop", new zzzs());
    localHashMap.put("push", new zzzt());
    localHashMap.put("reduce", new zzzu());
    localHashMap.put("reduceRight", new zzzv());
    localHashMap.put("reverse", new zzzw());
    localHashMap.put("shift", new zzzx());
    localHashMap.put("slice", new zzzy());
    localHashMap.put("some", new zzzz());
    localHashMap.put("sort", new zzaaa());
    localHashMap.put("splice", new zzaab());
    localHashMap.put("toString", new zzach());
    localHashMap.put("unshift", new zzaac());
    aMc = Collections.unmodifiableMap(localHashMap);
  }
  
  public zzafp(List<zzafk<?>> paramList)
  {
    zzaa.zzy(paramList);
    this.aMl = new ArrayList(paramList);
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = false;
    if (this == paramObject) {
      bool1 = true;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (!(paramObject instanceof zzafp));
      paramObject = (List)((zzafp)paramObject).zzckf();
      bool1 = bool2;
    } while (this.aMl.size() != ((List)paramObject).size());
    int i = 0;
    boolean bool1 = true;
    while (i < this.aMl.size())
    {
      if (this.aMl.get(i) == null) {
        if (((List)paramObject).get(i) == null) {
          bool1 = true;
        }
      }
      while (!bool1)
      {
        return bool1;
        bool1 = false;
        continue;
        bool1 = ((zzafk)this.aMl.get(i)).equals(((List)paramObject).get(i));
      }
      i += 1;
    }
    return bool1;
  }
  
  public void setSize(int paramInt)
  {
    boolean bool;
    if (paramInt >= 0)
    {
      bool = true;
      zzaa.zzb(bool, "Invalid array length");
      if (this.aMl.size() != paramInt) {
        break label29;
      }
    }
    for (;;)
    {
      return;
      bool = false;
      break;
      label29:
      if (this.aMl.size() >= paramInt) {
        break label77;
      }
      this.aMl.ensureCapacity(paramInt);
      int i = this.aMl.size();
      while (i < paramInt)
      {
        this.aMl.add(null);
        i += 1;
      }
    }
    label77:
    this.aMl.subList(paramInt, this.aMl.size()).clear();
  }
  
  public String toString()
  {
    return this.aMl.toString();
  }
  
  public void zza(int paramInt, zzafk<?> paramzzafk)
  {
    if (paramInt < 0) {
      throw new IndexOutOfBoundsException();
    }
    if (paramInt >= this.aMl.size()) {
      setSize(paramInt + 1);
    }
    this.aMl.set(paramInt, paramzzafk);
  }
  
  public zzafk<?> zzaal(int paramInt)
  {
    Object localObject;
    if ((paramInt < 0) || (paramInt >= this.aMl.size())) {
      localObject = zzafo.aMi;
    }
    zzafk localzzafk;
    do
    {
      return (zzafk<?>)localObject;
      localzzafk = (zzafk)this.aMl.get(paramInt);
      localObject = localzzafk;
    } while (localzzafk != null);
    return zzafo.aMi;
  }
  
  public boolean zzaam(int paramInt)
  {
    return (paramInt >= 0) && (paramInt < this.aMl.size()) && (this.aMl.get(paramInt) != null);
  }
  
  public Iterator<zzafk<?>> zzcke()
  {
    new Iterator()
    {
      private int aMm = 0;
      
      public boolean hasNext()
      {
        int i = this.aMm;
        while (i < zzafp.zza(zzafp.this).size())
        {
          if (zzafp.zza(zzafp.this).get(i) != null) {
            return true;
          }
          i += 1;
        }
        return false;
      }
      
      public void remove()
      {
        throw new UnsupportedOperationException();
      }
      
      public zzafk<?> zzckh()
      {
        if (this.aMm >= zzafp.zza(zzafp.this).size()) {
          throw new NoSuchElementException();
        }
        int i = this.aMm;
        while (i < zzafp.zza(zzafp.this).size())
        {
          if (zzafp.zza(zzafp.this).get(i) != null)
          {
            this.aMm = i;
            i = this.aMm;
            this.aMm = (i + 1);
            return new zzafm(Double.valueOf(i));
          }
          i += 1;
        }
        throw new NoSuchElementException();
      }
    }
    {
      public boolean hasNext()
      {
        return (this.aMo.hasNext()) || (this.aMp.hasNext());
      }
      
      public void remove()
      {
        throw new UnsupportedOperationException();
      }
      
      public zzafk<?> zzckh()
      {
        if (this.aMo.hasNext()) {
          return (zzafk)this.aMo.next();
        }
        return (zzafk)this.aMp.next();
      }
    };
  }
  
  public List<zzafk<?>> zzckn()
  {
    return this.aMl;
  }
  
  public boolean zzrg(String paramString)
  {
    return aMc.containsKey(paramString);
  }
  
  public zzzh zzrh(String paramString)
  {
    if (zzrg(paramString)) {
      return (zzzh)aMc.get(paramString);
    }
    throw new IllegalStateException(String.valueOf(paramString).length() + 51 + "Native Method " + paramString + " is not defined for type ListWrapper.");
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzafp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */