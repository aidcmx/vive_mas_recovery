package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public final class zzafq
  extends zzafk<Map<String, zzafk<?>>>
{
  private static final Map<String, zzzh> aMc;
  private boolean aMq = false;
  
  static
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put("hasOwnProperty", zzabf.aKK);
    aMc = Collections.unmodifiableMap(localHashMap);
  }
  
  public zzafq(Map<String, zzafk<?>> paramMap)
  {
    this.aLY = ((Map)zzaa.zzy(paramMap));
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if ((paramObject instanceof zzafq)) {
      return this.aLY.entrySet().equals(((Map)((zzafq)paramObject).zzckf()).entrySet());
    }
    return false;
  }
  
  public String toString()
  {
    return this.aLY.toString();
  }
  
  public Iterator<zzafk<?>> zzcke()
  {
    return zzckg();
  }
  
  public Map<String, zzafk<?>> zzcko()
  {
    return this.aLY;
  }
  
  public void zzckp()
  {
    this.aMq = true;
  }
  
  public boolean zzckq()
  {
    return this.aMq;
  }
  
  public zzafk<?> zzrf(String paramString)
  {
    zzafk localzzafk = super.zzrf(paramString);
    paramString = localzzafk;
    if (localzzafk == null) {
      paramString = zzafo.aMi;
    }
    return paramString;
  }
  
  public boolean zzrg(String paramString)
  {
    return aMc.containsKey(paramString);
  }
  
  public zzzh zzrh(String paramString)
  {
    if (zzrg(paramString)) {
      return (zzzh)aMc.get(paramString);
    }
    throw new IllegalStateException(String.valueOf(paramString).length() + 51 + "Native Method " + paramString + " is not defined for type ListWrapper.");
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzafq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */