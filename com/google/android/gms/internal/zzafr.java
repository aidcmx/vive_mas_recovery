package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;
import java.util.List;

public class zzafr
  extends zzafk<String>
{
  private final String aMr;
  private final List<zzafk<?>> aMs;
  
  public zzafr(String paramString, List<zzafk<?>> paramList)
  {
    zzaa.zzb(paramString, "Instruction name must be a string.");
    zzaa.zzy(paramList);
    this.aMr = paramString;
    this.aMs = paramList;
  }
  
  public String toString()
  {
    String str1 = this.aMr;
    String str2 = String.valueOf(this.aMs.toString());
    return String.valueOf(str1).length() + 3 + String.valueOf(str2).length() + "*" + str1 + ": " + str2;
  }
  
  public String value()
  {
    return toString();
  }
  
  public String zzckr()
  {
    return this.aMr;
  }
  
  public List<zzafk<?>> zzcks()
  {
    return this.aMs;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzafr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */