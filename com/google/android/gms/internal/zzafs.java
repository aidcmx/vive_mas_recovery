package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzaa;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;

public final class zzafs
  extends zzafk<String>
{
  private static final Map<String, zzzh> aMc;
  private final String mValue;
  
  static
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put("charAt", new zzabu());
    localHashMap.put("concat", new zzabv());
    localHashMap.put("hasOwnProperty", zzabf.aKK);
    localHashMap.put("indexOf", new zzabw());
    localHashMap.put("lastIndexOf", new zzabx());
    localHashMap.put("match", new zzaby());
    localHashMap.put("replace", new zzabz());
    localHashMap.put("search", new zzaca());
    localHashMap.put("slice", new zzacb());
    localHashMap.put("split", new zzacc());
    localHashMap.put("substring", new zzacd());
    localHashMap.put("toLocaleLowerCase", new zzace());
    localHashMap.put("toLocaleUpperCase", new zzacf());
    localHashMap.put("toLowerCase", new zzacg());
    localHashMap.put("toUpperCase", new zzaci());
    localHashMap.put("toString", new zzach());
    localHashMap.put("trim", new zzacj());
    aMc = Collections.unmodifiableMap(localHashMap);
  }
  
  public zzafs(String paramString)
  {
    zzaa.zzy(paramString);
    this.mValue = paramString;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if ((paramObject instanceof zzafs)) {
      return this.mValue.equals((String)((zzafs)paramObject).zzckf());
    }
    return false;
  }
  
  public String toString()
  {
    return this.mValue.toString();
  }
  
  public String value()
  {
    return this.mValue;
  }
  
  public zzafk<?> zzaan(int paramInt)
  {
    if ((paramInt >= 0) && (paramInt < this.mValue.length())) {
      return new zzafs(String.valueOf(this.mValue.charAt(paramInt)));
    }
    return zzafo.aMi;
  }
  
  public Iterator<zzafk<?>> zzcke()
  {
    new Iterator()
    {
      private int aMm = 0;
      
      public boolean hasNext()
      {
        return this.aMm < zzafs.zza(zzafs.this).length();
      }
      
      public void remove()
      {
        throw new UnsupportedOperationException();
      }
      
      public zzafk<?> zzckh()
      {
        if (this.aMm >= zzafs.zza(zzafs.this).length()) {
          throw new NoSuchElementException();
        }
        int i = this.aMm;
        this.aMm = (i + 1);
        return new zzafm(Double.valueOf(i));
      }
    };
  }
  
  public boolean zzrg(String paramString)
  {
    return aMc.containsKey(paramString);
  }
  
  public zzzh zzrh(String paramString)
  {
    if (zzrg(paramString)) {
      return (zzzh)aMc.get(paramString);
    }
    throw new IllegalStateException(String.valueOf(paramString).length() + 51 + "Native Method " + paramString + " is not defined for type ListWrapper.");
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzafs.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */