package com.google.android.gms.internal;

import android.os.Bundle;
import android.support.annotation.Nullable;
import com.google.android.gms.common.internal.zzaa;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class zzaft
{
  public static zzafk zza(zzyu paramzzyu, zzafk paramzzafk)
  {
    zzaa.zzy(paramzzafk);
    zzafk localzzafk = paramzzafk;
    if (!zzn(paramzzafk))
    {
      localzzafk = paramzzafk;
      if (!(paramzzafk instanceof zzafn))
      {
        localzzafk = paramzzafk;
        if (!(paramzzafk instanceof zzafp))
        {
          if (!(paramzzafk instanceof zzafq)) {
            break label62;
          }
          localzzafk = paramzzafk;
        }
      }
    }
    while (localzzafk != null)
    {
      if (!(localzzafk instanceof zzafr)) {
        return localzzafk;
      }
      throw new IllegalArgumentException("AbstractType evaluated to illegal type Statement.");
      label62:
      if ((paramzzafk instanceof zzafr)) {
        localzzafk = zza(paramzzyu, (zzafr)paramzzafk);
      } else {
        throw new UnsupportedOperationException("Attempting to evaluate unknown type");
      }
    }
    throw new IllegalArgumentException("AbstractType evaluated to Java null");
    return localzzafk;
  }
  
  public static zzafk zza(zzyu paramzzyu, zzafr paramzzafr)
  {
    String str = paramzzafr.zzckr();
    paramzzafr = paramzzafr.zzcks();
    zzafk localzzafk = paramzzyu.zzqn(str);
    if (localzzafk == null) {
      throw new UnsupportedOperationException(String.valueOf(str).length() + 28 + "Function '" + str + "' is not supported");
    }
    if (!(localzzafk instanceof zzafn)) {
      throw new UnsupportedOperationException(String.valueOf(str).length() + 29 + "Function '" + str + "' is not a function");
    }
    return ((zzzh)((zzafn)localzzafk).zzckf()).zzb(paramzzyu, (zzafk[])paramzzafr.toArray(new zzafk[paramzzafr.size()]));
  }
  
  public static zzafo zza(zzyu paramzzyu, List<zzafk<?>> paramList)
  {
    paramList = paramList.iterator();
    while (paramList.hasNext())
    {
      zzafk localzzafk = (zzafk)paramList.next();
      zzaa.zzbt(localzzafk instanceof zzafr);
      localzzafk = zza(paramzzyu, localzzafk);
      if (zzo(localzzafk)) {
        return (zzafo)localzzafk;
      }
    }
    return zzafo.aMi;
  }
  
  private static List<Object> zzan(List<Object> paramList)
  {
    ArrayList localArrayList = new ArrayList();
    paramList = paramList.iterator();
    while (paramList.hasNext())
    {
      Object localObject = paramList.next();
      if ((localObject instanceof Bundle)) {
        localArrayList.add(zzao((Bundle)localObject));
      } else if ((localObject instanceof List)) {
        localArrayList.add(zzan((List)localObject));
      } else {
        localArrayList.add(localObject);
      }
    }
    return localArrayList;
  }
  
  public static Map<String, Object> zzao(Bundle paramBundle)
  {
    HashMap localHashMap = new HashMap();
    Iterator localIterator = paramBundle.keySet().iterator();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      Object localObject = paramBundle.get(str);
      if ((localObject instanceof Bundle)) {
        localHashMap.put(str, zzao((Bundle)localObject));
      } else if ((localObject instanceof List)) {
        localHashMap.put(str, zzan((List)localObject));
      } else {
        localHashMap.put(str, localObject);
      }
    }
    return localHashMap;
  }
  
  public static zzafk<?> zzbb(@Nullable Object paramObject)
  {
    if (paramObject == null) {
      return zzafo.aMh;
    }
    if ((paramObject instanceof zzafk)) {
      return (zzafk)paramObject;
    }
    if ((paramObject instanceof Boolean)) {
      return new zzafl((Boolean)paramObject);
    }
    if ((paramObject instanceof Short)) {
      return new zzafm(Double.valueOf(((Short)paramObject).doubleValue()));
    }
    if ((paramObject instanceof Integer)) {
      return new zzafm(Double.valueOf(((Integer)paramObject).doubleValue()));
    }
    if ((paramObject instanceof Long)) {
      return new zzafm(Double.valueOf(((Long)paramObject).doubleValue()));
    }
    if ((paramObject instanceof Float)) {
      return new zzafm(Double.valueOf(((Float)paramObject).doubleValue()));
    }
    if ((paramObject instanceof Double)) {
      return new zzafm((Double)paramObject);
    }
    if ((paramObject instanceof Byte)) {
      return new zzafs(paramObject.toString());
    }
    if ((paramObject instanceof Character)) {
      return new zzafs(paramObject.toString());
    }
    if ((paramObject instanceof String)) {
      return new zzafs((String)paramObject);
    }
    Object localObject1;
    if ((paramObject instanceof List))
    {
      localObject1 = new ArrayList();
      paramObject = ((List)paramObject).iterator();
      while (((Iterator)paramObject).hasNext()) {
        ((List)localObject1).add(zzbb(((Iterator)paramObject).next()));
      }
      return new zzafp((List)localObject1);
    }
    Object localObject2;
    if ((paramObject instanceof Map))
    {
      localObject1 = new HashMap();
      paramObject = ((Map)paramObject).entrySet().iterator();
      while (((Iterator)paramObject).hasNext())
      {
        localObject2 = (Map.Entry)((Iterator)paramObject).next();
        zzaa.zzbt(((Map.Entry)localObject2).getKey() instanceof String);
        ((Map)localObject1).put((String)((Map.Entry)localObject2).getKey(), zzbb(((Map.Entry)localObject2).getValue()));
      }
      return new zzafq((Map)localObject1);
    }
    if ((paramObject instanceof Bundle))
    {
      localObject1 = new HashMap();
      localObject2 = ((Bundle)paramObject).keySet().iterator();
      while (((Iterator)localObject2).hasNext())
      {
        String str = (String)((Iterator)localObject2).next();
        ((Map)localObject1).put(str, zzbb(((Bundle)paramObject).get(str)));
      }
      return new zzafq((Map)localObject1);
    }
    paramObject = String.valueOf(paramObject.getClass());
    throw new UnsupportedOperationException(String.valueOf(paramObject).length() + 20 + "Type not supported: " + (String)paramObject);
  }
  
  public static Bundle zzbr(Map<String, zzafk<?>> paramMap)
  {
    if (paramMap == null) {
      return null;
    }
    Bundle localBundle = new Bundle(paramMap.size());
    paramMap = paramMap.entrySet().iterator();
    while (paramMap.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)paramMap.next();
      if ((localEntry.getValue() instanceof zzafs)) {
        localBundle.putString((String)localEntry.getKey(), (String)((zzafs)localEntry.getValue()).zzckf());
      } else if ((localEntry.getValue() instanceof zzafl)) {
        localBundle.putBoolean((String)localEntry.getKey(), ((Boolean)((zzafl)localEntry.getValue()).zzckf()).booleanValue());
      } else if ((localEntry.getValue() instanceof zzafm)) {
        localBundle.putDouble((String)localEntry.getKey(), ((Double)((zzafm)localEntry.getValue()).zzckf()).doubleValue());
      } else if ((localEntry.getValue() instanceof zzafq)) {
        localBundle.putBundle((String)localEntry.getKey(), zzbr((Map)((zzafq)localEntry.getValue()).zzckf()));
      } else {
        throw new IllegalArgumentException(String.format("Invalid param type for key '%s'. Only boolean, double and string types and maps of thereof are supported.", new Object[] { localEntry.getKey() }));
      }
    }
    return localBundle;
  }
  
  public static Object zzl(zzafk<?> paramzzafk)
  {
    if (paramzzafk == null) {
      return null;
    }
    if (paramzzafk == zzafo.aMh) {
      return null;
    }
    if ((paramzzafk instanceof zzafl)) {
      return (Boolean)((zzafl)paramzzafk).zzckf();
    }
    if ((paramzzafk instanceof zzafm))
    {
      double d = ((Double)((zzafm)paramzzafk).zzckf()).doubleValue();
      if ((!Double.isInfinite(d)) && (d - Math.floor(d) < 1.0E-5D)) {
        return Integer.valueOf((int)d);
      }
      return ((Double)((zzafm)paramzzafk).zzckf()).toString();
    }
    if ((paramzzafk instanceof zzafs)) {
      return (String)((zzafs)paramzzafk).zzckf();
    }
    Object localObject1;
    Object localObject2;
    Object localObject3;
    if ((paramzzafk instanceof zzafp))
    {
      localObject1 = new ArrayList();
      paramzzafk = ((List)((zzafp)paramzzafk).zzckf()).iterator();
      while (paramzzafk.hasNext())
      {
        localObject2 = (zzafk)paramzzafk.next();
        localObject3 = zzl((zzafk)localObject2);
        if (localObject3 == null)
        {
          zzyl.e(String.format("Failure to convert a list element to object: %s (%s)", new Object[] { localObject2, localObject2.getClass().getCanonicalName() }));
          return null;
        }
        ((List)localObject1).add(localObject3);
      }
      return localObject1;
    }
    if ((paramzzafk instanceof zzafq))
    {
      localObject1 = new HashMap();
      paramzzafk = ((Map)((zzafq)paramzzafk).zzckf()).entrySet().iterator();
      while (paramzzafk.hasNext())
      {
        localObject2 = (Map.Entry)paramzzafk.next();
        localObject3 = zzl((zzafk)((Map.Entry)localObject2).getValue());
        if (localObject3 == null)
        {
          zzyl.e(String.format("Failure to convert a map's value to object: %s (%s)", new Object[] { ((Map.Entry)localObject2).getValue(), ((zzafk)((Map.Entry)localObject2).getValue()).getClass().getCanonicalName() }));
          return null;
        }
        ((Map)localObject1).put((String)((Map.Entry)localObject2).getKey(), localObject3);
      }
      return localObject1;
    }
    paramzzafk = String.valueOf(paramzzafk.getClass());
    zzyl.e(String.valueOf(paramzzafk).length() + 49 + "Converting to Object from unknown abstract type: " + paramzzafk);
    return null;
  }
  
  public static zzafk zzm(zzafk<?> paramzzafk)
  {
    if (!(paramzzafk instanceof zzafq)) {}
    for (;;)
    {
      return paramzzafk;
      Object localObject = new HashSet();
      Map localMap = (Map)((zzafq)paramzzafk).zzckf();
      Iterator localIterator = localMap.entrySet().iterator();
      while (localIterator.hasNext())
      {
        Map.Entry localEntry = (Map.Entry)localIterator.next();
        if (localEntry.getValue() == zzafo.aMi) {
          ((Set)localObject).add((String)localEntry.getKey());
        }
      }
      localObject = ((Set)localObject).iterator();
      while (((Iterator)localObject).hasNext()) {
        localMap.remove((String)((Iterator)localObject).next());
      }
    }
  }
  
  public static boolean zzn(zzafk paramzzafk)
  {
    if (((paramzzafk instanceof zzafl)) || ((paramzzafk instanceof zzafm)) || ((paramzzafk instanceof zzafs))) {}
    while ((paramzzafk == zzafo.aMh) || (paramzzafk == zzafo.aMi)) {
      return true;
    }
    return false;
  }
  
  public static boolean zzo(zzafk paramzzafk)
  {
    return (paramzzafk == zzafo.aMg) || (paramzzafk == zzafo.aMf) || (((paramzzafk instanceof zzafo)) && (((zzafo)paramzzafk).zzckm()));
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaft.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */