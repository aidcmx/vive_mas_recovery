package com.google.android.gms.internal;

import com.google.android.gms.tagmanager.zzbo;
import com.google.android.gms.tagmanager.zzdm;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class zzafw
{
  private static zza zza(zzai.zzb paramzzb, zzai.zzf paramzzf, zzaj.zza[] paramArrayOfzza, int paramInt)
    throws zzafw.zzg
  {
    zzb localzzb = zza.zzcku();
    paramzzb = paramzzb.zzvu;
    int i = paramzzb.length;
    paramInt = 0;
    if (paramInt < i)
    {
      int j = paramzzb[paramInt];
      Object localObject = (zzai.zze)zza(paramzzf.zzwk, Integer.valueOf(j).intValue(), "properties");
      String str = (String)zza(paramzzf.zzwi, ((zzai.zze)localObject).key, "keys");
      localObject = (zzaj.zza)zza(paramArrayOfzza, ((zzai.zze)localObject).value, "values");
      if (zzah.zzst.toString().equals(str)) {
        localzzb.zzq((zzaj.zza)localObject);
      }
      for (;;)
      {
        paramInt += 1;
        break;
        localzzb.zzb(str, (zzaj.zza)localObject);
      }
    }
    return localzzb.zzckv();
  }
  
  private static zze zza(zzai.zzg paramzzg, List<zza> paramList1, List<zza> paramList2, List<zza> paramList3, zzai.zzf paramzzf)
  {
    zzf localzzf = zze.zzckz();
    int[] arrayOfInt = paramzzg.zzwy;
    int j = arrayOfInt.length;
    int i = 0;
    while (i < j)
    {
      localzzf.zzd((zza)paramList3.get(Integer.valueOf(arrayOfInt[i]).intValue()));
      i += 1;
    }
    arrayOfInt = paramzzg.zzwz;
    j = arrayOfInt.length;
    i = 0;
    while (i < j)
    {
      localzzf.zze((zza)paramList3.get(Integer.valueOf(arrayOfInt[i]).intValue()));
      i += 1;
    }
    paramList3 = paramzzg.zzxa;
    j = paramList3.length;
    i = 0;
    while (i < j)
    {
      localzzf.zzf((zza)paramList1.get(Integer.valueOf(paramList3[i]).intValue()));
      i += 1;
    }
    paramList3 = paramzzg.zzxc;
    j = paramList3.length;
    i = 0;
    int k;
    while (i < j)
    {
      k = paramList3[i];
      localzzf.zzrj(paramzzf.zzwj[Integer.valueOf(k).intValue()].string);
      i += 1;
    }
    paramList3 = paramzzg.zzxb;
    j = paramList3.length;
    i = 0;
    while (i < j)
    {
      localzzf.zzg((zza)paramList1.get(Integer.valueOf(paramList3[i]).intValue()));
      i += 1;
    }
    paramList1 = paramzzg.zzxd;
    j = paramList1.length;
    i = 0;
    while (i < j)
    {
      k = paramList1[i];
      localzzf.zzrk(paramzzf.zzwj[Integer.valueOf(k).intValue()].string);
      i += 1;
    }
    paramList1 = paramzzg.zzxe;
    j = paramList1.length;
    i = 0;
    while (i < j)
    {
      localzzf.zzh((zza)paramList2.get(Integer.valueOf(paramList1[i]).intValue()));
      i += 1;
    }
    paramList1 = paramzzg.zzxg;
    j = paramList1.length;
    i = 0;
    while (i < j)
    {
      k = paramList1[i];
      localzzf.zzrl(paramzzf.zzwj[Integer.valueOf(k).intValue()].string);
      i += 1;
    }
    paramList1 = paramzzg.zzxf;
    j = paramList1.length;
    i = 0;
    while (i < j)
    {
      localzzf.zzi((zza)paramList2.get(Integer.valueOf(paramList1[i]).intValue()));
      i += 1;
    }
    paramzzg = paramzzg.zzxh;
    j = paramzzg.length;
    i = 0;
    while (i < j)
    {
      k = paramzzg[i];
      localzzf.zzrm(paramzzf.zzwj[Integer.valueOf(k).intValue()].string);
      i += 1;
    }
    return localzzf.zzcle();
  }
  
  private static zzaj.zza zza(int paramInt, zzai.zzf paramzzf, zzaj.zza[] paramArrayOfzza, Set<Integer> paramSet)
    throws zzafw.zzg
  {
    int k = 0;
    int m = 0;
    int j = 0;
    if (paramSet.contains(Integer.valueOf(paramInt)))
    {
      localObject = String.valueOf(paramSet);
      zzqt(String.valueOf(localObject).length() + 90 + "Value cycle detected.  Current value reference: " + paramInt + ".  Previous value references: " + (String)localObject + ".");
    }
    zzaj.zza localzza1 = (zzaj.zza)zza(paramzzf.zzwj, paramInt, "values");
    if (paramArrayOfzza[paramInt] != null) {
      return paramArrayOfzza[paramInt];
    }
    Object localObject = null;
    paramSet.add(Integer.valueOf(paramInt));
    switch (localzza1.type)
    {
    }
    for (;;)
    {
      if (localObject == null)
      {
        paramzzf = String.valueOf(localzza1);
        zzqt(String.valueOf(paramzzf).length() + 15 + "Invalid value: " + paramzzf);
      }
      paramArrayOfzza[paramInt] = localObject;
      paramSet.remove(Integer.valueOf(paramInt));
      return (zzaj.zza)localObject;
      localObject = zzp(localzza1);
      zzaj.zza localzza2 = zzo(localzza1);
      localzza2.zzxy = new zzaj.zza[((zzai.zzh)localObject).zzxk.length];
      int[] arrayOfInt = ((zzai.zzh)localObject).zzxk;
      k = arrayOfInt.length;
      int i = 0;
      for (;;)
      {
        localObject = localzza2;
        if (j >= k) {
          break;
        }
        m = arrayOfInt[j];
        localzza2.zzxy[i] = zza(m, paramzzf, paramArrayOfzza, paramSet);
        j += 1;
        i += 1;
      }
      localzza2 = zzo(localzza1);
      localObject = zzp(localzza1);
      if (((zzai.zzh)localObject).zzxl.length != ((zzai.zzh)localObject).zzxm.length)
      {
        i = ((zzai.zzh)localObject).zzxl.length;
        j = ((zzai.zzh)localObject).zzxm.length;
        zzqt(58 + "Uneven map keys (" + i + ") and map values (" + j + ")");
      }
      localzza2.zzxz = new zzaj.zza[((zzai.zzh)localObject).zzxl.length];
      localzza2.zzya = new zzaj.zza[((zzai.zzh)localObject).zzxl.length];
      arrayOfInt = ((zzai.zzh)localObject).zzxl;
      m = arrayOfInt.length;
      j = 0;
      i = 0;
      while (j < m)
      {
        int n = arrayOfInt[j];
        localzza2.zzxz[i] = zza(n, paramzzf, paramArrayOfzza, paramSet);
        j += 1;
        i += 1;
      }
      arrayOfInt = ((zzai.zzh)localObject).zzxm;
      m = arrayOfInt.length;
      i = 0;
      j = k;
      for (;;)
      {
        localObject = localzza2;
        if (j >= m) {
          break;
        }
        k = arrayOfInt[j];
        localzza2.zzya[i] = zza(k, paramzzf, paramArrayOfzza, paramSet);
        j += 1;
        i += 1;
      }
      localObject = zzo(localzza1);
      ((zzaj.zza)localObject).zzyb = zzdm.zzg(zza(zzp(localzza1).zzxp, paramzzf, paramArrayOfzza, paramSet));
      continue;
      localzza2 = zzo(localzza1);
      localObject = zzp(localzza1);
      localzza2.zzyf = new zzaj.zza[((zzai.zzh)localObject).zzxo.length];
      arrayOfInt = ((zzai.zzh)localObject).zzxo;
      k = arrayOfInt.length;
      i = 0;
      j = m;
      for (;;)
      {
        localObject = localzza2;
        if (j >= k) {
          break;
        }
        m = arrayOfInt[j];
        localzza2.zzyf[i] = zza(m, paramzzf, paramArrayOfzza, paramSet);
        j += 1;
        i += 1;
      }
      localObject = localzza1;
    }
  }
  
  private static <T> T zza(T[] paramArrayOfT, int paramInt, String paramString)
    throws zzafw.zzg
  {
    if ((paramInt < 0) || (paramInt >= paramArrayOfT.length)) {
      zzqt(String.valueOf(paramString).length() + 45 + "Index out of bounds detected: " + paramInt + " in " + paramString);
    }
    return paramArrayOfT[paramInt];
  }
  
  public static zzc zzb(zzai.zzf paramzzf)
    throws zzafw.zzg
  {
    int j = 0;
    Object localObject = new zzaj.zza[paramzzf.zzwj.length];
    int i = 0;
    while (i < paramzzf.zzwj.length)
    {
      zza(i, paramzzf, (zzaj.zza[])localObject, new HashSet(0));
      i += 1;
    }
    zzd localzzd = zzc.zzckw();
    ArrayList localArrayList1 = new ArrayList();
    i = 0;
    while (i < paramzzf.zzwm.length)
    {
      localArrayList1.add(zza(paramzzf.zzwm[i], paramzzf, (zzaj.zza[])localObject, i));
      i += 1;
    }
    ArrayList localArrayList2 = new ArrayList();
    i = 0;
    while (i < paramzzf.zzwn.length)
    {
      localArrayList2.add(zza(paramzzf.zzwn[i], paramzzf, (zzaj.zza[])localObject, i));
      i += 1;
    }
    ArrayList localArrayList3 = new ArrayList();
    i = 0;
    while (i < paramzzf.zzwl.length)
    {
      zza localzza = zza(paramzzf.zzwl[i], paramzzf, (zzaj.zza[])localObject, i);
      localzzd.zzc(localzza);
      localArrayList3.add(localzza);
      i += 1;
    }
    localObject = paramzzf.zzwo;
    int k = localObject.length;
    i = j;
    while (i < k)
    {
      localzzd.zzb(zza(localObject[i], localArrayList1, localArrayList3, localArrayList2, paramzzf));
      i += 1;
    }
    localzzd.zzri(paramzzf.version);
    localzzd.zzaao(paramzzf.zzww);
    return localzzd.zzcky();
  }
  
  public static void zzc(InputStream paramInputStream, OutputStream paramOutputStream)
    throws IOException
  {
    byte[] arrayOfByte = new byte['Ѐ'];
    for (;;)
    {
      int i = paramInputStream.read(arrayOfByte);
      if (i == -1) {
        return;
      }
      paramOutputStream.write(arrayOfByte, 0, i);
    }
  }
  
  public static zzaj.zza zzo(zzaj.zza paramzza)
  {
    zzaj.zza localzza = new zzaj.zza();
    localzza.type = paramzza.type;
    localzza.zzyg = ((int[])paramzza.zzyg.clone());
    if (paramzza.zzyh) {
      localzza.zzyh = paramzza.zzyh;
    }
    return localzza;
  }
  
  private static zzai.zzh zzp(zzaj.zza paramzza)
    throws zzafw.zzg
  {
    if ((zzai.zzh)paramzza.zza(zzai.zzh.zzxi) == null)
    {
      String str = String.valueOf(paramzza);
      zzqt(String.valueOf(str).length() + 54 + "Expected a ServingValue and didn't get one. Value is: " + str);
    }
    return (zzai.zzh)paramzza.zza(zzai.zzh.zzxi);
  }
  
  private static void zzqt(String paramString)
    throws zzafw.zzg
  {
    zzbo.e(paramString);
    throw new zzg(paramString);
  }
  
  public static class zza
  {
    private final zzaj.zza aHd;
    private final Map<String, zzaj.zza> aLM;
    
    private zza(Map<String, zzaj.zza> paramMap, zzaj.zza paramzza)
    {
      this.aLM = paramMap;
      this.aHd = paramzza;
    }
    
    public static zzafw.zzb zzcku()
    {
      return new zzafw.zzb(null);
    }
    
    public String toString()
    {
      String str1 = String.valueOf(zzcjt());
      String str2 = String.valueOf(this.aHd);
      return String.valueOf(str1).length() + 32 + String.valueOf(str2).length() + "Properties: " + str1 + " pushAfterEvaluate: " + str2;
    }
    
    public void zza(String paramString, zzaj.zza paramzza)
    {
      this.aLM.put(paramString, paramzza);
    }
    
    public zzaj.zza zzcgm()
    {
      return this.aHd;
    }
    
    public Map<String, zzaj.zza> zzcjt()
    {
      return Collections.unmodifiableMap(this.aLM);
    }
  }
  
  public static class zzb
  {
    private zzaj.zza aHd;
    private final Map<String, zzaj.zza> aLM = new HashMap();
    
    public zzb zzb(String paramString, zzaj.zza paramzza)
    {
      this.aLM.put(paramString, paramzza);
      return this;
    }
    
    public zzafw.zza zzckv()
    {
      return new zzafw.zza(this.aLM, this.aHd, null);
    }
    
    public zzb zzq(zzaj.zza paramzza)
    {
      this.aHd = paramzza;
      return this;
    }
  }
  
  public static class zzc
  {
    private final List<zzafw.zze> aLJ;
    private final Map<String, List<zzafw.zza>> aLK;
    private final int aLL;
    private final String um;
    
    private zzc(List<zzafw.zze> paramList, Map<String, List<zzafw.zza>> paramMap, String paramString, int paramInt)
    {
      this.aLJ = Collections.unmodifiableList(paramList);
      this.aLK = Collections.unmodifiableMap(paramMap);
      this.um = paramString;
      this.aLL = paramInt;
    }
    
    public static zzafw.zzd zzckw()
    {
      return new zzafw.zzd(null);
    }
    
    public String getVersion()
    {
      return this.um;
    }
    
    public String toString()
    {
      String str1 = String.valueOf(zzcjr());
      String str2 = String.valueOf(this.aLK);
      return String.valueOf(str1).length() + 17 + String.valueOf(str2).length() + "Rules: " + str1 + "  Macros: " + str2;
    }
    
    public List<zzafw.zze> zzcjr()
    {
      return this.aLJ;
    }
    
    public Map<String, List<zzafw.zza>> zzckx()
    {
      return this.aLK;
    }
  }
  
  public static class zzd
  {
    private final List<zzafw.zze> aLJ = new ArrayList();
    private final Map<String, List<zzafw.zza>> aLK = new HashMap();
    private int aLL = 0;
    private String um = "";
    
    public zzd zzaao(int paramInt)
    {
      this.aLL = paramInt;
      return this;
    }
    
    public zzd zzb(zzafw.zze paramzze)
    {
      this.aLJ.add(paramzze);
      return this;
    }
    
    public zzd zzc(zzafw.zza paramzza)
    {
      String str = zzdm.zzg((zzaj.zza)paramzza.zzcjt().get(zzah.zzqm.toString()));
      List localList = (List)this.aLK.get(str);
      Object localObject = localList;
      if (localList == null)
      {
        localObject = new ArrayList();
        this.aLK.put(str, localObject);
      }
      ((List)localObject).add(paramzza);
      return this;
    }
    
    public zzafw.zzc zzcky()
    {
      return new zzafw.zzc(this.aLJ, this.aLK, this.um, this.aLL, null);
    }
    
    public zzd zzri(String paramString)
    {
      this.um = paramString;
      return this;
    }
  }
  
  public static class zze
  {
    private final List<zzafw.zza> aLO;
    private final List<zzafw.zza> aLP;
    private final List<zzafw.zza> aLQ;
    private final List<zzafw.zza> aLR;
    private final List<String> aMA;
    private final List<String> aMB;
    private final List<String> aMC;
    private final List<zzafw.zza> aMx;
    private final List<zzafw.zza> aMy;
    private final List<String> aMz;
    
    private zze(List<zzafw.zza> paramList1, List<zzafw.zza> paramList2, List<zzafw.zza> paramList3, List<zzafw.zza> paramList4, List<zzafw.zza> paramList5, List<zzafw.zza> paramList6, List<String> paramList7, List<String> paramList8, List<String> paramList9, List<String> paramList10)
    {
      this.aLO = Collections.unmodifiableList(paramList1);
      this.aLP = Collections.unmodifiableList(paramList2);
      this.aLQ = Collections.unmodifiableList(paramList3);
      this.aLR = Collections.unmodifiableList(paramList4);
      this.aMx = Collections.unmodifiableList(paramList5);
      this.aMy = Collections.unmodifiableList(paramList6);
      this.aMz = Collections.unmodifiableList(paramList7);
      this.aMA = Collections.unmodifiableList(paramList8);
      this.aMB = Collections.unmodifiableList(paramList9);
      this.aMC = Collections.unmodifiableList(paramList10);
    }
    
    public static zzafw.zzf zzckz()
    {
      return new zzafw.zzf(null);
    }
    
    public String toString()
    {
      String str1 = String.valueOf(zzcjv());
      String str2 = String.valueOf(zzcjw());
      String str3 = String.valueOf(zzcjx());
      String str4 = String.valueOf(zzcjy());
      String str5 = String.valueOf(zzcla());
      String str6 = String.valueOf(zzcld());
      return String.valueOf(str1).length() + 102 + String.valueOf(str2).length() + String.valueOf(str3).length() + String.valueOf(str4).length() + String.valueOf(str5).length() + String.valueOf(str6).length() + "Positive predicates: " + str1 + "  Negative predicates: " + str2 + "  Add tags: " + str3 + "  Remove tags: " + str4 + "  Add macros: " + str5 + "  Remove macros: " + str6;
    }
    
    public List<zzafw.zza> zzcjv()
    {
      return this.aLO;
    }
    
    public List<zzafw.zza> zzcjw()
    {
      return this.aLP;
    }
    
    public List<zzafw.zza> zzcjx()
    {
      return this.aLQ;
    }
    
    public List<zzafw.zza> zzcjy()
    {
      return this.aLR;
    }
    
    public List<zzafw.zza> zzcla()
    {
      return this.aMx;
    }
    
    public List<String> zzclb()
    {
      return this.aMB;
    }
    
    public List<String> zzclc()
    {
      return this.aMC;
    }
    
    public List<zzafw.zza> zzcld()
    {
      return this.aMy;
    }
  }
  
  public static class zzf
  {
    private final List<zzafw.zza> aLO = new ArrayList();
    private final List<zzafw.zza> aLP = new ArrayList();
    private final List<zzafw.zza> aLQ = new ArrayList();
    private final List<zzafw.zza> aLR = new ArrayList();
    private final List<String> aMA = new ArrayList();
    private final List<String> aMB = new ArrayList();
    private final List<String> aMC = new ArrayList();
    private final List<zzafw.zza> aMx = new ArrayList();
    private final List<zzafw.zza> aMy = new ArrayList();
    private final List<String> aMz = new ArrayList();
    
    public zzafw.zze zzcle()
    {
      return new zzafw.zze(this.aLO, this.aLP, this.aLQ, this.aLR, this.aMx, this.aMy, this.aMz, this.aMA, this.aMB, this.aMC, null);
    }
    
    public zzf zzd(zzafw.zza paramzza)
    {
      this.aLO.add(paramzza);
      return this;
    }
    
    public zzf zze(zzafw.zza paramzza)
    {
      this.aLP.add(paramzza);
      return this;
    }
    
    public zzf zzf(zzafw.zza paramzza)
    {
      this.aLQ.add(paramzza);
      return this;
    }
    
    public zzf zzg(zzafw.zza paramzza)
    {
      this.aLR.add(paramzza);
      return this;
    }
    
    public zzf zzh(zzafw.zza paramzza)
    {
      this.aMx.add(paramzza);
      return this;
    }
    
    public zzf zzi(zzafw.zza paramzza)
    {
      this.aMy.add(paramzza);
      return this;
    }
    
    public zzf zzrj(String paramString)
    {
      this.aMB.add(paramString);
      return this;
    }
    
    public zzf zzrk(String paramString)
    {
      this.aMC.add(paramString);
      return this;
    }
    
    public zzf zzrl(String paramString)
    {
      this.aMz.add(paramString);
      return this;
    }
    
    public zzf zzrm(String paramString)
    {
      this.aMA.add(paramString);
      return this;
    }
  }
  
  public static class zzg
    extends Exception
  {
    public zzg(String paramString)
    {
      super();
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzafw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */