package com.google.android.gms.internal;

import android.annotation.SuppressLint;
import com.google.android.gms.common.api.BooleanResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.wallet.FullWalletRequest;
import com.google.android.gms.wallet.IsReadyToPayRequest;
import com.google.android.gms.wallet.IsReadyToPayRequest.Builder;
import com.google.android.gms.wallet.MaskedWalletRequest;
import com.google.android.gms.wallet.NotifyTransactionStatusRequest;
import com.google.android.gms.wallet.Payments;
import com.google.android.gms.wallet.Wallet.zza;
import com.google.android.gms.wallet.Wallet.zzb;

@SuppressLint({"MissingRemoteException"})
public class zzagi
  implements Payments
{
  public void changeMaskedWallet(GoogleApiClient paramGoogleApiClient, final String paramString1, final String paramString2, final int paramInt)
  {
    paramGoogleApiClient.zza(new Wallet.zzb(paramGoogleApiClient)
    {
      protected void zza(zzagj paramAnonymouszzagj)
      {
        paramAnonymouszzagj.zzf(paramString1, paramString2, paramInt);
        zzc(Status.xZ);
      }
    });
  }
  
  public void checkForPreAuthorization(GoogleApiClient paramGoogleApiClient, final int paramInt)
  {
    paramGoogleApiClient.zza(new Wallet.zzb(paramGoogleApiClient)
    {
      protected void zza(zzagj paramAnonymouszzagj)
      {
        paramAnonymouszzagj.zzacy(paramInt);
        zzc(Status.xZ);
      }
    });
  }
  
  public void isNewUser(GoogleApiClient paramGoogleApiClient, final int paramInt)
  {
    paramGoogleApiClient.zza(new Wallet.zzb(paramGoogleApiClient)
    {
      protected void zza(zzagj paramAnonymouszzagj)
      {
        paramAnonymouszzagj.zzacz(paramInt);
        zzc(Status.xZ);
      }
    });
  }
  
  public PendingResult<BooleanResult> isReadyToPay(GoogleApiClient paramGoogleApiClient)
  {
    paramGoogleApiClient.zza(new Wallet.zza(paramGoogleApiClient)
    {
      protected void zza(zzagj paramAnonymouszzagj)
      {
        paramAnonymouszzagj.zza(IsReadyToPayRequest.newBuilder().build(), this);
      }
      
      protected BooleanResult zzam(Status paramAnonymousStatus)
      {
        return new BooleanResult(paramAnonymousStatus, false);
      }
    });
  }
  
  public PendingResult<BooleanResult> isReadyToPay(GoogleApiClient paramGoogleApiClient, final IsReadyToPayRequest paramIsReadyToPayRequest)
  {
    paramGoogleApiClient.zza(new Wallet.zza(paramGoogleApiClient)
    {
      protected void zza(zzagj paramAnonymouszzagj)
      {
        paramAnonymouszzagj.zza(paramIsReadyToPayRequest, this);
      }
      
      protected BooleanResult zzam(Status paramAnonymousStatus)
      {
        return new BooleanResult(paramAnonymousStatus, false);
      }
    });
  }
  
  public void loadFullWallet(GoogleApiClient paramGoogleApiClient, final FullWalletRequest paramFullWalletRequest, final int paramInt)
  {
    paramGoogleApiClient.zza(new Wallet.zzb(paramGoogleApiClient)
    {
      protected void zza(zzagj paramAnonymouszzagj)
      {
        paramAnonymouszzagj.zza(paramFullWalletRequest, paramInt);
        zzc(Status.xZ);
      }
    });
  }
  
  public void loadMaskedWallet(GoogleApiClient paramGoogleApiClient, final MaskedWalletRequest paramMaskedWalletRequest, final int paramInt)
  {
    paramGoogleApiClient.zza(new Wallet.zzb(paramGoogleApiClient)
    {
      protected void zza(zzagj paramAnonymouszzagj)
      {
        paramAnonymouszzagj.zza(paramMaskedWalletRequest, paramInt);
        zzc(Status.xZ);
      }
    });
  }
  
  public void notifyTransactionStatus(GoogleApiClient paramGoogleApiClient, final NotifyTransactionStatusRequest paramNotifyTransactionStatusRequest)
  {
    paramGoogleApiClient.zza(new Wallet.zzb(paramGoogleApiClient)
    {
      protected void zza(zzagj paramAnonymouszzagj)
      {
        paramAnonymouszzagj.zza(paramNotifyTransactionStatusRequest);
        zzc(Status.xZ);
      }
    });
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzagi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */