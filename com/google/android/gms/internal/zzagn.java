package com.google.android.gms.internal;

import com.google.android.gms.wearable.Asset;
import com.google.android.gms.wearable.DataMap;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;

public final class zzagn
{
  private static int zza(String paramString, zzago.zza.zza[] paramArrayOfzza)
  {
    int m = paramArrayOfzza.length;
    int j = 0;
    int i = 14;
    if (j < m)
    {
      zzago.zza.zza localzza = paramArrayOfzza[j];
      int k;
      if (i == 14) {
        if ((localzza.type == 9) || (localzza.type == 2) || (localzza.type == 6)) {
          k = localzza.type;
        }
      }
      do
      {
        do
        {
          j += 1;
          i = k;
          break;
          k = i;
        } while (localzza.type == 14);
        i = localzza.type;
        throw new IllegalArgumentException(String.valueOf(paramString).length() + 48 + "Unexpected TypedValue type: " + i + " for key " + paramString);
        k = i;
      } while (localzza.type == i);
      j = localzza.type;
      throw new IllegalArgumentException(String.valueOf(paramString).length() + 126 + "The ArrayList elements should all be the same type, but ArrayList with key " + paramString + " contains items of type " + i + " and " + j);
    }
    return i;
  }
  
  static int zza(List<Asset> paramList, Asset paramAsset)
  {
    paramList.add(paramAsset);
    return paramList.size() - 1;
  }
  
  public static zza zza(DataMap paramDataMap)
  {
    zzago localzzago = new zzago();
    ArrayList localArrayList = new ArrayList();
    localzzago.aUN = zza(paramDataMap, localArrayList);
    return new zza(localzzago, localArrayList);
  }
  
  private static zzago.zza.zza zza(List<Asset> paramList, Object paramObject)
  {
    zzago.zza.zza localzza1 = new zzago.zza.zza();
    if (paramObject == null)
    {
      localzza1.type = 14;
      return localzza1;
    }
    localzza1.aUR = new zzago.zza.zza.zza();
    if ((paramObject instanceof String))
    {
      localzza1.type = 2;
      localzza1.aUR.aUT = ((String)paramObject);
    }
    Object localObject2;
    Object localObject1;
    int i;
    Object localObject3;
    for (;;)
    {
      return localzza1;
      if ((paramObject instanceof Integer))
      {
        localzza1.type = 6;
        localzza1.aUR.aUX = ((Integer)paramObject).intValue();
      }
      else if ((paramObject instanceof Long))
      {
        localzza1.type = 5;
        localzza1.aUR.aUW = ((Long)paramObject).longValue();
      }
      else if ((paramObject instanceof Double))
      {
        localzza1.type = 3;
        localzza1.aUR.aUU = ((Double)paramObject).doubleValue();
      }
      else if ((paramObject instanceof Float))
      {
        localzza1.type = 4;
        localzza1.aUR.aUV = ((Float)paramObject).floatValue();
      }
      else if ((paramObject instanceof Boolean))
      {
        localzza1.type = 8;
        localzza1.aUR.aUZ = ((Boolean)paramObject).booleanValue();
      }
      else if ((paramObject instanceof Byte))
      {
        localzza1.type = 7;
        localzza1.aUR.aUY = ((Byte)paramObject).byteValue();
      }
      else if ((paramObject instanceof byte[]))
      {
        localzza1.type = 1;
        localzza1.aUR.aUS = ((byte[])paramObject);
      }
      else if ((paramObject instanceof String[]))
      {
        localzza1.type = 11;
        localzza1.aUR.aVc = ((String[])paramObject);
      }
      else if ((paramObject instanceof long[]))
      {
        localzza1.type = 12;
        localzza1.aUR.aVd = ((long[])paramObject);
      }
      else if ((paramObject instanceof float[]))
      {
        localzza1.type = 15;
        localzza1.aUR.aVe = ((float[])paramObject);
      }
      else if ((paramObject instanceof Asset))
      {
        localzza1.type = 13;
        localzza1.aUR.aVf = zza(paramList, (Asset)paramObject);
      }
      else
      {
        if (!(paramObject instanceof DataMap)) {
          break;
        }
        localzza1.type = 9;
        paramObject = (DataMap)paramObject;
        localObject2 = new TreeSet(((DataMap)paramObject).keySet());
        localObject1 = new zzago.zza[((TreeSet)localObject2).size()];
        localObject2 = ((TreeSet)localObject2).iterator();
        i = 0;
        while (((Iterator)localObject2).hasNext())
        {
          localObject3 = (String)((Iterator)localObject2).next();
          localObject1[i] = new zzago.zza();
          localObject1[i].name = ((String)localObject3);
          localObject1[i].aUP = zza(paramList, ((DataMap)paramObject).get((String)localObject3));
          i += 1;
        }
        localzza1.aUR.aVa = ((zzago.zza[])localObject1);
      }
    }
    int j;
    label575:
    zzago.zza.zza localzza2;
    if ((paramObject instanceof ArrayList))
    {
      localzza1.type = 10;
      localObject2 = (ArrayList)paramObject;
      localObject3 = new zzago.zza.zza[((ArrayList)localObject2).size()];
      paramObject = null;
      int k = ((ArrayList)localObject2).size();
      j = 0;
      i = 14;
      if (j < k)
      {
        localObject1 = ((ArrayList)localObject2).get(j);
        localzza2 = zza(paramList, localObject1);
        if ((localzza2.type != 14) && (localzza2.type != 2) && (localzza2.type != 6) && (localzza2.type != 9))
        {
          paramList = String.valueOf(localObject1.getClass());
          throw new IllegalArgumentException(String.valueOf(paramList).length() + 130 + "The only ArrayList element types supported by DataBundleUtil are String, Integer, Bundle, and null, but this ArrayList contains a " + paramList);
        }
        if ((i == 14) && (localzza2.type != 14))
        {
          i = localzza2.type;
          paramObject = localObject1;
        }
      }
    }
    for (;;)
    {
      localObject3[j] = localzza2;
      j += 1;
      break label575;
      if (localzza2.type != i)
      {
        paramList = String.valueOf(paramObject.getClass());
        paramObject = String.valueOf(localObject1.getClass());
        throw new IllegalArgumentException(String.valueOf(paramList).length() + 80 + String.valueOf(paramObject).length() + "ArrayList elements must all be of the sameclass, but this one contains a " + paramList + " and a " + (String)paramObject);
        localzza1.aUR.aVb = ((zzago.zza.zza[])localObject3);
        break;
        paramList = String.valueOf(paramObject.getClass().getSimpleName());
        if (paramList.length() != 0) {}
        for (paramList = "newFieldValueFromValue: unexpected value ".concat(paramList);; paramList = new String("newFieldValueFromValue: unexpected value ")) {
          throw new RuntimeException(paramList);
        }
      }
    }
  }
  
  public static DataMap zza(zza paramzza)
  {
    DataMap localDataMap = new DataMap();
    zzago.zza[] arrayOfzza = paramzza.aUL.aUN;
    int j = arrayOfzza.length;
    int i = 0;
    while (i < j)
    {
      zzago.zza localzza = arrayOfzza[i];
      zza(paramzza.aUM, localDataMap, localzza.name, localzza.aUP);
      i += 1;
    }
    return localDataMap;
  }
  
  private static ArrayList zza(List<Asset> paramList, zzago.zza.zza.zza paramzza, int paramInt)
  {
    ArrayList localArrayList = new ArrayList(paramzza.aVb.length);
    paramzza = paramzza.aVb;
    int k = paramzza.length;
    int i = 0;
    if (i < k)
    {
      zzago.zza[] arrayOfzza = paramzza[i];
      if (arrayOfzza.type == 14) {
        localArrayList.add(null);
      }
      for (;;)
      {
        i += 1;
        break;
        if (paramInt == 9)
        {
          DataMap localDataMap = new DataMap();
          arrayOfzza = arrayOfzza.aUR.aVa;
          int m = arrayOfzza.length;
          int j = 0;
          while (j < m)
          {
            zzago.zza localzza = arrayOfzza[j];
            zza(paramList, localDataMap, localzza.name, localzza.aUP);
            j += 1;
          }
          localArrayList.add(localDataMap);
        }
        else if (paramInt == 2)
        {
          localArrayList.add(arrayOfzza.aUR.aUT);
        }
        else
        {
          if (paramInt != 6) {
            break label191;
          }
          localArrayList.add(Integer.valueOf(arrayOfzza.aUR.aUX));
        }
      }
      label191:
      throw new IllegalArgumentException(39 + "Unexpected typeOfArrayList: " + paramInt);
    }
    return localArrayList;
  }
  
  private static void zza(List<Asset> paramList, DataMap paramDataMap, String paramString, zzago.zza.zza paramzza)
  {
    int i = paramzza.type;
    if (i == 14)
    {
      paramDataMap.putString(paramString, null);
      return;
    }
    Object localObject1 = paramzza.aUR;
    if (i == 1)
    {
      paramDataMap.putByteArray(paramString, ((zzago.zza.zza.zza)localObject1).aUS);
      return;
    }
    if (i == 11)
    {
      paramDataMap.putStringArray(paramString, ((zzago.zza.zza.zza)localObject1).aVc);
      return;
    }
    if (i == 12)
    {
      paramDataMap.putLongArray(paramString, ((zzago.zza.zza.zza)localObject1).aVd);
      return;
    }
    if (i == 15)
    {
      paramDataMap.putFloatArray(paramString, ((zzago.zza.zza.zza)localObject1).aVe);
      return;
    }
    if (i == 2)
    {
      paramDataMap.putString(paramString, ((zzago.zza.zza.zza)localObject1).aUT);
      return;
    }
    if (i == 3)
    {
      paramDataMap.putDouble(paramString, ((zzago.zza.zza.zza)localObject1).aUU);
      return;
    }
    if (i == 4)
    {
      paramDataMap.putFloat(paramString, ((zzago.zza.zza.zza)localObject1).aUV);
      return;
    }
    if (i == 5)
    {
      paramDataMap.putLong(paramString, ((zzago.zza.zza.zza)localObject1).aUW);
      return;
    }
    if (i == 6)
    {
      paramDataMap.putInt(paramString, ((zzago.zza.zza.zza)localObject1).aUX);
      return;
    }
    if (i == 7)
    {
      paramDataMap.putByte(paramString, (byte)((zzago.zza.zza.zza)localObject1).aUY);
      return;
    }
    if (i == 8)
    {
      paramDataMap.putBoolean(paramString, ((zzago.zza.zza.zza)localObject1).aUZ);
      return;
    }
    if (i == 13)
    {
      if (paramList == null)
      {
        paramList = String.valueOf(paramString);
        if (paramList.length() != 0) {}
        for (paramList = "populateBundle: unexpected type for: ".concat(paramList);; paramList = new String("populateBundle: unexpected type for: ")) {
          throw new RuntimeException(paramList);
        }
      }
      paramDataMap.putAsset(paramString, (Asset)paramList.get((int)((zzago.zza.zza.zza)localObject1).aVf));
      return;
    }
    if (i == 9)
    {
      paramzza = new DataMap();
      localObject1 = ((zzago.zza.zza.zza)localObject1).aVa;
      int j = localObject1.length;
      i = 0;
      while (i < j)
      {
        Object localObject2 = localObject1[i];
        zza(paramList, paramzza, ((zzago.zza)localObject2).name, ((zzago.zza)localObject2).aUP);
        i += 1;
      }
      paramDataMap.putDataMap(paramString, paramzza);
      return;
    }
    if (i == 10)
    {
      i = zza(paramString, ((zzago.zza.zza.zza)localObject1).aVb);
      paramList = zza(paramList, (zzago.zza.zza.zza)localObject1, i);
      if (i == 14)
      {
        paramDataMap.putStringArrayList(paramString, paramList);
        return;
      }
      if (i == 9)
      {
        paramDataMap.putDataMapArrayList(paramString, paramList);
        return;
      }
      if (i == 2)
      {
        paramDataMap.putStringArrayList(paramString, paramList);
        return;
      }
      if (i == 6)
      {
        paramDataMap.putIntegerArrayList(paramString, paramList);
        return;
      }
      throw new IllegalStateException(39 + "Unexpected typeOfArrayList: " + i);
    }
    throw new RuntimeException(43 + "populateBundle: unexpected type " + i);
  }
  
  private static zzago.zza[] zza(DataMap paramDataMap, List<Asset> paramList)
  {
    Object localObject1 = new TreeSet(paramDataMap.keySet());
    zzago.zza[] arrayOfzza = new zzago.zza[((TreeSet)localObject1).size()];
    localObject1 = ((TreeSet)localObject1).iterator();
    int i = 0;
    while (((Iterator)localObject1).hasNext())
    {
      String str = (String)((Iterator)localObject1).next();
      Object localObject2 = paramDataMap.get(str);
      arrayOfzza[i] = new zzago.zza();
      arrayOfzza[i].name = str;
      arrayOfzza[i].aUP = zza(paramList, localObject2);
      i += 1;
    }
    return arrayOfzza;
  }
  
  public static class zza
  {
    public final zzago aUL;
    public final List<Asset> aUM;
    
    public zza(zzago paramzzago, List<Asset> paramList)
    {
      this.aUL = paramzzago;
      this.aUM = paramList;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzagn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */