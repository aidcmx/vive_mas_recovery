package com.google.android.gms.internal;

import java.io.IOException;
import java.util.Arrays;

public final class zzago
  extends zzaru<zzago>
{
  public zza[] aUN;
  
  public zzago()
  {
    zzcna();
  }
  
  public static zzago zzar(byte[] paramArrayOfByte)
    throws zzarz
  {
    return (zzago)zzasa.zza(new zzago(), paramArrayOfByte);
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = false;
    boolean bool1;
    if (paramObject == this) {
      bool1 = true;
    }
    do
    {
      do
      {
        do
        {
          return bool1;
          bool1 = bool2;
        } while (!(paramObject instanceof zzago));
        paramObject = (zzago)paramObject;
        bool1 = bool2;
      } while (!zzary.equals(this.aUN, ((zzago)paramObject).aUN));
      if ((this.btG != null) && (!this.btG.isEmpty())) {
        break label79;
      }
      if (((zzago)paramObject).btG == null) {
        break;
      }
      bool1 = bool2;
    } while (!((zzago)paramObject).btG.isEmpty());
    return true;
    label79:
    return this.btG.equals(((zzago)paramObject).btG);
  }
  
  public int hashCode()
  {
    int j = getClass().getName().hashCode();
    int k = zzary.hashCode(this.aUN);
    if ((this.btG == null) || (this.btG.isEmpty())) {}
    for (int i = 0;; i = this.btG.hashCode()) {
      return i + ((j + 527) * 31 + k) * 31;
    }
  }
  
  public void zza(zzart paramzzart)
    throws IOException
  {
    if ((this.aUN != null) && (this.aUN.length > 0))
    {
      int i = 0;
      while (i < this.aUN.length)
      {
        zza localzza = this.aUN[i];
        if (localzza != null) {
          paramzzart.zza(1, localzza);
        }
        i += 1;
      }
    }
    super.zza(paramzzart);
  }
  
  public zzago zzax(zzars paramzzars)
    throws IOException
  {
    for (;;)
    {
      int i = paramzzars.bU();
      switch (i)
      {
      default: 
        if (super.zza(paramzzars, i)) {}
        break;
      case 0: 
        return this;
      case 10: 
        int j = zzasd.zzc(paramzzars, 10);
        if (this.aUN == null) {}
        zza[] arrayOfzza;
        for (i = 0;; i = this.aUN.length)
        {
          arrayOfzza = new zza[j + i];
          j = i;
          if (i != 0)
          {
            System.arraycopy(this.aUN, 0, arrayOfzza, 0, i);
            j = i;
          }
          while (j < arrayOfzza.length - 1)
          {
            arrayOfzza[j] = new zza();
            paramzzars.zza(arrayOfzza[j]);
            paramzzars.bU();
            j += 1;
          }
        }
        arrayOfzza[j] = new zza();
        paramzzars.zza(arrayOfzza[j]);
        this.aUN = arrayOfzza;
      }
    }
  }
  
  public zzago zzcna()
  {
    this.aUN = zza.zzcnb();
    this.btG = null;
    this.btP = -1;
    return this;
  }
  
  protected int zzx()
  {
    int i = super.zzx();
    int k = i;
    if (this.aUN != null)
    {
      k = i;
      if (this.aUN.length > 0)
      {
        int j = 0;
        for (;;)
        {
          k = i;
          if (j >= this.aUN.length) {
            break;
          }
          zza localzza = this.aUN[j];
          k = i;
          if (localzza != null) {
            k = i + zzart.zzc(1, localzza);
          }
          j += 1;
          i = k;
        }
      }
    }
    return k;
  }
  
  public static final class zza
    extends zzaru<zza>
  {
    private static volatile zza[] aUO;
    public zza aUP;
    public String name;
    
    public zza()
    {
      zzcnc();
    }
    
    public static zza[] zzcnb()
    {
      if (aUO == null) {}
      synchronized (zzary.btO)
      {
        if (aUO == null) {
          aUO = new zza[0];
        }
        return aUO;
      }
    }
    
    public boolean equals(Object paramObject)
    {
      boolean bool2 = false;
      boolean bool1;
      if (paramObject == this) {
        bool1 = true;
      }
      label41:
      do
      {
        do
        {
          do
          {
            return bool1;
            bool1 = bool2;
          } while (!(paramObject instanceof zza));
          paramObject = (zza)paramObject;
          if (this.name != null) {
            break;
          }
          bool1 = bool2;
        } while (((zza)paramObject).name != null);
        if (this.aUP != null) {
          break label111;
        }
        bool1 = bool2;
      } while (((zza)paramObject).aUP != null);
      for (;;)
      {
        if ((this.btG == null) || (this.btG.isEmpty()))
        {
          if (((zza)paramObject).btG != null)
          {
            bool1 = bool2;
            if (!((zza)paramObject).btG.isEmpty()) {
              break;
            }
          }
          return true;
          if (this.name.equals(((zza)paramObject).name)) {
            break label41;
          }
          return false;
          label111:
          if (!this.aUP.equals(((zza)paramObject).aUP)) {
            return false;
          }
        }
      }
      return this.btG.equals(((zza)paramObject).btG);
    }
    
    public int hashCode()
    {
      int m = 0;
      int n = getClass().getName().hashCode();
      int i;
      int j;
      if (this.name == null)
      {
        i = 0;
        if (this.aUP != null) {
          break label89;
        }
        j = 0;
        label33:
        k = m;
        if (this.btG != null) {
          if (!this.btG.isEmpty()) {
            break label100;
          }
        }
      }
      label89:
      label100:
      for (int k = m;; k = this.btG.hashCode())
      {
        return (j + (i + (n + 527) * 31) * 31) * 31 + k;
        i = this.name.hashCode();
        break;
        j = this.aUP.hashCode();
        break label33;
      }
    }
    
    public void zza(zzart paramzzart)
      throws IOException
    {
      paramzzart.zzq(1, this.name);
      if (this.aUP != null) {
        paramzzart.zza(2, this.aUP);
      }
      super.zza(paramzzart);
    }
    
    public zza zzay(zzars paramzzars)
      throws IOException
    {
      for (;;)
      {
        int i = paramzzars.bU();
        switch (i)
        {
        default: 
          if (super.zza(paramzzars, i)) {}
          break;
        case 0: 
          return this;
        case 10: 
          this.name = paramzzars.readString();
          break;
        case 18: 
          if (this.aUP == null) {
            this.aUP = new zza();
          }
          paramzzars.zza(this.aUP);
        }
      }
    }
    
    public zza zzcnc()
    {
      this.name = "";
      this.aUP = null;
      this.btG = null;
      this.btP = -1;
      return this;
    }
    
    protected int zzx()
    {
      int j = super.zzx() + zzart.zzr(1, this.name);
      int i = j;
      if (this.aUP != null) {
        i = j + zzart.zzc(2, this.aUP);
      }
      return i;
    }
    
    public static final class zza
      extends zzaru<zza>
    {
      private static volatile zza[] aUQ;
      public zza aUR;
      public int type;
      
      public zza()
      {
        zzcne();
      }
      
      public static zza[] zzcnd()
      {
        if (aUQ == null) {}
        synchronized (zzary.btO)
        {
          if (aUQ == null) {
            aUQ = new zza[0];
          }
          return aUQ;
        }
      }
      
      public boolean equals(Object paramObject)
      {
        boolean bool2 = false;
        boolean bool1;
        if (paramObject == this) {
          bool1 = true;
        }
        do
        {
          do
          {
            do
            {
              return bool1;
              bool1 = bool2;
            } while (!(paramObject instanceof zza));
            paramObject = (zza)paramObject;
            bool1 = bool2;
          } while (this.type != ((zza)paramObject).type);
          if (this.aUR != null) {
            break;
          }
          bool1 = bool2;
        } while (((zza)paramObject).aUR != null);
        for (;;)
        {
          if ((this.btG == null) || (this.btG.isEmpty()))
          {
            if (((zza)paramObject).btG != null)
            {
              bool1 = bool2;
              if (!((zza)paramObject).btG.isEmpty()) {
                break;
              }
            }
            return true;
            if (!this.aUR.equals(((zza)paramObject).aUR)) {
              return false;
            }
          }
        }
        return this.btG.equals(((zza)paramObject).btG);
      }
      
      public int hashCode()
      {
        int k = 0;
        int m = getClass().getName().hashCode();
        int n = this.type;
        int i;
        if (this.aUR == null)
        {
          i = 0;
          j = k;
          if (this.btG != null) {
            if (!this.btG.isEmpty()) {
              break label84;
            }
          }
        }
        label84:
        for (int j = k;; j = this.btG.hashCode())
        {
          return (i + ((m + 527) * 31 + n) * 31) * 31 + j;
          i = this.aUR.hashCode();
          break;
        }
      }
      
      public void zza(zzart paramzzart)
        throws IOException
      {
        paramzzart.zzaf(1, this.type);
        if (this.aUR != null) {
          paramzzart.zza(2, this.aUR);
        }
        super.zza(paramzzart);
      }
      
      public zza zzaz(zzars paramzzars)
        throws IOException
      {
        for (;;)
        {
          int i = paramzzars.bU();
          switch (i)
          {
          default: 
            if (super.zza(paramzzars, i)) {}
            break;
          case 0: 
            return this;
          case 8: 
            i = paramzzars.bY();
            switch (i)
            {
            default: 
              break;
            case 1: 
            case 2: 
            case 3: 
            case 4: 
            case 5: 
            case 6: 
            case 7: 
            case 8: 
            case 9: 
            case 10: 
            case 11: 
            case 12: 
            case 13: 
            case 14: 
            case 15: 
              this.type = i;
            }
            break;
          case 18: 
            if (this.aUR == null) {
              this.aUR = new zza();
            }
            paramzzars.zza(this.aUR);
          }
        }
      }
      
      public zza zzcne()
      {
        this.type = 1;
        this.aUR = null;
        this.btG = null;
        this.btP = -1;
        return this;
      }
      
      protected int zzx()
      {
        int j = super.zzx() + zzart.zzah(1, this.type);
        int i = j;
        if (this.aUR != null) {
          i = j + zzart.zzc(2, this.aUR);
        }
        return i;
      }
      
      public static final class zza
        extends zzaru<zza>
      {
        public byte[] aUS;
        public String aUT;
        public double aUU;
        public float aUV;
        public long aUW;
        public int aUX;
        public int aUY;
        public boolean aUZ;
        public zzago.zza[] aVa;
        public zzago.zza.zza[] aVb;
        public String[] aVc;
        public long[] aVd;
        public float[] aVe;
        public long aVf;
        
        public zza()
        {
          zzcnf();
        }
        
        public boolean equals(Object paramObject)
        {
          boolean bool2 = false;
          boolean bool1;
          if (paramObject == this) {
            bool1 = true;
          }
          do
          {
            do
            {
              do
              {
                return bool1;
                bool1 = bool2;
              } while (!(paramObject instanceof zza));
              paramObject = (zza)paramObject;
              bool1 = bool2;
            } while (!Arrays.equals(this.aUS, ((zza)paramObject).aUS));
            if (this.aUT != null) {
              break;
            }
            bool1 = bool2;
          } while (((zza)paramObject).aUT != null);
          while (this.aUT.equals(((zza)paramObject).aUT))
          {
            bool1 = bool2;
            if (Double.doubleToLongBits(this.aUU) != Double.doubleToLongBits(((zza)paramObject).aUU)) {
              break;
            }
            bool1 = bool2;
            if (Float.floatToIntBits(this.aUV) != Float.floatToIntBits(((zza)paramObject).aUV)) {
              break;
            }
            bool1 = bool2;
            if (this.aUW != ((zza)paramObject).aUW) {
              break;
            }
            bool1 = bool2;
            if (this.aUX != ((zza)paramObject).aUX) {
              break;
            }
            bool1 = bool2;
            if (this.aUY != ((zza)paramObject).aUY) {
              break;
            }
            bool1 = bool2;
            if (this.aUZ != ((zza)paramObject).aUZ) {
              break;
            }
            bool1 = bool2;
            if (!zzary.equals(this.aVa, ((zza)paramObject).aVa)) {
              break;
            }
            bool1 = bool2;
            if (!zzary.equals(this.aVb, ((zza)paramObject).aVb)) {
              break;
            }
            bool1 = bool2;
            if (!zzary.equals(this.aVc, ((zza)paramObject).aVc)) {
              break;
            }
            bool1 = bool2;
            if (!zzary.equals(this.aVd, ((zza)paramObject).aVd)) {
              break;
            }
            bool1 = bool2;
            if (!zzary.equals(this.aVe, ((zza)paramObject).aVe)) {
              break;
            }
            bool1 = bool2;
            if (this.aVf != ((zza)paramObject).aVf) {
              break;
            }
            if ((this.btG != null) && (!this.btG.isEmpty())) {
              break label297;
            }
            if (((zza)paramObject).btG != null)
            {
              bool1 = bool2;
              if (!((zza)paramObject).btG.isEmpty()) {
                break;
              }
            }
            return true;
          }
          return false;
          label297:
          return this.btG.equals(((zza)paramObject).btG);
        }
        
        public int hashCode()
        {
          int m = 0;
          int n = getClass().getName().hashCode();
          int i1 = Arrays.hashCode(this.aUS);
          int i;
          int i2;
          int i3;
          int i4;
          int i5;
          int i6;
          int j;
          label100:
          int i7;
          int i8;
          int i9;
          int i10;
          int i11;
          int i12;
          if (this.aUT == null)
          {
            i = 0;
            long l = Double.doubleToLongBits(this.aUU);
            i2 = (int)(l ^ l >>> 32);
            i3 = Float.floatToIntBits(this.aUV);
            i4 = (int)(this.aUW ^ this.aUW >>> 32);
            i5 = this.aUX;
            i6 = this.aUY;
            if (!this.aUZ) {
              break label288;
            }
            j = 1231;
            i7 = zzary.hashCode(this.aVa);
            i8 = zzary.hashCode(this.aVb);
            i9 = zzary.hashCode(this.aVc);
            i10 = zzary.hashCode(this.aVd);
            i11 = zzary.hashCode(this.aVe);
            i12 = (int)(this.aVf ^ this.aVf >>> 32);
            k = m;
            if (this.btG != null) {
              if (!this.btG.isEmpty()) {
                break label295;
              }
            }
          }
          label288:
          label295:
          for (int k = m;; k = this.btG.hashCode())
          {
            return (((((((j + ((((((i + ((n + 527) * 31 + i1) * 31) * 31 + i2) * 31 + i3) * 31 + i4) * 31 + i5) * 31 + i6) * 31) * 31 + i7) * 31 + i8) * 31 + i9) * 31 + i10) * 31 + i11) * 31 + i12) * 31 + k;
            i = this.aUT.hashCode();
            break;
            j = 1237;
            break label100;
          }
        }
        
        public void zza(zzart paramzzart)
          throws IOException
        {
          int j = 0;
          if (!Arrays.equals(this.aUS, zzasd.btY)) {
            paramzzart.zzb(1, this.aUS);
          }
          if ((this.aUT != null) && (!this.aUT.equals(""))) {
            paramzzart.zzq(2, this.aUT);
          }
          if (Double.doubleToLongBits(this.aUU) != Double.doubleToLongBits(0.0D)) {
            paramzzart.zza(3, this.aUU);
          }
          if (Float.floatToIntBits(this.aUV) != Float.floatToIntBits(0.0F)) {
            paramzzart.zzc(4, this.aUV);
          }
          if (this.aUW != 0L) {
            paramzzart.zzb(5, this.aUW);
          }
          if (this.aUX != 0) {
            paramzzart.zzaf(6, this.aUX);
          }
          if (this.aUY != 0) {
            paramzzart.zzag(7, this.aUY);
          }
          if (this.aUZ) {
            paramzzart.zzg(8, this.aUZ);
          }
          int i;
          Object localObject;
          if ((this.aVa != null) && (this.aVa.length > 0))
          {
            i = 0;
            while (i < this.aVa.length)
            {
              localObject = this.aVa[i];
              if (localObject != null) {
                paramzzart.zza(9, (zzasa)localObject);
              }
              i += 1;
            }
          }
          if ((this.aVb != null) && (this.aVb.length > 0))
          {
            i = 0;
            while (i < this.aVb.length)
            {
              localObject = this.aVb[i];
              if (localObject != null) {
                paramzzart.zza(10, (zzasa)localObject);
              }
              i += 1;
            }
          }
          if ((this.aVc != null) && (this.aVc.length > 0))
          {
            i = 0;
            while (i < this.aVc.length)
            {
              localObject = this.aVc[i];
              if (localObject != null) {
                paramzzart.zzq(11, (String)localObject);
              }
              i += 1;
            }
          }
          if ((this.aVd != null) && (this.aVd.length > 0))
          {
            i = 0;
            while (i < this.aVd.length)
            {
              paramzzart.zzb(12, this.aVd[i]);
              i += 1;
            }
          }
          if (this.aVf != 0L) {
            paramzzart.zzb(13, this.aVf);
          }
          if ((this.aVe != null) && (this.aVe.length > 0))
          {
            i = j;
            while (i < this.aVe.length)
            {
              paramzzart.zzc(14, this.aVe[i]);
              i += 1;
            }
          }
          super.zza(paramzzart);
        }
        
        public zza zzba(zzars paramzzars)
          throws IOException
        {
          for (;;)
          {
            int i = paramzzars.bU();
            int j;
            Object localObject;
            int k;
            switch (i)
            {
            default: 
              if (super.zza(paramzzars, i)) {}
              break;
            case 0: 
              return this;
            case 10: 
              this.aUS = paramzzars.readBytes();
              break;
            case 18: 
              this.aUT = paramzzars.readString();
              break;
            case 25: 
              this.aUU = paramzzars.readDouble();
              break;
            case 37: 
              this.aUV = paramzzars.readFloat();
              break;
            case 40: 
              this.aUW = paramzzars.bX();
              break;
            case 48: 
              this.aUX = paramzzars.bY();
              break;
            case 56: 
              this.aUY = paramzzars.cb();
              break;
            case 64: 
              this.aUZ = paramzzars.ca();
              break;
            case 74: 
              j = zzasd.zzc(paramzzars, 74);
              if (this.aVa == null) {}
              for (i = 0;; i = this.aVa.length)
              {
                localObject = new zzago.zza[j + i];
                j = i;
                if (i != 0)
                {
                  System.arraycopy(this.aVa, 0, localObject, 0, i);
                  j = i;
                }
                while (j < localObject.length - 1)
                {
                  localObject[j] = new zzago.zza();
                  paramzzars.zza(localObject[j]);
                  paramzzars.bU();
                  j += 1;
                }
              }
              localObject[j] = new zzago.zza();
              paramzzars.zza(localObject[j]);
              this.aVa = ((zzago.zza[])localObject);
              break;
            case 82: 
              j = zzasd.zzc(paramzzars, 82);
              if (this.aVb == null) {}
              for (i = 0;; i = this.aVb.length)
              {
                localObject = new zzago.zza.zza[j + i];
                j = i;
                if (i != 0)
                {
                  System.arraycopy(this.aVb, 0, localObject, 0, i);
                  j = i;
                }
                while (j < localObject.length - 1)
                {
                  localObject[j] = new zzago.zza.zza();
                  paramzzars.zza(localObject[j]);
                  paramzzars.bU();
                  j += 1;
                }
              }
              localObject[j] = new zzago.zza.zza();
              paramzzars.zza(localObject[j]);
              this.aVb = ((zzago.zza.zza[])localObject);
              break;
            case 90: 
              j = zzasd.zzc(paramzzars, 90);
              if (this.aVc == null) {}
              for (i = 0;; i = this.aVc.length)
              {
                localObject = new String[j + i];
                j = i;
                if (i != 0)
                {
                  System.arraycopy(this.aVc, 0, localObject, 0, i);
                  j = i;
                }
                while (j < localObject.length - 1)
                {
                  localObject[j] = paramzzars.readString();
                  paramzzars.bU();
                  j += 1;
                }
              }
              localObject[j] = paramzzars.readString();
              this.aVc = ((String[])localObject);
              break;
            case 96: 
              j = zzasd.zzc(paramzzars, 96);
              if (this.aVd == null) {}
              for (i = 0;; i = this.aVd.length)
              {
                localObject = new long[j + i];
                j = i;
                if (i != 0)
                {
                  System.arraycopy(this.aVd, 0, localObject, 0, i);
                  j = i;
                }
                while (j < localObject.length - 1)
                {
                  localObject[j] = paramzzars.bX();
                  paramzzars.bU();
                  j += 1;
                }
              }
              localObject[j] = paramzzars.bX();
              this.aVd = ((long[])localObject);
              break;
            case 98: 
              k = paramzzars.zzagt(paramzzars.cd());
              i = paramzzars.getPosition();
              j = 0;
              while (paramzzars.ci() > 0)
              {
                paramzzars.bX();
                j += 1;
              }
              paramzzars.zzagv(i);
              if (this.aVd == null) {}
              for (i = 0;; i = this.aVd.length)
              {
                localObject = new long[j + i];
                j = i;
                if (i != 0)
                {
                  System.arraycopy(this.aVd, 0, localObject, 0, i);
                  j = i;
                }
                while (j < localObject.length)
                {
                  localObject[j] = paramzzars.bX();
                  j += 1;
                }
              }
              this.aVd = ((long[])localObject);
              paramzzars.zzagu(k);
              break;
            case 104: 
              this.aVf = paramzzars.bX();
              break;
            case 117: 
              j = zzasd.zzc(paramzzars, 117);
              if (this.aVe == null) {}
              for (i = 0;; i = this.aVe.length)
              {
                localObject = new float[j + i];
                j = i;
                if (i != 0)
                {
                  System.arraycopy(this.aVe, 0, localObject, 0, i);
                  j = i;
                }
                while (j < localObject.length - 1)
                {
                  localObject[j] = paramzzars.readFloat();
                  paramzzars.bU();
                  j += 1;
                }
              }
              localObject[j] = paramzzars.readFloat();
              this.aVe = ((float[])localObject);
              break;
            case 114: 
              i = paramzzars.cd();
              k = paramzzars.zzagt(i);
              j = i / 4;
              if (this.aVe == null) {}
              for (i = 0;; i = this.aVe.length)
              {
                localObject = new float[j + i];
                j = i;
                if (i != 0)
                {
                  System.arraycopy(this.aVe, 0, localObject, 0, i);
                  j = i;
                }
                while (j < localObject.length)
                {
                  localObject[j] = paramzzars.readFloat();
                  j += 1;
                }
              }
              this.aVe = ((float[])localObject);
              paramzzars.zzagu(k);
            }
          }
        }
        
        public zza zzcnf()
        {
          this.aUS = zzasd.btY;
          this.aUT = "";
          this.aUU = 0.0D;
          this.aUV = 0.0F;
          this.aUW = 0L;
          this.aUX = 0;
          this.aUY = 0;
          this.aUZ = false;
          this.aVa = zzago.zza.zzcnb();
          this.aVb = zzago.zza.zza.zzcnd();
          this.aVc = zzasd.btW;
          this.aVd = zzasd.btS;
          this.aVe = zzasd.btT;
          this.aVf = 0L;
          this.btG = null;
          this.btP = -1;
          return this;
        }
        
        protected int zzx()
        {
          int i2 = 0;
          int j = super.zzx();
          int i = j;
          if (!Arrays.equals(this.aUS, zzasd.btY)) {
            i = j + zzart.zzc(1, this.aUS);
          }
          j = i;
          if (this.aUT != null)
          {
            j = i;
            if (!this.aUT.equals("")) {
              j = i + zzart.zzr(2, this.aUT);
            }
          }
          i = j;
          if (Double.doubleToLongBits(this.aUU) != Double.doubleToLongBits(0.0D)) {
            i = j + zzart.zzb(3, this.aUU);
          }
          j = i;
          if (Float.floatToIntBits(this.aUV) != Float.floatToIntBits(0.0F)) {
            j = i + zzart.zzd(4, this.aUV);
          }
          i = j;
          if (this.aUW != 0L) {
            i = j + zzart.zzf(5, this.aUW);
          }
          j = i;
          if (this.aUX != 0) {
            j = i + zzart.zzah(6, this.aUX);
          }
          int k = j;
          if (this.aUY != 0) {
            k = j + zzart.zzai(7, this.aUY);
          }
          i = k;
          if (this.aUZ) {
            i = k + zzart.zzh(8, this.aUZ);
          }
          j = i;
          Object localObject;
          if (this.aVa != null)
          {
            j = i;
            if (this.aVa.length > 0)
            {
              j = 0;
              while (j < this.aVa.length)
              {
                localObject = this.aVa[j];
                k = i;
                if (localObject != null) {
                  k = i + zzart.zzc(9, (zzasa)localObject);
                }
                j += 1;
                i = k;
              }
              j = i;
            }
          }
          i = j;
          if (this.aVb != null)
          {
            i = j;
            if (this.aVb.length > 0)
            {
              i = j;
              j = 0;
              while (j < this.aVb.length)
              {
                localObject = this.aVb[j];
                k = i;
                if (localObject != null) {
                  k = i + zzart.zzc(10, (zzasa)localObject);
                }
                j += 1;
                i = k;
              }
            }
          }
          j = i;
          if (this.aVc != null)
          {
            j = i;
            if (this.aVc.length > 0)
            {
              j = 0;
              k = 0;
              int n;
              for (int m = 0; j < this.aVc.length; m = n)
              {
                localObject = this.aVc[j];
                int i1 = k;
                n = m;
                if (localObject != null)
                {
                  n = m + 1;
                  i1 = k + zzart.zzuy((String)localObject);
                }
                j += 1;
                k = i1;
              }
              j = i + k + m * 1;
            }
          }
          i = j;
          if (this.aVd != null)
          {
            i = j;
            if (this.aVd.length > 0)
            {
              k = 0;
              i = i2;
              while (i < this.aVd.length)
              {
                k += zzart.zzcz(this.aVd[i]);
                i += 1;
              }
              i = j + k + this.aVd.length * 1;
            }
          }
          j = i;
          if (this.aVf != 0L) {
            j = i + zzart.zzf(13, this.aVf);
          }
          i = j;
          if (this.aVe != null)
          {
            i = j;
            if (this.aVe.length > 0) {
              i = j + this.aVe.length * 4 + this.aVe.length * 1;
            }
          }
          return i;
        }
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzago.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */