package com.google.android.gms.internal;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.support.v4.util.SimpleArrayMap;

public class zzagq
  extends AnimatorListenerAdapter
{
  private SimpleArrayMap<Animator, Boolean> aVo = new SimpleArrayMap();
  
  public void onAnimationCancel(Animator paramAnimator)
  {
    this.aVo.put(paramAnimator, Boolean.valueOf(true));
  }
  
  public void onAnimationStart(Animator paramAnimator)
  {
    this.aVo.put(paramAnimator, Boolean.valueOf(false));
  }
  
  protected final boolean zzb(Animator paramAnimator)
  {
    return (this.aVo.containsKey(paramAnimator)) && (((Boolean)this.aVo.get(paramAnimator)).booleanValue());
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzagq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */