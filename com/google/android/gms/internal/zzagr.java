package com.google.android.gms.internal;

import android.animation.Animator;
import android.support.annotation.Nullable;

public class zzagr
  extends zzagq
{
  protected final Animator aVp;
  private final zzags aVq;
  private final Runnable aVr;
  private zzags.zza aVs = new zzags.zza()
  {
    public void doFrame(long paramAnonymousLong)
    {
      if ((!zzagr.this.zzb(zzagr.this.aVp)) && (!zzagr.this.aVp.isStarted()))
      {
        if (zzagr.zza(zzagr.this) != null) {
          zzagr.zza(zzagr.this).run();
        }
        zzagr.this.aVp.start();
      }
    }
  };
  
  private zzagr(Animator paramAnimator, @Nullable Runnable paramRunnable)
  {
    this.aVp = paramAnimator;
    this.aVr = paramRunnable;
    this.aVq = zzags.zzcnh();
  }
  
  public static zzagr zza(Animator paramAnimator, @Nullable Runnable paramRunnable)
  {
    paramRunnable = new zzagr(paramAnimator, paramRunnable);
    paramAnimator.addListener(paramRunnable);
    return paramRunnable;
  }
  
  public static zzagr zzc(Animator paramAnimator)
  {
    return zza(paramAnimator, null);
  }
  
  public void onAnimationEnd(Animator paramAnimator)
  {
    if (!zzb(paramAnimator)) {
      this.aVq.zza(this.aVs);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzagr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */