package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Looper;
import android.view.Choreographer;
import android.view.Choreographer.FrameCallback;

public abstract class zzags
{
  private static final ThreadLocal<zzags> aVu = new ThreadLocal()
  {
    protected zzags zzcni()
    {
      if (Build.VERSION.SDK_INT >= 16) {
        return new zzags.zzc();
      }
      Looper localLooper = Looper.myLooper();
      if (localLooper == null) {
        throw new IllegalStateException("The current thread must have a looper!");
      }
      return new zzags.zzb(localLooper);
    }
  };
  
  public static zzags zzcnh()
  {
    return (zzags)aVu.get();
  }
  
  public abstract void zza(zza paramzza);
  
  public static abstract class zza
  {
    private Runnable aVv;
    private Choreographer.FrameCallback aVw;
    
    public abstract void doFrame(long paramLong);
    
    @TargetApi(16)
    Choreographer.FrameCallback zzcnj()
    {
      if (this.aVw == null) {
        this.aVw = new Choreographer.FrameCallback()
        {
          public void doFrame(long paramAnonymousLong)
          {
            zzags.zza.this.doFrame(paramAnonymousLong);
          }
        };
      }
      return this.aVw;
    }
    
    Runnable zzcnk()
    {
      if (this.aVv == null) {
        this.aVv = new Runnable()
        {
          public void run()
          {
            zzags.zza.this.doFrame(System.nanoTime());
          }
        };
      }
      return this.aVv;
    }
  }
  
  private static class zzb
    extends zzags
  {
    private Handler handler;
    
    public zzb(Looper paramLooper)
    {
      this.handler = new Handler(paramLooper);
    }
    
    public void zza(zzags.zza paramzza)
    {
      this.handler.postDelayed(paramzza.zzcnk(), 0L);
    }
  }
  
  @TargetApi(16)
  private static class zzc
    extends zzags
  {
    private Choreographer aVy = Choreographer.getInstance();
    
    public void zza(zzags.zza paramzza)
    {
      this.aVy.postFrameCallback(paramzza.zzcnj());
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzags.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */