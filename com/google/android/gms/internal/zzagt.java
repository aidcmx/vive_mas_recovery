package com.google.android.gms.internal;

public class zzagt
{
  public static zzagw zzcnl()
  {
    return zza.zzcno();
  }
  
  public static zzagw zzcnm()
  {
    return zza.zzcnp();
  }
  
  public static zzagw zzcnn()
  {
    return zza.zzcnq();
  }
  
  private static class zza
  {
    private static final zzagw aVA = new zzagw(0.4F, 0.0F, 1.0F, 1.0F);
    private static final zzagw aVB = new zzagw(0.4F, 0.0F, 0.2F, 1.0F);
    private static final zzagw aVz = new zzagw(0.0F, 0.0F, 0.2F, 1.0F);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzagt.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */