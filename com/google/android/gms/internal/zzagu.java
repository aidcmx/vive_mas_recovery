package com.google.android.gms.internal;

public final class zzagu
{
  public static float zza(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
  {
    if ((paramFloat1 > paramFloat2) && (paramFloat1 > paramFloat3) && (paramFloat1 > paramFloat4)) {
      return paramFloat1;
    }
    if ((paramFloat2 > paramFloat3) && (paramFloat2 > paramFloat4)) {
      return paramFloat2;
    }
    if (paramFloat3 > paramFloat4) {
      return paramFloat3;
    }
    return paramFloat4;
  }
  
  public static float zza(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6)
  {
    return zza(zzb(paramFloat1, paramFloat2, paramFloat3, paramFloat4), zzb(paramFloat1, paramFloat2, paramFloat5, paramFloat4), zzb(paramFloat1, paramFloat2, paramFloat5, paramFloat6), zzb(paramFloat1, paramFloat2, paramFloat3, paramFloat6));
  }
  
  public static float zzb(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
  {
    return (float)Math.hypot(paramFloat3 - paramFloat1, paramFloat4 - paramFloat2);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzagu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */