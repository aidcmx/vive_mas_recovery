package com.google.android.gms.internal;

import android.graphics.PointF;

public abstract interface zzagv
{
  public abstract PointF zza(float paramFloat, PointF[] paramArrayOfPointF, PointF paramPointF);
  
  public static class zza
    implements zzagv
  {
    public PointF zza(float paramFloat, PointF[] paramArrayOfPointF, PointF paramPointF)
    {
      float f1 = 1.0F - paramFloat;
      float f2 = f1 * f1;
      float f3 = f2 * f1;
      float f4 = paramFloat * paramFloat;
      float f5 = f4 * paramFloat;
      float f6 = paramArrayOfPointF[0].x;
      float f7 = paramArrayOfPointF[1].x;
      float f8 = paramArrayOfPointF[2].x;
      float f9 = paramArrayOfPointF[3].x;
      float f10 = paramArrayOfPointF[0].y;
      float f11 = paramArrayOfPointF[1].y;
      paramPointF.set(f6 * f3 + 3.0F * f2 * paramFloat * f7 + 3.0F * f1 * f4 * f8 + f9 * f5, f1 * 3.0F * f4 * paramArrayOfPointF[2].y + (f2 * 3.0F * paramFloat * f11 + f3 * f10) + paramArrayOfPointF[3].y * f5);
      return paramPointF;
    }
  }
  
  public static class zzb
    implements zzagv
  {
    public PointF zza(float paramFloat, PointF[] paramArrayOfPointF, PointF paramPointF)
    {
      float f1 = 1.0F - paramFloat;
      float f2 = paramArrayOfPointF[0].x;
      float f3 = paramArrayOfPointF[1].x;
      float f4 = paramArrayOfPointF[1].x;
      float f5 = paramArrayOfPointF[2].x;
      float f6 = paramArrayOfPointF[0].y;
      float f7 = paramArrayOfPointF[1].y;
      paramPointF.set((f2 * f1 + f3 * paramFloat) * f1 + (f4 * f1 + f5 * paramFloat) * paramFloat, (f1 * paramArrayOfPointF[1].y + paramArrayOfPointF[2].y * paramFloat) * paramFloat + (f6 * f1 + f7 * paramFloat) * f1);
      return paramPointF;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzagv.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */