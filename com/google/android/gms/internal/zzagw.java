package com.google.android.gms.internal;

import android.view.animation.Interpolator;

public class zzagw
  implements Interpolator
{
  private float[] aVC;
  private float[] aVD;
  
  public zzagw(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
  {
    zzagx localzzagx = new zzagx();
    localzzagx.moveTo(0.0F, 0.0F);
    localzzagx.cubicTo(paramFloat1, paramFloat2, paramFloat3, paramFloat4, 1.0F, 1.0F);
    zza(localzzagx);
  }
  
  private void zza(zzagx paramzzagx)
  {
    int i = 0;
    paramzzagx = paramzzagx.zzj(0.002F);
    int k = paramzzagx.length / 3;
    if ((paramzzagx[1] != 0.0F) || (paramzzagx[2] != 0.0F) || (paramzzagx[(paramzzagx.length - 2)] != 1.0F) || (paramzzagx[(paramzzagx.length - 1)] != 1.0F)) {
      throw new IllegalArgumentException("The Path must start at (0,0) and end at (1,1)");
    }
    this.aVC = new float[k];
    this.aVD = new float[k];
    float f1 = 0.0F;
    float f2 = 0.0F;
    int j = 0;
    while (i < k)
    {
      int m = j + 1;
      float f4 = paramzzagx[j];
      int n = m + 1;
      float f3 = paramzzagx[m];
      j = n + 1;
      float f5 = paramzzagx[n];
      if ((f4 == f1) && (f3 != f2)) {
        throw new IllegalArgumentException("The Path cannot have discontinuity in the X axis.");
      }
      if (f3 < f2) {
        throw new IllegalArgumentException("The Path cannot loop back on itself.");
      }
      this.aVC[i] = f3;
      this.aVD[i] = f5;
      i += 1;
      f1 = f4;
      f2 = f3;
    }
  }
  
  public float getInterpolation(float paramFloat)
  {
    float f = 1.0F;
    if (paramFloat <= 0.0F) {
      f = 0.0F;
    }
    while (paramFloat >= 1.0F) {
      return f;
    }
    int j = 0;
    int i = this.aVC.length - 1;
    if (i - j > 1)
    {
      int k = (j + i) / 2;
      if (paramFloat < this.aVC[k]) {
        i = k;
      }
      for (;;)
      {
        break;
        j = k;
      }
    }
    f = this.aVC[i] - this.aVC[j];
    if (f == 0.0F) {
      return this.aVD[j];
    }
    paramFloat = (paramFloat - this.aVC[j]) / f;
    f = this.aVD[j];
    return paramFloat * (this.aVD[i] - f) + f;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzagw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */