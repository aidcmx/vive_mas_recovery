package com.google.android.gms.internal;

import android.graphics.PointF;
import java.util.ArrayList;

public class zzagx
{
  private static final zzagv.zzb aVE = new zzagv.zzb();
  private static final zzagv.zza aVF = new zzagv.zza();
  private ArrayList<zzd> HK = new ArrayList();
  
  private static void zza(PointF[] paramArrayOfPointF, zzagv paramzzagv, boolean paramBoolean, ArrayList<PointF> paramArrayList, ArrayList<Float> paramArrayList1, float paramFloat, PointF paramPointF)
  {
    Object localObject2 = new zza(null, 1.0F, paramzzagv.zza(1.0F, paramArrayOfPointF, new PointF()));
    zza localzza = new zza((zza)localObject2, 0.0F, paramzzagv.zza(0.0F, paramArrayOfPointF, new PointF()));
    float[] arrayOfFloat1 = new float[1];
    Object localObject3 = localzza;
    boolean bool1;
    label70:
    Object localObject1;
    boolean bool2;
    if (localObject2 != null)
    {
      bool1 = paramBoolean;
      localObject1 = new PointF();
      bool2 = zza(paramArrayOfPointF, paramzzagv, ((zza)localObject3).aVH, ((zza)localObject3).aVI, ((zza)localObject2).aVH, ((zza)localObject2).aVI, arrayOfFloat1, (PointF)localObject1, paramFloat);
      paramBoolean = bool2;
      if (bool2) {
        break label388;
      }
      paramBoolean = bool2;
      if (!bool1) {
        break label388;
      }
      PointF localPointF = new PointF();
      float[] arrayOfFloat2 = new float[1];
      bool2 = zza(paramArrayOfPointF, paramzzagv, ((zza)localObject3).aVH, ((zza)localObject3).aVI, arrayOfFloat1[0], (PointF)localObject1, arrayOfFloat2, localPointF, paramFloat);
      paramBoolean = bool2;
      if (!bool2) {
        break label388;
      }
    }
    for (paramBoolean = false;; paramBoolean = bool1)
    {
      if (bool2)
      {
        localObject1 = new zza((zza)localObject2, arrayOfFloat1[0], (PointF)localObject1);
        ((zza)localObject3).aVG = ((zza)localObject1);
      }
      for (;;)
      {
        localObject2 = localObject1;
        bool1 = paramBoolean;
        if (bool2) {
          break label70;
        }
        localObject2 = ((zza)localObject1).aVG;
        localObject3 = localObject1;
        break;
        if (paramArrayList.isEmpty())
        {
          paramArrayList.add(new PointF(0.0F, 0.0F));
          paramArrayList1.add(Float.valueOf(0.0F));
          paramPointF.set(0.0F, 0.0F);
        }
        paramzzagv = (PointF)paramArrayList.get(paramArrayList.size() - 1);
        paramFloat = ((Float)paramArrayList1.get(paramArrayList1.size() - 1)).floatValue();
        for (paramArrayOfPointF = localzza; paramArrayOfPointF != null; paramArrayOfPointF = paramArrayOfPointF.aVG)
        {
          paramArrayList.add(paramArrayOfPointF.aVI);
          paramFloat += PointF.length(paramArrayOfPointF.aVI.x - paramzzagv.x, paramArrayOfPointF.aVI.y - paramzzagv.y);
          paramArrayList1.add(Float.valueOf(paramFloat));
          paramzzagv = paramArrayOfPointF.aVI;
        }
        return;
        localObject1 = localObject2;
      }
      label388:
      bool2 = paramBoolean;
    }
  }
  
  static boolean zza(PointF[] paramArrayOfPointF, zzagv paramzzagv, float paramFloat1, PointF paramPointF1, float paramFloat2, PointF paramPointF2, float[] paramArrayOfFloat, PointF paramPointF3, float paramFloat3)
  {
    boolean bool = false;
    paramArrayOfFloat[0] = ((paramFloat2 + paramFloat1) / 2.0F);
    paramFloat2 = (paramPointF2.x + paramPointF1.x) / 2.0F;
    paramFloat1 = (paramPointF2.y + paramPointF1.y) / 2.0F;
    paramzzagv.zza(paramArrayOfFloat[0], paramArrayOfPointF, paramPointF3);
    paramFloat2 = paramPointF3.x - paramFloat2;
    paramFloat1 = paramPointF3.y - paramFloat1;
    if (paramFloat2 * paramFloat2 + paramFloat1 * paramFloat1 > paramFloat3) {
      bool = true;
    }
    return bool;
  }
  
  public void cubicTo(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6)
  {
    this.HK.add(new zzb(paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramFloat5, paramFloat6, false));
  }
  
  public void moveTo(float paramFloat1, float paramFloat2)
  {
    this.HK.add(new zzc(paramFloat1, paramFloat2, false));
  }
  
  float[] zzj(float paramFloat)
  {
    int k = 0;
    int j = this.HK.size();
    ArrayList localArrayList1 = new ArrayList(j + 1);
    ArrayList localArrayList2 = new ArrayList(j + 1);
    Object localObject = new PointF();
    int i = 0;
    while (i < j)
    {
      ((zzd)this.HK.get(i)).zza(localArrayList1, localArrayList2, paramFloat * paramFloat, (PointF)localObject);
      i += 1;
    }
    if (localArrayList1.isEmpty())
    {
      localArrayList1.add(new PointF(0.0F, 0.0F));
      localArrayList2.add(Float.valueOf(0.0F));
    }
    paramFloat = ((Float)localArrayList2.get(localArrayList2.size() - 1)).floatValue();
    if (paramFloat == 0.0F)
    {
      localArrayList1.add((PointF)localArrayList1.get(localArrayList1.size() - 1));
      localArrayList2.add(Float.valueOf(1.0F));
      paramFloat = 1.0F;
    }
    for (;;)
    {
      int m = localArrayList1.size();
      localObject = new float[m * 3];
      j = 0;
      i = k;
      while (i < m)
      {
        PointF localPointF = (PointF)localArrayList1.get(i);
        k = j + 1;
        localObject[j] = (((Float)localArrayList2.get(i)).floatValue() / paramFloat);
        j = k + 1;
        localObject[k] = localPointF.x;
        localObject[j] = localPointF.y;
        i += 1;
        j += 1;
      }
      return (float[])localObject;
    }
  }
  
  private static class zza
  {
    zza aVG;
    float aVH;
    PointF aVI;
    
    zza(zza paramzza, float paramFloat, PointF paramPointF)
    {
      this.aVG = paramzza;
      this.aVH = paramFloat;
      this.aVI = paramPointF;
    }
  }
  
  public static class zzb
    implements zzagx.zzd
  {
    final float aVJ;
    final float aVK;
    final float aVL;
    final float aVM;
    final float aVN;
    final float aVO;
    private final boolean aVP;
    
    public zzb(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6, boolean paramBoolean)
    {
      this.aVJ = paramFloat1;
      this.aVK = paramFloat2;
      this.aVL = paramFloat3;
      this.aVM = paramFloat4;
      this.aVN = paramFloat5;
      this.aVO = paramFloat6;
      this.aVP = paramBoolean;
    }
    
    public void zza(ArrayList<PointF> paramArrayList, ArrayList<Float> paramArrayList1, float paramFloat, PointF paramPointF)
    {
      PointF localPointF1;
      PointF localPointF2;
      float f1;
      float f2;
      float f3;
      if (paramArrayList.isEmpty())
      {
        localObject = new PointF(0.0F, 0.0F);
        if (!this.aVP) {
          break label179;
        }
        localPointF1 = new PointF(this.aVJ + ((PointF)localObject).x, this.aVK + ((PointF)localObject).y);
        localPointF2 = new PointF(this.aVL + ((PointF)localObject).x, this.aVM + ((PointF)localObject).y);
        f1 = this.aVN;
        f2 = ((PointF)localObject).x;
        f3 = this.aVO;
      }
      label179:
      for (Object localObject = { localObject, localPointF1, localPointF2, new PointF(f1 + f2, ((PointF)localObject).y + f3) };; localObject = new PointF[] { localObject, new PointF(this.aVJ, this.aVK), new PointF(this.aVL, this.aVM), new PointF(this.aVN, this.aVO) })
      {
        zzagx.zzb((PointF[])localObject, zzagx.zzcnr(), true, paramArrayList, paramArrayList1, paramFloat, paramPointF);
        return;
        localObject = (PointF)paramArrayList.get(paramArrayList.size() - 1);
        break;
      }
    }
  }
  
  public static class zzc
    implements zzagx.zzd
  {
    private final boolean aVP;
    final float x;
    final float y;
    
    public zzc(float paramFloat1, float paramFloat2, boolean paramBoolean)
    {
      this.x = paramFloat1;
      this.y = paramFloat2;
      this.aVP = paramBoolean;
    }
    
    public void zza(ArrayList<PointF> paramArrayList, ArrayList<Float> paramArrayList1, float paramFloat, PointF paramPointF)
    {
      if (!paramArrayList1.isEmpty())
      {
        paramArrayList1.add((Float)paramArrayList1.get(paramArrayList1.size() - 1));
        if ((!this.aVP) || (paramArrayList.isEmpty())) {
          break label104;
        }
        paramArrayList1 = (PointF)paramArrayList.get(paramArrayList.size() - 1);
      }
      label104:
      for (paramArrayList1 = new PointF(paramArrayList1.x + this.x, paramArrayList1.y + this.y);; paramArrayList1 = new PointF(this.x, this.y))
      {
        paramArrayList.add(paramArrayList1);
        paramPointF.set(paramArrayList1);
        return;
        paramArrayList1.add(Float.valueOf(0.0F));
        break;
      }
    }
  }
  
  public static abstract interface zzd
  {
    public abstract void zza(ArrayList<PointF> paramArrayList, ArrayList<Float> paramArrayList1, float paramFloat, PointF paramPointF);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzagx.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */