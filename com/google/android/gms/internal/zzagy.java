package com.google.android.gms.internal;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public final class zzagy
{
  @NonNull
  public static <T> T zzy(@Nullable T paramT)
  {
    if (paramT == null) {
      throw new NullPointerException();
    }
    return paramT;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzagy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */