package com.google.android.gms.internal;

import java.io.IOException;

public final class zzagz
  extends zzaru<zzagz>
{
  public String[] aVQ;
  public int[] aVR;
  public byte[][] aVS;
  
  public zzagz()
  {
    zzcns();
  }
  
  public static zzagz zzas(byte[] paramArrayOfByte)
    throws zzarz
  {
    return (zzagz)zzasa.zza(new zzagz(), paramArrayOfByte);
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = false;
    boolean bool1;
    if (paramObject == this) {
      bool1 = true;
    }
    do
    {
      do
      {
        do
        {
          do
          {
            do
            {
              return bool1;
              bool1 = bool2;
            } while (!(paramObject instanceof zzagz));
            paramObject = (zzagz)paramObject;
            bool1 = bool2;
          } while (!zzary.equals(this.aVQ, ((zzagz)paramObject).aVQ));
          bool1 = bool2;
        } while (!zzary.equals(this.aVR, ((zzagz)paramObject).aVR));
        bool1 = bool2;
      } while (!zzary.zza(this.aVS, ((zzagz)paramObject).aVS));
      if ((this.btG != null) && (!this.btG.isEmpty())) {
        break label111;
      }
      if (((zzagz)paramObject).btG == null) {
        break;
      }
      bool1 = bool2;
    } while (!((zzagz)paramObject).btG.isEmpty());
    return true;
    label111:
    return this.btG.equals(((zzagz)paramObject).btG);
  }
  
  public int hashCode()
  {
    int j = getClass().getName().hashCode();
    int k = zzary.hashCode(this.aVQ);
    int m = zzary.hashCode(this.aVR);
    int n = zzary.zzb(this.aVS);
    if ((this.btG == null) || (this.btG.isEmpty())) {}
    for (int i = 0;; i = this.btG.hashCode()) {
      return i + ((((j + 527) * 31 + k) * 31 + m) * 31 + n) * 31;
    }
  }
  
  public void zza(zzart paramzzart)
    throws IOException
  {
    int j = 0;
    int i;
    Object localObject;
    if ((this.aVQ != null) && (this.aVQ.length > 0))
    {
      i = 0;
      while (i < this.aVQ.length)
      {
        localObject = this.aVQ[i];
        if (localObject != null) {
          paramzzart.zzq(1, (String)localObject);
        }
        i += 1;
      }
    }
    if ((this.aVR != null) && (this.aVR.length > 0))
    {
      i = 0;
      while (i < this.aVR.length)
      {
        paramzzart.zzaf(2, this.aVR[i]);
        i += 1;
      }
    }
    if ((this.aVS != null) && (this.aVS.length > 0))
    {
      i = j;
      while (i < this.aVS.length)
      {
        localObject = this.aVS[i];
        if (localObject != null) {
          paramzzart.zzb(3, (byte[])localObject);
        }
        i += 1;
      }
    }
    super.zza(paramzzart);
  }
  
  public zzagz zzbb(zzars paramzzars)
    throws IOException
  {
    for (;;)
    {
      int i = paramzzars.bU();
      int j;
      Object localObject;
      switch (i)
      {
      default: 
        if (super.zza(paramzzars, i)) {}
        break;
      case 0: 
        return this;
      case 10: 
        j = zzasd.zzc(paramzzars, 10);
        if (this.aVQ == null) {}
        for (i = 0;; i = this.aVQ.length)
        {
          localObject = new String[j + i];
          j = i;
          if (i != 0)
          {
            System.arraycopy(this.aVQ, 0, localObject, 0, i);
            j = i;
          }
          while (j < localObject.length - 1)
          {
            localObject[j] = paramzzars.readString();
            paramzzars.bU();
            j += 1;
          }
        }
        localObject[j] = paramzzars.readString();
        this.aVQ = ((String[])localObject);
        break;
      case 16: 
        j = zzasd.zzc(paramzzars, 16);
        if (this.aVR == null) {}
        for (i = 0;; i = this.aVR.length)
        {
          localObject = new int[j + i];
          j = i;
          if (i != 0)
          {
            System.arraycopy(this.aVR, 0, localObject, 0, i);
            j = i;
          }
          while (j < localObject.length - 1)
          {
            localObject[j] = paramzzars.bY();
            paramzzars.bU();
            j += 1;
          }
        }
        localObject[j] = paramzzars.bY();
        this.aVR = ((int[])localObject);
        break;
      case 18: 
        int k = paramzzars.zzagt(paramzzars.cd());
        i = paramzzars.getPosition();
        j = 0;
        while (paramzzars.ci() > 0)
        {
          paramzzars.bY();
          j += 1;
        }
        paramzzars.zzagv(i);
        if (this.aVR == null) {}
        for (i = 0;; i = this.aVR.length)
        {
          localObject = new int[j + i];
          j = i;
          if (i != 0)
          {
            System.arraycopy(this.aVR, 0, localObject, 0, i);
            j = i;
          }
          while (j < localObject.length)
          {
            localObject[j] = paramzzars.bY();
            j += 1;
          }
        }
        this.aVR = ((int[])localObject);
        paramzzars.zzagu(k);
        break;
      case 26: 
        j = zzasd.zzc(paramzzars, 26);
        if (this.aVS == null) {}
        for (i = 0;; i = this.aVS.length)
        {
          localObject = new byte[j + i][];
          j = i;
          if (i != 0)
          {
            System.arraycopy(this.aVS, 0, localObject, 0, i);
            j = i;
          }
          while (j < localObject.length - 1)
          {
            localObject[j] = paramzzars.readBytes();
            paramzzars.bU();
            j += 1;
          }
        }
        localObject[j] = paramzzars.readBytes();
        this.aVS = ((byte[][])localObject);
      }
    }
  }
  
  public zzagz zzcns()
  {
    this.aVQ = zzasd.btW;
    this.aVR = zzasd.btR;
    this.aVS = zzasd.btX;
    this.btG = null;
    this.btP = -1;
    return this;
  }
  
  protected int zzx()
  {
    int i1 = 0;
    int i2 = super.zzx();
    int i;
    int k;
    Object localObject;
    int n;
    int m;
    if ((this.aVQ != null) && (this.aVQ.length > 0))
    {
      i = 0;
      j = 0;
      for (k = 0; i < this.aVQ.length; k = m)
      {
        localObject = this.aVQ[i];
        n = j;
        m = k;
        if (localObject != null)
        {
          m = k + 1;
          n = j + zzart.zzuy((String)localObject);
        }
        i += 1;
        j = n;
      }
    }
    for (int j = i2 + j + k * 1;; j = i2)
    {
      i = j;
      if (this.aVR != null)
      {
        i = j;
        if (this.aVR.length > 0)
        {
          i = 0;
          k = 0;
          while (i < this.aVR.length)
          {
            k += zzart.zzagz(this.aVR[i]);
            i += 1;
          }
          i = j + k + this.aVR.length * 1;
        }
      }
      j = i;
      if (this.aVS != null)
      {
        j = i;
        if (this.aVS.length > 0)
        {
          k = 0;
          m = 0;
          j = i1;
          while (j < this.aVS.length)
          {
            localObject = this.aVS[j];
            i1 = k;
            n = m;
            if (localObject != null)
            {
              n = m + 1;
              i1 = k + zzart.zzbg((byte[])localObject);
            }
            j += 1;
            k = i1;
            m = n;
          }
          j = i + k + m * 1;
        }
      }
      return j;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzagz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */