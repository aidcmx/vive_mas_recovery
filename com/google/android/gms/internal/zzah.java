package com.google.android.gms.internal;

public enum zzah
{
  private final String zzvp;
  
  private zzah(String paramString)
  {
    this.zzvp = paramString;
  }
  
  public String toString()
  {
    return this.zzvp;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzah.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */