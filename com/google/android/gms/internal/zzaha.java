package com.google.android.gms.internal;

public class zzaha
  implements zzaoo
{
  public boolean zza(zzaop paramzzaop)
  {
    return paramzzaop.getAnnotation(zzahk.class) != null;
  }
  
  public boolean zzh(Class<?> paramClass)
  {
    return paramClass.getAnnotation(zzahk.class) != null;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaha.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */