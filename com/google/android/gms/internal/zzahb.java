package com.google.android.gms.internal;

import android.content.Context;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.zzc;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.EmailAuthCredential;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.auth.ProviderQueryResult;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.auth.api.model.GetAccountInfoUser;
import com.google.firebase.auth.api.model.GetTokenResponse;
import com.google.firebase.auth.api.model.ProviderUserInfo;
import com.google.firebase.auth.api.model.VerifyAssertionRequest;
import com.google.firebase.zza;
import java.util.ArrayList;
import java.util.List;

public class zzahb
  extends zzc<zzahj.zza>
{
  zzahb(@NonNull Context paramContext, @NonNull zzahj.zza paramzza)
  {
    super(paramContext, zzahj.aXv, paramzza, new zza());
  }
  
  private <ResultT, CallbackT> zzg<ResultT, CallbackT> zza(zzahm<ResultT, CallbackT> paramzzahm)
  {
    return new zzg(paramzzahm);
  }
  
  @NonNull
  private static zzahv zza(@NonNull FirebaseApp paramFirebaseApp, @NonNull GetAccountInfoUser paramGetAccountInfoUser)
  {
    return zza(paramFirebaseApp, paramGetAccountInfoUser, false);
  }
  
  @NonNull
  private static zzahv zza(@NonNull FirebaseApp paramFirebaseApp, @NonNull GetAccountInfoUser paramGetAccountInfoUser, boolean paramBoolean)
  {
    zzaa.zzy(paramFirebaseApp);
    zzaa.zzy(paramGetAccountInfoUser);
    ArrayList localArrayList = new ArrayList();
    localArrayList.add(new zzaht(paramGetAccountInfoUser, "firebase"));
    paramGetAccountInfoUser = paramGetAccountInfoUser.zzcpr();
    if ((paramGetAccountInfoUser != null) && (!paramGetAccountInfoUser.isEmpty()))
    {
      int i = 0;
      while (i < paramGetAccountInfoUser.size())
      {
        localArrayList.add(new zzaht((ProviderUserInfo)paramGetAccountInfoUser.get(i)));
        i += 1;
      }
    }
    paramFirebaseApp = new zzahv(paramFirebaseApp, localArrayList);
    paramGetAccountInfoUser = (zzahv)paramFirebaseApp.zzcu(paramBoolean);
    return paramFirebaseApp;
  }
  
  @NonNull
  private Task<AuthResult> zza(@NonNull FirebaseApp paramFirebaseApp, @NonNull AuthCredential paramAuthCredential, @NonNull FirebaseUser paramFirebaseUser, @NonNull zzahq paramzzahq)
  {
    zzaa.zzy(paramFirebaseApp);
    zzaa.zzy(paramAuthCredential);
    zzaa.zzy(paramFirebaseUser);
    zzaa.zzy(paramzzahq);
    List localList = paramFirebaseUser.getProviders();
    if ((localList != null) && (localList.contains(paramAuthCredential.getProvider()))) {
      return Tasks.forException(zzahe.zzfc(new Status(17015)));
    }
    return doWrite(zza(new zzf(paramAuthCredential).zze(paramFirebaseApp).zze(paramFirebaseUser).zzbd(paramzzahq)));
  }
  
  @NonNull
  private Task<AuthResult> zza(@NonNull FirebaseApp paramFirebaseApp, @NonNull EmailAuthCredential paramEmailAuthCredential, @NonNull FirebaseUser paramFirebaseUser, @NonNull zzahq paramzzahq)
  {
    return doWrite(zza(new zze(paramEmailAuthCredential).zze(paramFirebaseApp).zze(paramFirebaseUser).zzbd(paramzzahq)));
  }
  
  @NonNull
  private Task<AuthResult> zza(@NonNull FirebaseApp paramFirebaseApp, @NonNull FirebaseUser paramFirebaseUser, @NonNull zzahq paramzzahq)
  {
    return doWrite(zza(new zzq().zze(paramFirebaseApp).zze(paramFirebaseUser).zzbd(paramzzahq)));
  }
  
  @NonNull
  private Task<AuthResult> zza(@NonNull FirebaseApp paramFirebaseApp, @NonNull String paramString, @NonNull FirebaseUser paramFirebaseUser, @NonNull zzahq paramzzahq)
  {
    return doWrite(zza(new zzr(paramString).zze(paramFirebaseApp).zze(paramFirebaseUser).zzbd(paramzzahq)));
  }
  
  public Task<AuthResult> zza(@NonNull FirebaseApp paramFirebaseApp, @NonNull zzahq paramzzahq)
  {
    return doWrite(zza(new zzm().zze(paramFirebaseApp).zzbd(paramzzahq)));
  }
  
  public Task<AuthResult> zza(@NonNull FirebaseApp paramFirebaseApp, @NonNull AuthCredential paramAuthCredential, @NonNull zzahq paramzzahq)
  {
    return doWrite(zza(new zzn(paramAuthCredential).zze(paramFirebaseApp).zzbd(paramzzahq)));
  }
  
  public Task<Void> zza(@NonNull FirebaseApp paramFirebaseApp, @NonNull FirebaseUser paramFirebaseUser, @NonNull AuthCredential paramAuthCredential, @NonNull zzahq paramzzahq)
  {
    return doWrite(zza(new zzh(paramAuthCredential).zze(paramFirebaseApp).zze(paramFirebaseUser).zzbd(paramzzahq)));
  }
  
  public Task<Void> zza(@NonNull FirebaseApp paramFirebaseApp, @NonNull FirebaseUser paramFirebaseUser, @NonNull UserProfileChangeRequest paramUserProfileChangeRequest, @NonNull zzahq paramzzahq)
  {
    return doWrite(zza(new zzu(paramUserProfileChangeRequest).zze(paramFirebaseApp).zze(paramFirebaseUser).zzbd(paramzzahq)));
  }
  
  public Task<GetTokenResult> zza(@NonNull FirebaseApp paramFirebaseApp, @NonNull FirebaseUser paramFirebaseUser, @NonNull String paramString, @NonNull zzahq paramzzahq)
  {
    return doRead(zza(new zzd(paramString).zze(paramFirebaseApp).zze(paramFirebaseUser).zzbd(paramzzahq)));
  }
  
  public Task<Void> zza(@NonNull FirebaseApp paramFirebaseApp, @NonNull FirebaseUser paramFirebaseUser, @NonNull String paramString1, @NonNull String paramString2, @NonNull zzahq paramzzahq)
  {
    return doWrite(zza(new zzi(paramString1, paramString2).zze(paramFirebaseApp).zze(paramFirebaseUser).zzbd(paramzzahq)));
  }
  
  public Task<ProviderQueryResult> zza(@NonNull FirebaseApp paramFirebaseApp, @NonNull String paramString)
  {
    return doRead(zza(new zzc(paramString).zze(paramFirebaseApp)));
  }
  
  public Task<AuthResult> zza(@NonNull FirebaseApp paramFirebaseApp, @NonNull String paramString, @NonNull zzahq paramzzahq)
  {
    return doWrite(zza(new zzo(paramString).zze(paramFirebaseApp).zzbd(paramzzahq)));
  }
  
  public Task<AuthResult> zza(@NonNull FirebaseApp paramFirebaseApp, @NonNull String paramString1, @NonNull String paramString2, @NonNull zzahq paramzzahq)
  {
    return doWrite(zza(new zza(paramString1, paramString2).zze(paramFirebaseApp).zzbd(paramzzahq)));
  }
  
  @NonNull
  public Task<Void> zza(@NonNull FirebaseUser paramFirebaseUser, @NonNull zzahy paramzzahy)
  {
    return doWrite(zza(new zzb().zze(paramFirebaseUser).zzbd(paramzzahy)));
  }
  
  @NonNull
  public Task<Void> zzb(@NonNull FirebaseApp paramFirebaseApp, @NonNull FirebaseUser paramFirebaseUser, @NonNull zzahq paramzzahq)
  {
    return doRead(zza(new zzj().zze(paramFirebaseApp).zze(paramFirebaseUser).zzbd(paramzzahq)));
  }
  
  public Task<AuthResult> zzb(@NonNull FirebaseApp paramFirebaseApp, @NonNull FirebaseUser paramFirebaseUser, @NonNull AuthCredential paramAuthCredential, @NonNull zzahq paramzzahq)
  {
    zzaa.zzy(paramFirebaseApp);
    zzaa.zzy(paramAuthCredential);
    zzaa.zzy(paramFirebaseUser);
    zzaa.zzy(paramzzahq);
    if (EmailAuthCredential.class.isAssignableFrom(paramAuthCredential.getClass())) {
      return zza(paramFirebaseApp, (EmailAuthCredential)paramAuthCredential, paramFirebaseUser, paramzzahq);
    }
    return zza(paramFirebaseApp, paramAuthCredential, paramFirebaseUser, paramzzahq);
  }
  
  public Task<Void> zzb(@NonNull FirebaseApp paramFirebaseApp, @NonNull FirebaseUser paramFirebaseUser, @NonNull String paramString, @NonNull zzahq paramzzahq)
  {
    return doWrite(zza(new zzs(paramString).zze(paramFirebaseApp).zze(paramFirebaseUser).zzbd(paramzzahq)));
  }
  
  public Task<Void> zzb(@NonNull FirebaseApp paramFirebaseApp, @NonNull String paramString)
  {
    return doWrite(zza(new zzl(paramString).zze(paramFirebaseApp)));
  }
  
  public Task<AuthResult> zzb(@NonNull FirebaseApp paramFirebaseApp, @NonNull String paramString1, @NonNull String paramString2, @NonNull zzahq paramzzahq)
  {
    return doWrite(zza(new zzp(paramString1, paramString2).zze(paramFirebaseApp).zzbd(paramzzahq)));
  }
  
  public Task<Void> zzc(@NonNull FirebaseApp paramFirebaseApp, @NonNull FirebaseUser paramFirebaseUser, @NonNull String paramString, @NonNull zzahq paramzzahq)
  {
    return doWrite(zza(new zzt(paramString).zze(paramFirebaseApp).zze(paramFirebaseUser).zzbd(paramzzahq)));
  }
  
  public Task<Void> zzc(@NonNull FirebaseApp paramFirebaseApp, @NonNull String paramString)
  {
    return doWrite(zza(new zzk(paramString).zze(paramFirebaseApp)));
  }
  
  public Task<AuthResult> zzd(@NonNull FirebaseApp paramFirebaseApp, @NonNull FirebaseUser paramFirebaseUser, @NonNull String paramString, @NonNull zzahq paramzzahq)
  {
    zzaa.zzy(paramFirebaseApp);
    zzaa.zzib(paramString);
    zzaa.zzy(paramFirebaseUser);
    zzaa.zzy(paramzzahq);
    List localList = paramFirebaseUser.getProviders();
    if (((localList != null) && (!localList.contains(paramString))) || (paramFirebaseUser.isAnonymous())) {
      return Tasks.forException(zzahe.zzfc(new Status(17016, paramString)));
    }
    int i = -1;
    switch (paramString.hashCode())
    {
    }
    for (;;)
    {
      switch (i)
      {
      default: 
        return zza(paramFirebaseApp, paramString, paramFirebaseUser, paramzzahq);
        if (paramString.equals("password")) {
          i = 0;
        }
        break;
      }
    }
    return zza(paramFirebaseApp, paramFirebaseUser, paramzzahq);
  }
  
  static final class zza
    extends zzahm<AuthResult, zzahq>
  {
    @NonNull
    private String io;
    @NonNull
    private String jg;
    
    public zza(@NonNull String paramString1, @NonNull String paramString2)
    {
      super();
      this.jg = zzaa.zzh(paramString1, "email cannot be null or empty");
      this.io = zzaa.zzh(paramString2, "password cannot be null or empty");
    }
    
    public void dispatch()
      throws RemoteException
    {
      this.aXz.zzc(this.jg, this.io, this.aXx);
    }
    
    public void zzcpf()
    {
      zzahv localzzahv = zzahb.zzb(this.aWU, this.aXD);
      ((zzahq)this.aXA).zza(this.aXC, localzzahv);
      zzbe(new zzahs(localzzahv));
    }
  }
  
  static final class zzb
    extends zzahm<Void, zzahy>
  {
    public zzb()
    {
      super();
    }
    
    public void dispatch()
      throws RemoteException
    {
      this.aXz.zzg(this.aXy.zzcoy(), this.aXx);
    }
    
    public void zzcpf()
    {
      ((zzahy)this.aXA).zzcov();
      zzbe(null);
    }
  }
  
  static final class zzc
    extends zzahm<ProviderQueryResult, zzahq>
  {
    @NonNull
    private final String jg;
    
    public zzc(@NonNull String paramString)
    {
      super();
      this.jg = zzaa.zzh(paramString, "email cannot be null or empty");
    }
    
    public void dispatch()
      throws RemoteException
    {
      this.aXz.zzc(this.jg, this.aXx);
    }
    
    public void zzcpf()
    {
      zzbe(new zzahw(this.aXE));
    }
  }
  
  static final class zzd
    extends zzahm<GetTokenResult, zzahq>
  {
    @NonNull
    private final String aXk;
    
    public zzd(@NonNull String paramString)
    {
      super();
      this.aXk = zzaa.zzh(paramString, "refresh token cannot be null");
    }
    
    public void dispatch()
      throws RemoteException
    {
      this.aXz.zza(this.aXk, this.aXx);
    }
    
    public void zzcpf()
    {
      this.aXC.zzrv(this.aXk);
      ((zzahq)this.aXA).zza(this.aXC, this.aXy);
      zzbe(new GetTokenResult(this.aXC.getAccessToken()));
    }
  }
  
  static final class zze
    extends zzahm<AuthResult, zzahq>
  {
    @NonNull
    private final EmailAuthCredential aXl;
    
    public zze(@NonNull EmailAuthCredential paramEmailAuthCredential)
    {
      super();
      this.aXl = ((EmailAuthCredential)zzaa.zzb(paramEmailAuthCredential, "credential cannot be null"));
    }
    
    public void dispatch()
      throws RemoteException
    {
      this.aXz.zza(this.aXl.getEmail(), this.aXl.getPassword(), this.aXy.zzcoy(), this.aXx);
    }
    
    public void zzcpf()
    {
      zzahv localzzahv = zzahb.zzb(this.aWU, this.aXD);
      ((zzahq)this.aXA).zza(this.aXC, localzzahv);
      zzbe(new zzahs(localzzahv));
    }
  }
  
  static final class zzf
    extends zzahm<AuthResult, zzahq>
  {
    @NonNull
    private final VerifyAssertionRequest aXm;
    
    public zzf(@NonNull AuthCredential paramAuthCredential)
    {
      super();
      zzaa.zzb(paramAuthCredential, "credential cannot be null");
      this.aXm = zzahr.zza(paramAuthCredential);
    }
    
    public void dispatch()
      throws RemoteException
    {
      this.aXz.zza(this.aXy.zzcoy(), this.aXm, this.aXx);
    }
    
    public void zzcpf()
    {
      zzahv localzzahv = zzahb.zzb(this.aWU, this.aXD);
      ((zzahq)this.aXA).zza(this.aXC, localzzahv);
      zzbe(new zzahs(localzzahv));
    }
  }
  
  private class zzg<ResultT, CallbackT>
    extends zzse<zzahc, ResultT>
    implements zzahl<ResultT>
  {
    private zzahm<ResultT, CallbackT> aXn;
    private TaskCompletionSource<ResultT> yg;
    
    public zzg()
    {
      zzahm localzzahm;
      this.aXn = localzzahm;
      this.aXn.zza(this);
    }
    
    protected void zza(zzahc paramzzahc, TaskCompletionSource<ResultT> paramTaskCompletionSource)
      throws RemoteException
    {
      this.yg = paramTaskCompletionSource;
      this.aXn.zza(paramzzahc.zzcpg());
    }
    
    public final void zza(ResultT paramResultT, Status paramStatus)
    {
      zzaa.zzb(this.yg, "doExecute must be called before onComplete");
      if (paramStatus != null)
      {
        this.yg.setException(zzahe.zzfc(paramStatus));
        return;
      }
      this.yg.setResult(paramResultT);
    }
  }
  
  static final class zzh
    extends zzahm<Void, zzahq>
  {
    @NonNull
    private final VerifyAssertionRequest aXm;
    
    public zzh(@NonNull AuthCredential paramAuthCredential)
    {
      super();
      zzaa.zzb(paramAuthCredential, "credential cannot be null");
      this.aXm = zzahr.zza(paramAuthCredential);
    }
    
    public void dispatch()
      throws RemoteException
    {
      this.aXz.zza(this.aXm, this.aXx);
    }
    
    public void zzcpf()
    {
      zzahv localzzahv = zzahb.zzb(this.aWU, this.aXD);
      if (this.aXy.getUid().equalsIgnoreCase(localzzahv.getUid()))
      {
        ((zzahq)this.aXA).zza(this.aXC, localzzahv);
        zzcpm();
        return;
      }
      zzfd(zzahx.zzcqh());
    }
  }
  
  static final class zzi
    extends zzahm<Void, zzahq>
  {
    @NonNull
    private final String io;
    @NonNull
    private final String jg;
    
    public zzi(@NonNull String paramString1, @NonNull String paramString2)
    {
      super();
      this.jg = zzaa.zzh(paramString1, "email cannot be null or empty");
      this.io = zzaa.zzh(paramString2, "password cannot be null or empty");
    }
    
    public void dispatch()
      throws RemoteException
    {
      this.aXz.zzd(this.jg, this.io, this.aXx);
    }
    
    public void zzcpf()
    {
      zzahv localzzahv = zzahb.zzb(this.aWU, this.aXD);
      if (this.aXy.getUid().equalsIgnoreCase(localzzahv.getUid()))
      {
        ((zzahq)this.aXA).zza(this.aXC, localzzahv);
        zzcpm();
        return;
      }
      zzfd(zzahx.zzcqh());
    }
  }
  
  static final class zzj
    extends zzahm<Void, zzahq>
  {
    public zzj()
    {
      super();
    }
    
    public void dispatch()
      throws RemoteException
    {
      this.aXz.zzf(this.aXy.zzcoy(), this.aXx);
    }
    
    public void zzcpf()
    {
      zzahv localzzahv = zzahb.zzb(this.aWU, this.aXD, this.aXy.isAnonymous());
      ((zzahq)this.aXA).zza(this.aXC, localzzahv);
      zzbe(null);
    }
  }
  
  static final class zzk
    extends zzahm<Void, zzahq>
  {
    @NonNull
    private String hN;
    
    public zzk(@NonNull String paramString)
    {
      super();
      this.hN = zzaa.zzh(paramString, "token cannot be null or empty");
    }
    
    public void dispatch()
      throws RemoteException
    {
      this.aXz.zzh(this.hN, this.aXx);
    }
    
    public void zzcpf()
    {
      zzcpm();
    }
  }
  
  static final class zzl
    extends zzahm<Void, zzahq>
  {
    @NonNull
    private String jg;
    
    public zzl(@NonNull String paramString)
    {
      super();
      this.jg = zzaa.zzh(paramString, "email cannot be null or empty");
    }
    
    public void dispatch()
      throws RemoteException
    {
      this.aXz.zzd(this.jg, this.aXx);
    }
    
    public void zzcpf()
    {
      zzcpm();
    }
  }
  
  static final class zzm
    extends zzahm<AuthResult, zzahq>
  {
    public zzm()
    {
      super();
    }
    
    public void dispatch()
      throws RemoteException
    {
      this.aXz.zza(this.aXx);
    }
    
    public void zzcpf()
    {
      zzahv localzzahv = zzahb.zzb(this.aWU, this.aXD, true);
      ((zzahq)this.aXA).zza(this.aXC, localzzahv);
      zzbe(new zzahs(localzzahv));
    }
  }
  
  static final class zzn
    extends zzahm<AuthResult, zzahq>
  {
    @NonNull
    private final VerifyAssertionRequest aXm;
    
    public zzn(@NonNull AuthCredential paramAuthCredential)
    {
      super();
      zzaa.zzb(paramAuthCredential, "credential cannot be null");
      this.aXm = zzahr.zza(paramAuthCredential);
    }
    
    public void dispatch()
      throws RemoteException
    {
      this.aXz.zza(this.aXm, this.aXx);
    }
    
    public void zzcpf()
    {
      zzahv localzzahv = zzahb.zzb(this.aWU, this.aXD);
      ((zzahq)this.aXA).zza(this.aXC, localzzahv);
      zzbe(new zzahs(localzzahv));
    }
  }
  
  static final class zzo
    extends zzahm<AuthResult, zzahq>
  {
    @NonNull
    private final String hN;
    
    public zzo(@NonNull String paramString)
    {
      super();
      this.hN = zzaa.zzh(paramString, "token cannot be null or empty");
    }
    
    public void dispatch()
      throws RemoteException
    {
      this.aXz.zzb(this.hN, this.aXx);
    }
    
    public void zzcpf()
    {
      zzahv localzzahv = zzahb.zzb(this.aWU, this.aXD);
      ((zzahq)this.aXA).zza(this.aXC, localzzahv);
      zzbe(new zzahs(localzzahv));
    }
  }
  
  static final class zzp
    extends zzahm<AuthResult, zzahq>
  {
    @NonNull
    private String io;
    @NonNull
    private String jg;
    
    public zzp(String paramString1, String paramString2)
    {
      super();
      this.jg = zzaa.zzh(paramString1, "email cannot be null or empty");
      this.io = zzaa.zzh(paramString2, "password cannot be null or empty");
    }
    
    public void dispatch()
      throws RemoteException
    {
      this.aXz.zzd(this.jg, this.io, this.aXx);
    }
    
    public void zzcpf()
    {
      zzahv localzzahv = zzahb.zzb(this.aWU, this.aXD);
      ((zzahq)this.aXA).zza(this.aXC, localzzahv);
      zzbe(new zzahs(localzzahv));
    }
  }
  
  static final class zzq
    extends zzahm<AuthResult, zzahq>
  {
    public zzq()
    {
      super();
    }
    
    public void dispatch()
      throws RemoteException
    {
      this.aXz.zze(this.aXy.zzcoy(), this.aXx);
    }
    
    public void zzcpf()
    {
      zzahv localzzahv = zzahb.zzb(this.aWU, this.aXD);
      ((zzahq)this.aXA).zza(this.aXC, localzzahv);
      zzbe(new zzahs(localzzahv));
    }
  }
  
  static final class zzr
    extends zzahm<AuthResult, zzahq>
  {
    @NonNull
    private String aXp;
    
    public zzr(@NonNull String paramString)
    {
      super();
      this.aXp = zzaa.zzh(paramString, "provider cannot be null or empty");
    }
    
    public void dispatch()
      throws RemoteException
    {
      this.aXz.zze(this.aXp, this.aXy.zzcoy(), this.aXx);
    }
    
    public void zzcpf()
    {
      zzahv localzzahv = zzahb.zzb(this.aWU, this.aXD);
      ((zzahq)this.aXA).zza(this.aXC, localzzahv);
      zzbe(new zzahs(localzzahv));
    }
  }
  
  static final class zzs
    extends zzahm<Void, zzahq>
  {
    @NonNull
    private final String jg;
    
    public zzs(String paramString)
    {
      super();
      this.jg = zzaa.zzh(paramString, "email cannot be null or empty");
    }
    
    public void dispatch()
      throws RemoteException
    {
      this.aXz.zza(this.aXy.zzcoy(), this.jg, this.aXx);
    }
    
    public void zzcpf()
    {
      ((zzahq)this.aXA).zza(this.aXC, zzahb.zzb(this.aWU, this.aXD));
      zzcpm();
    }
  }
  
  static final class zzt
    extends zzahm<Void, zzahq>
  {
    @NonNull
    private final String io;
    
    public zzt(@NonNull String paramString)
    {
      super();
      this.io = zzaa.zzh(paramString, "password cannot be null or empty");
    }
    
    public void dispatch()
      throws RemoteException
    {
      this.aXz.zzb(this.aXy.zzcoy(), this.io, this.aXx);
    }
    
    public void zzcpf()
    {
      ((zzahq)this.aXA).zza(this.aXC, zzahb.zzb(this.aWU, this.aXD));
      zzcpm();
    }
  }
  
  static final class zzu
    extends zzahm<Void, zzahq>
  {
    @NonNull
    private final UserProfileChangeRequest aXq;
    
    public zzu(UserProfileChangeRequest paramUserProfileChangeRequest)
    {
      super();
      this.aXq = ((UserProfileChangeRequest)zzaa.zzb(paramUserProfileChangeRequest, "request cannot be null"));
    }
    
    public void dispatch()
      throws RemoteException
    {
      this.aXz.zza(this.aXy.zzcoy(), this.aXq, this.aXx);
    }
    
    public void zzcpf()
    {
      ((zzahq)this.aXA).zza(this.aXC, zzahb.zzb(this.aWU, this.aXD));
      zzcpm();
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzahb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */