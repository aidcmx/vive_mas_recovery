package com.google.android.gms.internal;

import android.os.DeadObjectException;
import com.google.android.gms.common.api.Api.zze;

public abstract interface zzahc
  extends Api.zze
{
  public abstract zzahi zzcpg()
    throws DeadObjectException;
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzahc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */