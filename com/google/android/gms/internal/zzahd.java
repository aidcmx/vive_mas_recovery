package com.google.android.gms.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.text.TextUtils;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzj;

public class zzahd
  extends zzj<zzahi>
  implements zzahc
{
  private static zzsu hF = new zzsu("FirebaseAuth", new String[] { "FirebaseAuth:" });
  private final zzahj.zza aXr;
  private final Context mContext;
  
  public zzahd(Context paramContext, Looper paramLooper, zzf paramzzf, zzahj.zza paramzza, GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
  {
    super(paramContext, paramLooper, 112, paramzzf, paramConnectionCallbacks, paramOnConnectionFailedListener);
    this.mContext = ((Context)zzaa.zzy(paramContext));
    this.aXr = paramzza;
  }
  
  protected Bundle zzahv()
  {
    Bundle localBundle2 = super.zzahv();
    Bundle localBundle1 = localBundle2;
    if (localBundle2 == null) {
      localBundle1 = new Bundle();
    }
    if (this.aXr != null) {
      localBundle1.putString("com.google.firebase.auth.API_KEY", this.aXr.getApiKey());
    }
    return localBundle1;
  }
  
  public boolean zzaqx()
  {
    return zztl.zzaa(this.mContext, "com.google.firebase.auth") == 0;
  }
  
  protected String zzauz()
  {
    String str2 = zzahp.getProperty("firebear.preference");
    String str1 = str2;
    if (TextUtils.isEmpty(str2)) {
      str1 = "default";
    }
    switch (str1.hashCode())
    {
    default: 
      label48:
      i = -1;
      switch (i)
      {
      default: 
        label50:
        str1 = "default";
      }
      switch (str1.hashCode())
      {
      }
      break;
    }
    label96:
    for (int i = -1;; i = 0) {
      switch (i)
      {
      default: 
        hF.zza("Loading module via default loading order.", new Object[0]);
        i = zztl.zzaa(this.mContext, "com.google.firebase.auth");
        if (zztl.zzab(this.mContext, "com.google.android.gms.firebase_auth") < i) {
          break label228;
        }
        hF.zza("Loading remote module.", new Object[0]);
        return "com.google.android.gms";
        if (!str1.equals("local")) {
          break label48;
        }
        i = 0;
        break label50;
        if (!str1.equals("default")) {
          break label48;
        }
        i = 1;
        break label50;
        if (!str1.equals("local")) {
          break label96;
        }
      }
    }
    hF.zza("Loading fallback module override.", new Object[0]);
    return this.mContext.getPackageName();
    label228:
    hF.zza("Loading fallback module.", new Object[0]);
    return this.mContext.getPackageName();
  }
  
  protected String zzjx()
  {
    return "com.google.firebase.auth.api.gms.service.START";
  }
  
  protected String zzjy()
  {
    return "com.google.firebase.auth.api.internal.IFirebaseAuthService";
  }
  
  protected zzahi zzma(IBinder paramIBinder)
  {
    return zzahi.zza.zzmc(paramIBinder);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzahd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */