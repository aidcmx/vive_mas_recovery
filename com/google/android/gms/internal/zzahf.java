package com.google.android.gms.internal;

import android.support.annotation.NonNull;
import com.google.android.gms.common.internal.zzaa;
import com.google.firebase.auth.api.model.GetAccountInfoUser;
import com.google.firebase.auth.api.model.GetAccountInfoUserList;
import java.io.IOException;
import java.util.List;

public class zzahf
  extends zzapk<GetAccountInfoUserList>
{
  private zzaos aXt;
  
  public GetAccountInfoUserList zza(zzaqp paramzzaqp)
    throws IOException
  {
    if (paramzzaqp.bq() == zzaqq.brJ)
    {
      paramzzaqp.nextNull();
      return null;
    }
    GetAccountInfoUserList localGetAccountInfoUserList = new GetAccountInfoUserList();
    zzapk localzzapk = this.aXt.zzk(GetAccountInfoUser.class);
    paramzzaqp.beginArray();
    while (paramzzaqp.hasNext())
    {
      GetAccountInfoUser localGetAccountInfoUser = (GetAccountInfoUser)localzzapk.zzb(paramzzaqp);
      localGetAccountInfoUserList.zzcpt().add(localGetAccountInfoUser);
    }
    paramzzaqp.endArray();
    return localGetAccountInfoUserList;
  }
  
  public void zza(@NonNull zzaos paramzzaos)
  {
    this.aXt = ((zzaos)zzaa.zzy(paramzzaos));
  }
  
  public void zza(zzaqr paramzzaqr, GetAccountInfoUserList paramGetAccountInfoUserList)
    throws IOException
  {
    int j = 0;
    if (paramGetAccountInfoUserList == null)
    {
      paramzzaqr.bA();
      return;
    }
    zzapk localzzapk = this.aXt.zzk(GetAccountInfoUser.class);
    paramzzaqr.bw();
    paramGetAccountInfoUserList = paramGetAccountInfoUserList.zzcpt();
    int i;
    if (paramGetAccountInfoUserList != null) {
      i = paramGetAccountInfoUserList.size();
    }
    while (j < i)
    {
      localzzapk.zza(paramzzaqr, (GetAccountInfoUser)paramGetAccountInfoUserList.get(j));
      j += 1;
      continue;
      i = 0;
    }
    paramzzaqr.bx();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzahf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */