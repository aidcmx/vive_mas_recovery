package com.google.android.gms.internal;

import com.google.firebase.auth.api.model.GetAccountInfoUserList;
import com.google.firebase.auth.api.model.ProviderUserInfoList;
import com.google.firebase.auth.api.model.StringList;

public class zzahg
{
  public static zzaos zzcph()
  {
    zzahf localzzahf = new zzahf();
    zzaho localzzaho = new zzaho();
    zzahn localzzahn = new zzahn();
    zzaos localzzaos = new zzaot().zzf(new int[] { 8, 128, 64 }).aR().zza(new zzaoo[] { new zzaha() }).zza(GetAccountInfoUserList.class, localzzahf).zza(StringList.class, localzzaho).zza(ProviderUserInfoList.class, localzzahn).aS();
    localzzahf.zza(localzzaos);
    localzzaho.zza(localzzaos);
    localzzahn.zza(localzzaos);
    return localzzaos;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzahg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */