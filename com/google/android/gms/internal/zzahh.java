package com.google.android.gms.internal;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.firebase.auth.api.model.CreateAuthUriResponse;
import com.google.firebase.auth.api.model.GetAccountInfoUser;
import com.google.firebase.auth.api.model.GetTokenResponse;

public abstract interface zzahh
  extends IInterface
{
  public abstract void onFailure(Status paramStatus)
    throws RemoteException;
  
  public abstract void zza(CreateAuthUriResponse paramCreateAuthUriResponse)
    throws RemoteException;
  
  public abstract void zza(GetTokenResponse paramGetTokenResponse, GetAccountInfoUser paramGetAccountInfoUser)
    throws RemoteException;
  
  public abstract void zzb(GetTokenResponse paramGetTokenResponse)
    throws RemoteException;
  
  public abstract void zzcpi()
    throws RemoteException;
  
  public abstract void zzcpj()
    throws RemoteException;
  
  public abstract void zzcpk()
    throws RemoteException;
  
  public static abstract class zza
    extends Binder
    implements zzahh
  {
    public zza()
    {
      attachInterface(this, "com.google.firebase.auth.api.internal.IFirebaseAuthCallbacks");
    }
    
    public static zzahh zzmb(IBinder paramIBinder)
    {
      if (paramIBinder == null) {
        return null;
      }
      IInterface localIInterface = paramIBinder.queryLocalInterface("com.google.firebase.auth.api.internal.IFirebaseAuthCallbacks");
      if ((localIInterface != null) && ((localIInterface instanceof zzahh))) {
        return (zzahh)localIInterface;
      }
      return new zza(paramIBinder);
    }
    
    public IBinder asBinder()
    {
      return this;
    }
    
    public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
      throws RemoteException
    {
      switch (paramInt1)
      {
      default: 
        return super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
      case 1598968902: 
        paramParcel2.writeString("com.google.firebase.auth.api.internal.IFirebaseAuthCallbacks");
        return true;
      case 1: 
        paramParcel1.enforceInterface("com.google.firebase.auth.api.internal.IFirebaseAuthCallbacks");
        if (paramParcel1.readInt() != 0) {}
        for (paramParcel1 = (GetTokenResponse)GetTokenResponse.CREATOR.createFromParcel(paramParcel1);; paramParcel1 = null)
        {
          zzb(paramParcel1);
          return true;
        }
      case 2: 
        paramParcel1.enforceInterface("com.google.firebase.auth.api.internal.IFirebaseAuthCallbacks");
        if (paramParcel1.readInt() != 0)
        {
          paramParcel2 = (GetTokenResponse)GetTokenResponse.CREATOR.createFromParcel(paramParcel1);
          if (paramParcel1.readInt() == 0) {
            break label191;
          }
        }
        for (paramParcel1 = (GetAccountInfoUser)GetAccountInfoUser.CREATOR.createFromParcel(paramParcel1);; paramParcel1 = null)
        {
          zza(paramParcel2, paramParcel1);
          return true;
          paramParcel2 = null;
          break;
        }
      case 3: 
        paramParcel1.enforceInterface("com.google.firebase.auth.api.internal.IFirebaseAuthCallbacks");
        if (paramParcel1.readInt() != 0) {}
        for (paramParcel1 = (CreateAuthUriResponse)CreateAuthUriResponse.CREATOR.createFromParcel(paramParcel1);; paramParcel1 = null)
        {
          zza(paramParcel1);
          return true;
        }
      case 4: 
        paramParcel1.enforceInterface("com.google.firebase.auth.api.internal.IFirebaseAuthCallbacks");
        zzcpi();
        return true;
      case 5: 
        paramParcel1.enforceInterface("com.google.firebase.auth.api.internal.IFirebaseAuthCallbacks");
        if (paramParcel1.readInt() != 0) {}
        for (paramParcel1 = (Status)Status.CREATOR.createFromParcel(paramParcel1);; paramParcel1 = null)
        {
          onFailure(paramParcel1);
          return true;
        }
      case 6: 
        label191:
        paramParcel1.enforceInterface("com.google.firebase.auth.api.internal.IFirebaseAuthCallbacks");
        zzcpj();
        return true;
      }
      paramParcel1.enforceInterface("com.google.firebase.auth.api.internal.IFirebaseAuthCallbacks");
      zzcpk();
      return true;
    }
    
    private static class zza
      implements zzahh
    {
      private IBinder zzajq;
      
      zza(IBinder paramIBinder)
      {
        this.zzajq = paramIBinder;
      }
      
      public IBinder asBinder()
      {
        return this.zzajq;
      }
      
      /* Error */
      public void onFailure(Status paramStatus)
        throws RemoteException
      {
        // Byte code:
        //   0: invokestatic 31	android/os/Parcel:obtain	()Landroid/os/Parcel;
        //   3: astore_2
        //   4: aload_2
        //   5: ldc 33
        //   7: invokevirtual 37	android/os/Parcel:writeInterfaceToken	(Ljava/lang/String;)V
        //   10: aload_1
        //   11: ifnull +33 -> 44
        //   14: aload_2
        //   15: iconst_1
        //   16: invokevirtual 41	android/os/Parcel:writeInt	(I)V
        //   19: aload_1
        //   20: aload_2
        //   21: iconst_0
        //   22: invokevirtual 47	com/google/android/gms/common/api/Status:writeToParcel	(Landroid/os/Parcel;I)V
        //   25: aload_0
        //   26: getfield 18	com/google/android/gms/internal/zzahh$zza$zza:zzajq	Landroid/os/IBinder;
        //   29: iconst_5
        //   30: aload_2
        //   31: aconst_null
        //   32: iconst_1
        //   33: invokeinterface 53 5 0
        //   38: pop
        //   39: aload_2
        //   40: invokevirtual 56	android/os/Parcel:recycle	()V
        //   43: return
        //   44: aload_2
        //   45: iconst_0
        //   46: invokevirtual 41	android/os/Parcel:writeInt	(I)V
        //   49: goto -24 -> 25
        //   52: astore_1
        //   53: aload_2
        //   54: invokevirtual 56	android/os/Parcel:recycle	()V
        //   57: aload_1
        //   58: athrow
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	59	0	this	zza
        //   0	59	1	paramStatus	Status
        //   3	51	2	localParcel	Parcel
        // Exception table:
        //   from	to	target	type
        //   4	10	52	finally
        //   14	25	52	finally
        //   25	39	52	finally
        //   44	49	52	finally
      }
      
      /* Error */
      public void zza(CreateAuthUriResponse paramCreateAuthUriResponse)
        throws RemoteException
      {
        // Byte code:
        //   0: invokestatic 31	android/os/Parcel:obtain	()Landroid/os/Parcel;
        //   3: astore_2
        //   4: aload_2
        //   5: ldc 33
        //   7: invokevirtual 37	android/os/Parcel:writeInterfaceToken	(Ljava/lang/String;)V
        //   10: aload_1
        //   11: ifnull +33 -> 44
        //   14: aload_2
        //   15: iconst_1
        //   16: invokevirtual 41	android/os/Parcel:writeInt	(I)V
        //   19: aload_1
        //   20: aload_2
        //   21: iconst_0
        //   22: invokevirtual 61	com/google/firebase/auth/api/model/CreateAuthUriResponse:writeToParcel	(Landroid/os/Parcel;I)V
        //   25: aload_0
        //   26: getfield 18	com/google/android/gms/internal/zzahh$zza$zza:zzajq	Landroid/os/IBinder;
        //   29: iconst_3
        //   30: aload_2
        //   31: aconst_null
        //   32: iconst_1
        //   33: invokeinterface 53 5 0
        //   38: pop
        //   39: aload_2
        //   40: invokevirtual 56	android/os/Parcel:recycle	()V
        //   43: return
        //   44: aload_2
        //   45: iconst_0
        //   46: invokevirtual 41	android/os/Parcel:writeInt	(I)V
        //   49: goto -24 -> 25
        //   52: astore_1
        //   53: aload_2
        //   54: invokevirtual 56	android/os/Parcel:recycle	()V
        //   57: aload_1
        //   58: athrow
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	59	0	this	zza
        //   0	59	1	paramCreateAuthUriResponse	CreateAuthUriResponse
        //   3	51	2	localParcel	Parcel
        // Exception table:
        //   from	to	target	type
        //   4	10	52	finally
        //   14	25	52	finally
        //   25	39	52	finally
        //   44	49	52	finally
      }
      
      public void zza(GetTokenResponse paramGetTokenResponse, GetAccountInfoUser paramGetAccountInfoUser)
        throws RemoteException
      {
        Parcel localParcel = Parcel.obtain();
        for (;;)
        {
          try
          {
            localParcel.writeInterfaceToken("com.google.firebase.auth.api.internal.IFirebaseAuthCallbacks");
            if (paramGetTokenResponse != null)
            {
              localParcel.writeInt(1);
              paramGetTokenResponse.writeToParcel(localParcel, 0);
              if (paramGetAccountInfoUser != null)
              {
                localParcel.writeInt(1);
                paramGetAccountInfoUser.writeToParcel(localParcel, 0);
                this.zzajq.transact(2, localParcel, null, 1);
              }
            }
            else
            {
              localParcel.writeInt(0);
              continue;
            }
            localParcel.writeInt(0);
          }
          finally
          {
            localParcel.recycle();
          }
        }
      }
      
      /* Error */
      public void zzb(GetTokenResponse paramGetTokenResponse)
        throws RemoteException
      {
        // Byte code:
        //   0: invokestatic 31	android/os/Parcel:obtain	()Landroid/os/Parcel;
        //   3: astore_2
        //   4: aload_2
        //   5: ldc 33
        //   7: invokevirtual 37	android/os/Parcel:writeInterfaceToken	(Ljava/lang/String;)V
        //   10: aload_1
        //   11: ifnull +33 -> 44
        //   14: aload_2
        //   15: iconst_1
        //   16: invokevirtual 41	android/os/Parcel:writeInt	(I)V
        //   19: aload_1
        //   20: aload_2
        //   21: iconst_0
        //   22: invokevirtual 65	com/google/firebase/auth/api/model/GetTokenResponse:writeToParcel	(Landroid/os/Parcel;I)V
        //   25: aload_0
        //   26: getfield 18	com/google/android/gms/internal/zzahh$zza$zza:zzajq	Landroid/os/IBinder;
        //   29: iconst_1
        //   30: aload_2
        //   31: aconst_null
        //   32: iconst_1
        //   33: invokeinterface 53 5 0
        //   38: pop
        //   39: aload_2
        //   40: invokevirtual 56	android/os/Parcel:recycle	()V
        //   43: return
        //   44: aload_2
        //   45: iconst_0
        //   46: invokevirtual 41	android/os/Parcel:writeInt	(I)V
        //   49: goto -24 -> 25
        //   52: astore_1
        //   53: aload_2
        //   54: invokevirtual 56	android/os/Parcel:recycle	()V
        //   57: aload_1
        //   58: athrow
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	59	0	this	zza
        //   0	59	1	paramGetTokenResponse	GetTokenResponse
        //   3	51	2	localParcel	Parcel
        // Exception table:
        //   from	to	target	type
        //   4	10	52	finally
        //   14	25	52	finally
        //   25	39	52	finally
        //   44	49	52	finally
      }
      
      public void zzcpi()
        throws RemoteException
      {
        Parcel localParcel = Parcel.obtain();
        try
        {
          localParcel.writeInterfaceToken("com.google.firebase.auth.api.internal.IFirebaseAuthCallbacks");
          this.zzajq.transact(4, localParcel, null, 1);
          return;
        }
        finally
        {
          localParcel.recycle();
        }
      }
      
      public void zzcpj()
        throws RemoteException
      {
        Parcel localParcel = Parcel.obtain();
        try
        {
          localParcel.writeInterfaceToken("com.google.firebase.auth.api.internal.IFirebaseAuthCallbacks");
          this.zzajq.transact(6, localParcel, null, 1);
          return;
        }
        finally
        {
          localParcel.recycle();
        }
      }
      
      public void zzcpk()
        throws RemoteException
      {
        Parcel localParcel = Parcel.obtain();
        try
        {
          localParcel.writeInterfaceToken("com.google.firebase.auth.api.internal.IFirebaseAuthCallbacks");
          this.zzajq.transact(7, localParcel, null, 1);
          return;
        }
        finally
        {
          localParcel.recycle();
        }
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzahh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */