package com.google.android.gms.internal;

import android.content.Context;
import android.os.Looper;
import android.support.annotation.NonNull;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.ApiOptions.HasOptions;
import com.google.android.gms.common.api.Api.zza;
import com.google.android.gms.common.api.Api.zzf;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zzf;

public final class zzahj
{
  private static final Api.zza<zzahc, zza> aXu = new Api.zza()
  {
    public zzahc zza(Context paramAnonymousContext, Looper paramAnonymousLooper, zzf paramAnonymouszzf, zzahj.zza paramAnonymouszza, GoogleApiClient.ConnectionCallbacks paramAnonymousConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramAnonymousOnConnectionFailedListener)
    {
      return new zzahd(paramAnonymousContext, paramAnonymousLooper, paramAnonymouszzf, paramAnonymouszza, paramAnonymousConnectionCallbacks, paramAnonymousOnConnectionFailedListener);
    }
  };
  public static final Api<zza> aXv = new Api("InternalFirebaseAuth.FIREBASE_AUTH_API", aXu, hg);
  public static final Api.zzf<zzahc> hg = new Api.zzf();
  
  public static zzahb zza(Context paramContext, zza paramzza)
  {
    return new zzahb(paramContext, paramzza);
  }
  
  public static final class zza
    implements Api.ApiOptions.HasOptions
  {
    private final String aWg;
    
    private zza(@NonNull String paramString)
    {
      this.aWg = zzaa.zzh(paramString, "A valid API key must be provided");
    }
    
    public String getApiKey()
    {
      return this.aWg;
    }
    
    public static final class zza
    {
      private String aWg;
      
      public zza(@NonNull String paramString)
      {
        this.aWg = zzaa.zzib(paramString);
      }
      
      public zzahj.zza zzcpl()
      {
        return new zzahj.zza(this.aWg, null);
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzahj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */