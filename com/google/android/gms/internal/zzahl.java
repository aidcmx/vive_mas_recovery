package com.google.android.gms.internal;

import com.google.android.gms.common.api.Status;

abstract interface zzahl<ResultT>
{
  public abstract void zza(ResultT paramResultT, Status paramStatus);
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzahl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */