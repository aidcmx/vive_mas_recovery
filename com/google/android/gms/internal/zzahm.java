package com.google.android.gms.internal;

import android.os.RemoteException;
import android.support.annotation.NonNull;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzaa;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.api.model.CreateAuthUriResponse;
import com.google.firebase.auth.api.model.GetAccountInfoUser;
import com.google.firebase.auth.api.model.GetTokenResponse;

abstract class zzahm<SuccessT, CallbackT>
{
  private boolean aMV;
  protected FirebaseApp aWU;
  protected CallbackT aXA;
  protected zzahl<SuccessT> aXB;
  protected GetTokenResponse aXC;
  protected GetAccountInfoUser aXD;
  protected CreateAuthUriResponse aXE;
  boolean aXF;
  SuccessT aXG;
  Status aXH;
  protected final int aXw;
  protected final zza aXx = new zza(null);
  protected FirebaseUser aXy;
  protected zzahi aXz;
  
  public zzahm(int paramInt)
  {
    this.aXw = paramInt;
  }
  
  private void zzcpn()
  {
    zzcpf();
    zzaa.zza(this.aMV, "no success or failure set on method implementation");
  }
  
  protected abstract void dispatch()
    throws RemoteException;
  
  public zzahm<SuccessT, CallbackT> zza(zzahl<SuccessT> paramzzahl)
  {
    this.aXB = paramzzahl;
    return this;
  }
  
  public void zza(zzahi paramzzahi)
    throws RemoteException
  {
    this.aXz = paramzzahi;
    dispatch();
  }
  
  public zzahm<SuccessT, CallbackT> zzbd(CallbackT paramCallbackT)
  {
    this.aXA = zzaa.zzb(paramCallbackT, "external callback cannot be null");
    return this;
  }
  
  public void zzbe(SuccessT paramSuccessT)
  {
    this.aMV = true;
    this.aXF = true;
    this.aXG = paramSuccessT;
    this.aXB.zza(paramSuccessT, null);
  }
  
  public abstract void zzcpf();
  
  public void zzcpm()
  {
    zzbe(null);
  }
  
  public zzahm<SuccessT, CallbackT> zze(FirebaseApp paramFirebaseApp)
  {
    this.aWU = ((FirebaseApp)zzaa.zzb(paramFirebaseApp, "firebaseApp cannot be null"));
    return this;
  }
  
  public zzahm<SuccessT, CallbackT> zze(FirebaseUser paramFirebaseUser)
  {
    this.aXy = ((FirebaseUser)zzaa.zzb(paramFirebaseUser, "firebaseUser cannot be null"));
    return this;
  }
  
  public void zzfd(Status paramStatus)
  {
    this.aMV = true;
    this.aXF = false;
    this.aXH = paramStatus;
    this.aXB.zza(null, paramStatus);
  }
  
  private class zza
    extends zzahh.zza
  {
    private zza() {}
    
    public void onFailure(@NonNull Status paramStatus)
      throws RemoteException
    {
      zzahm.this.zzfd(paramStatus);
    }
    
    public void zza(@NonNull CreateAuthUriResponse paramCreateAuthUriResponse)
      throws RemoteException
    {
      if (zzahm.this.aXw == 3) {}
      for (boolean bool = true;; bool = false)
      {
        int i = zzahm.this.aXw;
        zzaa.zza(bool, 36 + "Unexpected response type " + i);
        zzahm.this.aXE = paramCreateAuthUriResponse;
        zzahm.zzb(zzahm.this);
        return;
      }
    }
    
    public void zza(@NonNull GetTokenResponse paramGetTokenResponse, @NonNull GetAccountInfoUser paramGetAccountInfoUser)
      throws RemoteException
    {
      if (zzahm.this.aXw == 2) {}
      for (boolean bool = true;; bool = false)
      {
        int i = zzahm.this.aXw;
        zzaa.zza(bool, 37 + "Unexpected response type: " + i);
        zzahm.this.aXC = paramGetTokenResponse;
        zzahm.this.aXD = paramGetAccountInfoUser;
        zzahm.zzb(zzahm.this);
        return;
      }
    }
    
    public void zzb(@NonNull GetTokenResponse paramGetTokenResponse)
      throws RemoteException
    {
      boolean bool = true;
      if (zzahm.this.aXw == 1) {}
      for (;;)
      {
        int i = zzahm.this.aXw;
        zzaa.zza(bool, 37 + "Unexpected response type: " + i);
        zzahm.this.aXC = paramGetTokenResponse;
        zzahm.zzb(zzahm.this);
        return;
        bool = false;
      }
    }
    
    public void zzcpi()
      throws RemoteException
    {
      if (zzahm.this.aXw == 4) {}
      for (boolean bool = true;; bool = false)
      {
        int i = zzahm.this.aXw;
        zzaa.zza(bool, 36 + "Unexpected response type " + i);
        zzahm.zzb(zzahm.this);
        return;
      }
    }
    
    public void zzcpj()
      throws RemoteException
    {
      if (zzahm.this.aXw == 5) {}
      for (boolean bool = true;; bool = false)
      {
        int i = zzahm.this.aXw;
        zzaa.zza(bool, 36 + "Unexpected response type " + i);
        zzahm.zzb(zzahm.this);
        return;
      }
    }
    
    public void zzcpk()
      throws RemoteException
    {
      if (zzahm.this.aXw == 6) {}
      for (boolean bool = true;; bool = false)
      {
        int i = zzahm.this.aXw;
        zzaa.zza(bool, 36 + "Unexpected response type " + i);
        zzahm.zzb(zzahm.this);
        return;
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzahm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */