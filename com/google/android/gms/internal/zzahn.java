package com.google.android.gms.internal;

import android.support.annotation.NonNull;
import com.google.android.gms.common.internal.zzaa;
import com.google.firebase.auth.api.model.ProviderUserInfo;
import com.google.firebase.auth.api.model.ProviderUserInfoList;
import java.io.IOException;
import java.util.List;

public class zzahn
  extends zzapk<ProviderUserInfoList>
{
  private zzaos aXt;
  
  public void zza(@NonNull zzaos paramzzaos)
  {
    this.aXt = ((zzaos)zzaa.zzy(paramzzaos));
  }
  
  public void zza(zzaqr paramzzaqr, ProviderUserInfoList paramProviderUserInfoList)
    throws IOException
  {
    int j = 0;
    if (paramProviderUserInfoList == null)
    {
      paramzzaqr.bA();
      return;
    }
    zzapk localzzapk = this.aXt.zzk(ProviderUserInfo.class);
    paramzzaqr.bw();
    paramProviderUserInfoList = paramProviderUserInfoList.zzcpr();
    int i;
    if (paramProviderUserInfoList != null) {
      i = paramProviderUserInfoList.size();
    }
    while (j < i)
    {
      localzzapk.zza(paramzzaqr, (ProviderUserInfo)paramProviderUserInfoList.get(j));
      j += 1;
      continue;
      i = 0;
    }
    paramzzaqr.bx();
  }
  
  public ProviderUserInfoList zzc(zzaqp paramzzaqp)
    throws IOException
  {
    if (paramzzaqp.bq() == zzaqq.brJ)
    {
      paramzzaqp.nextNull();
      return null;
    }
    ProviderUserInfoList localProviderUserInfoList = new ProviderUserInfoList();
    zzapk localzzapk = this.aXt.zzk(ProviderUserInfo.class);
    paramzzaqp.beginArray();
    while (paramzzaqp.hasNext())
    {
      ProviderUserInfo localProviderUserInfo = (ProviderUserInfo)localzzapk.zzb(paramzzaqp);
      localProviderUserInfoList.zzcpr().add(localProviderUserInfo);
    }
    paramzzaqp.endArray();
    return localProviderUserInfoList;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzahn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */