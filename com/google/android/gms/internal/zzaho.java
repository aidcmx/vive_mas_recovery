package com.google.android.gms.internal;

import android.support.annotation.NonNull;
import com.google.android.gms.common.internal.zzaa;
import com.google.firebase.auth.api.model.StringList;
import java.io.IOException;
import java.util.List;

public class zzaho
  extends zzapk<StringList>
{
  private zzaos aXt;
  
  public void zza(@NonNull zzaos paramzzaos)
  {
    this.aXt = ((zzaos)zzaa.zzy(paramzzaos));
  }
  
  public void zza(zzaqr paramzzaqr, StringList paramStringList)
    throws IOException
  {
    int j = 0;
    if (paramStringList == null)
    {
      paramzzaqr.bA();
      return;
    }
    zzapk localzzapk = this.aXt.zzk(String.class);
    paramzzaqr.bw();
    paramStringList = paramStringList.zzcqa();
    int i;
    if (paramStringList != null) {
      i = paramStringList.size();
    }
    while (j < i)
    {
      localzzapk.zza(paramzzaqr, (String)paramStringList.get(j));
      j += 1;
      continue;
      i = 0;
    }
    paramzzaqr.bx();
  }
  
  public StringList zzd(zzaqp paramzzaqp)
    throws IOException
  {
    if (paramzzaqp.bq() == zzaqq.brJ)
    {
      paramzzaqp.nextNull();
      return null;
    }
    StringList localStringList = new StringList();
    zzapk localzzapk = this.aXt.zzk(String.class);
    paramzzaqp.beginArray();
    while (paramzzaqp.hasNext())
    {
      String str = (String)localzzapk.zzb(paramzzaqp);
      localStringList.zzcqa().add(str);
    }
    paramzzaqp.endArray();
    return localStringList;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaho.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */