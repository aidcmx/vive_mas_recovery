package com.google.android.gms.internal;

import android.support.annotation.NonNull;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.api.model.GetTokenResponse;

public abstract interface zzahq
{
  public abstract void zza(@NonNull GetTokenResponse paramGetTokenResponse, @NonNull FirebaseUser paramFirebaseUser);
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzahq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */