package com.google.android.gms.internal;

import android.support.annotation.NonNull;
import com.google.android.gms.common.internal.zzaa;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.FacebookAuthCredential;
import com.google.firebase.auth.GithubAuthCredential;
import com.google.firebase.auth.GoogleAuthCredential;
import com.google.firebase.auth.TwitterAuthCredential;
import com.google.firebase.auth.api.model.VerifyAssertionRequest;

public class zzahr
{
  @NonNull
  public static VerifyAssertionRequest zza(@NonNull AuthCredential paramAuthCredential)
  {
    zzaa.zzy(paramAuthCredential);
    if (GoogleAuthCredential.class.isAssignableFrom(paramAuthCredential.getClass())) {
      return GoogleAuthCredential.zza((GoogleAuthCredential)paramAuthCredential);
    }
    if (FacebookAuthCredential.class.isAssignableFrom(paramAuthCredential.getClass())) {
      return FacebookAuthCredential.zza((FacebookAuthCredential)paramAuthCredential);
    }
    if (TwitterAuthCredential.class.isAssignableFrom(paramAuthCredential.getClass())) {
      return TwitterAuthCredential.zza((TwitterAuthCredential)paramAuthCredential);
    }
    if (GithubAuthCredential.class.isAssignableFrom(paramAuthCredential.getClass())) {
      return GithubAuthCredential.zza((GithubAuthCredential)paramAuthCredential);
    }
    throw new IllegalArgumentException("Unsupported credential type.");
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzahr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */