package com.google.android.gms.internal;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.common.internal.zzaa;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseUser;

public class zzahs
  implements AuthResult
{
  private zzahv aYa;
  
  public zzahs(@NonNull zzahv paramzzahv)
  {
    this.aYa = ((zzahv)zzaa.zzy(paramzzahv));
  }
  
  @Nullable
  public FirebaseUser getUser()
  {
    return this.aYa;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzahs.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */