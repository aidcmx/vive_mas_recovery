package com.google.android.gms.internal;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.android.gms.common.internal.zzaa;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.auth.api.model.GetAccountInfoUser;
import com.google.firebase.auth.api.model.ProviderUserInfo;

public class zzaht
  implements UserInfo
{
  @zzapn("photoUrl")
  @Nullable
  private String Pg;
  @zzapn("providerId")
  @NonNull
  private String aXK;
  @zzapn("isEmailVerified")
  private boolean aXN;
  @zzahk
  @Nullable
  private Uri aXj;
  @zzapn("userId")
  @NonNull
  private String ck;
  @zzapn("email")
  @Nullable
  private String jg;
  @zzapn("displayName")
  @Nullable
  private String jh;
  
  public zzaht(@NonNull UserInfo paramUserInfo)
  {
    zzaa.zzy(paramUserInfo);
    this.ck = zzaa.zzib(paramUserInfo.getUid());
    this.aXK = zzaa.zzib(paramUserInfo.getProviderId());
    this.jh = paramUserInfo.getDisplayName();
    if (paramUserInfo.getPhotoUrl() != null)
    {
      this.aXj = paramUserInfo.getPhotoUrl();
      this.Pg = paramUserInfo.getPhotoUrl().toString();
    }
    this.jg = paramUserInfo.getEmail();
    this.aXN = paramUserInfo.isEmailVerified();
  }
  
  public zzaht(@NonNull GetAccountInfoUser paramGetAccountInfoUser, @NonNull String paramString)
  {
    zzaa.zzy(paramGetAccountInfoUser);
    zzaa.zzib(paramString);
    this.ck = zzaa.zzib(paramGetAccountInfoUser.getLocalId());
    this.aXK = paramString;
    this.jg = paramGetAccountInfoUser.getEmail();
    this.jh = paramGetAccountInfoUser.getDisplayName();
    paramString = paramGetAccountInfoUser.getPhotoUri();
    if (paramString != null)
    {
      this.Pg = paramString.toString();
      this.aXj = paramString;
    }
    this.aXN = paramGetAccountInfoUser.isEmailVerified();
  }
  
  public zzaht(@NonNull ProviderUserInfo paramProviderUserInfo)
  {
    zzaa.zzy(paramProviderUserInfo);
    this.ck = zzaa.zzib(paramProviderUserInfo.zzcpy());
    this.aXK = zzaa.zzib(paramProviderUserInfo.getProviderId());
    this.jh = paramProviderUserInfo.getDisplayName();
    paramProviderUserInfo = paramProviderUserInfo.getPhotoUri();
    if (paramProviderUserInfo != null)
    {
      this.Pg = paramProviderUserInfo.toString();
      this.aXj = paramProviderUserInfo;
    }
    this.jg = null;
    this.aXN = false;
  }
  
  @Nullable
  public String getDisplayName()
  {
    return this.jh;
  }
  
  @Nullable
  public String getEmail()
  {
    return this.jg;
  }
  
  @Nullable
  public Uri getPhotoUrl()
  {
    if ((!TextUtils.isEmpty(this.Pg)) && (this.aXj == null)) {
      this.aXj = Uri.parse(this.Pg);
    }
    return this.aXj;
  }
  
  @NonNull
  public String getProviderId()
  {
    return this.aXK;
  }
  
  @NonNull
  public String getUid()
  {
    return this.ck;
  }
  
  public boolean isEmailVerified()
  {
    return this.aXN;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaht.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */