package com.google.android.gms.internal;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;
import com.google.android.gms.common.internal.zzaa;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.auth.api.model.GetTokenResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class zzahv
  extends FirebaseUser
{
  private zzaos aXt;
  private GetTokenResponse aYb;
  private zzaht aYc;
  private String aYd;
  private String aYe;
  private List<zzaht> aYf;
  private List<String> aYg;
  private Map<String, zzaht> aYh;
  private String aYi;
  private boolean aYj;
  
  public zzahv(@NonNull FirebaseApp paramFirebaseApp, @NonNull List<? extends UserInfo> paramList)
  {
    zzaa.zzy(paramFirebaseApp);
    this.aYd = paramFirebaseApp.getName();
    this.aXt = zzahg.zzcph();
    this.aYe = "com.google.firebase.auth.internal.DefaultFirebaseUser";
    this.aYi = "2";
    zzaq(paramList);
  }
  
  @Nullable
  public String getDisplayName()
  {
    return this.aYc.getDisplayName();
  }
  
  @Nullable
  public String getEmail()
  {
    return this.aYc.getEmail();
  }
  
  @Nullable
  public Uri getPhotoUrl()
  {
    return this.aYc.getPhotoUrl();
  }
  
  @NonNull
  public List<? extends UserInfo> getProviderData()
  {
    return this.aYf;
  }
  
  @NonNull
  public String getProviderId()
  {
    return this.aYc.getProviderId();
  }
  
  @Nullable
  public List<String> getProviders()
  {
    return this.aYg;
  }
  
  @NonNull
  public String getUid()
  {
    return this.aYc.getUid();
  }
  
  public boolean isAnonymous()
  {
    return this.aYj;
  }
  
  public boolean isEmailVerified()
  {
    return this.aYc.isEmailVerified();
  }
  
  public void zza(@NonNull GetTokenResponse paramGetTokenResponse)
  {
    this.aYb = ((GetTokenResponse)zzaa.zzy(paramGetTokenResponse));
  }
  
  @NonNull
  public FirebaseUser zzaq(@NonNull List<? extends UserInfo> paramList)
  {
    zzaa.zzy(paramList);
    this.aYf = new ArrayList(paramList.size());
    this.aYg = new ArrayList(paramList.size());
    this.aYh = new ArrayMap();
    int i = 0;
    if (i < paramList.size())
    {
      zzaht localzzaht = new zzaht((UserInfo)paramList.get(i));
      if (localzzaht.getProviderId().equals("firebase")) {
        this.aYc = localzzaht;
      }
      for (;;)
      {
        this.aYf.add(localzzaht);
        this.aYh.put(localzzaht.getProviderId(), localzzaht);
        i += 1;
        break;
        this.aYg.add(localzzaht.getProviderId());
      }
    }
    if (this.aYc == null) {
      this.aYc = ((zzaht)this.aYf.get(0));
    }
    return this;
  }
  
  @NonNull
  public FirebaseApp zzcow()
  {
    return FirebaseApp.getInstance(this.aYd);
  }
  
  @NonNull
  public GetTokenResponse zzcox()
  {
    return this.aYb;
  }
  
  @NonNull
  public String zzcoy()
  {
    return this.aXt.zzck(this.aYb);
  }
  
  @NonNull
  public String zzcoz()
  {
    return zzcox().getAccessToken();
  }
  
  public List<zzaht> zzcqg()
  {
    return this.aYf;
  }
  
  public zzahv zzcv(boolean paramBoolean)
  {
    this.aYj = paramBoolean;
    return this;
  }
  
  public zzahv zzrw(@NonNull String paramString)
  {
    this.aYi = paramString;
    return this;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzahv.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */