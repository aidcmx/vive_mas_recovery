package com.google.android.gms.internal;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.common.internal.zzaa;
import com.google.firebase.auth.ProviderQueryResult;
import com.google.firebase.auth.api.model.CreateAuthUriResponse;
import java.util.List;

public class zzahw
  implements ProviderQueryResult
{
  private List<String> aYg;
  
  public zzahw(@NonNull CreateAuthUriResponse paramCreateAuthUriResponse)
  {
    zzaa.zzy(paramCreateAuthUriResponse);
    this.aYg = paramCreateAuthUriResponse.getAllProviders();
  }
  
  @Nullable
  public List<String> getProviders()
  {
    return this.aYg;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzahw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */