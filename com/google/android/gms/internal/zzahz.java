package com.google.android.gms.internal;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.android.gms.common.internal.zzaa;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.api.model.GetTokenResponse;
import java.util.ArrayList;
import java.util.List;

public class zzahz
{
  private zzaos aXt;
  private String aYk;
  private zzapd aYl;
  private Context mContext;
  private SharedPreferences zzbct;
  
  public zzahz(@NonNull Context paramContext, @NonNull String paramString, @NonNull zzaos paramzzaos)
  {
    zzaa.zzy(paramContext);
    this.aYk = zzaa.zzib(paramString);
    this.mContext = paramContext.getApplicationContext();
    paramContext = String.format("com.google.firebase.auth.api.Store.%s", new Object[] { this.aYk });
    this.aXt = ((zzaos)zzaa.zzy(paramzzaos));
    this.aYl = new zzapd();
    this.zzbct = this.mContext.getSharedPreferences(paramContext, 0);
  }
  
  private zzahv zza(@NonNull zzapb paramzzapb)
  {
    String str1 = paramzzapb.zzuo("cachedTokenState").aU();
    String str2 = paramzzapb.zzuo("applicationName").aU();
    boolean bool = paramzzapb.zzuo("anonymous").getAsBoolean();
    Object localObject = paramzzapb.zzuo("version");
    if ((localObject != null) && (!((zzaoy)localObject).aY())) {}
    for (localObject = ((zzaoy)localObject).aU();; localObject = "2")
    {
      paramzzapb = paramzzapb.zzup("userInfos");
      int j = paramzzapb.size();
      ArrayList localArrayList = new ArrayList(j);
      int i = 0;
      while (i < j)
      {
        localArrayList.add((zzaht)this.aXt.zza(paramzzapb.zzagm(i), zzaht.class));
        i += 1;
      }
      paramzzapb = new zzahv(FirebaseApp.getInstance(str2), localArrayList);
      if (!TextUtils.isEmpty(str1)) {
        paramzzapb.zza((GetTokenResponse)this.aXt.zzf(str1, GetTokenResponse.class));
      }
      ((zzahv)paramzzapb.zzcu(bool)).zzrw((String)localObject);
      return paramzzapb;
    }
  }
  
  @Nullable
  private String zzi(@NonNull FirebaseUser paramFirebaseUser)
  {
    zzapb localzzapb = new zzapb();
    if (zzahv.class.isAssignableFrom(paramFirebaseUser.getClass()))
    {
      paramFirebaseUser = (zzahv)paramFirebaseUser;
      localzzapb.zzcb("cachedTokenState", paramFirebaseUser.zzcoy());
      localzzapb.zzcb("applicationName", paramFirebaseUser.zzcow().getName());
      localzzapb.zzcb("type", "com.google.firebase.auth.internal.DefaultFirebaseUser");
      if (paramFirebaseUser.zzcqg() != null)
      {
        zzaov localzzaov = new zzaov();
        List localList = paramFirebaseUser.zzcqg();
        int i = 0;
        while (i < localList.size())
        {
          zzaht localzzaht = (zzaht)localList.get(i);
          localzzaov.zzc(zzrx(this.aXt.zzck(localzzaht)));
          i += 1;
        }
        localzzapb.zza("userInfos", localzzaov);
      }
      localzzapb.zzb("anonymous", Boolean.valueOf(paramFirebaseUser.isAnonymous()));
      localzzapb.zzcb("version", "2");
      return localzzapb.toString();
    }
    return null;
  }
  
  private static zzaoy zzrx(String paramString)
  {
    return new zzapd().zzuq(paramString);
  }
  
  public void clear(String paramString)
  {
    this.zzbct.edit().remove(paramString).apply();
  }
  
  @Nullable
  public String get(String paramString)
  {
    return this.zzbct.getString(paramString, null);
  }
  
  public void zza(@NonNull FirebaseUser paramFirebaseUser, @NonNull GetTokenResponse paramGetTokenResponse)
  {
    zzaa.zzy(paramFirebaseUser);
    zzaa.zzy(paramGetTokenResponse);
    zzp(String.format("com.google.firebase.auth.GET_TOKEN_RESPONSE.%s", new Object[] { paramFirebaseUser.getUid() }), paramGetTokenResponse);
  }
  
  public void zzbk(String paramString1, String paramString2)
  {
    this.zzbct.edit().putString(paramString1, paramString2).apply();
  }
  
  @Nullable
  public FirebaseUser zzcqi()
  {
    Object localObject = get("com.google.firebase.auth.FIREBASE_USER");
    if (TextUtils.isEmpty((CharSequence)localObject)) {}
    for (;;)
    {
      return null;
      try
      {
        localObject = this.aYl.zzuq((String)localObject).aZ();
        if ((((zzapb)localObject).has("type")) && ("com.google.firebase.auth.internal.DefaultFirebaseUser".equalsIgnoreCase(((zzapb)localObject).zzuo("type").aU())))
        {
          localObject = zza((zzapb)localObject);
          return (FirebaseUser)localObject;
        }
      }
      catch (zzaph localzzaph) {}
    }
    return null;
  }
  
  public void zzcqj()
  {
    clear("com.google.firebase.auth.FIREBASE_USER");
  }
  
  @Nullable
  public <T> T zze(String paramString, Class<T> paramClass)
  {
    paramString = get(paramString);
    if (TextUtils.isEmpty(paramString)) {
      return null;
    }
    return (T)this.aXt.zzf(paramString, paramClass);
  }
  
  public void zzf(@NonNull FirebaseUser paramFirebaseUser)
  {
    zzaa.zzy(paramFirebaseUser);
    paramFirebaseUser = zzi(paramFirebaseUser);
    if (!TextUtils.isEmpty(paramFirebaseUser)) {
      zzbk("com.google.firebase.auth.FIREBASE_USER", paramFirebaseUser);
    }
  }
  
  public GetTokenResponse zzg(@NonNull FirebaseUser paramFirebaseUser)
  {
    zzaa.zzy(paramFirebaseUser);
    return (GetTokenResponse)zze(String.format("com.google.firebase.auth.GET_TOKEN_RESPONSE.%s", new Object[] { paramFirebaseUser.getUid() }), GetTokenResponse.class);
  }
  
  public void zzh(@NonNull FirebaseUser paramFirebaseUser)
  {
    zzaa.zzy(paramFirebaseUser);
    clear(String.format("com.google.firebase.auth.GET_TOKEN_RESPONSE.%s", new Object[] { paramFirebaseUser.getUid() }));
  }
  
  public void zzp(String paramString, Object paramObject)
  {
    this.zzbct.edit().putString(paramString, this.aXt.zzck(paramObject)).apply();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzahz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */