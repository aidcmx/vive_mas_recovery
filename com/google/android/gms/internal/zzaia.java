package com.google.android.gms.internal;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import java.util.concurrent.Executor;

public class zzaia
  implements Executor
{
  private static zzaia aYm = new zzaia();
  private Handler mHandler = new Handler(Looper.getMainLooper());
  
  public static zzaia zzcqk()
  {
    return aYm;
  }
  
  public void execute(@NonNull Runnable paramRunnable)
  {
    this.mHandler.post(paramRunnable);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaia.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */