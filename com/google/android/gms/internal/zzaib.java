package com.google.android.gms.internal;

import android.content.Context;
import java.util.concurrent.TimeUnit;

public final class zzaib
{
  public static final zzvq<String> aYA = zzvq.zzc(0, "crash:gateway_url", "https://mobilecrashreporting.googleapis.com/v1/crashes:batchCreate?key=");
  public static final zzvq<Integer> aYB = zzvq.zzb(0, "crash:log_buffer_capacity", 100);
  public static final zzvq<Integer> aYC = zzvq.zzb(0, "crash:log_buffer_max_total_size", 32768);
  public static final zzvq<Integer> aYD = zzvq.zzb(0, "crash:crash_backlog_capacity", 5);
  public static final zzvq<Long> aYE = zzvq.zzb(0, "crash:crash_backlog_max_age", 604800000L);
  public static final zzvq<Long> aYF = zzvq.zzb(0, "crash:starting_backoff", TimeUnit.SECONDS.toMillis(1L));
  public static final zzvq<Long> aYG = zzvq.zzb(0, "crash:backoff_limit", TimeUnit.MINUTES.toMillis(60L));
  public static final zzvq<Integer> aYH = zzvq.zzb(0, "crash:retry_num_attempts", 12);
  public static final zzvq<Integer> aYI = zzvq.zzb(0, "crash:batch_size", 5);
  public static final zzvq<Long> aYJ = zzvq.zzb(0, "crash:batch_throttle", TimeUnit.MINUTES.toMillis(5L));
  public static final zzvq<Integer> aYK = zzvq.zzb(0, "crash:frame_depth", 60);
  public static final zzvq<Integer> aYL = zzvq.zzb(0, "crash:receiver_delay", 100);
  public static final zzvq<Integer> aYM = zzvq.zzb(0, "crash:thread_idle_timeout", 10);
  public static final zzvq<Boolean> aYz = zzvq.zzb(0, "crash:enabled", Boolean.valueOf(true));
  
  public static final void initialize(Context paramContext)
  {
    zzvu.zzbhe();
    zzvr.initialize(paramContext);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaib.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */