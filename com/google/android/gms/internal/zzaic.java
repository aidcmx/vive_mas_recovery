package com.google.android.gms.internal;

import android.support.annotation.NonNull;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApiNotAvailableException;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseApp.zza;
import com.google.firebase.auth.GetTokenResult;
import java.util.concurrent.ScheduledExecutorService;

public class zzaic
  implements zzajf
{
  private final ScheduledExecutorService aZE;
  private final FirebaseApp aZF;
  
  public zzaic(@NonNull FirebaseApp paramFirebaseApp, @NonNull ScheduledExecutorService paramScheduledExecutorService)
  {
    this.aZF = paramFirebaseApp;
    this.aZE = paramScheduledExecutorService;
  }
  
  private FirebaseApp.zza zzb(final zzajf.zzb paramzzb)
  {
    new FirebaseApp.zza()
    {
      public void zzb(@NonNull final zzant paramAnonymouszzant)
      {
        zzaic.zza(zzaic.this).execute(new Runnable()
        {
          public void run()
          {
            zzaic.3.this.aZI.zzsr(paramAnonymouszzant.getToken());
          }
        });
      }
    };
  }
  
  public void zza(zzajf.zzb paramzzb)
  {
    paramzzb = zzb(paramzzb);
    this.aZF.zza(paramzzb);
  }
  
  public void zza(boolean paramBoolean, @NonNull final zzajf.zza paramzza)
  {
    this.aZF.getToken(paramBoolean).addOnSuccessListener(this.aZE, new OnSuccessListener()
    {
      public void zza(GetTokenResult paramAnonymousGetTokenResult)
      {
        paramzza.zzsi(paramAnonymousGetTokenResult.getToken());
      }
    }).addOnFailureListener(this.aZE, new OnFailureListener()
    {
      private boolean zzb(Exception paramAnonymousException)
      {
        return ((paramAnonymousException instanceof FirebaseApiNotAvailableException)) || ((paramAnonymousException instanceof zzanu));
      }
      
      public void onFailure(@NonNull Exception paramAnonymousException)
      {
        if (zzb(paramAnonymousException))
        {
          paramzza.zzsi(null);
          return;
        }
        paramzza.onError(paramAnonymousException.getMessage());
      }
    });
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaic.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */