package com.google.android.gms.internal;

import android.os.Handler;
import android.os.Looper;

public class zzaid
  implements zzajn
{
  private final Handler handler = new Handler(Looper.getMainLooper());
  
  public void restart() {}
  
  public void shutdown() {}
  
  public void zzq(Runnable paramRunnable)
  {
    this.handler.post(paramRunnable);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaid.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */