package com.google.android.gms.internal;

import android.content.Context;
import android.os.Build.VERSION;
import android.os.Handler;
import android.util.Log;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseApp.zzb;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.connection.idl.ConnectionConfig;
import com.google.firebase.database.connection.idl.zzc;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ScheduledExecutorService;

public class zzaie
  implements zzajr
{
  private final FirebaseApp aZF;
  private final Set<String> aZK = new HashSet();
  private final Context zzaht;
  
  public zzaie(FirebaseApp paramFirebaseApp)
  {
    this.aZF = paramFirebaseApp;
    if (this.aZF == null)
    {
      Log.e("FirebaseDatabase", "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
      Log.e("FirebaseDatabase", "ERROR: You must call FirebaseApp.initializeApp() before using Firebase Database.");
      Log.e("FirebaseDatabase", "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
      throw new RuntimeException("You need to call FirebaseApp.initializeApp() before using Firebase Database.");
    }
    this.zzaht = this.aZF.getApplicationContext();
  }
  
  public zzaiy zza(final zzajj paramzzajj, zzaiu paramzzaiu, zzaiw paramzzaiw, zzaiy.zza paramzza)
  {
    paramzzajj = new ConnectionConfig(paramzzaiw, paramzzajj.zzctu(), paramzzajj.zzcuo(), paramzzajj.zzcsk(), FirebaseDatabase.getSdkVersion(), paramzzajj.zzux());
    paramzzajj = zzc.zza(this.zzaht, paramzzajj, paramzzaiu, paramzza);
    this.aZF.zza(new FirebaseApp.zzb()
    {
      public void zzcr(boolean paramAnonymousBoolean)
      {
        if (paramAnonymousBoolean)
        {
          paramzzajj.interrupt("app_in_background");
          return;
        }
        paramzzajj.resume("app_in_background");
      }
    });
    return paramzzajj;
  }
  
  public zzajf zza(ScheduledExecutorService paramScheduledExecutorService)
  {
    return new zzaic(this.aZF, paramScheduledExecutorService);
  }
  
  public zzajn zza(zzajj paramzzajj)
  {
    return new zzaid();
  }
  
  public zzaku zza(zzajj paramzzajj, String paramString)
  {
    String str = paramzzajj.zzcut();
    paramString = String.valueOf(paramString).length() + 1 + String.valueOf(str).length() + paramString + "_" + str;
    if (this.aZK.contains(paramString)) {
      throw new DatabaseException(String.valueOf(str).length() + 47 + "SessionPersistenceKey '" + str + "' has already been used.");
    }
    this.aZK.add(paramString);
    return new zzakr(paramzzajj, new zzaif(this.zzaht, paramzzajj, paramString), new zzaks(paramzzajj.zzcuq()));
  }
  
  public zzalx zza(zzajj paramzzajj, zzalx.zza paramzza, List<String> paramList)
  {
    return new zzalu(paramzza, paramList);
  }
  
  public zzajv zzb(zzajj paramzzajj)
  {
    new zzanh()
    {
      public void zzj(final Throwable paramAnonymousThrowable)
      {
        final String str = zzanh.zzl(paramAnonymousThrowable);
        this.aZL.zzd(str, paramAnonymousThrowable);
        new Handler(zzaie.zza(zzaie.this).getMainLooper()).post(new Runnable()
        {
          public void run()
          {
            throw new RuntimeException(str, paramAnonymousThrowable);
          }
        });
        zzcsj().shutdownNow();
      }
    };
  }
  
  public String zzc(zzajj paramzzajj)
  {
    int i = Build.VERSION.SDK_INT;
    return 19 + i + "/Android";
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaie.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */