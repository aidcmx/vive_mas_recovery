package com.google.android.gms.internal;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabaseLockedException;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import com.google.firebase.database.DatabaseException;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class zzaif
  implements zzakv
{
  private static final Charset vp;
  private final SQLiteDatabase aZQ;
  private final zzalw aZR;
  private boolean aZS;
  private long aZT = 0L;
  
  static
  {
    if (!zzaif.class.desiredAssertionStatus()) {}
    for (boolean bool = true;; bool = false)
    {
      $assertionsDisabled = bool;
      vp = Charset.forName("UTF-8");
      return;
    }
  }
  
  public zzaif(Context paramContext, zzajj paramzzajj, String paramString)
  {
    try
    {
      paramString = URLEncoder.encode(paramString, "utf-8");
      this.aZR = paramzzajj.zzss("Persistence");
      this.aZQ = zzai(paramContext, paramString);
      return;
    }
    catch (IOException paramContext)
    {
      throw new RuntimeException(paramContext);
    }
  }
  
  private int zza(zzajq paramzzajq, List<String> paramList, int paramInt)
  {
    int i = paramInt + 1;
    String str = zzc(paramzzajq);
    if (!((String)paramList.get(paramInt)).startsWith(str)) {
      throw new IllegalStateException("Extracting split nodes needs to start with path prefix");
    }
    while ((i < paramList.size()) && (((String)paramList.get(i)).equals(zza(paramzzajq, i - paramInt)))) {
      i += 1;
    }
    if (i < paramList.size())
    {
      paramList = (String)paramList.get(i);
      paramzzajq = String.valueOf(str);
      str = String.valueOf(".part-");
      if (str.length() != 0) {}
      for (paramzzajq = paramzzajq.concat(str); paramList.startsWith(paramzzajq); paramzzajq = new String(paramzzajq)) {
        throw new IllegalStateException("Run did not finish with all parts");
      }
    }
    return i - paramInt;
  }
  
  private int zza(String paramString, zzajq paramzzajq)
  {
    paramzzajq = zzc(paramzzajq);
    String str = zzsd(paramzzajq);
    return this.aZQ.delete(paramString, "path >= ? AND path < ?", new String[] { paramzzajq, str });
  }
  
  private Cursor zza(zzajq paramzzajq, String[] paramArrayOfString)
  {
    String str2 = zzc(paramzzajq);
    String str3 = zzsd(str2);
    String[] arrayOfString = new String[paramzzajq.size() + 3];
    String str1 = String.valueOf(zzb(paramzzajq, arrayOfString));
    String str4 = String.valueOf(" OR (path > ? AND path < ?)");
    if (str4.length() != 0) {}
    for (str1 = str1.concat(str4);; str1 = new String(str1))
    {
      arrayOfString[(paramzzajq.size() + 1)] = str2;
      arrayOfString[(paramzzajq.size() + 2)] = str3;
      return this.aZQ.query("serverCache", paramArrayOfString, str1, arrayOfString, null, null, "path");
    }
  }
  
  private String zza(zzajq paramzzajq, int paramInt)
  {
    paramzzajq = String.valueOf(zzc(paramzzajq));
    String str = String.valueOf(String.format(".part-%04d", new Object[] { Integer.valueOf(paramInt) }));
    if (str.length() != 0) {
      return paramzzajq.concat(str);
    }
    return new String(paramzzajq);
  }
  
  private void zza(zzajq paramzzajq, long paramLong, String paramString, byte[] paramArrayOfByte)
  {
    zzcri();
    this.aZQ.delete("writes", "id = ?", new String[] { String.valueOf(paramLong) });
    if (paramArrayOfByte.length >= 262144)
    {
      paramArrayOfByte = zzd(paramArrayOfByte, 262144);
      int i = 0;
      while (i < paramArrayOfByte.size())
      {
        localContentValues = new ContentValues();
        localContentValues.put("id", Long.valueOf(paramLong));
        localContentValues.put("path", zzc(paramzzajq));
        localContentValues.put("type", paramString);
        localContentValues.put("part", Integer.valueOf(i));
        localContentValues.put("node", (byte[])paramArrayOfByte.get(i));
        this.aZQ.insertWithOnConflict("writes", null, localContentValues, 5);
        i += 1;
      }
    }
    ContentValues localContentValues = new ContentValues();
    localContentValues.put("id", Long.valueOf(paramLong));
    localContentValues.put("path", zzc(paramzzajq));
    localContentValues.put("type", paramString);
    localContentValues.put("part", null);
    localContentValues.put("node", paramArrayOfByte);
    this.aZQ.insertWithOnConflict("writes", null, localContentValues, 5);
  }
  
  private void zza(zzajq paramzzajq1, final zzajq paramzzajq2, zzakz<Long> paramzzakz1, final zzakz<Long> paramzzakz2, zzakw paramzzakw, final List<zzank<zzajq, zzaml>> paramList)
  {
    if (paramzzakz1.getValue() != null)
    {
      int i = ((Integer)paramzzakw.zza(Integer.valueOf(0), new zzakz.zza()
      {
        public Integer zza(zzajq paramAnonymouszzajq, Void paramAnonymousVoid, Integer paramAnonymousInteger)
        {
          if (paramzzakz2.zzak(paramAnonymouszzajq) == null) {}
          for (int i = paramAnonymousInteger.intValue() + 1;; i = paramAnonymousInteger.intValue()) {
            return Integer.valueOf(i);
          }
        }
      })).intValue();
      if (i > 0)
      {
        paramzzajq1 = paramzzajq1.zzh(paramzzajq2);
        if (this.aZR.zzcyu()) {
          this.aZR.zzi(String.format("Need to rewrite %d nodes below path %s", new Object[] { Integer.valueOf(i), paramzzajq1 }), new Object[0]);
        }
        paramzzakw.zza(null, new zzakz.zza()
        {
          public Void zza(zzajq paramAnonymouszzajq, Void paramAnonymousVoid1, Void paramAnonymousVoid2)
          {
            if (paramzzakz2.zzak(paramAnonymouszzajq) == null) {
              paramList.add(new zzank(paramzzajq2.zzh(paramAnonymouszzajq), this.aZY.zzao(paramAnonymouszzajq)));
            }
            return null;
          }
        });
      }
    }
    for (;;)
    {
      return;
      paramzzakz1 = paramzzakz1.zzcxf().iterator();
      while (paramzzakz1.hasNext())
      {
        Map.Entry localEntry = (Map.Entry)paramzzakz1.next();
        zzalz localzzalz = (zzalz)localEntry.getKey();
        zzakw localzzakw = paramzzakw.zzd((zzalz)localEntry.getKey());
        zza(paramzzajq1, paramzzajq2.zza(localzzalz), (zzakz)localEntry.getValue(), paramzzakz2.zze(localzzalz), localzzakw, paramList);
      }
    }
  }
  
  private void zza(zzajq paramzzajq, zzaml paramzzaml, boolean paramBoolean)
  {
    long l1 = System.currentTimeMillis();
    int k;
    int m;
    if (!paramBoolean)
    {
      k = zza("serverCache", paramzzajq);
      m = zzc(paramzzajq, paramzzaml);
      long l2 = System.currentTimeMillis();
      if (this.aZR.zzcyu()) {
        this.aZR.zzi(String.format("Persisted a total of %d rows and deleted %d rows for a set at %s in %dms", new Object[] { Integer.valueOf(m), Integer.valueOf(k), paramzzajq.toString(), Long.valueOf(l2 - l1) }), new Object[0]);
      }
      return;
    }
    paramzzaml = paramzzaml.iterator();
    int i = 0;
    int j = 0;
    for (;;)
    {
      m = i;
      k = j;
      if (!paramzzaml.hasNext()) {
        break;
      }
      zzamk localzzamk = (zzamk)paramzzaml.next();
      j += zza("serverCache", paramzzajq.zza(localzzamk.a()));
      i = zzc(paramzzajq.zza(localzzamk.a()), localzzamk.zzcqy()) + i;
    }
  }
  
  private SQLiteDatabase zzai(Context paramContext, String paramString)
  {
    paramContext = new zza(paramContext, paramString);
    try
    {
      paramContext = paramContext.getWritableDatabase();
      paramContext.rawQuery("PRAGMA locking_mode = EXCLUSIVE", null).close();
      paramContext.beginTransaction();
      paramContext.endTransaction();
      return paramContext;
    }
    catch (SQLiteException paramContext)
    {
      if ((paramContext instanceof SQLiteDatabaseLockedException)) {
        throw new DatabaseException("Failed to gain exclusive lock to Firebase Database's offline persistence. This generally means you are using Firebase Database from multiple processes in your app. Keep in mind that multi-process Android apps execute the code in your Application class in all processes, so you may need to avoid initializing FirebaseDatabase in your Application class. If you are intentionally using Firebase Database from multiple processes, you can only enable offline persistence (i.e. call setPersistenceEnabled(true)) in one of them.", paramContext);
      }
      throw paramContext;
    }
  }
  
  private byte[] zzar(List<byte[]> paramList)
  {
    Object localObject = paramList.iterator();
    for (int i = 0; ((Iterator)localObject).hasNext(); i = ((byte[])((Iterator)localObject).next()).length + i) {}
    localObject = new byte[i];
    paramList = paramList.iterator();
    byte[] arrayOfByte;
    for (i = 0; paramList.hasNext(); i = arrayOfByte.length + i)
    {
      arrayOfByte = (byte[])paramList.next();
      System.arraycopy(arrayOfByte, 0, localObject, i, arrayOfByte.length);
    }
    return (byte[])localObject;
  }
  
  private zzaml zzat(byte[] paramArrayOfByte)
  {
    try
    {
      zzaml localzzaml = zzamm.zzbt(zzane.zztc(new String(paramArrayOfByte, vp)));
      return localzzaml;
    }
    catch (IOException localIOException)
    {
      paramArrayOfByte = String.valueOf(new String(paramArrayOfByte, vp));
      if (paramArrayOfByte.length() == 0) {}
    }
    for (paramArrayOfByte = "Could not deserialize node: ".concat(paramArrayOfByte);; paramArrayOfByte = new String("Could not deserialize node: ")) {
      throw new RuntimeException(paramArrayOfByte, localIOException);
    }
  }
  
  private zzaml zzb(zzajq paramzzajq)
  {
    ArrayList localArrayList2 = new ArrayList();
    ArrayList localArrayList1 = new ArrayList();
    long l1 = System.currentTimeMillis();
    Object localObject1 = zza(paramzzajq, new String[] { "path", "value" });
    long l2 = System.currentTimeMillis();
    long l3 = System.currentTimeMillis();
    try
    {
      while (((Cursor)localObject1).moveToNext())
      {
        localArrayList2.add(((Cursor)localObject1).getString(0));
        localArrayList1.add(((Cursor)localObject1).getBlob(1));
      }
      l4 = System.currentTimeMillis();
    }
    finally
    {
      ((Cursor)localObject1).close();
    }
    long l4;
    long l5 = System.currentTimeMillis();
    localObject1 = zzame.zzczq();
    int i = 0;
    HashMap localHashMap = new HashMap();
    int j = 0;
    Object localObject3;
    if (j < localArrayList1.size())
    {
      Object localObject4;
      if (((String)localArrayList2.get(j)).endsWith(".part-0000"))
      {
        localObject2 = (String)localArrayList2.get(j);
        localObject2 = new zzajq(((String)localObject2).substring(0, ((String)localObject2).length() - ".part-0000".length()));
        int k = zza((zzajq)localObject2, localArrayList2, j);
        if (this.aZR.zzcyu()) {
          this.aZR.zzi(42 + "Loading split node with " + k + " parts.", new Object[0]);
        }
        localObject4 = zzat(zzar(localArrayList1.subList(j, j + k)));
        j = j + k - 1;
        localObject3 = localObject2;
        label310:
        if ((((zzajq)localObject3).zzcvm() == null) || (!((zzajq)localObject3).zzcvm().zzczb())) {
          break label392;
        }
        localHashMap.put(localObject3, localObject4);
      }
      for (;;)
      {
        j += 1;
        break;
        localObject2 = zzat((byte[])localArrayList1.get(j));
        localObject3 = new zzajq((String)localArrayList2.get(j));
        localObject4 = localObject2;
        break label310;
        label392:
        if (((zzajq)localObject3).zzi(paramzzajq))
        {
          if (i == 0) {}
          for (boolean bool = true;; bool = false)
          {
            zzann.zzb(bool, "Descendants of path must come after ancestors.");
            localObject1 = ((zzaml)localObject4).zzao(zzajq.zza((zzajq)localObject3, paramzzajq));
            break;
          }
        }
        if (!paramzzajq.zzi((zzajq)localObject3)) {
          break label471;
        }
        i = 1;
        localObject1 = ((zzaml)localObject1).zzl(zzajq.zza(paramzzajq, (zzajq)localObject3), (zzaml)localObject4);
      }
      label471:
      throw new IllegalStateException(String.format("Loading an unrelated row with path %s for %s", new Object[] { localObject3, paramzzajq }));
    }
    Object localObject2 = localHashMap.entrySet().iterator();
    while (((Iterator)localObject2).hasNext())
    {
      localObject3 = (Map.Entry)((Iterator)localObject2).next();
      localObject1 = ((zzaml)localObject1).zzl(zzajq.zza(paramzzajq, (zzajq)((Map.Entry)localObject3).getKey()), (zzaml)((Map.Entry)localObject3).getValue());
    }
    long l6 = System.currentTimeMillis();
    long l7 = System.currentTimeMillis();
    if (this.aZR.zzcyu()) {
      this.aZR.zzi(String.format("Loaded a total of %d rows for a total of %d nodes at %s in %dms (Query: %dms, Loading: %dms, Serializing: %dms)", new Object[] { Integer.valueOf(localArrayList1.size()), Integer.valueOf(zzani.zzu((zzaml)localObject1)), paramzzajq, Long.valueOf(l7 - l1), Long.valueOf(l2 - l1), Long.valueOf(l4 - l3), Long.valueOf(l6 - l5) }), new Object[0]);
    }
    return (zzaml)localObject1;
  }
  
  private static String zzb(zzajq paramzzajq, String[] paramArrayOfString)
  {
    assert (paramArrayOfString.length >= paramzzajq.size() + 1);
    int i = 0;
    StringBuilder localStringBuilder = new StringBuilder("(");
    while (!paramzzajq.isEmpty())
    {
      localStringBuilder.append("path");
      localStringBuilder.append(" = ? OR ");
      paramArrayOfString[i] = zzc(paramzzajq);
      paramzzajq = paramzzajq.zzcvl();
      i += 1;
    }
    localStringBuilder.append("path");
    localStringBuilder.append(" = ?)");
    paramArrayOfString[i] = zzc(zzajq.zzcvg());
    return localStringBuilder.toString();
  }
  
  private byte[] zzbf(Object paramObject)
  {
    try
    {
      paramObject = zzane.zzbv(paramObject).getBytes(vp);
      return (byte[])paramObject;
    }
    catch (IOException paramObject)
    {
      throw new RuntimeException("Could not serialize leaf node", (Throwable)paramObject);
    }
  }
  
  private int zzc(zzajq paramzzajq, zzaml paramzzaml)
  {
    long l = zzani.zzt(paramzzaml);
    if (((paramzzaml instanceof zzama)) && (l > 16384L))
    {
      if (this.aZR.zzcyu()) {
        this.aZR.zzi(String.format("Node estimated serialized size at path %s of %d bytes exceeds limit of %d bytes. Splitting up.", new Object[] { paramzzajq, Long.valueOf(l), Integer.valueOf(16384) }), new Object[0]);
      }
      Iterator localIterator = paramzzaml.iterator();
      zzamk localzzamk;
      for (int i = 0; localIterator.hasNext(); i = zzc(paramzzajq.zza(localzzamk.a()), localzzamk.zzcqy()) + i) {
        localzzamk = (zzamk)localIterator.next();
      }
      int j = i;
      if (!paramzzaml.zzczf().isEmpty())
      {
        zzd(paramzzajq.zza(zzalz.zzcyz()), paramzzaml.zzczf());
        j = i + 1;
      }
      zzd(paramzzajq, zzame.zzczq());
      return j + 1;
    }
    zzd(paramzzajq, paramzzaml);
    return 1;
  }
  
  private static String zzc(zzajq paramzzajq)
  {
    if (paramzzajq.isEmpty()) {
      return "/";
    }
    return String.valueOf(paramzzajq.toString()).concat("/");
  }
  
  private void zzcri()
  {
    zzann.zzb(this.aZS, "Transaction expected to already be in progress.");
  }
  
  private static List<byte[]> zzd(byte[] paramArrayOfByte, int paramInt)
  {
    int j = (paramArrayOfByte.length - 1) / paramInt + 1;
    ArrayList localArrayList = new ArrayList(j);
    int i = 0;
    while (i < j)
    {
      int k = Math.min(paramInt, paramArrayOfByte.length - i * paramInt);
      byte[] arrayOfByte = new byte[k];
      System.arraycopy(paramArrayOfByte, i * paramInt, arrayOfByte, 0, k);
      localArrayList.add(arrayOfByte);
      i += 1;
    }
    return localArrayList;
  }
  
  private void zzd(zzajq paramzzajq, zzaml paramzzaml)
  {
    paramzzaml = zzbf(paramzzaml.getValue(true));
    if (paramzzaml.length >= 262144)
    {
      paramzzaml = zzd(paramzzaml, 262144);
      if (this.aZR.zzcyu())
      {
        localObject = this.aZR;
        i = paramzzaml.size();
        ((zzalw)localObject).zzi(45 + "Saving huge leaf node with " + i + " parts.", new Object[0]);
      }
      int i = 0;
      while (i < paramzzaml.size())
      {
        localObject = new ContentValues();
        ((ContentValues)localObject).put("path", zza(paramzzajq, i));
        ((ContentValues)localObject).put("value", (byte[])paramzzaml.get(i));
        this.aZQ.insertWithOnConflict("serverCache", null, (ContentValues)localObject, 5);
        i += 1;
      }
    }
    Object localObject = new ContentValues();
    ((ContentValues)localObject).put("path", zzc(paramzzajq));
    ((ContentValues)localObject).put("value", paramzzaml);
    this.aZQ.insertWithOnConflict("serverCache", null, (ContentValues)localObject, 5);
  }
  
  private String zzp(Collection<Long> paramCollection)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    paramCollection = paramCollection.iterator();
    for (int i = 1; paramCollection.hasNext(); i = 0)
    {
      long l = ((Long)paramCollection.next()).longValue();
      if (i == 0) {
        localStringBuilder.append(",");
      }
      localStringBuilder.append(l);
    }
    return localStringBuilder.toString();
  }
  
  private static String zzsd(String paramString)
  {
    assert (paramString.endsWith("/")) : "Path keys must end with a '/'";
    paramString = String.valueOf(paramString.substring(0, paramString.length() - 1));
    return String.valueOf(paramString).length() + 1 + paramString + '0';
  }
  
  public void beginTransaction()
  {
    if (!this.aZS) {}
    for (boolean bool = true;; bool = false)
    {
      zzann.zzb(bool, "runInTransaction called when an existing transaction is already in progress.");
      if (this.aZR.zzcyu()) {
        this.aZR.zzi("Starting transaction.", new Object[0]);
      }
      this.aZQ.beginTransaction();
      this.aZS = true;
      this.aZT = System.currentTimeMillis();
      return;
    }
  }
  
  public void endTransaction()
  {
    this.aZQ.endTransaction();
    this.aZS = false;
    long l1 = System.currentTimeMillis();
    long l2 = this.aZT;
    if (this.aZR.zzcyu()) {
      this.aZR.zzi(String.format("Transaction completed. Elapsed: %dms", new Object[] { Long.valueOf(l1 - l2) }), new Object[0]);
    }
  }
  
  public void setTransactionSuccessful()
  {
    this.aZQ.setTransactionSuccessful();
  }
  
  public zzaml zza(zzajq paramzzajq)
  {
    return zzb(paramzzajq);
  }
  
  public void zza(long paramLong, Set<zzalz> paramSet)
  {
    zzcri();
    long l1 = System.currentTimeMillis();
    this.aZQ.delete("trackedKeys", "id = ?", new String[] { String.valueOf(paramLong) });
    Iterator localIterator = paramSet.iterator();
    while (localIterator.hasNext())
    {
      zzalz localzzalz = (zzalz)localIterator.next();
      ContentValues localContentValues = new ContentValues();
      localContentValues.put("id", Long.valueOf(paramLong));
      localContentValues.put("key", localzzalz.asString());
      this.aZQ.insertWithOnConflict("trackedKeys", null, localContentValues, 5);
    }
    long l2 = System.currentTimeMillis();
    if (this.aZR.zzcyu()) {
      this.aZR.zzi(String.format("Set %d tracked query keys for tracked query %d in %dms", new Object[] { Integer.valueOf(paramSet.size()), Long.valueOf(paramLong), Long.valueOf(l2 - l1) }), new Object[0]);
    }
  }
  
  public void zza(long paramLong, Set<zzalz> paramSet1, Set<zzalz> paramSet2)
  {
    zzcri();
    long l1 = System.currentTimeMillis();
    Iterator localIterator = paramSet2.iterator();
    zzalz localzzalz;
    while (localIterator.hasNext())
    {
      localzzalz = (zzalz)localIterator.next();
      this.aZQ.delete("trackedKeys", "id = ? AND key = ?", new String[] { String.valueOf(paramLong), localzzalz.asString() });
    }
    localIterator = paramSet1.iterator();
    while (localIterator.hasNext())
    {
      localzzalz = (zzalz)localIterator.next();
      ContentValues localContentValues = new ContentValues();
      localContentValues.put("id", Long.valueOf(paramLong));
      localContentValues.put("key", localzzalz.asString());
      this.aZQ.insertWithOnConflict("trackedKeys", null, localContentValues, 5);
    }
    long l2 = System.currentTimeMillis();
    if (this.aZR.zzcyu()) {
      this.aZR.zzi(String.format("Updated tracked query keys (%d added, %d removed) for tracked query id %d in %dms", new Object[] { Integer.valueOf(paramSet1.size()), Integer.valueOf(paramSet2.size()), Long.valueOf(paramLong), Long.valueOf(l2 - l1) }), new Object[0]);
    }
  }
  
  public void zza(zzajq paramzzajq, zzajh paramzzajh)
  {
    zzcri();
    long l1 = System.currentTimeMillis();
    paramzzajh = paramzzajh.iterator();
    int j = 0;
    Map.Entry localEntry;
    for (int i = 0; paramzzajh.hasNext(); i = zzc(paramzzajq.zzh((zzajq)localEntry.getKey()), (zzaml)localEntry.getValue()) + i)
    {
      localEntry = (Map.Entry)paramzzajh.next();
      j += zza("serverCache", paramzzajq.zzh((zzajq)localEntry.getKey()));
    }
    long l2 = System.currentTimeMillis();
    if (this.aZR.zzcyu()) {
      this.aZR.zzi(String.format("Persisted a total of %d rows and deleted %d rows for a merge at %s in %dms", new Object[] { Integer.valueOf(i), Integer.valueOf(j), paramzzajq.toString(), Long.valueOf(l2 - l1) }), new Object[0]);
    }
  }
  
  public void zza(zzajq paramzzajq, zzajh paramzzajh, long paramLong)
  {
    zzcri();
    long l = System.currentTimeMillis();
    zza(paramzzajq, paramLong, "m", zzbf(paramzzajh.zzda(true)));
    paramLong = System.currentTimeMillis();
    if (this.aZR.zzcyu()) {
      this.aZR.zzi(String.format("Persisted user merge in %dms", new Object[] { Long.valueOf(paramLong - l) }), new Object[0]);
    }
  }
  
  public void zza(zzajq paramzzajq, zzakw paramzzakw)
  {
    if (!paramzzakw.zzcwz()) {}
    long l1;
    long l2;
    int j;
    int i;
    do
    {
      return;
      zzcri();
      l1 = System.currentTimeMillis();
      Object localObject3 = zza(paramzzajq, new String[] { "rowid", "path" });
      Object localObject2 = new zzakz(null);
      Object localObject1 = new zzakz(null);
      while (((Cursor)localObject3).moveToNext())
      {
        l2 = ((Cursor)localObject3).getLong(0);
        Object localObject4 = new zzajq(((Cursor)localObject3).getString(1));
        Object localObject5;
        String str;
        if (!paramzzajq.zzi((zzajq)localObject4))
        {
          localObject5 = this.aZR;
          str = String.valueOf(paramzzajq);
          localObject4 = String.valueOf(localObject4);
          ((zzalw)localObject5).warn(String.valueOf(str).length() + 67 + String.valueOf(localObject4).length() + "We are pruning at " + str + " but we have data stored higher up at " + (String)localObject4 + ". Ignoring.");
        }
        else
        {
          localObject5 = zzajq.zza(paramzzajq, (zzajq)localObject4);
          if (paramzzakw.zzw((zzajq)localObject5))
          {
            localObject2 = ((zzakz)localObject2).zzb((zzajq)localObject5, Long.valueOf(l2));
          }
          else if (paramzzakw.zzx((zzajq)localObject5))
          {
            localObject1 = ((zzakz)localObject1).zzb((zzajq)localObject5, Long.valueOf(l2));
          }
          else
          {
            localObject5 = this.aZR;
            str = String.valueOf(paramzzajq);
            localObject4 = String.valueOf(localObject4);
            ((zzalw)localObject5).warn(String.valueOf(str).length() + 88 + String.valueOf(localObject4).length() + "We are pruning at " + str + " and have data at " + (String)localObject4 + " that isn't marked for pruning or keeping. Ignoring.");
          }
        }
      }
      j = 0;
      i = 0;
      if (!((zzakz)localObject2).isEmpty())
      {
        localObject3 = new ArrayList();
        zza(paramzzajq, zzajq.zzcvg(), (zzakz)localObject2, (zzakz)localObject1, paramzzakw, (List)localObject3);
        paramzzakw = ((zzakz)localObject2).values();
        localObject1 = String.valueOf(zzp(paramzzakw));
        localObject1 = String.valueOf(localObject1).length() + 11 + "rowid IN (" + (String)localObject1 + ")";
        this.aZQ.delete("serverCache", (String)localObject1, null);
        localObject1 = ((List)localObject3).iterator();
        while (((Iterator)localObject1).hasNext())
        {
          localObject2 = (zzank)((Iterator)localObject1).next();
          zzc(paramzzajq.zzh((zzajq)((zzank)localObject2).getFirst()), (zzaml)((zzank)localObject2).A());
        }
        j = paramzzakw.size();
        i = ((List)localObject3).size();
      }
      l2 = System.currentTimeMillis();
    } while (!this.aZR.zzcyu());
    this.aZR.zzi(String.format("Pruned %d rows with %d nodes resaved in %dms", new Object[] { Integer.valueOf(j), Integer.valueOf(i), Long.valueOf(l2 - l1) }), new Object[0]);
  }
  
  public void zza(zzajq paramzzajq, zzaml paramzzaml)
  {
    zzcri();
    zza(paramzzajq, paramzzaml, false);
  }
  
  public void zza(zzajq paramzzajq, zzaml paramzzaml, long paramLong)
  {
    zzcri();
    long l = System.currentTimeMillis();
    zza(paramzzajq, paramLong, "o", zzbf(paramzzaml.getValue(true)));
    paramLong = System.currentTimeMillis();
    if (this.aZR.zzcyu()) {
      this.aZR.zzi(String.format("Persisted user overwrite in %dms", new Object[] { Long.valueOf(paramLong - l) }), new Object[0]);
    }
  }
  
  public void zza(zzakx paramzzakx)
  {
    zzcri();
    long l1 = System.currentTimeMillis();
    ContentValues localContentValues = new ContentValues();
    localContentValues.put("id", Long.valueOf(paramzzakx.id));
    localContentValues.put("path", zzc(paramzzakx.bgL.zzcrc()));
    localContentValues.put("queryParams", paramzzakx.bgL.zzcyh().zzcyf());
    localContentValues.put("lastUse", Long.valueOf(paramzzakx.bgM));
    localContentValues.put("complete", Boolean.valueOf(paramzzakx.bgN));
    localContentValues.put("active", Boolean.valueOf(paramzzakx.bgO));
    this.aZQ.insertWithOnConflict("trackedQueries", null, localContentValues, 5);
    long l2 = System.currentTimeMillis();
    if (this.aZR.zzcyu()) {
      this.aZR.zzi(String.format("Saved new tracked query in %dms", new Object[] { Long.valueOf(l2 - l1) }), new Object[0]);
    }
  }
  
  public void zzb(zzajq paramzzajq, zzaml paramzzaml)
  {
    zzcri();
    zza(paramzzajq, paramzzaml, true);
  }
  
  public void zzby(long paramLong)
  {
    zzcri();
    long l1 = System.currentTimeMillis();
    int i = this.aZQ.delete("writes", "id = ?", new String[] { String.valueOf(paramLong) });
    long l2 = System.currentTimeMillis();
    if (this.aZR.zzcyu()) {
      this.aZR.zzi(String.format("Deleted %d write(s) with writeId %d in %dms", new Object[] { Integer.valueOf(i), Long.valueOf(paramLong), Long.valueOf(l2 - l1) }), new Object[0]);
    }
  }
  
  public void zzbz(long paramLong)
  {
    zzcri();
    String str = String.valueOf(paramLong);
    this.aZQ.delete("trackedQueries", "id = ?", new String[] { str });
    this.aZQ.delete("trackedKeys", "id = ?", new String[] { str });
  }
  
  public void zzca(long paramLong)
  {
    zzcri();
    long l = System.currentTimeMillis();
    ContentValues localContentValues = new ContentValues();
    localContentValues.put("active", Boolean.valueOf(false));
    localContentValues.put("lastUse", Long.valueOf(paramLong));
    this.aZQ.updateWithOnConflict("trackedQueries", localContentValues, "active = 1", new String[0], 5);
    paramLong = System.currentTimeMillis();
    if (this.aZR.zzcyu()) {
      this.aZR.zzi(String.format("Reset active tracked queries in %dms", new Object[] { Long.valueOf(paramLong - l) }), new Object[0]);
    }
  }
  
  public Set<zzalz> zzcb(long paramLong)
  {
    return zzh(Collections.singleton(Long.valueOf(paramLong)));
  }
  
  public List<zzake> zzcre()
  {
    long l1 = System.currentTimeMillis();
    Cursor localCursor = this.aZQ.query("writes", new String[] { "id", "path", "type", "part", "node" }, null, null, null, null, "id, part");
    ArrayList localArrayList = new ArrayList();
    String str;
    for (;;)
    {
      zzajq localzzajq;
      try
      {
        if (!localCursor.moveToNext()) {
          break label364;
        }
        l2 = localCursor.getLong(0);
        localzzajq = new zzajq(localCursor.getString(1));
        str = localCursor.getString(2);
        if (localCursor.isNull(3))
        {
          Object localObject1 = localCursor.getBlob(4);
          localObject1 = zzane.zztc(new String((byte[])localObject1, vp));
          if (!"o".equals(str)) {
            break label280;
          }
          localObject1 = new zzake(l2, localzzajq, zzamm.zzbt(localObject1), true);
          localArrayList.add(localObject1);
          continue;
        }
        localObject3 = new ArrayList();
      }
      catch (IOException localIOException)
      {
        throw new RuntimeException("Failed to load writes", localIOException);
      }
      finally
      {
        localCursor.close();
      }
      do
      {
        ((List)localObject3).add(localCursor.getBlob(4));
      } while ((localCursor.moveToNext()) && (localCursor.getLong(0) == l2));
      localCursor.moveToPrevious();
      localObject3 = zzar((List)localObject3);
      continue;
      label280:
      if (!"m".equals(str)) {
        break;
      }
      localObject3 = new zzake(l2, localzzajq, zzajh.zzca((Map)localObject3));
    }
    Object localObject3 = String.valueOf(str);
    if (((String)localObject3).length() != 0) {}
    for (localObject3 = "Got invalid write type: ".concat((String)localObject3);; localObject3 = new String("Got invalid write type: ")) {
      throw new IllegalStateException((String)localObject3);
    }
    label364:
    long l2 = System.currentTimeMillis();
    if (this.aZR.zzcyu()) {
      this.aZR.zzi(String.format("Loaded %d writes in %dms", new Object[] { Integer.valueOf(localArrayList.size()), Long.valueOf(l2 - l1) }), new Object[0]);
    }
    localCursor.close();
    return localArrayList;
  }
  
  public long zzcrf()
  {
    Object localObject1 = String.format("SELECT sum(length(%s) + length(%s)) FROM %s", new Object[] { "value", "path", "serverCache" });
    localObject1 = this.aZQ.rawQuery((String)localObject1, null);
    try
    {
      if (((Cursor)localObject1).moveToFirst())
      {
        long l = ((Cursor)localObject1).getLong(0);
        return l;
      }
      throw new IllegalStateException("Couldn't read database result!");
    }
    finally
    {
      ((Cursor)localObject1).close();
    }
  }
  
  public List<zzakx> zzcrg()
  {
    long l1 = System.currentTimeMillis();
    Cursor localCursor = this.aZQ.query("trackedQueries", new String[] { "id", "path", "queryParams", "lastUse", "complete", "active" }, null, null, null, null, "id");
    ArrayList localArrayList = new ArrayList();
    for (;;)
    {
      try
      {
        if (localCursor.moveToNext())
        {
          l2 = localCursor.getLong(0);
          localObject2 = new zzajq(localCursor.getString(1));
          localObject3 = localCursor.getString(2);
        }
      }
      finally
      {
        try
        {
          Object localObject3 = zzane.zztb((String)localObject3);
          Object localObject2 = zzall.zzb((zzajq)localObject2, (Map)localObject3);
          long l3 = localCursor.getLong(3);
          if (localCursor.getInt(4) == 0) {
            break label288;
          }
          bool1 = true;
          if (localCursor.getInt(5) == 0) {
            break label294;
          }
          bool2 = true;
          localArrayList.add(new zzakx(l2, (zzall)localObject2, l3, bool1, bool2));
        }
        catch (IOException localIOException)
        {
          throw new RuntimeException(localIOException);
        }
        localObject1 = finally;
        localCursor.close();
      }
      long l2 = System.currentTimeMillis();
      if (this.aZR.zzcyu()) {
        this.aZR.zzi(String.format("Loaded %d tracked queries in %dms", new Object[] { Integer.valueOf(localIOException.size()), Long.valueOf(l2 - l1) }), new Object[0]);
      }
      localCursor.close();
      return localIOException;
      label288:
      boolean bool1 = false;
      continue;
      label294:
      boolean bool2 = false;
    }
  }
  
  public void zzcrh()
  {
    zzcri();
    long l1 = System.currentTimeMillis();
    int i = this.aZQ.delete("writes", null, null);
    long l2 = System.currentTimeMillis();
    if (this.aZR.zzcyu()) {
      this.aZR.zzi(String.format("Deleted %d (all) write(s) in %dms", new Object[] { Integer.valueOf(i), Long.valueOf(l2 - l1) }), new Object[0]);
    }
  }
  
  public Set<zzalz> zzh(Set<Long> paramSet)
  {
    long l1 = System.currentTimeMillis();
    Object localObject1 = String.valueOf("id IN (");
    Object localObject2 = String.valueOf(zzp(paramSet));
    localObject1 = String.valueOf(localObject1).length() + 1 + String.valueOf(localObject2).length() + (String)localObject1 + (String)localObject2 + ")";
    localObject1 = this.aZQ.query(true, "trackedKeys", new String[] { "key" }, (String)localObject1, null, null, null, null, null);
    localObject2 = new HashSet();
    try
    {
      while (((Cursor)localObject1).moveToNext()) {
        ((Set)localObject2).add(zzalz.zzsx(((Cursor)localObject1).getString(0)));
      }
      l2 = System.currentTimeMillis();
    }
    finally
    {
      ((Cursor)localObject1).close();
    }
    long l2;
    if (this.aZR.zzcyu()) {
      this.aZR.zzi(String.format("Loaded %d tracked queries keys for tracked queries %s in %dms", new Object[] { Integer.valueOf(((Set)localObject2).size()), paramSet.toString(), Long.valueOf(l2 - l1) }), new Object[0]);
    }
    ((Cursor)localObject1).close();
    return (Set<zzalz>)localObject2;
  }
  
  private static class zza
    extends SQLiteOpenHelper
  {
    static
    {
      if (!zzaif.class.desiredAssertionStatus()) {}
      for (boolean bool = true;; bool = false)
      {
        $assertionsDisabled = bool;
        return;
      }
    }
    
    public zza(Context paramContext, String paramString)
    {
      super(paramString, null, 2);
    }
    
    private void zzc(SQLiteDatabase paramSQLiteDatabase, String paramString)
    {
      paramString = String.valueOf(paramString);
      if (paramString.length() != 0) {}
      for (paramString = "DROP TABLE IF EXISTS ".concat(paramString);; paramString = new String("DROP TABLE IF EXISTS "))
      {
        paramSQLiteDatabase.execSQL(paramString);
        return;
      }
    }
    
    public void onCreate(SQLiteDatabase paramSQLiteDatabase)
    {
      paramSQLiteDatabase.execSQL("CREATE TABLE serverCache (path TEXT PRIMARY KEY, value BLOB);");
      paramSQLiteDatabase.execSQL("CREATE TABLE writes (id INTEGER, path TEXT, type TEXT, part INTEGER, node BLOB, UNIQUE (id, part));");
      paramSQLiteDatabase.execSQL("CREATE TABLE trackedQueries (id INTEGER PRIMARY KEY, path TEXT, queryParams TEXT, lastUse INTEGER, complete INTEGER, active INTEGER);");
      paramSQLiteDatabase.execSQL("CREATE TABLE trackedKeys (id INTEGER, key TEXT);");
    }
    
    public void onUpgrade(SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
    {
      assert (paramInt2 == 2) : "Why is onUpgrade() called with a different version?";
      if (paramInt1 <= 1)
      {
        zzc(paramSQLiteDatabase, "serverCache");
        paramSQLiteDatabase.execSQL("CREATE TABLE serverCache (path TEXT PRIMARY KEY, value BLOB);");
        zzc(paramSQLiteDatabase, "complete");
        paramSQLiteDatabase.execSQL("CREATE TABLE trackedKeys (id INTEGER, key TEXT);");
        paramSQLiteDatabase.execSQL("CREATE TABLE trackedQueries (id INTEGER PRIMARY KEY, path TEXT, queryParams TEXT, lastUse INTEGER, complete INTEGER, active INTEGER);");
        return;
      }
      throw new AssertionError(40 + "We don't handle upgrading to " + paramInt2);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaif.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */