package com.google.android.gms.internal;

import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class zzaig<K, V>
  extends zzaih<K, V>
{
  private final K[] aZZ;
  private final V[] baa;
  private final Comparator<K> bab;
  
  public zzaig(Comparator<K> paramComparator)
  {
    this.aZZ = new Object[0];
    this.baa = new Object[0];
    this.bab = paramComparator;
  }
  
  private zzaig(Comparator<K> paramComparator, K[] paramArrayOfK, V[] paramArrayOfV)
  {
    this.aZZ = paramArrayOfK;
    this.baa = paramArrayOfV;
    this.bab = paramComparator;
  }
  
  public static <A, B, C> zzaig<A, C> zza(List<A> paramList, Map<B, C> paramMap, zzaih.zza.zza<A, B> paramzza, Comparator<A> paramComparator)
  {
    Collections.sort(paramList, paramComparator);
    int i = paramList.size();
    Object[] arrayOfObject1 = new Object[i];
    Object[] arrayOfObject2 = new Object[i];
    i = 0;
    paramList = paramList.iterator();
    while (paramList.hasNext())
    {
      Object localObject = paramList.next();
      arrayOfObject1[i] = localObject;
      arrayOfObject2[i] = paramMap.get(paramzza.zzbk(localObject));
      i += 1;
    }
    return new zzaig(paramComparator, arrayOfObject1, arrayOfObject2);
  }
  
  public static <K, V> zzaig<K, V> zza(Map<K, V> paramMap, Comparator<K> paramComparator)
  {
    return zza(new ArrayList(paramMap.keySet()), paramMap, zzaih.zza.zzcrm(), paramComparator);
  }
  
  private static <T> T[] zza(T[] paramArrayOfT, int paramInt)
  {
    int i = paramArrayOfT.length - 1;
    Object[] arrayOfObject = new Object[i];
    System.arraycopy(paramArrayOfT, 0, arrayOfObject, 0, paramInt);
    System.arraycopy(paramArrayOfT, paramInt + 1, arrayOfObject, paramInt, i - paramInt);
    return arrayOfObject;
  }
  
  private static <T> T[] zza(T[] paramArrayOfT, int paramInt, T paramT)
  {
    int i = paramArrayOfT.length + 1;
    Object[] arrayOfObject = new Object[i];
    System.arraycopy(paramArrayOfT, 0, arrayOfObject, 0, paramInt);
    arrayOfObject[paramInt] = paramT;
    System.arraycopy(paramArrayOfT, paramInt, arrayOfObject, paramInt + 1, i - paramInt - 1);
    return arrayOfObject;
  }
  
  private static <T> T[] zzb(T[] paramArrayOfT, int paramInt, T paramT)
  {
    int i = paramArrayOfT.length;
    Object[] arrayOfObject = new Object[i];
    System.arraycopy(paramArrayOfT, 0, arrayOfObject, 0, i);
    arrayOfObject[paramInt] = paramT;
    return arrayOfObject;
  }
  
  private int zzbi(K paramK)
  {
    int i = 0;
    while ((i < this.aZZ.length) && (this.bab.compare(this.aZZ[i], paramK) < 0)) {
      i += 1;
    }
    return i;
  }
  
  private int zzbj(K paramK)
  {
    int i = 0;
    Object[] arrayOfObject = this.aZZ;
    int k = arrayOfObject.length;
    int j = 0;
    while (j < k)
    {
      Object localObject = arrayOfObject[j];
      if (this.bab.compare(paramK, localObject) == 0) {
        return i;
      }
      j += 1;
      i += 1;
    }
    return -1;
  }
  
  private Iterator<Map.Entry<K, V>> zze(final int paramInt, final boolean paramBoolean)
  {
    new Iterator()
    {
      int bac = paramInt;
      
      public boolean hasNext()
      {
        if (paramBoolean) {
          if (this.bac < 0) {}
        }
        while (this.bac < zzaig.zza(zzaig.this).length)
        {
          return true;
          return false;
        }
        return false;
      }
      
      public Map.Entry<K, V> next()
      {
        Object localObject1 = zzaig.zza(zzaig.this)[this.bac];
        Object localObject2 = zzaig.zzb(zzaig.this)[this.bac];
        if (paramBoolean) {}
        for (int i = this.bac - 1;; i = this.bac + 1)
        {
          this.bac = i;
          return new AbstractMap.SimpleImmutableEntry(localObject1, localObject2);
        }
      }
      
      public void remove()
      {
        throw new UnsupportedOperationException("Can't remove elements from ImmutableSortedMap");
      }
    };
  }
  
  public boolean containsKey(K paramK)
  {
    return zzbj(paramK) != -1;
  }
  
  public V get(K paramK)
  {
    int i = zzbj(paramK);
    if (i != -1) {
      return (V)this.baa[i];
    }
    return null;
  }
  
  public Comparator<K> getComparator()
  {
    return this.bab;
  }
  
  public boolean isEmpty()
  {
    return this.aZZ.length == 0;
  }
  
  public Iterator<Map.Entry<K, V>> iterator()
  {
    return zze(0, false);
  }
  
  public int size()
  {
    return this.aZZ.length;
  }
  
  public void zza(zzaim.zzb<K, V> paramzzb)
  {
    int i = 0;
    while (i < this.aZZ.length)
    {
      paramzzb.zzk(this.aZZ[i], this.baa[i]);
      i += 1;
    }
  }
  
  public zzaih<K, V> zzbg(K paramK)
  {
    int i = zzbj(paramK);
    if (i == -1) {
      return this;
    }
    paramK = zza(this.aZZ, i);
    Object[] arrayOfObject = zza(this.baa, i);
    return new zzaig(this.bab, paramK, arrayOfObject);
  }
  
  public K zzbh(K paramK)
  {
    int i = zzbj(paramK);
    if (i == -1) {
      throw new IllegalArgumentException("Can't find predecessor of nonexistent key");
    }
    if (i > 0) {
      return (K)this.aZZ[(i - 1)];
    }
    return null;
  }
  
  public K zzcrj()
  {
    if (this.aZZ.length > 0) {
      return (K)this.aZZ[0];
    }
    return null;
  }
  
  public K zzcrk()
  {
    if (this.aZZ.length > 0) {
      return (K)this.aZZ[(this.aZZ.length - 1)];
    }
    return null;
  }
  
  public Iterator<Map.Entry<K, V>> zzcrl()
  {
    return zze(this.aZZ.length - 1, true);
  }
  
  public zzaih<K, V> zzj(K paramK, V paramV)
  {
    int i = zzbj(paramK);
    if (i != -1)
    {
      if ((this.aZZ[i] == paramK) && (this.baa[i] == paramV)) {
        return this;
      }
      paramK = zzb(this.aZZ, i, paramK);
      paramV = zzb(this.baa, i, paramV);
      return new zzaig(this.bab, paramK, paramV);
    }
    if (this.aZZ.length > 25)
    {
      HashMap localHashMap = new HashMap(this.aZZ.length + 1);
      i = 0;
      while (i < this.aZZ.length)
      {
        localHashMap.put(this.aZZ[i], this.baa[i]);
        i += 1;
      }
      localHashMap.put(paramK, paramV);
      return zzaip.zzc(localHashMap, this.bab);
    }
    i = zzbi(paramK);
    paramK = zza(this.aZZ, i, paramK);
    paramV = zza(this.baa, i, paramV);
    return new zzaig(this.bab, paramK, paramV);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaig.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */