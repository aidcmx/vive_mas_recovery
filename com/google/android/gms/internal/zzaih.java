package com.google.android.gms.internal;

import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public abstract class zzaih<K, V>
  implements Iterable<Map.Entry<K, V>>
{
  public abstract boolean containsKey(K paramK);
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if (!(paramObject instanceof zzaih)) {
      return false;
    }
    Object localObject = (zzaih)paramObject;
    if (!getComparator().equals(((zzaih)localObject).getComparator())) {
      return false;
    }
    if (size() != ((zzaih)localObject).size()) {
      return false;
    }
    paramObject = iterator();
    localObject = ((zzaih)localObject).iterator();
    while (((Iterator)paramObject).hasNext()) {
      if (!((Map.Entry)((Iterator)paramObject).next()).equals(((Iterator)localObject).next())) {
        return false;
      }
    }
    return true;
  }
  
  public abstract V get(K paramK);
  
  public abstract Comparator<K> getComparator();
  
  public int hashCode()
  {
    int i = getComparator().hashCode();
    Iterator localIterator = iterator();
    while (localIterator.hasNext()) {
      i = ((Map.Entry)localIterator.next()).hashCode() + i * 31;
    }
    return i;
  }
  
  public abstract boolean isEmpty();
  
  public abstract Iterator<Map.Entry<K, V>> iterator();
  
  public abstract int size();
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(getClass().getSimpleName());
    localStringBuilder.append("{");
    Iterator localIterator = iterator();
    int i = 1;
    if (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      if (i != 0) {
        i = 0;
      }
      for (;;)
      {
        localStringBuilder.append("(");
        localStringBuilder.append(localEntry.getKey());
        localStringBuilder.append("=>");
        localStringBuilder.append(localEntry.getValue());
        localStringBuilder.append(")");
        break;
        localStringBuilder.append(", ");
      }
    }
    localStringBuilder.append("};");
    return localStringBuilder.toString();
  }
  
  public abstract void zza(zzaim.zzb<K, V> paramzzb);
  
  public abstract zzaih<K, V> zzbg(K paramK);
  
  public abstract K zzbh(K paramK);
  
  public abstract K zzcrj();
  
  public abstract K zzcrk();
  
  public abstract Iterator<Map.Entry<K, V>> zzcrl();
  
  public abstract zzaih<K, V> zzj(K paramK, V paramV);
  
  public static class zza
  {
    private static final zza bag = new zza()
    {
      public Object zzbk(Object paramAnonymousObject)
      {
        return paramAnonymousObject;
      }
    };
    
    public static <K, V> zzaih<K, V> zza(Comparator<K> paramComparator)
    {
      return new zzaig(paramComparator);
    }
    
    public static <A, B, C> zzaih<A, C> zzb(List<A> paramList, Map<B, C> paramMap, zza<A, B> paramzza, Comparator<A> paramComparator)
    {
      if (paramList.size() < 25) {
        return zzaig.zza(paramList, paramMap, paramzza, paramComparator);
      }
      return zzaip.zzc(paramList, paramMap, paramzza, paramComparator);
    }
    
    public static <A, B> zzaih<A, B> zzb(Map<A, B> paramMap, Comparator<A> paramComparator)
    {
      if (paramMap.size() < 25) {
        return zzaig.zza(paramMap, paramComparator);
      }
      return zzaip.zzc(paramMap, paramComparator);
    }
    
    public static <A> zza<A, A> zzcrm()
    {
      return bag;
    }
    
    public static abstract interface zza<C, D>
    {
      public abstract D zzbk(C paramC);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaih.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */