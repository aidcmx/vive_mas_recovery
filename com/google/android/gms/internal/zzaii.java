package com.google.android.gms.internal;

import java.util.AbstractMap.SimpleEntry;
import java.util.Comparator;
import java.util.EmptyStackException;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.Stack;

public class zzaii<K, V>
  implements Iterator<Map.Entry<K, V>>
{
  private final Stack<zzaio<K, V>> bah = new Stack();
  private final boolean bai;
  
  zzaii(zzaim<K, V> paramzzaim, K paramK, Comparator<K> paramComparator, boolean paramBoolean)
  {
    this.bai = paramBoolean;
    for (;;)
    {
      if (!paramzzaim.isEmpty())
      {
        int i;
        if (paramK != null) {
          if (paramBoolean) {
            i = paramComparator.compare(paramK, paramzzaim.getKey());
          }
        }
        for (;;)
        {
          if (i < 0)
          {
            if (paramBoolean)
            {
              paramzzaim = paramzzaim.zzcrs();
              break;
              i = paramComparator.compare(paramzzaim.getKey(), paramK);
              continue;
              i = 1;
              continue;
            }
            paramzzaim = paramzzaim.zzcrt();
            break;
          }
        }
        if (i == 0) {
          this.bah.push((zzaio)paramzzaim);
        }
      }
      else
      {
        return;
      }
      this.bah.push((zzaio)paramzzaim);
      if (paramBoolean) {
        paramzzaim = paramzzaim.zzcrt();
      } else {
        paramzzaim = paramzzaim.zzcrs();
      }
    }
  }
  
  public boolean hasNext()
  {
    return this.bah.size() > 0;
  }
  
  public Map.Entry<K, V> next()
  {
    try
    {
      Object localObject = (zzaio)this.bah.pop();
      AbstractMap.SimpleEntry localSimpleEntry = new AbstractMap.SimpleEntry(((zzaio)localObject).getKey(), ((zzaio)localObject).getValue());
      if (this.bai) {
        for (localObject = ((zzaio)localObject).zzcrs(); !((zzaim)localObject).isEmpty(); localObject = ((zzaim)localObject).zzcrt()) {
          this.bah.push((zzaio)localObject);
        }
      }
      for (localObject = ((zzaio)localObject).zzcrt(); !((zzaim)localObject).isEmpty(); localObject = ((zzaim)localObject).zzcrs()) {
        this.bah.push((zzaio)localObject);
      }
      return localSimpleEntry;
    }
    catch (EmptyStackException localEmptyStackException)
    {
      throw new NoSuchElementException();
    }
  }
  
  public void remove()
  {
    throw new UnsupportedOperationException("remove called on immutable collection");
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaii.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */