package com.google.android.gms.internal;

import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

public class zzaij<T>
  implements Iterable<T>
{
  private final zzaih<T, Void> baj;
  
  private zzaij(zzaih<T, Void> paramzzaih)
  {
    this.baj = paramzzaih;
  }
  
  public zzaij(List<T> paramList, Comparator<T> paramComparator)
  {
    this.baj = zzaih.zza.zzb(paramList, Collections.emptyMap(), zzaih.zza.zzcrm(), paramComparator);
  }
  
  public Iterator<T> iterator()
  {
    return new zza(this.baj.iterator());
  }
  
  public zzaij<T> zzbl(T paramT)
  {
    paramT = this.baj.zzbg(paramT);
    if (paramT == this.baj) {
      return this;
    }
    return new zzaij(paramT);
  }
  
  public zzaij<T> zzbm(T paramT)
  {
    return new zzaij(this.baj.zzj(paramT, null));
  }
  
  public T zzbn(T paramT)
  {
    return (T)this.baj.zzbh(paramT);
  }
  
  public Iterator<T> zzcrl()
  {
    return new zza(this.baj.zzcrl());
  }
  
  public T zzcrn()
  {
    return (T)this.baj.zzcrj();
  }
  
  public T zzcro()
  {
    return (T)this.baj.zzcrk();
  }
  
  private static class zza<T>
    implements Iterator<T>
  {
    final Iterator<Map.Entry<T, Void>> bak;
    
    public zza(Iterator<Map.Entry<T, Void>> paramIterator)
    {
      this.bak = paramIterator;
    }
    
    public boolean hasNext()
    {
      return this.bak.hasNext();
    }
    
    public T next()
    {
      return (T)((Map.Entry)this.bak.next()).getKey();
    }
    
    public void remove()
    {
      this.bak.remove();
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaij.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */