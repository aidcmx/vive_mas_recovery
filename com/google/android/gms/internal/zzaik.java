package com.google.android.gms.internal;

public class zzaik<K, V>
  extends zzaio<K, V>
{
  zzaik(K paramK, V paramV, zzaim<K, V> paramzzaim1, zzaim<K, V> paramzzaim2)
  {
    super(paramK, paramV, paramzzaim1, paramzzaim2);
  }
  
  protected zzaio<K, V> zza(K paramK, V paramV, zzaim<K, V> paramzzaim1, zzaim<K, V> paramzzaim2)
  {
    Object localObject = paramK;
    if (paramK == null) {
      localObject = getKey();
    }
    paramK = paramV;
    if (paramV == null) {
      paramK = getValue();
    }
    paramV = paramzzaim1;
    if (paramzzaim1 == null) {
      paramV = zzcrs();
    }
    paramzzaim1 = paramzzaim2;
    if (paramzzaim2 == null) {
      paramzzaim1 = zzcrt();
    }
    return new zzaik(localObject, paramK, paramV, paramzzaim1);
  }
  
  protected zzaim.zza zzcrp()
  {
    return zzaim.zza.ban;
  }
  
  public boolean zzcrq()
  {
    return false;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaik.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */