package com.google.android.gms.internal;

import java.util.Comparator;

public class zzail<K, V>
  implements zzaim<K, V>
{
  private static final zzail bal = new zzail();
  
  public static <K, V> zzail<K, V> zzcrr()
  {
    return bal;
  }
  
  public K getKey()
  {
    return null;
  }
  
  public V getValue()
  {
    return null;
  }
  
  public boolean isEmpty()
  {
    return true;
  }
  
  public zzaim<K, V> zza(K paramK, V paramV, zzaim.zza paramzza, zzaim<K, V> paramzzaim1, zzaim<K, V> paramzzaim2)
  {
    return this;
  }
  
  public zzaim<K, V> zza(K paramK, V paramV, Comparator<K> paramComparator)
  {
    return new zzain(paramK, paramV);
  }
  
  public zzaim<K, V> zza(K paramK, Comparator<K> paramComparator)
  {
    return this;
  }
  
  public void zza(zzaim.zzb<K, V> paramzzb) {}
  
  public boolean zzcrq()
  {
    return false;
  }
  
  public zzaim<K, V> zzcrs()
  {
    return this;
  }
  
  public zzaim<K, V> zzcrt()
  {
    return this;
  }
  
  public zzaim<K, V> zzcru()
  {
    return this;
  }
  
  public zzaim<K, V> zzcrv()
  {
    return this;
  }
  
  public int zzcrw()
  {
    return 0;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzail.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */