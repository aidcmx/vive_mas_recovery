package com.google.android.gms.internal;

import java.util.Comparator;

public abstract interface zzaim<K, V>
{
  public abstract K getKey();
  
  public abstract V getValue();
  
  public abstract boolean isEmpty();
  
  public abstract zzaim<K, V> zza(K paramK, V paramV, zza paramzza, zzaim<K, V> paramzzaim1, zzaim<K, V> paramzzaim2);
  
  public abstract zzaim<K, V> zza(K paramK, V paramV, Comparator<K> paramComparator);
  
  public abstract zzaim<K, V> zza(K paramK, Comparator<K> paramComparator);
  
  public abstract void zza(zzb<K, V> paramzzb);
  
  public abstract boolean zzcrq();
  
  public abstract zzaim<K, V> zzcrs();
  
  public abstract zzaim<K, V> zzcrt();
  
  public abstract zzaim<K, V> zzcru();
  
  public abstract zzaim<K, V> zzcrv();
  
  public abstract int zzcrw();
  
  public static enum zza
  {
    private zza() {}
  }
  
  public static abstract class zzb<K, V>
  {
    public abstract void zzk(K paramK, V paramV);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaim.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */