package com.google.android.gms.internal;

public class zzain<K, V>
  extends zzaio<K, V>
{
  zzain(K paramK, V paramV)
  {
    super(paramK, paramV, zzail.zzcrr(), zzail.zzcrr());
  }
  
  zzain(K paramK, V paramV, zzaim<K, V> paramzzaim1, zzaim<K, V> paramzzaim2)
  {
    super(paramK, paramV, paramzzaim1, paramzzaim2);
  }
  
  protected zzaio<K, V> zza(K paramK, V paramV, zzaim<K, V> paramzzaim1, zzaim<K, V> paramzzaim2)
  {
    Object localObject = paramK;
    if (paramK == null) {
      localObject = getKey();
    }
    paramK = paramV;
    if (paramV == null) {
      paramK = getValue();
    }
    paramV = paramzzaim1;
    if (paramzzaim1 == null) {
      paramV = zzcrs();
    }
    paramzzaim1 = paramzzaim2;
    if (paramzzaim2 == null) {
      paramzzaim1 = zzcrt();
    }
    return new zzain(localObject, paramK, paramV, paramzzaim1);
  }
  
  protected zzaim.zza zzcrp()
  {
    return zzaim.zza.bam;
  }
  
  public boolean zzcrq()
  {
    return true;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzain.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */