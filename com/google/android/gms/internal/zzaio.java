package com.google.android.gms.internal;

import java.util.Comparator;

public abstract class zzaio<K, V>
  implements zzaim<K, V>
{
  private final K bap;
  private zzaim<K, V> baq;
  private final zzaim<K, V> bar;
  private final V value;
  
  zzaio(K paramK, V paramV, zzaim<K, V> paramzzaim1, zzaim<K, V> paramzzaim2)
  {
    this.bap = paramK;
    this.value = paramV;
    paramK = paramzzaim1;
    if (paramzzaim1 == null) {
      paramK = zzail.zzcrr();
    }
    this.baq = paramK;
    paramK = paramzzaim2;
    if (paramzzaim2 == null) {
      paramK = zzail.zzcrr();
    }
    this.bar = paramK;
  }
  
  private static zzaim.zza zza(zzaim paramzzaim)
  {
    if (paramzzaim.zzcrq()) {
      return zzaim.zza.ban;
    }
    return zzaim.zza.bam;
  }
  
  private zzaim<K, V> zzcrx()
  {
    if (this.baq.isEmpty()) {
      return zzail.zzcrr();
    }
    zzaio localzzaio = this;
    if (!zzcrs().zzcrq())
    {
      localzzaio = this;
      if (!zzcrs().zzcrs().zzcrq()) {
        localzzaio = zzcry();
      }
    }
    return localzzaio.zza(null, null, ((zzaio)localzzaio.baq).zzcrx(), null).zzcsa();
  }
  
  private zzaio<K, V> zzcry()
  {
    zzaio localzzaio = zzcsd();
    if (localzzaio.zzcrt().zzcrs().zzcrq()) {
      return localzzaio.zza(null, null, null, ((zzaio)localzzaio.zzcrt()).zzcsc()).zzcsb().zzcsd();
    }
    return localzzaio;
  }
  
  private zzaio<K, V> zzcrz()
  {
    zzaio localzzaio2 = zzcsd();
    zzaio localzzaio1 = localzzaio2;
    if (localzzaio2.zzcrs().zzcrs().zzcrq()) {
      localzzaio1 = localzzaio2.zzcsc().zzcsd();
    }
    return localzzaio1;
  }
  
  private zzaio<K, V> zzcsa()
  {
    Object localObject2 = this;
    if (this.bar.zzcrq())
    {
      localObject2 = this;
      if (!this.baq.zzcrq()) {
        localObject2 = zzcsb();
      }
    }
    Object localObject1 = localObject2;
    if (((zzaio)localObject2).baq.zzcrq())
    {
      localObject1 = localObject2;
      if (((zzaio)((zzaio)localObject2).baq).baq.zzcrq()) {
        localObject1 = ((zzaio)localObject2).zzcsc();
      }
    }
    localObject2 = localObject1;
    if (((zzaio)localObject1).baq.zzcrq())
    {
      localObject2 = localObject1;
      if (((zzaio)localObject1).bar.zzcrq()) {
        localObject2 = ((zzaio)localObject1).zzcsd();
      }
    }
    return (zzaio<K, V>)localObject2;
  }
  
  private zzaio<K, V> zzcsb()
  {
    zzaio localzzaio = (zzaio)zza(null, null, zzaim.zza.bam, null, ((zzaio)this.bar).baq);
    return (zzaio)this.bar.zza(null, null, zzcrp(), localzzaio, null);
  }
  
  private zzaio<K, V> zzcsc()
  {
    zzaio localzzaio = (zzaio)zza(null, null, zzaim.zza.bam, ((zzaio)this.baq).bar, null);
    return (zzaio)this.baq.zza(null, null, zzcrp(), null, localzzaio);
  }
  
  private zzaio<K, V> zzcsd()
  {
    zzaim localzzaim1 = this.baq.zza(null, null, zza(this.baq), null, null);
    zzaim localzzaim2 = this.bar.zza(null, null, zza(this.bar), null, null);
    return (zzaio)zza(null, null, zza(this), localzzaim1, localzzaim2);
  }
  
  public K getKey()
  {
    return (K)this.bap;
  }
  
  public V getValue()
  {
    return (V)this.value;
  }
  
  public boolean isEmpty()
  {
    return false;
  }
  
  public zzaim<K, V> zza(K paramK, V paramV, Comparator<K> paramComparator)
  {
    int i = paramComparator.compare(paramK, this.bap);
    if (i < 0) {
      paramK = zza(null, null, this.baq.zza(paramK, paramV, paramComparator), null);
    }
    for (;;)
    {
      return paramK.zzcsa();
      if (i == 0) {
        paramK = zza(paramK, paramV, null, null);
      } else {
        paramK = zza(null, null, null, this.bar.zza(paramK, paramV, paramComparator));
      }
    }
  }
  
  public zzaim<K, V> zza(K paramK, Comparator<K> paramComparator)
  {
    Object localObject1;
    if (paramComparator.compare(paramK, this.bap) < 0)
    {
      localObject1 = this;
      if (!this.baq.isEmpty())
      {
        localObject1 = this;
        if (!this.baq.zzcrq())
        {
          localObject1 = this;
          if (!((zzaio)this.baq).baq.zzcrq()) {
            localObject1 = zzcry();
          }
        }
      }
    }
    Object localObject2;
    for (paramK = ((zzaio)localObject1).zza(null, null, ((zzaio)localObject1).baq.zza(paramK, paramComparator), null);; paramK = ((zzaio)localObject2).zza(null, null, null, ((zzaio)localObject2).bar.zza(paramK, paramComparator)))
    {
      return paramK.zzcsa();
      localObject2 = this;
      if (this.baq.zzcrq()) {
        localObject2 = zzcsc();
      }
      localObject1 = localObject2;
      if (!((zzaio)localObject2).bar.isEmpty())
      {
        localObject1 = localObject2;
        if (!((zzaio)localObject2).bar.zzcrq())
        {
          localObject1 = localObject2;
          if (!((zzaio)((zzaio)localObject2).bar).baq.zzcrq()) {
            localObject1 = ((zzaio)localObject2).zzcrz();
          }
        }
      }
      localObject2 = localObject1;
      if (paramComparator.compare(paramK, ((zzaio)localObject1).bap) == 0)
      {
        if (((zzaio)localObject1).bar.isEmpty()) {
          return zzail.zzcrr();
        }
        localObject2 = ((zzaio)localObject1).bar.zzcru();
        localObject2 = ((zzaio)localObject1).zza(((zzaim)localObject2).getKey(), ((zzaim)localObject2).getValue(), null, ((zzaio)((zzaio)localObject1).bar).zzcrx());
      }
    }
  }
  
  protected abstract zzaio<K, V> zza(K paramK, V paramV, zzaim<K, V> paramzzaim1, zzaim<K, V> paramzzaim2);
  
  public void zza(zzaim.zzb<K, V> paramzzb)
  {
    this.baq.zza(paramzzb);
    paramzzb.zzk(this.bap, this.value);
    this.bar.zza(paramzzb);
  }
  
  public zzaio<K, V> zzb(K paramK, V paramV, zzaim.zza paramzza, zzaim<K, V> paramzzaim1, zzaim<K, V> paramzzaim2)
  {
    Object localObject = paramK;
    if (paramK == null) {
      localObject = this.bap;
    }
    paramK = paramV;
    if (paramV == null) {
      paramK = this.value;
    }
    paramV = paramzzaim1;
    if (paramzzaim1 == null) {
      paramV = this.baq;
    }
    paramzzaim1 = paramzzaim2;
    if (paramzzaim2 == null) {
      paramzzaim1 = this.bar;
    }
    if (paramzza == zzaim.zza.bam) {
      return new zzain(localObject, paramK, paramV, paramzzaim1);
    }
    return new zzaik(localObject, paramK, paramV, paramzzaim1);
  }
  
  void zzb(zzaim<K, V> paramzzaim)
  {
    this.baq = paramzzaim;
  }
  
  protected abstract zzaim.zza zzcrp();
  
  public zzaim<K, V> zzcrs()
  {
    return this.baq;
  }
  
  public zzaim<K, V> zzcrt()
  {
    return this.bar;
  }
  
  public zzaim<K, V> zzcru()
  {
    if (this.baq.isEmpty()) {
      return this;
    }
    return this.baq.zzcru();
  }
  
  public zzaim<K, V> zzcrv()
  {
    if (this.bar.isEmpty()) {
      return this;
    }
    return this.bar.zzcrv();
  }
  
  public int zzcrw()
  {
    return this.baq.zzcrw() + 1 + this.bar.zzcrw();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaio.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */