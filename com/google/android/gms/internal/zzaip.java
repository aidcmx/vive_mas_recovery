package com.google.android.gms.internal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class zzaip<K, V>
  extends zzaih<K, V>
{
  private Comparator<K> bab;
  private zzaim<K, V> bas;
  
  private zzaip(zzaim<K, V> paramzzaim, Comparator<K> paramComparator)
  {
    this.bas = paramzzaim;
    this.bab = paramComparator;
  }
  
  private zzaim<K, V> zzbo(K paramK)
  {
    zzaim localzzaim = this.bas;
    while (!localzzaim.isEmpty())
    {
      int i = this.bab.compare(paramK, localzzaim.getKey());
      if (i < 0)
      {
        localzzaim = localzzaim.zzcrs();
      }
      else
      {
        if (i == 0) {
          return localzzaim;
        }
        localzzaim = localzzaim.zzcrt();
      }
    }
    return null;
  }
  
  public static <A, B, C> zzaip<A, C> zzc(List<A> paramList, Map<B, C> paramMap, zzaih.zza.zza<A, B> paramzza, Comparator<A> paramComparator)
  {
    return zza.zzc(paramList, paramMap, paramzza, paramComparator);
  }
  
  public static <A, B> zzaip<A, B> zzc(Map<A, B> paramMap, Comparator<A> paramComparator)
  {
    return zza.zzc(new ArrayList(paramMap.keySet()), paramMap, zzaih.zza.zzcrm(), paramComparator);
  }
  
  public boolean containsKey(K paramK)
  {
    return zzbo(paramK) != null;
  }
  
  public V get(K paramK)
  {
    paramK = zzbo(paramK);
    if (paramK != null) {
      return (V)paramK.getValue();
    }
    return null;
  }
  
  public Comparator<K> getComparator()
  {
    return this.bab;
  }
  
  public boolean isEmpty()
  {
    return this.bas.isEmpty();
  }
  
  public Iterator<Map.Entry<K, V>> iterator()
  {
    return new zzaii(this.bas, null, this.bab, false);
  }
  
  public int size()
  {
    return this.bas.zzcrw();
  }
  
  public void zza(zzaim.zzb<K, V> paramzzb)
  {
    this.bas.zza(paramzzb);
  }
  
  public zzaih<K, V> zzbg(K paramK)
  {
    if (!containsKey(paramK)) {
      return this;
    }
    return new zzaip(this.bas.zza(paramK, this.bab).zza(null, null, zzaim.zza.ban, null, null), this.bab);
  }
  
  public K zzbh(K paramK)
  {
    Object localObject3 = null;
    Object localObject1 = this.bas;
    Object localObject2 = null;
    while (!((zzaim)localObject1).isEmpty())
    {
      int i = this.bab.compare(paramK, ((zzaim)localObject1).getKey());
      if (i == 0)
      {
        if (!((zzaim)localObject1).zzcrs().isEmpty())
        {
          for (paramK = ((zzaim)localObject1).zzcrs(); !paramK.zzcrt().isEmpty(); paramK = paramK.zzcrt()) {}
          paramK = paramK.getKey();
        }
        do
        {
          return paramK;
          paramK = (K)localObject3;
        } while (localObject2 == null);
        return (K)((zzaim)localObject2).getKey();
      }
      if (i < 0)
      {
        localObject1 = ((zzaim)localObject1).zzcrs();
      }
      else
      {
        zzaim localzzaim = ((zzaim)localObject1).zzcrt();
        localObject2 = localObject1;
        localObject1 = localzzaim;
      }
    }
    paramK = String.valueOf(paramK);
    throw new IllegalArgumentException(String.valueOf(paramK).length() + 50 + "Couldn't find predecessor key of non-present key: " + paramK);
  }
  
  public K zzcrj()
  {
    return (K)this.bas.zzcru().getKey();
  }
  
  public K zzcrk()
  {
    return (K)this.bas.zzcrv().getKey();
  }
  
  public Iterator<Map.Entry<K, V>> zzcrl()
  {
    return new zzaii(this.bas, null, this.bab, true);
  }
  
  public zzaih<K, V> zzj(K paramK, V paramV)
  {
    return new zzaip(this.bas.zza(paramK, paramV, this.bab).zza(null, null, zzaim.zza.ban, null, null), this.bab);
  }
  
  private static class zza<A, B, C>
  {
    private final List<A> bat;
    private final zzaih.zza.zza<A, B> bau;
    private zzaio<A, C> bav;
    private zzaio<A, C> baw;
    private final Map<B, C> values;
    
    private zza(List<A> paramList, Map<B, C> paramMap, zzaih.zza.zza<A, B> paramzza)
    {
      this.bat = paramList;
      this.values = paramMap;
      this.bau = paramzza;
    }
    
    private void zza(zzaim.zza paramzza, int paramInt1, int paramInt2)
    {
      zzaim localzzaim = zzab(paramInt2 + 1, paramInt1 - 1);
      Object localObject = this.bat.get(paramInt2);
      if (paramzza == zzaim.zza.bam) {}
      for (paramzza = new zzain(localObject, zzbp(localObject), null, localzzaim); this.bav == null; paramzza = new zzaik(localObject, zzbp(localObject), null, localzzaim))
      {
        this.bav = paramzza;
        this.baw = paramzza;
        return;
      }
      this.baw.zzb(paramzza);
      this.baw = paramzza;
    }
    
    private zzaim<A, C> zzab(int paramInt1, int paramInt2)
    {
      if (paramInt2 == 0) {
        return zzail.zzcrr();
      }
      if (paramInt2 == 1)
      {
        localObject1 = this.bat.get(paramInt1);
        return new zzaik(localObject1, zzbp(localObject1), null, null);
      }
      paramInt2 /= 2;
      int i = paramInt1 + paramInt2;
      Object localObject1 = zzab(paramInt1, paramInt2);
      zzaim localzzaim = zzab(i + 1, paramInt2);
      Object localObject2 = this.bat.get(i);
      return new zzaik(localObject2, zzbp(localObject2), (zzaim)localObject1, localzzaim);
    }
    
    private C zzbp(A paramA)
    {
      return (C)this.values.get(this.bau.zzbk(paramA));
    }
    
    public static <A, B, C> zzaip<A, C> zzc(List<A> paramList, Map<B, C> paramMap, zzaih.zza.zza<A, B> paramzza, Comparator<A> paramComparator)
    {
      paramMap = new zza(paramList, paramMap, paramzza);
      Collections.sort(paramList, paramComparator);
      paramzza = new zza(paramList.size()).iterator();
      int i = paramList.size();
      if (paramzza.hasNext())
      {
        paramList = (zzb)paramzza.next();
        i -= paramList.baA;
        if (paramList.baz) {
          paramMap.zza(zzaim.zza.ban, paramList.baA, i);
        }
        for (;;)
        {
          break;
          paramMap.zza(zzaim.zza.ban, paramList.baA, i);
          i -= paramList.baA;
          paramMap.zza(zzaim.zza.bam, paramList.baA, i);
        }
      }
      if (paramMap.bav == null) {}
      for (paramList = zzail.zzcrr();; paramList = paramMap.bav) {
        return new zzaip(paramList, paramComparator, null);
      }
    }
    
    static class zza
      implements Iterable<zzaip.zza.zzb>
    {
      private final int length;
      private long value;
      
      public zza(int paramInt)
      {
        paramInt += 1;
        this.length = ((int)Math.floor(Math.log(paramInt) / Math.log(2.0D)));
        long l = Math.pow(2.0D, this.length);
        this.value = (paramInt & l - 1L);
      }
      
      public Iterator<zzaip.zza.zzb> iterator()
      {
        new Iterator()
        {
          private int bax = zzaip.zza.zza.zza(zzaip.zza.zza.this) - 1;
          
          public boolean hasNext()
          {
            return this.bax >= 0;
          }
          
          public void remove() {}
          
          public zzaip.zza.zzb zzcse()
          {
            boolean bool = true;
            long l1 = zzaip.zza.zza.zzb(zzaip.zza.zza.this);
            long l2 = 1 << this.bax;
            zzaip.zza.zzb localzzb = new zzaip.zza.zzb();
            if ((l1 & l2) == 0L) {}
            for (;;)
            {
              localzzb.baz = bool;
              localzzb.baA = ((int)Math.pow(2.0D, this.bax));
              this.bax -= 1;
              return localzzb;
              bool = false;
            }
          }
        };
      }
    }
    
    static class zzb
    {
      public int baA;
      public boolean baz;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaip.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */