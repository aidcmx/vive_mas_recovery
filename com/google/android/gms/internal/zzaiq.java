package com.google.android.gms.internal;

import java.util.Comparator;

public class zzaiq<A extends Comparable<A>>
  implements Comparator<A>
{
  private static zzaiq baB = new zzaiq();
  
  public static <T extends Comparable<T>> zzaiq<T> zzi(Class<T> paramClass)
  {
    return baB;
  }
  
  public int zza(A paramA1, A paramA2)
  {
    return paramA1.compareTo(paramA2);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaiq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */