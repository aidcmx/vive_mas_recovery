package com.google.android.gms.internal;

import java.util.Collections;
import java.util.List;

public class zzair
{
  private final List<List<String>> baC;
  private final List<String> baD;
  
  public zzair(List<List<String>> paramList, List<String> paramList1)
  {
    if (paramList.size() != paramList1.size() - 1) {
      throw new IllegalArgumentException("Number of posts need to be n-1 for n hashes in CompoundHash");
    }
    this.baC = paramList;
    this.baD = paramList1;
  }
  
  public List<List<String>> zzcsf()
  {
    return Collections.unmodifiableList(this.baC);
  }
  
  public List<String> zzcsg()
  {
    return Collections.unmodifiableList(this.baD);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzair.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */