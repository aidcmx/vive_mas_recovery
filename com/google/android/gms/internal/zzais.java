package com.google.android.gms.internal;

import java.util.HashMap;
import java.util.Map;

class zzais
  implements zzajc.zza
{
  private static long baE = 0L;
  private final zzalw aZR;
  private zzaiw baF;
  private zzajc baG;
  private zza baH;
  private zzc baI;
  
  public zzais(zzaiu paramzzaiu, zzaiw paramzzaiw, String paramString1, zza paramzza, String paramString2)
  {
    long l = baE;
    baE = 1L + l;
    this.baF = paramzzaiw;
    this.baH = paramzza;
    this.aZR = new zzalw(paramzzaiu.zzcsh(), "Connection", 25 + "conn_" + l);
    this.baI = zzc.baM;
    this.baG = new zzajc(paramzzaiu, paramzzaiw, paramString1, this, paramString2);
  }
  
  private void zzb(Map<String, Object> paramMap, boolean paramBoolean)
  {
    if (this.baI != zzc.baN)
    {
      this.aZR.zzi("Tried to send on an unconnected connection", new Object[0]);
      return;
    }
    if (paramBoolean) {
      this.aZR.zzi("Sending data (contents hidden)", new Object[0]);
    }
    for (;;)
    {
      this.baG.send(paramMap);
      return;
      this.aZR.zzi("Sending data: %s", new Object[] { paramMap });
    }
  }
  
  private void zzbt(Map<String, Object> paramMap)
  {
    zzalw localzzalw;
    if (this.aZR.zzcyu())
    {
      localzzalw = this.aZR;
      str = String.valueOf(paramMap.toString());
      if (str.length() == 0) {
        break label57;
      }
    }
    label57:
    for (String str = "received data message: ".concat(str);; str = new String("received data message: "))
    {
      localzzalw.zzi(str, new Object[0]);
      this.baH.zzbt(paramMap);
      return;
    }
  }
  
  private void zzbu(Map<String, Object> paramMap)
  {
    Object localObject2;
    if (this.aZR.zzcyu())
    {
      localObject2 = this.aZR;
      localObject1 = String.valueOf(paramMap.toString());
      if (((String)localObject1).length() == 0) {
        break label87;
      }
      localObject1 = "Got control message: ".concat((String)localObject1);
    }
    for (;;)
    {
      ((zzalw)localObject2).zzi((String)localObject1, new Object[0]);
      try
      {
        localObject2 = (String)paramMap.get("t");
        if (localObject2 == null) {
          break label259;
        }
        if (((String)localObject2).equals("s"))
        {
          zzse((String)paramMap.get("d"));
          return;
          label87:
          localObject1 = new String("Got control message: ");
        }
        else
        {
          if (!((String)localObject2).equals("r")) {
            break label177;
          }
          zzsf((String)paramMap.get("d"));
          return;
        }
      }
      catch (ClassCastException paramMap)
      {
        if (!this.aZR.zzcyu()) {
          break label172;
        }
      }
    }
    Object localObject1 = this.aZR;
    paramMap = String.valueOf(paramMap.toString());
    if (paramMap.length() != 0) {}
    for (paramMap = "Failed to parse control message: ".concat(paramMap);; paramMap = new String("Failed to parse control message: "))
    {
      ((zzalw)localObject1).zzi(paramMap, new Object[0]);
      label172:
      close();
      return;
      label177:
      if (((String)localObject2).equals("h"))
      {
        zzbv((Map)paramMap.get("d"));
        return;
      }
      if (!this.aZR.zzcyu()) {
        break;
      }
      localObject1 = this.aZR;
      paramMap = String.valueOf(localObject2);
      if (paramMap.length() != 0) {}
      for (paramMap = "Ignoring unknown control message: ".concat(paramMap);; paramMap = new String("Ignoring unknown control message: "))
      {
        ((zzalw)localObject1).zzi(paramMap, new Object[0]);
        return;
      }
      label259:
      if (this.aZR.zzcyu())
      {
        localObject1 = this.aZR;
        paramMap = String.valueOf(paramMap.toString());
        if (paramMap.length() == 0) {
          break label310;
        }
      }
      label310:
      for (paramMap = "Got invalid control message: ".concat(paramMap);; paramMap = new String("Got invalid control message: "))
      {
        ((zzalw)localObject1).zzi(paramMap, new Object[0]);
        close();
        return;
      }
    }
  }
  
  private void zzbv(Map<String, Object> paramMap)
  {
    long l = ((Long)paramMap.get("ts")).longValue();
    String str = (String)paramMap.get("h");
    this.baH.zzsg(str);
    paramMap = (String)paramMap.get("s");
    if (this.baI == zzc.baM)
    {
      this.baG.start();
      zzi(l, paramMap);
    }
  }
  
  private void zzi(long paramLong, String paramString)
  {
    if (this.aZR.zzcyu()) {
      this.aZR.zzi("realtime connection established", new Object[0]);
    }
    this.baI = zzc.baN;
    this.baH.zzj(paramLong, paramString);
  }
  
  private void zzse(String paramString)
  {
    if (this.aZR.zzcyu()) {
      this.aZR.zzi("Connection shutdown command received. Shutting down...", new Object[0]);
    }
    this.baH.zzsh(paramString);
    close();
  }
  
  private void zzsf(String paramString)
  {
    if (this.aZR.zzcyu())
    {
      zzalw localzzalw = this.aZR;
      String str = String.valueOf(this.baF.getHost());
      localzzalw.zzi(String.valueOf(str).length() + 62 + String.valueOf(paramString).length() + "Got a reset; killing connection to " + str + "; Updating internalHost to " + paramString, new Object[0]);
    }
    this.baH.zzsg(paramString);
    zza(zzb.baJ);
  }
  
  public void close()
  {
    zza(zzb.baK);
  }
  
  public void open()
  {
    if (this.aZR.zzcyu()) {
      this.aZR.zzi("Opening a connection", new Object[0]);
    }
    this.baG.open();
  }
  
  public void zza(zzb paramzzb)
  {
    if (this.baI != zzc.baO)
    {
      if (this.aZR.zzcyu()) {
        this.aZR.zzi("closing realtime connection", new Object[0]);
      }
      this.baI = zzc.baO;
      if (this.baG != null)
      {
        this.baG.close();
        this.baG = null;
      }
      this.baH.zzb(paramzzb);
    }
  }
  
  public void zza(Map<String, Object> paramMap, boolean paramBoolean)
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put("t", "d");
    localHashMap.put("d", paramMap);
    zzb(localHashMap, paramBoolean);
  }
  
  public void zzbs(Map<String, Object> paramMap)
  {
    for (;;)
    {
      String str;
      try
      {
        str = (String)paramMap.get("t");
        if (str == null) {
          break label175;
        }
        if (str.equals("d"))
        {
          zzbt((Map)paramMap.get("d"));
          return;
        }
        if (str.equals("c"))
        {
          zzbu((Map)paramMap.get("d"));
          return;
        }
      }
      catch (ClassCastException paramMap)
      {
        if (this.aZR.zzcyu())
        {
          localzzalw = this.aZR;
          paramMap = String.valueOf(paramMap.toString());
          if (paramMap.length() == 0) {
            break label241;
          }
          paramMap = "Failed to parse server message: ".concat(paramMap);
          localzzalw.zzi(paramMap, new Object[0]);
        }
        close();
        return;
      }
      if (!this.aZR.zzcyu()) {
        break;
      }
      zzalw localzzalw = this.aZR;
      paramMap = String.valueOf(str);
      if (paramMap.length() != 0) {}
      for (paramMap = "Ignoring unknown server message type: ".concat(paramMap);; paramMap = new String("Ignoring unknown server message type: "))
      {
        localzzalw.zzi(paramMap, new Object[0]);
        return;
      }
      label175:
      if (this.aZR.zzcyu())
      {
        localzzalw = this.aZR;
        paramMap = String.valueOf(paramMap.toString());
        if (paramMap.length() == 0) {
          break label227;
        }
      }
      label227:
      for (paramMap = "Failed to parse server message: missing message type:".concat(paramMap);; paramMap = new String("Failed to parse server message: missing message type:"))
      {
        localzzalw.zzi(paramMap, new Object[0]);
        close();
        return;
      }
      label241:
      paramMap = new String("Failed to parse server message: ");
    }
  }
  
  public void zzcw(boolean paramBoolean)
  {
    this.baG = null;
    if ((!paramBoolean) && (this.baI == zzc.baM)) {
      if (this.aZR.zzcyu()) {
        this.aZR.zzi("Realtime connection failed", new Object[0]);
      }
    }
    for (;;)
    {
      close();
      return;
      if (this.aZR.zzcyu()) {
        this.aZR.zzi("Realtime connection lost", new Object[0]);
      }
    }
  }
  
  public static abstract interface zza
  {
    public abstract void zzb(zzais.zzb paramzzb);
    
    public abstract void zzbt(Map<String, Object> paramMap);
    
    public abstract void zzj(long paramLong, String paramString);
    
    public abstract void zzsg(String paramString);
    
    public abstract void zzsh(String paramString);
  }
  
  public static enum zzb
  {
    private zzb() {}
  }
  
  private static enum zzc
  {
    private zzc() {}
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzais.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */