package com.google.android.gms.internal;

public abstract interface zzait
{
  public abstract void zza(boolean paramBoolean, zza paramzza);
  
  public static abstract interface zza
  {
    public abstract void onError(String paramString);
    
    public abstract void zzsi(String paramString);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzait.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */