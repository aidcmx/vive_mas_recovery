package com.google.android.gms.internal;

import java.util.concurrent.ScheduledExecutorService;

public class zzaiu
{
  private final ScheduledExecutorService aZE;
  private final zzait baQ;
  private final zzalx baR;
  private final boolean baS;
  private final String baT;
  private final String baU;
  
  public zzaiu(zzalx paramzzalx, zzait paramzzait, ScheduledExecutorService paramScheduledExecutorService, boolean paramBoolean, String paramString1, String paramString2)
  {
    this.baR = paramzzalx;
    this.baQ = paramzzait;
    this.aZE = paramScheduledExecutorService;
    this.baS = paramBoolean;
    this.baT = paramString1;
    this.baU = paramString2;
  }
  
  public zzalx zzcsh()
  {
    return this.baR;
  }
  
  public zzait zzcsi()
  {
    return this.baQ;
  }
  
  public ScheduledExecutorService zzcsj()
  {
    return this.aZE;
  }
  
  public boolean zzcsk()
  {
    return this.baS;
  }
  
  public String zzcsl()
  {
    return this.baT;
  }
  
  public String zzux()
  {
    return this.baU;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaiu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */