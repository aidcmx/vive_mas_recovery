package com.google.android.gms.internal;

import java.net.URI;

public class zzaiw
{
  private final String EY;
  private final String baV;
  private final boolean baW;
  
  public zzaiw(String paramString1, String paramString2, boolean paramBoolean)
  {
    this.baV = paramString1;
    this.EY = paramString2;
    this.baW = paramBoolean;
  }
  
  public static URI zza(String paramString1, boolean paramBoolean, String paramString2, String paramString3)
  {
    if (paramBoolean) {}
    for (String str1 = "wss";; str1 = "ws")
    {
      String str2 = String.valueOf("v");
      String str3 = String.valueOf("5");
      paramString2 = String.valueOf(str1).length() + 13 + String.valueOf(paramString1).length() + String.valueOf(paramString2).length() + String.valueOf(str2).length() + String.valueOf(str3).length() + str1 + "://" + paramString1 + "/.ws?ns=" + paramString2 + "&" + str2 + "=" + str3;
      paramString1 = paramString2;
      if (paramString3 != null)
      {
        paramString1 = String.valueOf(paramString2);
        paramString2 = String.valueOf("&ls=");
        paramString1 = String.valueOf(paramString1).length() + 0 + String.valueOf(paramString2).length() + String.valueOf(paramString3).length() + paramString1 + paramString2 + paramString3;
      }
      return URI.create(paramString1);
    }
  }
  
  public String getHost()
  {
    return this.baV;
  }
  
  public String getNamespace()
  {
    return this.EY;
  }
  
  public boolean isSecure()
  {
    return this.baW;
  }
  
  public String toString()
  {
    if (this.baW) {}
    for (String str1 = "s";; str1 = "")
    {
      String str2 = this.baV;
      return String.valueOf(str1).length() + 7 + String.valueOf(str2).length() + "http" + str1 + "://" + str2;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaiw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */