package com.google.android.gms.internal;

import java.util.List;
import java.util.Map;

public abstract interface zzaiy
{
  public abstract void initialize();
  
  public abstract void interrupt(String paramString);
  
  public abstract boolean isInterrupted(String paramString);
  
  public abstract void purgeOutstandingWrites();
  
  public abstract void refreshAuthToken();
  
  public abstract void resume(String paramString);
  
  public abstract void shutdown();
  
  public abstract void zza(List<String> paramList, zzajb paramzzajb);
  
  public abstract void zza(List<String> paramList, Object paramObject, zzajb paramzzajb);
  
  public abstract void zza(List<String> paramList, Object paramObject, String paramString, zzajb paramzzajb);
  
  public abstract void zza(List<String> paramList, Map<String, Object> paramMap);
  
  public abstract void zza(List<String> paramList, Map<String, Object> paramMap, zzaix paramzzaix, Long paramLong, zzajb paramzzajb);
  
  public abstract void zza(List<String> paramList, Map<String, Object> paramMap, zzajb paramzzajb);
  
  public abstract void zzb(List<String> paramList, Object paramObject, zzajb paramzzajb);
  
  public abstract void zzb(List<String> paramList, Map<String, Object> paramMap, zzajb paramzzajb);
  
  public abstract void zzsk(String paramString);
  
  public static abstract interface zza
  {
    public abstract void onDisconnect();
    
    public abstract void zza(List<String> paramList, Object paramObject, boolean paramBoolean, Long paramLong);
    
    public abstract void zza(List<String> paramList, List<zzaja> paramList1, Long paramLong);
    
    public abstract void zzbw(Map<String, Object> paramMap);
    
    public abstract void zzcsp();
    
    public abstract void zzcy(boolean paramBoolean);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaiy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */