package com.google.android.gms.internal;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class zzaiz
  implements zzais.zza, zzaiy
{
  private static long baE;
  private final ScheduledExecutorService aZE;
  private final zzalw aZR;
  private final zzaiw baF;
  private final zzait baQ;
  private final zzaiy.zza baX;
  private String baY;
  private HashSet<String> baZ = new HashSet();
  private boolean bba = true;
  private long bbb;
  private zzais bbc;
  private zzb bbd = zzb.bbD;
  private long bbe = 0L;
  private long bbf = 0L;
  private Map<Long, zza> bbg;
  private List<zzd> bbh;
  private Map<Long, zzf> bbi;
  private Map<zzc, zze> bbj;
  private String bbk;
  private boolean bbl;
  private final zzaiu bbm;
  private final zzajd bbn;
  private String bbo;
  private long bbp = 0L;
  private int bbq = 0;
  private ScheduledFuture<?> bbr = null;
  private long bbs;
  private boolean bbt;
  
  static
  {
    if (!zzaiz.class.desiredAssertionStatus()) {}
    for (boolean bool = true;; bool = false)
    {
      $assertionsDisabled = bool;
      baE = 0L;
      return;
    }
  }
  
  public zzaiz(zzaiu paramzzaiu, zzaiw paramzzaiw, zzaiy.zza paramzza)
  {
    this.baX = paramzza;
    this.bbm = paramzzaiu;
    this.aZE = paramzzaiu.zzcsj();
    this.baQ = paramzzaiu.zzcsi();
    this.baF = paramzzaiw;
    this.bbj = new HashMap();
    this.bbg = new HashMap();
    this.bbi = new HashMap();
    this.bbh = new ArrayList();
    this.bbn = new zzajd.zza(this.aZE, paramzzaiu.zzcsh(), "ConnectionRetryHelper").zzcg(1000L).zzj(1.3D).zzch(30000L).zzk(0.7D).zzcty();
    long l = baE;
    baE = 1L + l;
    this.aZR = new zzalw(paramzzaiu.zzcsh(), "PersistentConnection", 23 + "pc_" + l);
    this.bbo = null;
    zzctc();
  }
  
  private boolean isIdle()
  {
    return (this.bbj.isEmpty()) && (this.bbg.isEmpty()) && (!this.bbt) && (this.bbi.isEmpty());
  }
  
  private zze zza(zzc paramzzc)
  {
    if (this.aZR.zzcyu())
    {
      localObject = this.aZR;
      String str = String.valueOf(paramzzc);
      ((zzalw)localObject).zzi(String.valueOf(str).length() + 15 + "removing query " + str, new Object[0]);
    }
    if (!this.bbj.containsKey(paramzzc))
    {
      if (this.aZR.zzcyu())
      {
        localObject = this.aZR;
        paramzzc = String.valueOf(paramzzc);
        ((zzalw)localObject).zzi(String.valueOf(paramzzc).length() + 64 + "Trying to remove listener for QuerySpec " + paramzzc + " but no listener exists.", new Object[0]);
      }
      return null;
    }
    Object localObject = (zze)this.bbj.get(paramzzc);
    this.bbj.remove(paramzzc);
    zzctc();
    return (zze)localObject;
  }
  
  private Map<String, Object> zza(List<String> paramList, Object paramObject, String paramString)
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put("p", zzaiv.zzas(paramList));
    localHashMap.put("d", paramObject);
    if (paramString != null) {
      localHashMap.put("h", paramString);
    }
    return localHashMap;
  }
  
  private void zza(zze paramzze)
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put("p", zzaiv.zzas(zzc.zzb(zze.zzc(paramzze))));
    Long localLong = paramzze.zzcth();
    if (localLong != null)
    {
      localHashMap.put("q", zzc.zzc(paramzze.zzctg()));
      localHashMap.put("t", localLong);
    }
    zza("n", localHashMap, null);
  }
  
  private void zza(String paramString, List<String> paramList, Object paramObject, final zzajb paramzzajb)
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put("p", zzaiv.zzas(paramList));
    localHashMap.put("d", paramObject);
    zza(paramString, localHashMap, new zza()
    {
      public void zzbz(Map<String, Object> paramAnonymousMap)
      {
        String str1 = null;
        String str2 = (String)paramAnonymousMap.get("s");
        if (!str2.equals("ok")) {
          str1 = (String)paramAnonymousMap.get("d");
        }
        for (paramAnonymousMap = str2;; paramAnonymousMap = null)
        {
          if (paramzzajb != null) {
            paramzzajb.zzbn(paramAnonymousMap, str1);
          }
          return;
        }
      }
    });
  }
  
  private void zza(String paramString1, List<String> paramList, Object paramObject, String paramString2, zzajb paramzzajb)
  {
    paramList = zza(paramList, paramObject, paramString2);
    long l = this.bbe;
    this.bbe = (1L + l);
    this.bbi.put(Long.valueOf(l), new zzf(paramString1, paramList, paramzzajb, null));
    if (zzcsr()) {
      zzcd(l);
    }
    this.bbs = System.currentTimeMillis();
    zzctc();
  }
  
  private void zza(String paramString, Map<String, Object> paramMap, zza paramzza)
  {
    zza(paramString, false, paramMap, paramzza);
  }
  
  private void zza(String paramString, boolean paramBoolean, Map<String, Object> paramMap, zza paramzza)
  {
    long l = zzctb();
    HashMap localHashMap = new HashMap();
    localHashMap.put("r", Long.valueOf(l));
    localHashMap.put("a", paramString);
    localHashMap.put("b", paramMap);
    this.bbc.zza(localHashMap, paramBoolean);
    this.bbg.put(Long.valueOf(l), paramzza);
  }
  
  private void zza(List<String> paramList, zzc paramzzc)
  {
    if (paramList.contains("no_index"))
    {
      paramList = String.valueOf(zzc.zzc(paramzzc).get("i"));
      paramList = String.valueOf(paramList).length() + 14 + "\".indexOn\": \"" + paramList + "\"";
      zzalw localzzalw = this.aZR;
      paramzzc = String.valueOf(zzaiv.zzas(zzc.zzb(paramzzc)));
      localzzalw.warn(String.valueOf(paramList).length() + 118 + String.valueOf(paramzzc).length() + "Using an unspecified index. Consider adding '" + paramList + "' at " + paramzzc + " to your security and Firebase Database rules for better performance");
    }
  }
  
  private Collection<zze> zzat(List<String> paramList)
  {
    if (this.aZR.zzcyu())
    {
      localObject1 = this.aZR;
      localObject2 = String.valueOf(paramList);
      ((zzalw)localObject1).zzi(String.valueOf(localObject2).length() + 29 + "removing all listens at path " + (String)localObject2, new Object[0]);
    }
    Object localObject1 = new ArrayList();
    Object localObject2 = this.bbj.entrySet().iterator();
    while (((Iterator)localObject2).hasNext())
    {
      Object localObject3 = (Map.Entry)((Iterator)localObject2).next();
      zzc localzzc = (zzc)((Map.Entry)localObject3).getKey();
      localObject3 = (zze)((Map.Entry)localObject3).getValue();
      if (zzc.zzb(localzzc).equals(paramList)) {
        ((List)localObject1).add(localObject3);
      }
    }
    paramList = ((List)localObject1).iterator();
    while (paramList.hasNext())
    {
      localObject2 = (zze)paramList.next();
      this.bbj.remove(((zze)localObject2).zzctg());
    }
    zzctc();
    return (Collection<zze>)localObject1;
  }
  
  private void zzau(List<String> paramList)
  {
    paramList = zzat(paramList);
    if (paramList != null)
    {
      paramList = paramList.iterator();
      while (paramList.hasNext()) {
        zze.zzd((zze)paramList.next()).zzbn("permission_denied", null);
      }
    }
  }
  
  private void zzb(final zze paramzze)
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put("p", zzaiv.zzas(zzc.zzb(paramzze.zzctg())));
    Object localObject1 = paramzze.zzcth();
    if (localObject1 != null)
    {
      localHashMap.put("q", zzc.zzc(zze.zzc(paramzze)));
      localHashMap.put("t", localObject1);
    }
    localObject1 = paramzze.zzcti();
    localHashMap.put("h", ((zzaix)localObject1).zzcsm());
    if (((zzaix)localObject1).zzcsn())
    {
      localObject1 = ((zzaix)localObject1).zzcso();
      ArrayList localArrayList = new ArrayList();
      Object localObject2 = ((zzair)localObject1).zzcsf().iterator();
      while (((Iterator)localObject2).hasNext()) {
        localArrayList.add(zzaiv.zzas((List)((Iterator)localObject2).next()));
      }
      localObject2 = new HashMap();
      ((Map)localObject2).put("hs", ((zzair)localObject1).zzcsg());
      ((Map)localObject2).put("ps", localArrayList);
      localHashMap.put("ch", localObject2);
    }
    zza("q", localHashMap, new zza()
    {
      public void zzbz(Map<String, Object> paramAnonymousMap)
      {
        String str = (String)paramAnonymousMap.get("s");
        if (str.equals("ok"))
        {
          Object localObject = (Map)paramAnonymousMap.get("d");
          if (((Map)localObject).containsKey("w"))
          {
            localObject = (List)((Map)localObject).get("w");
            zzaiz.zza(zzaiz.this, (List)localObject, zzaiz.zze.zzc(paramzze));
          }
        }
        if ((zzaiz.zze)zzaiz.zzo(zzaiz.this).get(paramzze.zzctg()) == paramzze)
        {
          if (!str.equals("ok"))
          {
            zzaiz.zza(zzaiz.this, paramzze.zzctg());
            paramAnonymousMap = (String)paramAnonymousMap.get("d");
            zzaiz.zze.zzd(paramzze).zzbn(str, paramAnonymousMap);
          }
        }
        else {
          return;
        }
        zzaiz.zze.zzd(paramzze).zzbn(null, null);
      }
    });
  }
  
  private void zzbm(String paramString1, String paramString2)
  {
    this.aZR.warn(String.valueOf(paramString1).length() + 23 + String.valueOf(paramString2).length() + "Auth token revoked: " + paramString1 + " (" + paramString2 + ")");
    this.bbk = null;
    this.bbl = true;
    this.baX.zzcy(false);
    this.bbc.close();
  }
  
  private void zzbx(Map<String, Object> paramMap)
  {
    this.aZR.info((String)paramMap.get("msg"));
  }
  
  private void zzby(Map<String, Integer> paramMap)
  {
    if (!paramMap.isEmpty())
    {
      localHashMap = new HashMap();
      localHashMap.put("c", paramMap);
      zza("s", localHashMap, new zza()
      {
        public void zzbz(Map<String, Object> paramAnonymousMap)
        {
          String str = (String)paramAnonymousMap.get("s");
          if (!str.equals("ok"))
          {
            paramAnonymousMap = (String)paramAnonymousMap.get("d");
            if (zzaiz.zza(zzaiz.this).zzcyu()) {
              zzaiz.zza(zzaiz.this).zzi(String.valueOf(str).length() + 34 + String.valueOf(paramAnonymousMap).length() + "Failed to send stats: " + str + " (message: " + paramAnonymousMap + ")", new Object[0]);
            }
          }
        }
      });
    }
    while (!this.aZR.zzcyu())
    {
      HashMap localHashMap;
      return;
    }
    this.aZR.zzi("Not sending stats because stats are empty", new Object[0]);
  }
  
  private void zzcc(long paramLong)
  {
    if (this.aZR.zzcyu()) {
      this.aZR.zzi("handling timestamp", new Object[0]);
    }
    long l = System.currentTimeMillis();
    HashMap localHashMap = new HashMap();
    localHashMap.put("serverTimeOffset", Long.valueOf(paramLong - l));
    this.baX.zzbw(localHashMap);
  }
  
  private void zzcd(final long paramLong)
  {
    assert (zzcsr()) : "sendPut called when we can't send writes (we're disconnected or writes are paused).";
    zzf localzzf = (zzf)this.bbi.get(Long.valueOf(paramLong));
    final zzajb localzzajb = localzzf.zzctf();
    final String str = localzzf.getAction();
    localzzf.zzctk();
    zza(str, localzzf.zzctj(), new zza()
    {
      public void zzbz(Map<String, Object> paramAnonymousMap)
      {
        Object localObject;
        if (zzaiz.zza(zzaiz.this).zzcyu())
        {
          localObject = zzaiz.zza(zzaiz.this);
          String str1 = str;
          String str2 = String.valueOf(paramAnonymousMap);
          ((zzalw)localObject).zzi(String.valueOf(str1).length() + 11 + String.valueOf(str2).length() + str1 + " response: " + str2, new Object[0]);
        }
        if ((zzaiz.zzf)zzaiz.zzm(zzaiz.this).get(Long.valueOf(paramLong)) == localzzajb)
        {
          zzaiz.zzm(zzaiz.this).remove(Long.valueOf(paramLong));
          if (this.bby != null)
          {
            localObject = (String)paramAnonymousMap.get("s");
            if (!((String)localObject).equals("ok")) {
              break label186;
            }
            this.bby.zzbn(null, null);
          }
        }
        for (;;)
        {
          zzaiz.zzn(zzaiz.this);
          return;
          label186:
          paramAnonymousMap = (String)paramAnonymousMap.get("d");
          this.bby.zzbn((String)localObject, paramAnonymousMap);
          continue;
          if (zzaiz.zza(zzaiz.this).zzcyu())
          {
            paramAnonymousMap = zzaiz.zza(zzaiz.this);
            long l = paramLong;
            paramAnonymousMap.zzi(81 + "Ignoring on complete for put " + l + " because it was removed already.", new Object[0]);
          }
        }
      }
    });
  }
  
  private boolean zzcsq()
  {
    return (this.bbd == zzb.bbG) || (this.bbd == zzb.bbH);
  }
  
  private boolean zzcsr()
  {
    return this.bbd == zzb.bbH;
  }
  
  private void zzcst()
  {
    if (zzcss()) {
      if (this.bbd != zzb.bbD) {
        break label78;
      }
    }
    label78:
    for (final boolean bool = true;; bool = false)
    {
      zzaiv.zzc(bool, "Not in disconnected state: %s", new Object[] { this.bbd });
      bool = this.bbl;
      this.aZR.zzi("Scheduling connection attempt", new Object[0]);
      this.bbl = false;
      this.bbn.zzr(new Runnable()
      {
        public void run()
        {
          zzaiz.zza(zzaiz.this).zzi("Trying to fetch auth token", new Object[0]);
          if (zzaiz.zzb(zzaiz.this) == zzaiz.zzb.bbD) {}
          for (boolean bool = true;; bool = false)
          {
            zzaiv.zzc(bool, "Not in disconnected state: %s", new Object[] { zzaiz.zzb(zzaiz.this) });
            zzaiz.zza(zzaiz.this, zzaiz.zzb.bbE);
            zzaiz.zzc(zzaiz.this);
            final long l = zzaiz.zzd(zzaiz.this);
            zzaiz.zzf(zzaiz.this).zza(bool, new zzait.zza()
            {
              public void onError(String paramAnonymous2String)
              {
                if (l == zzaiz.zzd(zzaiz.this))
                {
                  zzaiz.zza(zzaiz.this, zzaiz.zzb.bbD);
                  zzalw localzzalw = zzaiz.zza(zzaiz.this);
                  paramAnonymous2String = String.valueOf(paramAnonymous2String);
                  if (paramAnonymous2String.length() != 0) {}
                  for (paramAnonymous2String = "Error fetching token: ".concat(paramAnonymous2String);; paramAnonymous2String = new String("Error fetching token: "))
                  {
                    localzzalw.zzi(paramAnonymous2String, new Object[0]);
                    zzaiz.zze(zzaiz.this);
                    return;
                  }
                }
                zzaiz.zza(zzaiz.this).zzi("Ignoring getToken error, because this was not the latest attempt.", new Object[0]);
              }
              
              public void zzsi(String paramAnonymous2String)
              {
                if (l == zzaiz.zzd(zzaiz.this))
                {
                  if (zzaiz.zzb(zzaiz.this) == zzaiz.zzb.bbE)
                  {
                    zzaiz.zza(zzaiz.this).zzi("Successfully fetched token, opening connection", new Object[0]);
                    zzaiz.this.zzsl(paramAnonymous2String);
                    return;
                  }
                  if (zzaiz.zzb(zzaiz.this) == zzaiz.zzb.bbD) {}
                  for (boolean bool = true;; bool = false)
                  {
                    zzaiv.zzc(bool, "Expected connection state disconnected, but was %s", new Object[] { zzaiz.zzb(zzaiz.this) });
                    zzaiz.zza(zzaiz.this).zzi("Not opening connection after token refresh, because connection was set to disconnected", new Object[0]);
                    return;
                  }
                }
                zzaiz.zza(zzaiz.this).zzi("Ignoring getToken result, because this was not the latest attempt.", new Object[0]);
              }
            });
            return;
          }
        }
      });
      return;
    }
  }
  
  private void zzcsu()
  {
    Iterator localIterator = this.bbi.entrySet().iterator();
    while (localIterator.hasNext())
    {
      zzf localzzf = (zzf)((Map.Entry)localIterator.next()).getValue();
      if ((localzzf.zzctj().containsKey("h")) && (localzzf.zzctl()))
      {
        localzzf.zzctf().zzbn("disconnected", null);
        localIterator.remove();
      }
    }
  }
  
  private void zzcsv()
  {
    zzcz(false);
  }
  
  private void zzcsw()
  {
    zzcz(true);
  }
  
  private void zzcsx()
  {
    zzaiv.zzc(zzcsq(), "Must be connected to send unauth.", new Object[0]);
    if (this.bbk == null) {}
    for (boolean bool = true;; bool = false)
    {
      zzaiv.zzc(bool, "Auth token must not be set.", new Object[0]);
      zza("unauth", Collections.emptyMap(), null);
      return;
    }
  }
  
  private void zzcsy()
  {
    if (this.aZR.zzcyu()) {
      this.aZR.zzi("calling restore state", new Object[0]);
    }
    if (this.bbd == zzb.bbF) {}
    for (boolean bool = true;; bool = false)
    {
      zzaiv.zzc(bool, "Wanted to restore auth, but was in wrong state: %s", new Object[] { this.bbd });
      if (this.bbk != null) {
        break;
      }
      if (this.aZR.zzcyu()) {
        this.aZR.zzi("Not restoring auth because token is null.", new Object[0]);
      }
      this.bbd = zzb.bbH;
      zzcsz();
      return;
    }
    if (this.aZR.zzcyu()) {
      this.aZR.zzi("Restoring auth.", new Object[0]);
    }
    this.bbd = zzb.bbG;
    zzcsw();
  }
  
  private void zzcsz()
  {
    if (this.bbd == zzb.bbH) {}
    Object localObject2;
    for (boolean bool = true;; bool = false)
    {
      zzaiv.zzc(bool, "Should be connected if we're restoring state, but we are: %s", new Object[] { this.bbd });
      if (this.aZR.zzcyu()) {
        this.aZR.zzi("Restoring outstanding listens", new Object[0]);
      }
      localObject1 = this.bbj.values().iterator();
      while (((Iterator)localObject1).hasNext())
      {
        localObject2 = (zze)((Iterator)localObject1).next();
        if (this.aZR.zzcyu())
        {
          zzalw localzzalw = this.aZR;
          String str = String.valueOf(((zze)localObject2).zzctg());
          localzzalw.zzi(String.valueOf(str).length() + 17 + "Restoring listen " + str, new Object[0]);
        }
        zzb((zze)localObject2);
      }
    }
    if (this.aZR.zzcyu()) {
      this.aZR.zzi("Restoring writes.", new Object[0]);
    }
    Object localObject1 = new ArrayList(this.bbi.keySet());
    Collections.sort((List)localObject1);
    localObject1 = ((ArrayList)localObject1).iterator();
    while (((Iterator)localObject1).hasNext()) {
      zzcd(((Long)((Iterator)localObject1).next()).longValue());
    }
    localObject1 = this.bbh.iterator();
    while (((Iterator)localObject1).hasNext())
    {
      localObject2 = (zzd)((Iterator)localObject1).next();
      zza(((zzd)localObject2).getAction(), ((zzd)localObject2).zzcte(), ((zzd)localObject2).getData(), ((zzd)localObject2).zzctf());
    }
    this.bbh.clear();
  }
  
  private void zzcta()
  {
    HashMap localHashMap = new HashMap();
    if (zzanc.x())
    {
      if (this.bbm.zzcsk()) {
        localHashMap.put("persistence.android.enabled", Integer.valueOf(1));
      }
      str = String.valueOf(this.bbm.zzcsl().replace('.', '-'));
      if (str.length() != 0) {}
      for (str = "sdk.android.".concat(str);; str = new String("sdk.android."))
      {
        localHashMap.put(str, Integer.valueOf(1));
        if (this.aZR.zzcyu()) {
          this.aZR.zzi("Sending first connection stats", new Object[0]);
        }
        zzby(localHashMap);
        return;
      }
    }
    assert (!this.bbm.zzcsk()) : "Stats for persistence on JVM missing (persistence not yet supported)";
    String str = String.valueOf(this.bbm.zzcsl().replace('.', '-'));
    if (str.length() != 0) {}
    for (str = "sdk.java.".concat(str);; str = new String("sdk.java."))
    {
      localHashMap.put(str, Integer.valueOf(1));
      break;
    }
  }
  
  private long zzctb()
  {
    long l = this.bbf;
    this.bbf = (1L + l);
    return l;
  }
  
  private void zzctc()
  {
    boolean bool = false;
    if (isIdle())
    {
      if (this.bbr != null) {
        this.bbr.cancel(false);
      }
      this.bbr = this.aZE.schedule(new Runnable()
      {
        public void run()
        {
          zzaiz.zza(zzaiz.this, null);
          if (zzaiz.zzp(zzaiz.this))
          {
            zzaiz.this.interrupt("connection_idle");
            return;
          }
          zzaiz.zzn(zzaiz.this);
        }
      }, 60000L, TimeUnit.MILLISECONDS);
    }
    while (!isInterrupted("connection_idle")) {
      return;
    }
    if (!isIdle()) {
      bool = true;
    }
    zzaiv.zzcx(bool);
    resume("connection_idle");
  }
  
  private boolean zzctd()
  {
    long l = System.currentTimeMillis();
    return (isIdle()) && (l > this.bbs + 60000L);
  }
  
  private void zzcz(final boolean paramBoolean)
  {
    zzaiv.zzc(zzcsq(), "Must be connected to send auth, but was: %s", new Object[] { this.bbd });
    if (this.bbk != null) {}
    zza local3;
    HashMap localHashMap;
    for (boolean bool = true;; bool = false)
    {
      zzaiv.zzc(bool, "Auth token must be set to authenticate!", new Object[0]);
      local3 = new zza()
      {
        public void zzbz(Map<String, Object> paramAnonymousMap)
        {
          zzaiz.zza(zzaiz.this, zzaiz.zzb.bbH);
          String str = (String)paramAnonymousMap.get("s");
          if (str.equals("ok"))
          {
            zzaiz.zza(zzaiz.this, 0);
            zzaiz.zzg(zzaiz.this).zzcy(true);
            if (paramBoolean) {
              zzaiz.zzh(zzaiz.this);
            }
          }
          do
          {
            do
            {
              return;
              zzaiz.zza(zzaiz.this, null);
              zzaiz.zza(zzaiz.this, true);
              zzaiz.zzg(zzaiz.this).zzcy(false);
              paramAnonymousMap = (String)paramAnonymousMap.get("d");
              zzaiz.zza(zzaiz.this).zzi(String.valueOf(str).length() + 26 + String.valueOf(paramAnonymousMap).length() + "Authentication failed: " + str + " (" + paramAnonymousMap + ")", new Object[0]);
              zzaiz.zzi(zzaiz.this).close();
            } while (!str.equals("invalid_token"));
            zzaiz.zzj(zzaiz.this);
          } while (zzaiz.zzk(zzaiz.this) < 3L);
          zzaiz.zzl(zzaiz.this).zzctx();
          zzaiz.zza(zzaiz.this).warn("Provided authentication credentials are invalid. This usually indicates your FirebaseApp instance was not initialized correctly. Make sure your google-services.json file has the correct firebase_url and api_key. You can re-download google-services.json from https://console.firebase.google.com/.");
        }
      };
      localHashMap = new HashMap();
      zzand localzzand = zzand.zzta(this.bbk);
      if (localzzand == null) {
        break;
      }
      localHashMap.put("cred", localzzand.getToken());
      if (localzzand.y() != null) {
        localHashMap.put("authvar", localzzand.y());
      }
      zza("gauth", true, localHashMap, local3);
      return;
    }
    localHashMap.put("cred", this.bbk);
    zza("auth", true, localHashMap, local3);
  }
  
  private void zzl(String paramString, Map<String, Object> paramMap)
  {
    Object localObject1;
    Object localObject2;
    if (this.aZR.zzcyu())
    {
      localObject1 = this.aZR;
      localObject2 = String.valueOf(paramMap);
      ((zzalw)localObject1).zzi(String.valueOf(paramString).length() + 22 + String.valueOf(localObject2).length() + "handleServerMessage: " + paramString + " " + (String)localObject2, new Object[0]);
    }
    boolean bool;
    if ((paramString.equals("d")) || (paramString.equals("m")))
    {
      bool = paramString.equals("m");
      paramString = (String)paramMap.get("p");
      localObject1 = paramMap.get("d");
      paramMap = zzaiv.zzbq(paramMap.get("t"));
      if ((bool) && ((localObject1 instanceof Map)) && (((Map)localObject1).size() == 0)) {
        if (this.aZR.zzcyu())
        {
          paramMap = this.aZR;
          paramString = String.valueOf(paramString);
          if (paramString.length() == 0) {
            break label216;
          }
          paramString = "ignoring empty merge for path ".concat(paramString);
          paramMap.zzi(paramString, new Object[0]);
        }
      }
    }
    label216:
    label386:
    label430:
    label505:
    label521:
    do
    {
      Long localLong;
      ArrayList localArrayList;
      do
      {
        return;
        paramString = new String("ignoring empty merge for path ");
        break;
        paramString = zzaiv.zzsj(paramString);
        this.baX.zza(paramString, localObject1, bool, paramMap);
        return;
        if (!paramString.equals("rm")) {
          break label521;
        }
        localObject1 = (String)paramMap.get("p");
        localObject2 = zzaiv.zzsj((String)localObject1);
        paramString = paramMap.get("d");
        localLong = zzaiv.zzbq(paramMap.get("t"));
        paramString = (List)paramString;
        localArrayList = new ArrayList();
        Iterator localIterator = paramString.iterator();
        if (localIterator.hasNext())
        {
          Map localMap = (Map)localIterator.next();
          paramString = (String)localMap.get("s");
          paramMap = (String)localMap.get("e");
          if (paramString != null)
          {
            paramString = zzaiv.zzsj(paramString);
            if (paramMap == null) {
              break label430;
            }
          }
          for (paramMap = zzaiv.zzsj(paramMap);; paramMap = null)
          {
            localArrayList.add(new zzaja(paramString, paramMap, localMap.get("m")));
            break;
            paramString = null;
            break label386;
          }
        }
        if (!localArrayList.isEmpty()) {
          break label505;
        }
      } while (!this.aZR.zzcyu());
      paramMap = this.aZR;
      paramString = String.valueOf(localObject1);
      if (paramString.length() != 0) {}
      for (paramString = "Ignoring empty range merge for path ".concat(paramString);; paramString = new String("Ignoring empty range merge for path "))
      {
        paramMap.zzi(paramString, new Object[0]);
        return;
      }
      this.baX.zza((List)localObject2, localArrayList, localLong);
      return;
      if (paramString.equals("c"))
      {
        zzau(zzaiv.zzsj((String)paramMap.get("p")));
        return;
      }
      if (paramString.equals("ac"))
      {
        zzbm((String)paramMap.get("s"), (String)paramMap.get("d"));
        return;
      }
      if (paramString.equals("sd"))
      {
        zzbx(paramMap);
        return;
      }
    } while (!this.aZR.zzcyu());
    paramMap = this.aZR;
    paramString = String.valueOf(paramString);
    if (paramString.length() != 0) {}
    for (paramString = "Unrecognized action from server: ".concat(paramString);; paramString = new String("Unrecognized action from server: "))
    {
      paramMap.zzi(paramString, new Object[0]);
      return;
    }
  }
  
  public void initialize()
  {
    zzcst();
  }
  
  public void interrupt(String paramString)
  {
    String str;
    if (this.aZR.zzcyu())
    {
      zzalw localzzalw = this.aZR;
      str = String.valueOf(paramString);
      if (str.length() != 0)
      {
        str = "Connection interrupted for: ".concat(str);
        localzzalw.zzi(str, new Object[0]);
      }
    }
    else
    {
      this.baZ.add(paramString);
      if (this.bbc == null) {
        break label94;
      }
      this.bbc.close();
      this.bbc = null;
    }
    for (;;)
    {
      this.bbn.zzcpm();
      return;
      str = new String("Connection interrupted for: ");
      break;
      label94:
      this.bbn.cancel();
      this.bbd = zzb.bbD;
    }
  }
  
  public boolean isInterrupted(String paramString)
  {
    return this.baZ.contains(paramString);
  }
  
  public void purgeOutstandingWrites()
  {
    Iterator localIterator = this.bbi.values().iterator();
    Object localObject;
    while (localIterator.hasNext())
    {
      localObject = (zzf)localIterator.next();
      if (zzf.zza((zzf)localObject) != null) {
        zzf.zza((zzf)localObject).zzbn("write_canceled", null);
      }
    }
    localIterator = this.bbh.iterator();
    while (localIterator.hasNext())
    {
      localObject = (zzd)localIterator.next();
      if (zzd.zza((zzd)localObject) != null) {
        zzd.zza((zzd)localObject).zzbn("write_canceled", null);
      }
    }
    this.bbi.clear();
    this.bbh.clear();
    if (!zzcsq()) {
      this.bbt = false;
    }
    zzctc();
  }
  
  public void refreshAuthToken()
  {
    this.aZR.zzi("Auth token refresh requested", new Object[0]);
    interrupt("token_refresh");
    resume("token_refresh");
  }
  
  public void resume(String paramString)
  {
    zzalw localzzalw;
    if (this.aZR.zzcyu())
    {
      localzzalw = this.aZR;
      str = String.valueOf(paramString);
      if (str.length() == 0) {
        break label75;
      }
    }
    label75:
    for (String str = "Connection no longer interrupted for: ".concat(str);; str = new String("Connection no longer interrupted for: "))
    {
      localzzalw.zzi(str, new Object[0]);
      this.baZ.remove(paramString);
      if ((zzcss()) && (this.bbd == zzb.bbD)) {
        zzcst();
      }
      return;
    }
  }
  
  public void shutdown()
  {
    interrupt("shutdown");
  }
  
  public void zza(List<String> paramList, zzajb paramzzajb)
  {
    if (zzcsr()) {
      zza("oc", paramList, null, paramzzajb);
    }
    for (;;)
    {
      zzctc();
      return;
      this.bbh.add(new zzd("oc", paramList, null, paramzzajb, null));
    }
  }
  
  public void zza(List<String> paramList, Object paramObject, zzajb paramzzajb)
  {
    zza("p", paramList, paramObject, null, paramzzajb);
  }
  
  public void zza(List<String> paramList, Object paramObject, String paramString, zzajb paramzzajb)
  {
    zza("p", paramList, paramObject, paramString, paramzzajb);
  }
  
  public void zza(List<String> paramList, Map<String, Object> paramMap)
  {
    paramList = new zzc(paramList, paramMap);
    if (this.aZR.zzcyu())
    {
      paramMap = this.aZR;
      String str = String.valueOf(paramList);
      paramMap.zzi(String.valueOf(str).length() + 15 + "unlistening on " + str, new Object[0]);
    }
    paramList = zza(paramList);
    if ((paramList != null) && (zzcsq())) {
      zza(paramList);
    }
    zzctc();
  }
  
  public void zza(List<String> paramList, Map<String, Object> paramMap, zzaix paramzzaix, Long paramLong, zzajb paramzzajb)
  {
    paramList = new zzc(paramList, paramMap);
    String str;
    if (this.aZR.zzcyu())
    {
      paramMap = this.aZR;
      str = String.valueOf(paramList);
      paramMap.zzi(String.valueOf(str).length() + 13 + "Listening on " + str, new Object[0]);
    }
    if (!this.bbj.containsKey(paramList)) {}
    for (boolean bool = true;; bool = false)
    {
      zzaiv.zzc(bool, "listen() called twice for same QuerySpec.", new Object[0]);
      if (this.aZR.zzcyu())
      {
        paramMap = this.aZR;
        str = String.valueOf(paramList);
        paramMap.zzi(String.valueOf(str).length() + 21 + "Adding listen query: " + str, new Object[0]);
      }
      paramMap = new zze(paramzzajb, paramList, paramLong, paramzzaix, null);
      this.bbj.put(paramList, paramMap);
      if (zzcsq()) {
        zzb(paramMap);
      }
      zzctc();
      return;
    }
  }
  
  public void zza(List<String> paramList, Map<String, Object> paramMap, zzajb paramzzajb)
  {
    zza("m", paramList, paramMap, null, paramzzajb);
  }
  
  public void zzb(zzais.zzb paramzzb)
  {
    String str;
    int i;
    if (this.aZR.zzcyu())
    {
      zzalw localzzalw = this.aZR;
      str = String.valueOf(paramzzb.name());
      if (str.length() != 0)
      {
        str = "Got on disconnect due to ".concat(str);
        localzzalw.zzi(str, new Object[0]);
      }
    }
    else
    {
      this.bbd = zzb.bbD;
      this.bbc = null;
      this.bbt = false;
      this.bbg.clear();
      zzcsu();
      if (zzcss())
      {
        long l1 = System.currentTimeMillis();
        long l2 = this.bbb;
        if (this.bbb <= 0L) {
          break label180;
        }
        if (l1 - l2 <= 30000L) {
          break label175;
        }
        i = 1;
      }
    }
    for (;;)
    {
      if ((paramzzb == zzais.zzb.baJ) || (i != 0)) {
        this.bbn.zzcpm();
      }
      zzcst();
      this.bbb = 0L;
      this.baX.onDisconnect();
      return;
      str = new String("Got on disconnect due to ");
      break;
      label175:
      i = 0;
      continue;
      label180:
      i = 0;
    }
  }
  
  public void zzb(List<String> paramList, Object paramObject, zzajb paramzzajb)
  {
    this.bbt = true;
    if (zzcsr()) {
      zza("o", paramList, paramObject, paramzzajb);
    }
    for (;;)
    {
      zzctc();
      return;
      this.bbh.add(new zzd("o", paramList, paramObject, paramzzajb, null));
    }
  }
  
  public void zzb(List<String> paramList, Map<String, Object> paramMap, zzajb paramzzajb)
  {
    this.bbt = true;
    if (zzcsr()) {
      zza("om", paramList, paramMap, paramzzajb);
    }
    for (;;)
    {
      zzctc();
      return;
      this.bbh.add(new zzd("om", paramList, paramMap, paramzzajb, null));
    }
  }
  
  public void zzbt(Map<String, Object> paramMap)
  {
    if (paramMap.containsKey("r"))
    {
      long l = ((Integer)paramMap.get("r")).intValue();
      localObject = (zza)this.bbg.remove(Long.valueOf(l));
      if (localObject != null) {
        ((zza)localObject).zzbz((Map)paramMap.get("b"));
      }
    }
    do
    {
      do
      {
        return;
      } while (paramMap.containsKey("error"));
      if (paramMap.containsKey("a"))
      {
        zzl((String)paramMap.get("a"), (Map)paramMap.get("b"));
        return;
      }
    } while (!this.aZR.zzcyu());
    Object localObject = this.aZR;
    paramMap = String.valueOf(paramMap);
    ((zzalw)localObject).zzi(String.valueOf(paramMap).length() + 26 + "Ignoring unknown message: " + paramMap, new Object[0]);
  }
  
  boolean zzcss()
  {
    return this.baZ.size() == 0;
  }
  
  public void zzj(long paramLong, String paramString)
  {
    if (this.aZR.zzcyu()) {
      this.aZR.zzi("onReady", new Object[0]);
    }
    this.bbb = System.currentTimeMillis();
    zzcc(paramLong);
    if (this.bba) {
      zzcta();
    }
    zzcsy();
    this.bba = false;
    this.bbo = paramString;
    this.baX.zzcsp();
  }
  
  public void zzsg(String paramString)
  {
    this.baY = paramString;
  }
  
  public void zzsh(String paramString)
  {
    zzalw localzzalw;
    if (this.aZR.zzcyu())
    {
      localzzalw = this.aZR;
      paramString = String.valueOf(paramString);
      if (paramString.length() == 0) {
        break label52;
      }
    }
    label52:
    for (paramString = "Firebase Database connection was forcefully killed by the server. Will not attempt reconnect. Reason: ".concat(paramString);; paramString = new String("Firebase Database connection was forcefully killed by the server. Will not attempt reconnect. Reason: "))
    {
      localzzalw.zzi(paramString, new Object[0]);
      interrupt("server_kill");
      return;
    }
  }
  
  public void zzsk(String paramString)
  {
    this.aZR.zzi("Auth token refreshed.", new Object[0]);
    this.bbk = paramString;
    if (zzcsq())
    {
      if (paramString != null) {
        zzcsv();
      }
    }
    else {
      return;
    }
    zzcsx();
  }
  
  public void zzsl(String paramString)
  {
    if (this.bbd == zzb.bbE) {}
    for (boolean bool = true;; bool = false)
    {
      zzaiv.zzc(bool, "Trying to open network connection while in the wrong state: %s", new Object[] { this.bbd });
      if (paramString == null) {
        this.baX.zzcy(false);
      }
      this.bbk = paramString;
      this.bbd = zzb.bbF;
      this.bbc = new zzais(this.bbm, this.baF, this.baY, this, this.bbo);
      this.bbc.open();
      return;
    }
  }
  
  private static abstract interface zza
  {
    public abstract void zzbz(Map<String, Object> paramMap);
  }
  
  private static enum zzb
  {
    private zzb() {}
  }
  
  private static class zzc
  {
    private final List<String> bbJ;
    private final Map<String, Object> bbK;
    
    public zzc(List<String> paramList, Map<String, Object> paramMap)
    {
      this.bbJ = paramList;
      this.bbK = paramMap;
    }
    
    public boolean equals(Object paramObject)
    {
      boolean bool2 = false;
      boolean bool1;
      if (this == paramObject) {
        bool1 = true;
      }
      do
      {
        do
        {
          return bool1;
          bool1 = bool2;
        } while (!(paramObject instanceof zzc));
        paramObject = (zzc)paramObject;
        bool1 = bool2;
      } while (!this.bbJ.equals(((zzc)paramObject).bbJ));
      return this.bbK.equals(((zzc)paramObject).bbK);
    }
    
    public int hashCode()
    {
      return this.bbJ.hashCode() * 31 + this.bbK.hashCode();
    }
    
    public String toString()
    {
      String str1 = String.valueOf(zzaiv.zzas(this.bbJ));
      String str2 = String.valueOf(this.bbK);
      return String.valueOf(str1).length() + 11 + String.valueOf(str2).length() + str1 + " (params: " + str2 + ")";
    }
  }
  
  private static class zzd
  {
    private final List<String> bbJ;
    private final String bbL;
    private final zzajb bbM;
    private final Object data;
    
    private zzd(String paramString, List<String> paramList, Object paramObject, zzajb paramzzajb)
    {
      this.bbL = paramString;
      this.bbJ = paramList;
      this.data = paramObject;
      this.bbM = paramzzajb;
    }
    
    public String getAction()
    {
      return this.bbL;
    }
    
    public Object getData()
    {
      return this.data;
    }
    
    public List<String> zzcte()
    {
      return this.bbJ;
    }
    
    public zzajb zzctf()
    {
      return this.bbM;
    }
  }
  
  private static class zze
  {
    private final zzajb bbN;
    private final zzaiz.zzc bbO;
    private final zzaix bbP;
    private final Long bbQ;
    
    private zze(zzajb paramzzajb, zzaiz.zzc paramzzc, Long paramLong, zzaix paramzzaix)
    {
      this.bbN = paramzzajb;
      this.bbO = paramzzc;
      this.bbP = paramzzaix;
      this.bbQ = paramLong;
    }
    
    public String toString()
    {
      String str1 = String.valueOf(this.bbO.toString());
      String str2 = String.valueOf(this.bbQ);
      return String.valueOf(str1).length() + 8 + String.valueOf(str2).length() + str1 + " (Tag: " + str2 + ")";
    }
    
    public zzaiz.zzc zzctg()
    {
      return this.bbO;
    }
    
    public Long zzcth()
    {
      return this.bbQ;
    }
    
    public zzaix zzcti()
    {
      return this.bbP;
    }
  }
  
  private static class zzf
  {
    private String bbL;
    private zzajb bbM;
    private Map<String, Object> bbR;
    private boolean bbS;
    
    private zzf(String paramString, Map<String, Object> paramMap, zzajb paramzzajb)
    {
      this.bbL = paramString;
      this.bbR = paramMap;
      this.bbM = paramzzajb;
    }
    
    public String getAction()
    {
      return this.bbL;
    }
    
    public zzajb zzctf()
    {
      return this.bbM;
    }
    
    public Map<String, Object> zzctj()
    {
      return this.bbR;
    }
    
    public void zzctk()
    {
      this.bbS = true;
    }
    
    public boolean zzctl()
    {
      return this.bbS;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaiz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */