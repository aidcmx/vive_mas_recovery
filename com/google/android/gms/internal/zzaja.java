package com.google.android.gms.internal;

import java.util.List;

public class zzaja
{
  private final List<String> bbT;
  private final List<String> bbU;
  private final Object bbV;
  
  public zzaja(List<String> paramList1, List<String> paramList2, Object paramObject)
  {
    this.bbT = paramList1;
    this.bbU = paramList2;
    this.bbV = paramObject;
  }
  
  public List<String> zzctm()
  {
    return this.bbT;
  }
  
  public List<String> zzctn()
  {
    return this.bbU;
  }
  
  public Object zzcto()
  {
    return this.bbV;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaja.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */