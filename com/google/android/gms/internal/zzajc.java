package com.google.android.gms.internal;

import java.io.EOFException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

class zzajc
{
  private static long bbW = 0L;
  private final ScheduledExecutorService aZE;
  private final zzalw aZR;
  private zzb bbX;
  private boolean bbY = false;
  private boolean bbZ = false;
  private long bca = 0L;
  private zzaje bcb;
  private zza bcc;
  private ScheduledFuture<?> bcd;
  private ScheduledFuture<?> bce;
  private final zzaiu bcf;
  
  public zzajc(zzaiu paramzzaiu, zzaiw paramzzaiw, String paramString1, zza paramzza, String paramString2)
  {
    this.bcf = paramzzaiu;
    this.aZE = paramzzaiu.zzcsj();
    this.bcc = paramzza;
    long l = bbW;
    bbW = 1L + l;
    this.aZR = new zzalw(paramzzaiu.zzcsh(), "WebSocket", 23 + "ws_" + l);
    this.bbX = zza(paramzzaiw, paramString1, paramString2);
  }
  
  private boolean isBuffering()
  {
    return this.bcb != null;
  }
  
  private void shutdown()
  {
    this.bbZ = true;
    this.bcc.zzcw(this.bbY);
  }
  
  private zzb zza(zzaiw paramzzaiw, String paramString1, String paramString2)
  {
    if (paramString1 != null) {}
    for (;;)
    {
      paramzzaiw = zzaiw.zza(paramString1, paramzzaiw.isSecure(), paramzzaiw.getNamespace(), paramString2);
      paramString1 = new HashMap();
      paramString1.put("User-Agent", this.bcf.zzux());
      return new zzc(new zzamv(paramzzaiw, null, paramString1), null);
      paramString1 = paramzzaiw.getHost();
    }
  }
  
  private static String[] zzae(String paramString, int paramInt)
  {
    int i = 0;
    if (paramString.length() <= paramInt) {
      return new String[] { paramString };
    }
    ArrayList localArrayList = new ArrayList();
    while (i < paramString.length())
    {
      localArrayList.add(paramString.substring(i, Math.min(i + paramInt, paramString.length())));
      i += paramInt;
    }
    return (String[])localArrayList.toArray(new String[localArrayList.size()]);
  }
  
  private void zzaft(int paramInt)
  {
    this.bca = paramInt;
    this.bcb = new zzaje();
    if (this.aZR.zzcyu())
    {
      zzalw localzzalw = this.aZR;
      long l = this.bca;
      localzzalw.zzi(41 + "HandleNewFrameCount: " + l, new Object[0]);
    }
  }
  
  private void zzctp()
  {
    if (!this.bbZ)
    {
      if (this.bcd == null) {
        break label106;
      }
      this.bcd.cancel(false);
      if (this.aZR.zzcyu())
      {
        zzalw localzzalw = this.aZR;
        long l = this.bcd.getDelay(TimeUnit.MILLISECONDS);
        localzzalw.zzi(48 + "Reset keepAlive. Remaining: " + l, new Object[0]);
      }
    }
    for (;;)
    {
      this.bcd = this.aZE.schedule(zzctq(), 45000L, TimeUnit.MILLISECONDS);
      return;
      label106:
      if (this.aZR.zzcyu()) {
        this.aZR.zzi("Reset keepAlive", new Object[0]);
      }
    }
  }
  
  private Runnable zzctq()
  {
    new Runnable()
    {
      public void run()
      {
        if (zzajc.zzg(zzajc.this) != null)
        {
          zzajc.zzg(zzajc.this).zzsp("0");
          zzajc.zzc(zzajc.this);
        }
      }
    };
  }
  
  private void zzctr()
  {
    if (!this.bbZ)
    {
      if (this.aZR.zzcyu()) {
        this.aZR.zzi("closing itself", new Object[0]);
      }
      shutdown();
    }
    this.bbX = null;
    if (this.bcd != null) {
      this.bcd.cancel(false);
    }
  }
  
  private void zzcts()
  {
    if ((!this.bbY) && (!this.bbZ))
    {
      if (this.aZR.zzcyu()) {
        this.aZR.zzi("timed out on connect", new Object[0]);
      }
      this.bbX.close();
    }
  }
  
  private void zzsm(String paramString)
  {
    this.bcb.zzsq(paramString);
    this.bca -= 1L;
    if (this.bca == 0L) {}
    Object localObject;
    try
    {
      this.bcb.zzctz();
      paramString = zzane.zztb(this.bcb.toString());
      this.bcb = null;
      if (this.aZR.zzcyu())
      {
        zzalw localzzalw = this.aZR;
        localObject = String.valueOf(paramString);
        localzzalw.zzi(String.valueOf(localObject).length() + 36 + "handleIncomingFrame complete frame: " + (String)localObject, new Object[0]);
      }
      this.bcc.zzbs(paramString);
      return;
    }
    catch (IOException localIOException)
    {
      localObject = this.aZR;
      paramString = String.valueOf(this.bcb.toString());
      if (paramString.length() != 0) {}
      for (paramString = "Error parsing frame: ".concat(paramString);; paramString = new String("Error parsing frame: "))
      {
        ((zzalw)localObject).zzd(paramString, localIOException);
        close();
        shutdown();
        return;
      }
    }
    catch (ClassCastException localClassCastException)
    {
      localObject = this.aZR;
      paramString = String.valueOf(this.bcb.toString());
      if (paramString.length() == 0) {}
    }
    for (paramString = "Error parsing frame (cast error): ".concat(paramString);; paramString = new String("Error parsing frame (cast error): "))
    {
      ((zzalw)localObject).zzd(paramString, localClassCastException);
      close();
      shutdown();
      return;
    }
  }
  
  private String zzsn(String paramString)
  {
    if (paramString.length() <= 6) {
      try
      {
        int i = Integer.parseInt(paramString);
        if (i > 0) {
          zzaft(i);
        }
        return null;
      }
      catch (NumberFormatException localNumberFormatException) {}
    }
    zzaft(1);
    return paramString;
  }
  
  private void zzso(String paramString)
  {
    if (!this.bbZ)
    {
      zzctp();
      if (!isBuffering()) {
        break label24;
      }
      zzsm(paramString);
    }
    label24:
    do
    {
      return;
      paramString = zzsn(paramString);
    } while (paramString == null);
    zzsm(paramString);
  }
  
  public void close()
  {
    if (this.aZR.zzcyu()) {
      this.aZR.zzi("websocket is being closed", new Object[0]);
    }
    this.bbZ = true;
    this.bbX.close();
    if (this.bce != null) {
      this.bce.cancel(true);
    }
    if (this.bcd != null) {
      this.bcd.cancel(true);
    }
  }
  
  public void open()
  {
    this.bbX.connect();
    this.bce = this.aZE.schedule(new Runnable()
    {
      public void run()
      {
        zzajc.zzf(zzajc.this);
      }
    }, 30000L, TimeUnit.MILLISECONDS);
  }
  
  public void send(Map<String, Object> paramMap)
  {
    zzctp();
    for (;;)
    {
      try
      {
        arrayOfString = zzae(zzane.zzce(paramMap), 16384);
        if (arrayOfString.length <= 1) {
          continue;
        }
        localObject = this.bbX;
        i = arrayOfString.length;
        ((zzb)localObject).zzsp(11 + i);
      }
      catch (IOException localIOException)
      {
        String[] arrayOfString;
        Object localObject = this.aZR;
        paramMap = String.valueOf(paramMap.toString());
        if (paramMap.length() == 0) {
          continue;
        }
        paramMap = "Failed to serialize message: ".concat(paramMap);
        ((zzalw)localObject).zzd(paramMap, localIOException);
        shutdown();
        return;
        paramMap = new String("Failed to serialize message: ");
        continue;
        int i = 0;
        continue;
      }
      if (i >= arrayOfString.length) {
        continue;
      }
      this.bbX.zzsp(arrayOfString[i]);
      i += 1;
    }
  }
  
  public void start() {}
  
  public static abstract interface zza
  {
    public abstract void zzbs(Map<String, Object> paramMap);
    
    public abstract void zzcw(boolean paramBoolean);
  }
  
  private static abstract interface zzb
  {
    public abstract void close();
    
    public abstract void connect();
    
    public abstract void zzsp(String paramString);
  }
  
  private class zzc
    implements zzajc.zzb, zzamw
  {
    private zzamv bch;
    
    private zzc(zzamv paramzzamv)
    {
      this.bch = paramzzamv;
      this.bch.zza(this);
    }
    
    private void shutdown()
    {
      this.bch.close();
      try
      {
        this.bch.m();
        return;
      }
      catch (InterruptedException localInterruptedException)
      {
        zzajc.zzb(zzajc.this).zzd("Interrupted while shutting down websocket threads", localInterruptedException);
      }
    }
    
    public void close()
    {
      this.bch.close();
    }
    
    public void connect()
    {
      try
      {
        this.bch.connect();
        return;
      }
      catch (zzamx localzzamx)
      {
        if (zzajc.zzb(zzajc.this).zzcyu()) {
          zzajc.zzb(zzajc.this).zza("Error connecting", localzzamx, new Object[0]);
        }
        shutdown();
      }
    }
    
    public void onClose()
    {
      zzajc.zzd(zzajc.this).execute(new Runnable()
      {
        public void run()
        {
          if (zzajc.zzb(zzajc.this).zzcyu()) {
            zzajc.zzb(zzajc.this).zzi("closed", new Object[0]);
          }
          zzajc.zze(zzajc.this);
        }
      });
    }
    
    public void zza(final zzamx paramzzamx)
    {
      zzajc.zzd(zzajc.this).execute(new Runnable()
      {
        public void run()
        {
          if ((paramzzamx.getCause() != null) && ((paramzzamx.getCause() instanceof EOFException))) {
            zzajc.zzb(zzajc.this).zzi("WebSocket reached EOF.", new Object[0]);
          }
          for (;;)
          {
            zzajc.zze(zzajc.this);
            return;
            zzajc.zzb(zzajc.this).zza("WebSocket error.", paramzzamx, new Object[0]);
          }
        }
      });
    }
    
    public void zza(zzamz paramzzamz)
    {
      final String str = paramzzamz.getText();
      zzalw localzzalw;
      if (zzajc.zzb(zzajc.this).zzcyu())
      {
        localzzalw = zzajc.zzb(zzajc.this);
        paramzzamz = String.valueOf(str);
        if (paramzzamz.length() == 0) {
          break label76;
        }
      }
      label76:
      for (paramzzamz = "ws message: ".concat(paramzzamz);; paramzzamz = new String("ws message: "))
      {
        localzzalw.zzi(paramzzamz, new Object[0]);
        zzajc.zzd(zzajc.this).execute(new Runnable()
        {
          public void run()
          {
            zzajc.zza(zzajc.this, str);
          }
        });
        return;
      }
    }
    
    public void zzctt()
    {
      zzajc.zzd(zzajc.this).execute(new Runnable()
      {
        public void run()
        {
          zzajc.zza(zzajc.this).cancel(false);
          zzajc.zza(zzajc.this, true);
          if (zzajc.zzb(zzajc.this).zzcyu()) {
            zzajc.zzb(zzajc.this).zzi("websocket opened", new Object[0]);
          }
          zzajc.zzc(zzajc.this);
        }
      });
    }
    
    public void zzsp(String paramString)
    {
      this.bch.zzsp(paramString);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzajc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */