package com.google.android.gms.internal;

import java.util.Random;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class zzajd
{
  private final ScheduledExecutorService aZE;
  private final zzalw aZR;
  private final long bcG;
  private final long bcH;
  private final double bcI;
  private final double bcJ;
  private final Random bcK = new Random();
  private ScheduledFuture<?> bcL;
  private long bcM;
  private boolean bcN = true;
  
  private zzajd(ScheduledExecutorService paramScheduledExecutorService, zzalw paramzzalw, long paramLong1, long paramLong2, double paramDouble1, double paramDouble2)
  {
    this.aZE = paramScheduledExecutorService;
    this.aZR = paramzzalw;
    this.bcG = paramLong1;
    this.bcH = paramLong2;
    this.bcJ = paramDouble1;
    this.bcI = paramDouble2;
  }
  
  public void cancel()
  {
    if (this.bcL != null)
    {
      this.aZR.zzi("Cancelling existing retry attempt", new Object[0]);
      this.bcL.cancel(false);
      this.bcL = null;
    }
    for (;;)
    {
      this.bcM = 0L;
      return;
      this.aZR.zzi("No existing retry attempt to cancel", new Object[0]);
    }
  }
  
  public void zzcpm()
  {
    this.bcN = true;
    this.bcM = 0L;
  }
  
  public void zzctx()
  {
    this.bcM = this.bcH;
  }
  
  public void zzr(final Runnable paramRunnable)
  {
    long l = 0L;
    paramRunnable = new Runnable()
    {
      public void run()
      {
        zzajd.zza(zzajd.this, null);
        paramRunnable.run();
      }
    };
    if (this.bcL != null)
    {
      this.aZR.zzi("Cancelling previous scheduled retry", new Object[0]);
      this.bcL.cancel(false);
      this.bcL = null;
    }
    if (this.bcN)
    {
      this.bcN = false;
      this.aZR.zzi("Scheduling retry in %dms", new Object[] { Long.valueOf(l) });
      this.bcL = this.aZE.schedule(paramRunnable, l, TimeUnit.MILLISECONDS);
      return;
    }
    if (this.bcM == 0L) {}
    for (this.bcM = this.bcG;; this.bcM = Math.min((this.bcM * this.bcJ), this.bcH))
    {
      l = ((1.0D - this.bcI) * this.bcM + this.bcI * this.bcM * this.bcK.nextDouble());
      break;
    }
  }
  
  public static class zza
  {
    private final zzalw aZR;
    private long bcG = 1000L;
    private double bcI = 0.5D;
    private double bcJ = 1.3D;
    private final ScheduledExecutorService bcP;
    private long bcQ = 30000L;
    
    public zza(ScheduledExecutorService paramScheduledExecutorService, zzalx paramzzalx, String paramString)
    {
      this.bcP = paramScheduledExecutorService;
      this.aZR = new zzalw(paramzzalx, paramString);
    }
    
    public zza zzcg(long paramLong)
    {
      this.bcG = paramLong;
      return this;
    }
    
    public zza zzch(long paramLong)
    {
      this.bcQ = paramLong;
      return this;
    }
    
    public zzajd zzcty()
    {
      return new zzajd(this.bcP, this.aZR, this.bcG, this.bcQ, this.bcJ, this.bcI, null);
    }
    
    public zza zzj(double paramDouble)
    {
      this.bcJ = paramDouble;
      return this;
    }
    
    public zza zzk(double paramDouble)
    {
      if ((paramDouble < 0.0D) || (paramDouble > 1.0D)) {
        throw new IllegalArgumentException(47 + "Argument out of range: " + paramDouble);
      }
      this.bcI = paramDouble;
      return this;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzajd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */