package com.google.android.gms.internal;

import java.io.IOException;
import java.io.Reader;
import java.nio.CharBuffer;
import java.util.Iterator;
import java.util.List;

public class zzaje
  extends Reader
{
  private List<String> bcR = null;
  private int bcS;
  private int bcT;
  private int bcU = this.bcS;
  private int bcV = this.bcT;
  private boolean bcW = false;
  private boolean closed = false;
  
  private long zzci(long paramLong)
  {
    long l1 = 0L;
    while ((this.bcT < this.bcR.size()) && (l1 < paramLong))
    {
      int i = zzcub();
      long l2 = paramLong - l1;
      if (l2 < i)
      {
        this.bcS = ((int)(this.bcS + l2));
        l1 += l2;
      }
      else
      {
        l1 += i;
        this.bcS = 0;
        this.bcT += 1;
      }
    }
    return l1;
  }
  
  private String zzcua()
  {
    if (this.bcT < this.bcR.size()) {
      return (String)this.bcR.get(this.bcT);
    }
    return null;
  }
  
  private int zzcub()
  {
    String str = zzcua();
    if (str == null) {
      return 0;
    }
    return str.length() - this.bcS;
  }
  
  private void zzcuc()
    throws IOException
  {
    if (this.closed) {
      throw new IOException("Stream already closed");
    }
    if (!this.bcW) {
      throw new IOException("Reader needs to be frozen before read operations can be called");
    }
  }
  
  public void close()
    throws IOException
  {
    zzcuc();
    this.closed = true;
  }
  
  public void mark(int paramInt)
    throws IOException
  {
    zzcuc();
    this.bcU = this.bcS;
    this.bcV = this.bcT;
  }
  
  public boolean markSupported()
  {
    return true;
  }
  
  public int read()
    throws IOException
  {
    zzcuc();
    String str = zzcua();
    if (str == null) {
      return -1;
    }
    int i = str.charAt(this.bcS);
    zzci(1L);
    return i;
  }
  
  public int read(CharBuffer paramCharBuffer)
    throws IOException
  {
    zzcuc();
    int j = paramCharBuffer.remaining();
    int i = 0;
    for (String str = zzcua(); (j > 0) && (str != null); str = zzcua())
    {
      int k = Math.min(str.length() - this.bcS, j);
      paramCharBuffer.put((String)this.bcR.get(this.bcT), this.bcS, this.bcS + k);
      j -= k;
      i += k;
      zzci(k);
    }
    if ((i > 0) || (str != null)) {
      return i;
    }
    return -1;
  }
  
  public int read(char[] paramArrayOfChar, int paramInt1, int paramInt2)
    throws IOException
  {
    zzcuc();
    String str = zzcua();
    int i = 0;
    while ((str != null) && (i < paramInt2))
    {
      int j = Math.min(zzcub(), paramInt2 - i);
      str.getChars(this.bcS, this.bcS + j, paramArrayOfChar, paramInt1 + i);
      zzci(j);
      str = zzcua();
      i += j;
    }
    if ((i > 0) || (str != null)) {
      return i;
    }
    return -1;
  }
  
  public boolean ready()
    throws IOException
  {
    zzcuc();
    return true;
  }
  
  public void reset()
    throws IOException
  {
    this.bcS = this.bcU;
    this.bcT = this.bcV;
  }
  
  public long skip(long paramLong)
    throws IOException
  {
    zzcuc();
    return zzci(paramLong);
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    Iterator localIterator = this.bcR.iterator();
    while (localIterator.hasNext()) {
      localStringBuilder.append((String)localIterator.next());
    }
    return localStringBuilder.toString();
  }
  
  public void zzctz()
  {
    if (this.bcW) {
      throw new IllegalStateException("Trying to freeze frozen StringListReader");
    }
    this.bcW = true;
  }
  
  public void zzsq(String paramString)
  {
    if (this.bcW) {
      throw new IllegalStateException("Trying to add string after reading");
    }
    if (paramString.length() > 0) {
      this.bcR.add(paramString);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaje.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */