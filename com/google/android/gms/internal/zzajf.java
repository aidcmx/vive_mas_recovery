package com.google.android.gms.internal;

public abstract interface zzajf
{
  public abstract void zza(zzb paramzzb);
  
  public abstract void zza(boolean paramBoolean, zza paramzza);
  
  public static abstract interface zza
  {
    public abstract void onError(String paramString);
    
    public abstract void zzsi(String paramString);
  }
  
  public static abstract interface zzb
  {
    public abstract void zzsr(String paramString);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzajf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */