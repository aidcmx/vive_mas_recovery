package com.google.android.gms.internal;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.zza;

public class zzajg
  extends zzajl
{
  private final zzajs aZj;
  private final ChildEventListener bcX;
  private final zzall bcY;
  
  public zzajg(zzajs paramzzajs, ChildEventListener paramChildEventListener, zzall paramzzall)
  {
    this.aZj = paramzzajs;
    this.bcX = paramChildEventListener;
    this.bcY = paramzzall;
  }
  
  public boolean equals(Object paramObject)
  {
    return ((paramObject instanceof zzajg)) && (((zzajg)paramObject).bcX.equals(this.bcX)) && (((zzajg)paramObject).aZj.equals(this.aZj)) && (((zzajg)paramObject).bcY.equals(this.bcY));
  }
  
  public int hashCode()
  {
    return (this.bcX.hashCode() * 31 + this.aZj.hashCode()) * 31 + this.bcY.hashCode();
  }
  
  public String toString()
  {
    return "ChildEventRegistration";
  }
  
  public zzajl zza(zzall paramzzall)
  {
    return new zzajg(this.aZj, this.bcX, paramzzall);
  }
  
  public zzalg zza(zzalf paramzzalf, zzall paramzzall)
  {
    DataSnapshot localDataSnapshot = zza.zza(zza.zza(this.aZj, paramzzall.zzcrc().zza(paramzzalf.zzcxl())), paramzzalf.zzcxj());
    if (paramzzalf.zzcxn() != null) {}
    for (paramzzall = paramzzalf.zzcxn().asString();; paramzzall = null) {
      return new zzalg(paramzzalf.zzcxm(), this, localDataSnapshot, paramzzall);
    }
  }
  
  public void zza(zzalg paramzzalg)
  {
    if (zzcvc()) {
      return;
    }
    switch (1.bcZ[paramzzalg.zzcxm().ordinal()])
    {
    default: 
      return;
    case 1: 
      this.bcX.onChildAdded(paramzzalg.zzcxp(), paramzzalg.zzcxq());
      return;
    case 2: 
      this.bcX.onChildChanged(paramzzalg.zzcxp(), paramzzalg.zzcxq());
      return;
    case 3: 
      this.bcX.onChildMoved(paramzzalg.zzcxp(), paramzzalg.zzcxq());
      return;
    }
    this.bcX.onChildRemoved(paramzzalg.zzcxp());
  }
  
  public void zza(DatabaseError paramDatabaseError)
  {
    this.bcX.onCancelled(paramDatabaseError);
  }
  
  public boolean zza(zzalh.zza paramzza)
  {
    return paramzza != zzalh.zza.bhz;
  }
  
  public boolean zzc(zzajl paramzzajl)
  {
    return ((paramzzajl instanceof zzajg)) && (((zzajg)paramzzajl).bcX.equals(this.bcX));
  }
  
  public zzall zzcud()
  {
    return this.bcY;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzajg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */