package com.google.android.gms.internal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class zzajh
  implements Iterable<Map.Entry<zzajq, zzaml>>
{
  private static final zzajh bda;
  private final zzakz<zzaml> bdb;
  
  static
  {
    if (!zzajh.class.desiredAssertionStatus()) {}
    for (boolean bool = true;; bool = false)
    {
      $assertionsDisabled = bool;
      bda = new zzajh(new zzakz(null));
      return;
    }
  }
  
  private zzajh(zzakz<zzaml> paramzzakz)
  {
    this.bdb = paramzzakz;
  }
  
  private zzaml zza(zzajq paramzzajq, zzakz<zzaml> paramzzakz, zzaml paramzzaml)
  {
    Object localObject1;
    if (paramzzakz.getValue() != null) {
      localObject1 = paramzzaml.zzl(paramzzajq, (zzaml)paramzzakz.getValue());
    }
    do
    {
      do
      {
        return (zzaml)localObject1;
        localObject1 = null;
        Iterator localIterator = paramzzakz.zzcxf().iterator();
        paramzzakz = paramzzaml;
        paramzzaml = (zzaml)localObject1;
        if (localIterator.hasNext())
        {
          Object localObject2 = (Map.Entry)localIterator.next();
          localObject1 = (zzakz)((Map.Entry)localObject2).getValue();
          localObject2 = (zzalz)((Map.Entry)localObject2).getKey();
          if (((zzalz)localObject2).zzczb())
          {
            assert (((zzakz)localObject1).getValue() != null) : "Priority writes must always be leaf nodes";
            paramzzaml = (zzaml)((zzakz)localObject1).getValue();
          }
          for (;;)
          {
            break;
            paramzzakz = zza(paramzzajq.zza((zzalz)localObject2), (zzakz)localObject1, paramzzakz);
          }
        }
        localObject1 = paramzzakz;
      } while (paramzzakz.zzao(paramzzajq).isEmpty());
      localObject1 = paramzzakz;
    } while (paramzzaml == null);
    return paramzzakz.zzl(paramzzajq.zza(zzalz.zzcyz()), paramzzaml);
  }
  
  public static zzajh zzca(Map<String, Object> paramMap)
  {
    Object localObject = zzakz.zzcxe();
    Iterator localIterator = paramMap.entrySet().iterator();
    zzakz localzzakz;
    for (paramMap = (Map<String, Object>)localObject; localIterator.hasNext(); paramMap = paramMap.zza(new zzajq((String)((Map.Entry)localObject).getKey()), localzzakz))
    {
      localObject = (Map.Entry)localIterator.next();
      localzzakz = new zzakz(zzamm.zzbt(((Map.Entry)localObject).getValue()));
    }
    return new zzajh(paramMap);
  }
  
  public static zzajh zzcb(Map<zzajq, zzaml> paramMap)
  {
    Object localObject = zzakz.zzcxe();
    Iterator localIterator = paramMap.entrySet().iterator();
    zzakz localzzakz;
    for (paramMap = (Map<zzajq, zzaml>)localObject; localIterator.hasNext(); paramMap = paramMap.zza((zzajq)((Map.Entry)localObject).getKey(), localzzakz))
    {
      localObject = (Map.Entry)localIterator.next();
      localzzakz = new zzakz((zzaml)((Map.Entry)localObject).getValue());
    }
    return new zzajh(paramMap);
  }
  
  public static zzajh zzcue()
  {
    return bda;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == this) {
      return true;
    }
    if ((paramObject == null) || (paramObject.getClass() != getClass())) {
      return false;
    }
    return ((zzajh)paramObject).zzda(true).equals(zzda(true));
  }
  
  public int hashCode()
  {
    return zzda(true).hashCode();
  }
  
  public boolean isEmpty()
  {
    return this.bdb.isEmpty();
  }
  
  public Iterator<Map.Entry<zzajq, zzaml>> iterator()
  {
    return this.bdb.iterator();
  }
  
  public String toString()
  {
    String str = String.valueOf(zzda(true).toString());
    return String.valueOf(str).length() + 15 + "CompoundWrite{" + str + "}";
  }
  
  public zzajh zza(zzalz paramzzalz, zzaml paramzzaml)
  {
    return zze(new zzajq(new zzalz[] { paramzzalz }), paramzzaml);
  }
  
  public zzajh zzb(final zzajq paramzzajq, zzajh paramzzajh)
  {
    (zzajh)paramzzajh.bdb.zzb(this, new zzakz.zza()
    {
      public zzajh zza(zzajq paramAnonymouszzajq, zzaml paramAnonymouszzaml, zzajh paramAnonymouszzajh)
      {
        return paramAnonymouszzajh.zze(paramzzajq.zzh(paramAnonymouszzajq), paramAnonymouszzaml);
      }
    });
  }
  
  public zzaml zzb(zzaml paramzzaml)
  {
    return zza(zzajq.zzcvg(), this.bdb, paramzzaml);
  }
  
  public zzaml zzcuf()
  {
    return (zzaml)this.bdb.getValue();
  }
  
  public List<zzamk> zzcug()
  {
    ArrayList localArrayList = new ArrayList();
    Object localObject;
    if (this.bdb.getValue() != null)
    {
      localIterator = ((zzaml)this.bdb.getValue()).iterator();
      while (localIterator.hasNext())
      {
        localObject = (zzamk)localIterator.next();
        localArrayList.add(new zzamk(((zzamk)localObject).a(), ((zzamk)localObject).zzcqy()));
      }
    }
    Iterator localIterator = this.bdb.zzcxf().iterator();
    while (localIterator.hasNext())
    {
      localObject = (Map.Entry)localIterator.next();
      zzakz localzzakz = (zzakz)((Map.Entry)localObject).getValue();
      if (localzzakz.getValue() != null) {
        localArrayList.add(new zzamk((zzalz)((Map.Entry)localObject).getKey(), (zzaml)localzzakz.getValue()));
      }
    }
    return localArrayList;
  }
  
  public Map<zzalz, zzajh> zzcuh()
  {
    HashMap localHashMap = new HashMap();
    Iterator localIterator = this.bdb.zzcxf().iterator();
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      localHashMap.put((zzalz)localEntry.getKey(), new zzajh((zzakz)localEntry.getValue()));
    }
    return localHashMap;
  }
  
  public zzajh zzd(zzajq paramzzajq)
  {
    if (paramzzajq.isEmpty()) {
      return bda;
    }
    return new zzajh(this.bdb.zza(paramzzajq, zzakz.zzcxe()));
  }
  
  public Map<String, Object> zzda(final boolean paramBoolean)
  {
    final HashMap localHashMap = new HashMap();
    this.bdb.zza(new zzakz.zza()
    {
      public Void zza(zzajq paramAnonymouszzajq, zzaml paramAnonymouszzaml, Void paramAnonymousVoid)
      {
        localHashMap.put(paramAnonymouszzajq.zzcvh(), paramAnonymouszzaml.getValue(paramBoolean));
        return null;
      }
    });
    return localHashMap;
  }
  
  public zzajh zze(zzajq paramzzajq, zzaml paramzzaml)
  {
    if (paramzzajq.isEmpty()) {
      paramzzajq = new zzajh(new zzakz(paramzzaml));
    }
    zzajq localzzajq1;
    zzajq localzzajq2;
    zzaml localzzaml;
    do
    {
      return paramzzajq;
      localzzajq1 = this.bdb.zzag(paramzzajq);
      if (localzzajq1 == null) {
        break label125;
      }
      localzzajq2 = zzajq.zza(localzzajq1, paramzzajq);
      localzzaml = (zzaml)this.bdb.zzak(localzzajq1);
      paramzzajq = localzzajq2.zzcvm();
      if ((paramzzajq == null) || (!paramzzajq.zzczb())) {
        break;
      }
      paramzzajq = this;
    } while (localzzaml.zzao(localzzajq2.zzcvl()).isEmpty());
    paramzzajq = localzzaml.zzl(localzzajq2, paramzzaml);
    return new zzajh(this.bdb.zzb(localzzajq1, paramzzajq));
    label125:
    paramzzaml = new zzakz(paramzzaml);
    return new zzajh(this.bdb.zza(paramzzajq, paramzzaml));
  }
  
  public boolean zze(zzajq paramzzajq)
  {
    return zzf(paramzzajq) != null;
  }
  
  public zzaml zzf(zzajq paramzzajq)
  {
    zzajq localzzajq = this.bdb.zzag(paramzzajq);
    if (localzzajq != null) {
      return ((zzaml)this.bdb.zzak(localzzajq)).zzao(zzajq.zza(localzzajq, paramzzajq));
    }
    return null;
  }
  
  public zzajh zzg(zzajq paramzzajq)
  {
    if (paramzzajq.isEmpty()) {
      return this;
    }
    zzaml localzzaml = zzf(paramzzajq);
    if (localzzaml != null) {
      return new zzajh(new zzakz(localzzaml));
    }
    return new zzajh(this.bdb.zzai(paramzzajq));
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzajh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */