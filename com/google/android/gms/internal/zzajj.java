package com.google.android.gms.internal;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.FirebaseDatabase;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;

public class zzajj
{
  protected FirebaseApp aZF;
  protected zzalx baR;
  protected boolean baS;
  protected String baU;
  private boolean bcW = false;
  protected zzajn bdk;
  protected zzajf bdl;
  protected zzajv bdm;
  protected String bdn;
  protected zzalx.zza bdo = zzalx.zza.bip;
  private boolean bdp = false;
  private zzajr bdq;
  protected long cacheSize = 10485760L;
  
  private static zzait zza(zzajf paramzzajf)
  {
    new zzait()
    {
      public void zza(boolean paramAnonymousBoolean, final zzait.zza paramAnonymouszza)
      {
        zzajj.this.zza(paramAnonymousBoolean, new zzajf.zza()
        {
          public void onError(String paramAnonymous2String)
          {
            paramAnonymouszza.onError(paramAnonymous2String);
          }
          
          public void zzsi(String paramAnonymous2String)
          {
            paramAnonymouszza.zzsi(paramAnonymous2String);
          }
        });
      }
    };
  }
  
  private ScheduledExecutorService zzcsj()
  {
    zzajv localzzajv = zzcus();
    if (!(localzzajv instanceof zzanh)) {
      throw new RuntimeException("Custom run loops are not supported!");
    }
    return ((zzanh)localzzajv).zzcsj();
  }
  
  private zzajr zzcui()
  {
    if (this.bdq == null)
    {
      if (!zzanc.x()) {
        break label22;
      }
      zzcuj();
    }
    for (;;)
    {
      return this.bdq;
      label22:
      if (zzajo.isActive())
      {
        zzajo localzzajo = zzajo.bdx;
        localzzajo.initialize();
        this.bdq = localzzajo;
      }
      else
      {
        this.bdq = zzajp.bdC;
      }
    }
  }
  
  private void zzcuj()
  {
    try
    {
      this.bdq = new zzaie(this.aZF);
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  private void zzcul()
  {
    zzcuv();
    zzcui();
    zzcuy();
    zzcux();
    zzcuw();
    zzcva();
    zzcuz();
  }
  
  private void zzcum()
  {
    this.bdk.restart();
    this.bdm.restart();
  }
  
  private void zzcuv()
  {
    if (this.baR == null) {
      this.baR = zzcui().zza(this, this.bdo, null);
    }
  }
  
  private void zzcuw()
  {
    if (this.bdm == null) {
      this.bdm = this.bdq.zzb(this);
    }
  }
  
  private void zzcux()
  {
    if (this.bdk == null) {
      this.bdk = zzcui().zza(this);
    }
  }
  
  private void zzcuy()
  {
    if (this.baU == null) {
      this.baU = zzsu(zzcui().zzc(this));
    }
  }
  
  private void zzcuz()
  {
    if (this.bdl == null) {
      this.bdl = zzcui().zza(zzcsj());
    }
  }
  
  private void zzcva()
  {
    if (this.bdn == null) {
      this.bdn = "default";
    }
  }
  
  private String zzsu(String paramString)
  {
    return "Firebase/" + "5" + "/" + FirebaseDatabase.getSdkVersion() + "/" + paramString;
  }
  
  public boolean isFrozen()
  {
    return this.bcW;
  }
  
  void stop()
  {
    this.bdp = true;
    this.bdk.shutdown();
    this.bdm.shutdown();
  }
  
  public zzaiy zza(zzaiw paramzzaiw, zzaiy.zza paramzza)
  {
    return zzcui().zza(this, zzcup(), paramzzaiw, paramzza);
  }
  
  public zzalx zzcsh()
  {
    return this.baR;
  }
  
  public boolean zzcsk()
  {
    return this.baS;
  }
  
  public zzalx.zza zzctu()
  {
    return this.bdo;
  }
  
  void zzctz()
  {
    try
    {
      if (!this.bcW)
      {
        this.bcW = true;
        zzcul();
      }
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public void zzcuk()
  {
    if (this.bdp)
    {
      zzcum();
      this.bdp = false;
    }
  }
  
  protected void zzcun()
  {
    if (isFrozen()) {
      throw new DatabaseException("Modifications to DatabaseConfig objects must occur before they are in use");
    }
  }
  
  public List<String> zzcuo()
  {
    return null;
  }
  
  public zzaiu zzcup()
  {
    return new zzaiu(zzcsh(), zza(zzcuu()), zzcsj(), zzcsk(), FirebaseDatabase.getSdkVersion(), zzux());
  }
  
  public long zzcuq()
  {
    return this.cacheSize;
  }
  
  public zzajn zzcur()
  {
    return this.bdk;
  }
  
  public zzajv zzcus()
  {
    return this.bdm;
  }
  
  public String zzcut()
  {
    return this.bdn;
  }
  
  public zzajf zzcuu()
  {
    return this.bdl;
  }
  
  public zzalw zzss(String paramString)
  {
    return new zzalw(this.baR, paramString);
  }
  
  zzaku zzst(String paramString)
  {
    if (this.baS)
    {
      zzaku localzzaku = this.bdq.zza(this, paramString);
      paramString = localzzaku;
      if (localzzaku == null) {
        throw new IllegalArgumentException("You have enabled persistence, but persistence is not supported on this platform.");
      }
    }
    else
    {
      paramString = new zzakt();
    }
    return paramString;
  }
  
  public String zzux()
  {
    return this.baU;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzajj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */