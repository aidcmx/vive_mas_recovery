package com.google.android.gms.internal;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.Logger.Level;

public class zzajk
  extends zzajj
{
  public void setLogLevel(Logger.Level paramLevel)
  {
    for (;;)
    {
      try
      {
        zzcun();
        switch (1.bdt[paramLevel.ordinal()])
        {
        case 1: 
          paramLevel = String.valueOf(paramLevel);
          throw new IllegalArgumentException(String.valueOf(paramLevel).length() + 19 + "Unknown log level: " + paramLevel);
        }
      }
      finally {}
      this.bdo = zzalx.zza.bio;
      for (;;)
      {
        return;
        this.bdo = zzalx.zza.bip;
        continue;
        this.bdo = zzalx.zza.biq;
        continue;
        this.bdo = zzalx.zza.bir;
        continue;
        this.bdo = zzalx.zza.bis;
      }
    }
  }
  
  public void setPersistenceEnabled(boolean paramBoolean)
  {
    try
    {
      zzcun();
      this.baS = paramBoolean;
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public void zzf(FirebaseApp paramFirebaseApp)
  {
    try
    {
      this.aZF = paramFirebaseApp;
      return;
    }
    finally
    {
      paramFirebaseApp = finally;
      throw paramFirebaseApp;
    }
  }
  
  public void zzsv(String paramString)
  {
    try
    {
      zzcun();
      if ((paramString == null) || (paramString.isEmpty())) {
        throw new IllegalArgumentException("Session identifier is not allowed to be empty or null!");
      }
    }
    finally {}
    this.bdn = paramString;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzajk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */