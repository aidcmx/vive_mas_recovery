package com.google.android.gms.internal;

import com.google.firebase.database.DatabaseError;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class zzajl
{
  private AtomicBoolean bdu = new AtomicBoolean(false);
  private zzajm bdv;
  private boolean bdw = false;
  
  static
  {
    if (!zzajl.class.desiredAssertionStatus()) {}
    for (boolean bool = true;; bool = false)
    {
      $assertionsDisabled = bool;
      return;
    }
  }
  
  public abstract zzajl zza(zzall paramzzall);
  
  public abstract zzalg zza(zzalf paramzzalf, zzall paramzzall);
  
  public void zza(zzajm paramzzajm)
  {
    assert (!zzcvc());
    assert (this.bdv == null);
    this.bdv = paramzzajm;
  }
  
  public abstract void zza(zzalg paramzzalg);
  
  public abstract void zza(DatabaseError paramDatabaseError);
  
  public abstract boolean zza(zzalh.zza paramzza);
  
  public abstract boolean zzc(zzajl paramzzajl);
  
  public abstract zzall zzcud();
  
  public void zzcvb()
  {
    if ((this.bdu.compareAndSet(false, true)) && (this.bdv != null))
    {
      this.bdv.zzd(this);
      this.bdv = null;
    }
  }
  
  public boolean zzcvc()
  {
    return this.bdu.get();
  }
  
  public boolean zzcvd()
  {
    return this.bdw;
  }
  
  public void zzdb(boolean paramBoolean)
  {
    this.bdw = paramBoolean;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzajl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */