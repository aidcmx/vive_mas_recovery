package com.google.android.gms.internal;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;

 enum zzajo
  implements zzajr
{
  static ThreadFactory bdy;
  static final zzakc bdz = new zzakc()
  {
    public void zza(Thread paramAnonymousThread, String paramAnonymousString) {}
    
    public void zza(Thread paramAnonymousThread, Thread.UncaughtExceptionHandler paramAnonymousUncaughtExceptionHandler)
    {
      paramAnonymousThread.setUncaughtExceptionHandler(paramAnonymousUncaughtExceptionHandler);
    }
    
    public void zza(Thread paramAnonymousThread, boolean paramAnonymousBoolean) {}
  };
  
  private zzajo() {}
  
  public static boolean isActive()
  {
    return zzcve() != null;
  }
  
  private static ThreadFactory zzcve()
  {
    if (bdy == null) {}
    try
    {
      Class localClass = Class.forName("com.google.appengine.api.ThreadManager");
      if (localClass != null) {
        bdy = (ThreadFactory)localClass.getMethod("backgroundThreadFactory", new Class[0]).invoke(null, new Object[0]);
      }
      return bdy;
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      return null;
    }
    catch (InvocationTargetException localInvocationTargetException)
    {
      throw new RuntimeException(localInvocationTargetException);
    }
    catch (NoSuchMethodException localNoSuchMethodException)
    {
      throw new RuntimeException(localNoSuchMethodException);
    }
    catch (IllegalAccessException localIllegalAccessException)
    {
      throw new RuntimeException(localIllegalAccessException);
    }
  }
  
  public void initialize()
  {
    zzamv.zza(bdy, new zzamu()
    {
      public void zza(Thread paramAnonymousThread, String paramAnonymousString)
      {
        zzajo.bdz.zza(paramAnonymousThread, paramAnonymousString);
      }
    });
  }
  
  public zzaiy zza(zzajj paramzzajj, zzaiu paramzzaiu, zzaiw paramzzaiw, zzaiy.zza paramzza)
  {
    return new zzaiz(paramzzajj.zzcup(), paramzzaiw, paramzza);
  }
  
  public zzajf zza(ScheduledExecutorService paramScheduledExecutorService)
  {
    throw new RuntimeException("Authentication is not implemented yet");
  }
  
  public zzajn zza(zzajj paramzzajj)
  {
    return new zzakd(zzcve(), bdz);
  }
  
  public zzaku zza(zzajj paramzzajj, String paramString)
  {
    return null;
  }
  
  public zzalx zza(zzajj paramzzajj, zzalx.zza paramzza, List<String> paramList)
  {
    return new zzalv(paramzza, paramList);
  }
  
  public zzajv zzb(zzajj paramzzajj)
  {
    new zzanh()
    {
      protected ThreadFactory getThreadFactory()
      {
        return zzajo.bdy;
      }
      
      protected zzakc zzcvf()
      {
        return zzajo.bdz;
      }
      
      public void zzj(Throwable paramAnonymousThrowable)
      {
        this.aZL.zzd(zzanh.zzl(paramAnonymousThrowable), paramAnonymousThrowable);
      }
    };
  }
  
  public String zzc(zzajj paramzzajj)
  {
    paramzzajj = System.getProperty("java.specification.version", "Unknown");
    return String.valueOf(paramzzajj).length() + 1 + String.valueOf("AppEngine").length() + paramzzajj + "/" + "AppEngine";
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzajo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */