package com.google.android.gms.internal;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

 enum zzajp
  implements zzajr
{
  private zzajp() {}
  
  public zzaiy zza(zzajj paramzzajj, zzaiu paramzzaiu, zzaiw paramzzaiw, zzaiy.zza paramzza)
  {
    return new zzaiz(paramzzajj.zzcup(), paramzzaiw, paramzza);
  }
  
  public zzajf zza(ScheduledExecutorService paramScheduledExecutorService)
  {
    throw new RuntimeException("Authentication is not implemented yet");
  }
  
  public zzajn zza(zzajj paramzzajj)
  {
    return new zzakd(Executors.defaultThreadFactory(), zzakc.bfH);
  }
  
  public zzaku zza(zzajj paramzzajj, String paramString)
  {
    return null;
  }
  
  public zzalx zza(zzajj paramzzajj, zzalx.zza paramzza, List<String> paramList)
  {
    return new zzalv(paramzza, paramList);
  }
  
  public zzajv zzb(zzajj paramzzajj)
  {
    new zzanh()
    {
      public void zzj(Throwable paramAnonymousThrowable)
      {
        this.aZL.zzd(zzanh.zzl(paramAnonymousThrowable), paramAnonymousThrowable);
      }
    };
  }
  
  public String zzc(zzajj paramzzajj)
  {
    paramzzajj = System.getProperty("java.vm.name", "Unknown JVM");
    String str = System.getProperty("java.specification.version", "Unknown");
    return String.valueOf(str).length() + 1 + String.valueOf(paramzzajj).length() + str + "/" + paramzzajj;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzajp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */