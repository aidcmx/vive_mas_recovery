package com.google.android.gms.internal;

import com.google.firebase.database.DatabaseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public class zzajq
  implements Comparable<zzajq>, Iterable<zzalz>
{
  private static final zzajq bdG;
  private final zzalz[] bdF;
  private final int end;
  private final int start;
  
  static
  {
    if (!zzajq.class.desiredAssertionStatus()) {}
    for (boolean bool = true;; bool = false)
    {
      $assertionsDisabled = bool;
      bdG = new zzajq("");
      return;
    }
  }
  
  public zzajq(String paramString)
  {
    paramString = paramString.split("/");
    int m = paramString.length;
    int i = 0;
    int k;
    for (int j = 0; i < m; j = k)
    {
      k = j;
      if (paramString[i].length() > 0) {
        k = j + 1;
      }
      i += 1;
    }
    this.bdF = new zzalz[j];
    m = paramString.length;
    j = 0;
    i = 0;
    if (j < m)
    {
      String str = paramString[j];
      if (str.length() <= 0) {
        break label132;
      }
      zzalz[] arrayOfzzalz = this.bdF;
      k = i + 1;
      arrayOfzzalz[i] = zzalz.zzsx(str);
      i = k;
    }
    label132:
    for (;;)
    {
      j += 1;
      break;
      this.start = 0;
      this.end = this.bdF.length;
      return;
    }
  }
  
  public zzajq(List<String> paramList)
  {
    this.bdF = new zzalz[paramList.size()];
    Iterator localIterator = paramList.iterator();
    int i = 0;
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      this.bdF[i] = zzalz.zzsx(str);
      i += 1;
    }
    this.start = 0;
    this.end = paramList.size();
  }
  
  public zzajq(zzalz... paramVarArgs)
  {
    this.bdF = ((zzalz[])Arrays.copyOf(paramVarArgs, paramVarArgs.length));
    this.start = 0;
    this.end = paramVarArgs.length;
    int j = paramVarArgs.length;
    int i = 0;
    while (i < j)
    {
      zzalz localzzalz = paramVarArgs[i];
      assert (localzzalz != null) : "Can't construct a path with a null value!";
      i += 1;
    }
  }
  
  private zzajq(zzalz[] paramArrayOfzzalz, int paramInt1, int paramInt2)
  {
    this.bdF = paramArrayOfzzalz;
    this.start = paramInt1;
    this.end = paramInt2;
  }
  
  public static zzajq zza(zzajq paramzzajq1, zzajq paramzzajq2)
  {
    zzalz localzzalz1 = paramzzajq1.zzcvj();
    zzalz localzzalz2 = paramzzajq2.zzcvj();
    if (localzzalz1 == null) {
      return paramzzajq2;
    }
    if (localzzalz1.equals(localzzalz2)) {
      return zza(paramzzajq1.zzcvk(), paramzzajq2.zzcvk());
    }
    paramzzajq2 = String.valueOf(paramzzajq2);
    paramzzajq1 = String.valueOf(paramzzajq1);
    throw new DatabaseException(String.valueOf(paramzzajq2).length() + 37 + String.valueOf(paramzzajq1).length() + "INTERNAL ERROR: " + paramzzajq2 + " is not contained in " + paramzzajq1);
  }
  
  public static zzajq zzcvg()
  {
    return bdG;
  }
  
  public boolean equals(Object paramObject)
  {
    if (!(paramObject instanceof zzajq)) {
      return false;
    }
    if (this == paramObject) {
      return true;
    }
    paramObject = (zzajq)paramObject;
    if (size() != ((zzajq)paramObject).size()) {
      return false;
    }
    int j = this.start;
    int i = ((zzajq)paramObject).start;
    while ((j < this.end) && (i < ((zzajq)paramObject).end))
    {
      if (!this.bdF[j].equals(paramObject.bdF[i])) {
        return false;
      }
      j += 1;
      i += 1;
    }
    return true;
  }
  
  public int hashCode()
  {
    int j = 0;
    int i = this.start;
    while (i < this.end)
    {
      j = j * 37 + this.bdF[i].hashCode();
      i += 1;
    }
    return j;
  }
  
  public boolean isEmpty()
  {
    return this.start >= this.end;
  }
  
  public Iterator<zzalz> iterator()
  {
    new Iterator()
    {
      int offset = zzajq.zzk(zzajq.this);
      
      public boolean hasNext()
      {
        return this.offset < zzajq.zzl(zzajq.this);
      }
      
      public void remove()
      {
        throw new UnsupportedOperationException("Can't remove component from immutable Path!");
      }
      
      public zzalz zzcvn()
      {
        if (!hasNext()) {
          throw new NoSuchElementException("No more elements.");
        }
        zzalz localzzalz = zzajq.zzm(zzajq.this)[this.offset];
        this.offset += 1;
        return localzzalz;
      }
    };
  }
  
  public int size()
  {
    return this.end - this.start;
  }
  
  public String toString()
  {
    if (isEmpty()) {
      return "/";
    }
    StringBuilder localStringBuilder = new StringBuilder();
    int i = this.start;
    while (i < this.end)
    {
      localStringBuilder.append("/");
      localStringBuilder.append(this.bdF[i].asString());
      i += 1;
    }
    return localStringBuilder.toString();
  }
  
  public zzajq zza(zzalz paramzzalz)
  {
    int i = size();
    zzalz[] arrayOfzzalz = new zzalz[i + 1];
    System.arraycopy(this.bdF, this.start, arrayOfzzalz, 0, i);
    arrayOfzzalz[i] = paramzzalz;
    return new zzajq(arrayOfzzalz, 0, i + 1);
  }
  
  public String zzcvh()
  {
    if (isEmpty()) {
      return "/";
    }
    StringBuilder localStringBuilder = new StringBuilder();
    int i = this.start;
    while (i < this.end)
    {
      if (i > this.start) {
        localStringBuilder.append("/");
      }
      localStringBuilder.append(this.bdF[i].asString());
      i += 1;
    }
    return localStringBuilder.toString();
  }
  
  public List<String> zzcvi()
  {
    ArrayList localArrayList = new ArrayList(size());
    Iterator localIterator = iterator();
    while (localIterator.hasNext()) {
      localArrayList.add(((zzalz)localIterator.next()).asString());
    }
    return localArrayList;
  }
  
  public zzalz zzcvj()
  {
    if (isEmpty()) {
      return null;
    }
    return this.bdF[this.start];
  }
  
  public zzajq zzcvk()
  {
    int j = this.start;
    int i = j;
    if (!isEmpty()) {
      i = j + 1;
    }
    return new zzajq(this.bdF, i, this.end);
  }
  
  public zzajq zzcvl()
  {
    if (isEmpty()) {
      return null;
    }
    return new zzajq(this.bdF, this.start, this.end - 1);
  }
  
  public zzalz zzcvm()
  {
    if (!isEmpty()) {
      return this.bdF[(this.end - 1)];
    }
    return null;
  }
  
  public zzajq zzh(zzajq paramzzajq)
  {
    int i = size() + paramzzajq.size();
    zzalz[] arrayOfzzalz = new zzalz[i];
    System.arraycopy(this.bdF, this.start, arrayOfzzalz, 0, size());
    System.arraycopy(paramzzajq.bdF, paramzzajq.start, arrayOfzzalz, size(), paramzzajq.size());
    return new zzajq(arrayOfzzalz, 0, i);
  }
  
  public boolean zzi(zzajq paramzzajq)
  {
    if (size() > paramzzajq.size()) {
      return false;
    }
    int j = this.start;
    int i = paramzzajq.start;
    while (j < this.end)
    {
      if (!this.bdF[j].equals(paramzzajq.bdF[i])) {
        return false;
      }
      j += 1;
      i += 1;
    }
    return true;
  }
  
  public int zzj(zzajq paramzzajq)
  {
    int j = this.start;
    int i = paramzzajq.start;
    while ((j < this.end) && (i < paramzzajq.end))
    {
      int k = this.bdF[j].zzi(paramzzajq.bdF[i]);
      if (k != 0) {
        return k;
      }
      j += 1;
      i += 1;
    }
    if ((j == this.end) && (i == paramzzajq.end)) {
      return 0;
    }
    if (j == this.end) {
      return -1;
    }
    return 1;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzajq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */