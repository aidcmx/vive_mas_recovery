package com.google.android.gms.internal;

import java.util.List;
import java.util.concurrent.ScheduledExecutorService;

public abstract interface zzajr
{
  public abstract zzaiy zza(zzajj paramzzajj, zzaiu paramzzaiu, zzaiw paramzzaiw, zzaiy.zza paramzza);
  
  public abstract zzajf zza(ScheduledExecutorService paramScheduledExecutorService);
  
  public abstract zzajn zza(zzajj paramzzajj);
  
  public abstract zzaku zza(zzajj paramzzajj, String paramString);
  
  public abstract zzalx zza(zzajj paramzzajj, zzalx.zza paramzza, List<String> paramList);
  
  public abstract zzajv zzb(zzajj paramzzajj);
  
  public abstract String zzc(zzajj paramzzajj);
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzajr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */