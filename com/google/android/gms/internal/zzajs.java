package com.google.android.gms.internal;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.DatabaseReference.CompletionListener;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.Transaction.Handler;
import com.google.firebase.database.Transaction.Result;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.database.zza;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class zzajs
  implements zzaiy.zza
{
  private final zzajt aZh;
  private final zzaiy bcu;
  private final zzanj bdI = new zzanj(new zzang(), 0L);
  private zzajx bdJ;
  private zzajy bdK;
  private zzalb<List<zza>> bdL;
  private boolean bdM = false;
  private final zzalj bdN;
  private final zzajj bdO;
  private final zzalw bdP;
  private final zzalw bdQ;
  private final zzalw bdR;
  public long bdS = 0L;
  private long bdT = 1L;
  private zzaka bdU;
  private zzaka bdV;
  private FirebaseDatabase bdW;
  private boolean bdX = false;
  private long bdY = 0L;
  
  static
  {
    if (!zzajs.class.desiredAssertionStatus()) {}
    for (boolean bool = true;; bool = false)
    {
      $assertionsDisabled = bool;
      return;
    }
  }
  
  zzajs(zzajt paramzzajt, zzajj paramzzajj, FirebaseDatabase paramFirebaseDatabase)
  {
    this.aZh = paramzzajt;
    this.bdO = paramzzajj;
    this.bdW = paramFirebaseDatabase;
    this.bdP = this.bdO.zzss("RepoOperation");
    this.bdQ = this.bdO.zzss("Transaction");
    this.bdR = this.bdO.zzss("DataOperation");
    this.bdN = new zzalj(this.bdO);
    this.bcu = paramzzajj.zza(new zzaiw(paramzzajt.baV, paramzzajt.EY, paramzzajt.baW), this);
    zzs(new Runnable()
    {
      public void run()
      {
        zzajs.zza(zzajs.this);
      }
    });
  }
  
  private zzaml zza(zzajq paramzzajq, List<Long> paramList)
  {
    paramList = this.bdV.zzc(paramzzajq, paramList);
    paramzzajq = paramList;
    if (paramList == null) {
      paramzzajq = zzame.zzczq();
    }
    return paramzzajq;
  }
  
  private void zza(long paramLong, zzajq paramzzajq, DatabaseError paramDatabaseError)
  {
    if ((paramDatabaseError != null) && (paramDatabaseError.getCode() == -25)) {
      return;
    }
    int i;
    if (paramDatabaseError == null)
    {
      i = 1;
      paramDatabaseError = this.bdV;
      if (i != 0) {
        break label82;
      }
    }
    label82:
    for (boolean bool = true;; bool = false)
    {
      paramDatabaseError = paramDatabaseError.zza(paramLong, bool, true, this.bdI);
      if (paramDatabaseError.size() > 0) {
        zzo(paramzzajq);
      }
      zzav(paramDatabaseError);
      return;
      i = 0;
      break;
    }
  }
  
  private void zza(zzaku paramzzaku)
  {
    Object localObject1 = paramzzaku.zzcre();
    paramzzaku = zzajw.zza(this.bdI);
    localObject1 = ((List)localObject1).iterator();
    long l1 = Long.MIN_VALUE;
    if (((Iterator)localObject1).hasNext())
    {
      final zzake localzzake = (zzake)((Iterator)localObject1).next();
      Object localObject2 = new zzajb()
      {
        public void zzbn(String paramAnonymousString1, String paramAnonymousString2)
        {
          paramAnonymousString1 = zzajs.zzbp(paramAnonymousString1, paramAnonymousString2);
          zzajs.zza(zzajs.this, "Persisted write", localzzake.zzcrc(), paramAnonymousString1);
          zzajs.zza(zzajs.this, localzzake.zzcwd(), localzzake.zzcrc(), paramAnonymousString1);
        }
      };
      if (l1 >= localzzake.zzcwd()) {
        throw new IllegalStateException("Write ids were not in order.");
      }
      l1 = localzzake.zzcwd();
      this.bdT = (localzzake.zzcwd() + 1L);
      zzalw localzzalw;
      long l2;
      if (localzzake.zzcwg())
      {
        if (this.bdP.zzcyu())
        {
          localzzalw = this.bdP;
          l2 = localzzake.zzcwd();
          localzzalw.zzi(48 + "Restoring overwrite with id " + l2, new Object[0]);
        }
        this.bcu.zza(localzzake.zzcrc().zzcvi(), localzzake.zzcwe().getValue(true), (zzajb)localObject2);
        localObject2 = zzajw.zza(localzzake.zzcwe(), paramzzaku);
        this.bdV.zza(localzzake.zzcrc(), localzzake.zzcwe(), (zzaml)localObject2, localzzake.zzcwd(), true, false);
      }
      for (;;)
      {
        break;
        if (this.bdP.zzcyu())
        {
          localzzalw = this.bdP;
          l2 = localzzake.zzcwd();
          localzzalw.zzi(44 + "Restoring merge with id " + l2, new Object[0]);
        }
        this.bcu.zza(localzzake.zzcrc().zzcvi(), localzzake.zzcwf().zzda(true), (zzajb)localObject2);
        localObject2 = zzajw.zza(localzzake.zzcwf(), paramzzaku);
        this.bdV.zza(localzzake.zzcrc(), localzzake.zzcwf(), (zzajh)localObject2, localzzake.zzcwd(), false);
      }
    }
  }
  
  private void zza(zzalb<List<zza>> paramzzalb)
  {
    List localList;
    if ((List)paramzzalb.getValue() != null)
    {
      localList = zzc(paramzzalb);
      assert (localList.size() > 0);
      localObject = localList.iterator();
      do
      {
        if (!((Iterator)localObject).hasNext()) {
          break;
        }
      } while (zza.zzd((zza)((Iterator)localObject).next()) == zzb.beK);
    }
    for (Object localObject = Boolean.valueOf(false);; localObject = Boolean.valueOf(true))
    {
      if (((Boolean)localObject).booleanValue()) {
        zza(localList, paramzzalb.zzcrc());
      }
      do
      {
        return;
      } while (!paramzzalb.hasChildren());
      paramzzalb.zzb(new zzalb.zzb()
      {
        public void zzd(zzalb<List<zzajs.zza>> paramAnonymouszzalb)
        {
          zzajs.zza(zzajs.this, paramAnonymouszzalb);
        }
      });
      return;
    }
  }
  
  private void zza(zzalb<List<zza>> paramzzalb, int paramInt)
  {
    List localList = (List)paramzzalb.getValue();
    ArrayList localArrayList1 = new ArrayList();
    if (localList != null)
    {
      ArrayList localArrayList2 = new ArrayList();
      final DatabaseError localDatabaseError;
      int j;
      int i;
      label51:
      final zza localzza;
      if (paramInt == -9)
      {
        localDatabaseError = DatabaseError.zzsa("overriddenBySet");
        j = 0;
        i = -1;
        if (j >= localList.size()) {
          break label355;
        }
        localzza = (zza)localList.get(j);
        if (zza.zzd(localzza) != zzb.beN) {
          break label149;
        }
      }
      for (;;)
      {
        j += 1;
        break label51;
        if (paramInt == -25) {}
        for (bool = true;; bool = false)
        {
          zzann.zzb(bool, 45 + "Unknown transaction abort reason: " + paramInt);
          localDatabaseError = DatabaseError.zzafs(-25);
          break;
        }
        label149:
        if (zza.zzd(localzza) == zzb.beL)
        {
          assert (i == j - 1);
          zza.zza(localzza, zzb.beN);
          zza.zza(localzza, localDatabaseError);
          i = j;
        }
        else
        {
          assert (zza.zzd(localzza) == zzb.beK);
          zze(new zzakg(this, zza.zzj(localzza), zzall.zzan(zza.zzf(localzza))));
          if (paramInt != -9) {
            break label310;
          }
          localArrayList1.addAll(this.bdV.zza(zza.zzc(localzza), true, false, this.bdI));
          localArrayList2.add(new Runnable()
          {
            public void run()
            {
              zzajs.zza.zzi(localzza).onComplete(localDatabaseError, false, null);
            }
          });
        }
      }
      label310:
      if (paramInt == -25) {}
      for (boolean bool = true;; bool = false)
      {
        zzann.zzb(bool, 45 + "Unknown transaction abort reason: " + paramInt);
        break;
      }
      label355:
      if (i == -1) {
        paramzzalb.setValue(null);
      }
      for (;;)
      {
        zzav(localArrayList1);
        paramzzalb = localArrayList2.iterator();
        while (paramzzalb.hasNext()) {
          zzq((Runnable)paramzzalb.next());
        }
        paramzzalb.setValue(localList.subList(0, i + 1));
      }
    }
  }
  
  private void zza(String paramString, zzajq paramzzajq, DatabaseError paramDatabaseError)
  {
    if ((paramDatabaseError != null) && (paramDatabaseError.getCode() != -1) && (paramDatabaseError.getCode() != -25))
    {
      zzalw localzzalw = this.bdP;
      paramzzajq = String.valueOf(paramzzajq.toString());
      paramDatabaseError = String.valueOf(paramDatabaseError.toString());
      localzzalw.warn(String.valueOf(paramString).length() + 13 + String.valueOf(paramzzajq).length() + String.valueOf(paramDatabaseError).length() + paramString + " at " + paramzzajq + " failed: " + paramDatabaseError);
    }
  }
  
  private void zza(final List<zza> paramList, final zzajq paramzzajq)
  {
    Object localObject1 = new ArrayList();
    Object localObject2 = paramList.iterator();
    while (((Iterator)localObject2).hasNext()) {
      ((List)localObject1).add(Long.valueOf(zza.zzc((zza)((Iterator)localObject2).next())));
    }
    localObject1 = zza(paramzzajq, (List)localObject1);
    localObject2 = ((zzaml)localObject1).zzczd();
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
    {
      zza localzza = (zza)localIterator.next();
      assert (zza.zzd(localzza) == zzb.beK);
      zza.zza(localzza, zzb.beL);
      zza.zze(localzza);
      localObject1 = ((zzaml)localObject1).zzl(zzajq.zza(paramzzajq, zza.zzf(localzza)), zza.zzg(localzza));
    }
    localObject1 = ((zzaml)localObject1).getValue(true);
    zzcvs();
    this.bcu.zza(paramzzajq.zzcvi(), localObject1, (String)localObject2, new zzajb()
    {
      public void zzbn(String paramAnonymousString1, String paramAnonymousString2)
      {
        int i = 0;
        paramAnonymousString2 = zzajs.zzbp(paramAnonymousString1, paramAnonymousString2);
        zzajs.zza(zzajs.this, "Transaction", paramzzajq, paramAnonymousString2);
        paramAnonymousString1 = new ArrayList();
        Object localObject;
        if (paramAnonymousString2 == null)
        {
          paramAnonymousString2 = new ArrayList();
          localObject = paramList.iterator();
          while (((Iterator)localObject).hasNext())
          {
            final zzajs.zza localzza = (zzajs.zza)((Iterator)localObject).next();
            zzajs.zza.zza(localzza, zzajs.zzb.beM);
            paramAnonymousString1.addAll(zzajs.zzg(zzajs.this).zza(zzajs.zza.zzc(localzza), false, false, zzajs.zzh(zzajs.this)));
            zzaml localzzaml = zzajs.zza.zzh(localzza);
            paramAnonymousString2.add(new Runnable()
            {
              public void run()
              {
                zzajs.zza.zzi(localzza).onComplete(null, true, this.bef);
              }
            });
            zzajs.this.zze(new zzakg(zzajs.this, zzajs.zza.zzj(localzza), zzall.zzan(zzajs.zza.zzf(localzza))));
          }
          zzajs.zzb(zzajs.this, zzajs.zzi(zzajs.this).zzal(paramzzajq));
          zzajs.zzj(zzajs.this);
          zzajs.zza(jdField_this, paramAnonymousString1);
          while (i < paramAnonymousString2.size())
          {
            zzajs.this.zzq((Runnable)paramAnonymousString2.get(i));
            i += 1;
          }
        }
        if (paramAnonymousString2.getCode() == -1)
        {
          paramAnonymousString1 = paramList.iterator();
          while (paramAnonymousString1.hasNext())
          {
            paramAnonymousString2 = (zzajs.zza)paramAnonymousString1.next();
            if (zzajs.zza.zzd(paramAnonymousString2) == zzajs.zzb.beN) {
              zzajs.zza.zza(paramAnonymousString2, zzajs.zzb.beO);
            } else {
              zzajs.zza.zza(paramAnonymousString2, zzajs.zzb.beK);
            }
          }
        }
        paramAnonymousString1 = paramList.iterator();
        while (paramAnonymousString1.hasNext())
        {
          localObject = (zzajs.zza)paramAnonymousString1.next();
          zzajs.zza.zza((zzajs.zza)localObject, zzajs.zzb.beO);
          zzajs.zza.zza((zzajs.zza)localObject, paramAnonymousString2);
        }
        zzajs.zzb(zzajs.this, paramzzajq);
      }
    });
  }
  
  private void zza(final List<zza> paramList, zzalb<List<zza>> paramzzalb)
  {
    List localList = (List)paramzzalb.getValue();
    if (localList != null) {
      paramList.addAll(localList);
    }
    paramzzalb.zzb(new zzalb.zzb()
    {
      public void zzd(zzalb<List<zzajs.zza>> paramAnonymouszzalb)
      {
        zzajs.zza(zzajs.this, paramList, paramAnonymouszzalb);
      }
    });
  }
  
  private void zzav(List<? extends zzalh> paramList)
  {
    if (!paramList.isEmpty()) {
      this.bdN.zzax(paramList);
    }
  }
  
  private zzajq zzb(zzajq paramzzajq, final int paramInt)
  {
    zzajq localzzajq = zzp(paramzzajq).zzcrc();
    if (this.bdQ.zzcyu())
    {
      zzalw localzzalw = this.bdP;
      String str1 = String.valueOf(paramzzajq);
      String str2 = String.valueOf(localzzajq);
      localzzalw.zzi(String.valueOf(str1).length() + 44 + String.valueOf(str2).length() + "Aborting transactions for path: " + str1 + ". Affected: " + str2, new Object[0]);
    }
    paramzzajq = this.bdL.zzal(paramzzajq);
    paramzzajq.zza(new zzalb.zza()
    {
      public boolean zze(zzalb<List<zzajs.zza>> paramAnonymouszzalb)
      {
        zzajs.zza(zzajs.this, paramAnonymouszzalb, paramInt);
        return false;
      }
    });
    zza(paramzzajq, paramInt);
    paramzzajq.zza(new zzalb.zzb()
    {
      public void zzd(zzalb<List<zzajs.zza>> paramAnonymouszzalb)
      {
        zzajs.zza(zzajs.this, paramAnonymouszzalb, paramInt);
      }
    });
    return localzzajq;
  }
  
  private void zzb(zzalb<List<zza>> paramzzalb)
  {
    List localList = (List)paramzzalb.getValue();
    if (localList != null)
    {
      int i = 0;
      if (i < localList.size())
      {
        if (zza.zzd((zza)localList.get(i)) == zzb.beM) {
          localList.remove(i);
        }
        for (;;)
        {
          break;
          i += 1;
        }
      }
      if (localList.size() <= 0) {
        break label88;
      }
      paramzzalb.setValue(localList);
    }
    for (;;)
    {
      paramzzalb.zzb(new zzalb.zzb()
      {
        public void zzd(zzalb<List<zzajs.zza>> paramAnonymouszzalb)
        {
          zzajs.zzb(zzajs.this, paramAnonymouszzalb);
        }
      });
      return;
      label88:
      paramzzalb.setValue(null);
    }
  }
  
  private void zzb(zzalz paramzzalz, Object paramObject)
  {
    if (paramzzalz.equals(zzaji.bdh)) {
      this.bdI.zzcn(((Long)paramObject).longValue());
    }
    paramzzalz = new zzajq(new zzalz[] { zzaji.bdg, paramzzalz });
    try
    {
      paramObject = zzamm.zzbt(paramObject);
      this.bdJ.zzg(paramzzalz, (zzaml)paramObject);
      zzav(this.bdU.zzi(paramzzalz, (zzaml)paramObject));
      return;
    }
    catch (DatabaseException paramzzalz)
    {
      this.bdP.zzd("Failed to parse info update", paramzzalz);
    }
  }
  
  private void zzb(final List<zza> paramList, zzajq paramzzajq)
  {
    if (paramList.isEmpty()) {
      return;
    }
    ArrayList localArrayList1 = new ArrayList();
    ArrayList localArrayList2 = new ArrayList();
    final Object localObject1 = paramList.iterator();
    while (((Iterator)localObject1).hasNext()) {
      localArrayList2.add(Long.valueOf(zza.zzc((zza)((Iterator)localObject1).next())));
    }
    Iterator localIterator = paramList.iterator();
    final zza localzza;
    ArrayList localArrayList3;
    int i;
    if (localIterator.hasNext())
    {
      localzza = (zza)localIterator.next();
      paramList = zzajq.zza(paramzzajq, zza.zzf(localzza));
      localArrayList3 = new ArrayList();
      assert (paramList != null);
      if (zza.zzd(localzza) == zzb.beO)
      {
        int j = 1;
        localObject1 = zza.zzk(localzza);
        i = j;
        paramList = (List<zza>)localObject1;
        if (((DatabaseError)localObject1).getCode() != -25)
        {
          localArrayList3.addAll(this.bdV.zza(zza.zzc(localzza), true, false, this.bdI));
          paramList = (List<zza>)localObject1;
          i = j;
        }
      }
    }
    for (;;)
    {
      zzav(localArrayList3);
      if (i == 0) {
        break;
      }
      zza.zza(localzza, zzb.beM);
      localObject1 = zza.zza(zza.zza(this, zza.zzf(localzza)), zzamg.zzn(zza.zzb(localzza)));
      zzs(new Runnable()
      {
        public void run()
        {
          zzajs.this.zze(new zzakg(zzajs.this, zzajs.zza.zzj(localzza), zzall.zzan(zzajs.zza.zzf(localzza))));
        }
      });
      localArrayList1.add(new Runnable()
      {
        public void run()
        {
          zzajs.zza.zzi(localzza).onComplete(paramList, false, localObject1);
        }
      });
      break;
      if (zza.zzd(localzza) == zzb.beK)
      {
        if (zza.zzl(localzza) >= 25)
        {
          i = 1;
          paramList = DatabaseError.zzsa("maxretries");
          localArrayList3.addAll(this.bdV.zza(zza.zzc(localzza), true, false, this.bdI));
        }
        else
        {
          paramList = zza(zza.zzf(localzza), localArrayList2);
          zza.zza(localzza, paramList);
          paramList = zza.zza(paramList);
          try
          {
            localObject1 = zza.zzi(localzza).doTransaction(paramList);
            paramList = null;
          }
          catch (Throwable paramList)
          {
            for (;;)
            {
              Object localObject2;
              paramList = DatabaseError.fromException(paramList);
              localObject1 = Transaction.abort();
            }
            i = 1;
            localArrayList3.addAll(this.bdV.zza(zza.zzc(localzza), true, false, this.bdI));
          }
          if (((Transaction.Result)localObject1).isSuccess())
          {
            paramList = Long.valueOf(zza.zzc(localzza));
            localObject2 = zzajw.zza(this.bdI);
            localObject1 = ((Transaction.Result)localObject1).zzcqy();
            localObject2 = zzajw.zza((zzaml)localObject1, (Map)localObject2);
            zza.zzb(localzza, (zzaml)localObject1);
            zza.zzc(localzza, (zzaml)localObject2);
            zza.zza(localzza, zzcvs());
            localArrayList2.remove(paramList);
            localArrayList3.addAll(this.bdV.zza(zza.zzf(localzza), (zzaml)localObject1, (zzaml)localObject2, zza.zzc(localzza), zza.zzm(localzza), false));
            localArrayList3.addAll(this.bdV.zza(paramList.longValue(), true, false, this.bdI));
            paramList = null;
            i = 0;
          }
          else
          {
            continue;
            zzb(this.bdL);
            i = 0;
            while (i < localArrayList1.size())
            {
              zzq((Runnable)localArrayList1.get(i));
              i += 1;
            }
            zzcvu();
          }
        }
      }
      else
      {
        paramList = null;
        i = 0;
      }
    }
  }
  
  private static DatabaseError zzbo(String paramString1, String paramString2)
  {
    if (paramString1 != null) {
      return DatabaseError.zzbl(paramString1, paramString2);
    }
    return null;
  }
  
  private List<zza> zzc(zzalb<List<zza>> paramzzalb)
  {
    ArrayList localArrayList = new ArrayList();
    zza(localArrayList, paramzzalb);
    Collections.sort(localArrayList);
    return localArrayList;
  }
  
  private void zzcvo()
  {
    this.bdO.zzcuu().zza(new zzajf.zzb()
    {
      public void zzsr(String paramAnonymousString)
      {
        zzajs.zzb(zzajs.this).zzi("Auth token changed, triggering auth token refresh", new Object[0]);
        zzajs.zzc(zzajs.this).zzsk(paramAnonymousString);
      }
    });
    this.bcu.initialize();
    zzaku localzzaku = this.bdO.zzst(this.aZh.baV);
    this.bdJ = new zzajx();
    this.bdK = new zzajy();
    this.bdL = new zzalb();
    this.bdU = new zzaka(this.bdO, new zzakt(), new zzaka.zzd()
    {
      public void zza(zzall paramAnonymouszzall, zzakb paramAnonymouszzakb) {}
      
      public void zza(final zzall paramAnonymouszzall, zzakb paramAnonymouszzakb, zzaix paramAnonymouszzaix, final zzaka.zza paramAnonymouszza)
      {
        zzajs.this.zzs(new Runnable()
        {
          public void run()
          {
            Object localObject = zzajs.zzd(zzajs.this).zzq(paramAnonymouszzall.zzcrc());
            if (!((zzaml)localObject).isEmpty())
            {
              localObject = zzajs.zze(zzajs.this).zzi(paramAnonymouszzall.zzcrc(), (zzaml)localObject);
              zzajs.zza(zzajs.this, (List)localObject);
              paramAnonymouszza.zzb(null);
            }
          }
        });
      }
    });
    this.bdV = new zzaka(this.bdO, localzzaku, new zzaka.zzd()
    {
      public void zza(zzall paramAnonymouszzall, zzakb paramAnonymouszzakb)
      {
        zzajs.zzc(zzajs.this).zza(paramAnonymouszzall.zzcrc().zzcvi(), paramAnonymouszzall.zzcyh().zzcyd());
      }
      
      public void zza(zzall paramAnonymouszzall, zzakb paramAnonymouszzakb, zzaix paramAnonymouszzaix, final zzaka.zza paramAnonymouszza)
      {
        zzaiy localzzaiy = zzajs.zzc(zzajs.this);
        List localList = paramAnonymouszzall.zzcrc().zzcvi();
        Map localMap = paramAnonymouszzall.zzcyh().zzcyd();
        if (paramAnonymouszzakb != null) {}
        for (paramAnonymouszzall = Long.valueOf(paramAnonymouszzakb.zzcwc());; paramAnonymouszzall = null)
        {
          localzzaiy.zza(localList, localMap, paramAnonymouszzaix, paramAnonymouszzall, new zzajb()
          {
            public void zzbn(String paramAnonymous2String1, String paramAnonymous2String2)
            {
              paramAnonymous2String1 = zzajs.zzbp(paramAnonymous2String1, paramAnonymous2String2);
              paramAnonymous2String1 = paramAnonymouszza.zzb(paramAnonymous2String1);
              zzajs.zza(zzajs.this, paramAnonymous2String1);
            }
          });
          return;
        }
      }
    });
    zza(localzzaku);
    zzb(zzaji.bdi, Boolean.valueOf(false));
    zzb(zzaji.bdj, Boolean.valueOf(false));
  }
  
  private long zzcvs()
  {
    long l = this.bdT;
    this.bdT = (1L + l);
    return l;
  }
  
  private void zzcvt()
  {
    Object localObject = zzajw.zza(this.bdI);
    localObject = zzajw.zza(this.bdK, (Map)localObject);
    final ArrayList localArrayList = new ArrayList();
    ((zzajy)localObject).zza(zzajq.zzcvg(), new zzajy.zzb()
    {
      public void zzf(zzajq paramAnonymouszzajq, zzaml paramAnonymouszzaml)
      {
        localArrayList.addAll(zzajs.zzg(zzajs.this).zzi(paramAnonymouszzajq, paramAnonymouszzaml));
        paramAnonymouszzajq = zzajs.zza(zzajs.this, paramAnonymouszzajq, -9);
        zzajs.zzb(zzajs.this, paramAnonymouszzajq);
      }
    });
    this.bdK = new zzajy();
    zzav(localArrayList);
  }
  
  private void zzcvu()
  {
    zzalb localzzalb = this.bdL;
    zzb(localzzalb);
    zza(localzzalb);
  }
  
  private long zzcvv()
  {
    long l = this.bdY;
    this.bdY = (1L + l);
    return l;
  }
  
  private zzaml zzn(zzajq paramzzajq)
  {
    return zza(paramzzajq, new ArrayList());
  }
  
  private zzajq zzo(zzajq paramzzajq)
  {
    paramzzajq = zzp(paramzzajq);
    zzajq localzzajq = paramzzajq.zzcrc();
    zzb(zzc(paramzzajq), localzzajq);
    return localzzajq;
  }
  
  private zzalb<List<zza>> zzp(zzajq paramzzajq)
  {
    zzalb localzzalb = this.bdL;
    while ((!paramzzajq.isEmpty()) && (localzzalb.getValue() == null))
    {
      localzzalb = localzzalb.zzal(new zzajq(new zzalz[] { paramzzajq.zzcvj() }));
      paramzzajq = paramzzajq.zzcvk();
    }
    return localzzalb;
  }
  
  public FirebaseDatabase getDatabase()
  {
    return this.bdW;
  }
  
  void interrupt()
  {
    this.bcu.interrupt("repo_interrupt");
  }
  
  public void onDisconnect()
  {
    zza(zzaji.bdj, Boolean.valueOf(false));
    zzcvt();
  }
  
  public void purgeOutstandingWrites()
  {
    if (this.bdP.zzcyu()) {
      this.bdP.zzi("Purging writes", new Object[0]);
    }
    zzav(this.bdV.zzcwa());
    zzb(zzajq.zzcvg(), -25);
    this.bcu.purgeOutstandingWrites();
  }
  
  void resume()
  {
    this.bcu.resume("repo_interrupt");
  }
  
  public String toString()
  {
    return this.aZh.toString();
  }
  
  public void zza(final zzajq paramzzajq, zzajh paramzzajh, DatabaseReference.CompletionListener paramCompletionListener, Map<String, Object> paramMap)
  {
    String str1;
    if (this.bdP.zzcyu())
    {
      localObject = this.bdP;
      str1 = String.valueOf(paramzzajq);
      ((zzalw)localObject).zzi(String.valueOf(str1).length() + 8 + "update: " + str1, new Object[0]);
    }
    if (this.bdR.zzcyu())
    {
      localObject = this.bdR;
      str1 = String.valueOf(paramzzajq);
      String str2 = String.valueOf(paramMap);
      ((zzalw)localObject).zzi(String.valueOf(str1).length() + 9 + String.valueOf(str2).length() + "update: " + str1 + " " + str2, new Object[0]);
    }
    if (paramzzajh.isEmpty())
    {
      if (this.bdP.zzcyu()) {
        this.bdP.zzi("update called with no changes. No-op", new Object[0]);
      }
      zza(paramCompletionListener, null, paramzzajq);
      return;
    }
    Object localObject = zzajw.zza(paramzzajh, zzajw.zza(this.bdI));
    final long l = zzcvs();
    zzav(this.bdV.zza(paramzzajq, paramzzajh, (zzajh)localObject, l, true));
    this.bcu.zza(paramzzajq.zzcvi(), paramMap, new zzajb()
    {
      public void zzbn(String paramAnonymousString1, String paramAnonymousString2)
      {
        paramAnonymousString1 = zzajs.zzbp(paramAnonymousString1, paramAnonymousString2);
        zzajs.zza(zzajs.this, "updateChildren", paramzzajq, paramAnonymousString1);
        zzajs.zza(zzajs.this, l, paramzzajq, paramAnonymousString1);
        zzajs.this.zza(this.bec, paramAnonymousString1, paramzzajq);
      }
    });
    zzo(zzb(paramzzajq, -9));
  }
  
  public void zza(final zzajq paramzzajq, zzaml paramzzaml, DatabaseReference.CompletionListener paramCompletionListener)
  {
    String str1;
    if (this.bdP.zzcyu())
    {
      localObject = this.bdP;
      str1 = String.valueOf(paramzzajq);
      ((zzalw)localObject).zzi(String.valueOf(str1).length() + 5 + "set: " + str1, new Object[0]);
    }
    if (this.bdR.zzcyu())
    {
      localObject = this.bdR;
      str1 = String.valueOf(paramzzajq);
      String str2 = String.valueOf(paramzzaml);
      ((zzalw)localObject).zzi(String.valueOf(str1).length() + 6 + String.valueOf(str2).length() + "set: " + str1 + " " + str2, new Object[0]);
    }
    Object localObject = zzajw.zza(paramzzaml, zzajw.zza(this.bdI));
    final long l = zzcvs();
    zzav(this.bdV.zza(paramzzajq, paramzzaml, (zzaml)localObject, l, true, true));
    this.bcu.zza(paramzzajq.zzcvi(), paramzzaml.getValue(true), new zzajb()
    {
      public void zzbn(String paramAnonymousString1, String paramAnonymousString2)
      {
        paramAnonymousString1 = zzajs.zzbp(paramAnonymousString1, paramAnonymousString2);
        zzajs.zza(zzajs.this, "setValue", paramzzajq, paramAnonymousString1);
        zzajs.zza(zzajs.this, l, paramzzajq, paramAnonymousString1);
        zzajs.this.zza(this.bec, paramAnonymousString1, paramzzajq);
      }
    });
    zzo(zzb(paramzzajq, -9));
  }
  
  public void zza(final zzajq paramzzajq, final DatabaseReference.CompletionListener paramCompletionListener)
  {
    this.bcu.zza(paramzzajq.zzcvi(), new zzajb()
    {
      public void zzbn(String paramAnonymousString1, String paramAnonymousString2)
      {
        paramAnonymousString1 = zzajs.zzbp(paramAnonymousString1, paramAnonymousString2);
        if (paramAnonymousString1 == null) {
          zzajs.zzf(zzajs.this).zzr(paramzzajq);
        }
        zzajs.this.zza(paramCompletionListener, paramAnonymousString1, paramzzajq);
      }
    });
  }
  
  public void zza(zzajq paramzzajq, final Transaction.Handler paramHandler, boolean paramBoolean)
  {
    final Object localObject3;
    if (this.bdP.zzcyu())
    {
      localObject1 = this.bdP;
      localObject3 = String.valueOf(paramzzajq);
      ((zzalw)localObject1).zzi(String.valueOf(localObject3).length() + 13 + "transaction: " + (String)localObject3, new Object[0]);
    }
    if (this.bdR.zzcyu())
    {
      localObject1 = this.bdP;
      localObject3 = String.valueOf(paramzzajq);
      ((zzalw)localObject1).zzi(String.valueOf(localObject3).length() + 13 + "transaction: " + (String)localObject3, new Object[0]);
    }
    if ((this.bdO.zzcsk()) && (!this.bdX))
    {
      this.bdX = true;
      this.bdQ.info("runTransaction() usage detected while persistence is enabled. Please be aware that transactions *will not* be persisted across database restarts.  See https://www.firebase.com/docs/android/guide/offline-capabilities.html#section-handling-transactions-offline for more details.");
    }
    Object localObject4 = zza.zza(this, paramzzajq);
    Object localObject1 = new ValueEventListener()
    {
      public void onCancelled(DatabaseError paramAnonymousDatabaseError) {}
      
      public void onDataChange(DataSnapshot paramAnonymousDataSnapshot) {}
    };
    zzf(new zzakg(this, (ValueEventListener)localObject1, ((DatabaseReference)localObject4).zzcrd()));
    zza localzza = new zza(paramzzajq, paramHandler, (ValueEventListener)localObject1, zzb.beJ, paramBoolean, zzcvv(), null);
    localObject1 = zzn(paramzzajq);
    zza.zza(localzza, (zzaml)localObject1);
    localObject1 = zza.zza((zzaml)localObject1);
    try
    {
      localObject1 = paramHandler.doTransaction((MutableData)localObject1);
      if (localObject1 == null) {
        throw new NullPointerException("Transaction returned null as result");
      }
    }
    catch (Throwable localThrowable)
    {
      localObject3 = DatabaseError.fromException(localThrowable);
      Object localObject2 = Transaction.abort();
      while (!((Transaction.Result)localObject2).isSuccess())
      {
        zza.zzb(localzza, null);
        zza.zzc(localzza, null);
        zzq(new Runnable()
        {
          public void run()
          {
            paramHandler.onComplete(localObject3, false, this.bef);
          }
        });
        return;
        localObject3 = null;
      }
      zza.zza(localzza, zzb.beK);
      localObject4 = this.bdL.zzal(paramzzajq);
      localObject3 = (List)((zzalb)localObject4).getValue();
      paramHandler = (Transaction.Handler)localObject3;
      if (localObject3 == null) {
        paramHandler = new ArrayList();
      }
      paramHandler.add(localzza);
      ((zzalb)localObject4).setValue(paramHandler);
      paramHandler = zzajw.zza(this.bdI);
      localObject2 = ((Transaction.Result)localObject2).zzcqy();
      paramHandler = zzajw.zza((zzaml)localObject2, paramHandler);
      zza.zzb(localzza, (zzaml)localObject2);
      zza.zzc(localzza, paramHandler);
      zza.zza(localzza, zzcvs());
      zzav(this.bdV.zza(paramzzajq, (zzaml)localObject2, paramHandler, zza.zzc(localzza), paramBoolean, false));
      zzcvu();
    }
  }
  
  public void zza(final zzajq paramzzajq, final Map<zzajq, zzaml> paramMap, final DatabaseReference.CompletionListener paramCompletionListener, Map<String, Object> paramMap1)
  {
    this.bcu.zzb(paramzzajq.zzcvi(), paramMap1, new zzajb()
    {
      public void zzbn(String paramAnonymousString1, String paramAnonymousString2)
      {
        paramAnonymousString1 = zzajs.zzbp(paramAnonymousString1, paramAnonymousString2);
        zzajs.zza(zzajs.this, "onDisconnect().updateChildren", paramzzajq, paramAnonymousString1);
        if (paramAnonymousString1 == null)
        {
          paramAnonymousString2 = paramMap.entrySet().iterator();
          while (paramAnonymousString2.hasNext())
          {
            Map.Entry localEntry = (Map.Entry)paramAnonymousString2.next();
            zzajs.zzf(zzajs.this).zzh(paramzzajq.zzh((zzajq)localEntry.getKey()), (zzaml)localEntry.getValue());
          }
        }
        zzajs.this.zza(paramCompletionListener, paramAnonymousString1, paramzzajq);
      }
    });
  }
  
  public void zza(zzall paramzzall, boolean paramBoolean)
  {
    assert ((paramzzall.zzcrc().isEmpty()) || (!paramzzall.zzcrc().zzcvj().equals(zzaji.bdg)));
    this.bdV.zza(paramzzall, paramBoolean);
  }
  
  public void zza(zzalz paramzzalz, Object paramObject)
  {
    zzb(paramzzalz, paramObject);
  }
  
  void zza(final DatabaseReference.CompletionListener paramCompletionListener, final DatabaseError paramDatabaseError, final zzajq paramzzajq)
  {
    if (paramCompletionListener != null)
    {
      zzalz localzzalz = paramzzajq.zzcvm();
      if ((localzzalz == null) || (!localzzalz.zzczb())) {
        break label48;
      }
    }
    label48:
    for (paramzzajq = zza.zza(this, paramzzajq.zzcvl());; paramzzajq = zza.zza(this, paramzzajq))
    {
      zzq(new Runnable()
      {
        public void run()
        {
          paramCompletionListener.onComplete(paramDatabaseError, paramzzajq);
        }
      });
      return;
    }
  }
  
  public void zza(List<String> paramList, Object paramObject, boolean paramBoolean, Long paramLong)
  {
    zzajq localzzajq = new zzajq(paramList);
    Object localObject1;
    if (this.bdP.zzcyu())
    {
      paramList = this.bdP;
      localObject1 = String.valueOf(localzzajq);
      paramList.zzi(String.valueOf(localObject1).length() + 14 + "onDataUpdate: " + (String)localObject1, new Object[0]);
    }
    Object localObject2;
    if (this.bdR.zzcyu())
    {
      paramList = this.bdP;
      localObject1 = String.valueOf(localzzajq);
      localObject2 = String.valueOf(paramObject);
      paramList.zzi(String.valueOf(localObject1).length() + 15 + String.valueOf(localObject2).length() + "onDataUpdate: " + (String)localObject1 + " " + (String)localObject2, new Object[0]);
    }
    this.bdS += 1L;
    if (paramLong != null) {
      try
      {
        paramList = new zzakb(paramLong.longValue());
        if (paramBoolean)
        {
          paramLong = new HashMap();
          paramObject = ((Map)paramObject).entrySet().iterator();
          while (((Iterator)paramObject).hasNext())
          {
            localObject1 = (Map.Entry)((Iterator)paramObject).next();
            localObject2 = zzamm.zzbt(((Map.Entry)localObject1).getValue());
            paramLong.put(new zzajq((String)((Map.Entry)localObject1).getKey()), localObject2);
          }
          paramList = this.bdV.zza(localzzajq, paramLong, paramList);
        }
      }
      catch (DatabaseException paramList)
      {
        this.bdP.zzd("FIREBASE INTERNAL ERROR", paramList);
        return;
      }
    }
    for (;;)
    {
      if (paramList.size() > 0) {
        zzo(localzzajq);
      }
      zzav(paramList);
      return;
      paramObject = zzamm.zzbt(paramObject);
      paramList = this.bdV.zza(localzzajq, (zzaml)paramObject, paramList);
      continue;
      if (paramBoolean)
      {
        paramList = new HashMap();
        paramObject = ((Map)paramObject).entrySet().iterator();
        while (((Iterator)paramObject).hasNext())
        {
          paramLong = (Map.Entry)((Iterator)paramObject).next();
          localObject1 = zzamm.zzbt(paramLong.getValue());
          paramList.put(new zzajq((String)paramLong.getKey()), localObject1);
        }
        paramList = this.bdV.zza(localzzajq, paramList);
      }
      else
      {
        paramList = zzamm.zzbt(paramObject);
        paramList = this.bdV.zzi(localzzajq, paramList);
      }
    }
  }
  
  public void zza(List<String> paramList, List<zzaja> paramList1, Long paramLong)
  {
    zzajq localzzajq = new zzajq(paramList);
    String str1;
    if (this.bdP.zzcyu())
    {
      paramList = this.bdP;
      str1 = String.valueOf(localzzajq);
      paramList.zzi(String.valueOf(str1).length() + 20 + "onRangeMergeUpdate: " + str1, new Object[0]);
    }
    if (this.bdR.zzcyu())
    {
      paramList = this.bdP;
      str1 = String.valueOf(localzzajq);
      String str2 = String.valueOf(paramList1);
      paramList.zzi(String.valueOf(str1).length() + 21 + String.valueOf(str2).length() + "onRangeMergeUpdate: " + str1 + " " + str2, new Object[0]);
    }
    this.bdS += 1L;
    paramList = new ArrayList(paramList1.size());
    paramList1 = paramList1.iterator();
    while (paramList1.hasNext()) {
      paramList.add(new zzamq((zzaja)paramList1.next()));
    }
    if (paramLong != null) {}
    for (paramList = this.bdV.zza(localzzajq, paramList, new zzakb(paramLong.longValue()));; paramList = this.bdV.zzb(localzzajq, paramList))
    {
      if (paramList.size() > 0) {
        zzo(localzzajq);
      }
      zzav(paramList);
      return;
    }
  }
  
  public void zzb(final zzajq paramzzajq, final zzaml paramzzaml, final DatabaseReference.CompletionListener paramCompletionListener)
  {
    this.bcu.zzb(paramzzajq.zzcvi(), paramzzaml.getValue(true), new zzajb()
    {
      public void zzbn(String paramAnonymousString1, String paramAnonymousString2)
      {
        paramAnonymousString1 = zzajs.zzbp(paramAnonymousString1, paramAnonymousString2);
        zzajs.zza(zzajs.this, "onDisconnect().setValue", paramzzajq, paramAnonymousString1);
        if (paramAnonymousString1 == null) {
          zzajs.zzf(zzajs.this).zzh(paramzzajq, paramzzaml);
        }
        zzajs.this.zza(paramCompletionListener, paramAnonymousString1, paramzzajq);
      }
    });
  }
  
  public void zzbw(Map<String, Object> paramMap)
  {
    paramMap = paramMap.entrySet().iterator();
    while (paramMap.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)paramMap.next();
      zzb(zzalz.zzsx((String)localEntry.getKey()), localEntry.getValue());
    }
  }
  
  public void zzcsp()
  {
    zza(zzaji.bdj, Boolean.valueOf(true));
  }
  
  public zzajt zzcvp()
  {
    return this.aZh;
  }
  
  public long zzcvq()
  {
    return this.bdI.z();
  }
  
  boolean zzcvr()
  {
    return (!this.bdU.isEmpty()) || (!this.bdV.isEmpty());
  }
  
  public void zzcy(boolean paramBoolean)
  {
    zza(zzaji.bdi, Boolean.valueOf(paramBoolean));
  }
  
  public void zze(zzajl paramzzajl)
  {
    if (zzaji.bdg.equals(paramzzajl.zzcud().zzcrc().zzcvj())) {}
    for (paramzzajl = this.bdU.zzh(paramzzajl);; paramzzajl = this.bdV.zzh(paramzzajl))
    {
      zzav(paramzzajl);
      return;
    }
  }
  
  public void zzf(zzajl paramzzajl)
  {
    zzalz localzzalz = paramzzajl.zzcud().zzcrc().zzcvj();
    if ((localzzalz != null) && (localzzalz.equals(zzaji.bdg))) {}
    for (paramzzajl = this.bdU.zzg(paramzzajl);; paramzzajl = this.bdV.zzg(paramzzajl))
    {
      zzav(paramzzajl);
      return;
    }
  }
  
  public void zzq(Runnable paramRunnable)
  {
    this.bdO.zzcuk();
    this.bdO.zzcur().zzq(paramRunnable);
  }
  
  public void zzs(Runnable paramRunnable)
  {
    this.bdO.zzcuk();
    this.bdO.zzcus().zzs(paramRunnable);
  }
  
  private static class zza
    implements Comparable<zza>
  {
    private zzajq aZr;
    private ValueEventListener beA;
    private zzajs.zzb beB;
    private long beC;
    private boolean beD;
    private DatabaseError beE;
    private long beF;
    private zzaml beG;
    private zzaml beH;
    private zzaml beI;
    private Transaction.Handler bez;
    private int retryCount;
    
    private zza(zzajq paramzzajq, Transaction.Handler paramHandler, ValueEventListener paramValueEventListener, zzajs.zzb paramzzb, boolean paramBoolean, long paramLong)
    {
      this.aZr = paramzzajq;
      this.bez = paramHandler;
      this.beA = paramValueEventListener;
      this.beB = paramzzb;
      this.retryCount = 0;
      this.beD = paramBoolean;
      this.beC = paramLong;
      this.beE = null;
      this.beG = null;
      this.beH = null;
      this.beI = null;
    }
    
    public int zza(zza paramzza)
    {
      if (this.beC < paramzza.beC) {
        return -1;
      }
      if (this.beC == paramzza.beC) {
        return 0;
      }
      return 1;
    }
  }
  
  private static enum zzb
  {
    private zzb() {}
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzajs.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */