package com.google.android.gms.internal;

import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.FirebaseDatabase;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class zzaju
{
  private static final zzaju beR = new zzaju();
  private final Map<zzajj, Map<String, zzajs>> beS = new HashMap();
  
  public static zzajs zza(zzajj paramzzajj, zzajt paramzzajt, FirebaseDatabase paramFirebaseDatabase)
    throws DatabaseException
  {
    return beR.zzb(paramzzajj, paramzzajt, paramFirebaseDatabase);
  }
  
  private zzajs zzb(zzajj paramzzajj, zzajt paramzzajt, FirebaseDatabase paramFirebaseDatabase)
    throws DatabaseException
  {
    paramzzajj.zzctz();
    ??? = paramzzajt.baV;
    String str = paramzzajt.EY;
    str = String.valueOf(???).length() + 9 + String.valueOf(str).length() + "https://" + (String)??? + "/" + str;
    synchronized (this.beS)
    {
      if (!this.beS.containsKey(paramzzajj))
      {
        localObject2 = new HashMap();
        this.beS.put(paramzzajj, localObject2);
      }
      Object localObject2 = (Map)this.beS.get(paramzzajj);
      if (!((Map)localObject2).containsKey(str))
      {
        paramzzajj = new zzajs(paramzzajt, paramzzajj, paramFirebaseDatabase);
        ((Map)localObject2).put(str, paramzzajj);
        return paramzzajj;
      }
      throw new IllegalStateException("createLocalRepo() called for existing repo.");
    }
  }
  
  public static void zzd(zzajj paramzzajj)
  {
    beR.zzf(paramzzajj);
  }
  
  public static void zze(zzajj paramzzajj)
  {
    beR.zzg(paramzzajj);
  }
  
  private void zzf(final zzajj paramzzajj)
  {
    zzajv localzzajv = paramzzajj.zzcus();
    if (localzzajv != null) {
      localzzajv.zzs(new Runnable()
      {
        public void run()
        {
          int i;
          synchronized (zzaju.zza(zzaju.this))
          {
            if (zzaju.zza(zzaju.this).containsKey(paramzzajj))
            {
              Iterator localIterator = ((Map)zzaju.zza(zzaju.this).get(paramzzajj)).values().iterator();
              i = 1;
              if (localIterator.hasNext())
              {
                zzajs localzzajs = (zzajs)localIterator.next();
                localzzajs.interrupt();
                if ((i == 0) || (localzzajs.zzcvr())) {
                  break label125;
                }
                i = 1;
                break label122;
              }
              if (i != 0) {
                paramzzajj.stop();
              }
            }
            return;
          }
          for (;;)
          {
            label122:
            break;
            label125:
            i = 0;
          }
        }
      });
    }
  }
  
  private void zzg(final zzajj paramzzajj)
  {
    zzajv localzzajv = paramzzajj.zzcus();
    if (localzzajv != null) {
      localzzajv.zzs(new Runnable()
      {
        public void run()
        {
          synchronized (zzaju.zza(zzaju.this))
          {
            if (zzaju.zza(zzaju.this).containsKey(paramzzajj))
            {
              Iterator localIterator = ((Map)zzaju.zza(zzaju.this).get(paramzzajj)).values().iterator();
              if (localIterator.hasNext()) {
                ((zzajs)localIterator.next()).resume();
              }
            }
          }
        }
      });
    }
  }
  
  public static void zzk(zzajs paramzzajs)
  {
    paramzzajs.zzs(new Runnable()
    {
      public void run()
      {
        zzaju.this.interrupt();
      }
    });
  }
  
  public static void zzl(zzajs paramzzajs)
  {
    paramzzajs.zzs(new Runnable()
    {
      public void run()
      {
        zzaju.this.resume();
      }
    });
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaju.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */