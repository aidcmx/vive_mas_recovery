package com.google.android.gms.internal;

public abstract interface zzajv
{
  public abstract void restart();
  
  public abstract void shutdown();
  
  public abstract void zzs(Runnable paramRunnable);
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzajv.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */