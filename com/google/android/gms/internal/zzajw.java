package com.google.android.gms.internal;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class zzajw
{
  public static zzajh zza(zzajh paramzzajh, Map<String, Object> paramMap)
  {
    Object localObject = zzajh.zzcue();
    Iterator localIterator = paramzzajh.iterator();
    for (paramzzajh = (zzajh)localObject; localIterator.hasNext(); paramzzajh = paramzzajh.zze((zzajq)((Map.Entry)localObject).getKey(), zza((zzaml)((Map.Entry)localObject).getValue(), paramMap))) {
      localObject = (Map.Entry)localIterator.next();
    }
    return paramzzajh;
  }
  
  public static zzajy zza(zzajy paramzzajy, final Map<String, Object> paramMap)
  {
    zzajy localzzajy = new zzajy();
    paramzzajy.zza(new zzajq(""), new zzajy.zzb()
    {
      public void zzf(zzajq paramAnonymouszzajq, zzaml paramAnonymouszzaml)
      {
        zzajw.this.zzh(paramAnonymouszzajq, zzajw.zza(paramAnonymouszzaml, paramMap));
      }
    });
    return localzzajy;
  }
  
  public static zzaml zza(zzaml paramzzaml, Map<String, Object> paramMap)
  {
    Object localObject2 = paramzzaml.zzczf().getValue();
    final Object localObject1 = localObject2;
    if ((localObject2 instanceof Map))
    {
      Map localMap = (Map)localObject2;
      localObject1 = localObject2;
      if (localMap.containsKey(".sv")) {
        localObject1 = paramMap.get((String)localMap.get(".sv"));
      }
    }
    localObject2 = zzamp.zzbu(localObject1);
    if (paramzzaml.zzcze())
    {
      paramMap = zza(paramzzaml.getValue(), paramMap);
      if (paramMap.equals(paramzzaml.getValue()))
      {
        localObject1 = paramzzaml;
        if (localObject2.equals(paramzzaml.zzczf())) {}
      }
      else
      {
        localObject1 = zzamm.zza(paramMap, (zzaml)localObject2);
      }
    }
    do
    {
      return (zzaml)localObject1;
      localObject1 = paramzzaml;
    } while (paramzzaml.isEmpty());
    paramzzaml = (zzama)paramzzaml;
    localObject1 = new zzajx(paramzzaml);
    paramzzaml.zza(new zzama.zza()
    {
      public void zzb(zzalz paramAnonymouszzalz, zzaml paramAnonymouszzaml)
      {
        zzaml localzzaml = zzajw.zza(paramAnonymouszzaml, zzajw.this);
        if (localzzaml != paramAnonymouszzaml) {
          localObject1.zzg(new zzajq(paramAnonymouszzalz.asString()), localzzaml);
        }
      }
    });
    if (!((zzajx)localObject1).zzcvw().zzczf().equals(localObject2)) {
      return ((zzajx)localObject1).zzcvw().zzg((zzaml)localObject2);
    }
    return ((zzajx)localObject1).zzcvw();
  }
  
  public static Object zza(Object paramObject, Map<String, Object> paramMap)
  {
    Object localObject1 = paramObject;
    if ((paramObject instanceof Map))
    {
      Object localObject2 = (Map)paramObject;
      localObject1 = paramObject;
      if (((Map)localObject2).containsKey(".sv"))
      {
        localObject2 = (String)((Map)localObject2).get(".sv");
        localObject1 = paramObject;
        if (paramMap.containsKey(localObject2)) {
          localObject1 = paramMap.get(localObject2);
        }
      }
    }
    return localObject1;
  }
  
  public static Map<String, Object> zza(zzanf paramzzanf)
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put("timestamp", Long.valueOf(paramzzanf.z()));
    return localHashMap;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzajw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */