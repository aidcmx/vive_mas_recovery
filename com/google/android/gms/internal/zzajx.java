package com.google.android.gms.internal;

public class zzajx
{
  private zzaml beY;
  
  zzajx()
  {
    this.beY = zzame.zzczq();
  }
  
  public zzajx(zzaml paramzzaml)
  {
    this.beY = paramzzaml;
  }
  
  public zzaml zzcvw()
  {
    return this.beY;
  }
  
  public void zzg(zzajq paramzzajq, zzaml paramzzaml)
  {
    this.beY = this.beY.zzl(paramzzajq, paramzzaml);
  }
  
  public zzaml zzq(zzajq paramzzajq)
  {
    return this.beY.zzao(paramzzajq);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzajx.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */