package com.google.android.gms.internal;

import com.google.firebase.database.DatabaseError;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class zzajz
{
  private final Map<zzalk, zzalm> bfe = new HashMap();
  private final zzaku bff;
  
  static
  {
    if (!zzajz.class.desiredAssertionStatus()) {}
    for (boolean bool = true;; bool = false)
    {
      $assertionsDisabled = bool;
      return;
    }
  }
  
  public zzajz(zzaku paramzzaku)
  {
    this.bff = paramzzaku;
  }
  
  private List<zzalg> zza(zzalm paramzzalm, zzakn paramzzakn, zzaki paramzzaki, zzaml paramzzaml)
  {
    paramzzakn = paramzzalm.zzb(paramzzakn, paramzzaki, paramzzaml);
    if (!paramzzalm.zzcyi().zzcye())
    {
      paramzzaki = new HashSet();
      paramzzaml = new HashSet();
      Iterator localIterator = paramzzakn.bhX.iterator();
      while (localIterator.hasNext())
      {
        zzalf localzzalf = (zzalf)localIterator.next();
        zzalh.zza localzza = localzzalf.zzcxm();
        if (localzza == zzalh.zza.bhw) {
          paramzzaml.add(localzzalf.zzcxl());
        } else if (localzza == zzalh.zza.bhv) {
          paramzzaki.add(localzzalf.zzcxl());
        }
      }
      if ((!paramzzaml.isEmpty()) || (!paramzzaki.isEmpty())) {
        this.bff.zza(paramzzalm.zzcyi(), paramzzaml, paramzzaki);
      }
    }
    return paramzzakn.bhW;
  }
  
  public boolean isEmpty()
  {
    return this.bfe.isEmpty();
  }
  
  public zzank<List<zzall>, List<zzalh>> zza(zzall paramzzall, zzajl paramzzajl, DatabaseError paramDatabaseError)
  {
    ArrayList localArrayList1 = new ArrayList();
    ArrayList localArrayList2 = new ArrayList();
    boolean bool = zzcvy();
    if (paramzzall.isDefault())
    {
      localObject = this.bfe.entrySet().iterator();
      while (((Iterator)localObject).hasNext())
      {
        zzalm localzzalm = (zzalm)((Map.Entry)((Iterator)localObject).next()).getValue();
        localArrayList2.addAll(localzzalm.zza(paramzzajl, paramDatabaseError));
        if (localzzalm.isEmpty())
        {
          ((Iterator)localObject).remove();
          if (!localzzalm.zzcyi().zzcye()) {
            localArrayList1.add(localzzalm.zzcyi());
          }
        }
      }
    }
    Object localObject = (zzalm)this.bfe.get(paramzzall.zzcyh());
    if (localObject != null)
    {
      localArrayList2.addAll(((zzalm)localObject).zza(paramzzajl, paramDatabaseError));
      if (((zzalm)localObject).isEmpty())
      {
        this.bfe.remove(paramzzall.zzcyh());
        if (!((zzalm)localObject).zzcyi().zzcye()) {
          localArrayList1.add(((zzalm)localObject).zzcyi());
        }
      }
    }
    if ((bool) && (!zzcvy())) {
      localArrayList1.add(zzall.zzan(paramzzall.zzcrc()));
    }
    return new zzank(localArrayList1, localArrayList2);
  }
  
  public List<zzalg> zza(zzajl paramzzajl, zzaki paramzzaki, zzald paramzzald)
  {
    zzall localzzall = paramzzajl.zzcud();
    zzalm localzzalm = (zzalm)this.bfe.get(localzzall.zzcyh());
    Object localObject = localzzalm;
    if (localzzalm == null)
    {
      boolean bool;
      if (paramzzald.zzcxh())
      {
        localObject = paramzzald.zzcqy();
        localObject = paramzzaki.zzc((zzaml)localObject);
        if (localObject == null) {
          break label168;
        }
        bool = true;
        paramzzaki = (zzaki)localObject;
      }
      for (;;)
      {
        localObject = new zzalm(localzzall, new zzaln(new zzald(zzamg.zza(paramzzaki, localzzall.zzcya()), bool, false), paramzzald));
        if (localzzall.zzcye()) {
          break label195;
        }
        paramzzaki = new HashSet();
        paramzzald = ((zzalm)localObject).zzcyk().iterator();
        while (paramzzald.hasNext()) {
          paramzzaki.add(((zzamk)paramzzald.next()).a());
        }
        localObject = null;
        break;
        label168:
        paramzzaki = paramzzaki.zzd(paramzzald.zzcqy());
        bool = false;
      }
      this.bff.zza(localzzall, paramzzaki);
      label195:
      this.bfe.put(localzzall.zzcyh(), localObject);
    }
    ((zzalm)localObject).zzb(paramzzajl);
    return ((zzalm)localObject).zzl(paramzzajl);
  }
  
  public List<zzalg> zza(zzakn paramzzakn, zzaki paramzzaki, zzaml paramzzaml)
  {
    Object localObject = paramzzakn.zzcwp().zzcwu();
    if (localObject != null)
    {
      localObject = (zzalm)this.bfe.get(localObject);
      assert (localObject != null);
      return zza((zzalm)localObject, paramzzakn, paramzzaki, paramzzaml);
    }
    localObject = new ArrayList();
    Iterator localIterator = this.bfe.entrySet().iterator();
    while (localIterator.hasNext()) {
      ((List)localObject).addAll(zza((zzalm)((Map.Entry)localIterator.next()).getValue(), paramzzakn, paramzzaki, paramzzaml));
    }
    return (List<zzalg>)localObject;
  }
  
  public zzalm zzb(zzall paramzzall)
  {
    if (paramzzall.zzcye()) {
      return zzcvz();
    }
    return (zzalm)this.bfe.get(paramzzall.zzcyh());
  }
  
  public boolean zzc(zzall paramzzall)
  {
    return zzb(paramzzall) != null;
  }
  
  public List<zzalm> zzcvx()
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = this.bfe.entrySet().iterator();
    while (localIterator.hasNext())
    {
      zzalm localzzalm = (zzalm)((Map.Entry)localIterator.next()).getValue();
      if (!localzzalm.zzcyi().zzcye()) {
        localArrayList.add(localzzalm);
      }
    }
    return localArrayList;
  }
  
  public boolean zzcvy()
  {
    return zzcvz() != null;
  }
  
  public zzalm zzcvz()
  {
    Iterator localIterator = this.bfe.entrySet().iterator();
    while (localIterator.hasNext())
    {
      zzalm localzzalm = (zzalm)((Map.Entry)localIterator.next()).getValue();
      if (localzzalm.zzcyi().zzcye()) {
        return localzzalm;
      }
    }
    return null;
  }
  
  public zzaml zzs(zzajq paramzzajq)
  {
    Iterator localIterator = this.bfe.values().iterator();
    while (localIterator.hasNext())
    {
      zzalm localzzalm = (zzalm)localIterator.next();
      if (localzzalm.zzs(paramzzajq) != null) {
        return localzzalm.zzs(paramzzajq);
      }
    }
    return null;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzajz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */