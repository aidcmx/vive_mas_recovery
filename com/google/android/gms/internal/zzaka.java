package com.google.android.gms.internal;

import com.google.firebase.database.DatabaseError;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Callable;

public class zzaka
{
  private final zzalw aZR;
  private final zzaku bff;
  private zzakz<zzajz> bfg = zzakz.zzcxe();
  private final zzakh bfh = new zzakh();
  private final Map<zzakb, zzall> bfi = new HashMap();
  private final Map<zzall, zzakb> bfj = new HashMap();
  private final Set<zzall> bfk = new HashSet();
  private final zzd bfl;
  private long bfm = 1L;
  
  static
  {
    if (!zzaka.class.desiredAssertionStatus()) {}
    for (boolean bool = true;; bool = false)
    {
      $assertionsDisabled = bool;
      return;
    }
  }
  
  public zzaka(zzajj paramzzajj, zzaku paramzzaku, zzd paramzzd)
  {
    this.bfl = paramzzd;
    this.bff = paramzzaku;
    this.aZR = paramzzajj.zzss("SyncTree");
  }
  
  private List<zzalh> zza(zzakn paramzzakn)
  {
    return zza(paramzzakn, this.bfg, null, this.bfh.zzu(zzajq.zzcvg()));
  }
  
  private List<zzalh> zza(zzakn paramzzakn, zzakz<zzajz> paramzzakz, zzaml paramzzaml, zzaki paramzzaki)
  {
    if (paramzzakn.zzcrc().isEmpty()) {
      return zzb(paramzzakn, paramzzakz, paramzzaml, paramzzaki);
    }
    zzajz localzzajz = (zzajz)paramzzakz.getValue();
    zzaml localzzaml = paramzzaml;
    if (paramzzaml == null)
    {
      localzzaml = paramzzaml;
      if (localzzajz != null) {
        localzzaml = localzzajz.zzs(zzajq.zzcvg());
      }
    }
    paramzzaml = new ArrayList();
    zzalz localzzalz = paramzzakn.zzcrc().zzcvj();
    zzakn localzzakn = paramzzakn.zzc(localzzalz);
    zzakz localzzakz = (zzakz)paramzzakz.zzcxf().get(localzzalz);
    if ((localzzakz != null) && (localzzakn != null)) {
      if (localzzaml == null) {
        break label165;
      }
    }
    label165:
    for (paramzzakz = localzzaml.zzm(localzzalz);; paramzzakz = null)
    {
      paramzzaml.addAll(zza(localzzakn, localzzakz, paramzzakz, paramzzaki.zzb(localzzalz)));
      if (localzzajz != null) {
        paramzzaml.addAll(localzzajz.zza(paramzzakn, paramzzaki, localzzaml));
      }
      return paramzzaml;
    }
  }
  
  private List<zzalm> zza(zzakz<zzajz> paramzzakz)
  {
    ArrayList localArrayList = new ArrayList();
    zza(paramzzakz, localArrayList);
    return localArrayList;
  }
  
  private List<? extends zzalh> zza(zzall paramzzall, zzakn paramzzakn)
  {
    paramzzall = paramzzall.zzcrc();
    zzajz localzzajz = (zzajz)this.bfg.zzak(paramzzall);
    assert (localzzajz != null) : "Missing sync point for query tag that we're tracking";
    return localzzajz.zza(paramzzakn, this.bfh.zzu(paramzzall), null);
  }
  
  private void zza(zzakz<zzajz> paramzzakz, List<zzalm> paramList)
  {
    zzajz localzzajz = (zzajz)paramzzakz.getValue();
    if ((localzzajz != null) && (localzzajz.zzcvy())) {
      paramList.add(localzzajz.zzcvz());
    }
    for (;;)
    {
      return;
      if (localzzajz != null) {
        paramList.addAll(localzzajz.zzcvx());
      }
      paramzzakz = paramzzakz.zzcxf().iterator();
      while (paramzzakz.hasNext()) {
        zza((zzakz)((Map.Entry)paramzzakz.next()).getValue(), paramList);
      }
    }
  }
  
  private void zza(zzall paramzzall, zzalm paramzzalm)
  {
    zzajq localzzajq = paramzzall.zzcrc();
    zzakb localzzakb = zze(paramzzall);
    paramzzalm = new zzc(paramzzalm);
    this.bfl.zza(zzd(paramzzall), localzzakb, paramzzalm, paramzzalm);
    paramzzall = this.bfg.zzai(localzzajq);
    if (localzzakb != null)
    {
      if ((!$assertionsDisabled) && (((zzajz)paramzzall.getValue()).zzcvy())) {
        throw new AssertionError("If we're adding a query, it shouldn't be shadowed");
      }
    }
    else {
      paramzzall.zza(new zzakz.zza()
      {
        public Void zza(zzajq paramAnonymouszzajq, zzajz paramAnonymouszzajz, Void paramAnonymousVoid)
        {
          if ((!paramAnonymouszzajq.isEmpty()) && (paramAnonymouszzajz.zzcvy()))
          {
            paramAnonymouszzajq = paramAnonymouszzajz.zzcvz().zzcyi();
            zzaka.zzh(zzaka.this).zza(zzaka.zzb(zzaka.this, paramAnonymouszzajq), zzaka.zza(zzaka.this, paramAnonymouszzajq));
          }
          for (;;)
          {
            return null;
            paramAnonymouszzajq = paramAnonymouszzajz.zzcvx().iterator();
            while (paramAnonymouszzajq.hasNext())
            {
              paramAnonymouszzajz = ((zzalm)paramAnonymouszzajq.next()).zzcyi();
              zzaka.zzh(zzaka.this).zza(zzaka.zzb(zzaka.this, paramAnonymouszzajz), zzaka.zza(zzaka.this, paramAnonymouszzajz));
            }
          }
        }
      });
    }
  }
  
  private void zzaw(List<zzall> paramList)
  {
    paramList = paramList.iterator();
    while (paramList.hasNext())
    {
      zzall localzzall = (zzall)paramList.next();
      if (!localzzall.zzcye())
      {
        zzakb localzzakb = zze(localzzall);
        assert (localzzakb != null);
        this.bfj.remove(localzzall);
        this.bfi.remove(localzzakb);
      }
    }
  }
  
  private zzall zzb(zzakb paramzzakb)
  {
    return (zzall)this.bfi.get(paramzzakb);
  }
  
  private List<zzalh> zzb(final zzakn paramzzakn, zzakz<zzajz> paramzzakz, final zzaml paramzzaml, final zzaki paramzzaki)
  {
    zzajz localzzajz = (zzajz)paramzzakz.getValue();
    if ((paramzzaml == null) && (localzzajz != null)) {
      paramzzaml = localzzajz.zzs(zzajq.zzcvg());
    }
    for (;;)
    {
      final ArrayList localArrayList = new ArrayList();
      paramzzakz.zzcxf().zza(new zzaim.zzb()
      {
        public void zza(zzalz paramAnonymouszzalz, zzakz<zzajz> paramAnonymouszzakz)
        {
          zzaml localzzaml = null;
          if (paramzzaml != null) {
            localzzaml = paramzzaml.zzm(paramAnonymouszzalz);
          }
          zzaki localzzaki = paramzzaki.zzb(paramAnonymouszzalz);
          paramAnonymouszzalz = paramzzakn.zzc(paramAnonymouszzalz);
          if (paramAnonymouszzalz != null) {
            localArrayList.addAll(zzaka.zza(zzaka.this, paramAnonymouszzalz, paramAnonymouszzakz, localzzaml, localzzaki));
          }
        }
      });
      if (localzzajz != null) {
        localArrayList.addAll(localzzajz.zza(paramzzakn, paramzzaki, paramzzaml));
      }
      return localArrayList;
    }
  }
  
  private List<zzalh> zzb(final zzall paramzzall, final zzajl paramzzajl, final DatabaseError paramDatabaseError)
  {
    (List)this.bff.zzf(new Callable()
    {
      static
      {
        if (!zzaka.class.desiredAssertionStatus()) {}
        for (boolean bool = true;; bool = false)
        {
          $assertionsDisabled = bool;
          return;
        }
      }
      
      public List<zzalh> zzbym()
      {
        Object localObject4 = paramzzall.zzcrc();
        Object localObject3 = (zzajz)zzaka.zzd(zzaka.this).zzak((zzajq)localObject4);
        Object localObject2 = new ArrayList();
        Object localObject1 = localObject2;
        int k;
        Object localObject5;
        int i;
        if (localObject3 != null) {
          if (!paramzzall.isDefault())
          {
            localObject1 = localObject2;
            if (!((zzajz)localObject3).zzc(paramzzall)) {}
          }
          else
          {
            localObject1 = ((zzajz)localObject3).zza(paramzzall, paramzzajl, paramDatabaseError);
            if (((zzajz)localObject3).isEmpty()) {
              zzaka.zza(zzaka.this, zzaka.zzd(zzaka.this).zzaj((zzajq)localObject4));
            }
            localObject3 = (List)((zzank)localObject1).getFirst();
            localObject2 = (List)((zzank)localObject1).A();
            localObject1 = ((List)localObject3).iterator();
            k = 0;
            if (((Iterator)localObject1).hasNext())
            {
              localObject5 = (zzall)((Iterator)localObject1).next();
              zzaka.zzb(zzaka.this).zzh(paramzzall);
              if ((k != 0) || (((zzall)localObject5).zzcye())) {}
              for (i = 1;; i = 0)
              {
                k = i;
                break;
              }
            }
            localObject1 = zzaka.zzd(zzaka.this);
            if ((((zzakz)localObject1).getValue() != null) && (((zzajz)((zzakz)localObject1).getValue()).zzcvy()))
            {
              i = 1;
              localObject5 = ((zzajq)localObject4).iterator();
              label250:
              j = i;
              if (((Iterator)localObject5).hasNext())
              {
                localObject1 = ((zzakz)localObject1).zze((zzalz)((Iterator)localObject5).next());
                if ((i == 0) && ((((zzakz)localObject1).getValue() == null) || (!((zzajz)((zzakz)localObject1).getValue()).zzcvy()))) {
                  break label449;
                }
                i = 1;
                label307:
                if (i != 0) {
                  break label609;
                }
                if (!((zzakz)localObject1).isEmpty()) {
                  break label454;
                }
              }
            }
          }
        }
        label449:
        label454:
        label522:
        label609:
        for (int j = i;; j = i)
        {
          if ((k != 0) && (j == 0))
          {
            localObject1 = zzaka.zzd(zzaka.this).zzai((zzajq)localObject4);
            if (!((zzakz)localObject1).isEmpty())
            {
              localObject1 = zzaka.zzb(zzaka.this, (zzakz)localObject1).iterator();
              for (;;)
              {
                if (((Iterator)localObject1).hasNext())
                {
                  localObject5 = (zzalm)((Iterator)localObject1).next();
                  localObject4 = new zzaka.zzc(zzaka.this, (zzalm)localObject5);
                  localObject5 = ((zzalm)localObject5).zzcyi();
                  zzaka.zzh(zzaka.this).zza(zzaka.zzb(zzaka.this, (zzall)localObject5), zzaka.zzc.zza((zzaka.zzc)localObject4), (zzaix)localObject4, (zzaka.zza)localObject4);
                  continue;
                  i = 0;
                  break;
                  i = 0;
                  break label307;
                  break label250;
                }
              }
            }
          }
          if ((j == 0) && (!((List)localObject3).isEmpty()) && (paramDatabaseError == null))
          {
            if (k == 0) {
              break label522;
            }
            zzaka.zzh(zzaka.this).zza(zzaka.zzb(zzaka.this, paramzzall), null);
          }
          for (;;)
          {
            zzaka.zza(zzaka.this, (List)localObject3);
            localObject1 = localObject2;
            return (List<zzalh>)localObject1;
            localObject1 = ((List)localObject3).iterator();
            while (((Iterator)localObject1).hasNext())
            {
              localObject4 = (zzall)((Iterator)localObject1).next();
              localObject5 = zzaka.zza(zzaka.this, (zzall)localObject4);
              assert (localObject5 != null);
              zzaka.zzh(zzaka.this).zza(zzaka.zzb(zzaka.this, (zzall)localObject4), (zzakb)localObject5);
            }
          }
        }
      }
    });
  }
  
  private zzakb zzcwb()
  {
    long l = this.bfm;
    this.bfm = (1L + l);
    return new zzakb(l);
  }
  
  private zzall zzd(zzall paramzzall)
  {
    zzall localzzall = paramzzall;
    if (paramzzall.zzcye())
    {
      localzzall = paramzzall;
      if (!paramzzall.isDefault()) {
        localzzall = zzall.zzan(paramzzall.zzcrc());
      }
    }
    return localzzall;
  }
  
  private zzakb zze(zzall paramzzall)
  {
    return (zzakb)this.bfj.get(paramzzall);
  }
  
  public boolean isEmpty()
  {
    return this.bfg.isEmpty();
  }
  
  public List<? extends zzalh> zza(final long paramLong, boolean paramBoolean1, final boolean paramBoolean2, final zzanf paramzzanf)
  {
    (List)this.bff.zzf(new Callable()
    {
      public List<? extends zzalh> zzbym()
      {
        if (paramBoolean2) {
          zzaka.zzb(zzaka.this).zzby(paramLong);
        }
        zzake localzzake = zzaka.zzc(zzaka.this).zzcj(paramLong);
        boolean bool = zzaka.zzc(zzaka.this).zzck(paramLong);
        if ((localzzake.isVisible()) && (!paramzzanf))
        {
          localObject1 = zzajw.zza(this.bfC);
          if (!localzzake.zzcwg()) {
            break label121;
          }
          localObject1 = zzajw.zza(localzzake.zzcwe(), (Map)localObject1);
          zzaka.zzb(zzaka.this).zzk(localzzake.zzcrc(), (zzaml)localObject1);
        }
        while (!bool)
        {
          return Collections.emptyList();
          label121:
          localObject1 = zzajw.zza(localzzake.zzcwf(), (Map)localObject1);
          zzaka.zzb(zzaka.this).zzc(localzzake.zzcrc(), (zzajh)localObject1);
        }
        Object localObject1 = zzakz.zzcxe();
        Object localObject2;
        if (localzzake.zzcwg())
        {
          localObject2 = ((zzakz)localObject1).zzb(zzajq.zzcvg(), Boolean.valueOf(true));
          return zzaka.zza(zzaka.this, new zzakk(localzzake.zzcrc(), (zzakz)localObject2, paramzzanf));
        }
        Iterator localIterator = localzzake.zzcwf().iterator();
        for (;;)
        {
          localObject2 = localObject1;
          if (!localIterator.hasNext()) {
            break;
          }
          localObject1 = ((zzakz)localObject1).zzb((zzajq)((Map.Entry)localIterator.next()).getKey(), Boolean.valueOf(true));
        }
      }
    });
  }
  
  public List<? extends zzalh> zza(final zzajq paramzzajq, final zzajh paramzzajh1, zzajh paramzzajh2, final long paramLong, final boolean paramBoolean)
  {
    (List)this.bff.zzf(new Callable()
    {
      public List<? extends zzalh> zzbym()
        throws Exception
      {
        if (paramBoolean) {
          zzaka.zzb(zzaka.this).zza(paramzzajq, paramzzajh1, paramLong);
        }
        zzaka.zzc(zzaka.this).zza(paramzzajq, this.bfA, Long.valueOf(paramLong));
        return zzaka.zza(zzaka.this, new zzakm(zzako.bgp, paramzzajq, this.bfA));
      }
    });
  }
  
  public List<? extends zzalh> zza(final zzajq paramzzajq, final zzaml paramzzaml, final zzakb paramzzakb)
  {
    (List)this.bff.zzf(new Callable()
    {
      public List<? extends zzalh> zzbym()
      {
        zzall localzzall = zzaka.zza(zzaka.this, paramzzakb);
        if (localzzall != null)
        {
          zzajq localzzajq = zzajq.zza(localzzall.zzcrc(), paramzzajq);
          if (localzzajq.isEmpty()) {}
          for (Object localObject = localzzall;; localObject = zzall.zzan(paramzzajq))
          {
            zzaka.zzb(zzaka.this).zza((zzall)localObject, paramzzaml);
            localObject = new zzakp(zzako.zzc(localzzall.zzcyh()), localzzajq, paramzzaml);
            return zzaka.zza(zzaka.this, localzzall, (zzakn)localObject);
          }
        }
        return Collections.emptyList();
      }
    });
  }
  
  public List<? extends zzalh> zza(final zzajq paramzzajq, final zzaml paramzzaml1, zzaml paramzzaml2, final long paramLong, final boolean paramBoolean1, final boolean paramBoolean2)
  {
    if ((paramBoolean1) || (!paramBoolean2)) {}
    for (boolean bool = true;; bool = false)
    {
      zzann.zzb(bool, "We shouldn't be persisting non-visible writes.");
      (List)this.bff.zzf(new Callable()
      {
        public List<? extends zzalh> zzbym()
        {
          if (paramBoolean2) {
            zzaka.zzb(zzaka.this).zza(paramzzajq, paramzzaml1, paramLong);
          }
          zzaka.zzc(zzaka.this).zza(paramzzajq, paramBoolean1, Long.valueOf(paramLong), this.bfq);
          if (!this.bfq) {
            return Collections.emptyList();
          }
          return zzaka.zza(zzaka.this, new zzakp(zzako.bgp, paramzzajq, paramBoolean1));
        }
      });
    }
  }
  
  public List<? extends zzalh> zza(zzajq paramzzajq, List<zzamq> paramList, zzakb paramzzakb)
  {
    Object localObject1 = zzb(paramzzakb);
    if (localObject1 != null)
    {
      assert (paramzzajq.equals(((zzall)localObject1).zzcrc()));
      Object localObject2 = (zzajz)this.bfg.zzak(((zzall)localObject1).zzcrc());
      assert (localObject2 != null) : "Missing sync point for query tag that we're tracking";
      localObject1 = ((zzajz)localObject2).zzb((zzall)localObject1);
      assert (localObject1 != null) : "Missing view for query tag that we're tracking";
      localObject1 = ((zzalm)localObject1).zzcyj();
      localObject2 = paramList.iterator();
      for (paramList = (List<zzamq>)localObject1; ((Iterator)localObject2).hasNext(); paramList = ((zzamq)((Iterator)localObject2).next()).zzr(paramList)) {}
      return zza(paramzzajq, paramList, paramzzakb);
    }
    return Collections.emptyList();
  }
  
  public List<? extends zzalh> zza(final zzajq paramzzajq, final Map<zzajq, zzaml> paramMap)
  {
    (List)this.bff.zzf(new Callable()
    {
      public List<? extends zzalh> zzbym()
      {
        zzajh localzzajh = zzajh.zzcb(paramMap);
        zzaka.zzb(zzaka.this).zzd(paramzzajq, localzzajh);
        return zzaka.zza(zzaka.this, new zzakm(zzako.bgq, paramzzajq, localzzajh));
      }
    });
  }
  
  public List<? extends zzalh> zza(final zzajq paramzzajq, final Map<zzajq, zzaml> paramMap, final zzakb paramzzakb)
  {
    (List)this.bff.zzf(new Callable()
    {
      public List<? extends zzalh> zzbym()
      {
        zzall localzzall = zzaka.zza(zzaka.this, paramzzakb);
        if (localzzall != null)
        {
          Object localObject = zzajq.zza(localzzall.zzcrc(), paramzzajq);
          zzajh localzzajh = zzajh.zzcb(paramMap);
          zzaka.zzb(zzaka.this).zzd(paramzzajq, localzzajh);
          localObject = new zzakm(zzako.zzc(localzzall.zzcyh()), (zzajq)localObject, localzzajh);
          return zzaka.zza(zzaka.this, localzzall, (zzakn)localObject);
        }
        return Collections.emptyList();
      }
    });
  }
  
  public List<? extends zzalh> zza(final zzakb paramzzakb)
  {
    (List)this.bff.zzf(new Callable()
    {
      public List<? extends zzalh> zzbym()
      {
        zzall localzzall = zzaka.zza(zzaka.this, paramzzakb);
        if (localzzall != null)
        {
          zzaka.zzb(zzaka.this).zzi(localzzall);
          zzakl localzzakl = new zzakl(zzako.zzc(localzzall.zzcyh()), zzajq.zzcvg());
          return zzaka.zza(zzaka.this, localzzall, localzzakl);
        }
        return Collections.emptyList();
      }
    });
  }
  
  public List<zzalh> zza(zzall paramzzall, DatabaseError paramDatabaseError)
  {
    return zzb(paramzzall, null, paramDatabaseError);
  }
  
  public void zza(zzall paramzzall, boolean paramBoolean)
  {
    if ((paramBoolean) && (!this.bfk.contains(paramzzall)))
    {
      zzg(new zzb(paramzzall));
      this.bfk.add(paramzzall);
    }
    while ((paramBoolean) || (!this.bfk.contains(paramzzall))) {
      return;
    }
    zzh(new zzb(paramzzall));
    this.bfk.remove(paramzzall);
  }
  
  public List<? extends zzalh> zzb(zzajq paramzzajq, List<zzamq> paramList)
  {
    Object localObject = (zzajz)this.bfg.zzak(paramzzajq);
    if (localObject == null) {
      return Collections.emptyList();
    }
    localObject = ((zzajz)localObject).zzcvz();
    if (localObject != null)
    {
      localObject = ((zzalm)localObject).zzcyj();
      Iterator localIterator = paramList.iterator();
      for (paramList = (List<zzamq>)localObject; localIterator.hasNext(); paramList = ((zzamq)localIterator.next()).zzr(paramList)) {}
      return zzi(paramzzajq, paramList);
    }
    return Collections.emptyList();
  }
  
  public zzaml zzc(zzajq paramzzajq, List<Long> paramList)
  {
    zzakz localzzakz = this.bfg;
    Object localObject1 = (zzajz)localzzakz.getValue();
    localObject1 = null;
    zzajq localzzajq1 = zzajq.zzcvg();
    zzajq localzzajq2 = paramzzajq;
    label125:
    label128:
    for (;;)
    {
      Object localObject2 = localzzajq2.zzcvj();
      localzzajq2 = localzzajq2.zzcvk();
      localzzajq1 = localzzajq1.zza((zzalz)localObject2);
      zzajq localzzajq3 = zzajq.zza(localzzajq1, paramzzajq);
      if (localObject2 != null)
      {
        localzzakz = localzzakz.zze((zzalz)localObject2);
        localObject2 = (zzajz)localzzakz.getValue();
        if (localObject2 == null) {
          break label125;
        }
        localObject1 = ((zzajz)localObject2).zzs(localzzajq3);
      }
      for (;;)
      {
        if ((!localzzajq2.isEmpty()) && (localObject1 == null)) {
          break label128;
        }
        return this.bfh.zza(paramzzajq, (zzaml)localObject1, paramList, true);
        localzzakz = zzakz.zzcxe();
        break;
      }
    }
  }
  
  public List<? extends zzalh> zzcwa()
  {
    (List)this.bff.zzf(new Callable()
    {
      public List<? extends zzalh> zzbym()
        throws Exception
      {
        zzaka.zzb(zzaka.this).zzcrh();
        if (zzaka.zzc(zzaka.this).zzcwj().isEmpty()) {
          return Collections.emptyList();
        }
        zzakz localzzakz = new zzakz(Boolean.valueOf(true));
        return zzaka.zza(zzaka.this, new zzakk(zzajq.zzcvg(), localzzakz, true));
      }
    });
  }
  
  public List<? extends zzalh> zzg(final zzajl paramzzajl)
  {
    (List)this.bff.zzf(new Callable()
    {
      static
      {
        if (!zzaka.class.desiredAssertionStatus()) {}
        for (boolean bool = true;; bool = false)
        {
          $assertionsDisabled = bool;
          return;
        }
      }
      
      public List<? extends zzalh> zzbym()
      {
        zzall localzzall = paramzzajl.zzcud();
        zzajq localzzajq = localzzall.zzcrc();
        Object localObject1 = null;
        Object localObject3 = zzaka.zzd(zzaka.this);
        Object localObject2 = localzzajq;
        int i = 0;
        Object localObject4;
        if (!((zzakz)localObject3).isEmpty())
        {
          localObject4 = (zzajz)((zzakz)localObject3).getValue();
          if (localObject4 == null) {
            break label651;
          }
          if (localObject1 != null)
          {
            label60:
            if ((i == 0) && (!((zzajz)localObject4).zzcvy())) {
              break label119;
            }
            i = 1;
          }
        }
        label74:
        label119:
        label298:
        label310:
        label322:
        label352:
        label376:
        label548:
        label591:
        label648:
        label651:
        for (;;)
        {
          if (((zzajq)localObject2).isEmpty()) {}
          for (localObject4 = zzalz.zzsx("");; localObject4 = ((zzajq)localObject2).zzcvj())
          {
            localObject3 = ((zzakz)localObject3).zze((zzalz)localObject4);
            localObject2 = ((zzajq)localObject2).zzcvk();
            break;
            localObject1 = ((zzajz)localObject4).zzs((zzajq)localObject2);
            break label60;
            i = 0;
            break label74;
          }
          localObject2 = (zzajz)zzaka.zzd(zzaka.this).zzak(localzzajq);
          if (localObject2 == null)
          {
            localObject2 = new zzajz(zzaka.zzb(zzaka.this));
            zzaka.zza(zzaka.this, zzaka.zzd(zzaka.this).zzb(localzzajq, localObject2));
            zzaka.zzb(zzaka.this).zzg(localzzall);
            if (localObject1 == null) {
              break label322;
            }
          }
          boolean bool;
          for (localObject1 = new zzald(zzamg.zza((zzaml)localObject1, localzzall.zzcya()), true, false);; localObject1 = localObject3)
          {
            bool = ((zzajz)localObject2).zzc(localzzall);
            if ((bool) || (localzzall.zzcye())) {
              break label591;
            }
            if (($assertionsDisabled) || (!zzaka.zze(zzaka.this).containsKey(localzzall))) {
              break label548;
            }
            throw new AssertionError("View does not exist but we have a tag");
            if ((i != 0) || (((zzajz)localObject2).zzcvy()))
            {
              i = 1;
              if (localObject1 == null) {
                break label310;
              }
            }
            for (;;)
            {
              break;
              i = 0;
              break label298;
              localObject1 = ((zzajz)localObject2).zzs(zzajq.zzcvg());
            }
            localObject3 = zzaka.zzb(zzaka.this).zzf(localzzall);
            if (!((zzald)localObject3).zzcxh()) {
              break label352;
            }
          }
          localObject1 = zzame.zzczq();
          localObject4 = zzaka.zzd(zzaka.this).zzai(localzzajq).zzcxf().iterator();
          if (((Iterator)localObject4).hasNext())
          {
            Map.Entry localEntry = (Map.Entry)((Iterator)localObject4).next();
            Object localObject5 = (zzajz)((zzakz)localEntry.getValue()).getValue();
            if (localObject5 == null) {
              break label648;
            }
            localObject5 = ((zzajz)localObject5).zzs(zzajq.zzcvg());
            if (localObject5 == null) {
              break label648;
            }
            localObject1 = ((zzaml)localObject1).zze((zzalz)localEntry.getKey(), (zzaml)localObject5);
          }
          for (;;)
          {
            break label376;
            localObject3 = ((zzald)localObject3).zzcqy().iterator();
            while (((Iterator)localObject3).hasNext())
            {
              localObject4 = (zzamk)((Iterator)localObject3).next();
              if (!((zzaml)localObject1).zzk(((zzamk)localObject4).a())) {
                localObject1 = ((zzaml)localObject1).zze(((zzamk)localObject4).a(), ((zzamk)localObject4).zzcqy());
              }
            }
            localObject1 = new zzald(zzamg.zza((zzaml)localObject1, localzzall.zzcya()), false, false);
            break;
            localObject3 = zzaka.zzf(zzaka.this);
            zzaka.zze(zzaka.this).put(localzzall, localObject3);
            zzaka.zzg(zzaka.this).put(localObject3, localzzall);
            localObject3 = zzaka.zzc(zzaka.this).zzu(localzzajq);
            localObject1 = ((zzajz)localObject2).zza(paramzzajl, (zzaki)localObject3, (zzald)localObject1);
            if ((!bool) && (i == 0))
            {
              localObject2 = ((zzajz)localObject2).zzb(localzzall);
              zzaka.zza(zzaka.this, localzzall, (zzalm)localObject2);
            }
            return (List<? extends zzalh>)localObject1;
          }
        }
      }
    });
  }
  
  public List<zzalh> zzh(zzajl paramzzajl)
  {
    return zzb(paramzzajl.zzcud(), paramzzajl, null);
  }
  
  public List<? extends zzalh> zzi(final zzajq paramzzajq, final zzaml paramzzaml)
  {
    (List)this.bff.zzf(new Callable()
    {
      public List<? extends zzalh> zzbym()
      {
        zzaka.zzb(zzaka.this).zza(zzall.zzan(paramzzajq), paramzzaml);
        return zzaka.zza(zzaka.this, new zzakp(zzako.bgq, paramzzajq, paramzzaml));
      }
    });
  }
  
  public List<? extends zzalh> zzt(final zzajq paramzzajq)
  {
    (List)this.bff.zzf(new Callable()
    {
      public List<? extends zzalh> zzbym()
      {
        zzaka.zzb(zzaka.this).zzi(zzall.zzan(paramzzajq));
        return zzaka.zza(zzaka.this, new zzakl(zzako.bgq, paramzzajq));
      }
    });
  }
  
  public static abstract interface zza
  {
    public abstract List<? extends zzalh> zzb(DatabaseError paramDatabaseError);
  }
  
  private static class zzb
    extends zzajl
  {
    private zzall bcY;
    
    public zzb(zzall paramzzall)
    {
      this.bcY = paramzzall;
    }
    
    public boolean equals(Object paramObject)
    {
      return ((paramObject instanceof zzb)) && (((zzb)paramObject).bcY.equals(this.bcY));
    }
    
    public int hashCode()
    {
      return this.bcY.hashCode();
    }
    
    public zzajl zza(zzall paramzzall)
    {
      return new zzb(paramzzall);
    }
    
    public zzalg zza(zzalf paramzzalf, zzall paramzzall)
    {
      return null;
    }
    
    public void zza(zzalg paramzzalg) {}
    
    public void zza(DatabaseError paramDatabaseError) {}
    
    public boolean zza(zzalh.zza paramzza)
    {
      return false;
    }
    
    public boolean zzc(zzajl paramzzajl)
    {
      return paramzzajl instanceof zzb;
    }
    
    public zzall zzcud()
    {
      return this.bcY;
    }
  }
  
  private class zzc
    implements zzaix, zzaka.zza
  {
    private final zzalm bfE;
    private final zzakb bfF;
    
    public zzc(zzalm paramzzalm)
    {
      this.bfE = paramzzalm;
      this.bfF = zzaka.zza(zzaka.this, paramzzalm.zzcyi());
    }
    
    public List<? extends zzalh> zzb(DatabaseError paramDatabaseError)
    {
      if (paramDatabaseError == null)
      {
        paramDatabaseError = this.bfE.zzcyi();
        if (this.bfF != null) {
          return zzaka.this.zza(this.bfF);
        }
        return zzaka.this.zzt(paramDatabaseError.zzcrc());
      }
      zzalw localzzalw = zzaka.zza(zzaka.this);
      String str1 = String.valueOf(this.bfE.zzcyi().zzcrc());
      String str2 = String.valueOf(paramDatabaseError.toString());
      localzzalw.warn(String.valueOf(str1).length() + 19 + String.valueOf(str2).length() + "Listen at " + str1 + " failed: " + str2);
      return zzaka.this.zza(this.bfE.zzcyi(), paramDatabaseError);
    }
    
    public String zzcsm()
    {
      return this.bfE.zzcyj().zzczd();
    }
    
    public boolean zzcsn()
    {
      return zzani.zzt(this.bfE.zzcyj()) > 1024L;
    }
    
    public zzair zzcso()
    {
      zzamb localzzamb = zzamb.zzi(this.bfE.zzcyj());
      Object localObject = localzzamb.zzcsf();
      ArrayList localArrayList = new ArrayList(((List)localObject).size());
      localObject = ((List)localObject).iterator();
      while (((Iterator)localObject).hasNext()) {
        localArrayList.add(((zzajq)((Iterator)localObject).next()).zzcvi());
      }
      return new zzair(localArrayList, localzzamb.zzcsg());
    }
  }
  
  public static abstract interface zzd
  {
    public abstract void zza(zzall paramzzall, zzakb paramzzakb);
    
    public abstract void zza(zzall paramzzall, zzakb paramzzakb, zzaix paramzzaix, zzaka.zza paramzza);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaka.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */