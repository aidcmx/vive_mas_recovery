package com.google.android.gms.internal;

public class zzakb
{
  private final long bfG;
  
  public zzakb(long paramLong)
  {
    this.bfG = paramLong;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    do
    {
      return true;
      if ((paramObject == null) || (getClass() != paramObject.getClass())) {
        return false;
      }
      paramObject = (zzakb)paramObject;
    } while (this.bfG == ((zzakb)paramObject).bfG);
    return false;
  }
  
  public int hashCode()
  {
    return (int)(this.bfG ^ this.bfG >>> 32);
  }
  
  public String toString()
  {
    long l = this.bfG;
    return 35 + "Tag{tagNumber=" + l + "}";
  }
  
  public long zzcwc()
  {
    return this.bfG;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzakb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */