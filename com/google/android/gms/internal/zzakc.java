package com.google.android.gms.internal;

public abstract interface zzakc
{
  public static final zzakc bfH = new zzakc()
  {
    public void zza(Thread paramAnonymousThread, String paramAnonymousString)
    {
      paramAnonymousThread.setName(paramAnonymousString);
    }
    
    public void zza(Thread paramAnonymousThread, Thread.UncaughtExceptionHandler paramAnonymousUncaughtExceptionHandler)
    {
      paramAnonymousThread.setUncaughtExceptionHandler(paramAnonymousUncaughtExceptionHandler);
    }
    
    public void zza(Thread paramAnonymousThread, boolean paramAnonymousBoolean)
    {
      paramAnonymousThread.setDaemon(paramAnonymousBoolean);
    }
  };
  
  public abstract void zza(Thread paramThread, String paramString);
  
  public abstract void zza(Thread paramThread, Thread.UncaughtExceptionHandler paramUncaughtExceptionHandler);
  
  public abstract void zza(Thread paramThread, boolean paramBoolean);
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzakc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */