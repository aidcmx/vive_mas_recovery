package com.google.android.gms.internal;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

class zzakd
  implements zzajn
{
  private final ThreadPoolExecutor bfI;
  
  public zzakd(final ThreadFactory paramThreadFactory, final zzakc paramzzakc)
  {
    LinkedBlockingQueue localLinkedBlockingQueue = new LinkedBlockingQueue();
    this.bfI = new ThreadPoolExecutor(1, 1, 3L, TimeUnit.SECONDS, localLinkedBlockingQueue, new ThreadFactory()
    {
      public Thread newThread(Runnable paramAnonymousRunnable)
      {
        paramAnonymousRunnable = paramThreadFactory.newThread(paramAnonymousRunnable);
        paramzzakc.zza(paramAnonymousRunnable, "FirebaseDatabaseEventTarget");
        paramzzakc.zza(paramAnonymousRunnable, true);
        return paramAnonymousRunnable;
      }
    });
  }
  
  public void restart()
  {
    this.bfI.setCorePoolSize(1);
  }
  
  public void shutdown()
  {
    this.bfI.setCorePoolSize(0);
  }
  
  public void zzq(Runnable paramRunnable)
  {
    this.bfI.execute(paramRunnable);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzakd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */