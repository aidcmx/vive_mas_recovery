package com.google.android.gms.internal;

public class zzake
{
  private final zzajq aZr;
  private final long bfM;
  private final zzaml bfN;
  private final zzajh bfO;
  private final boolean bfP;
  
  public zzake(long paramLong, zzajq paramzzajq, zzajh paramzzajh)
  {
    this.bfM = paramLong;
    this.aZr = paramzzajq;
    this.bfN = null;
    this.bfO = paramzzajh;
    this.bfP = true;
  }
  
  public zzake(long paramLong, zzajq paramzzajq, zzaml paramzzaml, boolean paramBoolean)
  {
    this.bfM = paramLong;
    this.aZr = paramzzajq;
    this.bfN = paramzzaml;
    this.bfO = null;
    this.bfP = paramBoolean;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    label93:
    do
    {
      return true;
      if ((paramObject == null) || (getClass() != paramObject.getClass())) {
        return false;
      }
      paramObject = (zzake)paramObject;
      if (this.bfM != ((zzake)paramObject).bfM) {
        return false;
      }
      if (!this.aZr.equals(((zzake)paramObject).aZr)) {
        return false;
      }
      if (this.bfP != ((zzake)paramObject).bfP) {
        return false;
      }
      if (this.bfN == null) {
        break;
      }
      if (!this.bfN.equals(((zzake)paramObject).bfN)) {
        break label123;
      }
      if (this.bfO == null) {
        break label125;
      }
    } while (this.bfO.equals(((zzake)paramObject).bfO));
    for (;;)
    {
      return false;
      if (((zzake)paramObject).bfN == null) {
        break label93;
      }
      label123:
      return false;
      label125:
      if (((zzake)paramObject).bfO == null) {
        break;
      }
    }
  }
  
  public int hashCode()
  {
    int j = 0;
    int k = Long.valueOf(this.bfM).hashCode();
    int m = Boolean.valueOf(this.bfP).hashCode();
    int n = this.aZr.hashCode();
    if (this.bfN != null) {}
    for (int i = this.bfN.hashCode();; i = 0)
    {
      if (this.bfO != null) {
        j = this.bfO.hashCode();
      }
      return (i + ((k * 31 + m) * 31 + n) * 31) * 31 + j;
    }
  }
  
  public boolean isVisible()
  {
    return this.bfP;
  }
  
  public String toString()
  {
    long l = this.bfM;
    String str1 = String.valueOf(this.aZr);
    boolean bool = this.bfP;
    String str2 = String.valueOf(this.bfN);
    String str3 = String.valueOf(this.bfO);
    return String.valueOf(str1).length() + 78 + String.valueOf(str2).length() + String.valueOf(str3).length() + "UserWriteRecord{id=" + l + " path=" + str1 + " visible=" + bool + " overwrite=" + str2 + " merge=" + str3 + "}";
  }
  
  public zzajq zzcrc()
  {
    return this.aZr;
  }
  
  public long zzcwd()
  {
    return this.bfM;
  }
  
  public zzaml zzcwe()
  {
    if (this.bfN == null) {
      throw new IllegalArgumentException("Can't access overwrite when write is a merge!");
    }
    return this.bfN;
  }
  
  public zzajh zzcwf()
  {
    if (this.bfO == null) {
      throw new IllegalArgumentException("Can't access merge when write is an overwrite!");
    }
    return this.bfO;
  }
  
  public boolean zzcwg()
  {
    return this.bfN != null;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzake.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */