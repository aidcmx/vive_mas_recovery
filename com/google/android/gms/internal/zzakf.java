package com.google.android.gms.internal;

import com.google.firebase.database.DatabaseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class zzakf
{
  private final List<String> bfQ = new ArrayList();
  private int bfR = 0;
  
  private zzakf(zzajq paramzzajq)
    throws DatabaseException
  {
    paramzzajq = paramzzajq.iterator();
    while (paramzzajq.hasNext())
    {
      zzalz localzzalz = (zzalz)paramzzajq.next();
      this.bfQ.add(localzzalz.asString());
    }
    this.bfR = Math.max(1, this.bfQ.size());
    while (i < this.bfQ.size())
    {
      int j = this.bfR;
      this.bfR = (zza((CharSequence)this.bfQ.get(i)) + j);
      i += 1;
    }
    zzcmr();
  }
  
  private static int zza(CharSequence paramCharSequence)
  {
    int j = 0;
    int k = paramCharSequence.length();
    int i = 0;
    if (j < k)
    {
      char c = paramCharSequence.charAt(j);
      if (c <= '') {
        i += 1;
      }
      for (;;)
      {
        j += 1;
        break;
        if (c <= '߿')
        {
          i += 2;
        }
        else if (Character.isHighSurrogate(c))
        {
          i += 4;
          j += 1;
        }
        else
        {
          i += 3;
        }
      }
    }
    return i;
  }
  
  public static void zza(zzajq paramzzajq, Object paramObject)
    throws DatabaseException
  {
    new zzakf(paramzzajq).zzbr(paramObject);
  }
  
  private void zzbr(Object paramObject)
    throws DatabaseException
  {
    if ((paramObject instanceof Map))
    {
      paramObject = (Map)paramObject;
      Iterator localIterator = ((Map)paramObject).keySet().iterator();
      while (localIterator.hasNext())
      {
        String str = (String)localIterator.next();
        if (!str.startsWith("."))
        {
          zzsw(str);
          zzbr(((Map)paramObject).get(str));
          zzcwh();
        }
      }
    }
    if ((paramObject instanceof List))
    {
      paramObject = (List)paramObject;
      int i = 0;
      while (i < ((List)paramObject).size())
      {
        zzsw(Integer.toString(i));
        zzbr(((List)paramObject).get(i));
        zzcwh();
        i += 1;
      }
    }
  }
  
  private void zzcmr()
    throws DatabaseException
  {
    String str1;
    if (this.bfR > 768)
    {
      str1 = String.valueOf("Data has a key path longer than 768 bytes (");
      int i = this.bfR;
      throw new DatabaseException(String.valueOf(str1).length() + 13 + str1 + i + ").");
    }
    if (this.bfQ.size() > 32)
    {
      str1 = String.valueOf("Path specified exceeds the maximum depth that can be written (32) or object contains a cycle ");
      String str2 = String.valueOf(zzcwi());
      if (str2.length() != 0) {}
      for (str1 = str1.concat(str2);; str1 = new String(str1)) {
        throw new DatabaseException(str1);
      }
    }
  }
  
  private String zzcwh()
  {
    String str = (String)this.bfQ.remove(this.bfQ.size() - 1);
    this.bfR -= zza(str);
    if (this.bfQ.size() > 0) {
      this.bfR -= 1;
    }
    return str;
  }
  
  private String zzcwi()
  {
    if (this.bfQ.size() == 0) {
      return "";
    }
    String str = String.valueOf(zze("/", this.bfQ));
    return String.valueOf(str).length() + 10 + "in path '" + str + "'";
  }
  
  private static String zze(String paramString, List<String> paramList)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    int i = 0;
    while (i < paramList.size())
    {
      if (i > 0) {
        localStringBuilder.append(paramString);
      }
      localStringBuilder.append((String)paramList.get(i));
      i += 1;
    }
    return localStringBuilder.toString();
  }
  
  private void zzsw(String paramString)
    throws DatabaseException
  {
    if (this.bfQ.size() > 0) {
      this.bfR += 1;
    }
    this.bfQ.add(paramString);
    this.bfR += zza(paramString);
    zzcmr();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzakf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */