package com.google.android.gms.internal;

import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.database.zza;

public class zzakg
  extends zzajl
{
  private final zzajs aZj;
  private final zzall bcY;
  private final ValueEventListener bfS;
  
  public zzakg(zzajs paramzzajs, ValueEventListener paramValueEventListener, zzall paramzzall)
  {
    this.aZj = paramzzajs;
    this.bfS = paramValueEventListener;
    this.bcY = paramzzall;
  }
  
  public boolean equals(Object paramObject)
  {
    return ((paramObject instanceof zzakg)) && (((zzakg)paramObject).bfS.equals(this.bfS)) && (((zzakg)paramObject).aZj.equals(this.aZj)) && (((zzakg)paramObject).bcY.equals(this.bcY));
  }
  
  public int hashCode()
  {
    return (this.bfS.hashCode() * 31 + this.aZj.hashCode()) * 31 + this.bcY.hashCode();
  }
  
  public String toString()
  {
    return "ValueEventRegistration";
  }
  
  public zzajl zza(zzall paramzzall)
  {
    return new zzakg(this.aZj, this.bfS, paramzzall);
  }
  
  public zzalg zza(zzalf paramzzalf, zzall paramzzall)
  {
    paramzzalf = zza.zza(zza.zza(this.aZj, paramzzall.zzcrc()), paramzzalf.zzcxj());
    return new zzalg(zzalh.zza.bhz, this, paramzzalf, null);
  }
  
  public void zza(zzalg paramzzalg)
  {
    if (zzcvc()) {
      return;
    }
    this.bfS.onDataChange(paramzzalg.zzcxp());
  }
  
  public void zza(DatabaseError paramDatabaseError)
  {
    this.bfS.onCancelled(paramDatabaseError);
  }
  
  public boolean zza(zzalh.zza paramzza)
  {
    return paramzza == zzalh.zza.bhz;
  }
  
  public boolean zzc(zzajl paramzzajl)
  {
    return ((paramzzajl instanceof zzakg)) && (((zzakg)paramzzajl).bfS.equals(this.bfS));
  }
  
  public zzall zzcud()
  {
    return this.bcY;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzakg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */