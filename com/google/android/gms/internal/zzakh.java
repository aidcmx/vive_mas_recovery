package com.google.android.gms.internal;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

public class zzakh
{
  private static final zzala<zzake> bfW;
  private zzajh bfT = zzajh.zzcue();
  private List<zzake> bfU = new ArrayList();
  private Long bfV = Long.valueOf(-1L);
  
  static
  {
    if (!zzakh.class.desiredAssertionStatus()) {}
    for (boolean bool = true;; bool = false)
    {
      $assertionsDisabled = bool;
      bfW = new zzala()
      {
        public boolean zza(zzake paramAnonymouszzake)
        {
          return paramAnonymouszzake.isVisible();
        }
      };
      return;
    }
  }
  
  private static zzajh zza(List<zzake> paramList, zzala<zzake> paramzzala, zzajq paramzzajq)
  {
    Object localObject = zzajh.zzcue();
    Iterator localIterator = paramList.iterator();
    paramList = (List<zzake>)localObject;
    zzajq localzzajq;
    if (localIterator.hasNext())
    {
      localObject = (zzake)localIterator.next();
      if (!paramzzala.zzbs(localObject)) {
        break label215;
      }
      localzzajq = ((zzake)localObject).zzcrc();
      if (((zzake)localObject).zzcwg()) {
        if (paramzzajq.zzi(localzzajq)) {
          paramList = paramList.zze(zzajq.zza(paramzzajq, localzzajq), ((zzake)localObject).zzcwe());
        }
      }
    }
    label215:
    for (;;)
    {
      break;
      if (localzzajq.zzi(paramzzajq))
      {
        paramList = paramList.zze(zzajq.zzcvg(), ((zzake)localObject).zzcwe().zzao(zzajq.zza(localzzajq, paramzzajq)));
        continue;
        if (paramzzajq.zzi(localzzajq))
        {
          paramList = paramList.zzb(zzajq.zza(paramzzajq, localzzajq), ((zzake)localObject).zzcwf());
        }
        else if (localzzajq.zzi(paramzzajq))
        {
          localzzajq = zzajq.zza(localzzajq, paramzzajq);
          if (localzzajq.isEmpty())
          {
            paramList = paramList.zzb(zzajq.zzcvg(), ((zzake)localObject).zzcwf());
          }
          else
          {
            localObject = ((zzake)localObject).zzcwf().zzf(localzzajq);
            if (localObject != null)
            {
              paramList = paramList.zze(zzajq.zzcvg(), (zzaml)localObject);
              continue;
              return paramList;
            }
          }
        }
      }
    }
  }
  
  private boolean zza(zzake paramzzake, zzajq paramzzajq)
  {
    if (paramzzake.zzcwg()) {
      return paramzzake.zzcrc().zzi(paramzzajq);
    }
    Iterator localIterator = paramzzake.zzcwf().iterator();
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      if (paramzzake.zzcrc().zzh((zzajq)localEntry.getKey()).zzi(paramzzajq)) {
        return true;
      }
    }
    return false;
  }
  
  private void zzcwk()
  {
    this.bfT = zza(this.bfU, bfW, zzajq.zzcvg());
    if (this.bfU.size() > 0)
    {
      this.bfV = Long.valueOf(((zzake)this.bfU.get(this.bfU.size() - 1)).zzcwd());
      return;
    }
    this.bfV = Long.valueOf(-1L);
  }
  
  public zzamk zza(zzajq paramzzajq, zzaml paramzzaml, zzamk paramzzamk, boolean paramBoolean, zzamf paramzzamf)
  {
    Object localObject1 = null;
    Object localObject2 = null;
    Object localObject3 = this.bfT.zzg(paramzzajq);
    paramzzajq = ((zzajh)localObject3).zzf(zzajq.zzcvg());
    if (paramzzajq != null)
    {
      localObject3 = paramzzajq.iterator();
      paramzzajq = (zzajq)localObject2;
      label40:
      localObject1 = paramzzajq;
      if (!((Iterator)localObject3).hasNext()) {
        break label120;
      }
      localObject1 = (zzamk)((Iterator)localObject3).next();
      if (paramzzamf.zza((zzamk)localObject1, paramzzamk, paramBoolean) <= 0) {
        break label123;
      }
      paramzzaml = (zzaml)localObject1;
      if (paramzzajq != null) {
        if (paramzzamf.zza((zzamk)localObject1, paramzzajq, paramBoolean) >= 0) {
          break label123;
        }
      }
    }
    label120:
    label123:
    for (paramzzaml = (zzaml)localObject1;; paramzzaml = paramzzajq)
    {
      paramzzajq = paramzzaml;
      break label40;
      if (paramzzaml != null)
      {
        paramzzajq = ((zzajh)localObject3).zzb(paramzzaml);
        break;
      }
      return (zzamk)localObject1;
    }
  }
  
  public zzaml zza(zzajq paramzzajq1, zzajq paramzzajq2, zzaml paramzzaml1, zzaml paramzzaml2)
  {
    assert ((paramzzaml1 != null) || (paramzzaml2 != null)) : "Either existingEventSnap or existingServerSnap must exist";
    paramzzajq1 = paramzzajq1.zzh(paramzzajq2);
    if (this.bfT.zze(paramzzajq1)) {
      return null;
    }
    paramzzajq1 = this.bfT.zzg(paramzzajq1);
    if (paramzzajq1.isEmpty()) {
      return paramzzaml2.zzao(paramzzajq2);
    }
    return paramzzajq1.zzb(paramzzaml2.zzao(paramzzajq2));
  }
  
  public zzaml zza(zzajq paramzzajq, zzalz paramzzalz, zzald paramzzald)
  {
    paramzzajq = paramzzajq.zza(paramzzalz);
    zzaml localzzaml = this.bfT.zzf(paramzzajq);
    if (localzzaml != null) {
      return localzzaml;
    }
    if (paramzzald.zzf(paramzzalz)) {
      return this.bfT.zzg(paramzzajq).zzb(paramzzald.zzcqy().zzm(paramzzalz));
    }
    return null;
  }
  
  public zzaml zza(final zzajq paramzzajq, zzaml paramzzaml, final List<Long> paramList, final boolean paramBoolean)
  {
    zzaml localzzaml;
    if ((paramList.isEmpty()) && (!paramBoolean))
    {
      localzzaml = this.bfT.zzf(paramzzajq);
      if (localzzaml == null) {}
    }
    zzajh localzzajh;
    do
    {
      do
      {
        return localzzaml;
        paramzzajq = this.bfT.zzg(paramzzajq);
        localzzaml = paramzzaml;
      } while (paramzzajq.isEmpty());
      if ((paramzzaml == null) && (!paramzzajq.zze(zzajq.zzcvg()))) {
        return null;
      }
      if (paramzzaml != null) {}
      for (;;)
      {
        return paramzzajq.zzb(paramzzaml);
        paramzzaml = zzame.zzczq();
      }
      localzzajh = this.bfT.zzg(paramzzajq);
      if (paramBoolean) {
        break;
      }
      localzzaml = paramzzaml;
    } while (localzzajh.isEmpty());
    if ((!paramBoolean) && (paramzzaml == null) && (!localzzajh.zze(zzajq.zzcvg()))) {
      return null;
    }
    paramList = new zzala()
    {
      public boolean zza(zzake paramAnonymouszzake)
      {
        return ((paramAnonymouszzake.isVisible()) || (paramBoolean)) && (!paramList.contains(Long.valueOf(paramAnonymouszzake.zzcwd()))) && ((paramAnonymouszzake.zzcrc().zzi(paramzzajq)) || (paramzzajq.zzi(paramAnonymouszzake.zzcrc())));
      }
    };
    paramzzajq = zza(this.bfU, paramList, paramzzajq);
    if (paramzzaml != null) {}
    for (;;)
    {
      return paramzzajq.zzb(paramzzaml);
      paramzzaml = zzame.zzczq();
    }
  }
  
  public void zza(zzajq paramzzajq, zzajh paramzzajh, Long paramLong)
  {
    assert (paramLong.longValue() > this.bfV.longValue());
    this.bfU.add(new zzake(paramLong.longValue(), paramzzajq, paramzzajh));
    this.bfT = this.bfT.zzb(paramzzajq, paramzzajh);
    this.bfV = paramLong;
  }
  
  public void zza(zzajq paramzzajq, zzaml paramzzaml, Long paramLong, boolean paramBoolean)
  {
    assert (paramLong.longValue() > this.bfV.longValue());
    this.bfU.add(new zzake(paramLong.longValue(), paramzzajq, paramzzaml, paramBoolean));
    if (paramBoolean) {
      this.bfT = this.bfT.zze(paramzzajq, paramzzaml);
    }
    this.bfV = paramLong;
  }
  
  public zzake zzcj(long paramLong)
  {
    Iterator localIterator = this.bfU.iterator();
    while (localIterator.hasNext())
    {
      zzake localzzake = (zzake)localIterator.next();
      if (localzzake.zzcwd() == paramLong) {
        return localzzake;
      }
    }
    return null;
  }
  
  public boolean zzck(long paramLong)
  {
    boolean bool2 = true;
    Object localObject2 = null;
    Object localObject3 = this.bfU.iterator();
    int j = 0;
    Object localObject1;
    for (;;)
    {
      localObject1 = localObject2;
      if (((Iterator)localObject3).hasNext())
      {
        localObject1 = (zzake)((Iterator)localObject3).next();
        if (((zzake)localObject1).zzcwd() != paramLong) {}
      }
      else
      {
        if (($assertionsDisabled) || (localObject1 != null)) {
          break;
        }
        throw new AssertionError("removeWrite called with nonexistent writeId");
      }
      j += 1;
    }
    this.bfU.remove(localObject1);
    boolean bool1 = ((zzake)localObject1).isVisible();
    int k = this.bfU.size() - 1;
    int i = 0;
    if ((bool1) && (k >= 0))
    {
      localObject2 = (zzake)this.bfU.get(k);
      if (!((zzake)localObject2).isVisible()) {
        break label323;
      }
      if ((k >= j) && (zza((zzake)localObject2, ((zzake)localObject1).zzcrc()))) {
        bool1 = false;
      }
    }
    label323:
    for (;;)
    {
      k -= 1;
      break;
      if (((zzake)localObject1).zzcrc().zzi(((zzake)localObject2).zzcrc()))
      {
        i = 1;
        continue;
        if (!bool1)
        {
          bool1 = false;
          return bool1;
        }
        if (i != 0)
        {
          zzcwk();
          return true;
        }
        if (((zzake)localObject1).zzcwg())
        {
          this.bfT = this.bfT.zzd(((zzake)localObject1).zzcrc());
          return true;
        }
        localObject2 = ((zzake)localObject1).zzcwf().iterator();
        for (;;)
        {
          bool1 = bool2;
          if (!((Iterator)localObject2).hasNext()) {
            break;
          }
          localObject3 = (zzajq)((Map.Entry)((Iterator)localObject2).next()).getKey();
          this.bfT = this.bfT.zzd(((zzake)localObject1).zzcrc().zzh((zzajq)localObject3));
        }
      }
    }
  }
  
  public List<zzake> zzcwj()
  {
    ArrayList localArrayList = new ArrayList(this.bfU);
    this.bfT = zzajh.zzcue();
    this.bfU = new ArrayList();
    return localArrayList;
  }
  
  public zzaml zzj(zzajq paramzzajq, zzaml paramzzaml)
  {
    Object localObject1 = zzame.zzczq();
    Object localObject2 = this.bfT.zzf(paramzzajq);
    if (localObject2 != null)
    {
      if (!((zzaml)localObject2).zzcze())
      {
        localObject2 = ((zzaml)localObject2).iterator();
        for (paramzzaml = (zzaml)localObject1;; paramzzaml = paramzzaml.zze(paramzzajq.a(), paramzzajq.zzcqy()))
        {
          paramzzajq = paramzzaml;
          if (!((Iterator)localObject2).hasNext()) {
            break;
          }
          paramzzajq = (zzamk)((Iterator)localObject2).next();
        }
      }
    }
    else
    {
      localObject2 = this.bfT.zzg(paramzzajq);
      paramzzaml = paramzzaml.iterator();
      zzaml localzzaml;
      for (paramzzajq = (zzajq)localObject1; paramzzaml.hasNext(); paramzzajq = paramzzajq.zze(((zzamk)localObject1).a(), localzzaml))
      {
        localObject1 = (zzamk)paramzzaml.next();
        localzzaml = ((zzajh)localObject2).zzg(new zzajq(new zzalz[] { ((zzamk)localObject1).a() })).zzb(((zzamk)localObject1).zzcqy());
      }
      localObject1 = ((zzajh)localObject2).zzcug().iterator();
      for (paramzzaml = paramzzajq;; paramzzaml = paramzzaml.zze(paramzzajq.a(), paramzzajq.zzcqy()))
      {
        paramzzajq = paramzzaml;
        if (!((Iterator)localObject1).hasNext()) {
          break;
        }
        paramzzajq = (zzamk)((Iterator)localObject1).next();
      }
    }
    paramzzajq = (zzajq)localObject1;
    return paramzzajq;
  }
  
  public zzaki zzu(zzajq paramzzajq)
  {
    return new zzaki(paramzzajq, this);
  }
  
  public zzaml zzv(zzajq paramzzajq)
  {
    return this.bfT.zzf(paramzzajq);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzakh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */