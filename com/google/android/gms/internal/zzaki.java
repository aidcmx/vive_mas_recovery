package com.google.android.gms.internal;

import java.util.Collections;
import java.util.List;

public class zzaki
{
  private final zzajq bgb;
  private final zzakh bgc;
  
  public zzaki(zzajq paramzzajq, zzakh paramzzakh)
  {
    this.bgb = paramzzajq;
    this.bgc = paramzzakh;
  }
  
  public zzamk zza(zzaml paramzzaml, zzamk paramzzamk, boolean paramBoolean, zzamf paramzzamf)
  {
    return this.bgc.zza(this.bgb, paramzzaml, paramzzamk, paramBoolean, paramzzamf);
  }
  
  public zzaml zza(zzajq paramzzajq, zzaml paramzzaml1, zzaml paramzzaml2)
  {
    return this.bgc.zza(this.bgb, paramzzajq, paramzzaml1, paramzzaml2);
  }
  
  public zzaml zza(zzalz paramzzalz, zzald paramzzald)
  {
    return this.bgc.zza(this.bgb, paramzzalz, paramzzald);
  }
  
  public zzaml zza(zzaml paramzzaml, List<Long> paramList)
  {
    return zza(paramzzaml, paramList, false);
  }
  
  public zzaml zza(zzaml paramzzaml, List<Long> paramList, boolean paramBoolean)
  {
    return this.bgc.zza(this.bgb, paramzzaml, paramList, paramBoolean);
  }
  
  public zzaki zzb(zzalz paramzzalz)
  {
    return new zzaki(this.bgb.zza(paramzzalz), this.bgc);
  }
  
  public zzaml zzc(zzaml paramzzaml)
  {
    return zza(paramzzaml, Collections.emptyList());
  }
  
  public zzaml zzd(zzaml paramzzaml)
  {
    return this.bgc.zzj(this.bgb, paramzzaml);
  }
  
  public zzaml zzv(zzajq paramzzajq)
  {
    return this.bgc.zzv(this.bgb.zzh(paramzzajq));
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaki.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */