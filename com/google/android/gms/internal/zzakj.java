package com.google.android.gms.internal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class zzakj
  implements zzajm
{
  private static zzakj bge;
  final HashMap<zzajl, List<zzajl>> bgd = new HashMap();
  
  static
  {
    if (!zzakj.class.desiredAssertionStatus()) {}
    for (boolean bool = true;; bool = false)
    {
      $assertionsDisabled = bool;
      bge = new zzakj();
      return;
    }
  }
  
  public static zzakj zzcwl()
  {
    return bge;
  }
  
  private void zzj(zzajl paramzzajl)
  {
    int j = 0;
    label129:
    label239:
    label244:
    label249:
    for (;;)
    {
      Object localObject;
      synchronized (this.bgd)
      {
        localObject = (List)this.bgd.get(paramzzajl);
        if (localObject == null) {
          break label244;
        }
        i = 0;
        if (i >= ((List)localObject).size()) {
          break label239;
        }
        if (((List)localObject).get(i) == paramzzajl)
        {
          int k = 1;
          ((List)localObject).remove(i);
          i = k;
          if (!((List)localObject).isEmpty()) {
            break label249;
          }
          this.bgd.remove(paramzzajl);
          break label249;
          if (($assertionsDisabled) || (i != 0) || (!paramzzajl.zzcvd())) {
            break label129;
          }
          throw new AssertionError();
        }
      }
      i += 1;
      continue;
      List localList;
      if (!paramzzajl.zzcud().isDefault())
      {
        localObject = paramzzajl.zza(zzall.zzan(paramzzajl.zzcud().zzcrc()));
        localList = (List)this.bgd.get(localObject);
        if (localList != null) {
          i = j;
        }
      }
      for (;;)
      {
        if (i < localList.size())
        {
          if (localList.get(i) == paramzzajl) {
            localList.remove(i);
          }
        }
        else
        {
          if (localList.isEmpty()) {
            this.bgd.remove(localObject);
          }
          return;
        }
        i += 1;
      }
      int i = 0;
      continue;
      i = 0;
    }
  }
  
  public void zzd(zzajl paramzzajl)
  {
    zzj(paramzzajl);
  }
  
  public void zzi(zzajl paramzzajl)
  {
    synchronized (this.bgd)
    {
      List localList = (List)this.bgd.get(paramzzajl);
      Object localObject = localList;
      if (localList == null)
      {
        localObject = new ArrayList();
        this.bgd.put(paramzzajl, localObject);
      }
      ((List)localObject).add(paramzzajl);
      if (!paramzzajl.zzcud().isDefault())
      {
        zzajl localzzajl = paramzzajl.zza(zzall.zzan(paramzzajl.zzcud().zzcrc()));
        localList = (List)this.bgd.get(localzzajl);
        localObject = localList;
        if (localList == null)
        {
          localObject = new ArrayList();
          this.bgd.put(localzzajl, localObject);
        }
        ((List)localObject).add(paramzzajl);
      }
      paramzzajl.zzdb(true);
      paramzzajl.zza(this);
      return;
    }
  }
  
  public void zzk(zzajl paramzzajl)
  {
    for (;;)
    {
      int i;
      synchronized (this.bgd)
      {
        List localList = (List)this.bgd.get(paramzzajl);
        if ((localList != null) && (!localList.isEmpty())) {
          if (paramzzajl.zzcud().isDefault())
          {
            paramzzajl = new HashSet();
            i = localList.size() - 1;
            if (i >= 0)
            {
              zzajl localzzajl = (zzajl)localList.get(i);
              if (paramzzajl.contains(localzzajl.zzcud())) {
                break label132;
              }
              paramzzajl.add(localzzajl.zzcud());
              localzzajl.zzcvb();
              break label132;
            }
          }
          else
          {
            ((zzajl)localList.get(0)).zzcvb();
          }
        }
        return;
      }
      label132:
      i -= 1;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzakj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */