package com.google.android.gms.internal;

public class zzakk
  extends zzakn
{
  private final boolean bgf;
  private final zzakz<Boolean> bgg;
  
  public zzakk(zzajq paramzzajq, zzakz<Boolean> paramzzakz, boolean paramBoolean)
  {
    super(zzakn.zza.bgm, zzako.bgp, paramzzajq);
    this.bgg = paramzzakz;
    this.bgf = paramBoolean;
  }
  
  public String toString()
  {
    return String.format("AckUserWrite { path=%s, revert=%s, affectedTree=%s }", new Object[] { zzcrc(), Boolean.valueOf(this.bgf), this.bgg });
  }
  
  public zzakn zzc(zzalz paramzzalz)
  {
    if (!this.aZr.isEmpty())
    {
      zzann.zzb(this.aZr.zzcvj().equals(paramzzalz), "operationForChild called for unrelated child.");
      return new zzakk(this.aZr.zzcvk(), this.bgg, this.bgf);
    }
    if (this.bgg.getValue() != null)
    {
      zzann.zzb(this.bgg.zzcxf().isEmpty(), "affectedTree should not have overlapping affected paths.");
      return this;
    }
    paramzzalz = this.bgg.zzai(new zzajq(new zzalz[] { paramzzalz }));
    return new zzakk(zzajq.zzcvg(), paramzzalz, this.bgf);
  }
  
  public zzakz<Boolean> zzcwm()
  {
    return this.bgg;
  }
  
  public boolean zzcwn()
  {
    return this.bgf;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzakk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */