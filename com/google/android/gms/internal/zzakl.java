package com.google.android.gms.internal;

public class zzakl
  extends zzakn
{
  static
  {
    if (!zzakl.class.desiredAssertionStatus()) {}
    for (boolean bool = true;; bool = false)
    {
      $assertionsDisabled = bool;
      return;
    }
  }
  
  public zzakl(zzako paramzzako, zzajq paramzzajq)
  {
    super(zzakn.zza.bgn, paramzzako, paramzzajq);
    assert (!paramzzako.zzcwr()) : "Can't have a listen complete from a user source";
  }
  
  public String toString()
  {
    return String.format("ListenComplete { path=%s, source=%s }", new Object[] { zzcrc(), zzcwp() });
  }
  
  public zzakn zzc(zzalz paramzzalz)
  {
    if (this.aZr.isEmpty()) {
      return new zzakl(this.bgj, zzajq.zzcvg());
    }
    return new zzakl(this.bgj, this.aZr.zzcvk());
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzakl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */