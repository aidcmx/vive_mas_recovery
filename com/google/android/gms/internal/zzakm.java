package com.google.android.gms.internal;

public class zzakm
  extends zzakn
{
  private final zzajh bgh;
  
  public zzakm(zzako paramzzako, zzajq paramzzajq, zzajh paramzzajh)
  {
    super(zzakn.zza.bgl, paramzzako, paramzzajq);
    this.bgh = paramzzajh;
  }
  
  public String toString()
  {
    return String.format("Merge { path=%s, source=%s, children=%s }", new Object[] { zzcrc(), zzcwp(), this.bgh });
  }
  
  public zzakn zzc(zzalz paramzzalz)
  {
    if (this.aZr.isEmpty())
    {
      paramzzalz = this.bgh.zzg(new zzajq(new zzalz[] { paramzzalz }));
      if (!paramzzalz.isEmpty()) {}
    }
    while (!this.aZr.zzcvj().equals(paramzzalz))
    {
      return null;
      if (paramzzalz.zzcuf() != null) {
        return new zzakp(this.bgj, zzajq.zzcvg(), paramzzalz.zzcuf());
      }
      return new zzakm(this.bgj, zzajq.zzcvg(), paramzzalz);
    }
    return new zzakm(this.bgj, this.aZr.zzcvk(), this.bgh);
  }
  
  public zzajh zzcwo()
  {
    return this.bgh;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzakm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */