package com.google.android.gms.internal;

public abstract class zzakn
{
  protected final zzajq aZr;
  protected final zza bgi;
  protected final zzako bgj;
  
  protected zzakn(zza paramzza, zzako paramzzako, zzajq paramzzajq)
  {
    this.bgi = paramzza;
    this.bgj = paramzzako;
    this.aZr = paramzzajq;
  }
  
  public abstract zzakn zzc(zzalz paramzzalz);
  
  public zzajq zzcrc()
  {
    return this.aZr;
  }
  
  public zzako zzcwp()
  {
    return this.bgj;
  }
  
  public zza zzcwq()
  {
    return this.bgi;
  }
  
  public static enum zza
  {
    private zza() {}
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzakn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */