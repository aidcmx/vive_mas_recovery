package com.google.android.gms.internal;

public class zzako
{
  public static final zzako bgp;
  public static final zzako bgq;
  private final zza bgr;
  private final zzalk bgs;
  private final boolean bgt;
  
  static
  {
    if (!zzako.class.desiredAssertionStatus()) {}
    for (boolean bool = true;; bool = false)
    {
      $assertionsDisabled = bool;
      bgp = new zzako(zza.bgu, null, false);
      bgq = new zzako(zza.bgv, null, false);
      return;
    }
  }
  
  public zzako(zza paramzza, zzalk paramzzalk, boolean paramBoolean)
  {
    this.bgr = paramzza;
    this.bgs = paramzzalk;
    this.bgt = paramBoolean;
    assert ((!paramBoolean) || (zzcws()));
  }
  
  public static zzako zzc(zzalk paramzzalk)
  {
    return new zzako(zza.bgv, paramzzalk, true);
  }
  
  public String toString()
  {
    String str1 = String.valueOf(this.bgr);
    String str2 = String.valueOf(this.bgs);
    boolean bool = this.bgt;
    return String.valueOf(str1).length() + 52 + String.valueOf(str2).length() + "OperationSource{source=" + str1 + ", queryParams=" + str2 + ", tagged=" + bool + "}";
  }
  
  public boolean zzcwr()
  {
    return this.bgr == zza.bgu;
  }
  
  public boolean zzcws()
  {
    return this.bgr == zza.bgv;
  }
  
  public boolean zzcwt()
  {
    return this.bgt;
  }
  
  public zzalk zzcwu()
  {
    return this.bgs;
  }
  
  private static enum zza
  {
    private zza() {}
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzako.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */