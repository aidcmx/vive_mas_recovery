package com.google.android.gms.internal;

public class zzakp
  extends zzakn
{
  private final zzaml bgx;
  
  public zzakp(zzako paramzzako, zzajq paramzzajq, zzaml paramzzaml)
  {
    super(zzakn.zza.bgk, paramzzako, paramzzajq);
    this.bgx = paramzzaml;
  }
  
  public String toString()
  {
    return String.format("Overwrite { path=%s, source=%s, snapshot=%s }", new Object[] { zzcrc(), zzcwp(), this.bgx });
  }
  
  public zzakn zzc(zzalz paramzzalz)
  {
    if (this.aZr.isEmpty()) {
      return new zzakp(this.bgj, zzajq.zzcvg(), this.bgx.zzm(paramzzalz));
    }
    return new zzakp(this.bgj, this.aZr.zzcvk(), this.bgx);
  }
  
  public zzaml zzcwv()
  {
    return this.bgx;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzakp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */