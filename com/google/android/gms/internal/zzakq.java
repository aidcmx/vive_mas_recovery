package com.google.android.gms.internal;

public abstract interface zzakq
{
  public static final zzakq bgy = new zzakq()
  {
    public boolean zzcl(long paramAnonymousLong)
    {
      return false;
    }
    
    public float zzcww()
    {
      return 0.0F;
    }
    
    public long zzcwx()
    {
      return Long.MAX_VALUE;
    }
    
    public boolean zzi(long paramAnonymousLong1, long paramAnonymousLong2)
    {
      return false;
    }
  };
  
  public abstract boolean zzcl(long paramLong);
  
  public abstract float zzcww();
  
  public abstract long zzcwx();
  
  public abstract boolean zzi(long paramLong1, long paramLong2);
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzakq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */