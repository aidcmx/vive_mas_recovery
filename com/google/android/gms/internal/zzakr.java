package com.google.android.gms.internal;

import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Callable;

public class zzakr
  implements zzaku
{
  private final zzalw aZR;
  private final zzaky bgA;
  private final zzakq bgB;
  private long bgC = 0L;
  private final zzakv bgz;
  
  static
  {
    if (!zzakr.class.desiredAssertionStatus()) {}
    for (boolean bool = true;; bool = false)
    {
      $assertionsDisabled = bool;
      return;
    }
  }
  
  public zzakr(zzajj paramzzajj, zzakv paramzzakv, zzakq paramzzakq)
  {
    this(paramzzajj, paramzzakv, paramzzakq, new zzang());
  }
  
  public zzakr(zzajj paramzzajj, zzakv paramzzakv, zzakq paramzzakq, zzanf paramzzanf)
  {
    this.bgz = paramzzakv;
    this.aZR = paramzzajj.zzss("Persistence");
    this.bgA = new zzaky(this.bgz, this.aZR, paramzzanf);
    this.bgB = paramzzakq;
  }
  
  private void zzcwy()
  {
    this.bgC += 1L;
    if (this.bgB.zzcl(this.bgC))
    {
      if (this.aZR.zzcyu()) {
        this.aZR.zzi("Reached prune check threshold.", new Object[0]);
      }
      this.bgC = 0L;
      int j = 1;
      long l2 = this.bgz.zzcrf();
      int i = j;
      long l1 = l2;
      if (this.aZR.zzcyu())
      {
        this.aZR.zzi(32 + "Cache size: " + l2, new Object[0]);
        l1 = l2;
        i = j;
      }
      if ((i != 0) && (this.bgB.zzi(l1, this.bgA.zzcxc())))
      {
        zzakw localzzakw = this.bgA.zza(this.bgB);
        if (localzzakw.zzcwz()) {
          this.bgz.zza(zzajq.zzcvg(), localzzakw);
        }
        for (j = i;; j = 0)
        {
          l2 = this.bgz.zzcrf();
          i = j;
          l1 = l2;
          if (!this.aZR.zzcyu()) {
            break;
          }
          this.aZR.zzi(44 + "Cache size after prune: " + l2, new Object[0]);
          i = j;
          l1 = l2;
          break;
        }
      }
    }
  }
  
  public void zza(zzajq paramzzajq, zzajh paramzzajh, long paramLong)
  {
    this.bgz.zza(paramzzajq, paramzzajh, paramLong);
  }
  
  public void zza(zzajq paramzzajq, zzaml paramzzaml, long paramLong)
  {
    this.bgz.zza(paramzzajq, paramzzaml, paramLong);
  }
  
  public void zza(zzall paramzzall, zzaml paramzzaml)
  {
    if (paramzzall.zzcye()) {
      this.bgz.zza(paramzzall.zzcrc(), paramzzaml);
    }
    for (;;)
    {
      zzi(paramzzall);
      zzcwy();
      return;
      this.bgz.zzb(paramzzall.zzcrc(), paramzzaml);
    }
  }
  
  public void zza(zzall paramzzall, Set<zzalz> paramSet)
  {
    assert (!paramzzall.zzcye()) : "We should only track keys for filtered queries.";
    paramzzall = this.bgA.zzl(paramzzall);
    assert ((paramzzall != null) && (paramzzall.bgO)) : "We only expect tracked keys for currently-active queries.";
    this.bgz.zza(paramzzall.id, paramSet);
  }
  
  public void zza(zzall paramzzall, Set<zzalz> paramSet1, Set<zzalz> paramSet2)
  {
    assert (!paramzzall.zzcye()) : "We should only track keys for filtered queries.";
    paramzzall = this.bgA.zzl(paramzzall);
    assert ((paramzzall != null) && (paramzzall.bgO)) : "We only expect tracked keys for currently-active queries.";
    this.bgz.zza(paramzzall.id, paramSet1, paramSet2);
  }
  
  public void zzby(long paramLong)
  {
    this.bgz.zzby(paramLong);
  }
  
  public void zzc(zzajq paramzzajq, zzajh paramzzajh)
  {
    paramzzajh = paramzzajh.iterator();
    while (paramzzajh.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)paramzzajh.next();
      zzk(paramzzajq.zzh((zzajq)localEntry.getKey()), (zzaml)localEntry.getValue());
    }
  }
  
  public List<zzake> zzcre()
  {
    return this.bgz.zzcre();
  }
  
  public void zzcrh()
  {
    this.bgz.zzcrh();
  }
  
  public void zzd(zzajq paramzzajq, zzajh paramzzajh)
  {
    this.bgz.zza(paramzzajq, paramzzajh);
    zzcwy();
  }
  
  public zzald zzf(zzall paramzzall)
  {
    Object localObject1;
    if (this.bgA.zzo(paramzzall))
    {
      localObject1 = this.bgA.zzl(paramzzall);
      if ((!paramzzall.zzcye()) && (localObject1 != null) && (((zzakx)localObject1).bgN)) {
        localObject1 = this.bgz.zzcb(((zzakx)localObject1).id);
      }
    }
    zzaml localzzaml;
    for (boolean bool = true;; bool = false)
    {
      localzzaml = this.bgz.zza(paramzzall.zzcrc());
      if (localObject1 == null) {
        break label172;
      }
      Object localObject2 = zzame.zzczq();
      Iterator localIterator = ((Set)localObject1).iterator();
      for (localObject1 = localObject2; localIterator.hasNext(); localObject1 = ((zzaml)localObject1).zze((zzalz)localObject2, localzzaml.zzm((zzalz)localObject2))) {
        localObject2 = (zzalz)localIterator.next();
      }
      localObject1 = null;
      break;
      localObject1 = this.bgA.zzab(paramzzall.zzcrc());
    }
    return new zzald(zzamg.zza((zzaml)localObject1, paramzzall.zzcya()), bool, true);
    label172:
    return new zzald(zzamg.zza(localzzaml, paramzzall.zzcya()), true, false);
  }
  
  public <T> T zzf(Callable<T> paramCallable)
  {
    this.bgz.beginTransaction();
    try
    {
      paramCallable = paramCallable.call();
      this.bgz.setTransactionSuccessful();
      return paramCallable;
    }
    catch (Throwable paramCallable)
    {
      throw new RuntimeException(paramCallable);
    }
    finally
    {
      this.bgz.endTransaction();
    }
  }
  
  public void zzg(zzall paramzzall)
  {
    this.bgA.zzg(paramzzall);
  }
  
  public void zzh(zzall paramzzall)
  {
    this.bgA.zzh(paramzzall);
  }
  
  public void zzi(zzall paramzzall)
  {
    if (paramzzall.zzcye())
    {
      this.bgA.zzaa(paramzzall.zzcrc());
      return;
    }
    this.bgA.zzn(paramzzall);
  }
  
  public void zzk(zzajq paramzzajq, zzaml paramzzaml)
  {
    if (!this.bgA.zzad(paramzzajq))
    {
      this.bgz.zza(paramzzajq, paramzzaml);
      this.bgA.zzac(paramzzajq);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzakr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */