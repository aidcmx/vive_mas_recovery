package com.google.android.gms.internal;

public class zzaks
  implements zzakq
{
  public final long bgD;
  
  public zzaks(long paramLong)
  {
    this.bgD = paramLong;
  }
  
  public boolean zzcl(long paramLong)
  {
    return paramLong > 1000L;
  }
  
  public float zzcww()
  {
    return 0.2F;
  }
  
  public long zzcwx()
  {
    return 1000L;
  }
  
  public boolean zzi(long paramLong1, long paramLong2)
  {
    return (paramLong1 > this.bgD) || (paramLong2 > 1000L);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaks.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */