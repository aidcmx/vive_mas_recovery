package com.google.android.gms.internal;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;

public class zzakt
  implements zzaku
{
  private boolean aZS = false;
  
  private void zzcri()
  {
    zzann.zzb(this.aZS, "Transaction expected to already be in progress.");
  }
  
  public void zza(zzajq paramzzajq, zzajh paramzzajh, long paramLong)
  {
    zzcri();
  }
  
  public void zza(zzajq paramzzajq, zzaml paramzzaml, long paramLong)
  {
    zzcri();
  }
  
  public void zza(zzall paramzzall, zzaml paramzzaml)
  {
    zzcri();
  }
  
  public void zza(zzall paramzzall, Set<zzalz> paramSet)
  {
    zzcri();
  }
  
  public void zza(zzall paramzzall, Set<zzalz> paramSet1, Set<zzalz> paramSet2)
  {
    zzcri();
  }
  
  public void zzby(long paramLong)
  {
    zzcri();
  }
  
  public void zzc(zzajq paramzzajq, zzajh paramzzajh)
  {
    zzcri();
  }
  
  public List<zzake> zzcre()
  {
    return Collections.emptyList();
  }
  
  public void zzcrh()
  {
    zzcri();
  }
  
  public void zzd(zzajq paramzzajq, zzajh paramzzajh)
  {
    zzcri();
  }
  
  public zzald zzf(zzall paramzzall)
  {
    return new zzald(zzamg.zza(zzame.zzczq(), paramzzall.zzcya()), false, false);
  }
  
  public <T> T zzf(Callable<T> paramCallable)
  {
    if (!this.aZS) {}
    for (boolean bool = true;; bool = false)
    {
      zzann.zzb(bool, "runInTransaction called when an existing transaction is already in progress.");
      this.aZS = true;
      try
      {
        paramCallable = paramCallable.call();
        return paramCallable;
      }
      catch (Throwable paramCallable)
      {
        throw new RuntimeException(paramCallable);
      }
      finally
      {
        this.aZS = false;
      }
    }
  }
  
  public void zzg(zzall paramzzall)
  {
    zzcri();
  }
  
  public void zzh(zzall paramzzall)
  {
    zzcri();
  }
  
  public void zzi(zzall paramzzall)
  {
    zzcri();
  }
  
  public void zzk(zzajq paramzzajq, zzaml paramzzaml)
  {
    zzcri();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzakt.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */