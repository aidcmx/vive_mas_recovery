package com.google.android.gms.internal;

import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;

public abstract interface zzaku
{
  public abstract void zza(zzajq paramzzajq, zzajh paramzzajh, long paramLong);
  
  public abstract void zza(zzajq paramzzajq, zzaml paramzzaml, long paramLong);
  
  public abstract void zza(zzall paramzzall, zzaml paramzzaml);
  
  public abstract void zza(zzall paramzzall, Set<zzalz> paramSet);
  
  public abstract void zza(zzall paramzzall, Set<zzalz> paramSet1, Set<zzalz> paramSet2);
  
  public abstract void zzby(long paramLong);
  
  public abstract void zzc(zzajq paramzzajq, zzajh paramzzajh);
  
  public abstract List<zzake> zzcre();
  
  public abstract void zzcrh();
  
  public abstract void zzd(zzajq paramzzajq, zzajh paramzzajh);
  
  public abstract zzald zzf(zzall paramzzall);
  
  public abstract <T> T zzf(Callable<T> paramCallable);
  
  public abstract void zzg(zzall paramzzall);
  
  public abstract void zzh(zzall paramzzall);
  
  public abstract void zzi(zzall paramzzall);
  
  public abstract void zzk(zzajq paramzzajq, zzaml paramzzaml);
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaku.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */