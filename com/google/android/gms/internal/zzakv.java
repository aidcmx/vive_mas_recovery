package com.google.android.gms.internal;

import java.util.List;
import java.util.Set;

public abstract interface zzakv
{
  public abstract void beginTransaction();
  
  public abstract void endTransaction();
  
  public abstract void setTransactionSuccessful();
  
  public abstract zzaml zza(zzajq paramzzajq);
  
  public abstract void zza(long paramLong, Set<zzalz> paramSet);
  
  public abstract void zza(long paramLong, Set<zzalz> paramSet1, Set<zzalz> paramSet2);
  
  public abstract void zza(zzajq paramzzajq, zzajh paramzzajh);
  
  public abstract void zza(zzajq paramzzajq, zzajh paramzzajh, long paramLong);
  
  public abstract void zza(zzajq paramzzajq, zzakw paramzzakw);
  
  public abstract void zza(zzajq paramzzajq, zzaml paramzzaml);
  
  public abstract void zza(zzajq paramzzajq, zzaml paramzzaml, long paramLong);
  
  public abstract void zza(zzakx paramzzakx);
  
  public abstract void zzb(zzajq paramzzajq, zzaml paramzzaml);
  
  public abstract void zzby(long paramLong);
  
  public abstract void zzbz(long paramLong);
  
  public abstract void zzca(long paramLong);
  
  public abstract Set<zzalz> zzcb(long paramLong);
  
  public abstract List<zzake> zzcre();
  
  public abstract long zzcrf();
  
  public abstract List<zzakx> zzcrg();
  
  public abstract void zzcrh();
  
  public abstract Set<zzalz> zzh(Set<Long> paramSet);
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzakv.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */