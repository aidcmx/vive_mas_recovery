package com.google.android.gms.internal;

public class zzakw
{
  private static final zzala<Boolean> bgF = new zzala()
  {
    public boolean zzf(Boolean paramAnonymousBoolean)
    {
      return !paramAnonymousBoolean.booleanValue();
    }
  };
  private static final zzala<Boolean> bgG = new zzala()
  {
    public boolean zzf(Boolean paramAnonymousBoolean)
    {
      return paramAnonymousBoolean.booleanValue();
    }
  };
  private static final zzakz<Boolean> bgH = new zzakz(Boolean.valueOf(true));
  private static final zzakz<Boolean> bgI = new zzakz(Boolean.valueOf(false));
  private final zzakz<Boolean> bgE;
  
  public zzakw()
  {
    this.bgE = zzakz.zzcxe();
  }
  
  private zzakw(zzakz<Boolean> paramzzakz)
  {
    this.bgE = paramzzakz;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    do
    {
      return true;
      if (!(paramObject instanceof zzakw)) {
        return false;
      }
      paramObject = (zzakw)paramObject;
    } while (this.bgE.equals(((zzakw)paramObject).bgE));
    return false;
  }
  
  public int hashCode()
  {
    return this.bgE.hashCode();
  }
  
  public String toString()
  {
    String str = String.valueOf(this.bgE.toString());
    return String.valueOf(str).length() + 14 + "{PruneForest:" + str + "}";
  }
  
  public <T> T zza(T paramT, final zzakz.zza<Void, T> paramzza)
  {
    (T)this.bgE.zzb(paramT, new zzakz.zza()
    {
      public T zza(zzajq paramAnonymouszzajq, Boolean paramAnonymousBoolean, T paramAnonymousT)
      {
        Object localObject = paramAnonymousT;
        if (!paramAnonymousBoolean.booleanValue()) {
          localObject = paramzza.zza(paramAnonymouszzajq, null, paramAnonymousT);
        }
        return (T)localObject;
      }
    });
  }
  
  public boolean zzcwz()
  {
    return this.bgE.zzb(bgG);
  }
  
  public zzakw zzd(zzalz paramzzalz)
  {
    paramzzalz = this.bgE.zze(paramzzalz);
    if (paramzzalz == null) {
      paramzzalz = new zzakz((Boolean)this.bgE.getValue());
    }
    for (;;)
    {
      return new zzakw(paramzzalz);
      if ((paramzzalz.getValue() == null) && (this.bgE.getValue() != null)) {
        paramzzalz = paramzzalz.zzb(zzajq.zzcvg(), (Boolean)this.bgE.getValue());
      }
    }
  }
  
  public boolean zzw(zzajq paramzzajq)
  {
    paramzzajq = (Boolean)this.bgE.zzah(paramzzajq);
    return (paramzzajq != null) && (paramzzajq.booleanValue());
  }
  
  public boolean zzx(zzajq paramzzajq)
  {
    paramzzajq = (Boolean)this.bgE.zzah(paramzzajq);
    return (paramzzajq != null) && (!paramzzajq.booleanValue());
  }
  
  public zzakw zzy(zzajq paramzzajq)
  {
    if (this.bgE.zzb(paramzzajq, bgF) != null) {
      throw new IllegalArgumentException("Can't prune path that was kept previously!");
    }
    if (this.bgE.zzb(paramzzajq, bgG) != null) {
      return this;
    }
    return new zzakw(this.bgE.zza(paramzzajq, bgH));
  }
  
  public zzakw zzz(zzajq paramzzajq)
  {
    if (this.bgE.zzb(paramzzajq, bgF) != null) {
      return this;
    }
    return new zzakw(this.bgE.zza(paramzzajq, bgI));
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzakw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */