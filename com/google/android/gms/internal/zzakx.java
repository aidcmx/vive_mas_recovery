package com.google.android.gms.internal;

public class zzakx
{
  public final zzall bgL;
  public final long bgM;
  public final boolean bgN;
  public final boolean bgO;
  public final long id;
  
  public zzakx(long paramLong1, zzall paramzzall, long paramLong2, boolean paramBoolean1, boolean paramBoolean2)
  {
    this.id = paramLong1;
    if ((paramzzall.zzcye()) && (!paramzzall.isDefault())) {
      throw new IllegalArgumentException("Can't create TrackedQuery for a non-default query that loads all data");
    }
    this.bgL = paramzzall;
    this.bgM = paramLong2;
    this.bgN = paramBoolean1;
    this.bgO = paramBoolean2;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    do
    {
      return true;
      if ((paramObject == null) || (paramObject.getClass() != getClass())) {
        return false;
      }
      paramObject = (zzakx)paramObject;
    } while ((this.id == ((zzakx)paramObject).id) && (this.bgL.equals(((zzakx)paramObject).bgL)) && (this.bgM == ((zzakx)paramObject).bgM) && (this.bgN == ((zzakx)paramObject).bgN) && (this.bgO == ((zzakx)paramObject).bgO));
    return false;
  }
  
  public int hashCode()
  {
    return (((Long.valueOf(this.id).hashCode() * 31 + this.bgL.hashCode()) * 31 + Long.valueOf(this.bgM).hashCode()) * 31 + Boolean.valueOf(this.bgN).hashCode()) * 31 + Boolean.valueOf(this.bgO).hashCode();
  }
  
  public String toString()
  {
    long l1 = this.id;
    String str = String.valueOf(this.bgL);
    long l2 = this.bgM;
    boolean bool1 = this.bgN;
    boolean bool2 = this.bgO;
    return String.valueOf(str).length() + 109 + "TrackedQuery{id=" + l1 + ", querySpec=" + str + ", lastUse=" + l2 + ", complete=" + bool1 + ", active=" + bool2 + "}";
  }
  
  public zzakx zzcm(long paramLong)
  {
    return new zzakx(this.id, this.bgL, paramLong, this.bgN, this.bgO);
  }
  
  public zzakx zzcxa()
  {
    return new zzakx(this.id, this.bgL, this.bgM, true, this.bgO);
  }
  
  public zzakx zzdc(boolean paramBoolean)
  {
    return new zzakx(this.id, this.bgL, this.bgM, this.bgN, paramBoolean);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzakx.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */