package com.google.android.gms.internal;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class zzaky
{
  private static final zzala<Map<zzalk, zzakx>> bgP;
  private static final zzala<Map<zzalk, zzakx>> bgQ;
  private static final zzala<zzakx> bgR;
  private static final zzala<zzakx> bgS;
  private final zzalw aZR;
  private zzakz<Map<zzalk, zzakx>> bgT;
  private final zzanf bgU;
  private long bgV = 0L;
  private final zzakv bgz;
  
  static
  {
    if (!zzaky.class.desiredAssertionStatus()) {}
    for (boolean bool = true;; bool = false)
    {
      $assertionsDisabled = bool;
      bgP = new zzala()
      {
        public boolean zzcc(Map<zzalk, zzakx> paramAnonymousMap)
        {
          paramAnonymousMap = (zzakx)paramAnonymousMap.get(zzalk.bhG);
          return (paramAnonymousMap != null) && (paramAnonymousMap.bgN);
        }
      };
      bgQ = new zzala()
      {
        public boolean zzcc(Map<zzalk, zzakx> paramAnonymousMap)
        {
          paramAnonymousMap = (zzakx)paramAnonymousMap.get(zzalk.bhG);
          return (paramAnonymousMap != null) && (paramAnonymousMap.bgO);
        }
      };
      bgR = new zzala()
      {
        public boolean zzc(zzakx paramAnonymouszzakx)
        {
          return !paramAnonymouszzakx.bgO;
        }
      };
      bgS = new zzala()
      {
        public boolean zzc(zzakx paramAnonymouszzakx)
        {
          return !zzaky.zzcxd().zzbs(paramAnonymouszzakx);
        }
      };
      return;
    }
  }
  
  public zzaky(zzakv paramzzakv, zzalw paramzzalw, zzanf paramzzanf)
  {
    this.bgz = paramzzakv;
    this.aZR = paramzzalw;
    this.bgU = paramzzanf;
    this.bgT = new zzakz(null);
    zzcxb();
    paramzzakv = this.bgz.zzcrg().iterator();
    while (paramzzakv.hasNext())
    {
      paramzzalw = (zzakx)paramzzakv.next();
      this.bgV = Math.max(paramzzalw.id + 1L, this.bgV);
      zzb(paramzzalw);
    }
  }
  
  private static long zza(zzakq paramzzakq, long paramLong)
  {
    return paramLong - Math.min(Math.floor((1.0F - paramzzakq.zzcww()) * (float)paramLong), paramzzakq.zzcwx());
  }
  
  private List<zzakx> zza(zzala<zzakx> paramzzala)
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator1 = this.bgT.iterator();
    while (localIterator1.hasNext())
    {
      Iterator localIterator2 = ((Map)((Map.Entry)localIterator1.next()).getValue()).values().iterator();
      while (localIterator2.hasNext())
      {
        zzakx localzzakx = (zzakx)localIterator2.next();
        if (paramzzala.zzbs(localzzakx)) {
          localArrayList.add(localzzakx);
        }
      }
    }
    return localArrayList;
  }
  
  private void zza(zzakx paramzzakx)
  {
    zzb(paramzzakx);
    this.bgz.zza(paramzzakx);
  }
  
  private boolean zzae(zzajq paramzzajq)
  {
    return this.bgT.zza(paramzzajq, bgP) != null;
  }
  
  private Set<Long> zzaf(zzajq paramzzajq)
  {
    HashSet localHashSet = new HashSet();
    paramzzajq = (Map)this.bgT.zzak(paramzzajq);
    if (paramzzajq != null)
    {
      paramzzajq = paramzzajq.values().iterator();
      while (paramzzajq.hasNext())
      {
        zzakx localzzakx = (zzakx)paramzzajq.next();
        if (!localzzakx.bgL.zzcye()) {
          localHashSet.add(Long.valueOf(localzzakx.id));
        }
      }
    }
    return localHashSet;
  }
  
  private void zzb(zzakx paramzzakx)
  {
    zzj(paramzzakx.bgL);
    Object localObject = (Map)this.bgT.zzak(paramzzakx.bgL.zzcrc());
    if (localObject == null)
    {
      localObject = new HashMap();
      this.bgT = this.bgT.zzb(paramzzakx.bgL.zzcrc(), localObject);
    }
    for (;;)
    {
      zzakx localzzakx = (zzakx)((Map)localObject).get(paramzzakx.bgL.zzcyh());
      if ((localzzakx == null) || (localzzakx.id == paramzzakx.id)) {}
      for (boolean bool = true;; bool = false)
      {
        zzann.zzcx(bool);
        ((Map)localObject).put(paramzzakx.bgL.zzcyh(), paramzzakx);
        return;
      }
    }
  }
  
  private void zzb(zzall paramzzall, boolean paramBoolean)
  {
    paramzzall = zzk(paramzzall);
    zzakx localzzakx = zzl(paramzzall);
    long l1 = this.bgU.z();
    if (localzzakx != null) {}
    long l2;
    for (paramzzall = localzzakx.zzcm(l1).zzdc(paramBoolean);; paramzzall = new zzakx(l2, paramzzall, l1, false, paramBoolean))
    {
      zza(paramzzall);
      return;
      assert (paramBoolean) : "If we're setting the query to inactive, we should already be tracking it!";
      l2 = this.bgV;
      this.bgV = (1L + l2);
    }
  }
  
  private void zzcxb()
  {
    try
    {
      this.bgz.beginTransaction();
      this.bgz.zzca(this.bgU.z());
      this.bgz.setTransactionSuccessful();
      return;
    }
    finally
    {
      this.bgz.endTransaction();
    }
  }
  
  private static void zzj(zzall paramzzall)
  {
    if ((!paramzzall.zzcye()) || (paramzzall.isDefault())) {}
    for (boolean bool = true;; bool = false)
    {
      zzann.zzb(bool, "Can't have tracked non-default query that loads all data");
      return;
    }
  }
  
  private static zzall zzk(zzall paramzzall)
  {
    zzall localzzall = paramzzall;
    if (paramzzall.zzcye()) {
      localzzall = zzall.zzan(paramzzall.zzcrc());
    }
    return localzzall;
  }
  
  public zzakw zza(zzakq paramzzakq)
  {
    Object localObject1 = zza(bgR);
    long l = zza(paramzzakq, ((List)localObject1).size());
    paramzzakq = new zzakw();
    Object localObject2;
    if (this.aZR.zzcyu())
    {
      localObject2 = this.aZR;
      i = ((List)localObject1).size();
      ((zzalw)localObject2).zzi(80 + "Pruning old queries.  Prunable: " + i + " Count to prune: " + l, new Object[0]);
    }
    Collections.sort((List)localObject1, new Comparator()
    {
      public int zza(zzakx paramAnonymouszzakx1, zzakx paramAnonymouszzakx2)
      {
        return zzann.zzj(paramAnonymouszzakx1.bgM, paramAnonymouszzakx2.bgM);
      }
    });
    int i = 0;
    while (i < l)
    {
      localObject2 = (zzakx)((List)localObject1).get(i);
      paramzzakq = paramzzakq.zzy(((zzakx)localObject2).bgL.zzcrc());
      zzm(((zzakx)localObject2).bgL);
      i += 1;
    }
    i = (int)l;
    while (i < ((List)localObject1).size())
    {
      paramzzakq = paramzzakq.zzz(((zzakx)((List)localObject1).get(i)).bgL.zzcrc());
      i += 1;
    }
    localObject1 = zza(bgS);
    if (this.aZR.zzcyu())
    {
      localObject2 = this.aZR;
      i = ((List)localObject1).size();
      ((zzalw)localObject2).zzi(31 + "Unprunable queries: " + i, new Object[0]);
    }
    localObject1 = ((List)localObject1).iterator();
    while (((Iterator)localObject1).hasNext()) {
      paramzzakq = paramzzakq.zzz(((zzakx)((Iterator)localObject1).next()).bgL.zzcrc());
    }
    return paramzzakq;
  }
  
  public void zzaa(zzajq paramzzajq)
  {
    this.bgT.zzai(paramzzajq).zza(new zzakz.zza()
    {
      public Void zza(zzajq paramAnonymouszzajq, Map<zzalk, zzakx> paramAnonymousMap, Void paramAnonymousVoid)
      {
        paramAnonymouszzajq = paramAnonymousMap.entrySet().iterator();
        while (paramAnonymouszzajq.hasNext())
        {
          paramAnonymousMap = (zzakx)((Map.Entry)paramAnonymouszzajq.next()).getValue();
          if (!paramAnonymousMap.bgN) {
            zzaky.zza(zzaky.this, paramAnonymousMap.zzcxa());
          }
        }
        return null;
      }
    });
  }
  
  public Set<zzalz> zzab(zzajq paramzzajq)
  {
    assert (!zzo(zzall.zzan(paramzzajq))) : "Path is fully complete.";
    HashSet localHashSet = new HashSet();
    Object localObject1 = zzaf(paramzzajq);
    if (!((Set)localObject1).isEmpty()) {
      localHashSet.addAll(this.bgz.zzh((Set)localObject1));
    }
    paramzzajq = this.bgT.zzai(paramzzajq).zzcxf().iterator();
    while (paramzzajq.hasNext())
    {
      Object localObject2 = (Map.Entry)paramzzajq.next();
      localObject1 = (zzalz)((Map.Entry)localObject2).getKey();
      localObject2 = (zzakz)((Map.Entry)localObject2).getValue();
      if ((((zzakz)localObject2).getValue() != null) && (bgP.zzbs((Map)((zzakz)localObject2).getValue()))) {
        localHashSet.add(localObject1);
      }
    }
    return localHashSet;
  }
  
  public void zzac(zzajq paramzzajq)
  {
    zzakx localzzakx;
    long l;
    if (!zzae(paramzzajq))
    {
      paramzzajq = zzall.zzan(paramzzajq);
      localzzakx = zzl(paramzzajq);
      if (localzzakx != null) {
        break label64;
      }
      l = this.bgV;
      this.bgV = (1L + l);
    }
    for (paramzzajq = new zzakx(l, paramzzajq, this.bgU.z(), true, false);; paramzzajq = localzzakx.zzcxa())
    {
      zza(paramzzajq);
      return;
      label64:
      assert (!localzzakx.bgN) : "This should have been handled above!";
    }
  }
  
  public boolean zzad(zzajq paramzzajq)
  {
    return this.bgT.zzb(paramzzajq, bgQ) != null;
  }
  
  public long zzcxc()
  {
    return zza(bgR).size();
  }
  
  public void zzg(zzall paramzzall)
  {
    zzb(paramzzall, true);
  }
  
  public void zzh(zzall paramzzall)
  {
    zzb(paramzzall, false);
  }
  
  public zzakx zzl(zzall paramzzall)
  {
    paramzzall = zzk(paramzzall);
    Map localMap = (Map)this.bgT.zzak(paramzzall.zzcrc());
    if (localMap != null) {
      return (zzakx)localMap.get(paramzzall.zzcyh());
    }
    return null;
  }
  
  public void zzm(zzall paramzzall)
  {
    paramzzall = zzk(paramzzall);
    Object localObject = zzl(paramzzall);
    assert (localObject != null) : "Query must exist to be removed.";
    this.bgz.zzbz(((zzakx)localObject).id);
    localObject = (Map)this.bgT.zzak(paramzzall.zzcrc());
    ((Map)localObject).remove(paramzzall.zzcyh());
    if (((Map)localObject).isEmpty()) {
      this.bgT = this.bgT.zzaj(paramzzall.zzcrc());
    }
  }
  
  public void zzn(zzall paramzzall)
  {
    paramzzall = zzl(zzk(paramzzall));
    if ((paramzzall != null) && (!paramzzall.bgN)) {
      zza(paramzzall.zzcxa());
    }
  }
  
  public boolean zzo(zzall paramzzall)
  {
    if (zzae(paramzzall.zzcrc())) {
      return true;
    }
    if (paramzzall.zzcye()) {
      return false;
    }
    Map localMap = (Map)this.bgT.zzak(paramzzall.zzcrc());
    if ((localMap != null) && (localMap.containsKey(paramzzall.zzcyh())) && (((zzakx)localMap.get(paramzzall.zzcyh())).bgN)) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaky.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */