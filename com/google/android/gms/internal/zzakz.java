package com.google.android.gms.internal;

import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

public class zzakz<T>
  implements Iterable<Map.Entry<zzajq, T>>
{
  private static final zzaih bgY = zzaih.zza.zza(zzaiq.zzi(zzalz.class));
  private static final zzakz bgZ = new zzakz(null, bgY);
  private final zzaih<zzalz, zzakz<T>> bgX;
  private final T value;
  
  public zzakz(T paramT)
  {
    this(paramT, bgY);
  }
  
  public zzakz(T paramT, zzaih<zzalz, zzakz<T>> paramzzaih)
  {
    this.value = paramT;
    this.bgX = paramzzaih;
  }
  
  private <R> R zza(zzajq paramzzajq, zza<? super T, R> paramzza, R paramR)
  {
    Object localObject = this.bgX.iterator();
    while (((Iterator)localObject).hasNext())
    {
      Map.Entry localEntry = (Map.Entry)((Iterator)localObject).next();
      paramR = ((zzakz)localEntry.getValue()).zza(paramzzajq.zza((zzalz)localEntry.getKey()), paramzza, paramR);
    }
    localObject = paramR;
    if (this.value != null) {
      localObject = paramzza.zza(paramzzajq, this.value, paramR);
    }
    return (R)localObject;
  }
  
  public static <V> zzakz<V> zzcxe()
  {
    return bgZ;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    do
    {
      return true;
      if ((paramObject == null) || (getClass() != paramObject.getClass())) {
        return false;
      }
      paramObject = (zzakz)paramObject;
      if (this.bgX != null)
      {
        if (this.bgX.equals(((zzakz)paramObject).bgX)) {}
      }
      else {
        while (((zzakz)paramObject).bgX != null) {
          return false;
        }
      }
      if (this.value == null) {
        break;
      }
    } while (this.value.equals(((zzakz)paramObject).value));
    for (;;)
    {
      return false;
      if (((zzakz)paramObject).value == null) {
        break;
      }
    }
  }
  
  public T getValue()
  {
    return (T)this.value;
  }
  
  public int hashCode()
  {
    int j = 0;
    if (this.value != null) {}
    for (int i = this.value.hashCode();; i = 0)
    {
      if (this.bgX != null) {
        j = this.bgX.hashCode();
      }
      return i * 31 + j;
    }
  }
  
  public boolean isEmpty()
  {
    return (this.value == null) && (this.bgX.isEmpty());
  }
  
  public Iterator<Map.Entry<zzajq, T>> iterator()
  {
    final ArrayList localArrayList = new ArrayList();
    zza(new zza()
    {
      public Void zza(zzajq paramAnonymouszzajq, T paramAnonymousT, Void paramAnonymousVoid)
      {
        localArrayList.add(new AbstractMap.SimpleImmutableEntry(paramAnonymouszzajq, paramAnonymousT));
        return null;
      }
    });
    return localArrayList.iterator();
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("ImmutableTree { value=");
    localStringBuilder.append(getValue());
    localStringBuilder.append(", children={");
    Iterator localIterator = this.bgX.iterator();
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      localStringBuilder.append(((zzalz)localEntry.getKey()).asString());
      localStringBuilder.append("=");
      localStringBuilder.append(localEntry.getValue());
    }
    localStringBuilder.append("} }");
    return localStringBuilder.toString();
  }
  
  public Collection<T> values()
  {
    final ArrayList localArrayList = new ArrayList();
    zza(new zza()
    {
      public Void zza(zzajq paramAnonymouszzajq, T paramAnonymousT, Void paramAnonymousVoid)
      {
        localArrayList.add(paramAnonymousT);
        return null;
      }
    });
    return localArrayList;
  }
  
  public zzajq zza(zzajq paramzzajq, zzala<? super T> paramzzala)
  {
    if ((this.value != null) && (paramzzala.zzbs(this.value))) {
      return zzajq.zzcvg();
    }
    if (paramzzajq.isEmpty()) {
      return null;
    }
    zzalz localzzalz = paramzzajq.zzcvj();
    zzakz localzzakz = (zzakz)this.bgX.get(localzzalz);
    if (localzzakz != null)
    {
      paramzzajq = localzzakz.zza(paramzzajq.zzcvk(), paramzzala);
      if (paramzzajq != null) {
        return new zzajq(new zzalz[] { localzzalz }).zzh(paramzzajq);
      }
      return null;
    }
    return null;
  }
  
  public zzakz<T> zza(zzajq paramzzajq, zzakz<T> paramzzakz)
  {
    if (paramzzajq.isEmpty()) {
      return paramzzakz;
    }
    zzalz localzzalz = paramzzajq.zzcvj();
    zzakz localzzakz2 = (zzakz)this.bgX.get(localzzalz);
    zzakz localzzakz1 = localzzakz2;
    if (localzzakz2 == null) {
      localzzakz1 = zzcxe();
    }
    paramzzajq = localzzakz1.zza(paramzzajq.zzcvk(), paramzzakz);
    if (paramzzajq.isEmpty()) {}
    for (paramzzajq = this.bgX.zzbg(localzzalz);; paramzzajq = this.bgX.zzj(localzzalz, paramzzajq)) {
      return new zzakz(this.value, paramzzajq);
    }
  }
  
  public void zza(zza<T, Void> paramzza)
  {
    zza(zzajq.zzcvg(), paramzza, null);
  }
  
  public zzajq zzag(zzajq paramzzajq)
  {
    return zza(paramzzajq, zzala.bhd);
  }
  
  public T zzah(zzajq paramzzajq)
  {
    return (T)zzc(paramzzajq, zzala.bhd);
  }
  
  public zzakz<T> zzai(zzajq paramzzajq)
  {
    if (paramzzajq.isEmpty()) {
      return this;
    }
    Object localObject = paramzzajq.zzcvj();
    localObject = (zzakz)this.bgX.get(localObject);
    if (localObject != null) {
      return ((zzakz)localObject).zzai(paramzzajq.zzcvk());
    }
    return zzcxe();
  }
  
  public zzakz<T> zzaj(zzajq paramzzajq)
  {
    zzakz localzzakz1;
    if (paramzzajq.isEmpty()) {
      if (this.bgX.isEmpty()) {
        localzzakz1 = zzcxe();
      }
    }
    zzalz localzzalz;
    zzakz localzzakz2;
    do
    {
      return localzzakz1;
      return new zzakz(null, this.bgX);
      localzzalz = paramzzajq.zzcvj();
      localzzakz2 = (zzakz)this.bgX.get(localzzalz);
      localzzakz1 = this;
    } while (localzzakz2 == null);
    paramzzajq = localzzakz2.zzaj(paramzzajq.zzcvk());
    if (paramzzajq.isEmpty()) {}
    for (paramzzajq = this.bgX.zzbg(localzzalz); (this.value == null) && (paramzzajq.isEmpty()); paramzzajq = this.bgX.zzj(localzzalz, paramzzajq)) {
      return zzcxe();
    }
    return new zzakz(this.value, paramzzajq);
  }
  
  public T zzak(zzajq paramzzajq)
  {
    if (paramzzajq.isEmpty()) {
      return (T)this.value;
    }
    Object localObject = paramzzajq.zzcvj();
    localObject = (zzakz)this.bgX.get(localObject);
    if (localObject != null) {
      return (T)((zzakz)localObject).zzak(paramzzajq.zzcvk());
    }
    return null;
  }
  
  public zzakz<T> zzb(zzajq paramzzajq, T paramT)
  {
    if (paramzzajq.isEmpty()) {
      return new zzakz(paramT, this.bgX);
    }
    zzalz localzzalz = paramzzajq.zzcvj();
    zzakz localzzakz2 = (zzakz)this.bgX.get(localzzalz);
    zzakz localzzakz1 = localzzakz2;
    if (localzzakz2 == null) {
      localzzakz1 = zzcxe();
    }
    paramzzajq = localzzakz1.zzb(paramzzajq.zzcvk(), paramT);
    paramzzajq = this.bgX.zzj(localzzalz, paramzzajq);
    return new zzakz(this.value, paramzzajq);
  }
  
  public T zzb(zzajq paramzzajq, zzala<? super T> paramzzala)
  {
    if ((this.value != null) && (paramzzala.zzbs(this.value))) {
      return (T)this.value;
    }
    Iterator localIterator = paramzzajq.iterator();
    paramzzajq = this;
    while (localIterator.hasNext())
    {
      zzalz localzzalz = (zzalz)localIterator.next();
      paramzzajq = (zzakz)paramzzajq.bgX.get(localzzalz);
      if (paramzzajq == null) {
        return null;
      }
      if ((paramzzajq.value != null) && (paramzzala.zzbs(paramzzajq.value))) {
        return (T)paramzzajq.value;
      }
    }
    return null;
  }
  
  public <R> R zzb(R paramR, zza<? super T, R> paramzza)
  {
    return (R)zza(zzajq.zzcvg(), paramzza, paramR);
  }
  
  public boolean zzb(zzala<? super T> paramzzala)
  {
    if ((this.value != null) && (paramzzala.zzbs(this.value))) {
      return true;
    }
    Iterator localIterator = this.bgX.iterator();
    while (localIterator.hasNext()) {
      if (((zzakz)((Map.Entry)localIterator.next()).getValue()).zzb(paramzzala)) {
        return true;
      }
    }
    return false;
  }
  
  public T zzc(zzajq paramzzajq, zzala<? super T> paramzzala)
  {
    Iterator localIterator;
    if ((this.value != null) && (paramzzala.zzbs(this.value)))
    {
      localObject1 = this.value;
      localIterator = paramzzajq.iterator();
      paramzzajq = (zzajq)localObject1;
    }
    Object localObject2;
    for (Object localObject1 = this;; localObject1 = localObject2)
    {
      if (localIterator.hasNext())
      {
        localObject2 = (zzalz)localIterator.next();
        localObject2 = (zzakz)((zzakz)localObject1).bgX.get(localObject2);
        if (localObject2 != null) {}
      }
      else
      {
        return paramzzajq;
        localObject1 = null;
        break;
      }
      localObject1 = paramzzajq;
      if (((zzakz)localObject2).value != null)
      {
        localObject1 = paramzzajq;
        if (paramzzala.zzbs(((zzakz)localObject2).value)) {
          localObject1 = ((zzakz)localObject2).value;
        }
      }
      paramzzajq = (zzajq)localObject1;
    }
  }
  
  public zzaih<zzalz, zzakz<T>> zzcxf()
  {
    return this.bgX;
  }
  
  public zzakz<T> zze(zzalz paramzzalz)
  {
    paramzzalz = (zzakz)this.bgX.get(paramzzalz);
    if (paramzzalz != null) {
      return paramzzalz;
    }
    return zzcxe();
  }
  
  public static abstract interface zza<T, R>
  {
    public abstract R zza(zzajq paramzzajq, T paramT, R paramR);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzakz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */