package com.google.android.gms.internal;

public abstract interface zzala<T>
{
  public static final zzala<Object> bhd = new zzala()
  {
    public boolean zzbs(Object paramAnonymousObject)
    {
      return true;
    }
  };
  
  public abstract boolean zzbs(T paramT);
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzala.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */