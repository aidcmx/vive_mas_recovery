package com.google.android.gms.internal;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class zzalb<T>
{
  private zzalz bhe;
  private zzalb<T> bhf;
  private zzalc<T> bhg;
  
  static
  {
    if (!zzalb.class.desiredAssertionStatus()) {}
    for (boolean bool = true;; bool = false)
    {
      $assertionsDisabled = bool;
      return;
    }
  }
  
  public zzalb()
  {
    this(null, null, new zzalc());
  }
  
  public zzalb(zzalz paramzzalz, zzalb<T> paramzzalb, zzalc<T> paramzzalc)
  {
    this.bhe = paramzzalz;
    this.bhf = paramzzalb;
    this.bhg = paramzzalc;
  }
  
  private void zza(zzalz paramzzalz, zzalb<T> paramzzalb)
  {
    boolean bool1 = paramzzalb.isEmpty();
    boolean bool2 = this.bhg.bfa.containsKey(paramzzalz);
    if ((bool1) && (bool2))
    {
      this.bhg.bfa.remove(paramzzalz);
      zzcxg();
    }
    while ((bool1) || (bool2)) {
      return;
    }
    this.bhg.bfa.put(paramzzalz, paramzzalb.bhg);
    zzcxg();
  }
  
  private void zzcxg()
  {
    if (this.bhf != null) {
      this.bhf.zza(this.bhe, this);
    }
  }
  
  public T getValue()
  {
    return (T)this.bhg.value;
  }
  
  public boolean hasChildren()
  {
    return !this.bhg.bfa.isEmpty();
  }
  
  public boolean isEmpty()
  {
    return (this.bhg.value == null) && (this.bhg.bfa.isEmpty());
  }
  
  public void setValue(T paramT)
  {
    this.bhg.value = paramT;
    zzcxg();
  }
  
  public String toString()
  {
    return toString("");
  }
  
  String toString(String paramString)
  {
    if (this.bhe == null) {}
    for (String str1 = "<anon>";; str1 = this.bhe.asString())
    {
      String str2 = String.valueOf(this.bhg.toString(String.valueOf(paramString).concat("\t")));
      return String.valueOf(paramString).length() + 1 + String.valueOf(str1).length() + String.valueOf(str2).length() + paramString + str1 + "\n" + str2;
    }
  }
  
  public void zza(zzb<T> paramzzb)
  {
    zza(paramzzb, false, false);
  }
  
  public void zza(final zzb<T> paramzzb, boolean paramBoolean1, final boolean paramBoolean2)
  {
    if ((paramBoolean1) && (!paramBoolean2)) {
      paramzzb.zzd(this);
    }
    zzb(new zzb()
    {
      public void zzd(zzalb<T> paramAnonymouszzalb)
      {
        paramAnonymouszzalb.zza(paramzzb, true, paramBoolean2);
      }
    });
    if ((paramBoolean1) && (paramBoolean2)) {
      paramzzb.zzd(this);
    }
  }
  
  public boolean zza(zza<T> paramzza)
  {
    return zza(paramzza, false);
  }
  
  public boolean zza(zza<T> paramzza, boolean paramBoolean)
  {
    zzalb localzzalb;
    if (paramBoolean) {
      localzzalb = this;
    }
    while (localzzalb != null)
    {
      paramzza.zze(localzzalb);
      localzzalb = localzzalb.bhf;
      continue;
      localzzalb = this.bhf;
    }
    return false;
  }
  
  public zzalb<T> zzal(zzajq paramzzajq)
  {
    Object localObject = paramzzajq.zzcvj();
    zzajq localzzajq = paramzzajq;
    zzalb localzzalb = this;
    paramzzajq = (zzajq)localObject;
    if (paramzzajq != null)
    {
      if (localzzalb.bhg.bfa.containsKey(paramzzajq)) {}
      for (localObject = (zzalc)localzzalb.bhg.bfa.get(paramzzajq);; localObject = new zzalc())
      {
        localzzalb = new zzalb(paramzzajq, localzzalb, (zzalc)localObject);
        localzzajq = localzzajq.zzcvk();
        paramzzajq = localzzajq.zzcvj();
        break;
      }
    }
    return localzzalb;
  }
  
  public void zzb(zzb<T> paramzzb)
  {
    Object[] arrayOfObject = this.bhg.bfa.entrySet().toArray();
    int i = 0;
    while (i < arrayOfObject.length)
    {
      Map.Entry localEntry = (Map.Entry)arrayOfObject[i];
      paramzzb.zzd(new zzalb((zzalz)localEntry.getKey(), this, (zzalc)localEntry.getValue()));
      i += 1;
    }
  }
  
  public zzajq zzcrc()
  {
    if (this.bhf != null)
    {
      assert (this.bhe != null);
      return this.bhf.zzcrc().zza(this.bhe);
    }
    if (this.bhe != null) {
      return new zzajq(new zzalz[] { this.bhe });
    }
    return zzajq.zzcvg();
  }
  
  public static abstract interface zza<T>
  {
    public abstract boolean zze(zzalb<T> paramzzalb);
  }
  
  public static abstract interface zzb<T>
  {
    public abstract void zzd(zzalb<T> paramzzalb);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzalb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */