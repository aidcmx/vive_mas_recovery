package com.google.android.gms.internal;

public class zzald
{
  private final zzamg bhk;
  private final boolean bhl;
  private final boolean bhm;
  
  public zzald(zzamg paramzzamg, boolean paramBoolean1, boolean paramBoolean2)
  {
    this.bhk = paramzzamg;
    this.bhl = paramBoolean1;
    this.bhm = paramBoolean2;
  }
  
  public boolean zzam(zzajq paramzzajq)
  {
    if (paramzzajq.isEmpty()) {
      return (zzcxh()) && (!this.bhm);
    }
    return zzf(paramzzajq.zzcvj());
  }
  
  public zzaml zzcqy()
  {
    return this.bhk.zzcqy();
  }
  
  public boolean zzcxh()
  {
    return this.bhl;
  }
  
  public boolean zzcxi()
  {
    return this.bhm;
  }
  
  public zzamg zzcxj()
  {
    return this.bhk;
  }
  
  public boolean zzf(zzalz paramzzalz)
  {
    return ((zzcxh()) && (!this.bhm)) || (this.bhk.zzcqy().zzk(paramzzalz));
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzald.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */