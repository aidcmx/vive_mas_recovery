package com.google.android.gms.internal;

import com.google.firebase.database.DatabaseError;

public class zzale
  implements zzalh
{
  private final zzajq aZr;
  private final zzajl bhn;
  private final DatabaseError bho;
  
  public zzale(zzajl paramzzajl, DatabaseError paramDatabaseError, zzajq paramzzajq)
  {
    this.bhn = paramzzajl;
    this.aZr = paramzzajq;
    this.bho = paramDatabaseError;
  }
  
  public String toString()
  {
    String str = String.valueOf(zzcrc());
    return String.valueOf(str).length() + 7 + str + ":CANCEL";
  }
  
  public zzajq zzcrc()
  {
    return this.aZr;
  }
  
  public void zzcxk()
  {
    this.bhn.zza(this.bho);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzale.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */