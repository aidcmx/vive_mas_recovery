package com.google.android.gms.internal;

public class zzalf
{
  private final zzamg bhk;
  private final zzalh.zza bhp;
  private final zzamg bhq;
  private final zzalz bhr;
  private final zzalz bhs;
  
  private zzalf(zzalh.zza paramzza, zzamg paramzzamg1, zzalz paramzzalz1, zzalz paramzzalz2, zzamg paramzzamg2)
  {
    this.bhp = paramzza;
    this.bhk = paramzzamg1;
    this.bhr = paramzzalz1;
    this.bhs = paramzzalz2;
    this.bhq = paramzzamg2;
  }
  
  public static zzalf zza(zzalz paramzzalz, zzamg paramzzamg)
  {
    return new zzalf(zzalh.zza.bhw, paramzzamg, paramzzalz, null, null);
  }
  
  public static zzalf zza(zzalz paramzzalz, zzamg paramzzamg1, zzamg paramzzamg2)
  {
    return new zzalf(zzalh.zza.bhy, paramzzamg1, paramzzalz, null, paramzzamg2);
  }
  
  public static zzalf zza(zzalz paramzzalz, zzaml paramzzaml1, zzaml paramzzaml2)
  {
    return zza(paramzzalz, zzamg.zzn(paramzzaml1), zzamg.zzn(paramzzaml2));
  }
  
  public static zzalf zza(zzamg paramzzamg)
  {
    return new zzalf(zzalh.zza.bhz, paramzzamg, null, null, null);
  }
  
  public static zzalf zzb(zzalz paramzzalz, zzamg paramzzamg)
  {
    return new zzalf(zzalh.zza.bhv, paramzzamg, paramzzalz, null, null);
  }
  
  public static zzalf zzc(zzalz paramzzalz, zzamg paramzzamg)
  {
    return new zzalf(zzalh.zza.bhx, paramzzamg, paramzzalz, null, null);
  }
  
  public static zzalf zzc(zzalz paramzzalz, zzaml paramzzaml)
  {
    return zza(paramzzalz, zzamg.zzn(paramzzaml));
  }
  
  public static zzalf zzd(zzalz paramzzalz, zzaml paramzzaml)
  {
    return zzb(paramzzalz, zzamg.zzn(paramzzaml));
  }
  
  public String toString()
  {
    String str1 = String.valueOf(this.bhp);
    String str2 = String.valueOf(this.bhr);
    return String.valueOf(str1).length() + 9 + String.valueOf(str2).length() + "Change: " + str1 + " " + str2;
  }
  
  public zzamg zzcxj()
  {
    return this.bhk;
  }
  
  public zzalz zzcxl()
  {
    return this.bhr;
  }
  
  public zzalh.zza zzcxm()
  {
    return this.bhp;
  }
  
  public zzalz zzcxn()
  {
    return this.bhs;
  }
  
  public zzamg zzcxo()
  {
    return this.bhq;
  }
  
  public zzalf zzg(zzalz paramzzalz)
  {
    return new zzalf(this.bhp, this.bhk, this.bhr, paramzzalz, this.bhq);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzalf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */