package com.google.android.gms.internal;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;

public class zzalg
  implements zzalh
{
  private final zzajl bhn;
  private final zzalh.zza bhp;
  private final DataSnapshot bht;
  private final String bhu;
  
  public zzalg(zzalh.zza paramzza, zzajl paramzzajl, DataSnapshot paramDataSnapshot, String paramString)
  {
    this.bhp = paramzza;
    this.bhn = paramzzajl;
    this.bht = paramDataSnapshot;
    this.bhu = paramString;
  }
  
  public String toString()
  {
    if (this.bhp == zzalh.zza.bhz)
    {
      str1 = String.valueOf(zzcrc());
      str2 = String.valueOf(this.bhp);
      str3 = String.valueOf(this.bht.getValue(true));
      return String.valueOf(str1).length() + 4 + String.valueOf(str2).length() + String.valueOf(str3).length() + str1 + ": " + str2 + ": " + str3;
    }
    String str1 = String.valueOf(zzcrc());
    String str2 = String.valueOf(this.bhp);
    String str3 = String.valueOf(this.bht.getKey());
    String str4 = String.valueOf(this.bht.getValue(true));
    return String.valueOf(str1).length() + 10 + String.valueOf(str2).length() + String.valueOf(str3).length() + String.valueOf(str4).length() + str1 + ": " + str2 + ": { " + str3 + ": " + str4 + " }";
  }
  
  public zzajq zzcrc()
  {
    zzajq localzzajq = this.bht.getRef().zzcrc();
    if (this.bhp == zzalh.zza.bhz) {
      return localzzajq;
    }
    return localzzajq.zzcvl();
  }
  
  public void zzcxk()
  {
    this.bhn.zza(this);
  }
  
  public zzalh.zza zzcxm()
  {
    return this.bhp;
  }
  
  public DataSnapshot zzcxp()
  {
    return this.bht;
  }
  
  public String zzcxq()
  {
    return this.bhu;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzalg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */