package com.google.android.gms.internal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class zzali
{
  private final zzall bhB;
  private final zzamf bhC;
  
  public zzali(zzall paramzzall)
  {
    this.bhB = paramzzall;
    this.bhC = paramzzall.zzcya();
  }
  
  private zzalg zza(zzalf paramzzalf, zzajl paramzzajl, zzamg paramzzamg)
  {
    zzalf localzzalf = paramzzalf;
    if (!paramzzalf.zzcxm().equals(zzalh.zza.bhz)) {
      if (!paramzzalf.zzcxm().equals(zzalh.zza.bhv)) {
        break label43;
      }
    }
    label43:
    for (localzzalf = paramzzalf;; localzzalf = paramzzalf.zzg(paramzzamg.zza(paramzzalf.zzcxl(), paramzzalf.zzcxj().zzcqy(), this.bhC))) {
      return paramzzajl.zza(localzzalf, this.bhB);
    }
  }
  
  private void zza(List<zzalg> paramList, zzalh.zza paramzza, List<zzalf> paramList1, List<zzajl> paramList2, zzamg paramzzamg)
  {
    Object localObject1 = new ArrayList();
    paramList1 = paramList1.iterator();
    Object localObject2;
    while (paramList1.hasNext())
    {
      localObject2 = (zzalf)paramList1.next();
      if (((zzalf)localObject2).zzcxm().equals(paramzza)) {
        ((List)localObject1).add(localObject2);
      }
    }
    Collections.sort((List)localObject1, zzcxr());
    paramList1 = ((List)localObject1).iterator();
    while (paramList1.hasNext())
    {
      localObject1 = (zzalf)paramList1.next();
      localObject2 = paramList2.iterator();
      while (((Iterator)localObject2).hasNext())
      {
        zzajl localzzajl = (zzajl)((Iterator)localObject2).next();
        if (localzzajl.zza(paramzza)) {
          paramList.add(zza((zzalf)localObject1, localzzajl, paramzzamg));
        }
      }
    }
  }
  
  private Comparator<zzalf> zzcxr()
  {
    new Comparator()
    {
      static
      {
        if (!zzali.class.desiredAssertionStatus()) {}
        for (boolean bool = true;; bool = false)
        {
          $assertionsDisabled = bool;
          return;
        }
      }
      
      public int zza(zzalf paramAnonymouszzalf1, zzalf paramAnonymouszzalf2)
      {
        assert ((paramAnonymouszzalf1.zzcxl() != null) && (paramAnonymouszzalf2.zzcxl() != null));
        paramAnonymouszzalf1 = new zzamk(paramAnonymouszzalf1.zzcxl(), paramAnonymouszzalf1.zzcxj().zzcqy());
        paramAnonymouszzalf2 = new zzamk(paramAnonymouszzalf2.zzcxl(), paramAnonymouszzalf2.zzcxj().zzcqy());
        return zzali.zza(zzali.this).compare(paramAnonymouszzalf1, paramAnonymouszzalf2);
      }
    };
  }
  
  public List<zzalg> zza(List<zzalf> paramList, zzamg paramzzamg, List<zzajl> paramList1)
  {
    ArrayList localArrayList1 = new ArrayList();
    ArrayList localArrayList2 = new ArrayList();
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
    {
      zzalf localzzalf = (zzalf)localIterator.next();
      if ((localzzalf.zzcxm().equals(zzalh.zza.bhy)) && (this.bhC.zza(localzzalf.zzcxo().zzcqy(), localzzalf.zzcxj().zzcqy()))) {
        localArrayList2.add(zzalf.zzc(localzzalf.zzcxl(), localzzalf.zzcxj()));
      }
    }
    zza(localArrayList1, zzalh.zza.bhv, paramList, paramList1, paramzzamg);
    zza(localArrayList1, zzalh.zza.bhw, paramList, paramList1, paramzzamg);
    zza(localArrayList1, zzalh.zza.bhx, localArrayList2, paramList1, paramzzamg);
    zza(localArrayList1, zzalh.zza.bhy, paramList, paramList1, paramzzamg);
    zza(localArrayList1, zzalh.zza.bhz, paramList, paramList1, paramzzamg);
    return localArrayList1;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzali.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */