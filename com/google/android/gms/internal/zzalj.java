package com.google.android.gms.internal;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class zzalj
{
  private final zzalw aZR;
  private final zzajn bdk;
  
  public zzalj(zzajj paramzzajj)
  {
    this.bdk = paramzzajj.zzcur();
    this.aZR = paramzzajj.zzss("EventRaiser");
  }
  
  public void zzax(final List<? extends zzalh> paramList)
  {
    if (this.aZR.zzcyu())
    {
      zzalw localzzalw = this.aZR;
      int i = paramList.size();
      localzzalw.zzi(28 + "Raising " + i + " event(s)", new Object[0]);
    }
    paramList = new ArrayList(paramList);
    this.bdk.zzq(new Runnable()
    {
      public void run()
      {
        Iterator localIterator = paramList.iterator();
        if (localIterator.hasNext())
        {
          zzalh localzzalh = (zzalh)localIterator.next();
          zzalw localzzalw;
          if (zzalj.zza(zzalj.this).zzcyu())
          {
            localzzalw = zzalj.zza(zzalj.this);
            str = String.valueOf(localzzalh.toString());
            if (str.length() == 0) {
              break label92;
            }
          }
          label92:
          for (String str = "Raising ".concat(str);; str = new String("Raising "))
          {
            localzzalw.zzi(str, new Object[0]);
            localzzalh.zzcxk();
            break;
          }
        }
      }
    });
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzalj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */