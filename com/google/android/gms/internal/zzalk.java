package com.google.android.gms.internal;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class zzalk
{
  public static final zzalk bhG;
  private zzamf bhC = zzamo.b();
  private Integer bhH;
  private zza bhI;
  private zzaml bhJ = null;
  private zzalz bhK = null;
  private zzaml bhL = null;
  private zzalz bhM = null;
  private String bhN = null;
  
  static
  {
    if (!zzalk.class.desiredAssertionStatus()) {}
    for (boolean bool = true;; bool = false)
    {
      $assertionsDisabled = bool;
      bhG = new zzalk();
      return;
    }
  }
  
  public static zzalk zzcd(Map<String, Object> paramMap)
  {
    zzalk localzzalk = new zzalk();
    localzzalk.bhH = ((Integer)paramMap.get("l"));
    if (paramMap.containsKey("sp"))
    {
      localzzalk.bhJ = zze(zzamm.zzbt(paramMap.get("sp")));
      localObject = (String)paramMap.get("sn");
      if (localObject != null) {
        localzzalk.bhK = zzalz.zzsx((String)localObject);
      }
    }
    if (paramMap.containsKey("ep"))
    {
      localzzalk.bhL = zze(zzamm.zzbt(paramMap.get("ep")));
      localObject = (String)paramMap.get("en");
      if (localObject != null) {
        localzzalk.bhM = zzalz.zzsx((String)localObject);
      }
    }
    Object localObject = (String)paramMap.get("vf");
    if (localObject != null) {
      if (!((String)localObject).equals("l")) {
        break label189;
      }
    }
    label189:
    for (localObject = zza.bhP;; localObject = zza.bhQ)
    {
      localzzalk.bhI = ((zza)localObject);
      paramMap = (String)paramMap.get("i");
      if (paramMap != null) {
        localzzalk.bhC = zzamf.zzsy(paramMap);
      }
      return localzzalk;
    }
  }
  
  private zzalk zzcyb()
  {
    zzalk localzzalk = new zzalk();
    localzzalk.bhH = this.bhH;
    localzzalk.bhJ = this.bhJ;
    localzzalk.bhK = this.bhK;
    localzzalk.bhL = this.bhL;
    localzzalk.bhM = this.bhM;
    localzzalk.bhI = this.bhI;
    localzzalk.bhC = this.bhC;
    return localzzalk;
  }
  
  private static zzaml zze(zzaml paramzzaml)
  {
    if (((paramzzaml instanceof zzamr)) || ((paramzzaml instanceof zzaly)) || ((paramzzaml instanceof zzamd)) || ((paramzzaml instanceof zzame))) {
      return paramzzaml;
    }
    if ((paramzzaml instanceof zzamj)) {
      return new zzamd(Double.valueOf(((Long)paramzzaml.getValue()).doubleValue()), zzamp.c());
    }
    paramzzaml = String.valueOf(paramzzaml.getValue());
    throw new IllegalStateException(String.valueOf(paramzzaml).length() + 43 + "Unexpected value passed to normalizeValue: " + paramzzaml);
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    do
    {
      return true;
      if ((paramObject == null) || (getClass() != paramObject.getClass())) {
        return false;
      }
      paramObject = (zzalk)paramObject;
      if (this.bhH != null)
      {
        if (this.bhH.equals(((zzalk)paramObject).bhH)) {}
      }
      else {
        while (((zzalk)paramObject).bhH != null) {
          return false;
        }
      }
      if (this.bhC != null)
      {
        if (this.bhC.equals(((zzalk)paramObject).bhC)) {}
      }
      else {
        while (((zzalk)paramObject).bhC != null) {
          return false;
        }
      }
      if (this.bhM != null)
      {
        if (this.bhM.equals(((zzalk)paramObject).bhM)) {}
      }
      else {
        while (((zzalk)paramObject).bhM != null) {
          return false;
        }
      }
      if (this.bhL != null)
      {
        if (this.bhL.equals(((zzalk)paramObject).bhL)) {}
      }
      else {
        while (((zzalk)paramObject).bhL != null) {
          return false;
        }
      }
      if (this.bhK != null)
      {
        if (this.bhK.equals(((zzalk)paramObject).bhK)) {}
      }
      else {
        while (((zzalk)paramObject).bhK != null) {
          return false;
        }
      }
      if (this.bhJ != null)
      {
        if (this.bhJ.equals(((zzalk)paramObject).bhJ)) {}
      }
      else {
        while (((zzalk)paramObject).bhJ != null) {
          return false;
        }
      }
    } while (zzcyc() == ((zzalk)paramObject).zzcyc());
    return false;
  }
  
  public int getLimit()
  {
    if (!zzcxy()) {
      throw new IllegalArgumentException("Cannot get limit if limit has not been set");
    }
    return this.bhH.intValue();
  }
  
  public int hashCode()
  {
    int i2 = 0;
    int i;
    int j;
    label29:
    int k;
    label44:
    int m;
    label60:
    int n;
    if (this.bhH != null)
    {
      i = this.bhH.intValue();
      if (!zzcyc()) {
        break label149;
      }
      j = 1231;
      if (this.bhJ == null) {
        break label156;
      }
      k = this.bhJ.hashCode();
      if (this.bhK == null) {
        break label161;
      }
      m = this.bhK.hashCode();
      if (this.bhL == null) {
        break label167;
      }
      n = this.bhL.hashCode();
      label76:
      if (this.bhM == null) {
        break label173;
      }
    }
    label149:
    label156:
    label161:
    label167:
    label173:
    for (int i1 = this.bhM.hashCode();; i1 = 0)
    {
      if (this.bhC != null) {
        i2 = this.bhC.hashCode();
      }
      return (i1 + (n + (m + (k + (j + i * 31) * 31) * 31) * 31) * 31) * 31 + i2;
      i = 0;
      break;
      j = 1237;
      break label29;
      k = 0;
      break label44;
      m = 0;
      break label60;
      n = 0;
      break label76;
    }
  }
  
  public boolean isDefault()
  {
    return (zzcye()) && (this.bhC.equals(zzamo.b()));
  }
  
  public boolean isValid()
  {
    return (!zzcxs()) || (!zzcxv()) || (!zzcxy()) || (zzcxz());
  }
  
  public String toString()
  {
    return zzcyd().toString();
  }
  
  public zzalk zza(zzamf paramzzamf)
  {
    zzalk localzzalk = zzcyb();
    localzzalk.bhC = paramzzamf;
    return localzzalk;
  }
  
  public zzalk zza(zzaml paramzzaml, zzalz paramzzalz)
  {
    assert ((paramzzaml.zzcze()) || (paramzzaml.isEmpty()));
    if (!(paramzzaml instanceof zzamj)) {}
    for (boolean bool = true;; bool = false)
    {
      zzann.zzcx(bool);
      zzalk localzzalk = zzcyb();
      localzzalk.bhJ = paramzzaml;
      localzzalk.bhK = paramzzalz;
      return localzzalk;
    }
  }
  
  public zzalk zzafy(int paramInt)
  {
    zzalk localzzalk = zzcyb();
    localzzalk.bhH = Integer.valueOf(paramInt);
    localzzalk.bhI = zza.bhP;
    return localzzalk;
  }
  
  public zzalk zzafz(int paramInt)
  {
    zzalk localzzalk = zzcyb();
    localzzalk.bhH = Integer.valueOf(paramInt);
    localzzalk.bhI = zza.bhQ;
    return localzzalk;
  }
  
  public zzalk zzb(zzaml paramzzaml, zzalz paramzzalz)
  {
    assert ((paramzzaml.zzcze()) || (paramzzaml.isEmpty()));
    if (!(paramzzaml instanceof zzamj)) {}
    for (boolean bool = true;; bool = false)
    {
      zzann.zzcx(bool);
      zzalk localzzalk = zzcyb();
      localzzalk.bhL = paramzzaml;
      localzzalk.bhM = paramzzalz;
      return localzzalk;
    }
  }
  
  public boolean zzcxs()
  {
    return this.bhJ != null;
  }
  
  public zzaml zzcxt()
  {
    if (!zzcxs()) {
      throw new IllegalArgumentException("Cannot get index start value if start has not been set");
    }
    return this.bhJ;
  }
  
  public zzalz zzcxu()
  {
    if (!zzcxs()) {
      throw new IllegalArgumentException("Cannot get index start name if start has not been set");
    }
    if (this.bhK != null) {
      return this.bhK;
    }
    return zzalz.zzcyx();
  }
  
  public boolean zzcxv()
  {
    return this.bhL != null;
  }
  
  public zzaml zzcxw()
  {
    if (!zzcxv()) {
      throw new IllegalArgumentException("Cannot get index end value if start has not been set");
    }
    return this.bhL;
  }
  
  public zzalz zzcxx()
  {
    if (!zzcxv()) {
      throw new IllegalArgumentException("Cannot get index end name if start has not been set");
    }
    if (this.bhM != null) {
      return this.bhM;
    }
    return zzalz.zzcyy();
  }
  
  public boolean zzcxy()
  {
    return this.bhH != null;
  }
  
  public boolean zzcxz()
  {
    return (zzcxy()) && (this.bhI != null);
  }
  
  public zzamf zzcya()
  {
    return this.bhC;
  }
  
  public boolean zzcyc()
  {
    if (this.bhI != null) {
      return this.bhI == zza.bhP;
    }
    return zzcxs();
  }
  
  public Map<String, Object> zzcyd()
  {
    HashMap localHashMap = new HashMap();
    if (zzcxs())
    {
      localHashMap.put("sp", this.bhJ.getValue());
      if (this.bhK != null) {
        localHashMap.put("sn", this.bhK.asString());
      }
    }
    if (zzcxv())
    {
      localHashMap.put("ep", this.bhL.getValue());
      if (this.bhM != null) {
        localHashMap.put("en", this.bhM.asString());
      }
    }
    zza localzza1;
    if (this.bhH != null)
    {
      localHashMap.put("l", this.bhH);
      zza localzza2 = this.bhI;
      localzza1 = localzza2;
      if (localzza2 == null)
      {
        if (!zzcxs()) {
          break label207;
        }
        localzza1 = zza.bhP;
      }
      switch (1.bhO[localzza1.ordinal()])
      {
      }
    }
    for (;;)
    {
      if (!this.bhC.equals(zzamo.b())) {
        localHashMap.put("i", this.bhC.zzczt());
      }
      return localHashMap;
      label207:
      localzza1 = zza.bhQ;
      break;
      localHashMap.put("vf", "l");
      continue;
      localHashMap.put("vf", "r");
    }
  }
  
  public boolean zzcye()
  {
    return (!zzcxs()) && (!zzcxv()) && (!zzcxy());
  }
  
  public String zzcyf()
  {
    if (this.bhN == null) {}
    try
    {
      this.bhN = zzane.zzce(zzcyd());
      return this.bhN;
    }
    catch (IOException localIOException)
    {
      throw new RuntimeException(localIOException);
    }
  }
  
  public zzals zzcyg()
  {
    if (zzcye()) {
      return new zzalq(zzcya());
    }
    if (zzcxy()) {
      return new zzalr(this);
    }
    return new zzalt(this);
  }
  
  private static enum zza
  {
    private zza() {}
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzalk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */