package com.google.android.gms.internal;

import java.util.Map;

public class zzall
{
  private final zzajq aZr;
  private final zzalk aZv;
  
  public zzall(zzajq paramzzajq, zzalk paramzzalk)
  {
    this.aZr = paramzzajq;
    this.aZv = paramzzalk;
  }
  
  public static zzall zzan(zzajq paramzzajq)
  {
    return new zzall(paramzzajq, zzalk.bhG);
  }
  
  public static zzall zzb(zzajq paramzzajq, Map<String, Object> paramMap)
  {
    return new zzall(paramzzajq, zzalk.zzcd(paramMap));
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    do
    {
      return true;
      if ((paramObject == null) || (getClass() != paramObject.getClass())) {
        return false;
      }
      paramObject = (zzall)paramObject;
      if (!this.aZr.equals(((zzall)paramObject).aZr)) {
        return false;
      }
    } while (this.aZv.equals(((zzall)paramObject).aZv));
    return false;
  }
  
  public int hashCode()
  {
    return this.aZr.hashCode() * 31 + this.aZv.hashCode();
  }
  
  public boolean isDefault()
  {
    return this.aZv.isDefault();
  }
  
  public String toString()
  {
    String str1 = String.valueOf(this.aZr);
    String str2 = String.valueOf(this.aZv);
    return String.valueOf(str1).length() + 1 + String.valueOf(str2).length() + str1 + ":" + str2;
  }
  
  public zzajq zzcrc()
  {
    return this.aZr;
  }
  
  public zzamf zzcya()
  {
    return this.aZv.zzcya();
  }
  
  public boolean zzcye()
  {
    return this.aZv.zzcye();
  }
  
  public zzalk zzcyh()
  {
    return this.aZv;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzall.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */