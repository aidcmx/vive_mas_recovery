package com.google.android.gms.internal;

import com.google.firebase.database.DatabaseError;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class zzalm
{
  private final zzall bhB;
  private final zzalo bhS;
  private zzaln bhT;
  private final List<zzajl> bhU;
  private final zzali bhV;
  
  static
  {
    if (!zzalm.class.desiredAssertionStatus()) {}
    for (boolean bool = true;; bool = false)
    {
      $assertionsDisabled = bool;
      return;
    }
  }
  
  public zzalm(zzall paramzzall, zzaln paramzzaln)
  {
    this.bhB = paramzzall;
    Object localObject = new zzalq(paramzzall.zzcya());
    zzals localzzals = paramzzall.zzcyh().zzcyg();
    this.bhS = new zzalo(localzzals);
    zzald localzzald = paramzzaln.zzcyn();
    paramzzaln = paramzzaln.zzcyl();
    zzamg localzzamg2 = zzamg.zza(zzame.zzczq(), paramzzall.zzcya());
    zzamg localzzamg1 = ((zzalq)localObject).zza(localzzamg2, localzzald.zzcxj(), null);
    localzzamg2 = localzzals.zza(localzzamg2, paramzzaln.zzcxj(), null);
    localObject = new zzald(localzzamg1, localzzald.zzcxh(), ((zzalq)localObject).zzcyr());
    this.bhT = new zzaln(new zzald(localzzamg2, paramzzaln.zzcxh(), localzzals.zzcyr()), (zzald)localObject);
    this.bhU = new ArrayList();
    this.bhV = new zzali(paramzzall);
  }
  
  private List<zzalg> zza(List<zzalf> paramList, zzamg paramzzamg, zzajl paramzzajl)
  {
    if (paramzzajl == null) {}
    for (paramzzajl = this.bhU;; paramzzajl = Arrays.asList(new zzajl[] { paramzzajl })) {
      return this.bhV.zza(paramList, paramzzamg, paramzzajl);
    }
  }
  
  public boolean isEmpty()
  {
    return this.bhU.isEmpty();
  }
  
  public List<zzalh> zza(zzajl paramzzajl, DatabaseError paramDatabaseError)
  {
    int i;
    int j;
    if (paramDatabaseError != null)
    {
      Object localObject = new ArrayList();
      assert (paramzzajl == null) : "A cancel should cancel all event registrations";
      zzajq localzzajq = this.bhB.zzcrc();
      Iterator localIterator = this.bhU.iterator();
      while (localIterator.hasNext()) {
        ((List)localObject).add(new zzale((zzajl)localIterator.next(), paramDatabaseError, localzzajq));
      }
      paramDatabaseError = (DatabaseError)localObject;
      if (paramzzajl == null) {
        break label204;
      }
      i = 0;
      j = -1;
      label106:
      if (i >= this.bhU.size()) {
        break label249;
      }
      localObject = (zzajl)this.bhU.get(i);
      if (!((zzajl)localObject).zzc(paramzzajl)) {
        break label197;
      }
      if (!((zzajl)localObject).zzcvc()) {
        break label194;
      }
    }
    for (;;)
    {
      if (i != -1)
      {
        paramzzajl = (zzajl)this.bhU.get(i);
        this.bhU.remove(i);
        paramzzajl.zzcvb();
      }
      return paramDatabaseError;
      paramDatabaseError = Collections.emptyList();
      break;
      label194:
      j = i;
      label197:
      i += 1;
      break label106;
      label204:
      paramzzajl = this.bhU.iterator();
      while (paramzzajl.hasNext()) {
        ((zzajl)paramzzajl.next()).zzcvb();
      }
      this.bhU.clear();
      return paramDatabaseError;
      label249:
      i = j;
    }
  }
  
  public zza zzb(zzakn paramzzakn, zzaki paramzzaki, zzaml paramzzaml)
  {
    if ((paramzzakn.zzcwq() == zzakn.zza.bgl) && (paramzzakn.zzcwp().zzcwu() != null))
    {
      assert (this.bhT.zzcyo() != null) : "We should always have a full cache before handling merges";
      assert (this.bhT.zzcym() != null) : "Missing event cache, even though we have a server cache";
    }
    zzaln localzzaln = this.bhT;
    paramzzakn = this.bhS.zza(localzzaln, paramzzakn, paramzzaki, paramzzaml);
    assert ((paramzzakn.bhT.zzcyn().zzcxh()) || (!localzzaln.zzcyn().zzcxh())) : "Once a server snap is complete, it should never go back";
    this.bhT = paramzzakn.bhT;
    return new zza(zza(paramzzakn.bhX, paramzzakn.bhT.zzcyl().zzcxj(), null), paramzzakn.bhX);
  }
  
  public void zzb(zzajl paramzzajl)
  {
    this.bhU.add(paramzzajl);
  }
  
  public zzall zzcyi()
  {
    return this.bhB;
  }
  
  public zzaml zzcyj()
  {
    return this.bhT.zzcyn().zzcqy();
  }
  
  public zzaml zzcyk()
  {
    return this.bhT.zzcyl().zzcqy();
  }
  
  public List<zzalg> zzl(zzajl paramzzajl)
  {
    zzald localzzald = this.bhT.zzcyl();
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = localzzald.zzcqy().iterator();
    while (localIterator.hasNext())
    {
      zzamk localzzamk = (zzamk)localIterator.next();
      localArrayList.add(zzalf.zzc(localzzamk.a(), localzzamk.zzcqy()));
    }
    if (localzzald.zzcxh()) {
      localArrayList.add(zzalf.zza(localzzald.zzcxj()));
    }
    return zza(localArrayList, localzzald.zzcxj(), paramzzajl);
  }
  
  public zzaml zzs(zzajq paramzzajq)
  {
    zzaml localzzaml = this.bhT.zzcyo();
    if ((localzzaml != null) && ((this.bhB.zzcye()) || ((!paramzzajq.isEmpty()) && (!localzzaml.zzm(paramzzajq.zzcvj()).isEmpty())))) {
      return localzzaml.zzao(paramzzajq);
    }
    return null;
  }
  
  public static class zza
  {
    public final List<zzalg> bhW;
    public final List<zzalf> bhX;
    
    public zza(List<zzalg> paramList, List<zzalf> paramList1)
    {
      this.bhW = paramList;
      this.bhX = paramList1;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzalm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */