package com.google.android.gms.internal;

public class zzaln
{
  private final zzald bhY;
  private final zzald bhZ;
  
  public zzaln(zzald paramzzald1, zzald paramzzald2)
  {
    this.bhY = paramzzald1;
    this.bhZ = paramzzald2;
  }
  
  public zzaln zza(zzamg paramzzamg, boolean paramBoolean1, boolean paramBoolean2)
  {
    return new zzaln(new zzald(paramzzamg, paramBoolean1, paramBoolean2), this.bhZ);
  }
  
  public zzaln zzb(zzamg paramzzamg, boolean paramBoolean1, boolean paramBoolean2)
  {
    return new zzaln(this.bhY, new zzald(paramzzamg, paramBoolean1, paramBoolean2));
  }
  
  public zzald zzcyl()
  {
    return this.bhY;
  }
  
  public zzaml zzcym()
  {
    if (this.bhY.zzcxh()) {
      return this.bhY.zzcqy();
    }
    return null;
  }
  
  public zzald zzcyn()
  {
    return this.bhZ;
  }
  
  public zzaml zzcyo()
  {
    if (this.bhZ.zzcxh()) {
      return this.bhZ.zzcqy();
    }
    return null;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzaln.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */