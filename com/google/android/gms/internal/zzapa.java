package com.google.android.gms.internal;

public final class zzapa
  extends zzaoy
{
  public static final zzapa bou = new zzapa();
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || ((paramObject instanceof zzapa));
  }
  
  public int hashCode()
  {
    return zzapa.class.hashCode();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzapa.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */