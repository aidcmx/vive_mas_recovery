package com.google.android.gms.internal;

import android.content.Context;
import android.net.Uri;
import android.os.RemoteException;
import android.view.MotionEvent;
import com.google.android.gms.dynamic.zze;

@zzji
public final class zzcf
{
  private final zzcj zzaks;
  
  public zzcf(String paramString, Context paramContext, boolean paramBoolean)
  {
    this.zzaks = zzci.zzb(paramString, paramContext, paramBoolean);
  }
  
  public void zza(MotionEvent paramMotionEvent)
    throws RemoteException
  {
    this.zzaks.zzd(zze.zzac(paramMotionEvent));
  }
  
  public Uri zzc(Uri paramUri, Context paramContext)
    throws zzcg, RemoteException
  {
    paramUri = zze.zzac(paramUri);
    paramContext = zze.zzac(paramContext);
    paramUri = this.zzaks.zza(paramUri, paramContext);
    if (paramUri == null) {
      throw new zzcg();
    }
    return (Uri)zze.zzae(paramUri);
  }
  
  public Uri zzd(Uri paramUri, Context paramContext)
    throws zzcg, RemoteException
  {
    paramUri = zze.zzac(paramUri);
    paramContext = zze.zzac(paramContext);
    paramUri = this.zzaks.zzb(paramUri, paramContext);
    if (paramUri == null) {
      throw new zzcg();
    }
    return (Uri)zze.zzae(paramUri);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzcf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */