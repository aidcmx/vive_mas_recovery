package com.google.android.gms.internal;

import android.content.Context;
import android.net.Uri;
import android.view.MotionEvent;
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;
import com.google.android.gms.dynamic.zzd;
import com.google.android.gms.dynamic.zze;

@zzji
public final class zzch
  extends zzcj.zza
{
  private final zzau zzakt;
  private final zzav zzaku;
  private final zzas zzakv;
  private boolean zzakw = false;
  
  public zzch(String paramString, Context paramContext, boolean paramBoolean)
  {
    this.zzakt = zzau.zza(paramString, paramContext, paramBoolean);
    this.zzaku = new zzav(this.zzakt);
    if (paramBoolean) {}
    for (paramString = null;; paramString = zzas.zzc(paramContext))
    {
      this.zzakv = paramString;
      return;
    }
  }
  
  private zzd zza(zzd paramzzd1, zzd paramzzd2, boolean paramBoolean)
  {
    try
    {
      paramzzd1 = (Uri)zze.zzae(paramzzd1);
      paramzzd2 = (Context)zze.zzae(paramzzd2);
      if (paramBoolean) {}
      for (paramzzd1 = this.zzaku.zza(paramzzd1, paramzzd2);; paramzzd1 = this.zzaku.zzb(paramzzd1, paramzzd2)) {
        return zze.zzac(paramzzd1);
      }
      return null;
    }
    catch (zzaw paramzzd1) {}
  }
  
  public zzd zza(zzd paramzzd1, zzd paramzzd2)
  {
    return zza(paramzzd1, paramzzd2, true);
  }
  
  public String zza(zzd paramzzd, String paramString)
  {
    paramzzd = (Context)zze.zzae(paramzzd);
    return this.zzakt.zzb(paramzzd, paramString);
  }
  
  public String zza(zzd paramzzd, byte[] paramArrayOfByte)
  {
    Context localContext = (Context)zze.zzae(paramzzd);
    paramzzd = this.zzakt.zza(localContext, paramArrayOfByte);
    if ((this.zzakv != null) && (this.zzakw))
    {
      paramArrayOfByte = this.zzakv.zza(localContext, paramArrayOfByte);
      paramzzd = this.zzakv.zza(paramzzd, paramArrayOfByte);
      this.zzakw = false;
      return paramzzd;
    }
    return paramzzd;
  }
  
  public boolean zza(zzd paramzzd)
  {
    paramzzd = (Uri)zze.zzae(paramzzd);
    return this.zzaku.zza(paramzzd);
  }
  
  public zzd zzb(zzd paramzzd1, zzd paramzzd2)
  {
    return zza(paramzzd1, paramzzd2, false);
  }
  
  public void zzb(String paramString1, String paramString2)
  {
    this.zzaku.zzb(paramString1, paramString2);
  }
  
  public boolean zzb(zzd paramzzd)
  {
    paramzzd = (Uri)zze.zzae(paramzzd);
    return this.zzaku.zzc(paramzzd);
  }
  
  public boolean zzb(String paramString, boolean paramBoolean)
  {
    if (this.zzakv == null) {
      return false;
    }
    paramString = new AdvertisingIdClient.Info(paramString, paramBoolean);
    this.zzakv.zza(paramString);
    this.zzakw = true;
    return true;
  }
  
  public String zzc(zzd paramzzd)
  {
    return zza(paramzzd, null);
  }
  
  public void zzd(zzd paramzzd)
  {
    paramzzd = (MotionEvent)zze.zzae(paramzzd);
    this.zzaku.zza(paramzzd);
  }
  
  public String zzdx()
  {
    return "ms";
  }
  
  public void zzm(String paramString)
  {
    this.zzaku.zzm(paramString);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzch.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */