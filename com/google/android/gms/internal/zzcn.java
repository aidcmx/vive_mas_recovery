package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import org.json.JSONObject;

@zzji
public final class zzcn
{
  private final String zzasu;
  private final JSONObject zzasv;
  private final String zzasw;
  private final String zzasx;
  private final boolean zzasy;
  private final boolean zzasz;
  
  public zzcn(String paramString1, VersionInfoParcel paramVersionInfoParcel, String paramString2, JSONObject paramJSONObject, boolean paramBoolean1, boolean paramBoolean2)
  {
    this.zzasx = paramVersionInfoParcel.zzda;
    this.zzasv = paramJSONObject;
    this.zzasw = paramString1;
    this.zzasu = paramString2;
    this.zzasy = paramBoolean1;
    this.zzasz = paramBoolean2;
  }
  
  public String zzhy()
  {
    return this.zzasu;
  }
  
  public String zzhz()
  {
    return this.zzasx;
  }
  
  public JSONObject zzia()
  {
    return this.zzasv;
  }
  
  public String zzib()
  {
    return this.zzasw;
  }
  
  public boolean zzic()
  {
    return this.zzasy;
  }
  
  public boolean zzid()
  {
    return this.zzasz;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzcn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */