package com.google.android.gms.internal;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.View;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.formats.zzi;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import java.util.WeakHashMap;

@zzji
public class zzco
  implements zzcq
{
  private final Object zzako = new Object();
  private final VersionInfoParcel zzanu;
  private final WeakHashMap<zzko, zzcp> zzata = new WeakHashMap();
  private final ArrayList<zzcp> zzatb = new ArrayList();
  private final Context zzatc;
  private final zzgh zzatd;
  
  public zzco(Context paramContext, VersionInfoParcel paramVersionInfoParcel, zzgh paramzzgh)
  {
    this.zzatc = paramContext.getApplicationContext();
    this.zzanu = paramVersionInfoParcel;
    this.zzatd = paramzzgh;
  }
  
  public void zza(AdSizeParcel paramAdSizeParcel, zzko paramzzko)
  {
    zza(paramAdSizeParcel, paramzzko, paramzzko.zzcbm.getView());
  }
  
  public void zza(AdSizeParcel paramAdSizeParcel, zzko paramzzko, View paramView)
  {
    zza(paramAdSizeParcel, paramzzko, new zzcp.zzd(paramView, paramzzko), null);
  }
  
  public void zza(AdSizeParcel paramAdSizeParcel, zzko paramzzko, View paramView, zzgi paramzzgi)
  {
    zza(paramAdSizeParcel, paramzzko, new zzcp.zzd(paramView, paramzzko), paramzzgi);
  }
  
  public void zza(AdSizeParcel paramAdSizeParcel, zzko paramzzko, zzi paramzzi)
  {
    zza(paramAdSizeParcel, paramzzko, new zzcp.zza(paramzzi), null);
  }
  
  public void zza(AdSizeParcel paramAdSizeParcel, zzko paramzzko, zzcw paramzzcw, @Nullable zzgi paramzzgi)
  {
    for (;;)
    {
      synchronized (this.zzako)
      {
        if (zzi(paramzzko))
        {
          paramAdSizeParcel = (zzcp)this.zzata.get(paramzzko);
          if (paramzzgi != null) {
            paramAdSizeParcel.zza(new zzcr(paramAdSizeParcel, paramzzgi));
          }
        }
        else
        {
          paramAdSizeParcel = new zzcp(this.zzatc, paramAdSizeParcel, paramzzko, this.zzanu, paramzzcw);
          paramAdSizeParcel.zza(this);
          this.zzata.put(paramzzko, paramAdSizeParcel);
          this.zzatb.add(paramAdSizeParcel);
        }
      }
      paramAdSizeParcel.zza(new zzcs(paramAdSizeParcel, this.zzatd));
    }
  }
  
  public void zza(zzcp paramzzcp)
  {
    synchronized (this.zzako)
    {
      if (!paramzzcp.zzii())
      {
        this.zzatb.remove(paramzzcp);
        Iterator localIterator = this.zzata.entrySet().iterator();
        while (localIterator.hasNext()) {
          if (((Map.Entry)localIterator.next()).getValue() == paramzzcp) {
            localIterator.remove();
          }
        }
      }
    }
  }
  
  public boolean zzi(zzko paramzzko)
  {
    for (;;)
    {
      synchronized (this.zzako)
      {
        paramzzko = (zzcp)this.zzata.get(paramzzko);
        if ((paramzzko != null) && (paramzzko.zzii()))
        {
          bool = true;
          return bool;
        }
      }
      boolean bool = false;
    }
  }
  
  public void zzj(zzko paramzzko)
  {
    synchronized (this.zzako)
    {
      paramzzko = (zzcp)this.zzata.get(paramzzko);
      if (paramzzko != null) {
        paramzzko.zzig();
      }
      return;
    }
  }
  
  public void zzk(zzko paramzzko)
  {
    synchronized (this.zzako)
    {
      paramzzko = (zzcp)this.zzata.get(paramzzko);
      if (paramzzko != null) {
        paramzzko.stop();
      }
      return;
    }
  }
  
  public void zzl(zzko paramzzko)
  {
    synchronized (this.zzako)
    {
      paramzzko = (zzcp)this.zzata.get(paramzzko);
      if (paramzzko != null) {
        paramzzko.pause();
      }
      return;
    }
  }
  
  public void zzm(zzko paramzzko)
  {
    synchronized (this.zzako)
    {
      paramzzko = (zzcp)this.zzata.get(paramzzko);
      if (paramzzko != null) {
        paramzzko.resume();
      }
      return;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzco.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */