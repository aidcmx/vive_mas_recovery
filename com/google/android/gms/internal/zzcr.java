package com.google.android.gms.internal;

import org.json.JSONObject;

@zzji
public class zzcr
  implements zzct
{
  private final zzcp zzaua;
  private final zzgi zzaub;
  private final zzfe zzauc = new zzcr.1(this);
  private final zzfe zzaud = new zzcr.2(this);
  private final zzfe zzaue = new zzcr.3(this);
  
  public zzcr(zzcp paramzzcp, zzgi paramzzgi)
  {
    this.zzaua = paramzzcp;
    this.zzaub = paramzzgi;
    zzc(this.zzaub);
    paramzzcp = String.valueOf(this.zzaua.zziq().zzib());
    if (paramzzcp.length() != 0) {}
    for (paramzzcp = "Custom JS tracking ad unit: ".concat(paramzzcp);; paramzzcp = new String("Custom JS tracking ad unit: "))
    {
      zzkx.zzdg(paramzzcp);
      return;
    }
  }
  
  void zzc(zzgi paramzzgi)
  {
    paramzzgi.zza("/updateActiveView", this.zzauc);
    paramzzgi.zza("/untrackActiveViewUnit", this.zzaud);
    paramzzgi.zza("/visibilityChanged", this.zzaue);
  }
  
  public void zzc(JSONObject paramJSONObject, boolean paramBoolean)
  {
    if (!paramBoolean)
    {
      this.zzaub.zza("AFMA_updateActiveView", paramJSONObject);
      return;
    }
    this.zzaua.zzb(this);
  }
  
  void zzd(zzgi paramzzgi)
  {
    paramzzgi.zzb("/visibilityChanged", this.zzaue);
    paramzzgi.zzb("/untrackActiveViewUnit", this.zzaud);
    paramzzgi.zzb("/updateActiveView", this.zzauc);
  }
  
  public boolean zziu()
  {
    return true;
  }
  
  public void zziv()
  {
    zzd(this.zzaub);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzcr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */