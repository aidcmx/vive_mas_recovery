package com.google.android.gms.internal;

import org.json.JSONObject;

@zzji
public class zzcs
  implements zzct
{
  private final zzcp zzaua;
  private final zzfe zzauc = new zzcs.5(this);
  private final zzfe zzaud = new zzcs.6(this);
  private final zzfe zzaue = new zzcs.7(this);
  private zzgh.zzc zzaug;
  private boolean zzauh;
  
  public zzcs(zzcp paramzzcp, zzgh paramzzgh)
  {
    this.zzaua = paramzzcp;
    this.zzaug = paramzzgh.zzny();
    this.zzaug.zza(new zzcs.1(this), new zzcs.2(this));
    paramzzcp = String.valueOf(this.zzaua.zziq().zzib());
    if (paramzzcp.length() != 0) {}
    for (paramzzcp = "Core JS tracking ad unit: ".concat(paramzzcp);; paramzzcp = new String("Core JS tracking ad unit: "))
    {
      zzkx.zzdg(paramzzcp);
      return;
    }
  }
  
  void zzc(zzgi paramzzgi)
  {
    paramzzgi.zza("/updateActiveView", this.zzauc);
    paramzzgi.zza("/untrackActiveViewUnit", this.zzaud);
    paramzzgi.zza("/visibilityChanged", this.zzaue);
  }
  
  public void zzc(JSONObject paramJSONObject, boolean paramBoolean)
  {
    this.zzaug.zza(new zzcs.3(this, paramJSONObject), new zzlw.zzb());
  }
  
  void zzd(zzgi paramzzgi)
  {
    paramzzgi.zzb("/visibilityChanged", this.zzaue);
    paramzzgi.zzb("/untrackActiveViewUnit", this.zzaud);
    paramzzgi.zzb("/updateActiveView", this.zzauc);
  }
  
  public boolean zziu()
  {
    return this.zzauh;
  }
  
  public void zziv()
  {
    this.zzaug.zza(new zzcs.4(this), new zzlw.zzb());
    this.zzaug.release();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzcs.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */