package com.google.android.gms.internal;

import android.support.annotation.Nullable;
import com.google.android.gms.ads.internal.zzu;
import java.util.ArrayList;
import java.util.Iterator;

@zzji
public class zzcx
{
  private final Object zzako = new Object();
  private final int zzavi;
  private final int zzavj;
  private final int zzavk;
  private final zzdd zzavl;
  private final zzdi zzavm;
  private ArrayList<String> zzavn = new ArrayList();
  private ArrayList<String> zzavo = new ArrayList();
  private ArrayList<zzdb> zzavp = new ArrayList();
  private int zzavq = 0;
  private int zzavr = 0;
  private int zzavs = 0;
  private int zzavt;
  private String zzavu = "";
  private String zzavv = "";
  private String zzavw = "";
  
  public zzcx(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7)
  {
    this.zzavi = paramInt1;
    this.zzavj = paramInt2;
    this.zzavk = paramInt3;
    this.zzavl = new zzdd(paramInt4);
    this.zzavm = new zzdi(paramInt5, paramInt6, paramInt7);
  }
  
  private String zza(ArrayList<String> paramArrayList, int paramInt)
  {
    if (paramArrayList.isEmpty()) {
      paramArrayList = "";
    }
    Object localObject;
    do
    {
      return paramArrayList;
      localObject = new StringBuffer();
      paramArrayList = paramArrayList.iterator();
      do
      {
        if (!paramArrayList.hasNext()) {
          break;
        }
        ((StringBuffer)localObject).append((String)paramArrayList.next());
        ((StringBuffer)localObject).append(' ');
      } while (((StringBuffer)localObject).length() <= paramInt);
      ((StringBuffer)localObject).deleteCharAt(((StringBuffer)localObject).length() - 1);
      localObject = ((StringBuffer)localObject).toString();
      paramArrayList = (ArrayList<String>)localObject;
    } while (((String)localObject).length() < paramInt);
    return ((String)localObject).substring(0, paramInt);
  }
  
  private void zzc(@Nullable String paramString, boolean paramBoolean, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
  {
    if ((paramString == null) || (paramString.length() < this.zzavk)) {
      return;
    }
    synchronized (this.zzako)
    {
      this.zzavn.add(paramString);
      this.zzavq += paramString.length();
      if (paramBoolean)
      {
        this.zzavo.add(paramString);
        this.zzavp.add(new zzdb(paramFloat1, paramFloat2, paramFloat3, paramFloat4, this.zzavo.size() - 1));
      }
      return;
    }
  }
  
  public boolean equals(Object paramObject)
  {
    if (!(paramObject instanceof zzcx)) {}
    do
    {
      return false;
      if (paramObject == this) {
        return true;
      }
      paramObject = (zzcx)paramObject;
    } while ((((zzcx)paramObject).zziy() == null) || (!((zzcx)paramObject).zziy().equals(zziy())));
    return true;
  }
  
  public int getScore()
  {
    return this.zzavt;
  }
  
  public int hashCode()
  {
    return zziy().hashCode();
  }
  
  public String toString()
  {
    int i = this.zzavr;
    int j = this.zzavt;
    int k = this.zzavq;
    String str1 = String.valueOf(zza(this.zzavn, 100));
    String str2 = String.valueOf(zza(this.zzavo, 100));
    String str3 = this.zzavu;
    String str4 = this.zzavv;
    String str5 = this.zzavw;
    return String.valueOf(str1).length() + 165 + String.valueOf(str2).length() + String.valueOf(str3).length() + String.valueOf(str4).length() + String.valueOf(str5).length() + "ActivityContent fetchId: " + i + " score:" + j + " total_length:" + k + "\n text: " + str1 + "\n viewableText" + str2 + "\n signture: " + str3 + "\n viewableSignture: " + str4 + "\n viewableSignatureForVertical: " + str5;
  }
  
  int zza(int paramInt1, int paramInt2)
  {
    return this.zzavi * paramInt1 + this.zzavj * paramInt2;
  }
  
  public void zza(String arg1, boolean paramBoolean, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
  {
    zzc(???, paramBoolean, paramFloat1, paramFloat2, paramFloat3, paramFloat4);
    synchronized (this.zzako)
    {
      if (this.zzavs < 0) {
        zzkx.zzdg("ActivityContent: negative number of WebViews.");
      }
      zzje();
      return;
    }
  }
  
  public void zzb(String paramString, boolean paramBoolean, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
  {
    zzc(paramString, paramBoolean, paramFloat1, paramFloat2, paramFloat3, paramFloat4);
  }
  
  public boolean zzix()
  {
    for (;;)
    {
      synchronized (this.zzako)
      {
        if (this.zzavs == 0)
        {
          bool = true;
          return bool;
        }
      }
      boolean bool = false;
    }
  }
  
  public String zziy()
  {
    return this.zzavu;
  }
  
  public String zziz()
  {
    return this.zzavv;
  }
  
  public String zzja()
  {
    return this.zzavw;
  }
  
  public void zzjb()
  {
    synchronized (this.zzako)
    {
      this.zzavt -= 100;
      return;
    }
  }
  
  public void zzjc()
  {
    synchronized (this.zzako)
    {
      this.zzavs -= 1;
      return;
    }
  }
  
  public void zzjd()
  {
    synchronized (this.zzako)
    {
      this.zzavs += 1;
      return;
    }
  }
  
  public void zzje()
  {
    synchronized (this.zzako)
    {
      int i = zza(this.zzavq, this.zzavr);
      if (i > this.zzavt)
      {
        this.zzavt = i;
        if ((((Boolean)zzdr.zzbfa.get()).booleanValue()) && (!zzu.zzgq().zzuq()))
        {
          this.zzavu = this.zzavl.zza(this.zzavn);
          this.zzavv = this.zzavl.zza(this.zzavo);
        }
        if ((((Boolean)zzdr.zzbfc.get()).booleanValue()) && (!zzu.zzgq().zzur())) {
          this.zzavw = this.zzavm.zza(this.zzavo, this.zzavp);
        }
      }
      return;
    }
  }
  
  int zzjf()
  {
    return this.zzavq;
  }
  
  public void zzn(int paramInt)
  {
    this.zzavr = paramInt;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzcx.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */