package com.google.android.gms.internal;

import android.support.annotation.Nullable;
import com.google.android.gms.ads.internal.zzu;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

@zzji
public class zzcy
{
  private final Object zzako = new Object();
  private int zzavx;
  private List<zzcx> zzavy = new LinkedList();
  
  public boolean zza(zzcx paramzzcx)
  {
    synchronized (this.zzako)
    {
      return this.zzavy.contains(paramzzcx);
    }
  }
  
  public boolean zzb(zzcx paramzzcx)
  {
    synchronized (this.zzako)
    {
      Iterator localIterator = this.zzavy.iterator();
      while (localIterator.hasNext())
      {
        zzcx localzzcx = (zzcx)localIterator.next();
        if ((((Boolean)zzdr.zzbfa.get()).booleanValue()) && (!zzu.zzgq().zzuq()))
        {
          if ((paramzzcx != localzzcx) && (localzzcx.zziy().equals(paramzzcx.zziy())))
          {
            localIterator.remove();
            return true;
          }
        }
        else if ((((Boolean)zzdr.zzbfc.get()).booleanValue()) && (!zzu.zzgq().zzur()) && (paramzzcx != localzzcx) && (localzzcx.zzja().equals(paramzzcx.zzja())))
        {
          localIterator.remove();
          return true;
        }
      }
      return false;
    }
  }
  
  public void zzc(zzcx paramzzcx)
  {
    synchronized (this.zzako)
    {
      if (this.zzavy.size() >= 10)
      {
        i = this.zzavy.size();
        zzkx.zzdg(41 + "Queue is full, current size = " + i);
        this.zzavy.remove(0);
      }
      int i = this.zzavx;
      this.zzavx = (i + 1);
      paramzzcx.zzn(i);
      this.zzavy.add(paramzzcx);
      return;
    }
  }
  
  @Nullable
  public zzcx zzjg()
  {
    Object localObject1 = null;
    int k = 0;
    for (;;)
    {
      int i;
      synchronized (this.zzako)
      {
        if (this.zzavy.size() == 0)
        {
          zzkx.zzdg("Queue empty");
          return null;
        }
        if (this.zzavy.size() >= 2)
        {
          j = Integer.MIN_VALUE;
          Iterator localIterator = this.zzavy.iterator();
          i = 0;
          if (localIterator.hasNext())
          {
            zzcx localzzcx2 = (zzcx)localIterator.next();
            m = localzzcx2.getScore();
            if (m <= j) {
              break label163;
            }
            k = m;
            localObject1 = localzzcx2;
            j = i;
            break label174;
          }
          this.zzavy.remove(k);
          return (zzcx)localObject1;
        }
      }
      zzcx localzzcx1 = (zzcx)this.zzavy.get(0);
      localzzcx1.zzjb();
      return localzzcx1;
      label163:
      int m = j;
      int j = k;
      k = m;
      label174:
      i += 1;
      m = k;
      k = j;
      j = m;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzcy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */