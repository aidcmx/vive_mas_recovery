package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.PowerManager;
import android.os.Process;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.TextView;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.common.util.zzs;
import java.util.Iterator;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

@zzji
@TargetApi(14)
public class zzda
  extends Thread
{
  private boolean mStarted = false;
  private final Object zzako;
  private final int zzavi;
  private final int zzavk;
  private boolean zzawh = false;
  private final zzcy zzawi;
  private final zzjh zzawj;
  private final int zzawk;
  private final int zzawl;
  private final int zzawm;
  private final int zzawn;
  private final int zzawo;
  private final int zzawp;
  private final String zzawq;
  private boolean zzbl = false;
  
  public zzda(zzcy paramzzcy, zzjh paramzzjh)
  {
    this.zzawi = paramzzcy;
    this.zzawj = paramzzjh;
    this.zzako = new Object();
    this.zzavi = ((Integer)zzdr.zzbev.get()).intValue();
    this.zzawl = ((Integer)zzdr.zzbew.get()).intValue();
    this.zzavk = ((Integer)zzdr.zzbex.get()).intValue();
    this.zzawm = ((Integer)zzdr.zzbey.get()).intValue();
    this.zzawn = ((Integer)zzdr.zzbfb.get()).intValue();
    this.zzawo = ((Integer)zzdr.zzbfd.get()).intValue();
    this.zzawp = ((Integer)zzdr.zzbfe.get()).intValue();
    this.zzawk = ((Integer)zzdr.zzbez.get()).intValue();
    this.zzawq = ((String)zzdr.zzbfg.get());
    setName("ContentFetchTask");
  }
  
  /* Error */
  public void run()
  {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual 130	com/google/android/gms/internal/zzda:zzji	()Z
    //   4: ifeq +88 -> 92
    //   7: invokestatic 136	com/google/android/gms/ads/internal/zzu:zzgp	()Lcom/google/android/gms/internal/zzcz;
    //   10: invokevirtual 142	com/google/android/gms/internal/zzcz:getActivity	()Landroid/app/Activity;
    //   13: astore_2
    //   14: aload_2
    //   15: ifnonnull +59 -> 74
    //   18: ldc -112
    //   20: invokestatic 149	com/google/android/gms/internal/zzkx:zzdg	(Ljava/lang/String;)V
    //   23: aload_0
    //   24: invokevirtual 152	com/google/android/gms/internal/zzda:zzjk	()V
    //   27: aload_0
    //   28: getfield 107	com/google/android/gms/internal/zzda:zzawk	I
    //   31: sipush 1000
    //   34: imul
    //   35: i2l
    //   36: invokestatic 156	java/lang/Thread:sleep	(J)V
    //   39: aload_0
    //   40: getfield 52	com/google/android/gms/internal/zzda:zzako	Ljava/lang/Object;
    //   43: astore_2
    //   44: aload_2
    //   45: monitorenter
    //   46: aload_0
    //   47: getfield 41	com/google/android/gms/internal/zzda:zzawh	Z
    //   50: istore_1
    //   51: iload_1
    //   52: ifeq +74 -> 126
    //   55: ldc -98
    //   57: invokestatic 149	com/google/android/gms/internal/zzkx:zzdg	(Ljava/lang/String;)V
    //   60: aload_0
    //   61: getfield 52	com/google/android/gms/internal/zzda:zzako	Ljava/lang/Object;
    //   64: invokevirtual 161	java/lang/Object:wait	()V
    //   67: goto -21 -> 46
    //   70: astore_3
    //   71: goto -25 -> 46
    //   74: aload_0
    //   75: aload_2
    //   76: invokevirtual 164	com/google/android/gms/internal/zzda:zza	(Landroid/app/Activity;)V
    //   79: goto -52 -> 27
    //   82: astore_2
    //   83: ldc -90
    //   85: aload_2
    //   86: invokestatic 170	com/google/android/gms/internal/zzkx:zzb	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   89: goto -50 -> 39
    //   92: ldc -84
    //   94: invokestatic 149	com/google/android/gms/internal/zzkx:zzdg	(Ljava/lang/String;)V
    //   97: aload_0
    //   98: invokevirtual 152	com/google/android/gms/internal/zzda:zzjk	()V
    //   101: goto -74 -> 27
    //   104: astore_2
    //   105: ldc -90
    //   107: aload_2
    //   108: invokestatic 170	com/google/android/gms/internal/zzkx:zzb	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   111: aload_0
    //   112: getfield 47	com/google/android/gms/internal/zzda:zzawj	Lcom/google/android/gms/internal/zzjh;
    //   115: aload_2
    //   116: ldc -82
    //   118: invokeinterface 179 3 0
    //   123: goto -84 -> 39
    //   126: aload_2
    //   127: monitorexit
    //   128: goto -128 -> 0
    //   131: astore_3
    //   132: aload_2
    //   133: monitorexit
    //   134: aload_3
    //   135: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	136	0	this	zzda
    //   50	2	1	bool	boolean
    //   82	4	2	localInterruptedException1	InterruptedException
    //   104	29	2	localThrowable	Throwable
    //   70	1	3	localInterruptedException2	InterruptedException
    //   131	4	3	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   55	67	70	java/lang/InterruptedException
    //   0	14	82	java/lang/InterruptedException
    //   18	27	82	java/lang/InterruptedException
    //   27	39	82	java/lang/InterruptedException
    //   74	79	82	java/lang/InterruptedException
    //   92	101	82	java/lang/InterruptedException
    //   0	14	104	java/lang/Throwable
    //   18	27	104	java/lang/Throwable
    //   27	39	104	java/lang/Throwable
    //   74	79	104	java/lang/Throwable
    //   92	101	104	java/lang/Throwable
    //   46	51	131	finally
    //   55	67	131	finally
    //   126	128	131	finally
    //   132	134	131	finally
  }
  
  public void wakeup()
  {
    synchronized (this.zzako)
    {
      this.zzawh = false;
      this.zzako.notifyAll();
      zzkx.zzdg("ContentFetchThread: wakeup");
      return;
    }
  }
  
  zza zza(@Nullable View paramView, zzcx paramzzcx)
  {
    int i = 0;
    if (paramView == null) {
      return new zza(0, 0);
    }
    Object localObject = zzu.zzgp().getContext();
    if (localObject != null)
    {
      localObject = (String)paramView.getTag(((Context)localObject).getResources().getIdentifier((String)zzdr.zzbff.get(), "id", ((Context)localObject).getPackageName()));
      if ((!TextUtils.isEmpty(this.zzawq)) && (localObject != null) && (((String)localObject).equals(this.zzawq))) {
        return new zza(0, 0);
      }
    }
    boolean bool = paramView.getGlobalVisibleRect(new Rect());
    if (((paramView instanceof TextView)) && (!(paramView instanceof EditText)))
    {
      localObject = ((TextView)paramView).getText();
      if (!TextUtils.isEmpty((CharSequence)localObject))
      {
        paramzzcx.zzb(((CharSequence)localObject).toString(), bool, paramView.getX(), paramView.getY(), paramView.getWidth(), paramView.getHeight());
        return new zza(1, 0);
      }
      return new zza(0, 0);
    }
    if (((paramView instanceof WebView)) && (!(paramView instanceof zzmd)))
    {
      paramzzcx.zzjd();
      if (zza((WebView)paramView, paramzzcx, bool)) {
        return new zza(0, 1);
      }
      return new zza(0, 0);
    }
    if ((paramView instanceof ViewGroup))
    {
      paramView = (ViewGroup)paramView;
      int j = 0;
      int k = 0;
      while (j < paramView.getChildCount())
      {
        localObject = zza(paramView.getChildAt(j), paramzzcx);
        k += ((zza)localObject).zzawy;
        i += ((zza)localObject).zzawz;
        j += 1;
      }
      return new zza(k, i);
    }
    return new zza(0, 0);
  }
  
  void zza(@Nullable Activity paramActivity)
  {
    if (paramActivity == null) {}
    do
    {
      return;
      Object localObject2 = null;
      Object localObject1 = localObject2;
      try
      {
        if (paramActivity.getWindow() != null)
        {
          localObject1 = localObject2;
          if (paramActivity.getWindow().getDecorView() != null) {
            localObject1 = paramActivity.getWindow().getDecorView().findViewById(16908290);
          }
        }
      }
      catch (Throwable paramActivity)
      {
        for (;;)
        {
          zzkx.zzdg("Failed getting root view of activity. Content not extracted.");
          localObject1 = localObject2;
        }
      }
    } while (localObject1 == null);
    zzh((View)localObject1);
  }
  
  void zza(zzcx paramzzcx, WebView paramWebView, String paramString, boolean paramBoolean)
  {
    paramzzcx.zzjc();
    try
    {
      if (!TextUtils.isEmpty(paramString))
      {
        paramString = new JSONObject(paramString).optString("text");
        if (TextUtils.isEmpty(paramWebView.getTitle())) {
          break label129;
        }
        String str = String.valueOf(paramWebView.getTitle());
        paramzzcx.zza(String.valueOf(str).length() + 1 + String.valueOf(paramString).length() + str + "\n" + paramString, paramBoolean, paramWebView.getX(), paramWebView.getY(), paramWebView.getWidth(), paramWebView.getHeight());
      }
      while (paramzzcx.zzix())
      {
        this.zzawi.zzb(paramzzcx);
        return;
        label129:
        paramzzcx.zza(paramString, paramBoolean, paramWebView.getX(), paramWebView.getY(), paramWebView.getWidth(), paramWebView.getHeight());
      }
      return;
    }
    catch (JSONException paramzzcx)
    {
      zzkx.zzdg("Json string may be malformed.");
      return;
    }
    catch (Throwable paramzzcx)
    {
      zzkx.zza("Failed to get webview content.", paramzzcx);
      this.zzawj.zza(paramzzcx, "ContentFetchTask.processWebViewContent");
    }
  }
  
  boolean zza(ActivityManager.RunningAppProcessInfo paramRunningAppProcessInfo)
  {
    return paramRunningAppProcessInfo.importance == 100;
  }
  
  @TargetApi(19)
  boolean zza(WebView paramWebView, zzcx paramzzcx, boolean paramBoolean)
  {
    if (!zzs.zzayu()) {
      return false;
    }
    paramzzcx.zzjd();
    paramWebView.post(new zzda.2(this, paramzzcx, paramWebView, paramBoolean));
    return true;
  }
  
  boolean zzh(Context paramContext)
  {
    paramContext = (PowerManager)paramContext.getSystemService("power");
    if (paramContext == null) {
      return false;
    }
    return paramContext.isScreenOn();
  }
  
  boolean zzh(@Nullable View paramView)
  {
    if (paramView == null) {
      return false;
    }
    paramView.post(new zzda.1(this, paramView));
    return true;
  }
  
  void zzi(View paramView)
  {
    try
    {
      zzcx localzzcx = new zzcx(this.zzavi, this.zzawl, this.zzavk, this.zzawm, this.zzawn, this.zzawo, this.zzawp);
      paramView = zza(paramView, localzzcx);
      localzzcx.zzje();
      if ((paramView.zzawy == 0) && (paramView.zzawz == 0)) {
        return;
      }
      if (((paramView.zzawz != 0) || (localzzcx.zzjf() != 0)) && ((paramView.zzawz != 0) || (!this.zzawi.zza(localzzcx))))
      {
        this.zzawi.zzc(localzzcx);
        return;
      }
    }
    catch (Exception paramView)
    {
      zzkx.zzb("Exception in fetchContentOnUIThread", paramView);
      this.zzawj.zza(paramView, "ContentFetchTask.fetchContent");
    }
  }
  
  public void zzjh()
  {
    synchronized (this.zzako)
    {
      if (this.mStarted)
      {
        zzkx.zzdg("Content hash thread already started, quiting...");
        return;
      }
      this.mStarted = true;
      start();
      return;
    }
  }
  
  boolean zzji()
  {
    try
    {
      Context localContext = zzu.zzgp().getContext();
      if (localContext == null) {
        return false;
      }
      Object localObject = (ActivityManager)localContext.getSystemService("activity");
      KeyguardManager localKeyguardManager = (KeyguardManager)localContext.getSystemService("keyguard");
      if ((localObject != null) && (localKeyguardManager != null))
      {
        localObject = ((ActivityManager)localObject).getRunningAppProcesses();
        if (localObject == null) {
          return false;
        }
        localObject = ((List)localObject).iterator();
        while (((Iterator)localObject).hasNext())
        {
          ActivityManager.RunningAppProcessInfo localRunningAppProcessInfo = (ActivityManager.RunningAppProcessInfo)((Iterator)localObject).next();
          if (Process.myPid() == localRunningAppProcessInfo.pid) {
            if ((zza(localRunningAppProcessInfo)) && (!localKeyguardManager.inKeyguardRestrictedInputMode()))
            {
              boolean bool = zzh(localContext);
              if (bool) {
                return true;
              }
            }
          }
        }
        return false;
      }
    }
    catch (Throwable localThrowable)
    {
      return false;
    }
    return false;
  }
  
  public zzcx zzjj()
  {
    return this.zzawi.zzjg();
  }
  
  public void zzjk()
  {
    synchronized (this.zzako)
    {
      this.zzawh = true;
      boolean bool = this.zzawh;
      zzkx.zzdg(42 + "ContentFetchThread: paused, mPause = " + bool);
      return;
    }
  }
  
  public boolean zzjl()
  {
    return this.zzawh;
  }
  
  @zzji
  class zza
  {
    final int zzawy;
    final int zzawz;
    
    zza(int paramInt1, int paramInt2)
    {
      this.zzawy = paramInt1;
      this.zzawz = paramInt2;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzda.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */