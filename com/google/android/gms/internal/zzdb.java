package com.google.android.gms.internal;

@zzji
public class zzdb
{
  private final float zzaxa;
  private final float zzaxb;
  private final float zzaxc;
  private final float zzaxd;
  private final int zzaxe;
  
  public zzdb(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, int paramInt)
  {
    this.zzaxa = paramFloat1;
    this.zzaxb = paramFloat2;
    this.zzaxc = (paramFloat1 + paramFloat3);
    this.zzaxd = (paramFloat2 + paramFloat4);
    this.zzaxe = paramInt;
  }
  
  float zzjm()
  {
    return this.zzaxa;
  }
  
  float zzjn()
  {
    return this.zzaxb;
  }
  
  float zzjo()
  {
    return this.zzaxc;
  }
  
  float zzjp()
  {
    return this.zzaxd;
  }
  
  int zzjq()
  {
    return this.zzaxe;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzdb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */