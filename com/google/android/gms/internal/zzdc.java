package com.google.android.gms.internal;

import android.support.annotation.Nullable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@zzji
public abstract class zzdc
{
  @Nullable
  private static MessageDigest zzaxf = null;
  protected Object zzako = new Object();
  
  abstract byte[] zzag(String paramString);
  
  @Nullable
  protected MessageDigest zzjr()
  {
    for (;;)
    {
      MessageDigest localMessageDigest;
      int i;
      synchronized (this.zzako)
      {
        if (zzaxf != null)
        {
          localMessageDigest = zzaxf;
          return localMessageDigest;
        }
        i = 0;
        if (i >= 2) {}
      }
      try
      {
        zzaxf = MessageDigest.getInstance("MD5");
        i += 1;
        continue;
        localMessageDigest = zzaxf;
        return localMessageDigest;
        localObject2 = finally;
        throw ((Throwable)localObject2);
      }
      catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
      {
        for (;;) {}
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzdc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */