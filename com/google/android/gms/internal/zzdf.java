package com.google.android.gms.internal;

import java.security.MessageDigest;

@zzji
public class zzdf
  extends zzdc
{
  private MessageDigest zzaxn;
  
  byte[] zza(String[] paramArrayOfString)
  {
    int i = 0;
    Object localObject;
    if (paramArrayOfString.length == 1)
    {
      localObject = zzde.zzp(zzde.zzai(paramArrayOfString[0]));
      return (byte[])localObject;
    }
    if (paramArrayOfString.length < 5)
    {
      localObject = new byte[paramArrayOfString.length * 2];
      i = 0;
      while (i < paramArrayOfString.length)
      {
        arrayOfByte = zzs(zzde.zzai(paramArrayOfString[i]));
        localObject[(i * 2)] = arrayOfByte[0];
        localObject[(i * 2 + 1)] = arrayOfByte[1];
        i += 1;
      }
      return (byte[])localObject;
    }
    byte[] arrayOfByte = new byte[paramArrayOfString.length];
    for (;;)
    {
      localObject = arrayOfByte;
      if (i >= paramArrayOfString.length) {
        break;
      }
      arrayOfByte[i] = zzr(zzde.zzai(paramArrayOfString[i]));
      i += 1;
    }
  }
  
  public byte[] zzag(String arg1)
  {
    int i = 4;
    byte[] arrayOfByte1 = zza(???.split(" "));
    this.zzaxn = zzjr();
    for (;;)
    {
      synchronized (this.zzako)
      {
        if (this.zzaxn == null) {
          return new byte[0];
        }
        this.zzaxn.reset();
        this.zzaxn.update(arrayOfByte1);
        arrayOfByte1 = this.zzaxn.digest();
        if (arrayOfByte1.length > 4)
        {
          byte[] arrayOfByte2 = new byte[i];
          System.arraycopy(arrayOfByte1, 0, arrayOfByte2, 0, arrayOfByte2.length);
          return arrayOfByte2;
        }
      }
      i = localObject.length;
    }
  }
  
  byte zzr(int paramInt)
  {
    return (byte)(paramInt & 0xFF ^ (0xFF00 & paramInt) >> 8 ^ (0xFF0000 & paramInt) >> 16 ^ (0xFF000000 & paramInt) >> 24);
  }
  
  byte[] zzs(int paramInt)
  {
    paramInt = 0xFFFF & paramInt ^ (0xFFFF0000 & paramInt) >> 16;
    return new byte[] { (byte)paramInt, (byte)(paramInt >> 8) };
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzdf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */