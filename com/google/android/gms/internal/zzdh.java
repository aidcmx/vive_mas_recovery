package com.google.android.gms.internal;

import java.nio.charset.Charset;
import java.security.MessageDigest;

@zzji
public class zzdh
  extends zzdc
{
  private MessageDigest zzaxn;
  private final int zzaxq;
  private final int zzaxr;
  
  public zzdh(int paramInt)
  {
    int j = paramInt / 8;
    int i = j;
    if (paramInt % 8 > 0) {
      i = j + 1;
    }
    this.zzaxq = i;
    this.zzaxr = paramInt;
  }
  
  public byte[] zzag(String paramString)
  {
    int j = 0;
    for (;;)
    {
      int i;
      byte[] arrayOfByte;
      long l2;
      synchronized (this.zzako)
      {
        this.zzaxn = zzjr();
        if (this.zzaxn == null) {
          return new byte[0];
        }
        this.zzaxn.reset();
        this.zzaxn.update(paramString.getBytes(Charset.forName("UTF-8")));
        paramString = this.zzaxn.digest();
        if (paramString.length > this.zzaxq)
        {
          i = this.zzaxq;
          arrayOfByte = new byte[i];
          System.arraycopy(paramString, 0, arrayOfByte, 0, arrayOfByte.length);
          if (this.zzaxr % 8 <= 0) {
            continue;
          }
          l1 = 0L;
          i = j;
          if (i < arrayOfByte.length)
          {
            l2 = l1;
            if (i <= 0) {
              break label177;
            }
            l2 = l1 << 8;
            break label177;
          }
        }
        else
        {
          i = paramString.length;
          continue;
        }
        l1 >>>= 8 - this.zzaxr % 8;
        i = this.zzaxq - 1;
        break label198;
        return arrayOfByte;
      }
      label177:
      long l1 = l2 + (arrayOfByte[i] & 0xFF);
      i += 1;
      continue;
      label198:
      while (i >= 0)
      {
        arrayOfByte[i] = ((byte)(int)(0xFF & l1));
        l1 >>>= 8;
        i -= 1;
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzdh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */