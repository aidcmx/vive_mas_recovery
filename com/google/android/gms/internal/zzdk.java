package com.google.android.gms.internal;

import android.text.TextUtils;

@zzji
public final class zzdk
{
  private String zzbcl;
  
  public zzdk()
  {
    this((String)zzdr.zzbcx.zzlp());
  }
  
  public zzdk(String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {
      paramString = (String)zzdr.zzbcx.zzlp();
    }
    for (;;)
    {
      this.zzbcl = paramString;
      return;
    }
  }
  
  public String zzlo()
  {
    return this.zzbcl;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzdk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */