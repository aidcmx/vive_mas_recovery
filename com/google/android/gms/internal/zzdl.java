package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import java.util.Collections;
import java.util.List;

@zzji
public class zzdl
  implements zzdm
{
  public List<String> zza(AdRequestInfoParcel paramAdRequestInfoParcel)
  {
    if (paramAdRequestInfoParcel.zzckj == null) {
      return Collections.emptyList();
    }
    return paramAdRequestInfoParcel.zzckj;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzdl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */