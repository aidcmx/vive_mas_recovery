package com.google.android.gms.internal;

import android.content.SharedPreferences;
import com.google.android.gms.ads.internal.zzu;

@zzji
public abstract class zzdn<T>
{
  private final int zzbcm;
  private final String zzbcn;
  private final T zzbco;
  
  private zzdn(int paramInt, String paramString, T paramT)
  {
    this.zzbcm = paramInt;
    this.zzbcn = paramString;
    this.zzbco = paramT;
    zzu.zzgx().zza(this);
  }
  
  public static zzdn<String> zza(int paramInt, String paramString)
  {
    paramString = zza(paramInt, paramString, null);
    zzu.zzgx().zzb(paramString);
    return paramString;
  }
  
  public static zzdn<Float> zza(int paramInt, String paramString, float paramFloat)
  {
    return new zzdn.4(paramInt, paramString, Float.valueOf(paramFloat));
  }
  
  public static zzdn<Integer> zza(int paramInt1, String paramString, int paramInt2)
  {
    return new zzdn.2(paramInt1, paramString, Integer.valueOf(paramInt2));
  }
  
  public static zzdn<Long> zza(int paramInt, String paramString, long paramLong)
  {
    return new zzdn.3(paramInt, paramString, Long.valueOf(paramLong));
  }
  
  public static zzdn<Boolean> zza(int paramInt, String paramString, Boolean paramBoolean)
  {
    return new zzdn.1(paramInt, paramString, paramBoolean);
  }
  
  public static zzdn<String> zza(int paramInt, String paramString1, String paramString2)
  {
    return new zzdn.5(paramInt, paramString1, paramString2);
  }
  
  public static zzdn<String> zzb(int paramInt, String paramString)
  {
    paramString = zza(paramInt, paramString, null);
    zzu.zzgx().zzc(paramString);
    return paramString;
  }
  
  public T get()
  {
    return (T)zzu.zzgy().zzd(this);
  }
  
  public String getKey()
  {
    return this.zzbcn;
  }
  
  protected abstract T zza(SharedPreferences paramSharedPreferences);
  
  public T zzlp()
  {
    return (T)this.zzbco;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzdn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */