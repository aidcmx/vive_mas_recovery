package com.google.android.gms.internal;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

@zzji
public class zzdo
{
  private final Collection<zzdn> zzbcp = new ArrayList();
  private final Collection<zzdn<String>> zzbcq = new ArrayList();
  private final Collection<zzdn<String>> zzbcr = new ArrayList();
  
  public void zza(zzdn paramzzdn)
  {
    this.zzbcp.add(paramzzdn);
  }
  
  public void zzb(zzdn<String> paramzzdn)
  {
    this.zzbcq.add(paramzzdn);
  }
  
  public void zzc(zzdn<String> paramzzdn)
  {
    this.zzbcr.add(paramzzdn);
  }
  
  public List<String> zzlq()
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = this.zzbcq.iterator();
    while (localIterator.hasNext())
    {
      String str = (String)((zzdn)localIterator.next()).get();
      if (str != null) {
        localArrayList.add(str);
      }
    }
    return localArrayList;
  }
  
  public List<String> zzlr()
  {
    List localList = zzlq();
    Iterator localIterator = this.zzbcr.iterator();
    while (localIterator.hasNext())
    {
      String str = (String)((zzdn)localIterator.next()).get();
      if (str != null) {
        localList.add(str);
      }
    }
    return localList;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzdo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */