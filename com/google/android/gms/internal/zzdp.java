package com.google.android.gms.internal;

import android.content.Context;
import android.content.SharedPreferences;

@zzji
public class zzdp
{
  public SharedPreferences zzm(Context paramContext)
  {
    return paramContext.getSharedPreferences("google_ads_flags", 1);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzdp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */