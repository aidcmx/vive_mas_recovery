package com.google.android.gms.internal;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.ConditionVariable;
import android.support.annotation.Nullable;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.common.zze;

@zzji
public class zzdq
{
  private final Object zzako = new Object();
  private volatile boolean zzaoz = false;
  private final ConditionVariable zzbcs = new ConditionVariable();
  @Nullable
  private SharedPreferences zzbct = null;
  
  public void initialize(Context paramContext)
  {
    if (this.zzaoz) {
      return;
    }
    synchronized (this.zzako)
    {
      if (this.zzaoz) {
        return;
      }
    }
    try
    {
      paramContext = zze.getRemoteContext(paramContext);
      if (paramContext == null)
      {
        this.zzbcs.open();
        return;
      }
      this.zzbct = zzu.zzgw().zzm(paramContext);
      this.zzaoz = true;
      this.zzbcs.open();
      return;
    }
    finally
    {
      this.zzbcs.open();
    }
  }
  
  public <T> T zzd(zzdn<T> paramzzdn)
  {
    if (!this.zzbcs.block(5000L)) {
      throw new IllegalStateException("Flags.initialize() was not called!");
    }
    if (!this.zzaoz) {}
    synchronized (this.zzako)
    {
      if (!this.zzaoz)
      {
        paramzzdn = paramzzdn.zzlp();
        return paramzzdn;
      }
      return (T)zzlo.zzb(new zzdq.1(this, paramzzdn));
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzdq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */