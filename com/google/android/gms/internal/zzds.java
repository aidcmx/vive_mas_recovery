package com.google.android.gms.internal;

import android.content.Context;
import android.os.Build.VERSION;
import com.google.android.gms.ads.internal.zzu;
import java.util.LinkedHashMap;
import java.util.Map;

@zzji
public class zzds
{
  private Context mContext = null;
  private String zzasx = null;
  private boolean zzblg;
  private String zzblh;
  private Map<String, String> zzbli;
  
  public zzds(Context paramContext, String paramString)
  {
    this.mContext = paramContext;
    this.zzasx = paramString;
    this.zzblg = ((Boolean)zzdr.zzbeq.get()).booleanValue();
    this.zzblh = ((String)zzdr.zzber.get());
    this.zzbli = new LinkedHashMap();
    this.zzbli.put("s", "gmob_sdk");
    this.zzbli.put("v", "3");
    this.zzbli.put("os", Build.VERSION.RELEASE);
    this.zzbli.put("sdk", Build.VERSION.SDK);
    this.zzbli.put("device", zzu.zzgm().zzvt());
    Map localMap = this.zzbli;
    if (paramContext.getApplicationContext() != null)
    {
      paramString = paramContext.getApplicationContext().getPackageName();
      localMap.put("app", paramString);
      paramString = this.zzbli;
      if (!zzu.zzgm().zzaj(paramContext)) {
        break label256;
      }
    }
    label256:
    for (paramContext = "1";; paramContext = "0")
    {
      paramString.put("is_lite_sdk", paramContext);
      paramContext = zzu.zzgv().zzv(this.mContext);
      this.zzbli.put("network_coarse", Integer.toString(paramContext.zzcqe));
      this.zzbli.put("network_fine", Integer.toString(paramContext.zzcqf));
      return;
      paramString = paramContext.getPackageName();
      break;
    }
  }
  
  Context getContext()
  {
    return this.mContext;
  }
  
  String zzhz()
  {
    return this.zzasx;
  }
  
  boolean zzls()
  {
    return this.zzblg;
  }
  
  String zzlt()
  {
    return this.zzblh;
  }
  
  Map<String, String> zzlu()
  {
    return this.zzbli;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzds.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */