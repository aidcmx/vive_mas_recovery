package com.google.android.gms.internal;

import android.support.annotation.Nullable;
import android.text.TextUtils;

@zzji
public class zzdu
{
  @Nullable
  public zzdt zza(@Nullable zzds paramzzds)
  {
    if (paramzzds == null) {
      throw new IllegalArgumentException("CSI configuration can't be null. ");
    }
    if (!paramzzds.zzls())
    {
      zzkx.v("CsiReporterFactory: CSI is not enabled. No CSI reporter created.");
      return null;
    }
    if (paramzzds.getContext() == null) {
      throw new IllegalArgumentException("Context can't be null. Please set up context in CsiConfiguration.");
    }
    if (TextUtils.isEmpty(paramzzds.zzhz())) {
      throw new IllegalArgumentException("AfmaVersion can't be null or empty. Please set up afmaVersion in CsiConfiguration.");
    }
    return new zzdt(paramzzds.getContext(), paramzzds.zzhz(), paramzzds.zzlt(), paramzzds.zzlu());
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzdu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */