package com.google.android.gms.internal;

import android.support.annotation.Nullable;

@zzji
public class zzdv
{
  @Nullable
  public static zzdx zza(@Nullable zzdz paramzzdz, long paramLong)
  {
    if (paramzzdz == null) {
      return null;
    }
    return paramzzdz.zzc(paramLong);
  }
  
  public static boolean zza(@Nullable zzdz paramzzdz, @Nullable zzdx paramzzdx, long paramLong, String... paramVarArgs)
  {
    if ((paramzzdz == null) || (paramzzdx == null)) {
      return false;
    }
    return paramzzdz.zza(paramzzdx, paramLong, paramVarArgs);
  }
  
  public static boolean zza(@Nullable zzdz paramzzdz, @Nullable zzdx paramzzdx, String... paramVarArgs)
  {
    if ((paramzzdz == null) || (paramzzdx == null)) {
      return false;
    }
    return paramzzdz.zza(paramzzdx, paramVarArgs);
  }
  
  @Nullable
  public static zzdx zzb(@Nullable zzdz paramzzdz)
  {
    if (paramzzdz == null) {
      return null;
    }
    return paramzzdz.zzlz();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzdv.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */