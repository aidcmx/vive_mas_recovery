package com.google.android.gms.internal;

import android.support.annotation.Nullable;
import java.util.Map;

@zzji
public abstract class zzdw
{
  @zzji
  public static final zzdw zzblq = new zzdw.1();
  @zzji
  public static final zzdw zzblr = new zzdw.2();
  @zzji
  public static final zzdw zzbls = new zzdw.3();
  
  public final void zza(Map<String, String> paramMap, String paramString1, String paramString2)
  {
    paramMap.put(paramString1, zzf((String)paramMap.get(paramString1), paramString2));
  }
  
  public abstract String zzf(@Nullable String paramString1, String paramString2);
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzdw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */