package com.google.android.gms.internal;

import android.support.annotation.Nullable;

@zzji
public class zzdx
{
  private final long zzblt;
  @Nullable
  private final String zzblu;
  @Nullable
  private final zzdx zzblv;
  
  public zzdx(long paramLong, @Nullable String paramString, @Nullable zzdx paramzzdx)
  {
    this.zzblt = paramLong;
    this.zzblu = paramString;
    this.zzblv = paramzzdx;
  }
  
  long getTime()
  {
    return this.zzblt;
  }
  
  String zzlw()
  {
    return this.zzblu;
  }
  
  @Nullable
  zzdx zzlx()
  {
    return this.zzblv;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzdx.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */