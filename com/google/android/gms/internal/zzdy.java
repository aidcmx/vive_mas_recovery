package com.google.android.gms.internal;

import android.support.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;

@zzji
public class zzdy
{
  @Nullable
  private final zzdz zzalt;
  private final Map<String, zzdx> zzblw;
  
  public zzdy(@Nullable zzdz paramzzdz)
  {
    this.zzalt = paramzzdz;
    this.zzblw = new HashMap();
  }
  
  public void zza(String paramString, zzdx paramzzdx)
  {
    this.zzblw.put(paramString, paramzzdx);
  }
  
  public void zza(String paramString1, String paramString2, long paramLong)
  {
    zzdv.zza(this.zzalt, (zzdx)this.zzblw.get(paramString2), paramLong, new String[] { paramString1 });
    this.zzblw.put(paramString1, zzdv.zza(this.zzalt, paramLong));
  }
  
  @Nullable
  public zzdz zzly()
  {
    return this.zzalt;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzdy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */