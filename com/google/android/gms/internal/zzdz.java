package com.google.android.gms.internal;

import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.common.util.zze;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@zzji
public class zzdz
{
  private final Object zzako = new Object();
  boolean zzblg;
  private final List<zzdx> zzblx = new LinkedList();
  private final Map<String, String> zzbly = new LinkedHashMap();
  private String zzblz;
  private zzdx zzbma;
  @Nullable
  private zzdz zzbmb;
  
  public zzdz(boolean paramBoolean, String paramString1, String paramString2)
  {
    this.zzblg = paramBoolean;
    this.zzbly.put("action", paramString1);
    this.zzbly.put("ad_format", paramString2);
  }
  
  public boolean zza(zzdx paramzzdx, long paramLong, String... paramVarArgs)
  {
    synchronized (this.zzako)
    {
      int j = paramVarArgs.length;
      int i = 0;
      while (i < j)
      {
        zzdx localzzdx = new zzdx(paramLong, paramVarArgs[i], paramzzdx);
        this.zzblx.add(localzzdx);
        i += 1;
      }
      return true;
    }
  }
  
  public boolean zza(@Nullable zzdx paramzzdx, String... paramVarArgs)
  {
    if ((!this.zzblg) || (paramzzdx == null)) {
      return false;
    }
    return zza(paramzzdx, zzu.zzgs().elapsedRealtime(), paramVarArgs);
  }
  
  public void zzaz(String paramString)
  {
    if (!this.zzblg) {
      return;
    }
    synchronized (this.zzako)
    {
      this.zzblz = paramString;
      return;
    }
  }
  
  @Nullable
  public zzdx zzc(long paramLong)
  {
    if (!this.zzblg) {
      return null;
    }
    return new zzdx(paramLong, null, null);
  }
  
  public void zzc(@Nullable zzdz paramzzdz)
  {
    synchronized (this.zzako)
    {
      this.zzbmb = paramzzdz;
      return;
    }
  }
  
  public void zzg(String paramString1, String paramString2)
  {
    if ((!this.zzblg) || (TextUtils.isEmpty(paramString2))) {}
    zzdt localzzdt;
    do
    {
      return;
      localzzdt = zzu.zzgq().zzuu();
    } while (localzzdt == null);
    synchronized (this.zzako)
    {
      localzzdt.zzax(paramString1).zza(this.zzbly, paramString1, paramString2);
      return;
    }
  }
  
  public zzdx zzlz()
  {
    return zzc(zzu.zzgs().elapsedRealtime());
  }
  
  public void zzma()
  {
    synchronized (this.zzako)
    {
      this.zzbma = zzlz();
      return;
    }
  }
  
  public String zzmb()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    synchronized (this.zzako)
    {
      Iterator localIterator = this.zzblx.iterator();
      while (localIterator.hasNext())
      {
        zzdx localzzdx = (zzdx)localIterator.next();
        long l1 = localzzdx.getTime();
        String str2 = localzzdx.zzlw();
        localzzdx = localzzdx.zzlx();
        if ((localzzdx != null) && (l1 > 0L))
        {
          long l2 = localzzdx.getTime();
          localStringBuilder.append(str2).append('.').append(l1 - l2).append(',');
        }
      }
    }
    this.zzblx.clear();
    if (!TextUtils.isEmpty(this.zzblz)) {
      ((StringBuilder)localObject2).append(this.zzblz);
    }
    for (;;)
    {
      String str1 = ((StringBuilder)localObject2).toString();
      return str1;
      if (str1.length() > 0) {
        str1.setLength(str1.length() - 1);
      }
    }
  }
  
  Map<String, String> zzmc()
  {
    synchronized (this.zzako)
    {
      Object localObject2 = zzu.zzgq().zzuu();
      if ((localObject2 == null) || (this.zzbmb == null))
      {
        localObject2 = this.zzbly;
        return (Map<String, String>)localObject2;
      }
      localObject2 = ((zzdt)localObject2).zza(this.zzbly, this.zzbmb.zzmc());
      return (Map<String, String>)localObject2;
    }
  }
  
  public zzdx zzmd()
  {
    synchronized (this.zzako)
    {
      zzdx localzzdx = this.zzbma;
      return localzzdx;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzdz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */