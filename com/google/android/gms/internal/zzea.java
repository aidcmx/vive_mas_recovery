package com.google.android.gms.internal;

import android.support.annotation.Nullable;
import android.view.View;
import com.google.android.gms.ads.internal.zzh;
import com.google.android.gms.dynamic.zzd;
import com.google.android.gms.dynamic.zze;

@zzji
public final class zzea
  extends zzec.zza
{
  private final zzh zzbmc;
  @Nullable
  private final String zzbmd;
  private final String zzbme;
  
  public zzea(zzh paramzzh, @Nullable String paramString1, String paramString2)
  {
    this.zzbmc = paramzzh;
    this.zzbmd = paramString1;
    this.zzbme = paramString2;
  }
  
  public String getContent()
  {
    return this.zzbme;
  }
  
  public void recordClick()
  {
    this.zzbmc.zzfa();
  }
  
  public void recordImpression()
  {
    this.zzbmc.zzfb();
  }
  
  public void zzi(@Nullable zzd paramzzd)
  {
    if (paramzzd == null) {
      return;
    }
    this.zzbmc.zzc((View)zze.zzae(paramzzd));
  }
  
  public String zzme()
  {
    return this.zzbmd;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzea.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */