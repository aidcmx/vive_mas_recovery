package com.google.android.gms.internal;

import android.os.RemoteException;
import android.view.View;
import com.google.android.gms.ads.doubleclick.CustomRenderedAd;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.dynamic.zze;

@zzji
public class zzeb
  implements CustomRenderedAd
{
  private final zzec zzbmf;
  
  public zzeb(zzec paramzzec)
  {
    this.zzbmf = paramzzec;
  }
  
  public String getBaseUrl()
  {
    try
    {
      String str = this.zzbmf.zzme();
      return str;
    }
    catch (RemoteException localRemoteException)
    {
      zzb.zzc("Could not delegate getBaseURL to CustomRenderedAd", localRemoteException);
    }
    return null;
  }
  
  public String getContent()
  {
    try
    {
      String str = this.zzbmf.getContent();
      return str;
    }
    catch (RemoteException localRemoteException)
    {
      zzb.zzc("Could not delegate getContent to CustomRenderedAd", localRemoteException);
    }
    return null;
  }
  
  public void onAdRendered(View paramView)
  {
    try
    {
      zzec localzzec = this.zzbmf;
      if (paramView != null) {}
      for (paramView = zze.zzac(paramView);; paramView = null)
      {
        localzzec.zzi(paramView);
        return;
      }
      return;
    }
    catch (RemoteException paramView)
    {
      zzb.zzc("Could not delegate onAdRendered to CustomRenderedAd", paramView);
    }
  }
  
  public void recordClick()
  {
    try
    {
      this.zzbmf.recordClick();
      return;
    }
    catch (RemoteException localRemoteException)
    {
      zzb.zzc("Could not delegate recordClick to CustomRenderedAd", localRemoteException);
    }
  }
  
  public void recordImpression()
  {
    try
    {
      this.zzbmf.recordImpression();
      return;
    }
    catch (RemoteException localRemoteException)
    {
      zzb.zzc("Could not delegate recordImpression to CustomRenderedAd", localRemoteException);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzeb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */