package com.google.android.gms.internal;

import com.google.android.gms.ads.doubleclick.OnCustomRenderedAdLoadedListener;

@zzji
public final class zzee
  extends zzed.zza
{
  private final OnCustomRenderedAdLoadedListener zzbbh;
  
  public zzee(OnCustomRenderedAdLoadedListener paramOnCustomRenderedAdLoadedListener)
  {
    this.zzbbh = paramOnCustomRenderedAdLoadedListener;
  }
  
  public void zza(zzec paramzzec)
  {
    this.zzbbh.onCustomRenderedAdLoaded(new zzeb(paramzzec));
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzee.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */