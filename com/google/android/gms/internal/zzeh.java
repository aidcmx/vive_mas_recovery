package com.google.android.gms.internal;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.RemoteException;
import com.google.android.gms.ads.formats.NativeAd.Image;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.dynamic.zze;

@zzji
public class zzeh
  extends NativeAd.Image
{
  private final Drawable mDrawable;
  private final Uri mUri;
  private final double zzbmx;
  private final zzeg zzbox;
  
  public zzeh(zzeg paramzzeg)
  {
    this.zzbox = paramzzeg;
    try
    {
      paramzzeg = this.zzbox.zzmn();
      if (paramzzeg == null) {
        break label83;
      }
      paramzzeg = (Drawable)zze.zzae(paramzzeg);
    }
    catch (RemoteException paramzzeg)
    {
      try
      {
        paramzzeg = this.zzbox.getUri();
        this.mUri = paramzzeg;
        double d1 = 1.0D;
        try
        {
          double d2 = this.zzbox.getScale();
          d1 = d2;
        }
        catch (RemoteException paramzzeg)
        {
          for (;;)
          {
            zzb.zzb("Failed to get scale.", paramzzeg);
          }
        }
        this.zzbmx = d1;
        return;
        paramzzeg = paramzzeg;
        zzb.zzb("Failed to get drawable.", paramzzeg);
        paramzzeg = null;
      }
      catch (RemoteException paramzzeg)
      {
        for (;;)
        {
          zzb.zzb("Failed to get uri.", paramzzeg);
          paramzzeg = (zzeg)localObject;
        }
      }
    }
    this.mDrawable = paramzzeg;
  }
  
  public Drawable getDrawable()
  {
    return this.mDrawable;
  }
  
  public double getScale()
  {
    return this.zzbmx;
  }
  
  public Uri getUri()
  {
    return this.mUri;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzeh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */