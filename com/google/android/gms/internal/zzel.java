package com.google.android.gms.internal;

import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.formats.NativeAd.Image;
import com.google.android.gms.ads.formats.NativeAppInstallAd;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.dynamic.zzd;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@zzji
public class zzel
  extends NativeAppInstallAd
{
  private VideoController zzbbc = new VideoController();
  private final zzek zzboy;
  private final List<NativeAd.Image> zzboz = new ArrayList();
  private final zzeh zzbpa;
  
  public zzel(zzek paramzzek)
  {
    this.zzboy = paramzzek;
    try
    {
      paramzzek = this.zzboy.getImages();
      if (paramzzek != null)
      {
        paramzzek = paramzzek.iterator();
        while (paramzzek.hasNext())
        {
          zzeg localzzeg = zze(paramzzek.next());
          if (localzzeg != null) {
            this.zzboz.add(new zzeh(localzzeg));
          }
        }
      }
      try
      {
        paramzzek = this.zzboy.zzmo();
        if (paramzzek == null) {
          break label140;
        }
        paramzzek = new zzeh(paramzzek);
      }
      catch (RemoteException paramzzek)
      {
        for (;;)
        {
          zzb.zzb("Failed to get icon.", paramzzek);
          paramzzek = null;
        }
      }
    }
    catch (RemoteException paramzzek)
    {
      zzb.zzb("Failed to get image.", paramzzek);
    }
    this.zzbpa = paramzzek;
  }
  
  public void destroy()
  {
    try
    {
      this.zzboy.destroy();
      return;
    }
    catch (RemoteException localRemoteException)
    {
      zzb.zzb("Failed to destroy", localRemoteException);
    }
  }
  
  public CharSequence getBody()
  {
    try
    {
      String str = this.zzboy.getBody();
      return str;
    }
    catch (RemoteException localRemoteException)
    {
      zzb.zzb("Failed to get body.", localRemoteException);
    }
    return null;
  }
  
  public CharSequence getCallToAction()
  {
    try
    {
      String str = this.zzboy.getCallToAction();
      return str;
    }
    catch (RemoteException localRemoteException)
    {
      zzb.zzb("Failed to get call to action.", localRemoteException);
    }
    return null;
  }
  
  public Bundle getExtras()
  {
    try
    {
      Bundle localBundle = this.zzboy.getExtras();
      return localBundle;
    }
    catch (RemoteException localRemoteException)
    {
      zzb.zzb("Failed to get extras", localRemoteException);
    }
    return null;
  }
  
  public CharSequence getHeadline()
  {
    try
    {
      String str = this.zzboy.getHeadline();
      return str;
    }
    catch (RemoteException localRemoteException)
    {
      zzb.zzb("Failed to get headline.", localRemoteException);
    }
    return null;
  }
  
  public NativeAd.Image getIcon()
  {
    return this.zzbpa;
  }
  
  public List<NativeAd.Image> getImages()
  {
    return this.zzboz;
  }
  
  public CharSequence getPrice()
  {
    try
    {
      String str = this.zzboy.getPrice();
      return str;
    }
    catch (RemoteException localRemoteException)
    {
      zzb.zzb("Failed to get price.", localRemoteException);
    }
    return null;
  }
  
  public Double getStarRating()
  {
    try
    {
      double d = this.zzboy.getStarRating();
      if (d == -1.0D) {
        return null;
      }
      return Double.valueOf(d);
    }
    catch (RemoteException localRemoteException)
    {
      zzb.zzb("Failed to get star rating.", localRemoteException);
    }
    return null;
  }
  
  public CharSequence getStore()
  {
    try
    {
      String str = this.zzboy.getStore();
      return str;
    }
    catch (RemoteException localRemoteException)
    {
      zzb.zzb("Failed to get store", localRemoteException);
    }
    return null;
  }
  
  public VideoController getVideoController()
  {
    try
    {
      if (this.zzboy.zzej() != null) {
        this.zzbbc.zza(this.zzboy.zzej());
      }
      return this.zzbbc;
    }
    catch (RemoteException localRemoteException)
    {
      for (;;)
      {
        zzb.zzb("Exception occurred while getting video controller", localRemoteException);
      }
    }
  }
  
  zzeg zze(Object paramObject)
  {
    if ((paramObject instanceof IBinder)) {
      return zzeg.zza.zzab((IBinder)paramObject);
    }
    return null;
  }
  
  protected zzd zzmp()
  {
    try
    {
      zzd localzzd = this.zzboy.zzmp();
      return localzzd;
    }
    catch (RemoteException localRemoteException)
    {
      zzb.zzb("Failed to retrieve native ad engine.", localRemoteException);
    }
    return null;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */