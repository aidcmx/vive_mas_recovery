package com.google.android.gms.internal;

import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import com.google.android.gms.ads.formats.NativeAd.Image;
import com.google.android.gms.ads.formats.NativeContentAd;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.dynamic.zzd;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@zzji
public class zzen
  extends NativeContentAd
{
  private final List<NativeAd.Image> zzboz = new ArrayList();
  private final zzem zzbpb;
  private final zzeh zzbpc;
  
  public zzen(zzem paramzzem)
  {
    this.zzbpb = paramzzem;
    try
    {
      paramzzem = this.zzbpb.getImages();
      if (paramzzem != null)
      {
        paramzzem = paramzzem.iterator();
        while (paramzzem.hasNext())
        {
          zzeg localzzeg = zze(paramzzem.next());
          if (localzzeg != null) {
            this.zzboz.add(new zzeh(localzzeg));
          }
        }
      }
      try
      {
        paramzzem = this.zzbpb.zzmt();
        if (paramzzem == null) {
          break label129;
        }
        paramzzem = new zzeh(paramzzem);
      }
      catch (RemoteException paramzzem)
      {
        for (;;)
        {
          zzb.zzb("Failed to get icon.", paramzzem);
          paramzzem = null;
        }
      }
    }
    catch (RemoteException paramzzem)
    {
      zzb.zzb("Failed to get image.", paramzzem);
    }
    this.zzbpc = paramzzem;
  }
  
  public void destroy()
  {
    try
    {
      this.zzbpb.destroy();
      return;
    }
    catch (RemoteException localRemoteException)
    {
      zzb.zzb("Failed to destroy", localRemoteException);
    }
  }
  
  public CharSequence getAdvertiser()
  {
    try
    {
      String str = this.zzbpb.getAdvertiser();
      return str;
    }
    catch (RemoteException localRemoteException)
    {
      zzb.zzb("Failed to get attribution.", localRemoteException);
    }
    return null;
  }
  
  public CharSequence getBody()
  {
    try
    {
      String str = this.zzbpb.getBody();
      return str;
    }
    catch (RemoteException localRemoteException)
    {
      zzb.zzb("Failed to get body.", localRemoteException);
    }
    return null;
  }
  
  public CharSequence getCallToAction()
  {
    try
    {
      String str = this.zzbpb.getCallToAction();
      return str;
    }
    catch (RemoteException localRemoteException)
    {
      zzb.zzb("Failed to get call to action.", localRemoteException);
    }
    return null;
  }
  
  public Bundle getExtras()
  {
    try
    {
      Bundle localBundle = this.zzbpb.getExtras();
      return localBundle;
    }
    catch (RemoteException localRemoteException)
    {
      zzb.zzc("Failed to get extras", localRemoteException);
    }
    return null;
  }
  
  public CharSequence getHeadline()
  {
    try
    {
      String str = this.zzbpb.getHeadline();
      return str;
    }
    catch (RemoteException localRemoteException)
    {
      zzb.zzb("Failed to get headline.", localRemoteException);
    }
    return null;
  }
  
  public List<NativeAd.Image> getImages()
  {
    return this.zzboz;
  }
  
  public NativeAd.Image getLogo()
  {
    return this.zzbpc;
  }
  
  zzeg zze(Object paramObject)
  {
    if ((paramObject instanceof IBinder)) {
      return zzeg.zza.zzab((IBinder)paramObject);
    }
    return null;
  }
  
  protected zzd zzmp()
  {
    try
    {
      zzd localzzd = this.zzbpb.zzmp();
      return localzzd;
    }
    catch (RemoteException localRemoteException)
    {
      zzb.zzb("Failed to retrieve native ad engine.", localRemoteException);
    }
    return null;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzen.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */