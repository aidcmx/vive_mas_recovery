package com.google.android.gms.internal;

import android.content.Context;
import android.os.IBinder;
import android.os.RemoteException;
import android.widget.FrameLayout;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.dynamic.zzd;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.dynamic.zzg;
import com.google.android.gms.dynamic.zzg.zza;

@zzji
public class zzeu
  extends zzg<zzej>
{
  public zzeu()
  {
    super("com.google.android.gms.ads.NativeAdViewDelegateCreatorImpl");
  }
  
  protected zzej zzal(IBinder paramIBinder)
  {
    return zzej.zza.zzad(paramIBinder);
  }
  
  public zzei zzb(Context paramContext, FrameLayout paramFrameLayout1, FrameLayout paramFrameLayout2)
  {
    try
    {
      zzd localzzd = zze.zzac(paramContext);
      paramFrameLayout1 = zze.zzac(paramFrameLayout1);
      paramFrameLayout2 = zze.zzac(paramFrameLayout2);
      paramContext = zzei.zza.zzac(((zzej)zzcr(paramContext)).zza(localzzd, paramFrameLayout1, paramFrameLayout2, 9877000));
      return paramContext;
    }
    catch (RemoteException paramContext)
    {
      zzb.zzc("Could not create remote NativeAdViewDelegate.", paramContext);
      return null;
    }
    catch (zzg.zza paramContext)
    {
      for (;;) {}
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzeu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */