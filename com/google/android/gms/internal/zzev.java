package com.google.android.gms.internal;

import com.google.android.gms.ads.formats.NativeAppInstallAd.OnAppInstallAdLoadedListener;

@zzji
public class zzev
  extends zzeq.zza
{
  private final NativeAppInstallAd.OnAppInstallAdLoadedListener zzbpe;
  
  public zzev(NativeAppInstallAd.OnAppInstallAdLoadedListener paramOnAppInstallAdLoadedListener)
  {
    this.zzbpe = paramOnAppInstallAdLoadedListener;
  }
  
  public void zza(zzek paramzzek)
  {
    this.zzbpe.onAppInstallAdLoaded(zzb(paramzzek));
  }
  
  zzel zzb(zzek paramzzek)
  {
    return new zzel(paramzzek);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzev.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */