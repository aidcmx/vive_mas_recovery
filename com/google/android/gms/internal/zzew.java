package com.google.android.gms.internal;

import com.google.android.gms.ads.formats.NativeContentAd.OnContentAdLoadedListener;

@zzji
public class zzew
  extends zzer.zza
{
  private final NativeContentAd.OnContentAdLoadedListener zzbpf;
  
  public zzew(NativeContentAd.OnContentAdLoadedListener paramOnContentAdLoadedListener)
  {
    this.zzbpf = paramOnContentAdLoadedListener;
  }
  
  public void zza(zzem paramzzem)
  {
    this.zzbpf.onContentAdLoaded(zzb(paramzzem));
  }
  
  zzen zzb(zzem paramzzem)
  {
    return new zzen(paramzzem);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzew.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */