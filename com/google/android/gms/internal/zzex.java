package com.google.android.gms.internal;

import com.google.android.gms.ads.formats.NativeCustomTemplateAd.OnCustomClickListener;

@zzji
public class zzex
  extends zzes.zza
{
  private final NativeCustomTemplateAd.OnCustomClickListener zzbpg;
  
  public zzex(NativeCustomTemplateAd.OnCustomClickListener paramOnCustomClickListener)
  {
    this.zzbpg = paramOnCustomClickListener;
  }
  
  public void zza(zzeo paramzzeo, String paramString)
  {
    this.zzbpg.onCustomClick(new zzep(paramzzeo), paramString);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzex.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */