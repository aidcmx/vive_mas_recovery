package com.google.android.gms.internal;

import com.google.android.gms.ads.formats.NativeCustomTemplateAd.OnCustomTemplateAdLoadedListener;

@zzji
public class zzey
  extends zzet.zza
{
  private final NativeCustomTemplateAd.OnCustomTemplateAdLoadedListener zzbph;
  
  public zzey(NativeCustomTemplateAd.OnCustomTemplateAdLoadedListener paramOnCustomTemplateAdLoadedListener)
  {
    this.zzbph = paramOnCustomTemplateAdLoadedListener;
  }
  
  public void zza(zzeo paramzzeo)
  {
    this.zzbph.onCustomTemplateAdLoaded(new zzep(paramzzeo));
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzey.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */