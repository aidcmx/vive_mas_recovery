package com.google.android.gms.internal;

import java.util.Map;

@zzji
public final class zzez
  implements zzfe
{
  private final zzfa zzbpi;
  
  public zzez(zzfa paramzzfa)
  {
    this.zzbpi = paramzzfa;
  }
  
  public void zza(zzmd paramzzmd, Map<String, String> paramMap)
  {
    paramzzmd = (String)paramMap.get("name");
    if (paramzzmd == null)
    {
      zzkx.zzdi("App event with no name parameter.");
      return;
    }
    this.zzbpi.onAppEvent(paramzzmd, (String)paramMap.get("info"));
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzez.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */