package com.google.android.gms.internal;

import android.text.TextUtils;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.common.util.zze;
import java.util.Map;

@zzji
public final class zzfc
  implements zzfe
{
  private void zzc(zzmd paramzzmd, Map<String, String> paramMap)
  {
    String str2 = (String)paramMap.get("label");
    String str1 = (String)paramMap.get("start_label");
    paramMap = (String)paramMap.get("timestamp");
    if (TextUtils.isEmpty(str2))
    {
      zzkx.zzdi("No label given for CSI tick.");
      return;
    }
    if (TextUtils.isEmpty(paramMap))
    {
      zzkx.zzdi("No timestamp given for CSI tick.");
      return;
    }
    try
    {
      long l = zzd(Long.parseLong(paramMap));
      paramMap = str1;
      if (TextUtils.isEmpty(str1)) {
        paramMap = "native:view_load";
      }
      paramzzmd.zzxm().zza(str2, paramMap, l);
      return;
    }
    catch (NumberFormatException paramzzmd)
    {
      zzkx.zzc("Malformed timestamp for CSI tick.", paramzzmd);
    }
  }
  
  private long zzd(long paramLong)
  {
    return paramLong - zzu.zzgs().currentTimeMillis() + zzu.zzgs().elapsedRealtime();
  }
  
  private void zzd(zzmd paramzzmd, Map<String, String> paramMap)
  {
    paramMap = (String)paramMap.get("value");
    if (TextUtils.isEmpty(paramMap))
    {
      zzkx.zzdi("No value given for CSI experiment.");
      return;
    }
    paramzzmd = paramzzmd.zzxm().zzly();
    if (paramzzmd == null)
    {
      zzkx.zzdi("No ticker for WebView, dropping experiment ID.");
      return;
    }
    paramzzmd.zzg("e", paramMap);
  }
  
  private void zze(zzmd paramzzmd, Map<String, String> paramMap)
  {
    String str = (String)paramMap.get("name");
    paramMap = (String)paramMap.get("value");
    if (TextUtils.isEmpty(paramMap))
    {
      zzkx.zzdi("No value given for CSI extra.");
      return;
    }
    if (TextUtils.isEmpty(str))
    {
      zzkx.zzdi("No name given for CSI extra.");
      return;
    }
    paramzzmd = paramzzmd.zzxm().zzly();
    if (paramzzmd == null)
    {
      zzkx.zzdi("No ticker for WebView, dropping extra parameter.");
      return;
    }
    paramzzmd.zzg(str, paramMap);
  }
  
  public void zza(zzmd paramzzmd, Map<String, String> paramMap)
  {
    String str = (String)paramMap.get("action");
    if ("tick".equals(str)) {
      zzc(paramzzmd, paramMap);
    }
    do
    {
      return;
      if ("experiment".equals(str))
      {
        zzd(paramzzmd, paramMap);
        return;
      }
    } while (!"extra".equals(str));
    zze(paramzzmd, paramMap);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzfc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */