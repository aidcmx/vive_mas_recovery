package com.google.android.gms.internal;

import android.content.Context;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zzu;
import java.io.BufferedOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@zzji
public class zzff
  implements zzfe
{
  private final Context mContext;
  private final VersionInfoParcel zzanu;
  
  public zzff(Context paramContext, VersionInfoParcel paramVersionInfoParcel)
  {
    this.mContext = paramContext;
    this.zzanu = paramVersionInfoParcel;
  }
  
  protected zzc zza(zzb paramzzb)
  {
    HttpURLConnection localHttpURLConnection;
    try
    {
      localHttpURLConnection = (HttpURLConnection)paramzzb.zznd().openConnection();
      zzu.zzgm().zza(this.mContext, this.zzanu.zzda, false, localHttpURLConnection);
      localObject1 = paramzzb.zzne().iterator();
      while (((Iterator)localObject1).hasNext())
      {
        localObject2 = (zza)((Iterator)localObject1).next();
        localHttpURLConnection.addRequestProperty(((zza)localObject2).getKey(), ((zza)localObject2).getValue());
      }
      if (TextUtils.isEmpty(paramzzb.zznf())) {
        break label144;
      }
    }
    catch (Exception paramzzb)
    {
      return new zzc(false, null, paramzzb.toString());
    }
    localHttpURLConnection.setDoOutput(true);
    Object localObject1 = paramzzb.zznf().getBytes();
    localHttpURLConnection.setFixedLengthStreamingMode(localObject1.length);
    Object localObject2 = new BufferedOutputStream(localHttpURLConnection.getOutputStream());
    ((BufferedOutputStream)localObject2).write((byte[])localObject1);
    ((BufferedOutputStream)localObject2).close();
    label144:
    localObject1 = new ArrayList();
    if (localHttpURLConnection.getHeaderFields() != null)
    {
      localObject2 = localHttpURLConnection.getHeaderFields().entrySet().iterator();
      while (((Iterator)localObject2).hasNext())
      {
        Map.Entry localEntry = (Map.Entry)((Iterator)localObject2).next();
        Iterator localIterator = ((List)localEntry.getValue()).iterator();
        while (localIterator.hasNext())
        {
          String str = (String)localIterator.next();
          ((ArrayList)localObject1).add(new zza((String)localEntry.getKey(), str));
        }
      }
    }
    paramzzb = new zzc(true, new zzd(paramzzb.zznc(), localHttpURLConnection.getResponseCode(), (List)localObject1, zzu.zzgm().zza(new InputStreamReader(localHttpURLConnection.getInputStream()))), null);
    return paramzzb;
  }
  
  protected JSONObject zza(zzd paramzzd)
  {
    JSONObject localJSONObject = new JSONObject();
    try
    {
      localJSONObject.put("http_request_id", paramzzd.zznc());
      if (paramzzd.getBody() != null) {
        localJSONObject.put("body", paramzzd.getBody());
      }
      JSONArray localJSONArray = new JSONArray();
      Iterator localIterator = paramzzd.zznh().iterator();
      while (localIterator.hasNext())
      {
        zza localzza = (zza)localIterator.next();
        localJSONArray.put(new JSONObject().put("key", localzza.getKey()).put("value", localzza.getValue()));
      }
      localJSONObject.put("headers", localJSONArray);
    }
    catch (JSONException paramzzd)
    {
      zzkx.zzb("Error constructing JSON for http response.", paramzzd);
      return localJSONObject;
    }
    localJSONObject.put("response_code", paramzzd.getResponseCode());
    return localJSONObject;
  }
  
  public void zza(zzmd paramzzmd, Map<String, String> paramMap)
  {
    zzla.zza(new zzff.1(this, paramMap, paramzzmd));
  }
  
  protected zzb zzb(JSONObject paramJSONObject)
  {
    String str1 = paramJSONObject.optString("http_request_id");
    Object localObject1 = paramJSONObject.optString("url");
    String str2 = paramJSONObject.optString("post_body", null);
    try
    {
      localObject1 = new URL((String)localObject1);
      localArrayList = new ArrayList();
      localObject2 = paramJSONObject.optJSONArray("headers");
      paramJSONObject = (JSONObject)localObject2;
      if (localObject2 == null) {
        paramJSONObject = new JSONArray();
      }
      int i = 0;
      for (;;)
      {
        if (i >= paramJSONObject.length()) {
          break label140;
        }
        localObject2 = paramJSONObject.optJSONObject(i);
        if (localObject2 != null) {
          break;
        }
        i += 1;
      }
    }
    catch (MalformedURLException localMalformedURLException)
    {
      ArrayList localArrayList;
      URL localURL;
      for (;;)
      {
        Object localObject2;
        zzkx.zzb("Error constructing http request.", localMalformedURLException);
        localURL = null;
        continue;
        localArrayList.add(new zza(((JSONObject)localObject2).optString("key"), ((JSONObject)localObject2).optString("value")));
      }
      label140:
      return new zzb(str1, localURL, localArrayList, str2);
    }
  }
  
  /* Error */
  public JSONObject zzbc(String paramString)
  {
    // Byte code:
    //   0: new 197	org/json/JSONObject
    //   3: dup
    //   4: aload_1
    //   5: invokespecial 291	org/json/JSONObject:<init>	(Ljava/lang/String;)V
    //   8: astore 4
    //   10: new 197	org/json/JSONObject
    //   13: dup
    //   14: invokespecial 198	org/json/JSONObject:<init>	()V
    //   17: astore_3
    //   18: ldc_w 293
    //   21: astore_1
    //   22: aload 4
    //   24: ldc -56
    //   26: invokevirtual 262	org/json/JSONObject:optString	(Ljava/lang/String;)Ljava/lang/String;
    //   29: astore_2
    //   30: aload_2
    //   31: astore_1
    //   32: aload_0
    //   33: aload_0
    //   34: aload 4
    //   36: invokevirtual 295	com/google/android/gms/internal/zzff:zzb	(Lorg/json/JSONObject;)Lcom/google/android/gms/internal/zzff$zzb;
    //   39: invokevirtual 297	com/google/android/gms/internal/zzff:zza	(Lcom/google/android/gms/internal/zzff$zzb;)Lcom/google/android/gms/internal/zzff$zzc;
    //   42: astore 4
    //   44: aload_2
    //   45: astore_1
    //   46: aload 4
    //   48: invokevirtual 300	com/google/android/gms/internal/zzff$zzc:isSuccess	()Z
    //   51: ifeq +68 -> 119
    //   54: aload_2
    //   55: astore_1
    //   56: aload_3
    //   57: ldc_w 302
    //   60: aload_0
    //   61: aload 4
    //   63: invokevirtual 306	com/google/android/gms/internal/zzff$zzc:zzng	()Lcom/google/android/gms/internal/zzff$zzd;
    //   66: invokevirtual 308	com/google/android/gms/internal/zzff:zza	(Lcom/google/android/gms/internal/zzff$zzd;)Lorg/json/JSONObject;
    //   69: invokevirtual 205	org/json/JSONObject:put	(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    //   72: pop
    //   73: aload_2
    //   74: astore_1
    //   75: aload_3
    //   76: ldc_w 310
    //   79: iconst_1
    //   80: invokevirtual 313	org/json/JSONObject:put	(Ljava/lang/String;Z)Lorg/json/JSONObject;
    //   83: pop
    //   84: aload_3
    //   85: areturn
    //   86: astore_1
    //   87: ldc_w 315
    //   90: invokestatic 318	com/google/android/gms/internal/zzkx:e	(Ljava/lang/String;)V
    //   93: new 197	org/json/JSONObject
    //   96: dup
    //   97: invokespecial 198	org/json/JSONObject:<init>	()V
    //   100: ldc_w 310
    //   103: iconst_0
    //   104: invokevirtual 313	org/json/JSONObject:put	(Ljava/lang/String;Z)Lorg/json/JSONObject;
    //   107: astore_1
    //   108: aload_1
    //   109: areturn
    //   110: astore_1
    //   111: new 197	org/json/JSONObject
    //   114: dup
    //   115: invokespecial 198	org/json/JSONObject:<init>	()V
    //   118: areturn
    //   119: aload_2
    //   120: astore_1
    //   121: aload_3
    //   122: ldc_w 302
    //   125: new 197	org/json/JSONObject
    //   128: dup
    //   129: invokespecial 198	org/json/JSONObject:<init>	()V
    //   132: ldc -56
    //   134: aload_2
    //   135: invokevirtual 205	org/json/JSONObject:put	(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    //   138: invokevirtual 205	org/json/JSONObject:put	(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    //   141: pop
    //   142: aload_2
    //   143: astore_1
    //   144: aload_3
    //   145: ldc_w 310
    //   148: iconst_0
    //   149: invokevirtual 313	org/json/JSONObject:put	(Ljava/lang/String;Z)Lorg/json/JSONObject;
    //   152: pop
    //   153: aload_2
    //   154: astore_1
    //   155: aload_3
    //   156: ldc_w 320
    //   159: aload 4
    //   161: invokevirtual 323	com/google/android/gms/internal/zzff$zzc:getReason	()Ljava/lang/String;
    //   164: invokevirtual 205	org/json/JSONObject:put	(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    //   167: pop
    //   168: aload_3
    //   169: areturn
    //   170: astore_2
    //   171: aload_3
    //   172: ldc_w 302
    //   175: new 197	org/json/JSONObject
    //   178: dup
    //   179: invokespecial 198	org/json/JSONObject:<init>	()V
    //   182: ldc -56
    //   184: aload_1
    //   185: invokevirtual 205	org/json/JSONObject:put	(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    //   188: invokevirtual 205	org/json/JSONObject:put	(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    //   191: pop
    //   192: aload_3
    //   193: ldc_w 310
    //   196: iconst_0
    //   197: invokevirtual 313	org/json/JSONObject:put	(Ljava/lang/String;Z)Lorg/json/JSONObject;
    //   200: pop
    //   201: aload_3
    //   202: ldc_w 320
    //   205: aload_2
    //   206: invokevirtual 99	java/lang/Exception:toString	()Ljava/lang/String;
    //   209: invokevirtual 205	org/json/JSONObject:put	(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    //   212: pop
    //   213: aload_3
    //   214: areturn
    //   215: astore_1
    //   216: aload_3
    //   217: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	218	0	this	zzff
    //   0	218	1	paramString	String
    //   29	125	2	str	String
    //   170	36	2	localException	Exception
    //   17	200	3	localJSONObject	JSONObject
    //   8	152	4	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   0	10	86	org/json/JSONException
    //   93	108	110	org/json/JSONException
    //   22	30	170	java/lang/Exception
    //   32	44	170	java/lang/Exception
    //   46	54	170	java/lang/Exception
    //   56	73	170	java/lang/Exception
    //   75	84	170	java/lang/Exception
    //   121	142	170	java/lang/Exception
    //   144	153	170	java/lang/Exception
    //   155	168	170	java/lang/Exception
    //   171	213	215	org/json/JSONException
  }
  
  @zzji
  static class zza
  {
    private final String mValue;
    private final String zzbcn;
    
    public zza(String paramString1, String paramString2)
    {
      this.zzbcn = paramString1;
      this.mValue = paramString2;
    }
    
    public String getKey()
    {
      return this.zzbcn;
    }
    
    public String getValue()
    {
      return this.mValue;
    }
  }
  
  @zzji
  static class zzb
  {
    private final String zzbqi;
    private final URL zzbqj;
    private final ArrayList<zzff.zza> zzbqk;
    private final String zzbql;
    
    public zzb(String paramString1, URL paramURL, ArrayList<zzff.zza> paramArrayList, String paramString2)
    {
      this.zzbqi = paramString1;
      this.zzbqj = paramURL;
      if (paramArrayList == null) {}
      for (this.zzbqk = new ArrayList();; this.zzbqk = paramArrayList)
      {
        this.zzbql = paramString2;
        return;
      }
    }
    
    public String zznc()
    {
      return this.zzbqi;
    }
    
    public URL zznd()
    {
      return this.zzbqj;
    }
    
    public ArrayList<zzff.zza> zzne()
    {
      return this.zzbqk;
    }
    
    public String zznf()
    {
      return this.zzbql;
    }
  }
  
  @zzji
  class zzc
  {
    private final zzff.zzd zzbqm;
    private final boolean zzbqn;
    private final String zzbqo;
    
    public zzc(boolean paramBoolean, zzff.zzd paramzzd, String paramString)
    {
      this.zzbqn = paramBoolean;
      this.zzbqm = paramzzd;
      this.zzbqo = paramString;
    }
    
    public String getReason()
    {
      return this.zzbqo;
    }
    
    public boolean isSuccess()
    {
      return this.zzbqn;
    }
    
    public zzff.zzd zzng()
    {
      return this.zzbqm;
    }
  }
  
  @zzji
  static class zzd
  {
    private final String zzbna;
    private final String zzbqi;
    private final int zzbqp;
    private final List<zzff.zza> zzbqq;
    
    public zzd(String paramString1, int paramInt, List<zzff.zza> paramList, String paramString2)
    {
      this.zzbqi = paramString1;
      this.zzbqp = paramInt;
      if (paramList == null) {}
      for (this.zzbqq = new ArrayList();; this.zzbqq = paramList)
      {
        this.zzbna = paramString2;
        return;
      }
    }
    
    public String getBody()
    {
      return this.zzbna;
    }
    
    public int getResponseCode()
    {
      return this.zzbqp;
    }
    
    public String zznc()
    {
      return this.zzbqi;
    }
    
    public Iterable<zzff.zza> zznh()
    {
      return this.zzbqq;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzff.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */