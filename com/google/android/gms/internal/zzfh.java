package com.google.android.gms.internal;

import java.util.Map;

@zzji
public class zzfh
  implements zzfe
{
  private final zzfi zzbqr;
  
  public zzfh(zzfi paramzzfi)
  {
    this.zzbqr = paramzzfi;
  }
  
  public void zza(zzmd paramzzmd, Map<String, String> paramMap)
  {
    boolean bool1 = "1".equals(paramMap.get("transparentBackground"));
    boolean bool2 = "1".equals(paramMap.get("blur"));
    try
    {
      if (paramMap.get("blurRadius") != null)
      {
        f = Float.parseFloat((String)paramMap.get("blurRadius"));
        this.zzbqr.zzg(bool1);
        this.zzbqr.zza(bool2, f);
        return;
      }
    }
    catch (NumberFormatException paramzzmd)
    {
      for (;;)
      {
        zzkx.zzb("Fail to parse float", paramzzmd);
        float f = 0.0F;
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzfh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */