package com.google.android.gms.internal;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;
import org.json.JSONException;
import org.json.JSONObject;

@zzji
public class zzfj
  implements zzfe
{
  final HashMap<String, zzlq<JSONObject>> zzbqs = new HashMap();
  
  public void zza(zzmd paramzzmd, Map<String, String> paramMap)
  {
    zzh((String)paramMap.get("request_id"), (String)paramMap.get("fetched_ad"));
  }
  
  public Future<JSONObject> zzbd(String paramString)
  {
    zzlq localzzlq = new zzlq();
    this.zzbqs.put(paramString, localzzlq);
    return localzzlq;
  }
  
  public void zzbe(String paramString)
  {
    zzlq localzzlq = (zzlq)this.zzbqs.get(paramString);
    if (localzzlq == null)
    {
      zzkx.e("Could not find the ad request for the corresponding ad response.");
      return;
    }
    if (!localzzlq.isDone()) {
      localzzlq.cancel(true);
    }
    this.zzbqs.remove(paramString);
  }
  
  public void zzh(String paramString1, String paramString2)
  {
    zzkx.zzdg("Received ad from the cache.");
    zzlq localzzlq = (zzlq)this.zzbqs.get(paramString1);
    if (localzzlq == null)
    {
      zzkx.e("Could not find the ad request for the corresponding ad response.");
      return;
    }
    try
    {
      localzzlq.zzh(new JSONObject(paramString2));
      return;
    }
    catch (JSONException paramString2)
    {
      zzkx.zzb("Failed constructing JSON object from value passed from javascript", paramString2);
      localzzlq.zzh(null);
      return;
    }
    finally
    {
      this.zzbqs.remove(paramString1);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzfj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */