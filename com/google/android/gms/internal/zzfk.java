package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.zze;
import com.google.android.gms.common.util.zzf;
import java.util.Map;

@zzji
public class zzfk
  implements zzfe
{
  static final Map<String, Integer> zzbqv = zzf.zza("resize", Integer.valueOf(1), "playVideo", Integer.valueOf(2), "storePicture", Integer.valueOf(3), "createCalendarEvent", Integer.valueOf(4), "setOrientationProperties", Integer.valueOf(5), "closeResizedAd", Integer.valueOf(6));
  private final zze zzbqt;
  private final zzhq zzbqu;
  
  public zzfk(zze paramzze, zzhq paramzzhq)
  {
    this.zzbqt = paramzze;
    this.zzbqu = paramzzhq;
  }
  
  public void zza(zzmd paramzzmd, Map<String, String> paramMap)
  {
    String str = (String)paramMap.get("a");
    int i = ((Integer)zzbqv.get(str)).intValue();
    if ((i != 5) && (this.zzbqt != null) && (!this.zzbqt.zzfe()))
    {
      this.zzbqt.zzy(null);
      return;
    }
    switch (i)
    {
    case 2: 
    default: 
      zzkx.zzdh("Unknown MRAID command called.");
      return;
    case 1: 
      this.zzbqu.execute(paramMap);
      return;
    case 4: 
      new zzhp(paramzzmd, paramMap).execute();
      return;
    case 3: 
      new zzhs(paramzzmd, paramMap).execute();
      return;
    case 5: 
      new zzhr(paramzzmd, paramMap).execute();
      return;
    }
    this.zzbqu.zzt(true);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzfk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */