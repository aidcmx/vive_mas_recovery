package com.google.android.gms.internal;

import android.content.Context;
import android.graphics.Color;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.MotionEvent;
import com.google.android.gms.ads.internal.client.zzm;
import com.google.android.gms.ads.internal.overlay.zzd;
import com.google.android.gms.ads.internal.overlay.zzk;
import com.google.android.gms.ads.internal.util.client.zza;
import java.util.Map;
import org.json.JSONObject;

@zzji
public final class zzfo
  implements zzfe
{
  private boolean zzbqz;
  
  private static int zza(Context paramContext, Map<String, String> paramMap, String paramString, int paramInt)
  {
    paramMap = (String)paramMap.get(paramString);
    int i = paramInt;
    if (paramMap != null) {}
    try
    {
      i = zzm.zzkr().zzb(paramContext, Integer.parseInt(paramMap));
      return i;
    }
    catch (NumberFormatException paramContext)
    {
      zzkx.zzdi(String.valueOf(paramString).length() + 34 + String.valueOf(paramMap).length() + "Could not parse " + paramString + " in a video GMSG: " + paramMap);
    }
    return paramInt;
  }
  
  public void zza(zzmd paramzzmd, Map<String, String> paramMap)
  {
    Object localObject2 = (String)paramMap.get("action");
    if (localObject2 == null)
    {
      zzkx.zzdi("Action missing from video GMSG.");
      return;
    }
    if (zzkx.zzbi(3))
    {
      localObject1 = new JSONObject(paramMap);
      ((JSONObject)localObject1).remove("google.afma.Notify_dt");
      localObject1 = String.valueOf(((JSONObject)localObject1).toString());
      zzkx.zzdg(String.valueOf(localObject2).length() + 13 + String.valueOf(localObject1).length() + "Video GMSG: " + (String)localObject2 + " " + (String)localObject1);
    }
    if ("background".equals(localObject2))
    {
      paramMap = (String)paramMap.get("color");
      if (TextUtils.isEmpty(paramMap))
      {
        zzkx.zzdi("Color parameter missing from color video GMSG.");
        return;
      }
      try
      {
        paramzzmd.setBackgroundColor(Color.parseColor(paramMap));
        return;
      }
      catch (IllegalArgumentException paramzzmd)
      {
        zzkx.zzdi("Invalid color parameter in video GMSG.");
        return;
      }
    }
    Object localObject1 = paramzzmd.zzxk();
    if (localObject1 == null)
    {
      zzkx.zzdi("Could not get underlay container for a video GMSG.");
      return;
    }
    boolean bool1 = "new".equals(localObject2);
    boolean bool2 = "position".equals(localObject2);
    int m;
    int n;
    int i;
    int j;
    if ((bool1) || (bool2))
    {
      localObject2 = paramzzmd.getContext();
      m = zza((Context)localObject2, paramMap, "x", 0);
      n = zza((Context)localObject2, paramMap, "y", 0);
      i = zza((Context)localObject2, paramMap, "w", -1);
      j = zza((Context)localObject2, paramMap, "h", -1);
      if (!((Boolean)zzdr.zzbjh.get()).booleanValue()) {
        break label931;
      }
      i = Math.min(i, paramzzmd.getMeasuredWidth() - m);
      j = Math.min(j, paramzzmd.getMeasuredHeight() - n);
    }
    label931:
    for (;;)
    {
      try
      {
        k = Integer.parseInt((String)paramMap.get("player"));
        bool2 = Boolean.parseBoolean((String)paramMap.get("spherical"));
        if ((bool1) && (((zzmc)localObject1).zzwv() == null))
        {
          ((zzmc)localObject1).zza(m, n, i, j, k, bool2);
          return;
        }
      }
      catch (NumberFormatException paramzzmd)
      {
        int k = 0;
        continue;
        ((zzmc)localObject1).zze(m, n, i, j);
        return;
      }
      localObject1 = ((zzmc)localObject1).zzwv();
      if (localObject1 == null)
      {
        zzk.zzi(paramzzmd);
        return;
      }
      if ("click".equals(localObject2))
      {
        paramzzmd = paramzzmd.getContext();
        i = zza(paramzzmd, paramMap, "x", 0);
        j = zza(paramzzmd, paramMap, "y", 0);
        long l = SystemClock.uptimeMillis();
        paramzzmd = MotionEvent.obtain(l, l, 0, i, j, 0);
        ((zzk)localObject1).zzf(paramzzmd);
        paramzzmd.recycle();
        return;
      }
      if ("currentTime".equals(localObject2))
      {
        paramzzmd = (String)paramMap.get("time");
        if (paramzzmd == null)
        {
          zzkx.zzdi("Time parameter missing from currentTime video GMSG.");
          return;
        }
        try
        {
          ((zzk)localObject1).seekTo((int)(Float.parseFloat(paramzzmd) * 1000.0F));
          return;
        }
        catch (NumberFormatException paramMap)
        {
          paramzzmd = String.valueOf(paramzzmd);
          if (paramzzmd.length() == 0) {}
        }
        for (paramzzmd = "Could not parse time parameter from currentTime video GMSG: ".concat(paramzzmd);; paramzzmd = new String("Could not parse time parameter from currentTime video GMSG: "))
        {
          zzkx.zzdi(paramzzmd);
          return;
        }
      }
      if ("hide".equals(localObject2))
      {
        ((zzk)localObject1).setVisibility(4);
        return;
      }
      if ("load".equals(localObject2))
      {
        ((zzk)localObject1).zznt();
        return;
      }
      if ("muted".equals(localObject2))
      {
        if (Boolean.parseBoolean((String)paramMap.get("muted")))
        {
          ((zzk)localObject1).zzqh();
          return;
        }
        ((zzk)localObject1).zzqi();
        return;
      }
      if ("pause".equals(localObject2))
      {
        ((zzk)localObject1).pause();
        return;
      }
      if ("play".equals(localObject2))
      {
        ((zzk)localObject1).play();
        return;
      }
      if ("show".equals(localObject2))
      {
        ((zzk)localObject1).setVisibility(0);
        return;
      }
      if ("src".equals(localObject2))
      {
        ((zzk)localObject1).zzce((String)paramMap.get("src"));
        return;
      }
      if ("touchMove".equals(localObject2))
      {
        localObject2 = paramzzmd.getContext();
        i = zza((Context)localObject2, paramMap, "dx", 0);
        j = zza((Context)localObject2, paramMap, "dy", 0);
        ((zzk)localObject1).zza(i, j);
        if (this.zzbqz) {
          break;
        }
        paramzzmd.zzxa().zzpt();
        this.zzbqz = true;
        return;
      }
      if ("volume".equals(localObject2))
      {
        paramzzmd = (String)paramMap.get("volume");
        if (paramzzmd == null)
        {
          zzkx.zzdi("Level parameter missing from volume video GMSG.");
          return;
        }
        try
        {
          ((zzk)localObject1).zzb(Float.parseFloat(paramzzmd));
          return;
        }
        catch (NumberFormatException paramMap)
        {
          paramzzmd = String.valueOf(paramzzmd);
          if (paramzzmd.length() == 0) {}
        }
        for (paramzzmd = "Could not parse volume parameter from volume video GMSG: ".concat(paramzzmd);; paramzzmd = new String("Could not parse volume parameter from volume video GMSG: "))
        {
          zzkx.zzdi(paramzzmd);
          return;
        }
      }
      if ("watermark".equals(localObject2))
      {
        ((zzk)localObject1).zzqj();
        return;
      }
      paramzzmd = String.valueOf(localObject2);
      if (paramzzmd.length() != 0) {}
      for (paramzzmd = "Unknown video action: ".concat(paramzzmd);; paramzzmd = new String("Unknown video action: "))
      {
        zzkx.zzdi(paramzzmd);
        return;
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */