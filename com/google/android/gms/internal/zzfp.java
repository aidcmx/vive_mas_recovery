package com.google.android.gms.internal;

import android.text.TextUtils;
import com.google.android.gms.ads.internal.zzu;
import java.util.Map;

@zzji
class zzfp
  implements zzfe
{
  private int zzh(Map<String, String> paramMap)
    throws NullPointerException, NumberFormatException
  {
    int j = Integer.parseInt((String)paramMap.get("playbackState"));
    int i;
    if (j >= 0)
    {
      i = j;
      if (3 >= j) {}
    }
    else
    {
      i = 0;
    }
    return i;
  }
  
  public void zza(zzmd paramzzmd, Map<String, String> paramMap)
  {
    if (!((Boolean)zzdr.zzbhe.get()).booleanValue()) {
      return;
    }
    zzmi localzzmi = paramzzmd.zzxn();
    if (localzzmi == null) {}
    for (;;)
    {
      try
      {
        localzzmi = new zzmi(paramzzmd, Float.parseFloat((String)paramMap.get("duration")));
        paramzzmd.zza(localzzmi);
        paramzzmd = localzzmi;
        bool = "1".equals(paramMap.get("muted"));
        f2 = Float.parseFloat((String)paramMap.get("currentTime"));
        i = zzh(paramMap);
        paramMap = (String)paramMap.get("aspectRatio");
        if (!TextUtils.isEmpty(paramMap)) {
          continue;
        }
        f1 = 0.0F;
      }
      catch (NullPointerException paramzzmd)
      {
        boolean bool;
        float f2;
        int i;
        zzkx.zzb("Unable to parse videoMeta message.", paramzzmd);
        zzu.zzgq().zza(paramzzmd, "VideoMetaGmsgHandler.onGmsg");
        return;
        float f1 = Float.parseFloat(paramMap);
        continue;
      }
      catch (NumberFormatException paramzzmd)
      {
        continue;
      }
      if (zzkx.zzbi(3)) {
        zzkx.zzdg(String.valueOf(paramMap).length() + 79 + "Video Meta GMSG: isMuted : " + bool + " , playbackState : " + i + " , aspectRatio : " + paramMap);
      }
      paramzzmd.zza(f2, i, bool, f1);
      return;
      paramzzmd = localzzmi;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzfp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */