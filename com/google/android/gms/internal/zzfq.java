package com.google.android.gms.internal;

import android.os.Handler;
import com.google.android.gms.ads.internal.zzu;

@zzji
public class zzfq
  extends zzkw
{
  final zzmd zzbnz;
  final zzfs zzbra;
  private final String zzbrb;
  
  zzfq(zzmd paramzzmd, zzfs paramzzfs, String paramString)
  {
    this.zzbnz = paramzzmd;
    this.zzbra = paramzzfs;
    this.zzbrb = paramString;
    zzu.zzhj().zza(this);
  }
  
  public void onStop()
  {
    this.zzbra.abort();
  }
  
  public void zzfp()
  {
    try
    {
      this.zzbra.zzbg(this.zzbrb);
      return;
    }
    finally
    {
      zzlb.zzcvl.post(new zzfq.1(this));
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzfq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */