package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.zzu;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

@zzji
public class zzfr
  implements Iterable<zzfq>
{
  private final List<zzfq> zzbrd = new LinkedList();
  
  private zzfq zzg(zzmd paramzzmd)
  {
    Iterator localIterator = zzu.zzhj().iterator();
    while (localIterator.hasNext())
    {
      zzfq localzzfq = (zzfq)localIterator.next();
      if (localzzfq.zzbnz == paramzzmd) {
        return localzzfq;
      }
    }
    return null;
  }
  
  public Iterator<zzfq> iterator()
  {
    return this.zzbrd.iterator();
  }
  
  public void zza(zzfq paramzzfq)
  {
    this.zzbrd.add(paramzzfq);
  }
  
  public void zzb(zzfq paramzzfq)
  {
    this.zzbrd.remove(paramzzfq);
  }
  
  public boolean zze(zzmd paramzzmd)
  {
    paramzzmd = zzg(paramzzmd);
    if (paramzzmd != null)
    {
      paramzzmd.zzbra.abort();
      return true;
    }
    return false;
  }
  
  public boolean zzf(zzmd paramzzmd)
  {
    return zzg(paramzzmd) != null;
  }
  
  public int zzni()
  {
    return this.zzbrd.size();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzfr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */