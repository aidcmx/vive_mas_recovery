package com.google.android.gms.internal;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.Nullable;
import com.google.android.gms.ads.internal.client.zzm;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.common.api.Releasable;
import java.lang.ref.WeakReference;
import java.util.Map;

@zzji
public abstract class zzfs
  implements Releasable
{
  protected Context mContext;
  protected String zzbre;
  protected WeakReference<zzmd> zzbrf;
  
  public zzfs(zzmd paramzzmd)
  {
    this.mContext = paramzzmd.getContext();
    this.zzbre = zzu.zzgm().zzh(this.mContext, paramzzmd.zzxf().zzda);
    this.zzbrf = new WeakReference(paramzzmd);
  }
  
  private void zza(String paramString, Map<String, String> paramMap)
  {
    zzmd localzzmd = (zzmd)this.zzbrf.get();
    if (localzzmd != null) {
      localzzmd.zza(paramString, paramMap);
    }
  }
  
  private String zzbi(String paramString)
  {
    int i = -1;
    switch (paramString.hashCode())
    {
    }
    for (;;)
    {
      switch (i)
      {
      default: 
        return "internal";
        if (paramString.equals("error"))
        {
          i = 0;
          continue;
          if (paramString.equals("playerFailed"))
          {
            i = 1;
            continue;
            if (paramString.equals("inProgress"))
            {
              i = 2;
              continue;
              if (paramString.equals("contentLengthMissing"))
              {
                i = 3;
                continue;
                if (paramString.equals("noCacheDir"))
                {
                  i = 4;
                  continue;
                  if (paramString.equals("expireFailed"))
                  {
                    i = 5;
                    continue;
                    if (paramString.equals("badUrl"))
                    {
                      i = 6;
                      continue;
                      if (paramString.equals("downloadTimeout"))
                      {
                        i = 7;
                        continue;
                        if (paramString.equals("sizeExceeded"))
                        {
                          i = 8;
                          continue;
                          if (paramString.equals("externalAbort")) {
                            i = 9;
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
        break;
      }
    }
    return "internal";
    return "io";
    return "network";
    return "policy";
  }
  
  public abstract void abort();
  
  public void release() {}
  
  protected void zza(String paramString1, String paramString2, int paramInt)
  {
    zza.zzcxr.post(new zzfs.2(this, paramString1, paramString2, paramInt));
  }
  
  protected void zza(String paramString1, String paramString2, int paramInt1, int paramInt2, boolean paramBoolean)
  {
    zza.zzcxr.post(new zzfs.1(this, paramString1, paramString2, paramInt1, paramInt2, paramBoolean));
  }
  
  public void zza(String paramString1, String paramString2, String paramString3, @Nullable String paramString4)
  {
    zza.zzcxr.post(new zzfs.3(this, paramString1, paramString2, paramString3, paramString4));
  }
  
  public abstract boolean zzbg(String paramString);
  
  protected String zzbh(String paramString)
  {
    return zzm.zzkr().zzdf(paramString);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzfs.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */