package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.zzd;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.common.internal.zzc;
import java.util.Map;
import java.util.concurrent.Future;

@zzji
public class zzft
  implements zzfe
{
  public void zza(zzmd paramzzmd, Map<String, String> paramMap)
  {
    zzfr localzzfr = zzu.zzhj();
    if (paramMap.containsKey("abort"))
    {
      if (!localzzfr.zze(paramzzmd)) {
        zzkx.zzdi("Precache abort but no preload task running.");
      }
      return;
    }
    String str = (String)paramMap.get("src");
    if (str == null)
    {
      zzkx.zzdi("Precache video action is missing the src parameter.");
      return;
    }
    try
    {
      i = Integer.parseInt((String)paramMap.get("player"));
      if (paramMap.containsKey("mimetype"))
      {
        paramMap = (String)paramMap.get("mimetype");
        if (!localzzfr.zzf(paramzzmd)) {
          break label121;
        }
        zzkx.zzdi("Precache task already running.");
      }
    }
    catch (NumberFormatException localNumberFormatException)
    {
      int i;
      for (;;)
      {
        i = 0;
        continue;
        paramMap = "";
      }
      label121:
      zzc.zzu(paramzzmd.zzec());
      paramzzmd = (Future)new zzfq(paramzzmd, paramzzmd.zzec().zzamp.zza(paramzzmd, i, paramMap), str).zzrz();
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzft.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */