package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zzd;
import com.google.android.gms.ads.internal.zzl;

@zzji
public class zzfw
{
  private final Context mContext;
  private final zzd zzamb;
  private final zzgz zzamf;
  private final VersionInfoParcel zzanu;
  
  zzfw(Context paramContext, zzgz paramzzgz, VersionInfoParcel paramVersionInfoParcel, zzd paramzzd)
  {
    this.mContext = paramContext;
    this.zzamf = paramzzgz;
    this.zzanu = paramVersionInfoParcel;
    this.zzamb = paramzzd;
  }
  
  public Context getApplicationContext()
  {
    return this.mContext.getApplicationContext();
  }
  
  public zzl zzbj(String paramString)
  {
    return new zzl(this.mContext, new AdSizeParcel(), paramString, this.zzamf, this.zzanu, this.zzamb);
  }
  
  public zzl zzbk(String paramString)
  {
    return new zzl(this.mContext.getApplicationContext(), new AdSizeParcel(), paramString, this.zzamf, this.zzanu, this.zzamb);
  }
  
  public zzfw zznl()
  {
    return new zzfw(getApplicationContext(), this.zzamf, this.zzanu, this.zzamb);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzfw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */