package com.google.android.gms.internal;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.Nullable;
import android.util.Base64;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.zzl;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.common.util.zze;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Pattern;

@zzji
public class zzfz
{
  private final Map<zzga, zzgb> zzbsl = new HashMap();
  private final LinkedList<zzga> zzbsm = new LinkedList();
  @Nullable
  private zzfw zzbsn;
  
  private static void zza(String paramString, zzga paramzzga)
  {
    if (zzkx.zzbi(2)) {
      zzkx.v(String.format(paramString, new Object[] { paramzzga }));
    }
  }
  
  private String[] zzbl(String paramString)
  {
    try
    {
      String[] arrayOfString = paramString.split("\000");
      int i = 0;
      for (;;)
      {
        paramString = arrayOfString;
        if (i >= arrayOfString.length) {
          break;
        }
        arrayOfString[i] = new String(Base64.decode(arrayOfString[i], 0), "UTF-8");
        i += 1;
      }
      return paramString;
    }
    catch (UnsupportedEncodingException paramString)
    {
      paramString = new String[0];
    }
  }
  
  private boolean zzbm(String paramString)
  {
    try
    {
      boolean bool = Pattern.matches((String)zzdr.zzbgn.get(), paramString);
      return bool;
    }
    catch (RuntimeException paramString)
    {
      zzu.zzgq().zza(paramString, "InterstitialAdPool.isExcludedAdUnit");
    }
    return false;
  }
  
  private static void zzc(Bundle paramBundle, String paramString)
  {
    paramString = paramString.split("/", 2);
    if (paramString.length == 0) {}
    do
    {
      return;
      String str = paramString[0];
      if (paramString.length == 1)
      {
        paramBundle.remove(str);
        return;
      }
      paramBundle = paramBundle.getBundle(str);
    } while (paramBundle == null);
    zzc(paramBundle, paramString[1]);
  }
  
  @Nullable
  static Bundle zzk(AdRequestParcel paramAdRequestParcel)
  {
    paramAdRequestParcel = paramAdRequestParcel.zzayv;
    if (paramAdRequestParcel == null) {
      return null;
    }
    return paramAdRequestParcel.getBundle("com.google.ads.mediation.admob.AdMobAdapter");
  }
  
  static AdRequestParcel zzl(AdRequestParcel paramAdRequestParcel)
  {
    AdRequestParcel localAdRequestParcel = zzo(paramAdRequestParcel);
    Bundle localBundle = zzk(localAdRequestParcel);
    paramAdRequestParcel = localBundle;
    if (localBundle == null)
    {
      paramAdRequestParcel = new Bundle();
      localAdRequestParcel.zzayv.putBundle("com.google.ads.mediation.admob.AdMobAdapter", paramAdRequestParcel);
    }
    paramAdRequestParcel.putBoolean("_skipMediation", true);
    return localAdRequestParcel;
  }
  
  static boolean zzm(AdRequestParcel paramAdRequestParcel)
  {
    paramAdRequestParcel = paramAdRequestParcel.zzayv;
    if (paramAdRequestParcel == null) {}
    do
    {
      return false;
      paramAdRequestParcel = paramAdRequestParcel.getBundle("com.google.ads.mediation.admob.AdMobAdapter");
    } while ((paramAdRequestParcel == null) || (!paramAdRequestParcel.containsKey("_skipMediation")));
    return true;
  }
  
  private static AdRequestParcel zzn(AdRequestParcel paramAdRequestParcel)
  {
    paramAdRequestParcel = zzo(paramAdRequestParcel);
    String[] arrayOfString = ((String)zzdr.zzbgj.get()).split(",");
    int j = arrayOfString.length;
    int i = 0;
    while (i < j)
    {
      String str = arrayOfString[i];
      zzc(paramAdRequestParcel.zzayv, str);
      i += 1;
    }
    return paramAdRequestParcel;
  }
  
  private String zznn()
  {
    try
    {
      StringBuilder localStringBuilder = new StringBuilder();
      Iterator localIterator = this.zzbsm.iterator();
      while (localIterator.hasNext())
      {
        localStringBuilder.append(Base64.encodeToString(((zzga)localIterator.next()).toString().getBytes("UTF-8"), 0));
        if (localIterator.hasNext()) {
          localStringBuilder.append("\000");
        }
      }
      str = localUnsupportedEncodingException.toString();
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      return "";
    }
    String str;
    return str;
  }
  
  static AdRequestParcel zzo(AdRequestParcel paramAdRequestParcel)
  {
    Parcel localParcel = Parcel.obtain();
    paramAdRequestParcel.writeToParcel(localParcel, 0);
    localParcel.setDataPosition(0);
    paramAdRequestParcel = (AdRequestParcel)AdRequestParcel.CREATOR.createFromParcel(localParcel);
    localParcel.recycle();
    AdRequestParcel.zzj(paramAdRequestParcel);
    return paramAdRequestParcel;
  }
  
  void flush()
  {
    while (this.zzbsm.size() > 0)
    {
      zzga localzzga = (zzga)this.zzbsm.remove();
      zzgb localzzgb = (zzgb)this.zzbsl.get(localzzga);
      zza("Flushing interstitial queue for %s.", localzzga);
      while (localzzgb.size() > 0) {
        localzzgb.zzp(null).zzbss.zzfn();
      }
      this.zzbsl.remove(localzzga);
    }
  }
  
  void restore()
  {
    if (this.zzbsn == null) {}
    for (;;)
    {
      return;
      Object localObject1 = this.zzbsn.getApplicationContext().getSharedPreferences("com.google.android.gms.ads.internal.interstitial.InterstitialAdPool", 0);
      flush();
      Object localObject2;
      try
      {
        HashMap localHashMap = new HashMap();
        localObject2 = ((SharedPreferences)localObject1).getAll().entrySet().iterator();
        while (((Iterator)localObject2).hasNext())
        {
          Object localObject3 = (Map.Entry)((Iterator)localObject2).next();
          if (!((String)((Map.Entry)localObject3).getKey()).equals("PoolKeys"))
          {
            Object localObject4 = zzgd.zzbn((String)((Map.Entry)localObject3).getValue());
            localObject3 = new zzga(((zzgd)localObject4).zzapj, ((zzgd)localObject4).zzant, ((zzgd)localObject4).zzbsq);
            if (!this.zzbsl.containsKey(localObject3))
            {
              localObject4 = new zzgb(((zzgd)localObject4).zzapj, ((zzgd)localObject4).zzant, ((zzgd)localObject4).zzbsq);
              this.zzbsl.put(localObject3, localObject4);
              localHashMap.put(((zzga)localObject3).toString(), localObject3);
              zza("Restored interstitial queue for %s.", (zzga)localObject3);
            }
          }
        }
        localObject1 = zzbl(((SharedPreferences)localObject1).getString("PoolKeys", ""));
      }
      catch (Throwable localThrowable)
      {
        zzu.zzgq().zza(localThrowable, "InterstitialAdPool.restore");
        zzkx.zzc("Malformed preferences value for InterstitialAdPool.", localThrowable);
        this.zzbsl.clear();
        this.zzbsm.clear();
        return;
      }
      int j = localObject1.length;
      int i = 0;
      while (i < j)
      {
        localObject2 = (zzga)localThrowable.get(localObject1[i]);
        if (this.zzbsl.containsKey(localObject2)) {
          this.zzbsm.add(localObject2);
        }
        i += 1;
      }
    }
  }
  
  void save()
  {
    if (this.zzbsn == null) {
      return;
    }
    SharedPreferences.Editor localEditor = this.zzbsn.getApplicationContext().getSharedPreferences("com.google.android.gms.ads.internal.interstitial.InterstitialAdPool", 0).edit();
    localEditor.clear();
    Iterator localIterator = this.zzbsl.entrySet().iterator();
    while (localIterator.hasNext())
    {
      Object localObject = (Map.Entry)localIterator.next();
      zzga localzzga = (zzga)((Map.Entry)localObject).getKey();
      localObject = (zzgb)((Map.Entry)localObject).getValue();
      if (((zzgb)localObject).zzns())
      {
        localObject = new zzgd((zzgb)localObject).zznv();
        localEditor.putString(localzzga.toString(), (String)localObject);
        zza("Saved interstitial queue for %s.", localzzga);
      }
    }
    localEditor.putString("PoolKeys", zznn());
    localEditor.apply();
  }
  
  @Nullable
  zzgb.zza zza(AdRequestParcel paramAdRequestParcel, String paramString)
  {
    if (zzbm(paramString)) {
      return null;
    }
    int i = new zzjr.zza(this.zzbsn.getApplicationContext()).zztr().zzcqe;
    AdRequestParcel localAdRequestParcel = zzn(paramAdRequestParcel);
    zzga localzzga = new zzga(localAdRequestParcel, paramString, i);
    paramAdRequestParcel = (zzgb)this.zzbsl.get(localzzga);
    if (paramAdRequestParcel == null)
    {
      zza("Interstitial pool created at %s.", localzzga);
      paramAdRequestParcel = new zzgb(localAdRequestParcel, paramString, i);
      this.zzbsl.put(localzzga, paramAdRequestParcel);
    }
    for (;;)
    {
      this.zzbsm.remove(localzzga);
      this.zzbsm.add(localzzga);
      paramAdRequestParcel.zznr();
      while (this.zzbsm.size() > ((Integer)zzdr.zzbgk.get()).intValue())
      {
        paramString = (zzga)this.zzbsm.remove();
        zzgb localzzgb = (zzgb)this.zzbsl.get(paramString);
        zza("Evicting interstitial queue for %s.", paramString);
        while (localzzgb.size() > 0) {
          localzzgb.zzp(null).zzbss.zzfn();
        }
        this.zzbsl.remove(paramString);
      }
      while (paramAdRequestParcel.size() > 0)
      {
        paramString = paramAdRequestParcel.zzp(localAdRequestParcel);
        if ((paramString.zzbsw) && (zzu.zzgs().currentTimeMillis() - paramString.zzbsv > 1000L * ((Integer)zzdr.zzbgm.get()).intValue()))
        {
          zza("Expired interstitial at %s.", localzzga);
        }
        else
        {
          if (paramString.zzbst != null) {}
          for (paramAdRequestParcel = " (inline) ";; paramAdRequestParcel = " ")
          {
            zza(String.valueOf(paramAdRequestParcel).length() + 34 + "Pooled interstitial" + paramAdRequestParcel + "returned at %s.", localzzga);
            return paramString;
          }
        }
      }
      return null;
    }
  }
  
  void zza(zzfw paramzzfw)
  {
    if (this.zzbsn == null)
    {
      this.zzbsn = paramzzfw.zznl();
      restore();
    }
  }
  
  void zzb(AdRequestParcel paramAdRequestParcel, String paramString)
  {
    if (this.zzbsn == null) {
      return;
    }
    int i = new zzjr.zza(this.zzbsn.getApplicationContext()).zztr().zzcqe;
    AdRequestParcel localAdRequestParcel = zzn(paramAdRequestParcel);
    zzga localzzga = new zzga(localAdRequestParcel, paramString, i);
    zzgb localzzgb2 = (zzgb)this.zzbsl.get(localzzga);
    zzgb localzzgb1 = localzzgb2;
    if (localzzgb2 == null)
    {
      zza("Interstitial pool created at %s.", localzzga);
      localzzgb1 = new zzgb(localAdRequestParcel, paramString, i);
      this.zzbsl.put(localzzga, localzzgb1);
    }
    localzzgb1.zza(this.zzbsn, paramAdRequestParcel);
    localzzgb1.zznr();
    zza("Inline entry added to the queue at %s.", localzzga);
  }
  
  void zznm()
  {
    if (this.zzbsn == null) {
      return;
    }
    Iterator localIterator = this.zzbsl.entrySet().iterator();
    while (localIterator.hasNext())
    {
      Object localObject = (Map.Entry)localIterator.next();
      zzga localzzga = (zzga)((Map.Entry)localObject).getKey();
      localObject = (zzgb)((Map.Entry)localObject).getValue();
      if (zzkx.zzbi(2))
      {
        int i = ((zzgb)localObject).size();
        int j = ((zzgb)localObject).zznp();
        if (j < i) {
          zzkx.v(String.format("Loading %s/%s pooled interstitials for %s.", new Object[] { Integer.valueOf(i - j), Integer.valueOf(i), localzzga }));
        }
      }
      ((zzgb)localObject).zznq();
      while (((zzgb)localObject).size() < ((Integer)zzdr.zzbgl.get()).intValue())
      {
        zza("Pooling and loading one new interstitial for %s.", localzzga);
        ((zzgb)localObject).zzb(this.zzbsn);
      }
    }
    save();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzfz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */