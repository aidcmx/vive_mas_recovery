package com.google.android.gms.internal;

import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

@zzji
class zzga
{
  private final Object[] mParams;
  
  zzga(AdRequestParcel paramAdRequestParcel, String paramString, int paramInt)
  {
    this.mParams = zza(paramAdRequestParcel, paramString, paramInt);
  }
  
  private static Object[] zza(AdRequestParcel paramAdRequestParcel, String paramString, int paramInt)
  {
    HashSet localHashSet = new HashSet(Arrays.asList(((String)zzdr.zzbgi.get()).split(",")));
    ArrayList localArrayList = new ArrayList();
    localArrayList.add(paramString);
    if (localHashSet.contains("networkType")) {
      localArrayList.add(Integer.valueOf(paramInt));
    }
    if (localHashSet.contains("birthday")) {
      localArrayList.add(Long.valueOf(paramAdRequestParcel.zzayl));
    }
    if (localHashSet.contains("extras")) {
      localArrayList.add(zzc(paramAdRequestParcel.extras));
    }
    if (localHashSet.contains("gender")) {
      localArrayList.add(Integer.valueOf(paramAdRequestParcel.zzaym));
    }
    if (localHashSet.contains("keywords"))
    {
      if (paramAdRequestParcel.zzayn != null) {
        localArrayList.add(paramAdRequestParcel.zzayn.toString());
      }
    }
    else
    {
      if (localHashSet.contains("isTestDevice")) {
        localArrayList.add(Boolean.valueOf(paramAdRequestParcel.zzayo));
      }
      if (localHashSet.contains("tagForChildDirectedTreatment")) {
        localArrayList.add(Integer.valueOf(paramAdRequestParcel.zzayp));
      }
      if (localHashSet.contains("manualImpressionsEnabled")) {
        localArrayList.add(Boolean.valueOf(paramAdRequestParcel.zzayq));
      }
      if (localHashSet.contains("publisherProvidedId")) {
        localArrayList.add(paramAdRequestParcel.zzayr);
      }
      if (localHashSet.contains("location"))
      {
        if (paramAdRequestParcel.zzayt == null) {
          break label447;
        }
        localArrayList.add(paramAdRequestParcel.zzayt.toString());
      }
      label289:
      if (localHashSet.contains("contentUrl")) {
        localArrayList.add(paramAdRequestParcel.zzayu);
      }
      if (localHashSet.contains("networkExtras")) {
        localArrayList.add(zzc(paramAdRequestParcel.zzayv));
      }
      if (localHashSet.contains("customTargeting")) {
        localArrayList.add(zzc(paramAdRequestParcel.zzayw));
      }
      if (localHashSet.contains("categoryExclusions"))
      {
        if (paramAdRequestParcel.zzayx == null) {
          break label457;
        }
        localArrayList.add(paramAdRequestParcel.zzayx.toString());
      }
    }
    for (;;)
    {
      if (localHashSet.contains("requestAgent")) {
        localArrayList.add(paramAdRequestParcel.zzayy);
      }
      if (localHashSet.contains("requestPackage")) {
        localArrayList.add(paramAdRequestParcel.zzayz);
      }
      return localArrayList.toArray();
      localArrayList.add(null);
      break;
      label447:
      localArrayList.add(null);
      break label289;
      label457:
      localArrayList.add(null);
    }
  }
  
  private static String zzc(@Nullable Bundle paramBundle)
  {
    if (paramBundle == null) {
      return null;
    }
    StringBuilder localStringBuilder = new StringBuilder();
    Iterator localIterator = new TreeSet(paramBundle.keySet()).iterator();
    if (localIterator.hasNext())
    {
      Object localObject = paramBundle.get((String)localIterator.next());
      if (localObject == null) {
        localObject = "null";
      }
      for (;;)
      {
        localStringBuilder.append((String)localObject);
        break;
        if ((localObject instanceof Bundle)) {
          localObject = zzc((Bundle)localObject);
        } else {
          localObject = localObject.toString();
        }
      }
    }
    return localStringBuilder.toString();
  }
  
  public boolean equals(Object paramObject)
  {
    if (!(paramObject instanceof zzga)) {
      return false;
    }
    paramObject = (zzga)paramObject;
    return Arrays.equals(this.mParams, ((zzga)paramObject).mParams);
  }
  
  public int hashCode()
  {
    return Arrays.hashCode(this.mParams);
  }
  
  public String toString()
  {
    String str = String.valueOf(Arrays.toString(this.mParams));
    return String.valueOf(str).length() + 24 + "[InterstitialAdPoolKey " + str + "]";
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzga.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */