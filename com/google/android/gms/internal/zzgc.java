package com.google.android.gms.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.client.VideoOptionsParcel;
import com.google.android.gms.ads.internal.client.zzab;
import com.google.android.gms.ads.internal.client.zzp;
import com.google.android.gms.ads.internal.client.zzq;
import com.google.android.gms.ads.internal.client.zzu.zza;
import com.google.android.gms.ads.internal.client.zzw;
import com.google.android.gms.ads.internal.client.zzy;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zzl;
import com.google.android.gms.ads.internal.zzu;

@zzji
public class zzgc
  extends zzu.zza
{
  private final String zzant;
  private final zzfw zzbsn;
  @Nullable
  private zzl zzbss;
  private final zzfy zzbsz;
  @Nullable
  private zzik zzbta;
  private String zzbtb;
  
  public zzgc(Context paramContext, String paramString, zzgz paramzzgz, VersionInfoParcel paramVersionInfoParcel, com.google.android.gms.ads.internal.zzd paramzzd)
  {
    this(paramString, new zzfw(paramContext, paramzzgz, paramVersionInfoParcel, paramzzd));
  }
  
  zzgc(String paramString, zzfw paramzzfw)
  {
    this.zzant = paramString;
    this.zzbsn = paramzzfw;
    this.zzbsz = new zzfy();
    zzu.zzhb().zza(paramzzfw);
  }
  
  private void zznu()
  {
    if ((this.zzbss != null) && (this.zzbta != null)) {
      this.zzbss.zza(this.zzbta, this.zzbtb);
    }
  }
  
  static boolean zzq(AdRequestParcel paramAdRequestParcel)
  {
    paramAdRequestParcel = zzfz.zzk(paramAdRequestParcel);
    return (paramAdRequestParcel != null) && (paramAdRequestParcel.containsKey("gw"));
  }
  
  static boolean zzr(AdRequestParcel paramAdRequestParcel)
  {
    paramAdRequestParcel = zzfz.zzk(paramAdRequestParcel);
    return (paramAdRequestParcel != null) && (paramAdRequestParcel.containsKey("_ad"));
  }
  
  void abort()
  {
    if (this.zzbss != null) {
      return;
    }
    this.zzbss = this.zzbsn.zzbj(this.zzant);
    this.zzbsz.zzc(this.zzbss);
    zznu();
  }
  
  public void destroy()
    throws RemoteException
  {
    if (this.zzbss != null) {
      this.zzbss.destroy();
    }
  }
  
  @Nullable
  public String getMediationAdapterClassName()
    throws RemoteException
  {
    if (this.zzbss != null) {
      return this.zzbss.getMediationAdapterClassName();
    }
    return null;
  }
  
  public boolean isLoading()
    throws RemoteException
  {
    return (this.zzbss != null) && (this.zzbss.isLoading());
  }
  
  public boolean isReady()
    throws RemoteException
  {
    return (this.zzbss != null) && (this.zzbss.isReady());
  }
  
  public void pause()
    throws RemoteException
  {
    if (this.zzbss != null) {
      this.zzbss.pause();
    }
  }
  
  public void resume()
    throws RemoteException
  {
    if (this.zzbss != null) {
      this.zzbss.resume();
    }
  }
  
  public void setManualImpressionsEnabled(boolean paramBoolean)
    throws RemoteException
  {
    abort();
    if (this.zzbss != null) {
      this.zzbss.setManualImpressionsEnabled(paramBoolean);
    }
  }
  
  public void setUserId(String paramString) {}
  
  public void showInterstitial()
    throws RemoteException
  {
    if (this.zzbss != null)
    {
      this.zzbss.showInterstitial();
      return;
    }
    zzkx.zzdi("Interstitial ad must be loaded before showInterstitial().");
  }
  
  public void stopLoading()
    throws RemoteException
  {
    if (this.zzbss != null) {
      this.zzbss.stopLoading();
    }
  }
  
  public void zza(AdSizeParcel paramAdSizeParcel)
    throws RemoteException
  {
    if (this.zzbss != null) {
      this.zzbss.zza(paramAdSizeParcel);
    }
  }
  
  public void zza(VideoOptionsParcel paramVideoOptionsParcel)
  {
    throw new IllegalStateException("getVideoController not implemented for interstitials");
  }
  
  public void zza(zzp paramzzp)
    throws RemoteException
  {
    this.zzbsz.zzbsi = paramzzp;
    if (this.zzbss != null) {
      this.zzbsz.zzc(this.zzbss);
    }
  }
  
  public void zza(zzq paramzzq)
    throws RemoteException
  {
    this.zzbsz.zzanl = paramzzq;
    if (this.zzbss != null) {
      this.zzbsz.zzc(this.zzbss);
    }
  }
  
  public void zza(zzw paramzzw)
    throws RemoteException
  {
    this.zzbsz.zzbsf = paramzzw;
    if (this.zzbss != null) {
      this.zzbsz.zzc(this.zzbss);
    }
  }
  
  public void zza(zzy paramzzy)
    throws RemoteException
  {
    abort();
    if (this.zzbss != null) {
      this.zzbss.zza(paramzzy);
    }
  }
  
  public void zza(com.google.android.gms.ads.internal.reward.client.zzd paramzzd)
  {
    this.zzbsz.zzbsj = paramzzd;
    if (this.zzbss != null) {
      this.zzbsz.zzc(this.zzbss);
    }
  }
  
  public void zza(zzed paramzzed)
    throws RemoteException
  {
    this.zzbsz.zzbsh = paramzzed;
    if (this.zzbss != null) {
      this.zzbsz.zzc(this.zzbss);
    }
  }
  
  public void zza(zzig paramzzig)
    throws RemoteException
  {
    this.zzbsz.zzbsg = paramzzig;
    if (this.zzbss != null) {
      this.zzbsz.zzc(this.zzbss);
    }
  }
  
  public void zza(zzik paramzzik, String paramString)
    throws RemoteException
  {
    this.zzbta = paramzzik;
    this.zzbtb = paramString;
    zznu();
  }
  
  public boolean zzb(AdRequestParcel paramAdRequestParcel)
    throws RemoteException
  {
    if (((Boolean)zzdr.zzbge.get()).booleanValue()) {
      AdRequestParcel.zzj(paramAdRequestParcel);
    }
    if (!zzq(paramAdRequestParcel)) {
      abort();
    }
    if (zzfz.zzm(paramAdRequestParcel)) {
      abort();
    }
    if (paramAdRequestParcel.zzays != null) {
      abort();
    }
    if (this.zzbss != null) {
      return this.zzbss.zzb(paramAdRequestParcel);
    }
    Object localObject = zzu.zzhb();
    if (zzr(paramAdRequestParcel)) {
      ((zzfz)localObject).zzb(paramAdRequestParcel, this.zzant);
    }
    localObject = ((zzfz)localObject).zza(paramAdRequestParcel, this.zzant);
    if (localObject != null)
    {
      if (!((zzgb.zza)localObject).zzbsw) {
        ((zzgb.zza)localObject).zznt();
      }
      this.zzbss = ((zzgb.zza)localObject).zzbss;
      ((zzgb.zza)localObject).zzbsu.zza(this.zzbsz);
      this.zzbsz.zzc(this.zzbss);
      zznu();
      return ((zzgb.zza)localObject).zzbsx;
    }
    abort();
    return this.zzbss.zzb(paramAdRequestParcel);
  }
  
  @Nullable
  public com.google.android.gms.dynamic.zzd zzef()
    throws RemoteException
  {
    if (this.zzbss != null) {
      return this.zzbss.zzef();
    }
    return null;
  }
  
  @Nullable
  public AdSizeParcel zzeg()
    throws RemoteException
  {
    if (this.zzbss != null) {
      return this.zzbss.zzeg();
    }
    return null;
  }
  
  public void zzei()
    throws RemoteException
  {
    if (this.zzbss != null)
    {
      this.zzbss.zzei();
      return;
    }
    zzkx.zzdi("Interstitial ad must be loaded before pingManualTrackingUrl().");
  }
  
  public zzab zzej()
  {
    throw new IllegalStateException("getVideoController not implemented for interstitials");
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzgc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */