package com.google.android.gms.internal;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.webkit.WebView;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.client.zzm;
import com.google.android.gms.ads.internal.overlay.zzg;
import com.google.android.gms.ads.internal.overlay.zzp;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zzd;
import com.google.android.gms.ads.internal.zze;
import com.google.android.gms.ads.internal.zzu;
import org.json.JSONObject;

@zzji
public class zzgg
  implements zzge
{
  private final zzmd zzbnz;
  
  public zzgg(Context paramContext, VersionInfoParcel paramVersionInfoParcel, @Nullable zzav paramzzav, zzd paramzzd)
  {
    this.zzbnz = zzu.zzgn().zza(paramContext, new AdSizeParcel(), false, false, paramzzav, paramVersionInfoParcel, null, null, paramzzd);
    this.zzbnz.getWebView().setWillNotDraw(true);
  }
  
  private void runOnUiThread(Runnable paramRunnable)
  {
    if (zzm.zzkr().zzwq())
    {
      paramRunnable.run();
      return;
    }
    zzlb.zzcvl.post(paramRunnable);
  }
  
  public void destroy()
  {
    this.zzbnz.destroy();
  }
  
  public void zza(com.google.android.gms.ads.internal.client.zza paramzza, zzg paramzzg, zzfa paramzzfa, zzp paramzzp, boolean paramBoolean, zzfg paramzzfg, zzfi paramzzfi, zze paramzze, zzhw paramzzhw)
  {
    this.zzbnz.zzxc().zza(paramzza, paramzzg, paramzzfa, paramzzp, paramBoolean, paramzzfg, paramzzfi, new zze(this.zzbnz.getContext(), false), paramzzhw, null);
  }
  
  public void zza(zzge.zza paramzza)
  {
    this.zzbnz.zzxc().zza(new zzgg.6(this, paramzza));
  }
  
  public void zza(String paramString, zzfe paramzzfe)
  {
    this.zzbnz.zzxc().zza(paramString, paramzzfe);
  }
  
  public void zza(String paramString, JSONObject paramJSONObject)
  {
    runOnUiThread(new zzgg.1(this, paramString, paramJSONObject));
  }
  
  public void zzb(String paramString, zzfe paramzzfe)
  {
    this.zzbnz.zzxc().zzb(paramString, paramzzfe);
  }
  
  public void zzb(String paramString, JSONObject paramJSONObject)
  {
    this.zzbnz.zzb(paramString, paramJSONObject);
  }
  
  public void zzbo(String paramString)
  {
    runOnUiThread(new zzgg.3(this, String.format("<!DOCTYPE html><html><head><script src=\"%s\"></script></head><body></body></html>", new Object[] { paramString })));
  }
  
  public void zzbp(String paramString)
  {
    runOnUiThread(new zzgg.5(this, paramString));
  }
  
  public void zzbq(String paramString)
  {
    runOnUiThread(new zzgg.4(this, paramString));
  }
  
  public void zzi(String paramString1, String paramString2)
  {
    runOnUiThread(new zzgg.2(this, paramString1, paramString2));
  }
  
  public zzgj zznw()
  {
    return new zzgk(this);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzgg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */