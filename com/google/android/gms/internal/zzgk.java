package com.google.android.gms.internal;

import java.util.AbstractMap.SimpleEntry;
import java.util.HashSet;
import java.util.Iterator;
import org.json.JSONObject;

@zzji
public class zzgk
  implements zzgj
{
  private final zzgi zzbuq;
  private final HashSet<AbstractMap.SimpleEntry<String, zzfe>> zzbur;
  
  public zzgk(zzgi paramzzgi)
  {
    this.zzbuq = paramzzgi;
    this.zzbur = new HashSet();
  }
  
  public void zza(String paramString, zzfe paramzzfe)
  {
    this.zzbuq.zza(paramString, paramzzfe);
    this.zzbur.add(new AbstractMap.SimpleEntry(paramString, paramzzfe));
  }
  
  public void zza(String paramString, JSONObject paramJSONObject)
  {
    this.zzbuq.zza(paramString, paramJSONObject);
  }
  
  public void zzb(String paramString, zzfe paramzzfe)
  {
    this.zzbuq.zzb(paramString, paramzzfe);
    this.zzbur.remove(new AbstractMap.SimpleEntry(paramString, paramzzfe));
  }
  
  public void zzb(String paramString, JSONObject paramJSONObject)
  {
    this.zzbuq.zzb(paramString, paramJSONObject);
  }
  
  public void zzi(String paramString1, String paramString2)
  {
    this.zzbuq.zzi(paramString1, paramString2);
  }
  
  public void zzod()
  {
    Iterator localIterator = this.zzbur.iterator();
    if (localIterator.hasNext())
    {
      AbstractMap.SimpleEntry localSimpleEntry = (AbstractMap.SimpleEntry)localIterator.next();
      String str = String.valueOf(((zzfe)localSimpleEntry.getValue()).toString());
      if (str.length() != 0) {}
      for (str = "Unregistering eventhandler: ".concat(str);; str = new String("Unregistering eventhandler: "))
      {
        zzkx.v(str);
        this.zzbuq.zzb((String)localSimpleEntry.getKey(), (zzfe)localSimpleEntry.getValue());
        break;
      }
    }
    this.zzbur.clear();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzgk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */