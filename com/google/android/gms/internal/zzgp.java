package com.google.android.gms.internal;

import android.support.annotation.Nullable;
import com.google.android.gms.ads.internal.zzu;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@zzji
public final class zzgp
{
  public final String zzbus;
  public final String zzbut;
  public final List<String> zzbuu;
  public final String zzbuv;
  public final String zzbuw;
  public final List<String> zzbux;
  public final List<String> zzbuy;
  public final List<String> zzbuz;
  public final String zzbva;
  public final List<String> zzbvb;
  public final List<String> zzbvc;
  @Nullable
  public final String zzbvd;
  @Nullable
  public final String zzbve;
  public final String zzbvf;
  @Nullable
  public final List<String> zzbvg;
  public final String zzbvh;
  
  public zzgp(String paramString1, String paramString2, List<String> paramList1, String paramString3, String paramString4, List<String> paramList2, List<String> paramList3, String paramString5, String paramString6, List<String> paramList4, List<String> paramList5, String paramString7, String paramString8, String paramString9, List<String> paramList6, String paramString10, List<String> paramList7)
  {
    this.zzbus = paramString1;
    this.zzbut = paramString2;
    this.zzbuu = paramList1;
    this.zzbuv = paramString3;
    this.zzbuw = paramString4;
    this.zzbux = paramList2;
    this.zzbuy = paramList3;
    this.zzbva = paramString5;
    this.zzbvb = paramList4;
    this.zzbvc = paramList5;
    this.zzbvd = paramString7;
    this.zzbve = paramString8;
    this.zzbvf = paramString9;
    this.zzbvg = paramList6;
    this.zzbvh = paramString10;
    this.zzbuz = paramList7;
  }
  
  public zzgp(JSONObject paramJSONObject)
    throws JSONException
  {
    this.zzbut = paramJSONObject.getString("id");
    Object localObject1 = paramJSONObject.getJSONArray("adapters");
    Object localObject3 = new ArrayList(((JSONArray)localObject1).length());
    int i = 0;
    while (i < ((JSONArray)localObject1).length())
    {
      ((List)localObject3).add(((JSONArray)localObject1).getString(i));
      i += 1;
    }
    this.zzbuu = Collections.unmodifiableList((List)localObject3);
    this.zzbuv = paramJSONObject.optString("allocation_id", null);
    this.zzbux = zzu.zzhf().zza(paramJSONObject, "clickurl");
    this.zzbuy = zzu.zzhf().zza(paramJSONObject, "imp_urls");
    this.zzbuz = zzu.zzhf().zza(paramJSONObject, "fill_urls");
    this.zzbvb = zzu.zzhf().zza(paramJSONObject, "video_start_urls");
    this.zzbvc = zzu.zzhf().zza(paramJSONObject, "video_complete_urls");
    localObject1 = paramJSONObject.optJSONObject("ad");
    if (localObject1 != null)
    {
      localObject1 = ((JSONObject)localObject1).toString();
      this.zzbus = ((String)localObject1);
      localObject3 = paramJSONObject.optJSONObject("data");
      if (localObject3 == null) {
        break label301;
      }
      localObject1 = ((JSONObject)localObject3).toString();
      label192:
      this.zzbva = ((String)localObject1);
      if (localObject3 == null) {
        break label306;
      }
      localObject1 = ((JSONObject)localObject3).optString("class_name");
      label210:
      this.zzbuw = ((String)localObject1);
      this.zzbvd = paramJSONObject.optString("html_template", null);
      this.zzbve = paramJSONObject.optString("ad_base_url", null);
      localObject1 = paramJSONObject.optJSONObject("assets");
      if (localObject1 == null) {
        break label311;
      }
    }
    label301:
    label306:
    label311:
    for (localObject1 = ((JSONObject)localObject1).toString();; localObject1 = null)
    {
      this.zzbvf = ((String)localObject1);
      this.zzbvg = zzu.zzhf().zza(paramJSONObject, "template_ids");
      localObject1 = paramJSONObject.optJSONObject("ad_loader_options");
      paramJSONObject = (JSONObject)localObject2;
      if (localObject1 != null) {
        paramJSONObject = ((JSONObject)localObject1).toString();
      }
      this.zzbvh = paramJSONObject;
      return;
      localObject1 = null;
      break;
      localObject1 = null;
      break label192;
      localObject1 = null;
      break label210;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzgp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */