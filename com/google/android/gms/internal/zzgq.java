package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.reward.mediation.client.RewardItemParcel;
import com.google.android.gms.ads.internal.zzu;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@zzji
public final class zzgq
{
  public final List<zzgp> zzbvi;
  public final long zzbvj;
  public final List<String> zzbvk;
  public final List<String> zzbvl;
  public final List<String> zzbvm;
  public final List<String> zzbvn;
  public final boolean zzbvo;
  public final String zzbvp;
  public final long zzbvq;
  public final String zzbvr;
  public final int zzbvs;
  public final int zzbvt;
  public final long zzbvu;
  public final boolean zzbvv;
  public int zzbvw;
  public int zzbvx;
  
  public zzgq(String paramString)
    throws JSONException
  {
    Object localObject = new JSONObject(paramString);
    if (zzkx.zzbi(2))
    {
      paramString = String.valueOf(((JSONObject)localObject).toString(2));
      if (paramString.length() == 0) {
        break label138;
      }
    }
    ArrayList localArrayList;
    int j;
    label138:
    for (paramString = "Mediation Response JSON: ".concat(paramString);; paramString = new String("Mediation Response JSON: "))
    {
      zzkx.v(paramString);
      paramString = ((JSONObject)localObject).getJSONArray("ad_networks");
      localArrayList = new ArrayList(paramString.length());
      int i = 0;
      int k;
      for (j = -1; i < paramString.length(); j = k)
      {
        zzgp localzzgp = new zzgp(paramString.getJSONObject(i));
        localArrayList.add(localzzgp);
        k = j;
        if (j < 0)
        {
          k = j;
          if (zza(localzzgp)) {
            k = i;
          }
        }
        i += 1;
      }
    }
    this.zzbvw = j;
    this.zzbvx = paramString.length();
    this.zzbvi = Collections.unmodifiableList(localArrayList);
    this.zzbvp = ((JSONObject)localObject).getString("qdata");
    this.zzbvt = ((JSONObject)localObject).optInt("fs_model_type", -1);
    this.zzbvu = ((JSONObject)localObject).optLong("timeout_ms", -1L);
    paramString = ((JSONObject)localObject).optJSONObject("settings");
    if (paramString != null)
    {
      this.zzbvj = paramString.optLong("ad_network_timeout_millis", -1L);
      this.zzbvk = zzu.zzhf().zza(paramString, "click_urls");
      this.zzbvl = zzu.zzhf().zza(paramString, "imp_urls");
      this.zzbvm = zzu.zzhf().zza(paramString, "nofill_urls");
      this.zzbvn = zzu.zzhf().zza(paramString, "remote_ping_urls");
      this.zzbvo = paramString.optBoolean("render_in_browser", false);
      long l = paramString.optLong("refresh", -1L);
      if (l > 0L)
      {
        l *= 1000L;
        this.zzbvq = l;
        localObject = RewardItemParcel.zza(paramString.optJSONArray("rewards"));
        if (localObject != null) {
          break label376;
        }
        this.zzbvr = null;
      }
      for (this.zzbvs = 0;; this.zzbvs = ((RewardItemParcel)localObject).zzcsc)
      {
        this.zzbvv = paramString.optBoolean("use_displayed_impression", false);
        return;
        l = -1L;
        break;
        label376:
        this.zzbvr = ((RewardItemParcel)localObject).type;
      }
    }
    this.zzbvj = -1L;
    this.zzbvk = null;
    this.zzbvl = null;
    this.zzbvm = null;
    this.zzbvn = null;
    this.zzbvq = -1L;
    this.zzbvr = null;
    this.zzbvs = 0;
    this.zzbvv = false;
    this.zzbvo = false;
  }
  
  public zzgq(List<zzgp> paramList, long paramLong1, List<String> paramList1, List<String> paramList2, List<String> paramList3, List<String> paramList4, boolean paramBoolean1, String paramString1, long paramLong2, int paramInt1, int paramInt2, String paramString2, int paramInt3, int paramInt4, long paramLong3, boolean paramBoolean2)
  {
    this.zzbvi = paramList;
    this.zzbvj = paramLong1;
    this.zzbvk = paramList1;
    this.zzbvl = paramList2;
    this.zzbvm = paramList3;
    this.zzbvn = paramList4;
    this.zzbvo = paramBoolean1;
    this.zzbvp = paramString1;
    this.zzbvq = paramLong2;
    this.zzbvw = paramInt1;
    this.zzbvx = paramInt2;
    this.zzbvr = paramString2;
    this.zzbvs = paramInt3;
    this.zzbvt = paramInt4;
    this.zzbvu = paramLong3;
    this.zzbvv = paramBoolean2;
  }
  
  private boolean zza(zzgp paramzzgp)
  {
    paramzzgp = paramzzgp.zzbuu.iterator();
    while (paramzzgp.hasNext()) {
      if (((String)paramzzgp.next()).equals("com.google.ads.mediation.admob.AdMobAdapter")) {
        return true;
      }
    }
    return false;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzgq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */