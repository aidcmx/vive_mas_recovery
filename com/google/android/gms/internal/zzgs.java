package com.google.android.gms.internal;

import android.support.annotation.Nullable;

@zzji
public final class zzgs
  extends zzhb.zza
{
  private final Object zzako = new Object();
  private zzgu.zza zzbvy;
  private zzgr zzbvz;
  
  public void onAdClicked()
  {
    synchronized (this.zzako)
    {
      if (this.zzbvz != null) {
        this.zzbvz.zzes();
      }
      return;
    }
  }
  
  public void onAdClosed()
  {
    synchronized (this.zzako)
    {
      if (this.zzbvz != null) {
        this.zzbvz.zzet();
      }
      return;
    }
  }
  
  public void onAdFailedToLoad(int paramInt)
  {
    for (;;)
    {
      synchronized (this.zzako)
      {
        if (this.zzbvy != null)
        {
          if (paramInt == 3)
          {
            paramInt = 1;
            this.zzbvy.zzad(paramInt);
            this.zzbvy = null;
          }
        }
        else {
          return;
        }
      }
      paramInt = 2;
    }
  }
  
  public void onAdImpression()
  {
    synchronized (this.zzako)
    {
      if (this.zzbvz != null) {
        this.zzbvz.zzex();
      }
      return;
    }
  }
  
  public void onAdLeftApplication()
  {
    synchronized (this.zzako)
    {
      if (this.zzbvz != null) {
        this.zzbvz.zzeu();
      }
      return;
    }
  }
  
  public void onAdLoaded()
  {
    synchronized (this.zzako)
    {
      if (this.zzbvy != null)
      {
        this.zzbvy.zzad(0);
        this.zzbvy = null;
        return;
      }
      if (this.zzbvz != null) {
        this.zzbvz.zzew();
      }
      return;
    }
  }
  
  public void onAdOpened()
  {
    synchronized (this.zzako)
    {
      if (this.zzbvz != null) {
        this.zzbvz.zzev();
      }
      return;
    }
  }
  
  public void zza(@Nullable zzgr paramzzgr)
  {
    synchronized (this.zzako)
    {
      this.zzbvz = paramzzgr;
      return;
    }
  }
  
  public void zza(zzgu.zza paramzza)
  {
    synchronized (this.zzako)
    {
      this.zzbvy = paramzza;
      return;
    }
  }
  
  public void zza(zzhc paramzzhc)
  {
    synchronized (this.zzako)
    {
      if (this.zzbvy != null)
      {
        this.zzbvy.zza(0, paramzzhc);
        this.zzbvy = null;
        return;
      }
      if (this.zzbvz != null) {
        this.zzbvz.zzew();
      }
      return;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzgs.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */