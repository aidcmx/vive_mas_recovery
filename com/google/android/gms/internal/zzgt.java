package com.google.android.gms.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.os.SystemClock;
import android.text.TextUtils;
import com.google.ads.mediation.AdUrlAdapter;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.NativeAdOptions.Builder;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.ads.mediation.MediationAdapter;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

@zzji
public class zzgt
  implements zzgu.zza
{
  private final Context mContext;
  private final Object zzako = new Object();
  private final zzgz zzamf;
  private final NativeAdOptionsParcel zzanq;
  private final List<String> zzanr;
  private final VersionInfoParcel zzanu;
  private AdRequestParcel zzapj;
  private final AdSizeParcel zzapp;
  private final boolean zzasz;
  private final String zzbwa;
  private final long zzbwb;
  private final zzgq zzbwc;
  private final zzgp zzbwd;
  private final boolean zzbwe;
  private zzha zzbwf;
  private int zzbwg = -2;
  private zzhc zzbwh;
  
  public zzgt(Context paramContext, String paramString, zzgz paramzzgz, zzgq paramzzgq, zzgp paramzzgp, AdRequestParcel paramAdRequestParcel, AdSizeParcel paramAdSizeParcel, VersionInfoParcel paramVersionInfoParcel, boolean paramBoolean1, boolean paramBoolean2, NativeAdOptionsParcel paramNativeAdOptionsParcel, List<String> paramList)
  {
    this.mContext = paramContext;
    this.zzamf = paramzzgz;
    this.zzbwd = paramzzgp;
    if ("com.google.ads.mediation.customevent.CustomEventAdapter".equals(paramString))
    {
      this.zzbwa = zzof();
      this.zzbwc = paramzzgq;
      if (paramzzgq.zzbvj == -1L) {
        break label136;
      }
    }
    label136:
    for (long l = paramzzgq.zzbvj;; l = 10000L)
    {
      this.zzbwb = l;
      this.zzapj = paramAdRequestParcel;
      this.zzapp = paramAdSizeParcel;
      this.zzanu = paramVersionInfoParcel;
      this.zzasz = paramBoolean1;
      this.zzbwe = paramBoolean2;
      this.zzanq = paramNativeAdOptionsParcel;
      this.zzanr = paramList;
      return;
      this.zzbwa = paramString;
      break;
    }
  }
  
  private long zza(long paramLong1, long paramLong2, long paramLong3, long paramLong4)
  {
    for (;;)
    {
      if (this.zzbwg != -2) {
        return zzu.zzgs().elapsedRealtime() - paramLong1;
      }
      zzb(paramLong1, paramLong2, paramLong3, paramLong4);
    }
  }
  
  private void zza(zzgs paramzzgs)
  {
    String str = zzbr(this.zzbwd.zzbva);
    try
    {
      if (this.zzanu.zzcyb < 4100000)
      {
        if (this.zzapp.zzazr)
        {
          this.zzbwf.zza(com.google.android.gms.dynamic.zze.zzac(this.mContext), this.zzapj, str, paramzzgs);
          return;
        }
        this.zzbwf.zza(com.google.android.gms.dynamic.zze.zzac(this.mContext), this.zzapp, this.zzapj, str, paramzzgs);
        return;
      }
    }
    catch (RemoteException paramzzgs)
    {
      zzkx.zzc("Could not request ad from mediation adapter.", paramzzgs);
      zzad(5);
      return;
    }
    if (this.zzasz)
    {
      this.zzbwf.zza(com.google.android.gms.dynamic.zze.zzac(this.mContext), this.zzapj, str, this.zzbwd.zzbus, paramzzgs, this.zzanq, this.zzanr);
      return;
    }
    if (this.zzapp.zzazr)
    {
      this.zzbwf.zza(com.google.android.gms.dynamic.zze.zzac(this.mContext), this.zzapj, str, this.zzbwd.zzbus, paramzzgs);
      return;
    }
    if (this.zzbwe)
    {
      if (this.zzbwd.zzbvd != null)
      {
        this.zzbwf.zza(com.google.android.gms.dynamic.zze.zzac(this.mContext), this.zzapj, str, this.zzbwd.zzbus, paramzzgs, new NativeAdOptionsParcel(zzbs(this.zzbwd.zzbvh)), this.zzbwd.zzbvg);
        return;
      }
      this.zzbwf.zza(com.google.android.gms.dynamic.zze.zzac(this.mContext), this.zzapp, this.zzapj, str, this.zzbwd.zzbus, paramzzgs);
      return;
    }
    this.zzbwf.zza(com.google.android.gms.dynamic.zze.zzac(this.mContext), this.zzapp, this.zzapj, str, this.zzbwd.zzbus, paramzzgs);
  }
  
  private boolean zzae(int paramInt)
  {
    boolean bool = false;
    for (;;)
    {
      try
      {
        Bundle localBundle;
        if (this.zzasz)
        {
          localBundle = this.zzbwf.zzop();
          if (localBundle != null)
          {
            if ((localBundle.getInt("capabilities", 0) & paramInt) == paramInt) {
              bool = true;
            }
          }
          else {
            return bool;
          }
        }
        else
        {
          if (this.zzapp.zzazr)
          {
            localBundle = this.zzbwf.getInterstitialAdapterInfo();
            continue;
          }
          localBundle = this.zzbwf.zzoo();
          continue;
        }
        bool = false;
      }
      catch (RemoteException localRemoteException)
      {
        zzkx.zzdi("Could not get adapter info. Returning false");
        return false;
      }
    }
  }
  
  private static zzhc zzaf(int paramInt)
  {
    return new zzgt.2(paramInt);
  }
  
  private void zzb(long paramLong1, long paramLong2, long paramLong3, long paramLong4)
  {
    long l = SystemClock.elapsedRealtime();
    paramLong1 = paramLong2 - (l - paramLong1);
    paramLong2 = paramLong4 - (l - paramLong3);
    if ((paramLong1 <= 0L) || (paramLong2 <= 0L))
    {
      zzkx.zzdh("Timed out waiting for adapter.");
      this.zzbwg = 3;
      return;
    }
    try
    {
      this.zzako.wait(Math.min(paramLong1, paramLong2));
      return;
    }
    catch (InterruptedException localInterruptedException)
    {
      this.zzbwg = -1;
    }
  }
  
  private String zzbr(String paramString)
  {
    if ((paramString == null) || (!zzoi()) || (zzae(2))) {
      return paramString;
    }
    try
    {
      Object localObject = new JSONObject(paramString);
      ((JSONObject)localObject).remove("cpm_floor_cents");
      localObject = ((JSONObject)localObject).toString();
      return (String)localObject;
    }
    catch (JSONException localJSONException)
    {
      zzkx.zzdi("Could not remove field. Returning the original value");
    }
    return paramString;
  }
  
  private static NativeAdOptions zzbs(String paramString)
  {
    NativeAdOptions.Builder localBuilder = new NativeAdOptions.Builder();
    if (paramString == null) {
      return localBuilder.build();
    }
    try
    {
      paramString = new JSONObject(paramString);
      localBuilder.setRequestMultipleImages(paramString.optBoolean("multiple_images", false));
      localBuilder.setReturnUrlsForImageAssets(paramString.optBoolean("only_urls", false));
      localBuilder.setImageOrientation(zzbt(paramString.optString("native_image_orientation", "any")));
      return localBuilder.build();
    }
    catch (JSONException paramString)
    {
      for (;;)
      {
        zzkx.zzc("Exception occurred when creating native ad options", paramString);
      }
    }
  }
  
  private static int zzbt(String paramString)
  {
    if ("landscape".equals(paramString)) {
      return 2;
    }
    if ("portrait".equals(paramString)) {
      return 1;
    }
    return 0;
  }
  
  private String zzof()
  {
    try
    {
      if (!TextUtils.isEmpty(this.zzbwd.zzbuw))
      {
        if (this.zzamf.zzbv(this.zzbwd.zzbuw)) {
          return "com.google.android.gms.ads.mediation.customevent.CustomEventAdapter";
        }
        return "com.google.ads.mediation.customevent.CustomEventAdapter";
      }
    }
    catch (RemoteException localRemoteException)
    {
      zzkx.zzdi("Fail to determine the custom event's version, assuming the old one.");
    }
    return "com.google.ads.mediation.customevent.CustomEventAdapter";
  }
  
  private zzhc zzog()
  {
    if ((this.zzbwg != 0) || (!zzoi())) {
      return null;
    }
    try
    {
      if ((zzae(4)) && (this.zzbwh != null) && (this.zzbwh.zzok() != 0))
      {
        zzhc localzzhc = this.zzbwh;
        return localzzhc;
      }
    }
    catch (RemoteException localRemoteException)
    {
      zzkx.zzdi("Could not get cpm value from MediationResponseMetadata");
    }
    return zzaf(zzoj());
  }
  
  private zzha zzoh()
  {
    Object localObject = String.valueOf(this.zzbwa);
    if (((String)localObject).length() != 0) {}
    for (localObject = "Instantiating mediation adapter: ".concat((String)localObject);; localObject = new String("Instantiating mediation adapter: "))
    {
      zzkx.zzdh((String)localObject);
      if (this.zzasz) {
        break label156;
      }
      if ((!((Boolean)zzdr.zzbhh.get()).booleanValue()) || (!"com.google.ads.mediation.admob.AdMobAdapter".equals(this.zzbwa))) {
        break;
      }
      return zza(new AdMobAdapter());
    }
    if ((((Boolean)zzdr.zzbhi.get()).booleanValue()) && ("com.google.ads.mediation.AdUrlAdapter".equals(this.zzbwa))) {
      return zza(new AdUrlAdapter());
    }
    if ("com.google.ads.mediation.admob.AdMobCustomTabsAdapter".equals(this.zzbwa)) {
      return new zzhg(new zzho());
    }
    try
    {
      label156:
      localObject = this.zzamf.zzbu(this.zzbwa);
      return (zzha)localObject;
    }
    catch (RemoteException localRemoteException)
    {
      localObject = String.valueOf(this.zzbwa);
      if (((String)localObject).length() == 0) {}
    }
    for (localObject = "Could not instantiate mediation adapter: ".concat((String)localObject);; localObject = new String("Could not instantiate mediation adapter: "))
    {
      zzkx.zza((String)localObject, localRemoteException);
      return null;
    }
  }
  
  private boolean zzoi()
  {
    return this.zzbwc.zzbvt != -1;
  }
  
  private int zzoj()
  {
    int j;
    if (this.zzbwd.zzbva == null)
    {
      j = 0;
      return j;
    }
    try
    {
      JSONObject localJSONObject = new JSONObject(this.zzbwd.zzbva);
      if ("com.google.ads.mediation.admob.AdMobAdapter".equals(this.zzbwa)) {
        return localJSONObject.optInt("cpm_cents", 0);
      }
    }
    catch (JSONException localJSONException)
    {
      zzkx.zzdi("Could not convert to json. Returning 0");
      return 0;
    }
    if (zzae(2)) {}
    for (int i = localJSONException.optInt("cpm_floor_cents", 0);; i = 0)
    {
      j = i;
      if (i != 0) {
        break;
      }
      return localJSONException.optInt("penalized_average_cpm_cents", 0);
    }
  }
  
  public void cancel()
  {
    synchronized (this.zzako)
    {
      try
      {
        if (this.zzbwf != null) {
          this.zzbwf.destroy();
        }
        this.zzbwg = -1;
        this.zzako.notify();
        return;
      }
      catch (RemoteException localRemoteException)
      {
        for (;;)
        {
          zzkx.zzc("Could not destroy mediation adapter.", localRemoteException);
        }
      }
    }
  }
  
  public zzgu zza(long paramLong1, long paramLong2)
  {
    synchronized (this.zzako)
    {
      long l = SystemClock.elapsedRealtime();
      Object localObject2 = new zzgs();
      zzlb.zzcvl.post(new zzgt.1(this, (zzgs)localObject2));
      paramLong1 = zza(l, this.zzbwb, paramLong1, paramLong2);
      localObject2 = new zzgu(this.zzbwd, this.zzbwf, this.zzbwa, (zzgs)localObject2, this.zzbwg, zzog(), paramLong1);
      return (zzgu)localObject2;
    }
  }
  
  protected zzha zza(MediationAdapter paramMediationAdapter)
  {
    return new zzhg(paramMediationAdapter);
  }
  
  public void zza(int paramInt, zzhc paramzzhc)
  {
    synchronized (this.zzako)
    {
      this.zzbwg = paramInt;
      this.zzbwh = paramzzhc;
      this.zzako.notify();
      return;
    }
  }
  
  public void zzad(int paramInt)
  {
    synchronized (this.zzako)
    {
      this.zzbwg = paramInt;
      this.zzako.notify();
      return;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzgt.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */