package com.google.android.gms.internal;

import android.content.Context;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.zzu;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Future;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@zzji
public class zzgv
{
  public List<String> zza(JSONObject paramJSONObject, String paramString)
    throws JSONException
  {
    paramJSONObject = paramJSONObject.optJSONArray(paramString);
    if (paramJSONObject != null)
    {
      paramString = new ArrayList(paramJSONObject.length());
      int i = 0;
      while (i < paramJSONObject.length())
      {
        paramString.add(paramJSONObject.getString(i));
        i += 1;
      }
      return Collections.unmodifiableList(paramString);
    }
    return null;
  }
  
  public void zza(Context paramContext, String paramString1, zzko paramzzko, String paramString2, boolean paramBoolean, List<String> paramList)
  {
    if ((paramList == null) || (paramList.isEmpty())) {
      return;
    }
    if (paramBoolean) {}
    for (String str = "1";; str = "0")
    {
      Iterator localIterator = paramList.iterator();
      while (localIterator.hasNext())
      {
        Object localObject = ((String)localIterator.next()).replaceAll("@gw_adlocid@", paramString2).replaceAll("@gw_adnetrefresh@", str).replaceAll("@gw_qdata@", paramzzko.zzcsk.zzbvp).replaceAll("@gw_sdkver@", paramString1).replaceAll("@gw_sessid@", zzu.zzgq().getSessionId()).replaceAll("@gw_seqnum@", paramzzko.zzcjx);
        paramList = (List<String>)localObject;
        if (!TextUtils.isEmpty(paramzzko.zzcsl)) {
          paramList = ((String)localObject).replaceAll("@gw_adnetstatus@", paramzzko.zzcsl);
        }
        localObject = paramList;
        if (paramzzko.zzbwm != null) {
          localObject = paramList.replaceAll("@gw_adnetid@", paramzzko.zzbwm.zzbut).replaceAll("@gw_allocid@", paramzzko.zzbwm.zzbuv);
        }
        paramList = (Future)new zzll(paramContext, paramString1, (String)localObject).zzrz();
      }
      break;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzgv.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */