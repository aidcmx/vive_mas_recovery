package com.google.android.gms.internal;

import android.content.Context;
import android.os.Handler;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@zzji
public class zzgw
  implements zzgo
{
  private final Context mContext;
  private final Object zzako = new Object();
  private final zzgz zzamf;
  private final boolean zzasz;
  private final zzgq zzbwc;
  private final boolean zzbwe;
  private final AdRequestInfoParcel zzbws;
  private final long zzbwt;
  private final long zzbwu;
  private final int zzbwv;
  private boolean zzbww = false;
  private final Map<zzlt<zzgu>, zzgt> zzbwx = new HashMap();
  private List<zzgu> zzbwy = new ArrayList();
  
  public zzgw(Context paramContext, AdRequestInfoParcel paramAdRequestInfoParcel, zzgz paramzzgz, zzgq paramzzgq, boolean paramBoolean1, boolean paramBoolean2, long paramLong1, long paramLong2, int paramInt)
  {
    this.mContext = paramContext;
    this.zzbws = paramAdRequestInfoParcel;
    this.zzamf = paramzzgz;
    this.zzbwc = paramzzgq;
    this.zzasz = paramBoolean1;
    this.zzbwe = paramBoolean2;
    this.zzbwt = paramLong1;
    this.zzbwu = paramLong2;
    this.zzbwv = paramInt;
  }
  
  private void zza(zzlt<zzgu> paramzzlt)
  {
    zzlb.zzcvl.post(new zzgw.2(this, paramzzlt));
  }
  
  private zzgu zze(List<zzlt<zzgu>> paramList)
  {
    for (;;)
    {
      synchronized (this.zzako)
      {
        if (this.zzbww)
        {
          paramList = new zzgu(-1);
          return paramList;
        }
        ??? = paramList.iterator();
        if (((Iterator)???).hasNext()) {
          paramList = (zzlt)((Iterator)???).next();
        }
      }
      try
      {
        zzgu localzzgu = (zzgu)paramList.get();
        this.zzbwy.add(localzzgu);
        if ((localzzgu == null) || (localzzgu.zzbwl != 0)) {
          continue;
        }
        zza(paramList);
        return localzzgu;
      }
      catch (InterruptedException paramList)
      {
        zzkx.zzc("Exception while processing an adapter; continuing with other adapters", paramList);
        continue;
        paramList = finally;
        throw paramList;
        zza(null);
        return new zzgu(1);
      }
      catch (ExecutionException paramList)
      {
        for (;;) {}
      }
    }
  }
  
  /* Error */
  private zzgu zzf(List<zzlt<zzgu>> paramList)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 39	com/google/android/gms/internal/zzgw:zzako	Ljava/lang/Object;
    //   4: astore 8
    //   6: aload 8
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield 41	com/google/android/gms/internal/zzgw:zzbww	Z
    //   13: ifeq +17 -> 30
    //   16: new 105	com/google/android/gms/internal/zzgu
    //   19: dup
    //   20: iconst_m1
    //   21: invokespecial 108	com/google/android/gms/internal/zzgu:<init>	(I)V
    //   24: astore_1
    //   25: aload 8
    //   27: monitorexit
    //   28: aload_1
    //   29: areturn
    //   30: aload 8
    //   32: monitorexit
    //   33: iconst_m1
    //   34: istore_2
    //   35: aconst_null
    //   36: astore 8
    //   38: aconst_null
    //   39: astore 9
    //   41: aload_0
    //   42: getfield 59	com/google/android/gms/internal/zzgw:zzbwc	Lcom/google/android/gms/internal/zzgq;
    //   45: getfield 157	com/google/android/gms/internal/zzgq:zzbvu	J
    //   48: ldc2_w 158
    //   51: lcmp
    //   52: ifeq +170 -> 222
    //   55: aload_0
    //   56: getfield 59	com/google/android/gms/internal/zzgw:zzbwc	Lcom/google/android/gms/internal/zzgq;
    //   59: getfield 157	com/google/android/gms/internal/zzgq:zzbvu	J
    //   62: lstore 4
    //   64: aload_1
    //   65: invokeinterface 114 1 0
    //   70: astore 11
    //   72: aload 11
    //   74: invokeinterface 120 1 0
    //   79: ifeq +225 -> 304
    //   82: aload 11
    //   84: invokeinterface 124 1 0
    //   89: checkcast 126	com/google/android/gms/internal/zzlt
    //   92: astore 10
    //   94: invokestatic 165	com/google/android/gms/ads/internal/zzu:zzgs	()Lcom/google/android/gms/common/util/zze;
    //   97: invokeinterface 171 1 0
    //   102: lstore 6
    //   104: lload 4
    //   106: lconst_0
    //   107: lcmp
    //   108: ifne +122 -> 230
    //   111: aload 10
    //   113: invokeinterface 174 1 0
    //   118: ifeq +112 -> 230
    //   121: aload 10
    //   123: invokeinterface 129 1 0
    //   128: checkcast 105	com/google/android/gms/internal/zzgu
    //   131: astore_1
    //   132: aload_0
    //   133: getfield 51	com/google/android/gms/internal/zzgw:zzbwy	Ljava/util/List;
    //   136: aload_1
    //   137: invokeinterface 133 2 0
    //   142: pop
    //   143: aload_1
    //   144: ifnull +192 -> 336
    //   147: aload_1
    //   148: getfield 136	com/google/android/gms/internal/zzgu:zzbwl	I
    //   151: ifne +185 -> 336
    //   154: aload_1
    //   155: getfield 178	com/google/android/gms/internal/zzgu:zzbwq	Lcom/google/android/gms/internal/zzhc;
    //   158: astore 12
    //   160: aload 12
    //   162: ifnull +174 -> 336
    //   165: aload 12
    //   167: invokeinterface 184 1 0
    //   172: iload_2
    //   173: if_icmple +163 -> 336
    //   176: aload 12
    //   178: invokeinterface 184 1 0
    //   183: istore_3
    //   184: iload_3
    //   185: istore_2
    //   186: aload 10
    //   188: astore 8
    //   190: lload 4
    //   192: invokestatic 165	com/google/android/gms/ads/internal/zzu:zzgs	()Lcom/google/android/gms/common/util/zze;
    //   195: invokeinterface 171 1 0
    //   200: lload 6
    //   202: lsub
    //   203: lsub
    //   204: lconst_0
    //   205: invokestatic 190	java/lang/Math:max	(JJ)J
    //   208: lstore 4
    //   210: aload_1
    //   211: astore 9
    //   213: goto -141 -> 72
    //   216: astore_1
    //   217: aload 8
    //   219: monitorexit
    //   220: aload_1
    //   221: athrow
    //   222: ldc2_w 191
    //   225: lstore 4
    //   227: goto -163 -> 64
    //   230: aload 10
    //   232: lload 4
    //   234: getstatic 198	java/util/concurrent/TimeUnit:MILLISECONDS	Ljava/util/concurrent/TimeUnit;
    //   237: invokeinterface 201 4 0
    //   242: checkcast 105	com/google/android/gms/internal/zzgu
    //   245: astore_1
    //   246: goto -114 -> 132
    //   249: astore_1
    //   250: ldc -116
    //   252: aload_1
    //   253: invokestatic 145	com/google/android/gms/internal/zzkx:zzc	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   256: lload 4
    //   258: invokestatic 165	com/google/android/gms/ads/internal/zzu:zzgs	()Lcom/google/android/gms/common/util/zze;
    //   261: invokeinterface 171 1 0
    //   266: lload 6
    //   268: lsub
    //   269: lsub
    //   270: lconst_0
    //   271: invokestatic 190	java/lang/Math:max	(JJ)J
    //   274: lstore 4
    //   276: aload 9
    //   278: astore_1
    //   279: goto -69 -> 210
    //   282: astore_1
    //   283: lload 4
    //   285: invokestatic 165	com/google/android/gms/ads/internal/zzu:zzgs	()Lcom/google/android/gms/common/util/zze;
    //   288: invokeinterface 171 1 0
    //   293: lload 6
    //   295: lsub
    //   296: lsub
    //   297: lconst_0
    //   298: invokestatic 190	java/lang/Math:max	(JJ)J
    //   301: pop2
    //   302: aload_1
    //   303: athrow
    //   304: aload_0
    //   305: aload 8
    //   307: invokespecial 138	com/google/android/gms/internal/zzgw:zza	(Lcom/google/android/gms/internal/zzlt;)V
    //   310: aload 9
    //   312: ifnonnull +30 -> 342
    //   315: new 105	com/google/android/gms/internal/zzgu
    //   318: dup
    //   319: iconst_1
    //   320: invokespecial 108	com/google/android/gms/internal/zzgu:<init>	(I)V
    //   323: areturn
    //   324: astore_1
    //   325: goto -75 -> 250
    //   328: astore_1
    //   329: goto -79 -> 250
    //   332: astore_1
    //   333: goto -83 -> 250
    //   336: aload 9
    //   338: astore_1
    //   339: goto -149 -> 190
    //   342: aload 9
    //   344: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	345	0	this	zzgw
    //   0	345	1	paramList	List<zzlt<zzgu>>
    //   34	152	2	i	int
    //   183	2	3	j	int
    //   62	222	4	l1	long
    //   102	192	6	l2	long
    //   4	302	8	localObject	Object
    //   39	304	9	localList	List<zzlt<zzgu>>
    //   92	139	10	localzzlt	zzlt
    //   70	13	11	localIterator	Iterator
    //   158	19	12	localzzhc	zzhc
    // Exception table:
    //   from	to	target	type
    //   9	28	216	finally
    //   30	33	216	finally
    //   217	220	216	finally
    //   111	132	249	android/os/RemoteException
    //   132	143	249	android/os/RemoteException
    //   147	160	249	android/os/RemoteException
    //   165	184	249	android/os/RemoteException
    //   230	246	249	android/os/RemoteException
    //   111	132	282	finally
    //   132	143	282	finally
    //   147	160	282	finally
    //   165	184	282	finally
    //   230	246	282	finally
    //   250	256	282	finally
    //   111	132	324	java/lang/InterruptedException
    //   132	143	324	java/lang/InterruptedException
    //   147	160	324	java/lang/InterruptedException
    //   165	184	324	java/lang/InterruptedException
    //   230	246	324	java/lang/InterruptedException
    //   111	132	328	java/util/concurrent/ExecutionException
    //   132	143	328	java/util/concurrent/ExecutionException
    //   147	160	328	java/util/concurrent/ExecutionException
    //   165	184	328	java/util/concurrent/ExecutionException
    //   230	246	328	java/util/concurrent/ExecutionException
    //   111	132	332	java/util/concurrent/TimeoutException
    //   132	143	332	java/util/concurrent/TimeoutException
    //   147	160	332	java/util/concurrent/TimeoutException
    //   165	184	332	java/util/concurrent/TimeoutException
    //   230	246	332	java/util/concurrent/TimeoutException
  }
  
  public void cancel()
  {
    synchronized (this.zzako)
    {
      this.zzbww = true;
      Iterator localIterator = this.zzbwx.values().iterator();
      if (localIterator.hasNext()) {
        ((zzgt)localIterator.next()).cancel();
      }
    }
  }
  
  public zzgu zzd(List<zzgp> paramList)
  {
    zzkx.zzdg("Starting mediation.");
    ExecutorService localExecutorService = Executors.newCachedThreadPool();
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = paramList.iterator();
    if (localIterator.hasNext())
    {
      zzgp localzzgp = (zzgp)localIterator.next();
      paramList = String.valueOf(localzzgp.zzbut);
      if (paramList.length() != 0) {}
      for (paramList = "Trying mediation network: ".concat(paramList);; paramList = new String("Trying mediation network: "))
      {
        zzkx.zzdh(paramList);
        paramList = localzzgp.zzbuu.iterator();
        while (paramList.hasNext())
        {
          Object localObject = (String)paramList.next();
          localObject = new zzgt(this.mContext, (String)localObject, this.zzamf, this.zzbwc, localzzgp, this.zzbws.zzcju, this.zzbws.zzarm, this.zzbws.zzari, this.zzasz, this.zzbwe, this.zzbws.zzasa, this.zzbws.zzase);
          zzlt localzzlt = zzla.zza(localExecutorService, new zzgw.1(this, (zzgt)localObject));
          this.zzbwx.put(localzzlt, localObject);
          localArrayList.add(localzzlt);
        }
        break;
      }
    }
    switch (this.zzbwv)
    {
    default: 
      return zze(localArrayList);
    }
    return zzf(localArrayList);
  }
  
  public List<zzgu> zzoe()
  {
    return this.zzbwy;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzgw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */