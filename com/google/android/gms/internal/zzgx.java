package com.google.android.gms.internal;

import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@zzji
public class zzgx
  implements zzgo
{
  private final Context mContext;
  private final Object zzako = new Object();
  private final zzdz zzalt;
  private final zzgz zzamf;
  private final boolean zzasz;
  private final zzgq zzbwc;
  private final boolean zzbwe;
  private final AdRequestInfoParcel zzbws;
  private final long zzbwt;
  private final long zzbwu;
  private boolean zzbww = false;
  private List<zzgu> zzbwy = new ArrayList();
  private zzgt zzbxc;
  
  public zzgx(Context paramContext, AdRequestInfoParcel paramAdRequestInfoParcel, zzgz paramzzgz, zzgq paramzzgq, boolean paramBoolean1, boolean paramBoolean2, long paramLong1, long paramLong2, zzdz paramzzdz)
  {
    this.mContext = paramContext;
    this.zzbws = paramAdRequestInfoParcel;
    this.zzamf = paramzzgz;
    this.zzbwc = paramzzgq;
    this.zzasz = paramBoolean1;
    this.zzbwe = paramBoolean2;
    this.zzbwt = paramLong1;
    this.zzbwu = paramLong2;
    this.zzalt = paramzzdz;
  }
  
  public void cancel()
  {
    synchronized (this.zzako)
    {
      this.zzbww = true;
      if (this.zzbxc != null) {
        this.zzbxc.cancel();
      }
      return;
    }
  }
  
  public zzgu zzd(List<zzgp> arg1)
  {
    zzkx.zzdg("Starting mediation.");
    Object localObject = new ArrayList();
    zzdx localzzdx1 = this.zzalt.zzlz();
    Iterator localIterator1 = ???.iterator();
    label403:
    while (localIterator1.hasNext())
    {
      zzgp localzzgp = (zzgp)localIterator1.next();
      ??? = String.valueOf(localzzgp.zzbut);
      Iterator localIterator2;
      if (???.length() != 0)
      {
        ??? = "Trying mediation network: ".concat(???);
        zzkx.zzdh(???);
        localIterator2 = localzzgp.zzbuu.iterator();
      }
      for (;;)
      {
        if (!localIterator2.hasNext()) {
          break label403;
        }
        String str = (String)localIterator2.next();
        zzdx localzzdx2 = this.zzalt.zzlz();
        synchronized (this.zzako)
        {
          if (this.zzbww)
          {
            localObject = new zzgu(-1);
            return (zzgu)localObject;
            ??? = new String("Trying mediation network: ");
            break;
          }
          this.zzbxc = new zzgt(this.mContext, str, this.zzamf, this.zzbwc, localzzgp, this.zzbws.zzcju, this.zzbws.zzarm, this.zzbws.zzari, this.zzasz, this.zzbwe, this.zzbws.zzasa, this.zzbws.zzase);
          ??? = this.zzbxc.zza(this.zzbwt, this.zzbwu);
          this.zzbwy.add(???);
          if (???.zzbwl == 0)
          {
            zzkx.zzdg("Adapter succeeded.");
            this.zzalt.zzg("mediation_network_succeed", str);
            if (!((List)localObject).isEmpty()) {
              this.zzalt.zzg("mediation_networks_fail", TextUtils.join(",", (Iterable)localObject));
            }
            this.zzalt.zza(localzzdx2, new String[] { "mls" });
            this.zzalt.zza(localzzdx1, new String[] { "ttm" });
            return (zzgu)???;
          }
        }
        localIterable.add(str);
        this.zzalt.zza(localzzdx2, new String[] { "mlf" });
        if (???.zzbwn != null) {
          zzlb.zzcvl.post(new zzgx.1(this, ???));
        }
      }
    }
    if (!localIterable.isEmpty()) {
      this.zzalt.zzg("mediation_networks_fail", TextUtils.join(",", localIterable));
    }
    return new zzgu(1);
  }
  
  public List<zzgu> zzoe()
  {
    return this.zzbwy;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzgx.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */