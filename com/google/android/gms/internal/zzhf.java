package com.google.android.gms.internal;

import android.location.Location;
import android.support.annotation.Nullable;
import com.google.android.gms.ads.mediation.MediationAdRequest;
import java.util.Date;
import java.util.Set;

@zzji
public final class zzhf
  implements MediationAdRequest
{
  private final int zzazc;
  private final boolean zzazo;
  private final int zzbxg;
  private final Date zzgr;
  private final Set<String> zzgt;
  private final boolean zzgu;
  private final Location zzgv;
  
  public zzhf(@Nullable Date paramDate, int paramInt1, @Nullable Set<String> paramSet, @Nullable Location paramLocation, boolean paramBoolean1, int paramInt2, boolean paramBoolean2)
  {
    this.zzgr = paramDate;
    this.zzazc = paramInt1;
    this.zzgt = paramSet;
    this.zzgv = paramLocation;
    this.zzgu = paramBoolean1;
    this.zzbxg = paramInt2;
    this.zzazo = paramBoolean2;
  }
  
  public Date getBirthday()
  {
    return this.zzgr;
  }
  
  public int getGender()
  {
    return this.zzazc;
  }
  
  public Set<String> getKeywords()
  {
    return this.zzgt;
  }
  
  public Location getLocation()
  {
    return this.zzgv;
  }
  
  public boolean isDesignedForFamilies()
  {
    return this.zzazo;
  }
  
  public boolean isTesting()
  {
    return this.zzgu;
  }
  
  public int taggedForChildDirectedTreatment()
  {
    return this.zzbxg;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzhf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */