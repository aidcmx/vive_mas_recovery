package com.google.android.gms.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel;
import com.google.android.gms.ads.mediation.MediationAdRequest;
import com.google.android.gms.ads.mediation.MediationAdapter;
import com.google.android.gms.ads.mediation.MediationBannerAdapter;
import com.google.android.gms.ads.mediation.MediationInterstitialAdapter;
import com.google.android.gms.ads.mediation.MediationNativeAdapter;
import com.google.android.gms.ads.mediation.NativeAdMapper;
import com.google.android.gms.ads.mediation.NativeAppInstallAdMapper;
import com.google.android.gms.ads.mediation.NativeContentAdMapper;
import com.google.android.gms.ads.mediation.OnContextChangedListener;
import com.google.android.gms.ads.reward.mediation.MediationRewardedVideoAdAdapter;
import com.google.android.gms.dynamic.zzd;
import com.google.android.gms.dynamic.zze;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.json.JSONObject;

@zzji
public final class zzhg
  extends zzha.zza
{
  private final MediationAdapter zzbxh;
  private zzhh zzbxi;
  
  public zzhg(MediationAdapter paramMediationAdapter)
  {
    this.zzbxh = paramMediationAdapter;
  }
  
  private Bundle zza(String paramString1, int paramInt, String paramString2)
    throws RemoteException
  {
    Object localObject = String.valueOf(paramString1);
    if (((String)localObject).length() != 0) {
      localObject = "Server parameters: ".concat((String)localObject);
    }
    for (;;)
    {
      com.google.android.gms.ads.internal.util.client.zzb.zzdi((String)localObject);
      try
      {
        localObject = new Bundle();
        if (paramString1 == null) {
          break;
        }
        paramString1 = new JSONObject(paramString1);
        localObject = new Bundle();
        Iterator localIterator = paramString1.keys();
        while (localIterator.hasNext())
        {
          String str = (String)localIterator.next();
          ((Bundle)localObject).putString(str, paramString1.getString(str));
        }
        localObject = new String("Server parameters: ");
      }
      catch (Throwable paramString1)
      {
        com.google.android.gms.ads.internal.util.client.zzb.zzc("Could not get Server Parameters Bundle.", paramString1);
        throw new RemoteException();
      }
    }
    if ((this.zzbxh instanceof AdMobAdapter))
    {
      ((Bundle)localObject).putString("adJson", paramString2);
      ((Bundle)localObject).putInt("tagForChildDirectedTreatment", paramInt);
    }
    return (Bundle)localObject;
  }
  
  public void destroy()
    throws RemoteException
  {
    try
    {
      this.zzbxh.onDestroy();
      return;
    }
    catch (Throwable localThrowable)
    {
      com.google.android.gms.ads.internal.util.client.zzb.zzc("Could not destroy adapter.", localThrowable);
      throw new RemoteException();
    }
  }
  
  public Bundle getInterstitialAdapterInfo()
  {
    if (!(this.zzbxh instanceof zzmr))
    {
      String str = String.valueOf(this.zzbxh.getClass().getCanonicalName());
      if (str.length() != 0) {}
      for (str = "MediationAdapter is not a v2 MediationInterstitialAdapter: ".concat(str);; str = new String("MediationAdapter is not a v2 MediationInterstitialAdapter: "))
      {
        com.google.android.gms.ads.internal.util.client.zzb.zzdi(str);
        return new Bundle();
      }
    }
    return ((zzmr)this.zzbxh).getInterstitialAdapterInfo();
  }
  
  public zzd getView()
    throws RemoteException
  {
    Object localObject;
    if (!(this.zzbxh instanceof MediationBannerAdapter))
    {
      localObject = String.valueOf(this.zzbxh.getClass().getCanonicalName());
      if (((String)localObject).length() != 0) {}
      for (localObject = "MediationAdapter is not a MediationBannerAdapter: ".concat((String)localObject);; localObject = new String("MediationAdapter is not a MediationBannerAdapter: "))
      {
        com.google.android.gms.ads.internal.util.client.zzb.zzdi((String)localObject);
        throw new RemoteException();
      }
    }
    try
    {
      localObject = zze.zzac(((MediationBannerAdapter)this.zzbxh).getBannerView());
      return (zzd)localObject;
    }
    catch (Throwable localThrowable)
    {
      com.google.android.gms.ads.internal.util.client.zzb.zzc("Could not get banner view from adapter.", localThrowable);
      throw new RemoteException();
    }
  }
  
  public boolean isInitialized()
    throws RemoteException
  {
    if (!(this.zzbxh instanceof MediationRewardedVideoAdAdapter))
    {
      String str = String.valueOf(this.zzbxh.getClass().getCanonicalName());
      if (str.length() != 0) {}
      for (str = "MediationAdapter is not a MediationRewardedVideoAdAdapter: ".concat(str);; str = new String("MediationAdapter is not a MediationRewardedVideoAdAdapter: "))
      {
        com.google.android.gms.ads.internal.util.client.zzb.zzdi(str);
        throw new RemoteException();
      }
    }
    com.google.android.gms.ads.internal.util.client.zzb.zzdg("Check if adapter is initialized.");
    try
    {
      boolean bool = ((MediationRewardedVideoAdAdapter)this.zzbxh).isInitialized();
      return bool;
    }
    catch (Throwable localThrowable)
    {
      com.google.android.gms.ads.internal.util.client.zzb.zzc("Could not check if adapter is initialized.", localThrowable);
      throw new RemoteException();
    }
  }
  
  public void pause()
    throws RemoteException
  {
    try
    {
      this.zzbxh.onPause();
      return;
    }
    catch (Throwable localThrowable)
    {
      com.google.android.gms.ads.internal.util.client.zzb.zzc("Could not pause adapter.", localThrowable);
      throw new RemoteException();
    }
  }
  
  public void resume()
    throws RemoteException
  {
    try
    {
      this.zzbxh.onResume();
      return;
    }
    catch (Throwable localThrowable)
    {
      com.google.android.gms.ads.internal.util.client.zzb.zzc("Could not resume adapter.", localThrowable);
      throw new RemoteException();
    }
  }
  
  public void showInterstitial()
    throws RemoteException
  {
    if (!(this.zzbxh instanceof MediationInterstitialAdapter))
    {
      String str = String.valueOf(this.zzbxh.getClass().getCanonicalName());
      if (str.length() != 0) {}
      for (str = "MediationAdapter is not a MediationInterstitialAdapter: ".concat(str);; str = new String("MediationAdapter is not a MediationInterstitialAdapter: "))
      {
        com.google.android.gms.ads.internal.util.client.zzb.zzdi(str);
        throw new RemoteException();
      }
    }
    com.google.android.gms.ads.internal.util.client.zzb.zzdg("Showing interstitial from adapter.");
    try
    {
      ((MediationInterstitialAdapter)this.zzbxh).showInterstitial();
      return;
    }
    catch (Throwable localThrowable)
    {
      com.google.android.gms.ads.internal.util.client.zzb.zzc("Could not show interstitial from adapter.", localThrowable);
      throw new RemoteException();
    }
  }
  
  public void showVideo()
    throws RemoteException
  {
    if (!(this.zzbxh instanceof MediationRewardedVideoAdAdapter))
    {
      String str = String.valueOf(this.zzbxh.getClass().getCanonicalName());
      if (str.length() != 0) {}
      for (str = "MediationAdapter is not a MediationRewardedVideoAdAdapter: ".concat(str);; str = new String("MediationAdapter is not a MediationRewardedVideoAdAdapter: "))
      {
        com.google.android.gms.ads.internal.util.client.zzb.zzdi(str);
        throw new RemoteException();
      }
    }
    com.google.android.gms.ads.internal.util.client.zzb.zzdg("Show rewarded video ad from adapter.");
    try
    {
      ((MediationRewardedVideoAdAdapter)this.zzbxh).showVideo();
      return;
    }
    catch (Throwable localThrowable)
    {
      com.google.android.gms.ads.internal.util.client.zzb.zzc("Could not show rewarded video ad from adapter.", localThrowable);
      throw new RemoteException();
    }
  }
  
  public void zza(AdRequestParcel paramAdRequestParcel, String paramString1, String paramString2)
    throws RemoteException
  {
    if (!(this.zzbxh instanceof MediationRewardedVideoAdAdapter))
    {
      paramAdRequestParcel = String.valueOf(this.zzbxh.getClass().getCanonicalName());
      if (paramAdRequestParcel.length() != 0) {}
      for (paramAdRequestParcel = "MediationAdapter is not a MediationRewardedVideoAdAdapter: ".concat(paramAdRequestParcel);; paramAdRequestParcel = new String("MediationAdapter is not a MediationRewardedVideoAdAdapter: "))
      {
        com.google.android.gms.ads.internal.util.client.zzb.zzdi(paramAdRequestParcel);
        throw new RemoteException();
      }
    }
    com.google.android.gms.ads.internal.util.client.zzb.zzdg("Requesting rewarded video ad from adapter.");
    for (;;)
    {
      try
      {
        MediationRewardedVideoAdAdapter localMediationRewardedVideoAdAdapter = (MediationRewardedVideoAdAdapter)this.zzbxh;
        if (paramAdRequestParcel.zzayn == null) {
          break label227;
        }
        localObject1 = new HashSet(paramAdRequestParcel.zzayn);
        Object localObject2;
        if (paramAdRequestParcel.zzayl == -1L)
        {
          localObject2 = null;
          localObject2 = new zzhf((Date)localObject2, paramAdRequestParcel.zzaym, (Set)localObject1, paramAdRequestParcel.zzayt, paramAdRequestParcel.zzayo, paramAdRequestParcel.zzayp, paramAdRequestParcel.zzaza);
          if (paramAdRequestParcel.zzayv != null)
          {
            localObject1 = paramAdRequestParcel.zzayv.getBundle(localMediationRewardedVideoAdAdapter.getClass().getName());
            localMediationRewardedVideoAdAdapter.loadAd((MediationAdRequest)localObject2, zza(paramString1, paramAdRequestParcel.zzayp, paramString2), (Bundle)localObject1);
          }
        }
        else
        {
          localObject2 = new Date(paramAdRequestParcel.zzayl);
          continue;
        }
        localObject1 = null;
      }
      catch (Throwable paramAdRequestParcel)
      {
        com.google.android.gms.ads.internal.util.client.zzb.zzc("Could not load rewarded video ad from adapter.", paramAdRequestParcel);
        throw new RemoteException();
      }
      continue;
      label227:
      Object localObject1 = null;
    }
  }
  
  public void zza(zzd paramzzd, AdRequestParcel paramAdRequestParcel, String paramString1, com.google.android.gms.ads.internal.reward.mediation.client.zza paramzza, String paramString2)
    throws RemoteException
  {
    if (!(this.zzbxh instanceof MediationRewardedVideoAdAdapter))
    {
      paramzzd = String.valueOf(this.zzbxh.getClass().getCanonicalName());
      if (paramzzd.length() != 0) {}
      for (paramzzd = "MediationAdapter is not a MediationRewardedVideoAdAdapter: ".concat(paramzzd);; paramzzd = new String("MediationAdapter is not a MediationRewardedVideoAdAdapter: "))
      {
        com.google.android.gms.ads.internal.util.client.zzb.zzdi(paramzzd);
        throw new RemoteException();
      }
    }
    com.google.android.gms.ads.internal.util.client.zzb.zzdg("Initialize rewarded video adapter.");
    for (;;)
    {
      try
      {
        MediationRewardedVideoAdAdapter localMediationRewardedVideoAdAdapter = (MediationRewardedVideoAdAdapter)this.zzbxh;
        if (paramAdRequestParcel.zzayn == null) {
          break label246;
        }
        localObject1 = new HashSet(paramAdRequestParcel.zzayn);
        Object localObject2;
        if (paramAdRequestParcel.zzayl == -1L)
        {
          localObject2 = null;
          localObject2 = new zzhf((Date)localObject2, paramAdRequestParcel.zzaym, (Set)localObject1, paramAdRequestParcel.zzayt, paramAdRequestParcel.zzayo, paramAdRequestParcel.zzayp, paramAdRequestParcel.zzaza);
          if (paramAdRequestParcel.zzayv != null)
          {
            localObject1 = paramAdRequestParcel.zzayv.getBundle(localMediationRewardedVideoAdAdapter.getClass().getName());
            localMediationRewardedVideoAdAdapter.initialize((Context)zze.zzae(paramzzd), (MediationAdRequest)localObject2, paramString1, new com.google.android.gms.ads.internal.reward.mediation.client.zzb(paramzza), zza(paramString2, paramAdRequestParcel.zzayp, null), (Bundle)localObject1);
          }
        }
        else
        {
          localObject2 = new Date(paramAdRequestParcel.zzayl);
          continue;
        }
        localObject1 = null;
      }
      catch (Throwable paramzzd)
      {
        com.google.android.gms.ads.internal.util.client.zzb.zzc("Could not initialize rewarded video adapter.", paramzzd);
        throw new RemoteException();
      }
      continue;
      label246:
      Object localObject1 = null;
    }
  }
  
  public void zza(zzd paramzzd, AdRequestParcel paramAdRequestParcel, String paramString, zzhb paramzzhb)
    throws RemoteException
  {
    zza(paramzzd, paramAdRequestParcel, paramString, null, paramzzhb);
  }
  
  public void zza(zzd paramzzd, AdRequestParcel paramAdRequestParcel, String paramString1, String paramString2, zzhb paramzzhb)
    throws RemoteException
  {
    if (!(this.zzbxh instanceof MediationInterstitialAdapter))
    {
      paramzzd = String.valueOf(this.zzbxh.getClass().getCanonicalName());
      if (paramzzd.length() != 0) {}
      for (paramzzd = "MediationAdapter is not a MediationInterstitialAdapter: ".concat(paramzzd);; paramzzd = new String("MediationAdapter is not a MediationInterstitialAdapter: "))
      {
        com.google.android.gms.ads.internal.util.client.zzb.zzdi(paramzzd);
        throw new RemoteException();
      }
    }
    com.google.android.gms.ads.internal.util.client.zzb.zzdg("Requesting interstitial ad from adapter.");
    for (;;)
    {
      try
      {
        MediationInterstitialAdapter localMediationInterstitialAdapter = (MediationInterstitialAdapter)this.zzbxh;
        if (paramAdRequestParcel.zzayn == null) {
          break label246;
        }
        localObject1 = new HashSet(paramAdRequestParcel.zzayn);
        Object localObject2;
        if (paramAdRequestParcel.zzayl == -1L)
        {
          localObject2 = null;
          localObject2 = new zzhf((Date)localObject2, paramAdRequestParcel.zzaym, (Set)localObject1, paramAdRequestParcel.zzayt, paramAdRequestParcel.zzayo, paramAdRequestParcel.zzayp, paramAdRequestParcel.zzaza);
          if (paramAdRequestParcel.zzayv != null)
          {
            localObject1 = paramAdRequestParcel.zzayv.getBundle(localMediationInterstitialAdapter.getClass().getName());
            localMediationInterstitialAdapter.requestInterstitialAd((Context)zze.zzae(paramzzd), new zzhh(paramzzhb), zza(paramString1, paramAdRequestParcel.zzayp, paramString2), (MediationAdRequest)localObject2, (Bundle)localObject1);
          }
        }
        else
        {
          localObject2 = new Date(paramAdRequestParcel.zzayl);
          continue;
        }
        localObject1 = null;
      }
      catch (Throwable paramzzd)
      {
        com.google.android.gms.ads.internal.util.client.zzb.zzc("Could not request interstitial ad from adapter.", paramzzd);
        throw new RemoteException();
      }
      continue;
      label246:
      Object localObject1 = null;
    }
  }
  
  public void zza(zzd paramzzd, AdRequestParcel paramAdRequestParcel, String paramString1, String paramString2, zzhb paramzzhb, NativeAdOptionsParcel paramNativeAdOptionsParcel, List<String> paramList)
    throws RemoteException
  {
    if (!(this.zzbxh instanceof MediationNativeAdapter))
    {
      paramzzd = String.valueOf(this.zzbxh.getClass().getCanonicalName());
      if (paramzzd.length() != 0) {}
      for (paramzzd = "MediationAdapter is not a MediationNativeAdapter: ".concat(paramzzd);; paramzzd = new String("MediationAdapter is not a MediationNativeAdapter: "))
      {
        com.google.android.gms.ads.internal.util.client.zzb.zzdi(paramzzd);
        throw new RemoteException();
      }
    }
    for (;;)
    {
      try
      {
        MediationNativeAdapter localMediationNativeAdapter = (MediationNativeAdapter)this.zzbxh;
        if (paramAdRequestParcel.zzayn == null) {
          break label254;
        }
        localHashSet = new HashSet(paramAdRequestParcel.zzayn);
        Date localDate;
        if (paramAdRequestParcel.zzayl == -1L)
        {
          localDate = null;
          paramList = new zzhk(localDate, paramAdRequestParcel.zzaym, localHashSet, paramAdRequestParcel.zzayt, paramAdRequestParcel.zzayo, paramAdRequestParcel.zzayp, paramNativeAdOptionsParcel, paramList, paramAdRequestParcel.zzaza);
          if (paramAdRequestParcel.zzayv != null)
          {
            paramNativeAdOptionsParcel = paramAdRequestParcel.zzayv.getBundle(localMediationNativeAdapter.getClass().getName());
            this.zzbxi = new zzhh(paramzzhb);
            localMediationNativeAdapter.requestNativeAd((Context)zze.zzae(paramzzd), this.zzbxi, zza(paramString1, paramAdRequestParcel.zzayp, paramString2), paramList, paramNativeAdOptionsParcel);
          }
        }
        else
        {
          localDate = new Date(paramAdRequestParcel.zzayl);
          continue;
        }
        paramNativeAdOptionsParcel = null;
      }
      catch (Throwable paramzzd)
      {
        com.google.android.gms.ads.internal.util.client.zzb.zzc("Could not request native ad from adapter.", paramzzd);
        throw new RemoteException();
      }
      continue;
      label254:
      HashSet localHashSet = null;
    }
  }
  
  public void zza(zzd paramzzd, AdSizeParcel paramAdSizeParcel, AdRequestParcel paramAdRequestParcel, String paramString, zzhb paramzzhb)
    throws RemoteException
  {
    zza(paramzzd, paramAdSizeParcel, paramAdRequestParcel, paramString, null, paramzzhb);
  }
  
  public void zza(zzd paramzzd, AdSizeParcel paramAdSizeParcel, AdRequestParcel paramAdRequestParcel, String paramString1, String paramString2, zzhb paramzzhb)
    throws RemoteException
  {
    if (!(this.zzbxh instanceof MediationBannerAdapter))
    {
      paramzzd = String.valueOf(this.zzbxh.getClass().getCanonicalName());
      if (paramzzd.length() != 0) {}
      for (paramzzd = "MediationAdapter is not a MediationBannerAdapter: ".concat(paramzzd);; paramzzd = new String("MediationAdapter is not a MediationBannerAdapter: "))
      {
        com.google.android.gms.ads.internal.util.client.zzb.zzdi(paramzzd);
        throw new RemoteException();
      }
    }
    com.google.android.gms.ads.internal.util.client.zzb.zzdg("Requesting banner ad from adapter.");
    for (;;)
    {
      try
      {
        MediationBannerAdapter localMediationBannerAdapter = (MediationBannerAdapter)this.zzbxh;
        if (paramAdRequestParcel.zzayn == null) {
          break label262;
        }
        localObject1 = new HashSet(paramAdRequestParcel.zzayn);
        Object localObject2;
        if (paramAdRequestParcel.zzayl == -1L)
        {
          localObject2 = null;
          localObject2 = new zzhf((Date)localObject2, paramAdRequestParcel.zzaym, (Set)localObject1, paramAdRequestParcel.zzayt, paramAdRequestParcel.zzayo, paramAdRequestParcel.zzayp, paramAdRequestParcel.zzaza);
          if (paramAdRequestParcel.zzayv != null)
          {
            localObject1 = paramAdRequestParcel.zzayv.getBundle(localMediationBannerAdapter.getClass().getName());
            localMediationBannerAdapter.requestBannerAd((Context)zze.zzae(paramzzd), new zzhh(paramzzhb), zza(paramString1, paramAdRequestParcel.zzayp, paramString2), com.google.android.gms.ads.zza.zza(paramAdSizeParcel.width, paramAdSizeParcel.height, paramAdSizeParcel.zzazq), (MediationAdRequest)localObject2, (Bundle)localObject1);
          }
        }
        else
        {
          localObject2 = new Date(paramAdRequestParcel.zzayl);
          continue;
        }
        localObject1 = null;
      }
      catch (Throwable paramzzd)
      {
        com.google.android.gms.ads.internal.util.client.zzb.zzc("Could not request banner ad from adapter.", paramzzd);
        throw new RemoteException();
      }
      continue;
      label262:
      Object localObject1 = null;
    }
  }
  
  public void zzc(AdRequestParcel paramAdRequestParcel, String paramString)
    throws RemoteException
  {
    zza(paramAdRequestParcel, paramString, null);
  }
  
  public void zzj(zzd paramzzd)
    throws RemoteException
  {
    try
    {
      paramzzd = (Context)zze.zzae(paramzzd);
      ((OnContextChangedListener)this.zzbxh).onContextChanged(paramzzd);
      return;
    }
    catch (Throwable paramzzd)
    {
      com.google.android.gms.ads.internal.util.client.zzb.zza("Could not inform adapter of changed context", paramzzd);
    }
  }
  
  public zzhd zzom()
  {
    NativeAdMapper localNativeAdMapper = this.zzbxi.zzoq();
    if ((localNativeAdMapper instanceof NativeAppInstallAdMapper)) {
      return new zzhi((NativeAppInstallAdMapper)localNativeAdMapper);
    }
    return null;
  }
  
  public zzhe zzon()
  {
    NativeAdMapper localNativeAdMapper = this.zzbxi.zzoq();
    if ((localNativeAdMapper instanceof NativeContentAdMapper)) {
      return new zzhj((NativeContentAdMapper)localNativeAdMapper);
    }
    return null;
  }
  
  public Bundle zzoo()
  {
    if (!(this.zzbxh instanceof zzmq))
    {
      String str = String.valueOf(this.zzbxh.getClass().getCanonicalName());
      if (str.length() != 0) {}
      for (str = "MediationAdapter is not a v2 MediationBannerAdapter: ".concat(str);; str = new String("MediationAdapter is not a v2 MediationBannerAdapter: "))
      {
        com.google.android.gms.ads.internal.util.client.zzb.zzdi(str);
        return new Bundle();
      }
    }
    return ((zzmq)this.zzbxh).zzoo();
  }
  
  public Bundle zzop()
  {
    return new Bundle();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzhg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */