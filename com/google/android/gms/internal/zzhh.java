package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.ads.mediation.MediationBannerAdapter;
import com.google.android.gms.ads.mediation.MediationBannerListener;
import com.google.android.gms.ads.mediation.MediationInterstitialAdapter;
import com.google.android.gms.ads.mediation.MediationInterstitialListener;
import com.google.android.gms.ads.mediation.MediationNativeAdapter;
import com.google.android.gms.ads.mediation.MediationNativeListener;
import com.google.android.gms.ads.mediation.NativeAdMapper;
import com.google.android.gms.common.internal.zzaa;

@zzji
public final class zzhh
  implements MediationBannerListener, MediationInterstitialListener, MediationNativeListener
{
  private final zzhb zzbxj;
  private NativeAdMapper zzbxk;
  
  public zzhh(zzhb paramzzhb)
  {
    this.zzbxj = paramzzhb;
  }
  
  public void onAdClicked(MediationBannerAdapter paramMediationBannerAdapter)
  {
    zzaa.zzhs("onAdClicked must be called on the main UI thread.");
    zzb.zzdg("Adapter called onAdClicked.");
    try
    {
      this.zzbxj.onAdClicked();
      return;
    }
    catch (RemoteException paramMediationBannerAdapter)
    {
      zzb.zzc("Could not call onAdClicked.", paramMediationBannerAdapter);
    }
  }
  
  public void onAdClicked(MediationInterstitialAdapter paramMediationInterstitialAdapter)
  {
    zzaa.zzhs("onAdClicked must be called on the main UI thread.");
    zzb.zzdg("Adapter called onAdClicked.");
    try
    {
      this.zzbxj.onAdClicked();
      return;
    }
    catch (RemoteException paramMediationInterstitialAdapter)
    {
      zzb.zzc("Could not call onAdClicked.", paramMediationInterstitialAdapter);
    }
  }
  
  public void onAdClicked(MediationNativeAdapter paramMediationNativeAdapter)
  {
    zzaa.zzhs("onAdClicked must be called on the main UI thread.");
    paramMediationNativeAdapter = zzoq();
    if (paramMediationNativeAdapter == null)
    {
      zzb.zzdi("Could not call onAdClicked since NativeAdMapper is null.");
      return;
    }
    if (!paramMediationNativeAdapter.getOverrideClickHandling())
    {
      zzb.zzdg("Could not call onAdClicked since setOverrideClickHandling is not set to true");
      return;
    }
    zzb.zzdg("Adapter called onAdClicked.");
    try
    {
      this.zzbxj.onAdClicked();
      return;
    }
    catch (RemoteException paramMediationNativeAdapter)
    {
      zzb.zzc("Could not call onAdClicked.", paramMediationNativeAdapter);
    }
  }
  
  public void onAdClosed(MediationBannerAdapter paramMediationBannerAdapter)
  {
    zzaa.zzhs("onAdClosed must be called on the main UI thread.");
    zzb.zzdg("Adapter called onAdClosed.");
    try
    {
      this.zzbxj.onAdClosed();
      return;
    }
    catch (RemoteException paramMediationBannerAdapter)
    {
      zzb.zzc("Could not call onAdClosed.", paramMediationBannerAdapter);
    }
  }
  
  public void onAdClosed(MediationInterstitialAdapter paramMediationInterstitialAdapter)
  {
    zzaa.zzhs("onAdClosed must be called on the main UI thread.");
    zzb.zzdg("Adapter called onAdClosed.");
    try
    {
      this.zzbxj.onAdClosed();
      return;
    }
    catch (RemoteException paramMediationInterstitialAdapter)
    {
      zzb.zzc("Could not call onAdClosed.", paramMediationInterstitialAdapter);
    }
  }
  
  public void onAdClosed(MediationNativeAdapter paramMediationNativeAdapter)
  {
    zzaa.zzhs("onAdClosed must be called on the main UI thread.");
    zzb.zzdg("Adapter called onAdClosed.");
    try
    {
      this.zzbxj.onAdClosed();
      return;
    }
    catch (RemoteException paramMediationNativeAdapter)
    {
      zzb.zzc("Could not call onAdClosed.", paramMediationNativeAdapter);
    }
  }
  
  public void onAdFailedToLoad(MediationBannerAdapter paramMediationBannerAdapter, int paramInt)
  {
    zzaa.zzhs("onAdFailedToLoad must be called on the main UI thread.");
    zzb.zzdg(55 + "Adapter called onAdFailedToLoad with error. " + paramInt);
    try
    {
      this.zzbxj.onAdFailedToLoad(paramInt);
      return;
    }
    catch (RemoteException paramMediationBannerAdapter)
    {
      zzb.zzc("Could not call onAdFailedToLoad.", paramMediationBannerAdapter);
    }
  }
  
  public void onAdFailedToLoad(MediationInterstitialAdapter paramMediationInterstitialAdapter, int paramInt)
  {
    zzaa.zzhs("onAdFailedToLoad must be called on the main UI thread.");
    zzb.zzdg(55 + "Adapter called onAdFailedToLoad with error " + paramInt + ".");
    try
    {
      this.zzbxj.onAdFailedToLoad(paramInt);
      return;
    }
    catch (RemoteException paramMediationInterstitialAdapter)
    {
      zzb.zzc("Could not call onAdFailedToLoad.", paramMediationInterstitialAdapter);
    }
  }
  
  public void onAdFailedToLoad(MediationNativeAdapter paramMediationNativeAdapter, int paramInt)
  {
    zzaa.zzhs("onAdFailedToLoad must be called on the main UI thread.");
    zzb.zzdg(55 + "Adapter called onAdFailedToLoad with error " + paramInt + ".");
    try
    {
      this.zzbxj.onAdFailedToLoad(paramInt);
      return;
    }
    catch (RemoteException paramMediationNativeAdapter)
    {
      zzb.zzc("Could not call onAdFailedToLoad.", paramMediationNativeAdapter);
    }
  }
  
  public void onAdImpression(MediationNativeAdapter paramMediationNativeAdapter)
  {
    zzaa.zzhs("onAdImpression must be called on the main UI thread.");
    paramMediationNativeAdapter = zzoq();
    if (paramMediationNativeAdapter == null)
    {
      zzb.zzdi("Could not call onAdImpression since NativeAdMapper is null. ");
      return;
    }
    if (!paramMediationNativeAdapter.getOverrideImpressionRecording())
    {
      zzb.zzdg("Could not call onAdImpression since setOverrideImpressionRecording is not set to true");
      return;
    }
    zzb.zzdg("Adapter called onAdImpression.");
    try
    {
      this.zzbxj.onAdImpression();
      return;
    }
    catch (RemoteException paramMediationNativeAdapter)
    {
      zzb.zzc("Could not call onAdImpression.", paramMediationNativeAdapter);
    }
  }
  
  public void onAdLeftApplication(MediationBannerAdapter paramMediationBannerAdapter)
  {
    zzaa.zzhs("onAdLeftApplication must be called on the main UI thread.");
    zzb.zzdg("Adapter called onAdLeftApplication.");
    try
    {
      this.zzbxj.onAdLeftApplication();
      return;
    }
    catch (RemoteException paramMediationBannerAdapter)
    {
      zzb.zzc("Could not call onAdLeftApplication.", paramMediationBannerAdapter);
    }
  }
  
  public void onAdLeftApplication(MediationInterstitialAdapter paramMediationInterstitialAdapter)
  {
    zzaa.zzhs("onAdLeftApplication must be called on the main UI thread.");
    zzb.zzdg("Adapter called onAdLeftApplication.");
    try
    {
      this.zzbxj.onAdLeftApplication();
      return;
    }
    catch (RemoteException paramMediationInterstitialAdapter)
    {
      zzb.zzc("Could not call onAdLeftApplication.", paramMediationInterstitialAdapter);
    }
  }
  
  public void onAdLeftApplication(MediationNativeAdapter paramMediationNativeAdapter)
  {
    zzaa.zzhs("onAdLeftApplication must be called on the main UI thread.");
    zzb.zzdg("Adapter called onAdLeftApplication.");
    try
    {
      this.zzbxj.onAdLeftApplication();
      return;
    }
    catch (RemoteException paramMediationNativeAdapter)
    {
      zzb.zzc("Could not call onAdLeftApplication.", paramMediationNativeAdapter);
    }
  }
  
  public void onAdLoaded(MediationBannerAdapter paramMediationBannerAdapter)
  {
    zzaa.zzhs("onAdLoaded must be called on the main UI thread.");
    zzb.zzdg("Adapter called onAdLoaded.");
    try
    {
      this.zzbxj.onAdLoaded();
      return;
    }
    catch (RemoteException paramMediationBannerAdapter)
    {
      zzb.zzc("Could not call onAdLoaded.", paramMediationBannerAdapter);
    }
  }
  
  public void onAdLoaded(MediationInterstitialAdapter paramMediationInterstitialAdapter)
  {
    zzaa.zzhs("onAdLoaded must be called on the main UI thread.");
    zzb.zzdg("Adapter called onAdLoaded.");
    try
    {
      this.zzbxj.onAdLoaded();
      return;
    }
    catch (RemoteException paramMediationInterstitialAdapter)
    {
      zzb.zzc("Could not call onAdLoaded.", paramMediationInterstitialAdapter);
    }
  }
  
  public void onAdLoaded(MediationNativeAdapter paramMediationNativeAdapter, NativeAdMapper paramNativeAdMapper)
  {
    zzaa.zzhs("onAdLoaded must be called on the main UI thread.");
    zzb.zzdg("Adapter called onAdLoaded.");
    this.zzbxk = paramNativeAdMapper;
    try
    {
      this.zzbxj.onAdLoaded();
      return;
    }
    catch (RemoteException paramMediationNativeAdapter)
    {
      zzb.zzc("Could not call onAdLoaded.", paramMediationNativeAdapter);
    }
  }
  
  public void onAdOpened(MediationBannerAdapter paramMediationBannerAdapter)
  {
    zzaa.zzhs("onAdOpened must be called on the main UI thread.");
    zzb.zzdg("Adapter called onAdOpened.");
    try
    {
      this.zzbxj.onAdOpened();
      return;
    }
    catch (RemoteException paramMediationBannerAdapter)
    {
      zzb.zzc("Could not call onAdOpened.", paramMediationBannerAdapter);
    }
  }
  
  public void onAdOpened(MediationInterstitialAdapter paramMediationInterstitialAdapter)
  {
    zzaa.zzhs("onAdOpened must be called on the main UI thread.");
    zzb.zzdg("Adapter called onAdOpened.");
    try
    {
      this.zzbxj.onAdOpened();
      return;
    }
    catch (RemoteException paramMediationInterstitialAdapter)
    {
      zzb.zzc("Could not call onAdOpened.", paramMediationInterstitialAdapter);
    }
  }
  
  public void onAdOpened(MediationNativeAdapter paramMediationNativeAdapter)
  {
    zzaa.zzhs("onAdOpened must be called on the main UI thread.");
    zzb.zzdg("Adapter called onAdOpened.");
    try
    {
      this.zzbxj.onAdOpened();
      return;
    }
    catch (RemoteException paramMediationNativeAdapter)
    {
      zzb.zzc("Could not call onAdOpened.", paramMediationNativeAdapter);
    }
  }
  
  public NativeAdMapper zzoq()
  {
    return this.zzbxk;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzhh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */