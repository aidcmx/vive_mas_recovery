package com.google.android.gms.internal;

import android.os.Bundle;
import android.view.View;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.formats.NativeAd.Image;
import com.google.android.gms.ads.internal.client.zzab;
import com.google.android.gms.ads.internal.formats.zzc;
import com.google.android.gms.ads.mediation.NativeAppInstallAdMapper;
import com.google.android.gms.dynamic.zzd;
import com.google.android.gms.dynamic.zze;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@zzji
public class zzhi
  extends zzhd.zza
{
  private final NativeAppInstallAdMapper zzbxl;
  
  public zzhi(NativeAppInstallAdMapper paramNativeAppInstallAdMapper)
  {
    this.zzbxl = paramNativeAppInstallAdMapper;
  }
  
  public String getBody()
  {
    return this.zzbxl.getBody();
  }
  
  public String getCallToAction()
  {
    return this.zzbxl.getCallToAction();
  }
  
  public Bundle getExtras()
  {
    return this.zzbxl.getExtras();
  }
  
  public String getHeadline()
  {
    return this.zzbxl.getHeadline();
  }
  
  public List getImages()
  {
    Object localObject = this.zzbxl.getImages();
    if (localObject != null)
    {
      ArrayList localArrayList = new ArrayList();
      localObject = ((List)localObject).iterator();
      while (((Iterator)localObject).hasNext())
      {
        NativeAd.Image localImage = (NativeAd.Image)((Iterator)localObject).next();
        localArrayList.add(new zzc(localImage.getDrawable(), localImage.getUri(), localImage.getScale()));
      }
      return localArrayList;
    }
    return null;
  }
  
  public boolean getOverrideClickHandling()
  {
    return this.zzbxl.getOverrideClickHandling();
  }
  
  public boolean getOverrideImpressionRecording()
  {
    return this.zzbxl.getOverrideImpressionRecording();
  }
  
  public String getPrice()
  {
    return this.zzbxl.getPrice();
  }
  
  public double getStarRating()
  {
    return this.zzbxl.getStarRating();
  }
  
  public String getStore()
  {
    return this.zzbxl.getStore();
  }
  
  public void recordImpression()
  {
    this.zzbxl.recordImpression();
  }
  
  public zzab zzej()
  {
    if (this.zzbxl.getVideoController() != null) {
      return this.zzbxl.getVideoController().zzdw();
    }
    return null;
  }
  
  public void zzk(zzd paramzzd)
  {
    this.zzbxl.handleClick((View)zze.zzae(paramzzd));
  }
  
  public void zzl(zzd paramzzd)
  {
    this.zzbxl.trackView((View)zze.zzae(paramzzd));
  }
  
  public void zzm(zzd paramzzd)
  {
    this.zzbxl.untrackView((View)zze.zzae(paramzzd));
  }
  
  public zzeg zzmo()
  {
    NativeAd.Image localImage = this.zzbxl.getIcon();
    if (localImage != null) {
      return new zzc(localImage.getDrawable(), localImage.getUri(), localImage.getScale());
    }
    return null;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzhi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */