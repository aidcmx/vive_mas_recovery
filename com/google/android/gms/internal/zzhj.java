package com.google.android.gms.internal;

import android.os.Bundle;
import android.view.View;
import com.google.android.gms.ads.formats.NativeAd.Image;
import com.google.android.gms.ads.internal.formats.zzc;
import com.google.android.gms.ads.mediation.NativeContentAdMapper;
import com.google.android.gms.dynamic.zzd;
import com.google.android.gms.dynamic.zze;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@zzji
public class zzhj
  extends zzhe.zza
{
  private final NativeContentAdMapper zzbxm;
  
  public zzhj(NativeContentAdMapper paramNativeContentAdMapper)
  {
    this.zzbxm = paramNativeContentAdMapper;
  }
  
  public String getAdvertiser()
  {
    return this.zzbxm.getAdvertiser();
  }
  
  public String getBody()
  {
    return this.zzbxm.getBody();
  }
  
  public String getCallToAction()
  {
    return this.zzbxm.getCallToAction();
  }
  
  public Bundle getExtras()
  {
    return this.zzbxm.getExtras();
  }
  
  public String getHeadline()
  {
    return this.zzbxm.getHeadline();
  }
  
  public List getImages()
  {
    Object localObject = this.zzbxm.getImages();
    if (localObject != null)
    {
      ArrayList localArrayList = new ArrayList();
      localObject = ((List)localObject).iterator();
      while (((Iterator)localObject).hasNext())
      {
        NativeAd.Image localImage = (NativeAd.Image)((Iterator)localObject).next();
        localArrayList.add(new zzc(localImage.getDrawable(), localImage.getUri(), localImage.getScale()));
      }
      return localArrayList;
    }
    return null;
  }
  
  public boolean getOverrideClickHandling()
  {
    return this.zzbxm.getOverrideClickHandling();
  }
  
  public boolean getOverrideImpressionRecording()
  {
    return this.zzbxm.getOverrideImpressionRecording();
  }
  
  public void recordImpression()
  {
    this.zzbxm.recordImpression();
  }
  
  public void zzk(zzd paramzzd)
  {
    this.zzbxm.handleClick((View)zze.zzae(paramzzd));
  }
  
  public void zzl(zzd paramzzd)
  {
    this.zzbxm.trackView((View)zze.zzae(paramzzd));
  }
  
  public void zzm(zzd paramzzd)
  {
    this.zzbxm.untrackView((View)zze.zzae(paramzzd));
  }
  
  public zzeg zzmt()
  {
    NativeAd.Image localImage = this.zzbxm.getLogo();
    if (localImage != null) {
      return new zzc(localImage.getDrawable(), localImage.getUri(), localImage.getScale());
    }
    return null;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzhj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */