package com.google.android.gms.internal;

import android.location.Location;
import android.support.annotation.Nullable;
import com.google.android.gms.ads.VideoOptions.Builder;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.NativeAdOptions.Builder;
import com.google.android.gms.ads.internal.client.VideoOptionsParcel;
import com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel;
import com.google.android.gms.ads.mediation.NativeMediationAdRequest;
import java.util.Date;
import java.util.List;
import java.util.Set;

@zzji
public final class zzhk
  implements NativeMediationAdRequest
{
  private final NativeAdOptionsParcel zzanq;
  private final List<String> zzanr;
  private final int zzazc;
  private final boolean zzazo;
  private final int zzbxg;
  private final Date zzgr;
  private final Set<String> zzgt;
  private final boolean zzgu;
  private final Location zzgv;
  
  public zzhk(@Nullable Date paramDate, int paramInt1, @Nullable Set<String> paramSet, @Nullable Location paramLocation, boolean paramBoolean1, int paramInt2, NativeAdOptionsParcel paramNativeAdOptionsParcel, List<String> paramList, boolean paramBoolean2)
  {
    this.zzgr = paramDate;
    this.zzazc = paramInt1;
    this.zzgt = paramSet;
    this.zzgv = paramLocation;
    this.zzgu = paramBoolean1;
    this.zzbxg = paramInt2;
    this.zzanq = paramNativeAdOptionsParcel;
    this.zzanr = paramList;
    this.zzazo = paramBoolean2;
  }
  
  public Date getBirthday()
  {
    return this.zzgr;
  }
  
  public int getGender()
  {
    return this.zzazc;
  }
  
  public Set<String> getKeywords()
  {
    return this.zzgt;
  }
  
  public Location getLocation()
  {
    return this.zzgv;
  }
  
  public NativeAdOptions getNativeAdOptions()
  {
    if (this.zzanq == null) {
      return null;
    }
    NativeAdOptions.Builder localBuilder = new NativeAdOptions.Builder().setReturnUrlsForImageAssets(this.zzanq.zzboj).setImageOrientation(this.zzanq.zzbok).setRequestMultipleImages(this.zzanq.zzbol);
    if (this.zzanq.versionCode >= 2) {
      localBuilder.setAdChoicesPlacement(this.zzanq.zzbom);
    }
    if ((this.zzanq.versionCode >= 3) && (this.zzanq.zzbon != null)) {
      localBuilder.setVideoOptions(new VideoOptions.Builder().setStartMuted(this.zzanq.zzbon.zzbck).build());
    }
    return localBuilder.build();
  }
  
  public boolean isAppInstallAdRequested()
  {
    return (this.zzanr != null) && (this.zzanr.contains("2"));
  }
  
  public boolean isContentAdRequested()
  {
    return (this.zzanr != null) && (this.zzanr.contains("1"));
  }
  
  public boolean isDesignedForFamilies()
  {
    return this.zzazo;
  }
  
  public boolean isTesting()
  {
    return this.zzgu;
  }
  
  public int taggedForChildDirectedTreatment()
  {
    return this.zzbxg;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzhk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */