package com.google.android.gms.internal;

import android.app.Activity;
import android.os.Bundle;
import android.os.RemoteException;
import com.google.ads.mediation.MediationAdapter;
import com.google.ads.mediation.MediationBannerAdapter;
import com.google.ads.mediation.MediationInterstitialAdapter;
import com.google.ads.mediation.MediationServerParameters;
import com.google.ads.mediation.NetworkExtras;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel;
import com.google.android.gms.ads.internal.reward.mediation.client.zza;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.dynamic.zzd;
import com.google.android.gms.dynamic.zze;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;

@zzji
public final class zzhl<NETWORK_EXTRAS extends NetworkExtras, SERVER_PARAMETERS extends MediationServerParameters>
  extends zzha.zza
{
  private final MediationAdapter<NETWORK_EXTRAS, SERVER_PARAMETERS> zzbxn;
  private final NETWORK_EXTRAS zzbxo;
  
  public zzhl(MediationAdapter<NETWORK_EXTRAS, SERVER_PARAMETERS> paramMediationAdapter, NETWORK_EXTRAS paramNETWORK_EXTRAS)
  {
    this.zzbxn = paramMediationAdapter;
    this.zzbxo = paramNETWORK_EXTRAS;
  }
  
  private SERVER_PARAMETERS zzb(String paramString1, int paramInt, String paramString2)
    throws RemoteException
  {
    if (paramString1 != null) {
      try
      {
        localObject = new JSONObject(paramString1);
        paramString2 = new HashMap(((JSONObject)localObject).length());
        Iterator localIterator = ((JSONObject)localObject).keys();
        for (;;)
        {
          paramString1 = paramString2;
          if (!localIterator.hasNext()) {
            break;
          }
          paramString1 = (String)localIterator.next();
          paramString2.put(paramString1, ((JSONObject)localObject).getString(paramString1));
        }
        paramString1 = new HashMap(0);
      }
      catch (Throwable paramString1)
      {
        zzb.zzc("Could not get MediationServerParameters.", paramString1);
        throw new RemoteException();
      }
    }
    Object localObject = this.zzbxn.getServerParametersType();
    paramString2 = null;
    if (localObject != null)
    {
      paramString2 = (MediationServerParameters)((Class)localObject).newInstance();
      paramString2.load(paramString1);
    }
    return paramString2;
  }
  
  public void destroy()
    throws RemoteException
  {
    try
    {
      this.zzbxn.destroy();
      return;
    }
    catch (Throwable localThrowable)
    {
      zzb.zzc("Could not destroy adapter.", localThrowable);
      throw new RemoteException();
    }
  }
  
  public Bundle getInterstitialAdapterInfo()
  {
    return new Bundle();
  }
  
  public zzd getView()
    throws RemoteException
  {
    Object localObject;
    if (!(this.zzbxn instanceof MediationBannerAdapter))
    {
      localObject = String.valueOf(this.zzbxn.getClass().getCanonicalName());
      if (((String)localObject).length() != 0) {}
      for (localObject = "MediationAdapter is not a MediationBannerAdapter: ".concat((String)localObject);; localObject = new String("MediationAdapter is not a MediationBannerAdapter: "))
      {
        zzb.zzdi((String)localObject);
        throw new RemoteException();
      }
    }
    try
    {
      localObject = zze.zzac(((MediationBannerAdapter)this.zzbxn).getBannerView());
      return (zzd)localObject;
    }
    catch (Throwable localThrowable)
    {
      zzb.zzc("Could not get banner view from adapter.", localThrowable);
      throw new RemoteException();
    }
  }
  
  public boolean isInitialized()
  {
    return true;
  }
  
  public void pause()
    throws RemoteException
  {
    throw new RemoteException();
  }
  
  public void resume()
    throws RemoteException
  {
    throw new RemoteException();
  }
  
  public void showInterstitial()
    throws RemoteException
  {
    if (!(this.zzbxn instanceof MediationInterstitialAdapter))
    {
      String str = String.valueOf(this.zzbxn.getClass().getCanonicalName());
      if (str.length() != 0) {}
      for (str = "MediationAdapter is not a MediationInterstitialAdapter: ".concat(str);; str = new String("MediationAdapter is not a MediationInterstitialAdapter: "))
      {
        zzb.zzdi(str);
        throw new RemoteException();
      }
    }
    zzb.zzdg("Showing interstitial from adapter.");
    try
    {
      ((MediationInterstitialAdapter)this.zzbxn).showInterstitial();
      return;
    }
    catch (Throwable localThrowable)
    {
      zzb.zzc("Could not show interstitial from adapter.", localThrowable);
      throw new RemoteException();
    }
  }
  
  public void showVideo() {}
  
  public void zza(AdRequestParcel paramAdRequestParcel, String paramString1, String paramString2) {}
  
  public void zza(zzd paramzzd, AdRequestParcel paramAdRequestParcel, String paramString1, zza paramzza, String paramString2)
    throws RemoteException
  {}
  
  public void zza(zzd paramzzd, AdRequestParcel paramAdRequestParcel, String paramString, zzhb paramzzhb)
    throws RemoteException
  {
    zza(paramzzd, paramAdRequestParcel, paramString, null, paramzzhb);
  }
  
  public void zza(zzd paramzzd, AdRequestParcel paramAdRequestParcel, String paramString1, String paramString2, zzhb paramzzhb)
    throws RemoteException
  {
    if (!(this.zzbxn instanceof MediationInterstitialAdapter))
    {
      paramzzd = String.valueOf(this.zzbxn.getClass().getCanonicalName());
      if (paramzzd.length() != 0) {}
      for (paramzzd = "MediationAdapter is not a MediationInterstitialAdapter: ".concat(paramzzd);; paramzzd = new String("MediationAdapter is not a MediationInterstitialAdapter: "))
      {
        zzb.zzdi(paramzzd);
        throw new RemoteException();
      }
    }
    zzb.zzdg("Requesting interstitial ad from adapter.");
    try
    {
      ((MediationInterstitialAdapter)this.zzbxn).requestInterstitialAd(new zzhm(paramzzhb), (Activity)zze.zzae(paramzzd), zzb(paramString1, paramAdRequestParcel.zzayp, paramString2), zzhn.zzs(paramAdRequestParcel), this.zzbxo);
      return;
    }
    catch (Throwable paramzzd)
    {
      zzb.zzc("Could not request interstitial ad from adapter.", paramzzd);
      throw new RemoteException();
    }
  }
  
  public void zza(zzd paramzzd, AdRequestParcel paramAdRequestParcel, String paramString1, String paramString2, zzhb paramzzhb, NativeAdOptionsParcel paramNativeAdOptionsParcel, List<String> paramList) {}
  
  public void zza(zzd paramzzd, AdSizeParcel paramAdSizeParcel, AdRequestParcel paramAdRequestParcel, String paramString, zzhb paramzzhb)
    throws RemoteException
  {
    zza(paramzzd, paramAdSizeParcel, paramAdRequestParcel, paramString, null, paramzzhb);
  }
  
  public void zza(zzd paramzzd, AdSizeParcel paramAdSizeParcel, AdRequestParcel paramAdRequestParcel, String paramString1, String paramString2, zzhb paramzzhb)
    throws RemoteException
  {
    if (!(this.zzbxn instanceof MediationBannerAdapter))
    {
      paramzzd = String.valueOf(this.zzbxn.getClass().getCanonicalName());
      if (paramzzd.length() != 0) {}
      for (paramzzd = "MediationAdapter is not a MediationBannerAdapter: ".concat(paramzzd);; paramzzd = new String("MediationAdapter is not a MediationBannerAdapter: "))
      {
        zzb.zzdi(paramzzd);
        throw new RemoteException();
      }
    }
    zzb.zzdg("Requesting banner ad from adapter.");
    try
    {
      ((MediationBannerAdapter)this.zzbxn).requestBannerAd(new zzhm(paramzzhb), (Activity)zze.zzae(paramzzd), zzb(paramString1, paramAdRequestParcel.zzayp, paramString2), zzhn.zzc(paramAdSizeParcel), zzhn.zzs(paramAdRequestParcel), this.zzbxo);
      return;
    }
    catch (Throwable paramzzd)
    {
      zzb.zzc("Could not request banner ad from adapter.", paramzzd);
      throw new RemoteException();
    }
  }
  
  public void zzc(AdRequestParcel paramAdRequestParcel, String paramString) {}
  
  public void zzj(zzd paramzzd)
    throws RemoteException
  {}
  
  public zzhd zzom()
  {
    return null;
  }
  
  public zzhe zzon()
  {
    return null;
  }
  
  public Bundle zzoo()
  {
    return new Bundle();
  }
  
  public Bundle zzop()
  {
    return new Bundle();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzhl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */