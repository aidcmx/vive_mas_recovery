package com.google.android.gms.internal;

import android.os.Handler;
import android.os.RemoteException;
import com.google.ads.AdRequest.ErrorCode;
import com.google.ads.mediation.MediationBannerAdapter;
import com.google.ads.mediation.MediationBannerListener;
import com.google.ads.mediation.MediationInterstitialAdapter;
import com.google.ads.mediation.MediationInterstitialListener;
import com.google.ads.mediation.MediationServerParameters;
import com.google.ads.mediation.NetworkExtras;
import com.google.android.gms.ads.internal.client.zzm;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.ads.internal.util.client.zzb;

@zzji
public final class zzhm<NETWORK_EXTRAS extends NetworkExtras, SERVER_PARAMETERS extends MediationServerParameters>
  implements MediationBannerListener, MediationInterstitialListener
{
  private final zzhb zzbxj;
  
  public zzhm(zzhb paramzzhb)
  {
    this.zzbxj = paramzzhb;
  }
  
  public void onClick(MediationBannerAdapter<?, ?> paramMediationBannerAdapter)
  {
    zzb.zzdg("Adapter called onClick.");
    if (!zzm.zzkr().zzwq())
    {
      zzb.zzdi("onClick must be called on the main UI thread.");
      zza.zzcxr.post(new zzhm.1(this));
      return;
    }
    try
    {
      this.zzbxj.onAdClicked();
      return;
    }
    catch (RemoteException paramMediationBannerAdapter)
    {
      zzb.zzc("Could not call onAdClicked.", paramMediationBannerAdapter);
    }
  }
  
  public void onDismissScreen(MediationBannerAdapter<?, ?> paramMediationBannerAdapter)
  {
    zzb.zzdg("Adapter called onDismissScreen.");
    if (!zzm.zzkr().zzwq())
    {
      zzb.zzdi("onDismissScreen must be called on the main UI thread.");
      zza.zzcxr.post(new zzhm.4(this));
      return;
    }
    try
    {
      this.zzbxj.onAdClosed();
      return;
    }
    catch (RemoteException paramMediationBannerAdapter)
    {
      zzb.zzc("Could not call onAdClosed.", paramMediationBannerAdapter);
    }
  }
  
  public void onDismissScreen(MediationInterstitialAdapter<?, ?> paramMediationInterstitialAdapter)
  {
    zzb.zzdg("Adapter called onDismissScreen.");
    if (!zzm.zzkr().zzwq())
    {
      zzb.zzdi("onDismissScreen must be called on the main UI thread.");
      zza.zzcxr.post(new zzhm.9(this));
      return;
    }
    try
    {
      this.zzbxj.onAdClosed();
      return;
    }
    catch (RemoteException paramMediationInterstitialAdapter)
    {
      zzb.zzc("Could not call onAdClosed.", paramMediationInterstitialAdapter);
    }
  }
  
  public void onFailedToReceiveAd(MediationBannerAdapter<?, ?> paramMediationBannerAdapter, AdRequest.ErrorCode paramErrorCode)
  {
    paramMediationBannerAdapter = String.valueOf(paramErrorCode);
    zzb.zzdg(String.valueOf(paramMediationBannerAdapter).length() + 47 + "Adapter called onFailedToReceiveAd with error. " + paramMediationBannerAdapter);
    if (!zzm.zzkr().zzwq())
    {
      zzb.zzdi("onFailedToReceiveAd must be called on the main UI thread.");
      zza.zzcxr.post(new zzhm.5(this, paramErrorCode));
      return;
    }
    try
    {
      this.zzbxj.onAdFailedToLoad(zzhn.zza(paramErrorCode));
      return;
    }
    catch (RemoteException paramMediationBannerAdapter)
    {
      zzb.zzc("Could not call onAdFailedToLoad.", paramMediationBannerAdapter);
    }
  }
  
  public void onFailedToReceiveAd(MediationInterstitialAdapter<?, ?> paramMediationInterstitialAdapter, AdRequest.ErrorCode paramErrorCode)
  {
    paramMediationInterstitialAdapter = String.valueOf(paramErrorCode);
    zzb.zzdg(String.valueOf(paramMediationInterstitialAdapter).length() + 47 + "Adapter called onFailedToReceiveAd with error " + paramMediationInterstitialAdapter + ".");
    if (!zzm.zzkr().zzwq())
    {
      zzb.zzdi("onFailedToReceiveAd must be called on the main UI thread.");
      zza.zzcxr.post(new zzhm.10(this, paramErrorCode));
      return;
    }
    try
    {
      this.zzbxj.onAdFailedToLoad(zzhn.zza(paramErrorCode));
      return;
    }
    catch (RemoteException paramMediationInterstitialAdapter)
    {
      zzb.zzc("Could not call onAdFailedToLoad.", paramMediationInterstitialAdapter);
    }
  }
  
  public void onLeaveApplication(MediationBannerAdapter<?, ?> paramMediationBannerAdapter)
  {
    zzb.zzdg("Adapter called onLeaveApplication.");
    if (!zzm.zzkr().zzwq())
    {
      zzb.zzdi("onLeaveApplication must be called on the main UI thread.");
      zza.zzcxr.post(new zzhm.6(this));
      return;
    }
    try
    {
      this.zzbxj.onAdLeftApplication();
      return;
    }
    catch (RemoteException paramMediationBannerAdapter)
    {
      zzb.zzc("Could not call onAdLeftApplication.", paramMediationBannerAdapter);
    }
  }
  
  public void onLeaveApplication(MediationInterstitialAdapter<?, ?> paramMediationInterstitialAdapter)
  {
    zzb.zzdg("Adapter called onLeaveApplication.");
    if (!zzm.zzkr().zzwq())
    {
      zzb.zzdi("onLeaveApplication must be called on the main UI thread.");
      zza.zzcxr.post(new zzhm.11(this));
      return;
    }
    try
    {
      this.zzbxj.onAdLeftApplication();
      return;
    }
    catch (RemoteException paramMediationInterstitialAdapter)
    {
      zzb.zzc("Could not call onAdLeftApplication.", paramMediationInterstitialAdapter);
    }
  }
  
  public void onPresentScreen(MediationBannerAdapter<?, ?> paramMediationBannerAdapter)
  {
    zzb.zzdg("Adapter called onPresentScreen.");
    if (!zzm.zzkr().zzwq())
    {
      zzb.zzdi("onPresentScreen must be called on the main UI thread.");
      zza.zzcxr.post(new zzhm.7(this));
      return;
    }
    try
    {
      this.zzbxj.onAdOpened();
      return;
    }
    catch (RemoteException paramMediationBannerAdapter)
    {
      zzb.zzc("Could not call onAdOpened.", paramMediationBannerAdapter);
    }
  }
  
  public void onPresentScreen(MediationInterstitialAdapter<?, ?> paramMediationInterstitialAdapter)
  {
    zzb.zzdg("Adapter called onPresentScreen.");
    if (!zzm.zzkr().zzwq())
    {
      zzb.zzdi("onPresentScreen must be called on the main UI thread.");
      zza.zzcxr.post(new zzhm.2(this));
      return;
    }
    try
    {
      this.zzbxj.onAdOpened();
      return;
    }
    catch (RemoteException paramMediationInterstitialAdapter)
    {
      zzb.zzc("Could not call onAdOpened.", paramMediationInterstitialAdapter);
    }
  }
  
  public void onReceivedAd(MediationBannerAdapter<?, ?> paramMediationBannerAdapter)
  {
    zzb.zzdg("Adapter called onReceivedAd.");
    if (!zzm.zzkr().zzwq())
    {
      zzb.zzdi("onReceivedAd must be called on the main UI thread.");
      zza.zzcxr.post(new zzhm.8(this));
      return;
    }
    try
    {
      this.zzbxj.onAdLoaded();
      return;
    }
    catch (RemoteException paramMediationBannerAdapter)
    {
      zzb.zzc("Could not call onAdLoaded.", paramMediationBannerAdapter);
    }
  }
  
  public void onReceivedAd(MediationInterstitialAdapter<?, ?> paramMediationInterstitialAdapter)
  {
    zzb.zzdg("Adapter called onReceivedAd.");
    if (!zzm.zzkr().zzwq())
    {
      zzb.zzdi("onReceivedAd must be called on the main UI thread.");
      zza.zzcxr.post(new zzhm.3(this));
      return;
    }
    try
    {
      this.zzbxj.onAdLoaded();
      return;
    }
    catch (RemoteException paramMediationInterstitialAdapter)
    {
      zzb.zzc("Could not call onAdLoaded.", paramMediationInterstitialAdapter);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzhm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */