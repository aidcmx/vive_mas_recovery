package com.google.android.gms.internal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.customtabs.CustomTabsIntent;
import android.support.customtabs.CustomTabsIntent.Builder;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.overlay.AdLauncherIntentInfoParcel;
import com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.ads.mediation.MediationAdRequest;
import com.google.android.gms.ads.mediation.MediationInterstitialAdapter;
import com.google.android.gms.ads.mediation.MediationInterstitialListener;

@zzji
public class zzho
  implements MediationInterstitialAdapter
{
  private Uri mUri;
  private Activity zzbxt;
  private zzef zzbxu;
  private MediationInterstitialListener zzbxv;
  
  public static boolean zzo(Context paramContext)
  {
    return zzef.zzn(paramContext);
  }
  
  public void onDestroy()
  {
    zzb.zzdg("Destroying AdMobCustomTabsAdapter adapter.");
    try
    {
      this.zzbxu.zzd(this.zzbxt);
      return;
    }
    catch (Exception localException)
    {
      zzb.zzb("Exception while unbinding from CustomTabsService.", localException);
    }
  }
  
  public void onPause()
  {
    zzb.zzdg("Pausing AdMobCustomTabsAdapter adapter.");
  }
  
  public void onResume()
  {
    zzb.zzdg("Resuming AdMobCustomTabsAdapter adapter.");
  }
  
  public void requestInterstitialAd(Context paramContext, MediationInterstitialListener paramMediationInterstitialListener, Bundle paramBundle1, MediationAdRequest paramMediationAdRequest, Bundle paramBundle2)
  {
    this.zzbxv = paramMediationInterstitialListener;
    if (this.zzbxv == null)
    {
      zzb.zzdi("Listener not set for mediation. Returning.");
      return;
    }
    if (!(paramContext instanceof Activity))
    {
      zzb.zzdi("AdMobCustomTabs can only work with Activity context. Bailing out.");
      this.zzbxv.onAdFailedToLoad(this, 0);
      return;
    }
    if (!zzo(paramContext))
    {
      zzb.zzdi("Default browser does not support custom tabs. Bailing out.");
      this.zzbxv.onAdFailedToLoad(this, 0);
      return;
    }
    paramMediationInterstitialListener = paramBundle1.getString("tab_url");
    if (TextUtils.isEmpty(paramMediationInterstitialListener))
    {
      zzb.zzdi("The tab_url retrieved from mediation metadata is empty. Bailing out.");
      this.zzbxv.onAdFailedToLoad(this, 0);
      return;
    }
    this.zzbxt = ((Activity)paramContext);
    this.mUri = Uri.parse(paramMediationInterstitialListener);
    this.zzbxu = new zzef();
    paramContext = new zzho.1(this);
    this.zzbxu.zza(paramContext);
    this.zzbxu.zze(this.zzbxt);
    this.zzbxv.onAdLoaded(this);
  }
  
  public void showInterstitial()
  {
    Object localObject = new CustomTabsIntent.Builder(this.zzbxu.zzmf()).build();
    ((CustomTabsIntent)localObject).intent.setData(this.mUri);
    localObject = new AdOverlayInfoParcel(new AdLauncherIntentInfoParcel(((CustomTabsIntent)localObject).intent), null, new zzho.2(this), null, new VersionInfoParcel(0, 0, false));
    zzlb.zzcvl.post(new zzho.3(this, (AdOverlayInfoParcel)localObject));
    zzu.zzgq().zzah(false);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzho.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */