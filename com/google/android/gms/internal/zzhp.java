package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.provider.CalendarContract.Events;
import android.text.TextUtils;
import com.google.android.gms.R.string;
import com.google.android.gms.ads.internal.zzu;
import java.util.Map;

@zzji
public class zzhp
  extends zzhv
{
  private final Context mContext;
  private final Map<String, String> zzbly;
  private String zzbxx;
  private long zzbxy;
  private long zzbxz;
  private String zzbya;
  private String zzbyb;
  
  public zzhp(zzmd paramzzmd, Map<String, String> paramMap)
  {
    super(paramzzmd, "createCalendarEvent");
    this.zzbly = paramMap;
    this.mContext = paramzzmd.zzwy();
    zzor();
  }
  
  private String zzby(String paramString)
  {
    if (TextUtils.isEmpty((CharSequence)this.zzbly.get(paramString))) {
      return "";
    }
    return (String)this.zzbly.get(paramString);
  }
  
  private long zzbz(String paramString)
  {
    paramString = (String)this.zzbly.get(paramString);
    if (paramString == null) {
      return -1L;
    }
    try
    {
      long l = Long.parseLong(paramString);
      return l;
    }
    catch (NumberFormatException paramString) {}
    return -1L;
  }
  
  private void zzor()
  {
    this.zzbxx = zzby("description");
    this.zzbya = zzby("summary");
    this.zzbxy = zzbz("start_ticks");
    this.zzbxz = zzbz("end_ticks");
    this.zzbyb = zzby("location");
  }
  
  @TargetApi(14)
  Intent createIntent()
  {
    Intent localIntent = new Intent("android.intent.action.EDIT").setData(CalendarContract.Events.CONTENT_URI);
    localIntent.putExtra("title", this.zzbxx);
    localIntent.putExtra("eventLocation", this.zzbyb);
    localIntent.putExtra("description", this.zzbya);
    if (this.zzbxy > -1L) {
      localIntent.putExtra("beginTime", this.zzbxy);
    }
    if (this.zzbxz > -1L) {
      localIntent.putExtra("endTime", this.zzbxz);
    }
    localIntent.setFlags(268435456);
    return localIntent;
  }
  
  public void execute()
  {
    if (this.mContext == null)
    {
      zzcb("Activity context is not available.");
      return;
    }
    if (!zzu.zzgm().zzac(this.mContext).zzln())
    {
      zzcb("This feature is not available on the device.");
      return;
    }
    AlertDialog.Builder localBuilder = zzu.zzgm().zzab(this.mContext);
    Resources localResources = zzu.zzgq().getResources();
    if (localResources != null)
    {
      str = localResources.getString(R.string.create_calendar_title);
      localBuilder.setTitle(str);
      if (localResources == null) {
        break label157;
      }
      str = localResources.getString(R.string.create_calendar_message);
      label85:
      localBuilder.setMessage(str);
      if (localResources == null) {
        break label163;
      }
      str = localResources.getString(R.string.accept);
      label103:
      localBuilder.setPositiveButton(str, new zzhp.1(this));
      if (localResources == null) {
        break label169;
      }
    }
    label157:
    label163:
    label169:
    for (String str = localResources.getString(R.string.decline);; str = "Decline")
    {
      localBuilder.setNegativeButton(str, new zzhp.2(this));
      localBuilder.create().show();
      return;
      str = "Create calendar event";
      break;
      str = "Allow Ad to create a calendar event?";
      break label85;
      str = "Accept";
      break label103;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzhp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */