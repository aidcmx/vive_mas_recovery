package com.google.android.gms.internal;

import android.app.Activity;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.client.zzm;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.common.util.zzf;
import java.util.Map;
import java.util.Set;

@zzji
public class zzhq
  extends zzhv
{
  static final Set<String> zzbyd = zzf.zzc(new String[] { "top-left", "top-right", "top-center", "center", "bottom-left", "bottom-right", "bottom-center" });
  private int zzakh = -1;
  private int zzaki = -1;
  private final Object zzako = new Object();
  private AdSizeParcel zzapp;
  private final zzmd zzbnz;
  private final Activity zzbxt;
  private String zzbye = "top-right";
  private boolean zzbyf = true;
  private int zzbyg = 0;
  private int zzbyh = 0;
  private int zzbyi = 0;
  private int zzbyj = 0;
  private ImageView zzbyk;
  private LinearLayout zzbyl;
  private zzhw zzbym;
  private PopupWindow zzbyn;
  private RelativeLayout zzbyo;
  private ViewGroup zzbyp;
  
  public zzhq(zzmd paramzzmd, zzhw paramzzhw)
  {
    super(paramzzmd, "resize");
    this.zzbnz = paramzzmd;
    this.zzbxt = paramzzmd.zzwy();
    this.zzbym = paramzzhw;
  }
  
  private void zzj(Map<String, String> paramMap)
  {
    if (!TextUtils.isEmpty((CharSequence)paramMap.get("width"))) {
      this.zzakh = zzu.zzgm().zzda((String)paramMap.get("width"));
    }
    if (!TextUtils.isEmpty((CharSequence)paramMap.get("height"))) {
      this.zzaki = zzu.zzgm().zzda((String)paramMap.get("height"));
    }
    if (!TextUtils.isEmpty((CharSequence)paramMap.get("offsetX"))) {
      this.zzbyi = zzu.zzgm().zzda((String)paramMap.get("offsetX"));
    }
    if (!TextUtils.isEmpty((CharSequence)paramMap.get("offsetY"))) {
      this.zzbyj = zzu.zzgm().zzda((String)paramMap.get("offsetY"));
    }
    if (!TextUtils.isEmpty((CharSequence)paramMap.get("allowOffscreen"))) {
      this.zzbyf = Boolean.parseBoolean((String)paramMap.get("allowOffscreen"));
    }
    paramMap = (String)paramMap.get("customClosePosition");
    if (!TextUtils.isEmpty(paramMap)) {
      this.zzbye = paramMap;
    }
  }
  
  private int[] zzot()
  {
    if (!zzov()) {
      return null;
    }
    if (this.zzbyf) {
      return new int[] { this.zzbyg + this.zzbyi, this.zzbyh + this.zzbyj };
    }
    int[] arrayOfInt1 = zzu.zzgm().zzi(this.zzbxt);
    int[] arrayOfInt2 = zzu.zzgm().zzk(this.zzbxt);
    int m = arrayOfInt1[0];
    int j = this.zzbyg + this.zzbyi;
    int k = this.zzbyh + this.zzbyj;
    int i;
    if (j < 0)
    {
      i = 0;
      if (k >= arrayOfInt2[0]) {
        break label149;
      }
      j = arrayOfInt2[0];
    }
    for (;;)
    {
      return new int[] { i, j };
      i = j;
      if (this.zzakh + j <= m) {
        break;
      }
      i = m - this.zzakh;
      break;
      label149:
      j = k;
      if (this.zzaki + k > arrayOfInt2[1]) {
        j = arrayOfInt2[1] - this.zzaki;
      }
    }
  }
  
  public void execute(Map<String, String> paramMap)
  {
    synchronized (this.zzako)
    {
      if (this.zzbxt == null)
      {
        zzcb("Not an activity context. Cannot resize.");
        return;
      }
      if (this.zzbnz.zzeg() == null)
      {
        zzcb("Webview is not yet available, size is not set.");
        return;
      }
    }
    if (this.zzbnz.zzeg().zzazr)
    {
      zzcb("Is interstitial. Cannot resize an interstitial.");
      return;
    }
    if (this.zzbnz.zzxg())
    {
      zzcb("Cannot resize an expanded banner.");
      return;
    }
    zzj(paramMap);
    if (!zzos())
    {
      zzcb("Invalid width and height options. Cannot resize.");
      return;
    }
    paramMap = this.zzbxt.getWindow();
    if ((paramMap == null) || (paramMap.getDecorView() == null))
    {
      zzcb("Activity context is not ready, cannot get window or decor view.");
      return;
    }
    int[] arrayOfInt = zzot();
    if (arrayOfInt == null)
    {
      zzcb("Resize location out of screen or close button is not visible.");
      return;
    }
    int i = zzm.zzkr().zzb(this.zzbxt, this.zzakh);
    int j = zzm.zzkr().zzb(this.zzbxt, this.zzaki);
    Object localObject2 = this.zzbnz.getView().getParent();
    if ((localObject2 != null) && ((localObject2 instanceof ViewGroup)))
    {
      ((ViewGroup)localObject2).removeView(this.zzbnz.getView());
      if (this.zzbyn == null)
      {
        this.zzbyp = ((ViewGroup)localObject2);
        localObject2 = zzu.zzgm().zzq(this.zzbnz.getView());
        this.zzbyk = new ImageView(this.zzbxt);
        this.zzbyk.setImageBitmap((Bitmap)localObject2);
        this.zzapp = this.zzbnz.zzeg();
        this.zzbyp.addView(this.zzbyk);
        this.zzbyo = new RelativeLayout(this.zzbxt);
        this.zzbyo.setBackgroundColor(0);
        this.zzbyo.setLayoutParams(new ViewGroup.LayoutParams(i, j));
        this.zzbyn = zzu.zzgm().zza(this.zzbyo, i, j, false);
        this.zzbyn.setOutsideTouchable(true);
        this.zzbyn.setTouchable(true);
        localObject2 = this.zzbyn;
        if (this.zzbyf) {
          break label1083;
        }
      }
    }
    label1024:
    label1038:
    label1040:
    label1083:
    for (boolean bool = true;; bool = false)
    {
      ((PopupWindow)localObject2).setClippingEnabled(bool);
      this.zzbyo.addView(this.zzbnz.getView(), -1, -1);
      this.zzbyl = new LinearLayout(this.zzbxt);
      localObject2 = new RelativeLayout.LayoutParams(zzm.zzkr().zzb(this.zzbxt, 50), zzm.zzkr().zzb(this.zzbxt, 50));
      String str = this.zzbye;
      switch (str.hashCode())
      {
      }
      for (;;)
      {
        ((RelativeLayout.LayoutParams)localObject2).addRule(10);
        ((RelativeLayout.LayoutParams)localObject2).addRule(11);
        for (;;)
        {
          this.zzbyl.setOnClickListener(new zzhq.1(this));
          this.zzbyl.setContentDescription("Close button");
          this.zzbyo.addView(this.zzbyl, (ViewGroup.LayoutParams)localObject2);
          try
          {
            this.zzbyn.showAtLocation(paramMap.getDecorView(), 0, zzm.zzkr().zzb(this.zzbxt, arrayOfInt[0]), zzm.zzkr().zzb(this.zzbxt, arrayOfInt[1]));
            zzb(arrayOfInt[0], arrayOfInt[1]);
            this.zzbnz.zza(new AdSizeParcel(this.zzbxt, new AdSize(this.zzakh, this.zzaki)));
            zzc(arrayOfInt[0], arrayOfInt[1]);
            zzcd("resized");
            return;
          }
          catch (RuntimeException paramMap)
          {
            paramMap = String.valueOf(paramMap.getMessage());
            if (paramMap.length() == 0) {
              break label1024;
            }
            for (paramMap = "Cannot show popup window: ".concat(paramMap);; paramMap = new String("Cannot show popup window: "))
            {
              zzcb(paramMap);
              this.zzbyo.removeView(this.zzbnz.getView());
              if (this.zzbyp != null)
              {
                this.zzbyp.removeView(this.zzbyk);
                this.zzbyp.addView(this.zzbnz.getView());
                this.zzbnz.zza(this.zzapp);
              }
              return;
            }
            i = -1;
            switch (i)
            {
            }
          }
          this.zzbyn.dismiss();
          break;
          zzcb("Webview is detached, probably in the middle of a resize or expand.");
          return;
          if (!str.equals("top-left")) {
            break label1038;
          }
          i = 0;
          break label1040;
          if (!str.equals("top-center")) {
            break label1038;
          }
          i = 1;
          break label1040;
          if (!str.equals("center")) {
            break label1038;
          }
          i = 2;
          break label1040;
          if (!str.equals("bottom-left")) {
            break label1038;
          }
          i = 3;
          break label1040;
          if (!str.equals("bottom-center")) {
            break label1038;
          }
          i = 4;
          break label1040;
          if (!str.equals("bottom-right")) {
            break label1038;
          }
          i = 5;
          break label1040;
          ((RelativeLayout.LayoutParams)localObject2).addRule(10);
          ((RelativeLayout.LayoutParams)localObject2).addRule(9);
          continue;
          ((RelativeLayout.LayoutParams)localObject2).addRule(10);
          ((RelativeLayout.LayoutParams)localObject2).addRule(14);
          continue;
          ((RelativeLayout.LayoutParams)localObject2).addRule(13);
          continue;
          ((RelativeLayout.LayoutParams)localObject2).addRule(12);
          ((RelativeLayout.LayoutParams)localObject2).addRule(9);
          continue;
          ((RelativeLayout.LayoutParams)localObject2).addRule(12);
          ((RelativeLayout.LayoutParams)localObject2).addRule(14);
          continue;
          ((RelativeLayout.LayoutParams)localObject2).addRule(12);
          ((RelativeLayout.LayoutParams)localObject2).addRule(11);
        }
      }
    }
  }
  
  public void zza(int paramInt1, int paramInt2, boolean paramBoolean)
  {
    synchronized (this.zzako)
    {
      this.zzbyg = paramInt1;
      this.zzbyh = paramInt2;
      if ((this.zzbyn != null) && (paramBoolean))
      {
        int[] arrayOfInt = zzot();
        if (arrayOfInt != null)
        {
          this.zzbyn.update(zzm.zzkr().zzb(this.zzbxt, arrayOfInt[0]), zzm.zzkr().zzb(this.zzbxt, arrayOfInt[1]), this.zzbyn.getWidth(), this.zzbyn.getHeight());
          zzc(arrayOfInt[0], arrayOfInt[1]);
        }
      }
      else
      {
        return;
      }
      zzt(true);
    }
  }
  
  void zzb(int paramInt1, int paramInt2)
  {
    if (this.zzbym != null) {
      this.zzbym.zza(paramInt1, paramInt2, this.zzakh, this.zzaki);
    }
  }
  
  void zzc(int paramInt1, int paramInt2)
  {
    zzb(paramInt1, paramInt2 - zzu.zzgm().zzk(this.zzbxt)[0], this.zzakh, this.zzaki);
  }
  
  public void zzd(int paramInt1, int paramInt2)
  {
    this.zzbyg = paramInt1;
    this.zzbyh = paramInt2;
  }
  
  boolean zzos()
  {
    return (this.zzakh > -1) && (this.zzaki > -1);
  }
  
  public boolean zzou()
  {
    for (;;)
    {
      synchronized (this.zzako)
      {
        if (this.zzbyn != null)
        {
          bool = true;
          return bool;
        }
      }
      boolean bool = false;
    }
  }
  
  boolean zzov()
  {
    Object localObject = zzu.zzgm().zzi(this.zzbxt);
    int[] arrayOfInt = zzu.zzgm().zzk(this.zzbxt);
    int k = localObject[0];
    int i = localObject[1];
    if ((this.zzakh < 50) || (this.zzakh > k))
    {
      zzkx.zzdi("Width is too small or too large.");
      return false;
    }
    if ((this.zzaki < 50) || (this.zzaki > i))
    {
      zzkx.zzdi("Height is too small or too large.");
      return false;
    }
    if ((this.zzaki == i) && (this.zzakh == k))
    {
      zzkx.zzdi("Cannot resize to a full-screen ad.");
      return false;
    }
    label188:
    int j;
    if (this.zzbyf)
    {
      localObject = this.zzbye;
      i = -1;
      switch (((String)localObject).hashCode())
      {
      default: 
        switch (i)
        {
        default: 
          j = this.zzbyg + this.zzbyi + this.zzakh - 50;
          i = this.zzbyh + this.zzbyj;
        }
        break;
      }
    }
    while ((j >= 0) && (j + 50 <= k) && (i >= arrayOfInt[0]) && (i + 50 <= arrayOfInt[1]))
    {
      return true;
      if (!((String)localObject).equals("top-left")) {
        break label188;
      }
      i = 0;
      break label188;
      if (!((String)localObject).equals("top-center")) {
        break label188;
      }
      i = 1;
      break label188;
      if (!((String)localObject).equals("center")) {
        break label188;
      }
      i = 2;
      break label188;
      if (!((String)localObject).equals("bottom-left")) {
        break label188;
      }
      i = 3;
      break label188;
      if (!((String)localObject).equals("bottom-center")) {
        break label188;
      }
      i = 4;
      break label188;
      if (!((String)localObject).equals("bottom-right")) {
        break label188;
      }
      i = 5;
      break label188;
      i = this.zzbyg;
      j = this.zzbyi + i;
      i = this.zzbyh + this.zzbyj;
      continue;
      j = this.zzbyg + this.zzbyi + this.zzakh / 2 - 25;
      i = this.zzbyh + this.zzbyj;
      continue;
      j = this.zzbyg + this.zzbyi + this.zzakh / 2 - 25;
      i = this.zzbyh + this.zzbyj + this.zzaki / 2 - 25;
      continue;
      i = this.zzbyg;
      j = this.zzbyi + i;
      i = this.zzbyh + this.zzbyj + this.zzaki - 50;
      continue;
      j = this.zzbyg + this.zzbyi + this.zzakh / 2 - 25;
      i = this.zzbyh + this.zzbyj + this.zzaki - 50;
      continue;
      j = this.zzbyg + this.zzbyi + this.zzakh - 50;
      i = this.zzbyh + this.zzbyj + this.zzaki - 50;
    }
  }
  
  public void zzt(boolean paramBoolean)
  {
    synchronized (this.zzako)
    {
      if (this.zzbyn != null)
      {
        this.zzbyn.dismiss();
        this.zzbyo.removeView(this.zzbnz.getView());
        if (this.zzbyp != null)
        {
          this.zzbyp.removeView(this.zzbyk);
          this.zzbyp.addView(this.zzbnz.getView());
          this.zzbnz.zza(this.zzapp);
        }
        if (paramBoolean)
        {
          zzcd("default");
          if (this.zzbym != null) {
            this.zzbym.zzfc();
          }
        }
        this.zzbyn = null;
        this.zzbyo = null;
        this.zzbyp = null;
        this.zzbyl = null;
      }
      return;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzhq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */