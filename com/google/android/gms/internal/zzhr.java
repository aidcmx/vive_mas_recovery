package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.zzu;
import java.util.Map;

@zzji
public class zzhr
{
  private final zzmd zzbnz;
  private final boolean zzbyr;
  private final String zzbys;
  
  public zzhr(zzmd paramzzmd, Map<String, String> paramMap)
  {
    this.zzbnz = paramzzmd;
    this.zzbys = ((String)paramMap.get("forceOrientation"));
    if (paramMap.containsKey("allowOrientationChange"))
    {
      this.zzbyr = Boolean.parseBoolean((String)paramMap.get("allowOrientationChange"));
      return;
    }
    this.zzbyr = true;
  }
  
  public void execute()
  {
    if (this.zzbnz == null)
    {
      zzkx.zzdi("AdWebView is null");
      return;
    }
    int i;
    if ("portrait".equalsIgnoreCase(this.zzbys)) {
      i = zzu.zzgo().zzvx();
    }
    for (;;)
    {
      this.zzbnz.setRequestedOrientation(i);
      return;
      if ("landscape".equalsIgnoreCase(this.zzbys)) {
        i = zzu.zzgo().zzvw();
      } else if (this.zzbyr) {
        i = -1;
      } else {
        i = zzu.zzgo().zzvy();
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzhr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */