package com.google.android.gms.internal;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.client.zzm;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.ads.internal.zzu;
import java.util.Map;

@zzji
public class zzhu
  extends zzhv
  implements zzfe
{
  private final Context mContext;
  private final WindowManager zzati;
  DisplayMetrics zzaur;
  private final zzmd zzbnz;
  private final zzdj zzbzb;
  private float zzbzc;
  int zzbzd = -1;
  int zzbze = -1;
  private int zzbzf;
  int zzbzg = -1;
  int zzbzh = -1;
  int zzbzi = -1;
  int zzbzj = -1;
  
  public zzhu(zzmd paramzzmd, Context paramContext, zzdj paramzzdj)
  {
    super(paramzzmd);
    this.zzbnz = paramzzmd;
    this.mContext = paramContext;
    this.zzbzb = paramzzdj;
    this.zzati = ((WindowManager)paramContext.getSystemService("window"));
  }
  
  private void zzox()
  {
    this.zzaur = new DisplayMetrics();
    Display localDisplay = this.zzati.getDefaultDisplay();
    localDisplay.getMetrics(this.zzaur);
    this.zzbzc = this.zzaur.density;
    this.zzbzf = localDisplay.getRotation();
  }
  
  private void zzpc()
  {
    int[] arrayOfInt = new int[2];
    this.zzbnz.getLocationOnScreen(arrayOfInt);
    zze(zzm.zzkr().zzc(this.mContext, arrayOfInt[0]), zzm.zzkr().zzc(this.mContext, arrayOfInt[1]));
  }
  
  private zzht zzpf()
  {
    zzht.zza localzza = new zzht.zza().zzv(this.zzbzb.zzlj()).zzu(this.zzbzb.zzlk()).zzw(this.zzbzb.zzln()).zzx(this.zzbzb.zzll());
    zzdj localzzdj = this.zzbzb;
    return localzza.zzy(true).zzow();
  }
  
  public void zza(zzmd paramzzmd, Map<String, String> paramMap)
  {
    zzpa();
  }
  
  public void zze(int paramInt1, int paramInt2)
  {
    if ((this.mContext instanceof Activity)) {}
    for (int i = zzu.zzgm().zzk((Activity)this.mContext)[0];; i = 0)
    {
      if ((this.zzbnz.zzeg() == null) || (!this.zzbnz.zzeg().zzazr))
      {
        this.zzbzi = zzm.zzkr().zzc(this.mContext, this.zzbnz.getMeasuredWidth());
        this.zzbzj = zzm.zzkr().zzc(this.mContext, this.zzbnz.getMeasuredHeight());
      }
      zzc(paramInt1, paramInt2 - i, this.zzbzi, this.zzbzj);
      this.zzbnz.zzxc().zzd(paramInt1, paramInt2);
      return;
    }
  }
  
  void zzoy()
  {
    this.zzbzd = zzm.zzkr().zzb(this.zzaur, this.zzaur.widthPixels);
    this.zzbze = zzm.zzkr().zzb(this.zzaur, this.zzaur.heightPixels);
    Object localObject = this.zzbnz.zzwy();
    if ((localObject == null) || (((Activity)localObject).getWindow() == null))
    {
      this.zzbzg = this.zzbzd;
      this.zzbzh = this.zzbze;
      return;
    }
    localObject = zzu.zzgm().zzh((Activity)localObject);
    this.zzbzg = zzm.zzkr().zzb(this.zzaur, localObject[0]);
    this.zzbzh = zzm.zzkr().zzb(this.zzaur, localObject[1]);
  }
  
  void zzoz()
  {
    if (this.zzbnz.zzeg().zzazr)
    {
      this.zzbzi = this.zzbzd;
      this.zzbzj = this.zzbze;
      return;
    }
    this.zzbnz.measure(0, 0);
  }
  
  public void zzpa()
  {
    zzox();
    zzoy();
    zzoz();
    zzpd();
    zzpe();
    zzpc();
    zzpb();
  }
  
  void zzpb()
  {
    if (zzkx.zzbi(2)) {
      zzkx.zzdh("Dispatching Ready Event.");
    }
    zzcc(this.zzbnz.zzxf().zzda);
  }
  
  void zzpd()
  {
    zza(this.zzbzd, this.zzbze, this.zzbzg, this.zzbzh, this.zzbzc, this.zzbzf);
  }
  
  void zzpe()
  {
    zzht localzzht = zzpf();
    this.zzbnz.zzb("onDeviceFeaturesReceived", localzzht.toJson());
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzhu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */