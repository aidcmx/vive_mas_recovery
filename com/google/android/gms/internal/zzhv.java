package com.google.android.gms.internal;

import org.json.JSONException;
import org.json.JSONObject;

@zzji
public class zzhv
{
  private final zzmd zzbnz;
  private final String zzbzk;
  
  public zzhv(zzmd paramzzmd)
  {
    this(paramzzmd, "");
  }
  
  public zzhv(zzmd paramzzmd, String paramString)
  {
    this.zzbnz = paramzzmd;
    this.zzbzk = paramString;
  }
  
  public void zza(int paramInt1, int paramInt2, int paramInt3, int paramInt4, float paramFloat, int paramInt5)
  {
    try
    {
      JSONObject localJSONObject = new JSONObject().put("width", paramInt1).put("height", paramInt2).put("maxSizeWidth", paramInt3).put("maxSizeHeight", paramInt4).put("density", paramFloat).put("rotation", paramInt5);
      this.zzbnz.zzb("onScreenInfoChanged", localJSONObject);
      return;
    }
    catch (JSONException localJSONException)
    {
      zzkx.zzb("Error occured while obtaining screen information.", localJSONException);
    }
  }
  
  public void zzb(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    try
    {
      JSONObject localJSONObject = new JSONObject().put("x", paramInt1).put("y", paramInt2).put("width", paramInt3).put("height", paramInt4);
      this.zzbnz.zzb("onSizeChanged", localJSONObject);
      return;
    }
    catch (JSONException localJSONException)
    {
      zzkx.zzb("Error occured while dispatching size change.", localJSONException);
    }
  }
  
  public void zzc(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    try
    {
      JSONObject localJSONObject = new JSONObject().put("x", paramInt1).put("y", paramInt2).put("width", paramInt3).put("height", paramInt4);
      this.zzbnz.zzb("onDefaultPositionReceived", localJSONObject);
      return;
    }
    catch (JSONException localJSONException)
    {
      zzkx.zzb("Error occured while dispatching default position.", localJSONException);
    }
  }
  
  public void zzcb(String paramString)
  {
    try
    {
      paramString = new JSONObject().put("message", paramString).put("action", this.zzbzk);
      this.zzbnz.zzb("onError", paramString);
      return;
    }
    catch (JSONException paramString)
    {
      zzkx.zzb("Error occurred while dispatching error event.", paramString);
    }
  }
  
  public void zzcc(String paramString)
  {
    try
    {
      paramString = new JSONObject().put("js", paramString);
      this.zzbnz.zzb("onReadyEventReceived", paramString);
      return;
    }
    catch (JSONException paramString)
    {
      zzkx.zzb("Error occured while dispatching ready Event.", paramString);
    }
  }
  
  public void zzcd(String paramString)
  {
    try
    {
      paramString = new JSONObject().put("state", paramString);
      this.zzbnz.zzb("onStateChanged", paramString);
      return;
    }
    catch (JSONException paramString)
    {
      zzkx.zzb("Error occured while dispatching state change.", paramString);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzhv.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */