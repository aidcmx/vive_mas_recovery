package com.google.android.gms.internal;

import android.app.Activity;
import android.os.IBinder;
import android.os.RemoteException;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.dynamic.zzd;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.dynamic.zzg;
import com.google.android.gms.dynamic.zzg.zza;

@zzji
public final class zzhx
  extends zzg<zzhz>
{
  public zzhx()
  {
    super("com.google.android.gms.ads.AdOverlayCreatorImpl");
  }
  
  protected zzhz zzas(IBinder paramIBinder)
  {
    return zzhz.zza.zzau(paramIBinder);
  }
  
  public zzhy zzf(Activity paramActivity)
  {
    try
    {
      zzd localzzd = zze.zzac(paramActivity);
      paramActivity = zzhy.zza.zzat(((zzhz)zzcr(paramActivity)).zzo(localzzd));
      return paramActivity;
    }
    catch (RemoteException paramActivity)
    {
      zzb.zzc("Could not create remote AdOverlay.", paramActivity);
      return null;
    }
    catch (zzg.zza paramActivity)
    {
      zzb.zzc("Could not create remote AdOverlay.", paramActivity);
    }
    return null;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzhx.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */