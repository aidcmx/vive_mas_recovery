package com.google.android.gms.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import org.json.JSONObject;

@zzji
public class zzid
  extends Handler
{
  private final zzic zzceu;
  
  public zzid(Context paramContext)
  {
    this(new zzie(localContext));
  }
  
  public zzid(zzic paramzzic)
  {
    this.zzceu = paramzzic;
  }
  
  private void zzd(JSONObject paramJSONObject)
  {
    try
    {
      this.zzceu.zza(paramJSONObject.getString("request_id"), paramJSONObject.getString("base_url"), paramJSONObject.getString("html"));
      return;
    }
    catch (Exception paramJSONObject) {}
  }
  
  public void handleMessage(Message paramMessage)
  {
    try
    {
      paramMessage = paramMessage.getData();
      if (paramMessage == null) {
        return;
      }
      paramMessage = new JSONObject(paramMessage.getString("data"));
      if ("fetch_html".equals(paramMessage.getString("message_name")))
      {
        zzd(paramMessage);
        return;
      }
    }
    catch (Exception paramMessage) {}
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzid.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */