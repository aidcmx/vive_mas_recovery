package com.google.android.gms.internal;

import android.content.Context;
import android.os.Handler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@zzji
public class zzie
  implements zzic
{
  private final Context mContext;
  final Set<WebView> zzcev = Collections.synchronizedSet(new HashSet());
  
  public zzie(Context paramContext)
  {
    this.mContext = paramContext;
  }
  
  public void zza(String paramString1, String paramString2, String paramString3)
  {
    zzkx.zzdg("Fetching assets for the given html");
    zzlb.zzcvl.post(new zzie.1(this, paramString2, paramString3));
  }
  
  public WebView zzrm()
  {
    WebView localWebView = new WebView(this.mContext);
    localWebView.getSettings().setJavaScriptEnabled(true);
    return localWebView;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzie.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */