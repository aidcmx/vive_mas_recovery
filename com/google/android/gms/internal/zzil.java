package com.google.android.gms.internal;

import com.google.android.gms.ads.purchase.InAppPurchaseListener;

@zzji
public final class zzil
  extends zzig.zza
{
  private final InAppPurchaseListener zzbbg;
  
  public zzil(InAppPurchaseListener paramInAppPurchaseListener)
  {
    this.zzbbg = paramInAppPurchaseListener;
  }
  
  public void zza(zzif paramzzif)
  {
    this.zzbbg.onInAppPurchaseRequested(new zzio(paramzzif));
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzil.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */