package com.google.android.gms.internal;

import android.app.Activity;
import android.os.IBinder;
import android.os.RemoteException;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.dynamic.zzd;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.dynamic.zzg;
import com.google.android.gms.dynamic.zzg.zza;

@zzji
public final class zzim
  extends zzg<zzii>
{
  public zzim()
  {
    super("com.google.android.gms.ads.InAppPurchaseManagerCreatorImpl");
  }
  
  protected zzii zzbc(IBinder paramIBinder)
  {
    return zzii.zza.zzaz(paramIBinder);
  }
  
  public zzih zzg(Activity paramActivity)
  {
    try
    {
      zzd localzzd = zze.zzac(paramActivity);
      paramActivity = zzih.zza.zzay(((zzii)zzcr(paramActivity)).zzp(localzzd));
      return paramActivity;
    }
    catch (RemoteException paramActivity)
    {
      zzb.zzc("Could not create remote InAppPurchaseManager.", paramActivity);
      return null;
    }
    catch (zzg.zza paramActivity)
    {
      zzb.zzc("Could not create remote InAppPurchaseManager.", paramActivity);
    }
    return null;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzim.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */