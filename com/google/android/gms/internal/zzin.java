package com.google.android.gms.internal;

import android.content.Intent;
import android.os.RemoteException;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.ads.purchase.InAppPurchaseResult;

@zzji
public class zzin
  implements InAppPurchaseResult
{
  private final zzij zzcgd;
  
  public zzin(zzij paramzzij)
  {
    this.zzcgd = paramzzij;
  }
  
  public void finishPurchase()
  {
    try
    {
      this.zzcgd.finishPurchase();
      return;
    }
    catch (RemoteException localRemoteException)
    {
      zzb.zzc("Could not forward finishPurchase to InAppPurchaseResult", localRemoteException);
    }
  }
  
  public String getProductId()
  {
    try
    {
      String str = this.zzcgd.getProductId();
      return str;
    }
    catch (RemoteException localRemoteException)
    {
      zzb.zzc("Could not forward getProductId to InAppPurchaseResult", localRemoteException);
    }
    return null;
  }
  
  public Intent getPurchaseData()
  {
    try
    {
      Intent localIntent = this.zzcgd.getPurchaseData();
      return localIntent;
    }
    catch (RemoteException localRemoteException)
    {
      zzb.zzc("Could not forward getPurchaseData to InAppPurchaseResult", localRemoteException);
    }
    return null;
  }
  
  public int getResultCode()
  {
    try
    {
      int i = this.zzcgd.getResultCode();
      return i;
    }
    catch (RemoteException localRemoteException)
    {
      zzb.zzc("Could not forward getPurchaseData to InAppPurchaseResult", localRemoteException);
    }
    return 0;
  }
  
  public boolean isVerified()
  {
    try
    {
      boolean bool = this.zzcgd.isVerified();
      return bool;
    }
    catch (RemoteException localRemoteException)
    {
      zzb.zzc("Could not forward isVerified to InAppPurchaseResult", localRemoteException);
    }
    return false;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzin.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */