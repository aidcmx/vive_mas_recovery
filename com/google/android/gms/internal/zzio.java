package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.ads.purchase.InAppPurchase;

@zzji
public class zzio
  implements InAppPurchase
{
  private final zzif zzcfp;
  
  public zzio(zzif paramzzif)
  {
    this.zzcfp = paramzzif;
  }
  
  public String getProductId()
  {
    try
    {
      String str = this.zzcfp.getProductId();
      return str;
    }
    catch (RemoteException localRemoteException)
    {
      zzb.zzc("Could not forward getProductId to InAppPurchase", localRemoteException);
    }
    return null;
  }
  
  public void recordPlayBillingResolution(int paramInt)
  {
    try
    {
      this.zzcfp.recordPlayBillingResolution(paramInt);
      return;
    }
    catch (RemoteException localRemoteException)
    {
      zzb.zzc("Could not forward recordPlayBillingResolution to InAppPurchase", localRemoteException);
    }
  }
  
  public void recordResolution(int paramInt)
  {
    try
    {
      this.zzcfp.recordResolution(paramInt);
      return;
    }
    catch (RemoteException localRemoteException)
    {
      zzb.zzc("Could not forward recordResolution to InAppPurchase", localRemoteException);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzio.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */