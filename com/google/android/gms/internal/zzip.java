package com.google.android.gms.internal;

import com.google.android.gms.ads.purchase.PlayStorePurchaseListener;

@zzji
public final class zzip
  extends zzik.zza
{
  private final PlayStorePurchaseListener zzbbi;
  
  public zzip(PlayStorePurchaseListener paramPlayStorePurchaseListener)
  {
    this.zzbbi = paramPlayStorePurchaseListener;
  }
  
  public boolean isValidPurchase(String paramString)
  {
    return this.zzbbi.isValidPurchase(paramString);
  }
  
  public void zza(zzij paramzzij)
  {
    this.zzbbi.onInAppPurchaseFinished(new zzin(paramzzij));
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzip.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */