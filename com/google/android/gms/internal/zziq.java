package com.google.android.gms.internal;

import android.content.Context;
import android.os.Handler;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.common.internal.zzaa;
import java.util.concurrent.atomic.AtomicBoolean;

@zzji
public abstract class zziq
  implements zzld<Void>, zzme.zza
{
  protected final Context mContext;
  protected final zzmd zzbnz;
  protected final zziu.zza zzcge;
  protected final zzko.zza zzcgf;
  protected AdResponseParcel zzcgg;
  private Runnable zzcgh;
  protected final Object zzcgi = new Object();
  private AtomicBoolean zzcgj = new AtomicBoolean(true);
  
  protected zziq(Context paramContext, zzko.zza paramzza, zzmd paramzzmd, zziu.zza paramzza1)
  {
    this.mContext = paramContext;
    this.zzcgf = paramzza;
    this.zzcgg = this.zzcgf.zzcsu;
    this.zzbnz = paramzzmd;
    this.zzcge = paramzza1;
  }
  
  private zzko zzap(int paramInt)
  {
    AdRequestInfoParcel localAdRequestInfoParcel = this.zzcgf.zzcmx;
    return new zzko(localAdRequestInfoParcel.zzcju, this.zzbnz, this.zzcgg.zzbvk, paramInt, this.zzcgg.zzbvl, this.zzcgg.zzcld, this.zzcgg.orientation, this.zzcgg.zzbvq, localAdRequestInfoParcel.zzcjx, this.zzcgg.zzclb, null, null, null, null, null, this.zzcgg.zzclc, this.zzcgf.zzarm, this.zzcgg.zzcla, this.zzcgf.zzcso, this.zzcgg.zzclf, this.zzcgg.zzclg, this.zzcgf.zzcsi, null, this.zzcgg.zzclq, this.zzcgg.zzclr, this.zzcgg.zzcls, this.zzcgg.zzclt, this.zzcgg.zzclu, null, this.zzcgg.zzbvn, this.zzcgg.zzclx);
  }
  
  public void cancel()
  {
    if (!this.zzcgj.getAndSet(false)) {
      return;
    }
    this.zzbnz.stopLoading();
    zzu.zzgo().zzl(this.zzbnz);
    zzao(-1);
    zzlb.zzcvl.removeCallbacks(this.zzcgh);
  }
  
  public void zza(zzmd paramzzmd, boolean paramBoolean)
  {
    int i = 0;
    zzkx.zzdg("WebView finished loading.");
    if (!this.zzcgj.getAndSet(false)) {
      return;
    }
    if (paramBoolean) {
      i = zzry();
    }
    zzao(i);
    zzlb.zzcvl.removeCallbacks(this.zzcgh);
  }
  
  protected void zzao(int paramInt)
  {
    if (paramInt != -2) {
      this.zzcgg = new AdResponseParcel(paramInt, this.zzcgg.zzbvq);
    }
    this.zzbnz.zzwx();
    this.zzcge.zzb(zzap(paramInt));
  }
  
  public final Void zzrw()
  {
    zzaa.zzhs("Webview render task needs to be called on UI thread.");
    this.zzcgh = new zziq.1(this);
    zzlb.zzcvl.postDelayed(this.zzcgh, ((Long)zzdr.zzbhk.get()).longValue());
    zzrx();
    return null;
  }
  
  protected abstract void zzrx();
  
  protected int zzry()
  {
    return -2;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zziq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */