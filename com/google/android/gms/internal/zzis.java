package com.google.android.gms.internal;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import com.google.android.gms.ads.internal.client.AdSizeParcel;

@zzji
public class zzis
  extends zziq
{
  private zzir zzcgu;
  
  zzis(Context paramContext, zzko.zza paramzza, zzmd paramzzmd, zziu.zza paramzza1)
  {
    super(paramContext, paramzza, paramzzmd, paramzza1);
  }
  
  protected void zzrx()
  {
    Object localObject = this.zzbnz.zzeg();
    int j;
    if (((AdSizeParcel)localObject).zzazr)
    {
      localObject = this.mContext.getResources().getDisplayMetrics();
      j = ((DisplayMetrics)localObject).widthPixels;
    }
    for (int i = ((DisplayMetrics)localObject).heightPixels;; i = ((AdSizeParcel)localObject).heightPixels)
    {
      this.zzcgu = new zzir(this, this.zzbnz, j, i);
      this.zzbnz.zzxc().zza(this);
      this.zzcgu.zza(this.zzcgg);
      return;
      j = ((AdSizeParcel)localObject).widthPixels;
    }
  }
  
  protected int zzry()
  {
    if (this.zzcgu.zzsc())
    {
      zzkx.zzdg("Ad-Network indicated no fill with passback URL.");
      return 3;
    }
    if (!this.zzcgu.zzsd()) {
      return 2;
    }
    return -2;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzis.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */