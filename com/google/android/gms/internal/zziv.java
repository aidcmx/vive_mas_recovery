package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.ads.internal.request.AdResponseParcel;

@zzji
public class zziv
  extends zziq
  implements zzme.zza
{
  zziv(Context paramContext, zzko.zza paramzza, zzmd paramzzmd, zziu.zza paramzza1)
  {
    super(paramContext, paramzza, paramzzmd, paramzza1);
  }
  
  protected void zzrx()
  {
    if (this.zzcgg.errorCode != -2) {
      return;
    }
    this.zzbnz.zzxc().zza(this);
    zzse();
    zzkx.zzdg("Loading HTML in WebView.");
    this.zzbnz.loadDataWithBaseURL(this.zzcgg.zzcbo, this.zzcgg.body, "text/html", "UTF-8", null);
  }
  
  protected void zzse() {}
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zziv.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */