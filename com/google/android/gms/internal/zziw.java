package com.google.android.gms.internal;

import android.os.Handler;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.request.AdResponseParcel;

@zzji
public class zziw
  extends zzkw
{
  private final zziu.zza zzcge;
  private final zzko.zza zzcgf;
  private final AdResponseParcel zzcgg;
  
  public zziw(zzko.zza paramzza, zziu.zza paramzza1)
  {
    this.zzcgf = paramzza;
    this.zzcgg = this.zzcgf.zzcsu;
    this.zzcge = paramzza1;
  }
  
  private zzko zzaq(int paramInt)
  {
    return new zzko(this.zzcgf.zzcmx.zzcju, null, null, paramInt, null, null, this.zzcgg.orientation, this.zzcgg.zzbvq, this.zzcgf.zzcmx.zzcjx, false, null, null, null, null, null, this.zzcgg.zzclc, this.zzcgf.zzarm, this.zzcgg.zzcla, this.zzcgf.zzcso, this.zzcgg.zzclf, this.zzcgg.zzclg, this.zzcgf.zzcsi, null, null, null, null, this.zzcgf.zzcsu.zzclt, this.zzcgf.zzcsu.zzclu, null, null, null);
  }
  
  public void onStop() {}
  
  public void zzfp()
  {
    zzko localzzko = zzaq(0);
    zzlb.zzcvl.post(new zziw.1(this, localzzko));
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zziw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */