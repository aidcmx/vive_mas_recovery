package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.PopupWindow;

@zzji
@TargetApi(19)
public class zzix
  extends zziv
{
  private Object zzcgz = new Object();
  private PopupWindow zzcha;
  private boolean zzchb = false;
  
  zzix(Context paramContext, zzko.zza paramzza, zzmd paramzzmd, zziu.zza paramzza1)
  {
    super(paramContext, paramzza, paramzzmd, paramzza1);
  }
  
  private void zzsf()
  {
    synchronized (this.zzcgz)
    {
      this.zzchb = true;
      if (((this.mContext instanceof Activity)) && (((Activity)this.mContext).isDestroyed())) {
        this.zzcha = null;
      }
      if (this.zzcha != null)
      {
        if (this.zzcha.isShowing()) {
          this.zzcha.dismiss();
        }
        this.zzcha = null;
      }
      return;
    }
  }
  
  public void cancel()
  {
    zzsf();
    super.cancel();
  }
  
  protected void zzao(int paramInt)
  {
    zzsf();
    super.zzao(paramInt);
  }
  
  protected void zzse()
  {
    if ((this.mContext instanceof Activity)) {}
    Object localObject2;
    for (Window localWindow = ((Activity)this.mContext).getWindow();; localObject2 = null)
    {
      if ((localWindow == null) || (localWindow.getDecorView() == null)) {}
      while (((Activity)this.mContext).isDestroyed()) {
        return;
      }
      FrameLayout localFrameLayout = new FrameLayout(this.mContext);
      localFrameLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
      localFrameLayout.addView(this.zzbnz.getView(), -1, -1);
      synchronized (this.zzcgz)
      {
        if (this.zzchb) {
          return;
        }
      }
      this.zzcha = new PopupWindow(localFrameLayout, 1, 1, false);
      this.zzcha.setOutsideTouchable(true);
      this.zzcha.setClippingEnabled(false);
      zzkx.zzdg("Displaying the 1x1 popup off the screen.");
      try
      {
        this.zzcha.showAtLocation(((Window)localObject1).getDecorView(), 0, -1, -1);
        return;
      }
      catch (Exception localException)
      {
        for (;;)
        {
          this.zzcha = null;
        }
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzix.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */