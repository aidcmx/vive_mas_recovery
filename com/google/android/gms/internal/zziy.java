package com.google.android.gms.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import com.google.android.gms.ads.internal.request.AutoClickProtectionConfigurationParcel;
import com.google.android.gms.ads.internal.reward.mediation.client.RewardItemParcel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import org.json.JSONObject;

@zzji
public class zziy
  extends zzit
{
  private final zzdz zzalt;
  private zzgz zzamf;
  private final zzmd zzbnz;
  private zzgq zzbwc;
  zzgo zzchc;
  protected zzgu zzchd;
  private boolean zzche;
  
  zziy(Context paramContext, zzko.zza paramzza, zzgz paramzzgz, zziu.zza paramzza1, zzdz paramzzdz, zzmd paramzzmd)
  {
    super(paramContext, paramzza, paramzza1);
    this.zzamf = paramzzgz;
    this.zzbwc = paramzza.zzcsk;
    this.zzalt = paramzzdz;
    this.zzbnz = paramzzmd;
  }
  
  private static String zza(zzgu paramzzgu)
  {
    String str = paramzzgu.zzbwm.zzbuv;
    int i = zzar(paramzzgu.zzbwl);
    long l = paramzzgu.zzbwr;
    return String.valueOf(str).length() + 33 + str + "." + i + "." + l;
  }
  
  private static int zzar(int paramInt)
  {
    switch (paramInt)
    {
    case 2: 
    default: 
      return 6;
    case 0: 
      return 0;
    case 1: 
      return 1;
    case 3: 
      return 2;
    case 4: 
      return 3;
    case -1: 
      return 4;
    }
    return 5;
  }
  
  private static String zzg(List<zzgu> paramList)
  {
    if (paramList == null) {
      return "".toString();
    }
    Iterator localIterator = paramList.iterator();
    label20:
    Object localObject;
    for (paramList = ""; localIterator.hasNext(); paramList = String.valueOf(paramList).length() + 1 + String.valueOf(localObject).length() + paramList + (String)localObject + "_")
    {
      localObject = (zzgu)localIterator.next();
      if ((localObject == null) || (((zzgu)localObject).zzbwm == null) || (TextUtils.isEmpty(((zzgu)localObject).zzbwm.zzbuv))) {
        break label20;
      }
      paramList = String.valueOf(paramList);
      localObject = String.valueOf(zza((zzgu)localObject));
    }
    return paramList.substring(0, Math.max(0, paramList.length() - 1));
  }
  
  private void zzsg()
    throws zzit.zza
  {
    ??? = new CountDownLatch(1);
    zzlb.zzcvl.post(new zziy.1(this, (CountDownLatch)???));
    String str;
    try
    {
      ((CountDownLatch)???).await(10L, TimeUnit.SECONDS);
      synchronized (this.zzcgi)
      {
        if (!this.zzche) {
          throw new zzit.zza("View could not be prepared", 0);
        }
      }
      if (!this.zzbnz.isDestroyed()) {
        break label133;
      }
    }
    catch (InterruptedException localInterruptedException)
    {
      str = String.valueOf(localInterruptedException);
      throw new zzit.zza(String.valueOf(str).length() + 38 + "Interrupted while waiting for latch : " + str, 0);
    }
    throw new zzit.zza("Assets not loaded, web view is destroyed", 0);
    label133:
  }
  
  public void onStop()
  {
    synchronized (this.zzcgi)
    {
      super.onStop();
      if (this.zzchc != null) {
        this.zzchc.cancel();
      }
      return;
    }
  }
  
  protected zzko zzap(int paramInt)
  {
    Object localObject = this.zzcgf.zzcmx;
    AdRequestParcel localAdRequestParcel = ((AdRequestInfoParcel)localObject).zzcju;
    zzmd localzzmd = this.zzbnz;
    List localList1 = this.zzcgg.zzbvk;
    List localList2 = this.zzcgg.zzbvl;
    List localList3 = this.zzcgg.zzcld;
    int i = this.zzcgg.orientation;
    long l1 = this.zzcgg.zzbvq;
    String str3 = ((AdRequestInfoParcel)localObject).zzcjx;
    boolean bool2 = this.zzcgg.zzclb;
    zzha localzzha;
    label113:
    String str1;
    label129:
    zzgq localzzgq;
    zzgs localzzgs;
    label151:
    long l2;
    AdSizeParcel localAdSizeParcel;
    long l3;
    long l4;
    long l5;
    String str4;
    JSONObject localJSONObject;
    RewardItemParcel localRewardItemParcel;
    List localList4;
    List localList5;
    boolean bool1;
    label257:
    AutoClickProtectionConfigurationParcel localAutoClickProtectionConfigurationParcel;
    if (this.zzchd != null)
    {
      localObject = this.zzchd.zzbwm;
      if (this.zzchd == null) {
        break label369;
      }
      localzzha = this.zzchd.zzbwn;
      if (this.zzchd == null) {
        break label375;
      }
      str1 = this.zzchd.zzbwo;
      localzzgq = this.zzbwc;
      if (this.zzchd == null) {
        break label386;
      }
      localzzgs = this.zzchd.zzbwp;
      l2 = this.zzcgg.zzclc;
      localAdSizeParcel = this.zzcgf.zzarm;
      l3 = this.zzcgg.zzcla;
      l4 = this.zzcgf.zzcso;
      l5 = this.zzcgg.zzclf;
      str4 = this.zzcgg.zzclg;
      localJSONObject = this.zzcgf.zzcsi;
      localRewardItemParcel = this.zzcgg.zzclq;
      localList4 = this.zzcgg.zzclr;
      localList5 = this.zzcgg.zzcls;
      if (this.zzbwc == null) {
        break label392;
      }
      bool1 = this.zzbwc.zzbvv;
      localAutoClickProtectionConfigurationParcel = this.zzcgg.zzclu;
      if (this.zzchc == null) {
        break label398;
      }
    }
    label369:
    label375:
    label386:
    label392:
    label398:
    for (String str2 = zzg(this.zzchc.zzoe());; str2 = null)
    {
      return new zzko(localAdRequestParcel, localzzmd, localList1, paramInt, localList2, localList3, i, l1, str3, bool2, (zzgp)localObject, localzzha, str1, localzzgq, localzzgs, l2, localAdSizeParcel, l3, l4, l5, str4, localJSONObject, null, localRewardItemParcel, localList4, localList5, bool1, localAutoClickProtectionConfigurationParcel, str2, this.zzcgg.zzbvn, this.zzcgg.zzclx);
      localObject = null;
      break;
      localzzha = null;
      break label113;
      str1 = AdMobAdapter.class.getName();
      break label129;
      localzzgs = null;
      break label151;
      bool1 = false;
      break label257;
    }
  }
  
  protected void zzh(long paramLong)
    throws zzit.zza
  {
    for (;;)
    {
      synchronized (this.zzcgi)
      {
        this.zzchc = zzi(paramLong);
        ??? = new ArrayList(this.zzbwc.zzbvi);
        Object localObject2 = this.zzcgf.zzcmx.zzcju.zzayv;
        if (localObject2 == null) {
          break label271;
        }
        localObject2 = ((Bundle)localObject2).getBundle("com.google.ads.mediation.admob.AdMobAdapter");
        if (localObject2 == null) {
          break label271;
        }
        bool = ((Bundle)localObject2).getBoolean("_skipMediation");
        if (bool)
        {
          localObject2 = ((List)???).listIterator();
          if (((ListIterator)localObject2).hasNext())
          {
            if (((zzgp)((ListIterator)localObject2).next()).zzbuu.contains("com.google.ads.mediation.admob.AdMobAdapter")) {
              continue;
            }
            ((ListIterator)localObject2).remove();
          }
        }
      }
      this.zzchd = this.zzchc.zzd((List)???);
      switch (this.zzchd.zzbwl)
      {
      default: 
        int i = this.zzchd.zzbwl;
        throw new zzit.zza(40 + "Unexpected mediation result: " + i, 0);
      case 1: 
        throw new zzit.zza("No fill from any mediation ad networks.", 3);
      }
      if ((this.zzchd.zzbwm != null) && (this.zzchd.zzbwm.zzbvd != null)) {
        zzsg();
      }
      return;
      label271:
      boolean bool = false;
    }
  }
  
  zzgo zzi(long paramLong)
  {
    if (this.zzbwc.zzbvt != -1) {
      return new zzgw(this.mContext, this.zzcgf.zzcmx, this.zzamf, this.zzbwc, this.zzcgg.zzazt, this.zzcgg.zzazv, paramLong, ((Long)zzdr.zzbhk.get()).longValue(), 2);
    }
    return new zzgx(this.mContext, this.zzcgf.zzcmx, this.zzamf, this.zzbwc, this.zzcgg.zzazt, this.zzcgg.zzazv, paramLong, ((Long)zzdr.zzbhk.get()).longValue(), this.zzalt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zziy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */