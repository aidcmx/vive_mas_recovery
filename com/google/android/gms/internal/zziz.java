package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import com.google.android.gms.ads.internal.zzq;
import java.util.concurrent.Future;

@zzji
public class zziz
  extends zzkw
{
  private final Object zzako = new Object();
  private final zziu.zza zzcge;
  private final zzko.zza zzcgf;
  private final AdResponseParcel zzcgg;
  private final zzjb zzchg;
  private Future<zzko> zzchh;
  
  public zziz(Context paramContext, zzq paramzzq, zzko.zza paramzza, zzav paramzzav, zziu.zza paramzza1, zzdz paramzzdz)
  {
    this(paramzza, paramzza1, new zzjb(paramContext, paramzzq, new zzli(paramContext), paramzzav, paramzza, paramzzdz));
  }
  
  zziz(zzko.zza paramzza, zziu.zza paramzza1, zzjb paramzzjb)
  {
    this.zzcgf = paramzza;
    this.zzcgg = paramzza.zzcsu;
    this.zzcge = paramzza1;
    this.zzchg = paramzzjb;
  }
  
  private zzko zzaq(int paramInt)
  {
    return new zzko(this.zzcgf.zzcmx.zzcju, null, null, paramInt, null, null, this.zzcgg.orientation, this.zzcgg.zzbvq, this.zzcgf.zzcmx.zzcjx, false, null, null, null, null, null, this.zzcgg.zzclc, this.zzcgf.zzarm, this.zzcgg.zzcla, this.zzcgf.zzcso, this.zzcgg.zzclf, this.zzcgg.zzclg, this.zzcgf.zzcsi, null, null, null, null, this.zzcgf.zzcsu.zzclt, this.zzcgf.zzcsu.zzclu, null, null, this.zzcgg.zzclx);
  }
  
  public void onStop()
  {
    synchronized (this.zzako)
    {
      if (this.zzchh != null) {
        this.zzchh.cancel(true);
      }
      return;
    }
  }
  
  /* Error */
  public void zzfp()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 42	com/google/android/gms/internal/zziz:zzako	Ljava/lang/Object;
    //   4: astore_2
    //   5: aload_2
    //   6: monitorenter
    //   7: aload_0
    //   8: aload_0
    //   9: getfield 55	com/google/android/gms/internal/zziz:zzchg	Lcom/google/android/gms/internal/zzjb;
    //   12: invokestatic 145	com/google/android/gms/internal/zzla:zza	(Ljava/util/concurrent/Callable;)Lcom/google/android/gms/internal/zzlt;
    //   15: putfield 125	com/google/android/gms/internal/zziz:zzchh	Ljava/util/concurrent/Future;
    //   18: aload_2
    //   19: monitorexit
    //   20: aload_0
    //   21: getfield 125	com/google/android/gms/internal/zziz:zzchh	Ljava/util/concurrent/Future;
    //   24: ldc2_w 146
    //   27: getstatic 153	java/util/concurrent/TimeUnit:MILLISECONDS	Ljava/util/concurrent/TimeUnit;
    //   30: invokeinterface 157 4 0
    //   35: checkcast 61	com/google/android/gms/internal/zzko
    //   38: astore_2
    //   39: bipush -2
    //   41: istore_1
    //   42: aload_2
    //   43: ifnull +73 -> 116
    //   46: getstatic 163	com/google/android/gms/internal/zzlb:zzcvl	Landroid/os/Handler;
    //   49: new 165	com/google/android/gms/internal/zziz$1
    //   52: dup
    //   53: aload_0
    //   54: aload_2
    //   55: invokespecial 168	com/google/android/gms/internal/zziz$1:<init>	(Lcom/google/android/gms/internal/zziz;Lcom/google/android/gms/internal/zzko;)V
    //   58: invokevirtual 174	android/os/Handler:post	(Ljava/lang/Runnable;)Z
    //   61: pop
    //   62: return
    //   63: astore_3
    //   64: aload_2
    //   65: monitorexit
    //   66: aload_3
    //   67: athrow
    //   68: astore_2
    //   69: ldc -80
    //   71: invokestatic 182	com/google/android/gms/internal/zzkx:zzdi	(Ljava/lang/String;)V
    //   74: aload_0
    //   75: getfield 125	com/google/android/gms/internal/zziz:zzchh	Ljava/util/concurrent/Future;
    //   78: iconst_1
    //   79: invokeinterface 131 2 0
    //   84: pop
    //   85: iconst_2
    //   86: istore_1
    //   87: aconst_null
    //   88: astore_2
    //   89: goto -47 -> 42
    //   92: astore_2
    //   93: aconst_null
    //   94: astore_2
    //   95: iconst_0
    //   96: istore_1
    //   97: goto -55 -> 42
    //   100: astore_2
    //   101: aconst_null
    //   102: astore_2
    //   103: iconst_0
    //   104: istore_1
    //   105: goto -63 -> 42
    //   108: astore_2
    //   109: aconst_null
    //   110: astore_2
    //   111: iconst_0
    //   112: istore_1
    //   113: goto -71 -> 42
    //   116: aload_0
    //   117: iload_1
    //   118: invokespecial 184	com/google/android/gms/internal/zziz:zzaq	(I)Lcom/google/android/gms/internal/zzko;
    //   121: astore_2
    //   122: goto -76 -> 46
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	125	0	this	zziz
    //   41	77	1	i	int
    //   68	1	2	localTimeoutException	java.util.concurrent.TimeoutException
    //   88	1	2	localObject2	Object
    //   92	1	2	localExecutionException	java.util.concurrent.ExecutionException
    //   94	1	2	localObject3	Object
    //   100	1	2	localInterruptedException	InterruptedException
    //   102	1	2	localObject4	Object
    //   108	1	2	localCancellationException	java.util.concurrent.CancellationException
    //   110	12	2	localzzko	zzko
    //   63	4	3	localObject5	Object
    // Exception table:
    //   from	to	target	type
    //   7	20	63	finally
    //   64	66	63	finally
    //   0	7	68	java/util/concurrent/TimeoutException
    //   20	39	68	java/util/concurrent/TimeoutException
    //   66	68	68	java/util/concurrent/TimeoutException
    //   0	7	92	java/util/concurrent/ExecutionException
    //   20	39	92	java/util/concurrent/ExecutionException
    //   66	68	92	java/util/concurrent/ExecutionException
    //   0	7	100	java/lang/InterruptedException
    //   20	39	100	java/lang/InterruptedException
    //   66	68	100	java/lang/InterruptedException
    //   0	7	108	java/util/concurrent/CancellationException
    //   20	39	108	java/util/concurrent/CancellationException
    //   66	68	108	java/util/concurrent/CancellationException
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zziz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */