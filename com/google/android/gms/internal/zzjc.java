package com.google.android.gms.internal;

import android.content.Context;
import android.view.View;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.ViewTreeObserver.OnScrollChangedListener;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.client.zzm;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.ads.internal.zzu;
import java.lang.ref.WeakReference;
import org.json.JSONObject;

@zzji
public class zzjc
{
  private final Context mContext;
  private final Object zzako = new Object();
  private final zzdz zzalt;
  private int zzasl = -1;
  private int zzasm = -1;
  private zzlm zzasn;
  private final zzq zzbnr;
  private final zzav zzbnx;
  private final zzko.zza zzcgf;
  private ViewTreeObserver.OnGlobalLayoutListener zzcir;
  private ViewTreeObserver.OnScrollChangedListener zzcis;
  
  public zzjc(Context paramContext, zzav paramzzav, zzko.zza paramzza, zzdz paramzzdz, zzq paramzzq)
  {
    this.mContext = paramContext;
    this.zzbnx = paramzzav;
    this.zzcgf = paramzza;
    this.zzalt = paramzzdz;
    this.zzbnr = paramzzq;
    this.zzasn = new zzlm(200L);
  }
  
  private ViewTreeObserver.OnGlobalLayoutListener zza(WeakReference<zzmd> paramWeakReference)
  {
    if (this.zzcir == null) {
      this.zzcir = new zzjc.3(this, paramWeakReference);
    }
    return this.zzcir;
  }
  
  private void zza(WeakReference<zzmd> paramWeakReference, boolean paramBoolean)
  {
    if (paramWeakReference == null) {}
    do
    {
      return;
      paramWeakReference = (zzmd)paramWeakReference.get();
    } while ((paramWeakReference == null) || (paramWeakReference.getView() == null) || ((paramBoolean) && (!this.zzasn.tryAcquire())));
    ??? = paramWeakReference.getView();
    int[] arrayOfInt = new int[2];
    ((View)???).getLocationOnScreen(arrayOfInt);
    int i = zzm.zzkr().zzc(this.mContext, arrayOfInt[0]);
    int j = zzm.zzkr().zzc(this.mContext, arrayOfInt[1]);
    for (;;)
    {
      synchronized (this.zzako)
      {
        if ((this.zzasl != i) || (this.zzasm != j))
        {
          this.zzasl = i;
          this.zzasm = j;
          paramWeakReference = paramWeakReference.zzxc();
          i = this.zzasl;
          j = this.zzasm;
          if (!paramBoolean)
          {
            paramBoolean = true;
            paramWeakReference.zza(i, j, paramBoolean);
          }
        }
        else
        {
          return;
        }
      }
      paramBoolean = false;
    }
  }
  
  private ViewTreeObserver.OnScrollChangedListener zzb(WeakReference<zzmd> paramWeakReference)
  {
    if (this.zzcis == null) {
      this.zzcis = new zzjc.4(this, paramWeakReference);
    }
    return this.zzcis;
  }
  
  private void zzj(zzmd paramzzmd)
  {
    paramzzmd = paramzzmd.zzxc();
    paramzzmd.zza("/video", zzfd.zzbpw);
    paramzzmd.zza("/videoMeta", zzfd.zzbpx);
    paramzzmd.zza("/precache", zzfd.zzbpy);
    paramzzmd.zza("/delayPageLoaded", zzfd.zzbqb);
    paramzzmd.zza("/instrument", zzfd.zzbpz);
    paramzzmd.zza("/log", zzfd.zzbpr);
    paramzzmd.zza("/videoClicked", zzfd.zzbps);
    paramzzmd.zza("/trackActiveViewUnit", new zzjc.2(this));
  }
  
  public zzlt<zzmd> zzg(JSONObject paramJSONObject)
  {
    zzlq localzzlq = new zzlq();
    zzu.zzgm().runOnUiThread(new zzjc.1(this, paramJSONObject, localzzlq));
    return localzzlq;
  }
  
  zzmd zzsx()
  {
    return zzu.zzgn().zza(this.mContext, AdSizeParcel.zzj(this.mContext), false, false, this.zzbnx, this.zzcgf.zzcmx.zzari, this.zzalt, null, this.zzbnr.zzec());
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzjc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */