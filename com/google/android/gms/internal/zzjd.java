package com.google.android.gms.internal;

import android.os.Bundle;
import com.google.android.gms.ads.internal.formats.zza;
import com.google.android.gms.ads.internal.formats.zzc;
import com.google.android.gms.ads.internal.formats.zzd;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.json.JSONException;
import org.json.JSONObject;

@zzji
public class zzjd
  implements zzjb.zza<zzd>
{
  private final boolean zzciy;
  private final boolean zzciz;
  
  public zzjd(boolean paramBoolean1, boolean paramBoolean2)
  {
    this.zzciy = paramBoolean1;
    this.zzciz = paramBoolean2;
  }
  
  private zzmd zzb(zzlt<zzmd> paramzzlt)
  {
    try
    {
      paramzzlt = (zzmd)paramzzlt.get(((Integer)zzdr.zzbje.get()).intValue(), TimeUnit.SECONDS);
      return paramzzlt;
    }
    catch (InterruptedException paramzzlt)
    {
      zzkx.zzc("InterruptedException occurred while waiting for video to load", paramzzlt);
      Thread.currentThread().interrupt();
      return null;
    }
    catch (ExecutionException paramzzlt)
    {
      for (;;)
      {
        zzkx.zzc("Exception occurred while waiting for video to load", paramzzlt);
      }
    }
    catch (CancellationException paramzzlt)
    {
      for (;;) {}
    }
    catch (TimeoutException paramzzlt)
    {
      for (;;) {}
    }
  }
  
  public zzd zzb(zzjb paramzzjb, JSONObject paramJSONObject)
    throws JSONException, InterruptedException, ExecutionException
  {
    Object localObject2 = paramzzjb.zza(paramJSONObject, "images", true, this.zzciy, this.zzciz);
    Object localObject3 = paramzzjb.zza(paramJSONObject, "app_icon", true, this.zzciy);
    Object localObject1 = paramzzjb.zzc(paramJSONObject, "video");
    paramzzjb = paramzzjb.zzf(paramJSONObject);
    ArrayList localArrayList = new ArrayList();
    localObject2 = ((List)localObject2).iterator();
    while (((Iterator)localObject2).hasNext()) {
      localArrayList.add((zzc)((zzlt)((Iterator)localObject2).next()).get());
    }
    zzmd localzzmd = zzb((zzlt)localObject1);
    localObject1 = paramJSONObject.getString("headline");
    localObject2 = paramJSONObject.getString("body");
    localObject3 = (zzeg)((Future)localObject3).get();
    String str1 = paramJSONObject.getString("call_to_action");
    double d = paramJSONObject.optDouble("rating", -1.0D);
    String str2 = paramJSONObject.optString("store");
    String str3 = paramJSONObject.optString("price");
    zza localzza = (zza)paramzzjb.get();
    Bundle localBundle = new Bundle();
    if (localzzmd != null)
    {
      paramzzjb = localzzmd.zzxn();
      if (localzzmd == null) {
        break label254;
      }
    }
    label254:
    for (paramJSONObject = localzzmd.getView();; paramJSONObject = null)
    {
      return new zzd((String)localObject1, localArrayList, (String)localObject2, (zzeg)localObject3, str1, d, str2, str3, localzza, localBundle, paramzzjb, paramJSONObject);
      paramzzjb = null;
      break;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzjd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */