package com.google.android.gms.internal;

import android.os.Bundle;
import com.google.android.gms.ads.internal.formats.zza;
import com.google.android.gms.ads.internal.formats.zzc;
import com.google.android.gms.ads.internal.formats.zze;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import org.json.JSONException;
import org.json.JSONObject;

@zzji
public class zzje
  implements zzjb.zza<zze>
{
  private final boolean zzciy;
  private final boolean zzciz;
  
  public zzje(boolean paramBoolean1, boolean paramBoolean2)
  {
    this.zzciy = paramBoolean1;
    this.zzciz = paramBoolean2;
  }
  
  public zze zzc(zzjb paramzzjb, JSONObject paramJSONObject)
    throws JSONException, InterruptedException, ExecutionException
  {
    Object localObject = paramzzjb.zza(paramJSONObject, "images", true, this.zzciy, this.zzciz);
    zzlt localzzlt = paramzzjb.zza(paramJSONObject, "secondary_image", false, this.zzciy);
    paramzzjb = paramzzjb.zzf(paramJSONObject);
    ArrayList localArrayList = new ArrayList();
    localObject = ((List)localObject).iterator();
    while (((Iterator)localObject).hasNext()) {
      localArrayList.add((zzc)((zzlt)((Iterator)localObject).next()).get());
    }
    return new zze(paramJSONObject.getString("headline"), localArrayList, paramJSONObject.getString("body"), (zzeg)localzzlt.get(), paramJSONObject.getString("call_to_action"), paramJSONObject.getString("advertiser"), (zza)paramzzjb.get(), new Bundle());
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzje.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */