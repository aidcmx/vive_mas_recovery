package com.google.android.gms.internal;

import android.content.Context;
import android.net.Uri.Builder;
import android.os.Build.VERSION;
import android.os.Looper;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zzu;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.WeakHashMap;

@zzji
public class zzjg
  implements zzjh
{
  private static final Object zzaox = new Object();
  private static zzjh zzcja = null;
  private final VersionInfoParcel zzapc;
  private final Object zzcjb = new Object();
  private final String zzcjc;
  private final WeakHashMap<Thread, Boolean> zzcjd = new WeakHashMap();
  
  zzjg(String paramString, VersionInfoParcel paramVersionInfoParcel)
  {
    this.zzcjc = paramString;
    this.zzapc = paramVersionInfoParcel;
    zzsz();
    zzsy();
  }
  
  public static zzjh zzb(Context paramContext, VersionInfoParcel paramVersionInfoParcel)
  {
    for (;;)
    {
      synchronized (zzaox)
      {
        String str;
        if (zzcja == null)
        {
          if (((Boolean)zzdr.zzbdc.get()).booleanValue()) {
            str = "unknown";
          }
        }
        else {
          try
          {
            paramContext = paramContext.getApplicationContext().getPackageName();
            zzcja = new zzjg(paramContext, paramVersionInfoParcel);
            return zzcja;
          }
          catch (Throwable paramContext)
          {
            zzkx.zzdi("Cannot obtain package name, proceeding.");
            paramContext = str;
            continue;
          }
        }
      }
      zzcja = new zzjh.zza();
    }
  }
  
  private Throwable zzd(Throwable paramThrowable)
  {
    if (((Boolean)zzdr.zzbdd.get()).booleanValue()) {
      return paramThrowable;
    }
    LinkedList localLinkedList = new LinkedList();
    while (paramThrowable != null)
    {
      localLinkedList.push(paramThrowable);
      paramThrowable = paramThrowable.getCause();
    }
    paramThrowable = null;
    Throwable localThrowable;
    if (!localLinkedList.isEmpty())
    {
      localThrowable = (Throwable)localLinkedList.pop();
      StackTraceElement[] arrayOfStackTraceElement = localThrowable.getStackTrace();
      ArrayList localArrayList = new ArrayList();
      localArrayList.add(new StackTraceElement(localThrowable.getClass().getName(), "<filtered>", "<filtered>", 1));
      int k = arrayOfStackTraceElement.length;
      int i = 0;
      int j = 0;
      if (i < k)
      {
        StackTraceElement localStackTraceElement = arrayOfStackTraceElement[i];
        if (zzck(localStackTraceElement.getClassName()))
        {
          localArrayList.add(localStackTraceElement);
          j = 1;
        }
        for (;;)
        {
          i += 1;
          break;
          if (zzcl(localStackTraceElement.getClassName())) {
            localArrayList.add(localStackTraceElement);
          } else {
            localArrayList.add(new StackTraceElement("<filtered>", "<filtered>", "<filtered>", 1));
          }
        }
      }
      if (j == 0) {
        break label261;
      }
      if (paramThrowable == null)
      {
        paramThrowable = new Throwable(localThrowable.getMessage());
        label223:
        paramThrowable.setStackTrace((StackTraceElement[])localArrayList.toArray(new StackTraceElement[0]));
      }
    }
    label261:
    for (;;)
    {
      break;
      paramThrowable = new Throwable(localThrowable.getMessage(), paramThrowable);
      break label223;
      return paramThrowable;
    }
  }
  
  private void zzsy()
  {
    Thread.setDefaultUncaughtExceptionHandler(new zzjg.1(this, Thread.getDefaultUncaughtExceptionHandler()));
  }
  
  private void zzsz()
  {
    zza(Looper.getMainLooper().getThread());
  }
  
  String zza(Class paramClass, Throwable paramThrowable, String paramString)
  {
    StringWriter localStringWriter = new StringWriter();
    paramThrowable.printStackTrace(new PrintWriter(localStringWriter));
    return new Uri.Builder().scheme("https").path("//pagead2.googlesyndication.com/pagead/gen_204").appendQueryParameter("id", "gmob-apps-report-exception").appendQueryParameter("os", Build.VERSION.RELEASE).appendQueryParameter("api", String.valueOf(Build.VERSION.SDK_INT)).appendQueryParameter("device", zzu.zzgm().zzvt()).appendQueryParameter("js", this.zzapc.zzda).appendQueryParameter("appid", this.zzcjc).appendQueryParameter("exceptiontype", paramClass.getName()).appendQueryParameter("stacktrace", localStringWriter.toString()).appendQueryParameter("eids", TextUtils.join(",", zzdr.zzlq())).appendQueryParameter("exceptionkey", paramString).appendQueryParameter("cl", "135396225").appendQueryParameter("rc", "dev").toString();
  }
  
  public void zza(Thread paramThread)
  {
    if (paramThread == null) {
      return;
    }
    synchronized (this.zzcjb)
    {
      this.zzcjd.put(paramThread, Boolean.valueOf(true));
      paramThread.setUncaughtExceptionHandler(new zzjg.2(this, paramThread.getUncaughtExceptionHandler()));
      return;
    }
  }
  
  protected void zza(Thread paramThread, Throwable paramThrowable)
  {
    if (zzb(paramThrowable)) {
      zzc(paramThrowable);
    }
  }
  
  public void zza(Throwable paramThrowable, String paramString)
  {
    Throwable localThrowable = zzd(paramThrowable);
    if (localThrowable == null) {
      return;
    }
    paramThrowable = paramThrowable.getClass();
    ArrayList localArrayList = new ArrayList();
    localArrayList.add(zza(paramThrowable, localThrowable, paramString));
    zzu.zzgm().zza(localArrayList, zzu.zzgq().zzux());
  }
  
  protected boolean zzb(Throwable paramThrowable)
  {
    boolean bool = true;
    if (paramThrowable == null) {
      return false;
    }
    int j = 0;
    int k = 0;
    while (paramThrowable != null)
    {
      StackTraceElement[] arrayOfStackTraceElement = paramThrowable.getStackTrace();
      int m = arrayOfStackTraceElement.length;
      int i = 0;
      while (i < m)
      {
        StackTraceElement localStackTraceElement = arrayOfStackTraceElement[i];
        if (zzck(localStackTraceElement.getClassName())) {
          k = 1;
        }
        if (getClass().getName().equals(localStackTraceElement.getClassName())) {
          j = 1;
        }
        i += 1;
      }
      paramThrowable = paramThrowable.getCause();
    }
    if ((k != 0) && (j == 0)) {}
    for (;;)
    {
      return bool;
      bool = false;
    }
  }
  
  public void zzc(Throwable paramThrowable)
  {
    zza(paramThrowable, "");
  }
  
  protected boolean zzck(String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {
      return false;
    }
    if (paramString.startsWith((String)zzdr.zzbde.get())) {
      return true;
    }
    try
    {
      boolean bool = Class.forName(paramString).isAnnotationPresent(zzji.class);
      return bool;
    }
    catch (Exception localException)
    {
      paramString = String.valueOf(paramString);
      if (paramString.length() == 0) {}
    }
    for (paramString = "Fail to check class type for class ".concat(paramString);; paramString = new String("Fail to check class type for class "))
    {
      zzkx.zza(paramString, localException);
      return false;
    }
  }
  
  protected boolean zzcl(String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {}
    while ((!paramString.startsWith("android.")) && (!paramString.startsWith("java."))) {
      return false;
    }
    return true;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzjg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */