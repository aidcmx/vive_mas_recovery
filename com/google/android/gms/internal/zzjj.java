package com.google.android.gms.internal;

import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;

@zzji
public class zzjj
{
  @Nullable
  public Location zzayt;
  @Nullable
  public String zzcjw;
  @Nullable
  public Bundle zzckb;
  @Nullable
  public List<String> zzckj = new ArrayList();
  @Nullable
  public Bundle zzcmu;
  @Nullable
  public zzjv.zza zzcmv;
  @Nullable
  public String zzcmw;
  public AdRequestInfoParcel zzcmx;
  public zzjr zzcmy;
  public JSONObject zzcmz = new JSONObject();
  
  public zzjj zza(zzjr paramzzjr)
  {
    this.zzcmy = paramzzjr;
    return this;
  }
  
  public zzjj zza(zzjv.zza paramzza)
  {
    this.zzcmv = paramzza;
    return this;
  }
  
  public zzjj zzc(Location paramLocation)
  {
    this.zzayt = paramLocation;
    return this;
  }
  
  public zzjj zzcm(String paramString)
  {
    this.zzcjw = paramString;
    return this;
  }
  
  public zzjj zzcn(String paramString)
  {
    this.zzcmw = paramString;
    return this;
  }
  
  public zzjj zze(Bundle paramBundle)
  {
    this.zzcmu = paramBundle;
    return this;
  }
  
  public zzjj zzf(Bundle paramBundle)
  {
    this.zzckb = paramBundle;
    return this;
  }
  
  public zzjj zzf(AdRequestInfoParcel paramAdRequestInfoParcel)
  {
    this.zzcmx = paramAdRequestInfoParcel;
    return this;
  }
  
  public zzjj zzi(JSONObject paramJSONObject)
  {
    this.zzcmz = paramJSONObject;
    return this;
  }
  
  public zzjj zzk(List<String> paramList)
  {
    if (paramList == null) {
      this.zzckj.clear();
    }
    this.zzckj = paramList;
    return this;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzjj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */