package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.cache.zze;
import com.google.android.gms.ads.internal.cache.zzg;

@zzji
public class zzjk
{
  public final zzjp zzcna;
  public final zze zzcnb;
  public final zzkk zzcnc;
  public final zzdm zzcnd;
  public final zzjt zzcne;
  public final zzgm zzcnf;
  public final zzju zzcng;
  public final zzjv zzcnh;
  public final zzib zzcni;
  public final zzkn zzcnj;
  
  zzjk(zzjp paramzzjp, zze paramzze, zzkk paramzzkk, zzdm paramzzdm, zzjt paramzzjt, zzgm paramzzgm, zzju paramzzju, zzjv paramzzjv, zzib paramzzib, zzkn paramzzkn)
  {
    this.zzcna = paramzzjp;
    this.zzcnb = paramzze;
    this.zzcnc = paramzzkk;
    this.zzcnd = paramzzdm;
    this.zzcne = paramzzjt;
    this.zzcnf = paramzzgm;
    this.zzcng = paramzzju;
    this.zzcnh = paramzzjv;
    this.zzcni = paramzzib;
    this.zzcnj = paramzzkn;
  }
  
  public static zzjk zzti()
  {
    return new zzjk(null, new zzg(), new zzkl(), new zzdl(), new zzjw(), new zzgn(), new zzjx(), new zzjy(), new zzia(), new zzkm());
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzjk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */