package com.google.android.gms.internal;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.cache.zze;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import com.google.android.gms.ads.internal.request.zzk.zza;
import com.google.android.gms.ads.internal.request.zzl;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zzu;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.json.JSONException;
import org.json.JSONObject;

@zzji
public final class zzjl
  extends zzk.zza
{
  private static final Object zzaox = new Object();
  private static zzjl zzcnk;
  private final Context mContext;
  private final zzjk zzcnl;
  private final zzdk zzcnm;
  private final zzgh zzcnn;
  
  zzjl(Context paramContext, zzdk paramzzdk, zzjk paramzzjk)
  {
    this.mContext = paramContext;
    this.zzcnl = paramzzjk;
    this.zzcnm = paramzzdk;
    if (paramContext.getApplicationContext() != null) {
      paramContext = paramContext.getApplicationContext();
    }
    for (;;)
    {
      this.zzcnn = new zzgh(paramContext, VersionInfoParcel.zzwr(), paramzzdk.zzlo(), new zzjl.4(this), new zzgh.zzb());
      return;
    }
  }
  
  private static AdResponseParcel zza(Context paramContext, zzgh paramzzgh, zzdk paramzzdk, zzjk paramzzjk, AdRequestInfoParcel paramAdRequestInfoParcel)
  {
    zzkx.zzdg("Starting ad request from service using: AFMA_getAd");
    zzdr.initialize(paramContext);
    zzlt localzzlt = paramzzjk.zzcni.zzrl();
    zzdz localzzdz = new zzdz(((Boolean)zzdr.zzbeq.get()).booleanValue(), "load_ad", paramAdRequestInfoParcel.zzarm.zzazq);
    if ((paramAdRequestInfoParcel.versionCode > 10) && (paramAdRequestInfoParcel.zzckl != -1L)) {
      localzzdz.zza(localzzdz.zzc(paramAdRequestInfoParcel.zzckl), new String[] { "cts" });
    }
    zzdx localzzdx = localzzdz.zzlz();
    if ((paramAdRequestInfoParcel.versionCode >= 4) && (paramAdRequestInfoParcel.zzckb != null)) {}
    label1033:
    Object localObject3;
    for (Object localObject1 = paramAdRequestInfoParcel.zzckb;; localObject3 = null)
    {
      if ((((Boolean)zzdr.zzbfh.get()).booleanValue()) && (paramzzjk.zzcna != null))
      {
        paramzzdk = (zzdk)localObject1;
        if (localObject1 == null)
        {
          paramzzdk = (zzdk)localObject1;
          if (((Boolean)zzdr.zzbfi.get()).booleanValue())
          {
            zzkx.v("contentInfo is not present, but we'll still launch the app index task");
            paramzzdk = new Bundle();
          }
        }
        if (paramzzdk != null) {
          localObject1 = zzla.zza(new zzjl.1(paramzzjk, paramContext, paramAdRequestInfoParcel, paramzzdk));
        }
      }
      for (;;)
      {
        Object localObject4 = new zzlr(null);
        Object localObject5 = paramAdRequestInfoParcel.zzcju.extras;
        int i;
        if ((localObject5 != null) && (((Bundle)localObject5).getString("_ad") != null))
        {
          i = 1;
          if ((!paramAdRequestInfoParcel.zzcks) || (i != 0)) {
            break label1033;
          }
          localObject4 = paramzzjk.zzcnf.zza(paramAdRequestInfoParcel.applicationInfo);
        }
        for (;;)
        {
          zzjr localzzjr = zzu.zzgv().zzv(paramContext);
          if (localzzjr.zzcqe == -1)
          {
            zzkx.zzdg("Device is offline.");
            return new AdResponseParcel(2);
            i = 0;
            break;
          }
          if (paramAdRequestInfoParcel.versionCode >= 7) {}
          zzjn localzzjn;
          for (localObject5 = paramAdRequestInfoParcel.zzcki;; localObject5 = UUID.randomUUID().toString())
          {
            localzzjn = new zzjn((String)localObject5, paramAdRequestInfoParcel.applicationInfo.packageName);
            if (paramAdRequestInfoParcel.zzcju.extras == null) {
              break;
            }
            localObject6 = paramAdRequestInfoParcel.zzcju.extras.getString("_ad");
            if (localObject6 == null) {
              break;
            }
            return zzjm.zza(paramContext, paramAdRequestInfoParcel, (String)localObject6);
          }
          Object localObject6 = paramzzjk.zzcnd.zza(paramAdRequestInfoParcel);
          String str = paramzzjk.zzcnj.zzg(paramAdRequestInfoParcel);
          zzjv localzzjv = paramzzjk.zzcnh;
          if (localObject1 != null) {}
          try
          {
            zzkx.v("Waiting for app index fetching task.");
            ((Future)localObject1).get(((Long)zzdr.zzbfj.get()).longValue(), TimeUnit.MILLISECONDS);
            zzkx.v("App index fetching task completed.");
            localObject1 = paramzzjk.zzcnc;
            localObject1 = paramAdRequestInfoParcel.zzcjv.packageName;
            zzd(localzzlt);
            paramzzdk = zzjm.zza(paramContext, new zzjj().zzf(paramAdRequestInfoParcel).zza(localzzjr).zza(null).zzc(zzc((zzlt)localObject4)).zze(zzd(localzzlt)).zzcm(str).zzk((List)localObject6).zzf(paramzzdk).zzcn(null).zzi(paramzzjk.zzcnb.zzi(paramContext)));
            if (paramzzdk == null) {
              return new AdResponseParcel(0);
            }
          }
          catch (InterruptedException localInterruptedException)
          {
            for (;;)
            {
              zzkx.zzc("Failed to fetch app index signal", localInterruptedException);
            }
          }
          catch (TimeoutException localTimeoutException)
          {
            for (;;)
            {
              zzkx.zzdg("Timed out waiting for app index fetching task");
            }
            if (paramAdRequestInfoParcel.versionCode < 7) {}
            try
            {
              paramzzdk.put("request_id", localObject5);
              try
              {
                paramzzdk.put("prefetch_mode", "url");
                paramzzdk = paramzzdk.toString();
                localzzdz.zza(localzzdx, new String[] { "arc" });
                localObject2 = localzzdz.zzlz();
                zzlb.zzcvl.post(new zzjl.2(paramzzgh, localzzjn, localzzdz, (zzdx)localObject2, paramzzdk));
              }
              catch (JSONException localJSONException1)
              {
                for (;;)
                {
                  try
                  {
                    Object localObject2 = (zzjq)localzzjn.zztk().get(10L, TimeUnit.SECONDS);
                    if (localObject2 != null) {
                      continue;
                    }
                    paramzzgh = new AdResponseParcel(0);
                    return paramzzgh;
                  }
                  catch (Exception paramzzgh)
                  {
                    paramzzgh = new AdResponseParcel(0);
                    return paramzzgh;
                    if (localJSONException1.getErrorCode() == -2) {
                      continue;
                    }
                    paramzzgh = new AdResponseParcel(localJSONException1.getErrorCode());
                    return paramzzgh;
                    if (localzzdz.zzmd() == null) {
                      continue;
                    }
                    localzzdz.zza(localzzdz.zzmd(), new String[] { "rur" });
                    paramzzdk = null;
                    if (TextUtils.isEmpty(localJSONException1.zztq())) {
                      continue;
                    }
                    paramzzdk = zzjm.zza(paramContext, paramAdRequestInfoParcel, localJSONException1.zztq());
                    paramzzgh = paramzzdk;
                    if (paramzzdk != null) {
                      continue;
                    }
                    paramzzgh = paramzzdk;
                    if (TextUtils.isEmpty(localJSONException1.getUrl())) {
                      continue;
                    }
                    paramzzgh = zza(paramAdRequestInfoParcel, paramContext, paramAdRequestInfoParcel.zzari.zzda, localJSONException1.getUrl(), null, localJSONException1, localzzdz, paramzzjk);
                    paramzzdk = paramzzgh;
                    if (paramzzgh != null) {
                      continue;
                    }
                    paramzzdk = new AdResponseParcel(0);
                    localzzdz.zza(localzzdx, new String[] { "tts" });
                    paramzzdk.zzclo = localzzdz.zzmb();
                    return paramzzdk;
                  }
                  finally
                  {
                    zzlb.zzcvl.post(new zzjl.3(paramzzjk, paramContext, localzzjn, paramAdRequestInfoParcel));
                  }
                  localJSONException1 = localJSONException1;
                  zzkx.zzc("Failed putting prefetch parameters to ad request.", localJSONException1);
                }
              }
            }
            catch (JSONException localJSONException2)
            {
              for (;;) {}
            }
          }
          catch (ExecutionException localExecutionException)
          {
            for (;;) {}
          }
        }
        localObject3 = null;
        continue;
        paramzzdk = (zzdk)localObject3;
        localObject3 = null;
      }
    }
  }
  
  /* Error */
  public static AdResponseParcel zza(AdRequestInfoParcel paramAdRequestInfoParcel, Context paramContext, String paramString1, String paramString2, String paramString3, zzjq paramzzjq, zzdz paramzzdz, zzjk paramzzjk)
  {
    // Byte code:
    //   0: aload 6
    //   2: ifnull +711 -> 713
    //   5: aload 6
    //   7: invokevirtual 166	com/google/android/gms/internal/zzdz:zzlz	()Lcom/google/android/gms/internal/zzdx;
    //   10: astore 12
    //   12: new 500	com/google/android/gms/internal/zzjo
    //   15: dup
    //   16: aload_0
    //   17: aload 5
    //   19: invokevirtual 503	com/google/android/gms/internal/zzjq:zztn	()Ljava/lang/String;
    //   22: invokespecial 506	com/google/android/gms/internal/zzjo:<init>	(Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;Ljava/lang/String;)V
    //   25: astore 15
    //   27: aload_3
    //   28: invokestatic 510	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   31: astore 13
    //   33: aload 13
    //   35: invokevirtual 513	java/lang/String:length	()I
    //   38: ifeq +329 -> 367
    //   41: ldc_w 515
    //   44: aload 13
    //   46: invokevirtual 518	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   49: astore 13
    //   51: aload 13
    //   53: invokestatic 88	com/google/android/gms/internal/zzkx:zzdg	(Ljava/lang/String;)V
    //   56: new 520	java/net/URL
    //   59: dup
    //   60: aload_3
    //   61: invokespecial 522	java/net/URL:<init>	(Ljava/lang/String;)V
    //   64: astore_3
    //   65: invokestatic 526	com/google/android/gms/ads/internal/zzu:zzgs	()Lcom/google/android/gms/common/util/zze;
    //   68: invokeinterface 531 1 0
    //   73: lstore 10
    //   75: iconst_0
    //   76: istore 8
    //   78: aload 7
    //   80: ifnull +13 -> 93
    //   83: aload 7
    //   85: getfield 535	com/google/android/gms/internal/zzjk:zzcng	Lcom/google/android/gms/internal/zzju;
    //   88: invokeinterface 540 1 0
    //   93: aload_3
    //   94: invokevirtual 544	java/net/URL:openConnection	()Ljava/net/URLConnection;
    //   97: checkcast 546	java/net/HttpURLConnection
    //   100: astore 14
    //   102: invokestatic 550	com/google/android/gms/ads/internal/zzu:zzgm	()Lcom/google/android/gms/internal/zzlb;
    //   105: aload_1
    //   106: aload_2
    //   107: iconst_0
    //   108: aload 14
    //   110: invokevirtual 553	com/google/android/gms/internal/zzlb:zza	(Landroid/content/Context;Ljava/lang/String;ZLjava/net/HttpURLConnection;)V
    //   113: aload 4
    //   115: invokestatic 475	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   118: ifne +21 -> 139
    //   121: aload 5
    //   123: invokevirtual 556	com/google/android/gms/internal/zzjq:zztp	()Z
    //   126: ifeq +13 -> 139
    //   129: aload 14
    //   131: ldc_w 558
    //   134: aload 4
    //   136: invokevirtual 561	java/net/HttpURLConnection:addRequestProperty	(Ljava/lang/String;Ljava/lang/String;)V
    //   139: aload_0
    //   140: getfield 564	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:zzckt	Ljava/lang/String;
    //   143: astore 13
    //   145: aload 13
    //   147: invokestatic 475	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   150: ifne +19 -> 169
    //   153: ldc_w 566
    //   156: invokestatic 88	com/google/android/gms/internal/zzkx:zzdg	(Ljava/lang/String;)V
    //   159: aload 14
    //   161: ldc_w 568
    //   164: aload 13
    //   166: invokevirtual 561	java/net/HttpURLConnection:addRequestProperty	(Ljava/lang/String;Ljava/lang/String;)V
    //   169: aload 5
    //   171: ifnull +64 -> 235
    //   174: aload 5
    //   176: invokevirtual 571	com/google/android/gms/internal/zzjq:zzto	()Ljava/lang/String;
    //   179: invokestatic 475	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   182: ifne +53 -> 235
    //   185: aload 14
    //   187: iconst_1
    //   188: invokevirtual 575	java/net/HttpURLConnection:setDoOutput	(Z)V
    //   191: aload 5
    //   193: invokevirtual 571	com/google/android/gms/internal/zzjq:zzto	()Ljava/lang/String;
    //   196: invokevirtual 579	java/lang/String:getBytes	()[B
    //   199: astore 16
    //   201: aload 14
    //   203: aload 16
    //   205: arraylength
    //   206: invokevirtual 582	java/net/HttpURLConnection:setFixedLengthStreamingMode	(I)V
    //   209: new 584	java/io/BufferedOutputStream
    //   212: dup
    //   213: aload 14
    //   215: invokevirtual 588	java/net/HttpURLConnection:getOutputStream	()Ljava/io/OutputStream;
    //   218: invokespecial 591	java/io/BufferedOutputStream:<init>	(Ljava/io/OutputStream;)V
    //   221: astore 13
    //   223: aload 13
    //   225: aload 16
    //   227: invokevirtual 595	java/io/BufferedOutputStream:write	([B)V
    //   230: aload 13
    //   232: invokestatic 601	com/google/android/gms/common/util/zzo:zzb	(Ljava/io/Closeable;)V
    //   235: aload 14
    //   237: invokevirtual 604	java/net/HttpURLConnection:getResponseCode	()I
    //   240: istore 9
    //   242: aload 14
    //   244: invokevirtual 608	java/net/HttpURLConnection:getHeaderFields	()Ljava/util/Map;
    //   247: astore 13
    //   249: iload 9
    //   251: sipush 200
    //   254: if_icmplt +206 -> 460
    //   257: iload 9
    //   259: sipush 300
    //   262: if_icmpge +198 -> 460
    //   265: aload_3
    //   266: invokevirtual 609	java/net/URL:toString	()Ljava/lang/String;
    //   269: astore_0
    //   270: new 611	java/io/InputStreamReader
    //   273: dup
    //   274: aload 14
    //   276: invokevirtual 615	java/net/HttpURLConnection:getInputStream	()Ljava/io/InputStream;
    //   279: invokespecial 618	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;)V
    //   282: astore_1
    //   283: invokestatic 550	com/google/android/gms/ads/internal/zzu:zzgm	()Lcom/google/android/gms/internal/zzlb;
    //   286: aload_1
    //   287: invokevirtual 621	com/google/android/gms/internal/zzlb:zza	(Ljava/io/InputStreamReader;)Ljava/lang/String;
    //   290: astore_2
    //   291: aload_1
    //   292: invokestatic 601	com/google/android/gms/common/util/zzo:zzb	(Ljava/io/Closeable;)V
    //   295: aload_0
    //   296: aload 13
    //   298: aload_2
    //   299: iload 9
    //   301: invokestatic 624	com/google/android/gms/internal/zzjl:zza	(Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;I)V
    //   304: aload 15
    //   306: aload_0
    //   307: aload 13
    //   309: aload_2
    //   310: invokevirtual 627	com/google/android/gms/internal/zzjo:zzb	(Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;)V
    //   313: aload 6
    //   315: ifnull +21 -> 336
    //   318: aload 6
    //   320: aload 12
    //   322: iconst_1
    //   323: anewarray 157	java/lang/String
    //   326: dup
    //   327: iconst_0
    //   328: ldc_w 629
    //   331: aastore
    //   332: invokevirtual 162	com/google/android/gms/internal/zzdz:zza	(Lcom/google/android/gms/internal/zzdx;[Ljava/lang/String;)Z
    //   335: pop
    //   336: aload 15
    //   338: lload 10
    //   340: invokevirtual 633	com/google/android/gms/internal/zzjo:zzj	(J)Lcom/google/android/gms/ads/internal/request/AdResponseParcel;
    //   343: astore_0
    //   344: aload 14
    //   346: invokevirtual 636	java/net/HttpURLConnection:disconnect	()V
    //   349: aload 7
    //   351: ifnull +360 -> 711
    //   354: aload 7
    //   356: getfield 535	com/google/android/gms/internal/zzjk:zzcng	Lcom/google/android/gms/internal/zzju;
    //   359: invokeinterface 639 1 0
    //   364: goto +347 -> 711
    //   367: new 157	java/lang/String
    //   370: dup
    //   371: ldc_w 515
    //   374: invokespecial 640	java/lang/String:<init>	(Ljava/lang/String;)V
    //   377: astore 13
    //   379: goto -328 -> 51
    //   382: astore_0
    //   383: aload_0
    //   384: invokevirtual 643	java/io/IOException:getMessage	()Ljava/lang/String;
    //   387: invokestatic 510	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   390: astore_0
    //   391: aload_0
    //   392: invokevirtual 513	java/lang/String:length	()I
    //   395: ifeq +291 -> 686
    //   398: ldc_w 645
    //   401: aload_0
    //   402: invokevirtual 518	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   405: astore_0
    //   406: aload_0
    //   407: invokestatic 648	com/google/android/gms/internal/zzkx:zzdi	(Ljava/lang/String;)V
    //   410: new 256	com/google/android/gms/ads/internal/request/AdResponseParcel
    //   413: dup
    //   414: iconst_2
    //   415: invokespecial 259	com/google/android/gms/ads/internal/request/AdResponseParcel:<init>	(I)V
    //   418: areturn
    //   419: astore_0
    //   420: aconst_null
    //   421: astore_1
    //   422: aload_1
    //   423: invokestatic 601	com/google/android/gms/common/util/zzo:zzb	(Ljava/io/Closeable;)V
    //   426: aload_0
    //   427: athrow
    //   428: astore_0
    //   429: aload 14
    //   431: invokevirtual 636	java/net/HttpURLConnection:disconnect	()V
    //   434: aload 7
    //   436: ifnull +13 -> 449
    //   439: aload 7
    //   441: getfield 535	com/google/android/gms/internal/zzjk:zzcng	Lcom/google/android/gms/internal/zzju;
    //   444: invokeinterface 639 1 0
    //   449: aload_0
    //   450: athrow
    //   451: astore_0
    //   452: aconst_null
    //   453: astore_1
    //   454: aload_1
    //   455: invokestatic 601	com/google/android/gms/common/util/zzo:zzb	(Ljava/io/Closeable;)V
    //   458: aload_0
    //   459: athrow
    //   460: aload_3
    //   461: invokevirtual 609	java/net/URL:toString	()Ljava/lang/String;
    //   464: aload 13
    //   466: aconst_null
    //   467: iload 9
    //   469: invokestatic 624	com/google/android/gms/internal/zzjl:zza	(Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;I)V
    //   472: iload 9
    //   474: sipush 300
    //   477: if_icmplt +122 -> 599
    //   480: iload 9
    //   482: sipush 400
    //   485: if_icmpge +114 -> 599
    //   488: aload 14
    //   490: ldc_w 650
    //   493: invokevirtual 653	java/net/HttpURLConnection:getHeaderField	(Ljava/lang/String;)Ljava/lang/String;
    //   496: astore_3
    //   497: aload_3
    //   498: invokestatic 475	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   501: ifeq +40 -> 541
    //   504: ldc_w 655
    //   507: invokestatic 648	com/google/android/gms/internal/zzkx:zzdi	(Ljava/lang/String;)V
    //   510: new 256	com/google/android/gms/ads/internal/request/AdResponseParcel
    //   513: dup
    //   514: iconst_0
    //   515: invokespecial 259	com/google/android/gms/ads/internal/request/AdResponseParcel:<init>	(I)V
    //   518: astore_0
    //   519: aload 14
    //   521: invokevirtual 636	java/net/HttpURLConnection:disconnect	()V
    //   524: aload 7
    //   526: ifnull +13 -> 539
    //   529: aload 7
    //   531: getfield 535	com/google/android/gms/internal/zzjk:zzcng	Lcom/google/android/gms/internal/zzju;
    //   534: invokeinterface 639 1 0
    //   539: aload_0
    //   540: areturn
    //   541: new 520	java/net/URL
    //   544: dup
    //   545: aload_3
    //   546: invokespecial 522	java/net/URL:<init>	(Ljava/lang/String;)V
    //   549: astore_3
    //   550: iload 8
    //   552: iconst_1
    //   553: iadd
    //   554: istore 8
    //   556: iload 8
    //   558: iconst_5
    //   559: if_icmple +97 -> 656
    //   562: ldc_w 657
    //   565: invokestatic 648	com/google/android/gms/internal/zzkx:zzdi	(Ljava/lang/String;)V
    //   568: new 256	com/google/android/gms/ads/internal/request/AdResponseParcel
    //   571: dup
    //   572: iconst_0
    //   573: invokespecial 259	com/google/android/gms/ads/internal/request/AdResponseParcel:<init>	(I)V
    //   576: astore_0
    //   577: aload 14
    //   579: invokevirtual 636	java/net/HttpURLConnection:disconnect	()V
    //   582: aload 7
    //   584: ifnull +13 -> 597
    //   587: aload 7
    //   589: getfield 535	com/google/android/gms/internal/zzjk:zzcng	Lcom/google/android/gms/internal/zzju;
    //   592: invokeinterface 639 1 0
    //   597: aload_0
    //   598: areturn
    //   599: new 659	java/lang/StringBuilder
    //   602: dup
    //   603: bipush 46
    //   605: invokespecial 660	java/lang/StringBuilder:<init>	(I)V
    //   608: ldc_w 662
    //   611: invokevirtual 666	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   614: iload 9
    //   616: invokevirtual 669	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   619: invokevirtual 670	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   622: invokestatic 648	com/google/android/gms/internal/zzkx:zzdi	(Ljava/lang/String;)V
    //   625: new 256	com/google/android/gms/ads/internal/request/AdResponseParcel
    //   628: dup
    //   629: iconst_0
    //   630: invokespecial 259	com/google/android/gms/ads/internal/request/AdResponseParcel:<init>	(I)V
    //   633: astore_0
    //   634: aload 14
    //   636: invokevirtual 636	java/net/HttpURLConnection:disconnect	()V
    //   639: aload 7
    //   641: ifnull +13 -> 654
    //   644: aload 7
    //   646: getfield 535	com/google/android/gms/internal/zzjk:zzcng	Lcom/google/android/gms/internal/zzju;
    //   649: invokeinterface 639 1 0
    //   654: aload_0
    //   655: areturn
    //   656: aload 15
    //   658: aload 13
    //   660: invokevirtual 673	com/google/android/gms/internal/zzjo:zzk	(Ljava/util/Map;)V
    //   663: aload 14
    //   665: invokevirtual 636	java/net/HttpURLConnection:disconnect	()V
    //   668: aload 7
    //   670: ifnull +13 -> 683
    //   673: aload 7
    //   675: getfield 535	com/google/android/gms/internal/zzjk:zzcng	Lcom/google/android/gms/internal/zzju;
    //   678: invokeinterface 639 1 0
    //   683: goto -605 -> 78
    //   686: new 157	java/lang/String
    //   689: dup
    //   690: ldc_w 645
    //   693: invokespecial 640	java/lang/String:<init>	(Ljava/lang/String;)V
    //   696: astore_0
    //   697: goto -291 -> 406
    //   700: astore_0
    //   701: goto -247 -> 454
    //   704: astore_0
    //   705: aload 13
    //   707: astore_1
    //   708: goto -286 -> 422
    //   711: aload_0
    //   712: areturn
    //   713: aconst_null
    //   714: astore 12
    //   716: goto -704 -> 12
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	719	0	paramAdRequestInfoParcel	AdRequestInfoParcel
    //   0	719	1	paramContext	Context
    //   0	719	2	paramString1	String
    //   0	719	3	paramString2	String
    //   0	719	4	paramString3	String
    //   0	719	5	paramzzjq	zzjq
    //   0	719	6	paramzzdz	zzdz
    //   0	719	7	paramzzjk	zzjk
    //   76	484	8	i	int
    //   240	375	9	j	int
    //   73	266	10	l	long
    //   10	705	12	localzzdx	zzdx
    //   31	675	13	localObject	Object
    //   100	564	14	localHttpURLConnection	java.net.HttpURLConnection
    //   25	632	15	localzzjo	zzjo
    //   199	27	16	arrayOfByte	byte[]
    // Exception table:
    //   from	to	target	type
    //   12	51	382	java/io/IOException
    //   51	75	382	java/io/IOException
    //   83	93	382	java/io/IOException
    //   93	102	382	java/io/IOException
    //   344	349	382	java/io/IOException
    //   354	364	382	java/io/IOException
    //   367	379	382	java/io/IOException
    //   429	434	382	java/io/IOException
    //   439	449	382	java/io/IOException
    //   449	451	382	java/io/IOException
    //   519	524	382	java/io/IOException
    //   529	539	382	java/io/IOException
    //   577	582	382	java/io/IOException
    //   587	597	382	java/io/IOException
    //   634	639	382	java/io/IOException
    //   644	654	382	java/io/IOException
    //   663	668	382	java/io/IOException
    //   673	683	382	java/io/IOException
    //   209	223	419	finally
    //   102	139	428	finally
    //   139	169	428	finally
    //   174	209	428	finally
    //   230	235	428	finally
    //   235	249	428	finally
    //   265	270	428	finally
    //   291	313	428	finally
    //   318	336	428	finally
    //   336	344	428	finally
    //   422	428	428	finally
    //   454	460	428	finally
    //   460	472	428	finally
    //   488	519	428	finally
    //   541	550	428	finally
    //   562	577	428	finally
    //   599	634	428	finally
    //   656	663	428	finally
    //   270	283	451	finally
    //   283	291	700	finally
    //   223	230	704	finally
  }
  
  public static zzjl zza(Context paramContext, zzdk paramzzdk, zzjk paramzzjk)
  {
    synchronized (zzaox)
    {
      if (zzcnk == null)
      {
        Context localContext = paramContext;
        if (paramContext.getApplicationContext() != null) {
          localContext = paramContext.getApplicationContext();
        }
        zzcnk = new zzjl(localContext, paramzzdk, paramzzjk);
      }
      paramContext = zzcnk;
      return paramContext;
    }
  }
  
  private static void zza(String paramString1, Map<String, List<String>> paramMap, String paramString2, int paramInt)
  {
    if (zzkx.zzbi(2))
    {
      zzkx.v(String.valueOf(paramString1).length() + 39 + "Http Response: {\n  URL:\n    " + paramString1 + "\n  Headers:");
      if (paramMap != null)
      {
        Iterator localIterator1 = paramMap.keySet().iterator();
        if (localIterator1.hasNext())
        {
          paramString1 = (String)localIterator1.next();
          zzkx.v(String.valueOf(paramString1).length() + 5 + "    " + paramString1 + ":");
          Iterator localIterator2 = ((List)paramMap.get(paramString1)).iterator();
          label139:
          if (localIterator2.hasNext())
          {
            paramString1 = String.valueOf((String)localIterator2.next());
            if (paramString1.length() == 0) {
              break label185;
            }
          }
          label185:
          for (paramString1 = "      ".concat(paramString1);; paramString1 = new String("      "))
          {
            zzkx.v(paramString1);
            break label139;
            break;
          }
        }
      }
      zzkx.v("  Body:");
      if (paramString2 != null)
      {
        int i = 0;
        while (i < Math.min(paramString2.length(), 100000))
        {
          zzkx.v(paramString2.substring(i, Math.min(paramString2.length(), i + 1000)));
          i += 1000;
        }
      }
      zzkx.v("    null");
      zzkx.v(34 + "  Response Code:\n    " + paramInt + "\n}");
    }
  }
  
  private static Location zzc(zzlt<Location> paramzzlt)
  {
    try
    {
      paramzzlt = (Location)paramzzlt.get(((Long)zzdr.zzbjr.get()).longValue(), TimeUnit.MILLISECONDS);
      return paramzzlt;
    }
    catch (Exception paramzzlt)
    {
      zzkx.zzc("Exception caught while getting location", paramzzlt);
    }
    return null;
  }
  
  private static Bundle zzd(zzlt<Bundle> paramzzlt)
  {
    Bundle localBundle = new Bundle();
    try
    {
      paramzzlt = (Bundle)paramzzlt.get(((Long)zzdr.zzbkj.get()).longValue(), TimeUnit.MILLISECONDS);
      return paramzzlt;
    }
    catch (Exception paramzzlt)
    {
      zzkx.zzc("Exception caught while getting parental controls.", paramzzlt);
    }
    return localBundle;
  }
  
  public void zza(AdRequestInfoParcel paramAdRequestInfoParcel, zzl paramzzl)
  {
    zzu.zzgq().zzc(this.mContext, paramAdRequestInfoParcel.zzari);
    zzla.zza(new zzjl.5(this, paramAdRequestInfoParcel, paramzzl));
  }
  
  public AdResponseParcel zzd(AdRequestInfoParcel paramAdRequestInfoParcel)
  {
    return zza(this.mContext, this.zzcnn, this.zzcnm, this.zzcnl, paramAdRequestInfoParcel);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzjl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */