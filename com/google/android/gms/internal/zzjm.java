package com.google.android.gms.internal;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Debug.MemoryInfo;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.client.SearchAdRequestParcel;
import com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import com.google.android.gms.ads.internal.request.CapabilityParcel;
import com.google.android.gms.ads.internal.reward.mediation.client.RewardItemParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zzu;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@zzji
public final class zzjm
{
  private static final SimpleDateFormat zzcny = new SimpleDateFormat("yyyyMMdd", Locale.US);
  
  /* Error */
  public static AdResponseParcel zza(Context paramContext, AdRequestInfoParcel paramAdRequestInfoParcel, String paramString)
  {
    // Byte code:
    //   0: new 32	org/json/JSONObject
    //   3: dup
    //   4: aload_2
    //   5: invokespecial 35	org/json/JSONObject:<init>	(Ljava/lang/String;)V
    //   8: astore 29
    //   10: aload 29
    //   12: ldc 37
    //   14: aconst_null
    //   15: invokevirtual 41	org/json/JSONObject:optString	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   18: astore 25
    //   20: aload 29
    //   22: ldc 43
    //   24: aconst_null
    //   25: invokevirtual 41	org/json/JSONObject:optString	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   28: astore 26
    //   30: aload 29
    //   32: ldc 45
    //   34: aconst_null
    //   35: invokevirtual 41	org/json/JSONObject:optString	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   38: astore 30
    //   40: aload 29
    //   42: ldc 47
    //   44: aload 30
    //   46: invokevirtual 41	org/json/JSONObject:optString	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   49: astore 31
    //   51: aload_1
    //   52: ifnull +799 -> 851
    //   55: aload_1
    //   56: getfield 53	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:zzcka	I
    //   59: ifeq +792 -> 851
    //   62: iconst_1
    //   63: istore 5
    //   65: aload 29
    //   67: ldc 55
    //   69: aconst_null
    //   70: invokevirtual 41	org/json/JSONObject:optString	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   73: astore_2
    //   74: aload_2
    //   75: astore 24
    //   77: aload_2
    //   78: ifnonnull +13 -> 91
    //   81: aload 29
    //   83: ldc 57
    //   85: aconst_null
    //   86: invokevirtual 41	org/json/JSONObject:optString	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   89: astore 24
    //   91: aload 24
    //   93: astore_2
    //   94: aload 24
    //   96: ifnonnull +12 -> 108
    //   99: aload 29
    //   101: ldc 59
    //   103: aconst_null
    //   104: invokevirtual 41	org/json/JSONObject:optString	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   107: astore_2
    //   108: ldc2_w 60
    //   111: lstore 18
    //   113: aload 29
    //   115: ldc 63
    //   117: aconst_null
    //   118: invokevirtual 41	org/json/JSONObject:optString	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   121: astore 32
    //   123: aload 29
    //   125: ldc 65
    //   127: aconst_null
    //   128: invokevirtual 41	org/json/JSONObject:optString	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   131: astore 33
    //   133: aload 29
    //   135: ldc 67
    //   137: invokevirtual 71	org/json/JSONObject:has	(Ljava/lang/String;)Z
    //   140: ifeq +717 -> 857
    //   143: aload 29
    //   145: ldc 67
    //   147: invokevirtual 75	org/json/JSONObject:getDouble	(Ljava/lang/String;)D
    //   150: ldc2_w 76
    //   153: dmul
    //   154: d2l
    //   155: lstore 16
    //   157: aload 29
    //   159: ldc 79
    //   161: aconst_null
    //   162: invokevirtual 41	org/json/JSONObject:optString	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   165: astore 24
    //   167: iconst_m1
    //   168: istore_3
    //   169: ldc 81
    //   171: aload 24
    //   173: invokevirtual 87	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   176: ifeq +82 -> 258
    //   179: invokestatic 93	com/google/android/gms/ads/internal/zzu:zzgo	()Lcom/google/android/gms/internal/zzlc;
    //   182: invokevirtual 99	com/google/android/gms/internal/zzlc:zzvx	()I
    //   185: istore_3
    //   186: aconst_null
    //   187: astore 27
    //   189: aload_2
    //   190: invokestatic 105	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   193: ifeq +649 -> 842
    //   196: aload 26
    //   198: invokestatic 105	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   201: ifne +641 -> 842
    //   204: aload_1
    //   205: aload_0
    //   206: aload_1
    //   207: getfield 109	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:zzari	Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;
    //   210: getfield 115	com/google/android/gms/ads/internal/util/client/VersionInfoParcel:zzda	Ljava/lang/String;
    //   213: aload 26
    //   215: aconst_null
    //   216: aconst_null
    //   217: aconst_null
    //   218: aconst_null
    //   219: invokestatic 120	com/google/android/gms/internal/zzjl:zza	(Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/internal/zzjq;Lcom/google/android/gms/internal/zzdz;Lcom/google/android/gms/internal/zzjk;)Lcom/google/android/gms/ads/internal/request/AdResponseParcel;
    //   222: astore 27
    //   224: aload 27
    //   226: getfield 125	com/google/android/gms/ads/internal/request/AdResponseParcel:zzcbo	Ljava/lang/String;
    //   229: astore_2
    //   230: aload 27
    //   232: getfield 127	com/google/android/gms/ads/internal/request/AdResponseParcel:body	Ljava/lang/String;
    //   235: astore 24
    //   237: aload 27
    //   239: getfield 131	com/google/android/gms/ads/internal/request/AdResponseParcel:zzclf	J
    //   242: lstore 18
    //   244: aload 24
    //   246: ifnonnull +32 -> 278
    //   249: new 122	com/google/android/gms/ads/internal/request/AdResponseParcel
    //   252: dup
    //   253: iconst_0
    //   254: invokespecial 134	com/google/android/gms/ads/internal/request/AdResponseParcel:<init>	(I)V
    //   257: areturn
    //   258: ldc -120
    //   260: aload 24
    //   262: invokevirtual 87	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   265: ifeq -79 -> 186
    //   268: invokestatic 93	com/google/android/gms/ads/internal/zzu:zzgo	()Lcom/google/android/gms/internal/zzlc;
    //   271: invokevirtual 139	com/google/android/gms/internal/zzlc:zzvw	()I
    //   274: istore_3
    //   275: goto -89 -> 186
    //   278: aload 29
    //   280: ldc -115
    //   282: invokevirtual 145	org/json/JSONObject:optJSONArray	(Ljava/lang/String;)Lorg/json/JSONArray;
    //   285: astore 26
    //   287: aload 27
    //   289: ifnonnull +506 -> 795
    //   292: aconst_null
    //   293: astore_0
    //   294: aload_0
    //   295: astore 25
    //   297: aload 26
    //   299: ifnull +11 -> 310
    //   302: aload 26
    //   304: aload_0
    //   305: invokestatic 148	com/google/android/gms/internal/zzjm:zza	(Lorg/json/JSONArray;Ljava/util/List;)Ljava/util/List;
    //   308: astore 25
    //   310: aload 29
    //   312: ldc -106
    //   314: invokevirtual 145	org/json/JSONObject:optJSONArray	(Ljava/lang/String;)Lorg/json/JSONArray;
    //   317: astore 28
    //   319: aload 27
    //   321: ifnonnull +483 -> 804
    //   324: aconst_null
    //   325: astore_0
    //   326: aload_0
    //   327: astore 26
    //   329: aload 28
    //   331: ifnull +11 -> 342
    //   334: aload 28
    //   336: aload_0
    //   337: invokestatic 148	com/google/android/gms/internal/zzjm:zza	(Lorg/json/JSONArray;Ljava/util/List;)Ljava/util/List;
    //   340: astore 26
    //   342: aload 29
    //   344: ldc -104
    //   346: invokevirtual 145	org/json/JSONObject:optJSONArray	(Ljava/lang/String;)Lorg/json/JSONArray;
    //   349: astore 34
    //   351: aload 27
    //   353: ifnonnull +460 -> 813
    //   356: aconst_null
    //   357: astore_0
    //   358: aload_0
    //   359: astore 28
    //   361: aload 34
    //   363: ifnull +11 -> 374
    //   366: aload 34
    //   368: aload_0
    //   369: invokestatic 148	com/google/android/gms/internal/zzjm:zza	(Lorg/json/JSONArray;Ljava/util/List;)Ljava/util/List;
    //   372: astore 28
    //   374: iload_3
    //   375: istore 4
    //   377: aload 27
    //   379: ifnull +457 -> 836
    //   382: aload 27
    //   384: getfield 154	com/google/android/gms/ads/internal/request/AdResponseParcel:orientation	I
    //   387: iconst_m1
    //   388: if_icmpeq +9 -> 397
    //   391: aload 27
    //   393: getfield 154	com/google/android/gms/ads/internal/request/AdResponseParcel:orientation	I
    //   396: istore_3
    //   397: iload_3
    //   398: istore 4
    //   400: aload 27
    //   402: getfield 157	com/google/android/gms/ads/internal/request/AdResponseParcel:zzcla	J
    //   405: lconst_0
    //   406: lcmp
    //   407: ifle +429 -> 836
    //   410: aload 27
    //   412: getfield 157	com/google/android/gms/ads/internal/request/AdResponseParcel:zzcla	J
    //   415: lstore 16
    //   417: aload 29
    //   419: ldc -97
    //   421: invokevirtual 162	org/json/JSONObject:optString	(Ljava/lang/String;)Ljava/lang/String;
    //   424: astore 27
    //   426: aconst_null
    //   427: astore_0
    //   428: aload 29
    //   430: ldc -92
    //   432: iconst_0
    //   433: invokevirtual 168	org/json/JSONObject:optBoolean	(Ljava/lang/String;Z)Z
    //   436: istore 6
    //   438: iload 6
    //   440: ifeq +12 -> 452
    //   443: aload 29
    //   445: ldc -86
    //   447: aconst_null
    //   448: invokevirtual 41	org/json/JSONObject:optString	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   451: astore_0
    //   452: aload 29
    //   454: ldc -84
    //   456: iconst_0
    //   457: invokevirtual 168	org/json/JSONObject:optBoolean	(Ljava/lang/String;Z)Z
    //   460: istore 7
    //   462: aload 29
    //   464: ldc -82
    //   466: iconst_0
    //   467: invokevirtual 168	org/json/JSONObject:optBoolean	(Ljava/lang/String;Z)Z
    //   470: istore 8
    //   472: aload 29
    //   474: ldc -80
    //   476: iconst_1
    //   477: invokevirtual 168	org/json/JSONObject:optBoolean	(Ljava/lang/String;Z)Z
    //   480: istore 9
    //   482: aload 29
    //   484: ldc -78
    //   486: iconst_1
    //   487: invokevirtual 168	org/json/JSONObject:optBoolean	(Ljava/lang/String;Z)Z
    //   490: istore 10
    //   492: aload 29
    //   494: ldc -76
    //   496: iconst_0
    //   497: invokevirtual 168	org/json/JSONObject:optBoolean	(Ljava/lang/String;Z)Z
    //   500: istore 11
    //   502: aload 29
    //   504: ldc -74
    //   506: ldc2_w 60
    //   509: invokevirtual 186	org/json/JSONObject:optLong	(Ljava/lang/String;J)J
    //   512: lstore 20
    //   514: aload 29
    //   516: ldc -68
    //   518: ldc2_w 60
    //   521: invokevirtual 186	org/json/JSONObject:optLong	(Ljava/lang/String;J)J
    //   524: lstore 22
    //   526: aload 29
    //   528: ldc -66
    //   530: ldc -64
    //   532: invokevirtual 41	org/json/JSONObject:optString	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   535: astore 34
    //   537: ldc -62
    //   539: aload 29
    //   541: ldc -60
    //   543: ldc -64
    //   545: invokevirtual 41	org/json/JSONObject:optString	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   548: invokevirtual 87	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   551: istore 12
    //   553: aload 29
    //   555: ldc -58
    //   557: iconst_0
    //   558: invokevirtual 168	org/json/JSONObject:optBoolean	(Ljava/lang/String;Z)Z
    //   561: istore 13
    //   563: aload 29
    //   565: ldc -56
    //   567: invokevirtual 145	org/json/JSONObject:optJSONArray	(Ljava/lang/String;)Lorg/json/JSONArray;
    //   570: aconst_null
    //   571: invokestatic 148	com/google/android/gms/internal/zzjm:zza	(Lorg/json/JSONArray;Ljava/util/List;)Ljava/util/List;
    //   574: astore 35
    //   576: aload 29
    //   578: ldc -54
    //   580: invokevirtual 145	org/json/JSONObject:optJSONArray	(Ljava/lang/String;)Lorg/json/JSONArray;
    //   583: aconst_null
    //   584: invokestatic 148	com/google/android/gms/internal/zzjm:zza	(Lorg/json/JSONArray;Ljava/util/List;)Ljava/util/List;
    //   587: astore 36
    //   589: aload 29
    //   591: ldc -52
    //   593: invokevirtual 145	org/json/JSONObject:optJSONArray	(Ljava/lang/String;)Lorg/json/JSONArray;
    //   596: invokestatic 209	com/google/android/gms/ads/internal/reward/mediation/client/RewardItemParcel:zza	(Lorg/json/JSONArray;)Lcom/google/android/gms/ads/internal/reward/mediation/client/RewardItemParcel;
    //   599: astore 37
    //   601: aload 29
    //   603: ldc -45
    //   605: iconst_0
    //   606: invokevirtual 168	org/json/JSONObject:optBoolean	(Ljava/lang/String;Z)Z
    //   609: istore 14
    //   611: aload 29
    //   613: ldc -43
    //   615: invokevirtual 217	org/json/JSONObject:optJSONObject	(Ljava/lang/String;)Lorg/json/JSONObject;
    //   618: invokestatic 223	com/google/android/gms/ads/internal/request/AutoClickProtectionConfigurationParcel:zzh	(Lorg/json/JSONObject;)Lcom/google/android/gms/ads/internal/request/AutoClickProtectionConfigurationParcel;
    //   621: astore 38
    //   623: aload 29
    //   625: ldc -31
    //   627: ldc -64
    //   629: invokevirtual 41	org/json/JSONObject:optString	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   632: astore 39
    //   634: aload 29
    //   636: ldc -29
    //   638: invokevirtual 145	org/json/JSONObject:optJSONArray	(Ljava/lang/String;)Lorg/json/JSONArray;
    //   641: aconst_null
    //   642: invokestatic 148	com/google/android/gms/internal/zzjm:zza	(Lorg/json/JSONArray;Ljava/util/List;)Ljava/util/List;
    //   645: astore 40
    //   647: aload 29
    //   649: ldc -27
    //   651: invokevirtual 217	org/json/JSONObject:optJSONObject	(Ljava/lang/String;)Lorg/json/JSONObject;
    //   654: invokestatic 235	com/google/android/gms/ads/internal/safebrowsing/SafeBrowsingConfigParcel:zzj	(Lorg/json/JSONObject;)Lcom/google/android/gms/ads/internal/safebrowsing/SafeBrowsingConfigParcel;
    //   657: astore 41
    //   659: aload 29
    //   661: ldc -19
    //   663: aload_1
    //   664: getfield 241	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:zzbvo	Z
    //   667: invokevirtual 168	org/json/JSONObject:optBoolean	(Ljava/lang/String;Z)Z
    //   670: istore 15
    //   672: new 122	com/google/android/gms/ads/internal/request/AdResponseParcel
    //   675: dup
    //   676: aload_1
    //   677: aload_2
    //   678: aload 24
    //   680: aload 25
    //   682: aload 26
    //   684: lload 16
    //   686: iload 7
    //   688: lload 22
    //   690: aload 28
    //   692: lload 20
    //   694: iload_3
    //   695: aload 30
    //   697: lload 18
    //   699: aload 32
    //   701: iload 6
    //   703: aload_0
    //   704: aload 27
    //   706: iload 8
    //   708: iload 5
    //   710: aload_1
    //   711: getfield 244	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:zzckc	Z
    //   714: iload 9
    //   716: iload 11
    //   718: aload 34
    //   720: iload 12
    //   722: iload 13
    //   724: aload 37
    //   726: aload 35
    //   728: aload 36
    //   730: iload 14
    //   732: aload 38
    //   734: aload_1
    //   735: getfield 247	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:zzcks	Z
    //   738: aload 39
    //   740: aload 40
    //   742: iload 15
    //   744: aload 31
    //   746: aload 41
    //   748: aload 33
    //   750: iload 10
    //   752: invokespecial 250	com/google/android/gms/ads/internal/request/AdResponseParcel:<init>	(Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;JZJLjava/util/List;JILjava/lang/String;JLjava/lang/String;ZLjava/lang/String;Ljava/lang/String;ZZZZZLjava/lang/String;ZZLcom/google/android/gms/ads/internal/reward/mediation/client/RewardItemParcel;Ljava/util/List;Ljava/util/List;ZLcom/google/android/gms/ads/internal/request/AutoClickProtectionConfigurationParcel;ZLjava/lang/String;Ljava/util/List;ZLjava/lang/String;Lcom/google/android/gms/ads/internal/safebrowsing/SafeBrowsingConfigParcel;Ljava/lang/String;Z)V
    //   755: astore_0
    //   756: aload_0
    //   757: areturn
    //   758: astore_0
    //   759: aload_0
    //   760: invokevirtual 254	org/json/JSONException:getMessage	()Ljava/lang/String;
    //   763: invokestatic 258	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   766: astore_0
    //   767: aload_0
    //   768: invokevirtual 261	java/lang/String:length	()I
    //   771: ifeq +51 -> 822
    //   774: ldc_w 263
    //   777: aload_0
    //   778: invokevirtual 266	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   781: astore_0
    //   782: aload_0
    //   783: invokestatic 271	com/google/android/gms/internal/zzkx:zzdi	(Ljava/lang/String;)V
    //   786: new 122	com/google/android/gms/ads/internal/request/AdResponseParcel
    //   789: dup
    //   790: iconst_0
    //   791: invokespecial 134	com/google/android/gms/ads/internal/request/AdResponseParcel:<init>	(I)V
    //   794: areturn
    //   795: aload 27
    //   797: getfield 275	com/google/android/gms/ads/internal/request/AdResponseParcel:zzbvk	Ljava/util/List;
    //   800: astore_0
    //   801: goto -507 -> 294
    //   804: aload 27
    //   806: getfield 278	com/google/android/gms/ads/internal/request/AdResponseParcel:zzbvl	Ljava/util/List;
    //   809: astore_0
    //   810: goto -484 -> 326
    //   813: aload 27
    //   815: getfield 281	com/google/android/gms/ads/internal/request/AdResponseParcel:zzcld	Ljava/util/List;
    //   818: astore_0
    //   819: goto -461 -> 358
    //   822: new 83	java/lang/String
    //   825: dup
    //   826: ldc_w 263
    //   829: invokespecial 282	java/lang/String:<init>	(Ljava/lang/String;)V
    //   832: astore_0
    //   833: goto -51 -> 782
    //   836: iload 4
    //   838: istore_3
    //   839: goto -422 -> 417
    //   842: aload_2
    //   843: astore 24
    //   845: aload 25
    //   847: astore_2
    //   848: goto -604 -> 244
    //   851: iconst_0
    //   852: istore 5
    //   854: goto -789 -> 65
    //   857: ldc2_w 60
    //   860: lstore 16
    //   862: goto -705 -> 157
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	865	0	paramContext	Context
    //   0	865	1	paramAdRequestInfoParcel	AdRequestInfoParcel
    //   0	865	2	paramString	String
    //   168	671	3	i	int
    //   375	462	4	j	int
    //   63	790	5	bool1	boolean
    //   436	266	6	bool2	boolean
    //   460	227	7	bool3	boolean
    //   470	237	8	bool4	boolean
    //   480	235	9	bool5	boolean
    //   490	261	10	bool6	boolean
    //   500	217	11	bool7	boolean
    //   551	170	12	bool8	boolean
    //   561	162	13	bool9	boolean
    //   609	122	14	bool10	boolean
    //   670	73	15	bool11	boolean
    //   155	706	16	l1	long
    //   111	587	18	l2	long
    //   512	181	20	l3	long
    //   524	165	22	l4	long
    //   75	769	24	str1	String
    //   18	828	25	localObject1	Object
    //   28	655	26	localObject2	Object
    //   187	627	27	localObject3	Object
    //   317	374	28	localObject4	Object
    //   8	652	29	localJSONObject	JSONObject
    //   38	658	30	str2	String
    //   49	696	31	str3	String
    //   121	579	32	str4	String
    //   131	618	33	str5	String
    //   349	370	34	localObject5	Object
    //   574	153	35	localList1	List
    //   587	142	36	localList2	List
    //   599	126	37	localRewardItemParcel	RewardItemParcel
    //   621	112	38	localAutoClickProtectionConfigurationParcel	com.google.android.gms.ads.internal.request.AutoClickProtectionConfigurationParcel
    //   632	107	39	str6	String
    //   645	96	40	localList3	List
    //   657	90	41	localSafeBrowsingConfigParcel	com.google.android.gms.ads.internal.safebrowsing.SafeBrowsingConfigParcel
    // Exception table:
    //   from	to	target	type
    //   0	51	758	org/json/JSONException
    //   55	62	758	org/json/JSONException
    //   65	74	758	org/json/JSONException
    //   81	91	758	org/json/JSONException
    //   99	108	758	org/json/JSONException
    //   113	157	758	org/json/JSONException
    //   157	167	758	org/json/JSONException
    //   169	186	758	org/json/JSONException
    //   189	244	758	org/json/JSONException
    //   249	258	758	org/json/JSONException
    //   258	275	758	org/json/JSONException
    //   278	287	758	org/json/JSONException
    //   302	310	758	org/json/JSONException
    //   310	319	758	org/json/JSONException
    //   334	342	758	org/json/JSONException
    //   342	351	758	org/json/JSONException
    //   366	374	758	org/json/JSONException
    //   382	397	758	org/json/JSONException
    //   400	417	758	org/json/JSONException
    //   417	426	758	org/json/JSONException
    //   428	438	758	org/json/JSONException
    //   443	452	758	org/json/JSONException
    //   452	756	758	org/json/JSONException
    //   795	801	758	org/json/JSONException
    //   804	810	758	org/json/JSONException
    //   813	819	758	org/json/JSONException
  }
  
  @Nullable
  private static List<String> zza(@Nullable JSONArray paramJSONArray, @Nullable List<String> paramList)
    throws JSONException
  {
    if (paramJSONArray == null)
    {
      paramList = null;
      return paramList;
    }
    Object localObject = paramList;
    if (paramList == null) {
      localObject = new LinkedList();
    }
    int i = 0;
    for (;;)
    {
      paramList = (List<String>)localObject;
      if (i >= paramJSONArray.length()) {
        break;
      }
      ((List)localObject).add(paramJSONArray.getString(i));
      i += 1;
    }
  }
  
  @Nullable
  public static JSONObject zza(Context paramContext, zzjj paramzzjj)
  {
    paramContext = paramzzjj.zzcmx;
    Location localLocation = paramzzjj.zzayt;
    zzjr localzzjr = paramzzjj.zzcmy;
    Bundle localBundle = paramzzjj.zzckb;
    JSONObject localJSONObject = paramzzjj.zzcmz;
    for (;;)
    {
      HashMap localHashMap;
      Object localObject;
      int m;
      int i;
      int j;
      AdSizeParcel localAdSizeParcel;
      int k;
      try
      {
        localHashMap = new HashMap();
        localHashMap.put("extra_caps", zzdr.zzbit.get());
        if (paramzzjj.zzckj.size() > 0) {
          localHashMap.put("eid", TextUtils.join(",", paramzzjj.zzckj));
        }
        if (paramContext.zzcjt != null) {
          localHashMap.put("ad_pos", paramContext.zzcjt);
        }
        zza(localHashMap, paramContext.zzcju);
        AdSizeParcel[] arrayOfAdSizeParcel;
        if (paramContext.zzarm.zzazs == null)
        {
          localHashMap.put("format", paramContext.zzarm.zzazq);
          if (paramContext.zzarm.zzazu) {
            localHashMap.put("fluid", "height");
          }
          if (paramContext.zzarm.width == -1) {
            localHashMap.put("smart_w", "full");
          }
          if (paramContext.zzarm.height == -2) {
            localHashMap.put("smart_h", "auto");
          }
          if (paramContext.zzarm.zzazs == null) {
            break label560;
          }
          localObject = new StringBuilder();
          arrayOfAdSizeParcel = paramContext.zzarm.zzazs;
          m = arrayOfAdSizeParcel.length;
          i = 0;
          j = 0;
          if (i >= m) {
            break label517;
          }
          localAdSizeParcel = arrayOfAdSizeParcel[i];
          if (localAdSizeParcel.zzazu)
          {
            j = 1;
            break label1562;
          }
        }
        else
        {
          localObject = paramContext.zzarm.zzazs;
          int n = localObject.length;
          j = 0;
          m = 0;
          i = 0;
          if (i >= n) {
            continue;
          }
          arrayOfAdSizeParcel = localObject[i];
          k = m;
          if (!arrayOfAdSizeParcel.zzazu)
          {
            k = m;
            if (m == 0)
            {
              localHashMap.put("format", arrayOfAdSizeParcel.zzazq);
              k = 1;
            }
          }
          m = j;
          if (!arrayOfAdSizeParcel.zzazu) {
            break label1569;
          }
          m = j;
          if (j != 0) {
            break label1569;
          }
          localHashMap.put("fluid", "height");
          m = 1;
          break label1569;
        }
        if (((StringBuilder)localObject).length() != 0) {
          ((StringBuilder)localObject).append("|");
        }
        if (localAdSizeParcel.width == -1)
        {
          k = (int)(localAdSizeParcel.widthPixels / localzzjr.zzavd);
          ((StringBuilder)localObject).append(k);
          ((StringBuilder)localObject).append("x");
          if (localAdSizeParcel.height != -2) {
            break label507;
          }
          k = (int)(localAdSizeParcel.heightPixels / localzzjr.zzavd);
          label456:
          ((StringBuilder)localObject).append(k);
        }
      }
      catch (JSONException paramContext)
      {
        paramContext = String.valueOf(paramContext.getMessage());
        if (paramContext.length() == 0) {}
      }
      for (paramContext = "Problem serializing ad request to JSON: ".concat(paramContext);; paramContext = new String("Problem serializing ad request to JSON: "))
      {
        zzkx.zzdi(paramContext);
        return null;
        k = localAdSizeParcel.width;
        break;
        label507:
        k = localAdSizeParcel.height;
        break label456;
        label517:
        if (j != 0)
        {
          if (((StringBuilder)localObject).length() != 0) {
            ((StringBuilder)localObject).insert(0, "|");
          }
          ((StringBuilder)localObject).insert(0, "320x50");
        }
        localHashMap.put("sz", localObject);
        label560:
        if (paramContext.zzcka != 0)
        {
          localHashMap.put("native_version", Integer.valueOf(paramContext.zzcka));
          localHashMap.put("native_templates", paramContext.zzase);
          localHashMap.put("native_image_orientation", zzc(paramContext.zzasa));
          if (!paramContext.zzckk.isEmpty()) {
            localHashMap.put("native_custom_templates", paramContext.zzckk);
          }
        }
        if (paramContext.zzarm.zzazv) {
          localHashMap.put("ene", Boolean.valueOf(true));
        }
        localHashMap.put("slotname", paramContext.zzarg);
        localHashMap.put("pn", paramContext.applicationInfo.packageName);
        if (paramContext.zzcjv != null) {
          localHashMap.put("vc", Integer.valueOf(paramContext.zzcjv.versionCode));
        }
        localHashMap.put("ms", paramzzjj.zzcjw);
        localHashMap.put("seq_num", paramContext.zzcjx);
        localHashMap.put("session_id", paramContext.zzcjy);
        localHashMap.put("js", paramContext.zzari.zzda);
        zza(localHashMap, localzzjr, paramzzjj.zzcmv, paramContext.zzckx, paramzzjj.zzcmu);
        zza(localHashMap, paramzzjj.zzcmw);
        localHashMap.put("platform", Build.MANUFACTURER);
        localHashMap.put("submodel", Build.MODEL);
        if (localLocation != null) {
          zza(localHashMap, localLocation);
        }
        for (;;)
        {
          if (paramContext.versionCode >= 2) {
            localHashMap.put("quality_signals", paramContext.zzcjz);
          }
          if ((paramContext.versionCode >= 4) && (paramContext.zzckc)) {
            localHashMap.put("forceHttps", Boolean.valueOf(paramContext.zzckc));
          }
          if (localBundle != null) {
            localHashMap.put("content_info", localBundle);
          }
          label958:
          boolean bool;
          if (paramContext.versionCode >= 5)
          {
            localHashMap.put("u_sd", Float.valueOf(paramContext.zzavd));
            localHashMap.put("sh", Integer.valueOf(paramContext.zzckf));
            localHashMap.put("sw", Integer.valueOf(paramContext.zzcke));
            if (paramContext.versionCode >= 6)
            {
              bool = TextUtils.isEmpty(paramContext.zzckg);
              if (bool) {}
            }
          }
          try
          {
            localHashMap.put("view_hierarchy", new JSONObject(paramContext.zzckg));
            localHashMap.put("correlation_id", Long.valueOf(paramContext.zzckh));
            if (paramContext.versionCode >= 7) {
              localHashMap.put("request_id", paramContext.zzcki);
            }
            if ((paramContext.versionCode >= 11) && (paramContext.zzckm != null)) {
              localHashMap.put("capability", paramContext.zzckm.toBundle());
            }
            if ((paramContext.versionCode >= 12) && (!TextUtils.isEmpty(paramContext.zzckn))) {
              localHashMap.put("anchor", paramContext.zzckn);
            }
            if (paramContext.versionCode >= 13) {
              localHashMap.put("android_app_volume", Float.valueOf(paramContext.zzcko));
            }
            if (paramContext.versionCode >= 18) {
              localHashMap.put("android_app_muted", Boolean.valueOf(paramContext.zzcku));
            }
            if ((paramContext.versionCode >= 14) && (paramContext.zzckp > 0)) {
              localHashMap.put("target_api", Integer.valueOf(paramContext.zzckp));
            }
            if (paramContext.versionCode >= 15)
            {
              if (paramContext.zzckq == -1)
              {
                i = -1;
                localHashMap.put("scroll_index", Integer.valueOf(i));
              }
            }
            else
            {
              if (paramContext.versionCode >= 16) {
                localHashMap.put("_activity_context", Boolean.valueOf(paramContext.zzckr));
              }
              if (paramContext.versionCode >= 18)
              {
                bool = TextUtils.isEmpty(paramContext.zzckv);
                if (bool) {}
              }
            }
          }
          catch (JSONException paramzzjj)
          {
            try
            {
              for (;;)
              {
                localHashMap.put("app_settings", new JSONObject(paramContext.zzckv));
                localHashMap.put("render_in_browser", Boolean.valueOf(paramContext.zzbvo));
                if (paramContext.versionCode >= 18) {
                  localHashMap.put("android_num_video_cache_tasks", Integer.valueOf(paramContext.zzckw));
                }
                zza(localHashMap);
                localHashMap.put("cache_state", localJSONObject);
                if (paramContext.versionCode >= 19) {
                  localHashMap.put("gct", paramContext.zzcky);
                }
                if (zzkx.zzbi(2))
                {
                  paramContext = String.valueOf(zzu.zzgm().zzap(localHashMap).toString(2));
                  if (paramContext.length() == 0) {
                    break label1534;
                  }
                  paramContext = "Ad Request JSON: ".concat(paramContext);
                  zzkx.v(paramContext);
                }
                return zzu.zzgm().zzap(localHashMap);
                if ((paramContext.zzcju.versionCode < 2) || (paramContext.zzcju.zzayt == null)) {
                  break;
                }
                zza(localHashMap, paramContext.zzcju.zzayt);
                break;
                localHashMap.put("u_sd", Float.valueOf(localzzjr.zzavd));
                localHashMap.put("sh", Integer.valueOf(localzzjr.zzckf));
                localHashMap.put("sw", Integer.valueOf(localzzjr.zzcke));
                break label958;
                paramzzjj = paramzzjj;
                zzkx.zzc("Problem serializing view hierarchy to JSON", paramzzjj);
              }
              i = paramContext.zzckq;
            }
            catch (JSONException paramzzjj)
            {
              for (;;)
              {
                zzkx.zzc("Problem creating json from app settings", paramzzjj);
                continue;
                label1534:
                paramContext = new String("Ad Request JSON: ");
              }
            }
          }
        }
      }
      label1562:
      i += 1;
      continue;
      label1569:
      if ((k == 0) || (m == 0))
      {
        i += 1;
        j = m;
        m = k;
      }
    }
  }
  
  private static void zza(HashMap<String, Object> paramHashMap)
  {
    Bundle localBundle1 = new Bundle();
    Bundle localBundle2 = new Bundle();
    localBundle2.putString("cl", "135396225");
    localBundle2.putString("rapid_rc", "dev");
    localBundle2.putString("rapid_rollup", "HEAD");
    localBundle1.putBundle("build_meta", localBundle2);
    localBundle1.putString("mf", Boolean.toString(((Boolean)zzdr.zzbiv.get()).booleanValue()));
    paramHashMap.put("sdk_env", localBundle1);
  }
  
  private static void zza(HashMap<String, Object> paramHashMap, Location paramLocation)
  {
    HashMap localHashMap = new HashMap();
    float f = paramLocation.getAccuracy();
    long l1 = paramLocation.getTime();
    long l2 = (paramLocation.getLatitude() * 1.0E7D);
    long l3 = (paramLocation.getLongitude() * 1.0E7D);
    localHashMap.put("radius", Float.valueOf(f * 1000.0F));
    localHashMap.put("lat", Long.valueOf(l2));
    localHashMap.put("long", Long.valueOf(l3));
    localHashMap.put("time", Long.valueOf(l1 * 1000L));
    paramHashMap.put("uule", localHashMap);
  }
  
  private static void zza(HashMap<String, Object> paramHashMap, AdRequestParcel paramAdRequestParcel)
  {
    String str = zzkv.zzvl();
    if (str != null) {
      paramHashMap.put("abf", str);
    }
    if (paramAdRequestParcel.zzayl != -1L) {
      paramHashMap.put("cust_age", zzcny.format(new Date(paramAdRequestParcel.zzayl)));
    }
    if (paramAdRequestParcel.extras != null) {
      paramHashMap.put("extras", paramAdRequestParcel.extras);
    }
    if (paramAdRequestParcel.zzaym != -1) {
      paramHashMap.put("cust_gender", Integer.valueOf(paramAdRequestParcel.zzaym));
    }
    if (paramAdRequestParcel.zzayn != null) {
      paramHashMap.put("kw", paramAdRequestParcel.zzayn);
    }
    if (paramAdRequestParcel.zzayp != -1) {
      paramHashMap.put("tag_for_child_directed_treatment", Integer.valueOf(paramAdRequestParcel.zzayp));
    }
    if (paramAdRequestParcel.zzayo) {
      paramHashMap.put("adtest", "on");
    }
    if (paramAdRequestParcel.versionCode >= 2)
    {
      if (paramAdRequestParcel.zzayq) {
        paramHashMap.put("d_imp_hdr", Integer.valueOf(1));
      }
      if (!TextUtils.isEmpty(paramAdRequestParcel.zzayr)) {
        paramHashMap.put("ppid", paramAdRequestParcel.zzayr);
      }
      if (paramAdRequestParcel.zzays != null) {
        zza(paramHashMap, paramAdRequestParcel.zzays);
      }
    }
    if ((paramAdRequestParcel.versionCode >= 3) && (paramAdRequestParcel.zzayu != null)) {
      paramHashMap.put("url", paramAdRequestParcel.zzayu);
    }
    if (paramAdRequestParcel.versionCode >= 5)
    {
      if (paramAdRequestParcel.zzayw != null) {
        paramHashMap.put("custom_targeting", paramAdRequestParcel.zzayw);
      }
      if (paramAdRequestParcel.zzayx != null) {
        paramHashMap.put("category_exclusions", paramAdRequestParcel.zzayx);
      }
      if (paramAdRequestParcel.zzayy != null) {
        paramHashMap.put("request_agent", paramAdRequestParcel.zzayy);
      }
    }
    if ((paramAdRequestParcel.versionCode >= 6) && (paramAdRequestParcel.zzayz != null)) {
      paramHashMap.put("request_pkg", paramAdRequestParcel.zzayz);
    }
    if (paramAdRequestParcel.versionCode >= 7) {
      paramHashMap.put("is_designed_for_families", Boolean.valueOf(paramAdRequestParcel.zzaza));
    }
  }
  
  private static void zza(HashMap<String, Object> paramHashMap, SearchAdRequestParcel paramSearchAdRequestParcel)
  {
    Object localObject2 = null;
    if (Color.alpha(paramSearchAdRequestParcel.zzbbx) != 0) {
      paramHashMap.put("acolor", zzaz(paramSearchAdRequestParcel.zzbbx));
    }
    if (Color.alpha(paramSearchAdRequestParcel.backgroundColor) != 0) {
      paramHashMap.put("bgcolor", zzaz(paramSearchAdRequestParcel.backgroundColor));
    }
    if ((Color.alpha(paramSearchAdRequestParcel.zzbby) != 0) && (Color.alpha(paramSearchAdRequestParcel.zzbbz) != 0))
    {
      paramHashMap.put("gradientto", zzaz(paramSearchAdRequestParcel.zzbby));
      paramHashMap.put("gradientfrom", zzaz(paramSearchAdRequestParcel.zzbbz));
    }
    if (Color.alpha(paramSearchAdRequestParcel.zzbca) != 0) {
      paramHashMap.put("bcolor", zzaz(paramSearchAdRequestParcel.zzbca));
    }
    paramHashMap.put("bthick", Integer.toString(paramSearchAdRequestParcel.zzbcb));
    Object localObject1;
    switch (paramSearchAdRequestParcel.zzbcc)
    {
    default: 
      localObject1 = null;
      if (localObject1 != null) {
        paramHashMap.put("btype", localObject1);
      }
      switch (paramSearchAdRequestParcel.zzbcd)
      {
      default: 
        localObject1 = localObject2;
      }
      break;
    }
    for (;;)
    {
      if (localObject1 != null) {
        paramHashMap.put("callbuttoncolor", localObject1);
      }
      if (paramSearchAdRequestParcel.zzbce != null) {
        paramHashMap.put("channel", paramSearchAdRequestParcel.zzbce);
      }
      if (Color.alpha(paramSearchAdRequestParcel.zzbcf) != 0) {
        paramHashMap.put("dcolor", zzaz(paramSearchAdRequestParcel.zzbcf));
      }
      if (paramSearchAdRequestParcel.zzbcg != null) {
        paramHashMap.put("font", paramSearchAdRequestParcel.zzbcg);
      }
      if (Color.alpha(paramSearchAdRequestParcel.zzbch) != 0) {
        paramHashMap.put("hcolor", zzaz(paramSearchAdRequestParcel.zzbch));
      }
      paramHashMap.put("headersize", Integer.toString(paramSearchAdRequestParcel.zzbci));
      if (paramSearchAdRequestParcel.zzbcj != null) {
        paramHashMap.put("q", paramSearchAdRequestParcel.zzbcj);
      }
      return;
      localObject1 = "none";
      break;
      localObject1 = "dashed";
      break;
      localObject1 = "dotted";
      break;
      localObject1 = "solid";
      break;
      localObject1 = "dark";
      continue;
      localObject1 = "light";
      continue;
      localObject1 = "medium";
    }
  }
  
  private static void zza(HashMap<String, Object> paramHashMap, zzjr paramzzjr, zzjv.zza paramzza, Bundle paramBundle1, Bundle paramBundle2)
  {
    paramHashMap.put("am", Integer.valueOf(paramzzjr.zzcps));
    paramHashMap.put("cog", zzac(paramzzjr.zzcpt));
    paramHashMap.put("coh", zzac(paramzzjr.zzcpu));
    if (!TextUtils.isEmpty(paramzzjr.zzcpv)) {
      paramHashMap.put("carrier", paramzzjr.zzcpv);
    }
    paramHashMap.put("gl", paramzzjr.zzcpw);
    if (paramzzjr.zzcpx) {
      paramHashMap.put("simulator", Integer.valueOf(1));
    }
    if (paramzzjr.zzcpy) {
      paramHashMap.put("is_sidewinder", Integer.valueOf(1));
    }
    paramHashMap.put("ma", zzac(paramzzjr.zzcpz));
    paramHashMap.put("sp", zzac(paramzzjr.zzcqa));
    paramHashMap.put("hl", paramzzjr.zzcqb);
    if (!TextUtils.isEmpty(paramzzjr.zzcqc)) {
      paramHashMap.put("mv", paramzzjr.zzcqc);
    }
    paramHashMap.put("muv", Integer.valueOf(paramzzjr.zzcqd));
    if (paramzzjr.zzcqe != -2) {
      paramHashMap.put("cnt", Integer.valueOf(paramzzjr.zzcqe));
    }
    paramHashMap.put("gnt", Integer.valueOf(paramzzjr.zzcqf));
    paramHashMap.put("pt", Integer.valueOf(paramzzjr.zzcqg));
    paramHashMap.put("rm", Integer.valueOf(paramzzjr.zzcqh));
    paramHashMap.put("riv", Integer.valueOf(paramzzjr.zzcqi));
    Bundle localBundle1 = new Bundle();
    localBundle1.putString("build", paramzzjr.zzcqn);
    Bundle localBundle2 = new Bundle();
    localBundle2.putBoolean("is_charging", paramzzjr.zzcqk);
    localBundle2.putDouble("battery_level", paramzzjr.zzcqj);
    localBundle1.putBundle("battery", localBundle2);
    localBundle2 = new Bundle();
    localBundle2.putInt("active_network_state", paramzzjr.zzcqm);
    localBundle2.putBoolean("active_network_metered", paramzzjr.zzcql);
    if (paramzza != null)
    {
      Bundle localBundle3 = new Bundle();
      localBundle3.putInt("predicted_latency_micros", paramzza.zzcqt);
      localBundle3.putLong("predicted_down_throughput_bps", paramzza.zzcqu);
      localBundle3.putLong("predicted_up_throughput_bps", paramzza.zzcqv);
      localBundle2.putBundle("predictions", localBundle3);
    }
    localBundle1.putBundle("network", localBundle2);
    paramzza = new Bundle();
    paramzza.putBoolean("is_browser_custom_tabs_capable", paramzzjr.zzcqo);
    localBundle1.putBundle("browser", paramzza);
    if (paramBundle1 != null) {
      localBundle1.putBundle("android_mem_info", zzg(paramBundle1));
    }
    paramzzjr = new Bundle();
    paramzzjr.putBundle("parental_controls", paramBundle2);
    localBundle1.putBundle("play_store", paramzzjr);
    paramHashMap.put("device", localBundle1);
  }
  
  private static void zza(HashMap<String, Object> paramHashMap, String paramString)
  {
    Bundle localBundle = new Bundle();
    localBundle.putString("doritos", paramString);
    paramHashMap.put("pii", localBundle);
  }
  
  private static Integer zzac(boolean paramBoolean)
  {
    if (paramBoolean) {}
    for (int i = 1;; i = 0) {
      return Integer.valueOf(i);
    }
  }
  
  private static String zzaz(int paramInt)
  {
    return String.format(Locale.US, "#%06x", new Object[] { Integer.valueOf(0xFFFFFF & paramInt) });
  }
  
  private static String zzc(NativeAdOptionsParcel paramNativeAdOptionsParcel)
  {
    if (paramNativeAdOptionsParcel != null) {}
    for (int i = paramNativeAdOptionsParcel.zzbok;; i = 0) {
      switch (i)
      {
      default: 
        return "any";
      }
    }
    return "portrait";
    return "landscape";
  }
  
  public static JSONObject zzc(AdResponseParcel paramAdResponseParcel)
    throws JSONException
  {
    JSONObject localJSONObject = new JSONObject();
    if (paramAdResponseParcel.zzcbo != null) {
      localJSONObject.put("ad_base_url", paramAdResponseParcel.zzcbo);
    }
    if (paramAdResponseParcel.zzcle != null) {
      localJSONObject.put("ad_size", paramAdResponseParcel.zzcle);
    }
    localJSONObject.put("native", paramAdResponseParcel.zzazt);
    if (paramAdResponseParcel.zzazt)
    {
      localJSONObject.put("ad_json", paramAdResponseParcel.body);
      if (paramAdResponseParcel.zzclg != null) {
        localJSONObject.put("debug_dialog", paramAdResponseParcel.zzclg);
      }
      if (paramAdResponseParcel.zzclx != null) {
        localJSONObject.put("debug_signals", paramAdResponseParcel.zzclx);
      }
      if (paramAdResponseParcel.zzcla != -1L) {
        localJSONObject.put("interstitial_timeout", paramAdResponseParcel.zzcla / 1000.0D);
      }
      if (paramAdResponseParcel.orientation != zzu.zzgo().zzvx()) {
        break label530;
      }
      localJSONObject.put("orientation", "portrait");
      label159:
      if (paramAdResponseParcel.zzbvk != null) {
        localJSONObject.put("click_urls", zzl(paramAdResponseParcel.zzbvk));
      }
      if (paramAdResponseParcel.zzbvl != null) {
        localJSONObject.put("impression_urls", zzl(paramAdResponseParcel.zzbvl));
      }
      if (paramAdResponseParcel.zzcld != null) {
        localJSONObject.put("manual_impression_urls", zzl(paramAdResponseParcel.zzcld));
      }
      if (paramAdResponseParcel.zzclj != null) {
        localJSONObject.put("active_view", paramAdResponseParcel.zzclj);
      }
      localJSONObject.put("ad_is_javascript", paramAdResponseParcel.zzclh);
      if (paramAdResponseParcel.zzcli != null) {
        localJSONObject.put("ad_passback_url", paramAdResponseParcel.zzcli);
      }
      localJSONObject.put("mediation", paramAdResponseParcel.zzclb);
      localJSONObject.put("custom_render_allowed", paramAdResponseParcel.zzclk);
      localJSONObject.put("content_url_opted_out", paramAdResponseParcel.zzcll);
      localJSONObject.put("content_vertical_opted_out", paramAdResponseParcel.zzcly);
      localJSONObject.put("prefetch", paramAdResponseParcel.zzclm);
      if (paramAdResponseParcel.zzbvq != -1L) {
        localJSONObject.put("refresh_interval_milliseconds", paramAdResponseParcel.zzbvq);
      }
      if (paramAdResponseParcel.zzclc != -1L) {
        localJSONObject.put("mediation_config_cache_time_milliseconds", paramAdResponseParcel.zzclc);
      }
      if (!TextUtils.isEmpty(paramAdResponseParcel.zzclp)) {
        localJSONObject.put("gws_query_id", paramAdResponseParcel.zzclp);
      }
      if (!paramAdResponseParcel.zzazu) {
        break label555;
      }
    }
    label530:
    label555:
    for (String str = "height";; str = "")
    {
      localJSONObject.put("fluid", str);
      localJSONObject.put("native_express", paramAdResponseParcel.zzazv);
      if (paramAdResponseParcel.zzclr != null) {
        localJSONObject.put("video_start_urls", zzl(paramAdResponseParcel.zzclr));
      }
      if (paramAdResponseParcel.zzcls != null) {
        localJSONObject.put("video_complete_urls", zzl(paramAdResponseParcel.zzcls));
      }
      if (paramAdResponseParcel.zzclq != null) {
        localJSONObject.put("rewards", paramAdResponseParcel.zzclq.zzue());
      }
      localJSONObject.put("use_displayed_impression", paramAdResponseParcel.zzclt);
      localJSONObject.put("auto_protection_configuration", paramAdResponseParcel.zzclu);
      localJSONObject.put("render_in_browser", paramAdResponseParcel.zzbvo);
      return localJSONObject;
      localJSONObject.put("ad_html", paramAdResponseParcel.body);
      break;
      if (paramAdResponseParcel.orientation != zzu.zzgo().zzvw()) {
        break label159;
      }
      localJSONObject.put("orientation", "landscape");
      break label159;
    }
  }
  
  private static Bundle zzg(Bundle paramBundle)
  {
    Bundle localBundle = new Bundle();
    localBundle.putString("runtime_free", Long.toString(paramBundle.getLong("runtime_free_memory", -1L)));
    localBundle.putString("runtime_max", Long.toString(paramBundle.getLong("runtime_max_memory", -1L)));
    localBundle.putString("runtime_total", Long.toString(paramBundle.getLong("runtime_total_memory", -1L)));
    paramBundle = (Debug.MemoryInfo)paramBundle.getParcelable("debug_memory_info");
    if (paramBundle != null)
    {
      localBundle.putString("debug_info_dalvik_private_dirty", Integer.toString(paramBundle.dalvikPrivateDirty));
      localBundle.putString("debug_info_dalvik_pss", Integer.toString(paramBundle.dalvikPss));
      localBundle.putString("debug_info_dalvik_shared_dirty", Integer.toString(paramBundle.dalvikSharedDirty));
      localBundle.putString("debug_info_native_private_dirty", Integer.toString(paramBundle.nativePrivateDirty));
      localBundle.putString("debug_info_native_pss", Integer.toString(paramBundle.nativePss));
      localBundle.putString("debug_info_native_shared_dirty", Integer.toString(paramBundle.nativeSharedDirty));
      localBundle.putString("debug_info_other_private_dirty", Integer.toString(paramBundle.otherPrivateDirty));
      localBundle.putString("debug_info_other_pss", Integer.toString(paramBundle.otherPss));
      localBundle.putString("debug_info_other_shared_dirty", Integer.toString(paramBundle.otherSharedDirty));
    }
    return localBundle;
  }
  
  @Nullable
  static JSONArray zzl(List<String> paramList)
    throws JSONException
  {
    JSONArray localJSONArray = new JSONArray();
    paramList = paramList.iterator();
    while (paramList.hasNext()) {
      localJSONArray.put((String)paramList.next());
    }
    return localJSONArray;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzjm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */