package com.google.android.gms.internal;

import java.util.concurrent.Future;

@zzji
public final class zzjn
{
  private final Object zzako = new Object();
  private String zzcec;
  private String zzcnz;
  private zzlq<zzjq> zzcoa = new zzlq();
  zzgh.zzc zzcob;
  public final zzfe zzcoc = new zzjn.1(this);
  public final zzfe zzcod = new zzjn.2(this);
  public final zzfe zzcoe = new zzjn.3(this);
  
  public zzjn(String paramString1, String paramString2)
  {
    this.zzcnz = paramString2;
    this.zzcec = paramString1;
  }
  
  public void zzb(zzgh.zzc paramzzc)
  {
    this.zzcob = paramzzc;
  }
  
  public zzgh.zzc zztj()
  {
    return this.zzcob;
  }
  
  public Future<zzjq> zztk()
  {
    return this.zzcoa;
  }
  
  public void zztl() {}
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzjn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */