package com.google.android.gms.internal;

import android.net.Uri;
import android.net.Uri.Builder;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import com.google.android.gms.ads.internal.request.AutoClickProtectionConfigurationParcel;
import com.google.android.gms.ads.internal.reward.mediation.client.RewardItemParcel;
import com.google.android.gms.ads.internal.safebrowsing.SafeBrowsingConfigParcel;
import com.google.android.gms.ads.internal.zzu;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

@zzji
public final class zzjo
{
  private int mOrientation = -1;
  private String zzbna;
  private boolean zzbwe = false;
  private final AdRequestInfoParcel zzbws;
  private List<String> zzchv;
  private String zzcog;
  private String zzcoh;
  private List<String> zzcoi;
  private String zzcoj;
  private String zzcok;
  private String zzcol;
  private List<String> zzcom;
  private long zzcon = -1L;
  private boolean zzcoo = false;
  private final long zzcop = -1L;
  private long zzcoq = -1L;
  private boolean zzcor = false;
  private boolean zzcos = false;
  private boolean zzcot = false;
  private boolean zzcou = true;
  private boolean zzcov = true;
  private String zzcow = "";
  private boolean zzcox = false;
  private RewardItemParcel zzcoy;
  private List<String> zzcoz;
  private List<String> zzcpa;
  private boolean zzcpb = false;
  private AutoClickProtectionConfigurationParcel zzcpc;
  private boolean zzcpd = false;
  private String zzcpe;
  private List<String> zzcpf;
  private boolean zzcpg;
  private String zzcph;
  private SafeBrowsingConfigParcel zzcpi;
  
  public zzjo(AdRequestInfoParcel paramAdRequestInfoParcel, String paramString)
  {
    this.zzcoh = paramString;
    this.zzbws = paramAdRequestInfoParcel;
  }
  
  private void zzaa(Map<String, List<String>> paramMap)
  {
    paramMap = (List)paramMap.get("X-Afma-Use-HTTPS");
    if ((paramMap != null) && (!paramMap.isEmpty())) {
      this.zzcot = Boolean.valueOf((String)paramMap.get(0)).booleanValue();
    }
  }
  
  private void zzab(Map<String, List<String>> paramMap)
  {
    paramMap = (List)paramMap.get("X-Afma-Content-Url-Opted-Out");
    if ((paramMap != null) && (!paramMap.isEmpty())) {
      this.zzcou = Boolean.valueOf((String)paramMap.get(0)).booleanValue();
    }
  }
  
  private void zzac(Map<String, List<String>> paramMap)
  {
    paramMap = (List)paramMap.get("X-Afma-Content-Vertical-Opted-Out");
    if ((paramMap != null) && (!paramMap.isEmpty())) {
      this.zzcov = Boolean.valueOf((String)paramMap.get(0)).booleanValue();
    }
  }
  
  private void zzad(Map<String, List<String>> paramMap)
  {
    paramMap = (List)paramMap.get("X-Afma-Gws-Query-Id");
    if ((paramMap != null) && (!paramMap.isEmpty())) {
      this.zzcow = ((String)paramMap.get(0));
    }
  }
  
  private void zzae(Map<String, List<String>> paramMap)
  {
    paramMap = zzd(paramMap, "X-Afma-Fluid");
    if ((paramMap != null) && (paramMap.equals("height"))) {
      this.zzcox = true;
    }
  }
  
  private void zzaf(Map<String, List<String>> paramMap)
  {
    this.zzbwe = "native_express".equals(zzd(paramMap, "X-Afma-Ad-Format"));
  }
  
  private void zzag(Map<String, List<String>> paramMap)
  {
    this.zzcoy = RewardItemParcel.zzct(zzd(paramMap, "X-Afma-Rewards"));
  }
  
  private void zzah(Map<String, List<String>> paramMap)
  {
    if (this.zzcoz != null) {
      return;
    }
    this.zzcoz = zzf(paramMap, "X-Afma-Reward-Video-Start-Urls");
  }
  
  private void zzai(Map<String, List<String>> paramMap)
  {
    if (this.zzcpa != null) {
      return;
    }
    this.zzcpa = zzf(paramMap, "X-Afma-Reward-Video-Complete-Urls");
  }
  
  private void zzaj(Map<String, List<String>> paramMap)
  {
    this.zzcpb |= zzg(paramMap, "X-Afma-Use-Displayed-Impression");
  }
  
  private void zzak(Map<String, List<String>> paramMap)
  {
    this.zzcpd |= zzg(paramMap, "X-Afma-Auto-Collect-Location");
  }
  
  private void zzal(Map<String, List<String>> paramMap)
  {
    paramMap = zzf(paramMap, "X-Afma-Remote-Ping-Urls");
    if (paramMap != null) {
      this.zzcpf = paramMap;
    }
  }
  
  private void zzam(Map<String, List<String>> paramMap)
  {
    paramMap = zzd(paramMap, "X-Afma-Auto-Protection-Configuration");
    if ((paramMap == null) || (TextUtils.isEmpty(paramMap)))
    {
      paramMap = Uri.parse("https://pagead2.googlesyndication.com/pagead/gen_204").buildUpon();
      paramMap.appendQueryParameter("id", "gmob-apps-blocked-navigation");
      if (!TextUtils.isEmpty(this.zzcok)) {
        paramMap.appendQueryParameter("debugDialog", this.zzcok);
      }
      boolean bool = ((Boolean)zzdr.zzbdf.get()).booleanValue();
      paramMap = String.valueOf(paramMap.toString());
      String str = String.valueOf("navigationURL");
      this.zzcpc = new AutoClickProtectionConfigurationParcel(bool, Arrays.asList(new String[] { String.valueOf(paramMap).length() + 18 + String.valueOf(str).length() + paramMap + "&" + str + "={NAVIGATION_URL}" }));
      return;
    }
    try
    {
      this.zzcpc = AutoClickProtectionConfigurationParcel.zzh(new JSONObject(paramMap));
      return;
    }
    catch (JSONException paramMap)
    {
      zzkx.zzc("Error parsing configuration JSON", paramMap);
      this.zzcpc = new AutoClickProtectionConfigurationParcel();
    }
  }
  
  private void zzan(Map<String, List<String>> paramMap)
  {
    this.zzcpe = zzd(paramMap, "Set-Cookie");
  }
  
  private void zzao(Map<String, List<String>> paramMap)
  {
    paramMap = zzd(paramMap, "X-Afma-Safe-Browsing");
    if (TextUtils.isEmpty(paramMap)) {
      return;
    }
    try
    {
      this.zzcpi = SafeBrowsingConfigParcel.zzj(new JSONObject(paramMap));
      return;
    }
    catch (JSONException paramMap)
    {
      zzkx.zzc("Error parsing safe browsing header", paramMap);
    }
  }
  
  static String zzd(Map<String, List<String>> paramMap, String paramString)
  {
    paramMap = (List)paramMap.get(paramString);
    if ((paramMap != null) && (!paramMap.isEmpty())) {
      return (String)paramMap.get(0);
    }
    return null;
  }
  
  static long zze(Map<String, List<String>> paramMap, String paramString)
  {
    paramMap = (List)paramMap.get(paramString);
    if ((paramMap != null) && (!paramMap.isEmpty()))
    {
      paramMap = (String)paramMap.get(0);
      try
      {
        float f = Float.parseFloat(paramMap);
        return (f * 1000.0F);
      }
      catch (NumberFormatException localNumberFormatException)
      {
        zzkx.zzdi(String.valueOf(paramString).length() + 36 + String.valueOf(paramMap).length() + "Could not parse float from " + paramString + " header: " + paramMap);
      }
    }
    return -1L;
  }
  
  static List<String> zzf(Map<String, List<String>> paramMap, String paramString)
  {
    paramMap = (List)paramMap.get(paramString);
    if ((paramMap != null) && (!paramMap.isEmpty()))
    {
      paramMap = (String)paramMap.get(0);
      if (paramMap != null) {
        return Arrays.asList(paramMap.trim().split("\\s+"));
      }
    }
    return null;
  }
  
  private boolean zzg(Map<String, List<String>> paramMap, String paramString)
  {
    paramMap = (List)paramMap.get(paramString);
    return (paramMap != null) && (!paramMap.isEmpty()) && (Boolean.valueOf((String)paramMap.get(0)).booleanValue());
  }
  
  private void zzl(Map<String, List<String>> paramMap)
  {
    this.zzcog = zzd(paramMap, "X-Afma-Ad-Size");
  }
  
  private void zzm(Map<String, List<String>> paramMap)
  {
    this.zzcph = zzd(paramMap, "X-Afma-Ad-Slot-Size");
  }
  
  private void zzn(Map<String, List<String>> paramMap)
  {
    paramMap = zzf(paramMap, "X-Afma-Click-Tracking-Urls");
    if (paramMap != null) {
      this.zzcoi = paramMap;
    }
  }
  
  private void zzo(Map<String, List<String>> paramMap)
  {
    this.zzcoj = zzd(paramMap, "X-Afma-Debug-Signals");
  }
  
  private void zzp(Map<String, List<String>> paramMap)
  {
    paramMap = (List)paramMap.get("X-Afma-Debug-Dialog");
    if ((paramMap != null) && (!paramMap.isEmpty())) {
      this.zzcok = ((String)paramMap.get(0));
    }
  }
  
  private void zzq(Map<String, List<String>> paramMap)
  {
    paramMap = zzf(paramMap, "X-Afma-Tracking-Urls");
    if (paramMap != null) {
      this.zzcom = paramMap;
    }
  }
  
  private void zzr(Map<String, List<String>> paramMap)
  {
    long l = zze(paramMap, "X-Afma-Interstitial-Timeout");
    if (l != -1L) {
      this.zzcon = l;
    }
  }
  
  private void zzs(Map<String, List<String>> paramMap)
  {
    this.zzcol = zzd(paramMap, "X-Afma-ActiveView");
  }
  
  private void zzt(Map<String, List<String>> paramMap)
  {
    this.zzcos = "native".equals(zzd(paramMap, "X-Afma-Ad-Format"));
  }
  
  private void zzu(Map<String, List<String>> paramMap)
  {
    this.zzcor |= zzg(paramMap, "X-Afma-Custom-Rendering-Allowed");
  }
  
  private void zzv(Map<String, List<String>> paramMap)
  {
    this.zzcoo |= zzg(paramMap, "X-Afma-Mediation");
  }
  
  private void zzw(Map<String, List<String>> paramMap)
  {
    this.zzcpg |= zzg(paramMap, "X-Afma-Render-In-Browser");
  }
  
  private void zzx(Map<String, List<String>> paramMap)
  {
    paramMap = zzf(paramMap, "X-Afma-Manual-Tracking-Urls");
    if (paramMap != null) {
      this.zzchv = paramMap;
    }
  }
  
  private void zzy(Map<String, List<String>> paramMap)
  {
    long l = zze(paramMap, "X-Afma-Refresh-Rate");
    if (l != -1L) {
      this.zzcoq = l;
    }
  }
  
  private void zzz(Map<String, List<String>> paramMap)
  {
    paramMap = (List)paramMap.get("X-Afma-Orientation");
    if ((paramMap != null) && (!paramMap.isEmpty()))
    {
      paramMap = (String)paramMap.get(0);
      if (!"portrait".equalsIgnoreCase(paramMap)) {
        break label58;
      }
      this.mOrientation = zzu.zzgo().zzvx();
    }
    label58:
    while (!"landscape".equalsIgnoreCase(paramMap)) {
      return;
    }
    this.mOrientation = zzu.zzgo().zzvw();
  }
  
  public void zzb(String paramString1, Map<String, List<String>> paramMap, String paramString2)
  {
    this.zzbna = paramString2;
    zzk(paramMap);
  }
  
  public AdResponseParcel zzj(long paramLong)
  {
    return new AdResponseParcel(this.zzbws, this.zzcoh, this.zzbna, this.zzcoi, this.zzcom, this.zzcon, this.zzcoo, -1L, this.zzchv, this.zzcoq, this.mOrientation, this.zzcog, paramLong, this.zzcok, this.zzcol, this.zzcor, this.zzcos, this.zzcot, this.zzcou, false, this.zzcow, this.zzcox, this.zzbwe, this.zzcoy, this.zzcoz, this.zzcpa, this.zzcpb, this.zzcpc, this.zzcpd, this.zzcpe, this.zzcpf, this.zzcpg, this.zzcph, this.zzcpi, this.zzcoj, this.zzcov);
  }
  
  public void zzk(Map<String, List<String>> paramMap)
  {
    zzl(paramMap);
    zzm(paramMap);
    zzn(paramMap);
    zzo(paramMap);
    zzp(paramMap);
    zzq(paramMap);
    zzr(paramMap);
    zzv(paramMap);
    zzx(paramMap);
    zzy(paramMap);
    zzz(paramMap);
    zzs(paramMap);
    zzaa(paramMap);
    zzu(paramMap);
    zzt(paramMap);
    zzab(paramMap);
    zzac(paramMap);
    zzad(paramMap);
    zzae(paramMap);
    zzaf(paramMap);
    zzag(paramMap);
    zzah(paramMap);
    zzai(paramMap);
    zzaj(paramMap);
    zzak(paramMap);
    zzan(paramMap);
    zzam(paramMap);
    zzal(paramMap);
    zzao(paramMap);
    zzw(paramMap);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzjo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */