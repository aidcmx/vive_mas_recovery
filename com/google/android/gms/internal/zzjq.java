package com.google.android.gms.internal;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@zzji
class zzjq
{
  private String zzae;
  private final String zzcec;
  private int zzcgw;
  private final List<String> zzcpj;
  private final List<String> zzcpk;
  private final String zzcpl;
  private final String zzcpm;
  private final String zzcpn;
  private final String zzcpo;
  private final boolean zzcpp;
  private final boolean zzcpq;
  private final String zzcpr;
  
  public zzjq(int paramInt, Map<String, String> paramMap)
  {
    this.zzae = ((String)paramMap.get("url"));
    this.zzcpm = ((String)paramMap.get("base_uri"));
    this.zzcpn = ((String)paramMap.get("post_parameters"));
    this.zzcpp = parseBoolean((String)paramMap.get("drt_include"));
    this.zzcpq = parseBoolean((String)paramMap.get("pan_include"));
    this.zzcpl = ((String)paramMap.get("activation_overlay_url"));
    this.zzcpk = zzco((String)paramMap.get("check_packages"));
    this.zzcec = ((String)paramMap.get("request_id"));
    this.zzcpo = ((String)paramMap.get("type"));
    this.zzcpj = zzco((String)paramMap.get("errors"));
    this.zzcgw = paramInt;
    this.zzcpr = ((String)paramMap.get("fetched_ad"));
  }
  
  private static boolean parseBoolean(String paramString)
  {
    return (paramString != null) && ((paramString.equals("1")) || (paramString.equals("true")));
  }
  
  private List<String> zzco(String paramString)
  {
    if (paramString == null) {
      return null;
    }
    return Arrays.asList(paramString.split(","));
  }
  
  public int getErrorCode()
  {
    return this.zzcgw;
  }
  
  public String getRequestId()
  {
    return this.zzcec;
  }
  
  public String getType()
  {
    return this.zzcpo;
  }
  
  public String getUrl()
  {
    return this.zzae;
  }
  
  public void setUrl(String paramString)
  {
    this.zzae = paramString;
  }
  
  public List<String> zztm()
  {
    return this.zzcpj;
  }
  
  public String zztn()
  {
    return this.zzcpm;
  }
  
  public String zzto()
  {
    return this.zzcpn;
  }
  
  public boolean zztp()
  {
    return this.zzcpp;
  }
  
  public String zztq()
  {
    return this.zzcpr;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzjq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */