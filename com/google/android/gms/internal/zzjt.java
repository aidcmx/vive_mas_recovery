package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;

@zzji
public abstract class zzjt
{
  public abstract void zza(Context paramContext, zzjn paramzzjn, VersionInfoParcel paramVersionInfoParcel);
  
  protected void zze(zzjn paramzzjn)
  {
    paramzzjn.zztl();
    if (paramzzjn.zztj() != null) {
      paramzzjn.zztj().release();
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzjt.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */