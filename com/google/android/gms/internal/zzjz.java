package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.reward.client.zza.zza;
import com.google.android.gms.common.internal.zzz;

@zzji
public class zzjz
  extends zza.zza
{
  private final String zzcpo;
  private final int zzcqw;
  
  public zzjz(String paramString, int paramInt)
  {
    this.zzcpo = paramString;
    this.zzcqw = paramInt;
  }
  
  public boolean equals(Object paramObject)
  {
    if ((paramObject == null) || (!(paramObject instanceof zzjz))) {}
    do
    {
      return false;
      paramObject = (zzjz)paramObject;
    } while ((!zzz.equal(getType(), ((zzjz)paramObject).getType())) || (!zzz.equal(Integer.valueOf(getAmount()), Integer.valueOf(((zzjz)paramObject).getAmount()))));
    return true;
  }
  
  public int getAmount()
  {
    return this.zzcqw;
  }
  
  public String getType()
  {
    return this.zzcpo;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzjz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */