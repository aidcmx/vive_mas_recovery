package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.reward.client.RewardedVideoAdRequestParcel;
import com.google.android.gms.ads.internal.reward.client.zzb.zza;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.dynamic.zze;

@zzji
public class zzka
  extends zzb.zza
{
  private final Context mContext;
  private final Object zzako = new Object();
  private final VersionInfoParcel zzanu;
  private final zzkb zzcqx;
  
  zzka(Context paramContext, VersionInfoParcel paramVersionInfoParcel, zzkb paramzzkb)
  {
    this.mContext = paramContext;
    this.zzanu = paramVersionInfoParcel;
    this.zzcqx = paramzzkb;
  }
  
  public zzka(Context paramContext, com.google.android.gms.ads.internal.zzd paramzzd, zzgz paramzzgz, VersionInfoParcel paramVersionInfoParcel)
  {
    this(paramContext, paramVersionInfoParcel, new zzkb(paramContext, paramzzd, AdSizeParcel.zzkc(), paramzzgz, paramVersionInfoParcel));
  }
  
  public void destroy()
  {
    zzh(null);
  }
  
  public boolean isLoaded()
  {
    synchronized (this.zzako)
    {
      boolean bool = this.zzcqx.isLoaded();
      return bool;
    }
  }
  
  public void pause()
  {
    zzf(null);
  }
  
  public void resume()
  {
    zzg(null);
  }
  
  public void setUserId(String paramString)
  {
    zzkx.zzdi("RewardedVideoAd.setUserId() is deprecated. Please do not call this method.");
  }
  
  public void show()
  {
    synchronized (this.zzako)
    {
      this.zzcqx.zztu();
      return;
    }
  }
  
  public void zza(RewardedVideoAdRequestParcel paramRewardedVideoAdRequestParcel)
  {
    synchronized (this.zzako)
    {
      this.zzcqx.zza(paramRewardedVideoAdRequestParcel);
      return;
    }
  }
  
  public void zza(com.google.android.gms.ads.internal.reward.client.zzd paramzzd)
  {
    synchronized (this.zzako)
    {
      this.zzcqx.zza(paramzzd);
      return;
    }
  }
  
  public void zzf(com.google.android.gms.dynamic.zzd arg1)
  {
    synchronized (this.zzako)
    {
      this.zzcqx.pause();
      return;
    }
  }
  
  public void zzg(com.google.android.gms.dynamic.zzd paramzzd)
  {
    Object localObject = this.zzako;
    if (paramzzd == null) {
      paramzzd = null;
    }
    for (;;)
    {
      if (paramzzd != null) {}
      try
      {
        this.zzcqx.onContextChanged(paramzzd);
        this.zzcqx.resume();
        return;
        paramzzd = (Context)zze.zzae(paramzzd);
      }
      catch (Exception paramzzd)
      {
        for (;;)
        {
          zzkx.zzc("Unable to extract updated context.", paramzzd);
        }
      }
      finally {}
    }
  }
  
  public void zzh(com.google.android.gms.dynamic.zzd arg1)
  {
    synchronized (this.zzako)
    {
      this.zzcqx.destroy();
      return;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzka.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */