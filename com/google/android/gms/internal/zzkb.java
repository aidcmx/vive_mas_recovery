package com.google.android.gms.internal;

import android.content.Context;
import android.os.Handler;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.reward.client.RewardedVideoAdRequestParcel;
import com.google.android.gms.ads.internal.reward.mediation.client.RewardItemParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zzb;
import com.google.android.gms.ads.internal.zzd;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.ads.internal.zzv;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.dynamic.zze;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

@zzji
public class zzkb
  extends zzb
  implements zzkf
{
  private static final zzgy zzcqy = new zzgy();
  private final Map<String, zzkj> zzcqz = new HashMap();
  private boolean zzcra;
  
  public zzkb(Context paramContext, zzd paramzzd, AdSizeParcel paramAdSizeParcel, zzgz paramzzgz, VersionInfoParcel paramVersionInfoParcel)
  {
    super(paramContext, paramAdSizeParcel, null, paramzzgz, paramVersionInfoParcel, paramzzd);
  }
  
  private zzko.zza zzd(zzko.zza paramzza)
  {
    zzkx.v("Creating mediation ad response for non-mediated rewarded ad.");
    try
    {
      Object localObject1 = zzjm.zzc(paramzza.zzcsu).toString();
      Object localObject2 = new JSONObject();
      ((JSONObject)localObject2).put("pubid", paramzza.zzcmx.zzarg);
      localObject2 = ((JSONObject)localObject2).toString();
      localObject1 = new zzgq(Arrays.asList(new zzgp[] { new zzgp((String)localObject1, null, Arrays.asList(new String[] { "com.google.ads.mediation.admob.AdMobAdapter" }), null, null, Collections.emptyList(), Collections.emptyList(), (String)localObject2, null, Collections.emptyList(), Collections.emptyList(), null, null, null, null, null, Collections.emptyList()) }), -1L, Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), false, "", -1L, 0, 1, null, 0, -1, -1L, false);
      return new zzko.zza(paramzza.zzcmx, paramzza.zzcsu, (zzgq)localObject1, paramzza.zzarm, paramzza.errorCode, paramzza.zzcso, paramzza.zzcsp, paramzza.zzcsi);
    }
    catch (JSONException localJSONException)
    {
      zzkx.zzb("Unable to generate ad state for non-mediated rewarded video.", localJSONException);
    }
    return zze(paramzza);
  }
  
  private zzko.zza zze(zzko.zza paramzza)
  {
    return new zzko.zza(paramzza.zzcmx, paramzza.zzcsu, null, paramzza.zzarm, 0, paramzza.zzcso, paramzza.zzcsp, paramzza.zzcsi);
  }
  
  public void destroy()
  {
    zzaa.zzhs("destroy must be called on the main UI thread.");
    Iterator localIterator = this.zzcqz.keySet().iterator();
    for (;;)
    {
      if (localIterator.hasNext())
      {
        String str = (String)localIterator.next();
        try
        {
          zzkj localzzkj = (zzkj)this.zzcqz.get(str);
          if ((localzzkj != null) && (localzzkj.zzuc() != null)) {
            localzzkj.zzuc().destroy();
          }
        }
        catch (RemoteException localRemoteException)
        {
          str = String.valueOf(str);
          if (str.length() != 0) {}
          for (str = "Fail to destroy adapter: ".concat(str);; str = new String("Fail to destroy adapter: "))
          {
            zzkx.zzdi(str);
            break;
          }
        }
      }
    }
  }
  
  public boolean isLoaded()
  {
    zzaa.zzhs("isLoaded must be called on the main UI thread.");
    return (this.zzaly.zzark == null) && (this.zzaly.zzarl == null) && (this.zzaly.zzarn != null) && (!this.zzcra);
  }
  
  public void onContextChanged(@NonNull Context paramContext)
  {
    Iterator localIterator = this.zzcqz.values().iterator();
    while (localIterator.hasNext())
    {
      zzkj localzzkj = (zzkj)localIterator.next();
      try
      {
        localzzkj.zzuc().zzj(zze.zzac(paramContext));
      }
      catch (RemoteException localRemoteException)
      {
        zzkx.zzb("Unable to call Adapter.onContextChanged.", localRemoteException);
      }
    }
  }
  
  public void onRewardedVideoAdClosed()
  {
    zzek();
  }
  
  public void onRewardedVideoAdLeftApplication()
  {
    zzel();
  }
  
  public void onRewardedVideoAdOpened()
  {
    zza(this.zzaly.zzarn, false);
    zzem();
  }
  
  public void onRewardedVideoStarted()
  {
    if ((this.zzaly.zzarn != null) && (this.zzaly.zzarn.zzbwm != null)) {
      zzu.zzhf().zza(this.zzaly.zzahs, this.zzaly.zzari.zzda, this.zzaly.zzarn, this.zzaly.zzarg, false, this.zzaly.zzarn.zzbwm.zzbvb);
    }
    zzeo();
  }
  
  public void pause()
  {
    zzaa.zzhs("pause must be called on the main UI thread.");
    Iterator localIterator = this.zzcqz.keySet().iterator();
    for (;;)
    {
      if (localIterator.hasNext())
      {
        String str = (String)localIterator.next();
        try
        {
          zzkj localzzkj = (zzkj)this.zzcqz.get(str);
          if ((localzzkj != null) && (localzzkj.zzuc() != null)) {
            localzzkj.zzuc().pause();
          }
        }
        catch (RemoteException localRemoteException)
        {
          str = String.valueOf(str);
          if (str.length() != 0) {}
          for (str = "Fail to pause adapter: ".concat(str);; str = new String("Fail to pause adapter: "))
          {
            zzkx.zzdi(str);
            break;
          }
        }
      }
    }
  }
  
  public void resume()
  {
    zzaa.zzhs("resume must be called on the main UI thread.");
    Iterator localIterator = this.zzcqz.keySet().iterator();
    for (;;)
    {
      if (localIterator.hasNext())
      {
        String str = (String)localIterator.next();
        try
        {
          zzkj localzzkj = (zzkj)this.zzcqz.get(str);
          if ((localzzkj != null) && (localzzkj.zzuc() != null)) {
            localzzkj.zzuc().resume();
          }
        }
        catch (RemoteException localRemoteException)
        {
          str = String.valueOf(str);
          if (str.length() != 0) {}
          for (str = "Fail to resume adapter: ".concat(str);; str = new String("Fail to resume adapter: "))
          {
            zzkx.zzdi(str);
            break;
          }
        }
      }
    }
  }
  
  public void zza(RewardedVideoAdRequestParcel paramRewardedVideoAdRequestParcel)
  {
    zzaa.zzhs("loadAd must be called on the main UI thread.");
    if (TextUtils.isEmpty(paramRewardedVideoAdRequestParcel.zzarg))
    {
      zzkx.zzdi("Invalid ad unit id. Aborting.");
      zzlb.zzcvl.post(new zzkb.1(this));
      return;
    }
    this.zzcra = false;
    this.zzaly.zzarg = paramRewardedVideoAdRequestParcel.zzarg;
    super.zzb(paramRewardedVideoAdRequestParcel.zzcju);
  }
  
  public void zza(zzko.zza paramzza, zzdz paramzzdz)
  {
    if (paramzza.errorCode != -2)
    {
      zzlb.zzcvl.post(new zzkb.2(this, paramzza));
      return;
    }
    this.zzaly.zzaro = paramzza;
    if (paramzza.zzcsk == null) {
      this.zzaly.zzaro = zzd(paramzza);
    }
    this.zzaly.zzasi = 0;
    this.zzaly.zzarl = zzu.zzgl().zza(this.zzaly.zzahs, this.zzaly.zzaro, this);
  }
  
  protected boolean zza(AdRequestParcel paramAdRequestParcel, zzko paramzzko, boolean paramBoolean)
  {
    return false;
  }
  
  public boolean zza(zzko paramzzko1, zzko paramzzko2)
  {
    return true;
  }
  
  public void zzc(@Nullable RewardItemParcel paramRewardItemParcel)
  {
    if ((this.zzaly.zzarn != null) && (this.zzaly.zzarn.zzbwm != null)) {
      zzu.zzhf().zza(this.zzaly.zzahs, this.zzaly.zzari.zzda, this.zzaly.zzarn, this.zzaly.zzarg, false, this.zzaly.zzarn.zzbwm.zzbvc);
    }
    RewardItemParcel localRewardItemParcel = paramRewardItemParcel;
    if (this.zzaly.zzarn != null)
    {
      localRewardItemParcel = paramRewardItemParcel;
      if (this.zzaly.zzarn.zzcsk != null)
      {
        localRewardItemParcel = paramRewardItemParcel;
        if (!TextUtils.isEmpty(this.zzaly.zzarn.zzcsk.zzbvr)) {
          localRewardItemParcel = new RewardItemParcel(this.zzaly.zzarn.zzcsk.zzbvr, this.zzaly.zzarn.zzcsk.zzbvs);
        }
      }
    }
    zza(localRewardItemParcel);
  }
  
  @Nullable
  public zzkj zzcp(String paramString)
  {
    Object localObject1 = (zzkj)this.zzcqz.get(paramString);
    Object localObject2 = localObject1;
    if (localObject1 == null) {}
    try
    {
      localObject2 = this.zzamf;
      if ("com.google.ads.mediation.admob.AdMobAdapter".equals(paramString))
      {
        localObject2 = zzcqy;
        localObject2 = new zzkj(((zzgz)localObject2).zzbu(paramString), this);
      }
    }
    catch (Exception localException1)
    {
      for (;;)
      {
        try
        {
          this.zzcqz.put(paramString, localObject2);
          return (zzkj)localObject2;
        }
        catch (Exception localException2)
        {
          localObject1 = localException1;
          Object localObject3 = localException2;
          continue;
        }
        localException1 = localException1;
        paramString = String.valueOf(paramString);
        if (paramString.length() != 0)
        {
          paramString = "Fail to instantiate adapter ".concat(paramString);
          zzkx.zzc(paramString, localException1);
          return (zzkj)localObject1;
        }
        paramString = new String("Fail to instantiate adapter ");
      }
    }
  }
  
  public void zztu()
  {
    zzaa.zzhs("showAd must be called on the main UI thread.");
    if (!isLoaded()) {
      zzkx.zzdi("The reward video has not loaded.");
    }
    zzkj localzzkj;
    do
    {
      return;
      this.zzcra = true;
      localzzkj = zzcp(this.zzaly.zzarn.zzbwo);
    } while ((localzzkj == null) || (localzzkj.zzuc() == null));
    try
    {
      localzzkj.zzuc().showVideo();
      return;
    }
    catch (RemoteException localRemoteException)
    {
      zzkx.zzc("Could not call showVideo.", localRemoteException);
    }
  }
  
  public void zztv()
  {
    onAdClicked();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzkb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */