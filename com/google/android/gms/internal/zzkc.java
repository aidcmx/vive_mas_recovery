package com.google.android.gms.internal;

import android.content.Context;
import android.os.Handler;
import android.os.RemoteException;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.common.util.zze;

@zzji
public class zzkc
  extends zzkw
  implements zzke, zzkh
{
  private final Context mContext;
  private final Object zzako;
  private final String zzbwa;
  private final zzko.zza zzcgf;
  private int zzcgw = 3;
  private final zzkj zzcre;
  private final zzkh zzcrf;
  private final String zzcrg;
  private final zzgp zzcrh;
  private final long zzcri;
  private int zzcrj = 0;
  private zzkd zzcrk;
  
  public zzkc(Context paramContext, String paramString1, String paramString2, zzgp paramzzgp, zzko.zza paramzza, zzkj paramzzkj, zzkh paramzzkh, long paramLong)
  {
    this.mContext = paramContext;
    this.zzbwa = paramString1;
    this.zzcrg = paramString2;
    this.zzcrh = paramzzgp;
    this.zzcgf = paramzza;
    this.zzcre = paramzzkj;
    this.zzako = new Object();
    this.zzcrf = paramzzkh;
    this.zzcri = paramLong;
  }
  
  private void zza(AdRequestParcel paramAdRequestParcel, zzha paramzzha)
  {
    this.zzcre.zzud().zza(this);
    try
    {
      if ("com.google.ads.mediation.admob.AdMobAdapter".equals(this.zzbwa))
      {
        paramzzha.zza(paramAdRequestParcel, this.zzcrg, this.zzcrh.zzbus);
        return;
      }
      paramzzha.zzc(paramAdRequestParcel, this.zzcrg);
      return;
    }
    catch (RemoteException paramAdRequestParcel)
    {
      zzkx.zzc("Fail to load ad from adapter.", paramAdRequestParcel);
      zza(this.zzbwa, 0);
    }
  }
  
  private void zzk(long paramLong)
  {
    for (;;)
    {
      synchronized (this.zzako)
      {
        if (this.zzcrj != 0)
        {
          zzkd.zza localzza = new zzkd.zza().zzl(zzu.zzgs().elapsedRealtime() - paramLong);
          if (1 == this.zzcrj)
          {
            i = 6;
            this.zzcrk = localzza.zzbc(i).zzcr(this.zzbwa).zzcs(this.zzcrh.zzbuv).zztz();
            return;
          }
          int i = this.zzcgw;
          continue;
        }
        if (!zzf(paramLong))
        {
          this.zzcrk = new zzkd.zza().zzbc(this.zzcgw).zzl(zzu.zzgs().elapsedRealtime() - paramLong).zzcr(this.zzbwa).zzcs(this.zzcrh.zzbuv).zztz();
          return;
        }
      }
    }
  }
  
  public void onStop() {}
  
  public void zza(String arg1, int paramInt)
  {
    synchronized (this.zzako)
    {
      this.zzcrj = 2;
      this.zzcgw = paramInt;
      this.zzako.notify();
      return;
    }
  }
  
  public void zzbb(int paramInt)
  {
    zza(this.zzbwa, 0);
  }
  
  public void zzcq(String arg1)
  {
    synchronized (this.zzako)
    {
      this.zzcrj = 1;
      this.zzako.notify();
      return;
    }
  }
  
  protected boolean zzf(long paramLong)
  {
    paramLong = this.zzcri - (zzu.zzgs().elapsedRealtime() - paramLong);
    if (paramLong <= 0L)
    {
      this.zzcgw = 4;
      return false;
    }
    try
    {
      this.zzako.wait(paramLong);
      return true;
    }
    catch (InterruptedException localInterruptedException)
    {
      Thread.currentThread().interrupt();
      this.zzcgw = 5;
    }
    return false;
  }
  
  public void zzfp()
  {
    if ((this.zzcre == null) || (this.zzcre.zzud() == null) || (this.zzcre.zzuc() == null)) {
      return;
    }
    zzkg localzzkg = this.zzcre.zzud();
    localzzkg.zza(null);
    localzzkg.zza(this);
    AdRequestParcel localAdRequestParcel = this.zzcgf.zzcmx.zzcju;
    zzha localzzha = this.zzcre.zzuc();
    try
    {
      if (localzzha.isInitialized()) {
        zza.zzcxr.post(new zzkc.1(this, localAdRequestParcel, localzzha));
      }
      for (;;)
      {
        zzk(zzu.zzgs().elapsedRealtime());
        localzzkg.zza(null);
        localzzkg.zza(null);
        if (this.zzcrj != 1) {
          break;
        }
        this.zzcrf.zzcq(this.zzbwa);
        return;
        zza.zzcxr.post(new zzkc.2(this, localzzha, localAdRequestParcel, localzzkg));
      }
    }
    catch (RemoteException localRemoteException)
    {
      for (;;)
      {
        zzkx.zzc("Fail to check if adapter is initialized.", localRemoteException);
        zza(this.zzbwa, 0);
      }
      this.zzcrf.zza(this.zzbwa, this.zzcgw);
    }
  }
  
  public zzkd zztw()
  {
    synchronized (this.zzako)
    {
      zzkd localzzkd = this.zzcrk;
      return localzzkd;
    }
  }
  
  public zzgp zztx()
  {
    return this.zzcrh;
  }
  
  public void zzty()
  {
    zza(this.zzcgf.zzcmx.zzcju, this.zzcre.zzuc());
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzkc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */