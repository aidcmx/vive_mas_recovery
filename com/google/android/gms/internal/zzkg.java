package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.reward.mediation.client.RewardItemParcel;
import com.google.android.gms.ads.internal.reward.mediation.client.zza.zza;
import com.google.android.gms.dynamic.zzd;
import com.google.android.gms.dynamic.zze;

@zzji
public class zzkg
  extends zza.zza
{
  private volatile zzkh zzcrf;
  private volatile zzke zzcrr;
  private volatile zzkf zzcrs;
  
  public zzkg(zzkf paramzzkf)
  {
    this.zzcrs = paramzzkf;
  }
  
  public void zza(zzd paramzzd, RewardItemParcel paramRewardItemParcel)
  {
    if (this.zzcrs != null) {
      this.zzcrs.zzc(paramRewardItemParcel);
    }
  }
  
  public void zza(zzke paramzzke)
  {
    this.zzcrr = paramzzke;
  }
  
  public void zza(zzkh paramzzkh)
  {
    this.zzcrf = paramzzkh;
  }
  
  public void zzb(zzd paramzzd, int paramInt)
  {
    if (this.zzcrr != null) {
      this.zzcrr.zzbb(paramInt);
    }
  }
  
  public void zzc(zzd paramzzd, int paramInt)
  {
    if (this.zzcrf != null) {
      this.zzcrf.zza(zze.zzae(paramzzd).getClass().getName(), paramInt);
    }
  }
  
  public void zzq(zzd paramzzd)
  {
    if (this.zzcrr != null) {
      this.zzcrr.zzty();
    }
  }
  
  public void zzr(zzd paramzzd)
  {
    if (this.zzcrf != null) {
      this.zzcrf.zzcq(zze.zzae(paramzzd).getClass().getName());
    }
  }
  
  public void zzs(zzd paramzzd)
  {
    if (this.zzcrs != null) {
      this.zzcrs.onRewardedVideoAdOpened();
    }
  }
  
  public void zzt(zzd paramzzd)
  {
    if (this.zzcrs != null) {
      this.zzcrs.onRewardedVideoStarted();
    }
  }
  
  public void zzu(zzd paramzzd)
  {
    if (this.zzcrs != null) {
      this.zzcrs.onRewardedVideoAdClosed();
    }
  }
  
  public void zzv(zzd paramzzd)
  {
    if (this.zzcrs != null) {
      this.zzcrs.zztv();
    }
  }
  
  public void zzw(zzd paramzzd)
  {
    if (this.zzcrs != null) {
      this.zzcrs.onRewardedVideoAdLeftApplication();
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzkg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */