package com.google.android.gms.internal;

import android.content.Context;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Future;

@zzji
public class zzki
  extends zzkw
  implements zzkh
{
  private final Context mContext;
  private final Object zzako = new Object();
  private final zzko.zza zzcgf;
  private final long zzcri;
  private final ArrayList<Future> zzcrt = new ArrayList();
  private final ArrayList<String> zzcru = new ArrayList();
  private final HashMap<String, zzkc> zzcrv = new HashMap();
  private final List<zzkd> zzcrw = new ArrayList();
  private final HashSet<String> zzcrx = new HashSet();
  private final zzkb zzcry;
  
  public zzki(Context paramContext, zzko.zza paramzza, zzkb paramzzkb)
  {
    this(paramContext, paramzza, paramzzkb, ((Long)zzdr.zzbfy.get()).longValue());
  }
  
  zzki(Context paramContext, zzko.zza paramzza, zzkb paramzzkb, long paramLong)
  {
    this.mContext = paramContext;
    this.zzcgf = paramzza;
    this.zzcry = paramzzkb;
    this.zzcri = paramLong;
  }
  
  private zzko zza(int paramInt, @Nullable String paramString, @Nullable zzgp paramzzgp)
  {
    return new zzko(this.zzcgf.zzcmx.zzcju, null, this.zzcgf.zzcsu.zzbvk, paramInt, this.zzcgf.zzcsu.zzbvl, this.zzcgf.zzcsu.zzcld, this.zzcgf.zzcsu.orientation, this.zzcgf.zzcsu.zzbvq, this.zzcgf.zzcmx.zzcjx, this.zzcgf.zzcsu.zzclb, paramzzgp, null, paramString, this.zzcgf.zzcsk, null, this.zzcgf.zzcsu.zzclc, this.zzcgf.zzarm, this.zzcgf.zzcsu.zzcla, this.zzcgf.zzcso, this.zzcgf.zzcsu.zzclf, this.zzcgf.zzcsu.zzclg, this.zzcgf.zzcsi, null, this.zzcgf.zzcsu.zzclq, this.zzcgf.zzcsu.zzclr, this.zzcgf.zzcsu.zzcls, this.zzcgf.zzcsu.zzclt, this.zzcgf.zzcsu.zzclu, zzub(), this.zzcgf.zzcsu.zzbvn, this.zzcgf.zzcsu.zzclx);
  }
  
  private zzko zza(String paramString, zzgp paramzzgp)
  {
    return zza(-2, paramString, paramzzgp);
  }
  
  private static String zza(zzkd paramzzkd)
  {
    String str = paramzzkd.zzbuv;
    int i = zzar(paramzzkd.errorCode);
    long l = paramzzkd.zzbwr;
    return String.valueOf(str).length() + 33 + str + "." + i + "." + l;
  }
  
  private void zza(String paramString1, String paramString2, zzgp paramzzgp)
  {
    synchronized (this.zzako)
    {
      zzkj localzzkj = this.zzcry.zzcp(paramString1);
      if ((localzzkj == null) || (localzzkj.zzud() == null) || (localzzkj.zzuc() == null))
      {
        this.zzcrw.add(new zzkd.zza().zzcs(paramzzgp.zzbuv).zzcr(paramString1).zzl(0L).zzbc(7).zztz());
        return;
      }
      paramString2 = zza(paramString1, paramString2, paramzzgp, localzzkj);
      this.zzcrt.add((Future)paramString2.zzrz());
      this.zzcru.add(paramString1);
      this.zzcrv.put(paramString1, paramString2);
      return;
    }
  }
  
  private static int zzar(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return 6;
    case 6: 
      return 0;
    case 3: 
      return 1;
    case 4: 
      return 2;
    case 7: 
      return 3;
    }
    return 4;
  }
  
  private zzko zzua()
  {
    return zza(3, null, null);
  }
  
  private String zzub()
  {
    StringBuilder localStringBuilder = new StringBuilder("");
    if (this.zzcrw == null) {
      return localStringBuilder.toString();
    }
    Iterator localIterator = this.zzcrw.iterator();
    while (localIterator.hasNext())
    {
      zzkd localzzkd = (zzkd)localIterator.next();
      if ((localzzkd != null) && (!TextUtils.isEmpty(localzzkd.zzbuv))) {
        localStringBuilder.append(String.valueOf(zza(localzzkd)).concat("_"));
      }
    }
    return localStringBuilder.substring(0, Math.max(0, localStringBuilder.length() - 1));
  }
  
  public void onStop() {}
  
  protected zzkc zza(String paramString1, String paramString2, zzgp paramzzgp, zzkj paramzzkj)
  {
    return new zzkc(this.mContext, paramString1, paramString2, paramzzgp, this.zzcgf, paramzzkj, this, this.zzcri);
  }
  
  public void zza(String paramString, int paramInt) {}
  
  public void zzcq(String paramString)
  {
    synchronized (this.zzako)
    {
      this.zzcrx.add(paramString);
      return;
    }
  }
  
  /* Error */
  public void zzfp()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 86	com/google/android/gms/internal/zzki:zzcgf	Lcom/google/android/gms/internal/zzko$zza;
    //   4: getfield 142	com/google/android/gms/internal/zzko$zza:zzcsk	Lcom/google/android/gms/internal/zzgq;
    //   7: getfield 372	com/google/android/gms/internal/zzgq:zzbvi	Ljava/util/List;
    //   10: invokeinterface 317 1 0
    //   15: astore 4
    //   17: aload 4
    //   19: invokeinterface 323 1 0
    //   24: ifeq +116 -> 140
    //   27: aload 4
    //   29: invokeinterface 326 1 0
    //   34: checkcast 267	com/google/android/gms/internal/zzgp
    //   37: astore 5
    //   39: aload 5
    //   41: getfield 375	com/google/android/gms/internal/zzgp:zzbva	Ljava/lang/String;
    //   44: astore 6
    //   46: aload 5
    //   48: getfield 378	com/google/android/gms/internal/zzgp:zzbuu	Ljava/util/List;
    //   51: invokeinterface 317 1 0
    //   56: astore 7
    //   58: aload 7
    //   60: invokeinterface 323 1 0
    //   65: ifeq -48 -> 17
    //   68: aload 7
    //   70: invokeinterface 326 1 0
    //   75: checkcast 219	java/lang/String
    //   78: astore_3
    //   79: ldc_w 380
    //   82: aload_3
    //   83: invokevirtual 383	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   86: ifne +15 -> 101
    //   89: aload_3
    //   90: astore_2
    //   91: ldc_w 385
    //   94: aload_3
    //   95: invokevirtual 383	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   98: ifeq +19 -> 117
    //   101: new 387	org/json/JSONObject
    //   104: dup
    //   105: aload 6
    //   107: invokespecial 388	org/json/JSONObject:<init>	(Ljava/lang/String;)V
    //   110: ldc_w 390
    //   113: invokevirtual 393	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   116: astore_2
    //   117: aload_0
    //   118: aload_2
    //   119: aload 6
    //   121: aload 5
    //   123: invokespecial 395	com/google/android/gms/internal/zzki:zza	(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/internal/zzgp;)V
    //   126: goto -68 -> 58
    //   129: astore_2
    //   130: ldc_w 397
    //   133: aload_2
    //   134: invokestatic 403	com/google/android/gms/internal/zzkx:zzb	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   137: goto -79 -> 58
    //   140: iconst_0
    //   141: istore_1
    //   142: iload_1
    //   143: aload_0
    //   144: getfield 63	com/google/android/gms/internal/zzki:zzcrt	Ljava/util/ArrayList;
    //   147: invokevirtual 406	java/util/ArrayList:size	()I
    //   150: if_icmpge +241 -> 391
    //   153: aload_0
    //   154: getfield 63	com/google/android/gms/internal/zzki:zzcrt	Ljava/util/ArrayList;
    //   157: iload_1
    //   158: invokevirtual 409	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   161: checkcast 301	java/util/concurrent/Future
    //   164: invokeinterface 410 1 0
    //   169: pop
    //   170: aload_0
    //   171: getfield 82	com/google/android/gms/internal/zzki:zzako	Ljava/lang/Object;
    //   174: astore_2
    //   175: aload_2
    //   176: monitorenter
    //   177: aload_0
    //   178: getfield 65	com/google/android/gms/internal/zzki:zzcru	Ljava/util/ArrayList;
    //   181: iload_1
    //   182: invokevirtual 409	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   185: checkcast 219	java/lang/String
    //   188: astore_3
    //   189: aload_3
    //   190: invokestatic 332	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   193: ifne +33 -> 226
    //   196: aload_0
    //   197: getfield 70	com/google/android/gms/internal/zzki:zzcrv	Ljava/util/HashMap;
    //   200: aload_3
    //   201: invokevirtual 413	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
    //   204: checkcast 354	com/google/android/gms/internal/zzkc
    //   207: astore_3
    //   208: aload_3
    //   209: ifnull +17 -> 226
    //   212: aload_0
    //   213: getfield 72	com/google/android/gms/internal/zzki:zzcrw	Ljava/util/List;
    //   216: aload_3
    //   217: invokevirtual 416	com/google/android/gms/internal/zzkc:zztw	()Lcom/google/android/gms/internal/zzkd;
    //   220: invokeinterface 293 2 0
    //   225: pop
    //   226: aload_2
    //   227: monitorexit
    //   228: aload_0
    //   229: getfield 82	com/google/android/gms/internal/zzki:zzako	Ljava/lang/Object;
    //   232: astore_3
    //   233: aload_3
    //   234: monitorenter
    //   235: aload_0
    //   236: getfield 77	com/google/android/gms/internal/zzki:zzcrx	Ljava/util/HashSet;
    //   239: aload_0
    //   240: getfield 65	com/google/android/gms/internal/zzki:zzcru	Ljava/util/ArrayList;
    //   243: iload_1
    //   244: invokevirtual 409	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   247: invokevirtual 419	java/util/HashSet:contains	(Ljava/lang/Object;)Z
    //   250: ifeq +319 -> 569
    //   253: aload_0
    //   254: getfield 65	com/google/android/gms/internal/zzki:zzcru	Ljava/util/ArrayList;
    //   257: iload_1
    //   258: invokevirtual 409	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   261: checkcast 219	java/lang/String
    //   264: astore 4
    //   266: aload_0
    //   267: getfield 70	com/google/android/gms/internal/zzki:zzcrv	Ljava/util/HashMap;
    //   270: aload 4
    //   272: invokevirtual 413	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
    //   275: ifnull +289 -> 564
    //   278: aload_0
    //   279: getfield 70	com/google/android/gms/internal/zzki:zzcrv	Ljava/util/HashMap;
    //   282: aload 4
    //   284: invokevirtual 413	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
    //   287: checkcast 354	com/google/android/gms/internal/zzkc
    //   290: invokevirtual 423	com/google/android/gms/internal/zzkc:zztx	()Lcom/google/android/gms/internal/zzgp;
    //   293: astore_2
    //   294: aload_0
    //   295: aload 4
    //   297: aload_2
    //   298: invokespecial 425	com/google/android/gms/internal/zzki:zza	(Ljava/lang/String;Lcom/google/android/gms/internal/zzgp;)Lcom/google/android/gms/internal/zzko;
    //   301: astore_2
    //   302: getstatic 431	com/google/android/gms/ads/internal/util/client/zza:zzcxr	Landroid/os/Handler;
    //   305: new 433	com/google/android/gms/internal/zzki$1
    //   308: dup
    //   309: aload_0
    //   310: aload_2
    //   311: invokespecial 436	com/google/android/gms/internal/zzki$1:<init>	(Lcom/google/android/gms/internal/zzki;Lcom/google/android/gms/internal/zzko;)V
    //   314: invokevirtual 442	android/os/Handler:post	(Ljava/lang/Runnable;)Z
    //   317: pop
    //   318: aload_3
    //   319: monitorexit
    //   320: return
    //   321: astore_3
    //   322: aload_2
    //   323: monitorexit
    //   324: aload_3
    //   325: athrow
    //   326: astore_2
    //   327: invokestatic 448	java/lang/Thread:currentThread	()Ljava/lang/Thread;
    //   330: invokevirtual 451	java/lang/Thread:interrupt	()V
    //   333: aload_0
    //   334: getfield 82	com/google/android/gms/internal/zzki:zzako	Ljava/lang/Object;
    //   337: astore_2
    //   338: aload_2
    //   339: monitorenter
    //   340: aload_0
    //   341: getfield 65	com/google/android/gms/internal/zzki:zzcru	Ljava/util/ArrayList;
    //   344: iload_1
    //   345: invokevirtual 409	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   348: checkcast 219	java/lang/String
    //   351: astore_3
    //   352: aload_3
    //   353: invokestatic 332	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   356: ifne +33 -> 389
    //   359: aload_0
    //   360: getfield 70	com/google/android/gms/internal/zzki:zzcrv	Ljava/util/HashMap;
    //   363: aload_3
    //   364: invokevirtual 413	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
    //   367: checkcast 354	com/google/android/gms/internal/zzkc
    //   370: astore_3
    //   371: aload_3
    //   372: ifnull +17 -> 389
    //   375: aload_0
    //   376: getfield 72	com/google/android/gms/internal/zzki:zzcrw	Ljava/util/List;
    //   379: aload_3
    //   380: invokevirtual 416	com/google/android/gms/internal/zzkc:zztw	()Lcom/google/android/gms/internal/zzkd;
    //   383: invokeinterface 293 2 0
    //   388: pop
    //   389: aload_2
    //   390: monitorexit
    //   391: aload_0
    //   392: invokespecial 453	com/google/android/gms/internal/zzki:zzua	()Lcom/google/android/gms/internal/zzko;
    //   395: astore_2
    //   396: getstatic 431	com/google/android/gms/ads/internal/util/client/zza:zzcxr	Landroid/os/Handler;
    //   399: new 455	com/google/android/gms/internal/zzki$2
    //   402: dup
    //   403: aload_0
    //   404: aload_2
    //   405: invokespecial 456	com/google/android/gms/internal/zzki$2:<init>	(Lcom/google/android/gms/internal/zzki;Lcom/google/android/gms/internal/zzko;)V
    //   408: invokevirtual 442	android/os/Handler:post	(Ljava/lang/Runnable;)Z
    //   411: pop
    //   412: return
    //   413: astore_3
    //   414: aload_2
    //   415: monitorexit
    //   416: aload_3
    //   417: athrow
    //   418: astore_2
    //   419: ldc_w 458
    //   422: aload_2
    //   423: invokestatic 461	com/google/android/gms/internal/zzkx:zzc	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   426: aload_0
    //   427: getfield 82	com/google/android/gms/internal/zzki:zzako	Ljava/lang/Object;
    //   430: astore_2
    //   431: aload_2
    //   432: monitorenter
    //   433: aload_0
    //   434: getfield 65	com/google/android/gms/internal/zzki:zzcru	Ljava/util/ArrayList;
    //   437: iload_1
    //   438: invokevirtual 409	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   441: checkcast 219	java/lang/String
    //   444: astore_3
    //   445: aload_3
    //   446: invokestatic 332	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   449: ifne +33 -> 482
    //   452: aload_0
    //   453: getfield 70	com/google/android/gms/internal/zzki:zzcrv	Ljava/util/HashMap;
    //   456: aload_3
    //   457: invokevirtual 413	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
    //   460: checkcast 354	com/google/android/gms/internal/zzkc
    //   463: astore_3
    //   464: aload_3
    //   465: ifnull +17 -> 482
    //   468: aload_0
    //   469: getfield 72	com/google/android/gms/internal/zzki:zzcrw	Ljava/util/List;
    //   472: aload_3
    //   473: invokevirtual 416	com/google/android/gms/internal/zzkc:zztw	()Lcom/google/android/gms/internal/zzkd;
    //   476: invokeinterface 293 2 0
    //   481: pop
    //   482: aload_2
    //   483: monitorexit
    //   484: goto +95 -> 579
    //   487: astore_3
    //   488: aload_2
    //   489: monitorexit
    //   490: aload_3
    //   491: athrow
    //   492: astore_3
    //   493: aload_0
    //   494: getfield 82	com/google/android/gms/internal/zzki:zzako	Ljava/lang/Object;
    //   497: astore_2
    //   498: aload_2
    //   499: monitorenter
    //   500: aload_0
    //   501: getfield 65	com/google/android/gms/internal/zzki:zzcru	Ljava/util/ArrayList;
    //   504: iload_1
    //   505: invokevirtual 409	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   508: checkcast 219	java/lang/String
    //   511: astore 4
    //   513: aload 4
    //   515: invokestatic 332	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   518: ifne +37 -> 555
    //   521: aload_0
    //   522: getfield 70	com/google/android/gms/internal/zzki:zzcrv	Ljava/util/HashMap;
    //   525: aload 4
    //   527: invokevirtual 413	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
    //   530: checkcast 354	com/google/android/gms/internal/zzkc
    //   533: astore 4
    //   535: aload 4
    //   537: ifnull +18 -> 555
    //   540: aload_0
    //   541: getfield 72	com/google/android/gms/internal/zzki:zzcrw	Ljava/util/List;
    //   544: aload 4
    //   546: invokevirtual 416	com/google/android/gms/internal/zzkc:zztw	()Lcom/google/android/gms/internal/zzkd;
    //   549: invokeinterface 293 2 0
    //   554: pop
    //   555: aload_2
    //   556: monitorexit
    //   557: aload_3
    //   558: athrow
    //   559: astore_3
    //   560: aload_2
    //   561: monitorexit
    //   562: aload_3
    //   563: athrow
    //   564: aconst_null
    //   565: astore_2
    //   566: goto -272 -> 294
    //   569: aload_3
    //   570: monitorexit
    //   571: goto +8 -> 579
    //   574: astore_2
    //   575: aload_3
    //   576: monitorexit
    //   577: aload_2
    //   578: athrow
    //   579: iload_1
    //   580: iconst_1
    //   581: iadd
    //   582: istore_1
    //   583: goto -441 -> 142
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	586	0	this	zzki
    //   141	442	1	i	int
    //   90	29	2	localObject1	Object
    //   129	5	2	localJSONException	org.json.JSONException
    //   326	1	2	localInterruptedException	InterruptedException
    //   418	5	2	localException	Exception
    //   574	4	2	localObject5	Object
    //   321	4	3	localObject7	Object
    //   351	29	3	localObject8	Object
    //   413	4	3	localObject9	Object
    //   444	29	3	localObject10	Object
    //   487	4	3	localObject11	Object
    //   492	66	3	localObject12	Object
    //   559	17	3	localObject13	Object
    //   15	530	4	localObject14	Object
    //   37	85	5	localzzgp	zzgp
    //   44	76	6	str	String
    //   56	13	7	localIterator	Iterator
    // Exception table:
    //   from	to	target	type
    //   101	117	129	org/json/JSONException
    //   177	208	321	finally
    //   212	226	321	finally
    //   226	228	321	finally
    //   322	324	321	finally
    //   153	170	326	java/lang/InterruptedException
    //   340	371	413	finally
    //   375	389	413	finally
    //   389	391	413	finally
    //   414	416	413	finally
    //   153	170	418	java/lang/Exception
    //   433	464	487	finally
    //   468	482	487	finally
    //   482	484	487	finally
    //   488	490	487	finally
    //   153	170	492	finally
    //   327	333	492	finally
    //   419	426	492	finally
    //   500	535	559	finally
    //   540	555	559	finally
    //   555	557	559	finally
    //   560	562	559	finally
    //   235	294	574	finally
    //   294	320	574	finally
    //   569	571	574	finally
    //   575	577	574	finally
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzki.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */