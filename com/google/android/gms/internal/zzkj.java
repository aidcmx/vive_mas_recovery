package com.google.android.gms.internal;

@zzji
public class zzkj
{
  private final zzha zzbwf;
  private final zzkg zzcsa;
  
  public zzkj(zzha paramzzha, zzkf paramzzkf)
  {
    this.zzbwf = paramzzha;
    this.zzcsa = new zzkg(paramzzkf);
  }
  
  public zzha zzuc()
  {
    return this.zzbwf;
  }
  
  public zzkg zzud()
  {
    return this.zzcsa;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzkj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */