package com.google.android.gms.internal;

import android.support.annotation.Nullable;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.formats.zzi.zza;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import com.google.android.gms.ads.internal.request.AutoClickProtectionConfigurationParcel;
import com.google.android.gms.ads.internal.reward.mediation.client.RewardItemParcel;
import java.util.Collections;
import java.util.List;
import org.json.JSONObject;

@zzji
public class zzko
{
  public final int errorCode;
  public final int orientation;
  public final List<String> zzbvk;
  public final List<String> zzbvl;
  @Nullable
  public final List<String> zzbvn;
  public final long zzbvq;
  @Nullable
  public final zzgp zzbwm;
  @Nullable
  public final zzha zzbwn;
  @Nullable
  public final String zzbwo;
  @Nullable
  public final zzgs zzbwp;
  @Nullable
  public final zzmd zzcbm;
  public final AdRequestParcel zzcju;
  public final String zzcjx;
  public final long zzcla;
  public final boolean zzclb;
  public final long zzclc;
  public final List<String> zzcld;
  public final String zzclg;
  @Nullable
  public final RewardItemParcel zzclq;
  @Nullable
  public final List<String> zzcls;
  public final boolean zzclt;
  public final AutoClickProtectionConfigurationParcel zzclu;
  public final String zzclx;
  public final JSONObject zzcsi;
  public boolean zzcsj;
  public final zzgq zzcsk;
  @Nullable
  public final String zzcsl;
  public final AdSizeParcel zzcsm;
  @Nullable
  public final List<String> zzcsn;
  public final long zzcso;
  public final long zzcsp;
  @Nullable
  public final zzi.zza zzcsq;
  public boolean zzcsr = false;
  public boolean zzcss = false;
  public boolean zzcst = false;
  
  public zzko(AdRequestParcel paramAdRequestParcel, @Nullable zzmd paramzzmd, List<String> paramList1, int paramInt1, List<String> paramList2, List<String> paramList3, int paramInt2, long paramLong1, String paramString1, boolean paramBoolean1, @Nullable zzgp paramzzgp, @Nullable zzha paramzzha, @Nullable String paramString2, zzgq paramzzgq, @Nullable zzgs paramzzgs, long paramLong2, AdSizeParcel paramAdSizeParcel, long paramLong3, long paramLong4, long paramLong5, String paramString3, JSONObject paramJSONObject, @Nullable zzi.zza paramzza, RewardItemParcel paramRewardItemParcel, List<String> paramList4, List<String> paramList5, boolean paramBoolean2, AutoClickProtectionConfigurationParcel paramAutoClickProtectionConfigurationParcel, @Nullable String paramString4, List<String> paramList6, String paramString5)
  {
    this.zzcju = paramAdRequestParcel;
    this.zzcbm = paramzzmd;
    this.zzbvk = zzm(paramList1);
    this.errorCode = paramInt1;
    this.zzbvl = zzm(paramList2);
    this.zzcld = zzm(paramList3);
    this.orientation = paramInt2;
    this.zzbvq = paramLong1;
    this.zzcjx = paramString1;
    this.zzclb = paramBoolean1;
    this.zzbwm = paramzzgp;
    this.zzbwn = paramzzha;
    this.zzbwo = paramString2;
    this.zzcsk = paramzzgq;
    this.zzbwp = paramzzgs;
    this.zzclc = paramLong2;
    this.zzcsm = paramAdSizeParcel;
    this.zzcla = paramLong3;
    this.zzcso = paramLong4;
    this.zzcsp = paramLong5;
    this.zzclg = paramString3;
    this.zzcsi = paramJSONObject;
    this.zzcsq = paramzza;
    this.zzclq = paramRewardItemParcel;
    this.zzcsn = zzm(paramList4);
    this.zzcls = zzm(paramList5);
    this.zzclt = paramBoolean2;
    this.zzclu = paramAutoClickProtectionConfigurationParcel;
    this.zzcsl = paramString4;
    this.zzbvn = zzm(paramList6);
    this.zzclx = paramString5;
  }
  
  public zzko(zza paramzza, @Nullable zzmd paramzzmd, @Nullable zzgp paramzzgp, @Nullable zzha paramzzha, @Nullable String paramString1, @Nullable zzgs paramzzgs, @Nullable zzi.zza paramzza1, @Nullable String paramString2)
  {
    this(paramzza.zzcmx.zzcju, paramzzmd, paramzza.zzcsu.zzbvk, paramzza.errorCode, paramzza.zzcsu.zzbvl, paramzza.zzcsu.zzcld, paramzza.zzcsu.orientation, paramzza.zzcsu.zzbvq, paramzza.zzcmx.zzcjx, paramzza.zzcsu.zzclb, paramzzgp, paramzzha, paramString1, paramzza.zzcsk, paramzzgs, paramzza.zzcsu.zzclc, paramzza.zzarm, paramzza.zzcsu.zzcla, paramzza.zzcso, paramzza.zzcsp, paramzza.zzcsu.zzclg, paramzza.zzcsi, paramzza1, paramzza.zzcsu.zzclq, paramzza.zzcsu.zzclr, paramzza.zzcsu.zzclr, paramzza.zzcsu.zzclt, paramzza.zzcsu.zzclu, paramString2, paramzza.zzcsu.zzbvn, paramzza.zzcsu.zzclx);
  }
  
  @Nullable
  private static <T> List<T> zzm(@Nullable List<T> paramList)
  {
    if (paramList == null) {
      return null;
    }
    return Collections.unmodifiableList(paramList);
  }
  
  public boolean zzic()
  {
    if ((this.zzcbm == null) || (this.zzcbm.zzxc() == null)) {
      return false;
    }
    return this.zzcbm.zzxc().zzic();
  }
  
  @zzji
  public static final class zza
  {
    public final int errorCode;
    @Nullable
    public final AdSizeParcel zzarm;
    public final AdRequestInfoParcel zzcmx;
    @Nullable
    public final JSONObject zzcsi;
    public final zzgq zzcsk;
    public final long zzcso;
    public final long zzcsp;
    public final AdResponseParcel zzcsu;
    
    public zza(AdRequestInfoParcel paramAdRequestInfoParcel, AdResponseParcel paramAdResponseParcel, zzgq paramzzgq, AdSizeParcel paramAdSizeParcel, int paramInt, long paramLong1, long paramLong2, JSONObject paramJSONObject)
    {
      this.zzcmx = paramAdRequestInfoParcel;
      this.zzcsu = paramAdResponseParcel;
      this.zzcsk = paramzzgq;
      this.zzarm = paramAdSizeParcel;
      this.errorCode = paramInt;
      this.zzcso = paramLong1;
      this.zzcsp = paramLong2;
      this.zzcsi = paramJSONObject;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzko.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */