package com.google.android.gms.internal;

import android.os.Bundle;
import android.os.SystemClock;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.zzu;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

@zzji
public class zzkp
{
  private final Object zzako = new Object();
  private final zzkr zzaqj;
  private boolean zzcoo = false;
  private final LinkedList<zza> zzcsv;
  private final String zzcsw;
  private final String zzcsx;
  private long zzcsy = -1L;
  private long zzcsz = -1L;
  private long zzcta = -1L;
  private long zzctb = 0L;
  private long zzctc = -1L;
  private long zzctd = -1L;
  
  public zzkp(zzkr paramzzkr, String paramString1, String paramString2)
  {
    this.zzaqj = paramzzkr;
    this.zzcsw = paramString1;
    this.zzcsx = paramString2;
    this.zzcsv = new LinkedList();
  }
  
  public zzkp(String paramString1, String paramString2)
  {
    this(zzu.zzgq(), paramString1, paramString2);
  }
  
  public Bundle toBundle()
  {
    ArrayList localArrayList;
    synchronized (this.zzako)
    {
      Bundle localBundle1 = new Bundle();
      localBundle1.putString("seq_num", this.zzcsw);
      localBundle1.putString("slotid", this.zzcsx);
      localBundle1.putBoolean("ismediation", this.zzcoo);
      localBundle1.putLong("treq", this.zzctc);
      localBundle1.putLong("tresponse", this.zzctd);
      localBundle1.putLong("timp", this.zzcsz);
      localBundle1.putLong("tload", this.zzcta);
      localBundle1.putLong("pcc", this.zzctb);
      localBundle1.putLong("tfetch", this.zzcsy);
      localArrayList = new ArrayList();
      Iterator localIterator = this.zzcsv.iterator();
      if (localIterator.hasNext()) {
        localArrayList.add(((zza)localIterator.next()).toBundle());
      }
    }
    localBundle2.putParcelableArrayList("tclick", localArrayList);
    return localBundle2;
  }
  
  public void zzad(boolean paramBoolean)
  {
    synchronized (this.zzako)
    {
      if (this.zzctd != -1L)
      {
        this.zzcta = SystemClock.elapsedRealtime();
        if (!paramBoolean)
        {
          this.zzcsz = this.zzcta;
          this.zzaqj.zza(this);
        }
      }
      return;
    }
  }
  
  public void zzae(boolean paramBoolean)
  {
    synchronized (this.zzako)
    {
      if (this.zzctd != -1L)
      {
        this.zzcoo = paramBoolean;
        this.zzaqj.zza(this);
      }
      return;
    }
  }
  
  public void zzm(long paramLong)
  {
    synchronized (this.zzako)
    {
      this.zzctd = paramLong;
      if (this.zzctd != -1L) {
        this.zzaqj.zza(this);
      }
      return;
    }
  }
  
  public void zzn(long paramLong)
  {
    synchronized (this.zzako)
    {
      if (this.zzctd != -1L)
      {
        this.zzcsy = paramLong;
        this.zzaqj.zza(this);
      }
      return;
    }
  }
  
  public void zzt(AdRequestParcel paramAdRequestParcel)
  {
    synchronized (this.zzako)
    {
      this.zzctc = SystemClock.elapsedRealtime();
      this.zzaqj.zzut().zzb(paramAdRequestParcel, this.zzctc);
      return;
    }
  }
  
  public void zzug()
  {
    synchronized (this.zzako)
    {
      if ((this.zzctd != -1L) && (this.zzcsz == -1L))
      {
        this.zzcsz = SystemClock.elapsedRealtime();
        this.zzaqj.zza(this);
      }
      this.zzaqj.zzut().zzug();
      return;
    }
  }
  
  public void zzuh()
  {
    synchronized (this.zzako)
    {
      if (this.zzctd != -1L)
      {
        zza localzza = new zza();
        localzza.zzul();
        this.zzcsv.add(localzza);
        this.zzctb += 1L;
        this.zzaqj.zzut().zzuh();
        this.zzaqj.zza(this);
      }
      return;
    }
  }
  
  public void zzui()
  {
    synchronized (this.zzako)
    {
      if ((this.zzctd != -1L) && (!this.zzcsv.isEmpty()))
      {
        zza localzza = (zza)this.zzcsv.getLast();
        if (localzza.zzuj() == -1L)
        {
          localzza.zzuk();
          this.zzaqj.zza(this);
        }
      }
      return;
    }
  }
  
  @zzji
  private static final class zza
  {
    private long zzcte = -1L;
    private long zzctf = -1L;
    
    public Bundle toBundle()
    {
      Bundle localBundle = new Bundle();
      localBundle.putLong("topen", this.zzcte);
      localBundle.putLong("tclose", this.zzctf);
      return localBundle;
    }
    
    public long zzuj()
    {
      return this.zzctf;
    }
    
    public void zzuk()
    {
      this.zzctf = SystemClock.elapsedRealtime();
    }
    
    public void zzul()
    {
      this.zzcte = SystemClock.elapsedRealtime();
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzkp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */