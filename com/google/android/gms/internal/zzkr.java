package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Looper;
import android.security.NetworkSecurityPolicy;
import com.google.android.gms.ads.internal.purchase.zzi;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.common.util.zze;
import com.google.android.gms.common.util.zzs;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.Future;

@zzji
public class zzkr
  implements zzcz.zzb, zzkz.zzb
{
  private Context mContext;
  private final Object zzako = new Object();
  private zzco zzama;
  private VersionInfoParcel zzanu;
  private boolean zzaoz = false;
  private zzcy zzawi = null;
  private String zzbre;
  private boolean zzcot = true;
  private boolean zzcou = true;
  private boolean zzcov = true;
  private boolean zzcpd = false;
  private final String zzctq;
  private final zzks zzctr;
  private BigInteger zzcts = BigInteger.ONE;
  private final HashSet<zzkp> zzctt = new HashSet();
  private final HashMap<String, zzku> zzctu = new HashMap();
  private boolean zzctv = false;
  private int zzctw = 0;
  private zzdt zzctx = null;
  private zzda zzcty = null;
  private String zzctz;
  private String zzcua;
  private Boolean zzcub = null;
  private boolean zzcuc = false;
  private boolean zzcud = false;
  private boolean zzcue = false;
  private String zzcuf = "";
  private long zzcug = 0L;
  private long zzcuh = 0L;
  private int zzcui = -1;
  
  public zzkr(zzlb paramzzlb)
  {
    this.zzctq = paramzzlb.zzvs();
    this.zzctr = new zzks(this.zzctq);
  }
  
  public Resources getResources()
  {
    Resources localResources = null;
    if (this.zzanu.zzcyc) {
      localResources = this.mContext.getResources();
    }
    for (;;)
    {
      return localResources;
      try
      {
        zztl localzztl = zztl.zza(this.mContext, zztl.Qm, "com.google.android.gms.ads.dynamite");
        if (localzztl != null)
        {
          localResources = localzztl.zzbdt().getResources();
          return localResources;
        }
      }
      catch (zztl.zza localzza)
      {
        zzkx.zzc("Cannot load resource from dynamite apk or local jar", localzza);
      }
    }
    return null;
  }
  
  public String getSessionId()
  {
    return this.zzctq;
  }
  
  public Bundle zza(Context paramContext, zzkt paramzzkt, String paramString)
  {
    Bundle localBundle;
    synchronized (this.zzako)
    {
      localBundle = new Bundle();
      localBundle.putBundle("app", this.zzctr.zze(paramContext, paramString));
      paramContext = new Bundle();
      paramString = this.zzctu.keySet().iterator();
      if (paramString.hasNext())
      {
        String str = (String)paramString.next();
        paramContext.putBundle(str, ((zzku)this.zzctu.get(str)).toBundle());
      }
    }
    localBundle.putBundle("slots", paramContext);
    paramContext = new ArrayList();
    paramString = this.zzctt.iterator();
    while (paramString.hasNext()) {
      paramContext.add(((zzkp)paramString.next()).toBundle());
    }
    localBundle.putParcelableArrayList("ads", paramContext);
    paramzzkt.zza(this.zzctt);
    this.zzctt.clear();
    return localBundle;
  }
  
  public void zza(zzkp paramzzkp)
  {
    synchronized (this.zzako)
    {
      this.zzctt.add(paramzzkp);
      return;
    }
  }
  
  public void zza(String paramString, zzku paramzzku)
  {
    synchronized (this.zzako)
    {
      this.zzctu.put(paramString, paramzzku);
      return;
    }
  }
  
  public void zza(Throwable paramThrowable, String paramString)
  {
    zzjg.zzb(this.mContext, this.zzanu).zza(paramThrowable, paramString);
  }
  
  public void zzaf(boolean paramBoolean)
  {
    synchronized (this.zzako)
    {
      if (this.zzcou != paramBoolean) {
        zzkz.zze(this.mContext, paramBoolean);
      }
      this.zzcou = paramBoolean;
      zzda localzzda = zzw(this.mContext);
      if ((localzzda != null) && (!localzzda.isAlive()))
      {
        zzkx.zzdh("start fetching content...");
        localzzda.zzjh();
      }
      return;
    }
  }
  
  public void zzag(boolean paramBoolean)
  {
    synchronized (this.zzako)
    {
      if (this.zzcov != paramBoolean) {
        zzkz.zze(this.mContext, paramBoolean);
      }
      zzkz.zze(this.mContext, paramBoolean);
      this.zzcov = paramBoolean;
      zzda localzzda = zzw(this.mContext);
      if ((localzzda != null) && (!localzzda.isAlive()))
      {
        zzkx.zzdh("start fetching content...");
        localzzda.zzjh();
      }
      return;
    }
  }
  
  public void zzah(boolean paramBoolean)
  {
    this.zzcue = paramBoolean;
  }
  
  public void zzai(boolean paramBoolean)
  {
    synchronized (this.zzako)
    {
      this.zzcuc = paramBoolean;
      return;
    }
  }
  
  public void zzb(Boolean paramBoolean)
  {
    synchronized (this.zzako)
    {
      this.zzcub = paramBoolean;
      return;
    }
  }
  
  public void zzb(HashSet<zzkp> paramHashSet)
  {
    synchronized (this.zzako)
    {
      this.zzctt.addAll(paramHashSet);
      return;
    }
  }
  
  public Future zzbf(int paramInt)
  {
    synchronized (this.zzako)
    {
      this.zzcui = paramInt;
      Future localFuture = zzkz.zza(this.mContext, paramInt);
      return localFuture;
    }
  }
  
  public Future zzc(Context paramContext, boolean paramBoolean)
  {
    synchronized (this.zzako)
    {
      if (paramBoolean != this.zzcot)
      {
        this.zzcot = paramBoolean;
        paramContext = zzkz.zzc(paramContext, paramBoolean);
        return paramContext;
      }
      return null;
    }
  }
  
  @TargetApi(23)
  public void zzc(Context paramContext, VersionInfoParcel paramVersionInfoParcel)
  {
    synchronized (this.zzako)
    {
      if (!this.zzaoz)
      {
        this.mContext = paramContext.getApplicationContext();
        this.zzanu = paramVersionInfoParcel;
        zzu.zzgp().zza(this);
        zzkz.zza(paramContext, this);
        zzkz.zzb(paramContext, this);
        zzkz.zzc(paramContext, this);
        zzkz.zzd(paramContext, this);
        zzkz.zze(paramContext, this);
        zzkz.zzf(paramContext, this);
        zzkz.zzg(paramContext, this);
        zzvh();
        this.zzbre = zzu.zzgm().zzh(paramContext, paramVersionInfoParcel.zzda);
        if ((zzs.zzayy()) && (!NetworkSecurityPolicy.getInstance().isCleartextTrafficPermitted())) {
          this.zzcud = true;
        }
        this.zzama = new zzco(paramContext.getApplicationContext(), this.zzanu, zzu.zzgm().zzd(paramContext, paramVersionInfoParcel));
        zzvj();
        zzu.zzha().zzr(this.mContext);
        this.zzaoz = true;
      }
      return;
    }
  }
  
  public Future zzcw(String paramString)
  {
    Object localObject = this.zzako;
    if (paramString != null) {}
    try
    {
      if (!paramString.equals(this.zzctz))
      {
        this.zzctz = paramString;
        paramString = zzkz.zzf(this.mContext, paramString);
        return paramString;
      }
      return null;
    }
    finally {}
  }
  
  public Future zzcx(String paramString)
  {
    Object localObject = this.zzako;
    if (paramString != null) {}
    try
    {
      if (!paramString.equals(this.zzcua))
      {
        this.zzcua = paramString;
        paramString = zzkz.zzg(this.mContext, paramString);
        return paramString;
      }
      return null;
    }
    finally {}
  }
  
  public Future zzd(Context paramContext, String paramString)
  {
    this.zzcug = zzu.zzgs().currentTimeMillis();
    Object localObject = this.zzako;
    if (paramString != null) {}
    try
    {
      if (!paramString.equals(this.zzcuf))
      {
        this.zzcuf = paramString;
        paramContext = zzkz.zza(paramContext, paramString, this.zzcug);
        return paramContext;
      }
      return null;
    }
    finally {}
  }
  
  public Future zzd(Context paramContext, boolean paramBoolean)
  {
    synchronized (this.zzako)
    {
      if (paramBoolean != this.zzcpd)
      {
        this.zzcpd = paramBoolean;
        paramContext = zzkz.zzf(paramContext, paramBoolean);
        return paramContext;
      }
      return null;
    }
  }
  
  public void zzh(Bundle paramBundle)
  {
    synchronized (this.zzako)
    {
      this.zzcot = paramBundle.getBoolean("use_https", this.zzcot);
      this.zzctw = paramBundle.getInt("webview_cache_version", this.zzctw);
      if (paramBundle.containsKey("content_url_opted_out")) {
        zzaf(paramBundle.getBoolean("content_url_opted_out"));
      }
      if (paramBundle.containsKey("content_url_hashes")) {
        this.zzctz = paramBundle.getString("content_url_hashes");
      }
      this.zzcpd = paramBundle.getBoolean("auto_collect_location", this.zzcpd);
      if (paramBundle.containsKey("content_vertical_opted_out")) {
        zzag(paramBundle.getBoolean("content_vertical_opted_out"));
      }
      if (paramBundle.containsKey("content_vertical_hashes")) {
        this.zzcua = paramBundle.getString("content_vertical_hashes");
      }
      if (paramBundle.containsKey("app_settings_json"))
      {
        str = paramBundle.getString("app_settings_json");
        this.zzcuf = str;
        this.zzcug = paramBundle.getLong("app_settings_last_update_ms", this.zzcug);
        this.zzcuh = paramBundle.getLong("app_last_background_time_ms", this.zzcuh);
        this.zzcui = paramBundle.getInt("request_in_session_count", this.zzcui);
        return;
      }
      String str = this.zzcuf;
    }
  }
  
  public void zzk(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      if (zzu.zzgs().currentTimeMillis() - this.zzcuh > ((Long)zzdr.zzbgb.get()).longValue())
      {
        this.zzctr.zzbg(-1);
        return;
      }
      this.zzctr.zzbg(this.zzcui);
      return;
    }
    zzo(zzu.zzgs().currentTimeMillis());
    zzbf(this.zzctr.zzvd());
  }
  
  public Future zzo(long paramLong)
  {
    synchronized (this.zzako)
    {
      if (this.zzcuh < paramLong)
      {
        this.zzcuh = paramLong;
        Future localFuture = zzkz.zza(this.mContext, paramLong);
        return localFuture;
      }
      return null;
    }
  }
  
  public boolean zzuq()
  {
    synchronized (this.zzako)
    {
      boolean bool = this.zzcou;
      return bool;
    }
  }
  
  public boolean zzur()
  {
    synchronized (this.zzako)
    {
      boolean bool = this.zzcov;
      return bool;
    }
  }
  
  public String zzus()
  {
    synchronized (this.zzako)
    {
      String str = this.zzcts.toString();
      this.zzcts = this.zzcts.add(BigInteger.ONE);
      return str;
    }
  }
  
  public zzks zzut()
  {
    synchronized (this.zzako)
    {
      zzks localzzks = this.zzctr;
      return localzzks;
    }
  }
  
  public zzdt zzuu()
  {
    synchronized (this.zzako)
    {
      zzdt localzzdt = this.zzctx;
      return localzzdt;
    }
  }
  
  public boolean zzuv()
  {
    synchronized (this.zzako)
    {
      boolean bool = this.zzctv;
      this.zzctv = true;
      return bool;
    }
  }
  
  public boolean zzuw()
  {
    for (;;)
    {
      synchronized (this.zzako)
      {
        if (!this.zzcot)
        {
          if (!this.zzcud) {
            break label38;
          }
          break label33;
          return bool;
        }
      }
      label33:
      boolean bool = true;
      continue;
      label38:
      bool = false;
    }
  }
  
  public String zzux()
  {
    synchronized (this.zzako)
    {
      String str = this.zzbre;
      return str;
    }
  }
  
  public String zzuy()
  {
    synchronized (this.zzako)
    {
      String str = this.zzctz;
      return str;
    }
  }
  
  public String zzuz()
  {
    synchronized (this.zzako)
    {
      String str = this.zzcua;
      return str;
    }
  }
  
  public Boolean zzva()
  {
    synchronized (this.zzako)
    {
      Boolean localBoolean = this.zzcub;
      return localBoolean;
    }
  }
  
  public boolean zzvb()
  {
    synchronized (this.zzako)
    {
      boolean bool = this.zzcpd;
      return bool;
    }
  }
  
  public long zzvc()
  {
    synchronized (this.zzako)
    {
      long l = this.zzcuh;
      return l;
    }
  }
  
  public int zzvd()
  {
    synchronized (this.zzako)
    {
      int i = this.zzcui;
      return i;
    }
  }
  
  public boolean zzve()
  {
    return this.zzcue;
  }
  
  public zzkq zzvf()
  {
    synchronized (this.zzako)
    {
      zzkq localzzkq = new zzkq(this.zzcuf, this.zzcug);
      return localzzkq;
    }
  }
  
  public zzco zzvg()
  {
    return this.zzama;
  }
  
  public void zzvh()
  {
    zzjg.zzb(this.mContext, this.zzanu);
  }
  
  public boolean zzvi()
  {
    synchronized (this.zzako)
    {
      boolean bool = this.zzcuc;
      return bool;
    }
  }
  
  void zzvj()
  {
    zzds localzzds = new zzds(this.mContext, this.zzanu.zzda);
    try
    {
      this.zzctx = zzu.zzgt().zza(localzzds);
      return;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      zzkx.zzc("Cannot initialize CSI reporter.", localIllegalArgumentException);
    }
  }
  
  public zzda zzw(Context paramContext)
  {
    if (!((Boolean)zzdr.zzbeu.get()).booleanValue()) {
      return null;
    }
    if (!zzs.zzayq()) {
      return null;
    }
    if ((!((Boolean)zzdr.zzbfc.get()).booleanValue()) && (!((Boolean)zzdr.zzbfa.get()).booleanValue())) {
      return null;
    }
    if ((zzuq()) && (zzur())) {
      return null;
    }
    synchronized (this.zzako)
    {
      if ((Looper.getMainLooper() == null) || (paramContext == null)) {
        return null;
      }
      if (this.zzawi == null) {
        this.zzawi = new zzcy();
      }
      if (this.zzcty == null) {
        this.zzcty = new zzda(this.zzawi, zzjg.zzb(this.mContext, this.zzanu));
      }
      this.zzcty.zzjh();
      paramContext = this.zzcty;
      return paramContext;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzkr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */