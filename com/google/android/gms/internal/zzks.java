package com.google.android.gms.internal;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.os.Bundle;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.zzu;

@zzji
public class zzks
{
  private final Object zzako = new Object();
  final String zzctq;
  int zzcui = -1;
  long zzcuj = -1L;
  long zzcuk = -1L;
  int zzcul = -1;
  int zzcum = 0;
  int zzcun = 0;
  
  public zzks(String paramString)
  {
    this.zzctq = paramString;
  }
  
  public static boolean zzx(Context paramContext)
  {
    int i = paramContext.getResources().getIdentifier("Theme.Translucent", "style", "android");
    if (i == 0)
    {
      zzkx.zzdh("Please set theme of AdActivity to @android:style/Theme.Translucent to enable transparent background interstitial ad.");
      return false;
    }
    ComponentName localComponentName = new ComponentName(paramContext.getPackageName(), "com.google.android.gms.ads.AdActivity");
    try
    {
      if (i == paramContext.getPackageManager().getActivityInfo(localComponentName, 0).theme) {
        return true;
      }
      zzkx.zzdh("Please set theme of AdActivity to @android:style/Theme.Translucent to enable transparent background interstitial ad.");
      return false;
    }
    catch (PackageManager.NameNotFoundException paramContext)
    {
      zzkx.zzdi("Fail to fetch AdActivity theme");
      zzkx.zzdh("Please set theme of AdActivity to @android:style/Theme.Translucent to enable transparent background interstitial ad.");
    }
    return false;
  }
  
  public void zzb(AdRequestParcel paramAdRequestParcel, long paramLong)
  {
    for (;;)
    {
      synchronized (this.zzako)
      {
        if (this.zzcuk == -1L)
        {
          if (paramLong - zzu.zzgq().zzvc() > ((Long)zzdr.zzbgb.get()).longValue())
          {
            zzbg(-1);
            this.zzcuk = paramLong;
            this.zzcuj = this.zzcuk;
            if ((paramAdRequestParcel.extras == null) || (paramAdRequestParcel.extras.getInt("gw", 2) != 1)) {
              break;
            }
            return;
          }
          zzbg(zzu.zzgq().zzvd());
        }
      }
      this.zzcuj = paramLong;
    }
    this.zzcul += 1;
    this.zzcui += 1;
  }
  
  public void zzbg(int paramInt)
  {
    this.zzcui = paramInt;
  }
  
  public Bundle zze(Context paramContext, String paramString)
  {
    synchronized (this.zzako)
    {
      Bundle localBundle = new Bundle();
      localBundle.putString("session_id", this.zzctq);
      localBundle.putLong("basets", this.zzcuk);
      localBundle.putLong("currts", this.zzcuj);
      localBundle.putString("seq_num", paramString);
      localBundle.putInt("preqs", this.zzcul);
      localBundle.putInt("preqs_in_session", this.zzcui);
      localBundle.putInt("pclick", this.zzcum);
      localBundle.putInt("pimp", this.zzcun);
      localBundle.putBoolean("support_transparent_background", zzx(paramContext));
      return localBundle;
    }
  }
  
  public void zzug()
  {
    synchronized (this.zzako)
    {
      this.zzcun += 1;
      return;
    }
  }
  
  public void zzuh()
  {
    synchronized (this.zzako)
    {
      this.zzcum += 1;
      return;
    }
  }
  
  public int zzvd()
  {
    return this.zzcui;
  }
  
  public long zzvk()
  {
    return this.zzcuk;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzks.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */