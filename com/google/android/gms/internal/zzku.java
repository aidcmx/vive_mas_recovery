package com.google.android.gms.internal;

import android.os.Bundle;
import com.google.android.gms.ads.internal.zzu;

@zzji
public class zzku
{
  private final Object zzako = new Object();
  private final zzkr zzaqj;
  private final String zzcsx;
  private int zzcuo;
  private int zzcup;
  
  zzku(zzkr paramzzkr, String paramString)
  {
    this.zzaqj = paramzzkr;
    this.zzcsx = paramString;
  }
  
  public zzku(String paramString)
  {
    this(zzu.zzgq(), paramString);
  }
  
  public Bundle toBundle()
  {
    synchronized (this.zzako)
    {
      Bundle localBundle = new Bundle();
      localBundle.putInt("pmnli", this.zzcuo);
      localBundle.putInt("pmnll", this.zzcup);
      return localBundle;
    }
  }
  
  public void zzj(int paramInt1, int paramInt2)
  {
    synchronized (this.zzako)
    {
      this.zzcuo = paramInt1;
      this.zzcup = paramInt2;
      this.zzaqj.zza(this.zzcsx, this);
      return;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzku.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */