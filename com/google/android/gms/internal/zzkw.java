package com.google.android.gms.internal;

import java.util.concurrent.Future;

@zzji
public abstract class zzkw
  implements zzld<Future>
{
  private volatile Thread zzcur;
  private boolean zzcus;
  private final Runnable zzw = new zzkw.1(this);
  
  public zzkw()
  {
    this.zzcus = false;
  }
  
  public zzkw(boolean paramBoolean)
  {
    this.zzcus = paramBoolean;
  }
  
  public final void cancel()
  {
    onStop();
    if (this.zzcur != null) {
      this.zzcur.interrupt();
    }
  }
  
  public abstract void onStop();
  
  public abstract void zzfp();
  
  public final Future zzvm()
  {
    if (this.zzcus) {
      return zzla.zza(1, this.zzw);
    }
    return zzla.zza(this.zzw);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzkw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */