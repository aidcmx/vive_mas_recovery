package com.google.android.gms.internal;

import android.util.Log;
import com.google.android.gms.ads.internal.util.client.zzb;

@zzji
public final class zzkx
  extends zzb
{
  public static void v(String paramString)
  {
    if (zzvo()) {
      Log.v("Ads", paramString);
    }
  }
  
  public static boolean zzvn()
  {
    return ((Boolean)zzdr.zzbgr.get()).booleanValue();
  }
  
  public static boolean zzvo()
  {
    return (zzbi(2)) && (zzvn());
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzkx.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */