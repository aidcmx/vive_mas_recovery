package com.google.android.gms.internal;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@zzji
public final class zzla
{
  private static final ThreadPoolExecutor zzcvd = new ThreadPoolExecutor(10, 10, 1L, TimeUnit.MINUTES, new LinkedBlockingQueue(), zzcy("Default"));
  private static final ThreadPoolExecutor zzcve = new ThreadPoolExecutor(5, 5, 1L, TimeUnit.MINUTES, new LinkedBlockingQueue(), zzcy("Loader"));
  
  static
  {
    zzcvd.allowCoreThreadTimeOut(true);
    zzcve.allowCoreThreadTimeOut(true);
  }
  
  public static zzlt<Void> zza(int paramInt, Runnable paramRunnable)
  {
    if (paramInt == 1) {
      return zza(zzcve, new zzla.1(paramRunnable));
    }
    return zza(zzcvd, new zzla.2(paramRunnable));
  }
  
  public static zzlt<Void> zza(Runnable paramRunnable)
  {
    return zza(0, paramRunnable);
  }
  
  public static <T> zzlt<T> zza(Callable<T> paramCallable)
  {
    return zza(zzcvd, paramCallable);
  }
  
  public static <T> zzlt<T> zza(ExecutorService paramExecutorService, Callable<T> paramCallable)
  {
    zzlq localzzlq = new zzlq();
    try
    {
      localzzlq.zzd(new zzla.4(localzzlq, paramExecutorService.submit(new zzla.3(localzzlq, paramCallable))));
      return localzzlq;
    }
    catch (RejectedExecutionException paramExecutorService)
    {
      zzkx.zzc("Thread execution is rejected.", paramExecutorService);
      localzzlq.cancel(true);
    }
    return localzzlq;
  }
  
  private static ThreadFactory zzcy(String paramString)
  {
    return new zzla.5(paramString);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzla.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */