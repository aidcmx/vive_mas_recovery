package com.google.android.gms.internal;

@zzji
public abstract interface zzld<T>
{
  public abstract void cancel();
  
  public abstract T zzrz();
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzld.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */