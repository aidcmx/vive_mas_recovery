package com.google.android.gms.internal;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.net.Uri.Builder;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import com.google.android.gms.R.string;
import com.google.android.gms.ads.internal.zzu;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

@zzji
public class zzle
{
  private final Context mContext;
  private int mState = 0;
  private String zzant;
  private final float zzbzc;
  private String zzcvu;
  private float zzcvv;
  private float zzcvw;
  private float zzcvx;
  
  public zzle(Context paramContext)
  {
    this.mContext = paramContext;
    this.zzbzc = paramContext.getResources().getDisplayMetrics().density;
  }
  
  public zzle(Context paramContext, String paramString)
  {
    this(paramContext);
    this.zzcvu = paramString;
  }
  
  private int zza(List<String> paramList, String paramString, boolean paramBoolean)
  {
    if (!paramBoolean) {
      return -1;
    }
    paramList.add(paramString);
    return paramList.size() - 1;
  }
  
  static String zzdd(String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {
      paramString = "No debug information";
    }
    Object localObject;
    do
    {
      return paramString;
      paramString = paramString.replaceAll("\\+", "%20");
      localObject = new Uri.Builder().encodedQuery(paramString).build();
      paramString = new StringBuilder();
      localObject = zzu.zzgm().zzg((Uri)localObject);
      Iterator localIterator = ((Map)localObject).keySet().iterator();
      while (localIterator.hasNext())
      {
        String str = (String)localIterator.next();
        paramString.append(str).append(" = ").append((String)((Map)localObject).get(str)).append("\n\n");
      }
      localObject = paramString.toString().trim();
      paramString = (String)localObject;
    } while (!TextUtils.isEmpty((CharSequence)localObject));
    return "No debug information";
  }
  
  private void zzwb()
  {
    if (!(this.mContext instanceof Activity))
    {
      zzkx.zzdh("Can not create dialog without Activity Context");
      return;
    }
    Object localObject = zzu.zzgq().getResources();
    String str1;
    String str2;
    label54:
    String str3;
    if (localObject != null)
    {
      str1 = ((Resources)localObject).getString(R.string.debug_menu_title);
      if (localObject == null) {
        break label204;
      }
      str2 = ((Resources)localObject).getString(R.string.debug_menu_ad_information);
      if (localObject == null) {
        break label212;
      }
      str3 = ((Resources)localObject).getString(R.string.debug_menu_creative_preview);
      label69:
      if (localObject == null) {
        break label220;
      }
    }
    label204:
    label212:
    label220:
    for (localObject = ((Resources)localObject).getString(R.string.debug_menu_troubleshooting);; localObject = "Troubleshooting")
    {
      ArrayList localArrayList = new ArrayList();
      int i = zza(localArrayList, str2, true);
      int j = zza(localArrayList, str3, ((Boolean)zzdr.zzbkw.get()).booleanValue());
      int k = zza(localArrayList, (String)localObject, ((Boolean)zzdr.zzbkx.get()).booleanValue());
      new AlertDialog.Builder(this.mContext).setTitle(str1).setItems((CharSequence[])localArrayList.toArray(new String[0]), new zzle.1(this, i, j, k)).create().show();
      return;
      str1 = "Select a Debug Mode";
      break;
      str2 = "Ad Information";
      break label54;
      str3 = "Creative Preview";
      break label69;
    }
  }
  
  private void zzwc()
  {
    if (!(this.mContext instanceof Activity))
    {
      zzkx.zzdh("Can not create dialog without Activity Context");
      return;
    }
    String str = zzdd(this.zzcvu);
    AlertDialog.Builder localBuilder = new AlertDialog.Builder(this.mContext);
    localBuilder.setMessage(str);
    localBuilder.setTitle("Ad Information");
    localBuilder.setPositiveButton("Share", new zzle.2(this, str));
    localBuilder.setNegativeButton("Close", new zzle.3(this));
    localBuilder.create().show();
  }
  
  private void zzwd()
  {
    zzkx.zzdg("Debug mode [Creative Preview] selected.");
    zzla.zza(new zzle.4(this));
  }
  
  private void zzwe()
  {
    zzkx.zzdg("Debug mode [Troubleshooting] selected.");
    zzla.zza(new zzle.5(this));
  }
  
  public void setAdUnitId(String paramString)
  {
    this.zzant = paramString;
  }
  
  public void showDialog()
  {
    if ((((Boolean)zzdr.zzbkx.get()).booleanValue()) || (((Boolean)zzdr.zzbkw.get()).booleanValue()))
    {
      zzwb();
      return;
    }
    zzwc();
  }
  
  void zza(int paramInt, float paramFloat1, float paramFloat2)
  {
    if (paramInt == 0)
    {
      this.mState = 0;
      this.zzcvv = paramFloat1;
      this.zzcvw = paramFloat2;
      this.zzcvx = paramFloat2;
    }
    label24:
    label227:
    do
    {
      do
      {
        break label24;
        do
        {
          return;
        } while (this.mState == -1);
        if (paramInt != 2) {
          break;
        }
        if (paramFloat2 > this.zzcvw) {
          this.zzcvw = paramFloat2;
        }
        while (this.zzcvw - this.zzcvx > 30.0F * this.zzbzc)
        {
          this.mState = -1;
          return;
          if (paramFloat2 < this.zzcvx) {
            this.zzcvx = paramFloat2;
          }
        }
        if ((this.mState == 0) || (this.mState == 2)) {
          if (paramFloat1 - this.zzcvv >= 50.0F * this.zzbzc) {
            this.zzcvv = paramFloat1;
          }
        }
        for (this.mState += 1;; this.mState += 1)
        {
          do
          {
            if ((this.mState != 1) && (this.mState != 3)) {
              break label227;
            }
            if (paramFloat1 <= this.zzcvv) {
              break;
            }
            this.zzcvv = paramFloat1;
            return;
          } while (((this.mState != 1) && (this.mState != 3)) || (paramFloat1 - this.zzcvv > -50.0F * this.zzbzc));
          this.zzcvv = paramFloat1;
        }
      } while ((this.mState != 2) || (paramFloat1 >= this.zzcvv));
      this.zzcvv = paramFloat1;
      return;
    } while ((paramInt != 1) || (this.mState != 4));
    showDialog();
  }
  
  public void zzdc(String paramString)
  {
    this.zzcvu = paramString;
  }
  
  public void zzg(MotionEvent paramMotionEvent)
  {
    int j = paramMotionEvent.getHistorySize();
    int i = 0;
    while (i < j)
    {
      zza(paramMotionEvent.getActionMasked(), paramMotionEvent.getHistoricalX(0, i), paramMotionEvent.getHistoricalY(0, i));
      i += 1;
    }
    zza(paramMotionEvent.getActionMasked(), paramMotionEvent.getX(), paramMotionEvent.getY());
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzle.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */