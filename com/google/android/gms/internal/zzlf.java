package com.google.android.gms.internal;

import android.content.Context;
import android.net.Uri;
import android.net.Uri.Builder;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.zzu;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@zzji
public class zzlf
{
  private final Object zzako = new Object();
  private String zzcwd = "";
  private String zzcwe = "";
  private boolean zzcwf = false;
  
  private Uri zze(Context paramContext, String paramString1, String paramString2)
  {
    paramString1 = Uri.parse(paramString1).buildUpon();
    paramString1.appendQueryParameter("linkedDeviceId", zzam(paramContext));
    paramString1.appendQueryParameter("adSlotPath", paramString2);
    return paramString1.build();
  }
  
  private void zzo(Context paramContext, String paramString)
  {
    zzu.zzgm().zza(paramContext, zze(paramContext, (String)zzdr.zzbky.get(), paramString));
  }
  
  public void zza(Context paramContext, String paramString1, String paramString2, String paramString3)
  {
    paramString3 = zze(paramContext, (String)zzdr.zzblb.get(), paramString3).buildUpon();
    paramString3.appendQueryParameter("debugData", paramString2);
    zzu.zzgm().zzc(paramContext, paramString1, paramString3.build().toString());
  }
  
  public void zzaj(boolean paramBoolean)
  {
    synchronized (this.zzako)
    {
      this.zzcwf = paramBoolean;
      return;
    }
  }
  
  public String zzam(Context paramContext)
  {
    synchronized (this.zzako)
    {
      if (TextUtils.isEmpty(this.zzcwd))
      {
        this.zzcwd = zzu.zzgm().zzi(paramContext, "debug_signals_id.txt");
        if (TextUtils.isEmpty(this.zzcwd))
        {
          this.zzcwd = zzu.zzgm().zzvr();
          zzu.zzgm().zzd(paramContext, "debug_signals_id.txt", this.zzcwd);
        }
      }
      paramContext = this.zzcwd;
      return paramContext;
    }
  }
  
  public void zzde(String paramString)
  {
    synchronized (this.zzako)
    {
      this.zzcwe = paramString;
      return;
    }
  }
  
  public void zzj(Context paramContext, String paramString)
  {
    if (zzl(paramContext, paramString))
    {
      zzkx.zzdg("Device is linked for in app preview.");
      return;
    }
    zzo(paramContext, paramString);
  }
  
  public void zzk(Context paramContext, String paramString)
  {
    if (zzm(paramContext, paramString))
    {
      zzkx.zzdg("Device is linked for debug signals.");
      return;
    }
    zzo(paramContext, paramString);
  }
  
  boolean zzl(Context paramContext, String paramString)
  {
    paramContext = zzn(paramContext, zze(paramContext, (String)zzdr.zzbkz.get(), paramString).toString());
    if (TextUtils.isEmpty(paramContext))
    {
      zzkx.zzdg("Not linked for in app preview.");
      return false;
    }
    zzde(paramContext.trim());
    return true;
  }
  
  boolean zzm(Context paramContext, String paramString)
  {
    paramContext = zzn(paramContext, zze(paramContext, (String)zzdr.zzbla.get(), paramString).toString());
    if (TextUtils.isEmpty(paramContext))
    {
      zzkx.zzdg("Not linked for debug signals.");
      return false;
    }
    boolean bool = Boolean.parseBoolean(paramContext.trim());
    zzaj(bool);
    return bool;
  }
  
  protected String zzn(Context paramContext, String paramString)
  {
    zzlt localzzlt = new zzli(paramContext).zza(paramString, new zzlf.1(this, paramString));
    try
    {
      paramContext = (String)localzzlt.get(((Integer)zzdr.zzblc.get()).intValue(), TimeUnit.MILLISECONDS);
      return paramContext;
    }
    catch (TimeoutException localTimeoutException)
    {
      paramContext = String.valueOf(paramString);
      if (paramContext.length() != 0) {}
      for (paramContext = "Timeout while retriving a response from: ".concat(paramContext);; paramContext = new String("Timeout while retriving a response from: "))
      {
        zzkx.zzb(paramContext, localTimeoutException);
        localzzlt.cancel(true);
        return null;
      }
    }
    catch (InterruptedException localInterruptedException)
    {
      paramContext = String.valueOf(paramString);
      if (paramContext.length() != 0) {}
      for (paramContext = "Interrupted while retriving a response from: ".concat(paramContext);; paramContext = new String("Interrupted while retriving a response from: "))
      {
        zzkx.zzb(paramContext, localInterruptedException);
        localzzlt.cancel(true);
        break;
      }
    }
    catch (Exception localException)
    {
      paramContext = String.valueOf(paramString);
      if (paramContext.length() == 0) {}
    }
    for (paramContext = "Error retriving a response from: ".concat(paramContext);; paramContext = new String("Error retriving a response from: "))
    {
      zzkx.zzb(paramContext, localException);
      break;
    }
  }
  
  public String zzwf()
  {
    synchronized (this.zzako)
    {
      String str = this.zzcwe;
      return str;
    }
  }
  
  public boolean zzwg()
  {
    synchronized (this.zzako)
    {
      boolean bool = this.zzcwf;
      return bool;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzlf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */