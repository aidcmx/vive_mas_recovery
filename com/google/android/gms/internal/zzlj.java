package com.google.android.gms.internal;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import com.google.android.gms.common.internal.zzaa;

@zzji
public class zzlj
{
  private Handler mHandler = null;
  private final Object zzako = new Object();
  private HandlerThread zzcxc = null;
  private int zzcxd = 0;
  
  public Looper zzwj()
  {
    for (;;)
    {
      synchronized (this.zzako)
      {
        if (this.zzcxd == 0)
        {
          if (this.zzcxc == null)
          {
            zzkx.v("Starting the looper thread.");
            this.zzcxc = new HandlerThread("LooperProvider");
            this.zzcxc.start();
            this.mHandler = new Handler(this.zzcxc.getLooper());
            zzkx.v("Looper thread started.");
            this.zzcxd += 1;
            Looper localLooper = this.zzcxc.getLooper();
            return localLooper;
          }
          zzkx.v("Resuming the looper thread");
          this.zzako.notifyAll();
        }
      }
      zzaa.zzb(this.zzcxc, "Invalid state: mHandlerThread should already been initialized.");
    }
  }
  
  public void zzwk()
  {
    for (;;)
    {
      synchronized (this.zzako)
      {
        if (this.zzcxd > 0)
        {
          bool = true;
          zzaa.zzb(bool, "Invalid state: release() called more times than expected.");
          int i = this.zzcxd - 1;
          this.zzcxd = i;
          if (i == 0) {
            this.mHandler.post(new zzlj.1(this));
          }
          return;
        }
      }
      boolean bool = false;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzlj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */