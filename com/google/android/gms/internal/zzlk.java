package com.google.android.gms.internal;

import android.graphics.Bitmap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

@zzji
public class zzlk
{
  Map<Integer, Bitmap> zzcxf = new ConcurrentHashMap();
  private AtomicInteger zzcxg = new AtomicInteger(0);
  
  public Bitmap zza(Integer paramInteger)
  {
    return (Bitmap)this.zzcxf.get(paramInteger);
  }
  
  public int zzb(Bitmap paramBitmap)
  {
    if (paramBitmap == null)
    {
      zzkx.zzdg("Bitmap is null. Skipping putting into the Memory Map.");
      return -1;
    }
    this.zzcxf.put(Integer.valueOf(this.zzcxg.get()), paramBitmap);
    return this.zzcxg.getAndIncrement();
  }
  
  public void zzb(Integer paramInteger)
  {
    this.zzcxf.remove(paramInteger);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzlk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */