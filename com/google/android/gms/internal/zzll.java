package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.ads.internal.util.client.zzc;
import com.google.android.gms.ads.internal.zzu;

@zzji
public final class zzll
  extends zzkw
{
  private final String zzae;
  private final zzc zzcxh;
  
  public zzll(Context paramContext, String paramString1, String paramString2)
  {
    this(paramString2, zzu.zzgm().zzh(paramContext, paramString1));
  }
  
  public zzll(String paramString1, String paramString2)
  {
    this.zzcxh = new zzc(paramString2);
    this.zzae = paramString1;
  }
  
  public void onStop() {}
  
  public void zzfp()
  {
    this.zzcxh.zzv(this.zzae);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzll.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */