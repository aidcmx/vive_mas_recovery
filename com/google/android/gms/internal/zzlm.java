package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.common.util.zze;

@zzji
public class zzlm
{
  private Object zzako = new Object();
  private long zzcxi;
  private long zzcxj = Long.MIN_VALUE;
  
  public zzlm(long paramLong)
  {
    this.zzcxi = paramLong;
  }
  
  public boolean tryAcquire()
  {
    synchronized (this.zzako)
    {
      long l = zzu.zzgs().elapsedRealtime();
      if (this.zzcxj + this.zzcxi > l) {
        return false;
      }
      this.zzcxj = l;
      return true;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzlm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */