package com.google.android.gms.internal;

@zzji
public class zzln<T>
{
  private T zzcxk;
  
  public T get()
  {
    return (T)this.zzcxk;
  }
  
  public void set(T paramT)
  {
    this.zzcxk = paramT;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzln.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */