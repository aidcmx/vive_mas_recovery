package com.google.android.gms.internal;

import android.app.Activity;
import android.view.View;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.ViewTreeObserver.OnScrollChangedListener;
import com.google.android.gms.ads.internal.zzu;

@zzji
public final class zzlp
{
  private final View mView;
  private Activity zzcxl;
  private boolean zzcxm;
  private boolean zzcxn;
  private boolean zzcxo;
  private ViewTreeObserver.OnGlobalLayoutListener zzcxp;
  private ViewTreeObserver.OnScrollChangedListener zzcxq;
  
  public zzlp(Activity paramActivity, View paramView, ViewTreeObserver.OnGlobalLayoutListener paramOnGlobalLayoutListener, ViewTreeObserver.OnScrollChangedListener paramOnScrollChangedListener)
  {
    this.zzcxl = paramActivity;
    this.mView = paramView;
    this.zzcxp = paramOnGlobalLayoutListener;
    this.zzcxq = paramOnScrollChangedListener;
  }
  
  private void zzwn()
  {
    if (!this.zzcxm)
    {
      if (this.zzcxp != null)
      {
        if (this.zzcxl != null) {
          zzu.zzgm().zza(this.zzcxl, this.zzcxp);
        }
        zzu.zzhk().zza(this.mView, this.zzcxp);
      }
      if (this.zzcxq != null)
      {
        if (this.zzcxl != null) {
          zzu.zzgm().zza(this.zzcxl, this.zzcxq);
        }
        zzu.zzhk().zza(this.mView, this.zzcxq);
      }
      this.zzcxm = true;
    }
  }
  
  private void zzwo()
  {
    if (this.zzcxl == null) {}
    while (!this.zzcxm) {
      return;
    }
    if ((this.zzcxp != null) && (this.zzcxl != null)) {
      zzu.zzgo().zzb(this.zzcxl, this.zzcxp);
    }
    if ((this.zzcxq != null) && (this.zzcxl != null)) {
      zzu.zzgm().zzb(this.zzcxl, this.zzcxq);
    }
    this.zzcxm = false;
  }
  
  public void onAttachedToWindow()
  {
    this.zzcxn = true;
    if (this.zzcxo) {
      zzwn();
    }
  }
  
  public void onDetachedFromWindow()
  {
    this.zzcxn = false;
    zzwo();
  }
  
  public void zzl(Activity paramActivity)
  {
    this.zzcxl = paramActivity;
  }
  
  public void zzwl()
  {
    this.zzcxo = true;
    if (this.zzcxn) {
      zzwn();
    }
  }
  
  public void zzwm()
  {
    this.zzcxo = false;
    zzwo();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzlp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */