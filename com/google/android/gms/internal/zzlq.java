package com.google.android.gms.internal;

import android.support.annotation.Nullable;
import com.google.android.gms.ads.internal.zzu;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@zzji
public class zzlq<T>
  implements zzlt<T>
{
  private final Object zzako = new Object();
  private boolean zzbww;
  private T zzcyd;
  private Throwable zzcye;
  private boolean zzcyf;
  private final zzlu zzcyg = new zzlu();
  
  private boolean zzws()
  {
    return (this.zzcye != null) || (this.zzcyf);
  }
  
  public boolean cancel(boolean paramBoolean)
  {
    if (!paramBoolean) {
      return false;
    }
    synchronized (this.zzako)
    {
      if (zzws()) {
        return false;
      }
    }
    this.zzbww = true;
    this.zzcyf = true;
    this.zzako.notifyAll();
    this.zzcyg.zzwt();
    return true;
  }
  
  public T get()
    throws CancellationException, ExecutionException, InterruptedException
  {
    synchronized (this.zzako)
    {
      boolean bool = zzws();
      if (bool) {}
    }
    if (this.zzbww) {
      throw new CancellationException("CallbackFuture was cancelled.");
    }
    Object localObject3 = this.zzcyd;
    return (T)localObject3;
  }
  
  public T get(long paramLong, TimeUnit paramTimeUnit)
    throws CancellationException, ExecutionException, InterruptedException, TimeoutException
  {
    synchronized (this.zzako)
    {
      boolean bool = zzws();
      if (bool) {}
    }
    if (!this.zzcyf) {
      throw new TimeoutException("CallbackFuture timed out.");
    }
    if (this.zzbww) {
      throw new CancellationException("CallbackFuture was cancelled.");
    }
    paramTimeUnit = this.zzcyd;
    return paramTimeUnit;
  }
  
  public boolean isCancelled()
  {
    synchronized (this.zzako)
    {
      boolean bool = this.zzbww;
      return bool;
    }
  }
  
  public boolean isDone()
  {
    synchronized (this.zzako)
    {
      boolean bool = zzws();
      return bool;
    }
  }
  
  public void zzc(Runnable paramRunnable)
  {
    this.zzcyg.zzc(paramRunnable);
  }
  
  public void zzd(Runnable paramRunnable)
  {
    this.zzcyg.zzd(paramRunnable);
  }
  
  public void zze(Throwable paramThrowable)
  {
    synchronized (this.zzako)
    {
      if (this.zzbww) {
        return;
      }
      if (zzws())
      {
        zzu.zzgq().zza(new IllegalStateException("Provided CallbackFuture with multiple values."), "CallbackFuture.provideException");
        return;
      }
    }
    this.zzcye = paramThrowable;
    this.zzako.notifyAll();
    this.zzcyg.zzwt();
  }
  
  public void zzh(@Nullable T paramT)
  {
    synchronized (this.zzako)
    {
      if (this.zzbww) {
        return;
      }
      if (zzws())
      {
        zzu.zzgq().zza(new IllegalStateException("Provided CallbackFuture with multiple values."), "CallbackFuture.provideValue");
        return;
      }
    }
    this.zzcyf = true;
    this.zzcyd = paramT;
    this.zzako.notifyAll();
    this.zzcyg.zzwt();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzlq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */