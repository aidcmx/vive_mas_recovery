package com.google.android.gms.internal;

import java.util.concurrent.TimeUnit;

@zzji
public class zzlr<T>
  implements zzlt<T>
{
  private final T zzcyd;
  private final zzlu zzcyg;
  
  public zzlr(T paramT)
  {
    this.zzcyd = paramT;
    this.zzcyg = new zzlu();
    this.zzcyg.zzwt();
  }
  
  public boolean cancel(boolean paramBoolean)
  {
    return false;
  }
  
  public T get()
  {
    return (T)this.zzcyd;
  }
  
  public T get(long paramLong, TimeUnit paramTimeUnit)
  {
    return (T)this.zzcyd;
  }
  
  public boolean isCancelled()
  {
    return false;
  }
  
  public boolean isDone()
  {
    return true;
  }
  
  public void zzc(Runnable paramRunnable)
  {
    this.zzcyg.zzc(paramRunnable);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzlr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */