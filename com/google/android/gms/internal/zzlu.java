package com.google.android.gms.internal;

import android.os.Handler;
import com.google.android.gms.ads.internal.util.client.zza;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@zzji
class zzlu
{
  private final Object zzcyo = new Object();
  private final List<Runnable> zzcyp = new ArrayList();
  private final List<Runnable> zzcyq = new ArrayList();
  private boolean zzcyr = false;
  
  private void zze(Runnable paramRunnable)
  {
    zzla.zza(paramRunnable);
  }
  
  private void zzf(Runnable paramRunnable)
  {
    zza.zzcxr.post(paramRunnable);
  }
  
  public void zzc(Runnable paramRunnable)
  {
    synchronized (this.zzcyo)
    {
      if (this.zzcyr)
      {
        zze(paramRunnable);
        return;
      }
      this.zzcyp.add(paramRunnable);
    }
  }
  
  public void zzd(Runnable paramRunnable)
  {
    synchronized (this.zzcyo)
    {
      if (this.zzcyr)
      {
        zzf(paramRunnable);
        return;
      }
      this.zzcyq.add(paramRunnable);
    }
  }
  
  public void zzwt()
  {
    synchronized (this.zzcyo)
    {
      if (this.zzcyr) {
        return;
      }
      Iterator localIterator1 = this.zzcyp.iterator();
      if (localIterator1.hasNext()) {
        zze((Runnable)localIterator1.next());
      }
    }
    Iterator localIterator2 = this.zzcyq.iterator();
    while (localIterator2.hasNext()) {
      zzf((Runnable)localIterator2.next());
    }
    this.zzcyp.clear();
    this.zzcyq.clear();
    this.zzcyr = true;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzlu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */