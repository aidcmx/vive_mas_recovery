package com.google.android.gms.internal;

import android.view.View;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.ViewTreeObserver.OnScrollChangedListener;

@zzji
public class zzly
{
  public void zza(View paramView, ViewTreeObserver.OnGlobalLayoutListener paramOnGlobalLayoutListener)
  {
    new zzlz(paramView, paramOnGlobalLayoutListener).zzwu();
  }
  
  public void zza(View paramView, ViewTreeObserver.OnScrollChangedListener paramOnScrollChangedListener)
  {
    new zzma(paramView, paramOnScrollChangedListener).zzwu();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzly.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */