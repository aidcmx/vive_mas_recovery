package com.google.android.gms.internal;

import android.content.Context;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import com.google.android.gms.ads.internal.overlay.zzk;
import com.google.android.gms.common.internal.zzaa;

@zzji
public class zzmc
{
  private final Context mContext;
  private final zzmd zzbnz;
  private zzk zzcep;
  private final ViewGroup zzcyz;
  
  public zzmc(Context paramContext, ViewGroup paramViewGroup, zzmd paramzzmd)
  {
    this(paramContext, paramViewGroup, paramzzmd, null);
  }
  
  zzmc(Context paramContext, ViewGroup paramViewGroup, zzmd paramzzmd, zzk paramzzk)
  {
    this.mContext = paramContext;
    this.zzcyz = paramViewGroup;
    this.zzbnz = paramzzmd;
    this.zzcep = paramzzk;
  }
  
  public void onDestroy()
  {
    zzaa.zzhs("onDestroy must be called from the UI thread.");
    if (this.zzcep != null)
    {
      this.zzcep.destroy();
      this.zzcyz.removeView(this.zzcep);
      this.zzcep = null;
    }
  }
  
  public void onPause()
  {
    zzaa.zzhs("onPause must be called from the UI thread.");
    if (this.zzcep != null) {
      this.zzcep.pause();
    }
  }
  
  public void zza(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, boolean paramBoolean)
  {
    if (this.zzcep != null) {
      return;
    }
    zzdv.zza(this.zzbnz.zzxm().zzly(), this.zzbnz.zzxl(), new String[] { "vpr2" });
    this.zzcep = new zzk(this.mContext, this.zzbnz, paramInt5, paramBoolean, this.zzbnz.zzxm().zzly());
    this.zzcyz.addView(this.zzcep, 0, new ViewGroup.LayoutParams(-1, -1));
    this.zzcep.zzd(paramInt1, paramInt2, paramInt3, paramInt4);
    this.zzbnz.zzxc().zzao(false);
  }
  
  public void zze(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    zzaa.zzhs("The underlay may only be modified from the UI thread.");
    if (this.zzcep != null) {
      this.zzcep.zzd(paramInt1, paramInt2, paramInt3, paramInt4);
    }
  }
  
  public zzk zzwv()
  {
    zzaa.zzhs("getAdVideoUnderlay must be called from the UI thread.");
    return this.zzcep;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzmc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */