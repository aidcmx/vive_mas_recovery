package com.google.android.gms.internal;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewParent;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.formats.zzg;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zzs;
import java.util.Map;
import org.json.JSONObject;

@zzji
public abstract interface zzmd
  extends zzs, zzcu.zzb, zzgi
{
  public abstract void destroy();
  
  public abstract Context getContext();
  
  public abstract ViewGroup.LayoutParams getLayoutParams();
  
  public abstract void getLocationOnScreen(int[] paramArrayOfInt);
  
  public abstract int getMeasuredHeight();
  
  public abstract int getMeasuredWidth();
  
  public abstract ViewParent getParent();
  
  public abstract String getRequestId();
  
  public abstract int getRequestedOrientation();
  
  public abstract View getView();
  
  public abstract WebView getWebView();
  
  public abstract boolean isDestroyed();
  
  public abstract void loadData(String paramString1, String paramString2, String paramString3);
  
  public abstract void loadDataWithBaseURL(String paramString1, String paramString2, String paramString3, String paramString4, @Nullable String paramString5);
  
  public abstract void loadUrl(String paramString);
  
  public abstract void measure(int paramInt1, int paramInt2);
  
  public abstract void onPause();
  
  public abstract void onResume();
  
  public abstract void setBackgroundColor(int paramInt);
  
  public abstract void setContext(Context paramContext);
  
  public abstract void setOnClickListener(View.OnClickListener paramOnClickListener);
  
  public abstract void setOnTouchListener(View.OnTouchListener paramOnTouchListener);
  
  public abstract void setRequestedOrientation(int paramInt);
  
  public abstract void setWebChromeClient(WebChromeClient paramWebChromeClient);
  
  public abstract void setWebViewClient(WebViewClient paramWebViewClient);
  
  public abstract void stopLoading();
  
  public abstract void zza(Context paramContext, AdSizeParcel paramAdSizeParcel, zzdz paramzzdz);
  
  public abstract void zza(AdSizeParcel paramAdSizeParcel);
  
  public abstract void zza(zzmi paramzzmi);
  
  public abstract void zza(String paramString, Map<String, ?> paramMap);
  
  public abstract void zza(String paramString, JSONObject paramJSONObject);
  
  public abstract void zzak(int paramInt);
  
  public abstract void zzak(boolean paramBoolean);
  
  public abstract void zzal(boolean paramBoolean);
  
  public abstract void zzam(boolean paramBoolean);
  
  public abstract void zzan(boolean paramBoolean);
  
  public abstract void zzb(zzg paramzzg);
  
  public abstract void zzb(com.google.android.gms.ads.internal.overlay.zzd paramzzd);
  
  public abstract void zzc(com.google.android.gms.ads.internal.overlay.zzd paramzzd);
  
  public abstract void zzdj(String paramString);
  
  public abstract void zzdk(String paramString);
  
  public abstract com.google.android.gms.ads.internal.zzd zzec();
  
  public abstract AdSizeParcel zzeg();
  
  public abstract void zzi(String paramString1, String paramString2);
  
  public abstract void zzps();
  
  public abstract void zzww();
  
  public abstract void zzwx();
  
  public abstract Activity zzwy();
  
  public abstract Context zzwz();
  
  public abstract com.google.android.gms.ads.internal.overlay.zzd zzxa();
  
  public abstract com.google.android.gms.ads.internal.overlay.zzd zzxb();
  
  @Nullable
  public abstract zzme zzxc();
  
  public abstract boolean zzxd();
  
  public abstract zzav zzxe();
  
  public abstract VersionInfoParcel zzxf();
  
  public abstract boolean zzxg();
  
  public abstract void zzxh();
  
  public abstract boolean zzxi();
  
  public abstract boolean zzxj();
  
  @Nullable
  public abstract zzmc zzxk();
  
  @Nullable
  public abstract zzdx zzxl();
  
  public abstract zzdy zzxm();
  
  @Nullable
  public abstract zzmi zzxn();
  
  public abstract boolean zzxo();
  
  public abstract void zzxp();
  
  public abstract void zzxq();
  
  @Nullable
  public abstract View.OnClickListener zzxr();
  
  public abstract zzg zzxs();
  
  public abstract void zzxt();
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzmd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */