package com.google.android.gms.internal;

import android.content.Context;
import android.support.annotation.Nullable;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zzd;
import com.google.android.gms.ads.internal.zzs;
import com.google.android.gms.ads.internal.zzu;

@zzji
public class zzmf
{
  public zzmd zza(Context paramContext, AdSizeParcel paramAdSizeParcel, boolean paramBoolean1, boolean paramBoolean2, @Nullable zzav paramzzav, VersionInfoParcel paramVersionInfoParcel)
  {
    return zza(paramContext, paramAdSizeParcel, paramBoolean1, paramBoolean2, paramzzav, paramVersionInfoParcel, null, null, null);
  }
  
  public zzmd zza(Context paramContext, AdSizeParcel paramAdSizeParcel, boolean paramBoolean1, boolean paramBoolean2, @Nullable zzav paramzzav, VersionInfoParcel paramVersionInfoParcel, zzdz paramzzdz, zzs paramzzs, zzd paramzzd)
  {
    paramContext = new zzmg(zzmh.zzb(paramContext, paramAdSizeParcel, paramBoolean1, paramBoolean2, paramzzav, paramVersionInfoParcel, paramzzdz, paramzzs, paramzzd));
    paramContext.setWebViewClient(zzu.zzgo().zzb(paramContext, paramBoolean2));
    paramContext.setWebChromeClient(zzu.zzgo().zzn(paramContext));
    return paramContext;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzmf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */