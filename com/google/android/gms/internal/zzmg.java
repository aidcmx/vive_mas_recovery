package com.google.android.gms.internal;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.formats.zzg;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import java.util.Map;
import org.json.JSONObject;

@zzji
class zzmg
  extends FrameLayout
  implements zzmd
{
  private static final int zzcak = Color.argb(0, 0, 0, 0);
  private final zzmd zzczv;
  private final zzmc zzczw;
  
  public zzmg(zzmd paramzzmd)
  {
    super(paramzzmd.getContext());
    this.zzczv = paramzzmd;
    this.zzczw = new zzmc(paramzzmd.zzwz(), this, this);
    paramzzmd = this.zzczv.zzxc();
    if (paramzzmd != null) {
      paramzzmd.zzo(this);
    }
    addView(this.zzczv.getView());
  }
  
  public void destroy()
  {
    this.zzczv.destroy();
  }
  
  public String getRequestId()
  {
    return this.zzczv.getRequestId();
  }
  
  public int getRequestedOrientation()
  {
    return this.zzczv.getRequestedOrientation();
  }
  
  public View getView()
  {
    return this;
  }
  
  public WebView getWebView()
  {
    return this.zzczv.getWebView();
  }
  
  public boolean isDestroyed()
  {
    return this.zzczv.isDestroyed();
  }
  
  public void loadData(String paramString1, String paramString2, String paramString3)
  {
    this.zzczv.loadData(paramString1, paramString2, paramString3);
  }
  
  public void loadDataWithBaseURL(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
  {
    this.zzczv.loadDataWithBaseURL(paramString1, paramString2, paramString3, paramString4, paramString5);
  }
  
  public void loadUrl(String paramString)
  {
    this.zzczv.loadUrl(paramString);
  }
  
  public void onPause()
  {
    this.zzczw.onPause();
    this.zzczv.onPause();
  }
  
  public void onResume()
  {
    this.zzczv.onResume();
  }
  
  public void setContext(Context paramContext)
  {
    this.zzczv.setContext(paramContext);
  }
  
  public void setOnClickListener(View.OnClickListener paramOnClickListener)
  {
    this.zzczv.setOnClickListener(paramOnClickListener);
  }
  
  public void setOnTouchListener(View.OnTouchListener paramOnTouchListener)
  {
    this.zzczv.setOnTouchListener(paramOnTouchListener);
  }
  
  public void setRequestedOrientation(int paramInt)
  {
    this.zzczv.setRequestedOrientation(paramInt);
  }
  
  public void setWebChromeClient(WebChromeClient paramWebChromeClient)
  {
    this.zzczv.setWebChromeClient(paramWebChromeClient);
  }
  
  public void setWebViewClient(WebViewClient paramWebViewClient)
  {
    this.zzczv.setWebViewClient(paramWebViewClient);
  }
  
  public void stopLoading()
  {
    this.zzczv.stopLoading();
  }
  
  public void zza(Context paramContext, AdSizeParcel paramAdSizeParcel, zzdz paramzzdz)
  {
    this.zzczw.onDestroy();
    this.zzczv.zza(paramContext, paramAdSizeParcel, paramzzdz);
  }
  
  public void zza(AdSizeParcel paramAdSizeParcel)
  {
    this.zzczv.zza(paramAdSizeParcel);
  }
  
  public void zza(zzcu.zza paramzza)
  {
    this.zzczv.zza(paramzza);
  }
  
  public void zza(zzmi paramzzmi)
  {
    this.zzczv.zza(paramzzmi);
  }
  
  public void zza(String paramString, zzfe paramzzfe)
  {
    this.zzczv.zza(paramString, paramzzfe);
  }
  
  public void zza(String paramString, Map<String, ?> paramMap)
  {
    this.zzczv.zza(paramString, paramMap);
  }
  
  public void zza(String paramString, JSONObject paramJSONObject)
  {
    this.zzczv.zza(paramString, paramJSONObject);
  }
  
  public void zzak(int paramInt)
  {
    this.zzczv.zzak(paramInt);
  }
  
  public void zzak(boolean paramBoolean)
  {
    this.zzczv.zzak(paramBoolean);
  }
  
  public void zzal(boolean paramBoolean)
  {
    this.zzczv.zzal(paramBoolean);
  }
  
  public void zzam(boolean paramBoolean)
  {
    this.zzczv.zzam(paramBoolean);
  }
  
  public void zzan(boolean paramBoolean)
  {
    this.zzczv.zzan(paramBoolean);
  }
  
  public void zzb(@Nullable zzg paramzzg)
  {
    this.zzczv.zzb(paramzzg);
  }
  
  public void zzb(com.google.android.gms.ads.internal.overlay.zzd paramzzd)
  {
    this.zzczv.zzb(paramzzd);
  }
  
  public void zzb(String paramString, zzfe paramzzfe)
  {
    this.zzczv.zzb(paramString, paramzzfe);
  }
  
  public void zzb(String paramString, JSONObject paramJSONObject)
  {
    this.zzczv.zzb(paramString, paramJSONObject);
  }
  
  public void zzc(com.google.android.gms.ads.internal.overlay.zzd paramzzd)
  {
    this.zzczv.zzc(paramzzd);
  }
  
  public void zzdj(String paramString)
  {
    this.zzczv.zzdj(paramString);
  }
  
  public void zzdk(String paramString)
  {
    this.zzczv.zzdk(paramString);
  }
  
  public com.google.android.gms.ads.internal.zzd zzec()
  {
    return this.zzczv.zzec();
  }
  
  public AdSizeParcel zzeg()
  {
    return this.zzczv.zzeg();
  }
  
  public void zzey()
  {
    this.zzczv.zzey();
  }
  
  public void zzez()
  {
    this.zzczv.zzez();
  }
  
  public void zzi(String paramString1, String paramString2)
  {
    this.zzczv.zzi(paramString1, paramString2);
  }
  
  public void zzps()
  {
    this.zzczv.zzps();
  }
  
  public void zzww()
  {
    this.zzczv.zzww();
  }
  
  public void zzwx()
  {
    this.zzczv.zzwx();
  }
  
  public Activity zzwy()
  {
    return this.zzczv.zzwy();
  }
  
  public Context zzwz()
  {
    return this.zzczv.zzwz();
  }
  
  public com.google.android.gms.ads.internal.overlay.zzd zzxa()
  {
    return this.zzczv.zzxa();
  }
  
  public com.google.android.gms.ads.internal.overlay.zzd zzxb()
  {
    return this.zzczv.zzxb();
  }
  
  public zzme zzxc()
  {
    return this.zzczv.zzxc();
  }
  
  public boolean zzxd()
  {
    return this.zzczv.zzxd();
  }
  
  public zzav zzxe()
  {
    return this.zzczv.zzxe();
  }
  
  public VersionInfoParcel zzxf()
  {
    return this.zzczv.zzxf();
  }
  
  public boolean zzxg()
  {
    return this.zzczv.zzxg();
  }
  
  public void zzxh()
  {
    this.zzczw.onDestroy();
    this.zzczv.zzxh();
  }
  
  public boolean zzxi()
  {
    return this.zzczv.zzxi();
  }
  
  public boolean zzxj()
  {
    return this.zzczv.zzxj();
  }
  
  public zzmc zzxk()
  {
    return this.zzczw;
  }
  
  public zzdx zzxl()
  {
    return this.zzczv.zzxl();
  }
  
  public zzdy zzxm()
  {
    return this.zzczv.zzxm();
  }
  
  public zzmi zzxn()
  {
    return this.zzczv.zzxn();
  }
  
  public boolean zzxo()
  {
    return this.zzczv.zzxo();
  }
  
  public void zzxp()
  {
    this.zzczv.zzxp();
  }
  
  public void zzxq()
  {
    this.zzczv.zzxq();
  }
  
  public View.OnClickListener zzxr()
  {
    return this.zzczv.zzxr();
  }
  
  @Nullable
  public zzg zzxs()
  {
    return this.zzczv.zzxs();
  }
  
  public void zzxt()
  {
    setBackgroundColor(zzcak);
    this.zzczv.setBackgroundColor(zzcak);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzmg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */