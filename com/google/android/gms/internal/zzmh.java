package com.google.android.gms.internal;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.MutableContextWrapper;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.ViewTreeObserver.OnScrollChangedListener;
import android.view.WindowManager;
import android.webkit.DownloadListener;
import android.webkit.ValueCallback;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.client.zzm;
import com.google.android.gms.ads.internal.formats.zzg;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.ads.internal.zzu;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

@zzji
class zzmh
  extends WebView
  implements ViewTreeObserver.OnGlobalLayoutListener, DownloadListener, zzmd
{
  private com.google.android.gms.ads.internal.overlay.zzd a;
  private boolean b;
  private boolean c;
  private boolean d;
  private boolean e;
  private int f;
  private boolean g = true;
  boolean h = false;
  private zzmi i;
  private boolean j;
  private boolean k;
  private zzg l;
  private int m;
  private int n;
  private zzdx o;
  private zzdx p;
  private zzdy q;
  private WeakReference<View.OnClickListener> r;
  private com.google.android.gms.ads.internal.overlay.zzd s;
  private Map<String, zzfs> t;
  private final Object zzako = new Object();
  private final com.google.android.gms.ads.internal.zzd zzamb;
  private final VersionInfoParcel zzanu;
  private AdSizeParcel zzapp;
  private zzlp zzass;
  private final WindowManager zzati;
  @Nullable
  private final zzav zzbnx;
  private int zzbzd = -1;
  private int zzbze = -1;
  private int zzbzg = -1;
  private int zzbzh = -1;
  private String zzcec = "";
  private zzdx zzced;
  private Boolean zzcub;
  private final zza zzczx;
  private final com.google.android.gms.ads.internal.zzs zzczy;
  private zzme zzczz;
  
  protected zzmh(zza paramzza, AdSizeParcel paramAdSizeParcel, boolean paramBoolean1, boolean paramBoolean2, @Nullable zzav paramzzav, VersionInfoParcel paramVersionInfoParcel, zzdz paramzzdz, com.google.android.gms.ads.internal.zzs paramzzs, com.google.android.gms.ads.internal.zzd paramzzd)
  {
    super(paramzza);
    this.zzczx = paramzza;
    this.zzapp = paramAdSizeParcel;
    this.d = paramBoolean1;
    this.f = -1;
    this.zzbnx = paramzzav;
    this.zzanu = paramVersionInfoParcel;
    this.zzczy = paramzzs;
    this.zzamb = paramzzd;
    this.zzati = ((WindowManager)getContext().getSystemService("window"));
    setBackgroundColor(0);
    paramAdSizeParcel = getSettings();
    paramAdSizeParcel.setAllowFileAccess(false);
    paramAdSizeParcel.setJavaScriptEnabled(true);
    paramAdSizeParcel.setSavePassword(false);
    paramAdSizeParcel.setSupportMultipleWindows(true);
    paramAdSizeParcel.setJavaScriptCanOpenWindowsAutomatically(true);
    if (Build.VERSION.SDK_INT >= 21) {
      paramAdSizeParcel.setMixedContentMode(2);
    }
    zzu.zzgm().zza(paramzza, paramVersionInfoParcel.zzda, paramAdSizeParcel);
    zzu.zzgo().zza(getContext(), paramAdSizeParcel);
    setDownloadListener(this);
    zzym();
    if (com.google.android.gms.common.util.zzs.zzays()) {
      addJavascriptInterface(new zzmj(this), "googleAdsJsInterface");
    }
    if (com.google.android.gms.common.util.zzs.zzayn())
    {
      removeJavascriptInterface("accessibility");
      removeJavascriptInterface("accessibilityTraversal");
    }
    this.zzass = new zzlp(this.zzczx.zzwy(), this, this, null);
    zzd(paramzzdz);
  }
  
  private void zzap(boolean paramBoolean)
  {
    HashMap localHashMap = new HashMap();
    if (paramBoolean) {}
    for (String str = "1";; str = "0")
    {
      localHashMap.put("isVisible", str);
      zza("onAdVisibilityChanged", localHashMap);
      return;
    }
  }
  
  static zzmh zzb(Context paramContext, AdSizeParcel paramAdSizeParcel, boolean paramBoolean1, boolean paramBoolean2, @Nullable zzav paramzzav, VersionInfoParcel paramVersionInfoParcel, zzdz paramzzdz, com.google.android.gms.ads.internal.zzs paramzzs, com.google.android.gms.ads.internal.zzd paramzzd)
  {
    return new zzmh(new zza(paramContext), paramAdSizeParcel, paramBoolean1, paramBoolean2, paramzzav, paramVersionInfoParcel, paramzzdz, paramzzs, paramzzd);
  }
  
  private void zzd(zzdz paramzzdz)
  {
    zzyq();
    this.q = new zzdy(new zzdz(true, "make_wv", this.zzapp.zzazq));
    this.q.zzly().zzc(paramzzdz);
    this.zzced = zzdv.zzb(this.q.zzly());
    this.q.zza("native:view_create", this.zzced);
    this.p = null;
    this.o = null;
  }
  
  private void zzyi()
  {
    synchronized (this.zzako)
    {
      this.zzcub = zzu.zzgq().zzva();
      Boolean localBoolean = this.zzcub;
      if (localBoolean == null) {}
      try
      {
        evaluateJavascript("(function(){})()", null);
        zzb(Boolean.valueOf(true));
        return;
      }
      catch (IllegalStateException localIllegalStateException)
      {
        for (;;)
        {
          zzb(Boolean.valueOf(false));
        }
      }
    }
  }
  
  private void zzyj()
  {
    zzdv.zza(this.q.zzly(), this.zzced, new String[] { "aeh2" });
  }
  
  private void zzyk()
  {
    zzdv.zza(this.q.zzly(), this.zzced, new String[] { "aebb2" });
  }
  
  private void zzym()
  {
    for (;;)
    {
      synchronized (this.zzako)
      {
        if ((this.d) || (this.zzapp.zzazr))
        {
          if (Build.VERSION.SDK_INT < 14)
          {
            zzkx.zzdg("Disabling hardware acceleration on an overlay.");
            zzyn();
            return;
          }
          zzkx.zzdg("Enabling hardware acceleration on an overlay.");
          zzyo();
        }
      }
      if (Build.VERSION.SDK_INT < 18)
      {
        zzkx.zzdg("Disabling hardware acceleration on an AdView.");
        zzyn();
      }
      else
      {
        zzkx.zzdg("Enabling hardware acceleration on an AdView.");
        zzyo();
      }
    }
  }
  
  private void zzyn()
  {
    synchronized (this.zzako)
    {
      if (!this.e) {
        zzu.zzgo().zzv(this);
      }
      this.e = true;
      return;
    }
  }
  
  private void zzyo()
  {
    synchronized (this.zzako)
    {
      if (this.e) {
        zzu.zzgo().zzu(this);
      }
      this.e = false;
      return;
    }
  }
  
  private void zzyp()
  {
    synchronized (this.zzako)
    {
      this.t = null;
      return;
    }
  }
  
  private void zzyq()
  {
    if (this.q == null) {}
    zzdz localzzdz;
    do
    {
      return;
      localzzdz = this.q.zzly();
    } while ((localzzdz == null) || (zzu.zzgq().zzuu() == null));
    zzu.zzgq().zzuu().zza(localzzdz);
  }
  
  public void destroy()
  {
    synchronized (this.zzako)
    {
      zzyq();
      this.zzass.zzwm();
      if (this.a != null)
      {
        this.a.close();
        this.a.onDestroy();
        this.a = null;
      }
      this.zzczz.reset();
      if (this.c) {
        return;
      }
      zzu.zzhj().zze(this);
      zzyp();
      this.c = true;
      zzkx.v("Initiating WebView self destruct sequence in 3...");
      this.zzczz.zzxz();
      return;
    }
  }
  
  @TargetApi(19)
  public void evaluateJavascript(String paramString, ValueCallback<String> paramValueCallback)
  {
    synchronized (this.zzako)
    {
      if (isDestroyed())
      {
        zzkx.zzdi("The webview is destroyed. Ignoring action.");
        if (paramValueCallback != null) {
          paramValueCallback.onReceiveValue(null);
        }
        return;
      }
      super.evaluateJavascript(paramString, paramValueCallback);
      return;
    }
  }
  
  protected void finalize()
    throws Throwable
  {
    synchronized (this.zzako)
    {
      if (!this.c)
      {
        this.zzczz.reset();
        zzu.zzhj().zze(this);
        zzyp();
      }
      super.finalize();
      return;
    }
  }
  
  public String getRequestId()
  {
    synchronized (this.zzako)
    {
      String str = this.zzcec;
      return str;
    }
  }
  
  public int getRequestedOrientation()
  {
    synchronized (this.zzako)
    {
      int i1 = this.f;
      return i1;
    }
  }
  
  public View getView()
  {
    return this;
  }
  
  public WebView getWebView()
  {
    return this;
  }
  
  public boolean isDestroyed()
  {
    synchronized (this.zzako)
    {
      boolean bool = this.c;
      return bool;
    }
  }
  
  public void loadData(String paramString1, String paramString2, String paramString3)
  {
    synchronized (this.zzako)
    {
      if (!isDestroyed())
      {
        super.loadData(paramString1, paramString2, paramString3);
        return;
      }
      zzkx.zzdi("The webview is destroyed. Ignoring action.");
    }
  }
  
  public void loadDataWithBaseURL(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
  {
    synchronized (this.zzako)
    {
      if (!isDestroyed())
      {
        super.loadDataWithBaseURL(paramString1, paramString2, paramString3, paramString4, paramString5);
        return;
      }
      zzkx.zzdi("The webview is destroyed. Ignoring action.");
    }
  }
  
  public void loadUrl(String paramString)
  {
    for (;;)
    {
      synchronized (this.zzako)
      {
        boolean bool = isDestroyed();
        if (!bool) {
          try
          {
            super.loadUrl(paramString);
            return;
          }
          catch (Throwable paramString)
          {
            paramString = String.valueOf(paramString);
            zzkx.zzdi(String.valueOf(paramString).length() + 24 + "Could not call loadUrl. " + paramString);
            continue;
          }
        }
      }
      zzkx.zzdi("The webview is destroyed. Ignoring action.");
    }
  }
  
  protected void onAttachedToWindow()
  {
    boolean bool2 = true;
    for (;;)
    {
      synchronized (this.zzako)
      {
        super.onAttachedToWindow();
        if (!isDestroyed()) {
          this.zzass.onAttachedToWindow();
        }
        boolean bool1 = this.j;
        if ((zzxc() != null) && (zzxc().zzxv()))
        {
          bool1 = bool2;
          if (!this.k)
          {
            Object localObject2 = zzxc().zzxw();
            if (localObject2 != null) {
              zzu.zzhk().zza(getView(), (ViewTreeObserver.OnGlobalLayoutListener)localObject2);
            }
            localObject2 = zzxc().zzxx();
            if (localObject2 != null) {
              zzu.zzhk().zza(getView(), (ViewTreeObserver.OnScrollChangedListener)localObject2);
            }
            this.k = true;
            bool1 = bool2;
          }
          zzap(bool1);
          return;
        }
      }
    }
  }
  
  protected void onDetachedFromWindow()
  {
    synchronized (this.zzako)
    {
      if (!isDestroyed()) {
        this.zzass.onDetachedFromWindow();
      }
      super.onDetachedFromWindow();
      if ((this.k) && (zzxc() != null) && (zzxc().zzxv()) && (getViewTreeObserver() != null) && (getViewTreeObserver().isAlive()))
      {
        Object localObject2 = zzxc().zzxw();
        if (localObject2 != null) {
          zzu.zzgo().zza(getViewTreeObserver(), (ViewTreeObserver.OnGlobalLayoutListener)localObject2);
        }
        localObject2 = zzxc().zzxx();
        if (localObject2 != null) {
          getViewTreeObserver().removeOnScrollChangedListener((ViewTreeObserver.OnScrollChangedListener)localObject2);
        }
        this.k = false;
      }
      zzap(false);
      return;
    }
  }
  
  public void onDownloadStart(String paramString1, String paramString2, String paramString3, String paramString4, long paramLong)
  {
    try
    {
      paramString2 = new Intent("android.intent.action.VIEW");
      paramString2.setDataAndType(Uri.parse(paramString1), paramString4);
      zzu.zzgm().zzb(getContext(), paramString2);
      return;
    }
    catch (ActivityNotFoundException paramString2)
    {
      zzkx.zzdg(String.valueOf(paramString1).length() + 51 + String.valueOf(paramString4).length() + "Couldn't find an Activity to view url/mimetype: " + paramString1 + " / " + paramString4);
    }
  }
  
  @TargetApi(21)
  protected void onDraw(Canvas paramCanvas)
  {
    if (isDestroyed()) {}
    do
    {
      do
      {
        return;
      } while ((Build.VERSION.SDK_INT == 21) && (paramCanvas.isHardwareAccelerated()) && (!isAttachedToWindow()));
      super.onDraw(paramCanvas);
    } while ((zzxc() == null) || (zzxc().zzyg() == null));
    zzxc().zzyg().zzff();
  }
  
  public boolean onGenericMotionEvent(MotionEvent paramMotionEvent)
  {
    if (((Boolean)zzdr.zzbfv.get()).booleanValue())
    {
      float f1 = paramMotionEvent.getAxisValue(9);
      float f2 = paramMotionEvent.getAxisValue(10);
      if (paramMotionEvent.getActionMasked() == 8) {}
      for (int i1 = 1; (i1 != 0) && (((f1 > 0.0F) && (!canScrollVertically(-1))) || ((f1 < 0.0F) && (!canScrollVertically(1))) || ((f2 > 0.0F) && (!canScrollHorizontally(-1))) || ((f2 < 0.0F) && (!canScrollHorizontally(1)))); i1 = 0) {
        return false;
      }
    }
    return super.onGenericMotionEvent(paramMotionEvent);
  }
  
  public void onGlobalLayout()
  {
    boolean bool = zzyh();
    com.google.android.gms.ads.internal.overlay.zzd localzzd = zzxa();
    if ((localzzd != null) && (bool)) {
      localzzd.zzpr();
    }
  }
  
  @SuppressLint({"DrawAllocation"})
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    int i3 = Integer.MAX_VALUE;
    synchronized (this.zzako)
    {
      if (isDestroyed())
      {
        setMeasuredDimension(0, 0);
        return;
      }
      if ((isInEditMode()) || (this.d) || (this.zzapp.zzazt))
      {
        super.onMeasure(paramInt1, paramInt2);
        return;
      }
    }
    float f1;
    int i1;
    if (this.zzapp.zzazu)
    {
      if ((((Boolean)zzdr.zzbjk.get()).booleanValue()) || (!com.google.android.gms.common.util.zzs.zzays()))
      {
        super.onMeasure(paramInt1, paramInt2);
        return;
      }
      zza("/contentHeight", zzyl());
      zzdn("(function() {  var height = -1;  if (document.body) { height = document.body.offsetHeight;}  else if (document.documentElement) {      height = document.documentElement.offsetHeight;  }  var url = 'gmsg://mobileads.google.com/contentHeight?';  url += 'height=' + height;  window.googleAdsJsInterface.notify(url);  })();");
      f1 = this.zzczx.getResources().getDisplayMetrics().density;
      i1 = View.MeasureSpec.getSize(paramInt1);
    }
    int i4;
    int i2;
    switch (this.n)
    {
    case -1: 
      for (paramInt1 = (int)(f1 * this.n);; paramInt1 = View.MeasureSpec.getSize(paramInt2))
      {
        setMeasuredDimension(i1, paramInt1);
        return;
      }
      if (this.zzapp.zzazr)
      {
        DisplayMetrics localDisplayMetrics = new DisplayMetrics();
        this.zzati.getDefaultDisplay().getMetrics(localDisplayMetrics);
        setMeasuredDimension(localDisplayMetrics.widthPixels, localDisplayMetrics.heightPixels);
        return;
      }
      int i5 = View.MeasureSpec.getMode(paramInt1);
      i1 = View.MeasureSpec.getSize(paramInt1);
      i4 = View.MeasureSpec.getMode(paramInt2);
      i2 = View.MeasureSpec.getSize(paramInt2);
      if (i5 != Integer.MIN_VALUE) {
        if (i5 != 1073741824) {
          break;
        }
      }
      break;
    }
    for (;;)
    {
      if ((this.zzapp.widthPixels > paramInt1) || (this.zzapp.heightPixels > paramInt2))
      {
        f1 = this.zzczx.getResources().getDisplayMetrics().density;
        paramInt1 = (int)(this.zzapp.widthPixels / f1);
        paramInt2 = (int)(this.zzapp.heightPixels / f1);
        i1 = (int)(i1 / f1);
        i2 = (int)(i2 / f1);
        zzkx.zzdi(103 + "Not enough space to show ad. Needs " + paramInt1 + "x" + paramInt2 + " dp, but only has " + i1 + "x" + i2 + " dp.");
        if (getVisibility() != 8) {
          setVisibility(4);
        }
        setMeasuredDimension(0, 0);
      }
      for (;;)
      {
        return;
        if (getVisibility() != 8) {
          setVisibility(0);
        }
        setMeasuredDimension(this.zzapp.widthPixels, this.zzapp.heightPixels);
      }
      paramInt1 = Integer.MAX_VALUE;
      break label509;
      break;
      paramInt1 = i1;
      label509:
      if (i4 != Integer.MIN_VALUE)
      {
        paramInt2 = i3;
        if (i4 != 1073741824) {}
      }
      else
      {
        paramInt2 = i2;
      }
    }
  }
  
  public void onPause()
  {
    if (isDestroyed()) {}
    for (;;)
    {
      return;
      try
      {
        if (com.google.android.gms.common.util.zzs.zzayn())
        {
          super.onPause();
          return;
        }
      }
      catch (Exception localException)
      {
        zzkx.zzb("Could not pause webview.", localException);
      }
    }
  }
  
  public void onResume()
  {
    if (isDestroyed()) {}
    for (;;)
    {
      return;
      try
      {
        if (com.google.android.gms.common.util.zzs.zzayn())
        {
          super.onResume();
          return;
        }
      }
      catch (Exception localException)
      {
        zzkx.zzb("Could not resume webview.", localException);
      }
    }
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    if (zzxc().zzxv()) {}
    for (;;)
    {
      synchronized (this.zzako)
      {
        if (this.l != null) {
          this.l.zzc(paramMotionEvent);
        }
        if (!isDestroyed()) {
          break;
        }
        return false;
      }
      if (this.zzbnx != null) {
        this.zzbnx.zza(paramMotionEvent);
      }
    }
    return super.onTouchEvent(paramMotionEvent);
  }
  
  public void setContext(Context paramContext)
  {
    this.zzczx.setBaseContext(paramContext);
    this.zzass.zzl(this.zzczx.zzwy());
  }
  
  public void setOnClickListener(View.OnClickListener paramOnClickListener)
  {
    this.r = new WeakReference(paramOnClickListener);
    super.setOnClickListener(paramOnClickListener);
  }
  
  public void setRequestedOrientation(int paramInt)
  {
    synchronized (this.zzako)
    {
      this.f = paramInt;
      if (this.a != null) {
        this.a.setRequestedOrientation(this.f);
      }
      return;
    }
  }
  
  public void setWebViewClient(WebViewClient paramWebViewClient)
  {
    super.setWebViewClient(paramWebViewClient);
    if ((paramWebViewClient instanceof zzme)) {
      this.zzczz = ((zzme)paramWebViewClient);
    }
  }
  
  public void stopLoading()
  {
    if (isDestroyed()) {
      return;
    }
    try
    {
      super.stopLoading();
      return;
    }
    catch (Exception localException)
    {
      zzkx.zzb("Could not stop loading webview.", localException);
    }
  }
  
  public void zza(Context paramContext, AdSizeParcel paramAdSizeParcel, zzdz paramzzdz)
  {
    synchronized (this.zzako)
    {
      this.zzass.zzwm();
      setContext(paramContext);
      this.a = null;
      this.zzapp = paramAdSizeParcel;
      this.d = false;
      this.b = false;
      this.zzcec = "";
      this.f = -1;
      zzu.zzgo().zzm(this);
      loadUrl("about:blank");
      this.zzczz.reset();
      setOnTouchListener(null);
      setOnClickListener(null);
      this.g = true;
      this.h = false;
      this.i = null;
      zzd(paramzzdz);
      this.j = false;
      this.m = 0;
      zzu.zzhj().zze(this);
      zzyp();
      return;
    }
  }
  
  public void zza(AdSizeParcel paramAdSizeParcel)
  {
    synchronized (this.zzako)
    {
      this.zzapp = paramAdSizeParcel;
      requestLayout();
      return;
    }
  }
  
  public void zza(zzcu.zza paramzza)
  {
    synchronized (this.zzako)
    {
      this.j = paramzza.zzave;
      zzap(paramzza.zzave);
      return;
    }
  }
  
  public void zza(zzmi paramzzmi)
  {
    synchronized (this.zzako)
    {
      if (this.i != null)
      {
        zzkx.e("Attempt to create multiple AdWebViewVideoControllers.");
        return;
      }
      this.i = paramzzmi;
      return;
    }
  }
  
  @TargetApi(19)
  protected void zza(String paramString, ValueCallback<String> paramValueCallback)
  {
    synchronized (this.zzako)
    {
      if (!isDestroyed()) {
        evaluateJavascript(paramString, paramValueCallback);
      }
      do
      {
        return;
        zzkx.zzdi("The webview is destroyed. Ignoring action.");
      } while (paramValueCallback == null);
      paramValueCallback.onReceiveValue(null);
    }
  }
  
  public void zza(String paramString, zzfe paramzzfe)
  {
    if (this.zzczz != null) {
      this.zzczz.zza(paramString, paramzzfe);
    }
  }
  
  public void zza(String paramString, Map<String, ?> paramMap)
  {
    try
    {
      paramMap = zzu.zzgm().zzap(paramMap);
      zzb(paramString, paramMap);
      return;
    }
    catch (JSONException paramString)
    {
      zzkx.zzdi("Could not convert parameters to JSON.");
    }
  }
  
  public void zza(String paramString, JSONObject paramJSONObject)
  {
    JSONObject localJSONObject = paramJSONObject;
    if (paramJSONObject == null) {
      localJSONObject = new JSONObject();
    }
    zzi(paramString, localJSONObject.toString());
  }
  
  public void zzak(int paramInt)
  {
    if (paramInt == 0) {
      zzyk();
    }
    zzyj();
    if (this.q.zzly() != null) {
      this.q.zzly().zzg("close_type", String.valueOf(paramInt));
    }
    HashMap localHashMap = new HashMap(2);
    localHashMap.put("closetype", String.valueOf(paramInt));
    localHashMap.put("version", this.zzanu.zzda);
    zza("onhide", localHashMap);
  }
  
  public void zzak(boolean paramBoolean)
  {
    synchronized (this.zzako)
    {
      this.d = paramBoolean;
      zzym();
      return;
    }
  }
  
  public void zzal(boolean paramBoolean)
  {
    synchronized (this.zzako)
    {
      if (this.a != null)
      {
        this.a.zza(this.zzczz.zzic(), paramBoolean);
        return;
      }
      this.b = paramBoolean;
    }
  }
  
  public void zzam(boolean paramBoolean)
  {
    synchronized (this.zzako)
    {
      this.g = paramBoolean;
      return;
    }
  }
  
  public void zzan(boolean paramBoolean)
  {
    for (;;)
    {
      synchronized (this.zzako)
      {
        int i2 = this.m;
        if (paramBoolean)
        {
          i1 = 1;
          this.m = (i1 + i2);
          if ((this.m <= 0) && (this.a != null)) {
            this.a.zzpu();
          }
          return;
        }
      }
      int i1 = -1;
    }
  }
  
  public void zzb(zzg paramzzg)
  {
    synchronized (this.zzako)
    {
      this.l = paramzzg;
      return;
    }
  }
  
  public void zzb(com.google.android.gms.ads.internal.overlay.zzd paramzzd)
  {
    synchronized (this.zzako)
    {
      this.a = paramzzd;
      return;
    }
  }
  
  void zzb(Boolean paramBoolean)
  {
    synchronized (this.zzako)
    {
      this.zzcub = paramBoolean;
      zzu.zzgq().zzb(paramBoolean);
      return;
    }
  }
  
  public void zzb(String paramString, zzfe paramzzfe)
  {
    if (this.zzczz != null) {
      this.zzczz.zzb(paramString, paramzzfe);
    }
  }
  
  public void zzb(String paramString, JSONObject paramJSONObject)
  {
    Object localObject = paramJSONObject;
    if (paramJSONObject == null) {
      localObject = new JSONObject();
    }
    localObject = ((JSONObject)localObject).toString();
    paramJSONObject = new StringBuilder();
    paramJSONObject.append("(window.AFMA_ReceiveMessage || function() {})('");
    paramJSONObject.append(paramString);
    paramJSONObject.append("'");
    paramJSONObject.append(",");
    paramJSONObject.append((String)localObject);
    paramJSONObject.append(");");
    paramString = String.valueOf(paramJSONObject.toString());
    if (paramString.length() != 0) {}
    for (paramString = "Dispatching AFMA event: ".concat(paramString);; paramString = new String("Dispatching AFMA event: "))
    {
      zzkx.zzdg(paramString);
      zzdn(paramJSONObject.toString());
      return;
    }
  }
  
  public void zzc(com.google.android.gms.ads.internal.overlay.zzd paramzzd)
  {
    synchronized (this.zzako)
    {
      this.s = paramzzd;
      return;
    }
  }
  
  public void zzdj(String paramString)
  {
    synchronized (this.zzako)
    {
      try
      {
        super.loadUrl(paramString);
        return;
      }
      catch (Throwable paramString)
      {
        for (;;)
        {
          paramString = String.valueOf(paramString);
          zzkx.zzdi(String.valueOf(paramString).length() + 24 + "Could not call loadUrl. " + paramString);
        }
      }
    }
  }
  
  public void zzdk(String paramString)
  {
    Object localObject = this.zzako;
    String str = paramString;
    if (paramString == null) {
      str = "";
    }
    try
    {
      this.zzcec = str;
      return;
    }
    finally {}
  }
  
  protected void zzdm(String paramString)
  {
    synchronized (this.zzako)
    {
      if (!isDestroyed())
      {
        loadUrl(paramString);
        return;
      }
      zzkx.zzdi("The webview is destroyed. Ignoring action.");
    }
  }
  
  protected void zzdn(String paramString)
  {
    if (com.google.android.gms.common.util.zzs.zzayu())
    {
      if (zzva() == null) {
        zzyi();
      }
      if (zzva().booleanValue())
      {
        zza(paramString, null);
        return;
      }
      paramString = String.valueOf(paramString);
      if (paramString.length() != 0) {}
      for (paramString = "javascript:".concat(paramString);; paramString = new String("javascript:"))
      {
        zzdm(paramString);
        return;
      }
    }
    paramString = String.valueOf(paramString);
    if (paramString.length() != 0) {}
    for (paramString = "javascript:".concat(paramString);; paramString = new String("javascript:"))
    {
      zzdm(paramString);
      return;
    }
  }
  
  public com.google.android.gms.ads.internal.zzd zzec()
  {
    return this.zzamb;
  }
  
  public AdSizeParcel zzeg()
  {
    synchronized (this.zzako)
    {
      AdSizeParcel localAdSizeParcel = this.zzapp;
      return localAdSizeParcel;
    }
  }
  
  public void zzey()
  {
    synchronized (this.zzako)
    {
      this.h = true;
      if (this.zzczy != null) {
        this.zzczy.zzey();
      }
      return;
    }
  }
  
  public void zzez()
  {
    synchronized (this.zzako)
    {
      this.h = false;
      if (this.zzczy != null) {
        this.zzczy.zzez();
      }
      return;
    }
  }
  
  public void zzi(String paramString1, String paramString2)
  {
    zzdn(String.valueOf(paramString1).length() + 3 + String.valueOf(paramString2).length() + paramString1 + "(" + paramString2 + ");");
  }
  
  public void zzps()
  {
    if (this.o == null)
    {
      zzdv.zza(this.q.zzly(), this.zzced, new String[] { "aes2" });
      this.o = zzdv.zzb(this.q.zzly());
      this.q.zza("native:view_show", this.o);
    }
    HashMap localHashMap = new HashMap(1);
    localHashMap.put("version", this.zzanu.zzda);
    zza("onshow", localHashMap);
  }
  
  Boolean zzva()
  {
    synchronized (this.zzako)
    {
      Boolean localBoolean = this.zzcub;
      return localBoolean;
    }
  }
  
  public void zzww()
  {
    zzyj();
    HashMap localHashMap = new HashMap(1);
    localHashMap.put("version", this.zzanu.zzda);
    zza("onhide", localHashMap);
  }
  
  public void zzwx()
  {
    HashMap localHashMap = new HashMap(3);
    localHashMap.put("app_muted", String.valueOf(zzu.zzgm().zzft()));
    localHashMap.put("app_volume", String.valueOf(zzu.zzgm().zzfr()));
    localHashMap.put("device_volume", String.valueOf(zzu.zzgm().zzah(getContext())));
    zza("volume", localHashMap);
  }
  
  public Activity zzwy()
  {
    return this.zzczx.zzwy();
  }
  
  public Context zzwz()
  {
    return this.zzczx.zzwz();
  }
  
  public com.google.android.gms.ads.internal.overlay.zzd zzxa()
  {
    synchronized (this.zzako)
    {
      com.google.android.gms.ads.internal.overlay.zzd localzzd = this.a;
      return localzzd;
    }
  }
  
  public com.google.android.gms.ads.internal.overlay.zzd zzxb()
  {
    synchronized (this.zzako)
    {
      com.google.android.gms.ads.internal.overlay.zzd localzzd = this.s;
      return localzzd;
    }
  }
  
  public zzme zzxc()
  {
    return this.zzczz;
  }
  
  public boolean zzxd()
  {
    synchronized (this.zzako)
    {
      boolean bool = this.b;
      return bool;
    }
  }
  
  public zzav zzxe()
  {
    return this.zzbnx;
  }
  
  public VersionInfoParcel zzxf()
  {
    return this.zzanu;
  }
  
  public boolean zzxg()
  {
    synchronized (this.zzako)
    {
      boolean bool = this.d;
      return bool;
    }
  }
  
  public void zzxh()
  {
    synchronized (this.zzako)
    {
      zzkx.v("Destroying WebView!");
      zzlb.zzcvl.post(new zzmh.2(this));
      return;
    }
  }
  
  public boolean zzxi()
  {
    synchronized (this.zzako)
    {
      boolean bool = this.g;
      return bool;
    }
  }
  
  public boolean zzxj()
  {
    synchronized (this.zzako)
    {
      boolean bool = this.h;
      return bool;
    }
  }
  
  public zzmc zzxk()
  {
    return null;
  }
  
  public zzdx zzxl()
  {
    return this.zzced;
  }
  
  public zzdy zzxm()
  {
    return this.q;
  }
  
  public zzmi zzxn()
  {
    synchronized (this.zzako)
    {
      zzmi localzzmi = this.i;
      return localzzmi;
    }
  }
  
  public boolean zzxo()
  {
    for (;;)
    {
      synchronized (this.zzako)
      {
        if (this.m > 0)
        {
          bool = true;
          return bool;
        }
      }
      boolean bool = false;
    }
  }
  
  public void zzxp()
  {
    this.zzass.zzwl();
  }
  
  public void zzxq()
  {
    if (this.p == null)
    {
      this.p = zzdv.zzb(this.q.zzly());
      this.q.zza("native:view_load", this.p);
    }
  }
  
  public View.OnClickListener zzxr()
  {
    return (View.OnClickListener)this.r.get();
  }
  
  public zzg zzxs()
  {
    synchronized (this.zzako)
    {
      zzg localzzg = this.l;
      return localzzg;
    }
  }
  
  public void zzxt()
  {
    setBackgroundColor(0);
  }
  
  public boolean zzyh()
  {
    if ((!zzxc().zzic()) && (!zzxc().zzxv())) {
      return false;
    }
    DisplayMetrics localDisplayMetrics = zzu.zzgm().zza(this.zzati);
    int i3 = zzm.zzkr().zzb(localDisplayMetrics, localDisplayMetrics.widthPixels);
    int i4 = zzm.zzkr().zzb(localDisplayMetrics, localDisplayMetrics.heightPixels);
    Object localObject = zzwy();
    int i2;
    int i1;
    if ((localObject == null) || (((Activity)localObject).getWindow() == null))
    {
      i2 = i4;
      i1 = i3;
      label87:
      if ((this.zzbzd == i3) && (this.zzbze == i4) && (this.zzbzg == i1) && (this.zzbzh == i2)) {
        break label234;
      }
      if ((this.zzbzd == i3) && (this.zzbze == i4)) {
        break label236;
      }
    }
    label234:
    label236:
    for (boolean bool = true;; bool = false)
    {
      this.zzbzd = i3;
      this.zzbze = i4;
      this.zzbzg = i1;
      this.zzbzh = i2;
      new zzhv(this).zza(i3, i4, i1, i2, localDisplayMetrics.density, this.zzati.getDefaultDisplay().getRotation());
      return bool;
      localObject = zzu.zzgm().zzh((Activity)localObject);
      i1 = zzm.zzkr().zzb(localDisplayMetrics, localObject[0]);
      i2 = zzm.zzkr().zzb(localDisplayMetrics, localObject[1]);
      break label87;
      break;
    }
  }
  
  zzfe zzyl()
  {
    return new zzmh.1(this);
  }
  
  @zzji
  public static class zza
    extends MutableContextWrapper
  {
    private Context v;
    private Context zzatc;
    private Activity zzcxl;
    
    public zza(Context paramContext)
    {
      super();
      setBaseContext(paramContext);
    }
    
    public Object getSystemService(String paramString)
    {
      return this.v.getSystemService(paramString);
    }
    
    public void setBaseContext(Context paramContext)
    {
      this.zzatc = paramContext.getApplicationContext();
      if ((paramContext instanceof Activity)) {}
      for (Activity localActivity = (Activity)paramContext;; localActivity = null)
      {
        this.zzcxl = localActivity;
        this.v = paramContext;
        super.setBaseContext(this.zzatc);
        return;
      }
    }
    
    public void startActivity(Intent paramIntent)
    {
      if (this.zzcxl != null)
      {
        this.zzcxl.startActivity(paramIntent);
        return;
      }
      paramIntent.setFlags(268435456);
      this.zzatc.startActivity(paramIntent);
    }
    
    public Activity zzwy()
    {
      return this.zzcxl;
    }
    
    public Context zzwz()
    {
      return this.v;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzmh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */