package com.google.android.gms.internal;

import android.support.annotation.Nullable;
import com.google.android.gms.ads.internal.client.zzab.zza;
import com.google.android.gms.ads.internal.client.zzac;
import com.google.android.gms.ads.internal.zzu;
import com.google.android.gms.common.util.zzf;
import java.util.HashMap;
import java.util.Map;

@zzji
public class zzmi
  extends zzab.zza
{
  private zzac A;
  private boolean B;
  private boolean C;
  private float D;
  private float E;
  private final float w;
  private int z;
  private final Object zzako = new Object();
  private boolean zzakr = true;
  private final zzmd zzbnz;
  
  public zzmi(zzmd paramzzmd, float paramFloat)
  {
    this.zzbnz = paramzzmd;
    this.w = paramFloat;
  }
  
  private void zzdo(String paramString)
  {
    zze(paramString, null);
  }
  
  private void zze(String paramString, @Nullable Map<String, String> paramMap)
  {
    if (paramMap == null) {}
    for (paramMap = new HashMap();; paramMap = new HashMap(paramMap))
    {
      paramMap.put("action", paramString);
      zzu.zzgm().runOnUiThread(new zzmi.1(this, paramMap));
      return;
    }
  }
  
  private void zzk(int paramInt1, int paramInt2)
  {
    zzu.zzgm().runOnUiThread(new zzmi.2(this, paramInt1, paramInt2));
  }
  
  public float getAspectRatio()
  {
    synchronized (this.zzako)
    {
      float f = this.E;
      return f;
    }
  }
  
  public int getPlaybackState()
  {
    synchronized (this.zzako)
    {
      int i = this.z;
      return i;
    }
  }
  
  public boolean isMuted()
  {
    synchronized (this.zzako)
    {
      boolean bool = this.C;
      return bool;
    }
  }
  
  public void pause()
  {
    zzdo("pause");
  }
  
  public void play()
  {
    zzdo("play");
  }
  
  public void zza(float paramFloat1, int paramInt, boolean paramBoolean, float paramFloat2)
  {
    synchronized (this.zzako)
    {
      this.D = paramFloat1;
      this.C = paramBoolean;
      int i = this.z;
      this.z = paramInt;
      this.E = paramFloat2;
      zzk(i, paramInt);
      return;
    }
  }
  
  public void zza(zzac paramzzac)
  {
    synchronized (this.zzako)
    {
      this.A = paramzzac;
      return;
    }
  }
  
  public void zzaq(boolean paramBoolean)
  {
    for (;;)
    {
      synchronized (this.zzako)
      {
        this.zzakr = paramBoolean;
        if (paramBoolean)
        {
          ??? = "1";
          zze("initialState", zzf.zze("muteStart", ???));
          return;
        }
      }
      ??? = "0";
    }
  }
  
  public float zzku()
  {
    return this.w;
  }
  
  public float zzkv()
  {
    synchronized (this.zzako)
    {
      float f = this.D;
      return f;
    }
  }
  
  public void zzn(boolean paramBoolean)
  {
    if (paramBoolean) {}
    for (String str = "mute";; str = "unmute")
    {
      zzdo(str);
      return;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzmi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */