package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;

@zzji
@TargetApi(11)
public class zzml
  extends zzmn
{
  public zzml(zzmd paramzzmd, boolean paramBoolean)
  {
    super(paramzzmd, paramBoolean);
  }
  
  public WebResourceResponse shouldInterceptRequest(WebView paramWebView, String paramString)
  {
    return zza(paramWebView, paramString, null);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzml.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */