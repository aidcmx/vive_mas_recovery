package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.view.View;
import android.webkit.WebChromeClient.CustomViewCallback;

@zzji
@TargetApi(14)
public final class zzmm
  extends zzmk
{
  public zzmm(zzmd paramzzmd)
  {
    super(paramzzmd);
  }
  
  public void onShowCustomView(View paramView, int paramInt, WebChromeClient.CustomViewCallback paramCustomViewCallback)
  {
    zza(paramView, paramInt, paramCustomViewCallback);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzmm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */