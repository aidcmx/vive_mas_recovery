package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.content.Context;
import android.support.annotation.Nullable;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.safebrowsing.zzc;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zzu;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@zzji
@TargetApi(11)
public class zzmn
  extends zzme
{
  public zzmn(zzmd paramzzmd, boolean paramBoolean)
  {
    super(paramzzmd, paramBoolean);
  }
  
  protected WebResourceResponse zza(WebView paramWebView, String paramString, @Nullable Map<String, String> paramMap)
  {
    if (!(paramWebView instanceof zzmd))
    {
      zzkx.zzdi("Tried to intercept request from a WebView that wasn't an AdWebView.");
      return null;
    }
    zzmd localzzmd = (zzmd)paramWebView;
    if (this.zzczo != null) {
      this.zzczo.zzb(paramString, paramMap);
    }
    if (!"mraid.js".equalsIgnoreCase(new File(paramString).getName())) {
      return super.shouldInterceptRequest(paramWebView, paramString);
    }
    if (localzzmd.zzxc() != null) {
      localzzmd.zzxc().zzpo();
    }
    if (localzzmd.zzeg().zzazr) {
      paramWebView = (String)zzdr.zzbep.get();
    }
    for (;;)
    {
      try
      {
        paramWebView = zzf(localzzmd.getContext(), localzzmd.zzxf().zzda, paramWebView);
        return paramWebView;
      }
      catch (InterruptedException paramWebView)
      {
        paramWebView = String.valueOf(paramWebView.getMessage());
        if (paramWebView.length() == 0) {
          continue;
        }
        paramWebView = "Could not fetch MRAID JS. ".concat(paramWebView);
        zzkx.zzdi(paramWebView);
        return null;
        paramWebView = new String("Could not fetch MRAID JS. ");
        continue;
      }
      catch (ExecutionException paramWebView)
      {
        continue;
      }
      catch (IOException paramWebView)
      {
        continue;
      }
      catch (TimeoutException paramWebView)
      {
        continue;
      }
      if (localzzmd.zzxg()) {
        paramWebView = (String)zzdr.zzbeo.get();
      } else {
        paramWebView = (String)zzdr.zzben.get();
      }
    }
  }
  
  protected WebResourceResponse zzf(Context paramContext, String paramString1, String paramString2)
    throws IOException, ExecutionException, InterruptedException, TimeoutException
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put("User-Agent", zzu.zzgm().zzh(paramContext, paramString1));
    localHashMap.put("Cache-Control", "max-stale=3600");
    paramContext = (String)new zzli(paramContext).zzd(paramString2, localHashMap).get(60L, TimeUnit.SECONDS);
    if (paramContext == null) {
      return null;
    }
    return new WebResourceResponse("application/javascript", "UTF-8", new ByteArrayInputStream(paramContext.getBytes("UTF-8")));
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzmn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */