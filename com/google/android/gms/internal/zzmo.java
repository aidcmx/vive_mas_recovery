package com.google.android.gms.internal;

import android.text.TextUtils;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.google.android.gms.common.internal.zzz;
import java.net.URI;
import java.net.URISyntaxException;

@zzji
public class zzmo
  extends WebViewClient
{
  private final String N = zzdq(paramString);
  private boolean O = false;
  private final zzmd zzbnz;
  private final zzir zzcgu;
  
  public zzmo(zzir paramzzir, zzmd paramzzmd, String paramString)
  {
    this.zzbnz = paramzzmd;
    this.zzcgu = paramzzir;
  }
  
  private String zzdq(String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {}
    for (;;)
    {
      return paramString;
      try
      {
        if (paramString.endsWith("/"))
        {
          String str = paramString.substring(0, paramString.length() - 1);
          return str;
        }
      }
      catch (IndexOutOfBoundsException localIndexOutOfBoundsException)
      {
        zzkx.e(localIndexOutOfBoundsException.getMessage());
      }
    }
    return paramString;
  }
  
  public void onLoadResource(WebView paramWebView, String paramString)
  {
    paramWebView = String.valueOf(paramString);
    if (paramWebView.length() != 0) {}
    for (paramWebView = "JavascriptAdWebViewClient::onLoadResource: ".concat(paramWebView);; paramWebView = new String("JavascriptAdWebViewClient::onLoadResource: "))
    {
      zzkx.zzdg(paramWebView);
      if (!zzdp(paramString)) {
        this.zzbnz.zzxc().onLoadResource(this.zzbnz.getWebView(), paramString);
      }
      return;
    }
  }
  
  public void onPageFinished(WebView paramWebView, String paramString)
  {
    paramWebView = String.valueOf(paramString);
    if (paramWebView.length() != 0) {}
    for (paramWebView = "JavascriptAdWebViewClient::onPageFinished: ".concat(paramWebView);; paramWebView = new String("JavascriptAdWebViewClient::onPageFinished: "))
    {
      zzkx.zzdg(paramWebView);
      if (!this.O)
      {
        this.zzcgu.zzsa();
        this.O = true;
      }
      return;
    }
  }
  
  public boolean shouldOverrideUrlLoading(WebView paramWebView, String paramString)
  {
    paramWebView = String.valueOf(paramString);
    if (paramWebView.length() != 0) {}
    for (paramWebView = "JavascriptAdWebViewClient::shouldOverrideUrlLoading: ".concat(paramWebView);; paramWebView = new String("JavascriptAdWebViewClient::shouldOverrideUrlLoading: "))
    {
      zzkx.zzdg(paramWebView);
      if (!zzdp(paramString)) {
        break;
      }
      zzkx.zzdg("shouldOverrideUrlLoading: received passback url");
      return true;
    }
    return this.zzbnz.zzxc().shouldOverrideUrlLoading(this.zzbnz.getWebView(), paramString);
  }
  
  protected boolean zzdp(String paramString)
  {
    paramString = zzdq(paramString);
    if (TextUtils.isEmpty(paramString)) {}
    for (;;)
    {
      return false;
      try
      {
        Object localObject1 = new URI(paramString);
        if ("passback".equals(((URI)localObject1).getScheme()))
        {
          zzkx.zzdg("Passback received");
          this.zzcgu.zzsb();
          return true;
        }
        if (!TextUtils.isEmpty(this.N))
        {
          Object localObject2 = new URI(this.N);
          paramString = ((URI)localObject2).getHost();
          String str = ((URI)localObject1).getHost();
          localObject2 = ((URI)localObject2).getPath();
          localObject1 = ((URI)localObject1).getPath();
          if ((zzz.equal(paramString, str)) && (zzz.equal(localObject2, localObject1)))
          {
            zzkx.zzdg("Passback received");
            this.zzcgu.zzsb();
            return true;
          }
        }
      }
      catch (URISyntaxException paramString)
      {
        zzkx.e(paramString.getMessage());
      }
    }
    return false;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzmo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */