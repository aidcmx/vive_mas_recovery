package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;

@zzji
@TargetApi(21)
public class zzmp
  extends zzmn
{
  public zzmp(zzmd paramzzmd, boolean paramBoolean)
  {
    super(paramzzmd, paramBoolean);
  }
  
  @Nullable
  public WebResourceResponse shouldInterceptRequest(WebView paramWebView, WebResourceRequest paramWebResourceRequest)
  {
    if ((paramWebResourceRequest == null) || (paramWebResourceRequest.getUrl() == null)) {
      return null;
    }
    return zza(paramWebView, paramWebResourceRequest.getUrl().toString(), paramWebResourceRequest.getRequestHeaders());
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzmp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */