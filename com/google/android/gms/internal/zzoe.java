package com.google.android.gms.internal;

public enum zzoe
{
  private final String kW;
  
  private zzoe(String paramString)
  {
    this.kW = paramString;
  }
  
  public static boolean zza(zzoe paramzzoe)
  {
    return (ke.equals(paramzzoe)) || (kn.equals(paramzzoe)) || (kq.equals(paramzzoe)) || (kr.equals(paramzzoe)) || (ki.equals(paramzzoe)) || (kt.equals(paramzzoe)) || (jX.equals(paramzzoe)) || (ky.equals(paramzzoe)) || (kz.equals(paramzzoe)) || (kA.equals(paramzzoe)) || (kB.equals(paramzzoe)) || (kC.equals(paramzzoe)) || (kD.equals(paramzzoe)) || (kE.equals(paramzzoe)) || (kx.equals(paramzzoe));
  }
  
  public static boolean zzb(zzoe paramzzoe)
  {
    return (kb.equals(paramzzoe)) || (kc.equals(paramzzoe));
  }
  
  public static final zzoe zzgi(String paramString)
  {
    Object localObject = null;
    zzoe[] arrayOfzzoe = values();
    int j = arrayOfzzoe.length;
    int i = 0;
    if (i < j)
    {
      zzoe localzzoe = arrayOfzzoe[i];
      if (!localzzoe.kW.equals(paramString)) {
        break label48;
      }
      localObject = localzzoe;
    }
    label48:
    for (;;)
    {
      i += 1;
      break;
      return (zzoe)localObject;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzoe.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */