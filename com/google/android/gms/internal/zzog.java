package com.google.android.gms.internal;

import android.app.Service;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.IBinder;
import android.os.RemoteException;
import com.google.android.gms.cast.framework.CastOptions;
import com.google.android.gms.cast.framework.media.CastMediaOptions;
import com.google.android.gms.cast.framework.media.zzc;
import com.google.android.gms.cast.framework.zzf;
import com.google.android.gms.cast.framework.zzg;
import com.google.android.gms.cast.framework.zzh;
import com.google.android.gms.cast.framework.zzl;
import com.google.android.gms.cast.framework.zzq;
import com.google.android.gms.dynamic.zzd;
import com.google.android.gms.dynamic.zze;
import java.util.Map;

public class zzog
{
  private static final com.google.android.gms.cast.internal.zzm oT = new com.google.android.gms.cast.internal.zzm("CastDynamiteModule");
  
  public static zzc zza(Service paramService, zzd paramzzd1, zzd paramzzd2, CastMediaOptions paramCastMediaOptions)
  {
    zzoj localzzoj = zzbe(paramService.getApplicationContext());
    try
    {
      paramService = localzzoj.zza(zze.zzac(paramService), paramzzd1, paramzzd2, paramCastMediaOptions);
      return paramService;
    }
    catch (RemoteException paramService)
    {
      oT.zzb(paramService, "Unable to call %s on %s.", new Object[] { "newMediaNotificationServiceImpl", zzoj.class.getSimpleName() });
    }
    return null;
  }
  
  public static zzg zza(Context paramContext, CastOptions paramCastOptions, zzok paramzzok, Map<String, IBinder> paramMap)
  {
    zzoj localzzoj = zzbe(paramContext);
    try
    {
      paramContext = localzzoj.zza(zze.zzac(paramContext.getApplicationContext()), paramCastOptions, paramzzok, paramMap);
      return paramContext;
    }
    catch (RemoteException paramContext)
    {
      oT.zzb(paramContext, "Unable to call %s on %s.", new Object[] { "newCastContextImpl", zzoj.class.getSimpleName() });
    }
    return null;
  }
  
  public static zzh zza(Context paramContext, CastOptions paramCastOptions, zzd paramzzd, zzf paramzzf)
  {
    paramContext = zzbe(paramContext);
    try
    {
      paramContext = paramContext.zza(paramCastOptions, paramzzd, paramzzf);
      return paramContext;
    }
    catch (RemoteException paramContext)
    {
      oT.zzb(paramContext, "Unable to call %s on %s.", new Object[] { "newCastSessionImpl", zzoj.class.getSimpleName() });
    }
    return null;
  }
  
  public static zzl zza(Service paramService, zzd paramzzd1, zzd paramzzd2)
  {
    zzoj localzzoj = zzbe(paramService.getApplicationContext());
    paramService = zze.zzac(paramService);
    try
    {
      paramService = localzzoj.zza(paramService, paramzzd1, paramzzd2);
      return paramService;
    }
    catch (RemoteException paramService)
    {
      oT.zzb(paramService, "Unable to call %s on %s.", new Object[] { "newReconnectionServiceImpl", zzoj.class.getSimpleName() });
    }
    return null;
  }
  
  public static com.google.android.gms.cast.framework.zzm zza(Context paramContext, String paramString1, String paramString2, zzq paramzzq)
  {
    paramContext = zzbe(paramContext);
    try
    {
      paramContext = paramContext.zza(paramString1, paramString2, paramzzq);
      return paramContext;
    }
    catch (RemoteException paramContext)
    {
      oT.zzb(paramContext, "Unable to call %s on %s.", new Object[] { "newSessionImpl", zzoj.class.getSimpleName() });
    }
    return null;
  }
  
  public static zzos zza(Context paramContext, AsyncTask<Uri, Long, Bitmap> paramAsyncTask, zzot paramzzot, int paramInt1, int paramInt2, boolean paramBoolean, long paramLong, int paramInt3, int paramInt4, int paramInt5)
  {
    paramContext = zzbe(paramContext.getApplicationContext());
    try
    {
      paramContext = paramContext.zza(zze.zzac(paramAsyncTask), paramzzot, paramInt1, paramInt2, paramBoolean, paramLong, paramInt3, paramInt4, paramInt5);
      return paramContext;
    }
    catch (RemoteException paramContext)
    {
      oT.zzb(paramContext, "Unable to call %s on %s.", new Object[] { "newFetchBitmapTaskImpl", zzoj.class.getSimpleName() });
    }
    return null;
  }
  
  private static zzoj zzbe(Context paramContext)
  {
    try
    {
      paramContext = zzoj.zza.zzcz(zztl.zza(paramContext, zztl.Qm, "com.google.android.gms.cast.framework.dynamite").zzjd("com.google.android.gms.cast.framework.internal.CastDynamiteModuleImpl"));
      return paramContext;
    }
    catch (zztl.zza paramContext)
    {
      throw new RuntimeException(paramContext);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzog.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */