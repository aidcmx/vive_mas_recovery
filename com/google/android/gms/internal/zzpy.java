package com.google.android.gms.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.Looper;
import android.os.RemoteException;
import com.google.android.gms.cast.CastDevice;
import com.google.android.gms.cast.CastRemoteDisplay.CastRemoteDisplaySessionCallbacks;
import com.google.android.gms.cast.internal.zzm;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzj;

public class zzpy
  extends zzj<zzqa>
  implements IBinder.DeathRecipient
{
  private static final zzm mz = new zzm("CastRemoteDisplayClientImpl");
  private CastDevice mg;
  private CastRemoteDisplay.CastRemoteDisplaySessionCallbacks vY;
  private Bundle vZ;
  
  public zzpy(Context paramContext, Looper paramLooper, zzf paramzzf, CastDevice paramCastDevice, Bundle paramBundle, CastRemoteDisplay.CastRemoteDisplaySessionCallbacks paramCastRemoteDisplaySessionCallbacks, GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
  {
    super(paramContext, paramLooper, 83, paramzzf, paramConnectionCallbacks, paramOnConnectionFailedListener);
    mz.zzb("instance created", new Object[0]);
    this.vY = paramCastRemoteDisplaySessionCallbacks;
    this.mg = paramCastDevice;
    this.vZ = paramBundle;
  }
  
  public void binderDied() {}
  
  /* Error */
  public void disconnect()
  {
    // Byte code:
    //   0: getstatic 27	com/google/android/gms/internal/zzpy:mz	Lcom/google/android/gms/cast/internal/zzm;
    //   3: ldc 56
    //   5: iconst_0
    //   6: anewarray 36	java/lang/Object
    //   9: invokevirtual 40	com/google/android/gms/cast/internal/zzm:zzb	(Ljava/lang/String;[Ljava/lang/Object;)V
    //   12: aload_0
    //   13: aconst_null
    //   14: putfield 42	com/google/android/gms/internal/zzpy:vY	Lcom/google/android/gms/cast/CastRemoteDisplay$CastRemoteDisplaySessionCallbacks;
    //   17: aload_0
    //   18: aconst_null
    //   19: putfield 44	com/google/android/gms/internal/zzpy:mg	Lcom/google/android/gms/cast/CastDevice;
    //   22: aload_0
    //   23: invokevirtual 60	com/google/android/gms/internal/zzpy:zzavg	()Landroid/os/IInterface;
    //   26: checkcast 62	com/google/android/gms/internal/zzqa
    //   29: invokeinterface 64 1 0
    //   34: aload_0
    //   35: invokespecial 65	com/google/android/gms/common/internal/zzj:disconnect	()V
    //   38: return
    //   39: astore_1
    //   40: aload_0
    //   41: invokespecial 65	com/google/android/gms/common/internal/zzj:disconnect	()V
    //   44: return
    //   45: astore_1
    //   46: aload_0
    //   47: invokespecial 65	com/google/android/gms/common/internal/zzj:disconnect	()V
    //   50: aload_1
    //   51: athrow
    //   52: astore_1
    //   53: goto -13 -> 40
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	56	0	this	zzpy
    //   39	1	1	localRemoteException	RemoteException
    //   45	6	1	localObject	Object
    //   52	1	1	localIllegalStateException	IllegalStateException
    // Exception table:
    //   from	to	target	type
    //   22	34	39	android/os/RemoteException
    //   22	34	45	finally
    //   22	34	52	java/lang/IllegalStateException
  }
  
  public void zza(zzpz paramzzpz)
    throws RemoteException
  {
    mz.zzb("stopRemoteDisplay", new Object[0]);
    ((zzqa)zzavg()).zza(paramzzpz);
  }
  
  public void zza(zzpz paramzzpz, int paramInt)
    throws RemoteException
  {
    ((zzqa)zzavg()).zza(paramzzpz, paramInt);
  }
  
  public void zza(zzpz paramzzpz, zzqb paramzzqb, String paramString)
    throws RemoteException
  {
    mz.zzb("startRemoteDisplay", new Object[0]);
    paramzzqb = new zzpy.1(this, paramzzqb);
    ((zzqa)zzavg()).zza(paramzzpz, paramzzqb, this.mg.getDeviceId(), paramString, this.vZ);
  }
  
  public zzqa zzdi(IBinder paramIBinder)
  {
    return zzqa.zza.zzdk(paramIBinder);
  }
  
  protected String zzjx()
  {
    return "com.google.android.gms.cast.remote_display.service.START";
  }
  
  protected String zzjy()
  {
    return "com.google.android.gms.cast.remote_display.ICastRemoteDisplayService";
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzpy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */