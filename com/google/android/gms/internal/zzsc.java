package com.google.android.gms.internal;

import android.os.Looper;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;

public class zzsc
  extends zzqq<Status>
{
  @Deprecated
  public zzsc(Looper paramLooper)
  {
    super(paramLooper);
  }
  
  public zzsc(GoogleApiClient paramGoogleApiClient)
  {
    super(paramGoogleApiClient);
  }
  
  protected Status zzb(Status paramStatus)
  {
    return paramStatus;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/internal/zzsc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */