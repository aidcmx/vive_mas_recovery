package com.google.android.gms.location.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.Nullable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.location.LocationRequest;
import java.util.Collections;
import java.util.List;

public class LocationRequestInternal
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<LocationRequestInternal> CREATOR = new zzm();
  static final List<ClientIdentity> alc = ;
  LocationRequest VR;
  List<ClientIdentity> ajK;
  boolean ajy;
  boolean ald;
  boolean ale;
  @Nullable
  String mTag;
  private final int mVersionCode;
  
  LocationRequestInternal(int paramInt, LocationRequest paramLocationRequest, boolean paramBoolean1, List<ClientIdentity> paramList, @Nullable String paramString, boolean paramBoolean2, boolean paramBoolean3)
  {
    this.mVersionCode = paramInt;
    this.VR = paramLocationRequest;
    this.ajy = paramBoolean1;
    this.ajK = paramList;
    this.mTag = paramString;
    this.ald = paramBoolean2;
    this.ale = paramBoolean3;
  }
  
  public static LocationRequestInternal zza(@Nullable String paramString, LocationRequest paramLocationRequest)
  {
    return new LocationRequestInternal(1, paramLocationRequest, true, alc, paramString, false, false);
  }
  
  @Deprecated
  public static LocationRequestInternal zzb(LocationRequest paramLocationRequest)
  {
    return zza(null, paramLocationRequest);
  }
  
  public boolean equals(Object paramObject)
  {
    if (!(paramObject instanceof LocationRequestInternal)) {}
    do
    {
      return false;
      paramObject = (LocationRequestInternal)paramObject;
    } while ((!zzz.equal(this.VR, ((LocationRequestInternal)paramObject).VR)) || (this.ajy != ((LocationRequestInternal)paramObject).ajy) || (this.ald != ((LocationRequestInternal)paramObject).ald) || (!zzz.equal(this.ajK, ((LocationRequestInternal)paramObject).ajK)) || (this.ale != ((LocationRequestInternal)paramObject).ale));
    return true;
  }
  
  int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    return this.VR.hashCode();
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(this.VR.toString());
    if (this.mTag != null) {
      localStringBuilder.append(" tag=").append(this.mTag);
    }
    localStringBuilder.append(" trigger=").append(this.ajy);
    localStringBuilder.append(" hideAppOps=").append(this.ald);
    localStringBuilder.append(" clients=").append(this.ajK);
    localStringBuilder.append(" forceCoarseLocation=").append(this.ale);
    return localStringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzm.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/location/internal/LocationRequestInternal.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */