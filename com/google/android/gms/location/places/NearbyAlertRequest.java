package com.google.android.gms.location.places;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;
import java.util.Set;

public final class NearbyAlertRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<NearbyAlertRequest> CREATOR = new zzd();
  private final int ajQ;
  private final int alE;
  @Deprecated
  private final PlaceFilter alF;
  private final NearbyAlertFilter alG;
  private final boolean alH;
  private final int alI;
  private int mPriority = 110;
  private final int mVersionCode;
  
  NearbyAlertRequest(int paramInt1, int paramInt2, int paramInt3, PlaceFilter paramPlaceFilter, NearbyAlertFilter paramNearbyAlertFilter, boolean paramBoolean, int paramInt4, int paramInt5)
  {
    this.mVersionCode = paramInt1;
    this.ajQ = paramInt2;
    this.alE = paramInt3;
    if (paramNearbyAlertFilter != null) {
      this.alG = paramNearbyAlertFilter;
    }
    for (;;)
    {
      this.alF = null;
      this.alH = paramBoolean;
      this.alI = paramInt4;
      this.mPriority = paramInt5;
      return;
      if (paramPlaceFilter != null)
      {
        if ((paramPlaceFilter.getPlaceIds() != null) && (!paramPlaceFilter.getPlaceIds().isEmpty())) {
          this.alG = NearbyAlertFilter.zzm(paramPlaceFilter.getPlaceIds());
        } else if ((paramPlaceFilter.zzbqv() != null) && (!paramPlaceFilter.zzbqv().isEmpty())) {
          this.alG = NearbyAlertFilter.zzn(paramPlaceFilter.zzbqv());
        } else {
          this.alG = null;
        }
      }
      else {
        this.alG = null;
      }
    }
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    do
    {
      return true;
      if (!(paramObject instanceof NearbyAlertRequest)) {
        return false;
      }
      paramObject = (NearbyAlertRequest)paramObject;
    } while ((this.ajQ == ((NearbyAlertRequest)paramObject).ajQ) && (this.alE == ((NearbyAlertRequest)paramObject).alE) && (zzz.equal(this.alG, ((NearbyAlertRequest)paramObject).alG)) && (this.mPriority == ((NearbyAlertRequest)paramObject).mPriority));
    return false;
  }
  
  public int getPriority()
  {
    return this.mPriority;
  }
  
  public int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { Integer.valueOf(this.ajQ), Integer.valueOf(this.alE), this.alG, Integer.valueOf(this.mPriority) });
  }
  
  public String toString()
  {
    return zzz.zzx(this).zzg("transitionTypes", Integer.valueOf(this.ajQ)).zzg("loiteringTimeMillis", Integer.valueOf(this.alE)).zzg("nearbyAlertFilter", this.alG).zzg("priority", Integer.valueOf(this.mPriority)).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzd.zza(this, paramParcel, paramInt);
  }
  
  public int zzbqm()
  {
    return this.ajQ;
  }
  
  public int zzbqq()
  {
    return this.alE;
  }
  
  @Deprecated
  public PlaceFilter zzbqr()
  {
    return null;
  }
  
  public NearbyAlertFilter zzbqs()
  {
    return this.alG;
  }
  
  public boolean zzbqt()
  {
    return this.alH;
  }
  
  public int zzbqu()
  {
    return this.alI;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/location/places/NearbyAlertRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */