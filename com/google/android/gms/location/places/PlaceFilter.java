package com.google.android.gms.location.places;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.Nullable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public final class PlaceFilter
  extends AbstractPlaceFilter
{
  public static final Parcelable.Creator<PlaceFilter> CREATOR = new zzf();
  private static final PlaceFilter alM = new PlaceFilter();
  private final Set<String> alB;
  private final Set<Integer> alC;
  private final Set<UserDataType> alD;
  final boolean alN;
  final List<String> alw;
  final List<Integer> alx;
  final List<UserDataType> aly;
  final int mVersionCode;
  
  public PlaceFilter()
  {
    this(false, null);
  }
  
  PlaceFilter(int paramInt, @Nullable List<Integer> paramList, boolean paramBoolean, @Nullable List<String> paramList1, @Nullable List<UserDataType> paramList2)
  {
    this.mVersionCode = paramInt;
    if (paramList == null)
    {
      paramList = Collections.emptyList();
      this.alx = paramList;
      this.alN = paramBoolean;
      if (paramList2 != null) {
        break label97;
      }
      paramList = Collections.emptyList();
      label36:
      this.aly = paramList;
      if (paramList1 != null) {
        break label106;
      }
    }
    label97:
    label106:
    for (paramList = Collections.emptyList();; paramList = Collections.unmodifiableList(paramList1))
    {
      this.alw = paramList;
      this.alC = zzac(this.alx);
      this.alD = zzac(this.aly);
      this.alB = zzac(this.alw);
      return;
      paramList = Collections.unmodifiableList(paramList);
      break;
      paramList = Collections.unmodifiableList(paramList2);
      break label36;
    }
  }
  
  public PlaceFilter(@Nullable Collection<Integer> paramCollection, boolean paramBoolean, @Nullable Collection<String> paramCollection1, @Nullable Collection<UserDataType> paramCollection2)
  {
    this(0, zzk(paramCollection), paramBoolean, zzk(paramCollection1), zzk(paramCollection2));
  }
  
  public PlaceFilter(boolean paramBoolean, @Nullable Collection<String> paramCollection)
  {
    this(null, paramBoolean, paramCollection, null);
  }
  
  @Deprecated
  public static PlaceFilter zzbqw()
  {
    return new zza(null).zzbqx();
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    do
    {
      return true;
      if (!(paramObject instanceof PlaceFilter)) {
        return false;
      }
      paramObject = (PlaceFilter)paramObject;
    } while ((this.alC.equals(((PlaceFilter)paramObject).alC)) && (this.alN == ((PlaceFilter)paramObject).alN) && (this.alD.equals(((PlaceFilter)paramObject).alD)) && (this.alB.equals(((PlaceFilter)paramObject).alB)));
    return false;
  }
  
  public Set<String> getPlaceIds()
  {
    return this.alB;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.alC, Boolean.valueOf(this.alN), this.alD, this.alB });
  }
  
  public boolean isRestrictedToPlacesOpenNow()
  {
    return this.alN;
  }
  
  public String toString()
  {
    zzz.zza localzza = zzz.zzx(this);
    if (!this.alC.isEmpty()) {
      localzza.zzg("types", this.alC);
    }
    localzza.zzg("requireOpenNow", Boolean.valueOf(this.alN));
    if (!this.alB.isEmpty()) {
      localzza.zzg("placeIds", this.alB);
    }
    if (!this.alD.isEmpty()) {
      localzza.zzg("requestedUserDataTypes", this.alD);
    }
    return localzza.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzf.zza(this, paramParcel, paramInt);
  }
  
  public Set<Integer> zzbqv()
  {
    return this.alC;
  }
  
  @Deprecated
  public static final class zza
  {
    private boolean alN = false;
    private Collection<Integer> alO = null;
    private Collection<UserDataType> alP = null;
    private String[] alQ = null;
    
    public PlaceFilter zzbqx()
    {
      return new PlaceFilter(null, false, null, null);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/location/places/PlaceFilter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */