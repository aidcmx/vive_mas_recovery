package com.google.android.gms.location.places;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;

public final class PlaceRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<PlaceRequest> CREATOR = new zzj();
  private final long ajR;
  private final long akm;
  private final PlaceFilter alW;
  private final boolean alX;
  private final boolean alY;
  private final int mPriority;
  final int mVersionCode;
  
  public PlaceRequest(int paramInt1, PlaceFilter paramPlaceFilter, long paramLong1, int paramInt2, long paramLong2, boolean paramBoolean1, boolean paramBoolean2)
  {
    this.mVersionCode = paramInt1;
    this.alW = paramPlaceFilter;
    this.akm = paramLong1;
    this.mPriority = paramInt2;
    this.ajR = paramLong2;
    this.alX = paramBoolean1;
    this.alY = paramBoolean2;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    do
    {
      return true;
      if (!(paramObject instanceof PlaceRequest)) {
        return false;
      }
      paramObject = (PlaceRequest)paramObject;
    } while ((zzz.equal(this.alW, ((PlaceRequest)paramObject).alW)) && (this.akm == ((PlaceRequest)paramObject).akm) && (this.mPriority == ((PlaceRequest)paramObject).mPriority) && (this.ajR == ((PlaceRequest)paramObject).ajR) && (this.alX == ((PlaceRequest)paramObject).alX));
    return false;
  }
  
  public long getExpirationTime()
  {
    return this.ajR;
  }
  
  public long getInterval()
  {
    return this.akm;
  }
  
  public int getPriority()
  {
    return this.mPriority;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.alW, Long.valueOf(this.akm), Integer.valueOf(this.mPriority), Long.valueOf(this.ajR), Boolean.valueOf(this.alX) });
  }
  
  @SuppressLint({"DefaultLocale"})
  public String toString()
  {
    return zzz.zzx(this).zzg("filter", this.alW).zzg("interval", Long.valueOf(this.akm)).zzg("priority", Integer.valueOf(this.mPriority)).zzg("expireAt", Long.valueOf(this.ajR)).zzg("receiveFailures", Boolean.valueOf(this.alX)).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzj.zza(this, paramParcel, paramInt);
  }
  
  @Deprecated
  public PlaceFilter zzbqr()
  {
    return this.alW;
  }
  
  public boolean zzbqy()
  {
    return this.alX;
  }
  
  public boolean zzbqz()
  {
    return this.alY;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/location/places/PlaceRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */