package com.google.android.gms.location.places.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;
import java.util.List;

@Deprecated
public final class PlaceLocalization
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<PlaceLocalization> CREATOR = new zzq();
  public final String address;
  public final String anb;
  public final String anc;
  public final List<String> and;
  public final String name;
  public final int versionCode;
  
  public PlaceLocalization(int paramInt, String paramString1, String paramString2, String paramString3, String paramString4, List<String> paramList)
  {
    this.versionCode = paramInt;
    this.name = paramString1;
    this.address = paramString2;
    this.anb = paramString3;
    this.anc = paramString4;
    this.and = paramList;
  }
  
  public static PlaceLocalization zza(String paramString1, String paramString2, String paramString3, String paramString4, List<String> paramList)
  {
    return new PlaceLocalization(0, paramString1, paramString2, paramString3, paramString4, paramList);
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    do
    {
      return true;
      if (!(paramObject instanceof PlaceLocalization)) {
        return false;
      }
      paramObject = (PlaceLocalization)paramObject;
    } while ((zzz.equal(this.name, ((PlaceLocalization)paramObject).name)) && (zzz.equal(this.address, ((PlaceLocalization)paramObject).address)) && (zzz.equal(this.anb, ((PlaceLocalization)paramObject).anb)) && (zzz.equal(this.anc, ((PlaceLocalization)paramObject).anc)) && (zzz.equal(this.and, ((PlaceLocalization)paramObject).and)));
    return false;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.name, this.address, this.anb, this.anc });
  }
  
  public String toString()
  {
    return zzz.zzx(this).zzg("name", this.name).zzg("address", this.address).zzg("internationalPhoneNumber", this.anb).zzg("regularOpenHours", this.anc).zzg("attributions", this.and).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzq.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/location/places/internal/PlaceLocalization.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */