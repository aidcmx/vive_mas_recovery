package com.google.android.gms.location.places.personalized;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;

@Deprecated
public class PlaceAlias
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<PlaceAlias> CREATOR = new zzd();
  public static final PlaceAlias anB = new PlaceAlias(0, "Home");
  public static final PlaceAlias anC = new PlaceAlias(0, "Work");
  private final String anD;
  final int mVersionCode;
  
  PlaceAlias(int paramInt, String paramString)
  {
    this.mVersionCode = paramInt;
    this.anD = paramString;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if (!(paramObject instanceof PlaceAlias)) {
      return false;
    }
    paramObject = (PlaceAlias)paramObject;
    return zzz.equal(this.anD, ((PlaceAlias)paramObject).anD);
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.anD });
  }
  
  public String toString()
  {
    return zzz.zzx(this).zzg("alias", this.anD).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzd.zza(this, paramParcel, paramInt);
  }
  
  public String zzbry()
  {
    return this.anD;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/location/places/personalized/PlaceAlias.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */