package com.google.android.gms.location.places.personalized;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.common.internal.zzz.zza;
import java.util.List;

@Deprecated
public class PlaceUserData
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<PlaceUserData> CREATOR = new zzf();
  private final String alV;
  private final List<PlaceAlias> anz;
  private final String hy;
  final int mVersionCode;
  
  PlaceUserData(int paramInt, String paramString1, String paramString2, List<PlaceAlias> paramList)
  {
    this.mVersionCode = paramInt;
    this.hy = paramString1;
    this.alV = paramString2;
    this.anz = paramList;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    do
    {
      return true;
      if (!(paramObject instanceof PlaceUserData)) {
        return false;
      }
      paramObject = (PlaceUserData)paramObject;
    } while ((this.hy.equals(((PlaceUserData)paramObject).hy)) && (this.alV.equals(((PlaceUserData)paramObject).alV)) && (this.anz.equals(((PlaceUserData)paramObject).anz)));
    return false;
  }
  
  public String getPlaceId()
  {
    return this.alV;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.hy, this.alV, this.anz });
  }
  
  public String toString()
  {
    return zzz.zzx(this).zzg("accountName", this.hy).zzg("placeId", this.alV).zzg("placeAliases", this.anz).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzf.zza(this, paramParcel, paramInt);
  }
  
  public List<PlaceAlias> zzbrw()
  {
    return this.anz;
  }
  
  public String zzbrz()
  {
    return this.hy;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/location/places/personalized/PlaceUserData.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */