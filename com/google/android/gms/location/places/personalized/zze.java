package com.google.android.gms.location.places.personalized;

import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.zzd;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.location.places.PlacesStatusCodes;

@Deprecated
public final class zze
  extends zzd<PlaceUserData>
  implements Result
{
  private final Status hv;
  
  public zze(DataHolder paramDataHolder)
  {
    this(paramDataHolder, PlacesStatusCodes.zzhl(paramDataHolder.getStatusCode()));
  }
  
  private zze(DataHolder paramDataHolder, Status paramStatus)
  {
    super(paramDataHolder, PlaceUserData.CREATOR);
    if ((paramDataHolder == null) || (paramDataHolder.getStatusCode() == paramStatus.getStatusCode())) {}
    for (boolean bool = true;; bool = false)
    {
      zzaa.zzbt(bool);
      this.hv = paramStatus;
      return;
    }
  }
  
  public Status getStatus()
  {
    return this.hv;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/location/places/personalized/zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */