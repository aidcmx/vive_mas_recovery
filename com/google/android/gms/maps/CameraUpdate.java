package com.google.android.gms.maps;

import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.dynamic.zzd;

public final class CameraUpdate
{
  private final zzd anN;
  
  CameraUpdate(zzd paramzzd)
  {
    this.anN = ((zzd)zzaa.zzy(paramzzd));
  }
  
  public zzd zzbsc()
  {
    return this.anN;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/maps/CameraUpdate.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */