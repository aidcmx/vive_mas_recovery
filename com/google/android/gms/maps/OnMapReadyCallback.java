package com.google.android.gms.maps;

public abstract interface OnMapReadyCallback
{
  public abstract void onMapReady(GoogleMap paramGoogleMap);
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/maps/OnMapReadyCallback.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */