package com.google.android.gms.maps.model;

import android.os.RemoteException;

public final class RuntimeRemoteException
  extends RuntimeException
{
  public RuntimeRemoteException(RemoteException paramRemoteException)
  {
    super(paramRemoteException);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/maps/model/RuntimeRemoteException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */