package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.MainThread;
import com.google.android.gms.common.internal.zzaa;

public final class zzu
{
  private final zza atv;
  
  public zzu(zza paramzza)
  {
    zzaa.zzy(paramzza);
    this.atv = paramzza;
  }
  
  public static boolean zzh(Context paramContext, boolean paramBoolean)
  {
    zzaa.zzy(paramContext);
    if (paramBoolean) {}
    for (String str = "com.google.android.gms.measurement.PackageMeasurementReceiver";; str = "com.google.android.gms.measurement.AppMeasurementReceiver") {
      return zzal.zza(paramContext, str, false);
    }
  }
  
  @MainThread
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    zzx localzzx = zzx.zzdq(paramContext);
    zzq localzzq = localzzx.zzbwb();
    if (paramIntent == null) {
      localzzq.zzbxa().log("Receiver called with null intent");
    }
    do
    {
      return;
      localzzx.zzbwd().zzayi();
      localObject = paramIntent.getAction();
      localzzq.zzbxe().zzj("Local receiver got", localObject);
      if ("com.google.android.gms.measurement.UPLOAD".equals(localObject))
      {
        zzaf.zzi(paramContext, false);
        paramIntent = new Intent().setClassName(paramContext, "com.google.android.gms.measurement.AppMeasurementService");
        paramIntent.setAction("com.google.android.gms.measurement.UPLOAD");
        this.atv.doStartService(paramContext, paramIntent);
        return;
      }
    } while (!"com.android.vending.INSTALL_REFERRER".equals(localObject));
    Object localObject = paramIntent.getStringExtra("referrer");
    if (localObject == null)
    {
      localzzq.zzbxe().log("Install referrer extras are null");
      return;
    }
    localObject = Uri.parse((String)localObject);
    localObject = localzzx.zzbvx().zzu((Uri)localObject);
    if (localObject == null)
    {
      localzzq.zzbxe().log("No campaign defined in install referrer broadcast");
      return;
    }
    long l = 1000L * paramIntent.getLongExtra("referrer_timestamp_seconds", 0L);
    if (l == 0L) {
      localzzq.zzbxa().log("Install referrer is missing timestamp");
    }
    localzzx.zzbwa().zzm(new zzu.1(this, localzzx, l, (Bundle)localObject, paramContext, localzzq));
  }
  
  public static abstract interface zza
  {
    public abstract void doStartService(Context paramContext, Intent paramIntent);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/measurement/internal/zzu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */