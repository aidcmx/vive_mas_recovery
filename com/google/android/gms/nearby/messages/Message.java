package com.google.android.gms.nearby.messages;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.nearby.messages.devices.NearbyDevice;
import java.util.Arrays;

public class Message
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<Message> CREATOR = new zza();
  public static final int MAX_CONTENT_SIZE_BYTES = 102400;
  public static final int MAX_TYPE_LENGTH = 32;
  public static final String MESSAGE_NAMESPACE_RESERVED = "__reserved_namespace";
  public static final String MESSAGE_TYPE_EDDYSTONE_UID = "__eddystone_uid";
  public static final String MESSAGE_TYPE_I_BEACON_ID = "__i_beacon_id";
  private static final NearbyDevice[] aym = { NearbyDevice.azd };
  private final String EY;
  @Deprecated
  final NearbyDevice[] ayn;
  private final long ayo;
  private final byte[] content;
  private final String type;
  final int versionCode;
  
  Message(int paramInt, @Nullable byte[] paramArrayOfByte, @Nullable String paramString1, String paramString2, @Nullable NearbyDevice[] paramArrayOfNearbyDevice, long paramLong)
  {
    this.versionCode = paramInt;
    this.type = ((String)zzaa.zzy(paramString2));
    String str = paramString1;
    if (paramString1 == null) {
      str = "";
    }
    this.EY = str;
    this.ayo = 0L;
    if (zzbc(this.EY, this.type)) {
      if (paramArrayOfByte == null)
      {
        bool = true;
        zzaa.zzb(bool, "Content must be null for a device presence message.");
        this.content = paramArrayOfByte;
        if (paramArrayOfNearbyDevice != null)
        {
          paramArrayOfByte = paramArrayOfNearbyDevice;
          if (paramArrayOfNearbyDevice.length != 0) {}
        }
        else
        {
          paramArrayOfByte = aym;
        }
        this.ayn = paramArrayOfByte;
        if (paramString2.length() > 32) {
          break label200;
        }
      }
    }
    label200:
    for (boolean bool = true;; bool = false)
    {
      zzaa.zzb(bool, "Type length(%d) must not exceed MAX_TYPE_LENGTH(%d)", new Object[] { Integer.valueOf(paramString2.length()), Integer.valueOf(32) });
      return;
      bool = false;
      break;
      zzaa.zzy(paramArrayOfByte);
      if (paramArrayOfByte.length <= 102400) {}
      for (bool = true;; bool = false)
      {
        zzaa.zzb(bool, "Content length(%d) must not exceed MAX_CONTENT_SIZE_BYTES(%d)", new Object[] { Integer.valueOf(paramArrayOfByte.length), Integer.valueOf(102400) });
        break;
      }
    }
  }
  
  public Message(byte[] paramArrayOfByte)
  {
    this(paramArrayOfByte, "", "");
  }
  
  public Message(byte[] paramArrayOfByte, String paramString)
  {
    this(paramArrayOfByte, "", paramString);
  }
  
  public Message(byte[] paramArrayOfByte, String paramString1, String paramString2)
  {
    this(paramArrayOfByte, paramString1, paramString2, aym);
  }
  
  public Message(byte[] paramArrayOfByte, String paramString1, String paramString2, NearbyDevice[] paramArrayOfNearbyDevice)
  {
    this(paramArrayOfByte, paramString1, paramString2, paramArrayOfNearbyDevice, 0L);
  }
  
  public Message(byte[] paramArrayOfByte, String paramString1, String paramString2, NearbyDevice[] paramArrayOfNearbyDevice, long paramLong)
  {
    this(2, paramArrayOfByte, paramString1, paramString2, paramArrayOfNearbyDevice, paramLong);
  }
  
  @Deprecated
  public static boolean zzbc(String paramString1, String paramString2)
  {
    return (paramString1.equals("__reserved_namespace")) && (paramString2.equals("__device_presence"));
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    do
    {
      return true;
      if (!(paramObject instanceof Message)) {
        return false;
      }
      paramObject = (Message)paramObject;
    } while ((TextUtils.equals(this.EY, ((Message)paramObject).EY)) && (TextUtils.equals(this.type, ((Message)paramObject).type)) && (Arrays.equals(this.content, ((Message)paramObject).content)) && (0L == 0L));
    return false;
  }
  
  public byte[] getContent()
  {
    return this.content;
  }
  
  public String getNamespace()
  {
    return this.EY;
  }
  
  public String getType()
  {
    return this.type;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.EY, this.type, Integer.valueOf(Arrays.hashCode(this.content)), Long.valueOf(0L) });
  }
  
  public String toString()
  {
    String str1 = this.EY;
    String str2 = this.type;
    if (this.content == null) {}
    for (int i = 0;; i = this.content.length) {
      return String.valueOf(str1).length() + 59 + String.valueOf(str2).length() + "Message{namespace='" + str1 + "', type='" + str2 + "', content=[" + i + " bytes]}";
    }
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zza.zza(this, paramParcel, paramInt);
  }
  
  public boolean zzcav()
  {
    return "__reserved_namespace".equals(getNamespace());
  }
  
  public long zzcaw()
  {
    return 0L;
  }
  
  public boolean zznn(String paramString)
  {
    return (zzcav()) && (paramString.equals(getType()));
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/nearby/messages/Message.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */