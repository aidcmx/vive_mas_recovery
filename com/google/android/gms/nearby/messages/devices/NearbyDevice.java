package com.google.android.gms.nearby.messages.devices;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.Nullable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zzz;

public class NearbyDevice
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<NearbyDevice> CREATOR = new zza();
  private static final NearbyDeviceId[] aza = new NearbyDeviceId[0];
  private static final String[] azb = new String[0];
  private static final String azc = null;
  public static final NearbyDevice azd = new NearbyDevice("", aza, azb, null);
  @Deprecated
  final NearbyDeviceId aze;
  private final String azf;
  @Deprecated
  final NearbyDeviceId[] azg;
  @Deprecated
  final String[] azh;
  @Nullable
  private final String azi;
  final int mVersionCode;
  @Deprecated
  @Nullable
  final String zzae;
  
  NearbyDevice(int paramInt, @Nullable NearbyDeviceId paramNearbyDeviceId, @Nullable String paramString1, @Nullable String paramString2, @Nullable NearbyDeviceId[] paramArrayOfNearbyDeviceId, @Nullable String[] paramArrayOfString, @Nullable String paramString3)
  {
    this.mVersionCode = ((Integer)zzaa.zzy(Integer.valueOf(paramInt))).intValue();
    paramNearbyDeviceId = paramString2;
    if (paramString2 == null) {
      paramNearbyDeviceId = "";
    }
    this.azf = paramNearbyDeviceId;
    paramNearbyDeviceId = paramArrayOfNearbyDeviceId;
    if (paramArrayOfNearbyDeviceId == null) {
      paramNearbyDeviceId = aza;
    }
    this.azg = paramNearbyDeviceId;
    paramNearbyDeviceId = paramArrayOfString;
    if (paramArrayOfString == null) {
      paramNearbyDeviceId = azb;
    }
    this.azh = paramNearbyDeviceId;
    if (this.azg.length == 0)
    {
      paramNearbyDeviceId = NearbyDeviceId.azm;
      this.aze = paramNearbyDeviceId;
      if (this.azh.length != 0) {
        break label120;
      }
    }
    label120:
    for (paramNearbyDeviceId = null;; paramNearbyDeviceId = this.azh[0])
    {
      this.zzae = paramNearbyDeviceId;
      this.azi = paramString3;
      return;
      paramNearbyDeviceId = this.azg[0];
      break;
    }
  }
  
  @Deprecated
  public NearbyDevice(String paramString1, NearbyDeviceId[] paramArrayOfNearbyDeviceId, String[] paramArrayOfString, String paramString2)
  {
    this(1, null, null, paramString1, paramArrayOfNearbyDeviceId, paramArrayOfString, paramString2);
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    do
    {
      return true;
      if (!(paramObject instanceof NearbyDevice)) {
        return false;
      }
      paramObject = (NearbyDevice)paramObject;
    } while ((zzz.equal(this.azf, ((NearbyDevice)paramObject).azf)) && (zzz.equal(this.azi, ((NearbyDevice)paramObject).azi)));
    return false;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { this.azf, this.azi });
  }
  
  public String toString()
  {
    String str1 = this.azf;
    String str2 = this.azi;
    return String.valueOf(str1).length() + 40 + String.valueOf(str2).length() + "NearbyDevice{handle=" + str1 + ", bluetoothAddress=" + str2 + "}";
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zza.zza(this, paramParcel, paramInt);
  }
  
  public String zzcbe()
  {
    return this.azf;
  }
  
  @Nullable
  public String zzcbf()
  {
    return this.azi;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/nearby/messages/devices/NearbyDevice.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */