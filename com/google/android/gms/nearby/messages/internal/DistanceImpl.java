package com.google.android.gms.nearby.messages.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzz;
import com.google.android.gms.nearby.messages.Distance;
import java.util.Locale;

public class DistanceImpl
  extends AbstractSafeParcelable
  implements Distance
{
  public static final Parcelable.Creator<DistanceImpl> CREATOR = new zzd();
  public final int accuracy;
  public final double azw;
  final int mVersionCode;
  
  public DistanceImpl(int paramInt, double paramDouble)
  {
    this(1, paramInt, paramDouble);
  }
  
  DistanceImpl(int paramInt1, int paramInt2, double paramDouble)
  {
    this.mVersionCode = paramInt1;
    this.accuracy = paramInt2;
    this.azw = paramDouble;
  }
  
  private static String zzyj(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return "UNKNOWN";
    }
    return "LOW";
  }
  
  public int compareTo(@NonNull Distance paramDistance)
  {
    if ((Double.isNaN(getMeters())) && (Double.isNaN(paramDistance.getMeters()))) {
      return 0;
    }
    return Double.compare(getMeters(), paramDistance.getMeters());
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    do
    {
      return true;
      if (!(paramObject instanceof DistanceImpl)) {
        return false;
      }
      paramObject = (DistanceImpl)paramObject;
    } while ((getAccuracy() == ((DistanceImpl)paramObject).getAccuracy()) && (compareTo((Distance)paramObject) == 0));
    return false;
  }
  
  public int getAccuracy()
  {
    return this.accuracy;
  }
  
  public double getMeters()
  {
    return this.azw;
  }
  
  public int hashCode()
  {
    return zzz.hashCode(new Object[] { Integer.valueOf(getAccuracy()), Double.valueOf(getMeters()) });
  }
  
  public String toString()
  {
    return String.format(Locale.US, "(%.1fm, %s)", new Object[] { Double.valueOf(this.azw), zzyj(this.accuracy) });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzd.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/nearby/messages/internal/DistanceImpl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */