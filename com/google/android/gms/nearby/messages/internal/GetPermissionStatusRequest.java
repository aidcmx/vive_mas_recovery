package com.google.android.gms.nearby.messages.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Deprecated
public class GetPermissionStatusRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<GetPermissionStatusRequest> CREATOR = new zzf();
  @Deprecated
  public final String ayv;
  public final zzj azx;
  @Deprecated
  public final ClientAppContext azy;
  final int mVersionCode;
  
  GetPermissionStatusRequest(int paramInt, IBinder paramIBinder, String paramString, ClientAppContext paramClientAppContext)
  {
    this.mVersionCode = paramInt;
    this.azx = zzj.zza.zzkb(paramIBinder);
    this.ayv = paramString;
    this.azy = ClientAppContext.zza(paramClientAppContext, null, paramString, false);
  }
  
  GetPermissionStatusRequest(IBinder paramIBinder)
  {
    this(1, paramIBinder, null, null);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzf.zza(this, paramParcel, paramInt);
  }
  
  IBinder zzcbg()
  {
    return this.azx.asBinder();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/nearby/messages/internal/GetPermissionStatusRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */