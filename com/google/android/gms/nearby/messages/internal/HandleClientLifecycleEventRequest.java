package com.google.android.gms.nearby.messages.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public final class HandleClientLifecycleEventRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<HandleClientLifecycleEventRequest> CREATOR = new zzg();
  @Deprecated
  public final ClientAppContext azy;
  public final int azz;
  public final int versionCode;
  
  public HandleClientLifecycleEventRequest(int paramInt)
  {
    this(1, null, paramInt);
  }
  
  HandleClientLifecycleEventRequest(int paramInt1, ClientAppContext paramClientAppContext, int paramInt2)
  {
    this.versionCode = paramInt1;
    this.azy = paramClientAppContext;
    this.azz = paramInt2;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzg.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/nearby/messages/internal/HandleClientLifecycleEventRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */