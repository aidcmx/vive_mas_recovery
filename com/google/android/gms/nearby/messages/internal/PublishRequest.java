package com.google.android.gms.nearby.messages.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.nearby.messages.Strategy;

public final class PublishRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<PublishRequest> CREATOR = new zzv();
  public final Strategy aAa;
  @Deprecated
  public final boolean aAb;
  public final zzl aAc;
  @Deprecated
  public final String ayv;
  @Deprecated
  public final boolean ayw;
  public final MessageWrapper azZ;
  @Deprecated
  public final String azt;
  public final zzj azx;
  @Deprecated
  public final ClientAppContext azy;
  final int mVersionCode;
  
  PublishRequest(int paramInt, MessageWrapper paramMessageWrapper, Strategy paramStrategy, IBinder paramIBinder1, String paramString1, String paramString2, boolean paramBoolean1, IBinder paramIBinder2, boolean paramBoolean2, ClientAppContext paramClientAppContext)
  {
    this.mVersionCode = paramInt;
    this.azZ = paramMessageWrapper;
    this.aAa = paramStrategy;
    this.azx = zzj.zza.zzkb(paramIBinder1);
    this.ayv = paramString1;
    this.azt = paramString2;
    this.aAb = paramBoolean1;
    if (paramIBinder2 == null) {}
    for (paramMessageWrapper = null;; paramMessageWrapper = zzl.zza.zzkd(paramIBinder2))
    {
      this.aAc = paramMessageWrapper;
      this.ayw = paramBoolean2;
      this.azy = ClientAppContext.zza(paramClientAppContext, paramString2, paramString1, paramBoolean2);
      return;
    }
  }
  
  PublishRequest(MessageWrapper paramMessageWrapper, Strategy paramStrategy, IBinder paramIBinder1, IBinder paramIBinder2)
  {
    this(2, paramMessageWrapper, paramStrategy, paramIBinder1, null, null, false, paramIBinder2, false, null);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzv.zza(this, paramParcel, paramInt);
  }
  
  IBinder zzcbg()
  {
    return this.azx.asBinder();
  }
  
  IBinder zzcbk()
  {
    if (this.aAc == null) {
      return null;
    }
    return this.aAc.asBinder();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/nearby/messages/internal/PublishRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */