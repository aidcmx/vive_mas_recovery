package com.google.android.gms.nearby.messages.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public final class RegisterStatusCallbackRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<RegisterStatusCallbackRequest> CREATOR = new zzw();
  public final zzm aAd;
  public boolean aAe;
  @Deprecated
  public String ayv;
  public final zzj azx;
  @Deprecated
  public final ClientAppContext azy;
  final int versionCode;
  
  RegisterStatusCallbackRequest(int paramInt, IBinder paramIBinder1, IBinder paramIBinder2, boolean paramBoolean, String paramString, ClientAppContext paramClientAppContext)
  {
    this.versionCode = paramInt;
    this.azx = zzj.zza.zzkb(paramIBinder1);
    this.aAd = zzm.zza.zzke(paramIBinder2);
    this.aAe = paramBoolean;
    this.ayv = paramString;
    this.azy = ClientAppContext.zza(paramClientAppContext, null, paramString, false);
  }
  
  RegisterStatusCallbackRequest(IBinder paramIBinder1, IBinder paramIBinder2)
  {
    this(1, paramIBinder1, paramIBinder2, false, null, null);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzw.zza(this, paramParcel, paramInt);
  }
  
  IBinder zzcbg()
  {
    return this.azx.asBinder();
  }
  
  IBinder zzcbl()
  {
    return this.aAd.asBinder();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/nearby/messages/internal/RegisterStatusCallbackRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */