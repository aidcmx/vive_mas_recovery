package com.google.android.gms.nearby.messages.internal;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.nearby.messages.MessageFilter;
import com.google.android.gms.nearby.messages.Strategy;

public final class SubscribeRequest
  extends AbstractSafeParcelable
  implements ReflectedParcelable
{
  public static final Parcelable.Creator<SubscribeRequest> CREATOR = new zzx();
  public final PendingIntent HQ;
  public final Strategy aAa;
  @Deprecated
  public final boolean aAb;
  public final zzi aAf;
  public final MessageFilter aAg;
  public final int aAh;
  public final byte[] aAi;
  public final zzn aAj;
  public final boolean ayR;
  @Deprecated
  public final String ayv;
  @Deprecated
  public final boolean ayw;
  @Deprecated
  public final String azt;
  public final zzj azx;
  @Deprecated
  public final ClientAppContext azy;
  final int mVersionCode;
  
  public SubscribeRequest(int paramInt1, IBinder paramIBinder1, Strategy paramStrategy, IBinder paramIBinder2, MessageFilter paramMessageFilter, PendingIntent paramPendingIntent, int paramInt2, String paramString1, String paramString2, byte[] paramArrayOfByte, boolean paramBoolean1, IBinder paramIBinder3, boolean paramBoolean2, ClientAppContext paramClientAppContext, boolean paramBoolean3)
  {
    this.mVersionCode = paramInt1;
    this.aAf = zzi.zza.zzka(paramIBinder1);
    this.aAa = paramStrategy;
    this.azx = zzj.zza.zzkb(paramIBinder2);
    this.aAg = paramMessageFilter;
    this.HQ = paramPendingIntent;
    this.aAh = paramInt2;
    this.ayv = paramString1;
    this.azt = paramString2;
    this.aAi = paramArrayOfByte;
    this.aAb = paramBoolean1;
    if (paramIBinder3 == null) {}
    for (paramIBinder1 = null;; paramIBinder1 = zzn.zza.zzkf(paramIBinder3))
    {
      this.aAj = paramIBinder1;
      this.ayw = paramBoolean2;
      this.azy = ClientAppContext.zza(paramClientAppContext, paramString2, paramString1, paramBoolean2);
      this.ayR = paramBoolean3;
      return;
    }
  }
  
  public SubscribeRequest(IBinder paramIBinder1, Strategy paramStrategy, IBinder paramIBinder2, MessageFilter paramMessageFilter, PendingIntent paramPendingIntent, int paramInt, byte[] paramArrayOfByte, IBinder paramIBinder3, boolean paramBoolean)
  {
    this(3, paramIBinder1, paramStrategy, paramIBinder2, paramMessageFilter, paramPendingIntent, paramInt, null, null, paramArrayOfByte, false, paramIBinder3, false, null, paramBoolean);
  }
  
  public String toString()
  {
    String str1 = String.valueOf(this.aAf);
    String str2 = String.valueOf(this.aAa);
    String str3 = String.valueOf(this.azx);
    String str4 = String.valueOf(this.aAg);
    String str5 = String.valueOf(this.HQ);
    int i = this.aAh;
    if (this.aAi == null) {}
    int j;
    for (Object localObject = null;; localObject = 19 + "<" + j + " bytes>")
    {
      String str6 = String.valueOf(this.aAj);
      boolean bool1 = this.ayw;
      String str7 = String.valueOf(this.azy);
      boolean bool2 = this.ayR;
      String str8 = this.ayv;
      String str9 = this.azt;
      boolean bool3 = this.aAb;
      return String.valueOf(str1).length() + 295 + String.valueOf(str2).length() + String.valueOf(str3).length() + String.valueOf(str4).length() + String.valueOf(str5).length() + String.valueOf(localObject).length() + String.valueOf(str6).length() + String.valueOf(str7).length() + String.valueOf(str8).length() + String.valueOf(str9).length() + "SubscribeRequest{messageListener=" + str1 + ", strategy=" + str2 + ", callback=" + str3 + ", filter=" + str4 + ", pendingIntent=" + str5 + ", messageListenerKey=" + i + ", hint=" + (String)localObject + ", subscribeCallback=" + str6 + ", useRealClientApiKey=" + bool1 + ", clientAppContext=" + str7 + ", isDiscardPendingIntent=" + bool2 + ", zeroPartyPackageName=" + str8 + ", realClientPackageName=" + str9 + ", isIgnoreNearbyPermission=" + bool3 + "}";
      j = this.aAi.length;
    }
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzx.zza(this, paramParcel, paramInt);
  }
  
  IBinder zzcbg()
  {
    if (this.azx == null) {
      return null;
    }
    return this.azx.asBinder();
  }
  
  IBinder zzcbm()
  {
    if (this.aAf == null) {
      return null;
    }
    return this.aAf.asBinder();
  }
  
  IBinder zzcbn()
  {
    if (this.aAj == null) {
      return null;
    }
    return this.aAj.asBinder();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/nearby/messages/internal/SubscribeRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */