package com.google.android.gms.nearby.messages.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public final class UnpublishRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<UnpublishRequest> CREATOR = new zzy();
  @Deprecated
  public final String ayv;
  @Deprecated
  public final boolean ayw;
  public final MessageWrapper azZ;
  @Deprecated
  public final String azt;
  public final zzj azx;
  @Deprecated
  public final ClientAppContext azy;
  final int mVersionCode;
  
  UnpublishRequest(int paramInt, MessageWrapper paramMessageWrapper, IBinder paramIBinder, String paramString1, String paramString2, boolean paramBoolean, ClientAppContext paramClientAppContext)
  {
    this.mVersionCode = paramInt;
    this.azZ = paramMessageWrapper;
    this.azx = zzj.zza.zzkb(paramIBinder);
    this.ayv = paramString1;
    this.azt = paramString2;
    this.ayw = paramBoolean;
    this.azy = ClientAppContext.zza(paramClientAppContext, paramString2, paramString1, paramBoolean);
  }
  
  UnpublishRequest(MessageWrapper paramMessageWrapper, IBinder paramIBinder, ClientAppContext paramClientAppContext)
  {
    this(1, paramMessageWrapper, paramIBinder, null, null, false, paramClientAppContext);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzy.zza(this, paramParcel, paramInt);
  }
  
  IBinder zzcbg()
  {
    return this.azx.asBinder();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/nearby/messages/internal/UnpublishRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */