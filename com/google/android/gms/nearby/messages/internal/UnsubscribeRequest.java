package com.google.android.gms.nearby.messages.internal;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.VisibleForTesting;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public final class UnsubscribeRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<UnsubscribeRequest> CREATOR = new zzz();
  public final PendingIntent HQ;
  public final zzi aAf;
  public final int aAh;
  @Deprecated
  public final String ayv;
  @Deprecated
  public final boolean ayw;
  @Deprecated
  public final String azt;
  public final zzj azx;
  @Deprecated
  public final ClientAppContext azy;
  final int mVersionCode;
  
  UnsubscribeRequest(int paramInt1, IBinder paramIBinder1, IBinder paramIBinder2, PendingIntent paramPendingIntent, int paramInt2, String paramString1, String paramString2, boolean paramBoolean, ClientAppContext paramClientAppContext)
  {
    this.mVersionCode = paramInt1;
    this.aAf = zzi.zza.zzka(paramIBinder1);
    this.azx = zzj.zza.zzkb(paramIBinder2);
    this.HQ = paramPendingIntent;
    this.aAh = paramInt2;
    this.ayv = paramString1;
    this.azt = paramString2;
    this.ayw = paramBoolean;
    this.azy = ClientAppContext.zza(paramClientAppContext, paramString2, paramString1, paramBoolean);
  }
  
  @VisibleForTesting
  public UnsubscribeRequest(IBinder paramIBinder1, IBinder paramIBinder2, PendingIntent paramPendingIntent, int paramInt)
  {
    this(1, paramIBinder1, paramIBinder2, paramPendingIntent, paramInt, null, null, false, null);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzz.zza(this, paramParcel, paramInt);
  }
  
  IBinder zzcbg()
  {
    return this.azx.asBinder();
  }
  
  IBinder zzcbm()
  {
    if (this.aAf == null) {
      return null;
    }
    return this.aAf.asBinder();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/nearby/messages/internal/UnsubscribeRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */