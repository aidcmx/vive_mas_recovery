package com.google.android.gms.nearby.messages.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzd
  implements Parcelable.Creator<DistanceImpl>
{
  static void zza(DistanceImpl paramDistanceImpl, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.zzcs(paramParcel);
    zzb.zzc(paramParcel, 1, paramDistanceImpl.mVersionCode);
    zzb.zzc(paramParcel, 2, paramDistanceImpl.accuracy);
    zzb.zza(paramParcel, 3, paramDistanceImpl.azw);
    zzb.zzaj(paramParcel, paramInt);
  }
  
  public DistanceImpl zzqu(Parcel paramParcel)
  {
    int j = 0;
    int k = zza.zzcr(paramParcel);
    double d = 0.0D;
    int i = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.zzcq(paramParcel);
      switch (zza.zzgu(m))
      {
      default: 
        zza.zzb(paramParcel, m);
        break;
      case 1: 
        i = zza.zzg(paramParcel, m);
        break;
      case 2: 
        j = zza.zzg(paramParcel, m);
        break;
      case 3: 
        d = zza.zzn(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza(37 + "Overread allowed size end=" + k, paramParcel);
    }
    return new DistanceImpl(i, j, d);
  }
  
  public DistanceImpl[] zzyk(int paramInt)
  {
    return new DistanceImpl[paramInt];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/nearby/messages/internal/zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */