package com.google.android.gms.plus;

import android.support.annotation.RequiresPermission;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;

@Deprecated
public abstract interface Account
{
  @Deprecated
  public abstract void clearDefaultAccount(GoogleApiClient paramGoogleApiClient);
  
  @Deprecated
  @RequiresPermission("android.permission.GET_ACCOUNTS")
  public abstract String getAccountName(GoogleApiClient paramGoogleApiClient);
  
  @Deprecated
  public abstract PendingResult<Status> revokeAccessAndDisconnect(GoogleApiClient paramGoogleApiClient);
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/plus/Account.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */