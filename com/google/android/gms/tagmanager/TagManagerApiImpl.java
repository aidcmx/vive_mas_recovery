package com.google.android.gms.tagmanager;

import android.content.Context;
import android.content.Intent;
import android.os.RemoteException;
import com.google.android.gms.common.util.DynamiteApi;
import com.google.android.gms.dynamic.zzd;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.internal.zzyl;
import com.google.android.gms.internal.zzyp;
import com.google.android.gms.internal.zzza;

@DynamiteApi
public class TagManagerApiImpl
  extends zzbc.zza
{
  private zzza aHM;
  
  public void initialize(zzd paramzzd, zzbb paramzzbb, zzay paramzzay)
    throws RemoteException
  {
    this.aHM = zzza.zza((Context)zze.zzae(paramzzd), paramzzbb, paramzzay);
    this.aHM.initialize();
  }
  
  @Deprecated
  public void preview(Intent paramIntent, zzd paramzzd)
  {
    zzyl.zzdi("Deprecated. Please use previewIntent instead.");
  }
  
  public void previewIntent(Intent paramIntent, zzd paramzzd1, zzd paramzzd2, zzbb paramzzbb, zzay paramzzay)
  {
    paramzzd1 = (Context)zze.zzae(paramzzd1);
    paramzzd2 = (Context)zze.zzae(paramzzd2);
    this.aHM = zzza.zza(paramzzd1, paramzzbb, paramzzay);
    new zzyp(paramIntent, paramzzd1, paramzzd2, this.aHM).zzcia();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/tagmanager/TagManagerApiImpl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */