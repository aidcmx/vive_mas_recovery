package com.google.android.gms.wallet;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Deprecated
public final class Address
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<Address> CREATOR = new zza();
  String aPd;
  String aPe;
  String ahQ;
  String ahR;
  String ahS;
  String ahX;
  boolean ahZ;
  String aia;
  private final int mVersionCode;
  String name;
  String phoneNumber;
  String zzcpw;
  
  Address()
  {
    this.mVersionCode = 1;
  }
  
  Address(int paramInt, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, String paramString9, boolean paramBoolean, String paramString10)
  {
    this.mVersionCode = paramInt;
    this.name = paramString1;
    this.ahQ = paramString2;
    this.ahR = paramString3;
    this.ahS = paramString4;
    this.zzcpw = paramString5;
    this.aPd = paramString6;
    this.aPe = paramString7;
    this.ahX = paramString8;
    this.phoneNumber = paramString9;
    this.ahZ = paramBoolean;
    this.aia = paramString10;
  }
  
  public int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zza.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/wallet/Address.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */