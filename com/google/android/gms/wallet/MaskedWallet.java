package com.google.android.gms.wallet;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.identity.intents.model.UserAddress;

public final class MaskedWallet
  extends AbstractSafeParcelable
  implements ReflectedParcelable
{
  public static final Parcelable.Creator<MaskedWallet> CREATOR = new zzl();
  String aPm;
  String aPn;
  String aPp;
  Address aPq;
  Address aPr;
  String[] aPs;
  UserAddress aPt;
  UserAddress aPu;
  InstrumentInfo[] aPv;
  LoyaltyWalletObject[] aQh;
  OfferWalletObject[] aQi;
  private final int mVersionCode;
  
  private MaskedWallet()
  {
    this.mVersionCode = 2;
  }
  
  MaskedWallet(int paramInt, String paramString1, String paramString2, String[] paramArrayOfString, String paramString3, Address paramAddress1, Address paramAddress2, LoyaltyWalletObject[] paramArrayOfLoyaltyWalletObject, OfferWalletObject[] paramArrayOfOfferWalletObject, UserAddress paramUserAddress1, UserAddress paramUserAddress2, InstrumentInfo[] paramArrayOfInstrumentInfo)
  {
    this.mVersionCode = paramInt;
    this.aPm = paramString1;
    this.aPn = paramString2;
    this.aPs = paramArrayOfString;
    this.aPp = paramString3;
    this.aPq = paramAddress1;
    this.aPr = paramAddress2;
    this.aQh = paramArrayOfLoyaltyWalletObject;
    this.aQi = paramArrayOfOfferWalletObject;
    this.aPt = paramUserAddress1;
    this.aPu = paramUserAddress2;
    this.aPv = paramArrayOfInstrumentInfo;
  }
  
  public static Builder newBuilderFrom(MaskedWallet paramMaskedWallet)
  {
    zzaa.zzy(paramMaskedWallet);
    return zzclx().setGoogleTransactionId(paramMaskedWallet.getGoogleTransactionId()).setMerchantTransactionId(paramMaskedWallet.getMerchantTransactionId()).setPaymentDescriptions(paramMaskedWallet.getPaymentDescriptions()).setInstrumentInfos(paramMaskedWallet.getInstrumentInfos()).setEmail(paramMaskedWallet.getEmail()).zza(paramMaskedWallet.zzcly()).zza(paramMaskedWallet.zzclz()).setBuyerBillingAddress(paramMaskedWallet.getBuyerBillingAddress()).setBuyerShippingAddress(paramMaskedWallet.getBuyerShippingAddress());
  }
  
  public static Builder zzclx()
  {
    MaskedWallet localMaskedWallet = new MaskedWallet();
    localMaskedWallet.getClass();
    return new Builder(null);
  }
  
  public UserAddress getBuyerBillingAddress()
  {
    return this.aPt;
  }
  
  public UserAddress getBuyerShippingAddress()
  {
    return this.aPu;
  }
  
  public String getEmail()
  {
    return this.aPp;
  }
  
  public String getGoogleTransactionId()
  {
    return this.aPm;
  }
  
  public InstrumentInfo[] getInstrumentInfos()
  {
    return this.aPv;
  }
  
  public String getMerchantTransactionId()
  {
    return this.aPn;
  }
  
  public String[] getPaymentDescriptions()
  {
    return this.aPs;
  }
  
  public int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzl.zza(this, paramParcel, paramInt);
  }
  
  @Deprecated
  public LoyaltyWalletObject[] zzcly()
  {
    return this.aQh;
  }
  
  @Deprecated
  public OfferWalletObject[] zzclz()
  {
    return this.aQi;
  }
  
  public final class Builder
  {
    private Builder() {}
    
    public MaskedWallet build()
    {
      return MaskedWallet.this;
    }
    
    public Builder setBuyerBillingAddress(UserAddress paramUserAddress)
    {
      MaskedWallet.this.aPt = paramUserAddress;
      return this;
    }
    
    public Builder setBuyerShippingAddress(UserAddress paramUserAddress)
    {
      MaskedWallet.this.aPu = paramUserAddress;
      return this;
    }
    
    public Builder setEmail(String paramString)
    {
      MaskedWallet.this.aPp = paramString;
      return this;
    }
    
    public Builder setGoogleTransactionId(String paramString)
    {
      MaskedWallet.this.aPm = paramString;
      return this;
    }
    
    public Builder setInstrumentInfos(InstrumentInfo[] paramArrayOfInstrumentInfo)
    {
      MaskedWallet.this.aPv = paramArrayOfInstrumentInfo;
      return this;
    }
    
    public Builder setMerchantTransactionId(String paramString)
    {
      MaskedWallet.this.aPn = paramString;
      return this;
    }
    
    public Builder setPaymentDescriptions(String[] paramArrayOfString)
    {
      MaskedWallet.this.aPs = paramArrayOfString;
      return this;
    }
    
    @Deprecated
    public Builder zza(LoyaltyWalletObject[] paramArrayOfLoyaltyWalletObject)
    {
      MaskedWallet.this.aQh = paramArrayOfLoyaltyWalletObject;
      return this;
    }
    
    @Deprecated
    public Builder zza(OfferWalletObject[] paramArrayOfOfferWalletObject)
    {
      MaskedWallet.this.aQi = paramArrayOfOfferWalletObject;
      return this;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/wallet/MaskedWallet.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */