package com.google.android.gms.wallet;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.util.ArrayList;
import java.util.Collection;

public final class MaskedWalletRequest
  extends AbstractSafeParcelable
  implements ReflectedParcelable
{
  public static final Parcelable.Creator<MaskedWalletRequest> CREATOR = new zzm();
  String aPg;
  String aPn;
  Cart aPx;
  boolean aQk;
  boolean aQl;
  boolean aQm;
  String aQn;
  String aQo;
  boolean aQp;
  boolean aQq;
  CountrySpecification[] aQr;
  boolean aQs;
  boolean aQt;
  ArrayList<com.google.android.gms.identity.intents.model.CountrySpecification> aQu;
  PaymentMethodTokenizationParameters aQv;
  ArrayList<Integer> aQw;
  private final int mVersionCode;
  String zzcpw;
  
  MaskedWalletRequest()
  {
    this.mVersionCode = 3;
    this.aQs = true;
    this.aQt = true;
  }
  
  MaskedWalletRequest(int paramInt, String paramString1, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, String paramString2, String paramString3, String paramString4, Cart paramCart, boolean paramBoolean4, boolean paramBoolean5, CountrySpecification[] paramArrayOfCountrySpecification, boolean paramBoolean6, boolean paramBoolean7, ArrayList<com.google.android.gms.identity.intents.model.CountrySpecification> paramArrayList, PaymentMethodTokenizationParameters paramPaymentMethodTokenizationParameters, ArrayList<Integer> paramArrayList1, String paramString5)
  {
    this.mVersionCode = paramInt;
    this.aPn = paramString1;
    this.aQk = paramBoolean1;
    this.aQl = paramBoolean2;
    this.aQm = paramBoolean3;
    this.aQn = paramString2;
    this.aPg = paramString3;
    this.aQo = paramString4;
    this.aPx = paramCart;
    this.aQp = paramBoolean4;
    this.aQq = paramBoolean5;
    this.aQr = paramArrayOfCountrySpecification;
    this.aQs = paramBoolean6;
    this.aQt = paramBoolean7;
    this.aQu = paramArrayList;
    this.aQv = paramPaymentMethodTokenizationParameters;
    this.aQw = paramArrayList1;
    this.zzcpw = paramString5;
  }
  
  public static Builder newBuilder()
  {
    MaskedWalletRequest localMaskedWalletRequest = new MaskedWalletRequest();
    localMaskedWalletRequest.getClass();
    return new Builder(null);
  }
  
  public boolean allowDebitCard()
  {
    return this.aQt;
  }
  
  public boolean allowPrepaidCard()
  {
    return this.aQs;
  }
  
  public ArrayList<Integer> getAllowedCardNetworks()
  {
    return this.aQw;
  }
  
  public ArrayList<com.google.android.gms.identity.intents.model.CountrySpecification> getAllowedCountrySpecificationsForShipping()
  {
    return this.aQu;
  }
  
  public CountrySpecification[] getAllowedShippingCountrySpecifications()
  {
    return this.aQr;
  }
  
  public Cart getCart()
  {
    return this.aPx;
  }
  
  public String getCountryCode()
  {
    return this.zzcpw;
  }
  
  public String getCurrencyCode()
  {
    return this.aPg;
  }
  
  public String getEstimatedTotalPrice()
  {
    return this.aQn;
  }
  
  public String getMerchantName()
  {
    return this.aQo;
  }
  
  public String getMerchantTransactionId()
  {
    return this.aPn;
  }
  
  public PaymentMethodTokenizationParameters getPaymentMethodTokenizationParameters()
  {
    return this.aQv;
  }
  
  public int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  @Deprecated
  public boolean isBillingAgreement()
  {
    return this.aQq;
  }
  
  public boolean isPhoneNumberRequired()
  {
    return this.aQk;
  }
  
  public boolean isShippingAddressRequired()
  {
    return this.aQl;
  }
  
  @Deprecated
  public boolean useMinimalBillingAddress()
  {
    return this.aQm;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzm.zza(this, paramParcel, paramInt);
  }
  
  public final class Builder
  {
    private Builder() {}
    
    public Builder addAllowedCardNetwork(int paramInt)
    {
      if (MaskedWalletRequest.this.aQw == null) {
        MaskedWalletRequest.this.aQw = new ArrayList();
      }
      MaskedWalletRequest.this.aQw.add(Integer.valueOf(paramInt));
      return this;
    }
    
    public Builder addAllowedCardNetworks(Collection<Integer> paramCollection)
    {
      if (paramCollection != null)
      {
        if (MaskedWalletRequest.this.aQw == null) {
          MaskedWalletRequest.this.aQw = new ArrayList();
        }
        MaskedWalletRequest.this.aQw.addAll(paramCollection);
      }
      return this;
    }
    
    public Builder addAllowedCountrySpecificationForShipping(com.google.android.gms.identity.intents.model.CountrySpecification paramCountrySpecification)
    {
      if (MaskedWalletRequest.this.aQu == null) {
        MaskedWalletRequest.this.aQu = new ArrayList();
      }
      MaskedWalletRequest.this.aQu.add(paramCountrySpecification);
      return this;
    }
    
    public Builder addAllowedCountrySpecificationsForShipping(Collection<com.google.android.gms.identity.intents.model.CountrySpecification> paramCollection)
    {
      if (paramCollection != null)
      {
        if (MaskedWalletRequest.this.aQu == null) {
          MaskedWalletRequest.this.aQu = new ArrayList();
        }
        MaskedWalletRequest.this.aQu.addAll(paramCollection);
      }
      return this;
    }
    
    public MaskedWalletRequest build()
    {
      return MaskedWalletRequest.this;
    }
    
    public Builder setAllowDebitCard(boolean paramBoolean)
    {
      MaskedWalletRequest.this.aQt = paramBoolean;
      return this;
    }
    
    public Builder setAllowPrepaidCard(boolean paramBoolean)
    {
      MaskedWalletRequest.this.aQs = paramBoolean;
      return this;
    }
    
    public Builder setCart(Cart paramCart)
    {
      MaskedWalletRequest.this.aPx = paramCart;
      return this;
    }
    
    public Builder setCountryCode(String paramString)
    {
      MaskedWalletRequest.this.zzcpw = paramString;
      return this;
    }
    
    public Builder setCurrencyCode(String paramString)
    {
      MaskedWalletRequest.this.aPg = paramString;
      return this;
    }
    
    public Builder setEstimatedTotalPrice(String paramString)
    {
      MaskedWalletRequest.this.aQn = paramString;
      return this;
    }
    
    @Deprecated
    public Builder setIsBillingAgreement(boolean paramBoolean)
    {
      MaskedWalletRequest.this.aQq = paramBoolean;
      return this;
    }
    
    public Builder setMerchantName(String paramString)
    {
      MaskedWalletRequest.this.aQo = paramString;
      return this;
    }
    
    public Builder setMerchantTransactionId(String paramString)
    {
      MaskedWalletRequest.this.aPn = paramString;
      return this;
    }
    
    public Builder setPaymentMethodTokenizationParameters(PaymentMethodTokenizationParameters paramPaymentMethodTokenizationParameters)
    {
      MaskedWalletRequest.this.aQv = paramPaymentMethodTokenizationParameters;
      return this;
    }
    
    public Builder setPhoneNumberRequired(boolean paramBoolean)
    {
      MaskedWalletRequest.this.aQk = paramBoolean;
      return this;
    }
    
    public Builder setShippingAddressRequired(boolean paramBoolean)
    {
      MaskedWalletRequest.this.aQl = paramBoolean;
      return this;
    }
    
    @Deprecated
    public Builder setUseMinimalBillingAddress(boolean paramBoolean)
    {
      MaskedWalletRequest.this.aQm = paramBoolean;
      return this;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/wallet/MaskedWalletRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */