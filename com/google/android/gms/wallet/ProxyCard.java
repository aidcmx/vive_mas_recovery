package com.google.android.gms.wallet;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Deprecated
public final class ProxyCard
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<ProxyCard> CREATOR = new zzr();
  String aQE;
  String aQF;
  int aQG;
  int aQH;
  private final int mVersionCode;
  
  ProxyCard(int paramInt1, String paramString1, String paramString2, int paramInt2, int paramInt3)
  {
    this.mVersionCode = paramInt1;
    this.aQE = paramString1;
    this.aQF = paramString2;
    this.aQG = paramInt2;
    this.aQH = paramInt3;
  }
  
  public String getCvn()
  {
    return this.aQF;
  }
  
  public int getExpirationMonth()
  {
    return this.aQG;
  }
  
  public int getExpirationYear()
  {
    return this.aQH;
  }
  
  public String getPan()
  {
    return this.aQE;
  }
  
  public int getVersionCode()
  {
    return this.mVersionCode;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzr.zza(this, paramParcel, paramInt);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/wallet/ProxyCard.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */