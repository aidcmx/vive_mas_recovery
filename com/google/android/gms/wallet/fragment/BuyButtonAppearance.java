package com.google.android.gms.wallet.fragment;

@Deprecated
public final class BuyButtonAppearance
{
  @Deprecated
  public static final int CLASSIC = 1;
  @Deprecated
  public static final int GRAYSCALE = 2;
  @Deprecated
  public static final int MONOCHROME = 3;
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/wallet/fragment/BuyButtonAppearance.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */