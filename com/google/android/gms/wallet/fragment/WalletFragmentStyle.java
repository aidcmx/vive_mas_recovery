package com.google.android.gms.wallet.fragment;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import com.google.android.gms.R.style;
import com.google.android.gms.R.styleable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public final class WalletFragmentStyle
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<WalletFragmentStyle> CREATOR = new zzc();
  int aRC;
  Bundle aRc;
  final int mVersionCode;
  
  public WalletFragmentStyle()
  {
    this.mVersionCode = 1;
    this.aRc = new Bundle();
    this.aRc.putInt("buyButtonAppearanceDefault", 4);
    this.aRc.putInt("maskedWalletDetailsLogoImageTypeDefault", 3);
  }
  
  WalletFragmentStyle(int paramInt1, Bundle paramBundle, int paramInt2)
  {
    this.mVersionCode = paramInt1;
    this.aRc = paramBundle;
    this.aRC = paramInt2;
  }
  
  private static int zza(long paramLong, DisplayMetrics paramDisplayMetrics)
  {
    int i = (int)(paramLong >>> 32);
    int j = (int)paramLong;
    switch (i)
    {
    default: 
      throw new IllegalStateException(36 + "Unexpected unit or type: " + i);
    case 129: 
      return j;
    case 128: 
      return TypedValue.complexToDimensionPixelSize(j, paramDisplayMetrics);
    case 0: 
      i = 0;
    }
    for (;;)
    {
      return Math.round(TypedValue.applyDimension(i, Float.intBitsToFloat(j), paramDisplayMetrics));
      i = 1;
      continue;
      i = 2;
      continue;
      i = 3;
      continue;
      i = 4;
      continue;
      i = 5;
    }
  }
  
  private static long zza(TypedValue paramTypedValue)
  {
    switch (paramTypedValue.type)
    {
    default: 
      int i = paramTypedValue.type;
      throw new IllegalArgumentException(38 + "Unexpected dimension type: " + i);
    case 16: 
      return zzacw(paramTypedValue.data);
    }
    return zzx(128, paramTypedValue.data);
  }
  
  private void zza(TypedArray paramTypedArray, int paramInt, String paramString)
  {
    if (this.aRc.containsKey(paramString)) {}
    do
    {
      return;
      paramTypedArray = paramTypedArray.peekValue(paramInt);
    } while (paramTypedArray == null);
    this.aRc.putLong(paramString, zza(paramTypedArray));
  }
  
  private void zza(TypedArray paramTypedArray, int paramInt, String paramString1, String paramString2)
  {
    if ((this.aRc.containsKey(paramString1)) || (this.aRc.containsKey(paramString2))) {}
    do
    {
      return;
      paramTypedArray = paramTypedArray.peekValue(paramInt);
    } while (paramTypedArray == null);
    if ((paramTypedArray.type >= 28) && (paramTypedArray.type <= 31))
    {
      this.aRc.putInt(paramString1, paramTypedArray.data);
      return;
    }
    this.aRc.putInt(paramString2, paramTypedArray.resourceId);
  }
  
  private static long zzacw(int paramInt)
  {
    if (paramInt < 0)
    {
      if ((paramInt == -1) || (paramInt == -2)) {
        return zzx(129, paramInt);
      }
      throw new IllegalArgumentException(39 + "Unexpected dimension value: " + paramInt);
    }
    return zzb(0, paramInt);
  }
  
  private static long zzb(int paramInt, float paramFloat)
  {
    switch (paramInt)
    {
    default: 
      throw new IllegalArgumentException(30 + "Unrecognized unit: " + paramInt);
    }
    return zzx(paramInt, Float.floatToIntBits(paramFloat));
  }
  
  private void zzb(TypedArray paramTypedArray, int paramInt, String paramString)
  {
    if (this.aRc.containsKey(paramString)) {}
    do
    {
      return;
      paramTypedArray = paramTypedArray.peekValue(paramInt);
    } while (paramTypedArray == null);
    this.aRc.putInt(paramString, paramTypedArray.data);
  }
  
  private static long zzx(int paramInt1, int paramInt2)
  {
    return paramInt1 << 32 | paramInt2 & 0xFFFFFFFF;
  }
  
  public WalletFragmentStyle setBuyButtonAppearance(int paramInt)
  {
    this.aRc.putInt("buyButtonAppearance", paramInt);
    return this;
  }
  
  public WalletFragmentStyle setBuyButtonHeight(int paramInt)
  {
    this.aRc.putLong("buyButtonHeight", zzacw(paramInt));
    return this;
  }
  
  public WalletFragmentStyle setBuyButtonHeight(int paramInt, float paramFloat)
  {
    this.aRc.putLong("buyButtonHeight", zzb(paramInt, paramFloat));
    return this;
  }
  
  public WalletFragmentStyle setBuyButtonText(int paramInt)
  {
    this.aRc.putInt("buyButtonText", paramInt);
    return this;
  }
  
  public WalletFragmentStyle setBuyButtonWidth(int paramInt)
  {
    this.aRc.putLong("buyButtonWidth", zzacw(paramInt));
    return this;
  }
  
  public WalletFragmentStyle setBuyButtonWidth(int paramInt, float paramFloat)
  {
    this.aRc.putLong("buyButtonWidth", zzb(paramInt, paramFloat));
    return this;
  }
  
  public WalletFragmentStyle setMaskedWalletDetailsBackgroundColor(int paramInt)
  {
    this.aRc.remove("maskedWalletDetailsBackgroundResource");
    this.aRc.putInt("maskedWalletDetailsBackgroundColor", paramInt);
    return this;
  }
  
  public WalletFragmentStyle setMaskedWalletDetailsBackgroundResource(int paramInt)
  {
    this.aRc.remove("maskedWalletDetailsBackgroundColor");
    this.aRc.putInt("maskedWalletDetailsBackgroundResource", paramInt);
    return this;
  }
  
  public WalletFragmentStyle setMaskedWalletDetailsButtonBackgroundColor(int paramInt)
  {
    this.aRc.remove("maskedWalletDetailsButtonBackgroundResource");
    this.aRc.putInt("maskedWalletDetailsButtonBackgroundColor", paramInt);
    return this;
  }
  
  public WalletFragmentStyle setMaskedWalletDetailsButtonBackgroundResource(int paramInt)
  {
    this.aRc.remove("maskedWalletDetailsButtonBackgroundColor");
    this.aRc.putInt("maskedWalletDetailsButtonBackgroundResource", paramInt);
    return this;
  }
  
  public WalletFragmentStyle setMaskedWalletDetailsButtonTextAppearance(int paramInt)
  {
    this.aRc.putInt("maskedWalletDetailsButtonTextAppearance", paramInt);
    return this;
  }
  
  public WalletFragmentStyle setMaskedWalletDetailsHeaderTextAppearance(int paramInt)
  {
    this.aRc.putInt("maskedWalletDetailsHeaderTextAppearance", paramInt);
    return this;
  }
  
  public WalletFragmentStyle setMaskedWalletDetailsLogoImageType(int paramInt)
  {
    this.aRc.putInt("maskedWalletDetailsLogoImageType", paramInt);
    return this;
  }
  
  @Deprecated
  public WalletFragmentStyle setMaskedWalletDetailsLogoTextColor(int paramInt)
  {
    this.aRc.putInt("maskedWalletDetailsLogoTextColor", paramInt);
    return this;
  }
  
  public WalletFragmentStyle setMaskedWalletDetailsTextAppearance(int paramInt)
  {
    this.aRc.putInt("maskedWalletDetailsTextAppearance", paramInt);
    return this;
  }
  
  public WalletFragmentStyle setStyleResourceId(int paramInt)
  {
    this.aRC = paramInt;
    return this;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzc.zza(this, paramParcel, paramInt);
  }
  
  public int zza(String paramString, DisplayMetrics paramDisplayMetrics, int paramInt)
  {
    if (this.aRc.containsKey(paramString)) {
      paramInt = zza(this.aRc.getLong(paramString), paramDisplayMetrics);
    }
    return paramInt;
  }
  
  public void zzek(Context paramContext)
  {
    if (this.aRC <= 0) {}
    for (int i = R.style.WalletFragmentDefaultStyle;; i = this.aRC)
    {
      paramContext = paramContext.obtainStyledAttributes(i, R.styleable.WalletFragmentStyle);
      zza(paramContext, R.styleable.WalletFragmentStyle_buyButtonWidth, "buyButtonWidth");
      zza(paramContext, R.styleable.WalletFragmentStyle_buyButtonHeight, "buyButtonHeight");
      zzb(paramContext, R.styleable.WalletFragmentStyle_buyButtonText, "buyButtonText");
      zzb(paramContext, R.styleable.WalletFragmentStyle_buyButtonAppearance, "buyButtonAppearance");
      zzb(paramContext, R.styleable.WalletFragmentStyle_maskedWalletDetailsTextAppearance, "maskedWalletDetailsTextAppearance");
      zzb(paramContext, R.styleable.WalletFragmentStyle_maskedWalletDetailsHeaderTextAppearance, "maskedWalletDetailsHeaderTextAppearance");
      zza(paramContext, R.styleable.WalletFragmentStyle_maskedWalletDetailsBackground, "maskedWalletDetailsBackgroundColor", "maskedWalletDetailsBackgroundResource");
      zzb(paramContext, R.styleable.WalletFragmentStyle_maskedWalletDetailsButtonTextAppearance, "maskedWalletDetailsButtonTextAppearance");
      zza(paramContext, R.styleable.WalletFragmentStyle_maskedWalletDetailsButtonBackground, "maskedWalletDetailsButtonBackgroundColor", "maskedWalletDetailsButtonBackgroundResource");
      zzb(paramContext, R.styleable.WalletFragmentStyle_maskedWalletDetailsLogoTextColor, "maskedWalletDetailsLogoTextColor");
      zzb(paramContext, R.styleable.WalletFragmentStyle_maskedWalletDetailsLogoImageType, "maskedWalletDetailsLogoImageType");
      paramContext.recycle();
      return;
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface BuyButtonAppearance
  {
    public static final int ANDROID_PAY_DARK = 4;
    public static final int ANDROID_PAY_LIGHT = 5;
    public static final int ANDROID_PAY_LIGHT_WITH_BORDER = 6;
    @Deprecated
    public static final int GOOGLE_WALLET_CLASSIC = 1;
    @Deprecated
    public static final int GOOGLE_WALLET_GRAYSCALE = 2;
    @Deprecated
    public static final int GOOGLE_WALLET_MONOCHROME = 3;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface BuyButtonText
  {
    public static final int BUY_WITH = 5;
    public static final int DONATE_WITH = 7;
    public static final int LOGO_ONLY = 6;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface Dimension
  {
    public static final int MATCH_PARENT = -1;
    public static final int UNIT_DIP = 1;
    public static final int UNIT_IN = 4;
    public static final int UNIT_MM = 5;
    public static final int UNIT_PT = 3;
    public static final int UNIT_PX = 0;
    public static final int UNIT_SP = 2;
    public static final int WRAP_CONTENT = -2;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface LogoImageType
  {
    public static final int ANDROID_PAY = 3;
    @Deprecated
    public static final int GOOGLE_WALLET_CLASSIC = 1;
    @Deprecated
    public static final int GOOGLE_WALLET_MONOCHROME = 2;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/wallet/fragment/WalletFragmentStyle.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */