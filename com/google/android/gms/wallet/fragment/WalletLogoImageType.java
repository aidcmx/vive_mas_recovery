package com.google.android.gms.wallet.fragment;

@Deprecated
public final class WalletLogoImageType
{
  @Deprecated
  public static final int CLASSIC = 1;
  @Deprecated
  public static final int MONOCHROME = 2;
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/android/gms/wallet/fragment/WalletLogoImageType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */