package com.google.firebase.appindexing;

import android.support.annotation.NonNull;
import com.google.firebase.FirebaseException;

public class FirebaseAppIndexingException
  extends FirebaseException
{
  public FirebaseAppIndexingException(@NonNull String paramString)
  {
    super(paramString);
  }
  
  public FirebaseAppIndexingException(@NonNull String paramString, Throwable paramThrowable)
  {
    super(paramString, paramThrowable);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/firebase/appindexing/FirebaseAppIndexingException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */