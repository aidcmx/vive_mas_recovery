package com.google.firebase.appindexing;

import android.support.annotation.NonNull;

public class FirebaseAppIndexingInvalidArgumentException
  extends FirebaseAppIndexingException
{
  public FirebaseAppIndexingInvalidArgumentException(@NonNull String paramString)
  {
    super(paramString);
  }
  
  public FirebaseAppIndexingInvalidArgumentException(@NonNull String paramString, Throwable paramThrowable)
  {
    super(paramString, paramThrowable);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/firebase/appindexing/FirebaseAppIndexingInvalidArgumentException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */