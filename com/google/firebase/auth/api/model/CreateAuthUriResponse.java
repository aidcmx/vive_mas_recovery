package com.google.firebase.auth.api.model;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.Nullable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.internal.zzahk;
import com.google.android.gms.internal.zzapn;
import java.util.List;

public class CreateAuthUriResponse
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<CreateAuthUriResponse> CREATOR = new zza();
  @zzapn("authUri")
  private String aXJ;
  @zzapn("providerId")
  private String aXK;
  @zzapn("forExistingProvider")
  private boolean aXL;
  @zzapn("allProviders")
  private StringList aXM;
  @zzapn("registered")
  private boolean fy;
  @zzahk
  public final int mVersionCode;
  
  public CreateAuthUriResponse()
  {
    this.mVersionCode = 1;
    this.aXM = StringList.zzcqb();
  }
  
  CreateAuthUriResponse(int paramInt, String paramString1, boolean paramBoolean1, String paramString2, boolean paramBoolean2, StringList paramStringList)
  {
    this.mVersionCode = paramInt;
    this.aXJ = paramString1;
    this.fy = paramBoolean1;
    this.aXK = paramString2;
    this.aXL = paramBoolean2;
    if (paramStringList == null) {}
    for (paramString1 = StringList.zzcqb();; paramString1 = StringList.zza(paramStringList))
    {
      this.aXM = paramString1;
      return;
    }
  }
  
  @Nullable
  public List<String> getAllProviders()
  {
    return this.aXM.zzcqa();
  }
  
  @Nullable
  public String getProviderId()
  {
    return this.aXK;
  }
  
  public boolean isRegistered()
  {
    return this.fy;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zza.zza(this, paramParcel, paramInt);
  }
  
  @Nullable
  public String zzcpo()
  {
    return this.aXJ;
  }
  
  public boolean zzcpp()
  {
    return this.aXL;
  }
  
  public StringList zzcpq()
  {
    return this.aXM;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/firebase/auth/api/model/CreateAuthUriResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */