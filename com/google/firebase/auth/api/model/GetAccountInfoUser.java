package com.google.firebase.auth.api.model;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.internal.zzahk;
import com.google.android.gms.internal.zzapn;
import java.util.List;

public class GetAccountInfoUser
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<GetAccountInfoUser> CREATOR = new zzb();
  @zzapn("localId")
  private String Mq;
  @zzapn("photoUrl")
  private String Pg;
  @zzapn("emailVerified")
  private boolean aXN;
  @zzapn("providerUserInfo")
  private ProviderUserInfoList aXO;
  @zzapn("passwordHash")
  private String io;
  @zzapn("email")
  private String jg;
  @zzapn("displayName")
  private String jh;
  @zzahk
  public final int mVersionCode;
  
  public GetAccountInfoUser()
  {
    this.mVersionCode = 1;
    this.aXO = new ProviderUserInfoList();
  }
  
  GetAccountInfoUser(int paramInt, String paramString1, String paramString2, boolean paramBoolean, String paramString3, String paramString4, ProviderUserInfoList paramProviderUserInfoList, String paramString5)
  {
    this.mVersionCode = paramInt;
    this.Mq = paramString1;
    this.jg = paramString2;
    this.aXN = paramBoolean;
    this.jh = paramString3;
    this.Pg = paramString4;
    if (paramProviderUserInfoList == null) {}
    for (paramString1 = ProviderUserInfoList.zzcpz();; paramString1 = ProviderUserInfoList.zza(paramProviderUserInfoList))
    {
      this.aXO = paramString1;
      this.io = paramString5;
      return;
    }
  }
  
  @Nullable
  public String getDisplayName()
  {
    return this.jh;
  }
  
  @Nullable
  public String getEmail()
  {
    return this.jg;
  }
  
  @NonNull
  public String getLocalId()
  {
    return this.Mq;
  }
  
  @Nullable
  public String getPassword()
  {
    return this.io;
  }
  
  @Nullable
  public Uri getPhotoUri()
  {
    if (!TextUtils.isEmpty(this.Pg)) {
      return Uri.parse(this.Pg);
    }
    return null;
  }
  
  public boolean isEmailVerified()
  {
    return this.aXN;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzb.zza(this, paramParcel, paramInt);
  }
  
  @Nullable
  public String zzcpc()
  {
    return this.Pg;
  }
  
  @NonNull
  public List<ProviderUserInfo> zzcpr()
  {
    return this.aXO.zzcpr();
  }
  
  public ProviderUserInfoList zzcps()
  {
    return this.aXO;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/firebase/auth/api/model/GetAccountInfoUser.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */