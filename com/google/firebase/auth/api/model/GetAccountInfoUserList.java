package com.google.firebase.auth.api.model;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.internal.zzahk;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GetAccountInfoUserList
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<GetAccountInfoUserList> CREATOR = new zzc();
  private List<GetAccountInfoUser> aXP;
  @zzahk
  public final int mVersionCode;
  
  public GetAccountInfoUserList()
  {
    this.mVersionCode = 1;
    this.aXP = new ArrayList();
  }
  
  GetAccountInfoUserList(int paramInt, List<GetAccountInfoUser> paramList)
  {
    this.mVersionCode = paramInt;
    if (paramList == null) {}
    for (paramList = Collections.emptyList();; paramList = Collections.unmodifiableList(paramList))
    {
      this.aXP = paramList;
      return;
    }
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzc.zza(this, paramParcel, paramInt);
  }
  
  public List<GetAccountInfoUser> zzcpt()
  {
    return this.aXP;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/firebase/auth/api/model/GetAccountInfoUserList.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */