package com.google.firebase.auth.api.model;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.util.zze;
import com.google.android.gms.common.util.zzh;
import com.google.android.gms.internal.zzahk;
import com.google.android.gms.internal.zzapn;

public class GetTokenResponse
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<GetTokenResponse> CREATOR = new zzd();
  @zzapn("access_token")
  private String aCW;
  @zzapn("expires_in")
  private Long aXQ;
  @zzapn("token_type")
  private String aXR;
  @zzapn("issued_at")
  private Long aXS;
  @zzapn("refresh_token")
  private String aXk;
  @zzahk
  public final int mVersionCode;
  
  public GetTokenResponse()
  {
    this.mVersionCode = 1;
    this.aXS = Long.valueOf(System.currentTimeMillis());
  }
  
  GetTokenResponse(int paramInt, String paramString1, String paramString2, Long paramLong1, String paramString3, Long paramLong2)
  {
    this.mVersionCode = paramInt;
    this.aXk = paramString1;
    this.aCW = paramString2;
    this.aXQ = paramLong1;
    this.aXR = paramString3;
    this.aXS = paramLong2;
  }
  
  public String getAccessToken()
  {
    return this.aCW;
  }
  
  public boolean isValid()
  {
    long l1 = this.aXS.longValue();
    long l2 = this.aXQ.longValue();
    return zzh.zzayl().currentTimeMillis() + 300000L < l1 + l2 * 1000L;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzd.zza(this, paramParcel, paramInt);
  }
  
  public String zzcpu()
  {
    return this.aXk;
  }
  
  public long zzcpv()
  {
    if (this.aXQ == null) {
      return 0L;
    }
    return this.aXQ.longValue();
  }
  
  @Nullable
  public String zzcpw()
  {
    return this.aXR;
  }
  
  public long zzcpx()
  {
    return this.aXS.longValue();
  }
  
  public void zzrv(@NonNull String paramString)
  {
    this.aXk = zzaa.zzib(paramString);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/firebase/auth/api/model/GetTokenResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */