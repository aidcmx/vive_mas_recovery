package com.google.firebase.auth.api.model;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.internal.zzahk;
import com.google.android.gms.internal.zzapn;

public class ProviderUserInfo
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<ProviderUserInfo> CREATOR = new zze();
  @zzapn("photoUrl")
  private String Pg;
  @zzapn("providerId")
  private String aXK;
  @zzapn("federatedId")
  private String aXT;
  @zzapn("displayName")
  private String jh;
  @zzahk
  public final int mVersionCode;
  
  public ProviderUserInfo()
  {
    this.mVersionCode = 1;
  }
  
  ProviderUserInfo(int paramInt, String paramString1, String paramString2, String paramString3, String paramString4)
  {
    this.mVersionCode = paramInt;
    this.aXT = paramString1;
    this.jh = paramString2;
    this.Pg = paramString3;
    this.aXK = paramString4;
  }
  
  @Nullable
  public String getDisplayName()
  {
    return this.jh;
  }
  
  @Nullable
  public Uri getPhotoUri()
  {
    if (!TextUtils.isEmpty(this.Pg)) {
      return Uri.parse(this.Pg);
    }
    return null;
  }
  
  public String getProviderId()
  {
    return this.aXK;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zze.zza(this, paramParcel, paramInt);
  }
  
  @Nullable
  public String zzcpc()
  {
    return this.Pg;
  }
  
  public String zzcpy()
  {
    return this.aXT;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/firebase/auth/api/model/ProviderUserInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */