package com.google.firebase.auth.api.model;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.Nullable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.internal.zzahk;
import com.google.android.gms.internal.zzapn;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class StringList
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<StringList> CREATOR = new zzg();
  @zzapn("values")
  private List<String> aXV;
  @zzahk
  public final int mVersionCode;
  
  public StringList()
  {
    this(null);
  }
  
  StringList(int paramInt, List<String> paramList)
  {
    this.mVersionCode = paramInt;
    if ((paramList == null) || (paramList.isEmpty()))
    {
      this.aXV = Collections.emptyList();
      return;
    }
    this.aXV = Collections.unmodifiableList(paramList);
  }
  
  public StringList(@Nullable List<String> paramList)
  {
    this.mVersionCode = 1;
    this.aXV = new ArrayList();
    if ((paramList != null) && (!paramList.isEmpty())) {
      this.aXV.addAll(paramList);
    }
  }
  
  public static StringList zza(StringList paramStringList)
  {
    if (paramStringList != null) {}
    for (paramStringList = paramStringList.zzcqa();; paramStringList = null) {
      return new StringList(paramStringList);
    }
  }
  
  public static StringList zzcqb()
  {
    return new StringList(null);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzg.zza(this, paramParcel, paramInt);
  }
  
  public List<String> zzcqa()
  {
    return this.aXV;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/firebase/auth/api/model/StringList.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */