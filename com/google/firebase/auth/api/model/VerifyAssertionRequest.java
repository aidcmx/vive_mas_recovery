package com.google.firebase.auth.api.model;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.internal.zzahk;
import com.google.android.gms.internal.zzapn;

public class VerifyAssertionRequest
  extends AbstractSafeParcelable
{
  public static final Parcelable.Creator<VerifyAssertionRequest> CREATOR = new zzh();
  @zzahk
  private String aCW;
  @zzahk
  private String aXK;
  @zzapn("requestUri")
  private String aXW;
  @zzapn("idToken")
  private String aXX;
  @zzapn("oauthTokenSecret")
  private String aXY;
  @zzapn("returnSecureToken")
  private boolean aXZ;
  @zzahk
  private String iF;
  @zzahk
  @Nullable
  private String jg;
  @zzahk
  public final int mVersionCode;
  @zzapn("postBody")
  private String zzbql;
  
  public VerifyAssertionRequest()
  {
    this.mVersionCode = 2;
    this.aXZ = true;
  }
  
  VerifyAssertionRequest(int paramInt, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, boolean paramBoolean)
  {
    this.mVersionCode = paramInt;
    this.aXW = paramString1;
    this.aXX = paramString2;
    this.iF = paramString3;
    this.aCW = paramString4;
    this.aXK = paramString5;
    this.jg = paramString6;
    this.zzbql = paramString7;
    this.aXY = paramString8;
    this.aXZ = paramBoolean;
  }
  
  public VerifyAssertionRequest(@Nullable String paramString1, @Nullable String paramString2, String paramString3, @Nullable String paramString4, @Nullable String paramString5)
  {
    this.mVersionCode = 2;
    this.aXW = "http://localhost";
    this.iF = paramString1;
    this.aCW = paramString2;
    this.aXY = paramString5;
    this.aXZ = true;
    if ((TextUtils.isEmpty(this.iF)) && (TextUtils.isEmpty(this.aCW))) {
      throw new IllegalArgumentException("Both idToken, and accessToken cannot be null");
    }
    this.aXK = zzaa.zzib(paramString3);
    this.jg = paramString4;
    paramString1 = new StringBuilder();
    if (!TextUtils.isEmpty(this.iF)) {
      paramString1.append("id_token").append("=").append(this.iF).append("&");
    }
    if (!TextUtils.isEmpty(this.aCW)) {
      paramString1.append("access_token").append("=").append(this.aCW).append("&");
    }
    if (!TextUtils.isEmpty(this.jg)) {
      paramString1.append("identifier").append("=").append(this.jg).append("&");
    }
    if (!TextUtils.isEmpty(this.aXY)) {
      paramString1.append("oauth_token_secret").append("=").append(this.aXY).append("&");
    }
    paramString1.append("providerId").append("=").append(this.aXK);
    this.zzbql = paramString1.toString();
  }
  
  public String getAccessToken()
  {
    return this.aCW;
  }
  
  @Nullable
  public String getEmail()
  {
    return this.jg;
  }
  
  public String getIdToken()
  {
    return this.iF;
  }
  
  public String getProviderId()
  {
    return this.aXK;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzh.zza(this, paramParcel, paramInt);
  }
  
  public String zzcqc()
  {
    return this.aXW;
  }
  
  public String zzcqd()
  {
    return this.aXX;
  }
  
  public String zzcqe()
  {
    return this.aXY;
  }
  
  public boolean zzcqf()
  {
    return this.aXZ;
  }
  
  public String zznf()
  {
    return this.zzbql;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/firebase/auth/api/model/VerifyAssertionRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */