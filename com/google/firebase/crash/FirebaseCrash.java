package com.google.firebase.crash;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.crash.internal.FirebaseCrashOptions;
import com.google.firebase.crash.internal.zza;
import com.google.firebase.crash.internal.zzb;
import com.google.firebase.crash.internal.zzd;
import com.google.firebase.crash.internal.zzf;
import com.google.firebase.iid.zzc;

public class FirebaseCrash
{
  private static final String LOG_TAG = FirebaseCrash.class.getSimpleName();
  private static volatile FirebaseCrash aYp;
  private zzd aYn;
  private zza aYo;
  private boolean arv;
  
  FirebaseCrash(FirebaseApp paramFirebaseApp, boolean paramBoolean)
  {
    this.arv = paramBoolean;
    Object localObject = paramFirebaseApp.getApplicationContext();
    if (localObject == null)
    {
      Log.w(LOG_TAG, "Application context is missing, disabling api");
      this.arv = false;
    }
    if (this.arv)
    {
      try
      {
        String str = paramFirebaseApp.getOptions().getApiKey();
        paramFirebaseApp = new FirebaseCrashOptions(paramFirebaseApp.getOptions().getApplicationId(), str);
        com.google.firebase.crash.internal.zze.zzcqt().zzbo((Context)localObject);
        this.aYn = com.google.firebase.crash.internal.zze.zzcqt().zzcqu();
        this.aYn.zza(com.google.android.gms.dynamic.zze.zzac(localObject), paramFirebaseApp);
        this.aYo = new zza((Context)localObject);
        zzcqp();
        localObject = LOG_TAG;
        paramFirebaseApp = String.valueOf(com.google.firebase.crash.internal.zze.zzcqt().toString());
        if (paramFirebaseApp.length() != 0) {}
        for (paramFirebaseApp = "FirebaseCrash reporting initialized ".concat(paramFirebaseApp);; paramFirebaseApp = new String("FirebaseCrash reporting initialized "))
        {
          Log.i((String)localObject, paramFirebaseApp);
          return;
        }
        paramFirebaseApp = "Failed to initialize crash reporting: ".concat(paramFirebaseApp);
      }
      catch (Exception paramFirebaseApp)
      {
        localObject = LOG_TAG;
        paramFirebaseApp = String.valueOf(paramFirebaseApp.getMessage());
        if (paramFirebaseApp.length() == 0) {}
      }
      for (;;)
      {
        Log.e((String)localObject, paramFirebaseApp);
        this.arv = false;
        return;
        paramFirebaseApp = new String("Failed to initialize crash reporting: ");
      }
    }
    Log.i(LOG_TAG, "Crash reporting is disabled");
  }
  
  /* Error */
  @Deprecated
  @android.support.annotation.Keep
  public static FirebaseCrash getInstance(FirebaseApp paramFirebaseApp)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual 39	com/google/firebase/FirebaseApp:getApplicationContext	()Landroid/content/Context;
    //   4: invokestatic 147	com/google/android/gms/internal/zzaib:initialize	(Landroid/content/Context;)V
    //   7: new 2	com/google/firebase/crash/FirebaseCrash
    //   10: dup
    //   11: aload_0
    //   12: getstatic 151	com/google/android/gms/internal/zzaib:aYz	Lcom/google/android/gms/internal/zzvq;
    //   15: invokevirtual 157	com/google/android/gms/internal/zzvq:get	()Ljava/lang/Object;
    //   18: checkcast 159	java/lang/Boolean
    //   21: invokevirtual 163	java/lang/Boolean:booleanValue	()Z
    //   24: invokespecial 165	com/google/firebase/crash/FirebaseCrash:<init>	(Lcom/google/firebase/FirebaseApp;Z)V
    //   27: astore_0
    //   28: ldc 2
    //   30: monitorenter
    //   31: getstatic 167	com/google/firebase/crash/FirebaseCrash:aYp	Lcom/google/firebase/crash/FirebaseCrash;
    //   34: ifnonnull +13 -> 47
    //   37: aload_0
    //   38: putstatic 167	com/google/firebase/crash/FirebaseCrash:aYp	Lcom/google/firebase/crash/FirebaseCrash;
    //   41: getstatic 167	com/google/firebase/crash/FirebaseCrash:aYp	Lcom/google/firebase/crash/FirebaseCrash;
    //   44: invokespecial 170	com/google/firebase/crash/FirebaseCrash:zzcqm	()V
    //   47: ldc 2
    //   49: monitorexit
    //   50: aload_0
    //   51: areturn
    //   52: astore_1
    //   53: getstatic 24	com/google/firebase/crash/FirebaseCrash:LOG_TAG	Ljava/lang/String;
    //   56: ldc -84
    //   58: invokestatic 175	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
    //   61: pop
    //   62: goto -15 -> 47
    //   65: astore_0
    //   66: ldc 2
    //   68: monitorexit
    //   69: aload_0
    //   70: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	71	0	paramFirebaseApp	FirebaseApp
    //   52	1	1	localzzb	zzb
    // Exception table:
    //   from	to	target	type
    //   41	47	52	com/google/firebase/crash/internal/zzb
    //   31	41	65	finally
    //   41	47	65	finally
    //   47	50	65	finally
    //   53	62	65	finally
    //   66	69	65	finally
  }
  
  private boolean isEnabled()
  {
    return this.arv;
  }
  
  public static void log(String paramString)
  {
    try
    {
      zzcql().zzry(paramString);
      return;
    }
    catch (zzb paramString)
    {
      Log.v(LOG_TAG, paramString.getMessage());
    }
  }
  
  public static void logcat(int paramInt, String paramString1, String paramString2)
  {
    try
    {
      zzcql().zze(paramInt, paramString1, paramString2);
      return;
    }
    catch (zzb paramString1)
    {
      Log.v(LOG_TAG, paramString1.getMessage());
    }
  }
  
  public static void report(Throwable paramThrowable)
  {
    try
    {
      zzcql().zzh(paramThrowable);
      return;
    }
    catch (zzb paramThrowable)
    {
      Log.v(LOG_TAG, paramThrowable.getMessage());
    }
  }
  
  public static void zza(String paramString, long paramLong, Bundle paramBundle)
  {
    try
    {
      zzcql().zzb(paramString, paramLong, paramBundle);
      return;
    }
    catch (zzb paramString)
    {
      Log.v(LOG_TAG, paramString.getMessage());
    }
  }
  
  public static FirebaseCrash zzcql()
  {
    if (aYp == null) {}
    try
    {
      if (aYp == null) {
        aYp = getInstance(FirebaseApp.getInstance());
      }
      return aYp;
    }
    finally {}
  }
  
  private void zzcqm()
    throws zzb
  {
    if (!isEnabled()) {
      throw new zzb("Firebase Crash Reporting is disabled.");
    }
    this.aYo.zzcqr();
  }
  
  private zzd zzcqn()
  {
    return this.aYn;
  }
  
  private static boolean zzcqo()
  {
    return Looper.getMainLooper().getThread().getId() == Thread.currentThread().getId();
  }
  
  private void zzcqp()
  {
    if (!zzcqo()) {
      throw new RuntimeException("FirebaseCrash reporting may only be initialized on the main thread (preferably in your app's Application.onCreate)");
    }
    Thread.setDefaultUncaughtExceptionHandler(new zzf(Thread.getDefaultUncaughtExceptionHandler(), this));
  }
  
  private String zzcqq()
  {
    return zzc.C().getId();
  }
  
  public void zzb(String paramString, long paramLong, Bundle paramBundle)
    throws zzb
  {
    if (!isEnabled()) {
      throw new zzb("Firebase Crash Reporting is disabled.");
    }
    zzd localzzd = zzcqn();
    if ((localzzd != null) && (paramString != null)) {}
    try
    {
      localzzd.zzb(paramString, paramLong, paramBundle);
      return;
    }
    catch (RemoteException paramString)
    {
      Log.e(LOG_TAG, "log remoting failed", paramString);
    }
  }
  
  public void zze(int paramInt, String paramString1, String paramString2)
    throws zzb
  {
    if (paramString2 != null)
    {
      String str = paramString1;
      if (paramString1 == null) {
        str = "";
      }
      Log.println(paramInt, str, paramString2);
      zzry(paramString2);
    }
  }
  
  public void zzh(Throwable paramThrowable)
    throws zzb
  {
    if (!isEnabled()) {
      throw new zzb("Firebase Crash Reporting is disabled.");
    }
    zzd localzzd = zzcqn();
    if ((localzzd != null) && (paramThrowable != null)) {
      this.aYo.zza(false, System.currentTimeMillis());
    }
    try
    {
      localzzd.zzrz(zzcqq());
      localzzd.zzan(com.google.android.gms.dynamic.zze.zzac(paramThrowable));
      return;
    }
    catch (RemoteException paramThrowable)
    {
      Log.e(LOG_TAG, "report remoting failed", paramThrowable);
    }
  }
  
  public void zzi(Throwable paramThrowable)
    throws zzb
  {
    if (!isEnabled()) {
      throw new zzb("Firebase Crash Reporting is disabled.");
    }
    zzd localzzd = zzcqn();
    if ((localzzd != null) && (paramThrowable != null)) {
      try
      {
        this.aYo.zza(true, System.currentTimeMillis());
        try
        {
          Thread.sleep(200L);
          localzzd.zzrz(zzcqq());
          localzzd.zzao(com.google.android.gms.dynamic.zze.zzac(paramThrowable));
          return;
        }
        catch (InterruptedException localInterruptedException)
        {
          for (;;)
          {
            Thread.currentThread().interrupt();
          }
        }
        return;
      }
      catch (RemoteException paramThrowable)
      {
        Log.e(LOG_TAG, "report remoting failed", paramThrowable);
      }
    }
  }
  
  public void zzry(String paramString)
    throws zzb
  {
    if (!isEnabled()) {
      throw new zzb("Firebase Crash Reporting is disabled.");
    }
    zzd localzzd = zzcqn();
    if ((localzzd != null) && (paramString != null)) {}
    try
    {
      localzzd.log(paramString);
      return;
    }
    catch (RemoteException paramString)
    {
      Log.e(LOG_TAG, "log remoting failed", paramString);
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/google/firebase/crash/FirebaseCrash.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */