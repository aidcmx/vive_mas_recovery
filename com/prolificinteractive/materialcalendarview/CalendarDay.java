package com.prolificinteractive.materialcalendarview;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.Calendar;
import java.util.Date;

public final class CalendarDay
  implements Parcelable
{
  public static final Parcelable.Creator<CalendarDay> CREATOR = new CalendarDay.1();
  private transient Calendar _calendar;
  private transient Date _date;
  private final int day;
  private final int month;
  private final int year;
  
  @Deprecated
  public CalendarDay()
  {
    this(CalendarUtils.getInstance());
  }
  
  @Deprecated
  public CalendarDay(int paramInt1, int paramInt2, int paramInt3)
  {
    this.year = paramInt1;
    this.month = paramInt2;
    this.day = paramInt3;
  }
  
  public CalendarDay(Parcel paramParcel)
  {
    this(paramParcel.readInt(), paramParcel.readInt(), paramParcel.readInt());
  }
  
  @Deprecated
  public CalendarDay(Calendar paramCalendar)
  {
    this(CalendarUtils.getYear(paramCalendar), CalendarUtils.getMonth(paramCalendar), CalendarUtils.getDay(paramCalendar));
  }
  
  @Deprecated
  public CalendarDay(Date paramDate)
  {
    this(CalendarUtils.getInstance(paramDate));
  }
  
  @NonNull
  public static CalendarDay from(int paramInt1, int paramInt2, int paramInt3)
  {
    return new CalendarDay(paramInt1, paramInt2, paramInt3);
  }
  
  public static CalendarDay from(@Nullable Calendar paramCalendar)
  {
    if (paramCalendar == null) {
      return null;
    }
    return from(CalendarUtils.getYear(paramCalendar), CalendarUtils.getMonth(paramCalendar), CalendarUtils.getDay(paramCalendar));
  }
  
  public static CalendarDay from(@Nullable Date paramDate)
  {
    if (paramDate == null) {
      return null;
    }
    return from(CalendarUtils.getInstance(paramDate));
  }
  
  private static int hashCode(int paramInt1, int paramInt2, int paramInt3)
  {
    return paramInt1 * 10000 + paramInt2 * 100 + paramInt3;
  }
  
  @NonNull
  public static CalendarDay today()
  {
    return from(CalendarUtils.getInstance());
  }
  
  public void copyTo(@NonNull Calendar paramCalendar)
  {
    paramCalendar.clear();
    paramCalendar.set(this.year, this.month, this.day);
  }
  
  void copyToMonthOnly(@NonNull Calendar paramCalendar)
  {
    paramCalendar.clear();
    paramCalendar.set(this.year, this.month, 1);
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    do
    {
      return true;
      if ((paramObject == null) || (getClass() != paramObject.getClass())) {
        return false;
      }
      paramObject = (CalendarDay)paramObject;
    } while ((this.day == ((CalendarDay)paramObject).day) && (this.month == ((CalendarDay)paramObject).month) && (this.year == ((CalendarDay)paramObject).year));
    return false;
  }
  
  @NonNull
  public Calendar getCalendar()
  {
    if (this._calendar == null)
    {
      this._calendar = CalendarUtils.getInstance();
      copyTo(this._calendar);
    }
    return this._calendar;
  }
  
  @NonNull
  public Date getDate()
  {
    if (this._date == null) {
      this._date = getCalendar().getTime();
    }
    return this._date;
  }
  
  public int getDay()
  {
    return this.day;
  }
  
  public int getMonth()
  {
    return this.month;
  }
  
  public int getYear()
  {
    return this.year;
  }
  
  public int hashCode()
  {
    return hashCode(this.year, this.month, this.day);
  }
  
  public boolean isAfter(@NonNull CalendarDay paramCalendarDay)
  {
    if (paramCalendarDay == null) {
      throw new IllegalArgumentException("other cannot be null");
    }
    if (this.year == paramCalendarDay.year) {
      if (this.month == paramCalendarDay.month) {
        if (this.day <= paramCalendarDay.day) {}
      }
    }
    while (this.year > paramCalendarDay.year)
    {
      do
      {
        return true;
        return false;
      } while (this.month > paramCalendarDay.month);
      return false;
    }
    return false;
  }
  
  public boolean isBefore(@NonNull CalendarDay paramCalendarDay)
  {
    if (paramCalendarDay == null) {
      throw new IllegalArgumentException("other cannot be null");
    }
    if (this.year == paramCalendarDay.year) {
      if (this.month == paramCalendarDay.month) {
        if (this.day >= paramCalendarDay.day) {}
      }
    }
    while (this.year < paramCalendarDay.year)
    {
      do
      {
        return true;
        return false;
      } while (this.month < paramCalendarDay.month);
      return false;
    }
    return false;
  }
  
  public boolean isInRange(@Nullable CalendarDay paramCalendarDay1, @Nullable CalendarDay paramCalendarDay2)
  {
    return ((paramCalendarDay1 == null) || (!paramCalendarDay1.isAfter(this))) && ((paramCalendarDay2 == null) || (!paramCalendarDay2.isBefore(this)));
  }
  
  public String toString()
  {
    return "CalendarDay{" + this.year + "-" + this.month + "-" + this.day + "}";
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramParcel.writeInt(this.year);
    paramParcel.writeInt(this.month);
    paramParcel.writeInt(this.day);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/prolificinteractive/materialcalendarview/CalendarDay.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */