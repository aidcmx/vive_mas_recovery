package com.prolificinteractive.materialcalendarview;

@Experimental
public enum CalendarMode
{
  MONTHS(6),  WEEKS(1);
  
  final int visibleWeeksCount;
  
  private CalendarMode(int paramInt)
  {
    this.visibleWeeksCount = paramInt;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/prolificinteractive/materialcalendarview/CalendarMode.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */