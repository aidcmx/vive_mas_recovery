package com.prolificinteractive.materialcalendarview;

import android.content.Context;
import android.support.v4.view.BetterViewPager;
import android.view.MotionEvent;

class CalendarPager
  extends BetterViewPager
{
  private boolean pagingEnabled = true;
  
  public CalendarPager(Context paramContext)
  {
    super(paramContext);
  }
  
  public boolean canScrollHorizontally(int paramInt)
  {
    return (this.pagingEnabled) && (super.canScrollHorizontally(paramInt));
  }
  
  public boolean canScrollVertically(int paramInt)
  {
    return (this.pagingEnabled) && (super.canScrollVertically(paramInt));
  }
  
  public boolean isPagingEnabled()
  {
    return this.pagingEnabled;
  }
  
  public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
  {
    return (this.pagingEnabled) && (super.onInterceptTouchEvent(paramMotionEvent));
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    return (this.pagingEnabled) && (super.onTouchEvent(paramMotionEvent));
  }
  
  public void setPagingEnabled(boolean paramBoolean)
  {
    this.pagingEnabled = paramBoolean;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/prolificinteractive/materialcalendarview/CalendarPager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */