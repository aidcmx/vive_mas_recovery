package com.prolificinteractive.materialcalendarview;

import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import com.prolificinteractive.materialcalendarview.format.DayFormatter;
import com.prolificinteractive.materialcalendarview.format.TitleFormatter;
import com.prolificinteractive.materialcalendarview.format.WeekDayFormatter;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

abstract class CalendarPagerAdapter<V extends CalendarPagerView>
  extends PagerAdapter
{
  private Integer color = null;
  private final ArrayDeque<V> currentViews;
  private Integer dateTextAppearance = null;
  private DayFormatter dayFormatter = DayFormatter.DEFAULT;
  private List<DecoratorResult> decoratorResults = null;
  private List<DayViewDecorator> decorators = new ArrayList();
  private CalendarDay maxDate = null;
  protected final MaterialCalendarView mcv;
  private CalendarDay minDate = null;
  private DateRangeIndex rangeIndex;
  private List<CalendarDay> selectedDates = new ArrayList();
  private boolean selectionEnabled = true;
  @MaterialCalendarView.ShowOtherDates
  private int showOtherDates = 4;
  private TitleFormatter titleFormatter = null;
  private final CalendarDay today;
  private WeekDayFormatter weekDayFormatter = WeekDayFormatter.DEFAULT;
  private Integer weekDayTextAppearance = null;
  
  CalendarPagerAdapter(MaterialCalendarView paramMaterialCalendarView)
  {
    this.mcv = paramMaterialCalendarView;
    this.today = CalendarDay.today();
    this.currentViews = new ArrayDeque();
    this.currentViews.iterator();
    setRangeDates(null, null);
  }
  
  private void invalidateSelectedDates()
  {
    validateSelectedDates();
    Iterator localIterator = this.currentViews.iterator();
    while (localIterator.hasNext()) {
      ((CalendarPagerView)localIterator.next()).setSelectedDates(this.selectedDates);
    }
  }
  
  private void validateSelectedDates()
  {
    int j;
    for (int i = 0; i < this.selectedDates.size(); i = j + 1)
    {
      CalendarDay localCalendarDay = (CalendarDay)this.selectedDates.get(i);
      if ((this.minDate == null) || (!this.minDate.isAfter(localCalendarDay)))
      {
        j = i;
        if (this.maxDate != null)
        {
          j = i;
          if (!this.maxDate.isBefore(localCalendarDay)) {}
        }
      }
      else
      {
        this.selectedDates.remove(i);
        this.mcv.onDateUnselected(localCalendarDay);
        j = i - 1;
      }
    }
  }
  
  public void clearSelections()
  {
    this.selectedDates.clear();
    invalidateSelectedDates();
  }
  
  protected abstract DateRangeIndex createRangeIndex(CalendarDay paramCalendarDay1, CalendarDay paramCalendarDay2);
  
  protected abstract V createView(int paramInt);
  
  public void destroyItem(ViewGroup paramViewGroup, int paramInt, Object paramObject)
  {
    paramObject = (CalendarPagerView)paramObject;
    this.currentViews.remove(paramObject);
    paramViewGroup.removeView((View)paramObject);
  }
  
  public int getCount()
  {
    return this.rangeIndex.getCount();
  }
  
  protected int getDateTextAppearance()
  {
    if (this.dateTextAppearance == null) {
      return 0;
    }
    return this.dateTextAppearance.intValue();
  }
  
  public int getIndexForDay(CalendarDay paramCalendarDay)
  {
    if (paramCalendarDay == null) {
      return getCount() / 2;
    }
    if ((this.minDate != null) && (paramCalendarDay.isBefore(this.minDate))) {
      return 0;
    }
    if ((this.maxDate != null) && (paramCalendarDay.isAfter(this.maxDate))) {
      return getCount() - 1;
    }
    return this.rangeIndex.indexOf(paramCalendarDay);
  }
  
  public CalendarDay getItem(int paramInt)
  {
    return this.rangeIndex.getItem(paramInt);
  }
  
  public int getItemPosition(Object paramObject)
  {
    int i;
    if (!isInstanceOfView(paramObject)) {
      i = -2;
    }
    int j;
    do
    {
      return i;
      if (((CalendarPagerView)paramObject).getFirstViewDay() == null) {
        return -2;
      }
      j = indexOf((CalendarPagerView)paramObject);
      i = j;
    } while (j >= 0);
    return -2;
  }
  
  public CharSequence getPageTitle(int paramInt)
  {
    if (this.titleFormatter == null) {
      return "";
    }
    return this.titleFormatter.format(getItem(paramInt));
  }
  
  public DateRangeIndex getRangeIndex()
  {
    return this.rangeIndex;
  }
  
  @NonNull
  public List<CalendarDay> getSelectedDates()
  {
    return Collections.unmodifiableList(this.selectedDates);
  }
  
  @MaterialCalendarView.ShowOtherDates
  public int getShowOtherDates()
  {
    return this.showOtherDates;
  }
  
  protected int getWeekDayTextAppearance()
  {
    if (this.weekDayTextAppearance == null) {
      return 0;
    }
    return this.weekDayTextAppearance.intValue();
  }
  
  protected abstract int indexOf(V paramV);
  
  public Object instantiateItem(ViewGroup paramViewGroup, int paramInt)
  {
    CalendarPagerView localCalendarPagerView = createView(paramInt);
    localCalendarPagerView.setContentDescription(this.mcv.getCalendarContentDescription());
    localCalendarPagerView.setAlpha(0.0F);
    localCalendarPagerView.setSelectionEnabled(this.selectionEnabled);
    localCalendarPagerView.setWeekDayFormatter(this.weekDayFormatter);
    localCalendarPagerView.setDayFormatter(this.dayFormatter);
    if (this.color != null) {
      localCalendarPagerView.setSelectionColor(this.color.intValue());
    }
    if (this.dateTextAppearance != null) {
      localCalendarPagerView.setDateTextAppearance(this.dateTextAppearance.intValue());
    }
    if (this.weekDayTextAppearance != null) {
      localCalendarPagerView.setWeekDayTextAppearance(this.weekDayTextAppearance.intValue());
    }
    localCalendarPagerView.setShowOtherDates(this.showOtherDates);
    localCalendarPagerView.setMinimumDate(this.minDate);
    localCalendarPagerView.setMaximumDate(this.maxDate);
    localCalendarPagerView.setSelectedDates(this.selectedDates);
    paramViewGroup.addView(localCalendarPagerView);
    this.currentViews.add(localCalendarPagerView);
    localCalendarPagerView.setDayViewDecorators(this.decoratorResults);
    return localCalendarPagerView;
  }
  
  public void invalidateDecorators()
  {
    this.decoratorResults = new ArrayList();
    Iterator localIterator = this.decorators.iterator();
    while (localIterator.hasNext())
    {
      DayViewDecorator localDayViewDecorator = (DayViewDecorator)localIterator.next();
      DayViewFacade localDayViewFacade = new DayViewFacade();
      localDayViewDecorator.decorate(localDayViewFacade);
      if (localDayViewFacade.isDecorated()) {
        this.decoratorResults.add(new DecoratorResult(localDayViewDecorator, localDayViewFacade));
      }
    }
    localIterator = this.currentViews.iterator();
    while (localIterator.hasNext()) {
      ((CalendarPagerView)localIterator.next()).setDayViewDecorators(this.decoratorResults);
    }
  }
  
  protected abstract boolean isInstanceOfView(Object paramObject);
  
  public boolean isViewFromObject(View paramView, Object paramObject)
  {
    return paramView == paramObject;
  }
  
  public CalendarPagerAdapter<?> migrateStateAndReturn(CalendarPagerAdapter<?> paramCalendarPagerAdapter)
  {
    paramCalendarPagerAdapter.titleFormatter = this.titleFormatter;
    paramCalendarPagerAdapter.color = this.color;
    paramCalendarPagerAdapter.dateTextAppearance = this.dateTextAppearance;
    paramCalendarPagerAdapter.weekDayTextAppearance = this.weekDayTextAppearance;
    paramCalendarPagerAdapter.showOtherDates = this.showOtherDates;
    paramCalendarPagerAdapter.minDate = this.minDate;
    paramCalendarPagerAdapter.maxDate = this.maxDate;
    paramCalendarPagerAdapter.selectedDates = this.selectedDates;
    paramCalendarPagerAdapter.weekDayFormatter = this.weekDayFormatter;
    paramCalendarPagerAdapter.dayFormatter = this.dayFormatter;
    paramCalendarPagerAdapter.decorators = this.decorators;
    paramCalendarPagerAdapter.decoratorResults = this.decoratorResults;
    paramCalendarPagerAdapter.selectionEnabled = this.selectionEnabled;
    return paramCalendarPagerAdapter;
  }
  
  public void setDateSelected(CalendarDay paramCalendarDay, boolean paramBoolean)
  {
    if (paramBoolean) {
      if (!this.selectedDates.contains(paramCalendarDay))
      {
        this.selectedDates.add(paramCalendarDay);
        invalidateSelectedDates();
      }
    }
    while (!this.selectedDates.contains(paramCalendarDay)) {
      return;
    }
    this.selectedDates.remove(paramCalendarDay);
    invalidateSelectedDates();
  }
  
  public void setDateTextAppearance(int paramInt)
  {
    if (paramInt == 0) {}
    for (;;)
    {
      return;
      this.dateTextAppearance = Integer.valueOf(paramInt);
      Iterator localIterator = this.currentViews.iterator();
      while (localIterator.hasNext()) {
        ((CalendarPagerView)localIterator.next()).setDateTextAppearance(paramInt);
      }
    }
  }
  
  public void setDayFormatter(DayFormatter paramDayFormatter)
  {
    this.dayFormatter = paramDayFormatter;
    Iterator localIterator = this.currentViews.iterator();
    while (localIterator.hasNext()) {
      ((CalendarPagerView)localIterator.next()).setDayFormatter(paramDayFormatter);
    }
  }
  
  public void setDecorators(List<DayViewDecorator> paramList)
  {
    this.decorators = paramList;
    invalidateDecorators();
  }
  
  public void setRangeDates(CalendarDay paramCalendarDay1, CalendarDay paramCalendarDay2)
  {
    this.minDate = paramCalendarDay1;
    this.maxDate = paramCalendarDay2;
    Object localObject = this.currentViews.iterator();
    while (((Iterator)localObject).hasNext())
    {
      CalendarPagerView localCalendarPagerView = (CalendarPagerView)((Iterator)localObject).next();
      localCalendarPagerView.setMinimumDate(paramCalendarDay1);
      localCalendarPagerView.setMaximumDate(paramCalendarDay2);
    }
    localObject = paramCalendarDay1;
    if (paramCalendarDay1 == null) {
      localObject = CalendarDay.from(this.today.getYear() - 200, this.today.getMonth(), this.today.getDay());
    }
    paramCalendarDay1 = paramCalendarDay2;
    if (paramCalendarDay2 == null) {
      paramCalendarDay1 = CalendarDay.from(this.today.getYear() + 200, this.today.getMonth(), this.today.getDay());
    }
    this.rangeIndex = createRangeIndex((CalendarDay)localObject, paramCalendarDay1);
    notifyDataSetChanged();
    invalidateSelectedDates();
  }
  
  public void setSelectionColor(int paramInt)
  {
    this.color = Integer.valueOf(paramInt);
    Iterator localIterator = this.currentViews.iterator();
    while (localIterator.hasNext()) {
      ((CalendarPagerView)localIterator.next()).setSelectionColor(paramInt);
    }
  }
  
  public void setSelectionEnabled(boolean paramBoolean)
  {
    this.selectionEnabled = paramBoolean;
    Iterator localIterator = this.currentViews.iterator();
    while (localIterator.hasNext()) {
      ((CalendarPagerView)localIterator.next()).setSelectionEnabled(this.selectionEnabled);
    }
  }
  
  public void setShowOtherDates(@MaterialCalendarView.ShowOtherDates int paramInt)
  {
    this.showOtherDates = paramInt;
    Iterator localIterator = this.currentViews.iterator();
    while (localIterator.hasNext()) {
      ((CalendarPagerView)localIterator.next()).setShowOtherDates(paramInt);
    }
  }
  
  public void setTitleFormatter(@NonNull TitleFormatter paramTitleFormatter)
  {
    this.titleFormatter = paramTitleFormatter;
  }
  
  public void setWeekDayFormatter(WeekDayFormatter paramWeekDayFormatter)
  {
    this.weekDayFormatter = paramWeekDayFormatter;
    Iterator localIterator = this.currentViews.iterator();
    while (localIterator.hasNext()) {
      ((CalendarPagerView)localIterator.next()).setWeekDayFormatter(paramWeekDayFormatter);
    }
  }
  
  public void setWeekDayTextAppearance(int paramInt)
  {
    if (paramInt == 0) {}
    for (;;)
    {
      return;
      this.weekDayTextAppearance = Integer.valueOf(paramInt);
      Iterator localIterator = this.currentViews.iterator();
      while (localIterator.hasNext()) {
        ((CalendarPagerView)localIterator.next()).setWeekDayTextAppearance(paramInt);
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/prolificinteractive/materialcalendarview/CalendarPagerAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */