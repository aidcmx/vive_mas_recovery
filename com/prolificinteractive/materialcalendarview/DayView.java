package com.prolificinteractive.materialcalendarview;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.ConstantState;
import android.graphics.drawable.RippleDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.StateListDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.text.SpannableString;
import android.text.Spanned;
import android.widget.CheckedTextView;
import com.prolificinteractive.materialcalendarview.format.DayFormatter;
import java.util.Iterator;
import java.util.List;

@SuppressLint({"ViewConstructor"})
class DayView
  extends CheckedTextView
{
  private Drawable customBackground = null;
  private CalendarDay date;
  private final int fadeTime = getResources().getInteger(17694720);
  private DayFormatter formatter = DayFormatter.DEFAULT;
  private boolean isDecoratedDisabled = false;
  private boolean isInMonth = true;
  private boolean isInRange = true;
  private Drawable mCircleDrawable;
  private int selectionColor = -7829368;
  private Drawable selectionDrawable;
  @MaterialCalendarView.ShowOtherDates
  private int showOtherDates = 4;
  private final Rect tempRect = new Rect();
  
  public DayView(Context paramContext, CalendarDay paramCalendarDay)
  {
    super(paramContext);
    setSelectionColor(this.selectionColor);
    setGravity(17);
    if (Build.VERSION.SDK_INT >= 17) {
      setTextAlignment(4);
    }
    setDay(paramCalendarDay);
  }
  
  private void calculateBounds(int paramInt1, int paramInt2)
  {
    int j = Math.min(paramInt2, paramInt1);
    if (Build.VERSION.SDK_INT == 21) {}
    for (int i = 4;; i = 2)
    {
      i = Math.abs(paramInt2 - paramInt1) / i;
      if (paramInt1 < paramInt2) {
        break;
      }
      this.tempRect.set(i, 0, j + i, paramInt2);
      return;
    }
    this.tempRect.set(0, i, paramInt1, j + i);
  }
  
  private static Drawable generateBackground(int paramInt1, int paramInt2, Rect paramRect)
  {
    StateListDrawable localStateListDrawable = new StateListDrawable();
    localStateListDrawable.setExitFadeDuration(paramInt2);
    Drawable localDrawable = generateCircleDrawable(paramInt1);
    localStateListDrawable.addState(new int[] { 16842912 }, localDrawable);
    if (Build.VERSION.SDK_INT >= 21)
    {
      paramRect = generateRippleDrawable(paramInt1, paramRect);
      localStateListDrawable.addState(new int[] { 16842919 }, paramRect);
    }
    for (;;)
    {
      paramRect = generateCircleDrawable(0);
      localStateListDrawable.addState(new int[0], paramRect);
      return localStateListDrawable;
      paramRect = generateCircleDrawable(paramInt1);
      localStateListDrawable.addState(new int[] { 16842919 }, paramRect);
    }
  }
  
  private static Drawable generateCircleDrawable(int paramInt)
  {
    ShapeDrawable localShapeDrawable = new ShapeDrawable(new OvalShape());
    localShapeDrawable.getPaint().setColor(paramInt);
    return localShapeDrawable;
  }
  
  @TargetApi(21)
  private static Drawable generateRippleDrawable(int paramInt, Rect paramRect)
  {
    RippleDrawable localRippleDrawable = new RippleDrawable(ColorStateList.valueOf(paramInt), null, generateCircleDrawable(-1));
    if (Build.VERSION.SDK_INT == 21) {
      localRippleDrawable.setBounds(paramRect);
    }
    if (Build.VERSION.SDK_INT == 22)
    {
      paramInt = (paramRect.left + paramRect.right) / 2;
      localRippleDrawable.setHotspotBounds(paramInt, paramRect.top, paramInt, paramRect.bottom);
    }
    return localRippleDrawable;
  }
  
  private void regenerateBackground()
  {
    if (this.selectionDrawable != null)
    {
      setBackgroundDrawable(this.selectionDrawable);
      return;
    }
    this.mCircleDrawable = generateBackground(this.selectionColor, this.fadeTime, this.tempRect);
    setBackgroundDrawable(this.mCircleDrawable);
  }
  
  private void setEnabled()
  {
    int i2 = 0;
    boolean bool1;
    label43:
    int i1;
    if ((this.isInMonth) && (this.isInRange) && (!this.isDecoratedDisabled))
    {
      i = 1;
      if ((!this.isInRange) || (this.isDecoratedDisabled)) {
        break label210;
      }
      bool1 = true;
      super.setEnabled(bool1);
      bool1 = MaterialCalendarView.showOtherMonths(this.showOtherDates);
      if ((!MaterialCalendarView.showOutOfRange(this.showOtherDates)) && (!bool1)) {
        break label216;
      }
      i1 = 1;
      label75:
      boolean bool2 = MaterialCalendarView.showDecoratedDisabled(this.showOtherDates);
      int k = i;
      if (!this.isInMonth)
      {
        k = i;
        if (bool1) {
          m = 1;
        }
      }
      i = m;
      if (!this.isInRange)
      {
        i = m;
        if (i1 != 0) {
          i = m | this.isInMonth;
        }
      }
      int m = i;
      if (this.isDecoratedDisabled)
      {
        m = i;
        if (bool2)
        {
          if ((!this.isInMonth) || (!this.isInRange)) {
            break label221;
          }
          m = 1;
          label156:
          m = i | m;
        }
      }
      if ((!this.isInMonth) && (m != 0)) {
        setTextColor(getTextColors().getColorForState(new int[] { -16842910 }, -7829368));
      }
      if (m == 0) {
        break label226;
      }
    }
    int j;
    label210:
    label216:
    label221:
    label226:
    for (int i = i2;; j = 4)
    {
      setVisibility(i);
      return;
      j = 0;
      break;
      bool1 = false;
      break label43;
      i1 = 0;
      break label75;
      int n = 0;
      break label156;
    }
  }
  
  void applyFacade(DayViewFacade paramDayViewFacade)
  {
    this.isDecoratedDisabled = paramDayViewFacade.areDaysDisabled();
    setEnabled();
    setCustomBackground(paramDayViewFacade.getBackgroundDrawable());
    setSelectionDrawable(paramDayViewFacade.getSelectionDrawable());
    Object localObject = paramDayViewFacade.getSpans();
    if (!((List)localObject).isEmpty())
    {
      paramDayViewFacade = getLabel();
      SpannableString localSpannableString = new SpannableString(getLabel());
      localObject = ((List)localObject).iterator();
      while (((Iterator)localObject).hasNext()) {
        localSpannableString.setSpan(((DayViewFacade.Span)((Iterator)localObject).next()).span, 0, paramDayViewFacade.length(), 33);
      }
      setText(localSpannableString);
      return;
    }
    setText(getLabel());
  }
  
  public CalendarDay getDate()
  {
    return this.date;
  }
  
  @NonNull
  public String getLabel()
  {
    return this.formatter.format(this.date);
  }
  
  protected void onDraw(@NonNull Canvas paramCanvas)
  {
    if (this.customBackground != null)
    {
      this.customBackground.setBounds(this.tempRect);
      this.customBackground.setState(getDrawableState());
      this.customBackground.draw(paramCanvas);
    }
    this.mCircleDrawable.setBounds(this.tempRect);
    super.onDraw(paramCanvas);
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
    calculateBounds(paramInt3 - paramInt1, paramInt4 - paramInt2);
    regenerateBackground();
  }
  
  public void setCustomBackground(Drawable paramDrawable)
  {
    if (paramDrawable == null) {}
    for (this.customBackground = null;; this.customBackground = paramDrawable.getConstantState().newDrawable(getResources()))
    {
      invalidate();
      return;
    }
  }
  
  public void setDay(CalendarDay paramCalendarDay)
  {
    this.date = paramCalendarDay;
    setText(getLabel());
  }
  
  public void setDayFormatter(DayFormatter paramDayFormatter)
  {
    Object localObject = paramDayFormatter;
    if (paramDayFormatter == null) {
      localObject = DayFormatter.DEFAULT;
    }
    this.formatter = ((DayFormatter)localObject);
    localObject = getText();
    paramDayFormatter = null;
    if ((localObject instanceof Spanned)) {
      paramDayFormatter = ((Spanned)localObject).getSpans(0, ((CharSequence)localObject).length(), Object.class);
    }
    localObject = new SpannableString(getLabel());
    if (paramDayFormatter != null)
    {
      int j = paramDayFormatter.length;
      int i = 0;
      while (i < j)
      {
        ((SpannableString)localObject).setSpan(paramDayFormatter[i], 0, ((SpannableString)localObject).length(), 33);
        i += 1;
      }
    }
    setText((CharSequence)localObject);
  }
  
  public void setSelectionColor(int paramInt)
  {
    this.selectionColor = paramInt;
    regenerateBackground();
  }
  
  public void setSelectionDrawable(Drawable paramDrawable)
  {
    if (paramDrawable == null) {}
    for (this.selectionDrawable = null;; this.selectionDrawable = paramDrawable.getConstantState().newDrawable(getResources()))
    {
      regenerateBackground();
      return;
    }
  }
  
  protected void setupSelection(@MaterialCalendarView.ShowOtherDates int paramInt, boolean paramBoolean1, boolean paramBoolean2)
  {
    this.showOtherDates = paramInt;
    this.isInMonth = paramBoolean2;
    this.isInRange = paramBoolean1;
    setEnabled();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/prolificinteractive/materialcalendarview/DayView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */