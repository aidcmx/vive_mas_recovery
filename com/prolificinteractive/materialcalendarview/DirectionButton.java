package com.prolificinteractive.materialcalendarview;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.graphics.PorterDuff.Mode;
import android.os.Build.VERSION;
import android.util.TypedValue;
import android.widget.ImageView;

class DirectionButton
  extends ImageView
{
  public DirectionButton(Context paramContext)
  {
    super(paramContext);
    setBackgroundResource(getThemeSelectableBackgroundId(paramContext));
  }
  
  private static int getThemeSelectableBackgroundId(Context paramContext)
  {
    int j = paramContext.getResources().getIdentifier("selectableItemBackgroundBorderless", "attr", paramContext.getPackageName());
    int i = j;
    if (j == 0) {
      if (Build.VERSION.SDK_INT < 21) {
        break label57;
      }
    }
    label57:
    for (i = 16843868;; i = 16843534)
    {
      TypedValue localTypedValue = new TypedValue();
      paramContext.getTheme().resolveAttribute(i, localTypedValue, true);
      return localTypedValue.resourceId;
    }
  }
  
  public void setColor(int paramInt)
  {
    setColorFilter(paramInt, PorterDuff.Mode.SRC_ATOP);
  }
  
  public void setEnabled(boolean paramBoolean)
  {
    super.setEnabled(paramBoolean);
    if (paramBoolean) {}
    for (float f = 1.0F;; f = 0.1F)
    {
      setAlpha(f);
      return;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/prolificinteractive/materialcalendarview/DirectionButton.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */