package com.prolificinteractive.materialcalendarview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.ArrayRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v4.view.ViewPager.PageTransformer;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.BaseSavedState;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import com.prolificinteractive.materialcalendarview.format.ArrayWeekDayFormatter;
import com.prolificinteractive.materialcalendarview.format.DateFormatTitleFormatter;
import com.prolificinteractive.materialcalendarview.format.DayFormatter;
import com.prolificinteractive.materialcalendarview.format.MonthArrayTitleFormatter;
import com.prolificinteractive.materialcalendarview.format.TitleFormatter;
import com.prolificinteractive.materialcalendarview.format.WeekDayFormatter;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class MaterialCalendarView
  extends ViewGroup
{
  private static final int DAY_NAMES_ROW = 1;
  private static final int DEFAULT_DAYS_IN_WEEK = 7;
  private static final int DEFAULT_MAX_WEEKS = 6;
  public static final int DEFAULT_TILE_SIZE_DP = 44;
  private static final TitleFormatter DEFAULT_TITLE_FORMATTER = new DateFormatTitleFormatter();
  public static final int SELECTION_MODE_MULTIPLE = 2;
  public static final int SELECTION_MODE_NONE = 0;
  public static final int SELECTION_MODE_RANGE = 3;
  public static final int SELECTION_MODE_SINGLE = 1;
  public static final int SHOW_ALL = 7;
  public static final int SHOW_DECORATED_DISABLED = 4;
  public static final int SHOW_DEFAULTS = 4;
  public static final int SHOW_NONE = 0;
  public static final int SHOW_OTHER_MONTHS = 1;
  public static final int SHOW_OUT_OF_RANGE = 2;
  private int accentColor = 0;
  private CalendarPagerAdapter<?> adapter;
  private boolean allowClickDaysOutsideCurrentMonth = true;
  private int arrowColor = -16777216;
  private final DirectionButton buttonFuture;
  private final DirectionButton buttonPast;
  CharSequence calendarContentDescription;
  private CalendarMode calendarMode;
  private CalendarDay currentMonth;
  private final ArrayList<DayViewDecorator> dayViewDecorators = new ArrayList();
  private int firstDayOfWeek;
  private Drawable leftArrowMask;
  private OnDateSelectedListener listener;
  private boolean mDynamicHeightEnabled;
  private CalendarDay maxDate = null;
  private CalendarDay minDate = null;
  private OnMonthChangedListener monthListener;
  private final View.OnClickListener onClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      if (paramAnonymousView == MaterialCalendarView.this.buttonFuture) {
        MaterialCalendarView.this.pager.setCurrentItem(MaterialCalendarView.this.pager.getCurrentItem() + 1, true);
      }
      while (paramAnonymousView != MaterialCalendarView.this.buttonPast) {
        return;
      }
      MaterialCalendarView.this.pager.setCurrentItem(MaterialCalendarView.this.pager.getCurrentItem() - 1, true);
    }
  };
  private final ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener()
  {
    public void onPageScrollStateChanged(int paramAnonymousInt) {}
    
    public void onPageScrolled(int paramAnonymousInt1, float paramAnonymousFloat, int paramAnonymousInt2) {}
    
    public void onPageSelected(int paramAnonymousInt)
    {
      MaterialCalendarView.this.titleChanger.setPreviousMonth(MaterialCalendarView.this.currentMonth);
      MaterialCalendarView.access$302(MaterialCalendarView.this, MaterialCalendarView.this.adapter.getItem(paramAnonymousInt));
      MaterialCalendarView.this.updateUi();
      MaterialCalendarView.this.dispatchOnMonthChanged(MaterialCalendarView.this.currentMonth);
    }
  };
  private final CalendarPager pager;
  private OnRangeSelectedListener rangeListener;
  private Drawable rightArrowMask;
  @SelectionMode
  private int selectionMode = 1;
  private State state;
  private int tileHeight = -1;
  private int tileWidth = -1;
  private final TextView title;
  private final TitleChanger titleChanger;
  private LinearLayout topbar;
  
  public MaterialCalendarView(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public MaterialCalendarView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    if (Build.VERSION.SDK_INT >= 19)
    {
      setClipToPadding(false);
      setClipChildren(false);
    }
    for (;;)
    {
      this.buttonPast = new DirectionButton(getContext());
      this.buttonPast.setContentDescription(getContext().getString(R.string.previous));
      this.title = new TextView(getContext());
      this.buttonFuture = new DirectionButton(getContext());
      this.buttonFuture.setContentDescription(getContext().getString(R.string.next));
      this.pager = new CalendarPager(getContext());
      this.title.setOnClickListener(this.onClickListener);
      this.buttonPast.setOnClickListener(this.onClickListener);
      this.buttonFuture.setOnClickListener(this.onClickListener);
      this.titleChanger = new TitleChanger(this.title);
      this.titleChanger.setTitleFormatter(DEFAULT_TITLE_FORMATTER);
      this.pager.setOnPageChangeListener(this.pageChangeListener);
      this.pager.setPageTransformer(false, new ViewPager.PageTransformer()
      {
        public void transformPage(View paramAnonymousView, float paramAnonymousFloat)
        {
          paramAnonymousView.setAlpha((float)Math.sqrt(1.0F - Math.abs(paramAnonymousFloat)));
        }
      });
      localTypedArray = paramContext.getTheme().obtainStyledAttributes(paramAttributeSet, R.styleable.MaterialCalendarView, 0, 0);
      try
      {
        int i = localTypedArray.getInteger(R.styleable.MaterialCalendarView_mcv_calendarMode, 0);
        this.firstDayOfWeek = localTypedArray.getInteger(R.styleable.MaterialCalendarView_mcv_firstDayOfWeek, -1);
        if (this.firstDayOfWeek < 0) {
          this.firstDayOfWeek = Calendar.getInstance().getFirstDayOfWeek();
        }
        newState().setFirstDayOfWeek(this.firstDayOfWeek).setCalendarDisplayMode(CalendarMode.values()[i]).commit();
        i = localTypedArray.getDimensionPixelSize(R.styleable.MaterialCalendarView_mcv_tileSize, -1);
        if (i > 0) {
          setTileSize(i);
        }
        i = localTypedArray.getDimensionPixelSize(R.styleable.MaterialCalendarView_mcv_tileWidth, -1);
        if (i > 0) {
          setTileWidth(i);
        }
        i = localTypedArray.getDimensionPixelSize(R.styleable.MaterialCalendarView_mcv_tileHeight, -1);
        if (i > 0) {
          setTileHeight(i);
        }
        setArrowColor(localTypedArray.getColor(R.styleable.MaterialCalendarView_mcv_arrowColor, -16777216));
        Drawable localDrawable = localTypedArray.getDrawable(R.styleable.MaterialCalendarView_mcv_leftArrowMask);
        paramAttributeSet = localDrawable;
        if (localDrawable == null) {
          paramAttributeSet = getResources().getDrawable(R.drawable.mcv_action_previous);
        }
        setLeftArrowMask(paramAttributeSet);
        localDrawable = localTypedArray.getDrawable(R.styleable.MaterialCalendarView_mcv_rightArrowMask);
        paramAttributeSet = localDrawable;
        if (localDrawable == null) {
          paramAttributeSet = getResources().getDrawable(R.drawable.mcv_action_next);
        }
        setRightArrowMask(paramAttributeSet);
        setSelectionColor(localTypedArray.getColor(R.styleable.MaterialCalendarView_mcv_selectionColor, getThemeAccentColor(paramContext)));
        paramContext = localTypedArray.getTextArray(R.styleable.MaterialCalendarView_mcv_weekDayLabels);
        if (paramContext != null) {
          setWeekDayFormatter(new ArrayWeekDayFormatter(paramContext));
        }
        paramContext = localTypedArray.getTextArray(R.styleable.MaterialCalendarView_mcv_monthLabels);
        if (paramContext != null) {
          setTitleFormatter(new MonthArrayTitleFormatter(paramContext));
        }
        setHeaderTextAppearance(localTypedArray.getResourceId(R.styleable.MaterialCalendarView_mcv_headerTextAppearance, R.style.TextAppearance_MaterialCalendarWidget_Header));
        setWeekDayTextAppearance(localTypedArray.getResourceId(R.styleable.MaterialCalendarView_mcv_weekDayTextAppearance, R.style.TextAppearance_MaterialCalendarWidget_WeekDay));
        setDateTextAppearance(localTypedArray.getResourceId(R.styleable.MaterialCalendarView_mcv_dateTextAppearance, R.style.TextAppearance_MaterialCalendarWidget_Date));
        setShowOtherDates(localTypedArray.getInteger(R.styleable.MaterialCalendarView_mcv_showOtherDates, 4));
        setAllowClickDaysOutsideCurrentMonth(localTypedArray.getBoolean(R.styleable.MaterialCalendarView_mcv_allowClickDaysOutsideCurrentMonth, true));
      }
      catch (Exception paramContext)
      {
        for (;;)
        {
          paramContext.printStackTrace();
          localTypedArray.recycle();
        }
      }
      finally
      {
        localTypedArray.recycle();
      }
      this.adapter.setTitleFormatter(DEFAULT_TITLE_FORMATTER);
      setupChildren();
      this.currentMonth = CalendarDay.today();
      setCurrentDate(this.currentMonth);
      if (isInEditMode())
      {
        removeView(this.pager);
        paramContext = new MonthView(this, this.currentMonth, getFirstDayOfWeek());
        paramContext.setSelectionColor(getSelectionColor());
        paramContext.setDateTextAppearance(this.adapter.getDateTextAppearance());
        paramContext.setWeekDayTextAppearance(this.adapter.getWeekDayTextAppearance());
        paramContext.setShowOtherDates(getShowOtherDates());
        addView(paramContext, new LayoutParams(this.calendarMode.visibleWeeksCount + 1));
      }
      return;
      setClipChildren(true);
      setClipToPadding(true);
    }
  }
  
  private static int clampSize(int paramInt1, int paramInt2)
  {
    int j = View.MeasureSpec.getMode(paramInt2);
    int i = View.MeasureSpec.getSize(paramInt2);
    paramInt2 = i;
    switch (j)
    {
    default: 
      paramInt2 = paramInt1;
    case 1073741824: 
      return paramInt2;
    }
    return Math.min(paramInt1, i);
  }
  
  private void commit(State paramState)
  {
    this.state = paramState;
    this.calendarMode = paramState.calendarMode;
    this.firstDayOfWeek = paramState.firstDayOfWeek;
    this.minDate = paramState.minDate;
    this.maxDate = paramState.maxDate;
    switch (this.calendarMode)
    {
    default: 
      throw new IllegalArgumentException("Provided display mode which is not yet implemented");
    case ???: 
      paramState = new MonthPagerAdapter(this);
      if (this.adapter == null)
      {
        this.adapter = paramState;
        label104:
        this.pager.setAdapter(this.adapter);
        setRangeDates(this.minDate, this.maxDate);
        this.pager.setLayoutParams(new LayoutParams(this.calendarMode.visibleWeeksCount + 1));
        if ((this.selectionMode != 1) || (this.adapter.getSelectedDates().isEmpty())) {
          break label231;
        }
      }
      break;
    }
    label231:
    for (paramState = (CalendarDay)this.adapter.getSelectedDates().get(0);; paramState = CalendarDay.today())
    {
      setCurrentDate(paramState);
      invalidateDecorators();
      updateUi();
      return;
      paramState = new WeekPagerAdapter(this);
      break;
      this.adapter = this.adapter.migrateStateAndReturn(paramState);
      break label104;
    }
  }
  
  private int dpToPx(int paramInt)
  {
    return (int)TypedValue.applyDimension(1, paramInt, getResources().getDisplayMetrics());
  }
  
  private static int getThemeAccentColor(Context paramContext)
  {
    if (Build.VERSION.SDK_INT >= 21) {}
    for (int i = 16843829;; i = paramContext.getResources().getIdentifier("colorAccent", "attr", paramContext.getPackageName()))
    {
      TypedValue localTypedValue = new TypedValue();
      paramContext.getTheme().resolveAttribute(i, localTypedValue, true);
      return localTypedValue.data;
    }
  }
  
  private int getWeekCountBasedOnMode()
  {
    int j = this.calendarMode.visibleWeeksCount;
    int i = j;
    if (this.calendarMode.equals(CalendarMode.MONTHS))
    {
      i = j;
      if (this.mDynamicHeightEnabled)
      {
        i = j;
        if (this.adapter != null)
        {
          i = j;
          if (this.pager != null)
          {
            Calendar localCalendar = (Calendar)this.adapter.getItem(this.pager.getCurrentItem()).getCalendar().clone();
            localCalendar.set(5, localCalendar.getActualMaximum(5));
            localCalendar.setFirstDayOfWeek(getFirstDayOfWeek());
            i = localCalendar.get(4);
          }
        }
      }
    }
    return i + 1;
  }
  
  private void setRangeDates(CalendarDay paramCalendarDay1, CalendarDay paramCalendarDay2)
  {
    CalendarDay localCalendarDay = this.currentMonth;
    this.adapter.setRangeDates(paramCalendarDay1, paramCalendarDay2);
    this.currentMonth = localCalendarDay;
    if (paramCalendarDay1 != null) {
      if (!paramCalendarDay1.isAfter(this.currentMonth)) {
        break label65;
      }
    }
    for (;;)
    {
      this.currentMonth = paramCalendarDay1;
      int i = this.adapter.getIndexForDay(localCalendarDay);
      this.pager.setCurrentItem(i, false);
      updateUi();
      return;
      label65:
      paramCalendarDay1 = this.currentMonth;
    }
  }
  
  private void setupChildren()
  {
    this.topbar = new LinearLayout(getContext());
    this.topbar.setOrientation(0);
    this.topbar.setClipChildren(false);
    this.topbar.setClipToPadding(false);
    addView(this.topbar, new LayoutParams(1));
    this.buttonPast.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
    this.buttonPast.setImageResource(R.drawable.mcv_action_previous);
    this.topbar.addView(this.buttonPast, new LinearLayout.LayoutParams(0, -1, 1.0F));
    this.title.setGravity(17);
    this.topbar.addView(this.title, new LinearLayout.LayoutParams(0, -1, 5.0F));
    this.buttonFuture.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
    this.buttonFuture.setImageResource(R.drawable.mcv_action_next);
    this.topbar.addView(this.buttonFuture, new LinearLayout.LayoutParams(0, -1, 1.0F));
    this.pager.setId(R.id.mcv_pager);
    this.pager.setOffscreenPageLimit(1);
    addView(this.pager, new LayoutParams(this.calendarMode.visibleWeeksCount + 1));
  }
  
  public static boolean showDecoratedDisabled(@ShowOtherDates int paramInt)
  {
    return (paramInt & 0x4) != 0;
  }
  
  public static boolean showOtherMonths(@ShowOtherDates int paramInt)
  {
    return (paramInt & 0x1) != 0;
  }
  
  public static boolean showOutOfRange(@ShowOtherDates int paramInt)
  {
    return (paramInt & 0x2) != 0;
  }
  
  private void updateUi()
  {
    this.titleChanger.change(this.currentMonth);
    this.buttonPast.setEnabled(canGoBack());
    this.buttonFuture.setEnabled(canGoForward());
  }
  
  public void addDecorator(DayViewDecorator paramDayViewDecorator)
  {
    if (paramDayViewDecorator == null) {
      return;
    }
    this.dayViewDecorators.add(paramDayViewDecorator);
    this.adapter.setDecorators(this.dayViewDecorators);
  }
  
  public void addDecorators(Collection<? extends DayViewDecorator> paramCollection)
  {
    if (paramCollection == null) {
      return;
    }
    this.dayViewDecorators.addAll(paramCollection);
    this.adapter.setDecorators(this.dayViewDecorators);
  }
  
  public void addDecorators(DayViewDecorator... paramVarArgs)
  {
    addDecorators(Arrays.asList(paramVarArgs));
  }
  
  public boolean allowClickDaysOutsideCurrentMonth()
  {
    return this.allowClickDaysOutsideCurrentMonth;
  }
  
  public boolean canGoBack()
  {
    return this.pager.getCurrentItem() > 0;
  }
  
  public boolean canGoForward()
  {
    return this.pager.getCurrentItem() < this.adapter.getCount() - 1;
  }
  
  protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
  {
    return paramLayoutParams instanceof LayoutParams;
  }
  
  public void clearSelection()
  {
    Object localObject = getSelectedDates();
    this.adapter.clearSelections();
    localObject = ((List)localObject).iterator();
    while (((Iterator)localObject).hasNext()) {
      dispatchOnDateSelected((CalendarDay)((Iterator)localObject).next(), false);
    }
  }
  
  protected void dispatchOnDateSelected(CalendarDay paramCalendarDay, boolean paramBoolean)
  {
    OnDateSelectedListener localOnDateSelectedListener = this.listener;
    if (localOnDateSelectedListener != null) {
      localOnDateSelectedListener.onDateSelected(this, paramCalendarDay, paramBoolean);
    }
  }
  
  protected void dispatchOnMonthChanged(CalendarDay paramCalendarDay)
  {
    OnMonthChangedListener localOnMonthChangedListener = this.monthListener;
    if (localOnMonthChangedListener != null) {
      localOnMonthChangedListener.onMonthChanged(this, paramCalendarDay);
    }
  }
  
  protected void dispatchOnRangeSelected(CalendarDay paramCalendarDay1, CalendarDay paramCalendarDay2)
  {
    OnRangeSelectedListener localOnRangeSelectedListener = this.rangeListener;
    ArrayList localArrayList = new ArrayList();
    Calendar localCalendar = Calendar.getInstance();
    localCalendar.setTime(paramCalendarDay1.getDate());
    paramCalendarDay1 = Calendar.getInstance();
    paramCalendarDay1.setTime(paramCalendarDay2.getDate());
    while ((localCalendar.before(paramCalendarDay1)) || (localCalendar.equals(paramCalendarDay1)))
    {
      paramCalendarDay2 = CalendarDay.from(localCalendar);
      this.adapter.setDateSelected(paramCalendarDay2, true);
      localArrayList.add(paramCalendarDay2);
      localCalendar.add(5, 1);
    }
    if (localOnRangeSelectedListener != null) {
      localOnRangeSelectedListener.onRangeSelected(this, localArrayList);
    }
  }
  
  protected void dispatchRestoreInstanceState(@NonNull SparseArray<Parcelable> paramSparseArray)
  {
    dispatchThawSelfOnly(paramSparseArray);
  }
  
  protected void dispatchSaveInstanceState(@NonNull SparseArray<Parcelable> paramSparseArray)
  {
    dispatchFreezeSelfOnly(paramSparseArray);
  }
  
  protected LayoutParams generateDefaultLayoutParams()
  {
    return new LayoutParams(1);
  }
  
  protected ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
  {
    return new LayoutParams(1);
  }
  
  public LayoutParams generateLayoutParams(AttributeSet paramAttributeSet)
  {
    return new LayoutParams(1);
  }
  
  public int getArrowColor()
  {
    return this.arrowColor;
  }
  
  public CharSequence getCalendarContentDescription()
  {
    if (this.calendarContentDescription != null) {
      return this.calendarContentDescription;
    }
    return getContext().getString(R.string.calendar);
  }
  
  public CalendarDay getCurrentDate()
  {
    return this.adapter.getItem(this.pager.getCurrentItem());
  }
  
  public int getFirstDayOfWeek()
  {
    return this.firstDayOfWeek;
  }
  
  public Drawable getLeftArrowMask()
  {
    return this.leftArrowMask;
  }
  
  public CalendarDay getMaximumDate()
  {
    return this.maxDate;
  }
  
  public CalendarDay getMinimumDate()
  {
    return this.minDate;
  }
  
  public Drawable getRightArrowMask()
  {
    return this.rightArrowMask;
  }
  
  public CalendarDay getSelectedDate()
  {
    List localList = this.adapter.getSelectedDates();
    if (localList.isEmpty()) {
      return null;
    }
    return (CalendarDay)localList.get(localList.size() - 1);
  }
  
  @NonNull
  public List<CalendarDay> getSelectedDates()
  {
    return this.adapter.getSelectedDates();
  }
  
  public int getSelectionColor()
  {
    return this.accentColor;
  }
  
  @SelectionMode
  public int getSelectionMode()
  {
    return this.selectionMode;
  }
  
  @ShowOtherDates
  public int getShowOtherDates()
  {
    return this.adapter.getShowOtherDates();
  }
  
  public int getTileHeight()
  {
    return this.tileHeight;
  }
  
  @Deprecated
  public int getTileSize()
  {
    return Math.max(this.tileHeight, this.tileWidth);
  }
  
  public int getTileWidth()
  {
    return this.tileWidth;
  }
  
  public boolean getTopbarVisible()
  {
    return this.topbar.getVisibility() == 0;
  }
  
  public void goToNext()
  {
    if (canGoForward()) {
      this.pager.setCurrentItem(this.pager.getCurrentItem() + 1, true);
    }
  }
  
  public void goToPrevious()
  {
    if (canGoBack()) {
      this.pager.setCurrentItem(this.pager.getCurrentItem() - 1, true);
    }
  }
  
  public void invalidateDecorators()
  {
    this.adapter.invalidateDecorators();
  }
  
  public boolean isDynamicHeightEnabled()
  {
    return this.mDynamicHeightEnabled;
  }
  
  public boolean isPagingEnabled()
  {
    return this.pager.isPagingEnabled();
  }
  
  public StateBuilder newState()
  {
    return new StateBuilder();
  }
  
  protected void onDateClicked(@NonNull CalendarDay paramCalendarDay, boolean paramBoolean)
  {
    switch (this.selectionMode)
    {
    default: 
      this.adapter.clearSelections();
      this.adapter.setDateSelected(paramCalendarDay, true);
      dispatchOnDateSelected(paramCalendarDay, true);
      return;
    case 2: 
      this.adapter.setDateSelected(paramCalendarDay, paramBoolean);
      dispatchOnDateSelected(paramCalendarDay, paramBoolean);
      return;
    }
    this.adapter.setDateSelected(paramCalendarDay, paramBoolean);
    if (this.adapter.getSelectedDates().size() > 2)
    {
      this.adapter.clearSelections();
      this.adapter.setDateSelected(paramCalendarDay, paramBoolean);
      dispatchOnDateSelected(paramCalendarDay, paramBoolean);
      return;
    }
    if (this.adapter.getSelectedDates().size() == 2)
    {
      paramCalendarDay = this.adapter.getSelectedDates();
      if (((CalendarDay)paramCalendarDay.get(0)).isAfter((CalendarDay)paramCalendarDay.get(1)))
      {
        dispatchOnRangeSelected((CalendarDay)paramCalendarDay.get(1), (CalendarDay)paramCalendarDay.get(0));
        return;
      }
      dispatchOnRangeSelected((CalendarDay)paramCalendarDay.get(0), (CalendarDay)paramCalendarDay.get(1));
      return;
    }
    this.adapter.setDateSelected(paramCalendarDay, paramBoolean);
    dispatchOnDateSelected(paramCalendarDay, paramBoolean);
  }
  
  protected void onDateClicked(DayView paramDayView)
  {
    boolean bool2 = true;
    boolean bool1 = true;
    int i = getCurrentDate().getMonth();
    int j = paramDayView.getDate().getMonth();
    if (this.calendarMode == CalendarMode.MONTHS)
    {
      if ((this.allowClickDaysOutsideCurrentMonth) || (i == j))
      {
        if (i <= j) {
          break label75;
        }
        goToPrevious();
        localCalendarDay = paramDayView.getDate();
        if (paramDayView.isChecked()) {
          break label87;
        }
      }
      for (;;)
      {
        onDateClicked(localCalendarDay, bool1);
        return;
        label75:
        if (i >= j) {
          break;
        }
        goToNext();
        break;
        label87:
        bool1 = false;
      }
    }
    CalendarDay localCalendarDay = paramDayView.getDate();
    if (!paramDayView.isChecked()) {}
    for (bool1 = bool2;; bool1 = false)
    {
      onDateClicked(localCalendarDay, bool1);
      return;
    }
  }
  
  protected void onDateUnselected(CalendarDay paramCalendarDay)
  {
    dispatchOnDateSelected(paramCalendarDay, false);
  }
  
  public void onInitializeAccessibilityEvent(@NonNull AccessibilityEvent paramAccessibilityEvent)
  {
    super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
    paramAccessibilityEvent.setClassName(MaterialCalendarView.class.getName());
  }
  
  public void onInitializeAccessibilityNodeInfo(@NonNull AccessibilityNodeInfo paramAccessibilityNodeInfo)
  {
    super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
    paramAccessibilityNodeInfo.setClassName(MaterialCalendarView.class.getName());
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    int i = getChildCount();
    int j = getPaddingLeft();
    int k = getPaddingRight();
    paramInt4 = getPaddingTop();
    paramInt2 = 0;
    if (paramInt2 < i)
    {
      View localView = getChildAt(paramInt2);
      if (localView.getVisibility() == 8) {}
      for (;;)
      {
        paramInt2 += 1;
        break;
        int m = localView.getMeasuredWidth();
        int n = localView.getMeasuredHeight();
        int i1 = j + (paramInt3 - paramInt1 - j - k - m) / 2;
        localView.layout(i1, paramInt4, i1 + m, paramInt4 + n);
        paramInt4 += n;
      }
    }
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    int i = View.MeasureSpec.getSize(paramInt1);
    int j = View.MeasureSpec.getMode(paramInt1);
    int k = View.MeasureSpec.getSize(paramInt2);
    int i4 = View.MeasureSpec.getMode(paramInt2);
    int i2 = getPaddingLeft();
    int i3 = getPaddingRight();
    int m = getPaddingTop();
    int n = getPaddingBottom();
    int i1 = getWeekCountBasedOnMode();
    if (getTopbarVisible())
    {
      i1 += 1;
      i = (i - i2 - i3) / 7;
      i3 = (k - m - n) / i1;
      i2 = -1;
      m = -1;
      n = -1;
      if ((this.tileWidth <= 0) && (this.tileHeight <= 0)) {
        break label292;
      }
      if (this.tileWidth > 0) {
        m = this.tileWidth;
      }
      k = n;
      i = i2;
      j = m;
      if (this.tileHeight > 0)
      {
        k = this.tileHeight;
        j = m;
        i = i2;
      }
      label157:
      if (i <= 0) {
        break label370;
      }
      n = i;
      m = i;
    }
    for (;;)
    {
      i = getPaddingLeft();
      j = getPaddingRight();
      k = getPaddingTop();
      i2 = getPaddingBottom();
      setMeasuredDimension(clampSize(m * 7 + (i + j), paramInt1), clampSize(n * i1 + (k + i2), paramInt2));
      paramInt2 = getChildCount();
      paramInt1 = 0;
      while (paramInt1 < paramInt2)
      {
        View localView = getChildAt(paramInt1);
        LayoutParams localLayoutParams = (LayoutParams)localView.getLayoutParams();
        localView.measure(View.MeasureSpec.makeMeasureSpec(m * 7, 1073741824), View.MeasureSpec.makeMeasureSpec(localLayoutParams.height * n, 1073741824));
        paramInt1 += 1;
      }
      break;
      label292:
      if (j == 1073741824)
      {
        if (i4 == 1073741824)
        {
          i = Math.max(i, i3);
          k = n;
          j = m;
          break label157;
        }
        k = n;
        j = m;
        break label157;
      }
      k = n;
      i = i2;
      j = m;
      if (i4 != 1073741824) {
        break label157;
      }
      i = i3;
      k = n;
      j = m;
      break label157;
      label370:
      n = k;
      m = j;
      if (i <= 0)
      {
        i = j;
        if (j <= 0) {
          i = dpToPx(44);
        }
        n = k;
        m = i;
        if (k <= 0)
        {
          n = dpToPx(44);
          m = i;
        }
      }
    }
  }
  
  protected void onRestoreInstanceState(Parcelable paramParcelable)
  {
    paramParcelable = (SavedState)paramParcelable;
    super.onRestoreInstanceState(paramParcelable.getSuperState());
    newState().setFirstDayOfWeek(paramParcelable.firstDayOfWeek).setCalendarDisplayMode(paramParcelable.calendarMode).setMinimumDate(paramParcelable.minDate).setMaximumDate(paramParcelable.maxDate).commit();
    setSelectionColor(paramParcelable.color);
    setDateTextAppearance(paramParcelable.dateTextAppearance);
    setWeekDayTextAppearance(paramParcelable.weekDayTextAppearance);
    setShowOtherDates(paramParcelable.showOtherDates);
    setAllowClickDaysOutsideCurrentMonth(paramParcelable.allowClickDaysOutsideCurrentMonth);
    clearSelection();
    Iterator localIterator = paramParcelable.selectedDates.iterator();
    while (localIterator.hasNext()) {
      setDateSelected((CalendarDay)localIterator.next(), true);
    }
    setTileWidth(paramParcelable.tileWidthPx);
    setTileHeight(paramParcelable.tileHeightPx);
    setTopbarVisible(paramParcelable.topbarVisible);
    setSelectionMode(paramParcelable.selectionMode);
    setDynamicHeightEnabled(paramParcelable.dynamicHeightEnabled);
    setCurrentDate(paramParcelable.currentMonth);
  }
  
  protected Parcelable onSaveInstanceState()
  {
    SavedState localSavedState = new SavedState(super.onSaveInstanceState());
    localSavedState.color = getSelectionColor();
    localSavedState.dateTextAppearance = this.adapter.getDateTextAppearance();
    localSavedState.weekDayTextAppearance = this.adapter.getWeekDayTextAppearance();
    localSavedState.showOtherDates = getShowOtherDates();
    localSavedState.allowClickDaysOutsideCurrentMonth = allowClickDaysOutsideCurrentMonth();
    localSavedState.minDate = getMinimumDate();
    localSavedState.maxDate = getMaximumDate();
    localSavedState.selectedDates = getSelectedDates();
    localSavedState.firstDayOfWeek = getFirstDayOfWeek();
    localSavedState.selectionMode = getSelectionMode();
    localSavedState.tileWidthPx = getTileWidth();
    localSavedState.tileHeightPx = getTileHeight();
    localSavedState.topbarVisible = getTopbarVisible();
    localSavedState.calendarMode = this.calendarMode;
    localSavedState.currentMonth = this.currentMonth;
    return localSavedState;
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    return this.pager.dispatchTouchEvent(paramMotionEvent);
  }
  
  public void removeDecorator(DayViewDecorator paramDayViewDecorator)
  {
    this.dayViewDecorators.remove(paramDayViewDecorator);
    this.adapter.setDecorators(this.dayViewDecorators);
  }
  
  public void removeDecorators()
  {
    this.dayViewDecorators.clear();
    this.adapter.setDecorators(this.dayViewDecorators);
  }
  
  public void selectRange(CalendarDay paramCalendarDay1, CalendarDay paramCalendarDay2)
  {
    clearSelection();
    if (paramCalendarDay1.isAfter(paramCalendarDay2))
    {
      dispatchOnRangeSelected(paramCalendarDay2, paramCalendarDay1);
      return;
    }
    dispatchOnRangeSelected(paramCalendarDay1, paramCalendarDay2);
  }
  
  public void setAllowClickDaysOutsideCurrentMonth(boolean paramBoolean)
  {
    this.allowClickDaysOutsideCurrentMonth = paramBoolean;
  }
  
  public void setArrowColor(int paramInt)
  {
    if (paramInt == 0) {
      return;
    }
    this.arrowColor = paramInt;
    this.buttonPast.setColor(paramInt);
    this.buttonFuture.setColor(paramInt);
    invalidate();
  }
  
  public void setContentDescriptionArrowFuture(CharSequence paramCharSequence)
  {
    this.buttonFuture.setContentDescription(paramCharSequence);
  }
  
  public void setContentDescriptionArrowPast(CharSequence paramCharSequence)
  {
    this.buttonPast.setContentDescription(paramCharSequence);
  }
  
  public void setContentDescriptionCalendar(CharSequence paramCharSequence)
  {
    this.calendarContentDescription = paramCharSequence;
  }
  
  public void setCurrentDate(@Nullable CalendarDay paramCalendarDay)
  {
    setCurrentDate(paramCalendarDay, true);
  }
  
  public void setCurrentDate(@Nullable CalendarDay paramCalendarDay, boolean paramBoolean)
  {
    if (paramCalendarDay == null) {
      return;
    }
    int i = this.adapter.getIndexForDay(paramCalendarDay);
    this.pager.setCurrentItem(i, paramBoolean);
    updateUi();
  }
  
  public void setCurrentDate(@Nullable Calendar paramCalendar)
  {
    setCurrentDate(CalendarDay.from(paramCalendar));
  }
  
  public void setCurrentDate(@Nullable Date paramDate)
  {
    setCurrentDate(CalendarDay.from(paramDate));
  }
  
  public void setDateSelected(@Nullable CalendarDay paramCalendarDay, boolean paramBoolean)
  {
    if (paramCalendarDay == null) {
      return;
    }
    this.adapter.setDateSelected(paramCalendarDay, paramBoolean);
  }
  
  public void setDateSelected(@Nullable Calendar paramCalendar, boolean paramBoolean)
  {
    setDateSelected(CalendarDay.from(paramCalendar), paramBoolean);
  }
  
  public void setDateSelected(@Nullable Date paramDate, boolean paramBoolean)
  {
    setDateSelected(CalendarDay.from(paramDate), paramBoolean);
  }
  
  public void setDateTextAppearance(int paramInt)
  {
    this.adapter.setDateTextAppearance(paramInt);
  }
  
  public void setDayFormatter(DayFormatter paramDayFormatter)
  {
    CalendarPagerAdapter localCalendarPagerAdapter = this.adapter;
    DayFormatter localDayFormatter = paramDayFormatter;
    if (paramDayFormatter == null) {
      localDayFormatter = DayFormatter.DEFAULT;
    }
    localCalendarPagerAdapter.setDayFormatter(localDayFormatter);
  }
  
  public void setDynamicHeightEnabled(boolean paramBoolean)
  {
    this.mDynamicHeightEnabled = paramBoolean;
  }
  
  public void setHeaderTextAppearance(int paramInt)
  {
    this.title.setTextAppearance(getContext(), paramInt);
  }
  
  public void setLeftArrowMask(Drawable paramDrawable)
  {
    this.leftArrowMask = paramDrawable;
    this.buttonPast.setImageDrawable(paramDrawable);
  }
  
  public void setOnDateChangedListener(OnDateSelectedListener paramOnDateSelectedListener)
  {
    this.listener = paramOnDateSelectedListener;
  }
  
  public void setOnMonthChangedListener(OnMonthChangedListener paramOnMonthChangedListener)
  {
    this.monthListener = paramOnMonthChangedListener;
  }
  
  public void setOnRangeSelectedListener(OnRangeSelectedListener paramOnRangeSelectedListener)
  {
    this.rangeListener = paramOnRangeSelectedListener;
  }
  
  public void setPagingEnabled(boolean paramBoolean)
  {
    this.pager.setPagingEnabled(paramBoolean);
    updateUi();
  }
  
  public void setRightArrowMask(Drawable paramDrawable)
  {
    this.rightArrowMask = paramDrawable;
    this.buttonFuture.setImageDrawable(paramDrawable);
  }
  
  public void setSelectedDate(@Nullable CalendarDay paramCalendarDay)
  {
    clearSelection();
    if (paramCalendarDay != null) {
      setDateSelected(paramCalendarDay, true);
    }
  }
  
  public void setSelectedDate(@Nullable Calendar paramCalendar)
  {
    setSelectedDate(CalendarDay.from(paramCalendar));
  }
  
  public void setSelectedDate(@Nullable Date paramDate)
  {
    setSelectedDate(CalendarDay.from(paramDate));
  }
  
  public void setSelectionColor(int paramInt)
  {
    int i = paramInt;
    if (paramInt == 0)
    {
      if (!isInEditMode()) {
        return;
      }
      i = -7829368;
    }
    this.accentColor = i;
    this.adapter.setSelectionColor(i);
    invalidate();
  }
  
  public void setSelectionMode(@SelectionMode int paramInt)
  {
    boolean bool = false;
    int i = this.selectionMode;
    this.selectionMode = paramInt;
    switch (paramInt)
    {
    default: 
      this.selectionMode = 0;
      if (i != 0) {
        clearSelection();
      }
      break;
    }
    for (;;)
    {
      CalendarPagerAdapter localCalendarPagerAdapter = this.adapter;
      if (this.selectionMode != 0) {
        bool = true;
      }
      localCalendarPagerAdapter.setSelectionEnabled(bool);
      return;
      clearSelection();
      continue;
      if (((i == 2) || (i == 3)) && (!getSelectedDates().isEmpty())) {
        setSelectedDate(getSelectedDate());
      }
    }
  }
  
  public void setShowOtherDates(@ShowOtherDates int paramInt)
  {
    this.adapter.setShowOtherDates(paramInt);
  }
  
  public void setTileHeight(int paramInt)
  {
    this.tileHeight = paramInt;
    requestLayout();
  }
  
  public void setTileHeightDp(int paramInt)
  {
    setTileHeight(dpToPx(paramInt));
  }
  
  public void setTileSize(int paramInt)
  {
    this.tileWidth = paramInt;
    this.tileHeight = paramInt;
    requestLayout();
  }
  
  public void setTileSizeDp(int paramInt)
  {
    setTileSize(dpToPx(paramInt));
  }
  
  public void setTileWidth(int paramInt)
  {
    this.tileWidth = paramInt;
    requestLayout();
  }
  
  public void setTileWidthDp(int paramInt)
  {
    setTileWidth(dpToPx(paramInt));
  }
  
  public void setTitleFormatter(TitleFormatter paramTitleFormatter)
  {
    TitleFormatter localTitleFormatter = paramTitleFormatter;
    if (paramTitleFormatter == null) {
      localTitleFormatter = DEFAULT_TITLE_FORMATTER;
    }
    this.titleChanger.setTitleFormatter(localTitleFormatter);
    this.adapter.setTitleFormatter(localTitleFormatter);
    updateUi();
  }
  
  public void setTitleMonths(@ArrayRes int paramInt)
  {
    setTitleMonths(getResources().getTextArray(paramInt));
  }
  
  public void setTitleMonths(CharSequence[] paramArrayOfCharSequence)
  {
    setTitleFormatter(new MonthArrayTitleFormatter(paramArrayOfCharSequence));
  }
  
  public void setTopbarVisible(boolean paramBoolean)
  {
    LinearLayout localLinearLayout = this.topbar;
    if (paramBoolean) {}
    for (int i = 0;; i = 8)
    {
      localLinearLayout.setVisibility(i);
      requestLayout();
      return;
    }
  }
  
  public void setWeekDayFormatter(WeekDayFormatter paramWeekDayFormatter)
  {
    CalendarPagerAdapter localCalendarPagerAdapter = this.adapter;
    WeekDayFormatter localWeekDayFormatter = paramWeekDayFormatter;
    if (paramWeekDayFormatter == null) {
      localWeekDayFormatter = WeekDayFormatter.DEFAULT;
    }
    localCalendarPagerAdapter.setWeekDayFormatter(localWeekDayFormatter);
  }
  
  public void setWeekDayLabels(@ArrayRes int paramInt)
  {
    setWeekDayLabels(getResources().getTextArray(paramInt));
  }
  
  public void setWeekDayLabels(CharSequence[] paramArrayOfCharSequence)
  {
    setWeekDayFormatter(new ArrayWeekDayFormatter(paramArrayOfCharSequence));
  }
  
  public void setWeekDayTextAppearance(int paramInt)
  {
    this.adapter.setWeekDayTextAppearance(paramInt);
  }
  
  public boolean shouldDelayChildPressedState()
  {
    return false;
  }
  
  public State state()
  {
    return this.state;
  }
  
  protected static class LayoutParams
    extends ViewGroup.MarginLayoutParams
  {
    public LayoutParams(int paramInt)
    {
      super(paramInt);
    }
  }
  
  public static class SavedState
    extends View.BaseSavedState
  {
    public static final Parcelable.Creator<SavedState> CREATOR = new MaterialCalendarView.SavedState.1();
    boolean allowClickDaysOutsideCurrentMonth = true;
    CalendarMode calendarMode = CalendarMode.MONTHS;
    int color = 0;
    CalendarDay currentMonth = null;
    int dateTextAppearance = 0;
    boolean dynamicHeightEnabled = false;
    int firstDayOfWeek = 1;
    CalendarDay maxDate = null;
    CalendarDay minDate = null;
    List<CalendarDay> selectedDates = new ArrayList();
    int selectionMode = 1;
    int showOtherDates = 4;
    int tileHeightPx = -1;
    int tileWidthPx = -1;
    boolean topbarVisible = true;
    int weekDayTextAppearance = 0;
    
    private SavedState(Parcel paramParcel)
    {
      super();
      this.color = paramParcel.readInt();
      this.dateTextAppearance = paramParcel.readInt();
      this.weekDayTextAppearance = paramParcel.readInt();
      this.showOtherDates = paramParcel.readInt();
      boolean bool1;
      ClassLoader localClassLoader;
      if (paramParcel.readByte() != 0)
      {
        bool1 = true;
        this.allowClickDaysOutsideCurrentMonth = bool1;
        localClassLoader = CalendarDay.class.getClassLoader();
        this.minDate = ((CalendarDay)paramParcel.readParcelable(localClassLoader));
        this.maxDate = ((CalendarDay)paramParcel.readParcelable(localClassLoader));
        paramParcel.readTypedList(this.selectedDates, CalendarDay.CREATOR);
        this.firstDayOfWeek = paramParcel.readInt();
        this.tileWidthPx = paramParcel.readInt();
        this.tileHeightPx = paramParcel.readInt();
        if (paramParcel.readInt() != 1) {
          break label287;
        }
        bool1 = true;
        label219:
        this.topbarVisible = bool1;
        this.selectionMode = paramParcel.readInt();
        bool1 = bool2;
        if (paramParcel.readInt() == 1) {
          bool1 = true;
        }
        this.dynamicHeightEnabled = bool1;
        if (paramParcel.readInt() != 1) {
          break label292;
        }
      }
      label287:
      label292:
      for (CalendarMode localCalendarMode = CalendarMode.WEEKS;; localCalendarMode = CalendarMode.MONTHS)
      {
        this.calendarMode = localCalendarMode;
        this.currentMonth = ((CalendarDay)paramParcel.readParcelable(localClassLoader));
        return;
        bool1 = false;
        break;
        bool1 = false;
        break label219;
      }
    }
    
    SavedState(Parcelable paramParcelable)
    {
      super();
    }
    
    public void writeToParcel(@NonNull Parcel paramParcel, int paramInt)
    {
      int i = 1;
      super.writeToParcel(paramParcel, paramInt);
      paramParcel.writeInt(this.color);
      paramParcel.writeInt(this.dateTextAppearance);
      paramParcel.writeInt(this.weekDayTextAppearance);
      paramParcel.writeInt(this.showOtherDates);
      if (this.allowClickDaysOutsideCurrentMonth)
      {
        paramInt = 1;
        paramParcel.writeByte((byte)paramInt);
        paramParcel.writeParcelable(this.minDate, 0);
        paramParcel.writeParcelable(this.maxDate, 0);
        paramParcel.writeTypedList(this.selectedDates);
        paramParcel.writeInt(this.firstDayOfWeek);
        paramParcel.writeInt(this.tileWidthPx);
        paramParcel.writeInt(this.tileHeightPx);
        if (!this.topbarVisible) {
          break label173;
        }
        paramInt = 1;
        label114:
        paramParcel.writeInt(paramInt);
        paramParcel.writeInt(this.selectionMode);
        if (!this.dynamicHeightEnabled) {
          break label178;
        }
        paramInt = 1;
        label136:
        paramParcel.writeInt(paramInt);
        if (this.calendarMode != CalendarMode.WEEKS) {
          break label183;
        }
      }
      label173:
      label178:
      label183:
      for (paramInt = i;; paramInt = 0)
      {
        paramParcel.writeInt(paramInt);
        paramParcel.writeParcelable(this.currentMonth, 0);
        return;
        paramInt = 0;
        break;
        paramInt = 0;
        break label114;
        paramInt = 0;
        break label136;
      }
    }
  }
  
  @Retention(RetentionPolicy.RUNTIME)
  public static @interface SelectionMode {}
  
  @Retention(RetentionPolicy.RUNTIME)
  @SuppressLint({"UniqueConstants"})
  public static @interface ShowOtherDates {}
  
  public class State
  {
    public final CalendarMode calendarMode;
    public final int firstDayOfWeek;
    public final CalendarDay maxDate;
    public final CalendarDay minDate;
    
    public State(MaterialCalendarView.StateBuilder paramStateBuilder)
    {
      this.calendarMode = MaterialCalendarView.StateBuilder.access$800(paramStateBuilder);
      this.firstDayOfWeek = MaterialCalendarView.StateBuilder.access$900(paramStateBuilder);
      this.minDate = paramStateBuilder.minDate;
      this.maxDate = paramStateBuilder.maxDate;
    }
    
    public MaterialCalendarView.StateBuilder edit()
    {
      return new MaterialCalendarView.StateBuilder(MaterialCalendarView.this, this, null);
    }
  }
  
  public class StateBuilder
  {
    private CalendarMode calendarMode = CalendarMode.MONTHS;
    private int firstDayOfWeek = Calendar.getInstance().getFirstDayOfWeek();
    public CalendarDay maxDate = null;
    public CalendarDay minDate = null;
    
    public StateBuilder() {}
    
    private StateBuilder(MaterialCalendarView.State paramState)
    {
      this.calendarMode = paramState.calendarMode;
      this.firstDayOfWeek = paramState.firstDayOfWeek;
      this.minDate = paramState.minDate;
      this.maxDate = paramState.maxDate;
    }
    
    public void commit()
    {
      MaterialCalendarView.this.commit(new MaterialCalendarView.State(MaterialCalendarView.this, this));
    }
    
    public StateBuilder setCalendarDisplayMode(CalendarMode paramCalendarMode)
    {
      this.calendarMode = paramCalendarMode;
      return this;
    }
    
    public StateBuilder setFirstDayOfWeek(int paramInt)
    {
      this.firstDayOfWeek = paramInt;
      return this;
    }
    
    public StateBuilder setMaximumDate(@Nullable CalendarDay paramCalendarDay)
    {
      this.maxDate = paramCalendarDay;
      return this;
    }
    
    public StateBuilder setMaximumDate(@Nullable Calendar paramCalendar)
    {
      setMaximumDate(CalendarDay.from(paramCalendar));
      return this;
    }
    
    public StateBuilder setMaximumDate(@Nullable Date paramDate)
    {
      setMaximumDate(CalendarDay.from(paramDate));
      return this;
    }
    
    public StateBuilder setMinimumDate(@Nullable CalendarDay paramCalendarDay)
    {
      this.minDate = paramCalendarDay;
      return this;
    }
    
    public StateBuilder setMinimumDate(@Nullable Calendar paramCalendar)
    {
      setMinimumDate(CalendarDay.from(paramCalendar));
      return this;
    }
    
    public StateBuilder setMinimumDate(@Nullable Date paramDate)
    {
      setMinimumDate(CalendarDay.from(paramDate));
      return this;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/prolificinteractive/materialcalendarview/MaterialCalendarView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */