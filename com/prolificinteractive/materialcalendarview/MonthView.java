package com.prolificinteractive.materialcalendarview;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import java.util.Calendar;
import java.util.Collection;

@SuppressLint({"ViewConstructor"})
class MonthView
  extends CalendarPagerView
{
  public MonthView(@NonNull MaterialCalendarView paramMaterialCalendarView, CalendarDay paramCalendarDay, int paramInt)
  {
    super(paramMaterialCalendarView, paramCalendarDay, paramInt);
  }
  
  protected void buildDayViews(Collection<DayView> paramCollection, Calendar paramCalendar)
  {
    int i = 0;
    while (i < 6)
    {
      int j = 0;
      while (j < 7)
      {
        addDayView(paramCollection, paramCalendar);
        j += 1;
      }
      i += 1;
    }
  }
  
  public CalendarDay getMonth()
  {
    return getFirstViewDay();
  }
  
  protected int getRows()
  {
    return 7;
  }
  
  protected boolean isDayEnabled(CalendarDay paramCalendarDay)
  {
    return paramCalendarDay.getMonth() == getFirstViewDay().getMonth();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/prolificinteractive/materialcalendarview/MonthView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */