package com.prolificinteractive.materialcalendarview;

import android.support.annotation.NonNull;

public abstract interface OnDateSelectedListener
{
  public abstract void onDateSelected(@NonNull MaterialCalendarView paramMaterialCalendarView, @NonNull CalendarDay paramCalendarDay, boolean paramBoolean);
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/prolificinteractive/materialcalendarview/OnDateSelectedListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */