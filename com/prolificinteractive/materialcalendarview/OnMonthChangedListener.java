package com.prolificinteractive.materialcalendarview;

public abstract interface OnMonthChangedListener
{
  public abstract void onMonthChanged(MaterialCalendarView paramMaterialCalendarView, CalendarDay paramCalendarDay);
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/prolificinteractive/materialcalendarview/OnMonthChangedListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */