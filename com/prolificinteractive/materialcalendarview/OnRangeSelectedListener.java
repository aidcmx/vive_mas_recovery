package com.prolificinteractive.materialcalendarview;

import android.support.annotation.NonNull;
import java.util.List;

public abstract interface OnRangeSelectedListener
{
  public abstract void onRangeSelected(@NonNull MaterialCalendarView paramMaterialCalendarView, @NonNull List<CalendarDay> paramList);
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/prolificinteractive/materialcalendarview/OnRangeSelectedListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */