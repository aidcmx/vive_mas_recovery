package com.prolificinteractive.materialcalendarview;

import android.content.res.Resources;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.ViewPropertyAnimator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.TextView;
import com.prolificinteractive.materialcalendarview.format.TitleFormatter;

class TitleChanger
{
  public static final int DEFAULT_ANIMATION_DELAY = 400;
  public static final int DEFAULT_Y_TRANSLATION_DP = 20;
  private final int animDelay;
  private final int animDuration;
  private final Interpolator interpolator = new DecelerateInterpolator(2.0F);
  private long lastAnimTime = 0L;
  private CalendarDay previousMonth = null;
  private final TextView title;
  private TitleFormatter titleFormatter;
  private final int yTranslate;
  
  public TitleChanger(TextView paramTextView)
  {
    this.title = paramTextView;
    paramTextView = paramTextView.getResources();
    this.animDelay = 400;
    this.animDuration = (paramTextView.getInteger(17694720) / 2);
    this.yTranslate = ((int)TypedValue.applyDimension(1, 20.0F, paramTextView.getDisplayMetrics()));
  }
  
  private void doChange(long paramLong, CalendarDay paramCalendarDay, boolean paramBoolean)
  {
    this.title.animate().cancel();
    this.title.setTranslationY(0.0F);
    this.title.setAlpha(1.0F);
    this.lastAnimTime = paramLong;
    CharSequence localCharSequence = this.titleFormatter.format(paramCalendarDay);
    if (!paramBoolean)
    {
      this.title.setText(localCharSequence);
      this.previousMonth = paramCalendarDay;
      return;
    }
    int j = this.yTranslate;
    if (this.previousMonth.isBefore(paramCalendarDay)) {}
    for (int i = 1;; i = -1)
    {
      i = j * i;
      this.title.animate().translationY(i * -1).alpha(0.0F).setDuration(this.animDuration).setInterpolator(this.interpolator).setListener(new TitleChanger.1(this, localCharSequence, i)).start();
      break;
    }
  }
  
  public void change(CalendarDay paramCalendarDay)
  {
    long l = System.currentTimeMillis();
    if (paramCalendarDay == null) {}
    do
    {
      return;
      if ((TextUtils.isEmpty(this.title.getText())) || (l - this.lastAnimTime < this.animDelay)) {
        doChange(l, paramCalendarDay, false);
      }
    } while ((paramCalendarDay.equals(this.previousMonth)) || (paramCalendarDay.getMonth() == this.previousMonth.getMonth()));
    doChange(l, paramCalendarDay, true);
  }
  
  public TitleFormatter getTitleFormatter()
  {
    return this.titleFormatter;
  }
  
  public void setPreviousMonth(CalendarDay paramCalendarDay)
  {
    this.previousMonth = paramCalendarDay;
  }
  
  public void setTitleFormatter(TitleFormatter paramTitleFormatter)
  {
    this.titleFormatter = paramTitleFormatter;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/prolificinteractive/materialcalendarview/TitleChanger.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */