package com.prolificinteractive.materialcalendarview.format;

public class ArrayWeekDayFormatter
  implements WeekDayFormatter
{
  private final CharSequence[] weekDayLabels;
  
  public ArrayWeekDayFormatter(CharSequence[] paramArrayOfCharSequence)
  {
    if (paramArrayOfCharSequence == null) {
      throw new IllegalArgumentException("Cannot be null");
    }
    if (paramArrayOfCharSequence.length != 7) {
      throw new IllegalArgumentException("Array must contain exactly 7 elements");
    }
    this.weekDayLabels = paramArrayOfCharSequence;
  }
  
  public CharSequence format(int paramInt)
  {
    return this.weekDayLabels[(paramInt - 1)];
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/prolificinteractive/materialcalendarview/format/ArrayWeekDayFormatter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */