package com.prolificinteractive.materialcalendarview.format;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class DateFormatTitleFormatter
  implements TitleFormatter
{
  private final DateFormat dateFormat;
  
  public DateFormatTitleFormatter()
  {
    this.dateFormat = new SimpleDateFormat("LLLL yyyy", Locale.getDefault());
  }
  
  public DateFormatTitleFormatter(DateFormat paramDateFormat)
  {
    this.dateFormat = paramDateFormat;
  }
  
  public CharSequence format(CalendarDay paramCalendarDay)
  {
    return this.dateFormat.format(paramCalendarDay.getDate());
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/prolificinteractive/materialcalendarview/format/DateFormatTitleFormatter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */