package com.prolificinteractive.materialcalendarview.format;

import android.support.annotation.NonNull;
import com.prolificinteractive.materialcalendarview.CalendarDay;

public abstract interface DayFormatter
{
  public static final DayFormatter DEFAULT = new DateFormatDayFormatter();
  
  @NonNull
  public abstract String format(@NonNull CalendarDay paramCalendarDay);
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/prolificinteractive/materialcalendarview/format/DayFormatter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */