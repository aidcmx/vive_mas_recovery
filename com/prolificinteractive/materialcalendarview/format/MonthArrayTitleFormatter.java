package com.prolificinteractive.materialcalendarview.format;

import android.text.SpannableStringBuilder;
import com.prolificinteractive.materialcalendarview.CalendarDay;

public class MonthArrayTitleFormatter
  implements TitleFormatter
{
  private final CharSequence[] monthLabels;
  
  public MonthArrayTitleFormatter(CharSequence[] paramArrayOfCharSequence)
  {
    if (paramArrayOfCharSequence == null) {
      throw new IllegalArgumentException("Label array cannot be null");
    }
    if (paramArrayOfCharSequence.length < 12) {
      throw new IllegalArgumentException("Label array is too short");
    }
    this.monthLabels = paramArrayOfCharSequence;
  }
  
  public CharSequence format(CalendarDay paramCalendarDay)
  {
    return new SpannableStringBuilder().append(this.monthLabels[paramCalendarDay.getMonth()]).append(" ").append(String.valueOf(paramCalendarDay.getYear()));
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/prolificinteractive/materialcalendarview/format/MonthArrayTitleFormatter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */