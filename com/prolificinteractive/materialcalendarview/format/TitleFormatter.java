package com.prolificinteractive.materialcalendarview.format;

import com.prolificinteractive.materialcalendarview.CalendarDay;

public abstract interface TitleFormatter
{
  public abstract CharSequence format(CalendarDay paramCalendarDay);
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/prolificinteractive/materialcalendarview/format/TitleFormatter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */