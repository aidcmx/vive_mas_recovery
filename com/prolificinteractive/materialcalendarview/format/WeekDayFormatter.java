package com.prolificinteractive.materialcalendarview.format;

import com.prolificinteractive.materialcalendarview.CalendarUtils;

public abstract interface WeekDayFormatter
{
  public static final WeekDayFormatter DEFAULT = new CalendarWeekDayFormatter(CalendarUtils.getInstance());
  
  public abstract CharSequence format(int paramInt);
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/prolificinteractive/materialcalendarview/format/WeekDayFormatter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */