package com.seatgeek.placesautocomplete;

import android.support.annotation.NonNull;
import com.seatgeek.placesautocomplete.model.Place;

public abstract interface OnPlaceSelectedListener
{
  public abstract void onPlaceSelected(@NonNull Place paramPlace);
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/seatgeek/placesautocomplete/OnPlaceSelectedListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */