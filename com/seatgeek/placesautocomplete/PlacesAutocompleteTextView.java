package com.seatgeek.placesautocomplete;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.InflateException;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.AutoCompleteTextView;
import android.widget.Filterable;
import android.widget.ListAdapter;
import com.seatgeek.placesautocomplete.adapter.AbstractPlacesAutocompleteAdapter;
import com.seatgeek.placesautocomplete.adapter.DefaultAutocompleteAdapter;
import com.seatgeek.placesautocomplete.async.BackgroundExecutorService;
import com.seatgeek.placesautocomplete.history.AutocompleteHistoryManager;
import com.seatgeek.placesautocomplete.history.DefaultAutocompleteHistoryManager;
import com.seatgeek.placesautocomplete.model.AutocompleteResultType;
import com.seatgeek.placesautocomplete.model.Place;
import com.seatgeek.placesautocomplete.network.PlacesHttpClientResolver;

public class PlacesAutocompleteTextView
  extends AutoCompleteTextView
{
  public static final boolean DEBUG = true;
  @NonNull
  private AbstractPlacesAutocompleteAdapter adapter;
  @NonNull
  private PlacesApi api;
  private boolean completionEnabled = true;
  @Nullable
  private AutocompleteHistoryManager historyManager;
  @Nullable
  private String languageCode;
  @Nullable
  private OnPlaceSelectedListener listener;
  @Nullable
  private AutocompleteResultType resultType;
  
  public PlacesAutocompleteTextView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    init(paramContext, paramAttributeSet, R.attr.pacv_placesAutoCompleteTextViewStyle, R.style.PACV_Widget_PlacesAutoCompleteTextView, null, paramContext.getString(R.string.pacv_default_history_file_name));
  }
  
  public PlacesAutocompleteTextView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    init(paramContext, paramAttributeSet, paramInt, R.style.PACV_Widget_PlacesAutoCompleteTextView, null, paramContext.getString(R.string.pacv_default_history_file_name));
  }
  
  @TargetApi(21)
  public PlacesAutocompleteTextView(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2)
  {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    init(paramContext, paramAttributeSet, paramInt1, paramInt2, null, paramContext.getString(R.string.pacv_default_history_file_name));
  }
  
  public PlacesAutocompleteTextView(@NonNull Context paramContext, @NonNull String paramString)
  {
    super(paramContext);
    init(paramContext, null, R.attr.pacv_placesAutoCompleteTextViewStyle, R.style.PACV_Widget_PlacesAutoCompleteTextView, paramString, paramContext.getString(R.string.pacv_default_history_file_name));
  }
  
  public PlacesAutocompleteTextView(@NonNull Context paramContext, @NonNull String paramString1, @NonNull String paramString2)
  {
    super(paramContext);
    init(paramContext, null, R.attr.pacv_placesAutoCompleteTextViewStyle, R.style.PACV_Widget_PlacesAutoCompleteTextView, paramString1, paramString2);
  }
  
  /* Error */
  private AbstractPlacesAutocompleteAdapter adapterForClass(Context paramContext, String paramString)
  {
    // Byte code:
    //   0: aload_2
    //   1: invokestatic 107	java/lang/Class:forName	(Ljava/lang/String;)Ljava/lang/Class;
    //   4: astore_3
    //   5: aload_3
    //   6: iconst_4
    //   7: anewarray 103	java/lang/Class
    //   10: dup
    //   11: iconst_0
    //   12: ldc 46
    //   14: aastore
    //   15: dup
    //   16: iconst_1
    //   17: ldc 109
    //   19: aastore
    //   20: dup
    //   21: iconst_2
    //   22: ldc 111
    //   24: aastore
    //   25: dup
    //   26: iconst_3
    //   27: ldc 113
    //   29: aastore
    //   30: invokevirtual 117	java/lang/Class:getConstructor	([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;
    //   33: astore_3
    //   34: aload_3
    //   35: iconst_4
    //   36: anewarray 119	java/lang/Object
    //   39: dup
    //   40: iconst_0
    //   41: aload_1
    //   42: aastore
    //   43: dup
    //   44: iconst_1
    //   45: aload_0
    //   46: getfield 87	com/seatgeek/placesautocomplete/PlacesAutocompleteTextView:api	Lcom/seatgeek/placesautocomplete/PlacesApi;
    //   49: aastore
    //   50: dup
    //   51: iconst_2
    //   52: aload_0
    //   53: getfield 121	com/seatgeek/placesautocomplete/PlacesAutocompleteTextView:resultType	Lcom/seatgeek/placesautocomplete/model/AutocompleteResultType;
    //   56: aastore
    //   57: dup
    //   58: iconst_3
    //   59: aload_0
    //   60: getfield 83	com/seatgeek/placesautocomplete/PlacesAutocompleteTextView:historyManager	Lcom/seatgeek/placesautocomplete/history/AutocompleteHistoryManager;
    //   63: aastore
    //   64: invokevirtual 127	java/lang/reflect/Constructor:newInstance	([Ljava/lang/Object;)Ljava/lang/Object;
    //   67: checkcast 129	com/seatgeek/placesautocomplete/adapter/AbstractPlacesAutocompleteAdapter
    //   70: astore_1
    //   71: aload_1
    //   72: areturn
    //   73: astore_1
    //   74: new 131	android/view/InflateException
    //   77: dup
    //   78: new 133	java/lang/StringBuilder
    //   81: dup
    //   82: invokespecial 136	java/lang/StringBuilder:<init>	()V
    //   85: ldc -118
    //   87: invokevirtual 142	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   90: aload_2
    //   91: invokevirtual 142	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   94: invokevirtual 146	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   97: aload_1
    //   98: invokespecial 149	android/view/InflateException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   101: athrow
    //   102: astore_1
    //   103: new 131	android/view/InflateException
    //   106: dup
    //   107: new 133	java/lang/StringBuilder
    //   110: dup
    //   111: invokespecial 136	java/lang/StringBuilder:<init>	()V
    //   114: aload_2
    //   115: invokevirtual 142	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   118: ldc -105
    //   120: invokevirtual 142	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   123: ldc -127
    //   125: invokevirtual 154	java/lang/Class:getSimpleName	()Ljava/lang/String;
    //   128: invokevirtual 142	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   131: invokevirtual 146	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   134: aload_1
    //   135: invokespecial 149	android/view/InflateException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   138: athrow
    //   139: astore_1
    //   140: new 131	android/view/InflateException
    //   143: dup
    //   144: new 133	java/lang/StringBuilder
    //   147: dup
    //   148: invokespecial 136	java/lang/StringBuilder:<init>	()V
    //   151: ldc -100
    //   153: invokevirtual 142	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   156: ldc 46
    //   158: invokevirtual 154	java/lang/Class:getSimpleName	()Ljava/lang/String;
    //   161: invokevirtual 142	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   164: ldc -98
    //   166: invokevirtual 142	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   169: ldc 109
    //   171: invokevirtual 154	java/lang/Class:getSimpleName	()Ljava/lang/String;
    //   174: invokevirtual 142	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   177: ldc -98
    //   179: invokevirtual 142	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   182: ldc 111
    //   184: invokevirtual 154	java/lang/Class:getSimpleName	()Ljava/lang/String;
    //   187: invokevirtual 142	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   190: ldc -96
    //   192: invokevirtual 142	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   195: ldc 113
    //   197: invokevirtual 154	java/lang/Class:getSimpleName	()Ljava/lang/String;
    //   200: invokevirtual 142	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   203: ldc -94
    //   205: invokevirtual 142	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   208: aload_2
    //   209: invokevirtual 142	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   212: invokevirtual 146	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   215: aload_1
    //   216: invokespecial 149	android/view/InflateException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   219: athrow
    //   220: astore_1
    //   221: new 131	android/view/InflateException
    //   224: dup
    //   225: new 133	java/lang/StringBuilder
    //   228: dup
    //   229: invokespecial 136	java/lang/StringBuilder:<init>	()V
    //   232: ldc -92
    //   234: invokevirtual 142	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   237: aload_2
    //   238: invokevirtual 142	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   241: invokevirtual 146	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   244: aload_1
    //   245: invokespecial 149	android/view/InflateException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   248: athrow
    //   249: astore_1
    //   250: new 131	android/view/InflateException
    //   253: dup
    //   254: new 133	java/lang/StringBuilder
    //   257: dup
    //   258: invokespecial 136	java/lang/StringBuilder:<init>	()V
    //   261: ldc -92
    //   263: invokevirtual 142	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   266: aload_2
    //   267: invokevirtual 142	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   270: invokevirtual 146	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   273: aload_1
    //   274: invokespecial 149	android/view/InflateException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   277: athrow
    //   278: astore_1
    //   279: new 131	android/view/InflateException
    //   282: dup
    //   283: new 133	java/lang/StringBuilder
    //   286: dup
    //   287: invokespecial 136	java/lang/StringBuilder:<init>	()V
    //   290: ldc -92
    //   292: invokevirtual 142	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   295: aload_2
    //   296: invokevirtual 142	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   299: invokevirtual 146	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   302: aload_1
    //   303: invokespecial 149	android/view/InflateException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   306: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	307	0	this	PlacesAutocompleteTextView
    //   0	307	1	paramContext	Context
    //   0	307	2	paramString	String
    //   4	31	3	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   0	5	73	java/lang/ClassNotFoundException
    //   0	5	102	java/lang/ClassCastException
    //   5	34	139	java/lang/NoSuchMethodException
    //   34	71	220	java/lang/InstantiationException
    //   34	71	249	java/lang/IllegalAccessException
    //   34	71	278	java/lang/reflect/InvocationTargetException
  }
  
  private void init(@NonNull Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2, String paramString1, String paramString2)
  {
    TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.PlacesAutocompleteTextView, paramInt1, paramInt2);
    paramAttributeSet = localTypedArray.getString(R.styleable.PlacesAutocompleteTextView_pacv_googleMapsApiKey);
    String str2 = localTypedArray.getString(R.styleable.PlacesAutocompleteTextView_pacv_adapterClass);
    String str1 = localTypedArray.getString(R.styleable.PlacesAutocompleteTextView_pacv_historyFile);
    this.languageCode = localTypedArray.getString(R.styleable.PlacesAutocompleteTextView_pacv_languageCode);
    this.resultType = AutocompleteResultType.fromEnum(localTypedArray.getInt(R.styleable.PlacesAutocompleteTextView_pacv_resultType, PlacesApi.DEFAULT_RESULT_TYPE.ordinal()));
    localTypedArray.recycle();
    if (paramString2 != null)
    {
      if (!TextUtils.isEmpty(paramString2)) {
        this.historyManager = DefaultAutocompleteHistoryManager.fromPath(paramContext, paramString2);
      }
      if (paramString1 == null) {
        break label133;
      }
    }
    for (;;)
    {
      if (!TextUtils.isEmpty(paramString1)) {
        break label139;
      }
      throw new InflateException("Did not specify googleApiKey!");
      paramString2 = str1;
      break;
      label133:
      paramString1 = paramAttributeSet;
    }
    label139:
    this.api = new PlacesApiBuilder().setApiClient(PlacesHttpClientResolver.PLACES_HTTP_CLIENT).setGoogleApiKey(paramString1).build();
    if (this.languageCode != null) {
      this.api.setLanguageCode(this.languageCode);
    }
    if (str2 != null) {}
    for (this.adapter = adapterForClass(paramContext, str2);; this.adapter = new DefaultAutocompleteAdapter(paramContext, this.api, this.resultType, this.historyManager))
    {
      super.setAdapter(this.adapter);
      super.setOnItemClickListener(new PlacesAutocompleteTextView.1(this));
      super.setDropDownBackgroundResource(R.drawable.pacv_popup_background_white);
      return;
    }
  }
  
  protected CharSequence convertSelectionToString(Object paramObject)
  {
    return ((Place)paramObject).description;
  }
  
  public boolean enoughToFilter()
  {
    return (this.completionEnabled) && ((this.historyManager != null) || (super.enoughToFilter()));
  }
  
  @NonNull
  public PlacesApi getApi()
  {
    return this.api;
  }
  
  @NonNull
  public AbstractPlacesAutocompleteAdapter getAutocompleteAdapter()
  {
    return this.adapter;
  }
  
  @Nullable
  public Location getCurrentLocation()
  {
    return this.api.getCurrentLocation();
  }
  
  public void getDetailsFor(Place paramPlace, DetailsCallback paramDetailsCallback)
  {
    BackgroundExecutorService.INSTANCE.enqueue(new PlacesAutocompleteTextView.2(this, paramPlace, paramDetailsCallback));
  }
  
  @Nullable
  public AutocompleteHistoryManager getHistoryManager()
  {
    return this.historyManager;
  }
  
  @Nullable
  public String getLanguageCode()
  {
    return this.languageCode;
  }
  
  @Nullable
  public Long getRadiusMeters()
  {
    return this.api.getRadiusMeters();
  }
  
  @Nullable
  public AutocompleteResultType getResultType()
  {
    return this.resultType;
  }
  
  public boolean isLocationBiasEnabled()
  {
    return this.api.isLocationBiasEnabled();
  }
  
  public void performCompletion()
  {
    if (!this.completionEnabled) {
      return;
    }
    super.performCompletion();
  }
  
  protected void performFiltering(CharSequence paramCharSequence, int paramInt)
  {
    if ((paramCharSequence == null) || (paramCharSequence.length() <= getThreshold()))
    {
      Object localObject = paramCharSequence;
      if (paramCharSequence == null) {
        localObject = "";
      }
      super.performFiltering("____history____=" + localObject, paramInt);
      return;
    }
    super.performFiltering(paramCharSequence, paramInt);
  }
  
  public final <T extends ListAdapter,  extends Filterable> void setAdapter(@NonNull T paramT)
  {
    if (!(paramT instanceof AbstractPlacesAutocompleteAdapter)) {
      throw new IllegalArgumentException("Custom adapters must inherit from " + AbstractPlacesAutocompleteAdapter.class.getSimpleName());
    }
    this.adapter = ((AbstractPlacesAutocompleteAdapter)paramT);
    this.historyManager = this.adapter.getHistoryManager();
    this.resultType = this.adapter.getResultType();
    this.api = this.adapter.getApi();
    super.setAdapter(paramT);
  }
  
  public void setApi(@NonNull PlacesApi paramPlacesApi)
  {
    this.api = paramPlacesApi;
    this.api.setLanguageCode(this.languageCode);
    this.adapter.setApi(paramPlacesApi);
  }
  
  public void setCompletionEnabled(boolean paramBoolean)
  {
    this.completionEnabled = paramBoolean;
  }
  
  public void setCurrentLocation(@Nullable Location paramLocation)
  {
    this.api.setCurrentLocation(paramLocation);
  }
  
  public void setHistoryManager(@Nullable AutocompleteHistoryManager paramAutocompleteHistoryManager)
  {
    this.historyManager = paramAutocompleteHistoryManager;
    this.adapter.setHistoryManager(paramAutocompleteHistoryManager);
  }
  
  public void setLanguageCode(@Nullable String paramString)
  {
    this.languageCode = paramString;
    this.api.setLanguageCode(this.languageCode);
  }
  
  public void setLocationBiasEnabled(boolean paramBoolean)
  {
    this.api.setLocationBiasEnabled(paramBoolean);
  }
  
  public final void setOnItemClickListener(AdapterView.OnItemClickListener paramOnItemClickListener)
  {
    throw new UnsupportedOperationException("Use set" + OnPlaceSelectedListener.class.getSimpleName() + "() instead");
  }
  
  public final void setOnItemSelectedListener(AdapterView.OnItemSelectedListener paramOnItemSelectedListener)
  {
    throw new UnsupportedOperationException("Use set" + OnPlaceSelectedListener.class.getSimpleName() + "() instead");
  }
  
  public void setOnPlaceSelectedListener(@Nullable OnPlaceSelectedListener paramOnPlaceSelectedListener)
  {
    this.listener = paramOnPlaceSelectedListener;
  }
  
  public void setRadiusMeters(Long paramLong)
  {
    this.api.setRadiusMeters(paramLong);
  }
  
  public void setResultType(@Nullable AutocompleteResultType paramAutocompleteResultType)
  {
    this.resultType = paramAutocompleteResultType;
    this.adapter.setResultType(paramAutocompleteResultType);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/seatgeek/placesautocomplete/PlacesAutocompleteTextView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */