package com.seatgeek.placesautocomplete.model;

import java.util.List;

public final class Place
{
  public final String description;
  public final List<MatchedSubstring> matched_substrings;
  public final String place_id;
  public final List<DescriptionTerm> terms;
  public final List<PlaceType> types;
  
  public Place(String paramString1, String paramString2, List<MatchedSubstring> paramList, List<DescriptionTerm> paramList1, List<PlaceType> paramList2)
  {
    this.description = paramString1;
    this.place_id = paramString2;
    this.matched_substrings = paramList;
    this.terms = paramList1;
    this.types = paramList2;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    do
    {
      return true;
      if (!(paramObject instanceof Place)) {
        return false;
      }
      paramObject = (Place)paramObject;
      if (this.description != null)
      {
        if (this.description.equals(((Place)paramObject).description)) {}
      }
      else {
        while (((Place)paramObject).description != null) {
          return false;
        }
      }
      if (this.matched_substrings != null)
      {
        if (this.matched_substrings.equals(((Place)paramObject).matched_substrings)) {}
      }
      else {
        while (((Place)paramObject).matched_substrings != null) {
          return false;
        }
      }
      if (this.place_id != null)
      {
        if (this.place_id.equals(((Place)paramObject).place_id)) {}
      }
      else {
        while (((Place)paramObject).place_id != null) {
          return false;
        }
      }
      if (this.terms != null)
      {
        if (this.terms.equals(((Place)paramObject).terms)) {}
      }
      else {
        while (((Place)paramObject).terms != null) {
          return false;
        }
      }
      if (this.types == null) {
        break;
      }
    } while (this.types.equals(((Place)paramObject).types));
    for (;;)
    {
      return false;
      if (((Place)paramObject).types == null) {
        break;
      }
    }
  }
  
  public int hashCode()
  {
    int n = 0;
    int i;
    int j;
    label33:
    int k;
    if (this.description != null)
    {
      i = this.description.hashCode();
      if (this.place_id == null) {
        break label115;
      }
      j = this.place_id.hashCode();
      if (this.matched_substrings == null) {
        break label120;
      }
      k = this.matched_substrings.hashCode();
      label50:
      if (this.terms == null) {
        break label125;
      }
    }
    label115:
    label120:
    label125:
    for (int m = this.terms.hashCode();; m = 0)
    {
      if (this.types != null) {
        n = this.types.hashCode();
      }
      return (((i * 31 + j) * 31 + k) * 31 + m) * 31 + n;
      i = 0;
      break;
      j = 0;
      break label33;
      k = 0;
      break label50;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/seatgeek/placesautocomplete/model/Place.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */