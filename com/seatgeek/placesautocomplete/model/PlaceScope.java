package com.seatgeek.placesautocomplete.model;

import com.google.gson.annotations.SerializedName;

public enum PlaceScope
{
  APP,  GOOGLE;
  
  private PlaceScope() {}
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/com/seatgeek/placesautocomplete/model/PlaceScope.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */