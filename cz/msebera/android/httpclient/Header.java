package cz.msebera.android.httpclient;

public abstract interface Header
{
  public abstract HeaderElement[] getElements()
    throws ParseException;
  
  public abstract String getName();
  
  public abstract String getValue();
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/cz/msebera/android/httpclient/Header.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */