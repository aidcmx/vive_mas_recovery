package cz.msebera.android.httpclient;

public abstract interface HttpRequest
  extends HttpMessage
{
  public abstract RequestLine getRequestLine();
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/cz/msebera/android/httpclient/HttpRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */