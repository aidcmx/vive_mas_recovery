package cz.msebera.android.httpclient;

public class ParseException
  extends RuntimeException
{
  private static final long serialVersionUID = -7288819855864183578L;
  
  public ParseException() {}
  
  public ParseException(String paramString)
  {
    super(paramString);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/cz/msebera/android/httpclient/ParseException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */