package cz.msebera.android.httpclient.client.methods;

import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.client.utils.CloneUtils;
import cz.msebera.android.httpclient.concurrent.Cancellable;
import cz.msebera.android.httpclient.conn.ClientConnectionRequest;
import cz.msebera.android.httpclient.conn.ConnectionReleaseTrigger;
import cz.msebera.android.httpclient.message.AbstractHttpMessage;
import cz.msebera.android.httpclient.message.HeaderGroup;
import cz.msebera.android.httpclient.params.HttpParams;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public abstract class AbstractExecutionAwareRequest
  extends AbstractHttpMessage
  implements HttpExecutionAware, AbortableHttpRequest, Cloneable, HttpRequest
{
  private final AtomicBoolean aborted = new AtomicBoolean(false);
  private final AtomicReference<Cancellable> cancellableRef = new AtomicReference(null);
  
  public void abort()
  {
    if (this.aborted.compareAndSet(false, true))
    {
      Cancellable localCancellable = (Cancellable)this.cancellableRef.getAndSet(null);
      if (localCancellable != null) {
        localCancellable.cancel();
      }
    }
  }
  
  public Object clone()
    throws CloneNotSupportedException
  {
    AbstractExecutionAwareRequest localAbstractExecutionAwareRequest = (AbstractExecutionAwareRequest)super.clone();
    localAbstractExecutionAwareRequest.headergroup = ((HeaderGroup)CloneUtils.cloneObject(this.headergroup));
    localAbstractExecutionAwareRequest.params = ((HttpParams)CloneUtils.cloneObject(this.params));
    return localAbstractExecutionAwareRequest;
  }
  
  public void completed()
  {
    this.cancellableRef.set(null);
  }
  
  public boolean isAborted()
  {
    return this.aborted.get();
  }
  
  public void reset()
  {
    Cancellable localCancellable = (Cancellable)this.cancellableRef.getAndSet(null);
    if (localCancellable != null) {
      localCancellable.cancel();
    }
    this.aborted.set(false);
  }
  
  public void setCancellable(Cancellable paramCancellable)
  {
    if (!this.aborted.get()) {
      this.cancellableRef.set(paramCancellable);
    }
  }
  
  @Deprecated
  public void setConnectionRequest(ClientConnectionRequest paramClientConnectionRequest)
  {
    setCancellable(new AbstractExecutionAwareRequest.1(this, paramClientConnectionRequest));
  }
  
  @Deprecated
  public void setReleaseTrigger(ConnectionReleaseTrigger paramConnectionReleaseTrigger)
  {
    setCancellable(new AbstractExecutionAwareRequest.2(this, paramConnectionReleaseTrigger));
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/cz/msebera/android/httpclient/client/methods/AbstractExecutionAwareRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */