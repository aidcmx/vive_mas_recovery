package cz.msebera.android.httpclient.client.methods;

import cz.msebera.android.httpclient.client.config.RequestConfig;

public abstract interface Configurable
{
  public abstract RequestConfig getConfig();
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/cz/msebera/android/httpclient/client/methods/Configurable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */