package cz.msebera.android.httpclient.client.params;

import cz.msebera.android.httpclient.annotation.Immutable;

@Deprecated
@Immutable
public final class CookiePolicy
{
  public static final String BEST_MATCH = "best-match";
  public static final String BROWSER_COMPATIBILITY = "compatibility";
  public static final String IGNORE_COOKIES = "ignoreCookies";
  public static final String NETSCAPE = "netscape";
  public static final String RFC_2109 = "rfc2109";
  public static final String RFC_2965 = "rfc2965";
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/cz/msebera/android/httpclient/client/params/CookiePolicy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */