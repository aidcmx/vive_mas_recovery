package cz.msebera.android.httpclient.client.protocol;

import cz.msebera.android.httpclient.HttpException;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.auth.AuthState;
import cz.msebera.android.httpclient.conn.HttpRoutedConnection;
import cz.msebera.android.httpclient.conn.routing.HttpRoute;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.protocol.HttpContext;
import cz.msebera.android.httpclient.util.Args;
import java.io.IOException;

@Deprecated
@Immutable
public class RequestProxyAuthentication
  extends RequestAuthenticationBase
{
  public void process(HttpRequest paramHttpRequest, HttpContext paramHttpContext)
    throws HttpException, IOException
  {
    Args.notNull(paramHttpRequest, "HTTP request");
    Args.notNull(paramHttpContext, "HTTP context");
    if (paramHttpRequest.containsHeader("Proxy-Authorization")) {}
    do
    {
      return;
      localObject = (HttpRoutedConnection)paramHttpContext.getAttribute("http.connection");
      if (localObject == null)
      {
        this.log.debug("HTTP connection not set in the context");
        return;
      }
    } while (((HttpRoutedConnection)localObject).getRoute().isTunnelled());
    Object localObject = (AuthState)paramHttpContext.getAttribute("http.auth.proxy-scope");
    if (localObject == null)
    {
      this.log.debug("Proxy auth state not set in the context");
      return;
    }
    if (this.log.isDebugEnabled()) {
      this.log.debug("Proxy auth state: " + ((AuthState)localObject).getState());
    }
    process((AuthState)localObject, paramHttpRequest, paramHttpContext);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/cz/msebera/android/httpclient/client/protocol/RequestProxyAuthentication.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */