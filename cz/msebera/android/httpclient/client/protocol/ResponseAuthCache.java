package cz.msebera.android.httpclient.client.protocol;

import cz.msebera.android.httpclient.HttpException;
import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.HttpResponseInterceptor;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.auth.AuthScheme;
import cz.msebera.android.httpclient.auth.AuthState;
import cz.msebera.android.httpclient.client.AuthCache;
import cz.msebera.android.httpclient.conn.scheme.Scheme;
import cz.msebera.android.httpclient.conn.scheme.SchemeRegistry;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.impl.client.BasicAuthCache;
import cz.msebera.android.httpclient.protocol.HttpContext;
import cz.msebera.android.httpclient.util.Args;
import java.io.IOException;

@Deprecated
@Immutable
public class ResponseAuthCache
  implements HttpResponseInterceptor
{
  public HttpClientAndroidLog log = new HttpClientAndroidLog(getClass());
  
  private void cache(AuthCache paramAuthCache, HttpHost paramHttpHost, AuthScheme paramAuthScheme)
  {
    if (this.log.isDebugEnabled()) {
      this.log.debug("Caching '" + paramAuthScheme.getSchemeName() + "' auth scheme for " + paramHttpHost);
    }
    paramAuthCache.put(paramHttpHost, paramAuthScheme);
  }
  
  private boolean isCachable(AuthState paramAuthState)
  {
    paramAuthState = paramAuthState.getAuthScheme();
    if ((paramAuthState == null) || (!paramAuthState.isComplete())) {}
    do
    {
      return false;
      paramAuthState = paramAuthState.getSchemeName();
    } while ((!paramAuthState.equalsIgnoreCase("Basic")) && (!paramAuthState.equalsIgnoreCase("Digest")));
    return true;
  }
  
  private void uncache(AuthCache paramAuthCache, HttpHost paramHttpHost, AuthScheme paramAuthScheme)
  {
    if (this.log.isDebugEnabled()) {
      this.log.debug("Removing from cache '" + paramAuthScheme.getSchemeName() + "' auth scheme for " + paramHttpHost);
    }
    paramAuthCache.remove(paramHttpHost);
  }
  
  public void process(HttpResponse paramHttpResponse, HttpContext paramHttpContext)
    throws HttpException, IOException
  {
    Args.notNull(paramHttpResponse, "HTTP request");
    Args.notNull(paramHttpContext, "HTTP context");
    Object localObject1 = (AuthCache)paramHttpContext.getAttribute("http.auth.auth-cache");
    Object localObject3 = (HttpHost)paramHttpContext.getAttribute("http.target_host");
    AuthState localAuthState = (AuthState)paramHttpContext.getAttribute("http.auth.target-scope");
    paramHttpResponse = (HttpResponse)localObject1;
    Object localObject2;
    if (localObject3 != null)
    {
      paramHttpResponse = (HttpResponse)localObject1;
      if (localAuthState != null)
      {
        if (this.log.isDebugEnabled()) {
          this.log.debug("Target auth state: " + localAuthState.getState());
        }
        paramHttpResponse = (HttpResponse)localObject1;
        if (isCachable(localAuthState))
        {
          paramHttpResponse = (SchemeRegistry)paramHttpContext.getAttribute("http.scheme-registry");
          localObject2 = localObject3;
          if (((HttpHost)localObject3).getPort() < 0)
          {
            paramHttpResponse = paramHttpResponse.getScheme((HttpHost)localObject3);
            localObject2 = new HttpHost(((HttpHost)localObject3).getHostName(), paramHttpResponse.resolvePort(((HttpHost)localObject3).getPort()), ((HttpHost)localObject3).getSchemeName());
          }
          paramHttpResponse = (HttpResponse)localObject1;
          if (localObject1 == null)
          {
            paramHttpResponse = new BasicAuthCache();
            paramHttpContext.setAttribute("http.auth.auth-cache", paramHttpResponse);
          }
          switch (ResponseAuthCache.1.$SwitchMap$cz$msebera$android$httpclient$auth$AuthProtocolState[localAuthState.getState().ordinal()])
          {
          }
        }
      }
    }
    for (;;)
    {
      localObject2 = (HttpHost)paramHttpContext.getAttribute("http.proxy_host");
      localObject3 = (AuthState)paramHttpContext.getAttribute("http.auth.proxy-scope");
      if ((localObject2 != null) && (localObject3 != null))
      {
        if (this.log.isDebugEnabled()) {
          this.log.debug("Proxy auth state: " + ((AuthState)localObject3).getState());
        }
        if (isCachable((AuthState)localObject3))
        {
          localObject1 = paramHttpResponse;
          if (paramHttpResponse == null)
          {
            localObject1 = new BasicAuthCache();
            paramHttpContext.setAttribute("http.auth.auth-cache", localObject1);
          }
        }
      }
      switch (ResponseAuthCache.1.$SwitchMap$cz$msebera$android$httpclient$auth$AuthProtocolState[localObject3.getState().ordinal()])
      {
      default: 
        return;
        cache(paramHttpResponse, (HttpHost)localObject2, localAuthState.getAuthScheme());
        continue;
        uncache(paramHttpResponse, (HttpHost)localObject2, localAuthState.getAuthScheme());
      }
    }
    cache((AuthCache)localObject1, (HttpHost)localObject2, ((AuthState)localObject3).getAuthScheme());
    return;
    uncache((AuthCache)localObject1, (HttpHost)localObject2, ((AuthState)localObject3).getAuthScheme());
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/cz/msebera/android/httpclient/client/protocol/ResponseAuthCache.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */