package cz.msebera.android.httpclient.conn;

import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.annotation.Immutable;
import java.io.IOException;
import java.net.ConnectException;
import java.net.InetAddress;

@Immutable
public class HttpHostConnectException
  extends ConnectException
{
  private static final long serialVersionUID = -3194482710275220224L;
  private final HttpHost host;
  
  @Deprecated
  public HttpHostConnectException(HttpHost paramHttpHost, ConnectException paramConnectException)
  {
    this(paramConnectException, paramHttpHost, null);
  }
  
  public HttpHostConnectException(IOException paramIOException, HttpHost paramHttpHost, InetAddress... paramVarArgs) {}
  
  public HttpHost getHost()
  {
    return this.host;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/cz/msebera/android/httpclient/conn/HttpHostConnectException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */