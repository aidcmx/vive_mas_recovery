package cz.msebera.android.httpclient.conn.params;

import cz.msebera.android.httpclient.annotation.ThreadSafe;
import cz.msebera.android.httpclient.conn.routing.HttpRoute;
import cz.msebera.android.httpclient.util.Args;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Deprecated
@ThreadSafe
public final class ConnPerRouteBean
  implements ConnPerRoute
{
  public static final int DEFAULT_MAX_CONNECTIONS_PER_ROUTE = 2;
  private volatile int defaultMax;
  private final ConcurrentHashMap<HttpRoute, Integer> maxPerHostMap = new ConcurrentHashMap();
  
  public ConnPerRouteBean()
  {
    this(2);
  }
  
  public ConnPerRouteBean(int paramInt)
  {
    setDefaultMaxPerRoute(paramInt);
  }
  
  public int getDefaultMax()
  {
    return this.defaultMax;
  }
  
  public int getDefaultMaxPerRoute()
  {
    return this.defaultMax;
  }
  
  public int getMaxForRoute(HttpRoute paramHttpRoute)
  {
    Args.notNull(paramHttpRoute, "HTTP route");
    paramHttpRoute = (Integer)this.maxPerHostMap.get(paramHttpRoute);
    if (paramHttpRoute != null) {
      return paramHttpRoute.intValue();
    }
    return this.defaultMax;
  }
  
  public void setDefaultMaxPerRoute(int paramInt)
  {
    Args.positive(paramInt, "Defautl max per route");
    this.defaultMax = paramInt;
  }
  
  public void setMaxForRoute(HttpRoute paramHttpRoute, int paramInt)
  {
    Args.notNull(paramHttpRoute, "HTTP route");
    Args.positive(paramInt, "Max per route");
    this.maxPerHostMap.put(paramHttpRoute, Integer.valueOf(paramInt));
  }
  
  public void setMaxForRoutes(Map<HttpRoute, Integer> paramMap)
  {
    if (paramMap == null) {
      return;
    }
    this.maxPerHostMap.clear();
    this.maxPerHostMap.putAll(paramMap);
  }
  
  public String toString()
  {
    return this.maxPerHostMap.toString();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/cz/msebera/android/httpclient/conn/params/ConnPerRouteBean.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */