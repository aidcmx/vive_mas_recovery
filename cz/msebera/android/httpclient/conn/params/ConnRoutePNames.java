package cz.msebera.android.httpclient.conn.params;

@Deprecated
public abstract interface ConnRoutePNames
{
  public static final String DEFAULT_PROXY = "http.route.default-proxy";
  public static final String FORCED_ROUTE = "http.route.forced-route";
  public static final String LOCAL_ADDRESS = "http.route.local-address";
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/cz/msebera/android/httpclient/conn/params/ConnRoutePNames.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */