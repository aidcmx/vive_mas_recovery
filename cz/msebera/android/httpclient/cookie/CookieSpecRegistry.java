package cz.msebera.android.httpclient.cookie;

import cz.msebera.android.httpclient.annotation.ThreadSafe;
import cz.msebera.android.httpclient.config.Lookup;
import cz.msebera.android.httpclient.params.HttpParams;
import cz.msebera.android.httpclient.util.Args;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Deprecated
@ThreadSafe
public final class CookieSpecRegistry
  implements Lookup<CookieSpecProvider>
{
  private final ConcurrentHashMap<String, CookieSpecFactory> registeredSpecs = new ConcurrentHashMap();
  
  public CookieSpec getCookieSpec(String paramString)
    throws IllegalStateException
  {
    return getCookieSpec(paramString, null);
  }
  
  public CookieSpec getCookieSpec(String paramString, HttpParams paramHttpParams)
    throws IllegalStateException
  {
    Args.notNull(paramString, "Name");
    CookieSpecFactory localCookieSpecFactory = (CookieSpecFactory)this.registeredSpecs.get(paramString.toLowerCase(Locale.ENGLISH));
    if (localCookieSpecFactory != null) {
      return localCookieSpecFactory.newInstance(paramHttpParams);
    }
    throw new IllegalStateException("Unsupported cookie spec: " + paramString);
  }
  
  public List<String> getSpecNames()
  {
    return new ArrayList(this.registeredSpecs.keySet());
  }
  
  public CookieSpecProvider lookup(String paramString)
  {
    return new CookieSpecRegistry.1(this, paramString);
  }
  
  public void register(String paramString, CookieSpecFactory paramCookieSpecFactory)
  {
    Args.notNull(paramString, "Name");
    Args.notNull(paramCookieSpecFactory, "Cookie spec factory");
    this.registeredSpecs.put(paramString.toLowerCase(Locale.ENGLISH), paramCookieSpecFactory);
  }
  
  public void setItems(Map<String, CookieSpecFactory> paramMap)
  {
    if (paramMap == null) {
      return;
    }
    this.registeredSpecs.clear();
    this.registeredSpecs.putAll(paramMap);
  }
  
  public void unregister(String paramString)
  {
    Args.notNull(paramString, "Id");
    this.registeredSpecs.remove(paramString.toLowerCase(Locale.ENGLISH));
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/cz/msebera/android/httpclient/cookie/CookieSpecRegistry.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */