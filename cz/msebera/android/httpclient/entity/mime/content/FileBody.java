package cz.msebera.android.httpclient.entity.mime.content;

import cz.msebera.android.httpclient.entity.ContentType;
import cz.msebera.android.httpclient.util.Args;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class FileBody
  extends AbstractContentBody
{
  private final File file;
  private final String filename;
  
  public FileBody(File paramFile) {}
  
  public FileBody(File paramFile, ContentType paramContentType)
  {
    this(paramFile, paramContentType, null);
  }
  
  public FileBody(File paramFile, ContentType paramContentType, String paramString)
  {
    super(paramContentType);
    Args.notNull(paramFile, "File");
    this.file = paramFile;
    this.filename = paramString;
  }
  
  @Deprecated
  public FileBody(File paramFile, String paramString)
  {
    this(paramFile, ContentType.create(paramString), null);
  }
  
  @Deprecated
  public FileBody(File paramFile, String paramString1, String paramString2)
  {
    this(paramFile, null, paramString1, paramString2);
  }
  
  @Deprecated
  public FileBody(File paramFile, String paramString1, String paramString2, String paramString3)
  {
    this(paramFile, ContentType.create(paramString2, paramString3), paramString1);
  }
  
  public long getContentLength()
  {
    return this.file.length();
  }
  
  public File getFile()
  {
    return this.file;
  }
  
  public String getFilename()
  {
    return this.filename;
  }
  
  public InputStream getInputStream()
    throws IOException
  {
    return new FileInputStream(this.file);
  }
  
  public String getTransferEncoding()
  {
    return "binary";
  }
  
  public void writeTo(OutputStream paramOutputStream)
    throws IOException
  {
    Args.notNull(paramOutputStream, "Output stream");
    FileInputStream localFileInputStream = new FileInputStream(this.file);
    try
    {
      byte[] arrayOfByte = new byte['က'];
      for (;;)
      {
        int i = localFileInputStream.read(arrayOfByte);
        if (i == -1) {
          break;
        }
        paramOutputStream.write(arrayOfByte, 0, i);
      }
    }
    finally
    {
      localFileInputStream.close();
    }
    localFileInputStream.close();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/cz/msebera/android/httpclient/entity/mime/content/FileBody.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */