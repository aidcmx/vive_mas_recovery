package cz.msebera.android.httpclient.entity.mime.content;

import cz.msebera.android.httpclient.entity.ContentType;
import cz.msebera.android.httpclient.util.Args;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class InputStreamBody
  extends AbstractContentBody
{
  private final String filename;
  private final InputStream in;
  
  public InputStreamBody(InputStream paramInputStream, ContentType paramContentType)
  {
    this(paramInputStream, paramContentType, null);
  }
  
  public InputStreamBody(InputStream paramInputStream, ContentType paramContentType, String paramString)
  {
    super(paramContentType);
    Args.notNull(paramInputStream, "Input stream");
    this.in = paramInputStream;
    this.filename = paramString;
  }
  
  public InputStreamBody(InputStream paramInputStream, String paramString)
  {
    this(paramInputStream, ContentType.DEFAULT_BINARY, paramString);
  }
  
  @Deprecated
  public InputStreamBody(InputStream paramInputStream, String paramString1, String paramString2)
  {
    this(paramInputStream, ContentType.create(paramString1), paramString2);
  }
  
  public long getContentLength()
  {
    return -1L;
  }
  
  public String getFilename()
  {
    return this.filename;
  }
  
  public InputStream getInputStream()
  {
    return this.in;
  }
  
  public String getTransferEncoding()
  {
    return "binary";
  }
  
  public void writeTo(OutputStream paramOutputStream)
    throws IOException
  {
    Args.notNull(paramOutputStream, "Output stream");
    try
    {
      byte[] arrayOfByte = new byte['က'];
      for (;;)
      {
        int i = this.in.read(arrayOfByte);
        if (i == -1) {
          break;
        }
        paramOutputStream.write(arrayOfByte, 0, i);
      }
      paramOutputStream.flush();
    }
    finally
    {
      this.in.close();
    }
    this.in.close();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/cz/msebera/android/httpclient/entity/mime/content/InputStreamBody.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */