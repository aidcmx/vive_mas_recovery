package cz.msebera.android.httpclient.impl.auth;

import cz.msebera.android.httpclient.Consts;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.auth.AuthenticationException;
import cz.msebera.android.httpclient.auth.ChallengeState;
import cz.msebera.android.httpclient.auth.Credentials;
import cz.msebera.android.httpclient.auth.MalformedChallengeException;
import cz.msebera.android.httpclient.extras.Base64;
import cz.msebera.android.httpclient.message.BufferedHeader;
import cz.msebera.android.httpclient.protocol.BasicHttpContext;
import cz.msebera.android.httpclient.protocol.HttpContext;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.CharArrayBuffer;
import cz.msebera.android.httpclient.util.EncodingUtils;
import java.nio.charset.Charset;
import java.security.Principal;

@NotThreadSafe
public class BasicScheme
  extends RFC2617Scheme
{
  private boolean complete;
  
  public BasicScheme()
  {
    this(Consts.ASCII);
  }
  
  @Deprecated
  public BasicScheme(ChallengeState paramChallengeState)
  {
    super(paramChallengeState);
  }
  
  public BasicScheme(Charset paramCharset)
  {
    super(paramCharset);
    this.complete = false;
  }
  
  @Deprecated
  public static Header authenticate(Credentials paramCredentials, String paramString, boolean paramBoolean)
  {
    Args.notNull(paramCredentials, "Credentials");
    Args.notNull(paramString, "charset");
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(paramCredentials.getUserPrincipal().getName());
    localStringBuilder.append(":");
    if (paramCredentials.getPassword() == null)
    {
      paramCredentials = "null";
      localStringBuilder.append(paramCredentials);
      paramCredentials = Base64.encode(EncodingUtils.getBytes(localStringBuilder.toString(), paramString), 2);
      paramString = new CharArrayBuffer(32);
      if (!paramBoolean) {
        break label129;
      }
      paramString.append("Proxy-Authorization");
    }
    for (;;)
    {
      paramString.append(": Basic ");
      paramString.append(paramCredentials, 0, paramCredentials.length);
      return new BufferedHeader(paramString);
      paramCredentials = paramCredentials.getPassword();
      break;
      label129:
      paramString.append("Authorization");
    }
  }
  
  @Deprecated
  public Header authenticate(Credentials paramCredentials, HttpRequest paramHttpRequest)
    throws AuthenticationException
  {
    return authenticate(paramCredentials, paramHttpRequest, new BasicHttpContext());
  }
  
  public Header authenticate(Credentials paramCredentials, HttpRequest paramHttpRequest, HttpContext paramHttpContext)
    throws AuthenticationException
  {
    Args.notNull(paramCredentials, "Credentials");
    Args.notNull(paramHttpRequest, "HTTP request");
    paramHttpContext = new StringBuilder();
    paramHttpContext.append(paramCredentials.getUserPrincipal().getName());
    paramHttpContext.append(":");
    if (paramCredentials.getPassword() == null)
    {
      paramCredentials = "null";
      paramHttpContext.append(paramCredentials);
      paramCredentials = Base64.encode(EncodingUtils.getBytes(paramHttpContext.toString(), getCredentialsCharset(paramHttpRequest)), 2);
      paramHttpRequest = new CharArrayBuffer(32);
      if (!isProxy()) {
        break label136;
      }
      paramHttpRequest.append("Proxy-Authorization");
    }
    for (;;)
    {
      paramHttpRequest.append(": Basic ");
      paramHttpRequest.append(paramCredentials, 0, paramCredentials.length);
      return new BufferedHeader(paramHttpRequest);
      paramCredentials = paramCredentials.getPassword();
      break;
      label136:
      paramHttpRequest.append("Authorization");
    }
  }
  
  public String getSchemeName()
  {
    return "basic";
  }
  
  public boolean isComplete()
  {
    return this.complete;
  }
  
  public boolean isConnectionBased()
  {
    return false;
  }
  
  public void processChallenge(Header paramHeader)
    throws MalformedChallengeException
  {
    super.processChallenge(paramHeader);
    this.complete = true;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/cz/msebera/android/httpclient/impl/auth/BasicScheme.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */