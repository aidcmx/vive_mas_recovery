package cz.msebera.android.httpclient.impl.auth;

import cz.msebera.android.httpclient.Consts;
import cz.msebera.android.httpclient.HeaderElement;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.auth.ChallengeState;
import cz.msebera.android.httpclient.auth.MalformedChallengeException;
import cz.msebera.android.httpclient.message.BasicHeaderValueParser;
import cz.msebera.android.httpclient.message.HeaderValueParser;
import cz.msebera.android.httpclient.message.ParserCursor;
import cz.msebera.android.httpclient.params.HttpParams;
import cz.msebera.android.httpclient.util.CharArrayBuffer;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@NotThreadSafe
public abstract class RFC2617Scheme
  extends AuthSchemeBase
{
  private final Charset credentialsCharset;
  private final Map<String, String> params = new HashMap();
  
  public RFC2617Scheme()
  {
    this(Consts.ASCII);
  }
  
  @Deprecated
  public RFC2617Scheme(ChallengeState paramChallengeState)
  {
    super(paramChallengeState);
    this.credentialsCharset = Consts.ASCII;
  }
  
  public RFC2617Scheme(Charset paramCharset)
  {
    if (paramCharset != null) {}
    for (;;)
    {
      this.credentialsCharset = paramCharset;
      return;
      paramCharset = Consts.ASCII;
    }
  }
  
  String getCredentialsCharset(HttpRequest paramHttpRequest)
  {
    String str = (String)paramHttpRequest.getParams().getParameter("http.auth.credential-charset");
    paramHttpRequest = str;
    if (str == null) {
      paramHttpRequest = getCredentialsCharset().name();
    }
    return paramHttpRequest;
  }
  
  public Charset getCredentialsCharset()
  {
    return this.credentialsCharset;
  }
  
  public String getParameter(String paramString)
  {
    if (paramString == null) {
      return null;
    }
    return (String)this.params.get(paramString.toLowerCase(Locale.ENGLISH));
  }
  
  protected Map<String, String> getParameters()
  {
    return this.params;
  }
  
  public String getRealm()
  {
    return getParameter("realm");
  }
  
  protected void parseChallenge(CharArrayBuffer paramCharArrayBuffer, int paramInt1, int paramInt2)
    throws MalformedChallengeException
  {
    paramCharArrayBuffer = BasicHeaderValueParser.INSTANCE.parseElements(paramCharArrayBuffer, new ParserCursor(paramInt1, paramCharArrayBuffer.length()));
    if (paramCharArrayBuffer.length == 0) {
      throw new MalformedChallengeException("Authentication challenge is empty");
    }
    this.params.clear();
    paramInt2 = paramCharArrayBuffer.length;
    paramInt1 = 0;
    while (paramInt1 < paramInt2)
    {
      Object localObject = paramCharArrayBuffer[paramInt1];
      this.params.put(((HeaderElement)localObject).getName().toLowerCase(Locale.ENGLISH), ((HeaderElement)localObject).getValue());
      paramInt1 += 1;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/cz/msebera/android/httpclient/impl/auth/RFC2617Scheme.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */