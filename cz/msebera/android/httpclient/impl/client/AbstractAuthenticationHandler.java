package cz.msebera.android.httpclient.impl.client;

import cz.msebera.android.httpclient.FormattedHeader;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.auth.AuthScheme;
import cz.msebera.android.httpclient.auth.AuthSchemeRegistry;
import cz.msebera.android.httpclient.auth.AuthenticationException;
import cz.msebera.android.httpclient.auth.MalformedChallengeException;
import cz.msebera.android.httpclient.client.AuthenticationHandler;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.protocol.HTTP;
import cz.msebera.android.httpclient.protocol.HttpContext;
import cz.msebera.android.httpclient.util.Asserts;
import cz.msebera.android.httpclient.util.CharArrayBuffer;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Deprecated
@Immutable
public abstract class AbstractAuthenticationHandler
  implements AuthenticationHandler
{
  private static final List<String> DEFAULT_SCHEME_PRIORITY = Collections.unmodifiableList(Arrays.asList(new String[] { "negotiate", "NTLM", "Digest", "Basic" }));
  public HttpClientAndroidLog log = new HttpClientAndroidLog(getClass());
  
  protected List<String> getAuthPreferences()
  {
    return DEFAULT_SCHEME_PRIORITY;
  }
  
  protected List<String> getAuthPreferences(HttpResponse paramHttpResponse, HttpContext paramHttpContext)
  {
    return getAuthPreferences();
  }
  
  protected Map<String, Header> parseChallenges(Header[] paramArrayOfHeader)
    throws MalformedChallengeException
  {
    HashMap localHashMap = new HashMap(paramArrayOfHeader.length);
    int m = paramArrayOfHeader.length;
    int k = 0;
    while (k < m)
    {
      Header localHeader = paramArrayOfHeader[k];
      CharArrayBuffer localCharArrayBuffer;
      int i;
      if ((localHeader instanceof FormattedHeader))
      {
        localCharArrayBuffer = ((FormattedHeader)localHeader).getBuffer();
        i = ((FormattedHeader)localHeader).getValuePos();
      }
      while ((i < localCharArrayBuffer.length()) && (HTTP.isWhitespace(localCharArrayBuffer.charAt(i))))
      {
        i += 1;
        continue;
        String str = localHeader.getValue();
        if (str == null) {
          throw new MalformedChallengeException("Header value is null");
        }
        localCharArrayBuffer = new CharArrayBuffer(str.length());
        localCharArrayBuffer.append(str);
        i = 0;
      }
      int j = i;
      while ((j < localCharArrayBuffer.length()) && (!HTTP.isWhitespace(localCharArrayBuffer.charAt(j)))) {
        j += 1;
      }
      localHashMap.put(localCharArrayBuffer.substring(i, j).toLowerCase(Locale.ENGLISH), localHeader);
      k += 1;
    }
    return localHashMap;
  }
  
  public AuthScheme selectScheme(Map<String, Header> paramMap, HttpResponse paramHttpResponse, HttpContext paramHttpContext)
    throws AuthenticationException
  {
    AuthSchemeRegistry localAuthSchemeRegistry = (AuthSchemeRegistry)paramHttpContext.getAttribute("http.authscheme-registry");
    Asserts.notNull(localAuthSchemeRegistry, "AuthScheme registry");
    List localList = getAuthPreferences(paramHttpResponse, paramHttpContext);
    paramHttpContext = localList;
    if (localList == null) {
      paramHttpContext = DEFAULT_SCHEME_PRIORITY;
    }
    if (this.log.isDebugEnabled()) {
      this.log.debug("Authentication schemes in the order of preference: " + paramHttpContext);
    }
    localList = null;
    Iterator localIterator = paramHttpContext.iterator();
    for (;;)
    {
      paramHttpContext = localList;
      String str;
      if (localIterator.hasNext())
      {
        str = (String)localIterator.next();
        if ((Header)paramMap.get(str.toLowerCase(Locale.ENGLISH)) == null) {
          break label260;
        }
        if (this.log.isDebugEnabled()) {
          this.log.debug(str + " authentication scheme selected");
        }
      }
      try
      {
        paramHttpContext = localAuthSchemeRegistry.getAuthScheme(str, paramHttpResponse.getParams());
        if (paramHttpContext != null) {
          break;
        }
        throw new AuthenticationException("Unable to respond to any of these challenges: " + paramMap);
      }
      catch (IllegalStateException paramHttpContext) {}
      if (this.log.isWarnEnabled())
      {
        this.log.warn("Authentication scheme " + str + " not supported");
        continue;
        label260:
        if (this.log.isDebugEnabled()) {
          this.log.debug("Challenge for " + str + " authentication scheme not available");
        }
      }
    }
    return paramHttpContext;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/cz/msebera/android/httpclient/impl/client/AbstractAuthenticationHandler.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */