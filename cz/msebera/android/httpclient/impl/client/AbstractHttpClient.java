package cz.msebera.android.httpclient.impl.client;

import cz.msebera.android.httpclient.ConnectionReuseStrategy;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpRequestInterceptor;
import cz.msebera.android.httpclient.HttpResponseInterceptor;
import cz.msebera.android.httpclient.annotation.GuardedBy;
import cz.msebera.android.httpclient.annotation.ThreadSafe;
import cz.msebera.android.httpclient.auth.AuthSchemeRegistry;
import cz.msebera.android.httpclient.client.AuthenticationHandler;
import cz.msebera.android.httpclient.client.AuthenticationStrategy;
import cz.msebera.android.httpclient.client.BackoffManager;
import cz.msebera.android.httpclient.client.ConnectionBackoffStrategy;
import cz.msebera.android.httpclient.client.CookieStore;
import cz.msebera.android.httpclient.client.CredentialsProvider;
import cz.msebera.android.httpclient.client.HttpRequestRetryHandler;
import cz.msebera.android.httpclient.client.RedirectHandler;
import cz.msebera.android.httpclient.client.RedirectStrategy;
import cz.msebera.android.httpclient.client.RequestDirector;
import cz.msebera.android.httpclient.client.UserTokenHandler;
import cz.msebera.android.httpclient.conn.ClientConnectionManager;
import cz.msebera.android.httpclient.conn.ClientConnectionManagerFactory;
import cz.msebera.android.httpclient.conn.ConnectionKeepAliveStrategy;
import cz.msebera.android.httpclient.conn.routing.HttpRoutePlanner;
import cz.msebera.android.httpclient.conn.scheme.SchemeRegistry;
import cz.msebera.android.httpclient.cookie.CookieSpecRegistry;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.impl.DefaultConnectionReuseStrategy;
import cz.msebera.android.httpclient.impl.auth.BasicSchemeFactory;
import cz.msebera.android.httpclient.impl.auth.DigestSchemeFactory;
import cz.msebera.android.httpclient.impl.auth.NTLMSchemeFactory;
import cz.msebera.android.httpclient.impl.conn.BasicClientConnectionManager;
import cz.msebera.android.httpclient.impl.conn.DefaultHttpRoutePlanner;
import cz.msebera.android.httpclient.impl.conn.SchemeRegistryFactory;
import cz.msebera.android.httpclient.impl.cookie.BestMatchSpecFactory;
import cz.msebera.android.httpclient.impl.cookie.BrowserCompatSpecFactory;
import cz.msebera.android.httpclient.impl.cookie.IgnoreSpecFactory;
import cz.msebera.android.httpclient.impl.cookie.NetscapeDraftSpecFactory;
import cz.msebera.android.httpclient.impl.cookie.RFC2109SpecFactory;
import cz.msebera.android.httpclient.impl.cookie.RFC2965SpecFactory;
import cz.msebera.android.httpclient.params.HttpParams;
import cz.msebera.android.httpclient.protocol.BasicHttpContext;
import cz.msebera.android.httpclient.protocol.BasicHttpProcessor;
import cz.msebera.android.httpclient.protocol.HttpContext;
import cz.msebera.android.httpclient.protocol.HttpProcessor;
import cz.msebera.android.httpclient.protocol.HttpRequestExecutor;
import cz.msebera.android.httpclient.protocol.ImmutableHttpProcessor;

@Deprecated
@ThreadSafe
public abstract class AbstractHttpClient
  extends CloseableHttpClient
{
  @GuardedBy("this")
  private BackoffManager backoffManager;
  @GuardedBy("this")
  private ClientConnectionManager connManager;
  @GuardedBy("this")
  private ConnectionBackoffStrategy connectionBackoffStrategy;
  @GuardedBy("this")
  private CookieStore cookieStore;
  @GuardedBy("this")
  private CredentialsProvider credsProvider;
  @GuardedBy("this")
  private HttpParams defaultParams;
  @GuardedBy("this")
  private ConnectionKeepAliveStrategy keepAliveStrategy;
  public HttpClientAndroidLog log = new HttpClientAndroidLog(getClass());
  @GuardedBy("this")
  private BasicHttpProcessor mutableProcessor;
  @GuardedBy("this")
  private ImmutableHttpProcessor protocolProcessor;
  @GuardedBy("this")
  private AuthenticationStrategy proxyAuthStrategy;
  @GuardedBy("this")
  private RedirectStrategy redirectStrategy;
  @GuardedBy("this")
  private HttpRequestExecutor requestExec;
  @GuardedBy("this")
  private HttpRequestRetryHandler retryHandler;
  @GuardedBy("this")
  private ConnectionReuseStrategy reuseStrategy;
  @GuardedBy("this")
  private HttpRoutePlanner routePlanner;
  @GuardedBy("this")
  private AuthSchemeRegistry supportedAuthSchemes;
  @GuardedBy("this")
  private CookieSpecRegistry supportedCookieSpecs;
  @GuardedBy("this")
  private AuthenticationStrategy targetAuthStrategy;
  @GuardedBy("this")
  private UserTokenHandler userTokenHandler;
  
  protected AbstractHttpClient(ClientConnectionManager paramClientConnectionManager, HttpParams paramHttpParams)
  {
    this.defaultParams = paramHttpParams;
    this.connManager = paramClientConnectionManager;
  }
  
  private HttpProcessor getProtocolProcessor()
  {
    try
    {
      if (this.protocolProcessor == null)
      {
        localObject1 = getHttpProcessor();
        int j = ((BasicHttpProcessor)localObject1).getRequestInterceptorCount();
        HttpRequestInterceptor[] arrayOfHttpRequestInterceptor = new HttpRequestInterceptor[j];
        int i = 0;
        while (i < j)
        {
          arrayOfHttpRequestInterceptor[i] = ((BasicHttpProcessor)localObject1).getRequestInterceptor(i);
          i += 1;
        }
        j = ((BasicHttpProcessor)localObject1).getResponseInterceptorCount();
        HttpResponseInterceptor[] arrayOfHttpResponseInterceptor = new HttpResponseInterceptor[j];
        i = 0;
        while (i < j)
        {
          arrayOfHttpResponseInterceptor[i] = ((BasicHttpProcessor)localObject1).getResponseInterceptor(i);
          i += 1;
        }
        this.protocolProcessor = new ImmutableHttpProcessor(arrayOfHttpRequestInterceptor, arrayOfHttpResponseInterceptor);
      }
      Object localObject1 = this.protocolProcessor;
      return (HttpProcessor)localObject1;
    }
    finally {}
  }
  
  public void addRequestInterceptor(HttpRequestInterceptor paramHttpRequestInterceptor)
  {
    try
    {
      getHttpProcessor().addInterceptor(paramHttpRequestInterceptor);
      this.protocolProcessor = null;
      return;
    }
    finally
    {
      paramHttpRequestInterceptor = finally;
      throw paramHttpRequestInterceptor;
    }
  }
  
  public void addRequestInterceptor(HttpRequestInterceptor paramHttpRequestInterceptor, int paramInt)
  {
    try
    {
      getHttpProcessor().addInterceptor(paramHttpRequestInterceptor, paramInt);
      this.protocolProcessor = null;
      return;
    }
    finally
    {
      paramHttpRequestInterceptor = finally;
      throw paramHttpRequestInterceptor;
    }
  }
  
  public void addResponseInterceptor(HttpResponseInterceptor paramHttpResponseInterceptor)
  {
    try
    {
      getHttpProcessor().addInterceptor(paramHttpResponseInterceptor);
      this.protocolProcessor = null;
      return;
    }
    finally
    {
      paramHttpResponseInterceptor = finally;
      throw paramHttpResponseInterceptor;
    }
  }
  
  public void addResponseInterceptor(HttpResponseInterceptor paramHttpResponseInterceptor, int paramInt)
  {
    try
    {
      getHttpProcessor().addInterceptor(paramHttpResponseInterceptor, paramInt);
      this.protocolProcessor = null;
      return;
    }
    finally
    {
      paramHttpResponseInterceptor = finally;
      throw paramHttpResponseInterceptor;
    }
  }
  
  public void clearRequestInterceptors()
  {
    try
    {
      getHttpProcessor().clearRequestInterceptors();
      this.protocolProcessor = null;
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public void clearResponseInterceptors()
  {
    try
    {
      getHttpProcessor().clearResponseInterceptors();
      this.protocolProcessor = null;
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public void close()
  {
    getConnectionManager().shutdown();
  }
  
  protected AuthSchemeRegistry createAuthSchemeRegistry()
  {
    AuthSchemeRegistry localAuthSchemeRegistry = new AuthSchemeRegistry();
    localAuthSchemeRegistry.register("Basic", new BasicSchemeFactory());
    localAuthSchemeRegistry.register("Digest", new DigestSchemeFactory());
    localAuthSchemeRegistry.register("NTLM", new NTLMSchemeFactory());
    return localAuthSchemeRegistry;
  }
  
  protected ClientConnectionManager createClientConnectionManager()
  {
    SchemeRegistry localSchemeRegistry = SchemeRegistryFactory.createDefault();
    HttpParams localHttpParams = getParams();
    ClientConnectionManagerFactory localClientConnectionManagerFactory = null;
    String str = (String)localHttpParams.getParameter("http.connection-manager.factory-class-name");
    if (str != null) {}
    try
    {
      localClientConnectionManagerFactory = (ClientConnectionManagerFactory)Class.forName(str).newInstance();
      if (localClientConnectionManagerFactory != null) {
        return localClientConnectionManagerFactory.newInstance(localHttpParams, localSchemeRegistry);
      }
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new IllegalStateException("Invalid class name: " + str);
    }
    catch (IllegalAccessException localIllegalAccessException)
    {
      throw new IllegalAccessError(localIllegalAccessException.getMessage());
    }
    catch (InstantiationException localInstantiationException)
    {
      throw new InstantiationError(localInstantiationException.getMessage());
    }
    return new BasicClientConnectionManager(localSchemeRegistry);
  }
  
  @Deprecated
  protected RequestDirector createClientRequestDirector(HttpRequestExecutor paramHttpRequestExecutor, ClientConnectionManager paramClientConnectionManager, ConnectionReuseStrategy paramConnectionReuseStrategy, ConnectionKeepAliveStrategy paramConnectionKeepAliveStrategy, HttpRoutePlanner paramHttpRoutePlanner, HttpProcessor paramHttpProcessor, HttpRequestRetryHandler paramHttpRequestRetryHandler, RedirectHandler paramRedirectHandler, AuthenticationHandler paramAuthenticationHandler1, AuthenticationHandler paramAuthenticationHandler2, UserTokenHandler paramUserTokenHandler, HttpParams paramHttpParams)
  {
    return new DefaultRequestDirector(paramHttpRequestExecutor, paramClientConnectionManager, paramConnectionReuseStrategy, paramConnectionKeepAliveStrategy, paramHttpRoutePlanner, paramHttpProcessor, paramHttpRequestRetryHandler, paramRedirectHandler, paramAuthenticationHandler1, paramAuthenticationHandler2, paramUserTokenHandler, paramHttpParams);
  }
  
  @Deprecated
  protected RequestDirector createClientRequestDirector(HttpRequestExecutor paramHttpRequestExecutor, ClientConnectionManager paramClientConnectionManager, ConnectionReuseStrategy paramConnectionReuseStrategy, ConnectionKeepAliveStrategy paramConnectionKeepAliveStrategy, HttpRoutePlanner paramHttpRoutePlanner, HttpProcessor paramHttpProcessor, HttpRequestRetryHandler paramHttpRequestRetryHandler, RedirectStrategy paramRedirectStrategy, AuthenticationHandler paramAuthenticationHandler1, AuthenticationHandler paramAuthenticationHandler2, UserTokenHandler paramUserTokenHandler, HttpParams paramHttpParams)
  {
    return new DefaultRequestDirector(this.log, paramHttpRequestExecutor, paramClientConnectionManager, paramConnectionReuseStrategy, paramConnectionKeepAliveStrategy, paramHttpRoutePlanner, paramHttpProcessor, paramHttpRequestRetryHandler, paramRedirectStrategy, paramAuthenticationHandler1, paramAuthenticationHandler2, paramUserTokenHandler, paramHttpParams);
  }
  
  protected RequestDirector createClientRequestDirector(HttpRequestExecutor paramHttpRequestExecutor, ClientConnectionManager paramClientConnectionManager, ConnectionReuseStrategy paramConnectionReuseStrategy, ConnectionKeepAliveStrategy paramConnectionKeepAliveStrategy, HttpRoutePlanner paramHttpRoutePlanner, HttpProcessor paramHttpProcessor, HttpRequestRetryHandler paramHttpRequestRetryHandler, RedirectStrategy paramRedirectStrategy, AuthenticationStrategy paramAuthenticationStrategy1, AuthenticationStrategy paramAuthenticationStrategy2, UserTokenHandler paramUserTokenHandler, HttpParams paramHttpParams)
  {
    return new DefaultRequestDirector(this.log, paramHttpRequestExecutor, paramClientConnectionManager, paramConnectionReuseStrategy, paramConnectionKeepAliveStrategy, paramHttpRoutePlanner, paramHttpProcessor, paramHttpRequestRetryHandler, paramRedirectStrategy, paramAuthenticationStrategy1, paramAuthenticationStrategy2, paramUserTokenHandler, paramHttpParams);
  }
  
  protected ConnectionKeepAliveStrategy createConnectionKeepAliveStrategy()
  {
    return new DefaultConnectionKeepAliveStrategy();
  }
  
  protected ConnectionReuseStrategy createConnectionReuseStrategy()
  {
    return new DefaultConnectionReuseStrategy();
  }
  
  protected CookieSpecRegistry createCookieSpecRegistry()
  {
    CookieSpecRegistry localCookieSpecRegistry = new CookieSpecRegistry();
    localCookieSpecRegistry.register("best-match", new BestMatchSpecFactory());
    localCookieSpecRegistry.register("compatibility", new BrowserCompatSpecFactory());
    localCookieSpecRegistry.register("netscape", new NetscapeDraftSpecFactory());
    localCookieSpecRegistry.register("rfc2109", new RFC2109SpecFactory());
    localCookieSpecRegistry.register("rfc2965", new RFC2965SpecFactory());
    localCookieSpecRegistry.register("ignoreCookies", new IgnoreSpecFactory());
    return localCookieSpecRegistry;
  }
  
  protected CookieStore createCookieStore()
  {
    return new BasicCookieStore();
  }
  
  protected CredentialsProvider createCredentialsProvider()
  {
    return new BasicCredentialsProvider();
  }
  
  protected HttpContext createHttpContext()
  {
    BasicHttpContext localBasicHttpContext = new BasicHttpContext();
    localBasicHttpContext.setAttribute("http.scheme-registry", getConnectionManager().getSchemeRegistry());
    localBasicHttpContext.setAttribute("http.authscheme-registry", getAuthSchemes());
    localBasicHttpContext.setAttribute("http.cookiespec-registry", getCookieSpecs());
    localBasicHttpContext.setAttribute("http.cookie-store", getCookieStore());
    localBasicHttpContext.setAttribute("http.auth.credentials-provider", getCredentialsProvider());
    return localBasicHttpContext;
  }
  
  protected abstract HttpParams createHttpParams();
  
  protected abstract BasicHttpProcessor createHttpProcessor();
  
  protected HttpRequestRetryHandler createHttpRequestRetryHandler()
  {
    return new DefaultHttpRequestRetryHandler();
  }
  
  protected HttpRoutePlanner createHttpRoutePlanner()
  {
    return new DefaultHttpRoutePlanner(getConnectionManager().getSchemeRegistry());
  }
  
  @Deprecated
  protected AuthenticationHandler createProxyAuthenticationHandler()
  {
    return new DefaultProxyAuthenticationHandler();
  }
  
  protected AuthenticationStrategy createProxyAuthenticationStrategy()
  {
    return new ProxyAuthenticationStrategy();
  }
  
  @Deprecated
  protected RedirectHandler createRedirectHandler()
  {
    return new DefaultRedirectHandler();
  }
  
  protected HttpRequestExecutor createRequestExecutor()
  {
    return new HttpRequestExecutor();
  }
  
  @Deprecated
  protected AuthenticationHandler createTargetAuthenticationHandler()
  {
    return new DefaultTargetAuthenticationHandler();
  }
  
  protected AuthenticationStrategy createTargetAuthenticationStrategy()
  {
    return new TargetAuthenticationStrategy();
  }
  
  protected UserTokenHandler createUserTokenHandler()
  {
    return new DefaultUserTokenHandler();
  }
  
  protected HttpParams determineParams(HttpRequest paramHttpRequest)
  {
    return new ClientParamsStack(null, getParams(), paramHttpRequest.getParams(), null);
  }
  
  /* Error */
  protected final cz.msebera.android.httpclient.client.methods.CloseableHttpResponse doExecute(cz.msebera.android.httpclient.HttpHost paramHttpHost, HttpRequest paramHttpRequest, HttpContext paramHttpContext)
    throws java.io.IOException, cz.msebera.android.httpclient.client.ClientProtocolException
  {
    // Byte code:
    //   0: aload_2
    //   1: ldc_w 414
    //   4: invokestatic 420	cz/msebera/android/httpclient/util/Args:notNull	(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
    //   7: pop
    //   8: aload_0
    //   9: monitorenter
    //   10: aload_0
    //   11: invokevirtual 422	cz/msebera/android/httpclient/impl/client/AbstractHttpClient:createHttpContext	()Lcz/msebera/android/httpclient/protocol/HttpContext;
    //   14: astore 4
    //   16: aload_3
    //   17: ifnonnull +165 -> 182
    //   20: aload 4
    //   22: astore_3
    //   23: aload_0
    //   24: aload_2
    //   25: invokevirtual 424	cz/msebera/android/httpclient/impl/client/AbstractHttpClient:determineParams	(Lcz/msebera/android/httpclient/HttpRequest;)Lcz/msebera/android/httpclient/params/HttpParams;
    //   28: astore 4
    //   30: aload_3
    //   31: ldc_w 426
    //   34: aload 4
    //   36: invokestatic 432	cz/msebera/android/httpclient/client/params/HttpClientParamConfig:getRequestConfig	(Lcz/msebera/android/httpclient/params/HttpParams;)Lcz/msebera/android/httpclient/client/config/RequestConfig;
    //   39: invokeinterface 325 3 0
    //   44: aload_0
    //   45: aload_0
    //   46: invokevirtual 435	cz/msebera/android/httpclient/impl/client/AbstractHttpClient:getRequestExecutor	()Lcz/msebera/android/httpclient/protocol/HttpRequestExecutor;
    //   49: aload_0
    //   50: invokevirtual 131	cz/msebera/android/httpclient/impl/client/AbstractHttpClient:getConnectionManager	()Lcz/msebera/android/httpclient/conn/ClientConnectionManager;
    //   53: aload_0
    //   54: invokevirtual 438	cz/msebera/android/httpclient/impl/client/AbstractHttpClient:getConnectionReuseStrategy	()Lcz/msebera/android/httpclient/ConnectionReuseStrategy;
    //   57: aload_0
    //   58: invokevirtual 441	cz/msebera/android/httpclient/impl/client/AbstractHttpClient:getConnectionKeepAliveStrategy	()Lcz/msebera/android/httpclient/conn/ConnectionKeepAliveStrategy;
    //   61: aload_0
    //   62: invokevirtual 444	cz/msebera/android/httpclient/impl/client/AbstractHttpClient:getRoutePlanner	()Lcz/msebera/android/httpclient/conn/routing/HttpRoutePlanner;
    //   65: aload_0
    //   66: invokespecial 446	cz/msebera/android/httpclient/impl/client/AbstractHttpClient:getProtocolProcessor	()Lcz/msebera/android/httpclient/protocol/HttpProcessor;
    //   69: aload_0
    //   70: invokevirtual 449	cz/msebera/android/httpclient/impl/client/AbstractHttpClient:getHttpRequestRetryHandler	()Lcz/msebera/android/httpclient/client/HttpRequestRetryHandler;
    //   73: aload_0
    //   74: invokevirtual 453	cz/msebera/android/httpclient/impl/client/AbstractHttpClient:getRedirectStrategy	()Lcz/msebera/android/httpclient/client/RedirectStrategy;
    //   77: aload_0
    //   78: invokevirtual 456	cz/msebera/android/httpclient/impl/client/AbstractHttpClient:getTargetAuthenticationStrategy	()Lcz/msebera/android/httpclient/client/AuthenticationStrategy;
    //   81: aload_0
    //   82: invokevirtual 459	cz/msebera/android/httpclient/impl/client/AbstractHttpClient:getProxyAuthenticationStrategy	()Lcz/msebera/android/httpclient/client/AuthenticationStrategy;
    //   85: aload_0
    //   86: invokevirtual 462	cz/msebera/android/httpclient/impl/client/AbstractHttpClient:getUserTokenHandler	()Lcz/msebera/android/httpclient/client/UserTokenHandler;
    //   89: aload 4
    //   91: invokevirtual 464	cz/msebera/android/httpclient/impl/client/AbstractHttpClient:createClientRequestDirector	(Lcz/msebera/android/httpclient/protocol/HttpRequestExecutor;Lcz/msebera/android/httpclient/conn/ClientConnectionManager;Lcz/msebera/android/httpclient/ConnectionReuseStrategy;Lcz/msebera/android/httpclient/conn/ConnectionKeepAliveStrategy;Lcz/msebera/android/httpclient/conn/routing/HttpRoutePlanner;Lcz/msebera/android/httpclient/protocol/HttpProcessor;Lcz/msebera/android/httpclient/client/HttpRequestRetryHandler;Lcz/msebera/android/httpclient/client/RedirectStrategy;Lcz/msebera/android/httpclient/client/AuthenticationStrategy;Lcz/msebera/android/httpclient/client/AuthenticationStrategy;Lcz/msebera/android/httpclient/client/UserTokenHandler;Lcz/msebera/android/httpclient/params/HttpParams;)Lcz/msebera/android/httpclient/client/RequestDirector;
    //   94: astore 7
    //   96: aload_0
    //   97: invokevirtual 444	cz/msebera/android/httpclient/impl/client/AbstractHttpClient:getRoutePlanner	()Lcz/msebera/android/httpclient/conn/routing/HttpRoutePlanner;
    //   100: astore 8
    //   102: aload_0
    //   103: invokevirtual 468	cz/msebera/android/httpclient/impl/client/AbstractHttpClient:getConnectionBackoffStrategy	()Lcz/msebera/android/httpclient/client/ConnectionBackoffStrategy;
    //   106: astore 5
    //   108: aload_0
    //   109: invokevirtual 472	cz/msebera/android/httpclient/impl/client/AbstractHttpClient:getBackoffManager	()Lcz/msebera/android/httpclient/client/BackoffManager;
    //   112: astore 6
    //   114: aload_0
    //   115: monitorexit
    //   116: aload 5
    //   118: ifnull +202 -> 320
    //   121: aload 6
    //   123: ifnull +197 -> 320
    //   126: aload_1
    //   127: ifnull +74 -> 201
    //   130: aload_1
    //   131: astore 4
    //   133: aload 8
    //   135: aload 4
    //   137: aload_2
    //   138: aload_3
    //   139: invokeinterface 478 4 0
    //   144: astore 4
    //   146: aload 7
    //   148: aload_1
    //   149: aload_2
    //   150: aload_3
    //   151: invokeinterface 484 4 0
    //   156: invokestatic 490	cz/msebera/android/httpclient/impl/client/CloseableHttpResponseProxy:newProxy	(Lcz/msebera/android/httpclient/HttpResponse;)Lcz/msebera/android/httpclient/client/methods/CloseableHttpResponse;
    //   159: astore_1
    //   160: aload 5
    //   162: aload_1
    //   163: invokeinterface 496 2 0
    //   168: ifeq +141 -> 309
    //   171: aload 6
    //   173: aload 4
    //   175: invokeinterface 502 2 0
    //   180: aload_1
    //   181: areturn
    //   182: new 504	cz/msebera/android/httpclient/protocol/DefaultedHttpContext
    //   185: dup
    //   186: aload_3
    //   187: aload 4
    //   189: invokespecial 507	cz/msebera/android/httpclient/protocol/DefaultedHttpContext:<init>	(Lcz/msebera/android/httpclient/protocol/HttpContext;Lcz/msebera/android/httpclient/protocol/HttpContext;)V
    //   192: astore_3
    //   193: goto -170 -> 23
    //   196: astore_1
    //   197: aload_0
    //   198: monitorexit
    //   199: aload_1
    //   200: athrow
    //   201: aload_0
    //   202: aload_2
    //   203: invokevirtual 424	cz/msebera/android/httpclient/impl/client/AbstractHttpClient:determineParams	(Lcz/msebera/android/httpclient/HttpRequest;)Lcz/msebera/android/httpclient/params/HttpParams;
    //   206: ldc_w 509
    //   209: invokeinterface 185 2 0
    //   214: checkcast 511	cz/msebera/android/httpclient/HttpHost
    //   217: astore 4
    //   219: goto -86 -> 133
    //   222: astore_1
    //   223: aload 5
    //   225: aload_1
    //   226: invokeinterface 514 2 0
    //   231: ifeq +12 -> 243
    //   234: aload 6
    //   236: aload 4
    //   238: invokeinterface 502 2 0
    //   243: aload_1
    //   244: athrow
    //   245: astore_1
    //   246: new 406	cz/msebera/android/httpclient/client/ClientProtocolException
    //   249: dup
    //   250: aload_1
    //   251: invokespecial 517	cz/msebera/android/httpclient/client/ClientProtocolException:<init>	(Ljava/lang/Throwable;)V
    //   254: athrow
    //   255: astore_1
    //   256: aload 5
    //   258: aload_1
    //   259: invokeinterface 514 2 0
    //   264: ifeq +12 -> 276
    //   267: aload 6
    //   269: aload 4
    //   271: invokeinterface 502 2 0
    //   276: aload_1
    //   277: instanceof 408
    //   280: ifeq +8 -> 288
    //   283: aload_1
    //   284: checkcast 408	cz/msebera/android/httpclient/HttpException
    //   287: athrow
    //   288: aload_1
    //   289: instanceof 404
    //   292: ifeq +8 -> 300
    //   295: aload_1
    //   296: checkcast 404	java/io/IOException
    //   299: athrow
    //   300: new 519	java/lang/reflect/UndeclaredThrowableException
    //   303: dup
    //   304: aload_1
    //   305: invokespecial 520	java/lang/reflect/UndeclaredThrowableException:<init>	(Ljava/lang/Throwable;)V
    //   308: athrow
    //   309: aload 6
    //   311: aload 4
    //   313: invokeinterface 523 2 0
    //   318: aload_1
    //   319: areturn
    //   320: aload 7
    //   322: aload_1
    //   323: aload_2
    //   324: aload_3
    //   325: invokeinterface 484 4 0
    //   330: invokestatic 490	cz/msebera/android/httpclient/impl/client/CloseableHttpResponseProxy:newProxy	(Lcz/msebera/android/httpclient/HttpResponse;)Lcz/msebera/android/httpclient/client/methods/CloseableHttpResponse;
    //   333: astore_1
    //   334: aload_1
    //   335: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	336	0	this	AbstractHttpClient
    //   0	336	1	paramHttpHost	cz.msebera.android.httpclient.HttpHost
    //   0	336	2	paramHttpRequest	HttpRequest
    //   0	336	3	paramHttpContext	HttpContext
    //   14	298	4	localObject	Object
    //   106	151	5	localConnectionBackoffStrategy	ConnectionBackoffStrategy
    //   112	198	6	localBackoffManager	BackoffManager
    //   94	227	7	localRequestDirector	RequestDirector
    //   100	34	8	localHttpRoutePlanner	HttpRoutePlanner
    // Exception table:
    //   from	to	target	type
    //   10	16	196	finally
    //   23	116	196	finally
    //   182	193	196	finally
    //   197	199	196	finally
    //   146	160	222	java/lang/RuntimeException
    //   133	146	245	cz/msebera/android/httpclient/HttpException
    //   146	160	245	cz/msebera/android/httpclient/HttpException
    //   160	180	245	cz/msebera/android/httpclient/HttpException
    //   201	219	245	cz/msebera/android/httpclient/HttpException
    //   223	243	245	cz/msebera/android/httpclient/HttpException
    //   243	245	245	cz/msebera/android/httpclient/HttpException
    //   256	276	245	cz/msebera/android/httpclient/HttpException
    //   276	288	245	cz/msebera/android/httpclient/HttpException
    //   288	300	245	cz/msebera/android/httpclient/HttpException
    //   300	309	245	cz/msebera/android/httpclient/HttpException
    //   309	318	245	cz/msebera/android/httpclient/HttpException
    //   320	334	245	cz/msebera/android/httpclient/HttpException
    //   146	160	255	java/lang/Exception
  }
  
  public final AuthSchemeRegistry getAuthSchemes()
  {
    try
    {
      if (this.supportedAuthSchemes == null) {
        this.supportedAuthSchemes = createAuthSchemeRegistry();
      }
      AuthSchemeRegistry localAuthSchemeRegistry = this.supportedAuthSchemes;
      return localAuthSchemeRegistry;
    }
    finally {}
  }
  
  public final BackoffManager getBackoffManager()
  {
    try
    {
      BackoffManager localBackoffManager = this.backoffManager;
      return localBackoffManager;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public final ConnectionBackoffStrategy getConnectionBackoffStrategy()
  {
    try
    {
      ConnectionBackoffStrategy localConnectionBackoffStrategy = this.connectionBackoffStrategy;
      return localConnectionBackoffStrategy;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public final ConnectionKeepAliveStrategy getConnectionKeepAliveStrategy()
  {
    try
    {
      if (this.keepAliveStrategy == null) {
        this.keepAliveStrategy = createConnectionKeepAliveStrategy();
      }
      ConnectionKeepAliveStrategy localConnectionKeepAliveStrategy = this.keepAliveStrategy;
      return localConnectionKeepAliveStrategy;
    }
    finally {}
  }
  
  public final ClientConnectionManager getConnectionManager()
  {
    try
    {
      if (this.connManager == null) {
        this.connManager = createClientConnectionManager();
      }
      ClientConnectionManager localClientConnectionManager = this.connManager;
      return localClientConnectionManager;
    }
    finally {}
  }
  
  public final ConnectionReuseStrategy getConnectionReuseStrategy()
  {
    try
    {
      if (this.reuseStrategy == null) {
        this.reuseStrategy = createConnectionReuseStrategy();
      }
      ConnectionReuseStrategy localConnectionReuseStrategy = this.reuseStrategy;
      return localConnectionReuseStrategy;
    }
    finally {}
  }
  
  public final CookieSpecRegistry getCookieSpecs()
  {
    try
    {
      if (this.supportedCookieSpecs == null) {
        this.supportedCookieSpecs = createCookieSpecRegistry();
      }
      CookieSpecRegistry localCookieSpecRegistry = this.supportedCookieSpecs;
      return localCookieSpecRegistry;
    }
    finally {}
  }
  
  public final CookieStore getCookieStore()
  {
    try
    {
      if (this.cookieStore == null) {
        this.cookieStore = createCookieStore();
      }
      CookieStore localCookieStore = this.cookieStore;
      return localCookieStore;
    }
    finally {}
  }
  
  public final CredentialsProvider getCredentialsProvider()
  {
    try
    {
      if (this.credsProvider == null) {
        this.credsProvider = createCredentialsProvider();
      }
      CredentialsProvider localCredentialsProvider = this.credsProvider;
      return localCredentialsProvider;
    }
    finally {}
  }
  
  protected final BasicHttpProcessor getHttpProcessor()
  {
    try
    {
      if (this.mutableProcessor == null) {
        this.mutableProcessor = createHttpProcessor();
      }
      BasicHttpProcessor localBasicHttpProcessor = this.mutableProcessor;
      return localBasicHttpProcessor;
    }
    finally {}
  }
  
  public final HttpRequestRetryHandler getHttpRequestRetryHandler()
  {
    try
    {
      if (this.retryHandler == null) {
        this.retryHandler = createHttpRequestRetryHandler();
      }
      HttpRequestRetryHandler localHttpRequestRetryHandler = this.retryHandler;
      return localHttpRequestRetryHandler;
    }
    finally {}
  }
  
  public final HttpParams getParams()
  {
    try
    {
      if (this.defaultParams == null) {
        this.defaultParams = createHttpParams();
      }
      HttpParams localHttpParams = this.defaultParams;
      return localHttpParams;
    }
    finally {}
  }
  
  @Deprecated
  public final AuthenticationHandler getProxyAuthenticationHandler()
  {
    try
    {
      AuthenticationHandler localAuthenticationHandler = createProxyAuthenticationHandler();
      return localAuthenticationHandler;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public final AuthenticationStrategy getProxyAuthenticationStrategy()
  {
    try
    {
      if (this.proxyAuthStrategy == null) {
        this.proxyAuthStrategy = createProxyAuthenticationStrategy();
      }
      AuthenticationStrategy localAuthenticationStrategy = this.proxyAuthStrategy;
      return localAuthenticationStrategy;
    }
    finally {}
  }
  
  @Deprecated
  public final RedirectHandler getRedirectHandler()
  {
    try
    {
      RedirectHandler localRedirectHandler = createRedirectHandler();
      return localRedirectHandler;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public final RedirectStrategy getRedirectStrategy()
  {
    try
    {
      if (this.redirectStrategy == null) {
        this.redirectStrategy = new DefaultRedirectStrategy();
      }
      RedirectStrategy localRedirectStrategy = this.redirectStrategy;
      return localRedirectStrategy;
    }
    finally {}
  }
  
  public final HttpRequestExecutor getRequestExecutor()
  {
    try
    {
      if (this.requestExec == null) {
        this.requestExec = createRequestExecutor();
      }
      HttpRequestExecutor localHttpRequestExecutor = this.requestExec;
      return localHttpRequestExecutor;
    }
    finally {}
  }
  
  public HttpRequestInterceptor getRequestInterceptor(int paramInt)
  {
    try
    {
      HttpRequestInterceptor localHttpRequestInterceptor = getHttpProcessor().getRequestInterceptor(paramInt);
      return localHttpRequestInterceptor;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public int getRequestInterceptorCount()
  {
    try
    {
      int i = getHttpProcessor().getRequestInterceptorCount();
      return i;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public HttpResponseInterceptor getResponseInterceptor(int paramInt)
  {
    try
    {
      HttpResponseInterceptor localHttpResponseInterceptor = getHttpProcessor().getResponseInterceptor(paramInt);
      return localHttpResponseInterceptor;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public int getResponseInterceptorCount()
  {
    try
    {
      int i = getHttpProcessor().getResponseInterceptorCount();
      return i;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public final HttpRoutePlanner getRoutePlanner()
  {
    try
    {
      if (this.routePlanner == null) {
        this.routePlanner = createHttpRoutePlanner();
      }
      HttpRoutePlanner localHttpRoutePlanner = this.routePlanner;
      return localHttpRoutePlanner;
    }
    finally {}
  }
  
  @Deprecated
  public final AuthenticationHandler getTargetAuthenticationHandler()
  {
    try
    {
      AuthenticationHandler localAuthenticationHandler = createTargetAuthenticationHandler();
      return localAuthenticationHandler;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public final AuthenticationStrategy getTargetAuthenticationStrategy()
  {
    try
    {
      if (this.targetAuthStrategy == null) {
        this.targetAuthStrategy = createTargetAuthenticationStrategy();
      }
      AuthenticationStrategy localAuthenticationStrategy = this.targetAuthStrategy;
      return localAuthenticationStrategy;
    }
    finally {}
  }
  
  public final UserTokenHandler getUserTokenHandler()
  {
    try
    {
      if (this.userTokenHandler == null) {
        this.userTokenHandler = createUserTokenHandler();
      }
      UserTokenHandler localUserTokenHandler = this.userTokenHandler;
      return localUserTokenHandler;
    }
    finally {}
  }
  
  public void removeRequestInterceptorByClass(Class<? extends HttpRequestInterceptor> paramClass)
  {
    try
    {
      getHttpProcessor().removeRequestInterceptorByClass(paramClass);
      this.protocolProcessor = null;
      return;
    }
    finally
    {
      paramClass = finally;
      throw paramClass;
    }
  }
  
  public void removeResponseInterceptorByClass(Class<? extends HttpResponseInterceptor> paramClass)
  {
    try
    {
      getHttpProcessor().removeResponseInterceptorByClass(paramClass);
      this.protocolProcessor = null;
      return;
    }
    finally
    {
      paramClass = finally;
      throw paramClass;
    }
  }
  
  public void setAuthSchemes(AuthSchemeRegistry paramAuthSchemeRegistry)
  {
    try
    {
      this.supportedAuthSchemes = paramAuthSchemeRegistry;
      return;
    }
    finally
    {
      paramAuthSchemeRegistry = finally;
      throw paramAuthSchemeRegistry;
    }
  }
  
  public void setBackoffManager(BackoffManager paramBackoffManager)
  {
    try
    {
      this.backoffManager = paramBackoffManager;
      return;
    }
    finally
    {
      paramBackoffManager = finally;
      throw paramBackoffManager;
    }
  }
  
  public void setConnectionBackoffStrategy(ConnectionBackoffStrategy paramConnectionBackoffStrategy)
  {
    try
    {
      this.connectionBackoffStrategy = paramConnectionBackoffStrategy;
      return;
    }
    finally
    {
      paramConnectionBackoffStrategy = finally;
      throw paramConnectionBackoffStrategy;
    }
  }
  
  public void setCookieSpecs(CookieSpecRegistry paramCookieSpecRegistry)
  {
    try
    {
      this.supportedCookieSpecs = paramCookieSpecRegistry;
      return;
    }
    finally
    {
      paramCookieSpecRegistry = finally;
      throw paramCookieSpecRegistry;
    }
  }
  
  public void setCookieStore(CookieStore paramCookieStore)
  {
    try
    {
      this.cookieStore = paramCookieStore;
      return;
    }
    finally
    {
      paramCookieStore = finally;
      throw paramCookieStore;
    }
  }
  
  public void setCredentialsProvider(CredentialsProvider paramCredentialsProvider)
  {
    try
    {
      this.credsProvider = paramCredentialsProvider;
      return;
    }
    finally
    {
      paramCredentialsProvider = finally;
      throw paramCredentialsProvider;
    }
  }
  
  public void setHttpRequestRetryHandler(HttpRequestRetryHandler paramHttpRequestRetryHandler)
  {
    try
    {
      this.retryHandler = paramHttpRequestRetryHandler;
      return;
    }
    finally
    {
      paramHttpRequestRetryHandler = finally;
      throw paramHttpRequestRetryHandler;
    }
  }
  
  public void setKeepAliveStrategy(ConnectionKeepAliveStrategy paramConnectionKeepAliveStrategy)
  {
    try
    {
      this.keepAliveStrategy = paramConnectionKeepAliveStrategy;
      return;
    }
    finally
    {
      paramConnectionKeepAliveStrategy = finally;
      throw paramConnectionKeepAliveStrategy;
    }
  }
  
  public void setParams(HttpParams paramHttpParams)
  {
    try
    {
      this.defaultParams = paramHttpParams;
      return;
    }
    finally
    {
      paramHttpParams = finally;
      throw paramHttpParams;
    }
  }
  
  @Deprecated
  public void setProxyAuthenticationHandler(AuthenticationHandler paramAuthenticationHandler)
  {
    try
    {
      this.proxyAuthStrategy = new AuthenticationStrategyAdaptor(paramAuthenticationHandler);
      return;
    }
    finally
    {
      paramAuthenticationHandler = finally;
      throw paramAuthenticationHandler;
    }
  }
  
  public void setProxyAuthenticationStrategy(AuthenticationStrategy paramAuthenticationStrategy)
  {
    try
    {
      this.proxyAuthStrategy = paramAuthenticationStrategy;
      return;
    }
    finally
    {
      paramAuthenticationStrategy = finally;
      throw paramAuthenticationStrategy;
    }
  }
  
  @Deprecated
  public void setRedirectHandler(RedirectHandler paramRedirectHandler)
  {
    try
    {
      this.redirectStrategy = new DefaultRedirectStrategyAdaptor(paramRedirectHandler);
      return;
    }
    finally
    {
      paramRedirectHandler = finally;
      throw paramRedirectHandler;
    }
  }
  
  public void setRedirectStrategy(RedirectStrategy paramRedirectStrategy)
  {
    try
    {
      this.redirectStrategy = paramRedirectStrategy;
      return;
    }
    finally
    {
      paramRedirectStrategy = finally;
      throw paramRedirectStrategy;
    }
  }
  
  public void setReuseStrategy(ConnectionReuseStrategy paramConnectionReuseStrategy)
  {
    try
    {
      this.reuseStrategy = paramConnectionReuseStrategy;
      return;
    }
    finally
    {
      paramConnectionReuseStrategy = finally;
      throw paramConnectionReuseStrategy;
    }
  }
  
  public void setRoutePlanner(HttpRoutePlanner paramHttpRoutePlanner)
  {
    try
    {
      this.routePlanner = paramHttpRoutePlanner;
      return;
    }
    finally
    {
      paramHttpRoutePlanner = finally;
      throw paramHttpRoutePlanner;
    }
  }
  
  @Deprecated
  public void setTargetAuthenticationHandler(AuthenticationHandler paramAuthenticationHandler)
  {
    try
    {
      this.targetAuthStrategy = new AuthenticationStrategyAdaptor(paramAuthenticationHandler);
      return;
    }
    finally
    {
      paramAuthenticationHandler = finally;
      throw paramAuthenticationHandler;
    }
  }
  
  public void setTargetAuthenticationStrategy(AuthenticationStrategy paramAuthenticationStrategy)
  {
    try
    {
      this.targetAuthStrategy = paramAuthenticationStrategy;
      return;
    }
    finally
    {
      paramAuthenticationStrategy = finally;
      throw paramAuthenticationStrategy;
    }
  }
  
  public void setUserTokenHandler(UserTokenHandler paramUserTokenHandler)
  {
    try
    {
      this.userTokenHandler = paramUserTokenHandler;
      return;
    }
    finally
    {
      paramUserTokenHandler = finally;
      throw paramUserTokenHandler;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/cz/msebera/android/httpclient/impl/client/AbstractHttpClient.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */