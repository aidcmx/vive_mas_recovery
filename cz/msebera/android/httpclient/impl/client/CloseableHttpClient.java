package cz.msebera.android.httpclient.impl.client;

import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.annotation.ThreadSafe;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.ResponseHandler;
import cz.msebera.android.httpclient.client.methods.CloseableHttpResponse;
import cz.msebera.android.httpclient.client.methods.HttpUriRequest;
import cz.msebera.android.httpclient.client.utils.URIUtils;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.protocol.HttpContext;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.EntityUtils;
import java.io.Closeable;
import java.io.IOException;
import java.lang.reflect.UndeclaredThrowableException;
import java.net.URI;

@ThreadSafe
public abstract class CloseableHttpClient
  implements HttpClient, Closeable
{
  public HttpClientAndroidLog log = new HttpClientAndroidLog(getClass());
  
  private static HttpHost determineTarget(HttpUriRequest paramHttpUriRequest)
    throws ClientProtocolException
  {
    HttpHost localHttpHost = null;
    URI localURI = paramHttpUriRequest.getURI();
    paramHttpUriRequest = localHttpHost;
    if (localURI.isAbsolute())
    {
      localHttpHost = URIUtils.extractHost(localURI);
      paramHttpUriRequest = localHttpHost;
      if (localHttpHost == null) {
        throw new ClientProtocolException("URI does not specify a valid host name: " + localURI);
      }
    }
    return paramHttpUriRequest;
  }
  
  protected abstract CloseableHttpResponse doExecute(HttpHost paramHttpHost, HttpRequest paramHttpRequest, HttpContext paramHttpContext)
    throws IOException, ClientProtocolException;
  
  public CloseableHttpResponse execute(HttpHost paramHttpHost, HttpRequest paramHttpRequest)
    throws IOException, ClientProtocolException
  {
    return doExecute(paramHttpHost, paramHttpRequest, (HttpContext)null);
  }
  
  public CloseableHttpResponse execute(HttpHost paramHttpHost, HttpRequest paramHttpRequest, HttpContext paramHttpContext)
    throws IOException, ClientProtocolException
  {
    return doExecute(paramHttpHost, paramHttpRequest, paramHttpContext);
  }
  
  public CloseableHttpResponse execute(HttpUriRequest paramHttpUriRequest)
    throws IOException, ClientProtocolException
  {
    return execute(paramHttpUriRequest, (HttpContext)null);
  }
  
  public CloseableHttpResponse execute(HttpUriRequest paramHttpUriRequest, HttpContext paramHttpContext)
    throws IOException, ClientProtocolException
  {
    Args.notNull(paramHttpUriRequest, "HTTP request");
    return doExecute(determineTarget(paramHttpUriRequest), paramHttpUriRequest, paramHttpContext);
  }
  
  public <T> T execute(HttpHost paramHttpHost, HttpRequest paramHttpRequest, ResponseHandler<? extends T> paramResponseHandler)
    throws IOException, ClientProtocolException
  {
    return (T)execute(paramHttpHost, paramHttpRequest, paramResponseHandler, null);
  }
  
  public <T> T execute(HttpHost paramHttpHost, HttpRequest paramHttpRequest, ResponseHandler<? extends T> paramResponseHandler, HttpContext paramHttpContext)
    throws IOException, ClientProtocolException
  {
    Args.notNull(paramResponseHandler, "Response handler");
    paramHttpHost = execute(paramHttpHost, paramHttpRequest, paramHttpContext);
    try
    {
      paramHttpRequest = paramResponseHandler.handleResponse(paramHttpHost);
      EntityUtils.consume(paramHttpHost.getEntity());
      return paramHttpRequest;
    }
    catch (Exception paramHttpRequest)
    {
      paramHttpHost = paramHttpHost.getEntity();
      try
      {
        EntityUtils.consume(paramHttpHost);
        if ((paramHttpRequest instanceof RuntimeException)) {
          throw ((RuntimeException)paramHttpRequest);
        }
      }
      catch (Exception paramHttpHost)
      {
        for (;;)
        {
          this.log.warn("Error consuming content after an exception.", paramHttpHost);
        }
        if ((paramHttpRequest instanceof IOException)) {
          throw ((IOException)paramHttpRequest);
        }
        throw new UndeclaredThrowableException(paramHttpRequest);
      }
    }
  }
  
  public <T> T execute(HttpUriRequest paramHttpUriRequest, ResponseHandler<? extends T> paramResponseHandler)
    throws IOException, ClientProtocolException
  {
    return (T)execute(paramHttpUriRequest, paramResponseHandler, null);
  }
  
  public <T> T execute(HttpUriRequest paramHttpUriRequest, ResponseHandler<? extends T> paramResponseHandler, HttpContext paramHttpContext)
    throws IOException, ClientProtocolException
  {
    return (T)execute(determineTarget(paramHttpUriRequest), paramHttpUriRequest, paramResponseHandler, paramHttpContext);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/cz/msebera/android/httpclient/impl/client/CloseableHttpClient.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */