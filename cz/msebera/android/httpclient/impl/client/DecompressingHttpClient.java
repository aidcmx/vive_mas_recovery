package cz.msebera.android.httpclient.impl.client;

import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpRequestInterceptor;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.HttpResponseInterceptor;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.ResponseHandler;
import cz.msebera.android.httpclient.client.methods.HttpUriRequest;
import cz.msebera.android.httpclient.client.protocol.RequestAcceptEncoding;
import cz.msebera.android.httpclient.client.protocol.ResponseContentEncoding;
import cz.msebera.android.httpclient.client.utils.URIUtils;
import cz.msebera.android.httpclient.conn.ClientConnectionManager;
import cz.msebera.android.httpclient.params.HttpParams;
import cz.msebera.android.httpclient.protocol.HttpContext;
import cz.msebera.android.httpclient.util.EntityUtils;
import java.io.IOException;

@Deprecated
public class DecompressingHttpClient
  implements HttpClient
{
  private final HttpRequestInterceptor acceptEncodingInterceptor;
  private final HttpClient backend;
  private final HttpResponseInterceptor contentEncodingInterceptor;
  
  public DecompressingHttpClient()
  {
    this(new DefaultHttpClient());
  }
  
  public DecompressingHttpClient(HttpClient paramHttpClient)
  {
    this(paramHttpClient, new RequestAcceptEncoding(), new ResponseContentEncoding());
  }
  
  DecompressingHttpClient(HttpClient paramHttpClient, HttpRequestInterceptor paramHttpRequestInterceptor, HttpResponseInterceptor paramHttpResponseInterceptor)
  {
    this.backend = paramHttpClient;
    this.acceptEncodingInterceptor = paramHttpRequestInterceptor;
    this.contentEncodingInterceptor = paramHttpResponseInterceptor;
  }
  
  public HttpResponse execute(HttpHost paramHttpHost, HttpRequest paramHttpRequest)
    throws IOException, ClientProtocolException
  {
    return execute(paramHttpHost, paramHttpRequest, (HttpContext)null);
  }
  
  /* Error */
  public HttpResponse execute(HttpHost paramHttpHost, HttpRequest paramHttpRequest, HttpContext paramHttpContext)
    throws IOException, ClientProtocolException
  {
    // Byte code:
    //   0: aload_3
    //   1: ifnull +100 -> 101
    //   4: aload_2
    //   5: instanceof 57
    //   8: ifeq +114 -> 122
    //   11: new 59	cz/msebera/android/httpclient/impl/client/EntityEnclosingRequestWrapper
    //   14: dup
    //   15: aload_2
    //   16: checkcast 57	cz/msebera/android/httpclient/HttpEntityEnclosingRequest
    //   19: invokespecial 62	cz/msebera/android/httpclient/impl/client/EntityEnclosingRequestWrapper:<init>	(Lcz/msebera/android/httpclient/HttpEntityEnclosingRequest;)V
    //   22: astore_2
    //   23: aload_0
    //   24: getfield 37	cz/msebera/android/httpclient/impl/client/DecompressingHttpClient:acceptEncodingInterceptor	Lcz/msebera/android/httpclient/HttpRequestInterceptor;
    //   27: aload_2
    //   28: aload_3
    //   29: invokeinterface 68 3 0
    //   34: aload_0
    //   35: getfield 35	cz/msebera/android/httpclient/impl/client/DecompressingHttpClient:backend	Lcz/msebera/android/httpclient/client/HttpClient;
    //   38: aload_1
    //   39: aload_2
    //   40: aload_3
    //   41: invokeinterface 69 4 0
    //   46: astore_1
    //   47: aload_0
    //   48: getfield 39	cz/msebera/android/httpclient/impl/client/DecompressingHttpClient:contentEncodingInterceptor	Lcz/msebera/android/httpclient/HttpResponseInterceptor;
    //   51: aload_1
    //   52: aload_3
    //   53: invokeinterface 74 3 0
    //   58: getstatic 80	java/lang/Boolean:TRUE	Ljava/lang/Boolean;
    //   61: aload_3
    //   62: ldc 82
    //   64: invokeinterface 86 2 0
    //   69: invokevirtual 90	java/lang/Boolean:equals	(Ljava/lang/Object;)Z
    //   72: ifeq +27 -> 99
    //   75: aload_1
    //   76: ldc 92
    //   78: invokeinterface 98 2 0
    //   83: aload_1
    //   84: ldc 100
    //   86: invokeinterface 98 2 0
    //   91: aload_1
    //   92: ldc 102
    //   94: invokeinterface 98 2 0
    //   99: aload_1
    //   100: areturn
    //   101: new 104	cz/msebera/android/httpclient/protocol/BasicHttpContext
    //   104: dup
    //   105: invokespecial 105	cz/msebera/android/httpclient/protocol/BasicHttpContext:<init>	()V
    //   108: astore_3
    //   109: goto -105 -> 4
    //   112: astore_1
    //   113: new 45	cz/msebera/android/httpclient/client/ClientProtocolException
    //   116: dup
    //   117: aload_1
    //   118: invokespecial 108	cz/msebera/android/httpclient/client/ClientProtocolException:<init>	(Ljava/lang/Throwable;)V
    //   121: athrow
    //   122: new 110	cz/msebera/android/httpclient/impl/client/RequestWrapper
    //   125: dup
    //   126: aload_2
    //   127: invokespecial 113	cz/msebera/android/httpclient/impl/client/RequestWrapper:<init>	(Lcz/msebera/android/httpclient/HttpRequest;)V
    //   130: astore_2
    //   131: goto -108 -> 23
    //   134: astore_2
    //   135: aload_1
    //   136: invokeinterface 117 1 0
    //   141: invokestatic 123	cz/msebera/android/httpclient/util/EntityUtils:consume	(Lcz/msebera/android/httpclient/HttpEntity;)V
    //   144: aload_2
    //   145: athrow
    //   146: astore_2
    //   147: aload_1
    //   148: invokeinterface 117 1 0
    //   153: invokestatic 123	cz/msebera/android/httpclient/util/EntityUtils:consume	(Lcz/msebera/android/httpclient/HttpEntity;)V
    //   156: aload_2
    //   157: athrow
    //   158: astore_2
    //   159: aload_1
    //   160: invokeinterface 117 1 0
    //   165: invokestatic 123	cz/msebera/android/httpclient/util/EntityUtils:consume	(Lcz/msebera/android/httpclient/HttpEntity;)V
    //   168: aload_2
    //   169: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	170	0	this	DecompressingHttpClient
    //   0	170	1	paramHttpHost	HttpHost
    //   0	170	2	paramHttpRequest	HttpRequest
    //   0	170	3	paramHttpContext	HttpContext
    // Exception table:
    //   from	to	target	type
    //   4	23	112	cz/msebera/android/httpclient/HttpException
    //   23	47	112	cz/msebera/android/httpclient/HttpException
    //   101	109	112	cz/msebera/android/httpclient/HttpException
    //   122	131	112	cz/msebera/android/httpclient/HttpException
    //   135	146	112	cz/msebera/android/httpclient/HttpException
    //   147	158	112	cz/msebera/android/httpclient/HttpException
    //   159	170	112	cz/msebera/android/httpclient/HttpException
    //   47	99	134	cz/msebera/android/httpclient/HttpException
    //   47	99	146	java/io/IOException
    //   47	99	158	java/lang/RuntimeException
  }
  
  public HttpResponse execute(HttpUriRequest paramHttpUriRequest)
    throws IOException, ClientProtocolException
  {
    return execute(getHttpHost(paramHttpUriRequest), paramHttpUriRequest, (HttpContext)null);
  }
  
  public HttpResponse execute(HttpUriRequest paramHttpUriRequest, HttpContext paramHttpContext)
    throws IOException, ClientProtocolException
  {
    return execute(getHttpHost(paramHttpUriRequest), paramHttpUriRequest, paramHttpContext);
  }
  
  public <T> T execute(HttpHost paramHttpHost, HttpRequest paramHttpRequest, ResponseHandler<? extends T> paramResponseHandler)
    throws IOException, ClientProtocolException
  {
    return (T)execute(paramHttpHost, paramHttpRequest, paramResponseHandler, null);
  }
  
  public <T> T execute(HttpHost paramHttpHost, HttpRequest paramHttpRequest, ResponseHandler<? extends T> paramResponseHandler, HttpContext paramHttpContext)
    throws IOException, ClientProtocolException
  {
    paramHttpHost = execute(paramHttpHost, paramHttpRequest, paramHttpContext);
    try
    {
      paramHttpRequest = paramResponseHandler.handleResponse(paramHttpHost);
      return paramHttpRequest;
    }
    finally
    {
      paramHttpHost = paramHttpHost.getEntity();
      if (paramHttpHost != null) {
        EntityUtils.consume(paramHttpHost);
      }
    }
  }
  
  public <T> T execute(HttpUriRequest paramHttpUriRequest, ResponseHandler<? extends T> paramResponseHandler)
    throws IOException, ClientProtocolException
  {
    return (T)execute(getHttpHost(paramHttpUriRequest), paramHttpUriRequest, paramResponseHandler);
  }
  
  public <T> T execute(HttpUriRequest paramHttpUriRequest, ResponseHandler<? extends T> paramResponseHandler, HttpContext paramHttpContext)
    throws IOException, ClientProtocolException
  {
    return (T)execute(getHttpHost(paramHttpUriRequest), paramHttpUriRequest, paramResponseHandler, paramHttpContext);
  }
  
  public ClientConnectionManager getConnectionManager()
  {
    return this.backend.getConnectionManager();
  }
  
  public HttpClient getHttpClient()
  {
    return this.backend;
  }
  
  HttpHost getHttpHost(HttpUriRequest paramHttpUriRequest)
  {
    return URIUtils.extractHost(paramHttpUriRequest.getURI());
  }
  
  public HttpParams getParams()
  {
    return this.backend.getParams();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/cz/msebera/android/httpclient/impl/client/DecompressingHttpClient.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */