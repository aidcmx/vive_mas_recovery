package cz.msebera.android.httpclient.impl.client;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.ProtocolException;
import cz.msebera.android.httpclient.RequestLine;
import cz.msebera.android.httpclient.StatusLine;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.client.CircularRedirectException;
import cz.msebera.android.httpclient.client.RedirectStrategy;
import cz.msebera.android.httpclient.client.config.RequestConfig;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.client.methods.HttpHead;
import cz.msebera.android.httpclient.client.methods.HttpUriRequest;
import cz.msebera.android.httpclient.client.methods.RequestBuilder;
import cz.msebera.android.httpclient.client.protocol.HttpClientContext;
import cz.msebera.android.httpclient.client.utils.URIBuilder;
import cz.msebera.android.httpclient.client.utils.URIUtils;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.protocol.HttpContext;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.Asserts;
import cz.msebera.android.httpclient.util.TextUtils;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Locale;

@Immutable
public class DefaultRedirectStrategy
  implements RedirectStrategy
{
  public static final DefaultRedirectStrategy INSTANCE = new DefaultRedirectStrategy();
  @Deprecated
  public static final String REDIRECT_LOCATIONS = "http.protocol.redirect-locations";
  private static final String[] REDIRECT_METHODS = { "GET", "HEAD" };
  public HttpClientAndroidLog log = new HttpClientAndroidLog(getClass());
  
  protected URI createLocationURI(String paramString)
    throws ProtocolException
  {
    try
    {
      Object localObject = new URIBuilder(new URI(paramString).normalize());
      String str = ((URIBuilder)localObject).getHost();
      if (str != null) {
        ((URIBuilder)localObject).setHost(str.toLowerCase(Locale.ENGLISH));
      }
      if (TextUtils.isEmpty(((URIBuilder)localObject).getPath())) {
        ((URIBuilder)localObject).setPath("/");
      }
      localObject = ((URIBuilder)localObject).build();
      return (URI)localObject;
    }
    catch (URISyntaxException localURISyntaxException)
    {
      throw new ProtocolException("Invalid redirect URI: " + paramString, localURISyntaxException);
    }
  }
  
  public URI getLocationURI(HttpRequest paramHttpRequest, HttpResponse paramHttpResponse, HttpContext paramHttpContext)
    throws ProtocolException
  {
    Args.notNull(paramHttpRequest, "HTTP request");
    Args.notNull(paramHttpResponse, "HTTP response");
    Args.notNull(paramHttpContext, "HTTP context");
    HttpClientContext localHttpClientContext = HttpClientContext.adapt(paramHttpContext);
    Object localObject = paramHttpResponse.getFirstHeader("location");
    if (localObject == null) {
      throw new ProtocolException("Received redirect response " + paramHttpResponse.getStatusLine() + " but no location header");
    }
    paramHttpResponse = ((Header)localObject).getValue();
    if (this.log.isDebugEnabled()) {
      this.log.debug("Redirect requested to location '" + paramHttpResponse + "'");
    }
    RequestConfig localRequestConfig = localHttpClientContext.getRequestConfig();
    localObject = createLocationURI(paramHttpResponse);
    paramHttpResponse = (HttpResponse)localObject;
    try
    {
      if (((URI)localObject).isAbsolute()) {
        break label249;
      }
      if (!localRequestConfig.isRelativeRedirectsAllowed()) {
        throw new ProtocolException("Relative redirect location '" + localObject + "' not allowed");
      }
    }
    catch (URISyntaxException paramHttpRequest)
    {
      throw new ProtocolException(paramHttpRequest.getMessage(), paramHttpRequest);
    }
    paramHttpResponse = localHttpClientContext.getTargetHost();
    Asserts.notNull(paramHttpResponse, "Target host");
    paramHttpResponse = URIUtils.resolve(URIUtils.rewriteURI(new URI(paramHttpRequest.getRequestLine().getUri()), paramHttpResponse, false), (URI)localObject);
    label249:
    localObject = (RedirectLocations)localHttpClientContext.getAttribute("http.protocol.redirect-locations");
    paramHttpRequest = (HttpRequest)localObject;
    if (localObject == null)
    {
      paramHttpRequest = new RedirectLocations();
      paramHttpContext.setAttribute("http.protocol.redirect-locations", paramHttpRequest);
    }
    if ((!localRequestConfig.isCircularRedirectsAllowed()) && (paramHttpRequest.contains(paramHttpResponse))) {
      throw new CircularRedirectException("Circular redirect to '" + paramHttpResponse + "'");
    }
    paramHttpRequest.add(paramHttpResponse);
    return paramHttpResponse;
  }
  
  public HttpUriRequest getRedirect(HttpRequest paramHttpRequest, HttpResponse paramHttpResponse, HttpContext paramHttpContext)
    throws ProtocolException
  {
    paramHttpContext = getLocationURI(paramHttpRequest, paramHttpResponse, paramHttpContext);
    String str = paramHttpRequest.getRequestLine().getMethod();
    if (str.equalsIgnoreCase("HEAD")) {
      return new HttpHead(paramHttpContext);
    }
    if (str.equalsIgnoreCase("GET")) {
      return new HttpGet(paramHttpContext);
    }
    if (paramHttpResponse.getStatusLine().getStatusCode() == 307) {
      return RequestBuilder.copy(paramHttpRequest).setUri(paramHttpContext).build();
    }
    return new HttpGet(paramHttpContext);
  }
  
  protected boolean isRedirectable(String paramString)
  {
    boolean bool2 = false;
    String[] arrayOfString = REDIRECT_METHODS;
    int j = arrayOfString.length;
    int i = 0;
    for (;;)
    {
      boolean bool1 = bool2;
      if (i < j)
      {
        if (arrayOfString[i].equalsIgnoreCase(paramString)) {
          bool1 = true;
        }
      }
      else {
        return bool1;
      }
      i += 1;
    }
  }
  
  public boolean isRedirected(HttpRequest paramHttpRequest, HttpResponse paramHttpResponse, HttpContext paramHttpContext)
    throws ProtocolException
  {
    boolean bool2 = true;
    Args.notNull(paramHttpRequest, "HTTP request");
    Args.notNull(paramHttpResponse, "HTTP response");
    int i = paramHttpResponse.getStatusLine().getStatusCode();
    paramHttpRequest = paramHttpRequest.getRequestLine().getMethod();
    paramHttpResponse = paramHttpResponse.getFirstHeader("location");
    boolean bool1 = bool2;
    switch (i)
    {
    case 304: 
    case 305: 
    case 306: 
    default: 
      bool1 = false;
    case 303: 
    case 302: 
      do
      {
        return bool1;
        if (!isRedirectable(paramHttpRequest)) {
          break;
        }
        bool1 = bool2;
      } while (paramHttpResponse != null);
      return false;
    }
    return isRedirectable(paramHttpRequest);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/cz/msebera/android/httpclient/impl/client/DefaultRedirectStrategy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */