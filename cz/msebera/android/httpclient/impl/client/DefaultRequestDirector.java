package cz.msebera.android.httpclient.impl.client;

import cz.msebera.android.httpclient.ConnectionReuseStrategy;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpEntityEnclosingRequest;
import cz.msebera.android.httpclient.HttpException;
import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NoHttpResponseException;
import cz.msebera.android.httpclient.ProtocolException;
import cz.msebera.android.httpclient.StatusLine;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.auth.AuthProtocolState;
import cz.msebera.android.httpclient.auth.AuthScheme;
import cz.msebera.android.httpclient.auth.AuthState;
import cz.msebera.android.httpclient.auth.UsernamePasswordCredentials;
import cz.msebera.android.httpclient.client.AuthenticationHandler;
import cz.msebera.android.httpclient.client.AuthenticationStrategy;
import cz.msebera.android.httpclient.client.HttpRequestRetryHandler;
import cz.msebera.android.httpclient.client.NonRepeatableRequestException;
import cz.msebera.android.httpclient.client.RedirectException;
import cz.msebera.android.httpclient.client.RedirectHandler;
import cz.msebera.android.httpclient.client.RedirectStrategy;
import cz.msebera.android.httpclient.client.RequestDirector;
import cz.msebera.android.httpclient.client.UserTokenHandler;
import cz.msebera.android.httpclient.client.methods.AbortableHttpRequest;
import cz.msebera.android.httpclient.client.methods.HttpUriRequest;
import cz.msebera.android.httpclient.client.params.HttpClientParams;
import cz.msebera.android.httpclient.client.utils.URIUtils;
import cz.msebera.android.httpclient.conn.BasicManagedEntity;
import cz.msebera.android.httpclient.conn.ClientConnectionManager;
import cz.msebera.android.httpclient.conn.ClientConnectionRequest;
import cz.msebera.android.httpclient.conn.ConnectionKeepAliveStrategy;
import cz.msebera.android.httpclient.conn.ManagedClientConnection;
import cz.msebera.android.httpclient.conn.routing.BasicRouteDirector;
import cz.msebera.android.httpclient.conn.routing.HttpRoute;
import cz.msebera.android.httpclient.conn.routing.HttpRouteDirector;
import cz.msebera.android.httpclient.conn.routing.HttpRoutePlanner;
import cz.msebera.android.httpclient.conn.scheme.Scheme;
import cz.msebera.android.httpclient.conn.scheme.SchemeRegistry;
import cz.msebera.android.httpclient.entity.BufferedHttpEntity;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.impl.auth.BasicScheme;
import cz.msebera.android.httpclient.impl.conn.ConnectionShutdownException;
import cz.msebera.android.httpclient.message.BasicHttpRequest;
import cz.msebera.android.httpclient.params.HttpConnectionParams;
import cz.msebera.android.httpclient.params.HttpParams;
import cz.msebera.android.httpclient.params.HttpProtocolParams;
import cz.msebera.android.httpclient.protocol.HttpContext;
import cz.msebera.android.httpclient.protocol.HttpProcessor;
import cz.msebera.android.httpclient.protocol.HttpRequestExecutor;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.EntityUtils;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.URI;
import java.util.concurrent.TimeUnit;

@Deprecated
@NotThreadSafe
public class DefaultRequestDirector
  implements RequestDirector
{
  private final HttpAuthenticator authenticator;
  protected final ClientConnectionManager connManager;
  private int execCount;
  protected final HttpProcessor httpProcessor;
  protected final ConnectionKeepAliveStrategy keepAliveStrategy;
  public HttpClientAndroidLog log;
  protected ManagedClientConnection managedConn;
  private final int maxRedirects;
  protected final HttpParams params;
  @Deprecated
  protected final AuthenticationHandler proxyAuthHandler;
  protected final AuthState proxyAuthState;
  protected final AuthenticationStrategy proxyAuthStrategy;
  private int redirectCount;
  @Deprecated
  protected final RedirectHandler redirectHandler;
  protected final RedirectStrategy redirectStrategy;
  protected final HttpRequestExecutor requestExec;
  protected final HttpRequestRetryHandler retryHandler;
  protected final ConnectionReuseStrategy reuseStrategy;
  protected final HttpRoutePlanner routePlanner;
  @Deprecated
  protected final AuthenticationHandler targetAuthHandler;
  protected final AuthState targetAuthState;
  protected final AuthenticationStrategy targetAuthStrategy;
  protected final UserTokenHandler userTokenHandler;
  private HttpHost virtualHost;
  
  @Deprecated
  public DefaultRequestDirector(HttpClientAndroidLog paramHttpClientAndroidLog, HttpRequestExecutor paramHttpRequestExecutor, ClientConnectionManager paramClientConnectionManager, ConnectionReuseStrategy paramConnectionReuseStrategy, ConnectionKeepAliveStrategy paramConnectionKeepAliveStrategy, HttpRoutePlanner paramHttpRoutePlanner, HttpProcessor paramHttpProcessor, HttpRequestRetryHandler paramHttpRequestRetryHandler, RedirectStrategy paramRedirectStrategy, AuthenticationHandler paramAuthenticationHandler1, AuthenticationHandler paramAuthenticationHandler2, UserTokenHandler paramUserTokenHandler, HttpParams paramHttpParams)
  {
    this(new HttpClientAndroidLog(DefaultRequestDirector.class), paramHttpRequestExecutor, paramClientConnectionManager, paramConnectionReuseStrategy, paramConnectionKeepAliveStrategy, paramHttpRoutePlanner, paramHttpProcessor, paramHttpRequestRetryHandler, paramRedirectStrategy, new AuthenticationStrategyAdaptor(paramAuthenticationHandler1), new AuthenticationStrategyAdaptor(paramAuthenticationHandler2), paramUserTokenHandler, paramHttpParams);
  }
  
  public DefaultRequestDirector(HttpClientAndroidLog paramHttpClientAndroidLog, HttpRequestExecutor paramHttpRequestExecutor, ClientConnectionManager paramClientConnectionManager, ConnectionReuseStrategy paramConnectionReuseStrategy, ConnectionKeepAliveStrategy paramConnectionKeepAliveStrategy, HttpRoutePlanner paramHttpRoutePlanner, HttpProcessor paramHttpProcessor, HttpRequestRetryHandler paramHttpRequestRetryHandler, RedirectStrategy paramRedirectStrategy, AuthenticationStrategy paramAuthenticationStrategy1, AuthenticationStrategy paramAuthenticationStrategy2, UserTokenHandler paramUserTokenHandler, HttpParams paramHttpParams)
  {
    Args.notNull(paramHttpClientAndroidLog, "Log");
    Args.notNull(paramHttpRequestExecutor, "Request executor");
    Args.notNull(paramClientConnectionManager, "Client connection manager");
    Args.notNull(paramConnectionReuseStrategy, "Connection reuse strategy");
    Args.notNull(paramConnectionKeepAliveStrategy, "Connection keep alive strategy");
    Args.notNull(paramHttpRoutePlanner, "Route planner");
    Args.notNull(paramHttpProcessor, "HTTP protocol processor");
    Args.notNull(paramHttpRequestRetryHandler, "HTTP request retry handler");
    Args.notNull(paramRedirectStrategy, "Redirect strategy");
    Args.notNull(paramAuthenticationStrategy1, "Target authentication strategy");
    Args.notNull(paramAuthenticationStrategy2, "Proxy authentication strategy");
    Args.notNull(paramUserTokenHandler, "User token handler");
    Args.notNull(paramHttpParams, "HTTP parameters");
    this.log = paramHttpClientAndroidLog;
    this.authenticator = new HttpAuthenticator(paramHttpClientAndroidLog);
    this.requestExec = paramHttpRequestExecutor;
    this.connManager = paramClientConnectionManager;
    this.reuseStrategy = paramConnectionReuseStrategy;
    this.keepAliveStrategy = paramConnectionKeepAliveStrategy;
    this.routePlanner = paramHttpRoutePlanner;
    this.httpProcessor = paramHttpProcessor;
    this.retryHandler = paramHttpRequestRetryHandler;
    this.redirectStrategy = paramRedirectStrategy;
    this.targetAuthStrategy = paramAuthenticationStrategy1;
    this.proxyAuthStrategy = paramAuthenticationStrategy2;
    this.userTokenHandler = paramUserTokenHandler;
    this.params = paramHttpParams;
    if ((paramRedirectStrategy instanceof DefaultRedirectStrategyAdaptor))
    {
      this.redirectHandler = ((DefaultRedirectStrategyAdaptor)paramRedirectStrategy).getHandler();
      if (!(paramAuthenticationStrategy1 instanceof AuthenticationStrategyAdaptor)) {
        break label315;
      }
      this.targetAuthHandler = ((AuthenticationStrategyAdaptor)paramAuthenticationStrategy1).getHandler();
      label232:
      if (!(paramAuthenticationStrategy2 instanceof AuthenticationStrategyAdaptor)) {
        break label323;
      }
    }
    label315:
    label323:
    for (this.proxyAuthHandler = ((AuthenticationStrategyAdaptor)paramAuthenticationStrategy2).getHandler();; this.proxyAuthHandler = null)
    {
      this.managedConn = null;
      this.execCount = 0;
      this.redirectCount = 0;
      this.targetAuthState = new AuthState();
      this.proxyAuthState = new AuthState();
      this.maxRedirects = this.params.getIntParameter("http.protocol.max-redirects", 100);
      return;
      this.redirectHandler = null;
      break;
      this.targetAuthHandler = null;
      break label232;
    }
  }
  
  @Deprecated
  public DefaultRequestDirector(HttpRequestExecutor paramHttpRequestExecutor, ClientConnectionManager paramClientConnectionManager, ConnectionReuseStrategy paramConnectionReuseStrategy, ConnectionKeepAliveStrategy paramConnectionKeepAliveStrategy, HttpRoutePlanner paramHttpRoutePlanner, HttpProcessor paramHttpProcessor, HttpRequestRetryHandler paramHttpRequestRetryHandler, RedirectHandler paramRedirectHandler, AuthenticationHandler paramAuthenticationHandler1, AuthenticationHandler paramAuthenticationHandler2, UserTokenHandler paramUserTokenHandler, HttpParams paramHttpParams)
  {
    this(new HttpClientAndroidLog(DefaultRequestDirector.class), paramHttpRequestExecutor, paramClientConnectionManager, paramConnectionReuseStrategy, paramConnectionKeepAliveStrategy, paramHttpRoutePlanner, paramHttpProcessor, paramHttpRequestRetryHandler, new DefaultRedirectStrategyAdaptor(paramRedirectHandler), new AuthenticationStrategyAdaptor(paramAuthenticationHandler1), new AuthenticationStrategyAdaptor(paramAuthenticationHandler2), paramUserTokenHandler, paramHttpParams);
  }
  
  private void abortConnection()
  {
    ManagedClientConnection localManagedClientConnection = this.managedConn;
    if (localManagedClientConnection != null) {
      this.managedConn = null;
    }
    try
    {
      localManagedClientConnection.abortConnection();
    }
    catch (IOException localIOException2)
    {
      for (;;)
      {
        try
        {
          localManagedClientConnection.releaseConnection();
          return;
        }
        catch (IOException localIOException1)
        {
          this.log.debug("Error releasing connection", localIOException1);
        }
        localIOException2 = localIOException2;
        if (this.log.isDebugEnabled()) {
          this.log.debug(localIOException2.getMessage(), localIOException2);
        }
      }
    }
  }
  
  private void tryConnect(RoutedRequest paramRoutedRequest, HttpContext paramHttpContext)
    throws HttpException, IOException
  {
    localHttpRoute = paramRoutedRequest.getRoute();
    paramRoutedRequest = paramRoutedRequest.getRequest();
    i = 0;
    for (;;)
    {
      paramHttpContext.setAttribute("http.request", paramRoutedRequest);
      j = i + 1;
      try
      {
        if (!this.managedConn.isOpen()) {
          this.managedConn.open(localHttpRoute, paramHttpContext, this.params);
        }
        for (;;)
        {
          establishRoute(localHttpRoute, paramHttpContext);
          return;
          this.managedConn.setSocketTimeout(HttpConnectionParams.getSoTimeout(this.params));
        }
        try
        {
          this.managedConn.close();
          if (this.retryHandler.retryRequest(localIOException1, j, paramHttpContext))
          {
            i = j;
            if (!this.log.isInfoEnabled()) {
              continue;
            }
            this.log.info("I/O exception (" + localIOException1.getClass().getName() + ") caught when connecting to " + localHttpRoute + ": " + localIOException1.getMessage());
            if (this.log.isDebugEnabled()) {
              this.log.debug(localIOException1.getMessage(), localIOException1);
            }
            this.log.info("Retrying connect to " + localHttpRoute);
            i = j;
            continue;
          }
          throw localIOException1;
        }
        catch (IOException localIOException2)
        {
          for (;;) {}
        }
      }
      catch (IOException localIOException1) {}
    }
  }
  
  private HttpResponse tryExecute(RoutedRequest paramRoutedRequest, HttpContext paramHttpContext)
    throws HttpException, IOException
  {
    RequestWrapper localRequestWrapper = paramRoutedRequest.getRequest();
    HttpRoute localHttpRoute = paramRoutedRequest.getRoute();
    paramRoutedRequest = null;
    for (;;)
    {
      this.execCount += 1;
      localRequestWrapper.incrementExecCount();
      if (!localRequestWrapper.isRepeatable())
      {
        this.log.debug("Cannot retry non-repeatable request");
        if (paramRoutedRequest != null) {
          throw new NonRepeatableRequestException("Cannot retry request with a non-repeatable request entity.  The cause lists the reason the original request failed.", paramRoutedRequest);
        }
        throw new NonRepeatableRequestException("Cannot retry request with a non-repeatable request entity.");
      }
      try
      {
        if (!this.managedConn.isOpen())
        {
          if (!localHttpRoute.isTunnelled())
          {
            this.log.debug("Reopening the direct connection.");
            this.managedConn.open(localHttpRoute, paramHttpContext, this.params);
          }
        }
        else
        {
          if (this.log.isDebugEnabled()) {
            this.log.debug("Attempt " + this.execCount + " to execute request");
          }
          return this.requestExec.execute(localRequestWrapper, this.managedConn, paramHttpContext);
        }
        this.log.debug("Proxied connection. Need to start over.");
        return null;
      }
      catch (IOException paramRoutedRequest)
      {
        this.log.debug("Closing the connection.");
      }
      try
      {
        this.managedConn.close();
        if (this.retryHandler.retryRequest(paramRoutedRequest, localRequestWrapper.getExecCount(), paramHttpContext))
        {
          if (this.log.isInfoEnabled()) {
            this.log.info("I/O exception (" + paramRoutedRequest.getClass().getName() + ") caught when processing request to " + localHttpRoute + ": " + paramRoutedRequest.getMessage());
          }
          if (this.log.isDebugEnabled()) {
            this.log.debug(paramRoutedRequest.getMessage(), paramRoutedRequest);
          }
          if (this.log.isInfoEnabled()) {
            this.log.info("Retrying request to " + localHttpRoute);
          }
          continue;
        }
        if ((paramRoutedRequest instanceof NoHttpResponseException))
        {
          paramHttpContext = new NoHttpResponseException(localHttpRoute.getTargetHost().toHostString() + " failed to respond");
          paramHttpContext.setStackTrace(paramRoutedRequest.getStackTrace());
          throw paramHttpContext;
        }
        throw paramRoutedRequest;
      }
      catch (IOException localIOException)
      {
        for (;;) {}
      }
    }
  }
  
  private RequestWrapper wrapRequest(HttpRequest paramHttpRequest)
    throws ProtocolException
  {
    if ((paramHttpRequest instanceof HttpEntityEnclosingRequest)) {
      return new EntityEnclosingRequestWrapper((HttpEntityEnclosingRequest)paramHttpRequest);
    }
    return new RequestWrapper(paramHttpRequest);
  }
  
  protected HttpRequest createConnectRequest(HttpRoute paramHttpRoute, HttpContext paramHttpContext)
  {
    paramHttpContext = paramHttpRoute.getTargetHost();
    paramHttpRoute = paramHttpContext.getHostName();
    int j = paramHttpContext.getPort();
    int i = j;
    if (j < 0) {
      i = this.connManager.getSchemeRegistry().getScheme(paramHttpContext.getSchemeName()).getDefaultPort();
    }
    paramHttpContext = new StringBuilder(paramHttpRoute.length() + 6);
    paramHttpContext.append(paramHttpRoute);
    paramHttpContext.append(':');
    paramHttpContext.append(Integer.toString(i));
    return new BasicHttpRequest("CONNECT", paramHttpContext.toString(), HttpProtocolParams.getVersion(this.params));
  }
  
  protected boolean createTunnelToProxy(HttpRoute paramHttpRoute, int paramInt, HttpContext paramHttpContext)
    throws HttpException, IOException
  {
    throw new HttpException("Proxy chains are not supported.");
  }
  
  protected boolean createTunnelToTarget(HttpRoute paramHttpRoute, HttpContext paramHttpContext)
    throws HttpException, IOException
  {
    HttpHost localHttpHost1 = paramHttpRoute.getProxyHost();
    HttpHost localHttpHost2 = paramHttpRoute.getTargetHost();
    Object localObject;
    for (;;)
    {
      if (!this.managedConn.isOpen()) {
        this.managedConn.open(paramHttpRoute, paramHttpContext, this.params);
      }
      localObject = createConnectRequest(paramHttpRoute, paramHttpContext);
      ((HttpRequest)localObject).setParams(this.params);
      paramHttpContext.setAttribute("http.target_host", localHttpHost2);
      paramHttpContext.setAttribute("http.route", paramHttpRoute);
      paramHttpContext.setAttribute("http.proxy_host", localHttpHost1);
      paramHttpContext.setAttribute("http.connection", this.managedConn);
      paramHttpContext.setAttribute("http.request", localObject);
      this.requestExec.preProcess((HttpRequest)localObject, this.httpProcessor, paramHttpContext);
      localObject = this.requestExec.execute((HttpRequest)localObject, this.managedConn, paramHttpContext);
      ((HttpResponse)localObject).setParams(this.params);
      this.requestExec.postProcess((HttpResponse)localObject, this.httpProcessor, paramHttpContext);
      if (((HttpResponse)localObject).getStatusLine().getStatusCode() < 200) {
        throw new HttpException("Unexpected response to CONNECT request: " + ((HttpResponse)localObject).getStatusLine());
      }
      if (HttpClientParams.isAuthenticating(this.params))
      {
        if ((!this.authenticator.isAuthenticationRequested(localHttpHost1, (HttpResponse)localObject, this.proxyAuthStrategy, this.proxyAuthState, paramHttpContext)) || (!this.authenticator.authenticate(localHttpHost1, (HttpResponse)localObject, this.proxyAuthStrategy, this.proxyAuthState, paramHttpContext))) {
          break;
        }
        if (this.reuseStrategy.keepAlive((HttpResponse)localObject, paramHttpContext))
        {
          this.log.debug("Connection kept alive");
          EntityUtils.consume(((HttpResponse)localObject).getEntity());
        }
        else
        {
          this.managedConn.close();
        }
      }
    }
    if (((HttpResponse)localObject).getStatusLine().getStatusCode() > 299)
    {
      paramHttpRoute = ((HttpResponse)localObject).getEntity();
      if (paramHttpRoute != null) {
        ((HttpResponse)localObject).setEntity(new BufferedHttpEntity(paramHttpRoute));
      }
      this.managedConn.close();
      throw new TunnelRefusedException("CONNECT refused by proxy: " + ((HttpResponse)localObject).getStatusLine(), (HttpResponse)localObject);
    }
    this.managedConn.markReusable();
    return false;
  }
  
  protected HttpRoute determineRoute(HttpHost paramHttpHost, HttpRequest paramHttpRequest, HttpContext paramHttpContext)
    throws HttpException
  {
    HttpRoutePlanner localHttpRoutePlanner = this.routePlanner;
    if (paramHttpHost != null) {}
    for (;;)
    {
      return localHttpRoutePlanner.determineRoute(paramHttpHost, paramHttpRequest, paramHttpContext);
      paramHttpHost = (HttpHost)paramHttpRequest.getParams().getParameter("http.default-host");
    }
  }
  
  protected void establishRoute(HttpRoute paramHttpRoute, HttpContext paramHttpContext)
    throws HttpException, IOException
  {
    BasicRouteDirector localBasicRouteDirector = new BasicRouteDirector();
    HttpRoute localHttpRoute = this.managedConn.getRoute();
    int i = localBasicRouteDirector.nextStep(paramHttpRoute, localHttpRoute);
    switch (i)
    {
    default: 
      throw new IllegalStateException("Unknown step indicator " + i + " from RouteDirector.");
    case 1: 
    case 2: 
      this.managedConn.open(paramHttpRoute, paramHttpContext, this.params);
    case 0: 
    case 3: 
    case 4: 
    case 5: 
      while (i <= 0)
      {
        return;
        boolean bool = createTunnelToTarget(paramHttpRoute, paramHttpContext);
        this.log.debug("Tunnel to target created.");
        this.managedConn.tunnelTarget(bool, this.params);
        continue;
        int j = localHttpRoute.getHopCount() - 1;
        bool = createTunnelToProxy(paramHttpRoute, j, paramHttpContext);
        this.log.debug("Tunnel to proxy created.");
        this.managedConn.tunnelProxy(paramHttpRoute.getHopTarget(j), bool, this.params);
        continue;
        this.managedConn.layerProtocol(paramHttpContext, this.params);
      }
    }
    throw new HttpException("Unable to establish route: planned = " + paramHttpRoute + "; current = " + localHttpRoute);
  }
  
  public HttpResponse execute(HttpHost paramHttpHost, HttpRequest paramHttpRequest, HttpContext paramHttpContext)
    throws HttpException, IOException
  {
    paramHttpContext.setAttribute("http.auth.target-scope", this.targetAuthState);
    paramHttpContext.setAttribute("http.auth.proxy-scope", this.proxyAuthState);
    Object localObject2 = wrapRequest(paramHttpRequest);
    ((RequestWrapper)localObject2).setParams(this.params);
    Object localObject3 = determineRoute(paramHttpHost, (HttpRequest)localObject2, paramHttpContext);
    this.virtualHost = ((HttpHost)((RequestWrapper)localObject2).getParams().getParameter("http.virtual-host"));
    Object localObject1;
    if ((this.virtualHost != null) && (this.virtualHost.getPort() == -1))
    {
      if (paramHttpHost == null) {
        break label799;
      }
      localObject1 = paramHttpHost;
      i = ((HttpHost)localObject1).getPort();
      if (i != -1) {
        this.virtualHost = new HttpHost(this.virtualHost.getHostName(), i, this.virtualHost.getSchemeName());
      }
    }
    localObject3 = new RoutedRequest((RequestWrapper)localObject2, (HttpRoute)localObject3);
    boolean bool1 = false;
    int i = 0;
    localObject2 = null;
    label159:
    if (i == 0) {}
    for (;;)
    {
      try
      {
        localObject2 = ((RoutedRequest)localObject3).getRequest();
        localObject4 = ((RoutedRequest)localObject3).getRoute();
        localObject6 = paramHttpContext.getAttribute("http.user-token");
        if (this.managedConn == null)
        {
          localObject1 = this.connManager.requestConnection((HttpRoute)localObject4, localObject6);
          if ((paramHttpRequest instanceof AbortableHttpRequest)) {
            ((AbortableHttpRequest)paramHttpRequest).setConnectionRequest((ClientConnectionRequest)localObject1);
          }
          l = HttpClientParams.getConnectionManagerTimeout(this.params);
        }
      }
      catch (ConnectionShutdownException paramHttpHost)
      {
        try
        {
          this.managedConn = ((ClientConnectionRequest)localObject1).getConnection(l, TimeUnit.MILLISECONDS);
          if ((HttpConnectionParams.isStaleCheckingEnabled(this.params)) && (this.managedConn.isOpen()))
          {
            this.log.debug("Stale connection check");
            if (this.managedConn.isStale())
            {
              this.log.debug("Stale connection detected");
              this.managedConn.close();
            }
          }
          if ((paramHttpRequest instanceof AbortableHttpRequest)) {
            ((AbortableHttpRequest)paramHttpRequest).setReleaseTrigger(this.managedConn);
          }
        }
        catch (InterruptedException paramHttpHost)
        {
          Object localObject6;
          long l;
          Object localObject5;
          Thread.currentThread().interrupt();
          throw new InterruptedIOException();
        }
        try
        {
          tryConnect((RoutedRequest)localObject3, paramHttpContext);
          localObject1 = ((RequestWrapper)localObject2).getURI().getUserInfo();
          if (localObject1 != null) {
            this.targetAuthState.update(new BasicScheme(), new UsernamePasswordCredentials((String)localObject1));
          }
          if (this.virtualHost == null) {
            continue;
          }
          paramHttpHost = this.virtualHost;
          localObject1 = paramHttpHost;
          if (paramHttpHost == null) {
            localObject1 = ((HttpRoute)localObject4).getTargetHost();
          }
          ((RequestWrapper)localObject2).resetHeaders();
          rewriteRequestURI((RequestWrapper)localObject2, (HttpRoute)localObject4);
          paramHttpContext.setAttribute("http.target_host", localObject1);
          paramHttpContext.setAttribute("http.route", localObject4);
          paramHttpContext.setAttribute("http.connection", this.managedConn);
          this.requestExec.preProcess((HttpRequest)localObject2, this.httpProcessor, paramHttpContext);
          localHttpResponse = tryExecute((RoutedRequest)localObject3, paramHttpContext);
          localObject2 = localHttpResponse;
          paramHttpHost = (HttpHost)localObject1;
          if (localHttpResponse == null) {
            break label159;
          }
          localHttpResponse.setParams(this.params);
          this.requestExec.postProcess(localHttpResponse, this.httpProcessor, paramHttpContext);
          bool2 = this.reuseStrategy.keepAlive(localHttpResponse, paramHttpContext);
          if (bool2)
          {
            l = this.keepAliveStrategy.getKeepAliveDuration(localHttpResponse, paramHttpContext);
            if (this.log.isDebugEnabled())
            {
              if (l <= 0L) {
                break label1153;
              }
              paramHttpHost = "for " + l + " " + TimeUnit.MILLISECONDS;
              this.log.debug("Connection can be kept alive " + paramHttpHost);
            }
            this.managedConn.setIdleDuration(l, TimeUnit.MILLISECONDS);
          }
          localObject4 = handleResponse((RoutedRequest)localObject3, localHttpResponse, paramHttpContext);
          if (localObject4 != null) {
            continue;
          }
          j = 1;
          localObject4 = localObject3;
          i = j;
          localObject2 = localHttpResponse;
          bool1 = bool2;
          localObject3 = localObject4;
          paramHttpHost = (HttpHost)localObject1;
          if (this.managedConn == null) {
            break label159;
          }
          localObject5 = localObject6;
          if (localObject6 == null)
          {
            localObject5 = this.userTokenHandler.getUserToken(paramHttpContext);
            paramHttpContext.setAttribute("http.user-token", localObject5);
          }
          i = j;
          localObject2 = localHttpResponse;
          bool1 = bool2;
          localObject3 = localObject4;
          paramHttpHost = (HttpHost)localObject1;
          if (localObject5 == null) {
            break label159;
          }
          this.managedConn.setState(localObject5);
          i = j;
          localObject2 = localHttpResponse;
          bool1 = bool2;
          localObject3 = localObject4;
          paramHttpHost = (HttpHost)localObject1;
        }
        catch (TunnelRefusedException paramHttpHost)
        {
          if (!this.log.isDebugEnabled()) {
            continue;
          }
          this.log.debug(paramHttpHost.getMessage());
          localObject2 = paramHttpHost.getResponse();
        }
        paramHttpHost = paramHttpHost;
        paramHttpRequest = new InterruptedIOException("Connection has been shut down");
        paramHttpRequest.initCause(paramHttpHost);
        throw paramHttpRequest;
        localObject1 = ((HttpRoute)localObject3).getTargetHost();
      }
      catch (HttpException paramHttpHost)
      {
        Object localObject4;
        HttpResponse localHttpResponse;
        boolean bool2;
        abortConnection();
        throw paramHttpHost;
        if ((localObject2 == null) || (((HttpResponse)localObject2).getEntity() == null) || (!((HttpResponse)localObject2).getEntity().isStreaming()))
        {
          if (bool1) {
            this.managedConn.markReusable();
          }
          releaseConnection();
          return (HttpResponse)localObject2;
          localObject1 = ((RequestWrapper)localObject2).getURI();
          if (!((URI)localObject1).isAbsolute()) {
            continue;
          }
          paramHttpHost = URIUtils.extractHost((URI)localObject1);
          continue;
          if (bool2)
          {
            EntityUtils.consume(localHttpResponse.getEntity());
            this.managedConn.markReusable();
            if (((RoutedRequest)localObject4).getRoute().equals(((RoutedRequest)localObject3).getRoute())) {
              break label1160;
            }
            releaseConnection();
            break label1160;
          }
          this.managedConn.close();
          if ((this.proxyAuthState.getState().compareTo(AuthProtocolState.CHALLENGED) > 0) && (this.proxyAuthState.getAuthScheme() != null) && (this.proxyAuthState.getAuthScheme().isConnectionBased()))
          {
            this.log.debug("Resetting proxy auth state");
            this.proxyAuthState.reset();
          }
          if ((this.targetAuthState.getState().compareTo(AuthProtocolState.CHALLENGED) <= 0) || (this.targetAuthState.getAuthScheme() == null) || (!this.targetAuthState.getAuthScheme().isConnectionBased())) {
            continue;
          }
          this.log.debug("Resetting target auth state");
          this.targetAuthState.reset();
          continue;
        }
      }
      catch (IOException paramHttpHost)
      {
        abortConnection();
        throw paramHttpHost;
        ((HttpResponse)localObject2).setEntity(new BasicManagedEntity(((HttpResponse)localObject2).getEntity(), this.managedConn, bool1));
        return (HttpResponse)localObject2;
      }
      catch (RuntimeException paramHttpHost)
      {
        label799:
        abortConnection();
        throw paramHttpHost;
      }
      label1153:
      paramHttpHost = "indefinitely";
      continue;
      label1160:
      int j = i;
    }
  }
  
  protected RoutedRequest handleResponse(RoutedRequest paramRoutedRequest, HttpResponse paramHttpResponse, HttpContext paramHttpContext)
    throws HttpException, IOException
  {
    HttpRoute localHttpRoute = paramRoutedRequest.getRoute();
    RequestWrapper localRequestWrapper = paramRoutedRequest.getRequest();
    HttpParams localHttpParams = localRequestWrapper.getParams();
    Object localObject2;
    Object localObject1;
    if (HttpClientParams.isAuthenticating(localHttpParams))
    {
      localObject2 = (HttpHost)paramHttpContext.getAttribute("http.target_host");
      localObject1 = localObject2;
      if (localObject2 == null) {
        localObject1 = localHttpRoute.getTargetHost();
      }
      localObject2 = localObject1;
      if (((HttpHost)localObject1).getPort() < 0)
      {
        localObject2 = this.connManager.getSchemeRegistry().getScheme((HttpHost)localObject1);
        localObject2 = new HttpHost(((HttpHost)localObject1).getHostName(), ((Scheme)localObject2).getDefaultPort(), ((HttpHost)localObject1).getSchemeName());
      }
      boolean bool1 = this.authenticator.isAuthenticationRequested((HttpHost)localObject2, paramHttpResponse, this.targetAuthStrategy, this.targetAuthState, paramHttpContext);
      HttpHost localHttpHost = localHttpRoute.getProxyHost();
      localObject1 = localHttpHost;
      if (localHttpHost == null) {
        localObject1 = localHttpRoute.getTargetHost();
      }
      boolean bool2 = this.authenticator.isAuthenticationRequested((HttpHost)localObject1, paramHttpResponse, this.proxyAuthStrategy, this.proxyAuthState, paramHttpContext);
      if ((bool1) && (this.authenticator.authenticate((HttpHost)localObject2, paramHttpResponse, this.targetAuthStrategy, this.targetAuthState, paramHttpContext))) {}
      while ((bool2) && (this.authenticator.authenticate((HttpHost)localObject1, paramHttpResponse, this.proxyAuthStrategy, this.proxyAuthState, paramHttpContext))) {
        return paramRoutedRequest;
      }
    }
    if ((HttpClientParams.isRedirecting(localHttpParams)) && (this.redirectStrategy.isRedirected(localRequestWrapper, paramHttpResponse, paramHttpContext)))
    {
      if (this.redirectCount >= this.maxRedirects) {
        throw new RedirectException("Maximum redirects (" + this.maxRedirects + ") exceeded");
      }
      this.redirectCount += 1;
      this.virtualHost = null;
      paramHttpResponse = this.redirectStrategy.getRedirect(localRequestWrapper, paramHttpResponse, paramHttpContext);
      paramHttpResponse.setHeaders(localRequestWrapper.getOriginal().getAllHeaders());
      paramRoutedRequest = paramHttpResponse.getURI();
      localObject1 = URIUtils.extractHost(paramRoutedRequest);
      if (localObject1 == null) {
        throw new ProtocolException("Redirect URI does not specify a valid host name: " + paramRoutedRequest);
      }
      if (!localHttpRoute.getTargetHost().equals(localObject1))
      {
        this.log.debug("Resetting target auth state");
        this.targetAuthState.reset();
        localObject2 = this.proxyAuthState.getAuthScheme();
        if ((localObject2 != null) && (((AuthScheme)localObject2).isConnectionBased()))
        {
          this.log.debug("Resetting proxy auth state");
          this.proxyAuthState.reset();
        }
      }
      paramHttpResponse = wrapRequest(paramHttpResponse);
      paramHttpResponse.setParams(localHttpParams);
      paramHttpContext = determineRoute((HttpHost)localObject1, paramHttpResponse, paramHttpContext);
      paramHttpResponse = new RoutedRequest(paramHttpResponse, paramHttpContext);
      if (this.log.isDebugEnabled()) {
        this.log.debug("Redirecting to '" + paramRoutedRequest + "' via " + paramHttpContext);
      }
      return paramHttpResponse;
    }
    return null;
  }
  
  protected void releaseConnection()
  {
    try
    {
      this.managedConn.releaseConnection();
      this.managedConn = null;
      return;
    }
    catch (IOException localIOException)
    {
      for (;;)
      {
        this.log.debug("IOException releasing connection", localIOException);
      }
    }
  }
  
  /* Error */
  protected void rewriteRequestURI(RequestWrapper paramRequestWrapper, HttpRoute paramHttpRoute)
    throws ProtocolException
  {
    // Byte code:
    //   0: aload_1
    //   1: invokevirtual 678	cz/msebera/android/httpclient/impl/client/RequestWrapper:getURI	()Ljava/net/URI;
    //   4: astore_3
    //   5: aload_2
    //   6: invokevirtual 451	cz/msebera/android/httpclient/conn/routing/HttpRoute:getProxyHost	()Lcz/msebera/android/httpclient/HttpHost;
    //   9: ifnull +41 -> 50
    //   12: aload_2
    //   13: invokevirtual 322	cz/msebera/android/httpclient/conn/routing/HttpRoute:isTunnelled	()Z
    //   16: ifne +34 -> 50
    //   19: aload_3
    //   20: invokevirtual 767	java/net/URI:isAbsolute	()Z
    //   23: ifne +19 -> 42
    //   26: aload_3
    //   27: aload_2
    //   28: invokevirtual 355	cz/msebera/android/httpclient/conn/routing/HttpRoute:getTargetHost	()Lcz/msebera/android/httpclient/HttpHost;
    //   31: iconst_1
    //   32: invokestatic 868	cz/msebera/android/httpclient/client/utils/URIUtils:rewriteURI	(Ljava/net/URI;Lcz/msebera/android/httpclient/HttpHost;Z)Ljava/net/URI;
    //   35: astore_2
    //   36: aload_1
    //   37: aload_2
    //   38: invokevirtual 872	cz/msebera/android/httpclient/impl/client/RequestWrapper:setURI	(Ljava/net/URI;)V
    //   41: return
    //   42: aload_3
    //   43: invokestatic 875	cz/msebera/android/httpclient/client/utils/URIUtils:rewriteURI	(Ljava/net/URI;)Ljava/net/URI;
    //   46: astore_2
    //   47: goto -11 -> 36
    //   50: aload_3
    //   51: invokevirtual 767	java/net/URI:isAbsolute	()Z
    //   54: ifeq +13 -> 67
    //   57: aload_3
    //   58: aconst_null
    //   59: iconst_1
    //   60: invokestatic 868	cz/msebera/android/httpclient/client/utils/URIUtils:rewriteURI	(Ljava/net/URI;Lcz/msebera/android/httpclient/HttpHost;Z)Ljava/net/URI;
    //   63: astore_2
    //   64: goto -28 -> 36
    //   67: aload_3
    //   68: invokestatic 875	cz/msebera/android/httpclient/client/utils/URIUtils:rewriteURI	(Ljava/net/URI;)Ljava/net/URI;
    //   71: astore_2
    //   72: goto -36 -> 36
    //   75: astore_2
    //   76: new 375	cz/msebera/android/httpclient/ProtocolException
    //   79: dup
    //   80: new 259	java/lang/StringBuilder
    //   83: dup
    //   84: invokespecial 260	java/lang/StringBuilder:<init>	()V
    //   87: ldc_w 877
    //   90: invokevirtual 266	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   93: aload_1
    //   94: invokevirtual 881	cz/msebera/android/httpclient/impl/client/RequestWrapper:getRequestLine	()Lcz/msebera/android/httpclient/RequestLine;
    //   97: invokeinterface 886 1 0
    //   102: invokevirtual 266	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   105: invokevirtual 285	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   108: aload_2
    //   109: invokespecial 887	cz/msebera/android/httpclient/ProtocolException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   112: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	113	0	this	DefaultRequestDirector
    //   0	113	1	paramRequestWrapper	RequestWrapper
    //   0	113	2	paramHttpRoute	HttpRoute
    //   4	64	3	localURI	URI
    // Exception table:
    //   from	to	target	type
    //   0	36	75	java/net/URISyntaxException
    //   36	41	75	java/net/URISyntaxException
    //   42	47	75	java/net/URISyntaxException
    //   50	64	75	java/net/URISyntaxException
    //   67	72	75	java/net/URISyntaxException
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/cz/msebera/android/httpclient/impl/client/DefaultRequestDirector.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */