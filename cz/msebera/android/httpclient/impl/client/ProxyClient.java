package cz.msebera.android.httpclient.impl.client;

import cz.msebera.android.httpclient.ConnectionReuseStrategy;
import cz.msebera.android.httpclient.HttpClientConnection;
import cz.msebera.android.httpclient.HttpException;
import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpRequestInterceptor;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.HttpVersion;
import cz.msebera.android.httpclient.StatusLine;
import cz.msebera.android.httpclient.auth.AuthSchemeRegistry;
import cz.msebera.android.httpclient.auth.AuthScope;
import cz.msebera.android.httpclient.auth.AuthState;
import cz.msebera.android.httpclient.auth.Credentials;
import cz.msebera.android.httpclient.client.config.RequestConfig;
import cz.msebera.android.httpclient.client.params.HttpClientParamConfig;
import cz.msebera.android.httpclient.client.protocol.RequestClientConnControl;
import cz.msebera.android.httpclient.config.ConnectionConfig;
import cz.msebera.android.httpclient.conn.HttpConnectionFactory;
import cz.msebera.android.httpclient.conn.ManagedHttpClientConnection;
import cz.msebera.android.httpclient.conn.routing.HttpRoute;
import cz.msebera.android.httpclient.conn.routing.RouteInfo.LayerType;
import cz.msebera.android.httpclient.conn.routing.RouteInfo.TunnelType;
import cz.msebera.android.httpclient.entity.BufferedHttpEntity;
import cz.msebera.android.httpclient.impl.DefaultConnectionReuseStrategy;
import cz.msebera.android.httpclient.impl.auth.BasicSchemeFactory;
import cz.msebera.android.httpclient.impl.auth.DigestSchemeFactory;
import cz.msebera.android.httpclient.impl.auth.HttpAuthenticator;
import cz.msebera.android.httpclient.impl.auth.NTLMSchemeFactory;
import cz.msebera.android.httpclient.impl.conn.ManagedHttpClientConnectionFactory;
import cz.msebera.android.httpclient.impl.execchain.TunnelRefusedException;
import cz.msebera.android.httpclient.message.BasicHttpRequest;
import cz.msebera.android.httpclient.params.BasicHttpParams;
import cz.msebera.android.httpclient.params.HttpParamConfig;
import cz.msebera.android.httpclient.params.HttpParams;
import cz.msebera.android.httpclient.protocol.BasicHttpContext;
import cz.msebera.android.httpclient.protocol.HttpContext;
import cz.msebera.android.httpclient.protocol.HttpProcessor;
import cz.msebera.android.httpclient.protocol.HttpRequestExecutor;
import cz.msebera.android.httpclient.protocol.ImmutableHttpProcessor;
import cz.msebera.android.httpclient.protocol.RequestTargetHost;
import cz.msebera.android.httpclient.protocol.RequestUserAgent;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.EntityUtils;
import java.io.IOException;
import java.net.Socket;

public class ProxyClient
{
  private final AuthSchemeRegistry authSchemeRegistry;
  private final HttpAuthenticator authenticator;
  private final HttpConnectionFactory<HttpRoute, ManagedHttpClientConnection> connFactory;
  private final ConnectionConfig connectionConfig;
  private final HttpProcessor httpProcessor;
  private final AuthState proxyAuthState;
  private final ProxyAuthenticationStrategy proxyAuthStrategy;
  private final RequestConfig requestConfig;
  private final HttpRequestExecutor requestExec;
  private final ConnectionReuseStrategy reuseStrategy;
  
  public ProxyClient()
  {
    this(null, null, null);
  }
  
  public ProxyClient(RequestConfig paramRequestConfig)
  {
    this(null, null, paramRequestConfig);
  }
  
  public ProxyClient(HttpConnectionFactory<HttpRoute, ManagedHttpClientConnection> paramHttpConnectionFactory, ConnectionConfig paramConnectionConfig, RequestConfig paramRequestConfig)
  {
    if (paramHttpConnectionFactory != null)
    {
      this.connFactory = paramHttpConnectionFactory;
      if (paramConnectionConfig == null) {
        break label198;
      }
      label17:
      this.connectionConfig = paramConnectionConfig;
      if (paramRequestConfig == null) {
        break label205;
      }
    }
    for (;;)
    {
      this.requestConfig = paramRequestConfig;
      this.httpProcessor = new ImmutableHttpProcessor(new HttpRequestInterceptor[] { new RequestTargetHost(), new RequestClientConnControl(), new RequestUserAgent() });
      this.requestExec = new HttpRequestExecutor();
      this.proxyAuthStrategy = new ProxyAuthenticationStrategy();
      this.authenticator = new HttpAuthenticator();
      this.proxyAuthState = new AuthState();
      this.authSchemeRegistry = new AuthSchemeRegistry();
      this.authSchemeRegistry.register("Basic", new BasicSchemeFactory());
      this.authSchemeRegistry.register("Digest", new DigestSchemeFactory());
      this.authSchemeRegistry.register("NTLM", new NTLMSchemeFactory());
      this.reuseStrategy = new DefaultConnectionReuseStrategy();
      return;
      paramHttpConnectionFactory = ManagedHttpClientConnectionFactory.INSTANCE;
      break;
      label198:
      paramConnectionConfig = ConnectionConfig.DEFAULT;
      break label17;
      label205:
      paramRequestConfig = RequestConfig.DEFAULT;
    }
  }
  
  @Deprecated
  public ProxyClient(HttpParams paramHttpParams)
  {
    this(null, HttpParamConfig.getConnectionConfig(paramHttpParams), HttpClientParamConfig.getRequestConfig(paramHttpParams));
  }
  
  @Deprecated
  public AuthSchemeRegistry getAuthSchemeRegistry()
  {
    return this.authSchemeRegistry;
  }
  
  @Deprecated
  public HttpParams getParams()
  {
    return new BasicHttpParams();
  }
  
  public Socket tunnel(HttpHost paramHttpHost1, HttpHost paramHttpHost2, Credentials paramCredentials)
    throws IOException, HttpException
  {
    Args.notNull(paramHttpHost1, "Proxy host");
    Args.notNull(paramHttpHost2, "Target host");
    Args.notNull(paramCredentials, "Credentials");
    Object localObject2 = paramHttpHost2;
    Object localObject1 = localObject2;
    if (((HttpHost)localObject2).getPort() <= 0) {
      localObject1 = new HttpHost(((HttpHost)localObject2).getHostName(), 80, ((HttpHost)localObject2).getSchemeName());
    }
    HttpRoute localHttpRoute = new HttpRoute((HttpHost)localObject1, this.requestConfig.getLocalAddress(), paramHttpHost1, false, RouteInfo.TunnelType.TUNNELLED, RouteInfo.LayerType.PLAIN);
    localObject2 = (ManagedHttpClientConnection)this.connFactory.create(localHttpRoute, this.connectionConfig);
    BasicHttpContext localBasicHttpContext = new BasicHttpContext();
    localObject1 = new BasicHttpRequest("CONNECT", ((HttpHost)localObject1).toHostString(), HttpVersion.HTTP_1_1);
    BasicCredentialsProvider localBasicCredentialsProvider = new BasicCredentialsProvider();
    localBasicCredentialsProvider.setCredentials(new AuthScope(paramHttpHost1), paramCredentials);
    localBasicHttpContext.setAttribute("http.target_host", paramHttpHost2);
    localBasicHttpContext.setAttribute("http.connection", localObject2);
    localBasicHttpContext.setAttribute("http.request", localObject1);
    localBasicHttpContext.setAttribute("http.route", localHttpRoute);
    localBasicHttpContext.setAttribute("http.auth.proxy-scope", this.proxyAuthState);
    localBasicHttpContext.setAttribute("http.auth.credentials-provider", localBasicCredentialsProvider);
    localBasicHttpContext.setAttribute("http.authscheme-registry", this.authSchemeRegistry);
    localBasicHttpContext.setAttribute("http.request-config", this.requestConfig);
    this.requestExec.preProcess((HttpRequest)localObject1, this.httpProcessor, localBasicHttpContext);
    if (!((ManagedHttpClientConnection)localObject2).isOpen()) {
      ((ManagedHttpClientConnection)localObject2).bind(new Socket(paramHttpHost1.getHostName(), paramHttpHost1.getPort()));
    }
    this.authenticator.generateAuthResponse((HttpRequest)localObject1, this.proxyAuthState, localBasicHttpContext);
    paramHttpHost2 = this.requestExec.execute((HttpRequest)localObject1, (HttpClientConnection)localObject2, localBasicHttpContext);
    if (paramHttpHost2.getStatusLine().getStatusCode() < 200) {
      throw new HttpException("Unexpected response to CONNECT request: " + paramHttpHost2.getStatusLine());
    }
    if ((this.authenticator.isAuthenticationRequested(paramHttpHost1, paramHttpHost2, this.proxyAuthStrategy, this.proxyAuthState, localBasicHttpContext)) && (this.authenticator.handleAuthChallenge(paramHttpHost1, paramHttpHost2, this.proxyAuthStrategy, this.proxyAuthState, localBasicHttpContext)))
    {
      if (this.reuseStrategy.keepAlive(paramHttpHost2, localBasicHttpContext)) {
        EntityUtils.consume(paramHttpHost2.getEntity());
      }
      for (;;)
      {
        ((HttpRequest)localObject1).removeHeaders("Proxy-Authorization");
        break;
        ((ManagedHttpClientConnection)localObject2).close();
      }
    }
    if (paramHttpHost2.getStatusLine().getStatusCode() > 299)
    {
      paramHttpHost1 = paramHttpHost2.getEntity();
      if (paramHttpHost1 != null) {
        paramHttpHost2.setEntity(new BufferedHttpEntity(paramHttpHost1));
      }
      ((ManagedHttpClientConnection)localObject2).close();
      throw new TunnelRefusedException("CONNECT refused by proxy: " + paramHttpHost2.getStatusLine(), paramHttpHost2);
    }
    return ((ManagedHttpClientConnection)localObject2).getSocket();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/cz/msebera/android/httpclient/impl/client/ProxyClient.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */