package cz.msebera.android.httpclient.impl.client;

import cz.msebera.android.httpclient.ConnectionReuseStrategy;
import cz.msebera.android.httpclient.annotation.ThreadSafe;
import cz.msebera.android.httpclient.conn.ClientConnectionManager;
import cz.msebera.android.httpclient.conn.routing.HttpRoutePlanner;
import cz.msebera.android.httpclient.impl.DefaultConnectionReuseStrategy;
import cz.msebera.android.httpclient.impl.NoConnectionReuseStrategy;
import cz.msebera.android.httpclient.impl.conn.PoolingClientConnectionManager;
import cz.msebera.android.httpclient.impl.conn.ProxySelectorRoutePlanner;
import cz.msebera.android.httpclient.impl.conn.SchemeRegistryFactory;
import cz.msebera.android.httpclient.params.HttpParams;
import java.net.ProxySelector;

@Deprecated
@ThreadSafe
public class SystemDefaultHttpClient
  extends DefaultHttpClient
{
  public SystemDefaultHttpClient()
  {
    super(null, null);
  }
  
  public SystemDefaultHttpClient(HttpParams paramHttpParams)
  {
    super(null, paramHttpParams);
  }
  
  protected ClientConnectionManager createClientConnectionManager()
  {
    PoolingClientConnectionManager localPoolingClientConnectionManager = new PoolingClientConnectionManager(SchemeRegistryFactory.createSystemDefault());
    if ("true".equalsIgnoreCase(System.getProperty("http.keepAlive", "true")))
    {
      int i = Integer.parseInt(System.getProperty("http.maxConnections", "5"));
      localPoolingClientConnectionManager.setDefaultMaxPerRoute(i);
      localPoolingClientConnectionManager.setMaxTotal(i * 2);
    }
    return localPoolingClientConnectionManager;
  }
  
  protected ConnectionReuseStrategy createConnectionReuseStrategy()
  {
    if ("true".equalsIgnoreCase(System.getProperty("http.keepAlive", "true"))) {
      return new DefaultConnectionReuseStrategy();
    }
    return new NoConnectionReuseStrategy();
  }
  
  protected HttpRoutePlanner createHttpRoutePlanner()
  {
    return new ProxySelectorRoutePlanner(getConnectionManager().getSchemeRegistry(), ProxySelector.getDefault());
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/cz/msebera/android/httpclient/impl/client/SystemDefaultHttpClient.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */