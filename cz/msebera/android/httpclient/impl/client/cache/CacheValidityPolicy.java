package cz.msebera.android.httpclient.impl.client.cache;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HeaderElement;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.client.cache.HttpCacheEntry;
import cz.msebera.android.httpclient.client.cache.Resource;
import cz.msebera.android.httpclient.client.utils.DateUtils;
import java.util.Date;

@Immutable
class CacheValidityPolicy
{
  public static final long MAX_AGE = 2147483648L;
  
  private boolean mayReturnStaleIfError(Header[] paramArrayOfHeader, long paramLong)
  {
    boolean bool1 = false;
    int k = paramArrayOfHeader.length;
    int i = 0;
    if (i < k)
    {
      HeaderElement[] arrayOfHeaderElement = paramArrayOfHeader[i].getElements();
      int m = arrayOfHeaderElement.length;
      int j = 0;
      for (;;)
      {
        boolean bool2 = bool1;
        HeaderElement localHeaderElement;
        if (j < m)
        {
          localHeaderElement = arrayOfHeaderElement[j];
          if (!"stale-if-error".equals(localHeaderElement.getName())) {}
        }
        else
        {
          try
          {
            int n = Integer.parseInt(localHeaderElement.getValue());
            if (paramLong <= n)
            {
              bool2 = true;
              i += 1;
              bool1 = bool2;
            }
          }
          catch (NumberFormatException localNumberFormatException) {}
        }
        j += 1;
      }
    }
    return bool1;
  }
  
  protected boolean contentLengthHeaderMatchesActualLength(HttpCacheEntry paramHttpCacheEntry)
  {
    return (!hasContentLengthHeader(paramHttpCacheEntry)) || (getContentLengthValue(paramHttpCacheEntry) == paramHttpCacheEntry.getResource().length());
  }
  
  protected long getAgeValue(HttpCacheEntry paramHttpCacheEntry)
  {
    long l2 = 0L;
    paramHttpCacheEntry = paramHttpCacheEntry.getHeaders("Age");
    int j = paramHttpCacheEntry.length;
    int i = 0;
    while (i < j)
    {
      Object localObject = paramHttpCacheEntry[i];
      try
      {
        l3 = Long.parseLong(((Header)localObject).getValue());
        l1 = l3;
        if (l3 < 0L) {
          l1 = 2147483648L;
        }
      }
      catch (NumberFormatException localNumberFormatException)
      {
        for (;;)
        {
          long l3;
          long l1 = 2147483648L;
        }
      }
      l3 = l2;
      if (l1 > l2) {
        l3 = l1;
      }
      i += 1;
      l2 = l3;
    }
    return l2;
  }
  
  protected long getApparentAgeSecs(HttpCacheEntry paramHttpCacheEntry)
  {
    long l1 = 0L;
    Date localDate = paramHttpCacheEntry.getDate();
    if (localDate == null) {
      l1 = 2147483648L;
    }
    long l2;
    do
    {
      return l1;
      l2 = paramHttpCacheEntry.getResponseDate().getTime() - localDate.getTime();
    } while (l2 < 0L);
    return l2 / 1000L;
  }
  
  protected long getContentLengthValue(HttpCacheEntry paramHttpCacheEntry)
  {
    paramHttpCacheEntry = paramHttpCacheEntry.getFirstHeader("Content-Length");
    if (paramHttpCacheEntry == null) {
      return -1L;
    }
    try
    {
      long l = Long.parseLong(paramHttpCacheEntry.getValue());
      return l;
    }
    catch (NumberFormatException paramHttpCacheEntry) {}
    return -1L;
  }
  
  protected long getCorrectedInitialAgeSecs(HttpCacheEntry paramHttpCacheEntry)
  {
    return getCorrectedReceivedAgeSecs(paramHttpCacheEntry) + getResponseDelaySecs(paramHttpCacheEntry);
  }
  
  protected long getCorrectedReceivedAgeSecs(HttpCacheEntry paramHttpCacheEntry)
  {
    long l1 = getApparentAgeSecs(paramHttpCacheEntry);
    long l2 = getAgeValue(paramHttpCacheEntry);
    if (l1 > l2) {
      return l1;
    }
    return l2;
  }
  
  public long getCurrentAgeSecs(HttpCacheEntry paramHttpCacheEntry, Date paramDate)
  {
    return getCorrectedInitialAgeSecs(paramHttpCacheEntry) + getResidentTimeSecs(paramHttpCacheEntry, paramDate);
  }
  
  @Deprecated
  protected Date getDateValue(HttpCacheEntry paramHttpCacheEntry)
  {
    return paramHttpCacheEntry.getDate();
  }
  
  protected Date getExpirationDate(HttpCacheEntry paramHttpCacheEntry)
  {
    paramHttpCacheEntry = paramHttpCacheEntry.getFirstHeader("Expires");
    if (paramHttpCacheEntry == null) {
      return null;
    }
    return DateUtils.parseDate(paramHttpCacheEntry.getValue());
  }
  
  public long getFreshnessLifetimeSecs(HttpCacheEntry paramHttpCacheEntry)
  {
    long l = getMaxAge(paramHttpCacheEntry);
    if (l > -1L) {
      return l;
    }
    Date localDate = paramHttpCacheEntry.getDate();
    if (localDate == null) {
      return 0L;
    }
    paramHttpCacheEntry = getExpirationDate(paramHttpCacheEntry);
    if (paramHttpCacheEntry == null) {
      return 0L;
    }
    return (paramHttpCacheEntry.getTime() - localDate.getTime()) / 1000L;
  }
  
  public long getHeuristicFreshnessLifetimeSecs(HttpCacheEntry paramHttpCacheEntry, float paramFloat, long paramLong)
  {
    Date localDate = paramHttpCacheEntry.getDate();
    paramHttpCacheEntry = getLastModifiedValue(paramHttpCacheEntry);
    long l = paramLong;
    if (localDate != null)
    {
      l = paramLong;
      if (paramHttpCacheEntry != null)
      {
        paramLong = localDate.getTime() - paramHttpCacheEntry.getTime();
        if (paramLong >= 0L) {
          break label50;
        }
        l = 0L;
      }
    }
    return l;
    label50:
    return ((float)(paramLong / 1000L) * paramFloat);
  }
  
  protected Date getLastModifiedValue(HttpCacheEntry paramHttpCacheEntry)
  {
    paramHttpCacheEntry = paramHttpCacheEntry.getFirstHeader("Last-Modified");
    if (paramHttpCacheEntry == null) {
      return null;
    }
    return DateUtils.parseDate(paramHttpCacheEntry.getValue());
  }
  
  protected long getMaxAge(HttpCacheEntry paramHttpCacheEntry)
  {
    long l2 = -1L;
    paramHttpCacheEntry = paramHttpCacheEntry.getHeaders("Cache-Control");
    int k = paramHttpCacheEntry.length;
    int i = 0;
    while (i < k)
    {
      HeaderElement[] arrayOfHeaderElement = paramHttpCacheEntry[i].getElements();
      int m = arrayOfHeaderElement.length;
      int j = 0;
      long l1;
      while (j < m)
      {
        HeaderElement localHeaderElement = arrayOfHeaderElement[j];
        if (!"max-age".equals(localHeaderElement.getName()))
        {
          l1 = l2;
          if (!"s-maxage".equals(localHeaderElement.getName())) {
            break label124;
          }
        }
        try
        {
          long l3 = Long.parseLong(localHeaderElement.getValue());
          if (l2 != -1L)
          {
            l1 = l2;
            if (l3 >= l2) {}
          }
          else
          {
            l1 = l3;
          }
        }
        catch (NumberFormatException localNumberFormatException)
        {
          for (;;)
          {
            label124:
            l1 = 0L;
          }
        }
        j += 1;
        l2 = l1;
      }
      i += 1;
    }
    return l2;
  }
  
  protected long getResidentTimeSecs(HttpCacheEntry paramHttpCacheEntry, Date paramDate)
  {
    return (paramDate.getTime() - paramHttpCacheEntry.getResponseDate().getTime()) / 1000L;
  }
  
  protected long getResponseDelaySecs(HttpCacheEntry paramHttpCacheEntry)
  {
    return (paramHttpCacheEntry.getResponseDate().getTime() - paramHttpCacheEntry.getRequestDate().getTime()) / 1000L;
  }
  
  public long getStalenessSecs(HttpCacheEntry paramHttpCacheEntry, Date paramDate)
  {
    long l1 = getCurrentAgeSecs(paramHttpCacheEntry, paramDate);
    long l2 = getFreshnessLifetimeSecs(paramHttpCacheEntry);
    if (l1 <= l2) {
      return 0L;
    }
    return l1 - l2;
  }
  
  public boolean hasCacheControlDirective(HttpCacheEntry paramHttpCacheEntry, String paramString)
  {
    boolean bool2 = false;
    paramHttpCacheEntry = paramHttpCacheEntry.getHeaders("Cache-Control");
    int k = paramHttpCacheEntry.length;
    int i = 0;
    for (;;)
    {
      boolean bool1 = bool2;
      HeaderElement[] arrayOfHeaderElement;
      int m;
      int j;
      if (i < k)
      {
        arrayOfHeaderElement = paramHttpCacheEntry[i].getElements();
        m = arrayOfHeaderElement.length;
        j = 0;
      }
      while (j < m)
      {
        if (paramString.equalsIgnoreCase(arrayOfHeaderElement[j].getName()))
        {
          bool1 = true;
          return bool1;
        }
        j += 1;
      }
      i += 1;
    }
  }
  
  protected boolean hasContentLengthHeader(HttpCacheEntry paramHttpCacheEntry)
  {
    return paramHttpCacheEntry.getFirstHeader("Content-Length") != null;
  }
  
  public boolean isResponseFresh(HttpCacheEntry paramHttpCacheEntry, Date paramDate)
  {
    return getCurrentAgeSecs(paramHttpCacheEntry, paramDate) < getFreshnessLifetimeSecs(paramHttpCacheEntry);
  }
  
  public boolean isResponseHeuristicallyFresh(HttpCacheEntry paramHttpCacheEntry, Date paramDate, float paramFloat, long paramLong)
  {
    return getCurrentAgeSecs(paramHttpCacheEntry, paramDate) < getHeuristicFreshnessLifetimeSecs(paramHttpCacheEntry, paramFloat, paramLong);
  }
  
  public boolean isRevalidatable(HttpCacheEntry paramHttpCacheEntry)
  {
    return (paramHttpCacheEntry.getFirstHeader("ETag") != null) || (paramHttpCacheEntry.getFirstHeader("Last-Modified") != null);
  }
  
  public boolean mayReturnStaleIfError(HttpRequest paramHttpRequest, HttpCacheEntry paramHttpCacheEntry, Date paramDate)
  {
    long l = getStalenessSecs(paramHttpCacheEntry, paramDate);
    return (mayReturnStaleIfError(paramHttpRequest.getHeaders("Cache-Control"), l)) || (mayReturnStaleIfError(paramHttpCacheEntry.getHeaders("Cache-Control"), l));
  }
  
  public boolean mayReturnStaleWhileRevalidating(HttpCacheEntry paramHttpCacheEntry, Date paramDate)
  {
    Header[] arrayOfHeader = paramHttpCacheEntry.getHeaders("Cache-Control");
    int k = arrayOfHeader.length;
    int i = 0;
    while (i < k)
    {
      HeaderElement[] arrayOfHeaderElement = arrayOfHeader[i].getElements();
      int m = arrayOfHeaderElement.length;
      int j = 0;
      while (j < m)
      {
        HeaderElement localHeaderElement = arrayOfHeaderElement[j];
        if ("stale-while-revalidate".equalsIgnoreCase(localHeaderElement.getName())) {
          try
          {
            int n = Integer.parseInt(localHeaderElement.getValue());
            long l = getStalenessSecs(paramHttpCacheEntry, paramDate);
            if (l <= n) {
              return true;
            }
          }
          catch (NumberFormatException localNumberFormatException) {}
        }
        j += 1;
      }
      i += 1;
    }
    return false;
  }
  
  public boolean mustRevalidate(HttpCacheEntry paramHttpCacheEntry)
  {
    return hasCacheControlDirective(paramHttpCacheEntry, "must-revalidate");
  }
  
  public boolean proxyRevalidate(HttpCacheEntry paramHttpCacheEntry)
  {
    return hasCacheControlDirective(paramHttpCacheEntry, "proxy-revalidate");
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/cz/msebera/android/httpclient/impl/client/cache/CacheValidityPolicy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */