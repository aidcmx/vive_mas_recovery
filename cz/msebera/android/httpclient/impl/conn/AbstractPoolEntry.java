package cz.msebera.android.httpclient.impl.conn;

import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.conn.ClientConnectionOperator;
import cz.msebera.android.httpclient.conn.OperatedClientConnection;
import cz.msebera.android.httpclient.conn.routing.HttpRoute;
import cz.msebera.android.httpclient.conn.routing.RouteTracker;
import cz.msebera.android.httpclient.params.HttpParams;
import cz.msebera.android.httpclient.protocol.HttpContext;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.Asserts;
import java.io.IOException;
import java.io.InterruptedIOException;

@Deprecated
public abstract class AbstractPoolEntry
{
  protected final ClientConnectionOperator connOperator;
  protected final OperatedClientConnection connection;
  protected volatile HttpRoute route;
  protected volatile Object state;
  protected volatile RouteTracker tracker;
  
  protected AbstractPoolEntry(ClientConnectionOperator paramClientConnectionOperator, HttpRoute paramHttpRoute)
  {
    Args.notNull(paramClientConnectionOperator, "Connection operator");
    this.connOperator = paramClientConnectionOperator;
    this.connection = paramClientConnectionOperator.createConnection();
    this.route = paramHttpRoute;
    this.tracker = null;
  }
  
  public Object getState()
  {
    return this.state;
  }
  
  public void layerProtocol(HttpContext paramHttpContext, HttpParams paramHttpParams)
    throws IOException
  {
    Args.notNull(paramHttpParams, "HTTP parameters");
    Asserts.notNull(this.tracker, "Route tracker");
    Asserts.check(this.tracker.isConnected(), "Connection not open");
    Asserts.check(this.tracker.isTunnelled(), "Protocol layering without a tunnel not supported");
    if (!this.tracker.isLayered()) {}
    for (boolean bool = true;; bool = false)
    {
      Asserts.check(bool, "Multiple protocol layering not supported");
      HttpHost localHttpHost = this.tracker.getTargetHost();
      this.connOperator.updateSecureConnection(this.connection, localHttpHost, paramHttpContext, paramHttpParams);
      this.tracker.layerProtocol(this.connection.isSecure());
      return;
    }
  }
  
  public void open(HttpRoute paramHttpRoute, HttpContext paramHttpContext, HttpParams paramHttpParams)
    throws IOException
  {
    Args.notNull(paramHttpRoute, "Route");
    Args.notNull(paramHttpParams, "HTTP parameters");
    boolean bool;
    HttpHost localHttpHost2;
    ClientConnectionOperator localClientConnectionOperator;
    OperatedClientConnection localOperatedClientConnection;
    if (this.tracker != null)
    {
      if (!this.tracker.isConnected())
      {
        bool = true;
        Asserts.check(bool, "Connection already open");
      }
    }
    else
    {
      this.tracker = new RouteTracker(paramHttpRoute);
      localHttpHost2 = paramHttpRoute.getProxyHost();
      localClientConnectionOperator = this.connOperator;
      localOperatedClientConnection = this.connection;
      if (localHttpHost2 == null) {
        break label122;
      }
    }
    label122:
    for (HttpHost localHttpHost1 = localHttpHost2;; localHttpHost1 = paramHttpRoute.getTargetHost())
    {
      localClientConnectionOperator.openConnection(localOperatedClientConnection, localHttpHost1, paramHttpRoute.getLocalAddress(), paramHttpContext, paramHttpParams);
      paramHttpRoute = this.tracker;
      if (paramHttpRoute != null) {
        break label131;
      }
      throw new InterruptedIOException("Request aborted");
      bool = false;
      break;
    }
    label131:
    if (localHttpHost2 == null)
    {
      paramHttpRoute.connectTarget(this.connection.isSecure());
      return;
    }
    paramHttpRoute.connectProxy(localHttpHost2, this.connection.isSecure());
  }
  
  public void setState(Object paramObject)
  {
    this.state = paramObject;
  }
  
  protected void shutdownEntry()
  {
    this.tracker = null;
    this.state = null;
  }
  
  public void tunnelProxy(HttpHost paramHttpHost, boolean paramBoolean, HttpParams paramHttpParams)
    throws IOException
  {
    Args.notNull(paramHttpHost, "Next proxy");
    Args.notNull(paramHttpParams, "Parameters");
    Asserts.notNull(this.tracker, "Route tracker");
    Asserts.check(this.tracker.isConnected(), "Connection not open");
    this.connection.update(null, paramHttpHost, paramBoolean, paramHttpParams);
    this.tracker.tunnelProxy(paramHttpHost, paramBoolean);
  }
  
  public void tunnelTarget(boolean paramBoolean, HttpParams paramHttpParams)
    throws IOException
  {
    Args.notNull(paramHttpParams, "HTTP parameters");
    Asserts.notNull(this.tracker, "Route tracker");
    Asserts.check(this.tracker.isConnected(), "Connection not open");
    if (!this.tracker.isTunnelled()) {}
    for (boolean bool = true;; bool = false)
    {
      Asserts.check(bool, "Connection is already tunnelled");
      this.connection.update(null, this.tracker.getTargetHost(), paramBoolean, paramHttpParams);
      this.tracker.tunnelTarget(paramBoolean);
      return;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/cz/msebera/android/httpclient/impl/conn/AbstractPoolEntry.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */