package cz.msebera.android.httpclient.impl.conn;

import cz.msebera.android.httpclient.HttpClientConnection;
import cz.msebera.android.httpclient.annotation.GuardedBy;
import cz.msebera.android.httpclient.annotation.ThreadSafe;
import cz.msebera.android.httpclient.conn.ClientConnectionManager;
import cz.msebera.android.httpclient.conn.ClientConnectionOperator;
import cz.msebera.android.httpclient.conn.ClientConnectionRequest;
import cz.msebera.android.httpclient.conn.ManagedClientConnection;
import cz.msebera.android.httpclient.conn.OperatedClientConnection;
import cz.msebera.android.httpclient.conn.routing.HttpRoute;
import cz.msebera.android.httpclient.conn.routing.RouteTracker;
import cz.msebera.android.httpclient.conn.scheme.SchemeRegistry;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.Asserts;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

@Deprecated
@ThreadSafe
public class BasicClientConnectionManager
  implements ClientConnectionManager
{
  private static final AtomicLong COUNTER = new AtomicLong();
  public static final String MISUSE_MESSAGE = "Invalid use of BasicClientConnManager: connection still allocated.\nMake sure to release the connection before allocating another one.";
  @GuardedBy("this")
  private ManagedClientConnectionImpl conn;
  private final ClientConnectionOperator connOperator;
  public HttpClientAndroidLog log = new HttpClientAndroidLog(getClass());
  @GuardedBy("this")
  private HttpPoolEntry poolEntry;
  private final SchemeRegistry schemeRegistry;
  @GuardedBy("this")
  private volatile boolean shutdown;
  
  public BasicClientConnectionManager()
  {
    this(SchemeRegistryFactory.createDefault());
  }
  
  public BasicClientConnectionManager(SchemeRegistry paramSchemeRegistry)
  {
    Args.notNull(paramSchemeRegistry, "Scheme registry");
    this.schemeRegistry = paramSchemeRegistry;
    this.connOperator = createConnectionOperator(paramSchemeRegistry);
  }
  
  private void assertNotShutdown()
  {
    if (!this.shutdown) {}
    for (boolean bool = true;; bool = false)
    {
      Asserts.check(bool, "Connection manager has been shut down");
      return;
    }
  }
  
  private void shutdownConnection(HttpClientConnection paramHttpClientConnection)
  {
    try
    {
      paramHttpClientConnection.shutdown();
      return;
    }
    catch (IOException paramHttpClientConnection)
    {
      while (!this.log.isDebugEnabled()) {}
      this.log.debug("I/O exception shutting down connection", paramHttpClientConnection);
    }
  }
  
  public void closeExpiredConnections()
  {
    try
    {
      assertNotShutdown();
      long l = System.currentTimeMillis();
      if ((this.poolEntry != null) && (this.poolEntry.isExpired(l)))
      {
        this.poolEntry.close();
        this.poolEntry.getTracker().reset();
      }
      return;
    }
    finally {}
  }
  
  public void closeIdleConnections(long paramLong, TimeUnit paramTimeUnit)
  {
    Args.notNull(paramTimeUnit, "Time unit");
    try
    {
      assertNotShutdown();
      long l = paramTimeUnit.toMillis(paramLong);
      paramLong = l;
      if (l < 0L) {
        paramLong = 0L;
      }
      l = System.currentTimeMillis();
      if ((this.poolEntry != null) && (this.poolEntry.getUpdated() <= l - paramLong))
      {
        this.poolEntry.close();
        this.poolEntry.getTracker().reset();
      }
      return;
    }
    finally {}
  }
  
  protected ClientConnectionOperator createConnectionOperator(SchemeRegistry paramSchemeRegistry)
  {
    return new DefaultClientConnectionOperator(paramSchemeRegistry);
  }
  
  protected void finalize()
    throws Throwable
  {
    try
    {
      shutdown();
      return;
    }
    finally
    {
      super.finalize();
    }
  }
  
  ManagedClientConnection getConnection(HttpRoute paramHttpRoute, Object paramObject)
  {
    Args.notNull(paramHttpRoute, "Route");
    for (;;)
    {
      try
      {
        assertNotShutdown();
        if (this.log.isDebugEnabled()) {
          this.log.debug("Get connection for route " + paramHttpRoute);
        }
        if (this.conn == null)
        {
          bool = true;
          Asserts.check(bool, "Invalid use of BasicClientConnManager: connection still allocated.\nMake sure to release the connection before allocating another one.");
          if ((this.poolEntry != null) && (!this.poolEntry.getPlannedRoute().equals(paramHttpRoute)))
          {
            this.poolEntry.close();
            this.poolEntry = null;
          }
          if (this.poolEntry == null)
          {
            paramObject = Long.toString(COUNTER.getAndIncrement());
            OperatedClientConnection localOperatedClientConnection = this.connOperator.createConnection();
            this.poolEntry = new HttpPoolEntry(this.log, (String)paramObject, paramHttpRoute, localOperatedClientConnection, 0L, TimeUnit.MILLISECONDS);
          }
          long l = System.currentTimeMillis();
          if (this.poolEntry.isExpired(l))
          {
            this.poolEntry.close();
            this.poolEntry.getTracker().reset();
          }
          this.conn = new ManagedClientConnectionImpl(this, this.connOperator, this.poolEntry);
          paramHttpRoute = this.conn;
          return paramHttpRoute;
        }
      }
      finally {}
      boolean bool = false;
    }
  }
  
  public SchemeRegistry getSchemeRegistry()
  {
    return this.schemeRegistry;
  }
  
  /* Error */
  public void releaseConnection(ManagedClientConnection paramManagedClientConnection, long paramLong, TimeUnit paramTimeUnit)
  {
    // Byte code:
    //   0: aload_1
    //   1: instanceof 214
    //   4: ldc -34
    //   6: invokestatic 223	cz/msebera/android/httpclient/util/Args:check	(ZLjava/lang/String;)V
    //   9: aload_1
    //   10: checkcast 214	cz/msebera/android/httpclient/impl/conn/ManagedClientConnectionImpl
    //   13: astore 6
    //   15: aload 6
    //   17: monitorenter
    //   18: aload_0
    //   19: getfield 60	cz/msebera/android/httpclient/impl/conn/BasicClientConnectionManager:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   22: invokevirtual 99	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:isDebugEnabled	()Z
    //   25: ifeq +29 -> 54
    //   28: aload_0
    //   29: getfield 60	cz/msebera/android/httpclient/impl/conn/BasicClientConnectionManager:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   32: new 163	java/lang/StringBuilder
    //   35: dup
    //   36: invokespecial 164	java/lang/StringBuilder:<init>	()V
    //   39: ldc -31
    //   41: invokevirtual 170	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   44: aload_1
    //   45: invokevirtual 173	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   48: invokevirtual 177	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   51: invokevirtual 179	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:debug	(Ljava/lang/Object;)V
    //   54: aload 6
    //   56: invokevirtual 229	cz/msebera/android/httpclient/impl/conn/ManagedClientConnectionImpl:getPoolEntry	()Lcz/msebera/android/httpclient/impl/conn/HttpPoolEntry;
    //   59: ifnonnull +7 -> 66
    //   62: aload 6
    //   64: monitorexit
    //   65: return
    //   66: aload 6
    //   68: invokevirtual 233	cz/msebera/android/httpclient/impl/conn/ManagedClientConnectionImpl:getManager	()Lcz/msebera/android/httpclient/conn/ClientConnectionManager;
    //   71: aload_0
    //   72: if_acmpne +40 -> 112
    //   75: iconst_1
    //   76: istore 5
    //   78: iload 5
    //   80: ldc -21
    //   82: invokestatic 87	cz/msebera/android/httpclient/util/Asserts:check	(ZLjava/lang/String;)V
    //   85: aload_0
    //   86: monitorenter
    //   87: aload_0
    //   88: getfield 79	cz/msebera/android/httpclient/impl/conn/BasicClientConnectionManager:shutdown	Z
    //   91: ifeq +27 -> 118
    //   94: aload_0
    //   95: aload 6
    //   97: invokespecial 237	cz/msebera/android/httpclient/impl/conn/BasicClientConnectionManager:shutdownConnection	(Lcz/msebera/android/httpclient/HttpClientConnection;)V
    //   100: aload_0
    //   101: monitorexit
    //   102: aload 6
    //   104: monitorexit
    //   105: return
    //   106: astore_1
    //   107: aload 6
    //   109: monitorexit
    //   110: aload_1
    //   111: athrow
    //   112: iconst_0
    //   113: istore 5
    //   115: goto -37 -> 78
    //   118: aload 6
    //   120: invokevirtual 240	cz/msebera/android/httpclient/impl/conn/ManagedClientConnectionImpl:isOpen	()Z
    //   123: ifeq +17 -> 140
    //   126: aload 6
    //   128: invokevirtual 243	cz/msebera/android/httpclient/impl/conn/ManagedClientConnectionImpl:isMarkedReusable	()Z
    //   131: ifne +9 -> 140
    //   134: aload_0
    //   135: aload 6
    //   137: invokespecial 237	cz/msebera/android/httpclient/impl/conn/BasicClientConnectionManager:shutdownConnection	(Lcz/msebera/android/httpclient/HttpClientConnection;)V
    //   140: aload 6
    //   142: invokevirtual 243	cz/msebera/android/httpclient/impl/conn/ManagedClientConnectionImpl:isMarkedReusable	()Z
    //   145: ifeq +96 -> 241
    //   148: aload_0
    //   149: getfield 116	cz/msebera/android/httpclient/impl/conn/BasicClientConnectionManager:poolEntry	Lcz/msebera/android/httpclient/impl/conn/HttpPoolEntry;
    //   152: astore 7
    //   154: aload 4
    //   156: ifnull +117 -> 273
    //   159: aload 4
    //   161: astore_1
    //   162: aload 7
    //   164: lload_2
    //   165: aload_1
    //   166: invokevirtual 246	cz/msebera/android/httpclient/impl/conn/HttpPoolEntry:updateExpiry	(JLjava/util/concurrent/TimeUnit;)V
    //   169: aload_0
    //   170: getfield 60	cz/msebera/android/httpclient/impl/conn/BasicClientConnectionManager:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   173: invokevirtual 99	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:isDebugEnabled	()Z
    //   176: ifeq +65 -> 241
    //   179: lload_2
    //   180: lconst_0
    //   181: lcmp
    //   182: ifle +98 -> 280
    //   185: new 163	java/lang/StringBuilder
    //   188: dup
    //   189: invokespecial 164	java/lang/StringBuilder:<init>	()V
    //   192: ldc -8
    //   194: invokevirtual 170	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   197: lload_2
    //   198: invokevirtual 251	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   201: ldc -3
    //   203: invokevirtual 170	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   206: aload 4
    //   208: invokevirtual 173	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   211: invokevirtual 177	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   214: astore_1
    //   215: aload_0
    //   216: getfield 60	cz/msebera/android/httpclient/impl/conn/BasicClientConnectionManager:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   219: new 163	java/lang/StringBuilder
    //   222: dup
    //   223: invokespecial 164	java/lang/StringBuilder:<init>	()V
    //   226: ldc -1
    //   228: invokevirtual 170	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   231: aload_1
    //   232: invokevirtual 170	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   235: invokevirtual 177	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   238: invokevirtual 179	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:debug	(Ljava/lang/Object;)V
    //   241: aload 6
    //   243: invokevirtual 258	cz/msebera/android/httpclient/impl/conn/ManagedClientConnectionImpl:detach	()Lcz/msebera/android/httpclient/impl/conn/HttpPoolEntry;
    //   246: pop
    //   247: aload_0
    //   248: aconst_null
    //   249: putfield 181	cz/msebera/android/httpclient/impl/conn/BasicClientConnectionManager:conn	Lcz/msebera/android/httpclient/impl/conn/ManagedClientConnectionImpl;
    //   252: aload_0
    //   253: getfield 116	cz/msebera/android/httpclient/impl/conn/BasicClientConnectionManager:poolEntry	Lcz/msebera/android/httpclient/impl/conn/HttpPoolEntry;
    //   256: invokevirtual 261	cz/msebera/android/httpclient/impl/conn/HttpPoolEntry:isClosed	()Z
    //   259: ifeq +8 -> 267
    //   262: aload_0
    //   263: aconst_null
    //   264: putfield 116	cz/msebera/android/httpclient/impl/conn/BasicClientConnectionManager:poolEntry	Lcz/msebera/android/httpclient/impl/conn/HttpPoolEntry;
    //   267: aload_0
    //   268: monitorexit
    //   269: aload 6
    //   271: monitorexit
    //   272: return
    //   273: getstatic 209	java/util/concurrent/TimeUnit:MILLISECONDS	Ljava/util/concurrent/TimeUnit;
    //   276: astore_1
    //   277: goto -115 -> 162
    //   280: ldc_w 263
    //   283: astore_1
    //   284: goto -69 -> 215
    //   287: astore_1
    //   288: aload 6
    //   290: invokevirtual 258	cz/msebera/android/httpclient/impl/conn/ManagedClientConnectionImpl:detach	()Lcz/msebera/android/httpclient/impl/conn/HttpPoolEntry;
    //   293: pop
    //   294: aload_0
    //   295: aconst_null
    //   296: putfield 181	cz/msebera/android/httpclient/impl/conn/BasicClientConnectionManager:conn	Lcz/msebera/android/httpclient/impl/conn/ManagedClientConnectionImpl;
    //   299: aload_0
    //   300: getfield 116	cz/msebera/android/httpclient/impl/conn/BasicClientConnectionManager:poolEntry	Lcz/msebera/android/httpclient/impl/conn/HttpPoolEntry;
    //   303: invokevirtual 261	cz/msebera/android/httpclient/impl/conn/HttpPoolEntry:isClosed	()Z
    //   306: ifeq +8 -> 314
    //   309: aload_0
    //   310: aconst_null
    //   311: putfield 116	cz/msebera/android/httpclient/impl/conn/BasicClientConnectionManager:poolEntry	Lcz/msebera/android/httpclient/impl/conn/HttpPoolEntry;
    //   314: aload_1
    //   315: athrow
    //   316: astore_1
    //   317: aload_0
    //   318: monitorexit
    //   319: aload_1
    //   320: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	321	0	this	BasicClientConnectionManager
    //   0	321	1	paramManagedClientConnection	ManagedClientConnection
    //   0	321	2	paramLong	long
    //   0	321	4	paramTimeUnit	TimeUnit
    //   76	38	5	bool	boolean
    //   13	276	6	localManagedClientConnectionImpl	ManagedClientConnectionImpl
    //   152	11	7	localHttpPoolEntry	HttpPoolEntry
    // Exception table:
    //   from	to	target	type
    //   18	54	106	finally
    //   54	65	106	finally
    //   66	75	106	finally
    //   78	87	106	finally
    //   102	105	106	finally
    //   107	110	106	finally
    //   269	272	106	finally
    //   319	321	106	finally
    //   118	140	287	finally
    //   140	154	287	finally
    //   162	179	287	finally
    //   185	215	287	finally
    //   215	241	287	finally
    //   273	277	287	finally
    //   87	102	316	finally
    //   241	267	316	finally
    //   267	269	316	finally
    //   288	314	316	finally
    //   314	316	316	finally
    //   317	319	316	finally
  }
  
  public final ClientConnectionRequest requestConnection(HttpRoute paramHttpRoute, Object paramObject)
  {
    return new BasicClientConnectionManager.1(this, paramHttpRoute, paramObject);
  }
  
  /* Error */
  public void shutdown()
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: iconst_1
    //   4: putfield 79	cz/msebera/android/httpclient/impl/conn/BasicClientConnectionManager:shutdown	Z
    //   7: aload_0
    //   8: getfield 116	cz/msebera/android/httpclient/impl/conn/BasicClientConnectionManager:poolEntry	Lcz/msebera/android/httpclient/impl/conn/HttpPoolEntry;
    //   11: ifnull +10 -> 21
    //   14: aload_0
    //   15: getfield 116	cz/msebera/android/httpclient/impl/conn/BasicClientConnectionManager:poolEntry	Lcz/msebera/android/httpclient/impl/conn/HttpPoolEntry;
    //   18: invokevirtual 125	cz/msebera/android/httpclient/impl/conn/HttpPoolEntry:close	()V
    //   21: aload_0
    //   22: aconst_null
    //   23: putfield 116	cz/msebera/android/httpclient/impl/conn/BasicClientConnectionManager:poolEntry	Lcz/msebera/android/httpclient/impl/conn/HttpPoolEntry;
    //   26: aload_0
    //   27: aconst_null
    //   28: putfield 181	cz/msebera/android/httpclient/impl/conn/BasicClientConnectionManager:conn	Lcz/msebera/android/httpclient/impl/conn/ManagedClientConnectionImpl;
    //   31: aload_0
    //   32: monitorexit
    //   33: return
    //   34: astore_1
    //   35: aload_0
    //   36: aconst_null
    //   37: putfield 116	cz/msebera/android/httpclient/impl/conn/BasicClientConnectionManager:poolEntry	Lcz/msebera/android/httpclient/impl/conn/HttpPoolEntry;
    //   40: aload_0
    //   41: aconst_null
    //   42: putfield 181	cz/msebera/android/httpclient/impl/conn/BasicClientConnectionManager:conn	Lcz/msebera/android/httpclient/impl/conn/ManagedClientConnectionImpl;
    //   45: aload_1
    //   46: athrow
    //   47: astore_1
    //   48: aload_0
    //   49: monitorexit
    //   50: aload_1
    //   51: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	52	0	this	BasicClientConnectionManager
    //   34	12	1	localObject1	Object
    //   47	4	1	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   7	21	34	finally
    //   2	7	47	finally
    //   21	33	47	finally
    //   35	47	47	finally
    //   48	50	47	finally
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/cz/msebera/android/httpclient/impl/conn/BasicClientConnectionManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */