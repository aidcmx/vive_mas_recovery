package cz.msebera.android.httpclient.impl.conn;

import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.annotation.ThreadSafe;
import cz.msebera.android.httpclient.conn.ClientConnectionOperator;
import cz.msebera.android.httpclient.conn.ConnectTimeoutException;
import cz.msebera.android.httpclient.conn.DnsResolver;
import cz.msebera.android.httpclient.conn.HttpInetSocketAddress;
import cz.msebera.android.httpclient.conn.OperatedClientConnection;
import cz.msebera.android.httpclient.conn.scheme.Scheme;
import cz.msebera.android.httpclient.conn.scheme.SchemeLayeredSocketFactory;
import cz.msebera.android.httpclient.conn.scheme.SchemeRegistry;
import cz.msebera.android.httpclient.conn.scheme.SchemeSocketFactory;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.params.HttpConnectionParams;
import cz.msebera.android.httpclient.params.HttpParams;
import cz.msebera.android.httpclient.protocol.HttpContext;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.Asserts;
import java.io.IOException;
import java.net.ConnectException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;

@Deprecated
@ThreadSafe
public class DefaultClientConnectionOperator
  implements ClientConnectionOperator
{
  protected final DnsResolver dnsResolver;
  public HttpClientAndroidLog log = new HttpClientAndroidLog(getClass());
  protected final SchemeRegistry schemeRegistry;
  
  public DefaultClientConnectionOperator(SchemeRegistry paramSchemeRegistry)
  {
    Args.notNull(paramSchemeRegistry, "Scheme registry");
    this.schemeRegistry = paramSchemeRegistry;
    this.dnsResolver = new SystemDefaultDnsResolver();
  }
  
  public DefaultClientConnectionOperator(SchemeRegistry paramSchemeRegistry, DnsResolver paramDnsResolver)
  {
    Args.notNull(paramSchemeRegistry, "Scheme registry");
    Args.notNull(paramDnsResolver, "DNS resolver");
    this.schemeRegistry = paramSchemeRegistry;
    this.dnsResolver = paramDnsResolver;
  }
  
  private SchemeRegistry getSchemeRegistry(HttpContext paramHttpContext)
  {
    SchemeRegistry localSchemeRegistry = (SchemeRegistry)paramHttpContext.getAttribute("http.scheme-registry");
    paramHttpContext = localSchemeRegistry;
    if (localSchemeRegistry == null) {
      paramHttpContext = this.schemeRegistry;
    }
    return paramHttpContext;
  }
  
  public OperatedClientConnection createConnection()
  {
    return new DefaultClientConnection();
  }
  
  public void openConnection(OperatedClientConnection paramOperatedClientConnection, HttpHost paramHttpHost, InetAddress paramInetAddress, HttpContext paramHttpContext, HttpParams paramHttpParams)
    throws IOException
  {
    Args.notNull(paramOperatedClientConnection, "Connection");
    Args.notNull(paramHttpHost, "Target host");
    Args.notNull(paramHttpParams, "HTTP parameters");
    boolean bool;
    Object localObject;
    SchemeSocketFactory localSchemeSocketFactory;
    InetAddress[] arrayOfInetAddress;
    int k;
    int i;
    if (!paramOperatedClientConnection.isOpen())
    {
      bool = true;
      Asserts.check(bool, "Connection must not be open");
      localObject = getSchemeRegistry(paramHttpContext).getScheme(paramHttpHost.getSchemeName());
      localSchemeSocketFactory = ((Scheme)localObject).getSchemeSocketFactory();
      arrayOfInetAddress = resolveHostname(paramHttpHost.getHostName());
      k = ((Scheme)localObject).resolvePort(paramHttpHost.getPort());
      i = 0;
    }
    int j;
    HttpInetSocketAddress localHttpInetSocketAddress;
    label279:
    label305:
    label352:
    for (;;)
    {
      if (i < arrayOfInetAddress.length)
      {
        localObject = arrayOfInetAddress[i];
        if (i != arrayOfInetAddress.length - 1) {
          break label279;
        }
      }
      for (j = 1;; j = 0)
      {
        Socket localSocket1 = localSchemeSocketFactory.createSocket(paramHttpParams);
        paramOperatedClientConnection.opening(localSocket1, paramHttpHost);
        localHttpInetSocketAddress = new HttpInetSocketAddress(paramHttpHost, (InetAddress)localObject, k);
        localObject = null;
        if (paramInetAddress != null) {
          localObject = new InetSocketAddress(paramInetAddress, 0);
        }
        if (this.log.isDebugEnabled()) {
          this.log.debug("Connecting to " + localHttpInetSocketAddress);
        }
        try
        {
          Socket localSocket2 = localSchemeSocketFactory.connectSocket(localSocket1, localHttpInetSocketAddress, (InetSocketAddress)localObject, paramHttpParams);
          localObject = localSocket1;
          if (localSocket1 != localSocket2)
          {
            localObject = localSocket2;
            paramOperatedClientConnection.opening((Socket)localObject, paramHttpHost);
          }
          prepareSocket((Socket)localObject, paramHttpContext, paramHttpParams);
          paramOperatedClientConnection.openCompleted(localSchemeSocketFactory.isSecure((Socket)localObject), paramHttpParams);
          return;
        }
        catch (ConnectException localConnectException)
        {
          if (j == 0) {
            break label305;
          }
          throw localConnectException;
        }
        catch (ConnectTimeoutException localConnectTimeoutException)
        {
          if (j == 0) {
            break label305;
          }
          throw localConnectTimeoutException;
          if (!this.log.isDebugEnabled()) {
            break label352;
          }
          this.log.debug("Connect to " + localHttpInetSocketAddress + " timed out. " + "Connection will be retried using another IP address");
          i += 1;
        }
        bool = false;
        break;
      }
    }
  }
  
  protected void prepareSocket(Socket paramSocket, HttpContext paramHttpContext, HttpParams paramHttpParams)
    throws IOException
  {
    paramSocket.setTcpNoDelay(HttpConnectionParams.getTcpNoDelay(paramHttpParams));
    paramSocket.setSoTimeout(HttpConnectionParams.getSoTimeout(paramHttpParams));
    int i = HttpConnectionParams.getLinger(paramHttpParams);
    if (i >= 0) {
      if (i <= 0) {
        break label44;
      }
    }
    label44:
    for (boolean bool = true;; bool = false)
    {
      paramSocket.setSoLinger(bool, i);
      return;
    }
  }
  
  protected InetAddress[] resolveHostname(String paramString)
    throws UnknownHostException
  {
    return this.dnsResolver.resolve(paramString);
  }
  
  public void updateSecureConnection(OperatedClientConnection paramOperatedClientConnection, HttpHost paramHttpHost, HttpContext paramHttpContext, HttpParams paramHttpParams)
    throws IOException
  {
    Args.notNull(paramOperatedClientConnection, "Connection");
    Args.notNull(paramHttpHost, "Target host");
    Args.notNull(paramHttpParams, "Parameters");
    Asserts.check(paramOperatedClientConnection.isOpen(), "Connection must be open");
    Object localObject = getSchemeRegistry(paramHttpContext).getScheme(paramHttpHost.getSchemeName());
    Asserts.check(((Scheme)localObject).getSchemeSocketFactory() instanceof SchemeLayeredSocketFactory, "Socket factory must implement SchemeLayeredSocketFactory");
    SchemeLayeredSocketFactory localSchemeLayeredSocketFactory = (SchemeLayeredSocketFactory)((Scheme)localObject).getSchemeSocketFactory();
    localObject = localSchemeLayeredSocketFactory.createLayeredSocket(paramOperatedClientConnection.getSocket(), paramHttpHost.getHostName(), ((Scheme)localObject).resolvePort(paramHttpHost.getPort()), paramHttpParams);
    prepareSocket((Socket)localObject, paramHttpContext, paramHttpParams);
    paramOperatedClientConnection.update((Socket)localObject, paramHttpHost, localSchemeLayeredSocketFactory.isSecure((Socket)localObject), paramHttpParams);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/cz/msebera/android/httpclient/impl/conn/DefaultClientConnectionOperator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */