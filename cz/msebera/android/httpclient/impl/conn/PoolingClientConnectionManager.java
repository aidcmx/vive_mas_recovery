package cz.msebera.android.httpclient.impl.conn;

import cz.msebera.android.httpclient.annotation.ThreadSafe;
import cz.msebera.android.httpclient.conn.ClientConnectionManager;
import cz.msebera.android.httpclient.conn.ClientConnectionOperator;
import cz.msebera.android.httpclient.conn.ClientConnectionRequest;
import cz.msebera.android.httpclient.conn.ConnectionPoolTimeoutException;
import cz.msebera.android.httpclient.conn.DnsResolver;
import cz.msebera.android.httpclient.conn.ManagedClientConnection;
import cz.msebera.android.httpclient.conn.routing.HttpRoute;
import cz.msebera.android.httpclient.conn.scheme.SchemeRegistry;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.pool.ConnPoolControl;
import cz.msebera.android.httpclient.pool.PoolStats;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.Asserts;
import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@Deprecated
@ThreadSafe
public class PoolingClientConnectionManager
  implements ClientConnectionManager, ConnPoolControl<HttpRoute>
{
  private final DnsResolver dnsResolver;
  public HttpClientAndroidLog log = new HttpClientAndroidLog(getClass());
  private final ClientConnectionOperator operator;
  private final HttpConnPool pool;
  private final SchemeRegistry schemeRegistry;
  
  public PoolingClientConnectionManager()
  {
    this(SchemeRegistryFactory.createDefault());
  }
  
  public PoolingClientConnectionManager(SchemeRegistry paramSchemeRegistry)
  {
    this(paramSchemeRegistry, -1L, TimeUnit.MILLISECONDS);
  }
  
  public PoolingClientConnectionManager(SchemeRegistry paramSchemeRegistry, long paramLong, TimeUnit paramTimeUnit)
  {
    this(paramSchemeRegistry, paramLong, paramTimeUnit, new SystemDefaultDnsResolver());
  }
  
  public PoolingClientConnectionManager(SchemeRegistry paramSchemeRegistry, long paramLong, TimeUnit paramTimeUnit, DnsResolver paramDnsResolver)
  {
    Args.notNull(paramSchemeRegistry, "Scheme registry");
    Args.notNull(paramDnsResolver, "DNS resolver");
    this.schemeRegistry = paramSchemeRegistry;
    this.dnsResolver = paramDnsResolver;
    this.operator = createConnectionOperator(paramSchemeRegistry);
    this.pool = new HttpConnPool(this.log, this.operator, 2, 20, paramLong, paramTimeUnit);
  }
  
  public PoolingClientConnectionManager(SchemeRegistry paramSchemeRegistry, DnsResolver paramDnsResolver)
  {
    this(paramSchemeRegistry, -1L, TimeUnit.MILLISECONDS, paramDnsResolver);
  }
  
  private String format(HttpRoute paramHttpRoute, Object paramObject)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("[route: ").append(paramHttpRoute).append("]");
    if (paramObject != null) {
      localStringBuilder.append("[state: ").append(paramObject).append("]");
    }
    return localStringBuilder.toString();
  }
  
  private String format(HttpPoolEntry paramHttpPoolEntry)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("[id: ").append(paramHttpPoolEntry.getId()).append("]");
    localStringBuilder.append("[route: ").append(paramHttpPoolEntry.getRoute()).append("]");
    paramHttpPoolEntry = paramHttpPoolEntry.getState();
    if (paramHttpPoolEntry != null) {
      localStringBuilder.append("[state: ").append(paramHttpPoolEntry).append("]");
    }
    return localStringBuilder.toString();
  }
  
  private String formatStats(HttpRoute paramHttpRoute)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    PoolStats localPoolStats = this.pool.getTotalStats();
    paramHttpRoute = this.pool.getStats(paramHttpRoute);
    localStringBuilder.append("[total kept alive: ").append(localPoolStats.getAvailable()).append("; ");
    localStringBuilder.append("route allocated: ").append(paramHttpRoute.getLeased() + paramHttpRoute.getAvailable());
    localStringBuilder.append(" of ").append(paramHttpRoute.getMax()).append("; ");
    localStringBuilder.append("total allocated: ").append(localPoolStats.getLeased() + localPoolStats.getAvailable());
    localStringBuilder.append(" of ").append(localPoolStats.getMax()).append("]");
    return localStringBuilder.toString();
  }
  
  public void closeExpiredConnections()
  {
    this.log.debug("Closing expired connections");
    this.pool.closeExpired();
  }
  
  public void closeIdleConnections(long paramLong, TimeUnit paramTimeUnit)
  {
    if (this.log.isDebugEnabled()) {
      this.log.debug("Closing connections idle longer than " + paramLong + " " + paramTimeUnit);
    }
    this.pool.closeIdle(paramLong, paramTimeUnit);
  }
  
  protected ClientConnectionOperator createConnectionOperator(SchemeRegistry paramSchemeRegistry)
  {
    return new DefaultClientConnectionOperator(paramSchemeRegistry, this.dnsResolver);
  }
  
  protected void finalize()
    throws Throwable
  {
    try
    {
      shutdown();
      return;
    }
    finally
    {
      super.finalize();
    }
  }
  
  public int getDefaultMaxPerRoute()
  {
    return this.pool.getDefaultMaxPerRoute();
  }
  
  public int getMaxPerRoute(HttpRoute paramHttpRoute)
  {
    return this.pool.getMaxPerRoute(paramHttpRoute);
  }
  
  public int getMaxTotal()
  {
    return this.pool.getMaxTotal();
  }
  
  public SchemeRegistry getSchemeRegistry()
  {
    return this.schemeRegistry;
  }
  
  public PoolStats getStats(HttpRoute paramHttpRoute)
  {
    return this.pool.getStats(paramHttpRoute);
  }
  
  public PoolStats getTotalStats()
  {
    return this.pool.getTotalStats();
  }
  
  ManagedClientConnection leaseConnection(Future<HttpPoolEntry> paramFuture, long paramLong, TimeUnit paramTimeUnit)
    throws InterruptedException, ConnectionPoolTimeoutException
  {
    try
    {
      paramTimeUnit = (HttpPoolEntry)paramFuture.get(paramLong, paramTimeUnit);
      if ((paramTimeUnit == null) || (paramFuture.isCancelled())) {
        throw new InterruptedException();
      }
    }
    catch (ExecutionException paramTimeUnit)
    {
      Throwable localThrowable = paramTimeUnit.getCause();
      paramFuture = localThrowable;
      if (localThrowable == null) {
        paramFuture = paramTimeUnit;
      }
      this.log.error("Unexpected exception leasing connection from pool", paramFuture);
      throw new InterruptedException();
      if (paramTimeUnit.getConnection() != null) {}
      for (boolean bool = true;; bool = false)
      {
        Asserts.check(bool, "Pool entry with no connection");
        if (this.log.isDebugEnabled()) {
          this.log.debug("Connection leased: " + format(paramTimeUnit) + formatStats((HttpRoute)paramTimeUnit.getRoute()));
        }
        paramFuture = new ManagedClientConnectionImpl(this, this.operator, paramTimeUnit);
        return paramFuture;
      }
    }
    catch (TimeoutException paramFuture)
    {
      throw new ConnectionPoolTimeoutException("Timeout waiting for connection from pool");
    }
  }
  
  /* Error */
  public void releaseConnection(ManagedClientConnection paramManagedClientConnection, long paramLong, TimeUnit paramTimeUnit)
  {
    // Byte code:
    //   0: aload_1
    //   1: instanceof 269
    //   4: ldc_w 285
    //   7: invokestatic 286	cz/msebera/android/httpclient/util/Args:check	(ZLjava/lang/String;)V
    //   10: aload_1
    //   11: checkcast 269	cz/msebera/android/httpclient/impl/conn/ManagedClientConnectionImpl
    //   14: astore 6
    //   16: aload 6
    //   18: invokevirtual 290	cz/msebera/android/httpclient/impl/conn/ManagedClientConnectionImpl:getManager	()Lcz/msebera/android/httpclient/conn/ClientConnectionManager;
    //   21: aload_0
    //   22: if_acmpne +33 -> 55
    //   25: iconst_1
    //   26: istore 5
    //   28: iload 5
    //   30: ldc_w 292
    //   33: invokestatic 261	cz/msebera/android/httpclient/util/Asserts:check	(ZLjava/lang/String;)V
    //   36: aload 6
    //   38: monitorenter
    //   39: aload 6
    //   41: invokevirtual 296	cz/msebera/android/httpclient/impl/conn/ManagedClientConnectionImpl:detach	()Lcz/msebera/android/httpclient/impl/conn/HttpPoolEntry;
    //   44: astore 7
    //   46: aload 7
    //   48: ifnonnull +13 -> 61
    //   51: aload 6
    //   53: monitorexit
    //   54: return
    //   55: iconst_0
    //   56: istore 5
    //   58: goto -30 -> 28
    //   61: aload 6
    //   63: invokevirtual 299	cz/msebera/android/httpclient/impl/conn/ManagedClientConnectionImpl:isOpen	()Z
    //   66: ifeq +20 -> 86
    //   69: aload 6
    //   71: invokevirtual 302	cz/msebera/android/httpclient/impl/conn/ManagedClientConnectionImpl:isMarkedReusable	()Z
    //   74: istore 5
    //   76: iload 5
    //   78: ifne +8 -> 86
    //   81: aload 6
    //   83: invokevirtual 303	cz/msebera/android/httpclient/impl/conn/ManagedClientConnectionImpl:shutdown	()V
    //   86: aload 6
    //   88: invokevirtual 302	cz/msebera/android/httpclient/impl/conn/ManagedClientConnectionImpl:isMarkedReusable	()Z
    //   91: ifeq +107 -> 198
    //   94: aload 4
    //   96: ifnull +225 -> 321
    //   99: aload 4
    //   101: astore_1
    //   102: aload 7
    //   104: lload_2
    //   105: aload_1
    //   106: invokevirtual 306	cz/msebera/android/httpclient/impl/conn/HttpPoolEntry:updateExpiry	(JLjava/util/concurrent/TimeUnit;)V
    //   109: aload_0
    //   110: getfield 63	cz/msebera/android/httpclient/impl/conn/PoolingClientConnectionManager:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   113: invokevirtual 178	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:isDebugEnabled	()Z
    //   116: ifeq +82 -> 198
    //   119: lload_2
    //   120: lconst_0
    //   121: lcmp
    //   122: ifle +206 -> 328
    //   125: new 95	java/lang/StringBuilder
    //   128: dup
    //   129: invokespecial 96	java/lang/StringBuilder:<init>	()V
    //   132: ldc_w 308
    //   135: invokevirtual 102	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   138: lload_2
    //   139: invokevirtual 183	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   142: ldc -71
    //   144: invokevirtual 102	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   147: aload 4
    //   149: invokevirtual 105	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   152: invokevirtual 113	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   155: astore_1
    //   156: aload_0
    //   157: getfield 63	cz/msebera/android/httpclient/impl/conn/PoolingClientConnectionManager:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   160: new 95	java/lang/StringBuilder
    //   163: dup
    //   164: invokespecial 96	java/lang/StringBuilder:<init>	()V
    //   167: ldc_w 310
    //   170: invokevirtual 102	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   173: aload_0
    //   174: aload 7
    //   176: invokespecial 265	cz/msebera/android/httpclient/impl/conn/PoolingClientConnectionManager:format	(Lcz/msebera/android/httpclient/impl/conn/HttpPoolEntry;)Ljava/lang/String;
    //   179: invokevirtual 102	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   182: ldc_w 312
    //   185: invokevirtual 102	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   188: aload_1
    //   189: invokevirtual 102	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   192: invokevirtual 113	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   195: invokevirtual 169	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:debug	(Ljava/lang/Object;)V
    //   198: aload_0
    //   199: getfield 90	cz/msebera/android/httpclient/impl/conn/PoolingClientConnectionManager:pool	Lcz/msebera/android/httpclient/impl/conn/HttpConnPool;
    //   202: aload 7
    //   204: aload 6
    //   206: invokevirtual 302	cz/msebera/android/httpclient/impl/conn/ManagedClientConnectionImpl:isMarkedReusable	()Z
    //   209: invokevirtual 316	cz/msebera/android/httpclient/impl/conn/HttpConnPool:release	(Lcz/msebera/android/httpclient/pool/PoolEntry;Z)V
    //   212: aload_0
    //   213: getfield 63	cz/msebera/android/httpclient/impl/conn/PoolingClientConnectionManager:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   216: invokevirtual 178	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:isDebugEnabled	()Z
    //   219: ifeq +50 -> 269
    //   222: aload_0
    //   223: getfield 63	cz/msebera/android/httpclient/impl/conn/PoolingClientConnectionManager:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   226: new 95	java/lang/StringBuilder
    //   229: dup
    //   230: invokespecial 96	java/lang/StringBuilder:<init>	()V
    //   233: ldc_w 318
    //   236: invokevirtual 102	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   239: aload_0
    //   240: aload 7
    //   242: invokespecial 265	cz/msebera/android/httpclient/impl/conn/PoolingClientConnectionManager:format	(Lcz/msebera/android/httpclient/impl/conn/HttpPoolEntry;)Ljava/lang/String;
    //   245: invokevirtual 102	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   248: aload_0
    //   249: aload 7
    //   251: invokevirtual 125	cz/msebera/android/httpclient/impl/conn/HttpPoolEntry:getRoute	()Ljava/lang/Object;
    //   254: checkcast 211	cz/msebera/android/httpclient/conn/routing/HttpRoute
    //   257: invokespecial 267	cz/msebera/android/httpclient/impl/conn/PoolingClientConnectionManager:formatStats	(Lcz/msebera/android/httpclient/conn/routing/HttpRoute;)Ljava/lang/String;
    //   260: invokevirtual 102	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   263: invokevirtual 113	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   266: invokevirtual 169	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:debug	(Ljava/lang/Object;)V
    //   269: aload 6
    //   271: monitorexit
    //   272: return
    //   273: astore_1
    //   274: aload 6
    //   276: monitorexit
    //   277: aload_1
    //   278: athrow
    //   279: astore_1
    //   280: aload_0
    //   281: getfield 63	cz/msebera/android/httpclient/impl/conn/PoolingClientConnectionManager:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   284: invokevirtual 178	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:isDebugEnabled	()Z
    //   287: ifeq -201 -> 86
    //   290: aload_0
    //   291: getfield 63	cz/msebera/android/httpclient/impl/conn/PoolingClientConnectionManager:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   294: ldc_w 320
    //   297: aload_1
    //   298: invokevirtual 322	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:debug	(Ljava/lang/Object;Ljava/lang/Throwable;)V
    //   301: goto -215 -> 86
    //   304: astore_1
    //   305: aload_0
    //   306: getfield 90	cz/msebera/android/httpclient/impl/conn/PoolingClientConnectionManager:pool	Lcz/msebera/android/httpclient/impl/conn/HttpConnPool;
    //   309: aload 7
    //   311: aload 6
    //   313: invokevirtual 302	cz/msebera/android/httpclient/impl/conn/ManagedClientConnectionImpl:isMarkedReusable	()Z
    //   316: invokevirtual 316	cz/msebera/android/httpclient/impl/conn/HttpConnPool:release	(Lcz/msebera/android/httpclient/pool/PoolEntry;Z)V
    //   319: aload_1
    //   320: athrow
    //   321: getstatic 41	java/util/concurrent/TimeUnit:MILLISECONDS	Ljava/util/concurrent/TimeUnit;
    //   324: astore_1
    //   325: goto -223 -> 102
    //   328: ldc_w 324
    //   331: astore_1
    //   332: goto -176 -> 156
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	335	0	this	PoolingClientConnectionManager
    //   0	335	1	paramManagedClientConnection	ManagedClientConnection
    //   0	335	2	paramLong	long
    //   0	335	4	paramTimeUnit	TimeUnit
    //   26	51	5	bool	boolean
    //   14	298	6	localManagedClientConnectionImpl	ManagedClientConnectionImpl
    //   44	266	7	localHttpPoolEntry	HttpPoolEntry
    // Exception table:
    //   from	to	target	type
    //   39	46	273	finally
    //   51	54	273	finally
    //   198	269	273	finally
    //   269	272	273	finally
    //   274	277	273	finally
    //   305	321	273	finally
    //   81	86	279	java/io/IOException
    //   61	76	304	finally
    //   81	86	304	finally
    //   86	94	304	finally
    //   102	119	304	finally
    //   125	156	304	finally
    //   156	198	304	finally
    //   280	301	304	finally
    //   321	325	304	finally
  }
  
  public ClientConnectionRequest requestConnection(HttpRoute paramHttpRoute, Object paramObject)
  {
    Args.notNull(paramHttpRoute, "HTTP route");
    if (this.log.isDebugEnabled()) {
      this.log.debug("Connection request: " + format(paramHttpRoute, paramObject) + formatStats(paramHttpRoute));
    }
    return new PoolingClientConnectionManager.1(this, this.pool.lease(paramHttpRoute, paramObject));
  }
  
  public void setDefaultMaxPerRoute(int paramInt)
  {
    this.pool.setDefaultMaxPerRoute(paramInt);
  }
  
  public void setMaxPerRoute(HttpRoute paramHttpRoute, int paramInt)
  {
    this.pool.setMaxPerRoute(paramHttpRoute, paramInt);
  }
  
  public void setMaxTotal(int paramInt)
  {
    this.pool.setMaxTotal(paramInt);
  }
  
  public void shutdown()
  {
    this.log.debug("Connection manager is shutting down");
    try
    {
      this.pool.shutdown();
      this.log.debug("Connection manager shut down");
      return;
    }
    catch (IOException localIOException)
    {
      for (;;)
      {
        this.log.debug("I/O exception shutting down connection manager", localIOException);
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/cz/msebera/android/httpclient/impl/conn/PoolingClientConnectionManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */