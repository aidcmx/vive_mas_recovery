package cz.msebera.android.httpclient.impl.conn;

import cz.msebera.android.httpclient.HttpException;
import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.conn.params.ConnRouteParams;
import cz.msebera.android.httpclient.conn.routing.HttpRoute;
import cz.msebera.android.httpclient.conn.routing.HttpRoutePlanner;
import cz.msebera.android.httpclient.conn.scheme.Scheme;
import cz.msebera.android.httpclient.conn.scheme.SchemeRegistry;
import cz.msebera.android.httpclient.protocol.HttpContext;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.Asserts;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.net.ProxySelector;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@Deprecated
@NotThreadSafe
public class ProxySelectorRoutePlanner
  implements HttpRoutePlanner
{
  protected ProxySelector proxySelector;
  protected final SchemeRegistry schemeRegistry;
  
  public ProxySelectorRoutePlanner(SchemeRegistry paramSchemeRegistry, ProxySelector paramProxySelector)
  {
    Args.notNull(paramSchemeRegistry, "SchemeRegistry");
    this.schemeRegistry = paramSchemeRegistry;
    this.proxySelector = paramProxySelector;
  }
  
  protected Proxy chooseProxy(List<Proxy> paramList, HttpHost paramHttpHost, HttpRequest paramHttpRequest, HttpContext paramHttpContext)
  {
    Args.notEmpty(paramList, "List of proxies");
    paramHttpHost = null;
    int i = 0;
    if ((paramHttpHost == null) && (i < paramList.size()))
    {
      paramHttpRequest = (Proxy)paramList.get(i);
      switch (ProxySelectorRoutePlanner.1.$SwitchMap$java$net$Proxy$Type[paramHttpRequest.type().ordinal()])
      {
      }
      for (;;)
      {
        i += 1;
        break;
        paramHttpHost = paramHttpRequest;
      }
    }
    paramList = paramHttpHost;
    if (paramHttpHost == null) {
      paramList = Proxy.NO_PROXY;
    }
    return paramList;
  }
  
  protected HttpHost determineProxy(HttpHost paramHttpHost, HttpRequest paramHttpRequest, HttpContext paramHttpContext)
    throws HttpException
  {
    Object localObject2 = this.proxySelector;
    Object localObject1 = localObject2;
    if (localObject2 == null) {
      localObject1 = ProxySelector.getDefault();
    }
    if (localObject1 == null) {}
    for (;;)
    {
      return null;
      try
      {
        localObject2 = new URI(paramHttpHost.toURI());
        paramHttpHost = chooseProxy(((ProxySelector)localObject1).select((URI)localObject2), paramHttpHost, paramHttpRequest, paramHttpContext);
        if (paramHttpHost.type() == Proxy.Type.HTTP) {
          if (!(paramHttpHost.address() instanceof InetSocketAddress)) {
            throw new HttpException("Unable to handle non-Inet proxy address: " + paramHttpHost.address());
          }
        }
      }
      catch (URISyntaxException paramHttpRequest)
      {
        throw new HttpException("Cannot convert host to URI: " + paramHttpHost, paramHttpRequest);
      }
    }
    paramHttpHost = (InetSocketAddress)paramHttpHost.address();
    return new HttpHost(getHost(paramHttpHost), paramHttpHost.getPort());
  }
  
  public HttpRoute determineRoute(HttpHost paramHttpHost, HttpRequest paramHttpRequest, HttpContext paramHttpContext)
    throws HttpException
  {
    Args.notNull(paramHttpRequest, "HTTP request");
    Object localObject = ConnRouteParams.getForcedRoute(paramHttpRequest.getParams());
    if (localObject != null) {
      return (HttpRoute)localObject;
    }
    Asserts.notNull(paramHttpHost, "Target host");
    localObject = ConnRouteParams.getLocalAddress(paramHttpRequest.getParams());
    paramHttpRequest = determineProxy(paramHttpHost, paramHttpRequest, paramHttpContext);
    boolean bool = this.schemeRegistry.getScheme(paramHttpHost.getSchemeName()).isLayered();
    if (paramHttpRequest == null) {}
    for (paramHttpHost = new HttpRoute(paramHttpHost, (InetAddress)localObject, bool);; paramHttpHost = new HttpRoute(paramHttpHost, (InetAddress)localObject, paramHttpRequest, bool)) {
      return paramHttpHost;
    }
  }
  
  protected String getHost(InetSocketAddress paramInetSocketAddress)
  {
    if (paramInetSocketAddress.isUnresolved()) {
      return paramInetSocketAddress.getHostName();
    }
    return paramInetSocketAddress.getAddress().getHostAddress();
  }
  
  public ProxySelector getProxySelector()
  {
    return this.proxySelector;
  }
  
  public void setProxySelector(ProxySelector paramProxySelector)
  {
    this.proxySelector = paramProxySelector;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/cz/msebera/android/httpclient/impl/conn/ProxySelectorRoutePlanner.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */