package cz.msebera.android.httpclient.impl.conn.tsccm;

import cz.msebera.android.httpclient.conn.routing.HttpRoute;
import cz.msebera.android.httpclient.util.Args;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;

@Deprecated
public class BasicPoolEntryRef
  extends WeakReference<BasicPoolEntry>
{
  private final HttpRoute route;
  
  public BasicPoolEntryRef(BasicPoolEntry paramBasicPoolEntry, ReferenceQueue<Object> paramReferenceQueue)
  {
    super(paramBasicPoolEntry, paramReferenceQueue);
    Args.notNull(paramBasicPoolEntry, "Pool entry");
    this.route = paramBasicPoolEntry.getPlannedRoute();
  }
  
  public final HttpRoute getRoute()
  {
    return this.route;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/cz/msebera/android/httpclient/impl/conn/tsccm/BasicPoolEntryRef.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */