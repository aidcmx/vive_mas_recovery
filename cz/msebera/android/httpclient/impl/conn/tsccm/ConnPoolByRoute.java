package cz.msebera.android.httpclient.impl.conn.tsccm;

import cz.msebera.android.httpclient.conn.ClientConnectionOperator;
import cz.msebera.android.httpclient.conn.OperatedClientConnection;
import cz.msebera.android.httpclient.conn.params.ConnManagerParams;
import cz.msebera.android.httpclient.conn.params.ConnPerRoute;
import cz.msebera.android.httpclient.conn.routing.HttpRoute;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.params.HttpParams;
import cz.msebera.android.httpclient.util.Args;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

@Deprecated
public class ConnPoolByRoute
  extends AbstractConnPool
{
  protected final ConnPerRoute connPerRoute;
  private final long connTTL;
  private final TimeUnit connTTLTimeUnit;
  protected final Queue<BasicPoolEntry> freeConnections;
  protected final Set<BasicPoolEntry> leasedConnections;
  public HttpClientAndroidLog log = new HttpClientAndroidLog(getClass());
  protected volatile int maxTotalConnections;
  protected volatile int numConnections;
  protected final ClientConnectionOperator operator;
  private final Lock poolLock;
  protected final Map<HttpRoute, RouteSpecificPool> routeToPool;
  protected volatile boolean shutdown;
  protected final Queue<WaitingThread> waitingThreads;
  
  public ConnPoolByRoute(ClientConnectionOperator paramClientConnectionOperator, ConnPerRoute paramConnPerRoute, int paramInt)
  {
    this(paramClientConnectionOperator, paramConnPerRoute, paramInt, -1L, TimeUnit.MILLISECONDS);
  }
  
  public ConnPoolByRoute(ClientConnectionOperator paramClientConnectionOperator, ConnPerRoute paramConnPerRoute, int paramInt, long paramLong, TimeUnit paramTimeUnit)
  {
    Args.notNull(paramClientConnectionOperator, "Connection operator");
    Args.notNull(paramConnPerRoute, "Connections per route");
    this.poolLock = this.poolLock;
    this.leasedConnections = this.leasedConnections;
    this.operator = paramClientConnectionOperator;
    this.connPerRoute = paramConnPerRoute;
    this.maxTotalConnections = paramInt;
    this.freeConnections = createFreeConnQueue();
    this.waitingThreads = createWaitingThreadQueue();
    this.routeToPool = createRouteToPoolMap();
    this.connTTL = paramLong;
    this.connTTLTimeUnit = paramTimeUnit;
  }
  
  @Deprecated
  public ConnPoolByRoute(ClientConnectionOperator paramClientConnectionOperator, HttpParams paramHttpParams)
  {
    this(paramClientConnectionOperator, ConnManagerParams.getMaxConnectionsPerRoute(paramHttpParams), ConnManagerParams.getMaxTotalConnections(paramHttpParams));
  }
  
  private void closeConnection(BasicPoolEntry paramBasicPoolEntry)
  {
    paramBasicPoolEntry = paramBasicPoolEntry.getConnection();
    if (paramBasicPoolEntry != null) {}
    try
    {
      paramBasicPoolEntry.close();
      return;
    }
    catch (IOException paramBasicPoolEntry)
    {
      this.log.debug("I/O error closing connection", paramBasicPoolEntry);
    }
  }
  
  public void closeExpiredConnections()
  {
    this.log.debug("Closing expired connections");
    long l = System.currentTimeMillis();
    this.poolLock.lock();
    try
    {
      Iterator localIterator = this.freeConnections.iterator();
      while (localIterator.hasNext())
      {
        BasicPoolEntry localBasicPoolEntry = (BasicPoolEntry)localIterator.next();
        if (localBasicPoolEntry.isExpired(l))
        {
          if (this.log.isDebugEnabled()) {
            this.log.debug("Closing connection expired @ " + new Date(localBasicPoolEntry.getExpiry()));
          }
          localIterator.remove();
          deleteEntry(localBasicPoolEntry);
        }
      }
    }
    finally
    {
      this.poolLock.unlock();
    }
  }
  
  public void closeIdleConnections(long paramLong, TimeUnit paramTimeUnit)
  {
    long l = 0L;
    Args.notNull(paramTimeUnit, "Time unit");
    if (paramLong > 0L) {
      l = paramLong;
    }
    if (this.log.isDebugEnabled()) {
      this.log.debug("Closing connections idle longer than " + l + " " + paramTimeUnit);
    }
    paramLong = System.currentTimeMillis();
    l = paramTimeUnit.toMillis(l);
    this.poolLock.lock();
    try
    {
      paramTimeUnit = this.freeConnections.iterator();
      while (paramTimeUnit.hasNext())
      {
        BasicPoolEntry localBasicPoolEntry = (BasicPoolEntry)paramTimeUnit.next();
        if (localBasicPoolEntry.getUpdated() <= paramLong - l)
        {
          if (this.log.isDebugEnabled()) {
            this.log.debug("Closing connection last used @ " + new Date(localBasicPoolEntry.getUpdated()));
          }
          paramTimeUnit.remove();
          deleteEntry(localBasicPoolEntry);
        }
      }
    }
    finally
    {
      this.poolLock.unlock();
    }
  }
  
  protected BasicPoolEntry createEntry(RouteSpecificPool paramRouteSpecificPool, ClientConnectionOperator paramClientConnectionOperator)
  {
    if (this.log.isDebugEnabled()) {
      this.log.debug("Creating new connection [" + paramRouteSpecificPool.getRoute() + "]");
    }
    paramClientConnectionOperator = new BasicPoolEntry(paramClientConnectionOperator, paramRouteSpecificPool.getRoute(), this.connTTL, this.connTTLTimeUnit);
    this.poolLock.lock();
    try
    {
      paramRouteSpecificPool.createdEntry(paramClientConnectionOperator);
      this.numConnections += 1;
      this.leasedConnections.add(paramClientConnectionOperator);
      return paramClientConnectionOperator;
    }
    finally
    {
      this.poolLock.unlock();
    }
  }
  
  protected Queue<BasicPoolEntry> createFreeConnQueue()
  {
    return new LinkedList();
  }
  
  protected Map<HttpRoute, RouteSpecificPool> createRouteToPoolMap()
  {
    return new HashMap();
  }
  
  protected Queue<WaitingThread> createWaitingThreadQueue()
  {
    return new LinkedList();
  }
  
  public void deleteClosedConnections()
  {
    this.poolLock.lock();
    try
    {
      Iterator localIterator = this.freeConnections.iterator();
      while (localIterator.hasNext())
      {
        BasicPoolEntry localBasicPoolEntry = (BasicPoolEntry)localIterator.next();
        if (!localBasicPoolEntry.getConnection().isOpen())
        {
          localIterator.remove();
          deleteEntry(localBasicPoolEntry);
        }
      }
    }
    finally
    {
      this.poolLock.unlock();
    }
  }
  
  protected void deleteEntry(BasicPoolEntry paramBasicPoolEntry)
  {
    HttpRoute localHttpRoute = paramBasicPoolEntry.getPlannedRoute();
    if (this.log.isDebugEnabled()) {
      this.log.debug("Deleting connection [" + localHttpRoute + "][" + paramBasicPoolEntry.getState() + "]");
    }
    this.poolLock.lock();
    try
    {
      closeConnection(paramBasicPoolEntry);
      RouteSpecificPool localRouteSpecificPool = getRoutePool(localHttpRoute, true);
      localRouteSpecificPool.deleteEntry(paramBasicPoolEntry);
      this.numConnections -= 1;
      if (localRouteSpecificPool.isUnused()) {
        this.routeToPool.remove(localHttpRoute);
      }
      return;
    }
    finally
    {
      this.poolLock.unlock();
    }
  }
  
  /* Error */
  protected void deleteLeastUsedEntry()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 75	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:poolLock	Ljava/util/concurrent/locks/Lock;
    //   4: invokeinterface 158 1 0
    //   9: aload_0
    //   10: getfield 90	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:freeConnections	Ljava/util/Queue;
    //   13: invokeinterface 304 1 0
    //   18: checkcast 127	cz/msebera/android/httpclient/impl/conn/tsccm/BasicPoolEntry
    //   21: astore_1
    //   22: aload_1
    //   23: ifnull +18 -> 41
    //   26: aload_0
    //   27: aload_1
    //   28: invokevirtual 211	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:deleteEntry	(Lcz/msebera/android/httpclient/impl/conn/tsccm/BasicPoolEntry;)V
    //   31: aload_0
    //   32: getfield 75	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:poolLock	Ljava/util/concurrent/locks/Lock;
    //   35: invokeinterface 214 1 0
    //   40: return
    //   41: aload_0
    //   42: getfield 62	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   45: invokevirtual 181	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:isDebugEnabled	()Z
    //   48: ifeq -17 -> 31
    //   51: aload_0
    //   52: getfield 62	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   55: ldc_w 306
    //   58: invokevirtual 147	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:debug	(Ljava/lang/Object;)V
    //   61: goto -30 -> 31
    //   64: astore_1
    //   65: aload_0
    //   66: getfield 75	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:poolLock	Ljava/util/concurrent/locks/Lock;
    //   69: invokeinterface 214 1 0
    //   74: aload_1
    //   75: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	76	0	this	ConnPoolByRoute
    //   21	7	1	localBasicPoolEntry	BasicPoolEntry
    //   64	11	1	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   9	22	64	finally
    //   26	31	64	finally
    //   41	61	64	finally
  }
  
  /* Error */
  public void freeEntry(BasicPoolEntry paramBasicPoolEntry, boolean paramBoolean, long paramLong, TimeUnit paramTimeUnit)
  {
    // Byte code:
    //   0: aload_1
    //   1: invokevirtual 277	cz/msebera/android/httpclient/impl/conn/tsccm/BasicPoolEntry:getPlannedRoute	()Lcz/msebera/android/httpclient/conn/routing/HttpRoute;
    //   4: astore 7
    //   6: aload_0
    //   7: getfield 62	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   10: invokevirtual 181	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:isDebugEnabled	()Z
    //   13: ifeq +49 -> 62
    //   16: aload_0
    //   17: getfield 62	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   20: new 183	java/lang/StringBuilder
    //   23: dup
    //   24: invokespecial 184	java/lang/StringBuilder:<init>	()V
    //   27: ldc_w 310
    //   30: invokevirtual 190	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   33: aload 7
    //   35: invokevirtual 201	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   38: ldc_w 281
    //   41: invokevirtual 190	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   44: aload_1
    //   45: invokevirtual 284	cz/msebera/android/httpclient/impl/conn/tsccm/BasicPoolEntry:getState	()Ljava/lang/Object;
    //   48: invokevirtual 201	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   51: ldc -10
    //   53: invokevirtual 190	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   56: invokevirtual 205	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   59: invokevirtual 147	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:debug	(Ljava/lang/Object;)V
    //   62: aload_0
    //   63: getfield 75	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:poolLock	Ljava/util/concurrent/locks/Lock;
    //   66: invokeinterface 158 1 0
    //   71: aload_0
    //   72: getfield 312	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:shutdown	Z
    //   75: ifeq +18 -> 93
    //   78: aload_0
    //   79: aload_1
    //   80: invokespecial 286	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:closeConnection	(Lcz/msebera/android/httpclient/impl/conn/tsccm/BasicPoolEntry;)V
    //   83: aload_0
    //   84: getfield 75	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:poolLock	Ljava/util/concurrent/locks/Lock;
    //   87: invokeinterface 214 1 0
    //   92: return
    //   93: aload_0
    //   94: getfield 78	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:leasedConnections	Ljava/util/Set;
    //   97: aload_1
    //   98: invokeinterface 314 2 0
    //   103: pop
    //   104: aload_0
    //   105: aload 7
    //   107: iconst_1
    //   108: invokevirtual 290	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:getRoutePool	(Lcz/msebera/android/httpclient/conn/routing/HttpRoute;Z)Lcz/msebera/android/httpclient/impl/conn/tsccm/RouteSpecificPool;
    //   111: astore 8
    //   113: iload_2
    //   114: ifeq +159 -> 273
    //   117: aload 8
    //   119: invokevirtual 318	cz/msebera/android/httpclient/impl/conn/tsccm/RouteSpecificPool:getCapacity	()I
    //   122: iflt +151 -> 273
    //   125: aload_0
    //   126: getfield 62	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   129: invokevirtual 181	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:isDebugEnabled	()Z
    //   132: ifeq +93 -> 225
    //   135: lload_3
    //   136: lconst_0
    //   137: lcmp
    //   138: ifle +127 -> 265
    //   141: new 183	java/lang/StringBuilder
    //   144: dup
    //   145: invokespecial 184	java/lang/StringBuilder:<init>	()V
    //   148: ldc_w 320
    //   151: invokevirtual 190	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   154: lload_3
    //   155: invokevirtual 223	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   158: ldc -31
    //   160: invokevirtual 190	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   163: aload 5
    //   165: invokevirtual 201	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   168: invokevirtual 205	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   171: astore 6
    //   173: aload_0
    //   174: getfield 62	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   177: new 183	java/lang/StringBuilder
    //   180: dup
    //   181: invokespecial 184	java/lang/StringBuilder:<init>	()V
    //   184: ldc_w 322
    //   187: invokevirtual 190	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   190: aload 7
    //   192: invokevirtual 201	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   195: ldc_w 281
    //   198: invokevirtual 190	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   201: aload_1
    //   202: invokevirtual 284	cz/msebera/android/httpclient/impl/conn/tsccm/BasicPoolEntry:getState	()Ljava/lang/Object;
    //   205: invokevirtual 201	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   208: ldc_w 324
    //   211: invokevirtual 190	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   214: aload 6
    //   216: invokevirtual 190	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   219: invokevirtual 205	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   222: invokevirtual 147	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:debug	(Ljava/lang/Object;)V
    //   225: aload 8
    //   227: aload_1
    //   228: invokevirtual 326	cz/msebera/android/httpclient/impl/conn/tsccm/RouteSpecificPool:freeEntry	(Lcz/msebera/android/httpclient/impl/conn/tsccm/BasicPoolEntry;)V
    //   231: aload_1
    //   232: lload_3
    //   233: aload 5
    //   235: invokevirtual 329	cz/msebera/android/httpclient/impl/conn/tsccm/BasicPoolEntry:updateExpiry	(JLjava/util/concurrent/TimeUnit;)V
    //   238: aload_0
    //   239: getfield 90	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:freeConnections	Ljava/util/Queue;
    //   242: aload_1
    //   243: invokeinterface 330 2 0
    //   248: pop
    //   249: aload_0
    //   250: aload 8
    //   252: invokevirtual 334	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:notifyWaitingThread	(Lcz/msebera/android/httpclient/impl/conn/tsccm/RouteSpecificPool;)V
    //   255: aload_0
    //   256: getfield 75	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:poolLock	Ljava/util/concurrent/locks/Lock;
    //   259: invokeinterface 214 1 0
    //   264: return
    //   265: ldc_w 336
    //   268: astore 6
    //   270: goto -97 -> 173
    //   273: aload_0
    //   274: aload_1
    //   275: invokespecial 286	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:closeConnection	(Lcz/msebera/android/httpclient/impl/conn/tsccm/BasicPoolEntry;)V
    //   278: aload 8
    //   280: invokevirtual 339	cz/msebera/android/httpclient/impl/conn/tsccm/RouteSpecificPool:dropEntry	()V
    //   283: aload_0
    //   284: aload_0
    //   285: getfield 254	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:numConnections	I
    //   288: iconst_1
    //   289: isub
    //   290: putfield 254	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:numConnections	I
    //   293: goto -44 -> 249
    //   296: astore_1
    //   297: aload_0
    //   298: getfield 75	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:poolLock	Ljava/util/concurrent/locks/Lock;
    //   301: invokeinterface 214 1 0
    //   306: aload_1
    //   307: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	308	0	this	ConnPoolByRoute
    //   0	308	1	paramBasicPoolEntry	BasicPoolEntry
    //   0	308	2	paramBoolean	boolean
    //   0	308	3	paramLong	long
    //   0	308	5	paramTimeUnit	TimeUnit
    //   171	98	6	str	String
    //   4	187	7	localHttpRoute	HttpRoute
    //   111	168	8	localRouteSpecificPool	RouteSpecificPool
    // Exception table:
    //   from	to	target	type
    //   71	83	296	finally
    //   93	113	296	finally
    //   117	135	296	finally
    //   141	173	296	finally
    //   173	225	296	finally
    //   225	249	296	finally
    //   249	255	296	finally
    //   273	293	296	finally
  }
  
  public int getConnectionsInPool()
  {
    this.poolLock.lock();
    try
    {
      int i = this.numConnections;
      return i;
    }
    finally
    {
      this.poolLock.unlock();
    }
  }
  
  public int getConnectionsInPool(HttpRoute paramHttpRoute)
  {
    int i = 0;
    this.poolLock.lock();
    try
    {
      paramHttpRoute = getRoutePool(paramHttpRoute, false);
      if (paramHttpRoute != null) {
        i = paramHttpRoute.getEntryCount();
      }
      return i;
    }
    finally
    {
      this.poolLock.unlock();
    }
  }
  
  /* Error */
  protected BasicPoolEntry getEntryBlocking(HttpRoute paramHttpRoute, Object paramObject, long paramLong, TimeUnit paramTimeUnit, WaitingThreadAborter paramWaitingThreadAborter)
    throws cz.msebera.android.httpclient.conn.ConnectionPoolTimeoutException, java.lang.InterruptedException
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 10
    //   3: lload_3
    //   4: lconst_0
    //   5: lcmp
    //   6: ifle +22 -> 28
    //   9: new 192	java/util/Date
    //   12: dup
    //   13: invokestatic 153	java/lang/System:currentTimeMillis	()J
    //   16: aload 5
    //   18: lload_3
    //   19: invokevirtual 229	java/util/concurrent/TimeUnit:toMillis	(J)J
    //   22: ladd
    //   23: invokespecial 198	java/util/Date:<init>	(J)V
    //   26: astore 10
    //   28: aconst_null
    //   29: astore 5
    //   31: aload_0
    //   32: getfield 75	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:poolLock	Ljava/util/concurrent/locks/Lock;
    //   35: invokeinterface 158 1 0
    //   40: aload_0
    //   41: aload_1
    //   42: iconst_1
    //   43: invokevirtual 290	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:getRoutePool	(Lcz/msebera/android/httpclient/conn/routing/HttpRoute;Z)Lcz/msebera/android/httpclient/impl/conn/tsccm/RouteSpecificPool;
    //   46: astore 12
    //   48: aconst_null
    //   49: astore 11
    //   51: aload 5
    //   53: astore 9
    //   55: aload 5
    //   57: ifnonnull +134 -> 191
    //   60: aload_0
    //   61: getfield 312	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:shutdown	Z
    //   64: ifne +139 -> 203
    //   67: iconst_1
    //   68: istore 8
    //   70: iload 8
    //   72: ldc_w 352
    //   75: invokestatic 358	cz/msebera/android/httpclient/util/Asserts:check	(ZLjava/lang/String;)V
    //   78: aload_0
    //   79: getfield 62	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   82: invokevirtual 181	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:isDebugEnabled	()Z
    //   85: ifeq +92 -> 177
    //   88: aload_0
    //   89: getfield 62	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   92: new 183	java/lang/StringBuilder
    //   95: dup
    //   96: invokespecial 184	java/lang/StringBuilder:<init>	()V
    //   99: ldc_w 360
    //   102: invokevirtual 190	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   105: aload_1
    //   106: invokevirtual 201	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   109: ldc_w 362
    //   112: invokevirtual 190	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   115: aload_0
    //   116: getfield 90	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:freeConnections	Ljava/util/Queue;
    //   119: invokeinterface 365 1 0
    //   124: invokevirtual 368	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   127: ldc_w 370
    //   130: invokevirtual 190	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   133: aload_0
    //   134: getfield 78	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:leasedConnections	Ljava/util/Set;
    //   137: invokeinterface 371 1 0
    //   142: invokevirtual 368	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   145: ldc_w 373
    //   148: invokevirtual 190	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   151: aload_0
    //   152: getfield 254	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:numConnections	I
    //   155: invokevirtual 368	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   158: ldc_w 375
    //   161: invokevirtual 190	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   164: aload_0
    //   165: getfield 84	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:maxTotalConnections	I
    //   168: invokevirtual 368	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   171: invokevirtual 205	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   174: invokevirtual 147	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:debug	(Ljava/lang/Object;)V
    //   177: aload_0
    //   178: aload 12
    //   180: aload_2
    //   181: invokevirtual 379	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:getFreeEntry	(Lcz/msebera/android/httpclient/impl/conn/tsccm/RouteSpecificPool;Ljava/lang/Object;)Lcz/msebera/android/httpclient/impl/conn/tsccm/BasicPoolEntry;
    //   184: astore 9
    //   186: aload 9
    //   188: ifnull +21 -> 209
    //   191: aload_0
    //   192: getfield 75	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:poolLock	Ljava/util/concurrent/locks/Lock;
    //   195: invokeinterface 214 1 0
    //   200: aload 9
    //   202: areturn
    //   203: iconst_0
    //   204: istore 8
    //   206: goto -136 -> 70
    //   209: aload 12
    //   211: invokevirtual 318	cz/msebera/android/httpclient/impl/conn/tsccm/RouteSpecificPool:getCapacity	()I
    //   214: ifle +384 -> 598
    //   217: iconst_1
    //   218: istore 7
    //   220: aload_0
    //   221: getfield 62	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   224: invokevirtual 181	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:isDebugEnabled	()Z
    //   227: ifeq +73 -> 300
    //   230: aload_0
    //   231: getfield 62	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   234: new 183	java/lang/StringBuilder
    //   237: dup
    //   238: invokespecial 184	java/lang/StringBuilder:<init>	()V
    //   241: ldc_w 381
    //   244: invokevirtual 190	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   247: aload 12
    //   249: invokevirtual 318	cz/msebera/android/httpclient/impl/conn/tsccm/RouteSpecificPool:getCapacity	()I
    //   252: invokevirtual 368	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   255: ldc_w 375
    //   258: invokevirtual 190	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   261: aload 12
    //   263: invokevirtual 384	cz/msebera/android/httpclient/impl/conn/tsccm/RouteSpecificPool:getMaxEntries	()I
    //   266: invokevirtual 368	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   269: ldc_w 386
    //   272: invokevirtual 190	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   275: aload_1
    //   276: invokevirtual 201	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   279: ldc_w 281
    //   282: invokevirtual 190	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   285: aload_2
    //   286: invokevirtual 201	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   289: ldc -10
    //   291: invokevirtual 190	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   294: invokevirtual 205	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   297: invokevirtual 147	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:debug	(Ljava/lang/Object;)V
    //   300: iload 7
    //   302: ifeq +29 -> 331
    //   305: aload_0
    //   306: getfield 254	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:numConnections	I
    //   309: aload_0
    //   310: getfield 84	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:maxTotalConnections	I
    //   313: if_icmpge +18 -> 331
    //   316: aload_0
    //   317: aload 12
    //   319: aload_0
    //   320: getfield 80	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:operator	Lcz/msebera/android/httpclient/conn/ClientConnectionOperator;
    //   323: invokevirtual 388	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:createEntry	(Lcz/msebera/android/httpclient/impl/conn/tsccm/RouteSpecificPool;Lcz/msebera/android/httpclient/conn/ClientConnectionOperator;)Lcz/msebera/android/httpclient/impl/conn/tsccm/BasicPoolEntry;
    //   326: astore 5
    //   328: goto -277 -> 51
    //   331: iload 7
    //   333: ifeq +42 -> 375
    //   336: aload_0
    //   337: getfield 90	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:freeConnections	Ljava/util/Queue;
    //   340: invokeinterface 391 1 0
    //   345: ifne +30 -> 375
    //   348: aload_0
    //   349: invokevirtual 393	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:deleteLeastUsedEntry	()V
    //   352: aload_0
    //   353: aload_1
    //   354: iconst_1
    //   355: invokevirtual 290	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:getRoutePool	(Lcz/msebera/android/httpclient/conn/routing/HttpRoute;Z)Lcz/msebera/android/httpclient/impl/conn/tsccm/RouteSpecificPool;
    //   358: astore 12
    //   360: aload_0
    //   361: aload 12
    //   363: aload_0
    //   364: getfield 80	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:operator	Lcz/msebera/android/httpclient/conn/ClientConnectionOperator;
    //   367: invokevirtual 388	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:createEntry	(Lcz/msebera/android/httpclient/impl/conn/tsccm/RouteSpecificPool;Lcz/msebera/android/httpclient/conn/ClientConnectionOperator;)Lcz/msebera/android/httpclient/impl/conn/tsccm/BasicPoolEntry;
    //   370: astore 5
    //   372: goto -321 -> 51
    //   375: aload_0
    //   376: getfield 62	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   379: invokevirtual 181	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:isDebugEnabled	()Z
    //   382: ifeq +45 -> 427
    //   385: aload_0
    //   386: getfield 62	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   389: new 183	java/lang/StringBuilder
    //   392: dup
    //   393: invokespecial 184	java/lang/StringBuilder:<init>	()V
    //   396: ldc_w 395
    //   399: invokevirtual 190	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   402: aload_1
    //   403: invokevirtual 201	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   406: ldc_w 281
    //   409: invokevirtual 190	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   412: aload_2
    //   413: invokevirtual 201	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   416: ldc -10
    //   418: invokevirtual 190	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   421: invokevirtual 205	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   424: invokevirtual 147	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:debug	(Ljava/lang/Object;)V
    //   427: aload 11
    //   429: astore 13
    //   431: aload 11
    //   433: ifnonnull +27 -> 460
    //   436: aload_0
    //   437: aload_0
    //   438: getfield 75	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:poolLock	Ljava/util/concurrent/locks/Lock;
    //   441: invokeinterface 399 1 0
    //   446: aload 12
    //   448: invokevirtual 403	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:newWaitingThread	(Ljava/util/concurrent/locks/Condition;Lcz/msebera/android/httpclient/impl/conn/tsccm/RouteSpecificPool;)Lcz/msebera/android/httpclient/impl/conn/tsccm/WaitingThread;
    //   451: astore 13
    //   453: aload 6
    //   455: aload 13
    //   457: invokevirtual 409	cz/msebera/android/httpclient/impl/conn/tsccm/WaitingThreadAborter:setWaitingThread	(Lcz/msebera/android/httpclient/impl/conn/tsccm/WaitingThread;)V
    //   460: aload 12
    //   462: aload 13
    //   464: invokevirtual 412	cz/msebera/android/httpclient/impl/conn/tsccm/RouteSpecificPool:queueThread	(Lcz/msebera/android/httpclient/impl/conn/tsccm/WaitingThread;)V
    //   467: aload_0
    //   468: getfield 95	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:waitingThreads	Ljava/util/Queue;
    //   471: aload 13
    //   473: invokeinterface 330 2 0
    //   478: pop
    //   479: aload 13
    //   481: aload 10
    //   483: invokevirtual 418	cz/msebera/android/httpclient/impl/conn/tsccm/WaitingThread:await	(Ljava/util/Date;)Z
    //   486: istore 8
    //   488: aload 12
    //   490: aload 13
    //   492: invokevirtual 421	cz/msebera/android/httpclient/impl/conn/tsccm/RouteSpecificPool:removeThread	(Lcz/msebera/android/httpclient/impl/conn/tsccm/WaitingThread;)V
    //   495: aload_0
    //   496: getfield 95	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:waitingThreads	Ljava/util/Queue;
    //   499: aload 13
    //   501: invokeinterface 422 2 0
    //   506: pop
    //   507: aload 9
    //   509: astore 5
    //   511: aload 13
    //   513: astore 11
    //   515: iload 8
    //   517: ifne -466 -> 51
    //   520: aload 9
    //   522: astore 5
    //   524: aload 13
    //   526: astore 11
    //   528: aload 10
    //   530: ifnull -479 -> 51
    //   533: aload 9
    //   535: astore 5
    //   537: aload 13
    //   539: astore 11
    //   541: aload 10
    //   543: invokevirtual 425	java/util/Date:getTime	()J
    //   546: invokestatic 153	java/lang/System:currentTimeMillis	()J
    //   549: lcmp
    //   550: ifgt -499 -> 51
    //   553: new 348	cz/msebera/android/httpclient/conn/ConnectionPoolTimeoutException
    //   556: dup
    //   557: ldc_w 427
    //   560: invokespecial 430	cz/msebera/android/httpclient/conn/ConnectionPoolTimeoutException:<init>	(Ljava/lang/String;)V
    //   563: athrow
    //   564: astore_1
    //   565: aload_0
    //   566: getfield 75	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:poolLock	Ljava/util/concurrent/locks/Lock;
    //   569: invokeinterface 214 1 0
    //   574: aload_1
    //   575: athrow
    //   576: astore_1
    //   577: aload 12
    //   579: aload 13
    //   581: invokevirtual 421	cz/msebera/android/httpclient/impl/conn/tsccm/RouteSpecificPool:removeThread	(Lcz/msebera/android/httpclient/impl/conn/tsccm/WaitingThread;)V
    //   584: aload_0
    //   585: getfield 95	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:waitingThreads	Ljava/util/Queue;
    //   588: aload 13
    //   590: invokeinterface 422 2 0
    //   595: pop
    //   596: aload_1
    //   597: athrow
    //   598: iconst_0
    //   599: istore 7
    //   601: goto -381 -> 220
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	604	0	this	ConnPoolByRoute
    //   0	604	1	paramHttpRoute	HttpRoute
    //   0	604	2	paramObject	Object
    //   0	604	3	paramLong	long
    //   0	604	5	paramTimeUnit	TimeUnit
    //   0	604	6	paramWaitingThreadAborter	WaitingThreadAborter
    //   218	382	7	i	int
    //   68	448	8	bool	boolean
    //   53	481	9	localObject1	Object
    //   1	541	10	localDate	Date
    //   49	491	11	localObject2	Object
    //   46	532	12	localRouteSpecificPool	RouteSpecificPool
    //   429	160	13	localObject3	Object
    // Exception table:
    //   from	to	target	type
    //   40	48	564	finally
    //   60	67	564	finally
    //   70	177	564	finally
    //   177	186	564	finally
    //   209	217	564	finally
    //   220	300	564	finally
    //   305	328	564	finally
    //   336	372	564	finally
    //   375	427	564	finally
    //   436	460	564	finally
    //   488	507	564	finally
    //   541	564	564	finally
    //   577	598	564	finally
    //   460	488	576	finally
  }
  
  protected BasicPoolEntry getFreeEntry(RouteSpecificPool paramRouteSpecificPool, Object paramObject)
  {
    Object localObject = null;
    this.poolLock.lock();
    int i = 0;
    while (i == 0)
    {
      BasicPoolEntry localBasicPoolEntry;
      try
      {
        localBasicPoolEntry = paramRouteSpecificPool.allocEntry(paramObject);
        if (localBasicPoolEntry == null) {
          break label223;
        }
        if (this.log.isDebugEnabled()) {
          this.log.debug("Getting free connection [" + paramRouteSpecificPool.getRoute() + "][" + paramObject + "]");
        }
        this.freeConnections.remove(localBasicPoolEntry);
        if (localBasicPoolEntry.isExpired(System.currentTimeMillis()))
        {
          if (this.log.isDebugEnabled()) {
            this.log.debug("Closing expired free connection [" + paramRouteSpecificPool.getRoute() + "][" + paramObject + "]");
          }
          closeConnection(localBasicPoolEntry);
          paramRouteSpecificPool.dropEntry();
          this.numConnections -= 1;
          localObject = localBasicPoolEntry;
          continue;
        }
      }
      finally
      {
        this.poolLock.unlock();
      }
      this.leasedConnections.add(localBasicPoolEntry);
      i = 1;
      localObject = localBasicPoolEntry;
      continue;
      label223:
      int j = 1;
      i = j;
      localObject = localBasicPoolEntry;
      if (this.log.isDebugEnabled())
      {
        this.log.debug("No free connections [" + paramRouteSpecificPool.getRoute() + "][" + paramObject + "]");
        i = j;
        localObject = localBasicPoolEntry;
      }
    }
    this.poolLock.unlock();
    return (BasicPoolEntry)localObject;
  }
  
  protected Lock getLock()
  {
    return this.poolLock;
  }
  
  public int getMaxTotalConnections()
  {
    return this.maxTotalConnections;
  }
  
  protected RouteSpecificPool getRoutePool(HttpRoute paramHttpRoute, boolean paramBoolean)
  {
    this.poolLock.lock();
    try
    {
      RouteSpecificPool localRouteSpecificPool2 = (RouteSpecificPool)this.routeToPool.get(paramHttpRoute);
      RouteSpecificPool localRouteSpecificPool1 = localRouteSpecificPool2;
      if (localRouteSpecificPool2 == null)
      {
        localRouteSpecificPool1 = localRouteSpecificPool2;
        if (paramBoolean)
        {
          localRouteSpecificPool1 = newRouteSpecificPool(paramHttpRoute);
          this.routeToPool.put(paramHttpRoute, localRouteSpecificPool1);
        }
      }
      return localRouteSpecificPool1;
    }
    finally
    {
      this.poolLock.unlock();
    }
  }
  
  protected void handleLostEntry(HttpRoute paramHttpRoute)
  {
    this.poolLock.lock();
    try
    {
      RouteSpecificPool localRouteSpecificPool = getRoutePool(paramHttpRoute, true);
      localRouteSpecificPool.dropEntry();
      if (localRouteSpecificPool.isUnused()) {
        this.routeToPool.remove(paramHttpRoute);
      }
      this.numConnections -= 1;
      notifyWaitingThread(localRouteSpecificPool);
      return;
    }
    finally
    {
      this.poolLock.unlock();
    }
  }
  
  protected RouteSpecificPool newRouteSpecificPool(HttpRoute paramHttpRoute)
  {
    return new RouteSpecificPool(paramHttpRoute, this.connPerRoute);
  }
  
  protected WaitingThread newWaitingThread(Condition paramCondition, RouteSpecificPool paramRouteSpecificPool)
  {
    return new WaitingThread(paramCondition, paramRouteSpecificPool);
  }
  
  /* Error */
  protected void notifyWaitingThread(RouteSpecificPool paramRouteSpecificPool)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_2
    //   2: aload_0
    //   3: getfield 75	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:poolLock	Ljava/util/concurrent/locks/Lock;
    //   6: invokeinterface 158 1 0
    //   11: aload_1
    //   12: ifnull +78 -> 90
    //   15: aload_1
    //   16: invokevirtual 465	cz/msebera/android/httpclient/impl/conn/tsccm/RouteSpecificPool:hasThread	()Z
    //   19: ifeq +71 -> 90
    //   22: aload_0
    //   23: getfield 62	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   26: invokevirtual 181	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:isDebugEnabled	()Z
    //   29: ifeq +38 -> 67
    //   32: aload_0
    //   33: getfield 62	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   36: new 183	java/lang/StringBuilder
    //   39: dup
    //   40: invokespecial 184	java/lang/StringBuilder:<init>	()V
    //   43: ldc_w 467
    //   46: invokevirtual 190	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   49: aload_1
    //   50: invokevirtual 244	cz/msebera/android/httpclient/impl/conn/tsccm/RouteSpecificPool:getRoute	()Lcz/msebera/android/httpclient/conn/routing/HttpRoute;
    //   53: invokevirtual 201	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   56: ldc -10
    //   58: invokevirtual 190	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   61: invokevirtual 205	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   64: invokevirtual 147	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:debug	(Ljava/lang/Object;)V
    //   67: aload_1
    //   68: invokevirtual 471	cz/msebera/android/httpclient/impl/conn/tsccm/RouteSpecificPool:nextThread	()Lcz/msebera/android/httpclient/impl/conn/tsccm/WaitingThread;
    //   71: astore_1
    //   72: aload_1
    //   73: ifnull +7 -> 80
    //   76: aload_1
    //   77: invokevirtual 474	cz/msebera/android/httpclient/impl/conn/tsccm/WaitingThread:wakeup	()V
    //   80: aload_0
    //   81: getfield 75	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:poolLock	Ljava/util/concurrent/locks/Lock;
    //   84: invokeinterface 214 1 0
    //   89: return
    //   90: aload_0
    //   91: getfield 95	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:waitingThreads	Ljava/util/Queue;
    //   94: invokeinterface 391 1 0
    //   99: ifne +39 -> 138
    //   102: aload_0
    //   103: getfield 62	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   106: invokevirtual 181	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:isDebugEnabled	()Z
    //   109: ifeq +13 -> 122
    //   112: aload_0
    //   113: getfield 62	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   116: ldc_w 476
    //   119: invokevirtual 147	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:debug	(Ljava/lang/Object;)V
    //   122: aload_0
    //   123: getfield 95	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:waitingThreads	Ljava/util/Queue;
    //   126: invokeinterface 304 1 0
    //   131: checkcast 414	cz/msebera/android/httpclient/impl/conn/tsccm/WaitingThread
    //   134: astore_1
    //   135: goto -63 -> 72
    //   138: aload_2
    //   139: astore_1
    //   140: aload_0
    //   141: getfield 62	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   144: invokevirtual 181	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:isDebugEnabled	()Z
    //   147: ifeq -75 -> 72
    //   150: aload_0
    //   151: getfield 62	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:log	Lcz/msebera/android/httpclient/extras/HttpClientAndroidLog;
    //   154: ldc_w 478
    //   157: invokevirtual 147	cz/msebera/android/httpclient/extras/HttpClientAndroidLog:debug	(Ljava/lang/Object;)V
    //   160: aload_2
    //   161: astore_1
    //   162: goto -90 -> 72
    //   165: astore_1
    //   166: aload_0
    //   167: getfield 75	cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute:poolLock	Ljava/util/concurrent/locks/Lock;
    //   170: invokeinterface 214 1 0
    //   175: aload_1
    //   176: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	177	0	this	ConnPoolByRoute
    //   0	177	1	paramRouteSpecificPool	RouteSpecificPool
    //   1	160	2	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   15	67	165	finally
    //   67	72	165	finally
    //   76	80	165	finally
    //   90	122	165	finally
    //   122	135	165	finally
    //   140	160	165	finally
  }
  
  public PoolEntryRequest requestPoolEntry(HttpRoute paramHttpRoute, Object paramObject)
  {
    return new ConnPoolByRoute.1(this, new WaitingThreadAborter(), paramHttpRoute, paramObject);
  }
  
  public void setMaxTotalConnections(int paramInt)
  {
    this.poolLock.lock();
    try
    {
      this.maxTotalConnections = paramInt;
      return;
    }
    finally
    {
      this.poolLock.unlock();
    }
  }
  
  public void shutdown()
  {
    this.poolLock.lock();
    Object localObject2;
    try
    {
      boolean bool = this.shutdown;
      if (bool) {
        return;
      }
      this.shutdown = true;
      Iterator localIterator1 = this.leasedConnections.iterator();
      while (localIterator1.hasNext())
      {
        localObject2 = (BasicPoolEntry)localIterator1.next();
        localIterator1.remove();
        closeConnection((BasicPoolEntry)localObject2);
      }
      localIterator2 = this.freeConnections.iterator();
    }
    finally
    {
      this.poolLock.unlock();
    }
    while (localIterator2.hasNext())
    {
      localObject2 = (BasicPoolEntry)localIterator2.next();
      localIterator2.remove();
      if (this.log.isDebugEnabled()) {
        this.log.debug("Closing connection [" + ((BasicPoolEntry)localObject2).getPlannedRoute() + "][" + ((BasicPoolEntry)localObject2).getState() + "]");
      }
      closeConnection((BasicPoolEntry)localObject2);
    }
    Iterator localIterator2 = this.waitingThreads.iterator();
    while (localIterator2.hasNext())
    {
      localObject2 = (WaitingThread)localIterator2.next();
      localIterator2.remove();
      ((WaitingThread)localObject2).wakeup();
    }
    this.routeToPool.clear();
    this.poolLock.unlock();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/cz/msebera/android/httpclient/impl/conn/tsccm/ConnPoolByRoute.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */