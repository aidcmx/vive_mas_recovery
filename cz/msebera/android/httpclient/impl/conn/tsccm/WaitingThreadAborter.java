package cz.msebera.android.httpclient.impl.conn.tsccm;

@Deprecated
public class WaitingThreadAborter
{
  private boolean aborted;
  private WaitingThread waitingThread;
  
  public void abort()
  {
    this.aborted = true;
    if (this.waitingThread != null) {
      this.waitingThread.interrupt();
    }
  }
  
  public void setWaitingThread(WaitingThread paramWaitingThread)
  {
    this.waitingThread = paramWaitingThread;
    if (this.aborted) {
      paramWaitingThread.interrupt();
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/cz/msebera/android/httpclient/impl/conn/tsccm/WaitingThreadAborter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */