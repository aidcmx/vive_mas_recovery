package cz.msebera.android.httpclient.impl.cookie;

import cz.msebera.android.httpclient.annotation.Immutable;

@Deprecated
@Immutable
public class DateParseException
  extends Exception
{
  private static final long serialVersionUID = 4417696455000643370L;
  
  public DateParseException() {}
  
  public DateParseException(String paramString)
  {
    super(paramString);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/cz/msebera/android/httpclient/impl/cookie/DateParseException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */