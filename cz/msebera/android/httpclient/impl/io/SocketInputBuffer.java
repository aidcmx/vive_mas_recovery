package cz.msebera.android.httpclient.impl.io;

import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.io.EofSensor;
import cz.msebera.android.httpclient.params.HttpParams;
import cz.msebera.android.httpclient.util.Args;
import java.io.IOException;
import java.net.Socket;

@Deprecated
@NotThreadSafe
public class SocketInputBuffer
  extends AbstractSessionInputBuffer
  implements EofSensor
{
  private boolean eof;
  private final Socket socket;
  
  public SocketInputBuffer(Socket paramSocket, int paramInt, HttpParams paramHttpParams)
    throws IOException
  {
    Args.notNull(paramSocket, "Socket");
    this.socket = paramSocket;
    this.eof = false;
    int i = paramInt;
    paramInt = i;
    if (i < 0) {
      paramInt = paramSocket.getReceiveBufferSize();
    }
    i = paramInt;
    if (paramInt < 1024) {
      i = 1024;
    }
    init(paramSocket.getInputStream(), i, paramHttpParams);
  }
  
  protected int fillBuffer()
    throws IOException
  {
    int i = super.fillBuffer();
    if (i == -1) {}
    for (boolean bool = true;; bool = false)
    {
      this.eof = bool;
      return i;
    }
  }
  
  public boolean isDataAvailable(int paramInt)
    throws IOException
  {
    boolean bool2 = hasBufferedData();
    boolean bool1 = bool2;
    int i;
    if (!bool2) {
      i = this.socket.getSoTimeout();
    }
    try
    {
      this.socket.setSoTimeout(paramInt);
      fillBuffer();
      bool1 = hasBufferedData();
      return bool1;
    }
    finally
    {
      this.socket.setSoTimeout(i);
    }
  }
  
  public boolean isEof()
  {
    return this.eof;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/cz/msebera/android/httpclient/impl/io/SocketInputBuffer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */