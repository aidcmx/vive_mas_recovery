package cz.msebera.android.httpclient.impl.pool;

import cz.msebera.android.httpclient.HttpClientConnection;
import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.annotation.ThreadSafe;
import cz.msebera.android.httpclient.config.ConnectionConfig;
import cz.msebera.android.httpclient.config.SocketConfig;
import cz.msebera.android.httpclient.params.HttpParams;
import cz.msebera.android.httpclient.pool.AbstractConnPool;
import cz.msebera.android.httpclient.pool.ConnFactory;
import java.util.concurrent.atomic.AtomicLong;

@ThreadSafe
public class BasicConnPool
  extends AbstractConnPool<HttpHost, HttpClientConnection, BasicPoolEntry>
{
  private static final AtomicLong COUNTER = new AtomicLong();
  
  public BasicConnPool()
  {
    super(new BasicConnFactory(SocketConfig.DEFAULT, ConnectionConfig.DEFAULT), 2, 20);
  }
  
  public BasicConnPool(SocketConfig paramSocketConfig, ConnectionConfig paramConnectionConfig)
  {
    super(new BasicConnFactory(paramSocketConfig, paramConnectionConfig), 2, 20);
  }
  
  @Deprecated
  public BasicConnPool(HttpParams paramHttpParams)
  {
    super(new BasicConnFactory(paramHttpParams), 2, 20);
  }
  
  public BasicConnPool(ConnFactory<HttpHost, HttpClientConnection> paramConnFactory)
  {
    super(paramConnFactory, 2, 20);
  }
  
  protected BasicPoolEntry createEntry(HttpHost paramHttpHost, HttpClientConnection paramHttpClientConnection)
  {
    return new BasicPoolEntry(Long.toString(COUNTER.getAndIncrement()), paramHttpHost, paramHttpClientConnection);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/cz/msebera/android/httpclient/impl/pool/BasicConnPool.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */