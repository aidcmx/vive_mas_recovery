package cz.msebera.android.httpclient.io;

@Deprecated
public abstract interface EofSensor
{
  public abstract boolean isEof();
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/cz/msebera/android/httpclient/io/EofSensor.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */