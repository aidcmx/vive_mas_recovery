package cz.msebera.android.httpclient.params;

import cz.msebera.android.httpclient.util.Args;

@Deprecated
public abstract class HttpAbstractParamBean
{
  protected final HttpParams params;
  
  public HttpAbstractParamBean(HttpParams paramHttpParams)
  {
    this.params = ((HttpParams)Args.notNull(paramHttpParams, "HTTP parameters"));
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/cz/msebera/android/httpclient/params/HttpAbstractParamBean.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */