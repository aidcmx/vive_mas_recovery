package cz.msebera.android.httpclient.params;

import cz.msebera.android.httpclient.config.ConnectionConfig;
import cz.msebera.android.httpclient.config.ConnectionConfig.Builder;
import cz.msebera.android.httpclient.config.MessageConstraints;
import cz.msebera.android.httpclient.config.MessageConstraints.Builder;
import cz.msebera.android.httpclient.config.SocketConfig;
import cz.msebera.android.httpclient.config.SocketConfig.Builder;
import java.nio.charset.Charset;
import java.nio.charset.CodingErrorAction;

@Deprecated
public final class HttpParamConfig
{
  public static ConnectionConfig getConnectionConfig(HttpParams paramHttpParams)
  {
    MessageConstraints localMessageConstraints = getMessageConstraints(paramHttpParams);
    Object localObject = (String)paramHttpParams.getParameter("http.protocol.element-charset");
    ConnectionConfig.Builder localBuilder = ConnectionConfig.custom();
    if (localObject != null) {}
    for (localObject = Charset.forName((String)localObject);; localObject = null) {
      return localBuilder.setCharset((Charset)localObject).setMalformedInputAction((CodingErrorAction)paramHttpParams.getParameter("http.malformed.input.action")).setMalformedInputAction((CodingErrorAction)paramHttpParams.getParameter("http.unmappable.input.action")).setMessageConstraints(localMessageConstraints).build();
    }
  }
  
  public static MessageConstraints getMessageConstraints(HttpParams paramHttpParams)
  {
    return MessageConstraints.custom().setMaxHeaderCount(paramHttpParams.getIntParameter("http.connection.max-header-count", -1)).setMaxLineLength(paramHttpParams.getIntParameter("http.connection.max-line-length", -1)).build();
  }
  
  public static SocketConfig getSocketConfig(HttpParams paramHttpParams)
  {
    return SocketConfig.custom().setSoTimeout(paramHttpParams.getIntParameter("http.socket.timeout", 0)).setSoReuseAddress(paramHttpParams.getBooleanParameter("http.socket.reuseaddr", false)).setSoKeepAlive(paramHttpParams.getBooleanParameter("http.socket.keepalive", false)).setSoLinger(paramHttpParams.getIntParameter("http.socket.linger", -1)).setTcpNoDelay(paramHttpParams.getBooleanParameter("http.tcp.nodelay", true)).build();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/cz/msebera/android/httpclient/params/HttpParamConfig.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */