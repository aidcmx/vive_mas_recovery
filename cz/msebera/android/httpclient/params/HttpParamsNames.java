package cz.msebera.android.httpclient.params;

import java.util.Set;

@Deprecated
public abstract interface HttpParamsNames
{
  public abstract Set<String> getNames();
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/cz/msebera/android/httpclient/params/HttpParamsNames.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */