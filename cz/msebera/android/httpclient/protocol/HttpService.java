package cz.msebera.android.httpclient.protocol;

import cz.msebera.android.httpclient.ConnectionReuseStrategy;
import cz.msebera.android.httpclient.HttpEntityEnclosingRequest;
import cz.msebera.android.httpclient.HttpException;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.HttpResponseFactory;
import cz.msebera.android.httpclient.HttpServerConnection;
import cz.msebera.android.httpclient.HttpVersion;
import cz.msebera.android.httpclient.MethodNotSupportedException;
import cz.msebera.android.httpclient.ProtocolException;
import cz.msebera.android.httpclient.RequestLine;
import cz.msebera.android.httpclient.StatusLine;
import cz.msebera.android.httpclient.UnsupportedHttpVersionException;
import cz.msebera.android.httpclient.annotation.Immutable;
import cz.msebera.android.httpclient.entity.ByteArrayEntity;
import cz.msebera.android.httpclient.impl.DefaultConnectionReuseStrategy;
import cz.msebera.android.httpclient.impl.DefaultHttpResponseFactory;
import cz.msebera.android.httpclient.params.HttpParams;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.EncodingUtils;
import cz.msebera.android.httpclient.util.EntityUtils;
import java.io.IOException;

@Immutable
public class HttpService
{
  private volatile ConnectionReuseStrategy connStrategy = null;
  private volatile HttpExpectationVerifier expectationVerifier = null;
  private volatile HttpRequestHandlerMapper handlerMapper = null;
  private volatile HttpParams params = null;
  private volatile HttpProcessor processor = null;
  private volatile HttpResponseFactory responseFactory = null;
  
  @Deprecated
  public HttpService(HttpProcessor paramHttpProcessor, ConnectionReuseStrategy paramConnectionReuseStrategy, HttpResponseFactory paramHttpResponseFactory)
  {
    setHttpProcessor(paramHttpProcessor);
    setConnReuseStrategy(paramConnectionReuseStrategy);
    setResponseFactory(paramHttpResponseFactory);
  }
  
  public HttpService(HttpProcessor paramHttpProcessor, ConnectionReuseStrategy paramConnectionReuseStrategy, HttpResponseFactory paramHttpResponseFactory, HttpRequestHandlerMapper paramHttpRequestHandlerMapper)
  {
    this(paramHttpProcessor, paramConnectionReuseStrategy, paramHttpResponseFactory, paramHttpRequestHandlerMapper, null);
  }
  
  public HttpService(HttpProcessor paramHttpProcessor, ConnectionReuseStrategy paramConnectionReuseStrategy, HttpResponseFactory paramHttpResponseFactory, HttpRequestHandlerMapper paramHttpRequestHandlerMapper, HttpExpectationVerifier paramHttpExpectationVerifier)
  {
    this.processor = ((HttpProcessor)Args.notNull(paramHttpProcessor, "HTTP processor"));
    if (paramConnectionReuseStrategy != null)
    {
      this.connStrategy = paramConnectionReuseStrategy;
      if (paramHttpResponseFactory == null) {
        break label85;
      }
    }
    for (;;)
    {
      this.responseFactory = paramHttpResponseFactory;
      this.handlerMapper = paramHttpRequestHandlerMapper;
      this.expectationVerifier = paramHttpExpectationVerifier;
      return;
      paramConnectionReuseStrategy = DefaultConnectionReuseStrategy.INSTANCE;
      break;
      label85:
      paramHttpResponseFactory = DefaultHttpResponseFactory.INSTANCE;
    }
  }
  
  @Deprecated
  public HttpService(HttpProcessor paramHttpProcessor, ConnectionReuseStrategy paramConnectionReuseStrategy, HttpResponseFactory paramHttpResponseFactory, HttpRequestHandlerResolver paramHttpRequestHandlerResolver, HttpParams paramHttpParams)
  {
    this(paramHttpProcessor, paramConnectionReuseStrategy, paramHttpResponseFactory, new HttpRequestHandlerResolverAdapter(paramHttpRequestHandlerResolver), null);
  }
  
  @Deprecated
  public HttpService(HttpProcessor paramHttpProcessor, ConnectionReuseStrategy paramConnectionReuseStrategy, HttpResponseFactory paramHttpResponseFactory, HttpRequestHandlerResolver paramHttpRequestHandlerResolver, HttpExpectationVerifier paramHttpExpectationVerifier, HttpParams paramHttpParams)
  {
    this(paramHttpProcessor, paramConnectionReuseStrategy, paramHttpResponseFactory, new HttpRequestHandlerResolverAdapter(paramHttpRequestHandlerResolver), paramHttpExpectationVerifier);
  }
  
  public HttpService(HttpProcessor paramHttpProcessor, HttpRequestHandlerMapper paramHttpRequestHandlerMapper)
  {
    this(paramHttpProcessor, null, null, paramHttpRequestHandlerMapper, null);
  }
  
  protected void doService(HttpRequest paramHttpRequest, HttpResponse paramHttpResponse, HttpContext paramHttpContext)
    throws HttpException, IOException
  {
    HttpRequestHandler localHttpRequestHandler = null;
    if (this.handlerMapper != null) {
      localHttpRequestHandler = this.handlerMapper.lookup(paramHttpRequest);
    }
    if (localHttpRequestHandler != null)
    {
      localHttpRequestHandler.handle(paramHttpRequest, paramHttpResponse, paramHttpContext);
      return;
    }
    paramHttpResponse.setStatusCode(501);
  }
  
  @Deprecated
  public HttpParams getParams()
  {
    return this.params;
  }
  
  protected void handleException(HttpException paramHttpException, HttpResponse paramHttpResponse)
  {
    if ((paramHttpException instanceof MethodNotSupportedException)) {
      paramHttpResponse.setStatusCode(501);
    }
    for (;;)
    {
      String str2 = paramHttpException.getMessage();
      String str1 = str2;
      if (str2 == null) {
        str1 = paramHttpException.toString();
      }
      paramHttpException = new ByteArrayEntity(EncodingUtils.getAsciiBytes(str1));
      paramHttpException.setContentType("text/plain; charset=US-ASCII");
      paramHttpResponse.setEntity(paramHttpException);
      return;
      if ((paramHttpException instanceof UnsupportedHttpVersionException)) {
        paramHttpResponse.setStatusCode(505);
      } else if ((paramHttpException instanceof ProtocolException)) {
        paramHttpResponse.setStatusCode(400);
      } else {
        paramHttpResponse.setStatusCode(500);
      }
    }
  }
  
  public void handleRequest(HttpServerConnection paramHttpServerConnection, HttpContext paramHttpContext)
    throws IOException, HttpException
  {
    paramHttpContext.setAttribute("http.connection", paramHttpServerConnection);
    localObject3 = null;
    for (;;)
    {
      try
      {
        localHttpRequest = paramHttpServerConnection.receiveRequestHeader();
        localObject1 = localObject3;
        if ((localHttpRequest instanceof HttpEntityEnclosingRequest))
        {
          if (!((HttpEntityEnclosingRequest)localHttpRequest).expectContinue()) {
            continue;
          }
          localObject1 = this.responseFactory.newHttpResponse(HttpVersion.HTTP_1_1, 100, paramHttpContext);
          HttpExpectationVerifier localHttpExpectationVerifier = this.expectationVerifier;
          localObject3 = localObject1;
          if (localHttpExpectationVerifier == null) {}
        }
      }
      catch (HttpException localHttpException2)
      {
        HttpRequest localHttpRequest;
        Object localObject1;
        Object localObject2 = this.responseFactory.newHttpResponse(HttpVersion.HTTP_1_0, 500, paramHttpContext);
        handleException(localHttpException2, (HttpResponse)localObject2);
        continue;
        paramHttpServerConnection.receiveRequestEntity((HttpEntityEnclosingRequest)localHttpRequest);
        localObject2 = localHttpException2;
        continue;
      }
      try
      {
        this.expectationVerifier.verify(localHttpRequest, (HttpResponse)localObject1, paramHttpContext);
        localObject3 = localObject1;
      }
      catch (HttpException localHttpException1)
      {
        localObject3 = this.responseFactory.newHttpResponse(HttpVersion.HTTP_1_0, 500, paramHttpContext);
        handleException(localHttpException1, (HttpResponse)localObject3);
      }
    }
    localObject1 = localObject3;
    if (((HttpResponse)localObject3).getStatusLine().getStatusCode() < 200)
    {
      paramHttpServerConnection.sendResponseHeader((HttpResponse)localObject3);
      paramHttpServerConnection.flush();
      localObject1 = null;
      paramHttpServerConnection.receiveRequestEntity((HttpEntityEnclosingRequest)localHttpRequest);
    }
    paramHttpContext.setAttribute("http.request", localHttpRequest);
    localObject3 = localObject1;
    if (localObject1 == null)
    {
      localObject3 = this.responseFactory.newHttpResponse(HttpVersion.HTTP_1_1, 200, paramHttpContext);
      this.processor.process(localHttpRequest, paramHttpContext);
      doService(localHttpRequest, (HttpResponse)localObject3, paramHttpContext);
    }
    localObject1 = localObject3;
    if ((localHttpRequest instanceof HttpEntityEnclosingRequest))
    {
      EntityUtils.consume(((HttpEntityEnclosingRequest)localHttpRequest).getEntity());
      localObject1 = localObject3;
    }
    paramHttpContext.setAttribute("http.response", localObject1);
    this.processor.process((HttpResponse)localObject1, paramHttpContext);
    paramHttpServerConnection.sendResponseHeader((HttpResponse)localObject1);
    paramHttpServerConnection.sendResponseEntity((HttpResponse)localObject1);
    paramHttpServerConnection.flush();
    if (!this.connStrategy.keepAlive((HttpResponse)localObject1, paramHttpContext)) {
      paramHttpServerConnection.close();
    }
  }
  
  @Deprecated
  public void setConnReuseStrategy(ConnectionReuseStrategy paramConnectionReuseStrategy)
  {
    Args.notNull(paramConnectionReuseStrategy, "Connection reuse strategy");
    this.connStrategy = paramConnectionReuseStrategy;
  }
  
  @Deprecated
  public void setExpectationVerifier(HttpExpectationVerifier paramHttpExpectationVerifier)
  {
    this.expectationVerifier = paramHttpExpectationVerifier;
  }
  
  @Deprecated
  public void setHandlerResolver(HttpRequestHandlerResolver paramHttpRequestHandlerResolver)
  {
    this.handlerMapper = new HttpRequestHandlerResolverAdapter(paramHttpRequestHandlerResolver);
  }
  
  @Deprecated
  public void setHttpProcessor(HttpProcessor paramHttpProcessor)
  {
    Args.notNull(paramHttpProcessor, "HTTP processor");
    this.processor = paramHttpProcessor;
  }
  
  @Deprecated
  public void setParams(HttpParams paramHttpParams)
  {
    this.params = paramHttpParams;
  }
  
  @Deprecated
  public void setResponseFactory(HttpResponseFactory paramHttpResponseFactory)
  {
    Args.notNull(paramHttpResponseFactory, "Response factory");
    this.responseFactory = paramHttpResponseFactory;
  }
  
  @Deprecated
  private static class HttpRequestHandlerResolverAdapter
    implements HttpRequestHandlerMapper
  {
    private final HttpRequestHandlerResolver resolver;
    
    public HttpRequestHandlerResolverAdapter(HttpRequestHandlerResolver paramHttpRequestHandlerResolver)
    {
      this.resolver = paramHttpRequestHandlerResolver;
    }
    
    public HttpRequestHandler lookup(HttpRequest paramHttpRequest)
    {
      return this.resolver.lookup(paramHttpRequest.getRequestLine().getUri());
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/cz/msebera/android/httpclient/protocol/HttpService.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */