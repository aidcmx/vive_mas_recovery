package javax.annotation.meta;

import java.lang.annotation.Annotation;
import java.lang.annotation.Documented;
import java.lang.annotation.Target;

@Documented
@Target({java.lang.annotation.ElementType.ANNOTATION_TYPE})
public @interface TypeQualifierNickname {}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/javax/annotation/meta/TypeQualifierNickname.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */