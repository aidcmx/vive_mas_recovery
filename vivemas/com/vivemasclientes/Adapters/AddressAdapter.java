package vivemas.com.vivemasclientes.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.mypopsy.maps.StaticMap;
import com.nostra13.universalimageloader.core.ImageLoader;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vivemas.com.vivemasclientes.Helpers.AbstractAdapter;

public class AddressAdapter
  extends AbstractAdapter
{
  public AddressAdapter(Context paramContext, JSONArray paramJSONArray)
  {
    super(paramContext, paramJSONArray);
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    Object localObject = paramView;
    paramView = (View)localObject;
    if (localObject == null) {
      paramView = ((LayoutInflater)this.context.getSystemService("layout_inflater")).inflate(2130968658, paramViewGroup, false);
    }
    try
    {
      paramViewGroup = this.jsonArray.getJSONObject(paramInt);
      ((TextView)paramView.findViewById(2131689852)).setText(paramViewGroup.getJSONObject("location").getJSONObject("address").getString("streetName"));
      double d1 = paramViewGroup.getJSONObject("location").getJSONObject("geo").getJSONArray("coordinates").getDouble(0);
      double d2 = paramViewGroup.getJSONObject("location").getJSONObject("geo").getJSONArray("coordinates").getDouble(1);
      paramViewGroup = (ImageView)paramView.findViewById(2131689853);
      localObject = new StaticMap().center(d1, d2).zoom(16).size(1200, 800);
      ImageLoader.getInstance().displayImage(((StaticMap)localObject).toString(), paramViewGroup, this.options);
      return paramView;
    }
    catch (JSONException paramViewGroup)
    {
      paramViewGroup.printStackTrace();
    }
    return paramView;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/vivemas/com/vivemasclientes/Adapters/AddressAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */