package vivemas.com.vivemasclientes.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vivemas.com.vivemasclientes.Helpers.AbstractAdapter;
import vivemas.com.vivemasclientes.Model.Model;

public class CategoriesAdapter
  extends AbstractAdapter
{
  public CategoriesAdapter(Context paramContext, JSONArray paramJSONArray)
  {
    super(paramContext, paramJSONArray);
  }
  
  public String getCategoryName(String paramString)
    throws JSONException
  {
    String str2 = "";
    JSONArray localJSONArray = new JSONArray(Model.getInstance().getLocalActivities());
    int i = 0;
    for (;;)
    {
      String str1 = str2;
      if (i < localJSONArray.length())
      {
        if (localJSONArray.getJSONObject(i).getString("id").equals(paramString)) {
          str1 = localJSONArray.getJSONObject(i).getString("name");
        }
      }
      else {
        return str1;
      }
      i += 1;
    }
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    Object localObject = paramView;
    paramView = (View)localObject;
    if (localObject == null) {
      paramView = ((LayoutInflater)this.context.getSystemService("layout_inflater")).inflate(2130968602, paramViewGroup, false);
    }
    localObject = (TextView)paramView.findViewById(2131689684);
    try
    {
      paramViewGroup = this.jsonArray.getString(paramInt);
      ((TextView)localObject).setText(getCategoryName(paramViewGroup));
      localObject = (Button)paramView.findViewById(2131689685);
      ((Button)localObject).setTag(paramViewGroup);
      ((Button)localObject).setOnClickListener(new CategoriesAdapter.1(this));
      return paramView;
    }
    catch (JSONException paramViewGroup)
    {
      paramViewGroup.printStackTrace();
    }
    return paramView;
  }
  
  public void reloadListDeleteTag(String paramString)
    throws JSONException
  {
    JSONArray localJSONArray = new JSONArray();
    int i = 0;
    while (i < this.jsonArray.length())
    {
      String str = this.jsonArray.getString(i);
      if (!str.equals(paramString)) {
        localJSONArray.put(str);
      }
      i += 1;
    }
    this.jsonArray = localJSONArray;
    Model.getInstance().serviceCategories = localJSONArray;
    this.count = this.jsonArray.length();
    notifyDataSetChanged();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/vivemas/com/vivemasclientes/Adapters/CategoriesAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */