package vivemas.com.vivemasclientes.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.io.PrintStream;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vivemas.com.vivemasclientes.Helpers.AbstractAdapter;

public class EmergencyAdapter
  extends AbstractAdapter
{
  public EmergencyAdapter(Context paramContext, JSONArray paramJSONArray)
  {
    super(paramContext, paramJSONArray);
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    Object localObject = paramView;
    paramView = (View)localObject;
    if (localObject == null) {
      paramView = ((LayoutInflater)this.context.getSystemService("layout_inflater")).inflate(2130968652, paramViewGroup, false);
    }
    paramViewGroup = (TextView)paramView.findViewById(2131689689);
    localObject = (TextView)paramView.findViewById(2131689844);
    TextView localTextView1 = (TextView)paramView.findViewById(2131689580);
    TextView localTextView2 = (TextView)paramView.findViewById(2131689690);
    TextView localTextView3 = (TextView)paramView.findViewById(2131689845);
    try
    {
      JSONObject localJSONObject = this.jsonArray.getJSONObject(paramInt);
      System.out.println(localJSONObject);
      paramViewGroup.setText(localJSONObject.getString("name"));
      ((TextView)localObject).setText("Parentesco: " + localJSONObject.getString("kinship"));
      localTextView1.setText("Dirección: " + localJSONObject.getString("address"));
      localTextView2.setText("Teléfono: " + localJSONObject.getString("tel"));
      localTextView3.setText("Celular: " + localJSONObject.getString("mobile"));
      return paramView;
    }
    catch (JSONException paramViewGroup)
    {
      paramViewGroup.printStackTrace();
    }
    return paramView;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/vivemas/com/vivemasclientes/Adapters/EmergencyAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */