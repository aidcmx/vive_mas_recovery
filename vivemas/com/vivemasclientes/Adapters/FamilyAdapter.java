package vivemas.com.vivemasclientes.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.nostra13.universalimageloader.core.ImageLoader;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vivemas.com.vivemasclientes.Helpers.AbstractAdapter;

public class FamilyAdapter
  extends AbstractAdapter
{
  public FamilyAdapter(Context paramContext, JSONArray paramJSONArray)
  {
    super(paramContext, paramJSONArray);
  }
  
  protected Bitmap getCircleBitmap(Bitmap paramBitmap)
  {
    Bitmap localBitmap = Bitmap.createBitmap(paramBitmap.getWidth(), paramBitmap.getHeight(), Bitmap.Config.ARGB_8888);
    Canvas localCanvas = new Canvas(localBitmap);
    Paint localPaint = new Paint();
    Rect localRect = new Rect(0, 0, paramBitmap.getWidth(), paramBitmap.getHeight());
    RectF localRectF = new RectF(localRect);
    localPaint.setAntiAlias(true);
    localCanvas.drawARGB(0, 0, 0, 0);
    localPaint.setColor(-65536);
    localCanvas.drawOval(localRectF, localPaint);
    localPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
    localCanvas.drawBitmap(paramBitmap, localRect, localRect, localPaint);
    paramBitmap.recycle();
    return localBitmap;
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    Object localObject1 = paramView;
    paramView = (View)localObject1;
    if (localObject1 == null) {
      paramView = ((LayoutInflater)this.context.getSystemService("layout_inflater")).inflate(2130968657, paramViewGroup, false);
    }
    try
    {
      paramViewGroup = this.jsonArray.getJSONObject(paramInt);
      localObject1 = (TextView)paramView.findViewById(2131689855);
      Object localObject2 = (TextView)paramView.findViewById(2131689856);
      ((TextView)localObject1).setText(paramViewGroup.getJSONObject("name").getString("first") + " " + paramViewGroup.getJSONObject("name").getString("last"));
      if (!paramViewGroup.getString("kinship").contains("self")) {
        ((TextView)localObject2).setText(paramViewGroup.getString("kinship"));
      }
      while (paramViewGroup.has("pic"))
      {
        paramViewGroup = paramViewGroup.getString("pic");
        localObject1 = ImageLoader.getInstance();
        localObject2 = "?" + UUID.randomUUID().toString();
        ((ImageLoader)localObject1).loadImage(paramViewGroup + (String)localObject2, this.options, new FamilyAdapter.1(this, paramView));
        return paramView;
        ((TextView)localObject2).setText("Yo");
      }
      return paramView;
    }
    catch (JSONException paramViewGroup)
    {
      paramViewGroup.printStackTrace();
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/vivemas/com/vivemasclientes/Adapters/FamilyAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */