package vivemas.com.vivemasclientes.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import org.json.JSONArray;
import vivemas.com.vivemasclientes.Helpers.AbstractAdapter;

public class NurseKindAdapter
  extends AbstractAdapter
{
  public NurseKindAdapter(Context paramContext, JSONArray paramJSONArray)
  {
    super(paramContext, paramJSONArray);
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    Object localObject = paramView;
    paramView = (View)localObject;
    if (localObject == null) {
      paramView = ((LayoutInflater)this.context.getSystemService("layout_inflater")).inflate(2130968686, paramViewGroup, false);
    }
    paramViewGroup = (TextView)paramView.findViewById(2131689644);
    localObject = (TextView)paramView.findViewById(2131689901);
    ImageView localImageView = (ImageView)paramView.findViewById(2131689640);
    if (paramInt == 0)
    {
      localImageView.setImageResource(2130837935);
      ((TextView)localObject).setText("Las Puericuristas cuentan con una carrera técnica en enfermería que les permite estar capacitados de a cuerdo al cuidado de cada paciente.");
      paramViewGroup.setText("Puericurista");
    }
    if (paramInt == 1)
    {
      localImageView.setImageResource(2130837699);
      ((TextView)localObject).setText("Las cuidadoras cuentan con una carrera técnica en enfermería que les permite estar capacitados de a cuerdo al cuidado de cada paciente.");
      paramViewGroup.setText("Cuidadora");
    }
    if (paramInt == 2)
    {
      localImageView.setImageResource(2130837591);
      ((TextView)localObject).setText("Los auxiliares de enfermería cuentan con una carrera técnica en enfermería que les permite estar capacitados de a cuerdo al cuidado de cada paciente.");
      paramViewGroup.setText("Auxiliar");
    }
    if (paramInt == 3)
    {
      localImageView.setImageResource(2130837704);
      ((TextView)localObject).setText("Las enfermeras cuentan con una carrera técnica en enfermería que les permite estar capacitados de a cuerdo al cuidado de cada paciente.");
      paramViewGroup.setText("Enfermera");
    }
    return paramView;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/vivemas/com/vivemasclientes/Adapters/NurseKindAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */