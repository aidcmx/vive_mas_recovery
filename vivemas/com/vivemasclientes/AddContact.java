package vivemas.com.vivemasclientes;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vivemas.com.vivemasclientes.Model.Customer;
import vivemas.com.vivemasclientes.Model.Model;

public class AddContact
  extends AiDC_Activity
{
  private ArrayAdapter<CharSequence> adapterKinship;
  JSONObject contactData;
  int index = -1;
  Boolean isEditing = Boolean.valueOf(false);
  Spinner spinnerKinship;
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130968604);
    setTitle("AGREGAR CONTACTO");
    setupSpinners();
    paramBundle = getIntent();
    this.isEditing = Boolean.valueOf(paramBundle.getBooleanExtra("isEditing", false));
    Button localButton = (Button)findViewById(2131689693);
    if (this.isEditing.booleanValue())
    {
      this.index = paramBundle.getIntExtra("row", 0);
      localButton.setVisibility(0);
    }
    try
    {
      this.contactData = Model.getInstance().emergencyContacts.getJSONObject(this.index);
      setupFromJSON();
      return;
    }
    catch (JSONException paramBundle) {}
  }
  
  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    getMenuInflater().inflate(2131755011, paramMenu);
    return true;
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    }
    for (;;)
    {
      return true;
      try
      {
        putContact();
      }
      catch (JSONException paramMenuItem) {}
    }
  }
  
  public void putContact()
    throws JSONException
  {
    JSONObject localJSONObject = new JSONObject();
    Object localObject = (EditText)findViewById(2131689689);
    EditText localEditText1 = (EditText)findViewById(2131689690);
    EditText localEditText2 = (EditText)findViewById(2131689691);
    EditText localEditText3 = (EditText)findViewById(2131689580);
    Spinner localSpinner = (Spinner)findViewById(2131689692);
    if ((localEditText1.getText().toString().length() < 10) && (localEditText1.getText().toString().length() != 0))
    {
      doAlert("Error", "El teléfono debe ser de por lo menos 10 dígitos");
      return;
    }
    if ((localEditText2.getText().toString().length() < 10) && (localEditText2.getText().toString().length() != 0))
    {
      doAlert("Error", "El celular debe ser de por lo menos 10 dígitos");
      return;
    }
    if ((localEditText2.getText().toString().length() == 0) && (localEditText1.getText().toString().length() == 0))
    {
      doAlert("Error", "Debes ingresar por lo menos un número de teléfono.");
      return;
    }
    localJSONObject.put("name", ((EditText)localObject).getText().toString());
    localJSONObject.put("kinship", localSpinner.getSelectedItem().toString());
    localJSONObject.put("address", localEditText3.getText().toString());
    localJSONObject.put("tel", localEditText1.getText().toString());
    localJSONObject.put("mobile", localEditText2.getText().toString());
    localObject = Customer.loadLocal();
    if (!this.isEditing.booleanValue()) {
      Model.getInstance().emergencyContacts.put(localJSONObject);
    }
    for (;;)
    {
      ((Customer)localObject).saveLocal();
      finish();
      return;
      Model.getInstance().emergencyContacts.put(this.index, localJSONObject);
    }
  }
  
  public void remove(View paramView)
  {
    if (this.isEditing.booleanValue())
    {
      Model.getInstance().emergencyContacts = remove(this.index, Model.getInstance().emergencyContacts);
      paramView = null;
    }
    try
    {
      Customer localCustomer = Customer.loadLocal();
      paramView = localCustomer;
    }
    catch (JSONException localJSONException)
    {
      for (;;) {}
    }
    paramView.saveLocal();
    finish();
  }
  
  public void setupFromJSON()
    throws JSONException
  {
    Object localObject = (EditText)findViewById(2131689689);
    EditText localEditText1 = (EditText)findViewById(2131689690);
    EditText localEditText2 = (EditText)findViewById(2131689691);
    EditText localEditText3 = (EditText)findViewById(2131689580);
    Spinner localSpinner = (Spinner)findViewById(2131689692);
    if (this.contactData.has("name")) {
      ((EditText)localObject).setText(this.contactData.getString("name"));
    }
    if (this.contactData.has("tel")) {
      localEditText1.setText(this.contactData.getString("tel"));
    }
    if (this.contactData.has("mobile")) {
      localEditText2.setText(this.contactData.getString("mobile"));
    }
    if (this.contactData.has("address")) {
      localEditText3.setText(this.contactData.getString("address"));
    }
    localObject = getResources().getStringArray(2131623949);
    int i = 0;
    while (i < localObject.length)
    {
      if (localObject[i].equals(this.contactData.getString("kinship"))) {
        localSpinner.setSelection(i);
      }
      i += 1;
    }
  }
  
  public void setupSpinners()
  {
    this.spinnerKinship = ((Spinner)findViewById(2131689692));
    this.adapterKinship = ArrayAdapter.createFromResource(this, 2131623949, 17367048);
    this.adapterKinship.setDropDownViewResource(17367049);
    this.spinnerKinship.setAdapter(this.adapterKinship);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/vivemas/com/vivemasclientes/AddContact.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */