package vivemas.com.vivemasclientes;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class AiDC_Activity
  extends AppCompatActivity
{
  protected static final int REQUEST_CAPTURE_FROM_CAMERA = 1;
  protected static final int REQUEST_PICK_FROM_FILE = 2;
  private static final String TAG = "VIVEMAS";
  protected long SIZE_LIMIT = 200L;
  ProgressDialog dialog;
  protected File file;
  protected ActionBar mActionBar;
  protected DrawerLayout mDrawerLayout;
  ListView mDrawerList;
  RelativeLayout mDrawerPane;
  protected ActionBarDrawerToggle mDrawerToggle;
  ArrayList<NavItem> mNavItems = new ArrayList();
  
  public static List<JSONObject> asList(JSONArray paramJSONArray)
  {
    int j = paramJSONArray.length();
    ArrayList localArrayList = new ArrayList(j);
    int i = 0;
    while (i < j)
    {
      JSONObject localJSONObject = paramJSONArray.optJSONObject(i);
      if (localJSONObject != null) {
        localArrayList.add(localJSONObject);
      }
      i += 1;
    }
    return localArrayList;
  }
  
  public static Bitmap cropToSquare(Bitmap paramBitmap)
  {
    int n = paramBitmap.getWidth();
    int j = paramBitmap.getHeight();
    int i;
    if (j > n)
    {
      i = n;
      if (j <= n) {
        break label86;
      }
    }
    label86:
    for (int k = j - (j - n);; k = j)
    {
      int i1 = (n - j) / 2;
      int m = i1;
      if (i1 < 0) {
        m = 0;
      }
      n = (j - n) / 2;
      j = n;
      if (n < 0) {
        j = 0;
      }
      return Bitmap.createBitmap(paramBitmap, m, j, i, k);
      i = j;
      break;
    }
  }
  
  public static JSONArray remove(int paramInt, JSONArray paramJSONArray)
  {
    Object localObject = asList(paramJSONArray);
    ((List)localObject).remove(paramInt);
    paramJSONArray = new JSONArray();
    localObject = ((List)localObject).iterator();
    while (((Iterator)localObject).hasNext()) {
      paramJSONArray.put((JSONObject)((Iterator)localObject).next());
    }
    return paramJSONArray;
  }
  
  public boolean checkEnabledInternet()
  {
    NetworkInfo localNetworkInfo = ((ConnectivityManager)getSystemService("connectivity")).getActiveNetworkInfo();
    return (localNetworkInfo != null) && (localNetworkInfo.isConnected());
  }
  
  public void compressBitmap(int paramInt1, int paramInt2)
  {
    try
    {
      Object localObject2 = new BitmapFactory.Options();
      ((BitmapFactory.Options)localObject2).inSampleSize = paramInt1;
      Object localObject1 = new FileInputStream(this.file);
      localObject2 = cropToSquare(BitmapFactory.decodeStream((InputStream)localObject1, null, (BitmapFactory.Options)localObject2));
      ((FileInputStream)localObject1).close();
      localObject1 = new FileOutputStream(this.file);
      ((Bitmap)localObject2).compress(Bitmap.CompressFormat.JPEG, paramInt2, (OutputStream)localObject1);
      ((FileOutputStream)localObject1).close();
      if (this.file.length() / 1024L > this.SIZE_LIMIT) {
        compressBitmap(paramInt1, paramInt2);
      }
      ((Bitmap)localObject2).recycle();
      return;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
  }
  
  public void doAlert(String paramString1, String paramString2)
  {
    new AlertDialog.Builder(this).setTitle(paramString1).setMessage(paramString2).setPositiveButton(17039370, new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt) {}
    }).setIcon(17301543).show();
  }
  
  protected Bitmap getBitmapFromUri(Uri paramUri)
    throws IOException
  {
    paramUri = getContentResolver().openFileDescriptor(paramUri, "r");
    Bitmap localBitmap = BitmapFactory.decodeFileDescriptor(paramUri.getFileDescriptor());
    paramUri.close();
    return localBitmap;
  }
  
  protected Bitmap getCircleBitmap(Bitmap paramBitmap)
  {
    Bitmap localBitmap = Bitmap.createBitmap(paramBitmap.getWidth(), paramBitmap.getHeight(), Bitmap.Config.ARGB_8888);
    Canvas localCanvas = new Canvas(localBitmap);
    Paint localPaint = new Paint();
    Rect localRect = new Rect(0, 0, paramBitmap.getWidth(), paramBitmap.getHeight());
    RectF localRectF = new RectF(localRect);
    localPaint.setAntiAlias(true);
    localCanvas.drawARGB(0, 0, 0, 0);
    localPaint.setColor(-65536);
    localCanvas.drawOval(localRectF, localPaint);
    localPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
    localCanvas.drawBitmap(paramBitmap, localRect, localRect, localPaint);
    return localBitmap;
  }
  
  public void getFromCamera()
  {
    if (ActivityCompat.checkSelfPermission(this, "android.permission.CAMERA") != 0)
    {
      toast("No has dado permisos para usar tu cámara. Necesitas este permiso para seguir.");
      return;
    }
    Object localObject = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "ViveMas");
    if ((!((File)localObject).exists()) && (!((File)localObject).mkdirs()))
    {
      Log.d("ViveMas", "failed to create directory");
      return;
    }
    String str = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
    this.file = new File(((File)localObject).getPath() + File.separator + "IMG_" + str + ".png");
    localObject = new Intent("android.media.action.IMAGE_CAPTURE");
    ((Intent)localObject).putExtra("output", Uri.fromFile(this.file));
    startActivityForResult((Intent)localObject, 1);
  }
  
  public void getFromGallery()
  {
    Intent localIntent = new Intent();
    localIntent.setType("image/*");
    localIntent.setAction("android.intent.action.GET_CONTENT");
    startActivityForResult(Intent.createChooser(localIntent, "Complete action using"), 2);
  }
  
  public void hideKeyboard()
  {
    View localView = getCurrentFocus();
    if (localView != null) {
      ((InputMethodManager)getSystemService("input_method")).hideSoftInputFromWindow(localView.getWindowToken(), 0);
    }
  }
  
  public void loading(Activity paramActivity)
  {
    this.dialog = ProgressDialog.show(paramActivity, "", "Loading. Please wait...", true);
  }
  
  public void loading(Activity paramActivity, String paramString1, String paramString2)
  {
    this.dialog = ProgressDialog.show(paramActivity, paramString1, paramString2, true);
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    this.mActionBar = getSupportActionBar();
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    if (this.mDrawerToggle.onOptionsItemSelected(paramMenuItem)) {
      return true;
    }
    return super.onOptionsItemSelected(paramMenuItem);
  }
  
  protected boolean saveBitmapToStorage(Bitmap paramBitmap)
  {
    this.file = new File(Environment.getExternalStorageDirectory() + File.separator + "img_cropped.jpg");
    Log.d("VIVEMAS", "file path = " + this.file.getAbsolutePath());
    try
    {
      FileOutputStream localFileOutputStream = new FileOutputStream(this.file);
      paramBitmap.compress(Bitmap.CompressFormat.JPEG, 100, localFileOutputStream);
      localFileOutputStream.flush();
      localFileOutputStream.close();
      paramBitmap = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
      paramBitmap.setData(Uri.fromFile(this.file));
      sendBroadcast(paramBitmap);
      return true;
    }
    catch (Exception paramBitmap)
    {
      paramBitmap.printStackTrace();
    }
    return false;
  }
  
  protected void selectItemFromDrawer(int paramInt) {}
  
  protected void setMenu()
  {
    this.mNavItems.add(new NavItem("Mis Favoritos", 2130837883));
    this.mNavItems.add(new NavItem("Métodos de pago", 2130837879));
    this.mNavItems.add(new NavItem("Promociones", 2130837882));
    this.mNavItems.add(new NavItem("Contacto", 2130837880));
    this.mNavItems.add(new NavItem("Acerca de", 2130837881));
    this.mDrawerLayout = ((DrawerLayout)findViewById(2131689718));
    this.mDrawerPane = ((RelativeLayout)findViewById(2131689722));
    this.mDrawerList = ((ListView)findViewById(2131689726));
    DrawerListAdapter localDrawerListAdapter = new DrawerListAdapter(this, this.mNavItems);
    this.mDrawerList.setAdapter(localDrawerListAdapter);
    this.mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener()
    {
      public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
      {
        AiDC_Activity.this.selectItemFromDrawer(paramAnonymousInt);
      }
    });
    this.mDrawerToggle = new ActionBarDrawerToggle(this, this.mDrawerLayout, 2131230871, 2131230870)
    {
      public void onDrawerClosed(View paramAnonymousView)
      {
        super.onDrawerClosed(paramAnonymousView);
        AiDC_Activity.this.invalidateOptionsMenu();
      }
      
      public void onDrawerOpened(View paramAnonymousView)
      {
        super.onDrawerOpened(paramAnonymousView);
        AiDC_Activity.this.invalidateOptionsMenu();
      }
    };
    this.mDrawerLayout.setDrawerListener(this.mDrawerToggle);
  }
  
  public void stoppedLoading()
  {
    this.dialog.dismiss();
  }
  
  public void toast(String paramString)
  {
    Toast.makeText(getApplicationContext(), paramString, 0).show();
  }
  
  class DrawerListAdapter
    extends BaseAdapter
  {
    Context mContext;
    ArrayList<AiDC_Activity.NavItem> mNavItems;
    
    public DrawerListAdapter(ArrayList<AiDC_Activity.NavItem> paramArrayList)
    {
      this.mContext = paramArrayList;
      ArrayList localArrayList;
      this.mNavItems = localArrayList;
    }
    
    public int getCount()
    {
      return this.mNavItems.size();
    }
    
    public Object getItem(int paramInt)
    {
      return this.mNavItems.get(paramInt);
    }
    
    public long getItemId(int paramInt)
    {
      return 0L;
    }
    
    public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      if (paramView == null) {
        paramView = ((LayoutInflater)this.mContext.getSystemService("layout_inflater")).inflate(2130968651, null);
      }
      for (;;)
      {
        paramViewGroup = (TextView)paramView.findViewById(2131689644);
        ImageView localImageView = (ImageView)paramView.findViewById(2131689643);
        paramViewGroup.setText(((AiDC_Activity.NavItem)this.mNavItems.get(paramInt)).mTitle);
        localImageView.setImageResource(((AiDC_Activity.NavItem)this.mNavItems.get(paramInt)).mIcon);
        return paramView;
      }
    }
  }
  
  class NavItem
  {
    int mIcon;
    String mTitle;
    
    public NavItem(String paramString, int paramInt)
    {
      this.mTitle = paramString;
      this.mIcon = paramInt;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/vivemas/com/vivemasclientes/AiDC_Activity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */