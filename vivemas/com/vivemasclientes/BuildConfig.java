package vivemas.com.vivemasclientes;

public final class BuildConfig
{
  public static final String APPLICATION_ID = "vivemas.com.vivemasclientes";
  public static final String BUILD_TYPE = "release";
  public static final boolean DEBUG = false;
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 24;
  public static final String VERSION_NAME = "1.0.24";
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/vivemas/com/vivemasclientes/BuildConfig.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */