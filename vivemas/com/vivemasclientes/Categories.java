package vivemas.com.vivemasclientes;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;
import org.json.JSONArray;
import vivemas.com.vivemasclientes.Adapters.CategoriesAdapter;
import vivemas.com.vivemasclientes.Model.Model;

public class Categories
  extends AiDC_Activity
{
  CategoriesAdapter adapter;
  JSONArray categories;
  ListView emergencyList;
  Boolean firstLoaded = Boolean.valueOf(true);
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130968605);
    setTitle("ACTIVIDADES");
  }
  
  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    getMenuInflater().inflate(2131755008, paramMenu);
    return true;
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    startActivity(new Intent(this, SelectCategory.class));
    return true;
  }
  
  public void onResume()
  {
    super.onResume();
    this.categories = Model.getInstance().serviceCategories;
    if (this.firstLoaded.booleanValue())
    {
      this.firstLoaded = Boolean.valueOf(false);
      if (this.categories.length() == 0) {
        startActivity(new Intent(this, SelectCategory.class));
      }
    }
    this.adapter = new CategoriesAdapter(this, this.categories);
    this.emergencyList = ((ListView)findViewById(2131689694));
    this.emergencyList.setAdapter(this.adapter);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/vivemas/com/vivemasclientes/Categories.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */