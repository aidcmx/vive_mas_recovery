package vivemas.com.vivemasclientes;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import cz.msebera.android.httpclient.Header;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vivemas.com.vivemasclientes.Model.Beneficiary;
import vivemas.com.vivemasclientes.Model.Customer;
import vivemas.com.vivemasclientes.Model.Model;

public class CreateFamily
  extends AiDC_Activity
{
  private static final int ASK_MULTIPLE_PERMISSION_REQUEST_CODE = 0;
  private static final int REQUEST_CAPTURE_FROM_CAMERA = 1;
  private static final int REQUEST_PICK_FROM_FILE = 2;
  private ArrayAdapter<CharSequence> adapterBlood;
  private ArrayAdapter<CharSequence> adapterKinship;
  private Beneficiary beneficiaryInstance;
  DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener()
  {
    public void onDateSet(DatePicker paramAnonymousDatePicker, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3)
    {
      CreateFamily.this.myCalendar.set(1, paramAnonymousInt1);
      CreateFamily.this.myCalendar.set(2, paramAnonymousInt2);
      CreateFamily.this.myCalendar.set(5, paramAnonymousInt3);
      CreateFamily.this.updateLabel();
    }
  };
  private EditText datePicker;
  private Button deleteButton;
  private JSONObject familyData;
  private boolean isWoman = true;
  private DatePickerDialog mDialog;
  Calendar myCalendar = Calendar.getInstance();
  private boolean photoSelected = false;
  AsyncHttpResponseHandler responseAdd = new AsyncHttpResponseHandler()
  {
    public void onFailure(int paramAnonymousInt, Header[] paramAnonymousArrayOfHeader, byte[] paramAnonymousArrayOfByte, Throwable paramAnonymousThrowable)
    {
      try
      {
        paramAnonymousArrayOfHeader = new String(paramAnonymousArrayOfByte, "UTF-8");
        CreateFamily.this.doAlert("error: ", "error " + paramAnonymousInt + " : " + paramAnonymousArrayOfHeader);
        return;
      }
      catch (UnsupportedEncodingException paramAnonymousArrayOfHeader) {}
    }
    
    public void onFinish()
    {
      super.onFinish();
      if (!CreateFamily.this.photoSelected) {
        CreateFamily.this.stoppedLoading();
      }
    }
    
    public void onStart()
    {
      super.onStart();
      CreateFamily.this.loading(CreateFamily.this);
    }
    
    public void onSuccess(int paramAnonymousInt, Header[] paramAnonymousArrayOfHeader, byte[] paramAnonymousArrayOfByte)
    {
      try
      {
        paramAnonymousArrayOfHeader = Customer.initWithJSONObject(new JSONObject(new String(paramAnonymousArrayOfByte, "UTF-8")));
        paramAnonymousArrayOfHeader.saveLocal();
        CreateFamily.access$102(CreateFamily.this, CreateFamily.this.getBeneficiaryInstance(paramAnonymousArrayOfHeader));
        if (CreateFamily.this.photoSelected)
        {
          if (CreateFamily.this.file.length() / 1024L > CreateFamily.this.SIZE_LIMIT) {
            CreateFamily.this.compressBitmap(2, 75);
          }
          CreateFamily.this.file.length();
          CreateFamily.this.beneficiaryInstance.uploadPicture(CreateFamily.this, CreateFamily.this.responsePicture, CreateFamily.this.file);
          return;
        }
        CreateFamily.this.finish();
        return;
      }
      catch (UnsupportedEncodingException paramAnonymousArrayOfHeader) {}catch (JSONException paramAnonymousArrayOfHeader) {}
    }
  };
  AsyncHttpResponseHandler responsePicture = new AsyncHttpResponseHandler()
  {
    public void onFailure(int paramAnonymousInt, Header[] paramAnonymousArrayOfHeader, byte[] paramAnonymousArrayOfByte, Throwable paramAnonymousThrowable)
    {
      try
      {
        paramAnonymousArrayOfHeader = new String(paramAnonymousArrayOfByte, "UTF-8");
        CreateFamily.this.doAlert("error: ", "error " + paramAnonymousInt + " : " + paramAnonymousArrayOfHeader);
        return;
      }
      catch (UnsupportedEncodingException paramAnonymousArrayOfHeader) {}
    }
    
    public void onFinish()
    {
      super.onFinish();
      CreateFamily.this.stoppedLoading();
    }
    
    public void onSuccess(int paramAnonymousInt, Header[] paramAnonymousArrayOfHeader, byte[] paramAnonymousArrayOfByte)
    {
      try
      {
        Customer.initWithJSONObject(new JSONObject(new String(paramAnonymousArrayOfByte, "UTF-8"))).saveLocal();
        CreateFamily.this.finish();
        return;
      }
      catch (JSONException paramAnonymousArrayOfHeader) {}catch (UnsupportedEncodingException paramAnonymousArrayOfHeader) {}
    }
  };
  private String savedDate;
  private Spinner spinnerBlood;
  private Spinner spinnerKinship;
  
  private void updateLabel()
  {
    SimpleDateFormat localSimpleDateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
    this.datePicker.setText(localSimpleDateFormat.format(this.myCalendar.getTime()));
    this.savedDate = new SimpleDateFormat("yyyy-MM-dd", Locale.US).format(this.myCalendar.getTime());
  }
  
  public void camera(View paramView)
  {
    paramView = new AlertDialog.Builder(this);
    paramView.setTitle("Fotografía");
    paramView.setMessage("¿Cómo vas a obtener la fotografía?");
    paramView.setPositiveButton("Cancelar", new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        paramAnonymousDialogInterface.cancel();
      }
    });
    paramView.setNeutralButton("Cámara", new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        CreateFamily.this.getFromCamera();
      }
    });
    paramView.setNegativeButton("Galería", new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        CreateFamily.this.getFromGallery();
      }
    });
    paramView.create().show();
  }
  
  public void contactos(View paramView)
  {
    startActivity(new Intent(this, EmergencyContacts.class));
  }
  
  public void cuidados(View paramView)
  {
    startActivity(new Intent(this, Categories.class));
  }
  
  public void delete(View paramView)
  {
    paramView = new AlertDialog.Builder(this);
    paramView.setTitle("Vive Más");
    paramView.setMessage("¿Estás seguro que quieres borrar a este familiar?");
    paramView.setPositiveButton("OK", new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        try
        {
          CreateFamily.this.beneficiaryInstance.deleteBeneficiary(CreateFamily.this, CreateFamily.this.responseAdd);
          return;
        }
        catch (JSONException paramAnonymousDialogInterface)
        {
          paramAnonymousDialogInterface.printStackTrace();
        }
      }
    });
    paramView.setNegativeButton("Cancelar", new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        paramAnonymousDialogInterface.dismiss();
      }
    });
    paramView.show();
  }
  
  public Beneficiary getBeneficiaryInstance(Customer paramCustomer)
    throws JSONException
  {
    if (this.beneficiaryInstance.userId != null) {
      return paramCustomer.getBeneficiaryWithId(this.beneficiaryInstance.userId);
    }
    return paramCustomer.getLastBeneficiary();
  }
  
  public void getFromCamera()
  {
    if (ActivityCompat.checkSelfPermission(this, "android.permission.CAMERA") != 0)
    {
      toast("No has dado permisos para usar tu cámara. Necesitas este permiso para seguir.");
      return;
    }
    Object localObject = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "ViveMas");
    if ((!((File)localObject).exists()) && (!((File)localObject).mkdirs()))
    {
      Log.d("ViveMas", "failed to create directory");
      return;
    }
    String str = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
    this.file = new File(((File)localObject).getPath() + File.separator + "IMG_" + str + ".png");
    localObject = new Intent("android.media.action.IMAGE_CAPTURE");
    ((Intent)localObject).putExtra("output", Uri.fromFile(this.file));
    startActivityForResult((Intent)localObject, 1);
  }
  
  public void getFromGallery()
  {
    Intent localIntent = new Intent();
    localIntent.setType("image/*");
    localIntent.setAction("android.intent.action.GET_CONTENT");
    startActivityForResult(Intent.createChooser(localIntent, "Complete action using"), 2);
  }
  
  public void loadDataFromDictionary()
    throws JSONException
  {
    EditText localEditText1 = (EditText)findViewById(2131689689);
    EditText localEditText2 = (EditText)findViewById(2131689696);
    Object localObject2 = (EditText)findViewById(2131689697);
    EditText localEditText3 = (EditText)findViewById(2131689698);
    EditText localEditText4 = (EditText)findViewById(2131689706);
    EditText localEditText5 = (EditText)findViewById(2131689707);
    EditText localEditText6 = (EditText)findViewById(2131689708);
    EditText localEditText7 = (EditText)findViewById(2131689709);
    Object localObject1 = null;
    if (this.familyData.has("meta")) {
      localObject1 = this.familyData.getJSONObject("meta");
    }
    String str1;
    if (this.familyData != null)
    {
      if (this.familyData.getString("gender").equals("male")) {
        man(null);
      }
    }
    else
    {
      if (this.familyData.has("pic"))
      {
        str1 = this.familyData.getString("pic");
        ImageLoader.getInstance().loadImage(str1, new SimpleImageLoadingListener()
        {
          public void onLoadingComplete(String paramAnonymousString, View paramAnonymousView, Bitmap paramAnonymousBitmap)
          {
            ((ImageView)CreateFamily.this.findViewById(2131689695)).setImageBitmap(CreateFamily.this.getCircleBitmap(paramAnonymousBitmap));
          }
        });
      }
      if (!this.familyData.has("activities")) {
        break label496;
      }
    }
    label496:
    for (Model.getInstance().serviceCategories = this.familyData.getJSONArray("activities");; Model.getInstance().serviceCategories = new JSONArray())
    {
      str1 = this.familyData.getJSONObject("name").getString("first");
      String str2 = this.familyData.getJSONObject("name").getString("last");
      String str3 = this.familyData.getString("birthday");
      str3 = str3.substring(0, Math.min(str3.length(), 10));
      if (localObject1 != null)
      {
        String str4 = ((JSONObject)localObject1).getString("alias");
        String str5 = ((JSONObject)localObject1).getString("weight");
        String str6 = ((JSONObject)localObject1).getString("height");
        String str7 = ((JSONObject)localObject1).getString("allergies");
        String str8 = ((JSONObject)localObject1).getString("ailments");
        ((EditText)localObject2).setText(str4);
        localEditText4.setText(str5);
        localEditText5.setText(str6);
        localEditText6.setText(str7);
        localEditText7.setText(str8);
        if (((JSONObject)localObject1).has("emergencyContacts")) {
          Model.getInstance().emergencyContacts = ((JSONObject)localObject1).getJSONArray("emergencyContacts");
        }
        localObject2 = this.familyData.getString("kinship");
        int i;
        if (!((String)localObject2).equals(null))
        {
          i = this.adapterKinship.getPosition(localObject2);
          this.spinnerKinship.setSelection(i);
        }
        localObject1 = ((JSONObject)localObject1).getString("blood");
        if (!((String)localObject1).equals(null))
        {
          i = this.adapterBlood.getPosition(localObject1);
          this.spinnerBlood.setSelection(i);
        }
      }
      localEditText1.setText(str1);
      localEditText2.setText(str2);
      localEditText3.setText(str3);
      return;
      woman(null);
      break;
    }
  }
  
  public void man(View paramView)
  {
    ((ImageButton)findViewById(2131689701)).setImageResource(2130837977);
    ((ImageButton)findViewById(2131689703)).setImageResource(2130837978);
    this.isWoman = false;
    setupSpinners();
  }
  
  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    ImageButton localImageButton = (ImageButton)findViewById(2131689695);
    if (paramInt2 != -1) {}
    for (;;)
    {
      return;
      if (paramInt1 == 1)
      {
        this.photoSelected = true;
        try
        {
          localImageButton.setImageBitmap(cropToSquare(getBitmapFromUri(Uri.fromFile(this.file))));
          return;
        }
        catch (IOException paramIntent)
        {
          paramIntent.printStackTrace();
          return;
        }
      }
      if (paramInt1 == 2) {
        try
        {
          paramIntent = paramIntent.getData();
          if (saveBitmapToStorage(cropToSquare(getBitmapFromUri(paramIntent))))
          {
            this.photoSelected = true;
            Uri.fromFile(this.file);
            localImageButton.setImageBitmap(getBitmapFromUri(paramIntent));
            return;
          }
        }
        catch (IOException paramIntent)
        {
          paramIntent.printStackTrace();
        }
      }
    }
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130968606);
    if ((ActivityCompat.checkSelfPermission(this, "android.permission.CAMERA") != 0) || (ActivityCompat.checkSelfPermission(this, "android.permission.READ_EXTERNAL_STORAGE") != 0)) {
      ActivityCompat.requestPermissions(this, new String[] { "android.permission.CAMERA", "android.permission.READ_EXTERNAL_STORAGE" }, 0);
    }
    this.datePicker = ((EditText)findViewById(2131689698));
    this.datePicker.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        CreateFamily.access$002(CreateFamily.this, new DatePickerDialog(CreateFamily.this, CreateFamily.this.date, CreateFamily.this.myCalendar.get(1), CreateFamily.this.myCalendar.get(2), CreateFamily.this.myCalendar.get(5)));
        CreateFamily.this.mDialog.getDatePicker().setMaxDate(CreateFamily.this.myCalendar.getTimeInMillis());
        CreateFamily.this.mDialog.show();
      }
    });
    setTitle("AGREGAR FAMILIAR");
    this.deleteButton = ((Button)findViewById(2131689711));
    this.beneficiaryInstance = new Beneficiary();
    setupSpinners();
    Model.getInstance().emergencyContacts = new JSONArray();
    paramBundle = getIntent().getExtras();
    if (paramBundle != null) {
      paramBundle = paramBundle.getString("familyData");
    }
    try
    {
      this.familyData = new JSONObject(paramBundle);
      this.beneficiaryInstance.initFromJSON(this.familyData);
      Model.getInstance().activeBeneficiary = this.beneficiaryInstance;
      loadDataFromDictionary();
      return;
    }
    catch (JSONException paramBundle) {}
    this.deleteButton.setVisibility(8);
    return;
  }
  
  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    getMenuInflater().inflate(2131755011, paramMenu);
    return true;
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    }
    for (;;)
    {
      return true;
      submit(null);
    }
  }
  
  public void onResume()
  {
    super.onResume();
    int i = Model.getInstance().emergencyContacts.length();
    ((TextView)findViewById(2131689710)).setText("Contactos de emergencia (" + i + ")");
  }
  
  public void setupSpinners()
  {
    this.spinnerKinship = ((Spinner)findViewById(2131689692));
    if ((this.beneficiaryInstance.kinship != null) && (this.beneficiaryInstance.kinship.contains("self")) && (this.familyData != null))
    {
      this.spinnerKinship.setVisibility(8);
      this.deleteButton.setVisibility(8);
      ((TextView)findViewById(2131689704)).setVisibility(8);
    }
    if (this.isWoman) {}
    for (this.adapterKinship = ArrayAdapter.createFromResource(this, 2131623950, 17367048);; this.adapterKinship = ArrayAdapter.createFromResource(this, 2131623949, 17367048))
    {
      this.adapterKinship.setDropDownViewResource(17367049);
      this.spinnerKinship.setAdapter(this.adapterKinship);
      this.spinnerBlood = ((Spinner)findViewById(2131689705));
      this.adapterBlood = ArrayAdapter.createFromResource(this, 2131623936, 17367048);
      this.adapterBlood.setDropDownViewResource(17367049);
      this.spinnerBlood.setAdapter(this.adapterBlood);
      return;
    }
  }
  
  public void submit(View paramView)
  {
    Object localObject2 = (EditText)findViewById(2131689689);
    Object localObject3 = (EditText)findViewById(2131689696);
    Object localObject4 = (EditText)findViewById(2131689697);
    Object localObject9 = (EditText)findViewById(2131689698);
    paramView = (Spinner)findViewById(2131689692);
    Object localObject6 = (Spinner)findViewById(2131689705);
    Object localObject8 = (EditText)findViewById(2131689707);
    Object localObject7 = (EditText)findViewById(2131689706);
    Object localObject5 = (EditText)findViewById(2131689708);
    Object localObject1 = (EditText)findViewById(2131689709);
    localObject2 = ((EditText)localObject2).getText().toString();
    localObject3 = ((EditText)localObject3).getText().toString();
    localObject4 = ((EditText)localObject4).getText().toString();
    localObject9 = ((EditText)localObject9).getText().toString();
    if (paramView.getVisibility() == 8) {}
    String str;
    for (paramView = "self";; paramView = paramView.getSelectedItem().toString())
    {
      localObject6 = ((Spinner)localObject6).getSelectedItem().toString();
      localObject8 = ((EditText)localObject8).getText().toString();
      localObject7 = ((EditText)localObject7).getText().toString();
      localObject5 = ((EditText)localObject5).getText().toString();
      str = ((EditText)localObject1).getText().toString();
      if ((!((String)localObject2).matches("")) && (!((String)localObject3).matches("")) && (!((String)localObject9).matches(""))) {
        break;
      }
      doAlert("Error", "Necesitamos por lo menos nombre, apellido, parentesco y fecha de nacimiento para guardar al familiar.");
      return;
    }
    if (this.isWoman) {
      localObject1 = "female";
    }
    for (;;)
    {
      try
      {
        localObject9 = Model.getInstance().serviceCategories;
        JSONObject localJSONObject = new JSONObject();
        localJSONObject.put("first", localObject2);
        localJSONObject.put("last", localObject3);
        localObject3 = new JSONObject();
        ((JSONObject)localObject3).put("alias", localObject4);
        ((JSONObject)localObject3).put("blood", localObject6);
        ((JSONObject)localObject3).put("weight", localObject7);
        ((JSONObject)localObject3).put("height", localObject8);
        ((JSONObject)localObject3).put("allergies", localObject5);
        ((JSONObject)localObject3).put("ailments", str);
        ((JSONObject)localObject3).put("emergencyContacts", Model.getInstance().emergencyContacts);
        if (this.familyData == null) {
          break label552;
        }
        localObject2 = this.familyData;
        ((JSONObject)localObject2).put("name", localJSONObject);
        ((JSONObject)localObject2).put("birthday", this.savedDate);
        ((JSONObject)localObject2).put("gender", localObject1);
        if (this.beneficiaryInstance.kinship == null) {
          break label563;
        }
        if ((this.beneficiaryInstance.kinship.contains("self")) && (this.familyData != null)) {
          System.out.print("Soy yo");
        }
        ((JSONObject)localObject2).put("meta", localObject3);
        ((JSONObject)localObject2).put("activities", localObject9);
        System.out.println(localObject2);
        if (this.familyData == null) {
          break;
        }
        this.beneficiaryInstance.updateBeneficiary(this, (JSONObject)localObject2, this.responseAdd);
        return;
      }
      catch (JSONException paramView)
      {
        return;
      }
      localObject1 = "male";
      continue;
      label552:
      localObject2 = new JSONObject();
      continue;
      label563:
      ((JSONObject)localObject2).put("kinship", paramView);
    }
    this.beneficiaryInstance.saveBeneficiary(this, (JSONObject)localObject2, this.responseAdd);
  }
  
  public void woman(View paramView)
  {
    ((ImageButton)findViewById(2131689701)).setImageResource(2130837978);
    ((ImageButton)findViewById(2131689703)).setImageResource(2130837977);
    this.isWoman = true;
    setupSpinners();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/vivemas/com/vivemasclientes/CreateFamily.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */