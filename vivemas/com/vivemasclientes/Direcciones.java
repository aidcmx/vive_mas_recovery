package vivemas.com.vivemasclientes;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import java.io.PrintStream;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vivemas.com.vivemasclientes.Adapters.AddressAdapter;
import vivemas.com.vivemasclientes.Model.Customer;
import vivemas.com.vivemasclientes.Model.Model;

public class Direcciones
  extends AppCompatActivity
{
  AddressAdapter adapter;
  ListView addressList;
  Customer c;
  View.OnClickListener favListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      paramAnonymousView = new Intent(Direcciones.this, NuevaDireccion.class);
      Direcciones.this.startActivity(paramAnonymousView);
    }
  };
  boolean isSelecting = false;
  AdapterView.OnItemClickListener onClickListener = new AdapterView.OnItemClickListener()
  {
    public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
    {
      if (Direcciones.this.isSelecting) {
        try
        {
          Model.getInstance().activeService.address = Direcciones.this.c.addresses.getJSONObject(paramAnonymousInt);
          Direcciones.this.finish();
          return;
        }
        catch (JSONException paramAnonymousAdapterView) {}
      }
      paramAnonymousAdapterView = new Intent(Direcciones.this, NuevaDireccion.class);
      try
      {
        paramAnonymousAdapterView.putExtra("addressData", Direcciones.this.c.addresses.getJSONObject(paramAnonymousInt).toString());
        Direcciones.this.startActivity(paramAnonymousAdapterView);
        return;
      }
      catch (JSONException paramAnonymousAdapterView)
      {
        paramAnonymousAdapterView.printStackTrace();
      }
    }
  };
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130968607);
    setTitle("DIRECCIONES");
    paramBundle = getIntent().getExtras();
    if (paramBundle == null) {
      this.isSelecting = false;
    }
    for (;;)
    {
      this.addressList = ((ListView)findViewById(2131689712));
      System.out.println(this.addressList);
      try
      {
        this.c = Customer.loadLocal();
        this.adapter = new AddressAdapter(this, this.c.addresses);
        this.addressList.setAdapter(this.adapter);
        this.addressList.setOnItemClickListener(this.onClickListener);
        paramBundle = (FloatingActionButton)findViewById(2131689713);
        if (paramBundle != null) {
          paramBundle.setOnClickListener(this.favListener);
        }
        return;
        this.isSelecting = paramBundle.getBoolean("isSelecting");
      }
      catch (JSONException paramBundle)
      {
        for (;;) {}
      }
    }
  }
  
  public void onResume()
  {
    super.onResume();
    try
    {
      this.c = Customer.loadLocal();
      this.adapter = new AddressAdapter(this, this.c.addresses);
      this.addressList.setAdapter(this.adapter);
      this.adapter.notifyDataSetChanged();
      return;
    }
    catch (JSONException localJSONException)
    {
      localJSONException.printStackTrace();
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/vivemas/com/vivemasclientes/Direcciones.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */