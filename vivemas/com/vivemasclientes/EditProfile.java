package vivemas.com.vivemasclientes;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphRequest.GraphJSONObjectCallback;
import com.facebook.GraphResponse;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import cz.msebera.android.httpclient.Header;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vivemas.com.vivemasclientes.Model.Beneficiary;
import vivemas.com.vivemasclientes.Model.Customer;
import vivemas.com.vivemasclientes.Model.Model;

public class EditProfile
  extends AiDC_Activity
{
  private static final int ASK_MULTIPLE_PERMISSION_REQUEST_CODE = 0;
  AsyncHttpResponseHandler benefResponse = new AsyncHttpResponseHandler()
  {
    public void onFailure(int paramAnonymousInt, Header[] paramAnonymousArrayOfHeader, byte[] paramAnonymousArrayOfByte, Throwable paramAnonymousThrowable)
    {
      try
      {
        paramAnonymousArrayOfHeader = new String(paramAnonymousArrayOfByte, "UTF-8");
        EditProfile.this.doAlert("error: ", "error " + paramAnonymousInt + " : " + paramAnonymousArrayOfHeader);
        return;
      }
      catch (UnsupportedEncodingException paramAnonymousArrayOfHeader) {}
    }
    
    public void onFinish()
    {
      super.onFinish();
      EditProfile.this.stoppedLoading();
    }
    
    /* Error */
    public void onSuccess(int paramAnonymousInt, Header[] paramAnonymousArrayOfHeader, byte[] paramAnonymousArrayOfByte)
    {
      // Byte code:
      //   0: new 22	java/lang/String
      //   3: dup
      //   4: aload_3
      //   5: ldc 24
      //   7: invokespecial 27	java/lang/String:<init>	([BLjava/lang/String;)V
      //   10: astore_2
      //   11: new 63	org/json/JSONObject
      //   14: dup
      //   15: aload_2
      //   16: invokespecial 66	org/json/JSONObject:<init>	(Ljava/lang/String;)V
      //   19: astore_2
      //   20: aload_0
      //   21: getfield 12	vivemas/com/vivemasclientes/EditProfile$7:this$0	Lvivemas/com/vivemasclientes/EditProfile;
      //   24: aload_2
      //   25: invokestatic 72	vivemas/com/vivemasclientes/Model/Customer:initWithJSONObject	(Lorg/json/JSONObject;)Lvivemas/com/vivemasclientes/Model/Customer;
      //   28: putfield 76	vivemas/com/vivemasclientes/EditProfile:customer	Lvivemas/com/vivemasclientes/Model/Customer;
      //   31: aload_0
      //   32: getfield 12	vivemas/com/vivemasclientes/EditProfile$7:this$0	Lvivemas/com/vivemasclientes/EditProfile;
      //   35: getfield 76	vivemas/com/vivemasclientes/EditProfile:customer	Lvivemas/com/vivemasclientes/Model/Customer;
      //   38: invokevirtual 79	vivemas/com/vivemasclientes/Model/Customer:saveLocal	()V
      //   41: aload_0
      //   42: getfield 12	vivemas/com/vivemasclientes/EditProfile$7:this$0	Lvivemas/com/vivemasclientes/EditProfile;
      //   45: getfield 83	vivemas/com/vivemasclientes/EditProfile:imageSet	Z
      //   48: ifne +10 -> 58
      //   51: aload_0
      //   52: getfield 12	vivemas/com/vivemasclientes/EditProfile$7:this$0	Lvivemas/com/vivemasclientes/EditProfile;
      //   55: invokevirtual 86	vivemas/com/vivemasclientes/EditProfile:finish	()V
      //   58: return
      //   59: astore_2
      //   60: return
      //   61: astore_2
      //   62: return
      //   63: astore_2
      //   64: return
      //   65: astore_2
      //   66: return
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	67	0	this	7
      //   0	67	1	paramAnonymousInt	int
      //   0	67	2	paramAnonymousArrayOfHeader	Header[]
      //   0	67	3	paramAnonymousArrayOfByte	byte[]
      // Exception table:
      //   from	to	target	type
      //   0	11	59	org/json/JSONException
      //   11	58	61	org/json/JSONException
      //   0	11	63	java/io/UnsupportedEncodingException
      //   11	58	65	java/io/UnsupportedEncodingException
    }
  };
  Beneficiary beneficiary;
  Customer customer;
  DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener()
  {
    public void onDateSet(DatePicker paramAnonymousDatePicker, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3)
    {
      EditProfile.this.myCalendar.set(1, paramAnonymousInt1);
      EditProfile.this.myCalendar.set(2, paramAnonymousInt2);
      EditProfile.this.myCalendar.set(5, paramAnonymousInt3);
      EditProfile.this.updateLabel();
    }
  };
  private EditText datePicker;
  AsyncHttpResponseHandler editResponse = new AsyncHttpResponseHandler()
  {
    public void onFailure(int paramAnonymousInt, Header[] paramAnonymousArrayOfHeader, byte[] paramAnonymousArrayOfByte, Throwable paramAnonymousThrowable)
    {
      try
      {
        new String(paramAnonymousArrayOfByte, "UTF-8");
        return;
      }
      catch (UnsupportedEncodingException paramAnonymousArrayOfHeader) {}
    }
    
    public void onStart()
    {
      super.onStart();
      EditProfile.this.loading(EditProfile.this);
    }
    
    /* Error */
    public void onSuccess(int paramAnonymousInt, Header[] paramAnonymousArrayOfHeader, byte[] paramAnonymousArrayOfByte)
    {
      // Byte code:
      //   0: new 22	java/lang/String
      //   3: dup
      //   4: aload_3
      //   5: ldc 24
      //   7: invokespecial 27	java/lang/String:<init>	([BLjava/lang/String;)V
      //   10: astore_2
      //   11: new 40	org/json/JSONObject
      //   14: dup
      //   15: aload_2
      //   16: invokespecial 43	org/json/JSONObject:<init>	(Ljava/lang/String;)V
      //   19: pop
      //   20: return
      //   21: astore_2
      //   22: return
      //   23: astore_2
      //   24: return
      //   25: astore_2
      //   26: return
      //   27: astore_2
      //   28: return
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	29	0	this	6
      //   0	29	1	paramAnonymousInt	int
      //   0	29	2	paramAnonymousArrayOfHeader	Header[]
      //   0	29	3	paramAnonymousArrayOfByte	byte[]
      // Exception table:
      //   from	to	target	type
      //   0	11	21	org/json/JSONException
      //   11	20	23	org/json/JSONException
      //   0	11	25	java/io/UnsupportedEncodingException
      //   11	20	27	java/io/UnsupportedEncodingException
    }
  };
  boolean imageSet = false;
  boolean isRegistration = false;
  boolean isWoman = true;
  Calendar myCalendar = Calendar.getInstance();
  AsyncHttpResponseHandler pictureResponse = new AsyncHttpResponseHandler()
  {
    public void onFailure(int paramAnonymousInt, Header[] paramAnonymousArrayOfHeader, byte[] paramAnonymousArrayOfByte, Throwable paramAnonymousThrowable)
    {
      try
      {
        paramAnonymousArrayOfHeader = new String(paramAnonymousArrayOfByte, "UTF-8");
        EditProfile.this.doAlert("error: ", "error " + paramAnonymousInt + " : " + paramAnonymousArrayOfHeader);
        return;
      }
      catch (UnsupportedEncodingException paramAnonymousArrayOfHeader) {}
    }
    
    public void onFinish()
    {
      super.onFinish();
      EditProfile.this.stoppedLoading();
    }
    
    public void onStart()
    {
      super.onStart();
      EditProfile.this.loading(EditProfile.this);
    }
    
    public void onSuccess(int paramAnonymousInt, Header[] paramAnonymousArrayOfHeader, byte[] paramAnonymousArrayOfByte)
    {
      EditProfile.this.toast("Foto actualizada");
    }
  };
  
  private void preloadFacebook()
  {
    GraphRequest localGraphRequest = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback()
    {
      public void onCompleted(JSONObject paramAnonymousJSONObject, GraphResponse paramAnonymousGraphResponse)
      {
        Log.v("LoginActivity", paramAnonymousGraphResponse.toString());
        paramAnonymousGraphResponse = (EditText)EditProfile.this.findViewById(2131689716);
        EditText localEditText1 = (EditText)EditProfile.this.findViewById(2131689696);
        localEditText1 = (EditText)EditProfile.this.findViewById(2131689553);
        EditText localEditText2 = (EditText)EditProfile.this.findViewById(2131689690);
        try
        {
          if (paramAnonymousJSONObject.has("name")) {
            paramAnonymousGraphResponse.setText(paramAnonymousJSONObject.getString("name"));
          }
          if (paramAnonymousJSONObject.has("email")) {
            localEditText1.setText(paramAnonymousJSONObject.getString("email"));
          }
          if (paramAnonymousJSONObject.has("gender"))
          {
            if (paramAnonymousJSONObject.getString("gender").equals("male"))
            {
              EditProfile.this.man(null);
              return;
            }
            EditProfile.this.woman(null);
            return;
          }
        }
        catch (JSONException paramAnonymousJSONObject) {}
      }
    });
    Bundle localBundle = new Bundle();
    localBundle.putString("fields", "id,name,email,gender,birthday");
    localGraphRequest.setParameters(localBundle);
    localGraphRequest.executeAsync();
  }
  
  private void updateLabel()
  {
    SimpleDateFormat localSimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
    this.datePicker.setText(localSimpleDateFormat.format(this.myCalendar.getTime()));
  }
  
  public void camera(View paramView)
  {
    paramView = new AlertDialog.Builder(this);
    paramView.setTitle("Fotografía");
    paramView.setMessage("¿Cómo vas a obtener la fotografía?");
    paramView.setPositiveButton("Cancelar", new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        paramAnonymousDialogInterface.cancel();
      }
    });
    paramView.setNeutralButton("Cámara", new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        EditProfile.this.getFromCamera();
      }
    });
    paramView.setNegativeButton("Galería", new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        EditProfile.this.getFromGallery();
      }
    });
    paramView.create().show();
  }
  
  public void deletePicture(View paramView)
  {
    try
    {
      Customer.loadLocal().deletePicture(this, this.pictureResponse);
      return;
    }
    catch (JSONException paramView)
    {
      paramView.printStackTrace();
    }
  }
  
  public void man(View paramView)
  {
    ((ImageButton)findViewById(2131689701)).setImageResource(2130837977);
    ((ImageButton)findViewById(2131689703)).setImageResource(2130837978);
    this.isWoman = false;
  }
  
  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    ImageView localImageView = (ImageView)findViewById(2131689695);
    if (paramInt2 != -1) {
      return;
    }
    if (paramInt1 == 1)
    {
      this.imageSet = true;
      paramIntent = Uri.fromFile(this.file);
    }
    try
    {
      localImageView.setImageBitmap(getCircleBitmap(cropToSquare(getBitmapFromUri(paramIntent))));
      for (;;)
      {
        if (this.file.length() / 1024L > this.SIZE_LIMIT) {
          compressBitmap(2, 75);
        }
        try
        {
          sendImage(this.customer.userId);
          return;
        }
        catch (JSONException paramIntent)
        {
          return;
        }
        if (paramInt1 == 2) {
          try
          {
            this.imageSet = true;
            paramIntent = getBitmapFromUri(paramIntent.getData());
            if (saveBitmapToStorage(paramIntent)) {
              localImageView.setImageBitmap(getCircleBitmap(cropToSquare(paramIntent)));
            }
          }
          catch (IOException paramIntent) {}
        }
      }
    }
    catch (IOException paramIntent)
    {
      for (;;) {}
    }
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130968609);
    this.datePicker = ((EditText)findViewById(2131689698));
    if ((ActivityCompat.checkSelfPermission(this, "android.permission.CAMERA") != 0) || (ActivityCompat.checkSelfPermission(this, "android.permission.READ_EXTERNAL_STORAGE") != 0)) {
      ActivityCompat.requestPermissions(this, new String[] { "android.permission.CAMERA", "android.permission.READ_EXTERNAL_STORAGE" }, 0);
    }
    try
    {
      this.customer = Customer.loadLocal();
      if (this.customer.defaultBeneficiary == null)
      {
        setTitle("REGISTRO");
        this.isRegistration = true;
        return;
      }
      this.beneficiary = this.customer.getDefaultBeneficiary();
      preload();
    }
    catch (JSONException paramBundle)
    {
      for (;;) {}
    }
    setTitle("EDITAR PERFIL");
    this.datePicker.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        paramAnonymousView = new DatePickerDialog(EditProfile.this, EditProfile.this.date, EditProfile.this.myCalendar.get(1), EditProfile.this.myCalendar.get(2), EditProfile.this.myCalendar.get(5));
        Calendar localCalendar = Calendar.getInstance();
        localCalendar.add(1, -18);
        paramAnonymousView.getDatePicker().setMaxDate(localCalendar.getTimeInMillis());
        paramAnonymousView.show();
      }
    });
  }
  
  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    getMenuInflater().inflate(2131755011, paramMenu);
    return true;
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    submit(null);
    return true;
  }
  
  public void preload()
  {
    Object localObject = (EditText)findViewById(2131689716);
    EditText localEditText1 = (EditText)findViewById(2131689696);
    EditText localEditText2 = (EditText)findViewById(2131689553);
    EditText localEditText3 = (EditText)findViewById(2131689690);
    final ImageView localImageView = (ImageView)findViewById(2131689695);
    ((EditText)localObject).setText(this.customer.first);
    localEditText1.setText(this.customer.last);
    localEditText2.setText(this.customer.email);
    localEditText3.setText(this.customer.phone);
    if (this.customer.picture != null) {
      ImageLoader.getInstance().loadImage(this.customer.picture, new SimpleImageLoadingListener()
      {
        public void onLoadingComplete(String paramAnonymousString, View paramAnonymousView, Bitmap paramAnonymousBitmap)
        {
          paramAnonymousString = EditProfile.this.getCircleBitmap(paramAnonymousBitmap);
          localImageView.setImageBitmap(paramAnonymousString);
        }
      });
    }
    localObject = this.beneficiary.gender;
    this.datePicker.setText(this.beneficiary.birthday.substring(0, Math.min(this.beneficiary.birthday.length(), 10)));
    if (((String)localObject).equals("male"))
    {
      man(null);
      return;
    }
    woman(null);
  }
  
  public void sendImage(String paramString)
    throws JSONException
  {
    if (!this.imageSet)
    {
      startActivity(new Intent(this, Home.class));
      stoppedLoading();
      finish();
      return;
    }
    Model.getInstance().uploadPictureRegistration(this, this.pictureResponse, this.file, paramString);
  }
  
  public void submit(View paramView)
  {
    Object localObject1 = (EditText)findViewById(2131689716);
    Object localObject3 = (EditText)findViewById(2131689696);
    Object localObject2 = (EditText)findViewById(2131689553);
    paramView = (EditText)findViewById(2131689690);
    localObject1 = ((EditText)localObject1).getText().toString();
    localObject3 = ((EditText)localObject3).getText().toString();
    localObject2 = ((EditText)localObject2).getText().toString();
    String str = paramView.getText().toString();
    if ((((String)localObject1).matches("")) || (((String)localObject3).matches("")) || (((String)localObject2).matches("")) || (str.matches("")))
    {
      doAlert("Error", "Debes llenar todos los campos.");
      return;
    }
    if (this.isWoman) {}
    for (paramView = "female";; paramView = "male") {
      try
      {
        this.customer.first = ((String)localObject1);
        this.customer.last = ((String)localObject3);
        localObject1 = new JSONObject();
        ((JSONObject)localObject1).put("first", this.customer.first);
        ((JSONObject)localObject1).put("last", this.customer.last);
        localObject3 = new JSONObject();
        if (!this.isRegistration) {
          ((JSONObject)localObject3).put("_id", this.beneficiary.userId);
        }
        if (this.isRegistration) {
          ((JSONObject)localObject3).put("kinship", "self");
        }
        ((JSONObject)localObject3).put("name", localObject1);
        ((JSONObject)localObject3).put("birthday", this.datePicker.getText().toString());
        ((JSONObject)localObject3).put("gender", paramView);
        new JSONArray().put(localObject3);
        this.customer.email = ((String)localObject2);
        this.customer.phone = str;
        this.customer.updateData(this, this.editResponse);
        if (this.customer.defaultBeneficiary != null) {
          break;
        }
        this.beneficiary = new Beneficiary();
        this.beneficiary.saveBeneficiary(this, (JSONObject)localObject3, this.benefResponse);
        return;
      }
      catch (JSONException paramView)
      {
        return;
      }
    }
    this.beneficiary.updateBeneficiary(this, (JSONObject)localObject3, this.benefResponse);
  }
  
  public void woman(View paramView)
  {
    ((ImageButton)findViewById(2131689701)).setImageResource(2130837978);
    ((ImageButton)findViewById(2131689703)).setImageResource(2130837977);
    this.isWoman = true;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/vivemas/com/vivemasclientes/EditProfile.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */