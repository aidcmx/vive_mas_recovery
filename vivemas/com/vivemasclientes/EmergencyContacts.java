package vivemas.com.vivemasclientes;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import org.json.JSONArray;
import vivemas.com.vivemasclientes.Adapters.EmergencyAdapter;
import vivemas.com.vivemasclientes.Model.Model;

public class EmergencyContacts
  extends AiDC_Activity
{
  EmergencyAdapter adapter;
  AdapterView.OnItemClickListener clickListener = new AdapterView.OnItemClickListener()
  {
    public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
    {
      paramAnonymousAdapterView = new Intent(EmergencyContacts.this, AddContact.class);
      paramAnonymousAdapterView.putExtra("isEditing", true);
      paramAnonymousAdapterView.putExtra("row", paramAnonymousInt);
      EmergencyContacts.this.startActivity(paramAnonymousAdapterView);
    }
  };
  JSONArray emergencyContacts;
  ListView emergencyList;
  Boolean firstLoaded = Boolean.valueOf(true);
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130968610);
    setTitle("CONTACTOS");
  }
  
  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    getMenuInflater().inflate(2131755008, paramMenu);
    return true;
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    paramMenuItem = new Intent(this, AddContact.class);
    paramMenuItem.putExtra("isEditing", false);
    startActivity(paramMenuItem);
    return true;
  }
  
  public void onResume()
  {
    super.onResume();
    this.emergencyContacts = Model.getInstance().emergencyContacts;
    if ((this.firstLoaded.booleanValue()) && (this.emergencyContacts.length() == 0))
    {
      Intent localIntent = new Intent(this, AddContact.class);
      localIntent.putExtra("isEditing", false);
      startActivity(localIntent);
      this.firstLoaded = Boolean.valueOf(false);
    }
    this.adapter = new EmergencyAdapter(this, this.emergencyContacts);
    this.emergencyList = ((ListView)findViewById(2131689717));
    this.emergencyList.setOnItemClickListener(this.clickListener);
    this.emergencyList.setAdapter(this.adapter);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/vivemas/com/vivemasclientes/EmergencyContacts.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */