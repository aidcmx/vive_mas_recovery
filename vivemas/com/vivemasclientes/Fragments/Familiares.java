package vivemas.com.vivemasclientes.Fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ListView;
import com.loopj.android.http.AsyncHttpResponseHandler;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vivemas.com.vivemasclientes.Adapters.FamilyAdapter;
import vivemas.com.vivemasclientes.Model.Beneficiary;

public class Familiares
  extends BaseFragment
{
  private Activity activity;
  private FamilyAdapter adapter;
  public JSONArray dataArray;
  private ListView listView;
  AsyncHttpResponseHandler responseHandler = new Familiares.2(this);
  
  public void onActivityCreated(Bundle paramBundle)
  {
    super.onActivityCreated(paramBundle);
  }
  
  public boolean onContextItemSelected(MenuItem paramMenuItem)
  {
    int i = ((AdapterView.AdapterContextMenuInfo)paramMenuItem.getMenuInfo()).position;
    try
    {
      paramMenuItem = this.dataArray.getJSONObject(i);
      Beneficiary localBeneficiary = new Beneficiary();
      localBeneficiary.initFromJSON(paramMenuItem);
      localBeneficiary.deleteBeneficiary(this.activity, this.responseHandler);
      return true;
    }
    catch (JSONException paramMenuItem)
    {
      for (;;) {}
    }
  }
  
  public void onCreateContextMenu(ContextMenu paramContextMenu, View paramView, ContextMenu.ContextMenuInfo paramContextMenuInfo)
  {
    super.onCreateContextMenu(paramContextMenu, paramView, paramContextMenuInfo);
    if (paramView.getId() == 2131689847) {
      this.activity.getMenuInflater().inflate(2131755009, paramContextMenu);
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    this.activity = getActivity();
    return paramLayoutInflater.inflate(2130968655, paramViewGroup, false);
  }
  
  public void updateTable(JSONArray paramJSONArray)
  {
    this.dataArray = new JSONArray();
    int i = 0;
    for (;;)
    {
      if (i < paramJSONArray.length()) {}
      try
      {
        JSONObject localJSONObject = paramJSONArray.getJSONObject(i);
        if (!localJSONObject.getBoolean("archived")) {
          this.dataArray.put(localJSONObject);
        }
        i += 1;
        continue;
        this.listView = ((ListView)this.activity.findViewById(2131689847));
        this.adapter = new FamilyAdapter(this.activity, this.dataArray);
        this.listView.setAdapter(this.adapter);
        registerForContextMenu(this.listView);
        this.listView.setOnItemClickListener(new Familiares.1(this));
        return;
      }
      catch (JSONException localJSONException)
      {
        for (;;) {}
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/vivemas/com/vivemasclientes/Fragments/Familiares.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */