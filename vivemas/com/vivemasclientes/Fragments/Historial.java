package vivemas.com.vivemasclientes.Fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import org.json.JSONArray;
import vivemas.com.vivemasclientes.Adapters.ServiceAdapter;

public class Historial
  extends BaseFragment
{
  private Activity activity;
  private ServiceAdapter adapter;
  public JSONArray dataArray;
  private ListView listView;
  
  public void onActivityCreated(Bundle paramBundle)
  {
    super.onActivityCreated(paramBundle);
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    this.activity = getActivity();
    return paramLayoutInflater.inflate(2130968656, paramViewGroup, false);
  }
  
  public void updateTable(JSONArray paramJSONArray)
  {
    this.dataArray = paramJSONArray;
    this.listView = ((ListView)this.activity.findViewById(2131689848));
    this.adapter = new ServiceAdapter(this.activity, this.dataArray);
    this.listView.setAdapter(this.adapter);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/vivemas/com/vivemasclientes/Fragments/Historial.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */