package vivemas.com.vivemasclientes.Helpers;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap.Config;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions.Builder;
import com.nostra13.universalimageloader.core.ImageLoader;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AbstractAdapter
  extends BaseAdapter
{
  protected Context context;
  public int count;
  ImageLoader imageLoader = ImageLoader.getInstance();
  public JSONArray jsonArray;
  public DisplayImageOptions options;
  protected ProgressDialog progDialog;
  
  public AbstractAdapter(Context paramContext, JSONArray paramJSONArray)
  {
    this.count = paramJSONArray.length();
    this.context = paramContext;
    this.jsonArray = paramJSONArray;
    this.options = new DisplayImageOptions.Builder().cacheInMemory(true).cacheOnDisk(true).bitmapConfig(Bitmap.Config.RGB_565).build();
  }
  
  public int getCount()
  {
    return this.count;
  }
  
  public Object getItem(int paramInt)
  {
    return Integer.valueOf(paramInt);
  }
  
  public long getItemId(int paramInt)
  {
    return paramInt;
  }
  
  public JSONObject getJSONforindex(int paramInt)
    throws JSONException
  {
    return this.jsonArray.getJSONObject(paramInt);
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    paramViewGroup = paramView;
    if (paramView == null) {
      paramViewGroup = new View(this.context);
    }
    return paramViewGroup;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/vivemas/com/vivemasclientes/Helpers/AbstractAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */