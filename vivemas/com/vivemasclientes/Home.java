package vivemas.com.vivemasclientes;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TabLayout.OnTabSelectedListener;
import android.support.design.widget.TabLayout.Tab;
import android.support.design.widget.TabLayout.TabLayoutOnPageChangeListener;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions.Builder;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import cz.msebera.android.httpclient.Header;
import java.io.UnsupportedEncodingException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vivemas.com.vivemasclientes.Fragments.Familiares;
import vivemas.com.vivemasclientes.Fragments.Historial;
import vivemas.com.vivemasclientes.Model.Customer;
import vivemas.com.vivemasclientes.Model.Model;
import vivemas.com.vivemasclientes.NewService.SelectUser;

public class Home
  extends AiDC_Activity
{
  private static final int ASK_MULTIPLE_PERMISSION_REQUEST_CODE = 0;
  private FloatingActionButton actionButton;
  View.OnClickListener addListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      if (Home.this.selectedIndex == 0)
      {
        paramAnonymousView = new Intent(Home.this, SelectUser.class);
        Home.this.startActivity(paramAnonymousView);
      }
      if (Home.this.selectedIndex == 1)
      {
        paramAnonymousView = new Intent(Home.this, CreateFamily.class);
        Home.this.startActivity(paramAnonymousView);
        Model.getInstance().serviceCategories = new JSONArray();
      }
    }
  };
  AsyncHttpResponseHandler customerHandler = new AsyncHttpResponseHandler()
  {
    public void onFailure(int paramAnonymousInt, Header[] paramAnonymousArrayOfHeader, byte[] paramAnonymousArrayOfByte, Throwable paramAnonymousThrowable)
    {
      if (paramAnonymousArrayOfByte == null) {
        return;
      }
      try
      {
        paramAnonymousArrayOfHeader = new String(paramAnonymousArrayOfByte, "UTF-8");
        Home.this.doAlert("error: ", "error " + paramAnonymousInt + " : " + paramAnonymousArrayOfHeader);
        return;
      }
      catch (UnsupportedEncodingException paramAnonymousArrayOfHeader) {}
    }
    
    public void onSuccess(int paramAnonymousInt, Header[] paramAnonymousArrayOfHeader, byte[] paramAnonymousArrayOfByte)
    {
      try
      {
        paramAnonymousArrayOfHeader = Customer.initWithJSONObject(new JSONObject(new String(paramAnonymousArrayOfByte, "UTF-8")));
        paramAnonymousArrayOfHeader.saveLocal();
        if (paramAnonymousArrayOfHeader.beneficiaries.length() == 0)
        {
          paramAnonymousArrayOfByte = new Intent(Home.this, EditProfile.class);
          Home.this.startActivity(paramAnonymousArrayOfByte);
        }
        Home.this.historial.updateTable(new JSONArray());
        Home.this.familiares.updateTable(paramAnonymousArrayOfHeader.beneficiaries);
        Home.this.initWithData(paramAnonymousArrayOfHeader);
        return;
      }
      catch (JSONException paramAnonymousArrayOfHeader) {}catch (UnsupportedEncodingException paramAnonymousArrayOfHeader) {}
    }
  };
  Familiares familiares;
  Historial historial;
  private int selectedIndex = 0;
  
  public void editProfile(View paramView)
  {
    startActivity(new Intent(this, ProfileOptions.class));
  }
  
  public void initWithData(Customer paramCustomer)
    throws JSONException
  {
    final ImageView localImageView = (ImageView)findViewById(2131689724);
    ((TextView)findViewById(2131689725)).setText(paramCustomer.fullName);
    if (paramCustomer.picture == null) {
      return;
    }
    ImageLoader localImageLoader = ImageLoader.getInstance();
    DisplayImageOptions localDisplayImageOptions = new DisplayImageOptions.Builder().cacheInMemory(true).cacheOnDisk(true).bitmapConfig(Bitmap.Config.RGB_565).build();
    localImageLoader.loadImage(paramCustomer.picture, localDisplayImageOptions, new SimpleImageLoadingListener()
    {
      public void onLoadingComplete(String paramAnonymousString, View paramAnonymousView, Bitmap paramAnonymousBitmap)
      {
        paramAnonymousString = Home.this.getCircleBitmap(paramAnonymousBitmap);
        localImageView.setImageBitmap(paramAnonymousString);
      }
    });
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130968611);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setHomeAsUpIndicator(2130837592);
    setMenu();
    setupPager();
    if ((ActivityCompat.checkSelfPermission(this, "android.permission.ACCESS_FINE_LOCATION") != 0) && (ActivityCompat.checkSelfPermission(this, "android.permission.ACCESS_COARSE_LOCATION") != 0)) {
      ActivityCompat.requestPermissions(this, new String[] { "android.permission.ACCESS_FINE_LOCATION", "android.permission.ACCESS_COARSE_LOCATION" }, 0);
    }
    this.actionButton = ((FloatingActionButton)findViewById(2131689713));
    this.actionButton.setOnClickListener(this.addListener);
    setTitle("Vive Más " + "1.0.24");
  }
  
  public void onResume()
  {
    super.onResume();
    Customer.getMyCustomer(this, this.customerHandler);
  }
  
  public void onStart()
  {
    super.onStart();
  }
  
  public void onStop()
  {
    super.onStop();
  }
  
  public void setupPager()
  {
    TabLayout localTabLayout = (TabLayout)findViewById(2131689720);
    localTabLayout.addTab(localTabLayout.newTab().setText("SERVICIOS"));
    localTabLayout.addTab(localTabLayout.newTab().setText("FAMILIARES"));
    localTabLayout.setTabGravity(0);
    final ViewPager localViewPager = (ViewPager)findViewById(2131689721);
    localViewPager.setAdapter(new PagerAdapter(getSupportFragmentManager(), localTabLayout.getTabCount()));
    localViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(localTabLayout));
    localTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener()
    {
      public void onTabReselected(TabLayout.Tab paramAnonymousTab) {}
      
      public void onTabSelected(TabLayout.Tab paramAnonymousTab)
      {
        localViewPager.setCurrentItem(paramAnonymousTab.getPosition());
        switch (paramAnonymousTab.getPosition())
        {
        default: 
          return;
        case 0: 
          Home.this.actionButton.setImageResource(2130837594);
          Home.access$002(Home.this, 0);
          return;
        }
        Home.this.actionButton.setImageResource(2130837588);
        Home.access$002(Home.this, 1);
      }
      
      public void onTabUnselected(TabLayout.Tab paramAnonymousTab) {}
    });
  }
  
  class PagerAdapter
    extends FragmentStatePagerAdapter
  {
    int mNumOfTabs;
    
    public PagerAdapter(FragmentManager paramFragmentManager, int paramInt)
    {
      super();
      this.mNumOfTabs = paramInt;
    }
    
    public int getCount()
    {
      return this.mNumOfTabs;
    }
    
    public Fragment getItem(int paramInt)
    {
      switch (paramInt)
      {
      default: 
        return null;
      case 0: 
        Home.this.historial = new Historial();
        return Home.this.historial;
      }
      Home.this.familiares = new Familiares();
      return Home.this.familiares;
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/vivemas/com/vivemasclientes/Home.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */