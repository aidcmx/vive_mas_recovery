package vivemas.com.vivemasclientes;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.View;
import android.widget.EditText;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.CallbackManager.Factory;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.loopj.android.http.AsyncHttpResponseHandler;
import cz.msebera.android.httpclient.Header;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import org.json.JSONException;
import vivemas.com.vivemasclientes.Model.Customer;

public class Login
  extends AiDC_Activity
{
  String accessToken;
  CallbackManager callbackManager;
  LoginButton loginButton;
  AsyncHttpResponseHandler responseLogin = new AsyncHttpResponseHandler()
  {
    public void onFailure(int paramAnonymousInt, Header[] paramAnonymousArrayOfHeader, byte[] paramAnonymousArrayOfByte, Throwable paramAnonymousThrowable)
    {
      try
      {
        paramAnonymousArrayOfHeader = new String(paramAnonymousArrayOfByte, "UTF-8");
        Login.this.doAlert("error: ", "error " + paramAnonymousInt + " : " + paramAnonymousArrayOfHeader);
        return;
      }
      catch (UnsupportedEncodingException paramAnonymousArrayOfHeader) {}
    }
    
    public void onFinish()
    {
      super.onFinish();
      Login.this.stoppedLoading();
    }
    
    public void onStart()
    {
      super.onStart();
      Login.this.loading(Login.this);
    }
    
    /* Error */
    public void onSuccess(int paramAnonymousInt, Header[] paramAnonymousArrayOfHeader, byte[] paramAnonymousArrayOfByte)
    {
      // Byte code:
      //   0: new 22	java/lang/String
      //   3: dup
      //   4: aload_3
      //   5: ldc 24
      //   7: invokespecial 27	java/lang/String:<init>	([BLjava/lang/String;)V
      //   10: astore_2
      //   11: new 70	org/json/JSONObject
      //   14: dup
      //   15: aload_2
      //   16: invokespecial 73	org/json/JSONObject:<init>	(Ljava/lang/String;)V
      //   19: astore_2
      //   20: invokestatic 79	vivemas/com/vivemasclientes/Model/Model:getInstance	()Lvivemas/com/vivemasclientes/Model/Model;
      //   23: aload_2
      //   24: ldc 81
      //   26: invokevirtual 85	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
      //   29: aload_2
      //   30: ldc 87
      //   32: invokevirtual 85	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
      //   35: invokevirtual 90	vivemas/com/vivemasclientes/Model/Model:loginToApp	(Ljava/lang/String;Ljava/lang/String;)V
      //   38: invokestatic 95	vivemas/com/vivemasclientes/Model/AsyncClient:setToken	()V
      //   41: new 97	android/content/Intent
      //   44: dup
      //   45: aload_0
      //   46: getfield 12	vivemas/com/vivemasclientes/Login$2:this$0	Lvivemas/com/vivemasclientes/Login;
      //   49: ldc 99
      //   51: invokespecial 102	android/content/Intent:<init>	(Landroid/content/Context;Ljava/lang/Class;)V
      //   54: astore_2
      //   55: aload_0
      //   56: getfield 12	vivemas/com/vivemasclientes/Login$2:this$0	Lvivemas/com/vivemasclientes/Login;
      //   59: aload_2
      //   60: invokevirtual 106	vivemas/com/vivemasclientes/Login:startActivity	(Landroid/content/Intent;)V
      //   63: aload_0
      //   64: getfield 12	vivemas/com/vivemasclientes/Login$2:this$0	Lvivemas/com/vivemasclientes/Login;
      //   67: invokevirtual 109	vivemas/com/vivemasclientes/Login:finish	()V
      //   70: return
      //   71: astore_2
      //   72: return
      //   73: astore_2
      //   74: return
      //   75: astore_2
      //   76: return
      //   77: astore_2
      //   78: return
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	79	0	this	2
      //   0	79	1	paramAnonymousInt	int
      //   0	79	2	paramAnonymousArrayOfHeader	Header[]
      //   0	79	3	paramAnonymousArrayOfByte	byte[]
      // Exception table:
      //   from	to	target	type
      //   0	11	71	org/json/JSONException
      //   11	70	73	org/json/JSONException
      //   0	11	75	java/io/UnsupportedEncodingException
      //   11	70	77	java/io/UnsupportedEncodingException
    }
  };
  
  public void login(View paramView)
  {
    paramView = (EditText)findViewById(2131689553);
    EditText localEditText = (EditText)findViewById(2131689729);
    if ((paramView.getText().toString() == "") && (localEditText.getText().toString() == ""))
    {
      doAlert("Error", "Debes ingresar tu e-mail y password");
      return;
    }
    try
    {
      Customer.login(this, paramView.getText().toString(), localEditText.getText().toString(), this.responseLogin);
      return;
    }
    catch (JSONException paramView) {}
  }
  
  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    this.callbackManager.onActivityResult(paramInt1, paramInt2, paramIntent);
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130968612);
    getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    FacebookSdk.sdkInitialize(getApplicationContext());
    this.callbackManager = CallbackManager.Factory.create();
    this.loginButton = ((LoginButton)findViewById(2131689730));
    this.loginButton.setReadPermissions(Arrays.asList(new String[] { "public_profile" }));
    this.loginButton.registerCallback(this.callbackManager, new FacebookCallback()
    {
      public void onCancel() {}
      
      public void onError(FacebookException paramAnonymousFacebookException)
      {
        Login.this.toast("Error al hacer login con Facebook");
      }
      
      public void onSuccess(LoginResult paramAnonymousLoginResult)
      {
        Login.this.accessToken = AccessToken.getCurrentAccessToken().getToken();
        try
        {
          Customer.loginWithFacebook(Login.this, Login.this.accessToken, Login.this.responseLogin);
          return;
        }
        catch (JSONException paramAnonymousLoginResult) {}
      }
    });
  }
  
  public void signup(View paramView)
  {
    startActivity(new Intent(this, Signup.class));
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/vivemas/com/vivemasclientes/Login.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */