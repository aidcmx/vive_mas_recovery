package vivemas.com.vivemasclientes;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import com.crittercism.app.Crittercism;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration.Builder;
import vivemas.com.vivemasclientes.Model.Constants;
import vivemas.com.vivemasclientes.Model.Model;

public class MainActivity
  extends AiDC_Activity
{
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130968613);
    Constants.getInstance(getBaseContext());
    resumeLoading();
    paramBundle = new ImageLoaderConfiguration.Builder(getApplicationContext()).memoryCache(new LruMemoryCache(2097152)).memoryCacheSize(2097152).diskCacheSize(52428800).diskCacheFileCount(100).build();
    ImageLoader.getInstance().init(paramBundle);
    Crittercism.initialize(this, "06c86d5c5aba4305bf7f083e5c1a0a8500555300");
  }
  
  public void resumeLoading()
  {
    new Handler().postDelayed(new Runnable()
    {
      public void run()
      {
        if (Model.getInstance().isUserLoggedIn()) {}
        for (Intent localIntent = new Intent(MainActivity.this, Home.class);; localIntent = new Intent(MainActivity.this, Login.class))
        {
          MainActivity.this.startActivity(localIntent);
          MainActivity.this.finish();
          return;
        }
      }
    }, 2500L);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/vivemas/com/vivemasclientes/MainActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */