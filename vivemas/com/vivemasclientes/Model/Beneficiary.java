package vivemas.com.vivemasclientes.Model;

import android.content.Context;
import com.loopj.android.http.AsyncHttpResponseHandler;
import java.io.File;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Beneficiary
{
  public JSONArray activities;
  public String birthday;
  public String gender;
  public String kinship;
  public JSONObject meta;
  public JSONObject name;
  public String picture;
  public String userId;
  
  public void deleteBeneficiary(Context paramContext, AsyncHttpResponseHandler paramAsyncHttpResponseHandler)
    throws JSONException
  {
    AsyncClient.sendRequest(paramContext, "", paramAsyncHttpResponseHandler, "customers/" + Model.getInstance().getUserId() + "/beneficiaries/" + this.userId, AsyncClient.Method.DELETE);
  }
  
  public void initFromJSON(JSONObject paramJSONObject)
    throws JSONException
  {
    this.userId = paramJSONObject.getString("_id");
    if (paramJSONObject.has("name")) {
      this.name = paramJSONObject.getJSONObject("name");
    }
    if (paramJSONObject.has("kinship")) {
      this.kinship = paramJSONObject.getString("kinship");
    }
    if (paramJSONObject.has("birthday")) {
      this.birthday = paramJSONObject.getString("birthday");
    }
    if (paramJSONObject.has("gender")) {
      this.gender = paramJSONObject.getString("gender");
    }
    if (paramJSONObject.has("meta")) {
      this.meta = paramJSONObject.getJSONObject("meta");
    }
    if (paramJSONObject.has("pic"))
    {
      String str = UUID.randomUUID().toString();
      this.picture = paramJSONObject.getString("pic");
      this.picture = (this.picture + "?" + str);
    }
    if (paramJSONObject.has("activities")) {
      this.activities = paramJSONObject.getJSONArray("activities");
    }
  }
  
  public void saveBeneficiary(Context paramContext, JSONObject paramJSONObject, AsyncHttpResponseHandler paramAsyncHttpResponseHandler)
  {
    String str = "customers/" + Model.getInstance().getUserId() + "/beneficiaries";
    AsyncClient.sendRequest(paramContext, paramJSONObject.toString(), paramAsyncHttpResponseHandler, str, AsyncClient.Method.POST);
  }
  
  public void updateBeneficiary(Context paramContext, JSONObject paramJSONObject, AsyncHttpResponseHandler paramAsyncHttpResponseHandler)
    throws JSONException
  {
    String str = "customers/" + Model.getInstance().getUserId() + "/beneficiaries/" + paramJSONObject.getString("_id");
    AsyncClient.sendRequest(paramContext, paramJSONObject.toString(), paramAsyncHttpResponseHandler, str, AsyncClient.Method.PATCH);
  }
  
  public void uploadPicture(Context paramContext, AsyncHttpResponseHandler paramAsyncHttpResponseHandler, File paramFile)
    throws JSONException
  {
    AsyncClient.sendRequest(paramContext, paramFile, paramAsyncHttpResponseHandler, "customers/" + Model.getInstance().getUserId() + "/beneficiaries/" + this.userId + "/pic", AsyncClient.Method.PUT);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/vivemas/com/vivemasclientes/Model/Beneficiary.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */