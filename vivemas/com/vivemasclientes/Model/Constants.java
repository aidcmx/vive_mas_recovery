package vivemas.com.vivemasclientes.Model;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Constants
{
  public static final String ACTIVITIES = "activitiesJSON";
  public static final String CUSTOMER = "customerJSON";
  public static final int SPLASH_DISPLAY_LENGHT = 2500;
  public static final String TOKEN = "token";
  public static final String USER_ID = "userId";
  public static final String USER_KEY = "userCreated";
  private static Constants instance = null;
  private SharedPreferences prefs = null;
  
  protected Constants(Context paramContext)
  {
    this.prefs = getPrefs(paramContext);
  }
  
  public static Constants getInstance()
  {
    if (instance == null) {
      return null;
    }
    return instance;
  }
  
  public static Constants getInstance(Context paramContext)
  {
    if (instance == null) {
      instance = new Constants(paramContext);
    }
    return instance;
  }
  
  private static SharedPreferences getPrefs(Context paramContext)
  {
    return PreferenceManager.getDefaultSharedPreferences(paramContext);
  }
  
  public SharedPreferences getPreferences()
  {
    return this.prefs;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/vivemas/com/vivemasclientes/Model/Constants.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */