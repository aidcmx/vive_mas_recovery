package vivemas.com.vivemasclientes.Model;

import android.content.Context;
import com.loopj.android.http.AsyncHttpResponseHandler;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Customer
{
  public JSONArray addresses;
  public JSONArray beneficiaries;
  public JSONObject defaultBeneficiary;
  public String email;
  public JSONArray emergencyContacts;
  public JSONObject extraData;
  public String first;
  public String fullName;
  public String last;
  public JSONObject meta;
  public String phone;
  public String picture;
  public String userId;
  
  public static void getMyCustomer(Context paramContext, AsyncHttpResponseHandler paramAsyncHttpResponseHandler)
  {
    AsyncClient.sendRequest(paramContext, "", paramAsyncHttpResponseHandler, "customers/" + Model.getInstance().getUserId(), AsyncClient.Method.GET);
  }
  
  public static Customer initWithJSONObject(JSONObject paramJSONObject)
    throws JSONException
  {
    Customer localCustomer = new Customer();
    Object localObject = paramJSONObject.getJSONObject("name");
    if (((JSONObject)localObject).has("first"))
    {
      localCustomer.fullName = (((JSONObject)localObject).getString("first") + " " + ((JSONObject)localObject).getString("last"));
      localCustomer.first = ((JSONObject)localObject).getString("first");
      localCustomer.last = ((JSONObject)localObject).getString("last");
    }
    localCustomer.beneficiaries = paramJSONObject.getJSONArray("beneficiaries");
    if (paramJSONObject.getJSONArray("beneficiaries").length() > 0) {
      localCustomer.defaultBeneficiary = localCustomer.beneficiaries.getJSONObject(0);
    }
    if (paramJSONObject.has("telephone")) {
      localCustomer.phone = paramJSONObject.getString("telephone");
    }
    if (paramJSONObject.has("email")) {
      localCustomer.email = paramJSONObject.getString("email");
    }
    localCustomer.userId = paramJSONObject.getString("_id");
    localCustomer.extraData = paramJSONObject;
    if (paramJSONObject.has("meta")) {
      localCustomer.meta = paramJSONObject.getJSONObject("meta");
    }
    if (paramJSONObject.has("addresses"))
    {
      localCustomer.addresses = paramJSONObject.getJSONArray("addresses");
      localObject = new JSONArray();
      int i = 0;
      while (i < localCustomer.addresses.length())
      {
        JSONObject localJSONObject = localCustomer.addresses.getJSONObject(i);
        if (!localJSONObject.getBoolean("archived")) {
          ((JSONArray)localObject).put(localJSONObject);
        }
        i += 1;
      }
      localCustomer.addresses = ((JSONArray)localObject);
    }
    if (paramJSONObject.has("pic"))
    {
      localObject = UUID.randomUUID().toString();
      localCustomer.picture = paramJSONObject.getString("pic");
      localCustomer.picture = (localCustomer.picture + "?" + (String)localObject);
    }
    return localCustomer;
  }
  
  public static Customer loadLocal()
    throws JSONException
  {
    return initWithJSONObject(new JSONObject(Model.getInstance().getLocalCustomer()));
  }
  
  public static void login(Context paramContext, String paramString1, String paramString2, AsyncHttpResponseHandler paramAsyncHttpResponseHandler)
    throws JSONException
  {
    paramString1 = paramString1.replace(" ", "");
    JSONObject localJSONObject = new JSONObject();
    localJSONObject.put("email", paramString1);
    localJSONObject.put("password", paramString2);
    AsyncClient.sendRequest(paramContext, localJSONObject.toString(), paramAsyncHttpResponseHandler, "login/customer", AsyncClient.Method.POST);
  }
  
  public static void loginWithFacebook(Context paramContext, String paramString, AsyncHttpResponseHandler paramAsyncHttpResponseHandler)
    throws JSONException
  {
    JSONObject localJSONObject = new JSONObject();
    localJSONObject.put("access_token", paramString);
    AsyncClient.sendRequest(paramContext, localJSONObject.toString(), paramAsyncHttpResponseHandler, "login/customer", AsyncClient.Method.POST);
  }
  
  public static void register(Context paramContext, JSONObject paramJSONObject, AsyncHttpResponseHandler paramAsyncHttpResponseHandler)
  {
    AsyncClient.sendRequest(paramContext, paramJSONObject.toString(), paramAsyncHttpResponseHandler, "customers", AsyncClient.Method.POST);
  }
  
  public void deleteAddress(Context paramContext, String paramString, AsyncHttpResponseHandler paramAsyncHttpResponseHandler)
  {
    AsyncClient.sendRequest(paramContext, "", paramAsyncHttpResponseHandler, "customers/" + Model.getInstance().getUserId() + "/addresses/" + paramString, AsyncClient.Method.DELETE);
  }
  
  public void deletePicture(Context paramContext, AsyncHttpResponseHandler paramAsyncHttpResponseHandler)
  {
    AsyncClient.sendRequest(paramContext, "", paramAsyncHttpResponseHandler, "customers/" + this.userId + "/pic", AsyncClient.Method.DELETE);
  }
  
  public Beneficiary getBeneficiaryWithId(String paramString)
    throws JSONException
  {
    int i = 0;
    while (i < this.beneficiaries.length())
    {
      JSONObject localJSONObject = this.beneficiaries.getJSONObject(i);
      if (localJSONObject.getString("id").equals(paramString))
      {
        paramString = new Beneficiary();
        paramString.initFromJSON(localJSONObject);
        return paramString;
      }
      i += 1;
    }
    return null;
  }
  
  public Beneficiary getDefaultBeneficiary()
    throws JSONException
  {
    JSONObject localJSONObject = this.beneficiaries.getJSONObject(0);
    Beneficiary localBeneficiary = new Beneficiary();
    localBeneficiary.initFromJSON(localJSONObject);
    return localBeneficiary;
  }
  
  public Beneficiary getLastBeneficiary()
    throws JSONException
  {
    JSONObject localJSONObject = this.beneficiaries.getJSONObject(this.beneficiaries.length() - 1);
    Beneficiary localBeneficiary = new Beneficiary();
    localBeneficiary.initFromJSON(localJSONObject);
    return localBeneficiary;
  }
  
  public void saveAddress(Context paramContext, JSONObject paramJSONObject, AsyncHttpResponseHandler paramAsyncHttpResponseHandler, boolean paramBoolean)
    throws JSONException
  {
    Object localObject2 = "customers/" + Model.getInstance().getUserId() + "/addresses";
    Object localObject1 = localObject2;
    if (paramBoolean) {
      localObject1 = (String)localObject2 + "/" + paramJSONObject.getString("id");
    }
    localObject2 = AsyncClient.Method.POST;
    if (paramBoolean) {
      localObject2 = AsyncClient.Method.PATCH;
    }
    AsyncClient.sendRequest(paramContext, paramJSONObject.toString(), paramAsyncHttpResponseHandler, (String)localObject1, (AsyncClient.Method)localObject2);
  }
  
  public void saveLocal()
  {
    String str = this.extraData.toString();
    Model.getInstance().saveLocalCustomer(str);
  }
  
  public void updateData(Context paramContext, AsyncHttpResponseHandler paramAsyncHttpResponseHandler)
    throws JSONException
  {
    Object localObject = this.extraData;
    ((JSONObject)localObject).put("first", this.first);
    ((JSONObject)localObject).put("last", this.last);
    JSONObject localJSONObject = new JSONObject();
    localJSONObject.put("name", localObject);
    localJSONObject.put("email", this.email);
    localJSONObject.put("telephone", this.phone);
    localObject = "customers/" + this.userId;
    AsyncClient.sendRequest(paramContext, localJSONObject.toString(), paramAsyncHttpResponseHandler, (String)localObject, AsyncClient.Method.PATCH);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/vivemas/com/vivemasclientes/Model/Customer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */