package vivemas.com.vivemasclientes.Model;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.loopj.android.http.AsyncHttpResponseHandler;
import java.io.File;
import org.json.JSONArray;
import org.json.JSONException;

public class Model
{
  private static Model instance = null;
  public Beneficiary activeBeneficiary = null;
  public Service activeService = new Service();
  public SharedPreferences.Editor e = null;
  public JSONArray emergencyContacts = null;
  public SharedPreferences prefs = null;
  public JSONArray serviceCategories = null;
  
  public static Model getInstance()
  {
    if (instance == null) {
      instance = new Model();
    }
    return instance;
  }
  
  public String getAuthToken()
  {
    return this.prefs.getString("token", "");
  }
  
  public void getCategories(Context paramContext, AsyncHttpResponseHandler paramAsyncHttpResponseHandler)
  {
    AsyncClient.sendRequest(paramContext, "", paramAsyncHttpResponseHandler, "categories", AsyncClient.Method.GET);
  }
  
  public String getLocalActivities()
  {
    return this.prefs.getString("activitiesJSON", "[]");
  }
  
  public String getLocalCustomer()
  {
    return this.prefs.getString("customerJSON", null);
  }
  
  public void getProducts(Context paramContext, AsyncHttpResponseHandler paramAsyncHttpResponseHandler)
  {
    AsyncClient.sendRequest(paramContext, "", paramAsyncHttpResponseHandler, "activities", AsyncClient.Method.GET);
  }
  
  public String getUserId()
  {
    return this.prefs.getString("userId", null);
  }
  
  public boolean isUserLoggedIn()
  {
    boolean bool = false;
    if (Boolean.valueOf(this.prefs.getBoolean("userCreated", false)).booleanValue()) {
      bool = true;
    }
    return bool;
  }
  
  public void loginToApp(String paramString1, String paramString2)
  {
    this.e.putString("userId", paramString1);
    this.e.putString("token", paramString2);
    this.e.putBoolean("userCreated", true);
    this.e.commit();
  }
  
  public void logout()
  {
    this.e.clear();
    this.e.commit();
  }
  
  public void saveLocalActivities(String paramString)
  {
    this.e.putString("activitiesJSON", paramString);
    this.e.commit();
  }
  
  public void saveLocalCustomer(String paramString)
  {
    this.e.putString("customerJSON", paramString);
    this.e.commit();
  }
  
  public void uploadPictureRegistration(Context paramContext, AsyncHttpResponseHandler paramAsyncHttpResponseHandler, File paramFile, String paramString)
    throws JSONException
  {
    AsyncClient.sendRequest(paramContext, paramFile, paramAsyncHttpResponseHandler, "customers/" + paramString + "/pic", AsyncClient.Method.PUT);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/vivemas/com/vivemasclientes/Model/Model.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */