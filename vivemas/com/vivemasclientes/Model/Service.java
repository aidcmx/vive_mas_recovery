package vivemas.com.vivemasclientes.Model;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;

public class Service
{
  public static final String EIGHT = "eight";
  public static final String TWELVE = "twelve";
  public static final String TWENTYFOUR = "twentyfour";
  public JSONObject address;
  public String defaultLength;
  public Integer defaultStartTime;
  public List<CalendarDay> selectedDates;
  public List<String> serviceLength;
  public List<Integer> startTime;
  
  public String getIndexLength(int paramInt)
  {
    return (String)this.serviceLength.get(paramInt);
  }
  
  public Integer getIndexTime(int paramInt)
  {
    return (Integer)this.startTime.get(paramInt);
  }
  
  public CalendarDay getSelectedDay(int paramInt)
  {
    return (CalendarDay)this.selectedDates.get(paramInt);
  }
  
  public void setIndexLength(int paramInt, String paramString)
  {
    this.serviceLength.set(paramInt, paramString);
  }
  
  public void setIndexTime(int paramInt, Integer paramInteger)
  {
    this.startTime.set(paramInt, paramInteger);
  }
  
  public void setServiceLength(String paramString)
  {
    if (this.selectedDates == null) {}
    for (;;)
    {
      return;
      this.serviceLength = new ArrayList();
      int i = 0;
      while (i < this.selectedDates.size())
      {
        this.serviceLength.add(paramString);
        i += 1;
      }
    }
  }
  
  public void setServiceTime(Integer paramInteger)
  {
    this.startTime = new ArrayList();
    if (this.selectedDates == null) {}
    for (;;)
    {
      return;
      int i = 0;
      while (i < this.selectedDates.size())
      {
        this.startTime.add(paramInteger);
        i += 1;
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/vivemas/com/vivemasclientes/Model/Service.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */