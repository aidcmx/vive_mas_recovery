package vivemas.com.vivemasclientes.NewService;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.TextView;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import vivemas.com.vivemasclientes.Model.Model;
import vivemas.com.vivemasclientes.Model.Service;

public class EditDayCell
  extends FrameLayout
{
  AdapterView.OnItemSelectedListener lengthListener = new EditDayCell.2(this);
  private TextView mDateView;
  private int mHours = 8;
  private int mIndex;
  private ArrayAdapter<CharSequence> mLengthAdapter;
  private Spinner mSpinnerLength;
  private Spinner mSpinnerTime;
  private ArrayAdapter<CharSequence> mTimeAdapter;
  AdapterView.OnItemSelectedListener timeListener = new EditDayCell.1(this);
  
  public EditDayCell(Context paramContext)
  {
    super(paramContext);
    setupView();
  }
  
  public EditDayCell(Context paramContext, int paramInt)
  {
    super(paramContext);
    this.mIndex = paramInt;
    setupView();
  }
  
  public EditDayCell(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    setupView();
  }
  
  public void reloadSpinners()
  {
    if (this.mHours == 8) {
      this.mTimeAdapter = ArrayAdapter.createFromResource(getContext(), 2131623946, 2130968695);
    }
    if (this.mHours == 12) {
      this.mTimeAdapter = ArrayAdapter.createFromResource(getContext(), 2131623940, 2130968695);
    }
    if (this.mHours == 24) {
      this.mTimeAdapter = ArrayAdapter.createFromResource(getContext(), 2131623943, 2130968695);
    }
    this.mSpinnerTime.setAdapter(this.mTimeAdapter);
  }
  
  public void setupSpinners()
  {
    String str = Model.getInstance().activeService.getIndexLength(this.mIndex);
    Model.getInstance().activeService.getIndexTime(this.mIndex);
    if (str.equals("eight")) {
      this.mHours = 8;
    }
    if (str.equals("twelve")) {
      this.mHours = 12;
    }
    if (str.equals("twentyfour")) {
      this.mHours = 24;
    }
    if (this.mHours == 8) {
      this.mTimeAdapter = ArrayAdapter.createFromResource(getContext(), 2131623946, 2130968695);
    }
    if (this.mHours == 12) {
      this.mTimeAdapter = ArrayAdapter.createFromResource(getContext(), 2131623940, 2130968695);
    }
    if (this.mHours == 24) {
      this.mTimeAdapter = ArrayAdapter.createFromResource(getContext(), 2131623943, 2130968695);
    }
    this.mLengthAdapter = ArrayAdapter.createFromResource(getContext(), 2131623948, 2130968695);
    this.mSpinnerTime.setAdapter(this.mTimeAdapter);
    this.mSpinnerLength.setAdapter(this.mLengthAdapter);
    this.mSpinnerTime.setOnItemSelectedListener(this.timeListener);
    this.mSpinnerLength.setOnItemSelectedListener(this.lengthListener);
    if (str.equals("eight")) {
      this.mSpinnerLength.setSelection(0);
    }
    if (str.equals("twelve")) {
      this.mSpinnerLength.setSelection(1);
    }
    if (str.equals("twentyfour")) {
      this.mSpinnerLength.setSelection(2);
    }
  }
  
  public void setupView()
  {
    ((LayoutInflater)getContext().getSystemService("layout_inflater")).inflate(2130968637, this);
    this.mSpinnerTime = ((Spinner)findViewById(2131689766));
    this.mSpinnerLength = ((Spinner)findViewById(2131689833));
    this.mDateView = ((TextView)findViewById(2131689832));
    CalendarDay localCalendarDay = Model.getInstance().activeService.getSelectedDay(this.mIndex);
    int i = localCalendarDay.getDay();
    int j = localCalendarDay.getMonth();
    int k = localCalendarDay.getYear();
    this.mDateView.setText(i + "/" + j + "/" + k);
    setupSpinners();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/vivemas/com/vivemasclientes/NewService/EditDayCell.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */