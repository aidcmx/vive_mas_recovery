package vivemas.com.vivemasclientes.NewService;

import android.os.Bundle;
import android.widget.LinearLayout;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import java.util.List;
import vivemas.com.vivemasclientes.AiDC_Activity;
import vivemas.com.vivemasclientes.Model.Model;
import vivemas.com.vivemasclientes.Model.Service;

public class EditDays
  extends AiDC_Activity
{
  private List<CalendarDay> days;
  private LinearLayout mContainer;
  
  public void initCalendar()
  {
    int i = 0;
    while (i < this.days.size())
    {
      EditDayCell localEditDayCell = new EditDayCell(this, i);
      this.mContainer.addView(localEditDayCell);
      i += 1;
    }
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130968608);
    setTitle("Editar días");
    this.mContainer = ((LinearLayout)findViewById(2131689715));
    this.days = Model.getInstance().activeService.selectedDates;
    initCalendar();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/vivemas/com/vivemasclientes/NewService/EditDays.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */