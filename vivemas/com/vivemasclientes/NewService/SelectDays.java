package vivemas.com.vivemasclientes.NewService;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView.State;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView.StateBuilder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import vivemas.com.vivemasclientes.AiDC_Activity;
import vivemas.com.vivemasclientes.Model.Model;
import vivemas.com.vivemasclientes.Model.Service;

public class SelectDays
  extends AiDC_Activity
{
  private MaterialCalendarView mCalendar;
  private CheckBox mChecked;
  private Date mMaxDate;
  private Date mMinDate;
  private Spinner mSpinner;
  private ArrayAdapter<CharSequence> mSpinnerAdapter;
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130968617);
    setTitle("Seleccionar dias");
    this.mCalendar = ((MaterialCalendarView)findViewById(2131689746));
    this.mCalendar.setSelectionMode(2);
    this.mCalendar.setDateTextAppearance(2131362016);
    paramBundle = new GregorianCalendar();
    paramBundle.add(5, 30);
    this.mMaxDate = paramBundle.getTime();
    paramBundle = new GregorianCalendar();
    paramBundle.add(5, 1);
    this.mMinDate = paramBundle.getTime();
    this.mCalendar.state().edit().setMinimumDate(CalendarDay.from(this.mMinDate)).setMaximumDate(CalendarDay.from(this.mMaxDate)).commit();
    paramBundle = Model.getInstance().activeService.selectedDates;
    TextView localTextView = (TextView)findViewById(2131689747);
    this.mChecked = ((CheckBox)findViewById(2131689745));
    this.mChecked.setTextColor(localTextView.getTextColors());
    this.mChecked.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
    {
      public void onCheckedChanged(CompoundButton paramAnonymousCompoundButton, boolean paramAnonymousBoolean)
      {
        SelectDays.this.selectMonth(paramAnonymousBoolean);
      }
    });
    if (paramBundle != null)
    {
      int i = 0;
      while (i < paramBundle.size())
      {
        this.mCalendar.setDateSelected((CalendarDay)paramBundle.get(i), true);
        i += 1;
      }
      if (paramBundle.size() == 30) {
        this.mChecked.setChecked(true);
      }
    }
  }
  
  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    getMenuInflater().inflate(2131755010, paramMenu);
    return true;
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    Model.getInstance().activeService.selectedDates = this.mCalendar.getSelectedDates();
    Model.getInstance().activeService.serviceLength = new ArrayList();
    Model.getInstance().activeService.startTime = new ArrayList();
    paramMenuItem = Model.getInstance().activeService;
    int i = 0;
    while (i < this.mCalendar.getSelectedDates().size())
    {
      paramMenuItem.serviceLength.add(paramMenuItem.defaultLength);
      paramMenuItem.startTime.add(paramMenuItem.defaultStartTime);
      i += 1;
    }
    if (this.mCalendar.getSelectedDates().size() == 0)
    {
      doAlert("Vive+", "Necesitas escoger por lo menos 1 día");
      return true;
    }
    finish();
    return true;
  }
  
  public void selectMonth(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      this.mCalendar.selectRange(CalendarDay.from(this.mMinDate), CalendarDay.from(this.mMaxDate));
      return;
    }
    this.mCalendar.clearSelection();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/vivemas/com/vivemasclientes/NewService/SelectDays.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */