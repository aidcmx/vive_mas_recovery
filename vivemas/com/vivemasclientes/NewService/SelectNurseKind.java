package vivemas.com.vivemasclientes.NewService;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import org.json.JSONArray;
import org.json.JSONException;
import vivemas.com.vivemasclientes.Adapters.NurseKindAdapter;

public class SelectNurseKind
  extends AppCompatActivity
{
  NurseKindAdapter adapter;
  JSONArray categories;
  ListView kindList;
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130968619);
    setTitle("Tipo de asociada");
    this.kindList = ((ListView)findViewById(2131689749));
    try
    {
      this.categories = new JSONArray("['1','2','3','4']");
      this.adapter = new NurseKindAdapter(this, this.categories);
      this.kindList.setAdapter(this.adapter);
      this.kindList.setOnItemClickListener(new AdapterView.OnItemClickListener()
      {
        public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
        {
          paramAnonymousAdapterView = new Intent(SelectNurseKind.this, SelectNurse.class);
          SelectNurseKind.this.startActivity(paramAnonymousAdapterView);
        }
      });
      return;
    }
    catch (JSONException paramBundle) {}
  }
  
  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    getMenuInflater().inflate(2131755010, paramMenu);
    return true;
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    startActivity(new Intent(this, SelectNurse.class));
    return true;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/vivemas/com/vivemasclientes/NewService/SelectNurseKind.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */