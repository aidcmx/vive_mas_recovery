package vivemas.com.vivemasclientes.NewService;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import java.io.PrintStream;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vivemas.com.vivemasclientes.Adapters.FamilyAdapter;
import vivemas.com.vivemasclientes.AiDC_Activity;
import vivemas.com.vivemasclientes.CreateFamily;
import vivemas.com.vivemasclientes.Model.Customer;
import vivemas.com.vivemasclientes.Model.Model;
import vivemas.com.vivemasclientes.Model.Service;

public class SelectUser
  extends AiDC_Activity
{
  FamilyAdapter adapter;
  public JSONArray dataArray;
  ListView listView;
  RelativeLayout modal;
  int selectedPosition = 0;
  
  public void cancel(View paramView)
  {
    this.modal.setVisibility(8);
  }
  
  public void newFamily(View paramView)
  {
    startActivity(new Intent(this, CreateFamily.class));
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130968620);
    setTitle("¿Para quién?");
    Model.getInstance().activeService = new Service();
    this.modal = ((RelativeLayout)findViewById(2131689755));
    this.modal.setVisibility(0);
  }
  
  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    getMenuInflater().inflate(2131755010, paramMenu);
    return true;
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    startActivity(new Intent(this, ServiceDetails.class));
    return true;
  }
  
  public void onResume()
  {
    super.onResume();
    try
    {
      updateTable(Customer.loadLocal().beneficiaries);
      return;
    }
    catch (JSONException localJSONException)
    {
      localJSONException.printStackTrace();
    }
  }
  
  public void reload(View paramView)
  {
    this.modal.setVisibility(0);
  }
  
  public void selectUser(View paramView)
  {
    try
    {
      paramView = new Intent(this, CreateFamily.class);
      paramView.putExtra("familyData", this.dataArray.getJSONObject(this.selectedPosition).toString());
      startActivity(paramView);
      Model.getInstance().serviceCategories = new JSONArray();
      return;
    }
    catch (JSONException paramView)
    {
      for (;;)
      {
        paramView.printStackTrace();
      }
    }
  }
  
  public void setupForFamily(JSONObject paramJSONObject)
    throws JSONException
  {
    System.out.println(paramJSONObject);
    this.modal.setVisibility(8);
    TextView localTextView1 = (TextView)findViewById(2131689754);
    TextView localTextView2 = (TextView)findViewById(2131689692);
    if (paramJSONObject.has("kinship")) {
      localTextView2.setText(paramJSONObject.getString("kinship"));
    }
    for (;;)
    {
      localTextView1.setText(paramJSONObject.getJSONObject("name").getString("first") + " " + paramJSONObject.getJSONObject("name").getString("last"));
      if (paramJSONObject.has("pic"))
      {
        paramJSONObject = paramJSONObject.getString("pic");
        ImageLoader.getInstance().loadImage(paramJSONObject, new SimpleImageLoadingListener()
        {
          public void onLoadingComplete(String paramAnonymousString, View paramAnonymousView, Bitmap paramAnonymousBitmap)
          {
            ((ImageView)SelectUser.this.findViewById(2131689752)).setImageBitmap(SelectUser.this.getCircleBitmap(paramAnonymousBitmap));
          }
        });
      }
      return;
      localTextView2.setText("Para mi");
    }
  }
  
  public void updateTable(JSONArray paramJSONArray)
  {
    this.dataArray = new JSONArray();
    int i = 0;
    for (;;)
    {
      if (i < paramJSONArray.length()) {}
      try
      {
        JSONObject localJSONObject = paramJSONArray.getJSONObject(i);
        if (!localJSONObject.getBoolean("archived")) {
          this.dataArray.put(localJSONObject);
        }
        i += 1;
        continue;
        this.listView = ((ListView)findViewById(2131689760));
        this.adapter = new FamilyAdapter(this, this.dataArray);
        this.listView.setAdapter(this.adapter);
        registerForContextMenu(this.listView);
        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
          public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
          {
            try
            {
              paramAnonymousAdapterView = SelectUser.this.dataArray.getJSONObject(paramAnonymousInt);
              SelectUser.this.selectedPosition = paramAnonymousInt;
              SelectUser.this.setupForFamily(paramAnonymousAdapterView);
              return;
            }
            catch (JSONException paramAnonymousAdapterView)
            {
              paramAnonymousAdapterView.printStackTrace();
            }
          }
        });
        return;
      }
      catch (JSONException localJSONException)
      {
        for (;;) {}
      }
    }
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/vivemas/com/vivemasclientes/NewService/SelectUser.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */