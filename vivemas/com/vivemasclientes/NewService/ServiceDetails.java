package vivemas.com.vivemasclientes.NewService;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;
import vivemas.com.vivemasclientes.AiDC_Activity;
import vivemas.com.vivemasclientes.Direcciones;
import vivemas.com.vivemasclientes.Model.Model;
import vivemas.com.vivemasclientes.Model.Service;

public class ServiceDetails
  extends AiDC_Activity
{
  private Button mAddressButton;
  private Button mDayButton;
  private ImageButton mEightButton;
  private int mHours = 8;
  private boolean mRecurrent = false;
  private ImageButton mRecurrentButton;
  private Spinner mSpinner;
  private ArrayAdapter<CharSequence> mSpinnerAdapter;
  private ImageButton mTwelveButton;
  private ImageButton mTwentyfourButton;
  AdapterView.OnItemSelectedListener spinnerListener = new AdapterView.OnItemSelectedListener()
  {
    public void onItemSelected(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
    {
      paramAnonymousAdapterView = null;
      if (ServiceDetails.this.mHours == 8) {
        paramAnonymousAdapterView = ServiceDetails.this.getResources().getStringArray(2131623947);
      }
      if (ServiceDetails.this.mHours == 12) {
        paramAnonymousAdapterView = ServiceDetails.this.getResources().getStringArray(2131623941);
      }
      if (ServiceDetails.this.mHours == 24) {
        paramAnonymousAdapterView = ServiceDetails.this.getResources().getStringArray(2131623944);
      }
      paramAnonymousAdapterView = Integer.valueOf(Integer.parseInt(paramAnonymousAdapterView[paramAnonymousInt]));
      Model.getInstance().activeService.defaultStartTime = paramAnonymousAdapterView;
      Model.getInstance().activeService.setServiceTime(paramAnonymousAdapterView);
    }
    
    public void onNothingSelected(AdapterView<?> paramAnonymousAdapterView) {}
  };
  
  public void editDays(View paramView)
  {
    startActivity(new Intent(this, EditDays.class));
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130968621);
    setTitle("Detalles de servicio");
    this.mEightButton = ((ImageButton)findViewById(2131689763));
    this.mTwelveButton = ((ImageButton)findViewById(2131689764));
    this.mTwentyfourButton = ((ImageButton)findViewById(2131689765));
    this.mRecurrentButton = ((ImageButton)findViewById(2131689768));
    this.mAddressButton = ((Button)findViewById(2131689767));
    this.mDayButton = ((Button)findViewById(2131689762));
    this.mEightButton.setTag(Integer.valueOf(1));
    this.mTwelveButton.setTag(Integer.valueOf(2));
    this.mTwentyfourButton.setTag(Integer.valueOf(3));
    Model.getInstance().activeService.defaultLength = "eight";
    Model.getInstance().activeService.defaultStartTime = Integer.valueOf(7);
    setupSpinners();
  }
  
  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    getMenuInflater().inflate(2131755010, paramMenu);
    return true;
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    startActivity(new Intent(this, SelectNurseKind.class));
    return true;
  }
  
  protected void onResume()
  {
    super.onResume();
    Object localObject = Model.getInstance().activeService.address;
    if (localObject != null) {}
    try
    {
      localObject = ((JSONObject)localObject).getJSONObject("location").getJSONObject("address");
      String str = ((JSONObject)localObject).getString("streetName") + " " + ((JSONObject)localObject).getString("streetNumber");
      this.mAddressButton.setText(str);
      localObject = ((JSONObject)localObject).getString("streetName") + " " + ((JSONObject)localObject).getString("streetNumber");
      this.mAddressButton.setText((CharSequence)localObject);
      localObject = Model.getInstance().activeService.selectedDates;
      if (localObject != null) {
        this.mDayButton.setText("Seleccionaste " + ((List)localObject).size() + " días.");
      }
      return;
    }
    catch (JSONException localJSONException)
    {
      for (;;) {}
    }
  }
  
  public void recurrent(View paramView)
  {
    if (!this.mRecurrent) {}
    for (boolean bool = true;; bool = false)
    {
      this.mRecurrent = bool;
      if (!this.mRecurrent) {
        break;
      }
      this.mRecurrentButton.setImageResource(2130837978);
      return;
    }
    this.mRecurrentButton.setImageResource(2130837977);
  }
  
  public void selectAddress(View paramView)
  {
    paramView = new Intent(this, Direcciones.class);
    paramView.putExtra("isSelecting", true);
    startActivity(paramView);
  }
  
  public void selectDay(View paramView)
  {
    startActivity(new Intent(this, SelectDays.class));
  }
  
  public void selectTime(View paramView)
  {
    paramView = (ImageButton)paramView;
    this.mEightButton.setImageResource(2130837977);
    this.mTwelveButton.setImageResource(2130837977);
    this.mTwentyfourButton.setImageResource(2130837977);
    if (paramView.getTag() == Integer.valueOf(1))
    {
      this.mHours = 8;
      this.mEightButton.setImageResource(2130837978);
    }
    if (paramView.getTag() == Integer.valueOf(2))
    {
      this.mHours = 12;
      this.mTwelveButton.setImageResource(2130837978);
    }
    if (paramView.getTag() == Integer.valueOf(3))
    {
      this.mHours = 24;
      this.mTwentyfourButton.setImageResource(2130837978);
    }
    setupSpinners();
  }
  
  public void setupSpinners()
  {
    this.mSpinner = ((Spinner)findViewById(2131689766));
    if (this.mHours == 8)
    {
      this.mSpinnerAdapter = ArrayAdapter.createFromResource(this, 2131623945, 2130968695);
      Model.getInstance().activeService.defaultLength = "eight";
      Model.getInstance().activeService.setServiceLength("eight");
    }
    if (this.mHours == 12)
    {
      this.mSpinnerAdapter = ArrayAdapter.createFromResource(this, 2131623939, 2130968695);
      Model.getInstance().activeService.defaultLength = "twelve";
      Model.getInstance().activeService.setServiceLength("twelve");
    }
    if (this.mHours == 24)
    {
      this.mSpinnerAdapter = ArrayAdapter.createFromResource(this, 2131623942, 2130968695);
      Model.getInstance().activeService.defaultLength = "twentyfour";
      Model.getInstance().activeService.setServiceLength("twentyfour");
    }
    this.mSpinner.setAdapter(this.mSpinnerAdapter);
    this.mSpinner.setOnItemSelectedListener(this.spinnerListener);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/vivemas/com/vivemasclientes/NewService/ServiceDetails.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */