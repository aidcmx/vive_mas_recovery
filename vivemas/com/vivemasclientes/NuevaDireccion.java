package vivemas.com.vivemasclientes;

import android.app.FragmentManager;
import android.content.Intent;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraIdleListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CameraPosition.Builder;
import com.google.android.gms.maps.model.LatLng;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.seatgeek.placesautocomplete.OnPlaceSelectedListener;
import com.seatgeek.placesautocomplete.PlacesAutocompleteTextView;
import com.seatgeek.placesautocomplete.model.AutocompleteResultType;
import com.seatgeek.placesautocomplete.model.Place;
import cz.msebera.android.httpclient.Header;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vivemas.com.vivemasclientes.Model.Customer;

public class NuevaDireccion
  extends AiDC_Activity
  implements OnMapReadyCallback
{
  AsyncHttpResponseHandler addResponse = new AsyncHttpResponseHandler()
  {
    public void onFailure(int paramAnonymousInt, Header[] paramAnonymousArrayOfHeader, byte[] paramAnonymousArrayOfByte, Throwable paramAnonymousThrowable)
    {
      try
      {
        paramAnonymousArrayOfHeader = new String(paramAnonymousArrayOfByte, "UTF-8");
        NuevaDireccion.this.doAlert("error: ", "error " + paramAnonymousInt + " : " + paramAnonymousArrayOfHeader);
        return;
      }
      catch (UnsupportedEncodingException paramAnonymousArrayOfHeader) {}
    }
    
    public void onFinish()
    {
      super.onFinish();
      NuevaDireccion.this.stoppedLoading();
    }
    
    public void onStart()
    {
      super.onStart();
      NuevaDireccion.this.loading(NuevaDireccion.this);
    }
    
    public void onSuccess(int paramAnonymousInt, Header[] paramAnonymousArrayOfHeader, byte[] paramAnonymousArrayOfByte)
    {
      try
      {
        Customer.initWithJSONObject(new JSONObject(new String(paramAnonymousArrayOfByte, "UTF-8"))).saveLocal();
        NuevaDireccion.this.finish();
        return;
      }
      catch (JSONException paramAnonymousArrayOfHeader)
      {
        for (;;) {}
      }
      catch (UnsupportedEncodingException paramAnonymousArrayOfHeader)
      {
        for (;;) {}
      }
    }
  };
  Address address;
  JSONObject addressData;
  GoogleMap googleMap;
  GoogleMap.OnCameraIdleListener idleListener = new GoogleMap.OnCameraIdleListener()
  {
    public void onCameraIdle()
    {
      Object localObject1 = NuevaDireccion.this.googleMap.getCameraPosition().target;
      Object localObject2 = new Geocoder(NuevaDireccion.this);
      try
      {
        localObject1 = ((Geocoder)localObject2).getFromLocation(((LatLng)localObject1).latitude, ((LatLng)localObject1).longitude, 1);
        localObject2 = NuevaDireccion.this;
        if (((List)localObject1).isEmpty()) {}
        for (localObject1 = null;; localObject1 = (Address)((List)localObject1).get(0))
        {
          ((NuevaDireccion)localObject2).address = ((Address)localObject1);
          new StringBuilder().append(NuevaDireccion.this.address.getAddressLine(0)).append(", ").append(NuevaDireccion.this.address.getAddressLine(1)).toString();
          NuevaDireccion.this.isMapReady = true;
          return;
        }
        return;
      }
      catch (IOException localIOException)
      {
        localIOException.printStackTrace();
      }
    }
  };
  boolean isEditing = false;
  boolean isMapReady = false;
  double lat = 0.0D;
  LocationListener locationChangeListener = new LocationListener()
  {
    public void onLocationChanged(Location paramAnonymousLocation)
    {
      if ((paramAnonymousLocation != null) && (!NuevaDireccion.this.isEditing) && (!NuevaDireccion.this.submittedAddress))
      {
        NuevaDireccion.this.googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(paramAnonymousLocation.getLatitude(), paramAnonymousLocation.getLongitude()), 13.0F));
        paramAnonymousLocation = new CameraPosition.Builder().target(new LatLng(paramAnonymousLocation.getLatitude(), paramAnonymousLocation.getLongitude())).zoom(17.0F).build();
        NuevaDireccion.this.googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(paramAnonymousLocation));
        NuevaDireccion.this.googleMap.setOnCameraIdleListener(NuevaDireccion.this.idleListener);
      }
    }
    
    public void onProviderDisabled(String paramAnonymousString) {}
    
    public void onProviderEnabled(String paramAnonymousString) {}
    
    public void onStatusChanged(String paramAnonymousString, int paramAnonymousInt, Bundle paramAnonymousBundle) {}
  };
  LocationManager locationManager;
  double lon = 0.0D;
  Place place;
  OnPlaceSelectedListener placeSelectedListener = new OnPlaceSelectedListener()
  {
    public void onPlaceSelected(@NonNull Place paramAnonymousPlace)
    {
      NuevaDireccion.this.place = paramAnonymousPlace;
      paramAnonymousPlace = new Geocoder(NuevaDireccion.this);
      try
      {
        paramAnonymousPlace = paramAnonymousPlace.getFromLocationName(NuevaDireccion.this.place.description, 1);
        EditText localEditText1 = (EditText)NuevaDireccion.this.findViewById(2131689736);
        EditText localEditText2 = (EditText)NuevaDireccion.this.findViewById(2131689737);
        EditText localEditText3 = (EditText)NuevaDireccion.this.findViewById(2131689738);
        EditText localEditText4 = (EditText)NuevaDireccion.this.findViewById(2131689739);
        if (paramAnonymousPlace.size() > 0)
        {
          NuevaDireccion.this.address = ((Address)paramAnonymousPlace.get(0));
          if (NuevaDireccion.this.address.getThoroughfare() != null) {
            localEditText1.setText(NuevaDireccion.this.address.getThoroughfare());
          }
          if (NuevaDireccion.this.address.getSubThoroughfare() != null) {
            localEditText2.setText(NuevaDireccion.this.address.getSubThoroughfare());
          }
          localEditText3.setText(NuevaDireccion.this.address.getSubLocality());
          localEditText4.setText(NuevaDireccion.this.address.getPostalCode());
          NuevaDireccion.this.shouldSearch = false;
          NuevaDireccion.this.googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(NuevaDireccion.this.address.getLatitude(), NuevaDireccion.this.address.getLongitude()), 13.0F));
          paramAnonymousPlace = new CameraPosition.Builder().target(new LatLng(NuevaDireccion.this.address.getLatitude(), NuevaDireccion.this.address.getLongitude())).zoom(17.0F).build();
          NuevaDireccion.this.googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(paramAnonymousPlace));
          NuevaDireccion.this.googleMap.setOnCameraIdleListener(NuevaDireccion.this.idleListener);
          NuevaDireccion.this.hideKeyboard();
        }
        return;
      }
      catch (IOException paramAnonymousPlace) {}
    }
  };
  boolean shouldSearch = true;
  boolean submittedAddress = false;
  
  public void delete(View paramView)
  {
    try
    {
      paramView = this.addressData.getString("id");
      Customer.loadLocal().deleteAddress(this, paramView, this.addResponse);
      return;
    }
    catch (JSONException paramView) {}
  }
  
  public void loadData()
    throws JSONException
  {
    EditText localEditText1 = (EditText)findViewById(2131689697);
    EditText localEditText2 = (EditText)findViewById(2131689734);
    if (this.addressData.has("nickname")) {
      localEditText1.setText(this.addressData.getString("nickname"));
    }
    if (this.addressData.getJSONObject("location").has("reference")) {
      localEditText2.setText(this.addressData.getJSONObject("location").getString("reference"));
    }
    this.lat = this.addressData.getJSONObject("location").getJSONObject("geo").getJSONArray("coordinates").getDouble(0);
    this.lon = this.addressData.getJSONObject("location").getJSONObject("geo").getJSONArray("coordinates").getDouble(1);
  }
  
  public void onBackPressed()
  {
    FrameLayout localFrameLayout = (FrameLayout)findViewById(2131689740);
    if (this.submittedAddress)
    {
      if (!this.submittedAddress) {}
      for (boolean bool = true;; bool = false)
      {
        this.submittedAddress = bool;
        localFrameLayout.setVisibility(8);
        return;
      }
    }
    finish();
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130968614);
    ((MapFragment)getFragmentManager().findFragmentById(2131689741)).getMapAsync(this);
    paramBundle = (PlacesAutocompleteTextView)findViewById(2131689733);
    paramBundle.setLocationBiasEnabled(true);
    paramBundle.setResultType(AutocompleteResultType.GEOCODE);
    paramBundle.setOnPlaceSelectedListener(this.placeSelectedListener);
    setTitle("AGREGAR DIRECCIÓN");
    paramBundle = getIntent().getExtras();
    if (paramBundle != null)
    {
      this.isEditing = true;
      paramBundle = paramBundle.getString("addressData");
      ((Button)findViewById(2131689628)).setVisibility(0);
    }
    try
    {
      this.addressData = new JSONObject(paramBundle);
      loadData();
      if ((ActivityCompat.checkSelfPermission(this, "android.permission.ACCESS_FINE_LOCATION") != 0) && (ActivityCompat.checkSelfPermission(this, "android.permission.ACCESS_COARSE_LOCATION") != 0))
      {
        toast("No tenemos permisos para usar tu ubicación");
        return;
      }
      this.locationManager = ((LocationManager)getSystemService("location"));
      this.locationManager.requestLocationUpdates("network", 0L, 0.0F, this.locationChangeListener);
      return;
    }
    catch (JSONException paramBundle)
    {
      for (;;) {}
    }
  }
  
  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    getMenuInflater().inflate(2131755011, paramMenu);
    return true;
  }
  
  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if ((Integer.parseInt(Build.VERSION.SDK) > 5) && (paramInt == 4) && (paramKeyEvent.getRepeatCount() == 0))
    {
      Log.d("CDA", "onKeyDown Called");
      onBackPressed();
      return true;
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }
  
  public void onMapReady(GoogleMap paramGoogleMap)
  {
    this.googleMap = paramGoogleMap;
    paramGoogleMap = new Criteria();
    if ((ActivityCompat.checkSelfPermission(this, "android.permission.ACCESS_FINE_LOCATION") != 0) && (ActivityCompat.checkSelfPermission(this, "android.permission.ACCESS_COARSE_LOCATION") != 0)) {
      toast("No tenemos permisos para usar tu ubicación");
    }
    do
    {
      return;
      paramGoogleMap = this.locationManager.getLastKnownLocation(this.locationManager.getBestProvider(paramGoogleMap, true));
      if (this.isEditing)
      {
        paramGoogleMap = new Location("");
        paramGoogleMap.setLatitude(this.lat);
        paramGoogleMap.setLongitude(this.lon);
      }
    } while (paramGoogleMap == null);
    this.googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(paramGoogleMap.getLatitude(), paramGoogleMap.getLongitude()), 13.0F));
    paramGoogleMap = new CameraPosition.Builder().target(new LatLng(paramGoogleMap.getLatitude(), paramGoogleMap.getLongitude())).zoom(17.0F).build();
    this.googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(paramGoogleMap));
    this.googleMap.setOnCameraIdleListener(this.idleListener);
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    paramMenuItem = (FrameLayout)findViewById(2131689740);
    if (!this.submittedAddress)
    {
      this.submittedAddress = true;
      paramMenuItem.setVisibility(0);
      return true;
    }
    try
    {
      saveAddres();
      return true;
    }
    catch (JSONException paramMenuItem)
    {
      paramMenuItem.printStackTrace();
    }
    return true;
  }
  
  protected void onStop()
  {
    super.onStop();
    if ((ActivityCompat.checkSelfPermission(this, "android.permission.ACCESS_FINE_LOCATION") != 0) && (ActivityCompat.checkSelfPermission(this, "android.permission.ACCESS_COARSE_LOCATION") != 0)) {
      return;
    }
    this.locationManager.removeUpdates(this.locationChangeListener);
  }
  
  public void saveAddres()
    throws JSONException
  {
    if (!this.isMapReady) {
      return;
    }
    Object localObject1 = (EditText)findViewById(2131689734);
    Object localObject2 = (EditText)findViewById(2131689697);
    localObject1 = ((EditText)localObject1).getText().toString();
    localObject2 = ((EditText)localObject2).getText().toString();
    JSONObject localJSONObject1 = new JSONObject();
    JSONObject localJSONObject2 = new JSONObject();
    JSONObject localJSONObject3 = new JSONObject();
    JSONObject localJSONObject4 = new JSONObject();
    JSONArray localJSONArray = new JSONArray();
    localJSONArray.put(Double.toString(this.address.getLatitude()));
    localJSONArray.put(Double.toString(this.address.getLongitude()));
    localJSONObject2.put("type", "Point");
    localJSONObject2.put("coordinates", localJSONArray);
    localJSONObject3.put("streetName", this.address.getThoroughfare());
    localJSONObject3.put("streetNumber", this.address.getSubThoroughfare());
    localJSONObject3.put("locality", this.address.getLocality());
    localJSONObject3.put("sublocality", this.address.getSubLocality());
    localJSONObject3.put("state", this.address.getAdminArea());
    localJSONObject3.put("country", this.address.getCountryName());
    localJSONObject3.put("postalCode", "06700");
    localJSONObject4.put("reference", localObject1);
    localJSONObject4.put("geo", localJSONObject2);
    localJSONObject4.put("address", localJSONObject3);
    localJSONObject1.put("nickname", localObject2);
    localJSONObject1.put("location", localJSONObject4);
    if (this.isEditing) {
      localJSONObject1.put("id", this.addressData.getString("id"));
    }
    Customer.loadLocal().saveAddress(this, localJSONObject1, this.addResponse, this.isEditing);
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/vivemas/com/vivemasclientes/NuevaDireccion.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */