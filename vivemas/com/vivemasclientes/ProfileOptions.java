package vivemas.com.vivemasclientes;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import vivemas.com.vivemasclientes.Model.Model;

public class ProfileOptions
  extends AiDC_Activity
{
  public void direccion(View paramView)
  {
    startActivity(new Intent(getApplicationContext(), Direcciones.class));
  }
  
  public void facturacion(View paramView) {}
  
  public void logout(View paramView)
  {
    Model.getInstance().logout();
    paramView = new Intent(getApplicationContext(), Login.class);
    paramView.addFlags(67108864);
    startActivity(paramView);
    finish();
  }
  
  public void notificaciones(View paramView) {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130968615);
    setTitle("MI PERFIL");
  }
  
  public void password(View paramView) {}
  
  public void perfil(View paramView)
  {
    startActivity(new Intent(getApplicationContext(), EditProfile.class));
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/vivemas/com/vivemasclientes/ProfileOptions.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */