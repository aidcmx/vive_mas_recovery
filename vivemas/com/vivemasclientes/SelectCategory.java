package vivemas.com.vivemasclientes;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import com.loopj.android.http.AsyncHttpResponseHandler;
import cz.msebera.android.httpclient.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vivemas.com.vivemasclientes.Model.Model;

public class SelectCategory
  extends AiDC_Activity
{
  JSONArray categoriesArray;
  AsyncHttpResponseHandler categoriesResponse = new AsyncHttpResponseHandler()
  {
    public void onFailure(int paramAnonymousInt, Header[] paramAnonymousArrayOfHeader, byte[] paramAnonymousArrayOfByte, Throwable paramAnonymousThrowable)
    {
      SelectCategory.this.didFailResponse();
    }
    
    /* Error */
    public void onSuccess(int paramAnonymousInt, Header[] paramAnonymousArrayOfHeader, byte[] paramAnonymousArrayOfByte)
    {
      // Byte code:
      //   0: aload_0
      //   1: getfield 12	vivemas/com/vivemasclientes/SelectCategory$4:this$0	Lvivemas/com/vivemasclientes/SelectCategory;
      //   4: iconst_1
      //   5: invokestatic 31	vivemas/com/vivemasclientes/SelectCategory:access$002	(Lvivemas/com/vivemasclientes/SelectCategory;Z)Z
      //   8: pop
      //   9: new 33	java/lang/String
      //   12: dup
      //   13: aload_3
      //   14: ldc 35
      //   16: invokespecial 38	java/lang/String:<init>	([BLjava/lang/String;)V
      //   19: astore_2
      //   20: aload_0
      //   21: getfield 12	vivemas/com/vivemasclientes/SelectCategory$4:this$0	Lvivemas/com/vivemasclientes/SelectCategory;
      //   24: new 40	org/json/JSONArray
      //   27: dup
      //   28: aload_2
      //   29: invokespecial 43	org/json/JSONArray:<init>	(Ljava/lang/String;)V
      //   32: putfield 47	vivemas/com/vivemasclientes/SelectCategory:categoriesArray	Lorg/json/JSONArray;
      //   35: aload_0
      //   36: getfield 12	vivemas/com/vivemasclientes/SelectCategory$4:this$0	Lvivemas/com/vivemasclientes/SelectCategory;
      //   39: invokevirtual 50	vivemas/com/vivemasclientes/SelectCategory:didRecieveResponse	()V
      //   42: return
      //   43: astore_2
      //   44: return
      //   45: astore_2
      //   46: return
      //   47: astore_2
      //   48: return
      //   49: astore_2
      //   50: return
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	51	0	this	4
      //   0	51	1	paramAnonymousInt	int
      //   0	51	2	paramAnonymousArrayOfHeader	Header[]
      //   0	51	3	paramAnonymousArrayOfByte	byte[]
      // Exception table:
      //   from	to	target	type
      //   9	20	43	org/json/JSONException
      //   20	42	45	org/json/JSONException
      //   9	20	47	java/io/UnsupportedEncodingException
      //   20	42	49	java/io/UnsupportedEncodingException
    }
  };
  View.OnClickListener clickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      paramAnonymousView = (JSONObject)((ImageButton)paramAnonymousView).getTag();
      try
      {
        Model.getInstance().serviceCategories.put(paramAnonymousView.getString("id"));
        SelectCategory.this.finish();
        return;
      }
      catch (JSONException paramAnonymousView)
      {
        for (;;) {}
      }
    }
  };
  LinearLayout layout;
  private boolean mHasCategories = false;
  private boolean mHasProducts = false;
  View.OnClickListener onClickListenerButton = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      GridLayout localGridLayout = (GridLayout)paramAnonymousView.getTag();
      paramAnonymousView = (ImageButton)paramAnonymousView;
      if (localGridLayout.getVisibility() == 8)
      {
        localGridLayout.setVisibility(0);
        paramAnonymousView.setImageResource(2130837923);
        return;
      }
      localGridLayout.setVisibility(8);
      paramAnonymousView.setImageResource(2130837922);
    }
  };
  View.OnClickListener onClickListenerText = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      GridLayout localGridLayout = (GridLayout)paramAnonymousView.getTag();
      paramAnonymousView = (ImageButton)paramAnonymousView.getTag(2131230739);
      if (localGridLayout.getVisibility() == 8)
      {
        localGridLayout.setVisibility(0);
        paramAnonymousView.setImageResource(2130837923);
        return;
      }
      localGridLayout.setVisibility(8);
      paramAnonymousView.setImageResource(2130837922);
    }
  };
  JSONArray productsArray;
  AsyncHttpResponseHandler productsResponse = new AsyncHttpResponseHandler()
  {
    public void onFailure(int paramAnonymousInt, Header[] paramAnonymousArrayOfHeader, byte[] paramAnonymousArrayOfByte, Throwable paramAnonymousThrowable)
    {
      SelectCategory.this.didFailResponse();
    }
    
    /* Error */
    public void onSuccess(int paramAnonymousInt, Header[] paramAnonymousArrayOfHeader, byte[] paramAnonymousArrayOfByte)
    {
      // Byte code:
      //   0: aload_0
      //   1: getfield 12	vivemas/com/vivemasclientes/SelectCategory$5:this$0	Lvivemas/com/vivemasclientes/SelectCategory;
      //   4: iconst_1
      //   5: invokestatic 31	vivemas/com/vivemasclientes/SelectCategory:access$102	(Lvivemas/com/vivemasclientes/SelectCategory;Z)Z
      //   8: pop
      //   9: new 33	java/lang/String
      //   12: dup
      //   13: aload_3
      //   14: ldc 35
      //   16: invokespecial 38	java/lang/String:<init>	([BLjava/lang/String;)V
      //   19: astore_2
      //   20: aload_0
      //   21: getfield 12	vivemas/com/vivemasclientes/SelectCategory$5:this$0	Lvivemas/com/vivemasclientes/SelectCategory;
      //   24: new 40	org/json/JSONArray
      //   27: dup
      //   28: aload_2
      //   29: invokespecial 43	org/json/JSONArray:<init>	(Ljava/lang/String;)V
      //   32: putfield 47	vivemas/com/vivemasclientes/SelectCategory:productsArray	Lorg/json/JSONArray;
      //   35: aload_0
      //   36: getfield 12	vivemas/com/vivemasclientes/SelectCategory$5:this$0	Lvivemas/com/vivemasclientes/SelectCategory;
      //   39: invokevirtual 50	vivemas/com/vivemasclientes/SelectCategory:didRecieveResponse	()V
      //   42: invokestatic 56	vivemas/com/vivemasclientes/Model/Model:getInstance	()Lvivemas/com/vivemasclientes/Model/Model;
      //   45: aload_0
      //   46: getfield 12	vivemas/com/vivemasclientes/SelectCategory$5:this$0	Lvivemas/com/vivemasclientes/SelectCategory;
      //   49: getfield 47	vivemas/com/vivemasclientes/SelectCategory:productsArray	Lorg/json/JSONArray;
      //   52: invokevirtual 60	org/json/JSONArray:toString	()Ljava/lang/String;
      //   55: invokevirtual 63	vivemas/com/vivemasclientes/Model/Model:saveLocalActivities	(Ljava/lang/String;)V
      //   58: return
      //   59: astore_2
      //   60: getstatic 69	java/lang/System:out	Ljava/io/PrintStream;
      //   63: aload_2
      //   64: invokevirtual 75	java/io/PrintStream:println	(Ljava/lang/Object;)V
      //   67: return
      //   68: astore_2
      //   69: return
      //   70: astore_2
      //   71: return
      //   72: astore_2
      //   73: goto -13 -> 60
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	76	0	this	5
      //   0	76	1	paramAnonymousInt	int
      //   0	76	2	paramAnonymousArrayOfHeader	Header[]
      //   0	76	3	paramAnonymousArrayOfByte	byte[]
      // Exception table:
      //   from	to	target	type
      //   9	20	59	java/io/UnsupportedEncodingException
      //   9	20	68	org/json/JSONException
      //   20	58	70	org/json/JSONException
      //   20	58	72	java/io/UnsupportedEncodingException
    }
  };
  
  public void didFailResponse()
  {
    stoppedLoading();
    doAlert("Error", "No pudimos contactar al servidor.");
  }
  
  public void didRecieveResponse()
    throws JSONException
  {
    if ((this.mHasProducts) && (this.mHasCategories))
    {
      stoppedLoading();
      int i = 0;
      while (i < this.categoriesArray.length())
      {
        this.categoriesArray.getJSONObject(i).put("activities", new JSONArray());
        i += 1;
      }
      i = 0;
      while (i < this.productsArray.length())
      {
        String str = this.productsArray.getJSONObject(i).getString("category");
        int j = 0;
        while (j < this.categoriesArray.length())
        {
          JSONObject localJSONObject = this.categoriesArray.getJSONObject(j);
          if (localJSONObject.getString("id").equals(str))
          {
            localJSONObject.getJSONArray("activities").put(this.productsArray.getJSONObject(i));
            this.categoriesArray.put(j, localJSONObject);
          }
          j += 1;
        }
        i += 1;
      }
      initLayout();
    }
  }
  
  public void initLayout()
    throws JSONException
  {
    LayoutInflater localLayoutInflater = (LayoutInflater)getBaseContext().getSystemService("layout_inflater");
    int i = 0;
    while (i < this.categoriesArray.length())
    {
      LinearLayout localLinearLayout = (LinearLayout)localLayoutInflater.inflate(2130968603, null);
      Object localObject1 = (GridLayout)localLinearLayout.findViewById(2131689688);
      Object localObject2 = (ImageButton)localLinearLayout.findViewById(2131689687);
      ((ImageButton)localObject2).setTag(localObject1);
      ((ImageButton)localObject2).setImageResource(2130837922);
      ((ImageButton)localObject2).setOnClickListener(this.onClickListenerButton);
      JSONArray localJSONArray = this.categoriesArray.getJSONObject(i).getJSONArray("activities");
      TextView localTextView = (TextView)localLinearLayout.findViewById(2131689686);
      localTextView.setText(this.categoriesArray.getJSONObject(i).getString("name"));
      localTextView.setTag(localObject1);
      localTextView.setTag(2131230739, localObject2);
      localTextView.setOnClickListener(this.onClickListenerText);
      double d2 = localJSONArray.length() / 2;
      double d1 = d2;
      if (localJSONArray.length() % 2 != 0) {
        d1 = d2 + 1.0D;
      }
      ((GridLayout)localObject1).setColumnCount(2);
      ((GridLayout)localObject1).setRowCount((int)d1);
      int j = 0;
      while (j < localJSONArray.length())
      {
        localObject2 = (LinearLayout)localLayoutInflater.inflate(2130968625, null);
        localTextView = (TextView)((LinearLayout)localObject2).findViewById(2131689786);
        ImageButton localImageButton = (ImageButton)((LinearLayout)localObject2).findViewById(2131689785);
        localImageButton.setTag(localJSONArray.get(j));
        localImageButton.setOnClickListener(this.clickListener);
        localTextView.setText(localJSONArray.getJSONObject(j).getString("name"));
        ((GridLayout)localObject1).addView((View)localObject2, j);
        j += 1;
      }
      localObject1 = new LinearLayout.LayoutParams(-1, -2);
      ((LinearLayout.LayoutParams)localObject1).setMargins(10, 10, 10, 10);
      this.layout.addView(localLinearLayout, (ViewGroup.LayoutParams)localObject1);
      i += 1;
    }
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130968616);
    setTitle("AGREGAR ACTIVIDAD");
    loading(this);
    Model.getInstance().getCategories(this, this.categoriesResponse);
    Model.getInstance().getProducts(this, this.productsResponse);
    this.layout = ((LinearLayout)findViewById(2131689749));
  }
  
  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    getMenuInflater().inflate(2131755011, paramMenu);
    return true;
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    finish();
    return true;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/vivemas/com/vivemasclientes/SelectCategory.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */