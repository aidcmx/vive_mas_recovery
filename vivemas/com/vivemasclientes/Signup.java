package vivemas.com.vivemasclientes;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import com.loopj.android.http.AsyncHttpResponseHandler;
import cz.msebera.android.httpclient.Header;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vivemas.com.vivemasclientes.Model.Customer;
import vivemas.com.vivemasclientes.Model.Model;

public class Signup
  extends AiDC_Activity
{
  private static final int ASK_MULTIPLE_PERMISSION_REQUEST_CODE = 0;
  private static final int REQUEST_CAPTURE_FROM_CAMERA = 1;
  private static final int REQUEST_PICK_FROM_FILE = 2;
  boolean acceptedTerms = false;
  DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener()
  {
    public void onDateSet(DatePicker paramAnonymousDatePicker, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3)
    {
      Signup.this.myCalendar.set(1, paramAnonymousInt1);
      Signup.this.myCalendar.set(2, paramAnonymousInt2);
      Signup.this.myCalendar.set(5, paramAnonymousInt3);
      Signup.this.updateLabel();
    }
  };
  EditText datePicker;
  File file;
  boolean imageSet = false;
  boolean isWoman = true;
  Calendar myCalendar = Calendar.getInstance();
  AsyncHttpResponseHandler pictureResponse = new AsyncHttpResponseHandler()
  {
    public void onFailure(int paramAnonymousInt, Header[] paramAnonymousArrayOfHeader, byte[] paramAnonymousArrayOfByte, Throwable paramAnonymousThrowable)
    {
      try
      {
        paramAnonymousArrayOfHeader = new String(paramAnonymousArrayOfByte, "UTF-8");
        Signup.this.doAlert("error: ", "error " + paramAnonymousInt + " : " + paramAnonymousArrayOfHeader);
        return;
      }
      catch (UnsupportedEncodingException paramAnonymousArrayOfHeader) {}
    }
    
    public void onFinish()
    {
      super.onFinish();
      Signup.this.stoppedLoading();
    }
    
    public void onStart()
    {
      super.onStart();
      Signup.this.loading(Signup.this);
    }
    
    public void onSuccess(int paramAnonymousInt, Header[] paramAnonymousArrayOfHeader, byte[] paramAnonymousArrayOfByte)
    {
      paramAnonymousArrayOfHeader = new Intent(Signup.this, Home.class);
      Signup.this.startActivity(paramAnonymousArrayOfHeader);
      Signup.this.finish();
    }
  };
  AsyncHttpResponseHandler signupResponse = new AsyncHttpResponseHandler()
  {
    public void onFailure(int paramAnonymousInt, Header[] paramAnonymousArrayOfHeader, byte[] paramAnonymousArrayOfByte, Throwable paramAnonymousThrowable)
    {
      try
      {
        paramAnonymousArrayOfHeader = new String(paramAnonymousArrayOfByte, "UTF-8");
        Signup.this.doAlert("error: ", "error " + paramAnonymousInt + " : " + paramAnonymousArrayOfHeader);
        return;
      }
      catch (UnsupportedEncodingException paramAnonymousArrayOfHeader) {}
    }
    
    public void onFinish()
    {
      super.onFinish();
      if (!Signup.this.imageSet) {
        Signup.this.stoppedLoading();
      }
    }
    
    public void onStart()
    {
      super.onStart();
      Signup.this.loading(Signup.this);
    }
    
    /* Error */
    public void onSuccess(int paramAnonymousInt, Header[] paramAnonymousArrayOfHeader, byte[] paramAnonymousArrayOfByte)
    {
      // Byte code:
      //   0: new 22	java/lang/String
      //   3: dup
      //   4: aload_3
      //   5: ldc 24
      //   7: invokespecial 27	java/lang/String:<init>	([BLjava/lang/String;)V
      //   10: astore_2
      //   11: new 74	org/json/JSONObject
      //   14: dup
      //   15: aload_2
      //   16: invokespecial 77	org/json/JSONObject:<init>	(Ljava/lang/String;)V
      //   19: astore_2
      //   20: invokestatic 83	vivemas/com/vivemasclientes/Model/Model:getInstance	()Lvivemas/com/vivemasclientes/Model/Model;
      //   23: aload_2
      //   24: ldc 85
      //   26: invokevirtual 89	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
      //   29: aload_2
      //   30: ldc 91
      //   32: invokevirtual 89	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
      //   35: invokevirtual 94	vivemas/com/vivemasclientes/Model/Model:loginToApp	(Ljava/lang/String;Ljava/lang/String;)V
      //   38: invokestatic 99	vivemas/com/vivemasclientes/Model/AsyncClient:setToken	()V
      //   41: aload_0
      //   42: getfield 12	vivemas/com/vivemasclientes/Signup$4:this$0	Lvivemas/com/vivemasclientes/Signup;
      //   45: aload_2
      //   46: ldc 85
      //   48: invokevirtual 89	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
      //   51: invokevirtual 102	vivemas/com/vivemasclientes/Signup:sendImage	(Ljava/lang/String;)V
      //   54: return
      //   55: astore_2
      //   56: return
      //   57: astore_2
      //   58: return
      //   59: astore_2
      //   60: return
      //   61: astore_2
      //   62: return
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	63	0	this	4
      //   0	63	1	paramAnonymousInt	int
      //   0	63	2	paramAnonymousArrayOfHeader	Header[]
      //   0	63	3	paramAnonymousArrayOfByte	byte[]
      // Exception table:
      //   from	to	target	type
      //   0	11	55	org/json/JSONException
      //   11	54	57	org/json/JSONException
      //   0	11	59	java/io/UnsupportedEncodingException
      //   11	54	61	java/io/UnsupportedEncodingException
    }
  };
  
  private void updateLabel()
  {
    SimpleDateFormat localSimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
    this.datePicker.setText(localSimpleDateFormat.format(this.myCalendar.getTime()));
  }
  
  public void camera(View paramView)
  {
    paramView = new AlertDialog.Builder(this);
    paramView.setTitle("Fotografía");
    paramView.setMessage("¿Cómo vas a obtener la fotografía?");
    paramView.setPositiveButton("Cancelar", new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        paramAnonymousDialogInterface.cancel();
      }
    });
    paramView.setNeutralButton("Cámara", new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        Signup.this.getFromCamera();
      }
    });
    paramView.setNegativeButton("Galería", new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        Signup.this.getFromGallery();
      }
    });
    paramView.create().show();
  }
  
  public void getFromCamera()
  {
    if (ActivityCompat.checkSelfPermission(this, "android.permission.CAMERA") != 0)
    {
      toast("No has dado permisos para usar tu cámara. Necesitas este permiso para seguir.");
      return;
    }
    Object localObject = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "ViveMas");
    if ((!((File)localObject).exists()) && (!((File)localObject).mkdirs()))
    {
      Log.d("ViveMas", "failed to create directory");
      return;
    }
    String str = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
    this.file = new File(((File)localObject).getPath() + File.separator + "IMG_" + str + ".png");
    localObject = new Intent("android.media.action.IMAGE_CAPTURE");
    ((Intent)localObject).putExtra("output", Uri.fromFile(this.file));
    startActivityForResult((Intent)localObject, 1);
  }
  
  public void getFromGallery()
  {
    Intent localIntent = new Intent();
    localIntent.setType("image/*");
    localIntent.setAction("android.intent.action.GET_CONTENT");
    startActivityForResult(Intent.createChooser(localIntent, "Complete action using"), 2);
  }
  
  public void man(View paramView)
  {
    ((ImageButton)findViewById(2131689701)).setImageResource(2130837977);
    ((ImageButton)findViewById(2131689703)).setImageResource(2130837978);
    this.isWoman = false;
  }
  
  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    ImageButton localImageButton = (ImageButton)findViewById(2131689695);
    if (paramInt2 != -1) {}
    for (;;)
    {
      return;
      if (paramInt1 == 1)
      {
        Uri.fromFile(this.file);
        return;
      }
      if (paramInt1 == 2) {
        try
        {
          if (saveBitmapToStorage(getBitmapFromUri(paramIntent.getData())))
          {
            Uri.fromFile(this.file);
            return;
          }
        }
        catch (IOException paramIntent)
        {
          paramIntent.printStackTrace();
        }
      }
    }
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130968622);
    setTitle("Registro");
    if ((ActivityCompat.checkSelfPermission(this, "android.permission.CAMERA") != 0) || (ActivityCompat.checkSelfPermission(this, "android.permission.READ_EXTERNAL_STORAGE") != 0)) {
      ActivityCompat.requestPermissions(this, new String[] { "android.permission.CAMERA", "android.permission.READ_EXTERNAL_STORAGE" }, 0);
    }
    this.datePicker = ((EditText)findViewById(2131689698));
    this.datePicker.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        paramAnonymousView = new DatePickerDialog(Signup.this, Signup.this.date, Signup.this.myCalendar.get(1), Signup.this.myCalendar.get(2), Signup.this.myCalendar.get(5));
        Calendar localCalendar = Calendar.getInstance();
        localCalendar.add(1, -18);
        paramAnonymousView.getDatePicker().setMaxDate(localCalendar.getTimeInMillis());
        paramAnonymousView.show();
      }
    });
  }
  
  public void sendImage(String paramString)
    throws JSONException
  {
    if (!this.imageSet)
    {
      startActivity(new Intent(this, Home.class));
      stoppedLoading();
      finish();
      return;
    }
    Model.getInstance().uploadPictureRegistration(this, this.pictureResponse, this.file, paramString);
  }
  
  public void submit(View paramView)
  {
    Object localObject1 = (EditText)findViewById(2131689716);
    Object localObject3 = (EditText)findViewById(2131689696);
    Object localObject4 = (EditText)findViewById(2131689698);
    Object localObject5 = (EditText)findViewById(2131689553);
    Object localObject2 = (EditText)findViewById(2131689775);
    Object localObject6 = (EditText)findViewById(2131689776);
    paramView = (EditText)findViewById(2131689690);
    localObject1 = ((EditText)localObject1).getText().toString();
    localObject3 = ((EditText)localObject3).getText().toString();
    localObject4 = ((EditText)localObject4).getText().toString();
    localObject5 = ((EditText)localObject5).getText().toString();
    localObject2 = ((EditText)localObject2).getText().toString();
    Object localObject7 = ((EditText)localObject6).getText().toString();
    localObject6 = paramView.getText().toString();
    if ((((String)localObject1).matches("")) || (((String)localObject3).matches("")) || (((String)localObject4).matches("")) || (((String)localObject5).matches("")) || (((String)localObject2).matches("")) || (((String)localObject7).matches("")) || (((String)localObject6).matches("")))
    {
      doAlert("Error", "Debes llenar todos los campos.");
      return;
    }
    if (((String)localObject6).length() != 10)
    {
      doAlert("Error", "El número debe ser de 10 dígitos.");
      return;
    }
    if (((String)localObject2).length() < 8)
    {
      doAlert("Error", "La contraseña debe ser de por lo menos 8 caracteres.");
      return;
    }
    if (!((String)localObject2).matches((String)localObject7))
    {
      doAlert("Error", "Las contraseñas no coinciden.");
      return;
    }
    if (!this.acceptedTerms)
    {
      doAlert("Error", "Debes aceptar los términos y condiciones");
      return;
    }
    if (this.isWoman) {}
    for (paramView = "female";; paramView = "male") {
      try
      {
        localObject7 = new JSONObject();
        ((JSONObject)localObject7).put("first", localObject1);
        ((JSONObject)localObject7).put("last", localObject3);
        localObject1 = new JSONObject();
        ((JSONObject)localObject1).put("name", localObject7);
        ((JSONObject)localObject1).put("birthday", localObject4);
        ((JSONObject)localObject1).put("gender", paramView);
        ((JSONObject)localObject1).put("kinship", "self");
        paramView = new JSONArray();
        paramView.put(localObject1);
        localObject1 = ((String)localObject5).replace(" ", "");
        localObject3 = new JSONObject();
        ((JSONObject)localObject3).put("name", localObject7);
        ((JSONObject)localObject3).put("beneficiaries", paramView);
        ((JSONObject)localObject3).put("email", ((String)localObject1).replace(" ", ""));
        ((JSONObject)localObject3).put("password", localObject2);
        ((JSONObject)localObject3).put("telephone", localObject6);
        ((JSONObject)localObject3).put("meta", new JSONObject());
        Customer.register(this, (JSONObject)localObject3, this.signupResponse);
        return;
      }
      catch (JSONException paramView)
      {
        return;
      }
    }
  }
  
  public void termsButton(View paramView)
  {
    paramView = (ImageButton)findViewById(2131689778);
    if (!this.acceptedTerms) {}
    for (boolean bool = true;; bool = false)
    {
      this.acceptedTerms = bool;
      if (!this.acceptedTerms) {
        break;
      }
      paramView.setImageResource(2130837978);
      return;
    }
    paramView.setImageResource(2130837977);
  }
  
  public void termsClick(View paramView)
  {
    startActivity(new Intent(this, Terms.class));
  }
  
  public void woman(View paramView)
  {
    ((ImageButton)findViewById(2131689701)).setImageResource(2130837978);
    ((ImageButton)findViewById(2131689703)).setImageResource(2130837977);
    this.isWoman = true;
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/vivemas/com/vivemasclientes/Signup.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */