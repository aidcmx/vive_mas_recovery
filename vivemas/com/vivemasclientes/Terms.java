package vivemas.com.vivemasclientes;

import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class Terms
  extends AiDC_Activity
{
  WebView mWebview;
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130968623);
    setTitle("Términos y condiciones");
    this.mWebview = ((WebView)findViewById(2131689773));
    this.mWebview.getSettings().setJavaScriptEnabled(true);
    this.mWebview.getSettings().setLoadWithOverviewMode(true);
    this.mWebview.getSettings().setUseWideViewPort(true);
    this.mWebview.setWebViewClient(new WebViewClient()
    {
      public void onReceivedError(WebView paramAnonymousWebView, int paramAnonymousInt, String paramAnonymousString1, String paramAnonymousString2)
      {
        Terms.this.toast("No pudimos cargar los términos y condiciones");
      }
    });
    this.mWebview.loadUrl("http://www.apple.com/legal/internet-services/itunes/us/terms.html");
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/vivemas/com/vivemasclientes/Terms.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */