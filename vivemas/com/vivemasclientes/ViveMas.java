package vivemas.com.vivemasclientes;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.support.multidex.MultiDex;
import android.util.Base64;
import android.util.Log;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class ViveMas
  extends Application
{
  public void attachBaseContext(Context paramContext)
  {
    super.attachBaseContext(paramContext);
    MultiDex.install(this);
  }
  
  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    super.onConfigurationChanged(paramConfiguration);
  }
  
  public void onCreate()
  {
    int i = 0;
    super.onCreate();
    FacebookSdk.sdkInitialize(getApplicationContext());
    AppEventsLogger.activateApp(this);
    try
    {
      Signature[] arrayOfSignature = getPackageManager().getPackageInfo("vivemas.com.vivemasclientes", 64).signatures;
      int j = arrayOfSignature.length;
      while (i < j)
      {
        Signature localSignature = arrayOfSignature[i];
        MessageDigest localMessageDigest = MessageDigest.getInstance("SHA");
        localMessageDigest.update(localSignature.toByteArray());
        Log.d("KeyHash:", Base64.encodeToString(localMessageDigest.digest(), 0));
        i += 1;
      }
      return;
    }
    catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
    {
      return;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException) {}
  }
  
  public void onLowMemory()
  {
    super.onLowMemory();
  }
  
  public void onTerminate()
  {
    super.onTerminate();
  }
}


/* Location:              /Users/franciscocatala/Desktop/ViveMasClientesReleseases/tempApk/app-dex2jar.jar!/vivemas/com/vivemasclientes/ViveMas.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */